///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;

using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Collections.Generic;

using log4net;
using System.Diagnostics;

namespace KlausEnt.KEP.Kaneva
{
    /// <summary>
    /// Summary description for KanevaGlobals.
    /// </summary>
    public class KanevaGlobals
    {
        public KanevaGlobals ()
        {
        }

        /// <summary>
        /// IsPottyMouth
        /// </summary>
        public static bool IsPottyMouth (string strText)
        {
            try
            {
                strText = strText.ToUpper ();

                if
                    (
                    (strText.IndexOf ("SHIT") > -1) || (strText.IndexOf ("FUCK") > -1)
                    || (strText.IndexOf ("PUSSY") > -1) || strText.Equals ("STFU")
                    || strText.Equals ("COCK") || strText.Equals ("COCKSUCKER")
                    || strText.Equals ("TITS") || strText.Equals ("WHORE") || strText.Equals ("PENIS")
                    || strText.Equals ("FAG") || strText.Equals ("HOMO") || strText.Equals ("LESBO")
                    || strText.Equals ("QUEER") || strText.Equals ("NIGGER") || strText.Equals ("NIGGA")
                    || strText.Equals ("WTF") || strText.Equals ("ANAL")
                    || strText.Equals ("BOOB") || strText.Equals ("BOOBS")
                    || strText.Equals ("ASSHOLE")
                    || strText.Equals ("CUNT") || strText.Equals ("VAGINA")
                    || strText.Equals ("PRICK")
                    )
                {
                    return true;
                }

                //				#if KSERVICE
                //				{
                //					return false;
                //				}
                //				#else
                //				{
                //					// Else look up the database table
                //					DataTable dtPottyMouth = WebCache.GetPottyWords ();
                //					return (dtPottyMouth.Select ("bad_word = '" + CleanText (strText) + "'").Length > 0);
                //				}
                //				#endif
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error in IsPottyMouth ", exc);
            }
            return false;
        }

        /// <summary>
        /// Clean text for db insertion
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public static string CleanText (string strText)
        {
            strText = strText.Replace ("'", "''");
            strText = strText.Replace ("\\", "\\\\");
            return strText.Replace ("\"", "\"\"");
        }

        /// <summary>
        /// Clean text for parature API
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public static string CleanParatureText (string strText)
        {
            strText = strText.Replace ("'", "&apos;");
            strText = strText.Replace ("''", "&quot;");
            strText = strText.Replace ("<", "&lt;");
            strText = strText.Replace (">", "&gt;");
            return strText.Replace ("&", "&amp;");
        }

        /// <summary>
        /// Clean text for db insertion
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public static string CleanHtmlEditorText (string strText)
        {
            // This is the only cleaning we need done with the HTML editor
            // It already does ".
            return strText.Replace ("'", "''");
        }

        /// <summary>
        /// Clean text for javascript
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public static string CleanJavascript (string strText)
        {
            return strText.Replace ("'", "\\'");
        }

        /// <summary>
        /// Clean text for javascript
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public static string CleanJavascriptFull (string strText)
        {
            return (strText.Replace ("'", "\\'")).Replace ("\r\n", "\\r\\n");
        }

        /// <summary>
        /// Get the time difference
        /// </summary>
        /// <returns></returns>
        public static string GetDateTimeDifference (Object dtStartDate)
        {
            DateTime dtCurrentTime = KanevaGlobals.GetCurrentDateTime ();
            TimeSpan tsDifference;

            if (dtStartDate.Equals (DBNull.Value))
            {
                return "";
            }

            tsDifference = dtCurrentTime - (DateTime) dtStartDate;
            return tsDifference.Days.ToString ("#,##0") + "d " + tsDifference.Hours.ToString ("#0") + "h " + tsDifference.Minutes.ToString ("#0") + "m";
        }

        //		/// <summary>
        //		/// Get the formated age for display
        //		/// </summary>
        //		/// <param name="dtBirthDate"></param>
        //		/// <returns></returns>
        //		public static string GetFormatedAge (Object dtBirthDate)
        //		{
        //			return (GetAgeInYears (dtBirthDate).ToString ("#,##0"));
        //		}

        /// <summary>
        /// Get someones age in years - doesn't take objects or nulls
        /// </summary>
        /// <param name="birthDay"></param>
        /// <returns></returns>
        /// 
        public static int GetAgeInYears (DateTime birthDay)
        {
            //retreive current time from database
            DateTime dtCurrentTime = KanevaGlobals.GetCurrentDateTime ();

            //.25 an attempt to correct for leap years
            double age = (((TimeSpan) dtCurrentTime.Subtract (birthDay.Date)).Days) / 365.25;
            //special case check for day of birthday scenarios
            if ((birthDay.Month == dtCurrentTime.Month) && (birthDay.Day == dtCurrentTime.Day))
            {
                return Convert.ToInt32 (Math.Ceiling (age));
            }
            return Convert.ToInt32 (Math.Floor (age));
        }


        /// <summary>
        /// Get the current date time
        /// </summary>
        /// <returns></returns>
        public static DateTime GetCurrentDateTime ()
        {
            return GetDatabaseUtility ().GetCurrentDateTime ();
        }

        /// <summary>
        /// This method shortens a string to a certain length and
        /// adds ... when the string is longer than a specified
        /// maximum length.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string TruncateWithEllipsis (string text, int length)
        {
            if (text.Length > length)
                text = text.Substring (0, length) + "...";
            return text;
        }

        /// <summary>
        /// This method chops off the end of a string when the
        /// string is longer than a maximum length.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string Truncate (string text, int length)
        {
            if (text.Length > length)
                text = text.Substring (0, length);
            return text;
        }

        /// <summary>
        /// Represents the path of the current application.
        /// </summary>
        public static string AppPath
        {
            get
            {
                if (HttpContext.Current.Request.ApplicationPath == "/")
                    return String.Empty;
                return HttpContext.Current.Request.ApplicationPath;
            }
        }

        public static string ResolveAbsoluteUrl (string Url)
        {
            if (HttpContext.Current.Request.IsSecureConnection)
                return "https://" + Url;
            else
                return "http://" + Url;
        }


        /// <summary>
        /// Removes everything from a request except for the page name.
        /// </summary>
        /// <param name="requestPath"></param>
        /// <returns></returns>
        public static string GetPageName (string requestPath)
        {
            // Remove query string
            if (requestPath.IndexOf ('?') != -1)
                requestPath = requestPath.Substring (0, requestPath.IndexOf ('?'));

            // Remove base path
            return requestPath.Remove (0, requestPath.LastIndexOf ("/"));
        }

        /// <summary>
        /// Retrieves Blogs per page from Web.Config file.
        /// </summary>
        public static int DatabaseType
        {
            get
            {
                switch (System.Configuration.ConfigurationManager.AppSettings["databaseType"])
                {
                    case "SQLServer":
                        {
                            return (int) Constants.eSUPPORTED_DATABASES.SQLSERVER;
                        }
                    case "MySQL":
                        {
                            return (int) Constants.eSUPPORTED_DATABASES.MYSQL;
                        }
                }

                return 0;
            }
        }

        public static string WokPatcherUrl
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["wokPatcherUrl"];
                if (ret == null)
                    ret = "http://www.kaneva.com/patch/wok";
                return ret;
            }
        }

        public static string _3dAppDownloadResourcesURL
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["_3dAppDownloadResourcesURL"];
                if (ret == null)
                    ret = "http://docs.kaneva.com/";
                return ret;
            }
        }

        /// <summary>
        /// Retrieves the Announce URL
        /// </summary>
        public static string AnnounceURL
        {
            get
            {
                return "http://" + System.Configuration.ConfigurationManager.AppSettings["tracker_ip"] + ":" + System.Configuration.ConfigurationManager.AppSettings["tracker_port"] + "/announce"; ;
            }
        }

        /// <summary>
        /// Retrieves Blogs per page from Web.Config file.
        /// </summary>
        public static int BlogsPerPage
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["BlogsPerPage"]);
            }
        }

        /// <summary>
        /// Retrieves Assets per page from Web.Config file.
        /// </summary>
        public static int AssetsPerPage
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["AssetsPerPage"]);
            }
        }

        /// <summary>
        /// Retrieves Servers per page from Web.Config file.
        /// </summary>
        public static int ServersPerPage
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["ServersPerPage"]);
            }
        }

        /// <summary>
        /// Retrieves ServersShowDaysOld per page from Web.Config file.
        /// </summary>
        public static int ServersShowDaysOld
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ServersShowDaysOld"] == null)
                {
                    return 30;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["ServersShowDaysOld"]);
                }
            }
        }

        /// <summary>
        /// Retrieves cutoff age of minor
        /// </summary>
        public static int MinorCutOffAge
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["AgeCutOff"] == null)
                {
                    return 13;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["AgeCutOff"]);
                }
            }
        }

        /// <summary>
        /// Retrieves official Access(Mature) pass
        /// </summary>
        public static int AccessPassPromotionID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["accessPassPromotionId"] == null)
                {
                    return 1;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["accessPassPromotionId"]);
                }
            }
        }

        /// <summary>
        /// Retrieves official Access(Mature) pass
        /// </summary>
        public static int AccessPassGroupID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["accessPassGroupId"] == null)
                {
                    return 1;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["accessPassGroupId"]);
                }
            }
        }

        /// <summary>
        /// Retrieves official VIP pass
        /// </summary>
        public static int VipPassGroupID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["vipPassGroupId"] == null)
                {
                    return 2;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["vipPassGroupId"]);
                }
            }
        }

        /// <summary>
        /// Retrieves official VIP pass
        /// </summary>
        public static int VIPPassPromotionID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["vipPassPromotionId"] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["vipPassPromotionId"]);
                }
            }
        }

        /// <summary>
        /// Retrieves Communities per page from Web.Config file.
        /// </summary>
        public static int CommunitiesPerPage
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["CommunitiesPerPage"]);
            }
        }

        /// <summary>
        /// Retrieves World Objects per page from Web.Config file.
        /// </summary>
        public static int WorldObjectsPerPage
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["WorldObjectsPerPage"]);
            }
        }

        /// <summary>
        /// Retrieves Members per page from Web.Config file.
        /// </summary>
        public static int MembersPerPage
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["MembersPerPage"]);
            }
        }

        /// <summary>
        /// Retrieves Topics per page from Web.Config file.
        /// </summary>
        public static int TopicsPerPage
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["TopicsPerPage"]);
            }
        }

        /// <summary>
        /// Retrieves Threads per page from Web.Config file.
        /// </summary>
        public static int ThreadsPerPage
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["ThreadsPerPage"]);
            }
        }

        /// <summary>
        /// Retrieves TransactionsPerPage per page from Web.Config file.
        /// </summary>
        public static int TransactionsPerPage
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["TransactionsPerPage"]);
            }
        }

        /// <summary>
        /// Retrieves CreditTransactionsPerPage per page from Web.Config file.
        /// </summary>
        public static int CreditTransactionsPerPage
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["CreditTransactionsPerPage"]);
            }
        }

        /// <summary>
        /// Retrieves Friends per page from Web.Config file.
        /// </summary>
        public static int FriendsPerPage
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["FriendsPerPage"]);
            }
        }

        /// <summary>
        /// Retrieves SearchResultsPerPage per page from Web.Config file.
        /// </summary>
        public static int SearchResultsPerPage
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["SearchResultsPerPage"]);
            }
        }

        /// <summary>
        /// Use Search Farm?
        /// </summary>
        public static bool UseSearchFarm
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["UseSearchFarm"] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["UseSearchFarm"]);
                }
            }
        }

        /// <summary>
        /// Max uploadable image size
        /// </summary>
        public static int MaxUploadedImageSize
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["MaxUploadedImageSize"]);
            }
        }

        /// <summary>
        /// Max uploadable Avatar size
        /// </summary>
        public static int MaxUploadedAvatarSize
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["MaxUploadedAvatarSize"]); ;
            }
        }

        /// <summary>
        /// Max uploadable Avatar size
        /// </summary>
        public static int MaxUploadedBannerSize
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["MaxUploadedBannerSize"]); ;
            }
        }

        /// <summary>
        /// Max uploadable Screenshot size
        /// </summary>
        public static int MaxUploadedScreenShotSize
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["MaxUploadedScreenShotSize"]);
            }
        }

        /// <summary>
        /// MaxNumberOfCommunitiesPerUser
        /// </summary>
        public static int MaxNumberOfCommunitiesPerUser
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["MaxNumberOfCommunitiesPerUser"]);
            }
        }

        /// <summary>
        /// MaxNumberOfLogicalAccounts
        /// </summary>
        public static int MaxNumberOfLogicalAccounts
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["MaxNumberOfLogicalAccounts"]);
            }
        }

        /// <summary>
        /// MaxCommentLength
        /// </summary>
        public static int MaxCommentLength
        {
            get
            {
                try
                {
                    int len = Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["MaxCommentLength"]);

                    if (len > 0)
                    {
                        return len;
                    }
                    else
                    {
                        return 256;
                    }
                }
                catch
                {
                    return 256;
                }
            }
        }

        /// <summary>
        /// MaxBlastLength
        /// </summary>
        public static int MaxBlastLength
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["MaxBlastLength"] == null)
                {
                    return 140;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["MaxBlastLength"]);
                }
            }
        }

        /// <summary>
        /// MaxContestLength
        /// </summary>
        public static int MaxContestLength
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["MaxContestLength"] == null)
                {
                    return 5000;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["MaxContestLength"]);
                }
            }
        }

        /// <summary>
        /// MaxNumberNonFriendMessagesPerDay
        /// </summary>
        public static int MaxNumberNonFriendMessagesPerDay
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["MaxNumberNonFriendMessagesPerDay"] == null)
                {
                    return 20;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["MaxNumberNonFriendMessagesPerDay"]);
                }
            }
        }

        /// <summary>
        /// NumberOfDaysDownloadIsAvailable
        /// </summary>
        public static int NumberOfDaysDownloadIsAvailable
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["NumberOfDaysDownloadIsAvailable"]);
            }
        }

        // NumberOfDaysDownloadStopDate
        public static int NumberOfDaysDownloadStopDate
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["NumberOfDaysDownloadStopDate"]);
            }
        }

        /// <summary>
        /// DefaultPercentRoyaltyPaid
        /// </summary>
        public static int DefaultPercentRoyaltyPaid
        {
            get
            {
                return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["DefaultPercentRoyaltyPaid"]);
            }
        }

        /// <summary>
        /// Retrieves MaxAchievementPoints per page from Web.Config file.
        /// </summary>
        public static int MaxAchievementPoints
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["MaxAchievementPoints"] == null)
                {
                    return 1000;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["MaxAchievementPoints"]);
                }
            }
        }

        /// <summary>
        /// Get list of trackers
        /// </summary>
        public static System.Collections.Hashtable Trackers
        {
            get
            {
                return (System.Collections.Hashtable) System.Configuration.ConfigurationManager.GetSection ("BitTorrentTrackers");
            }
        }

        /// <summary>
        /// Get list of possible Media Converter Servers
        /// </summary>
        public static System.Collections.Hashtable MediaConverters
        {
            get
            {
                return (System.Collections.Hashtable) System.Configuration.ConfigurationManager.GetSection ("MediaConverters");
            }
        }

        /// <summary>
        /// Get list of content servers
        /// </summary>
        public static System.Collections.Hashtable ContentServers
        {
            get
            {
                return (System.Collections.Hashtable) System.Configuration.ConfigurationManager.GetSection ("ContentServersSettings");
            }
        }

        /// <summary>
        /// Get list of upload servers
        /// </summary>
        public static System.Collections.Hashtable UploadServers
        {
            get
            {
                return (System.Collections.Hashtable) System.Configuration.ConfigurationManager.GetSection ("UploadServersSettings");
            }
        }

        /// <summary>
        /// ShareCatchpaCount
        /// </summary>
        public static int ShareCatchpaCount
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ShareCatchpaCount"] == null)
                {
                    return 50;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["ShareCatchpaCount"]);
                }
            }
        }

        /// <summary>
        /// CaptchaRegistrationWindow
        /// </summary>
        public static int CaptchaRegistrationWindow
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["RegistrationCaptchaWindow"] == null)
                {
                    return 30;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["RegistrationCaptchaWindow"]);
                }
            }
        }

        /// <summary>
        /// Retrieves the tracker username from the Web.Config file.
        /// </summary>
        public static string TrackerUser
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"].ToUpper ().Equals ("TRUE"))
                {
                    // Encrypt (System.Configuration.ConfigurationManager.AppSettings ["tracker_user"]);
                    return Decrypt (System.Configuration.ConfigurationManager.AppSettings["tracker_user"]);
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["tracker_user"];
                }
            }
        }

        /// <summary>
        /// Retrieves the tracker username from the Web.Config file.
        /// </summary>
        public static string TrackerPass
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"].ToUpper ().Equals ("TRUE"))
                {
                    // Encrypt (System.Configuration.ConfigurationManager.AppSettings ["tracker_pass"]);
                    return Decrypt (System.Configuration.ConfigurationManager.AppSettings["tracker_pass"]);
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["tracker_pass"];
                }
            }
        }

        /// <summary>
        /// Retrieves database connection string from Web.Config file.
        /// </summary>
        //[Obsolete("Use new Data Access Layer")]
        public static string ConnectionString
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"].ToUpper ().Equals ("TRUE"))
                {
                    // Encrypt (System.Configuration.ConfigurationManager.AppSettings ["ConnectionString"]);
                    return Decrypt (System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
                }
            }
        }

        /// <summary>
        /// Retrieves database connection string from Web.Config file.
        /// </summary>
        //[Obsolete("Use new Data Access Layer")]
        public static string ConnectionStringReadOnly
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"].ToUpper ().Equals ("TRUE"))
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringReadOnly"] != null)
                    {
                        // Encrypt (System.Configuration.ConfigurationManager.AppSettings ["ConnectionString"]);
                        return Decrypt (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringReadOnly"]);
                    }
                    else
                    {
                        return ConnectionString;
                    }
                }
                else
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringReadOnly"] != null)
                    {
                        return System.Configuration.ConfigurationManager.AppSettings["ConnectionStringReadOnly"];
                    }
                    else
                    {
                        return ConnectionString;
                    }
                }
            }
        }

        /// <summary>
        /// Retrieves database connection string from Web.Config file.
        /// </summary>
        //[Obsolete("Use new Data Access Layer")]
        public static string ConnectionStringReadOnly2
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"].ToUpper ().Equals ("TRUE"))
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringReadOnly2"] != null)
                    {
                        // Encrypt (System.Configuration.ConfigurationManager.AppSettings ["ConnectionString"]);
                        return Decrypt (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringReadOnly2"]);
                    }
                    else
                    {
                        return ConnectionStringReadOnly;
                    }
                }
                else
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringReadOnly2"] != null)
                    {
                        return System.Configuration.ConfigurationManager.AppSettings["ConnectionStringReadOnly2"];
                    }
                    else
                    {
                        return ConnectionStringReadOnly;
                    }
                }
            }
        }

        /// <summary>
        /// Retrieves Developer database connection string from Web.Config file.
        /// </summary>
        //[Obsolete("Use new Data Access Layer")]
        public static string ConnectionStringDeveloper
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"].ToUpper ().Equals ("TRUE"))
                {
                    return Decrypt (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringDeveloper"]);
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["ConnectionStringDeveloper"];
                }
            }
        }

        /// <summary>
        /// Retrieves KGP database connection string from Web.Config file.
        /// </summary>
        //[Obsolete("Use new Data Access Layer")]
        public static string ConnectionStringKGP
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"].ToUpper ().Equals ("TRUE"))
                {
                    return Decrypt (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringKGP"]);
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["ConnectionStringKGP"];
                }
            }
        }

        /// <summary>
        /// Retrieves database connection string for Search from Web.Config file.
        /// </summary>
        //[Obsolete("Use new Data Access Layer")]
        public static string ConnectionStringSearch
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"].ToUpper ().Equals ("TRUE"))
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringSearch"] != null)
                    {
                        return Decrypt (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringSearch"]);
                    }
                    else
                    {
                        return ConnectionString;
                    }
                }
                else
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringSearch"] != null)
                    {
                        return System.Configuration.ConfigurationManager.AppSettings["ConnectionStringSearch"];
                    }
                    else
                    {
                        return ConnectionString;
                    }
                }
            }
        }

        /// <summary>
        /// Retrieves database connection string for Master Search from Web.Config file.
        /// </summary>
        //[Obsolete("Use new Data Access Layer")]
        public static string ConnectionStringMasterSearch
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"].ToUpper ().Equals ("TRUE"))
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringMasterSearch"] != null)
                    {
                        return Decrypt (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringMasterSearch"]);
                    }
                    else
                    {
                        return ConnectionString;
                    }
                }
                else
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringMasterSearch"] != null)
                    {
                        return System.Configuration.ConfigurationManager.AppSettings["ConnectionStringMasterSearch"];
                    }
                    else
                    {
                        return ConnectionString;
                    }
                }
            }
        }

        /// <summary>
        /// Retrieves database connection string for Master Search from Web.Config file.
        /// </summary>
        //[Obsolete("Use new Data Access Layer")]
        public static string ConnectionStringEmailViral
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"].ToUpper ().Equals ("TRUE"))
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringEmailViral"] != null)
                    {
                        return Decrypt (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringEmailViral"]);
                    }
                    else
                    {
                        return ConnectionString;
                    }
                }
                else
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringEmailViral"] != null)
                    {
                        return System.Configuration.ConfigurationManager.AppSettings["ConnectionStringEmailViral"];
                    }
                    else
                    {
                        return ConnectionString;
                    }
                }
            }
        }


        /// <summary>
        /// Retrieves database connection string for Master Search from Web.Config file.
        /// </summary>
        //[Obsolete("Use new Data Access Layer")]
        public static string ConnectionStringTest
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"].ToUpper ().Equals ("TRUE"))
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringTest"] != null)
                    {
                        return Decrypt (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringTest"]);
                    }
                    else
                    {
                        return ConnectionString;
                    }
                }
                else
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringTest"] != null)
                    {
                        return System.Configuration.ConfigurationManager.AppSettings["ConnectionStringTest"];
                    }
                    else
                    {
                        return ConnectionString;
                    }
                }
            }
        }

        /// <summary>
        /// Retrieves database connection string for Master Stats from Web.Config file.
        /// </summary>
        //[Obsolete("Use new Data Access Layer")]
        public static string ConnectionStringStats
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"].ToUpper ().Equals ("TRUE"))
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringStats"] != null)
                    {
                        return Decrypt (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringStats"]);
                    }
                    else
                    {
                        return ConnectionString;
                    }
                }
                else
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringStats"] != null)
                    {
                        return System.Configuration.ConfigurationManager.AppSettings["ConnectionStringStats"];
                    }
                    else
                    {
                        return ConnectionString;
                    }
                }
            }
        }

        /// <summary>
        /// Retrieves database connection string for Master Stats from Web.Config file.
        /// </summary>
        //[Obsolete("Use new Data Access Layer")]
        public static string ConnectionStringUGC
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"].ToUpper ().Equals ("TRUE"))
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringUGC"] != null)
                    {
                        return Decrypt (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringUGC"]);
                    }
                    else
                    {
                        return ConnectionString;
                    }
                }
                else
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ConnectionStringUGC"] != null)
                    {
                        return System.Configuration.ConfigurationManager.AppSettings["ConnectionStringUGC"];
                    }
                    else
                    {
                        return ConnectionString;
                    }
                }
            }
        }

        /// <summary>
        /// Retrieves Kaneva database name
        /// </summary>
        public static string DbNameKaneva
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["DbNameKaneva"];
            }
        }

        /// <summary>
        /// Retrieves Kaneva database name that is not in the cluster
        /// </summary>
        public static string DbNameKanevaNonCluster
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["DbNameKanevaNonCluster"] == null)
                {
                    return "my_kaneva";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["DbNameKanevaNonCluster"];
                }
            }
        }

        /// <summary>
        /// Retrieves name of the World of Kanva game, different for dev, staging, prod
        /// </summary>
        public static string WokGameName
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["WokGameName"] == null)
                {
                    return "World of Kaneva";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["WokGameName"];
                }
            }
        }

        /// <summary>
        /// Retrieves the WOK Game id since it is different on dev/pview/rc/prod  prod is 3296
        /// </summary>
        public static int WokGameId
        {
            get
            {
                string s = System.Configuration.ConfigurationManager.AppSettings["WokGameId"];
                if (s == null)
                    s = "3296";
                return Convert.ToInt32 (s);
            }
        }

        /// <summary>
        /// Controls whether or not to use memcache for the token repository. Default is true.
        /// </summary>
        public static bool UseMemcachedTokenRepository
        {
            get
            {
                string s = System.Configuration.ConfigurationManager.AppSettings["UseMemcachedTokenRepository"];
                if (s == null)
                    s = "True";
                return Convert.ToBoolean(s);
            }
        }

        /// <summary>
        /// Retrieves the current version number of the patcher used by kgp/playwok.aspx.cs and shopping/itemdetails.aspx.cs
        /// </summary>
        public static int CurrentPatcherVersion
        {
            get
            {
                string s = System.Configuration.ConfigurationManager.AppSettings["PatcherVersion"];
                if (s == null)
                    s = "3";
                return Convert.ToInt32 (s);
            }
        }


        /// <summary>
        /// Retrieves KGP (WOK) database name
        /// </summary>
        public static string DbNameKGP
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["DbNameKGP"];
            }
        }

        /// <summary>
        /// Retrieves Kaneva shopping database name
        /// </summary>
        public static string DbNameShop
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["DbNameShop"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["DbNameShop"];
                }
                else
                {
                    return "shopping";
                }
            }
        }

        /// <summary>
        /// Retrieves Kaneva developer database name
        /// </summary>
        public static string DbNameDeveloper
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["DbNameDeveloper"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["DbNameDeveloper"];
                }
                else
                {
                    return "developer";
                }
            }
        }

        /// <summary>
        /// Retrieves URL to get the IP/Port of a STAR's game server
        /// </summary>
        public static string GameServerUrl
        {
            get
            {
                string s = System.Configuration.ConfigurationManager.AppSettings["GameServerUrl"];
                if (s == null)
                    s = "http://wok.kaneva.com/kgp/gameServer.aspx?gameId=";
                return s;
            }
        }

        /// <summary>
        /// Retrieves ImageServer string from Web.Config file.
        /// </summary>
        public static string ImageServer
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ImageServer"] == null)
                {
                    return "http://images.kaneva.com";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["ImageServer"];
                }
            }
        }

        /// <summary>
        /// Retrieves ImageServer string from Web.Config file.
        /// </summary>
        public static string TemplateServer
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["TemplateServer"] == null)
                {
                    return "http://templates.kaneva.com";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["TemplateServer"];
                }
            }
        }

        /// <summary>
        /// Retrieves ImageServer string from Web.Config file.
        /// </summary>
        public static string TextureServer
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["TextureServer"] == null)
                {
                    return "http://textures.kaneva.com";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["TextureServer"];
                }
            }
        }

        /// <summary>
        /// Retrieves TexturePath string from Web.Config file.
        /// </summary>
        public static string TexturePath
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["TexturePath"] == null)
                {
                    return "\\\\fs.kanevia.com\\Textures";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["TexturePath"];
                }
            }
        }

        private static Dictionary<string, Dictionary<string, string>> m_uniqueAssetPathMap = new Dictionary<string, Dictionary<string, string>>();

        public static Dictionary<string, string> GenerateUniqueAssetPathMap(string keyPrefix)
        {
            var pathMap = new System.Collections.Generic.Dictionary<string, string>();
            foreach (var key in System.Configuration.ConfigurationManager.AppSettings.AllKeys)
            {
                if (key.StartsWith(keyPrefix + "/"))
                {
                    string pathPrefix = key.Substring(keyPrefix.Length + 1);
                    pathMap.Add(pathPrefix, System.Configuration.ConfigurationManager.AppSettings[key]);
                }
            }

            return pathMap;
        }

        public static string GetUniqueAssetPath(string keyPrefix, string relPath)
        {
            if (System.Configuration.ConfigurationManager.AppSettings[keyPrefix] != null)
            {
                // Always use fixed path if defined
                return System.Configuration.ConfigurationManager.AppSettings[keyPrefix];
            }

            if (!m_uniqueAssetPathMap.ContainsKey(keyPrefix))
            {
                Dictionary<string, string> assetPathMap = GenerateUniqueAssetPathMap(keyPrefix);
                Debug.Assert(assetPathMap != null);
                if (assetPathMap != null)
                {
                    m_uniqueAssetPathMap[keyPrefix] = assetPathMap;
                }
            }

            // Otherwise try path prefix map
            if (m_uniqueAssetPathMap.ContainsKey(keyPrefix))
            {
                foreach (var kv in m_uniqueAssetPathMap[keyPrefix])
                {
                    if (relPath.StartsWith(kv.Key))
                    {
                        return kv.Value;
                    }
                }
            }

            return null;
        }


        /// <summary>
        /// Retrieves UniqueTexturePath string from Web.Config file.
        /// </summary>
        public static string GetUniqueMeshPath(string relPath)
        {
            string path = GetUniqueAssetPath("UniqueMeshPath", relPath);
            if (path == null)
            {
                // fallback to MeshPath
                path = MeshPath;
            }
            return path;
        }

        /// <summary>
        /// Retrieves UniqueTexturePath string from Web.Config file.
        /// </summary>
        public static string GetUniqueTexturePath(string relPath)
        {
            string path = GetUniqueAssetPath("UniqueTexturePath", relPath);
            if (path == null)
            {
                // fallback to TexturePath
                path = TexturePath;
            }
            return path;
        }

        /// <summary>
        /// Retrieves MeshServer string from Web.Config file.
        /// </summary>
        public static string MeshServer
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["MeshServer"] == null)
                {
                    return "http://meshes.kaneva.com";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["MeshServer"];
                }
            }
        }

        /// <summary>
        /// Retrieves MeshPath string from Web.Config file.
        /// </summary>
        public static string MeshPath
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["MeshPath"] == null)
                {
                    // Temporary hack to avoid problem caused by missing entry from web.config
                    return TexturePath + "\\..\\Meshes";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["MeshPath"];
                }
            }
        }

          /// <summary>
        /// Retrieves ScriptsPath string from Web.Config file.
        /// </summary>
        public static string ScriptsPath
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ScriptsPath"] == null)
                {
                    // Temporary hack to avoid problem caused by missing entry from web.config
                    return TexturePath + "\\..\\Scripts";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["ScriptsPath"];
                }
            }
        }

        

        /// <summary>
        /// Retrieves EquippableServer string from Web.Config file.
        /// </summary>
        public static string EquippableServer
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["EquippableServer"] == null)
                {
                    return "http://equippables.kaneva.com";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["EquippableServer"];
                }
            }
        }

        /// <summary>
        /// Retrieves EquippablePath string from Web.Config file.
        /// </summary>
        public static string EquippablePath
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["EquippablePath"] == null)
                {
                    // Temporary hack to avoid problem caused by missing entry from web.config
                    return TexturePath + "\\..\\equippables";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["EquippablePath"];
                }
            }
        }

        /// <summary>
        /// Retrieves AnimationServer string from Web.Config file.
        /// </summary>
        public static string AnimationServer
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["AnimationServer"] == null)
                {
                    return "http://animations.kaneva.com";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["AnimationServer"];
                }
            }
        }

        /// <summary>
        /// Retrieves AnimationPath string from Web.Config file.
        /// </summary>
        public static string AnimationPath
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["AnimationPath"] == null)
                {
                    // Temporary hack to avoid problem caused by missing entry from web.config
                    return TexturePath + "\\..\\animations";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["AnimationPath"];
                }
            }
        }

        /// <summary>
        /// Retrieves SoundServer string from Web.Config file.
        /// </summary>
        public static string SoundServer
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["SoundServer"] == null)
                {
                    return "http://sounds.kaneva.com";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["SoundServer"];
                }
            }
        }

        /// <summary>
        /// Retrieves SoundPath string from Web.Config file.
        /// </summary>
        public static string SoundPath
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["SoundPath"] == null)
                {
                    // Temporary hack to avoid problem caused by missing entry from web.config
                    return TexturePath + "\\..\\sounds";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["SoundPath"];
                }
            }
        }

        /// <summary>
        /// Retrieves AnimationFilestore string from Web.Config file.
        /// </summary>
        public static string AnimationFilestore
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["AnimationFilestore"] == null)
                {
                    return "";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["AnimationFilestore"];
                }
            }
        }

        /// <summary>
        /// Retrieves SoundFilestore string from Web.Config file.
        /// </summary>
        public static string SoundFilestore
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["SoundFilestore"] == null)
                {
                    return "";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["SoundFilestore"];
                }
            }
        }

        /// <summary>
        /// Retrieves TextureFilestore string from Web.Config file.
        /// </summary>
        public static string TextureFilestore
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["TextureFilestore"] == null)
                {
                    return "";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["TextureFilestore"];
                }
            }
        }

        /// <summary>
        /// Retrieves UniqueTextureFilestore string from Web.Config file.
        /// </summary>
        public static string UniqueTextureFilestore
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["UniqueTextureFilestore"] == null)
                {
                    return TextureFilestore;
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["UniqueTextureFilestore"];
                }
            }
        }

        /// <summary>
        /// Retrieves UniqueMeshFilestore string from Web.Config file.
        /// </summary>
        public static string UniqueMeshFilestore
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["UniqueMeshFilestore"] == null)
                {
                    return MeshFilestore;
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["UniqueMeshFilestore"];
                }
            }
        }

        /// <summary>
        /// Retrieves ScriptsFilestore string from Web.Config file.
        /// </summary>
        public static string ScriptsFilestore
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ScriptsFilestore"] == null)
                {
                    return "";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["ScriptsFilestore"];
                }
            }
        }

        /// <summary>
        /// Retrieves MeshFilestore string from Web.Config file.
        /// </summary>
        public static string MeshFilestore
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["MeshFilestore"] == null)
                {
                    return "";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["MeshFilestore"];
                }
            }
        }

        /// <summary>
        /// Retrieves EquippableFilestore string from Web.Config file.
        /// </summary>
        public static string EquippableFilestore
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["EquippableFilestore"] == null)
                {
                    return "";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["EquippableFilestore"];
                }
            }
        }

        /// <summary>
        /// Retrieves WOKImageServer string from Web.Config file.
        /// </summary>
        public static string WOKImageServer
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["WOKImageServer"] == null)
                {
                    return "http://images.kaneva.com";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["WOKImageServer"];
                }
            }
        }

        /// <summary>
        /// Retrieves GameImageServer string from Web.Config file.
        /// </summary>
        public static string GameImageServer
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["GameImageServer"] == null)
                {
                    return "http://images.kaneva.com";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["GameImageServer"];
                }
            }
        }

        /// <summary>
        /// Retrieves AppIncubator Owner email from Web.Config file.
        /// </summary>
        public static string IncubationOwner
        {
            get
            {
                string res = System.Configuration.ConfigurationManager.AppSettings["IncubationOwner"];
                if (res == null || res == "")
                {
                    return "apin@kaneva.com";
                }

                return res;
            }
        }

        #region Mail Settings

        /// <summary>
        /// Retrieves SMTPServer string from Web.Config file.
        /// </summary>
        public static string SMTPServer
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["SMTPServer"] == null)
                {
                    return "";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["SMTPServer"];
                }
            }
        }

        /// <summary>
        /// Retrieves EnableEmailBlacklist string from Web.Config file.
        /// </summary>
        public static bool EnableEmailBlacklist
        {
            get
            {
                return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["EnableEmailBlacklist"]);
            }
        }

        /// <summary>
        /// Retrieves EnableEmailUnsubscribe string from Web.Config file.
        /// </summary>
        public static bool EnableEmailUnsubscribe
        {
            get
            {
                return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["EnableEmailUnsubscribe"]);
            }
        }

        /// <summary>
        /// Retrieves EnableEmailDebug string from Web.Config file.
        /// </summary>
        public static bool EnableEmailDebug
        {
            get
            {
                return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["EnableEmailDebug"]);
            }
        }

        /// <summary>
        /// Retrieves Email to send to when EnableEmailDebug is true
        /// </summary>
        public static string EmailDebugAddress
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["EmailDebugAddress"] == null)
                {
                    return "";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["EmailDebugAddress"];
                }
            }
        }

        /// <summary>
        /// Retrieves from Email Display Name for system generated emails
        /// </summary>
        public static string FromDisplayName
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["FromDisplayName"] == null)
                {
                    return "";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["FromDisplayName"];
                }
            }
        }


        /// <summary>
        /// Retrieves Pickup Directory  Tier 1.
        /// </summary>
        public static string EmailTier1
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["EmailTier1"] == null)
                {
                    return "";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["EmailTier1"];
                }
            }
        }

        /// <summary>
        /// Retrieves Pickup Directory Tier 2.
        /// </summary>
        public static string EmailTier2
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["EmailTier2"] == null)
                {
                    return "";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["EmailTier2"];
                }
            }
        }

        /// <summary>
        /// Retrieves Pickup Directory Tier 3.
        /// </summary>
        public static string EmailTier3
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["EmailTier3"] == null)
                {
                    return "";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["EmailTier3"];
                }
            }
        }

        /// <summary>
        /// Retrieves Pickup Directory Tier 4.
        /// </summary>
        public static string EmailTier4
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["EmailTier4"] == null)
                {
                    return "";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["EmailTier4"];
                }
            }
        }

        /// <summary>
        /// Retrieves Default Pickup Directory.
        /// </summary>
        public static string EmailTierDefault
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["EmailTierDefault"] == null)
                {
                    return "";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["EmailTierDefault"];
                }
            }
        }


        /// <summary>
        /// Retrieves From string from Web.Config file.
        /// </summary>
        public static string FromEmail
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["FromEmail"] == null)
                {
                    return "kaneva@kaneva.com";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["FromEmail"];
                }
            }
        }

        /// <summary>
        /// Retrieves From string from Web.Config file.
        /// </summary>
        public static string FromSupportEmail
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["FromSupportEmail"] == null)
                {
                    return "customersupport@kaneva.com";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["FromSupportEmail"];
                }
            }
        }

        /// <summary>
        /// <summary>
        /// Retrieves From string from Web.Config file.
        /// </summary>
        public static string STARApplyToEmail
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["StarApplyToEmail"] == null)
                {
                    return "gfame@kaneva.com";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["StarApplyToEmail"];
                }
            }
        }

        /// <summary>
        /// Retrieves EnableEventKmails string from Web.Config file.
        /// </summary>
        public static bool EnableEventKMails
        {
            get
            {
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["EnableEventKMails"] == null)
                    {
                        return false;
                    }
                    else
                    {
                        return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["EnableEventKMails"]);
                    }
                }
                catch 
                {
                    return false;
                }
            }
        }

        #endregion

        /// <summary>
        /// Retrieves From string from Web.Config file.
        /// </summary>
        public static string WebSideStoryAccount
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["WebSideStoryAccount"] == null)
                {
                    // Default to dev
                    return "DM5701157NDM";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["WebSideStoryAccount"].ToString ();
                }
            }
        }


        /// <summary>
        /// Retrieves From string from Web.Config file.
        /// </summary>
        public static string GoogleAnalyticsAccount
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["GoogleAnalyticsAccount"] == null)
                {
                    // Default to live account
                    return "UA-572372-2";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["GoogleAnalyticsAccount"].ToString ();
                }
            }
        }

        /// <summary>
        /// Retrieves From string from Web.Config file.
        /// </summary>
        public static string GoogleAnalyticsAccountReports
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["GoogleAnalyticsAccountReports"] == null)
                {
                    // Default to live account
                    return "UA-572372-2";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["GoogleAnalyticsAccountReports"].ToString ();
                }
            }
        }

        /// <summary>
        /// Retrieves From string from Web.Config file.
        /// </summary>
        public static string GoogleAnalyticsAccountCredits
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["GoogleAnalyticsAccountCredits"] == null)
                {
                    // Default to live account
                    return "UA-572372-2";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["GoogleAnalyticsAccountCredits"].ToString ();
                }
            }
        }

        /// <summary>
        /// Retrieves From string from Web.Config file.
        /// </summary>
        public static string GoogleAnalyticsAccountRewards
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["GoogleAnalyticsAccountRewards"] == null)
                {
                    // Default to live account
                    return "UA-572372-2";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["GoogleAnalyticsAccountRewards"].ToString ();
                }
            }
        }

        /// <summary>
        /// Retrieves From string from Web.Config file.
        /// </summary>
        public static string GoogleAnalyticsAccountWorlds
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["GoogleAnalyticsAccountWorlds"] == null)
                {
                    // Default to live account
                    return "UA-10115643-20";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["GoogleAnalyticsAccountWorlds"].ToString();
                }
            }
        }

        /// <summary>
        /// Retrieves From string from Web.Config file.
        /// </summary>
        public static string GoogleAnalyticsAccountZonePurchase
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["GoogleAnalyticsAccountZonePurchase"] == null)
                {
                    // Default to live account
                    return "UA-10115643-21";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["GoogleAnalyticsAccountZonePurchase"].ToString();
                }
            }
        }

        /// <summary>
        /// Retrieves MinimumCreditCardTransactionAmount string from Web.Config file.
        /// </summary>
        public static double MinimumCreditCardTransactionAmount
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["MinimumCreditCardTransactionAmount"] == null)
                {
                    return 5.00;
                }
                else
                {
                    return Convert.ToDouble (System.Configuration.ConfigurationManager.AppSettings["MinimumCreditCardTransactionAmount"]);
                }
            }
        }

        /// <summary>
        /// Retrieves EnableCheckout string from Web.Config file.
        /// </summary>
        public static bool EnableCheckout
        {
            get
            {
                return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["EnableCheckout"]);
            }
        }

        /// <summary>
        /// Retrieves EnableCheckout_B string from Web.Config file.
        /// </summary>
        public static bool EnableCheckout_B
        {
            get
            {
                try
                {
                    return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["EnableCheckout_B"]);
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Retrieves EnablePaypal string from Web.Config file.
        /// </summary>
        public static bool EnablePaypal
        {
            get
            {
                try
                {
                    return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["EnablePaypal"]);
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Retrieves PayPalURL string from Web.Config file.
        /// </summary>
        public static string PayPalURL
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["PayPalURL"];
            }
        }

        /// <summary>
        /// Retrieves PayPalBusiness string from Web.Config file.
        /// </summary>
        public static string PayPalBusiness
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["PayPalBusiness"];
            }
        }

        /// <summary>
        /// Retrieves PayPalPDTToken string from Web.Config file.
        /// </summary>
        public static string PayPalPDTToken
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["PayPalPDTToken"];
            }
        }

        /// <summary>
        /// Retrieves InCommURL string from Web.Config file.
        /// </summary>
        public static string InCommURL
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["InCommURL"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["InCommURL"];
                }
                else
                {
                    return "http://66.147.172.198:8080/transferedvalue/kaneva";
                }
            }
        }

        /// <summary>
        /// Retrieves InCommAccountNumber string from Web.Config file.
        /// </summary>
        public static string InCommAccountNumber
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["InCommAccountNumber"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["InCommAccountNumber"];
                }
                else
                {
                    return "Kaneva";
                }
            }
        }


        /// <summary>
        /// Retrieves SSLLogin string from Web.Config file.
        /// </summary>
        public static bool SSLLogin
        {
            get
            {
                return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["SSLLogin"]);
            }
        }

        /// <summary>
        /// Retrieves LogFile string from Web.Config file.
        /// </summary>
        public static string LogFile
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["LogFile"];
            }
        }

        /// <summary>
        /// Retrieves site name of the current string from Web.Config file.
        /// </summary>
        public static string CurrentSiteName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(System.Configuration.ConfigurationManager.AppSettings["CurrentSiteName"]))
                    return System.Configuration.ConfigurationManager.AppSettings["SiteName"];

                return System.Configuration.ConfigurationManager.AppSettings["CurrentSiteName"];
            }
        }

        /// <summary>
        /// Retrieves SiteName string from Web.Config file.
        /// </summary>
        /// <remarks>
        /// Almost every site and service in the system uses this property to mean "Kaneva.com Site Name".
        /// Because of this, "CurrentSiteName" should be used when wishing to reference the hostname of the 
        /// current site (shop.kaneva.com, kwas.kaneva.com, etc.).
        /// </remarks>
        public static string SiteName
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["SiteName"];
            }
        }

        /// <summary>
        /// Retrieves SiteManagementName string from Web.Config file.
        /// </summary>
        public static string SiteManagementName
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["SiteManagementName"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["SiteManagementName"];
                }
                else
                {
                    return "kwas.kaneva.com";
                }
            }
        }

        public static int[] LoginCountsToShowPromo
        {
            get 
            {
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["LoginCountsToShowPromo"] != null &&
                        System.Configuration.ConfigurationManager.AppSettings["LoginCountsToShowPromo"].ToString().Length > 0)
                    {
                        string[] strings = System.Configuration.ConfigurationManager.AppSettings["LoginCountsToShowPromo"].Split (',');
                        int[] ints = new int[strings.Length];

                        for (int i = 0; i < strings.Length; i++)
                        {
                            ints[i] = int.Parse (strings[i]); // Provide some protection
                        }

                        return (ints);
                    }
                    else
                    {
                        return new int[0];
                    }
                }
                catch
                {
                    return new int[0];
                }
            }
        }

        /// <summary>
        /// Retrieves ShoppingSiteName string from Web.Config file.
        /// </summary>
        public static string ShoppingSiteName
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ShoppingSiteName"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["ShoppingSiteName"];
                }
                else
                {
                    return "shop.kaneva.com";
                }
            }
        }

        /// <summary>
        /// Retrieves DesignerSiteLink string from Web.Config file.
        /// </summary>
        public static string DesignerSiteLink
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["DesignerSiteLink"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["DesignerSiteLink"];
                }
                else
                {
                    return "http://docs.kaneva.com/mediawiki/index.php/Designer_Area";
                }
            }
        }

        /// <summary>
        /// Retrieves DeveloperSiteName string from Web.Config file.
        /// </summary>
        public static string DeveloperSiteName
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["DeveloperSiteName"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["DeveloperSiteName"];
                }
                else
                {
                    return "developer.kaneva.com";
                }
            }
        }

        /// <summary>
        /// Retrieves Join Location string from Web.Config file.
        /// </summary>
        public static string JoinLocation
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["JoinLocation"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["JoinLocation"];
                }
                else
                {
                    //return "~/register/kaneva/registerInfo.aspx";
                    return "~/home.aspx";
                }


            }
        }


        /// <summary>
        /// Retrieves ContentServerPath string from Web.Config file.
        /// </summary>
        public static string ContentServerPath
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["ContentServerPath"];
            }
        }

        /// <summary>
        /// Retrieves ContentServerPath string from Web.Config file.
        /// </summary>
        public static string WOKImagesServerPath
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["WOKImagesPath"];
            }
        }

        /// <summary>
        /// Retrieves ContentServerPath string from Web.Config file.
        /// </summary>
        public static string GameImagesServerPath
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["GameImagesPath"];
            }
        }

        /// <summary>
        /// Retrieves MediaRootPath string from Web.Config file.
        /// </summary>
        public static string MediaRootPath
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["MediaRootPath"];
            }
        }


        /// <summary>
        /// Retrieves FileStoreHack string from Web.Config file.
        /// </summary>
        public static string FileStoreHack
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["FileStoreHack"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["FileStoreHack"];
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// Retrieves WOKFileStoreHack string from Web.Config file.
        /// </summary>
        public static string WOKFileStoreHack
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["WOKFileStoreHack"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["WOKFileStoreHack"];
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// Retrieves GameFileStoreHack string from Web.Config file.
        /// </summary>
        public static string GameFileStoreHack
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["GameFileStoreHack"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["GameFileStoreHack"];
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// FormatBlogRating
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public static string FormatBlogRating (Object rating)
        {
            if (rating.Equals (DBNull.Value))
            {
                return Constants.BLOG_NO_RATING;
            }
            else
            {
                try
                {
                    return Convert.ToDouble (rating).ToString ("#0.##") + "/10";
                }
                catch (Exception exc)
                {
                    m_logger.Error ("FormatBlogRating Error", exc);
                    return Constants.BLOG_NO_RATING;
                }
            }
        }

        /// <summary>
        /// Format the date time into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public static string FormatDateTime (DateTime dtDate)
        {
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo ("en-US", true);
            return dtDate.ToString ("MMM-d-yy hh:mm tt", culture);
        }


        /// <summary>
        /// Format the date into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public static string FormatDate (DateTime dtDate)
        {
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo ("en-US", true);
            return dtDate.ToString ("MMM-d-yy", culture);
        }

        /// <summary>
        /// Format the date into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public static string FormatDateNumbersOnly (DateTime dtDate)
        {
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo ("en-US", true);
            return dtDate.ToString ("MM-dd-yy", culture);
        }

        /// <summary>
        /// Format the date into a time span
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public static string FormatDateTimeSpan (Object dtDate)
        {
            return FormatDateTimeSpan (dtDate, DateTime.Now);
        }
        public static string FormatDateTimeSpan (Object dtDate, Object dtDateNow)
        {
            string timespan = string.Empty;

            if (dtDate.Equals (DBNull.Value))
                return "";

            TimeSpan span = Convert.ToDateTime (dtDateNow) - Convert.ToDateTime (dtDate);

            if (span.Days > 29)
                timespan = (span.Days / 30).ToString () + " month" + ((span.Days / 30) > 1 ? "s" : "") + " ago";
            else if (span.Days > 0)
                timespan = span.Days.ToString () + " day" + (span.Days > 1 ? "s" : "") + " ago";
            else if (span.Hours > 0)
                timespan = span.Hours.ToString () + " hour" + (span.Hours > 1 ? "s" : "") + " ago";
            else if (span.Minutes > 0)
                timespan = span.Minutes.ToString () + " minute" + (span.Minutes > 1 ? "s" : "") + " ago";
            else if (span.Seconds > 0)
                timespan = span.Seconds.ToString () + " second" + (span.Seconds > 1 ? "s" : "") + " ago";
            else
                timespan = "1 second ago";

            return timespan;
        }
        /// <summary>
        /// Format the date into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public static DateTime UnFormatDate (string strDate)
        {
            try
            {
                System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo ("en-US", true);
                return DateTime.Parse (strDate, culture, System.Globalization.DateTimeStyles.None);
            }
            catch (Exception exc)
            {
                m_logger.Error ("Problem unformatting date", exc);
                return GetCurrentDateTime ();
            }
        }

        /// <summary>
        /// Format the date into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public static string FormatImageSize (long imageSize)
        {
            Double newImageSize;

            // Less than 1Kb show b
            if (imageSize < 1024)	// 1K = 1024 b
            {
                return imageSize.ToString ("#,##0.## b");
            }
            // Less than a MB show Kb
            else if (imageSize < 1048600)		// 1048600 b = 1MB = 1024 KB
            {
                newImageSize = (Double) imageSize / 1024;
                return newImageSize.ToString ("#,##0.## Kb");
            }
            // Show in MB
            else // if (imageSize < 1073700000) // 1073700000 b = 1 Gb, 1099500000000 b = 1 TB 
            {
                newImageSize = (Double) imageSize / 1048600;
                return newImageSize.ToString ("#,##0.## Mb");
            }
        }

        /// <summary>
        /// Format the date into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public static string FormatImageSize (double imageSize)
        {
            Double newImageSize;

            if (imageSize < 1000)
            {
                return imageSize.ToString ("#,##0.## b");
            }
            else if (imageSize < 1000000)
            {
                newImageSize = (Double) imageSize / 1000;
                return newImageSize.ToString ("#,##0.## Kb");
            }
            else
            {
                newImageSize = (Double) imageSize / 1000000;
                return newImageSize.ToString ("#,##0.## Mb");
            }
        }

        /// <summary>
        /// Format currency
        /// </summary>
        /// <param name="dblCurrency"></param>
        /// <returns></returns>
        public static string FormatCurrency (Object dblCurrency)
        {
            if (dblCurrency == null)
                return ("$0.00");

            return ((Double) dblCurrency).ToString ("$#,##0.00#");
        }

        /// <summary>
        /// Format currency
        /// </summary>
        /// <param name="dblCurrency"></param>
        /// <returns></returns>
        public static string FormatCurrencyForTextBox (Object dblCurrency)
        {
            if (dblCurrency == null)
                return ("0.00");

            // No partial pennies here.
            return ((Double) dblCurrency).ToString ("#0.00");
        }


        /// <summary>
        /// Format KPoints with other options
        /// </summary>
        /// <param name="dblKPoints"></param>
        /// <param name="bIsHTML"></param>
        /// <param name="bShowFreeText"></param>
        /// <returns></returns>
        public static string FormatKPoints (Double dblKPoints, bool bIsHTML, bool bShowFreeText)
        {
            string formatString = "#,##0 credits";
            //			string currencySymbol;
            //			
            //			if (bIsHTML)
            //			{
            //				currencySymbol = Constants.CURR_KPOINT_SYMBOL_HTML;
            //			}
            //			else
            //			{
            //				currencySymbol = Constants.CURR_KPOINT_SYMBOL_NON_HTML;
            //			}

            if (dblKPoints.Equals (null))
                return ("0");

            if (bShowFreeText && dblKPoints.Equals (0.0))
            {
                return "FREE - 0 kp";
            }

            if (dblKPoints < 0)
            {
                return (Math.Abs (dblKPoints)).ToString ("(" + formatString + ")");
            }
            else
            {
                return (dblKPoints).ToString (formatString);
            }
        }

        /// <summary>
        /// Format KPoints
        /// </summary>
        /// <param name="dblKPoints"></param>
        /// <returns></returns>
        public static string FormatKPoints (Double dblKPoints)
        {
            return FormatKPoints (dblKPoints, true, true);
        }

        /// <summary>
        /// Is the number numeric?
        /// </summary>
        /// <param name="candidate"></param>
        /// <returns></returns>
        public static bool IsNumeric (string candidate)
        {
            char[] ca = candidate.ToCharArray ();
            for (int i = 0; i < ca.Length; i++)
            {
                if (ca[i] > 57 || ca[i] < 48)
                {
                    // Handle negative number
                    if (i == 0 && ca[0] == '-')
                    {
                        // Staring with a -
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Converts an ascii string into hex string
        /// </summary>
        /// <param name="asciiString"></param>
        /// <returns></returns>
        public static string ConvertStringToHex (string asciiString)
        {
            string hex = "";
            foreach (char c in asciiString)
            {
                int tmp = c;
                hex += String.Format ("{0:x2}", (uint) System.Convert.ToUInt32 (tmp.ToString ()));
            }
            return hex;
        }

        /// <summary>
        /// Format seconds as time hh:mm:ss
        /// </summary>
        /// <param name="runTime"></param>
        /// <returns></returns>
        public static string FormatSecondsAsTime (int runTime)
        {
            return String.Format ("{0:D}:{1:D2}:{2:D2}", runTime / 3600, runTime / 60 % 60, runTime % 60);
        }
        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public static string GetResultsText (int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return GetResultsText (TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public static string GetResultsText (UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return GetResultsText (TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }


        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public static string GetResultsText (int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
        {

            return GetResultsText (TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts,
            "Results %START%-%END% of %TOTAL%");
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public static string GetResultsText (UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
        {

            return GetResultsText (TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts,
            "Results %START%-%END% of %TOTAL%");
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        /// <param name="template">%START%, %END%, %TOTAL% will be replaced by starting number, ending number and total
        /// number of items showing</param>
        /// <returns></returns>
        public static string GetResultsText (UInt32 TotalCount, int pageNumber, int resultsPerPage,
            int currentPageCount, bool bGetCounts, string template)
        {
            if (TotalCount > 0)
            {
                if (bGetCounts)
                {
                    int start = ((pageNumber - 1) * resultsPerPage) + 1;
                    int end = (pageNumber - 1) * resultsPerPage + currentPageCount;
                    return template.Replace ("%START%", start.ToString ()).Replace (
                        "%END%", end.ToString ()).Replace ("%TOTAL%", TotalCount.ToString ());
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "<B>No Results Found</B>";
            }
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        /// <param name="template">%START%, %END%, %TOTAL% will be replaced by starting number, ending number and total
        /// number of items showing</param>
        /// <returns></returns>
        public static string GetResultsText (int TotalCount, int pageNumber, int resultsPerPage,
            int currentPageCount, bool bGetCounts, string template)
        {
            if (TotalCount > 0)
            {
                if (bGetCounts)
                {
                    int start = ((pageNumber - 1) * resultsPerPage) + 1;
                    int end = (pageNumber - 1) * resultsPerPage + currentPageCount;
                    return template.Replace ("%START%", start.ToString ()).Replace (
                        "%END%", end.ToString ()).Replace ("%TOTAL%", TotalCount.ToString ());
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "<B>No Results Found</B>";
            }
        }

        /// <summary>
        /// Encrypt
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        public static string Encrypt (string plainText)
        {
            TripleDESCryptoServiceProvider tds = new TripleDESCryptoServiceProvider ();
            byte[] plainByte = ASCIIEncoding.ASCII.GetBytes (plainText);

            string key = "jb7431%o$#=@tp+&";
            PasswordDeriveBytes pdbkey = new PasswordDeriveBytes (key, ASCIIEncoding.ASCII.GetBytes (String.Empty));
            byte[] keyByte = pdbkey.GetBytes (key.Length);

            // Set private key
            tds.Key = keyByte;
            tds.IV = new byte[] { 0xf, 0x6f, 0x13, 0x2e, 0x35, 0xc2, 0xcd, 0xf9 };

            // Encryptor object
            ICryptoTransform cryptoTransform = tds.CreateEncryptor ();

            // Memory stream object
            MemoryStream ms = new MemoryStream ();

            // Crpto stream object
            CryptoStream cs = new CryptoStream (ms, cryptoTransform, CryptoStreamMode.Write);

            // Write encrypted byte to memory stream
            cs.Write (plainByte, 0, plainByte.Length);
            cs.FlushFinalBlock ();

            // Get the encrypted byte length
            byte[] cryptoByte = ms.ToArray ();

            ms.Close ();
            cs.Close ();

            // Convert into base 64 to enable result to be used in Config
            return Convert.ToBase64String (cryptoByte, 0, cryptoByte.GetLength (0));
        }

        /// <summary>
        /// Decrypt
        /// </summary>
        /// <param name="encryptedText"></param>
        /// <returns></returns>
        public static string Decrypt (string encryptedText)
        {
            TripleDESCryptoServiceProvider tds = new TripleDESCryptoServiceProvider ();
            string result = "";

            string key = "jb7431%o$#=@tp+&";
            PasswordDeriveBytes pdbkey = new PasswordDeriveBytes (key, ASCIIEncoding.ASCII.GetBytes (String.Empty));
            byte[] keyByte = pdbkey.GetBytes (key.Length);

            // Decrypt it
            // Convert from base 64 string to bytes
            byte[] cryptoByte = Convert.FromBase64String (encryptedText);

            // Set private key
            tds.Key = keyByte;
            tds.IV = new byte[] { 0xf, 0x6f, 0x13, 0x2e, 0x35, 0xc2, 0xcd, 0xf9 };

            // Decryptor object
            ICryptoTransform cryptoTransform = tds.CreateDecryptor ();
            MemoryStream msd = null;
            CryptoStream csd = null;
            StreamReader sr = null;

            try
            {
                // Memory stream object
                msd = new MemoryStream (cryptoByte, 0, cryptoByte.Length);

                // Crypto stream object
                csd = new CryptoStream (msd, cryptoTransform, CryptoStreamMode.Read);

                // Get the result from the Crypto stream
                sr = new StreamReader (csd);
                result = sr.ReadToEnd ();
            }
            catch (Exception exc)
            {
                m_logger.Error ("Decryption Error", exc);
            }
            finally
            {
                cryptoTransform.Dispose ();

                if (msd != null)
                {
                    msd.Close ();
                }
                if (csd != null)
                {
                    csd.Close ();
                }
                if (sr != null)
                {
                    sr.Close ();
                }
            }

            return result;
        }

        /// <summary>
        /// Generate a token
        /// </summary>
        /// <returns></returns>
        public static string GenerateUniqueString (int maxLength)
        {
            try
            {
                // Now, this is real randomization.
                Random random = new Random (GetRandomSeed());

                char[] token = new char[maxLength];

                for (int i = 0; i < token.Length; i++)
                {
                    token[i] = TOKEN_CHARACTERS[random.Next (0, TOKEN_CHARACTERS.Length - 1)];
                }

                return (new string (token)).Substring (0, maxLength);
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error generating token", exc);
            }

            return ("Kaneva");
        }

        public static int GetRandomSeed()
        {
            // Generate 4 random bytes for the seed
            byte[] randomBytes = new byte[4];
            new RNGCryptoServiceProvider().GetBytes(randomBytes);

            // Convert 4 bytes into a 32-bit integer value.
            int seed = (randomBytes[0] & 0x7f) << 24 |
                randomBytes[1] << 16 |
                randomBytes[2] << 8 |
                randomBytes[3];

            return seed;
        }

        /// <summary>
        /// Get the correct Media Type
        /// </summary>
        public static string GetWorldTemplateImageURL(string imagePath)
        {
            return KanevaGlobals.ImageServer + "/" + imagePath;
        }

        /// <summary>
        /// Return the personal channel link
        /// </summary>
        /// <param name="channelId"></param>
        /// <returns></returns>
        public static string GetPersonalChannelUrl (string nameNoSpaces)
        {
            return "http://" + KanevaGlobals.SiteName + "/channel/" + nameNoSpaces + ".people";
        }

        /// <summary>
        /// Return the broadcast channel link
        /// </summary>
        /// <param name="channelId"></param>
        /// <returns></returns>
        public static string GetBroadcastChannelUrl (string nameNoSpaces)
        {
            return "http://" + KanevaGlobals.SiteName + "/channel/" + nameNoSpaces + ".channel";
        }

        /// <summary>
        /// Return the blog link
        /// </summary>
        /// <param name="blogId"></param>
        /// <returns></returns>
        public static string GeBlogUrl (int blogId)
        {
            return "http://" + SiteName + "/blog/" + blogId.ToString () + ".blog";
        }

        /// <summary>
        /// Get Asset Details Link
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static string GetAssetDetailsLink (int assetId)
        {
            return "http://" + SiteName + "/asset/" + assetId.ToString () + ".media";
        }

        /// <summary>
        /// replace separators(commas, spaces, semicolons) with spaces
        /// </summary>
        /// <param name="tags"></param>
        /// <returns></returns>
        public static string GetNormalizedTags (string tags)
        {
            return tags.Replace (",", " ").Replace (";", " ").Replace ("  ", " ");
        }

        /// <summary>
        /// Get the version
        /// </summary>
        /// <returns></returns>
        public static string Version ()
        {
            if (version == null)
            {
                // Get the current version
                System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly ();
                System.Reflection.AssemblyName an = a.GetName ();
                version = "version " + an.Version.Major + "." + an.Version.Minor + "." + an.Version.Build + "." + an.Version.Revision;
            }

            return version;
        }

        /// <summary>
        /// Get the Build Revision
        /// </summary>
        /// <returns></returns>
        public static string BuildRevision()
        {
            if (m_BuildRevision == null)
            {
                // Get the current build revision
                System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();
                System.Reflection.AssemblyName an = a.GetName();
                m_BuildRevision = an.Version.Revision.ToString();
            }

            return m_BuildRevision;
        }

        /// <summary>
        /// Get the database utility
        /// </summary>
        /// <returns></returns>
        public static DatabaseUtility GetDatabaseUtilityKGP ()
        {
            if (m_DatabaseUtiltyKGP == null)
            {
                switch (KanevaGlobals.DatabaseType)
                {
                    case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
                        {
                            m_DatabaseUtiltyKGP = new SQLServerUtility (KanevaGlobals.ConnectionStringKGP);
                            break;
                        }
                    case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
                        {
                            m_DatabaseUtiltyKGP = new MySQLUtility (KanevaGlobals.ConnectionStringKGP);
                            break;
                        }
                    default:
                        {
                            throw new Exception ("This database type is not supported");
                        }
                }
            }
            return m_DatabaseUtiltyKGP;
        }

        /// <summary>
        /// Get the database utility
        /// </summary>
        /// <returns></returns>
        public static DatabaseUtility GetDatabaseUtilityDeveloper ()
        {
            if (m_DatabaseUtiltyDeveloper == null)
            {
                switch (KanevaGlobals.DatabaseType)
                {
                    case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
                        {
                            m_DatabaseUtiltyDeveloper = new SQLServerUtility (KanevaGlobals.ConnectionStringDeveloper);
                            break;
                        }
                    case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
                        {
                            m_DatabaseUtiltyDeveloper = new MySQLUtility (KanevaGlobals.ConnectionStringDeveloper);
                            break;
                        }
                    default:
                        {
                            throw new Exception ("This database type is not supported");
                        }
                }
            }
            return m_DatabaseUtiltyDeveloper;
        }
        /// <summary>
        /// Get the database utility
        /// </summary>
        /// <returns></returns>
        public static DatabaseUtility GetDatabaseUtility ()
        {
            if (m_DatabaseUtilty == null)
            {
                switch (KanevaGlobals.DatabaseType)
                {
                    case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
                        {
                            m_DatabaseUtilty = new SQLServerUtility (KanevaGlobals.ConnectionString);
                            break;
                        }
                    case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
                        {
                            m_DatabaseUtilty = new MySQLUtility (KanevaGlobals.ConnectionString);
                            break;
                        }
                    default:
                        {
                            throw new Exception ("This database type is not supported");
                        }
                }
            }
            return m_DatabaseUtilty;
        }

        /// <summary>
        /// Get the read only database utility slave
        /// </summary>
        /// <returns></returns>
        public static DatabaseUtility GetDatabaseUtilityReadOnly ()
        {
            if (m_DatabaseUtilityReadOnly == null)
            {
                switch (KanevaGlobals.DatabaseType)
                {
                    case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
                        {
                            m_DatabaseUtilityReadOnly = new SQLServerUtility (KanevaGlobals.ConnectionStringReadOnly);
                            break;
                        }
                    case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
                        {
                            m_DatabaseUtilityReadOnly = new MySQLUtility (KanevaGlobals.ConnectionStringReadOnly);
                            break;
                        }
                    default:
                        {
                            throw new Exception ("This database type is not supported");
                        }
                }
            }
            return m_DatabaseUtilityReadOnly;
        }

        /// <summary>
        /// Get the read only database utility read only
        /// </summary>
        /// <returns></returns>
        public static DatabaseUtility GetDatabaseUtilityReadOnly2 ()
        {
            if (m_DatabaseUtilityReadOnly2 == null)
            {
                switch (KanevaGlobals.DatabaseType)
                {
                    case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
                        {
                            m_DatabaseUtilityReadOnly2 = new SQLServerUtility (KanevaGlobals.ConnectionStringReadOnly2);
                            break;
                        }
                    case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
                        {
                            m_DatabaseUtilityReadOnly2 = new MySQLUtility (KanevaGlobals.ConnectionStringReadOnly2);
                            break;
                        }
                    default:
                        {
                            throw new Exception ("This database type is not supported");
                        }
                }
            }
            return m_DatabaseUtilityReadOnly2;
        }



        /// <summary>
        /// Get the search database utility
        /// </summary>
        /// <returns></returns>
        public static DatabaseUtility GetDatabaseUtilitySearch ()
        {
            if (m_DatabaseUtilitySearch == null)
            {
                switch (KanevaGlobals.DatabaseType)
                {
                    case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
                        {
                            m_DatabaseUtilitySearch = new SQLServerUtility (KanevaGlobals.ConnectionStringSearch);
                            break;
                        }
                    case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
                        {
                            m_DatabaseUtilitySearch = new MySQLUtility (KanevaGlobals.ConnectionStringSearch);
                            break;
                        }
                    default:
                        {
                            throw new Exception ("This database type is not supported");
                        }
                }
            }
            return m_DatabaseUtilitySearch;
        }

        /// <summary>
        /// Get the search master database utility
        /// </summary>
        /// <returns></returns>
        public static DatabaseUtility GetDatabaseUtilityMasterSearch ()
        {
            if (m_DatabaseUtilityMasterSearch == null)
            {
                switch (KanevaGlobals.DatabaseType)
                {
                    case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
                        {
                            m_DatabaseUtilityMasterSearch = new SQLServerUtility (KanevaGlobals.ConnectionStringMasterSearch);
                            break;
                        }
                    case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
                        {
                            m_DatabaseUtilityMasterSearch = new MySQLUtility (KanevaGlobals.ConnectionStringMasterSearch);
                            break;
                        }
                    default:
                        {
                            throw new Exception ("This database type is not supported");
                        }
                }
            }
            return m_DatabaseUtilityMasterSearch;
        }

        /// <summary>
        /// Get the email viral database utility
        /// </summary>
        /// <returns></returns>
        public static DatabaseUtility GetDatabaseUtilityEmailViral ()
        {
            if (m_DatabaseUtilityViralEmail == null)
            {
                switch (KanevaGlobals.DatabaseType)
                {
                    case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
                        {
                            m_DatabaseUtilityViralEmail = new SQLServerUtility (KanevaGlobals.ConnectionStringEmailViral);
                            break;
                        }
                    case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
                        {
                            m_DatabaseUtilityViralEmail = new MySQLUtility (KanevaGlobals.ConnectionStringEmailViral);
                            break;
                        }
                    default:
                        {
                            throw new Exception ("This database type is not supported");
                        }
                }
            }
            return m_DatabaseUtilityViralEmail;
        }

        /// <summary>
        /// Get the test database utility
        /// </summary>
        /// <returns></returns>
        public static DatabaseUtility GetDatabaseUtilityTest ()
        {
            if (m_DatabaseUtilityTest == null)
            {
                switch (KanevaGlobals.DatabaseType)
                {
                    case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
                        {
                            m_DatabaseUtilityTest = new SQLServerUtility (KanevaGlobals.ConnectionStringTest);
                            break;
                        }
                    case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
                        {
                            m_DatabaseUtilityTest = new MySQLUtility (KanevaGlobals.ConnectionStringTest);
                            break;
                        }
                    default:
                        {
                            throw new Exception ("This database type is not supported");
                        }
                }
            }
            return m_DatabaseUtilityTest;
        }


        /// <summary>
        /// Get the stats database utility
        /// </summary>
        /// <returns></returns>
        public static DatabaseUtility GetDatabaseUtilityStats ()
        {
            if (m_DatabaseUtilityStats == null)
            {
                switch (KanevaGlobals.DatabaseType)
                {
                    case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
                        {
                            m_DatabaseUtilityStats = new SQLServerUtility (KanevaGlobals.ConnectionStringStats);
                            break;
                        }
                    case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
                        {
                            m_DatabaseUtilityStats = new MySQLUtility (KanevaGlobals.ConnectionStringStats);
                            break;
                        }
                    default:
                        {
                            throw new Exception ("This database type is not supported");
                        }
                }
            }
            return m_DatabaseUtilityStats;
        }

        /// <summary>
        /// Get the stats database utility
        /// </summary>
        /// <returns></returns>
        public static DatabaseUtility GetDatabaseUtilityUGC ()
        {
            if (m_DatabaseUtilityUGC == null)
            {
                switch (KanevaGlobals.DatabaseType)
                {
                    case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
                        {
                            m_DatabaseUtilityUGC = new SQLServerUtility (KanevaGlobals.ConnectionStringUGC);
                            break;
                        }
                    case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
                        {
                            m_DatabaseUtilityUGC = new MySQLUtility (KanevaGlobals.ConnectionStringUGC);
                            break;
                        }
                    default:
                        {
                            throw new Exception ("This database type is not supported");
                        }
                }
            }
            return m_DatabaseUtilityUGC;
        }

        /// <summary>
        /// Version number
        /// </summary>
        private static string version = null;

        /// <summary>
        /// Build Revision number
        /// </summary>
        private static string m_BuildRevision = null;

        /// <summary>
        /// DataBase Utility Read only for Slave
        /// </summary>
        private static DatabaseUtility m_DatabaseUtilityReadOnly;

        /// <summary>
        /// DataBase Utility Read only for Slave 2
        /// </summary>
        private static DatabaseUtility m_DatabaseUtilityReadOnly2;

        /// <summary>
        /// DataBase Utility for Master Database
        /// </summary>
        private static DatabaseUtility m_DatabaseUtilty;

        /// <summary>
        /// DataBase Utility for KGP Database
        /// </summary>
        private static DatabaseUtility m_DatabaseUtiltyKGP;

        /// <summary>
        /// DataBase Utility for Developer Database
        /// </summary>
        private static DatabaseUtility m_DatabaseUtiltyDeveloper;

        /// <summary>
        /// DataBase Utility for Search
        /// </summary>
        private static DatabaseUtility m_DatabaseUtilitySearch;

        /// <summary>
        /// DataBase Utility for Master Search
        /// </summary>
        private static DatabaseUtility m_DatabaseUtilityMasterSearch;

        /// <summary>
        /// DataBase Utility for Viral Email
        /// </summary>
        private static DatabaseUtility m_DatabaseUtilityViralEmail;

        /// <summary>
        /// DataBase Utility for Testing
        /// </summary>
        private static DatabaseUtility m_DatabaseUtilityTest;

        /// <summary>
        /// DataBase Utility for Stats
        /// </summary>
        private static DatabaseUtility m_DatabaseUtilityStats;

        /// <summary>
        /// DataBase Utility for UGC
        /// </summary>
        private static DatabaseUtility m_DatabaseUtilityUGC;


        /// <summary>
        /// Retrieves FacebookAppId string from Web.Config file.
        /// </summary>
        public static string FacebookAppId
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["FacebookAppId"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["FacebookAppId"];
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// Retrieves FacebookSecret string from Web.Config file.
        /// </summary>
        public static string FacebookSecret
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["FacebookSecret"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["FacebookSecret"];
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// Retrieves FacebookSecret string from Web.Config file.
        /// </summary>
        public static string PostFacebookRegistrationLandingPage
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["PostFacebookRegistrationLandingPage"] != null)
                {
                    return "http://" + KanevaGlobals.SiteName + System.Configuration.ConfigurationManager.AppSettings["PostFacebookRegistrationLandingPage"];
                }
                else
                {
                    return "http://" + KanevaGlobals.SiteName + "/mykaneva/findFriends.aspx";
                }
            }
        }

        /// <summary>
        /// Note: Duplicated in Facade Configuration. This will eventually be deprecated following resolution of
        /// namespace issue with Configuration on some files.
        /// </summary>
        public static string FacebookGraphUrl
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["FacebookGraphUrl"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["FacebookGraphUrl"];
                }
                else
                {
                    return "http://graph.facebook.com/";
                }
            }
        }

        /// <summary>
        /// Note: Duplicated in Facade Configuration. This will eventually be deprecated following resolution of
        /// namespace issue with Configuration on some files.
        /// </summary>
        public static string FacebookProfileImageQueryString
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["FacebookProfileImageQueryString"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["FacebookProfileImageQueryString"];
                }
                else
                {
                    return "/picture?type=square";
                }
            }
        }

        /// <summary>
        /// Note: Duplicated in Facade Configuration. This will eventually be deprecated following resolution of
        /// namespace issue with Configuration on some files.
        /// </summary>
        public static string GetFacebookProfileImageQueryStringBySize (string size)
        {
            switch (size)
            {
                case "sm":
                    return "/picture?type=small";
                case "me":
                    return "/picture?type=normal";
                case "la":
                case "xl":
                    return "/picture?type=large";
                case "sq":
                    return "/picture?type=square";
                default:
                    return "/picture?type=square";
            }
               
        }

        public static ulong DownloadVarientGroup
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["DownloadVarientGroup"] != null)
                {
                    return (ulong) (Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["DownloadVarientGroup"]));
                }
                else
                {
                    return (ulong) 20;
                }
            }
        }

        /// <summary>
        /// Retrieves WorldBlastTestMode bool from Web.Config file.
        /// </summary>
        public static bool WorldBlastTestMode
        {
            get
            {
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["WorldBlastTestMode"] != null)
                    {
                        return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["WorldBlastTestMode"]);
                    }
                    else
                    {
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Retrieves WorldBlastTestMode_CommunityId bool from Web.Config file.
        /// </summary>
        public static int WorldBlastTestMode_CommunityId
        {
            get
            {
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["WorldBlastTestMode_CommunityId"] != null)
                    {
                        return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["WorldBlastTestMode_CommunityId"]);
                    }
                    else
                    {
                        return 0;
                    }
                }
                catch
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Retrieves WorldBlastTestMode_ToEmail bool from Web.Config file.
        /// </summary>
        public static string WorldBlastTestMode_ToEmail
        {
            get
            {
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["WorldBlastTestMode_CommunityId"] != null)
                    {
                        return System.Configuration.ConfigurationManager.AppSettings["WorldBlastTestMode_CommunityId"].ToString();
                    }
                    else
                    {
                        return "";
                    }
                }
                catch
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// Retrieves WorldMemberCountToEnableRecurringEvents int from Web.Config file.
        /// </summary>
        public static int WorldMemberCountToEnableRecurringEvents
        {
            get
            {
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["WorldMemberCountToEnableRecurringEvents"] != null)
                    {
                        return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["WorldMemberCountToEnableRecurringEvents"]);
                    }
                    else
                    {
                        return 30;
                    }
                }
                catch
                {
                    return 30;
                }
            }
        }

        /// <summary>
        /// Retrieves RecurringEventsTestMode bool from Web.Config file.
        /// </summary>
        public static bool RecurringEventsTestMode
        {
            get
            {
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["RecurringEventsTestMode"] != null)
                    {
                        return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["RecurringEventsTestMode"]);
                    }
                    else
                    {
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Retrieves RecurringEventsTestCommunityId bool from Web.Config file.
        /// </summary>
        public static int RecurringEventsTestCommunityId
        {
            get
            {
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["RecurringEventsTestCommunityId"] != null)
                    {
                        return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["RecurringEventsTestCommunityId"]);
                    }
                    else
                    {
                        return 0;
                    }
                }
                catch
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Retrieves CompletionChecklistStartDate string from Web.Config file.
        /// </summary>
        public static DateTime CompletionChecklistStartDate
        {
            get
            {
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["CompletionChecklistStartDate"] == null)
                    {
                        return new DateTime (2010, 11, 1);
                    }
                    else
                    {
                        return Convert.ToDateTime (System.Configuration.ConfigurationManager.AppSettings["CompletionChecklistStartDate"]);
                    }
                }
                catch
                {
                    return new DateTime (2010, 11, 1);
                }
            }
        }

        /// <summary>
        /// Retrieves PremiumItemDaysPendingBeforeRedeem bool from Web.Config file.
        /// </summary>
        public static int PremiumItemDaysPendingBeforeRedeem
        {
            get
            {
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["PremiumItemDaysPendingBeforeRedeem"] != null)
                    {
                        return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["PremiumItemDaysPendingBeforeRedeem"]);
                    }
                    else
                    {
                        return 7;
                    }
                }
                catch
                {
                    return 7;
                }
            }
        }

        /// <summary>
        /// Retrieves PremiumItemUserRedemptionPercentage bool from Web.Config file.
        /// </summary>
        public static double PremiumItemUserRedemptionPercentage
        {
            get
            {
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["PremiumItemUserRedemptionPercentage"] != null)
                    {
                        return Convert.ToDouble (System.Configuration.ConfigurationManager.AppSettings["PremiumItemUserRedemptionPercentage"]);
                    }
                    else
                    {
                        return .7;
                    }
                }
                catch
                {
                    return .7;
                }
            }
        }

        /// <summary>
        /// Retrieves CloudSpongeDomainKey bool from Web.Config file.
        /// </summary>
        public static string CloudSpongeDomainKey
        {
            get
            {
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["CloudSpongeDomainKey"] != null)
                    {
                        return System.Configuration.ConfigurationManager.AppSettings["CloudSpongeDomainKey"];
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Retrieves CloudSpongePassword bool from Web.Config file.
        /// </summary>
        public static string CloudSpongePassword
        {
            get
            {
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["CloudSpongePassword"] != null)
                    {
                        return System.Configuration.ConfigurationManager.AppSettings["CloudSpongePassword"];
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        public static int DailyWorldItemConversionLimit
        {
            get
            {
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["DailyWorldItemConversionLimit"] != null)
                    {
                        return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DailyWorldItemConversionLimit"]);
                    }
                    else
                    {
                        return 400;
                    }
                }
                catch
                {
                    return 400;
                }
            }
        }

        /// <summary>
        /// Retrieves WorldBlastTestMode_CommunityId bool from Web.Config file.
        /// </summary>
        public static int NewUserCreditBeginningBalance
        {
            get
            {
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["NewUserCreditBeginningBalance"] != null)
                    {
                        return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NewUserCreditBeginningBalance"]);
                    }
                    else
                    {
                        return 0;
                    }
                }
                catch
                {
                    return 0;
                }
            }
        }

        // Possible token strings
        private static char[] TOKEN_CHARACTERS = "abcdefgijkmnopqrstwxyzABCDEFGHJKLMNPQRSTWXYZ23456789".ToCharArray ();

        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

    }
}
