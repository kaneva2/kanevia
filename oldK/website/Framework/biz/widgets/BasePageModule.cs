///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace KlausEnt.KEP.Kaneva.framework.biz.widgets
{
	/// <summary>
	/// the base class for all layout page modules
	/// </summary>
	public class BasePageModule : IComparable
	{
		private int		_modulePageId;
		private int		_pageId;
		private int		_moduleId;
		private int		_zoneId;
		private int		_id;
		private int		_sequence;
		public BasePageModule()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public int ModulePageId
		{
			get { return _modulePageId; }
			set { _modulePageId = value; }
		}

		public int PageId
		{
			get { return _pageId; }
			set { _pageId = value; }
		}

		public int ModuleId
		{
			get { return _moduleId; }
			set { _moduleId = value; }
		}

		public int ZoneId
		{
			get { return _zoneId; }
			set { _zoneId = value; }
		}

		public int Id
		{
			get { return _id; }
			set { _id = value; }
		}

		public int Sequence
		{
			get { return _sequence; }
			set { _sequence = value; }
		}
		#region IComparable Members

		/// <summary>
		/// compare by zoneId first then sequence
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public int CompareTo(object obj)
		{
			int retVal = 0;
			if(obj is BasePageModule) 
			{
				BasePageModule temp = (BasePageModule) obj;
				retVal = this.ZoneId.CompareTo(temp.ZoneId);

				if(retVal == 0)
				{
					retVal = this.Sequence.CompareTo(temp.Sequence);
				}
			}
			else
			{
				throw new ArgumentException("object is not a BasePageModule");
			}

			return retVal;
		}

		#endregion
	}
}
