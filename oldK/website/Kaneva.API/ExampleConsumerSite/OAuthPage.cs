///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System.Web;
using System.Web.UI;
using Kaneva.OAuth.Consumer;
using Kaneva.OAuth.Tests;
using ExampleConsumerSite.Properties;

namespace ExampleConsumerSite
{
  public class OAuthPage : Page
  {
    protected IOAuthSession CreateSession()
    {
      string callBackUrl = "http://localhost:" + HttpContext.Current.Request.Url.Port + "/Callback.aspx";

      var consumerContext = new OAuthConsumerContext
        {
          ConsumerKey = "3DApp",
          ConsumerSecret = "tCDILm0y9YDzjmhFHYv7AQI84zb5jQ6U9DAAIRBVKkg",
          Key = TestCertificates.OAuthTestCertificate().PrivateKey
        };

      return new OAuthSession(consumerContext,
                                     Settings.Default.RequestTokenUrl,
                                     Settings.Default.UserAuthorizationUrl,
                                     Settings.Default.AccessTokenUrl,
                                     callBackUrl).RequiresCallbackConfirmation();      
    }
  }
}
