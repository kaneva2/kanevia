///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Web;
using Kaneva.OAuth.Consumer;
using Kaneva.OAuth.Framework;
using Kaneva.OAuth.Utility;
using ExampleConsumerSite.Properties;

using System.Xml.Serialization;

namespace ExampleConsumerSite
{
  public partial class ViewData : OAuthPage
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      var session = CreateSession();

      string accessTokenString = Request[Parameters.OAuth_Token];

      session.AccessToken = ((IToken) Session[accessTokenString]) ?? new TokenBase { ConsumerKey = "fake", Token = "fake"};

      try
      {
         // ConsumerRequest cs = (ConsumerRequest)session.Request().ForUrl(Settings.Default.OAuthRoot + "Game.svc/help");
        //  cs.SignWithToken();

          ConsumerRequest csGame = (ConsumerRequest)session.Request();
          csGame.Get();
          csGame.ForUrl(Settings.Default.OAuthRoot + "Game.svc/help");
          csGame.SignWithToken();
          csGame.Context.GenerateOAuthParametersForHeader();
           

          //session.UserAuthorizeUri.to
          //// Help pages
          aGame.HRef = csGame.ToWebRequest().RequestUri.ToString();
          aGame.InnerText = Settings.Default.OAuthRoot + "Game.svc/help";

          ConsumerRequest csViral = (ConsumerRequest)session.Request();
          csViral.Get();
          csViral.ForUrl(Settings.Default.OAuthRoot + "Blast.svc/help");
          csViral.SignWithToken();
          csViral.Context.GenerateOAuthParametersForHeader();
          aViral.HRef = csViral.ToWebRequest().RequestUri.ToString();
          aViral.InnerText = Settings.Default.OAuthRoot + "Blast.svc/help";

          ConsumerRequest csSocial = (ConsumerRequest)session.Request();
          csSocial.Get();
          csSocial.ForUrl(Settings.Default.OAuthRoot + "Friend.svc/help");
          csSocial.SignWithToken();
          csSocial.Context.GenerateOAuthParametersForHeader();
          aSocial.HRef = csSocial.ToWebRequest().RequestUri.ToString();
          aSocial.InnerText = Settings.Default.OAuthRoot + "Friend.svc/help";


          ConsumerRequest csMedia = (ConsumerRequest)session.Request();
          csMedia.Get();
          csMedia.ForUrl(Settings.Default.OAuthRoot + "Media.svc/help");
          csMedia.SignWithToken();
          csMedia.Context.GenerateOAuthParametersForHeader();
          aMedia.HRef = csMedia.ToWebRequest().RequestUri.ToString();
          aMedia.InnerText = Settings.Default.OAuthRoot + "Media.svc/help";

          //ConsumerRequest csLB = (ConsumerRequest)session.Request();
          //csLB.Get();
          //csLB.ForUrl(Settings.Default.OAuthRoot + "Game.svc/LeaderBoard?onlyFriends=false&sortBy=ASC&pageNumber=1&pageSize=10");
          //csLB.SignWithToken();
          //csLB.Context.GenerateOAuthParametersForHeader();
          //aShowLeaderboard.HRef = csLB.ToWebRequest().RequestUri.ToString();
          //aShowLeaderboard.InnerText = Settings.Default.OAuthRoot + "Game.svc/LeaderBoard?onlyFriends=false&sortBy=ASC&pageNumber=1&pageSize=10";

          // Show Blasts
          string response2 = session.Request()
            .Get()
            .ForUrl(Settings.Default.DataUrl2)
            .SignWithToken()
            .ReadBody();
            litBlasts.Text = response2.ToString();

            litBlasts.Text = session.AccessToken.ToString() + " TS=" + DateTime.Now.Epoch().ToString();

            //////// achiemements for single user
            //////string responseACS = session.Request()
            //////  .Get()
            //////  .ForUrl("http://localhost:1984/Game.svc/AchievementForUser?pageNumber=1&pageSize=10")
            //////.SignWithToken()
            //////.ReadBody();
            //////litBlasts.Text = responseACS.ToString();


          //  string responseAC = session.Request()
          //  .Get()
          //  .ForUrl("http://localhost:1984/media.svc/UpdateAssetGroupAssetSortOrder?assetGroupId=135&assetId=7129&sortOrder=44")
          //.SignWithToken()
          //.ReadBody();
          //  litBlasts.Text = responseAC.ToString();

          //  responseAC = session.Request()
          //  .Get()
          //  .ForUrl("http://localhost:1984/media.svc/UpdateBulkAssetGroupAssetSortOrder?assetGroupId=135&assetIds=2189581,6912&sortOrders=6,0")
          //  .SignWithToken()
          //  .ReadBody();
          //   litBlasts.Text = responseAC.ToString();


            //// achiemements for user - with friends
            //string responseAC = session.Request()
            //  .Get()
            //  .ForUrl("http://localhost:1984/Game.svc/AchievementEarnedForUser?sortBy=test&showAll=true&onlyFriends=true&pageNumber=1&pageSize=10")
            //.SignWithToken()
            //.ReadBody();
            //litBlasts.Text = responseAC.ToString();

            // achiemements for user - with friends
            string responseAC = session.Request()
              .Get()
              .ForUrl("http://localhost:1984/media.svc/AssetGroupAssets?assetGroupId=1")
            .SignWithToken()
            .ReadBody();
            litBlasts.Text = responseAC.ToString();

    //////        // Read profile
    //////        string response4 = session.Request()
    //////          .Get()
    //////          .ForUrl("http://localhost:1984/Friend.svc/ReadProfile?userId=4")
    //////          .SignWithToken()
    //////          .ReadBody();
    //////        litBlasts.Text = response4.ToString();

    //////        // Show Blasts
    //////        string response3 = session.Request()
    //////          .Get()
    //////          .ForUrl("http://localhost:1984/Game.svc/LeaderBoardWithAllData?onlyFriends=true&pageNumber=1&amp;pageSize=10")
    //////          .SignWithToken()
    //////          .ReadBody();
    //////        litBlasts.Text = response3.ToString();

           
    //////        // Achievements in an APP
    //////        response3 = session.Request()
    //////.Get()
    //////.ForUrl("http://localhost:1984/Game.svc/AchievementForApp?sortBy=test&pageNumber=1&pageSize=10")
    //////.SignWithToken()
    //////.ReadBody();
    //////        litBlasts.Text = response3.ToString();


    //        // friends online
    //        string responsefre = session.Request()
    //.Get()
    //.ForUrl("http://localhost:1984/Friend.svc/FriendsOnline?pageNumber=1&pageSize=10")
    //.SignWithToken()
    //.ReadBody();
    //        litBlasts.Text = responsefre.ToString();


    //        // Achievements in an APP
    //        string response3 = session.Request()
    //.Get()
    //.ForUrl("http://localhost:1984/Game.svc/EarnAchievement?achievementId=1")
    //.SignWithToken()
    //.ReadBody();
    //        litBlasts.Text = response3.ToString();

    //////        //        //// Show Blasts
    //////        //string response3 = session.Request()
    //////        //  .Get()
    //////        //  .ForUrl("http://localhost:1984/Game.svc/EarnPlayerData?newLeaderboardValue=450&levelNumber=22&percentComplete=65&title=Geek")
    //////        //  .SignWithToken()
    //////        //  .ReadBody();
    //////        //litBlasts.Text = response3.ToString();

             // aEarnLeaderboard

        string response = session.Request()
          .Get()
          .ForUrl(Settings.Default.DataUrl)
          .SignWithToken()
          .ReadBody();

        //xmlFeed.DocumentContent = response;

        litData.Text = response.ToString();

    

       ////// IOAuthSession apiSession2 = CreateSession();
       ////// //apiSession2.ConsumerContext.UseHeaderForOAuthParameters = true;
       ////// apiSession2.AccessToken = ((IToken)Session[accessTokenString]) ?? new TokenBase { ConsumerKey = "fake", Token = "fake" };
       ////// ConsumerRequest csBlast = (ConsumerRequest)apiSession2.Request();
       ////// csBlast.Get();
       ////// csBlast.ForUrl(Settings.Default.DataUrl2);
       ////// csBlast.SignWithToken();
       ////// string response2 = csBlast.ReadBody();

       ////// litBlasts.Text = response2.ToString();

       ////// litBlasts.Text = session.AccessToken.ToString() + " TS=" + DateTime.Now.Epoch().ToString();


       ////// ConsumerRequest csPostBlast = (ConsumerRequest)session.Request();
       ////// csPostBlast.Post();
       ////// csPostBlast.ForUrl("http://localhost:1984/Blast.svc/SendBlast?message=xxx&imageUrl=1&videoUrl=2&linkURL=3");
       ////// csPostBlast.SignWithToken();
       ////// string responseBlast = csPostBlast.ReadBody();

       ////// litBlasts.Text = response2.ToString();



       ////// IOAuthSession apiSession = CreateSession();
       ////// //Construct a session from your endpoints/Consumer keys etc.
       ////// // Hint to the lib to focus on the header
       ////// apiSession.ConsumerContext.UseHeaderForOAuthParameters = true;

       ////// apiSession.AccessToken = ((IToken)Session[accessTokenString]) ?? new TokenBase { ConsumerKey = "fake", Token = "fake" };
       ////// //apiSession.AccessToken = new TokenBase
       ////// //{
       ////// //    Token = session.AccessToken.Token,
       ////// //    ConsumerKey = session.AccessToken.ConsumerKey,
       ////// //    TokenSecret = session.AccessToken.TokenSecret
       ////// //};



       ////// string testEndpoint = "http://localhost:1984/Blast.svc/SendBlast?message=xxx&imageUrl=1&videoUrl=2&linkURL=3";
       ////// ConsumerRequest cs = (ConsumerRequest)apiSession.Request();
       ////// cs.Post();
       ////// cs.ForUrl(testEndpoint);
       ////// cs.SignWithToken();
       ////// //cs.Context.GenerateOAuthParametersForHeader();

       ////// string oAuthHeader = cs.Context.GenerateOAuthParametersForHeader();

       ////// Uri address = new Uri(testEndpoint);

       ////// HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
       ////// request.Method = "POST";
       //////// request.ContentType = "text/xml";
       ////// // Set the generated Header
       ////// request.Headers.Add("Authorization", oAuthHeader);
       ////// //using (Stream postStream = request.GetRequestStream())
       ////// //{
       ////// //    //Serialize the request
       ////// //    XmlSerializer serializer = new XmlSerializer(typeof(MyType));
       ////// //    serializer.Serialize(postStream, typeToSerialize);
       ////// //    using (StringWriter sw = new StringWriter())
       ////// //    {
       ////// //        serializer.Serialize(sw, TokenAuth);
       ////// //    }
       ////// //}
       ////// using (HttpWebResponse httpWebResponse = (HttpWebResponse)request.GetResponse())
       ////// {
       //////     using (StreamReader reader = new StreamReader(httpWebResponse.GetResponseStream()))
       //////     {
       //////         string responseString = reader.ReadToEnd();
       //////         Console.WriteLine(responseString);
       //////     }
       ////// }

          
       //////   ////  cs.SignWithToken();
       ////// string response3 = session.Request()
       //////     .Post()
       //////     .ForUrl("http://localhost:1984/Blast.svc/SendBlast?message=xxx&imageUrl=&videoUrl=&linkURL=")
       //////     .SignWithToken()
       //////     .ReadBody();

       ////// //litBlasts.Text = response3.ToString();
      }
      catch (WebException webEx)
      {
        var response = (HttpWebResponse) webEx.Response;

        if (response.StatusCode == HttpStatusCode.Forbidden)
        {
          ShowOAuthProblemDetails(response);
        }
        else
        {
          ShowStatusCodeDetails(response);
        }
      }
    }

    protected void btnLeaderboard_Click(object sender, EventArgs e)
      {
          var session = CreateSession();

          string accessTokenString = Request[Parameters.OAuth_Token];

          session.AccessToken = ((IToken)Session[accessTokenString]) ?? new TokenBase { ConsumerKey = "fake", Token = "fake" };
          ConsumerRequest csLB = (ConsumerRequest)session.Request();
          csLB.Get();
          csLB.ForUrl(Settings.Default.OAuthRoot + "Game.svc/EarnLeaderBoard?newValue=" + txtLBValue.Text);
          csLB.SignWithToken();
          csLB.Context.GenerateOAuthParametersForHeader();
          Response.Redirect(csLB.ToWebRequest().RequestUri.ToString());
      }

    protected void btnSendRequest_Click(object sender, EventArgs e)
      {
          var session = CreateSession();

          string accessTokenString = Request[Parameters.OAuth_Token];

          session.AccessToken = ((IToken)Session[accessTokenString]) ?? new TokenBase { ConsumerKey = "fake", Token = "fake" };
          ConsumerRequest csLB = (ConsumerRequest)session.Request();
          csLB.Get();
          csLB.ForUrl(Settings.Default.OAuthRoot + "Blast.svc/SendRequest?toUsername=" + txttoUsername.Text +"&requestDescriptionText=" + txtrequestDescriptionText.Text +"&requestActionLinkText=" + txtrequestActionLinkText.Text +"&requestImage=" + txtrequestImage.Text + "&requestActionParam=" + txtrequestActionParam.Text);
          csLB.SignWithToken();
          csLB.Context.GenerateOAuthParametersForHeader();
          Response.Redirect(csLB.ToWebRequest().RequestUri.ToString());
      }

      
      

    void ShowStatusCodeDetails(HttpWebResponse response)
    {
      ResultsPanel.Visible = false;

      NameValueCollection parameters = HttpUtility.ParseQueryString(response.ReadToEnd());

      ErrorInfo.Text = string.Format("Request failed, status code was: {0}, status description: {1}", response.StatusCode, response.StatusDescription);
    }

    void ShowOAuthProblemDetails(WebResponse response)
    {
      ResultsPanel.Visible = false;

      NameValueCollection parameters = HttpUtility.ParseQueryString(response.ReadToEnd());

      ErrorInfo.Text = "Access was denied to resource.<br/><br/>";

      foreach (string key in parameters.Keys)
        ErrorInfo.Text += key + " => " + parameters[key] + "<br/>";
    }
  }
}
