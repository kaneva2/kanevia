﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ExampleConsumerSite._Default" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">            
    <p>This will redirect the user to the Kaneva provider site, where they must log in, 
    and then grant access for the consumer to access the kaneva API information.  
    If access is granted, the provider site will redirect back to the consumer, at which point the consumer can then access the API data 
    and display them on screen.</p>
    <p>This process ensures that the users login and password are <strong>never exposed</strong> to the consumer site,
     and at any point in the future the provider (user) can revoke access to the consumer site.</p>
    <p>
     <asp:LinkButton ID="oauthRequest" runat="server" OnClick="oauthRequest_Click">Click here</asp:LinkButton>
    to view API Data with OAuth (will start the authentication flow), also to view help links.
    </p>
</asp:Content>