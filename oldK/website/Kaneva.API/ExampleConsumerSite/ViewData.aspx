﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewData.aspx.cs" Inherits="ExampleConsumerSite.ViewData" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">    
    <p>
    Below are the results from an access to the contacts API exposed by the provider. 
    </p>
    <asp:Panel runat="server" ID="ResultsPanel">   
            <b>Help Links</b><br/>
            <a runat="server" id="aGame">Game API</a><br/>
            <a runat="server" id="aViral">Viral API</a><br/>
            <a runat="server" id="aSocial">Social API</a><br/>
             <a runat="server" id="aMedia">Media API</a>
            
            <br/><br/><br/>
            Click Refresh bewteen each call to generate a new OAuth Nonce
            <br/><br/>
            SendRequest?toUsername={toUsername}&requestDescriptionText={requestDescriptionText}&requestActionLinkText={requestActionLinkText}&={requestImage}&requestActionParam={requestActionParam}<br/>
            <asp:Button runat="server" ID="btnSendRequest" Text="Send Request" OnClick="btnSendRequest_Click" /><br/>
                toUsername:<asp:TextBox runat="server" ID="txttoUsername">jason</asp:TextBox><br/><br/>
                requestDescriptionText:<asp:TextBox runat="server" ID="txtrequestDescriptionText">Hello ^senderName^,^appLink^^lineBreak^^appLink^</asp:TextBox><br/>
                requestActionLinkText:<asp:TextBox runat="server" ID="txtrequestActionLinkText">Accept</asp:TextBox><br/>
                requestImage:<asp:TextBox runat="server" ID="txtrequestImage">http://www.google.com/images/logos/mail_logo.png</asp:TextBox><br/>
                requestActionParam:<asp:TextBox runat="server" ID="txtrequestActionParam">param1=33amp;param4=55amp;goto=home</asp:TextBox><br/><br/>
            <br/><br/>
            <br/><br/><a runat="server" id="aShowLeaderboard">Show Leaderboard</a><br/>
            <br/>
           <asp:Button runat="server" ID="btnLeaderboard" Text="Set Leaderboard Value" OnClick="btnLeaderboard_Click" /><asp:TextBox runat="server" ID="txtLBValue">100</asp:TextBox>

            <br/><br/><br/>
            
            <b>Show First 10 Friends</b><br/>
            <asp:Literal ID="litData" runat="server"></asp:Literal>
            <br/><br/>
            <b>Show First 10 Blasts</b><br/>
            <asp:Literal ID="litBlasts" runat="server"></asp:Literal>
    </asp:Panel>
    <asp:Label ID="ErrorInfo" runat="server"></asp:Label>    
</asp:Content>
