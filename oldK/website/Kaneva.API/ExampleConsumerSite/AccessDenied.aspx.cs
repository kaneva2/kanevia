///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI;
using Kaneva.OAuth.Framework;

namespace ExampleConsumerSite
{
  public partial class AccessDenied : Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      var report = (OAuthProblemReport) Session["problem"];

      NameValueCollection parameters = HttpUtility.ParseQueryString(report.ToString());

      foreach (string key in parameters.Keys)
        problemReport.Text += key + " => " + parameters[key] + "<br/>";
    }
  }
}
