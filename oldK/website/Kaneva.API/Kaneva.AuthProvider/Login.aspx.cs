///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.Security;
using System.Web.UI;

namespace Kaneva.AuthProvider
{
  public partial class Login : Page
  {
    protected void LoginButton_Click(object sender, EventArgs e)
    {
      if (FormsAuthentication.Authenticate(UserName.Text, Password.Text))
        FormsAuthentication.RedirectFromLoginPage(UserName.Text, true);
      else
        FailureText.Text = "Invalid Login";
    }
  }
}
