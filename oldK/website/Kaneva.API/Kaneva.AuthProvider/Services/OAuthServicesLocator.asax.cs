///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System.Web;

namespace Kaneva.AuthProvider
{
  public static class OAuthServicesLocator
  {
    public static IOAuthServices Services
    {
      get { return (HttpContext.Current.ApplicationInstance as IOAuthServices); }
    }
  }
}
