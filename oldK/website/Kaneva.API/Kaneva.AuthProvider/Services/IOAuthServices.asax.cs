///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using Kaneva.OAuth.Provider;
using Kaneva.OAuth.Storage.Basic;

namespace Kaneva.AuthProvider
{
  public interface IOAuthServices
  {
    IOAuthProvider Provider { get; }
    ITokenRepository<AccessToken> AccessTokenRepository { get; }
    ITokenRepository<RequestToken> RequestTokenRepository { get; }
  }
}
