///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;
using Kaneva.OAuth.Framework;
using Kaneva.OAuth.Provider;

namespace Kaneva.OAuthChannel
{
    public partial class RequestToken : Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            IOAuthContext context = new OAuthContextBuilder().FromHttpRequest(Request);

            IOAuthProvider provider = OAuthServicesLocator.Provider;

            IToken token = provider.GrantRequestToken(context);

            Response.Write(token);
      }
      catch (OAuthException ex)
      {
        //Response.StatusCode = 400;
        Response.Write(ex.Report.ToString());        
      }

      Response.End();
    }
  }
}
