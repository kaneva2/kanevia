///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;
using Kaneva.OAuth.Framework;
using Kaneva.OAuth.Provider;

using System.Threading;
using System.Web;
using Kaneva.OAuth.Storage.Basic;

using log4net;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;

namespace Kaneva.OAuthChannel
{
  public partial class AccessToken : Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
          IOAuthContext context = new OAuthContextBuilder().FromHttpRequest(Request);

          //// Special Hard code WOK Case, does not need to Allow/Deny with OAuth
          //if (context.ConsumerKey.Equals("3DApp") && context.Signature.Equals("tCDILm0y9YDzjmhFHYv7AQI84zb5jQ6U9DAAIRBVKkg"))
          //{
              // Check auth for the passed in game ID
              if (Request["gameId"] != null)
              {
                  // Don't rely only on game id param, look it up...
                  APIAuthentication apiAuth = GetAPIAuth();

                 string tokenString = GetTokenString();
                 Kaneva.OAuth.Storage.Basic.RequestToken requestToken = RequestTokenRepository.GetToken(tokenString);

                  try
                  {
                     // int gameId = Convert.ToInt32(Request["gameId"].ToString ());

                      // Have they allowed?  Thread.CurrentPrincipal.Identity.Name
                      GameFacade gameFacade = new GameFacade();
                      if (gameFacade.IsUserAllowedGame(apiAuth.GameId, Convert.ToInt32(Page.User.Identity.Name)))
                      {
                          ApproveRequestForAccess(tokenString, requestToken);
                      }
                  }
                  catch (Exception exc)
                  {
                      m_logger.Error(exc.ToString ());
                      m_logger.Error("gId = " + Request["gameId"].ToString ());
                      m_logger.Error("Username = " + Page.User.Identity.Name);
                  }
              }
         // }

          IOAuthProvider provider = OAuthServicesLocator.Provider;

          IToken accessToken = provider.ExchangeRequestTokenForAccessToken(context);

          Response.Write(accessToken);
        }
        catch (OAuthException ex)
        {
           // Response.StatusCode = 400;
            Response.Write(ex.Report.ToString());
        }

        Response.End();
    }

    // Get Auth 
    private APIAuthentication GetAPIAuth()
    {
        string ConsumerSecret = "";
        string ConsumerKey = "";

        if (HttpContext.Current.Request.Params["oauth_signature"] != null && HttpContext.Current.Request.Params["oauth_consumer_key"] != null)
        {
            ConsumerSecret = HttpContext.Current.Request.Params["oauth_signature"].ToString();
            ConsumerKey = HttpContext.Current.Request.Params["oauth_consumer_key"].ToString();

            CommunityFacade gameFacade = new CommunityFacade();
            return gameFacade.GetAPIAuth(ConsumerKey, ConsumerSecret);
        }

        m_logger.Debug("GetAPIAuth unable tp look up auth");
        return new APIAuthentication();
    }

    string GetTokenString()
    {
        string token = Request[Parameters.OAuth_Token];

        if (string.IsNullOrEmpty(token)) throw new Exception("The token string in the parameters collection was invalid/missing");

        return token;
    }

    ITokenRepository<Kaneva.OAuth.Storage.Basic.AccessToken> AccessTokenRepository
    {
        get { return OAuthServicesLocator.AccessTokenRepository; }
    }

    ITokenRepository<Kaneva.OAuth.Storage.Basic.RequestToken> RequestTokenRepository
    {
        get { return OAuthServicesLocator.RequestTokenRepository; }
    }

    public string ConsumerKey
    {
        get { return RequestTokenRepository.GetToken(GetTokenString()).ConsumerKey; }
    }

    void ApproveRequestForAccess(string tokenString, Kaneva.OAuth.Storage.Basic.RequestToken requestToken)
    {
        

        var accessToken = new Kaneva.OAuth.Storage.Basic.AccessToken
        {
            ConsumerKey = requestToken.ConsumerKey,
            Realm = requestToken.Realm,
            Token = Guid.NewGuid().ToString(),
            TokenSecret = Guid.NewGuid().ToString(),
            UserName =Page.User.Identity.Name,
            ExpireyDate = DateTime.Now.AddMonths(12),
            Roles = new string[] { }
        };

        AccessTokenRepository.SaveToken(accessToken);

        requestToken.AccessToken = accessToken;

        RequestTokenRepository.SaveToken(requestToken);
    }

    // Logger
    private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
  }
}
