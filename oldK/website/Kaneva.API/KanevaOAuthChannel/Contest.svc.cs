///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿/// Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Net;
using System.Threading;

using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.ComponentModel;

using log4net;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

// The following line sets the default namespace for DataContract serialized typed to be ""
[assembly: ContractNamespace("", ClrNamespace = "ContestREST")]

namespace ContestREST
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Contest" in code, svc and config file together.
    [ServiceBehavior(IncludeExceptionDetailInFaults = true), AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed), ServiceContract]
    public partial class ContestService
    {
        /// <summary>
        /// Contests
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Contests?orderBy={orderBy}&filter={filter}&pageNumber={pageNumber}&pageSize={pageSize}")]
        [Description("Gets a list of Contests.")]
        public PagedList<Contest> Contests(string orderBy, string filter, int pageNumber, int pageSize)
        {
            if (orderBy == null)
            {
                orderBy = "";
            }

            if (filter == null)
            {
                filter = "";
            }

            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;

            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);
            
            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.GetContests(userId, orderBy, filter, pageNumber, pageSize);
        }

        /// <summary>
        /// SearchContests
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "SearchContests?searchString={searchString}&orderBy={orderBy}&pageNumber={pageNumber}&pageSize={pageSize}")]
        [Description("Search for Contests.")]
        public PagedList<Contest> SearchContests(string searchString, string orderBy, int pageNumber, int pageSize)
        {
            if (searchString == null)
            {
                searchString = "";
            }

            if (orderBy == null)
            {
                orderBy = "";
            }

            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;

            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.SearchContests (userId, searchString, orderBy, pageNumber, pageSize);
        }

        /// <summary>
        /// InsertContest
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "InsertContest?title={title}&description={description}")]
        [Description("Insert a Contest.")]
        public Contest InsertContest (string title, string description)
        {
            ContestFacade contestFacade = new ContestFacade();
            UserFacade userFacade = new UserFacade();

          
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);
            User user = userFacade.GetUser (userId);

            Contest contest = new Contest();
            contest.Owner = new ContestUser (userId, user.Username, user.Gender, user.ThumbnailSmallPath, user.ThumbnailMediumPath, user.ThumbnailLargePath, user.ThumbnailSquarePath);
            contest.Title = title;
            contest.Description = description;
            contestFacade.InsertContest (contest);

            UriTemplateMatch match = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            UriTemplate template = new UriTemplate("/Contest/{id}");
            WebOperationContext.Current.OutgoingResponse.SetStatusAsCreated(template.BindByPosition(match.BaseUri, contest.ContestId.ToString ()));

            return contest;
        }

        /// <summary>
        /// Contest
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Contest?contestId={contestId}")]
        [Description("Get a Contest.")]
        public Contest Contest(UInt64 contestId)
        {
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.GetContest(contestId);
        }

        /// <summary>
        /// ContestsEntered
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "ContestsEntered?orderBy={orderBy}&filter={filter}&pageNumber={pageNumber}&pageSize={pageSize}")]
        [Description("Gets a list of Contests Entered.")]
        public PagedList<Contest> ContestsEntered(string orderBy, string filter, int pageNumber, int pageSize)
        {
            if (orderBy == null)
            {
                orderBy = "";
            }

            if (filter == null)
            {
                filter = "";
            }

            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;

            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.GetContestsEntered (userId, orderBy, filter, pageNumber, pageSize);
        }

        /// <summary>
        /// ContestsFollowing
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "ContestsFollowing?orderBy={orderBy}&filter={filter}&pageNumber={pageNumber}&pageSize={pageSize}")]
        [Description("Gets a list of Contests Follwing.")]
        public PagedList<Contest> ContestsFollowing(string orderBy, string filter, int pageNumber, int pageSize)
        {
            if (orderBy == null)
            {
                orderBy = "";
            }

            if (filter == null)
            {
                filter = "";
            }

            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;

            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.GetContestsFollowing(userId, orderBy, filter, pageNumber, pageSize);
        }

        /// <summary>
        /// ContestsVotedOn
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "ContestsVotedOn?orderBy={orderBy}&filter={filter}&pageNumber={pageNumber}&pageSize={pageSize}")]
        [Description("Gets a list of Contests Voted On.")]
        public PagedList<Contest> ContestsVotedOn(string orderBy, string filter, int pageNumber, int pageSize)
        {
            if (orderBy == null)
            {
                orderBy = "";
            }

            if (filter == null)
            {
                filter = "";
            }

            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;

            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.GetContestsVotedOn(userId, orderBy, filter, pageNumber, pageSize);
        }

        /// <summary>
        /// InsertContestEntry
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "InsertContestEntry?description={description}")]
        [Description("Insert a Contest Entry.")]
        public ContestEntry InsertContestEntry (string description)
        {
            ContestFacade contestFacade = new ContestFacade();
            UserFacade userFacade = new UserFacade();


            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);
            User user = userFacade.GetUser(userId);

            UriTemplateMatch match = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            UriTemplate template = new UriTemplate("/ContestEntry/{id}");
            WebOperationContext.Current.OutgoingResponse.SetStatusAsCreated(template.BindByPosition(match.BaseUri, ""));

            ContestEntry contestEntry = new ContestEntry();
            contestEntry.Owner = new ContestUser (userId, user.Username, user.Gender, user.ThumbnailSmallPath, user.ThumbnailMediumPath, user.ThumbnailLargePath, user.ThumbnailSquarePath);
            contestEntry.Description = description;
            contestFacade.InsertContestEntry(contestEntry);

            return contestEntry;
        }

        /// <summary>
        /// ContestEntries
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "ContestEntries?contestId={contestId}&orderBy={orderBy}&filter={filter}&pageNumber={pageNumber}&pageSize={pageSize}")]
        [Description("Gets a list of Contest Entries.")]
        public PagedList<ContestEntry> ContestEntries(ulong contestId, string orderBy, string filter, int pageNumber, int pageSize)
        {
            if (orderBy == null)
            {
                orderBy = "";
            }

            if (filter == null)
            {
                filter = "";
            }

            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;

            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.GetContestEntries (contestId, orderBy, filter, pageNumber, pageSize);
        }

        /// <summary>
        /// ContestEntry
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "ContestEntry?contestEntryId={contestEntryId}")]
        [Description("Gets a list of Contests Follwing.")]
        public ContestEntry ContestEntry(UInt64 contestEntryId)
        {
            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.GetContestEntry(contestEntryId);
        }

        /// <summary>
        /// InsertComment
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "InsertComment?contestId={contestId}&contestEntryId={contestEntryId}&comment={comment}")]
        [Description("Insert a Contest Comment.")]
        public int InsertComment(ulong contestId, ulong contestEntryId, string comment)
        {
            ContestFacade contestFacade = new ContestFacade();
            UserFacade userFacade = new UserFacade();

            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            UriTemplateMatch match = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            UriTemplate template = new UriTemplate("/ContestComments/{id}");
            WebOperationContext.Current.OutgoingResponse.SetStatusAsCreated(template.BindByPosition(match.BaseUri, ""));

            return contestFacade.InsertComment(contestId, contestEntryId, userId, comment);
        }

        /// <summary>
        /// ContestComments
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "ContestComments?contestId={contestId}&contestEntryId={contestEntryId}&orderBy={orderBy}&pageNumber={pageNumber}&pageSize={pageSize}")]
        [Description("Gets a list of Contest Comments.")]
        public PagedList<ContestComment> ContestComments (ulong contestId, ulong contestEntryId, string orderBy, int pageNumber, int pageSize)
        {
            if (orderBy == null)
            {
                orderBy = "";
            }

            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;

            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.GetContestComments(contestId, contestEntryId, pageNumber, pageSize, orderBy);
        }

        /// <summary>
        /// InsertVote
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "InsertVote?contestId={contestId}&contestEntryId={contestEntryId}")]
        [Description("Insert a Contest Vote.")]
        public int InsertVote (ulong contestId, ulong contestEntryId)
        {
            ContestFacade contestFacade = new ContestFacade();
            UserFacade userFacade = new UserFacade();

            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            UriTemplateMatch match = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            UriTemplate template = new UriTemplate("/Vote/");
            WebOperationContext.Current.OutgoingResponse.SetStatusAsCreated(template.BindByPosition(match.BaseUri, ""));

            return contestFacade.InsertVote(userId, "", contestId, contestEntryId);
        }

        /// <summary>
        /// Follow Contest
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "FollowContest?contestId={contestId}")]
        [Description("Follow a Contest.")]
        public int FollowContest (ulong contestId)
        {
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.FollowContest(contestId, userId);
        }

        /// <summary>
        /// UnFollow Contest
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "UnFollowContest?contestId={contestId}")]
        [Description("UnFollow a Contest.")]
        public int UnFollowContest(ulong contestId)
        {
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.UnFollowContest(contestId, userId);
        }

        /// <summary>
        /// HasUserVotedOnContest
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "HasUserVotedOnContest?contestId={contestId}")]
        [Description("Has current user voted on a Contest.")]
        public bool HasUserVotedOnContest(ulong contestId)
        {
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.HasUserVotedOnContest(contestId, userId);
        }

        /// <summary>
        /// HasUserEnteredContest
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "HasUserEnteredContest?contestId={contestId}")]
        [Description("Has current user Entered a Contest.")]
        public bool HasUserEnteredContest(ulong contestId)
        {
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.HasUserEnteredContest(contestId, userId);
        }

        /// <summary>
        /// IsUserFollowingContest
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "IsUserFollowingContest?contestId={contestId}")]
        [Description("Is current user following a Contest.")]
        public bool IsUserFollowingContest(ulong contestId)
        {
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.IsUserFollowingContest(contestId, userId);
        }


        /// <summary>
        /// ContestEntryVoters
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "ContestEntryVoters?contestEntryId={contestEntryId}&orderBy={orderBy}&filter={filter}")]
        [Description("Gets a list of Contest Entry Voters.")]
        public IList<ContestUser> ContestEntryVoters (ulong contestEntryId, string orderBy, string filter)
        {
            if (orderBy == null)
            {
                orderBy = "";
            }

            if (filter == null)
            {
                filter = "";
            }

            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.GetContestEntryVoters(contestEntryId, orderBy, filter);
        }

        /// <summary>
        /// ContestFollowers
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "ContestFollowers?contestId={contestId}&orderBy={orderBy}&filter={filter}")]
        [Description("Gets a list of Contest Followers.")]
        public IList<ContestUser> ContestFollowers(ulong contestId, string orderBy, string filter)
        {
            if (orderBy == null)
            {
                orderBy = "";
            }

            if (filter == null)
            {
                filter = "";
            }

            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.GetContestFollowers(contestId, orderBy, filter);
        }

        /// <summary>
        /// ContestsUserWon
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "ContestsUserWon?orderBy={orderBy}&filter={filter}&pageNumber={pageNumber}&pageSize={pageSize}")]
        [Description("Gets a list of Contests the user has won.")]
        public PagedList<Contest> ContestsUserWon(string orderBy, string filter, int pageNumber, int pageSize)
        {
            if (orderBy == null)
            {
                orderBy = "";
            }

            if (filter == null)
            {
                filter = "";
            }

            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;

            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.GetContestsUserWon (userId, orderBy, filter, pageNumber, pageSize);
        }


        /// <summary>
        /// ContestsNeedWinner
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "ContestsNeedWinner?orderBy={orderBy}&filter={filter}&pageNumber={pageNumber}&pageSize={pageSize}")]
        [Description("Gets a list of Contests the user has won.")]
        public PagedList<Contest> ContestsNeedWinner(string orderBy, string filter, int pageNumber, int pageSize)
        {
            if (orderBy == null)
            {
                orderBy = "";
            }

            if (filter == null)
            {
                filter = "";
            }

            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;

            ContestFacade contestFacade = new ContestFacade();
            return contestFacade.GetContestsNeedWinner(userId, orderBy, filter, pageNumber, pageSize);
        }

    }
}
