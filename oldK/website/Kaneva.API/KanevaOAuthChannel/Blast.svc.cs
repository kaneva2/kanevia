///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿/// Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Net;
using log4net;
using System.Threading;
using System.Web;

using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.ComponentModel;

using KlausEnt.KEP.Kaneva.Mailer;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;

// The following line sets the default namespace for DataContract serialized typed to be ""
[assembly: ContractNamespace("", ClrNamespace = "ViralREST")]

namespace ViralREST
{
    // TODO: Please set IncludeExceptionDetailInFaults to false in production environments
    [ServiceBehavior(IncludeExceptionDetailInFaults = true), AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed), ServiceContract]
    public partial class BlastService
    {

        /// <summary>
        /// Blasts
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Description("Gets a list of Blasts.")]
        [WebGet(UriTemplate = "Blasts?pageNumber={pageNumber}&pageSize={pageSize}")]
        //[WebCache(
        [OperationContract]
        public PagedList<Blast> Blasts (int pageNumber, int pageSize)
        {
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;


            PagedList<Blast> plBlasts = new BlastFacade().GetActiveBlasts(userId, userId, pageSize, pageNumber);
            
            if (plBlasts.Count.Equals (0))
            {
               // WebOperationContext.Current.OutgoingResponse.SetStatusAsNotFound();
               // return null;
                return plBlasts; 
            }

            return plBlasts; 
        }

        /// <summary>
        /// BlastReplies
        /// </summary>
        /// <param name="entryId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Description("Gets a list of Blast Replies.")]
        [WebGet(UriTemplate = "BlastReplies?entryId={entryId}&pageNumber={pageNumber}&pageSize={pageSize}")]
        //[WebCache(
        [OperationContract]
        protected PagedList<BlastReply> BlastReplies(ulong entryId, int pageNumber, int pageSize)
        {
            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;


            return (PagedList<BlastReply>)new BlastFacade().GetBlastReplies(entryId, pageNumber, pageSize, "");
        }

        /// <summary>
        // SendBlast
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Description("Add a new Blast")]
        [WebGet(UriTemplate = "SendBlast?message={message}&imageUrl={imageUrl}&videoUrl={videoUrl}&linkURL={linkURL}")]
        [OperationContract]
        public void SendBlast(string message, string imageUrl, string videoUrl, string linkURL)
        {
            UserFacade userFacade = new UserFacade();
            BlastFacade blastFacade = new BlastFacade();
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            m_logger.Debug("Send Blast Called");

            int id = 0;
            User user = userFacade.GetUser(userId);

            if (imageUrl.Trim().Length > 0)
            {
                id = blastFacade.SendPhotoBlast(userId, 0, "", true, user.Username, user.NameNoSpaces, imageUrl, "", "", user.ThumbnailSmallPath);
            }
            else if (videoUrl.Trim().Length > 0)
            {
                id = blastFacade.SendVideoBlast(userId, 0, "", true, user.Username, user.NameNoSpaces, "", videoUrl, "", user.ThumbnailSmallPath);
            }
            else if (linkURL.Trim().Trim().Length > 0)
            {
                id = blastFacade.SendLinkBlast(userId, 0, user.Username, user.NameNoSpaces, "", linkURL, "", "", user.ThumbnailSmallPath);
            }
            else
            {
                id = blastFacade.SendTextBlast(userId, 0, user.Username, user.NameNoSpaces, message, "", user.ThumbnailSmallPath);
            }

            UriTemplateMatch match = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            UriTemplate template = new UriTemplate("/blast/{id}");
            WebOperationContext.Current.OutgoingResponse.SetStatusAsCreated(template.BindByPosition(match.BaseUri, id.ToString()));
        }

      
        /// <summary>
        // SendBlastCommment
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Description("Add a new Blast Comment")]
        [WebGet(UriTemplate = "SendBlastCommment?entryId={entryId}&message={message}")]
        [OperationContract]
        public int SendBlastCommment(ulong entryId, string message)
        {
            BlastFacade blastFacade = new BlastFacade();
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);
            return blastFacade.InsertBlastReply(entryId, userId, message, "");
        }

        /// <summary>
        // SendRequest
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Description("Add a new Reqeust")]
        [WebGet(UriTemplate = "SendRequest?toUsername={toUsername}&requestDescriptionText={requestDescriptionText}&requestActionLinkText={requestActionLinkText}&requestImage={requestImage}&requestActionParam={requestActionParam}&subject={subject}&emailDescriptionText={emailDescriptionText}&emailSubject={emailSubject}")]
        [OperationContract]
        public void SendRequest (string toUsername, string requestDescriptionText, string requestActionLinkText,
			string requestImage, string requestActionParam, string subject, string emailDescriptionText, string emailSubject)
        {
            APIAuthentication apiAuth = GetAPIAuth();
            UserFacade userFacade = new UserFacade();
            BlastFacade blastFacade = new BlastFacade();

            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            // Do some validations
            if (requestActionLinkText == null)
            {
                requestActionLinkText = "Accept";
            }

            if (requestImage ==null)
            {
                requestImage = "";
            }

            if (requestActionParam == null)
            {
                requestActionParam = "";
            }
            
            if (subject == null)
            {
                subject = "";
            }

            if (emailDescriptionText == null)
            {
                emailDescriptionText = requestDescriptionText;
            }

            if (emailSubject == null)
            {
                emailSubject = subject;
            }



            //ThrottleFacade throttleFacade = new ThrottleFacade();
            //if (throttleFacade.CheckThrottle(userId, apiAuth.CommunityId, ThrottleConfigType.AppRequest).Equals(ThrottleResults.OverLimit))
            //{
            //    WebOperationContext.Current.OutgoingResponse.SetStatusAsNotFound("Over Throttle Limit");
            //    return;
            //}

            User userTo = userFacade.GetUser(toUsername);

            if (userTo.UserId > 0 && userFacade.AreFriends(userId, userTo.UserId))
            {
                int id = 0;

                CommunityFacade communityFacade = new CommunityFacade ();
                Community community = communityFacade.GetCommunity(apiAuth.CommunityId);

                string appUrl = BuildAppLink(apiAuth.GameId, requestActionParam);

                // stpUrl needed for in-world invites
                string stpUrl = (StpUrl.MakeUrlPrefix(community.Name) + "/");

                User fromUser = userFacade.GetUser(userId);

                ThrottleFacade throttleFacade = new ThrottleFacade();

                // Standardize subject for WoK & KIM mediums
                string messageSubject = subject;
                messageSubject = messageSubject.Replace("^senderUserThenDisplayName^", "^senderName^");
                messageSubject = messageSubject.Replace("^senderDisplayThenUserName^", "^senderName^");

                // Standardize message. If message is a gift, in correct format, sent by our template code, show gift message.
                // Otherwise, default to standard API invite message.
                string message = fromUser.Username + " has invited you to join them in <br/>" + community.Name;
                bool isGiftInvite = false;
                bool passedInWorldThrottles = true;

                if (messageSubject == "Gift from ^senderName^")
                {

                    string messageGiftRequest = (String.IsNullOrWhiteSpace(requestDescriptionText) ? "" : requestDescriptionText.Trim());
                    messageGiftRequest = messageGiftRequest.Replace("^senderUserThenDisplayName^", "^senderName^");
                    messageGiftRequest = messageGiftRequest.Replace("^senderDisplayThenUserName^", "^senderName^");

                    int giftMessageStart = messageGiftRequest.IndexOf("^senderName^ has sent you ");
                    int giftMessageEnd = (giftMessageStart >= 0 ? (messageGiftRequest.IndexOf(" from ^gamename^", giftMessageStart) + (" from ^gamename^").Length) : (giftMessageStart - 1));

                    if (giftMessageEnd > giftMessageStart)
                    {
                        isGiftInvite = true;
                        message = messageGiftRequest.Substring(giftMessageStart, giftMessageEnd - giftMessageStart);

                        // Per Billy Request, only send these 1 per hour, 3 per day.
                        // Check the hourly throttle first. Worst case scenerio, use up an hourly throttle at 11:59pm and daily fails. Only lose notifications for an hour.
                        passedInWorldThrottles = throttleFacade.CheckThrottle(userTo.UserId, KanevaGlobals.WokGameId, ThrottleConfigType.AppRequestHourlyGiftNotification).Equals(ThrottleResults.Ok);
                        passedInWorldThrottles = (passedInWorldThrottles ? throttleFacade.CheckThrottle(userTo.UserId, KanevaGlobals.WokGameId, ThrottleConfigType.AppRequestDailyGiftNotification).Equals(ThrottleResults.Ok) : passedInWorldThrottles);
                    }
                }
                
                // Metrics tracking for accepts
                int requestType = (isGiftInvite ? Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_GAME_API_GIFT_REQUEST : Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_GAME_API_REQUEST);
                string requestId = userFacade.InsertTrackingRequest(requestType, userId, apiAuth.GameId, apiAuth.CommunityId);
                string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_BLAST, userTo.UserId);

                id = blastFacade.SendAppRequest(userId,  userTo.UserId, 
                    String.IsNullOrWhiteSpace(requestDescriptionText) ? "" : requestDescriptionText.Trim (),
                    String.IsNullOrWhiteSpace(requestActionLinkText) ? "" : requestActionLinkText.Trim (),
                    community.Name, apiAuth.GameId, appUrl + "&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeSentIdBlast,  
                    String.IsNullOrWhiteSpace(requestImage) ? "" : requestImage.Trim (), 
                    "",
                    subject);

                if (id != -1)
                {
                    // Send Email?
                    if (userTo.NotificationPreference.NotifyWorldRequests && userTo.StatusId.Equals((int)Constants.eUSER_STATUS.REGVALIDATED))
                    {
                        if (throttleFacade.CheckThrottle(userTo.UserId, apiAuth.GameId, ThrottleConfigType.AppRequestEmail).Equals(ThrottleResults.Ok))
                        {
                            string requestTypeSentIdEmail = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_EMAIL, userTo.UserId);
                            string strSubject = emailSubject;
                            string strEmailText = requestDescriptionText;

                            // Make the standard text replacements
                            ReplaceRequestPlaceholders(fromUser, userTo, community, appUrl, requestImage, requestActionLinkText, requestTypeSentIdEmail, false, ref strSubject, ref strEmailText);

                            // Send email
                            int overloadId = Queuer.AddOverloadField(0, "#subject#", strSubject);

                            if (overloadId > 0)
                            {
                                Queuer.AddOverloadField(overloadId, "#applogo#", "<img src=\"" + Common.GetBroadcastChannelImageURL(community.ThumbnailMediumPath, "me") + "\" border=\"0\"/>");
                                Queuer.AddOverloadField(overloadId, "#messagehtml#", strEmailText);
                                Queuer.AddOverloadField(overloadId, "#appname#", community.Name);

                                Queuer.QueueEmail(30, 58, userId, userTo.Email, fromUser.Email, 0, overloadId);
                            }
                            else
                            {
                                m_logger.Error("No email sent - Unable to obtain overloadId" + userTo.Username);
                            }


                        }
                        else
                        {
                            m_logger.Error("No email sent - over throttle limit " + userTo.Username);
                        }
                    }

                    m_logger.Debug("SendRequest API - In World invite check. User:" + userTo.Username + " STPURL:" + stpUrl + " ID:" + userTo.UserId.ToString());

                    try                                               
                    {
                        bool userInWorld = false;
                        bool userInKIM = false;

                        if (UsersUtility.GetCurrentPlaceInfo(userTo.UserId) != null)
                        {
                            userInWorld = true;
                        }
                        else
                        {
                            // Determine if the user is in KIM

                            userInKIM = UsersUtility.IsUserOnlineInKIM(userTo.UserId);
                        }

                        if (userInWorld && passedInWorldThrottles)
                        {
                            string fromUsername = UsersUtility.GetUserNameFromId(userId);

                            string requestTypeSentIdInWorld = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_IN_WORLD, userTo.UserId);

                            ReplaceRequestPlaceholders(fromUser, userTo, community, appUrl, requestImage, requestActionLinkText, requestTypeSentIdInWorld, true, ref messageSubject, ref message);

                            int ret = 0;

                            if (isGiftInvite)
                            {
                                ret = userFacade.SendInWorldGiftInviteToPlayer(fromUsername, toUsername,
                                                                            message,
                                                                            stpUrl,
                                                                            requestTypeSentIdInWorld);
                            }
                            else
                            {
                                ret = userFacade.SendInWorldInviteToPlayer(fromUsername, toUsername,
                                                                            message,
                                                                            stpUrl,
                                                                            requestTypeSentIdInWorld);
                            }

                            if (ret != 0)
                            {
                                m_logger.Error("No in world invite sent - Unable to send to:" + userTo.Username);
                            }

                            m_logger.Debug("SendRequest API In World invite sent to:" + userTo.Username + " STPURL:" + stpUrl);
                        }
                        else if ( userInKIM)
                        {
                            string fromUsername = UsersUtility.GetUserNameFromId(userId);

                            string requestTypeSentIdInKIM = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_KIM, userTo.UserId);

                            ReplaceRequestPlaceholders(fromUser, userTo, community, appUrl, requestImage, requestActionLinkText, requestTypeSentIdInKIM, true, ref messageSubject, ref message);

                            string gotourl = appUrl + "&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeSentIdInKIM;

                            gotourl = gotourl.Replace("&", "&amp;");

                            string kimUrl = @"<a href=""";
                            kimUrl += gotourl;
                            kimUrl += @"""><br/><br/><font color=""#3189fe"">CLICK HERE TO ACCEPT &amp; GO</font></a>";

                            bool ret = userFacade.SendKIMInviteToPlayer(fromUsername, toUsername, message, kimUrl);

                            if ( ! ret)
                            {
                                m_logger.Error("No in world invite sent to KIM - Unable to send to:" + userTo.Username);
                            }

                            m_logger.Debug("SendRequest API In World invite sent to KIM:" + userTo.Username + " STPURL:" + stpUrl);
                        }
                        else
                        {
                            m_logger.Debug("SendRequest API - No In World invite sent. User not in world. User:" + userTo.Username + " STPURL:" + stpUrl);
                        }
                    }
                    catch (Exception ex)
                    {
                        m_logger.Error("Exception SendRequest API - Ex:", ex);
                    }
                }
                else
                {
                    m_logger.Error("No email sent - Blast failure " + userTo.Username);
                }

     
                UriTemplateMatch match = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
                UriTemplate template = new UriTemplate("/blast/{id}");
                WebOperationContext.Current.OutgoingResponse.SetStatusAsCreated(template.BindByPosition(match.BaseUri, id.ToString()));
            }
            else
            {
                m_logger.Error("Send request failer - the users are not friends " + userId + " to "  + toUsername);
                WebOperationContext.Current.OutgoingResponse.SetStatusAsNotFound("These users are not friends");
            }
        }

        /// <summary>
        /// BuildAppLink
        /// </summary>
        private string BuildAppLink(int gameId, string strLinkParams)
        {
            string sitePrefix = "www";
            if (KanevaGlobals.WokGameId == 3298)
            {
                sitePrefix = "preview";
            }
            else if (KanevaGlobals.WokGameId == 5316)
            {
                sitePrefix = "dev-www";
            }

            string appUrl = "http://" + sitePrefix + ".kaneva.com/kgp/playwok.aspx?goto=U" + gameId.ToString() + "&ILC=MM3D&link=private";

            // Add custom parameters
            if (!String.IsNullOrWhiteSpace(strLinkParams))
            {
                // Clean special params
                strLinkParams = strLinkParams.Replace("goto=", "Reserved1=");
                strLinkParams = strLinkParams.Replace("ILC=", "Reserved2=");
                strLinkParams = strLinkParams.Replace("link=", "Reserved3=");

                // Put in amps
                strLinkParams = strLinkParams.Replace("amp;", "&");

                if (!strLinkParams.StartsWith("&"))
                {
                    strLinkParams = "&" + strLinkParams;
                }

                appUrl += strLinkParams;
            }

            return appUrl;
        }

        // Get Auth 
        private APIAuthentication GetAPIAuth()
        {
            string ConsumerSecret = "";
            string ConsumerKey = "";

            if (HttpContext.Current.Request.Params["oauth_signature"] != null && HttpContext.Current.Request.Params["oauth_consumer_key"] != null)
            {
                ConsumerSecret = HttpContext.Current.Request.Params["oauth_signature"].ToString();
                ConsumerKey = HttpContext.Current.Request.Params["oauth_consumer_key"].ToString();

                CommunityFacade gameFacade = new CommunityFacade();
                return gameFacade.GetAPIAuth(ConsumerKey, ConsumerSecret);
            }

            m_logger.Debug("GetAPIAuth unable tp look up auth");
            return new APIAuthentication();
        }

        // Replace placeholder fields in request message and subject
        private void ReplaceRequestPlaceholders(User fromUser, User toUser, Community community, string appUrl, string requestImage, string requestActionLinkText, string requestTypeSentId, bool formatLinksAsText, ref string subject, ref string message)
        {
            subject = subject.Replace("^senderName^", fromUser.Username);

            // New per Billy request
            subject = subject.Replace("^gamename^", community.Name);
            subject = subject.Replace("^recipientName^", toUser.Username);
            subject = subject.Replace("^senderRealName^", fromUser.DisplayName);
            subject = subject.Replace("^recipientRealName^", toUser.DisplayName);

            if (subject.Length.Equals(0))
            {
                subject = "Request from " + fromUser.Username;
            }

            // Body format...
            string fromUserLink = "<a style='text-decoration: none;' href=\"" + fromUser.URL + "\">" + fromUser.Username + "</a>";
            string appLink = "<a style='text-decoration: none;' href=\"" + appUrl + "&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeSentId + "\">" + community.Name + "</a>";
            string imgLink = "<a href=\"" + appUrl + "\"><img src=\"" + requestImage + "\"/></a>";
            string actionLink = "<a style='text-decoration: none;' href=\"" + appUrl + "&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeSentId + "\"><b>" + requestActionLinkText + "</b></a>";

            // Substitutions...  ^lineBreak^ ^senderName^ ^appLink^ ^receipientName^
            message = message.Replace("^lineBreak^", "<br/>");
            message = message.Replace("^senderName^", (formatLinksAsText ? fromUser.Username : fromUserLink));
            message = message.Replace("^appLink^", (formatLinksAsText ? requestActionLinkText : actionLink));

            // New per Billy request
            message = message.Replace("^gamename^", community.Name);
            message = message.Replace("^gameName^", community.Name);
            message = message.Replace("^recipientName^", toUser.Username);
            message = message.Replace("^recepientName^", toUser.Username);
            message = message.Replace("^receipientName^", toUser.Username);
            message = message.Replace("^recipientAvatar^", "<img src=\"" + Common.GetProfileImageURL(fromUser.ThumbnailSmallPath, "sm", fromUser.Gender) + "\" border=\"0\"/>");

            message = message.Replace("^senderRealName^", fromUser.DisplayName);
            message = message.Replace("^recipientRealName^", toUser.DisplayName);

            message = message.Replace("^senderDisplayThenUserName^", (fromUser.DisplayName + (string.IsNullOrEmpty(fromUser.DisplayName) ? fromUser.Username : " (" + fromUser.Username + ")")));
            message = message.Replace("^senderUserThenDisplayName^", (fromUser.Username + (string.IsNullOrEmpty(fromUser.DisplayName) ? "" : " (" + fromUser.DisplayName + ")")));
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


    }

}
