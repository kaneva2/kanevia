///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.ServiceModel;
using System.ServiceModel.Syndication;
using System.ServiceModel.Web;
using System.Threading;
using Kaneva.OAuthChannel.OAuth.Wcf.Repositories;
using Microsoft.ServiceModel.Web;

using System.ServiceModel.Activation;


namespace Kaneva.OAuthChannel
{
  [ServiceBehavior(IncludeExceptionDetailInFaults = true), AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
  [ServiceContract]
  public class FeedService
  {
    /// <summary>
    /// Returns an Atom feed.
    /// </summary>
    /// <returns>Atom feed in response to a HTTP GET request at URLs conforming to the URI template of the WebGetAttribute.</returns>
    [WebHelp(Comment = "Sample contacts feed.")]
    [WebGet]
    [OperationContract]
    public Atom10FeedFormatter GetContacts()
    {
      var items = new List<SyndicationItem>();

      var repository = new ContactsRepository();

      foreach (Contact contact in repository.GetContactsForUser(Thread.CurrentPrincipal.Identity.Name))
      {
        items.Add(new SyndicationItem
          {
            Id = String.Format(CultureInfo.InvariantCulture, "http://contacts.org/{0}", Guid.NewGuid()),
            Title = new TextSyndicationContent(contact.FullName),
            LastUpdatedTime = DateTime.UtcNow,
            Authors =
              {
                new SyndicationPerson
                  {
                    Name = "Sample Author"
                  }
              },
            Content = new TextSyndicationContent(contact.Email),
          });
      }

      // create the feed containing the syndication items.

      var feed = new SyndicationFeed
        {
          // The feed must have a unique stable URI id

          Id = "http://contacts/Feed",
          Title = new TextSyndicationContent("Contacts feed"),
          Items = items
        };

      feed.AddSelfLink(WebOperationContext.Current.IncomingRequest.GetRequestUri());

      WebOperationContext.Current.OutgoingResponse.ContentType = ContentTypes.Atom;

      return feed.GetAtom10Formatter();
    }
  }
}
