///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿/// Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Net;
using log4net;
using System.Threading;
using System.Web;

using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.ComponentModel;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;

// The following line sets the default namespace for DataContract serialized typed to be ""
[assembly: ContractNamespace("", ClrNamespace = "EventREST")]

namespace EventREST
{
    // TODO: Please set IncludeExceptionDetailInFaults to false in production environments
    [ServiceBehavior(IncludeExceptionDetailInFaults = true), AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed), ServiceContract]
    public partial class EventService
    {

        /// <summary>
        /// My Events
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Description("Gets a list of My Events.")]
        [WebGet(UriTemplate = "MyEvents?pageNumber={pageNumber}&pageSize={pageSize}")]
        [OperationContract]
        public PagedList<Event> MyEvents (int pageNumber, int pageSize)
        {
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;


            ////PagedList<Blast> plBlasts = new BlastFacade().GetActiveBlasts(userId, userId, pageSize, pageNumber);
            
            ////if (plBlasts.Count.Equals (0))
            ////{
            ////   // WebOperationContext.Current.OutgoingResponse.SetStatusAsNotFound();
            ////   // return null;
            ////    return plBlasts; 
            ////}

            ////return plBlasts; 

            return null;
        }

        /// <summary>
        /// Friend Events
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Description("Gets a list of Friends Events.")]
        [WebGet(UriTemplate = "FriendEvents?friendId={friendId}&pageNumber={pageNumber}&pageSize={pageSize}")]
        [OperationContract]
        public PagedList<Event> FriendEvents(int friendId, int pageNumber, int pageSize)
        {
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;


            ////PagedList<Blast> plBlasts = new BlastFacade().GetActiveBlasts(userId, userId, pageSize, pageNumber);

            ////if (plBlasts.Count.Equals (0))
            ////{
            ////   // WebOperationContext.Current.OutgoingResponse.SetStatusAsNotFound();
            ////   // return null;
            ////    return plBlasts; 
            ////}

            ////return plBlasts; 

            return null;
        }

        /// <summary>
        /// CommunityEvents
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Description("Gets a list of Community Events.")]
        [WebGet(UriTemplate = "CommunityEvents?communityId={communityId}&pageNumber={pageNumber}&pageSize={pageSize}")]
        [OperationContract]
        public PagedList<Event> CommunityEvents(int communityId, int pageNumber, int pageSize)
        {
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;


            ////PagedList<Blast> plBlasts = new BlastFacade().GetActiveBlasts(userId, userId, pageSize, pageNumber);

            ////if (plBlasts.Count.Equals (0))
            ////{
            ////   // WebOperationContext.Current.OutgoingResponse.SetStatusAsNotFound();
            ////   // return null;
            ////    return plBlasts; 
            ////}

            ////return plBlasts; 

            return null;
        }

        /// <summary>
        /// EventUsers
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Description("Gets a list of Users attending Event.")]
        [WebGet(UriTemplate = "EventUsers?eventId={eventId}&pageNumber={pageNumber}&pageSize={pageSize}")]
        [OperationContract]
        public PagedList<User> EventUsers(int eventId, int pageNumber, int pageSize)
        {
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;


            ////PagedList<Blast> plBlasts = new BlastFacade().GetActiveBlasts(userId, userId, pageSize, pageNumber);

            ////if (plBlasts.Count.Equals (0))
            ////{
            ////   // WebOperationContext.Current.OutgoingResponse.SetStatusAsNotFound();
            ////   // return null;
            ////    return plBlasts; 
            ////}

            ////return plBlasts; 

            return null;
        }

        /// <summary>
        // AddEvent
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Description("Add a new Event")]
        [WebGet(UriTemplate = "AddEvent?communityId={communityId}&title={title}&details={details}&startTime={startTime}&endTime={endTime}&typeId={typeId}&isAPEvent={isAPEvent}")]
        [OperationContract]
        public void AddEvent(int communityId, string title, string details,
            DateTime startTime, DateTime endTime, int typeId, string isAPEvent)
        {
            EventFacade eventFacade = new EventFacade();
            UserFacade userFacade = new UserFacade();
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            int id = 0;
            User user = userFacade.GetUser(userId);

              // Zone info      
            LocationDetail locationDetail = eventFacade.GetLocationDetail(0, 0, 0, communityId);

            if (locationDetail.ZoneIndex.Equals (0))
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                WebOperationContext.Current.OutgoingResponse.StatusDescription = "New homes and communities must be visited in world before creating events";
                return;
            }

            int ConcurrentCheck = eventFacade.ConcurrencyCheck(0, userId, communityId, startTime, endTime, locationDetail.ZoneIndex, locationDetail.ZoneInstanceId);

            if (ConcurrentCheck.Equals(2))
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                WebOperationContext.Current.OutgoingResponse.StatusDescription = "There can only be one event at a time in a particular zone";
                return;
            }

            if (ConcurrentCheck.Equals(0))
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                WebOperationContext.Current.OutgoingResponse.StatusDescription = "There can only be one event at a time in a particular zone.";
                return;
            }
            
            id = eventFacade.InsertEvent (communityId, userId, title, details, locationDetail.Name, locationDetail.ZoneIndex, locationDetail.ZoneInstanceId,
                startTime, endTime, typeId, 0, 0, 0, isAPEvent.Equals ("Y"), "");

            UriTemplateMatch match = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            UriTemplate template = new UriTemplate("/event/{id}");
            WebOperationContext.Current.OutgoingResponse.SetStatusAsCreated(template.BindByPosition(match.BaseUri, id.ToString()));
        }

        /// <summary>
        // AttendEvent
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Description("Attend an Event")]
        [WebGet(UriTemplate = "AttendEvent?eventId={eventId}&response={response}")]
        [OperationContract]
        public void AttendEvent(int eventId, string response)
        {
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);
 

            UriTemplateMatch match = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            UriTemplate template = new UriTemplate("/event/{id}");
            WebOperationContext.Current.OutgoingResponse.SetStatusAsCreated(template.BindByPosition(match.BaseUri, eventId.ToString()));
        }

        /// <summary>
        // SendInvite
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Description("Send Invite to an Event")]
        [WebGet(UriTemplate = "SendInvite?eventId={eventId}&friendId={friendId}")]
        [OperationContract]
        public void SendInvite(int eventId, int friendId)
        {
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);


            UriTemplateMatch match = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            UriTemplate template = new UriTemplate("/event/{id}");
            WebOperationContext.Current.OutgoingResponse.SetStatusAsCreated(template.BindByPosition(match.BaseUri, eventId.ToString()));
        }
      

        // Get Auth 
        private APIAuthentication GetAPIAuth()
        {
            string ConsumerSecret = "";
            string ConsumerKey = "";

            if (HttpContext.Current.Request.Params["oauth_signature"] != null && HttpContext.Current.Request.Params["oauth_consumer_key"] != null)
            {
                ConsumerSecret = HttpContext.Current.Request.Params["oauth_signature"].ToString();
                ConsumerKey = HttpContext.Current.Request.Params["oauth_consumer_key"].ToString();

                CommunityFacade gameFacade = new CommunityFacade();
                return gameFacade.GetAPIAuth(ConsumerKey, ConsumerSecret);
            }

            m_logger.Debug("GetAPIAuth unable tp look up auth");
            return new APIAuthentication();
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


    }

}
