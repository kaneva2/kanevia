///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿/// Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;

using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.ComponentModel;

using log4net;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;

// The following line sets the default namespace for DataContract serialized typed to be ""
[assembly: ContractNamespace("", ClrNamespace = "MediaREST")]

namespace MediaREST
{
    // TODO: Please set IncludeExceptionDetailInFaults to false in production environments
    [ServiceBehavior(IncludeExceptionDetailInFaults = true), AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed), ServiceContract]
    public partial class MediaService
    {

        /// <summary>
        /// Media
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Description("Gets a list of Media.")]
        [WebGet(UriTemplate = "Media?communityId={communityId}&assetTypeId={assetTypeId}&pageNumber={pageNumber}&pageSize={pageSize}")]
        //[WebCache(
        [OperationContract]
        public PagedList<Asset> Media(int communityId, int assetTypeId, int pageNumber, int pageSize)
        {
            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;

            return new MediaFacade().GetAccessibleAssetsInCommunity (communityId, 0, false, assetTypeId, 0, "", "", pageNumber, pageSize);
        }

        /// <summary>
        /// MediaDetails
        /// </summary>
        /// <param name="assetId"></param>
        [Description("Gets Details for a Media.")]
        [WebGet(UriTemplate = "MediaDetails?assetId={assetId}")]
        //[WebCache(
        [OperationContract]
        public Asset MediaDetails (int assetId)
        {
            return new MediaFacade().GetAsset (assetId);
        }

        /// <summary>
        /// Apps
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Description("Gets a list of Apps.")]
        [WebGet(UriTemplate = "Apps?pageNumber={pageNumber}&pageSize={pageSize}")]
        [OperationContract]
        public IList<Game> Apps(int pageNumber, int pageSize)
        {
            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;

            int currentUserId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);
            return new GameFacade().GetGamesByOwner(currentUserId, "", "");
        }

        /// <summary>
        /// AssetGroups
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Description("Gets a list of AssetGroups i.e. Playlists")]
        [WebGet(UriTemplate = "AssetGroups?pageNumber={pageNumber}&pageSize={pageSize}")]
        [OperationContract]
        public IList<AssetGroup> AssetGroups(int pageNumber, int pageSize)
        {
            APIAuthentication apiAuth = GetAPIAuth();
            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;

            return new MediaFacade().GetAssetGroups(apiAuth.CommunityId, "", "", pageNumber, pageSize);
        }

        /// <summary>
        /// InsertAssetGroup
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <returns>Level</returns>
        [Description("Adds a AssetGroup.")]
        [WebGet(UriTemplate = "InsertAssetGroup?name={name}&description={description}")]
        //[WebCache(
        [OperationContract]
        public int InsertAssetGroup(string name, string description)
        {
            APIAuthentication apiAuth = GetAPIAuth();

            if (name == null)
            {
                name = "";
            }

            if (description == null)
            {
                description = "";
            }

            MediaFacade mediaFacade = new MediaFacade();
            int result = mediaFacade.InsertAssetGroup (apiAuth.CommunityId, name, description, 0);

            UriTemplateMatch match = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            UriTemplate template = new UriTemplate("/AssetGroup/{id}");
            WebOperationContext.Current.OutgoingResponse.SetStatusAsCreated(template.BindByPosition(match.BaseUri, 0.ToString()));

            return result;
        }

        /// <summary>
        /// DeleteAssetGroup
        /// </summary>
        /// <param name="assetGroupId"></param>
        /// <returns>Level</returns>
        [Description("Deletes a AssetGroup.")]
        [WebGet(UriTemplate = "DeleteAssetGroup?assetGroupId={assetGroupId}")]
        [OperationContract]
        public int DeleteAssetGroup(int assetGroupId)
        {
            APIAuthentication apiAuth = GetAPIAuth();

            MediaFacade mediaFacade = new MediaFacade();
            int result = mediaFacade.DeleteAssetGroup (apiAuth.CommunityId, assetGroupId);

            UriTemplateMatch match = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            UriTemplate template = new UriTemplate("/AssetGroup/{id}");
            WebOperationContext.Current.OutgoingResponse.SetStatusAsCreated(template.BindByPosition(match.BaseUri, 0.ToString()));

            return result;
        }

        /// <summary>
        /// AssetGroupAssets
        /// </summary>
        /// <param name="assetGroupId"></param>
        /// <returns></returns>
        [Description("Gets a list of Media in an AssetGroups i.e. Playlists")]
        [WebGet(UriTemplate = "AssetGroupAssets?assetGroupId={assetGroupId}")]
        [OperationContract]
        public IList<AssetGroupAsset> AssetGroupAssets(int assetGroupId)
        {
            APIAuthentication apiAuth = GetAPIAuth();

            // SP does NOT support paging
            //pageNumber = (pageNumber > 0) ? pageNumber : 1;
            //pageSize = (pageSize > 0) ? pageSize : 10;
            //pageSize = (pageSize > 100) ? 100 : pageSize;

            return new MediaFacade().GetWokPlaylist(apiAuth.CommunityId, assetGroupId);
        }

        /// <summary>
        /// AddAssetToGroup
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="assetGroupId"></param>
        /// <returns></returns>
        [Description("Adds Media to an AssetGroups i.e. Playlists")]
        [WebGet(UriTemplate = "AddAssetToGroup?assetId={assetId}&assetGroupId={assetGroupId}")]
        [OperationContract]
        public int AddAssetToGroup (int assetId, int assetGroupId)
        {
            APIAuthentication apiAuth = GetAPIAuth();
            return new MediaFacade().InsertAssetInGroup (apiAuth.CommunityId, assetGroupId, assetId);
        }

        /// <summary>
        /// UpdateAssetGroupAssetSortOrder
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="assetGroupId"></param>
        /// <returns></returns>
        [Description("Updates sort order of an asset in a AssetGroups i.e. Playlists")]
        [WebGet(UriTemplate = "UpdateAssetGroupAssetSortOrder?assetGroupId={assetGroupId}&assetId={assetId}&sortOrder={sortOrder}")]
        [OperationContract]
        public int UpdateAssetGroupAssetSortOrder(int assetGroupId, int assetId, int sortOrder)
        {
            APIAuthentication apiAuth = GetAPIAuth();
            return new MediaFacade().UpdateAssetGroupAssetSortOrder(assetId, sortOrder, assetGroupId);
        }

        /// <summary>
        /// UpdateBulkAssetGroupAssetSortOrder
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="assetGroupId"></param>
        /// <returns></returns>
        [Description("Bulk updates sort order of an asset in a AssetGroups i.e. Playlists")]
        [WebGet(UriTemplate = "UpdateBulkAssetGroupAssetSortOrder?assetGroupId={assetGroupId}&assetIds={assetIds}&sortOrders={sortOrders}")]
        [OperationContract]
        public int UpdateBulkAssetGroupAssetSortOrder(int assetGroupId, string assetIds, string sortOrders)
        {
            APIAuthentication apiAuth = GetAPIAuth();

            char[] splitter = { ',' };
            string[] arAssetIds = null;
            string[] arSortIds = null;

            // Did they enter multiples?
            arAssetIds = assetIds.Split(splitter);
            arSortIds = sortOrders.Split(splitter);

            if (!arAssetIds.Length.Equals (arSortIds.Length))
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.UnsupportedMediaType;
                WebOperationContext.Current.OutgoingResponse.StatusDescription = "AsetIds must match number of sortOrders";
                return -1; 
            }

            MediaFacade mediaFacade = new MediaFacade();

            for (int j = 0; j < arAssetIds.Length; j++)
            {
                // Make sure they are numbers
                if (KanevaGlobals.IsNumeric (arAssetIds[j].ToString()) && KanevaGlobals.IsNumeric (arSortIds[j].ToString()))
                {
                    mediaFacade.UpdateAssetGroupAssetSortOrder(Convert.ToInt32(arAssetIds[j]), Convert.ToInt32(arSortIds[j]), assetGroupId);
                }
                else
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.UnsupportedMediaType;
                    WebOperationContext.Current.OutgoingResponse.StatusDescription = "AsetIds and sortOrders must be numbers";
                    return -1; 
                }
            }

            return 0;
        }


        /// <summary>
        /// DeleteAssetFromGroup
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        [Description("Removes Media from an AssetGroups i.e. Playlists")]
        [WebGet(UriTemplate = "DeleteAssetFromGroup?assetId={assetId}&assetGroupId={assetGroupId}")]
        [OperationContract]
        public int DeleteAssetFromGroup (int assetGroupId, int assetId)
        {
            APIAuthentication apiAuth = GetAPIAuth();
            return new MediaFacade().RemoveAssetFromGroup (apiAuth.CommunityId, assetGroupId, assetId);
        }

        /// <summary>
        /// UploadYoutube
        /// </summary>
        /// <param name="youTubeURL"></param>
        /// <param name="title"></param>
        /// <param name="category"></param>
        /// <param name="keywords"></param>
        /// <param name="description"></param>
        /// <returns>assetId</returns>
        [Description("Uploads a YouTube Video")]
        [WebGet(UriTemplate = "UploadYoutube?youTubeURL={youTubeURL}&title={title}&category={category}&keywords={keywords}&description={description}")]
        [OperationContract]
        public int UploadYoutube (string youTubeURL, string title, int category, string keywords, string description)
        {
            APIAuthentication apiAuth = GetAPIAuth();
            int assetId = 0;

            if (youTubeURL == null)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.UnsupportedMediaType;
                WebOperationContext.Current.OutgoingResponse.StatusDescription = "Invalid youTubeURL";
                return -1;
            }

            if (title == null)
            {
                title = "YouTube Video";
            }

            category = (category > 0) ? category : 1;

            if (keywords == null)
            {
                keywords = "";
            }

            if (description == null)
            {
                description = "YouTube Video";
            }

            //// Check to make sure user did not enter any "potty mouth" words
            //if (KanevaWebGlobals.isTextRestricted(tbYouTubeTitle.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
            //    KanevaWebGlobals.isTextRestricted(tbTagsYouTube.Value, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
            //    KanevaWebGlobals.isTextRestricted(tbBriefDescYouTube.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            //{
            //    AjaxCallHelper.WriteAlert(Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE);
            //    return;
            //}

            //get the youtube asset id
            //parse string to find youtube assetID

            try
            {
                //parse the embed code to return the movie URL - this function intentionally throws an exception
                //when there is an error - need the try catch
                string strippedOutURL = ParseYouTubeEmbedCode(youTubeURL);

                // Would be owner of 3dapp
                Community community = new CommunityFacade().GetCommunity(apiAuth.CommunityId);

                //process/save the embed code
                bool success = ProcessYouTubeEmbed(strippedOutURL, title, category, keywords, description, ref assetId, community.CreatorId, community.CommunityId);
                if (!success)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.UnsupportedMediaType;
                    WebOperationContext.Current.OutgoingResponse.StatusDescription = "Invalid youTubeURL";
                    return -2;
                }
            }
            catch (Exception)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.UnsupportedMediaType;
                WebOperationContext.Current.OutgoingResponse.StatusDescription = "Invalid youTubeURL";
                return -3;
            }

            return assetId;
        }

        /// <summary>
        /// ParseYouTubeEmbedCode - parses youtube embed code to return only the movie url
        /// throws an error for the calling function to handle for user message display
        /// </summary>
        /// <param name="flashWidgetEmbedCode"></param>
        /// <returns>string</returns>
        public static string ParseYouTubeEmbedCode(string youTubeEmbedCode)
        {
            string assetId = "";
            string errorMessage = null;

            if ((youTubeEmbedCode.Length > 0) && ((youTubeEmbedCode.ToUpper()).IndexOf("YOUTUBE") >= 0))
            {
                int endIndexOfassetId = -1;
                //check for you tube video marker
                int startIndexOfassetId = youTubeEmbedCode.IndexOf("v/");
                //if marker not found look for URL tag
                if (startIndexOfassetId < 0)
                {
                    startIndexOfassetId = youTubeEmbedCode.IndexOf("v=");
                    //if no URL tag found display error
                    if (startIndexOfassetId < 0)
                    {
                        errorMessage = "This is not a valid YouTube embed code.";
                    }
                    if (errorMessage == null)
                    {
                        //if URL tag found pull out the assetID
                        assetId = youTubeEmbedCode.Substring(startIndexOfassetId + 2);
                    }
                }
                //if embed tag found pull out assetID
                else
                {
                    //find end of asset id in embed string with end index
                    endIndexOfassetId = youTubeEmbedCode.IndexOf("\"", startIndexOfassetId);
                    //if end index not found display error
                    if (endIndexOfassetId < 0)
                    {
                        errorMessage = "This is not a valid YouTube embed code.";
                    }
                    if (errorMessage == null)
                    {
                        //pull out assetID from embed code
                        assetId = youTubeEmbedCode.Substring(startIndexOfassetId + 2, endIndexOfassetId - (startIndexOfassetId + 2));
                    }
                }

            }
            //no valid embed string provided display error
            else
            {
                errorMessage = "This is a not valid YouTube embed code.";
            }

            if (errorMessage == null)
            {
                //added logic to account for YouTube url string Changes
                //modify as necessary
                int amplocation = assetId.IndexOf("&");
                if (amplocation >= 0)
                {
                    assetId = assetId.Substring(0, amplocation);
                }
            }
            //notify calling function that something went awry
            else
            {
                throw new Exception(errorMessage);
            }

            return assetId;
        }


        /// <summary>
        /// ProcessYouTubeEmbed
        /// </summary>
        /// <param name="assetID"></param>
        /// <returns>bool</returns>
        private bool ProcessYouTubeEmbed(string ytAssetID, string title, int catagoryId, string tags, string desc, ref int assetId, int _userId, int _channelId)
        {
            //overall try catch to keep page processing going no matter the type of failure
            //error written to site log
            
            try
            {
                int permission = (int)Constants.eASSET_PERMISSION.PUBLIC;  //public or permission

                /* Code taken from assetEdit page  */
                int isMature = (int)Constants.eASSET_RATING.GENERAL;

                //assumption: all you tube materials are videos
                // Create a new asset record
                assetId = StoreUtility.InsertAsset((int)Constants.eASSET_TYPE.VIDEO, (int)Constants.eASSET_SUBTYPE.YOUTUBE, title,
                    _userId, UsersUtility.GetUserNameFromId(_userId),
                    (int)Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE, permission, isMature,
                    catagoryId, desc, "", "", ytAssetID);

                // Insert into user's personal channel
                int personalChannelId = new UserFacade().GetPersonalChannelId(_userId);
                StoreUtility.InsertAssetChannel(assetId, personalChannelId);

                if (_channelId != personalChannelId)
                {
                    //then insert into the broadcast channel
                    StoreUtility.InsertAssetChannel(assetId, _channelId);
                }

                // Update the tags
                if (tags.Length > 0)
                {
                    StoreUtility.UpdateAssetTags(assetId, tags);
                }

            }
            catch (Exception ex)
            {
                m_logger.Error("Error adding new offsite asset", ex);
                return false;
            }

            //scrape the thumbnails and other video data
            if (KlausEnt.KEP.Kaneva.framework.utils.ExternalURLHelper.GetYouTubeInfo(ytAssetID, _userId, assetId, m_logger) > 0)
            {
                //AjaxCallHelper.WriteAlert("An error occurred during video metadata grab. \n The link was saved to your media library.");
            }

            return true;
        }


        // Get Auth 
        private APIAuthentication GetAPIAuth()
        {
            string ConsumerSecret = "";
            string ConsumerKey = "";

            try
            {
                if (HttpContext.Current.Request.Params["oauth_signature"] != null && HttpContext.Current.Request.Params["oauth_consumer_key"] != null)
                {
                    ConsumerSecret = HttpContext.Current.Request.Params["oauth_signature"].ToString();
                    ConsumerKey = HttpContext.Current.Request.Params["oauth_consumer_key"].ToString();

                    CommunityFacade gameFacade = new CommunityFacade();
                    APIAuthentication apiAuth = gameFacade.GetAPIAuth(ConsumerKey, ConsumerSecret);

                    if (apiAuth == null)
                    {
                        m_logger.Debug("Unable to look up Auth");
                    }

                    return apiAuth;
                }
            }
            catch (Exception exc)
            {
                m_logger.Debug("Error looking up APIAuth", exc);
            }

            m_logger.Debug("GetAPIAuth unable tp look up auth");
            return new APIAuthentication();
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }

}
