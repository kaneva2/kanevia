///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;

namespace Kaneva.OAuthChannel.OAuth.Wcf.Repositories
{
  public class ContactsRepository
  {
    public List<Contact> GetContactsForUser(string userName)
    {
      switch (userName)
      {
        case "john":
          return new List<Contact>
            {
              new Contact {FullName = "Al Johnson", Email = "jake@test.com"},
              new Contact {FullName = "Mikey Miles", Email = "mmiles@test.com"}
            };
        case "jane":
          return new List<Contact>
            {
              new Contact {FullName = "Drake Diego", Email = "drake.diego@test.com"},
              new Contact {FullName = "Lake Winterse", Email = "wintersl@test.com"}
            };
        default:
          return new List<Contact>
            {
              new Contact {FullName = "Bart Simpson", Email = "drake.Bart@test.com"},
              new Contact {FullName = "Lindsay Lohen", Email = "Lindsay@test.com"}
            };
      }
    }
  }

  public class Contact
  {
    public string FullName { get; set; }
    public string Email { get; set; }
  }
}
