///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿/// Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;

using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.ComponentModel;

using log4net;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;
using KlausEnt.KEP.Kaneva;

// The following line sets the default namespace for DataContract serialized typed to be ""
[assembly: ContractNamespace("", ClrNamespace = "GameREST")]

namespace GameREST
{
     // TODO: Please set IncludeExceptionDetailInFaults to false in production environments
    [ServiceBehavior(IncludeExceptionDetailInFaults = true), AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed), ServiceContract]
    public partial class GameService
    {

        /// <summary>
        /// EarnAchievement
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>Achievement</returns>
        [Description("Earns an acheivement.")]
        [WebGet(UriTemplate = "EarnAchievement?achievementId={achievementId}&sendBlast={sendBlast}&linkText={linkText}&linkParams={linkParams}")]
        //[WebCache(
        [OperationContract]
        public Achievement EarnAchievement (uint achievementId, bool sendBlast, string linkText, string linkParams)
        {
            APIAuthentication apiAuth = GetAPIAuth();
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);
            
            if (!apiAuth.SendBlast)
            {
                sendBlast = false;
            }

            if (linkText == null)
            {
                linkText = "";
            }

            if (linkParams == null)
            {
                linkParams = "";
            }

            string appUrl = BuildAppLink(apiAuth.GameId, linkParams);

            GameFacade gameFacade = new GameFacade();
            gameFacade.EarnAchievement(apiAuth.CommunityId, userId, achievementId, sendBlast, linkText, appUrl);

            UriTemplateMatch match = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            UriTemplate template = new UriTemplate("/Achievement/{id}");
            WebOperationContext.Current.OutgoingResponse.SetStatusAsCreated(template.BindByPosition(match.BaseUri, achievementId.ToString()));

            return new Achievement(achievementId, apiAuth.CommunityId, "Test", "TEst", 50, "http://images.kaneva.com/default.jpg");
        }

        /// <summary>
        /// AchievementForUser - Get list of Achievements earned for a user along with his friends data
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Description("Gets a list of Achievements Earned for a user along with his friends data.")]
        [WebGet(UriTemplate = "AchievementForUser?sortBy={sortBy}&showAll={showAll}&onlyFriends={onlyFriends}&pageNumber={pageNumber}&pageSize={pageSize}")]
        //[WebCache(
        [OperationContract]
        public PagedList<AchievementEarned> AchievementForUser (string sortBy, bool showAll, bool onlyFriends, int pageNumber, int pageSize)
        {
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);
            APIAuthentication apiAuth = GetAPIAuth();

            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;


            GameFacade gameFacade = new GameFacade();
            return gameFacade.AchievementForUser(userId, apiAuth.CommunityId, sortBy, showAll, onlyFriends, pageNumber, pageSize);
        }

        

        /// <summary>
        /// AchievementEarnedForUser - Gets list of Achievements for one user for the app. Ordered by Earned first, then not earned.
        /// Not earned will have UserId = 0
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Description("Gets list of Achievements for one user for the app. Orders by Earned first, then not earned. Ordered by Earned first, then not earned. ")]
        [WebGet(UriTemplate = "AchievementEarnedForUser?pageNumber={pageNumber}&pageSize={pageSize}")]
        [OperationContract]
        public PagedList<UserAchievementData> AchievementEarnedForUser (int pageNumber, int pageSize)
        {
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);
            APIAuthentication apiAuth = GetAPIAuth();

            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;


            GameFacade gameFacade = new GameFacade();
            return gameFacade.GetUserAchievementsForApp (apiAuth.CommunityId, userId, "", pageNumber, pageSize);
        }

        /// <summary>
        /// AchievementForApp
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Description("Gets a list of Achievements For an App.")]
        [WebGet(UriTemplate = "AchievementForApp?sortBy={sortBy}&pageNumber={pageNumber}&pageSize={pageSize}")]
        //[WebCache(
        [OperationContract]
        public PagedList<Achievement> AchievementForApp (string sortBy, int pageNumber, int pageSize)
        {
            try
            {
                GameFacade gameFacade = new GameFacade();
                APIAuthentication apiAuth = GetAPIAuth();

                pageNumber = (pageNumber > 0) ? pageNumber : 1;
                pageSize = (pageSize > 0) ? pageSize : 10;
                pageSize = (pageSize > 100) ? 100 : pageSize;

                return gameFacade.AchievementForApp(apiAuth.CommunityId, sortBy, pageNumber, pageSize);
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error getting achievements", exc);
                return new PagedList<Achievement> ();
            }
        }

        /// <summary>
        /// EarnPlayerData
        /// </summary>
        [Description("Earns player data.")]
        [WebGet(UriTemplate = "EarnPlayerData?newLeaderboardValue={newLeaderboardValue}&levelNumber={levelNumber}&percentComplete={percentComplete}&title={title}&sendBlast={sendBlast}&linkText={linkText}&linkParams={linkParams}")]
        //[WebCache(
        [OperationContract]
        public void EarnPlayerData(int newLeaderboardValue, UInt32 levelNumber, UInt32 percentComplete, string title, bool sendBlast, string linkText, string linkParams)
        {
            APIAuthentication apiAuth = GetAPIAuth();
            newLeaderboardValue = (newLeaderboardValue > 0) ? newLeaderboardValue : 0;
            levelNumber = (levelNumber > 0) ? levelNumber : 0;
            percentComplete = (percentComplete > 0) ? percentComplete : 0;

            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            GameFacade gameFacade = new GameFacade();

            // Validate some data
            if (!apiAuth.SendBlast)
            {
                sendBlast = false;
            }
            else
            {
                if (sendBlast == null)
                {
                    sendBlast = false;
                }
            }

            if (linkText == null)
            {
                linkText = "";
            }

            if (linkParams == null)
            {
                linkParams = "";
            }

            string appUrl = BuildAppLink(apiAuth.GameId, linkParams);

            gameFacade.EarnLevel(apiAuth.CommunityId, userId, levelNumber, percentComplete, title, sendBlast, appUrl, linkText);
            gameFacade.EarnLeaderBoard(apiAuth.CommunityId, userId, newLeaderboardValue);



            //UriTemplateMatch match = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            //UriTemplate template = new UriTemplate("/LeaderboardValue/{id}");
            //WebOperationContext.Current.OutgoingResponse.SetStatusAsCreated(template.BindByPosition(match.BaseUri, 0.ToString()));

            return;
        }

        /// <summary>
        /// EarnLeaderBoard
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>LeaderboardValue</returns>
        [Description("Earns Leaderboard value.")]
        [WebGet(UriTemplate = "EarnLeaderBoard?newValue={newValue}")]
        //[WebCache(
        [OperationContract]
        public void EarnLeaderBoard(int newValue)
        {
            APIAuthentication apiAuth = GetAPIAuth();
            newValue = (newValue > 0) ? newValue : 0;

            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);
            
            GameFacade gameFacade = new GameFacade();
            gameFacade.EarnLeaderBoard(apiAuth.CommunityId, userId, newValue);

            //UriTemplateMatch match = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            //UriTemplate template = new UriTemplate("/LeaderboardValue/{id}");
            //WebOperationContext.Current.OutgoingResponse.SetStatusAsCreated(template.BindByPosition(match.BaseUri, 0.ToString()));

            return;
        }


        /// <summary>
        /// LeaderBoard
        /// </summary>
        [Description("Gets a list of LeaderBoard For an App.")]
        [WebGet(UriTemplate = "LeaderBoard?onlyFriends={onlyFriends}&pageNumber={pageNumber}&pageSize={pageSize}")]
        //[WebCache(
        [OperationContract]
        public PagedList<LeaderboardValue> LeaderBoard (bool onlyFriends, int pageNumber, int pageSize)
        {
            APIAuthentication apiAuth = GetAPIAuth();
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            pageNumber = (pageNumber > -1) ? pageNumber : 0;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;

            GameFacade gameFacade = new GameFacade();
            return gameFacade.GetLeaderBoardValues(apiAuth.CommunityId, userId, onlyFriends, pageNumber, pageSize);
        }

        /// <summary>
        /// LeaderBoard
        /// </summary>
        [Description("Gets a list of LeaderBoard For an App.")]
        [WebGet(UriTemplate = "LeaderBoardWithAllData?onlyFriends={onlyFriends}&pageNumber={pageNumber}&pageSize={pageSize}")]
        //[WebCache(
        [OperationContract]
        public PagedList<UserPlayerData> LeaderBoardWithAllData (bool onlyFriends, int pageNumber, int pageSize)
        {
            APIAuthentication apiAuth = GetAPIAuth();
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            pageNumber = (pageNumber > -1) ? pageNumber : 0;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;

            GameFacade gameFacade = new GameFacade();
            return gameFacade.GetLeaderBoardValuesWithAllData(apiAuth.CommunityId, userId, onlyFriends, pageNumber, pageSize);
        }


        /// <summary>
        /// EarnLevel
        /// </summary>
        /// <param name="levelNumber"></param>
        /// <returns>Level</returns>
        [Description("Earns a level.")]
        [WebGet(UriTemplate = "EarnLevel?levelNumber={levelNumber}&percentComplete={percentComplete}&title={title}&sendBlast={sendBlast}&linkText={linkText}&linkParams={linkParams}")]
        //[WebCache(
        [OperationContract]
        public Kaneva.BusinessLayer.BusinessObjects.API.LevelValue EarnLevel(uint levelNumber, uint percentComplete, string title, bool sendBlast, string linkText, string linkParams)
        {
            APIAuthentication apiAuth = GetAPIAuth();
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            // Validate some data
            if (!apiAuth.SendBlast)
            {
                sendBlast = false;
            }
            else
            {
                if (sendBlast == null)
                {
                    sendBlast = false;
                }
            }

            if (linkText == null)
            {
                linkText = "";
            }

            if (linkParams == null)
            {
                linkParams = "";
            }

            string appUrl = BuildAppLink(apiAuth.GameId, linkParams);

            GameFacade gameFacade = new GameFacade();
            gameFacade.EarnLevel(apiAuth.CommunityId, userId, levelNumber, percentComplete, title, sendBlast, appUrl, linkText);

            UriTemplateMatch match = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            UriTemplate template = new UriTemplate("/Level/{id}");
            WebOperationContext.Current.OutgoingResponse.SetStatusAsCreated(template.BindByPosition(match.BaseUri, 0.ToString()));

            return new Kaneva.BusinessLayer.BusinessObjects.API.LevelValue(apiAuth.CommunityId, userId, levelNumber, percentComplete, new DateTime(), title);
        }

        /// <summary>
        /// LevelForUser
        /// </summary>
        /// <returns></returns>
        [Description("Gets a list of Levels.")]
        [WebGet(UriTemplate = "LevelForUser?userId={userId}&sortBy={sortBy}&onlyFriends={onlyFriends}&pageNumber={pageNumber}&pageSize={pageSize}")]
        //[WebCache(
        [OperationContract]
        public PagedList<LevelValue> LevelForUser (int userId, string sortBy, bool onlyFriends, int pageNumber, int pageSize)
        {
            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;

            return null;
        }


        /// <summary>
        /// EarnTitle
        /// </summary>
        [Description("Gets a list of Levels.")]
        [WebGet(UriTemplate = "EarnTitle?title={title}")]
        //[WebCache(
        [OperationContract]
        public void EarnTitle(string title)
        {
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);
            GameFacade gameFacade = new GameFacade();
            APIAuthentication apiAuth = GetAPIAuth();

            int communityId = apiAuth.CommunityId;
            gameFacade.EarnTitle(communityId, userId, title);
        }

        /// <summary>
        /// GetTitle
        /// </summary>
        [Description("Gets a title")]
        [WebGet(UriTemplate = "Title")]
        //[WebCache(
        [OperationContract]
        public string GetTitle()
        {
            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            GameFacade gameFacade = new GameFacade();
            APIAuthentication apiAuth = GetAPIAuth();

            int communityId = apiAuth.CommunityId;
            return gameFacade.GetTitle(communityId, userId);
        }

        /// <summary>
        /// GiftItem
        /// </summary>
        [Description("")]
        [WebGet(UriTemplate = "GiftItem?globalId={globalId}")]
        [OperationContract]
        public void GiftItem(int globalId)
        {
            APIAuthentication apiAuth = GetAPIAuth();

            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            // A 3D App will be limited to 1 gift/reward to give per player
            GameFacade gameFacade = new GameFacade();
            int previousGiftedCount = gameFacade.GetGiftedItemsCount(apiAuth.GameId, userId);
            if (previousGiftedCount != 0)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                WebOperationContext.Current.OutgoingResponse.StatusDescription = "A gift has already been given to this user";
                return;
            }

            // The Gift/Reward Item must be in Shop
            ShoppingFacade shopFacade = new ShoppingFacade();
            WOKItem giftItem = shopFacade.GetItem(globalId);
            if (giftItem == null || giftItem.ItemActive.Equals((int)WOKItem.ItemActiveStates.Deleted))
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                WebOperationContext.Current.OutgoingResponse.StatusDescription = "globalId does not identify a valid item";
                return;
            }

            // The Item must have been created by the 3D App developer
            int ownerUserId = gameFacade.GetGameOwner(apiAuth.GameId);
            if (giftItem.ItemCreatorId != ownerUserId)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                WebOperationContext.Current.OutgoingResponse.StatusDescription = "The gifted item must be created by the app owner";
                return;
            }

            // An item marked AP can not be given to a user that does not have an Access Pass
            UserFacade userFacade = new UserFacade();
            if (giftItem.PassTypeId == 1)
            {
                User currentUser = userFacade.GetUser(userId);
                if (!currentUser.HasAccessPass)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                    WebOperationContext.Current.OutgoingResponse.StatusDescription = "This item requires the user to own an Access Pass";
                    return;
                }
            }

            userFacade.AddItemToPendingInventory(userId, "P", globalId, 1, 512);
            gameFacade.InsertGiftedItem(apiAuth.GameId, userId, globalId);
        }
        
        /// <summary>
        /// BuildAppLink
        /// </summary>
        private string BuildAppLink(int gameId, string strLinkParams)
        {
            string appUrl = "http://www.kaneva.com/kgp/playwok.aspx?goto=U" + gameId.ToString() + "&ILC=MM3D&link=private";

            // Add custom parameters
            if (!String.IsNullOrWhiteSpace(strLinkParams))
            {
                // Clean special params
                strLinkParams = strLinkParams.Replace("goto=", "Reserved1=");
                strLinkParams = strLinkParams.Replace("ILC=", "Reserved2=");
                strLinkParams = strLinkParams.Replace("link=", "Reserved3=");

                // Put in amps
                strLinkParams = strLinkParams.Replace("amp;", "&");

                if (!strLinkParams.StartsWith("&"))
                {
                    strLinkParams = "&" + strLinkParams;
                }

                appUrl += strLinkParams;
            }

            return appUrl;
        }




        // Get Auth 
        private APIAuthentication GetAPIAuth ()
        {
            string ConsumerSecret = "";
            string ConsumerKey = "";

            try
            {
                if (HttpContext.Current.Request.Params["oauth_signature"] != null && HttpContext.Current.Request.Params["oauth_consumer_key"] != null)
                {
                    ConsumerSecret = HttpContext.Current.Request.Params["oauth_signature"].ToString();
                    ConsumerKey = HttpContext.Current.Request.Params["oauth_consumer_key"].ToString();

                    CommunityFacade gameFacade = new CommunityFacade();
                    APIAuthentication apiAuth = gameFacade.GetAPIAuth(ConsumerKey, ConsumerSecret);

                    if (apiAuth == null)
                    {
                        m_logger.Debug("Unable to look up Auth");
                    }

                    return apiAuth;
                }
            }
            catch (Exception exc)
            {
                m_logger.Debug("Error looking up APIAuth", exc);
            }

            m_logger.Debug("GetAPIAuth unable tp look up auth");
            return new APIAuthentication();
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    }


}
