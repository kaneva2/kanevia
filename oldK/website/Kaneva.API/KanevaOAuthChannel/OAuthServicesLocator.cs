///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using Kaneva.OAuth.Provider;
using Kaneva.OAuth.Provider.Inspectors;
using Kaneva.OAuth.Storage.Basic;
using Kaneva.OAuth.Testing;
using Kaneva.OAuth.Storage;
using KlausEnt.KEP.Kaneva;

namespace Kaneva.OAuthChannel
{
  public class OAuthServicesLocator
  {
      static readonly ITokenRepository<Kaneva.OAuth.Storage.Basic.AccessToken> _accessTokenRepository;
    static readonly IOAuthProvider _provider;
    static readonly ITokenRepository<Kaneva.OAuth.Storage.Basic.RequestToken> _requestTokenRepository;

    static OAuthServicesLocator()
    {
      var consumerStore = new MySQLConsumerStore();
      var nonceStore = new TestNonceStore();

      if (KanevaGlobals.UseMemcachedTokenRepository)
      {
          _accessTokenRepository = new MemcachedTokenRepository<Kaneva.OAuth.Storage.Basic.AccessToken>();
          _requestTokenRepository = new MemcachedTokenRepository<Kaneva.OAuth.Storage.Basic.RequestToken>();
      }
      else
      {
          _accessTokenRepository = new InMemoryTokenRepository<Kaneva.OAuth.Storage.Basic.AccessToken>();
          _requestTokenRepository = new InMemoryTokenRepository<Kaneva.OAuth.Storage.Basic.RequestToken>();
      }

      var tokenStore = new SimpleTokenStore(_accessTokenRepository, _requestTokenRepository);

      _provider = new OAuthProvider(tokenStore,
                                    new SignatureValidationInspector(consumerStore),
                                    new NonceStoreInspector(nonceStore),
                                    new TimestampRangeInspector(new TimeSpan(1, 0, 0)),
                                    new ConsumerValidationInspector(consumerStore));
    }

    public static IOAuthProvider Provider
    {
      get { return _provider; }
    }

    public static ITokenRepository<Kaneva.OAuth.Storage.Basic.AccessToken> AccessTokenRepository
    {
      get { return _accessTokenRepository; }
    }

    public static ITokenRepository<Kaneva.OAuth.Storage.Basic.RequestToken> RequestTokenRepository
    {
      get { return _requestTokenRepository; }
    }
  }
}
