///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿/// Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Net;
using System.Threading;

using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.ComponentModel;

using log4net;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

// The following line sets the default namespace for DataContract serialized typed to be ""
[assembly: ContractNamespace("", ClrNamespace = "SocialREST")]

namespace SocialREST
{
     // TODO: Please set IncludeExceptionDetailInFaults to false in production environments
    [ServiceBehavior(IncludeExceptionDetailInFaults = true), AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed), ServiceContract]
    public partial class FriendService
    {

        /// <summary>
        /// Friend
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Friends?pageNumber={pageNumber}&pageSize={pageSize}")]
        [Description("Gets a list of Friends.")]
        //[WebCache(
        public PagedList<Friend> Friends(int pageNumber, int pageSize)
        {
            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;

            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);
            PagedList<Friend> unfilteredList = new UserFacade().GetFriends(userId, "", "u.username", pageNumber, pageSize);

            PagedList<Friend> returnList = new PagedList<Friend>();
            returnList.TotalCount = unfilteredList.TotalCount;

            foreach (Friend entry in unfilteredList)
            {
                returnList.Add(new Friend(entry.UserId, entry.Username, entry.DisplayName, 
                                          (entry.ShowGender ? entry.Gender : ""), 
                                          entry.GluedDate, entry.UState, 
                                          (entry.ShowLocation ? entry.Location : ""),
                                          entry.NameNoSpaces,
                                          entry.ThumbnailSmallPath, entry.ThumbnailMediumPath, entry.ThumbnailLargePath, entry.ThumbnailSquarePath,
                                          entry.NumberOfViews, entry.NumberOfDiggs, 
                                          entry.StatusId, 
                                          (entry.ShowAge ? entry.Age : -1), 
                                          entry.IsMatureProfile,
                                          entry.IsUnder18,
                                          entry.ShowAge, entry.ShowGender, entry.ShowLocation)
                             );
            }

            return returnList;
        }

        /// <summary>
        /// Friend
        /// </summary>
        /// <returns></returns>
        [Description("Gets a list of Online Friends.")]
        [WebGet(UriTemplate = "FriendsOnline?pageNumber={pageNumber}&pageSize={pageSize}")]
        //[WebCache(
        [OperationContract]
        public PagedList<Friend> FriendsOnline (int pageNumber, int pageSize)
        {
            pageNumber = (pageNumber > 0) ? pageNumber : 1;
            pageSize = (pageSize > 0) ? pageSize : 10;
            pageSize = (pageSize > 100) ? 100 : pageSize;

            int userId = Convert.ToInt32(Thread.CurrentPrincipal.Identity.Name);

            KlausEnt.KEP.Kaneva.PagedDataTable dt = KlausEnt.KEP.Kaneva.UsersUtility.GetFriends(userId, "", pageNumber, pageSize, null, true, false, false, false);

            PagedList<Friend> list = new PagedList<Friend>();
            list.TotalCount = (uint) dt.TotalCount;

            foreach (System.Data.DataRow row in dt.Rows)
            {
                int id = int.Parse(row["user_id"].ToString());

                list.Add(new Friend(id, row["username"].ToString(), row["display_name"].ToString(), row["gender"].ToString(), Convert.ToDateTime(row["glued_date"]),
                    row["ustate"].ToString(), row["location"].ToString(), row["name_no_spaces"].ToString(),
                    row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(),
                    int.Parse(row["number_of_views"].ToString()), int.Parse(row["number_of_diggs"].ToString()),
                    int.Parse(row["status_id"].ToString()), Convert.ToInt32(row["age"]), Convert.ToInt32(row["mature_profile"]).Equals(1)
                ));
            }
            return list;
        }

        /// <summary>
        /// Profile
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>User</returns>
        [Description("Gets a Profile.")]
        [WebGet(UriTemplate = "ReadProfile?userId={userId}")]
        //[WebCache(
        [OperationContract]
        public UserMinimum ReadProfile(int userId)
        {
            UserFacade userFacade = new UserFacade();
            User user = new UserFacade().GetUser(userId);

            return getUserMinimumData(user);
        }

        /// <summary>
        /// Profile
        /// </summary>
        /// <param name="userName"></param>
        /// <returns>User</returns>
        [Description("Gets a Profile.")]
        [WebGet(UriTemplate = "ReadProfileByUsername?username={username}")]
        //[WebCache(
        [OperationContract]
        public UserMinimum ReadProfileByUsername(string username)
        {
            UserFacade userFacade = new UserFacade();
            User user = new UserFacade().GetUser(username);

            return getUserMinimumData(user);
        }

        public UserMinimum getUserMinimumData(User current_user)
        {
            UserMinimum return_user = new UserMinimum();

            return_user.userId = current_user.UserId;
            return_user.UserName = current_user.Username;
            return_user.DisplayName = current_user.DisplayName;
            return_user.SignupDate = current_user.SignupDate;
            return_user.UState = current_user.Ustate;
            return_user.NameNoSpaces = current_user.NameNoSpaces;
            return_user.ThumbnailSmallPath = current_user.ThumbnailSmallPath;
            return_user.ThumbnailMediumPath = current_user.ThumbnailMediumPath;
            return_user.ThumbnailLargePath = current_user.ThumbnailLargePath;
            return_user.NumberOfDiggs = current_user.NumberOfDiggs;
            return_user.NumberOfViews = current_user.NumberOfViews;
            return_user.IsMatureProfile = current_user.MatureProfile;
            return_user.NumberOfFriends = current_user.Stats.NumberOfFriends;
            return_user.Under18 = (current_user.Age < 18);

            CommunityFacade communityFacade = new CommunityFacade();
            Community community = communityFacade.GetCommunity(current_user.CommunityId);

            return_user.Gender = (community.Preferences.ShowGender ? current_user.Gender : "");
            return_user.Location = (community.Preferences.ShowLocation ? current_user.Location : "");
            return_user.Age = (community.Preferences.ShowAge ? current_user.Age : -1);
            
            return return_user;
        }

        public FriendMinimum getFriendMinimumData(Friend current_friend)
        {
            FriendMinimum return_friend = new FriendMinimum();

            return_friend._userId = current_friend.UserId;
            return_friend._username = current_friend.Username;
            return_friend._displayName = current_friend.DisplayName;
            return_friend._UState = current_friend.UState;
            return_friend._NameNoSpaces = current_friend.NameNoSpaces;
            return_friend._ThumbnailSmallPath = current_friend.ThumbnailSmallPath;
            return_friend._ThumbnailMediumPath = current_friend.ThumbnailMediumPath;
            return_friend._ThumbnailLargePath = current_friend.ThumbnailLargePath;
            return_friend._NumberOfDiggs = current_friend.NumberOfDiggs;
            return_friend._NumberOfViews = current_friend.NumberOfViews;
            return_friend._IsMatureProfile = current_friend.IsMatureProfile;
            return_friend._GluedDate = current_friend.GluedDate;
            return_friend._StatusId = current_friend.StatusId;
            return_friend._Under18 = (current_friend.Age < 18);

            return_friend._Gender = (current_friend.ShowGender ? current_friend.Gender : "");
            return_friend._Location = (current_friend.ShowLocation ? current_friend.Location : "");
            return_friend._Age = (current_friend.ShowAge ? current_friend.Age : -1);

            return return_friend;
        }

        [DataContract]
        public class UserMinimum
        {
            [DataMember]
            public int userId { get; set; }
            [DataMember]
            public string UserName { get; set; }
            [DataMember]
            public string DisplayName{get; set;}
            [DataMember]
            public DateTime SignupDate { get; set; }
            [DataMember]
            public string UState { get; set; }
            [DataMember]
            public string Location { get; set; }
            [DataMember]
            public string Gender { get; set; }
            [DataMember]
            public string NameNoSpaces { get; set; }
            [DataMember]
            public string ThumbnailSmallPath { get; set; }
            [DataMember]
            public string ThumbnailMediumPath { get; set; }
            [DataMember]
            public string ThumbnailLargePath { get; set; }
            [DataMember]
            public int NumberOfDiggs { get; set; }
            [DataMember]
            public int NumberOfViews { get; set; }
            [DataMember]
            public bool IsMatureProfile { get; set; }
            [DataMember]
            public int NumberOfFriends { get; set; }
            [DataMember]
            public int Age { get; set; }
            [DataMember]
            public bool Under18 { get; set; }
        }

        [DataContract(Name = "Friend")]
        public class FriendMinimum
        {
            [DataMember]
            public int _userId { get; set; }
            [DataMember]
            public string _username { get; set; }
            [DataMember]
            public string _displayName { get; set; }
            [DataMember]
            public string _UState { get; set; }
            [DataMember]
            public string _Location { get; set; }
            [DataMember]
            public string _Gender { get; set; }
            [DataMember]
            public string _NameNoSpaces { get; set; }
            [DataMember]
            public string _ThumbnailSmallPath { get; set; }
            [DataMember]
            public string _ThumbnailMediumPath { get; set; }
            [DataMember]
            public string _ThumbnailLargePath { get; set; }
            [DataMember]
            public int _NumberOfDiggs { get; set; }
            [DataMember]
            public int _NumberOfViews { get; set; }
            [DataMember]
            public bool _IsMatureProfile { get; set; }
            [DataMember]
            public int _Age { get; set; }
            [DataMember]
            public bool _Under18 { get; set; }
            [DataMember]
            public DateTime _GluedDate { get; set; }
            [DataMember]
            public int _StatusId { get; set; }
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}
