///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Linq;
using System.Web.UI;
using System.Xml.Linq;
using Kaneva.OAuth.Framework;
using Kaneva.OAuth.Provider;

namespace Kaneva.OAuthChannel
{
    public partial class CheckToken : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                IOAuthContext context = new OAuthContextBuilder().FromHttpRequest(Request);
                IOAuthProvider provider = OAuthServicesLocator.Provider;

                var tokenRepository = OAuthServicesLocator.AccessTokenRepository;
                var accessToken = tokenRepository.GetToken(context.Token);

                string userName = accessToken.UserName;

                string strSecuretext = "<token>Valid</token>";
                Response.ContentType = "text/xml";
                Response.Write(strSecuretext);
            }
            catch (OAuthException authEx)
            {
                Response.StatusCode = 403;
                Response.Write(authEx.Report);
                Response.End();
            }
            catch (Exception)
            {
                //string strNotSecuretext = "<token>Invalid</token>";
                //Response.ContentType = "text/xml";
                //Response.Write(strNotSecuretext);
                //Response.End();
                Response.StatusCode = 403;
                Response.End();
            }

        }
    }
}
