///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;

namespace Kaneva.OAuthChannel
{
  public class Global : HttpApplication
  {
    protected void Application_Start(object sender, EventArgs e)
    {
    }
  }
}
