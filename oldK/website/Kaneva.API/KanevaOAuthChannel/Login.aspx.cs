///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.Security;
using System.Web.UI;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.OAuthChannel
{
  public partial class Login : Page
  {
    protected void LoginButton_Click(object sender, EventArgs e)
    {
        UserFacade userFacade = new UserFacade();
        int roleMembership = 0;

        if (userFacade.Authorize(UserName.Text, Password.Text, 0, ref roleMembership, Request.UserHostAddress).Equals((int)eLOGIN_RESULTS.SUCCESS))
        {
            User user = userFacade.GetUserByEmail(UserName.Text);
            FormsAuthentication.SetAuthCookie(user.UserId.ToString(), RememberMe.Checked);
            FormsAuthentication.RedirectFromLoginPage(user.UserId.ToString (), true);
        }
        else
        {
            FailureText.Text = "Invalid Login";
        }
    }
  }
}
