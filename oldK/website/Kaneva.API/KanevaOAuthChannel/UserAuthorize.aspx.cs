///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using System.Web.UI;
using Kaneva.OAuth.Framework;
using Kaneva.OAuth.Storage.Basic;

namespace Kaneva.OAuthChannel
{
  public partial class UserAuthorize : Page
  {
      ITokenRepository<Kaneva.OAuth.Storage.Basic.AccessToken> AccessTokenRepository
    {
      get { return OAuthServicesLocator.AccessTokenRepository; }
    }

      ITokenRepository<Kaneva.OAuth.Storage.Basic.RequestToken> RequestTokenRepository
    {
      get { return OAuthServicesLocator.RequestTokenRepository; }
    }

    public string ConsumerKey
    {
      get { return RequestTokenRepository.GetToken(GetTokenString()).ConsumerKey; }
    }

    string GetTokenString()
    {
      string token = Request[Parameters.OAuth_Token];

      if (string.IsNullOrEmpty(token)) throw new Exception("The token string in the parameters collection was invalid/missing");

      return token;
    }

    protected void Approve_Click(object sender, EventArgs e)
    {
      string token = GetTokenString();

      ApproveRequestForAccess(token);

      RedirectToClient(token, true);
    }

    protected void Deny_Click(object sender, EventArgs e)
    {
      string token = GetTokenString();

      DenyRequestForAccess(token);

      RedirectToClient(token, false);
    }

    void ApproveRequestForAccess(string tokenString)
    {
        Kaneva.OAuth.Storage.Basic.RequestToken requestToken = RequestTokenRepository.GetToken(tokenString);

        var accessToken = new Kaneva.OAuth.Storage.Basic.AccessToken
        {
          ConsumerKey = requestToken.ConsumerKey,
          Realm = requestToken.Realm,
          Token = Guid.NewGuid().ToString(),
          TokenSecret = Guid.NewGuid().ToString(),
          UserName = HttpContext.Current.User.Identity.Name,
          ExpireyDate = DateTime.Now.AddMonths(12),
          Roles = new string[] {}
        };

      AccessTokenRepository.SaveToken(accessToken);

      requestToken.AccessToken = accessToken;

      RequestTokenRepository.SaveToken(requestToken);
    }

    void DenyRequestForAccess(string tokenString)
    {
        Kaneva.OAuth.Storage.Basic.RequestToken requestToken = RequestTokenRepository.GetToken(tokenString);

      requestToken.AccessDenied = true;

      RequestTokenRepository.SaveToken(requestToken);
    }

    void RedirectToClient(string token, bool granted)
    {
       
        string callBackUrl = Request[Parameters.OAuth_Callback];
      //string verifier = requestToken.Verifier;

      if (!string.IsNullOrEmpty(callBackUrl))
      {
        if (!callBackUrl.Contains("?")) callBackUrl += "?";
        else callBackUrl += "&";

        callBackUrl += Parameters.OAuth_Token + "=" + UriUtility.UrlEncode(token);
        //callBackUrl += "&" + Parameters.OAuth_Verifier + "=" + UriUtility.UrlEncode(verifier);

        Response.Redirect(callBackUrl, true);
      }
      else
      {
        if (granted) Response.Write("Authorized");
        else Response.Write("Denied");

        Response.End();
      }
    }
  }
}
