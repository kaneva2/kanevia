﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AuthExample._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="height: 51px">
        <asp:Label ID="Label3" runat="server" Text="WOK Web Auth URL"></asp:Label>
        <asp:TextBox ID="txtAuthURL" runat="server" Width="554px"></asp:TextBox>
    </div>
    <div style="height: 136px; width: 1092px">
        <asp:TextBox ID="txtAuthStatus" runat="server" Height="19px" Width="1087px"></asp:TextBox>
        <br />
    </div>
    <div style="height: 303px">
        <asp:Label ID="Label4" runat="server" Text="Provider URL"></asp:Label>
        <asp:TextBox ID="txtProviderURL" runat="server" Width="619px"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
            style="height: 26px" Text="Get Request Token" />
        <asp:TextBox ID="TextBox6" runat="server" Height="57px" Width="612px"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="Button3" runat="server" Text="Get Access Token" Width="163px" />
        <asp:TextBox ID="TextBox7" runat="server" Height="53px" Width="618px"></asp:TextBox>
        <br />
        <br />
        <br />
        <asp:Button ID="Button4" runat="server" Text="Get Protected Data" 
            Width="162px" />
        <asp:TextBox ID="TextBox8" runat="server" Height="51px" Width="615px"></asp:TextBox>
    </div>
    </form>
</body>
</html>
