///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Net;
using System.IO;

namespace AuthExample
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtAuthURL.Text = System.Configuration.ConfigurationManager.AppSettings["WokWebURL"] + "?user=[playername]&pw=[password]";
                txtProviderURL.Text = System.Configuration.ConfigurationManager.AppSettings["ProviderURL"]; 
            }

            if (Request.IsAuthenticated)
            {
                txtAuthStatus.Text = "You are authenticated with wok web";
            }
            else
            {
                txtAuthStatus.Text = "You are NOT authenticated with wok web";
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

        }





    }
}
