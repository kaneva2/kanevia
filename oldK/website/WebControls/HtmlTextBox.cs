///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva.WebControls {

    using System;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.IO;
    using System.Collections.Specialized;
	using System.ComponentModel;


    //*********************************************************************
    //
    // HtmlTextBox Class
    //
    // This control displays an HTML editor for uplevel browsers (ie 5.5 and higher)
    // and a normal textarea for downlevel browsers.
    // 
    // Note: This control is based on the the MSDN Editor sample at:
    //
    //      http://msdn.microsoft.com/library/default.asp?url=/workshop/author/dhtml/reference/properties/canhavehtml.asp
    // 
    // Some of the client-side functionality of this control is based on
    // Nikhil Kothari and Vandana Datye's HtmlEditor control 
    // (read their book Developing Microsoft ASP.NET Server Controls and Components 
    //  -- it's the BEST book on control development)
    //
    // Notice that the control uses an hidden input field to preserve content
    // between postbacks. This is necessary since saving text in a TextArea tag
    // causes the content to be automatically HTMLEncoded on postback.
    //
    //*********************************************************************

    [ValidationPropertyAttribute("Text")]
	//[Designer(typeof(ASPNET.StarterKit.Communities.CommunityDesigner))]	
    public class HtmlTextBox : WebControl, INamingContainer, IPostBackDataHandler  {

    
        private AllowHtml _allowHtml = AllowHtml.None;
        private bool _isUplevel = false;
        private bool _renderUplevel = false;    
        private int _columns = 50;
        private int _rows = 13;
    

        public event EventHandler TextChanged;
            


        //*********************************************************************
        //
        // AllowHtml Property
        //
        // Determines whether a user can enter HTML content. Possible values are:
        //
        //  * None - No HTML
        //  * Limited - HTML can only be added through toolbar
        //  * Full - User can edit HTML source    
        //
        //*********************************************************************

        public AllowHtml AllowHtml {
            get { return _allowHtml; }
            set { _allowHtml = value; }
        
        }


        //*********************************************************************
        //
        // IsUplevel Property
        //
        // Enables user to force HtmlTextEditor into downlevel mode    
        //
        //*********************************************************************
        public bool IsUplevel {
            get { return _isUplevel; }
            set { _isUplevel = value; }
        }





        //*********************************************************************
        //
        // Text Property
        //
        // Represents the contents of the editor.
        // Notice that we HTML encode in the special case of a downlevel
        // browser with limited HTML enabled.
        //
        //*********************************************************************
        public string Text {
            get {
                if (ViewState["Text"] == null)
                    return String.Empty;
               
                if (_allowHtml == AllowHtml.Limited && _isUplevel == false) 
                    return SimpleHtmlEncode( (string)ViewState["Text"] );
                
                return (string)ViewState["Text"];
            }
            set {
                ViewState["Text"] = value; 
            }
        }



        //*********************************************************************
        //
        // SimpleHtmlEncode Method
        //
        // Replaces < with &lt; and > with &gt;    
        //
        //*********************************************************************

        private string SimpleHtmlEncode(string text) {
            text = text.Replace("<", "&lt;");
            text = text.Replace(">", "&gt;");
            return text;
        }


        //*********************************************************************
        //
        // Columns Property
        //
        // Only used for downlevel browsers    
        //
        //*********************************************************************
        public int Columns {
            get { return _columns; }
            set { _columns = value; }
        }


        //*********************************************************************
        //
        // Rows Property
        //
        // Only used for downlevel browsers    
        //
        //*********************************************************************
        public int Rows {
            get { return _rows; }
            set { _rows = value; }
        }




        //*********************************************************************
        //
        // DetermineUplevel Method
        //
        // Only render client script for IE 5.5 or higher    
        //
        //*********************************************************************

        protected void DetermineUplevel() {
            if (Context != null) {
                HttpBrowserCapabilities _browser = Context.Request.Browser;
                
                bool _hasEcmaScript = (_browser.EcmaScriptVersion.CompareTo(new Version(1,2)) >= 0);
                bool _hasDom = (_browser.MSDomVersion.Major >=4);
                bool _hasBehaviors = (_browser.MajorVersion > 5) || ((_browser.MajorVersion == 5) && (_browser.MinorVersion >= .5));
                _isUplevel = _hasEcmaScript && _hasDom && _hasBehaviors;
            }
        }    




        //*********************************************************************
        //
        // LoadPostData Property
        //
        // Updates control properties when page is posted
        //
        //*********************************************************************

        public bool LoadPostData(string postDataKey, NameValueCollection values) {
            if (!Text.Equals(values[UniqueID])){
                Text = values[UniqueID];
                return true;
            }
            return false;
        }
            

        //*********************************************************************
        //
        // RaisPostDataChangedEvent Method
        //
        // Raises the TextChanged event
        //
        //*********************************************************************            

        public virtual void RaisePostDataChangedEvent() {
            OnTextChanged(EventArgs.Empty);     
        }
            
        protected virtual void OnTextChanged(EventArgs e){
            if (TextChanged != null)
              TextChanged(this,e);
        }




        //*********************************************************************
        //
        // GetClientIncludes Method
        //
        // Returns the client scripts    
        //
        //*********************************************************************

        private string GetClientIncludes() {
            return String.Format
            (
                "<script language=\"JavaScript\" src=\"{0}\"></script>\r\n"
                + "<?xml:namespace prefix=\"community\"/>\r\n"
                + "<?import namespace=\"community\" implementation=\"{1}\"/>",
                Page.ResolveUrl("~/jscript/HtmlTextBox.js"),
                Page.ResolveUrl("~/jscript/HtmlTextBox.htc")
            );
        }    
    


        //*********************************************************************
        //
        // EmoticonPath Property
        //
        // Helper property for retrieving path of folder with emoticon icons    
        //
        //*********************************************************************

        string EmoticonPath {
            get { return ResolveUrl("~/images/Emoticons/"); }
        }


        //*********************************************************************
        //
        // EmoticonList Property
        //
        // Returns comma delimited list of emoticon images    
        //
        //*********************************************************************

        string EmoticonList {
            get {
                string strList = String.Empty;
                string[] arrFiles = Directory.GetFiles(Page.MapPath(EmoticonPath), "*.gif");
                for (int i=0;i<arrFiles.Length;i++)
                    if (i==0)
                       strList = "'" + Path.GetFileName(arrFiles[i]) + "'";
                    else
                        strList += ",'" + Path.GetFileName(arrFiles[i]) + "'";
                
                return strList;  
            }
         }




        //*********************************************************************
        //
        // OnPreRender Method
        //
        // If browser is uplevel, add the client scripts to the page    
        //
        //*********************************************************************

        protected override void OnPreRender(EventArgs e) {
            base.OnPreRender(e);
            
            // Determine render uplevel
            _renderUplevel = _isUplevel && (_allowHtml != AllowHtml.None);
        
            if (_renderUplevel) {
                string scriptKey = typeof(HtmlTextBox).FullName;

                // Register the editor in list of editors
                Page.ClientScript.RegisterArrayDeclaration("htmlDesignerList", "'" + ClientID + "'");


                // add behavior namespace and emoticonlist
                if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType(), scriptKey))
                {
                    Page.ClientScript.RegisterClientScriptBlock(GetType(), scriptKey, GetClientIncludes());
                    Page.ClientScript.RegisterArrayDeclaration("emoticonList", EmoticonList);
    		    }

                // Register startup script (only once for all instances)
                if (!Page.ClientScript.IsStartupScriptRegistered(GetType(), scriptKey))
                    Page.ClientScript.RegisterStartupScript(GetType(), scriptKey, String.Format("<script language=\"JavaScript\">var appBasePath='{0}'; htb_InitializeElements()</script>", AppPath));                
            }
        }

		public static string AppPath 
		{
			get 
			{
				if (HttpContext.Current.Request.ApplicationPath == "/")
					return String.Empty;
				return HttpContext.Current.Request.ApplicationPath;
			}
		}


        //*********************************************************************
        //
        // Render Method
        //
        // If downlevel (or allowHtml is none) render a textarea, otherwise
        // render an HTML editor    
        //
        //*********************************************************************

        protected override void Render(HtmlTextWriter writer) {
            if (_renderUplevel) 
                RenderUplevel(writer);
            else
                RenderDownlevel(writer);
        }
        


        //*********************************************************************
        //
        // RenderDownlevel Method
        //
        // For downlevel browsers, we just render a textarea.
        //
        //*********************************************************************
        
       private void RenderDownlevel(HtmlTextWriter writer) {        
            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, ClientID);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, UniqueID);
            writer.AddAttribute(HtmlTextWriterAttribute.Cols, _columns.ToString());
            writer.AddAttribute(HtmlTextWriterAttribute.Rows, _rows.ToString());
            writer.RenderBeginTag(HtmlTextWriterTag.Textarea);
            writer.Write(Text);
            writer.RenderEndTag();    
        }            



        //*********************************************************************
        //
        // RenderDownlevel Method
        //
        // For downlevel browsers, we just render a textarea.
        //
        //*********************************************************************
       
        private void RenderUplevel(HtmlTextWriter writer) {        
            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, ClientID);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, UniqueID);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
            writer.AddAttribute(HtmlTextWriterAttribute.Value, Text);
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();    
            
            writer.WriteLine();
            
            // Next, do the HTML Designer
            if (ControlStyleCreated)    
                ControlStyle.AddAttributesToRender(writer, this);


            writer.AddAttribute(HtmlTextWriterAttribute.Id, ClientID + "HtmlDesigner");
            writer.AddAttribute("allowhtml", _allowHtml.ToString());
            writer.AddAttribute("onHtmlChanged", "htb_OnHtmlChanged(this, document.all['" + ClientID + "'])", false);
            writer.RenderBeginTag("community:HtmlDesigner");
            writer.RenderEndTag();
            
            writer.RenderEndTag(); //span        
        }




        //*********************************************************************
        //
        // HtmlTextBox Constructor
        //
        // Set the base TextBox control to MultiLine mode
        // and determine AllowHTML from context    
        //
        //*********************************************************************

        public HtmlTextBox() {
            Width = new Unit("500px");
            Height = new Unit( "200px");
            DetermineUplevel();
//		    if (Context != null && Context.Items["SectionInfo"] != null)
//				_allowHtml = ((SectionInfo)Context.Items["SectionInfo"]).AllowHtmlInput;
        }


    }
    


    //*********************************************************************
    //
    // AllowHtml Enumeration
    //
    // Possible values for AllowHtml    
    //
    //*********************************************************************
    
    public enum AllowHtml {
        None = 0,
        Limited = 1,
        Full = 2
    } 
    
    
    
}
