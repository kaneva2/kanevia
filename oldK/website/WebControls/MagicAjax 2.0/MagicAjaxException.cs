using System;

namespace MagicAjax
{
	/// <summary>
	/// Summary description for MagicAjaxException.
	/// </summary>
	public class MagicAjaxException : Exception
	{
		public MagicAjaxException(string message) : base(message)
		{
		}
	}
}
