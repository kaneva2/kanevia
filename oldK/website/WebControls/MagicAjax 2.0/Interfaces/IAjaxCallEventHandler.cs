#region LGPL License
/*
MagicAjax.NET Framework
Copyright (C) 2005  MagicAjax Project Team

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
#endregion

using System;

namespace MagicAjax.UI
{
	/// <summary>
	/// Interface for controls that do no inherit from AjaxControl and want to receive
	/// AjaxCall events. ('Session/Cache' page storing modes)
	/// </summary>
	/// <remarks>
	/// The Load event of the page and its child controls is not raised during an AjaxCall
	/// for 'Session/Cache' page storing modes. If you want to handle the AjaxCall event
	/// instead, implement the IAjaxCallEventHandler on the page or on a custom control.
	/// </remarks>
	/// <example>
	///	public class CustomTextBox : System.Web.UI.WebControls.TextBox, IAjaxCallEventHandler
	///	{
	///		public void RaiseAjaxCallStartEvent()
	///		{
	///			OnAjaxCallStart(EventArgs.Empty);
	///		}
	///		public void RaiseAjaxCallEndEvent()
	///		{
	///			OnAjaxCallEnd(EventArgs.Empty);
	///		}
	///	}
	/// </example>
	public interface IAjaxCallEventHandler
	{
		void RaiseAjaxCallStartEvent();
		void RaiseAjaxCallEndEvent();
	}
}
