using System;

namespace MagicAjax.UI
{
	/// <summary>
	/// Identifies a control that will not be regarded by an AjaxPanel as a separate
	/// html holder (a control to check for changes between AjaxCalls)
	/// </summary>
	public interface INonHtmlHolder
	{
	}
}
