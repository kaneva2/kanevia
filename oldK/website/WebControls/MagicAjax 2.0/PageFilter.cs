#region LGPL License
/*
MagicAjax.NET Framework
Copyright (C) 2005  MagicAjax Project Team

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
#endregion

using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace MagicAjax
{
	/// <summary>
	/// Used to store and retrieve the html rendering of the page during an AjaxCall
	/// </summary>
	internal class PageFilter : Stream
	{
		private Stream _responseStream;
		private Stream _memStream;
		private MagicAjaxContext _magicAjaxContext;

		public PageFilter(Stream _responseStream)
		{
			this._responseStream = _responseStream;
			this._memStream = new MemoryStream();
			this._magicAjaxContext = MagicAjaxContext.Current;
		}

		public string GetViewStateFieldValue()
		{
			//TODO: use regular expression (much faster)
#if NET_2_0
			string search = "<input type=\"hidden\" name=\"__VIEWSTATE\" id=\"__VIEWSTATE\" value=\"";
#else
			string search = "<input type=\"hidden\" name=\"__VIEWSTATE\" value=\"";
#endif
			string html = GetHtmlPage();
			int si = html.IndexOf(search);
			if (si == -1)
				return null;

			si += search.Length;
			int ei = html.IndexOf('\"', si);
			if (ei == -1)
				return null;

			return html.Substring(si, ei - si);
		}

#if NET_2_0
		/// <summary>
		/// Look for javascript generated for draggable webparts (IE only), and returns these javascripts.
		/// TODO: make used regular expressions global
		/// </summary>
		public string GetWebPartManagerScriptValue(string formID)
		{
			string html = GetHtmlPage();

			// Look for webpartmanager object creation script
			string searchWPManager = "<script type=\"text\\/javascript\">\\r\\n\\r\\n__wpm = new WebPartManager\\(\\);\\r\\n(?<WPManagerScript>.*?)<\\/script>";
			Regex regExWPManager = new Regex(searchWPManager, RegexOptions.Singleline | RegexOptions.Compiled);
			Match match = regExWPManager.Match(html);

			// If no webpartmanager script exists in html -> exit
			if (!match.Success)
				return null;

			// Stringbuilder to hold the output script
			StringBuilder wpmScript = new StringBuilder();

			// First look for hidden drag element (if not exists : add this to page)
			string searchDragElm = "<div id=\"(?<DragElmId>.+?___Drag)\" style=\"display:none.+?><\\/div>";
			Regex regExDragElm = new Regex(searchDragElm, RegexOptions.Multiline | RegexOptions.Compiled);
			Match matchDragElm = regExDragElm.Match(html);
			if (matchDragElm.Success)
			{
				//add this element to the html page, if it didn't exist
				string elmID = matchDragElm.Groups["DragElmId"].Value;
				wpmScript.AppendLine(string.Format("if (document.getElementById('{0}') == null)", elmID));
				wpmScript.AppendLine(string.Format("  AJAXCbo.AddElement('{0}','span','__DragHolder',{1},'null');", formID, AjaxCallHelper.EncodeString(matchDragElm.Value)));
			}

			// Now append the WebpartManager script
			wpmScript.AppendLine(match.Groups["WPManagerScript"].Value);

			// Now append the webpart menu scripts
			string searchWPMenus = "<script type=\"text\\/javascript\">\\r\\n(?<MenuScript>var menuWebPart_.*?)<\\/script>";
			Regex regExMenuScripts = new Regex(searchWPMenus, RegexOptions.Singleline | RegexOptions.Compiled);
			MatchCollection matches = regExMenuScripts.Matches(html); //ei
			for (int i = 0; i < matches.Count; i++)
			{
				wpmScript.AppendLine(matches[i].Groups["MenuScript"].Value);
			}

			return wpmScript.ToString();
		}
#endif

		private string _htmlPage;
		public string GetHtmlPage()
		{
			if (_htmlPage == null)
			{
				byte[] buffer = new byte[_memStream.Length];

				_memStream.Position = 0;
				_memStream.Read(buffer, 0, (int)_memStream.Length);

				_htmlPage = System.Text.Encoding.GetEncoding(HttpContext.Current.Response.ContentEncoding.CodePage).GetString(buffer);
			}
			return _htmlPage;
		}

		#region Stream overrides
		public override bool CanRead
		{
			get { return true; }
		}

		public override bool CanSeek
		{
			get { return true; }
		}

		public override bool CanWrite
		{
			get { return true; }
		}

		public override void Close()
		{
			_responseStream.Close();
		}

		public override void Flush()
		{
			_responseStream.Flush();
		}

		public override long Length
		{
			get { return _responseStream.Length; }
		}

		public override long Position
		{
			get { return _responseStream.Position; }
			set { _responseStream.Position = value; }
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			return _responseStream.Seek(offset, origin);
		}

		public override void SetLength(long length)
		{
			_responseStream.SetLength(length);
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			return _responseStream.Read(buffer, offset, count);
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			if (_magicAjaxContext.IsAjaxCall)
			{
				if (_magicAjaxContext.CompletedAjaxCall)
					_responseStream.Write(buffer, offset, count);
				else
					_memStream.Write(buffer, offset, count);
			}
			else
			{
				_responseStream.Write(buffer, offset, count);
			}
		}
		#endregion
	}
}
