#region LGPL License
/*
MagicAjax.NET Framework
Copyright (C) 2005  MagicAjax Project Team

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
#endregion

using System;
using System.Diagnostics;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Reflection;
using System.Configuration;
using MagicAjax.Configuration;

namespace MagicAjax
{
	/// <summary>
	/// Defines the various AjaxCall types
	/// </summary>
	public enum AjaxCallType
	{
		/// <summary>
		/// Client didn't invoke an AjaxCall
		/// </summary>
		None,
		/// <summary>
		/// Standard AjaxCall invoked by a control on the page
		/// </summary>
		Control,
		/// <summary>
		/// AjaxCall invoked by the AjaxCallTimer
		/// </summary>
		Timer
	}

	/// <summary>
	/// MagicAjaxContext contains all information about the MagicAjax configuration and current request
	/// </summary>
	public class MagicAjaxContext
	{
		internal static readonly string ContextKey = "__MAGICAJAXCONTEXT";

		#region Private properties
		private bool _isAjaxCall = false;
		private bool _completedAjaxCall = false;
		private bool _isBrowserSupported = false;
		private StoredPageInfo _storedPageInfo = null;
		private MagicAjaxConfiguration _configuration = null;
		private string _magicAjaxVersion = null;
		#endregion

		#region Public properties

		/// <summary>
		/// Configuration settings for MagicAjax.
		/// </summary>
		/// <remarks>
		/// During a page request/postback, configuration settings are getting initialized
		/// by the settings of web.config but can change for a particular page request/postback
		/// at the Load event.
		/// During an AjaxCall, the configuration settings will be the same as the page request
		/// that the AjaxCall belongs to. Changes during an AjaxCall are not allowed.
		/// </remarks>
		public MagicAjaxConfiguration Configuration
		{
			get	{ return _configuration; }
		}

		/// <summary>
		/// Returns true if request was made from the browser's XmlHttpRequest object
		/// </summary>
		public bool IsAjaxCall
		{
			get { return _isAjaxCall; }
			set { _isAjaxCall = value; }
		}

		/// <summary>
		/// True if ajax call was completed
		/// </summary>
		public bool CompletedAjaxCall
		{
			get { return _completedAjaxCall; }
			set { _completedAjaxCall = value; }
		}

		/// <summary>
		/// Returns if this browser is supported by MagicAjax.
		/// For now (version 0.2) only IE and FireFox are supported.
		/// </summary>
		public bool IsBrowserSupported
		{
			get { return _isBrowserSupported; }
			set { _isBrowserSupported = value; }
		}

		/// <summary>
		/// Information about current (stored) page. Only used for Session/Cache store mode.
		/// </summary>
		public StoredPageInfo StoredPageInfo
		{
			get { return _storedPageInfo; }
			set { _storedPageInfo = value; }
		}

		/// <summary>
		/// Returns the AjaxCall type, suitable for a switch block.
		/// </summary>
		public AjaxCallType AjaxCallType
		{
			get
			{
				if (IsAjaxCallTimer)
					return AjaxCallType.Timer;
				else if (IsAjaxCall)
					return AjaxCallType.Control;
				else
					return AjaxCallType.None;
			}
		}

		/// <summary>
		/// Determines whether the page is stored or recreated at each AjaxCall.
		/// </summary>
		public bool IsPageNoStoreMode
		{
			get { return _configuration.PageStore.Mode == MagicAjax.Configuration.PageStoreMode.NoStore; }
		}

		/// <summary>
		/// Gets the argument string the was sent by the client.
		/// </summary>
		public string AjaxCallArgument
		{
			get
			{
				if (IsAjaxCall)
					return HttpContext.Current.Request.Form["__EVENTARGUMENT"];
				else
					return null;
			}
		}

		/// <summary>
		/// Determines whether the AjaxCall occured by the AjaxCallTimer.
		/// </summary>
		/// <remarks>
		/// See the remarks of the SetAjaxCallTimerInterval method.
		/// </remarks>
		public bool IsAjaxCallTimer
		{
			get
			{
				return (HttpContext.Current.Request.Form["__EVENTTARGET"] == "__AJAX_AJAXCALLTIMER");
			}
		}

		/// <summary>
		/// Determines if the page is being processed for an AjaxCall. It's more accurate
		/// than IsAjaxCall because it returns false if the page is being processed
		/// for Server.Transfer.
		/// </summary>
		/// <remarks>
		/// When the page is being processed for Server.Transfer, the Page's
		/// IsPostBack property will be false. If this is the case IsAjaxCallForPage
		/// will return false.
		/// </remarks>
		public bool IsAjaxCallForPage(Page page)
		{
			return (IsAjaxCall &&
				((IsPageNoStoreMode && page.IsPostBack) || (!IsPageNoStoreMode && StoredPageInfo.Page == page)));
		}
		
		/// <summary>
		/// Returns the current version of the MagicAjax dll
		/// </summary>
		public string MagicAjaxVersion
		{
			get
			{
				if (_magicAjaxVersion == null && HttpContext.Current != null)
				{
					object cachedMagicAjaxVersion = HttpContext.Current.Cache["__MAGICAJAX_VERSION"];
					if (cachedMagicAjaxVersion == null)
					{
						string assemblyLocation = Assembly.GetExecutingAssembly().Location;
						cachedMagicAjaxVersion = FileVersionInfo.GetVersionInfo(assemblyLocation).FileVersion;
						HttpContext.Current.Cache.Insert("__MAGICAJAX_VERSION", cachedMagicAjaxVersion, new System.Web.Caching.CacheDependency(assemblyLocation));
					}
					_magicAjaxVersion = (string)cachedMagicAjaxVersion;
				}
				return _magicAjaxVersion;
			}
		}
		#endregion

		#region Public Ctor
		internal MagicAjaxContext()
		{
			// Load configuration from web.config
#if !NET_2_0 || !MEDIUM_TRUST
            _configuration = (MagicAjaxConfiguration)ConfigurationManager.GetSection("magicAjax");		
#endif
			if (_configuration == null)
			{
				_configuration = new MagicAjaxConfiguration(null);
			}

			//store initial settings on first call (per request)
			if (HttpContext.Current != null)
			{
				HttpBrowserCapabilities caps = HttpContext.Current.Request.Browser;

				if (caps.Browser != null && caps.EcmaScriptVersion.Major >= 1)
				{
					switch (caps.Browser.ToLower(System.Globalization.CultureInfo.InvariantCulture))
					{
						case "ie":
							_isBrowserSupported = caps.MajorVersion > 5 || (caps.MajorVersion == 5 && caps.MinorVersion >= 5);
							break;
						case "gecko":
							_isBrowserSupported = caps.Type.ToLower(System.Globalization.CultureInfo.InvariantCulture).StartsWith("firefox") && caps.MajorVersion >= 1;
							break;
						case "firefox":
							_isBrowserSupported = caps.MajorVersion >= 1;
							break;
						case "netscape":
							_isBrowserSupported = caps.MajorVersion >= 5;
							break;
						case "opera":
							_isBrowserSupported = caps.MajorVersion >= 8;
							break;
                        case "chrome":
                            _isBrowserSupported = caps.MajorVersion >= 1;
                            break; 
						//TODO: add support for Safari
					}
				}
			}
		}
		#endregion

		#region Public Static properties
		public static MagicAjaxContext Current
		{
			get
			{
				MagicAjaxContext magicAjaxContext = null;

				if (HttpContext.Current != null) 
				{
					// get the portal context for the current HTTP request
					magicAjaxContext = (MagicAjaxContext)HttpContext.Current.Items[ContextKey];
					if ( magicAjaxContext == null )
					{
						throw new MagicAjaxException("The MagicAjax HttpModule is not included in web.config. Add [<httpModules><add name=\"MagicAjax\" type=\"MagicAjax.MagicAjaxModule, MagicAjax\" /></httpModules>] inside the <system.web> section.");
					}
				}

				return magicAjaxContext;
			}
		}
		#endregion
	}
}
