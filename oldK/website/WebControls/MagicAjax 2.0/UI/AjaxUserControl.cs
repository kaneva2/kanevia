#region LGPL License
/*
MagicAjax.NET Framework
Copyright (C) 2005  MagicAjax Project Team

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
#endregion

using System;
using System.Web;
using System.ComponentModel;

namespace MagicAjax.UI
{
	/// <summary>
	/// Includes the AjaxCall events and some helper properties.
	/// </summary>
	/// <remarks>
	/// Inheriting from AjaxUserControls is not required, you can implement the
	/// IAjaxCallEventHandler on your usercontrol to handle the AjaxCall events.
	/// </remarks>
	public class AjaxUserControl : System.Web.UI.UserControl, IAjaxCallEventHandler, IPreWriteScriptEventHandler
	{
		private MagicAjaxContext _magicAjaxContext;
		private bool _isAjaxCall;
		private bool _isPageNoStoreMode;

		/// <summary>
		/// Raised by the MagicAjaxModule and at Load event during an AjaxCall.
		/// </summary>
		public event EventHandler AjaxCallStart;

		/// <summary>
		/// Raised by the MagicAjaxModule and at PreRender event during an AjaxCall.
		/// </summary>
		public event EventHandler PreWriteScript;

		/// <summary>
		/// Raised by the MagicAjaxModule and at Unload event during an AjaxCall.
		/// </summary>
		public event EventHandler AjaxCallEnd;

		/// <summary>
		/// Implements the IAjaxCallEventHandler interface. It is called by the MagicAjaxModule.
		/// </summary>
		public void RaiseAjaxCallStartEvent()
		{
			SetAjaxIntrinsics();
			OnAjaxCallStart(EventArgs.Empty);
		}

		/// <summary>
		/// Implements the IPreWriteScriptEventHandler interface. It is called by the MagicAjaxModule.
		/// </summary>
		public void RaisePreWriteScriptEvent()
		{
			OnPreWriteScript (EventArgs.Empty);
		}

		/// <summary>
		/// Implements the IAjaxCallEventHandler interface. It is called by the MagicAjaxModule.
		/// </summary>
		public void RaiseAjaxCallEndEvent()
		{
			OnAjaxCallEnd(EventArgs.Empty);
		}

		[Browsable(false),DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool IsPageNoStoreMode
		{
			get { return _isPageNoStoreMode; }
		}

		/// <summary>
		/// Determines if the control is being processed during an AjaxCall.
		/// </summary>
		[Browsable(false),DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool IsAjaxCall
		{
			get { return _isAjaxCall; } 
		}

		protected MagicAjaxContext MagicAjaxContext
		{
			get { return _magicAjaxContext; }
		}

		protected override void OnLoad(EventArgs e)
		{
			SetAjaxIntrinsics();

			base.OnLoad (e);
			if ( IsAjaxCall )
				OnAjaxCallStart (e);
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender (e);
			if ( IsAjaxCall )
				OnPreWriteScript (e);
		}

		protected override void OnUnload(EventArgs e)
		{
			base.OnUnload (e);
			if ( IsAjaxCall )
				OnAjaxCallEnd (e);
		}

		protected virtual void OnAjaxCallStart(EventArgs e)
		{
			if (AjaxCallStart != null)
				AjaxCallStart(this, e);
		}
		
		protected virtual void OnPreWriteScript(EventArgs e)
		{
			if (PreWriteScript != null)
				PreWriteScript(this, e);
		}

		protected virtual void OnAjaxCallEnd(EventArgs e)
		{
			_magicAjaxContext = null;

			if (AjaxCallEnd != null)
				AjaxCallEnd(this, e);
		}

		protected virtual void SetAjaxIntrinsics()
		{
			_magicAjaxContext = MagicAjaxContext.Current;
			_isAjaxCall = ( HttpContext.Current != null && this.Page != null && MagicAjaxContext.Current.IsAjaxCallForPage(this.Page) );
			_isPageNoStoreMode = ( HttpContext.Current != null && MagicAjaxContext.Current.IsPageNoStoreMode );
		}
	}
}