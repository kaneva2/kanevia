using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Design;

namespace MagicAjax.UI.Design
{
#if NET_2_0
	public class AjaxPanelDesigner : System.Web.UI.Design.ContainerControlDesigner
	{
	}
#else
	public class AjaxPanelDesigner : System.Web.UI.Design.ReadWriteControlDesigner 
	{
	}
#endif
}