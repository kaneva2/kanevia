using System;
using System.Web.UI.HtmlControls;

namespace MagicAjax.UI.Controls
{
	/// <summary>
	/// HtmlAnchor control intented to be used for 'Session'/'Cache' page storing modes
	/// instead of ASP.NET's HtmlAnchor.
	/// </summary>
	/// <remarks>
	/// The HtmlAnchor control of ASP.NET, looses its 'Href' property when an AjaxCall
	/// is invoked at 'Session'/'Cache' page storing modes. This control don't have the
	/// same problem and can be used to replace ASP.NET's HtmlAnchor.
	/// </remarks>
	public class AjaxHtmlAnchor : HtmlAnchor
	{
		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			string href = this.HRef;

			base.Render (writer);

			this.HRef = href;
		}
	}
}
