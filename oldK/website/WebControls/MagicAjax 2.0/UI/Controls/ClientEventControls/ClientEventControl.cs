using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace MagicAjax.UI.Controls
{
	/// <summary>
	/// Base class for controls that expose and handle client events.
	/// </summary>
	public abstract class ClientEventControl : WebControl, IPostBackEventHandler
	{
		protected abstract void HandleEvent (string eventName, string argument);

		#region IPostBackEventHandler Members

		public void RaisePostBackEvent(string eventArgument)
		{
			int index = eventArgument.IndexOf(';');
			if ( index == -1 )
				throw new MagicAjaxException(String.Format("Invalid eventArgument '{0}' for client event wrapper '{1}'.", eventArgument, ClientID));

			HandleEvent (eventArgument.Substring(0, index), eventArgument.Substring(index + 1));
		}

		#endregion

		public ClientEventControl() : base(HtmlTextWriterTag.Span)
		{
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender (e);
			if (this.Page != null && Enabled)
				Util.CallPrivateMethod(this.Page, typeof(Page), "RegisterPostBackScript");
		}
	}
}
