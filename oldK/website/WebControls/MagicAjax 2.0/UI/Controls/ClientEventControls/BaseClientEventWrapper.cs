using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.ComponentModel;

namespace MagicAjax.UI.Controls
{
	/// <summary>
	/// Base class for controls that capture the client events of their inner controls.
	/// </summary>
	[Designer("MagicAjax.UI.Design.BaseClientEventWrapperDesigner, MagicAjax"),
		ParseChildrenAttribute(false),
		PersistChildren(true)]
	public abstract class BaseClientEventWrapper : ClientEventControl
	{
		protected abstract void AttachClientEvents (HtmlTextWriter writer);

		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			base.AddAttributesToRender (writer);
			AttachClientEvents(writer);
		}

		protected virtual void AttachEvent (string eventName, string getArgumentScript, HtmlTextWriter writer)
		{
			if (getArgumentScript == null)
				writer.AddAttribute (eventName, String.Format("__doPostBack('{0}',event.type+';');", UniqueID));
			else
				writer.AddAttribute (eventName, String.Format("arg={0};__doPostBack('{1}',event.type+';'+arg);", getArgumentScript, UniqueID));
		}
		protected void AttachEvent (string eventName, HtmlTextWriter writer)
		{
			AttachEvent (eventName, null, writer);
		}
	}
}
