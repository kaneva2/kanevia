using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.ComponentModel;

namespace MagicAjax.UI.Controls
{
	/// <summary>
	/// Captures a client event of a control.
	/// </summary>
	/// <remarks>
	///	The EventName property must be set to the client event (i.e. "focus","change", etc.)
	///	and the ControlID property must be set to the ID of the control whose event we want to
	///	capture. The AjaxCall that will be invoked is dependent at the ClientEventTrigger's
	///	placement, not the placement of ControlID's control. For example, if the ClientEventTrigger
	///	is inside an AjaxPanel and AjaxCall will be invoked even if the ControlID's control is
	///	not inside an AjaxPanel.
	///	
	///	The ClientEventTrigger must be inside the same NamingContainer (i.e. UserControl)
	///	as the ControlID's control.
	/// </remarks>
	[Designer("MagicAjax.UI.Design.ClientEventTriggerDesigner, MagicAjax"),
		ParseChildrenAttribute(true),
		PersistChildren(false),
		ToolboxData("<{0}:ClientEventTrigger runat=server></{0}:ClientEventTrigger>")]
	public class ClientEventTrigger : ClientEventControl, INonHtmlHolder
	{
		private string _controlID = String.Empty;
		private string _eventName = String.Empty;

		public event EventHandler Invoke;

		[Bindable(false),
			Category("Behaviour"), 
			DefaultValue("")]
		public string ControlID
		{
			get { return _controlID; }
			set { _controlID = value; }
		}

		[Bindable(false),
			Category("Behaviour"), 
			DefaultValue("")]
		public string EventName
		{
			get { return _eventName; }
			set { _eventName = value; }
		}

		protected override void HandleEvent(string eventName, string argument)
		{
			OnInvoke (EventArgs.Empty);
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if ( Page != null )
			{
				string attachScriptFormat = @"<script type='text/javascript'>
				<!--
				elem=document.getElementById('{0}');
				fn=function(e) {{__doPostBack('{1}',e.type+';');}};
				if (elem.addEventListener)
					elem.addEventListener('{2}',fn,false);
				else
					elem.attachEvent('on{2}',fn);
				// -->
				</script>";

				if (ControlID == null || ControlID == String.Empty)
					throw new MagicAjaxException(String.Format("ControlID property for trigger {0} is not set.", ClientID));

				if (EventName == null || EventName == String.Empty)
					throw new MagicAjaxException(String.Format("EventName property for trigger {0} is not set.", ClientID));

				Control con = this.NamingContainer.FindControl(ControlID);
				if ( con == null )
					throw new MagicAjaxException(String.Format("The ControlID '{0}' does not exist or is not in the same NamingContainer as trigger {1}", ControlID, ClientID));

				if ( ! Page.ClientScript.IsStartupScriptRegistered(Page.GetType (), "__TRIGGER" + this.ClientID) )
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "__TRIGGER" + this.ClientID, String.Format(attachScriptFormat, con.ClientID, this.UniqueID, EventName));

				writer.Write("<span id=\"{0}\"></span>", ClientID);
			}
		}
		
		protected virtual void OnInvoke (EventArgs e)
		{
			if (Invoke != null)
				Invoke (this, e);
		}
	}
}
