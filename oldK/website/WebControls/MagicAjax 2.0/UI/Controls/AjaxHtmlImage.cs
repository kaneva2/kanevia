using System;
using System.Web.UI.HtmlControls;

namespace MagicAjax.UI.Controls
{
	/// <summary>
	/// HtmlImage control intented to be used for 'Session'/'Cache' page storing modes
	/// instead of ASP.NET's HtmlImage.
	/// </summary>
	/// <remarks>
	/// The HtmlImage control of ASP.NET, looses its 'Src' property when an AjaxCall
	/// is invoked at 'Session'/'Cache' page storing modes. This control don't have the
	/// same problem and can be used to replace ASP.NET's HtmlImage.
	/// </remarks>
	public class AjaxHtmlImage : HtmlImage
	{
		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			string src = this.Src;

			base.Render (writer);

			this.Src = src;
		}
	}
}
