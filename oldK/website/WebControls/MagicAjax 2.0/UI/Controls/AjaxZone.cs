using System;
using System.Web.UI;
using System.ComponentModel;

namespace MagicAjax.UI.Controls
{
	/// <summary>
	/// An AjaxPanel that is isolated from the rest AjaxPanels of the page.
	/// </summary>
	/// <remarks>
	/// It's an AjaxPanel that has the MagicAjax attribute "AjaxLocalScope" set to true.
	/// It is provided for convenience and readability.
    /// 
    /// When an AjaxCall is invoked from a control inside an AjaxZone, only the values of the
    /// form elements that are contained inside this AjaxZone will be sent to the server
    /// and the server will check for changes and "reflect" only the AjaxPanels that are
    /// inside the AjaxZone. This helps reduce the Ajax traffic and speed up a bit the
    /// server response. It's intented for isolated and independent portions of a page
    /// like UserControls.
    /// 
    /// An AjaxZone can contain other AjaxZones. A control belongs to the AjaxZone that is
    /// its immediate parent.
	/// </remarks>
	[Designer("MagicAjax.UI.Design.AjaxPanelDesigner, MagicAjax"),
		ParseChildrenAttribute(false),
		PersistChildren(true),
		ToolboxData("<{0}:AjaxZone runat=server>AjaxZone</{0}:AjaxZone>")]
	public class AjaxZone : AjaxPanel
	{
		protected override void AddAjaxAttributesToRender(HtmlTextWriter writer)
		{
			base.AddAjaxAttributesToRender (writer);
			writer.AddAttribute ("AjaxLocalScope", "true");
		}
	}
}
