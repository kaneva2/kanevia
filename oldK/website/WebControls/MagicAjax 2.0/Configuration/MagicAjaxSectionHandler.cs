using System;
using System.Xml;
using System.Configuration;

namespace MagicAjax.Configuration
{
	/// <summary>
	/// Summary description for MagicAjaxSectionHandler.
	/// </summary>
	public class MagicAjaxSectionHandler : IConfigurationSectionHandler
	{
		#region IConfigurationSectionHandler Members

		public object Create(object parent, object configContext, XmlNode section)
		{
			return new MagicAjaxConfiguration(section);
		}
		#endregion
	}
}
