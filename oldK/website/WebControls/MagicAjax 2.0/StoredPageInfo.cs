#region LGPL License
/*
MagicAjax.NET Framework
Copyright (C) 2005  MagicAjax Project Team

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
#endregion

using System;
using System.Web.UI;

namespace MagicAjax
{
	/// <summary>
	/// Summary description for CachedPageInfo.
	/// </summary>
	public sealed class StoredPageInfo
	{
		private Page _page;
		private int _ajaxCallsCount;
		private DateTime _lastAccess;

		public Page Page
		{
			get { return _page; }
		}

		public int AjaxCallsCount
		{
			get { return _ajaxCallsCount; }
			set { _ajaxCallsCount = value; }
		}

		public DateTime LastAccess
		{
			get { return _lastAccess; }
			set { _lastAccess = value; }
		}

		public StoredPageInfo(Page page)
		{
			_page = page;
			_ajaxCallsCount = 0;
			_lastAccess = DateTime.Now;
		}
	}
}
