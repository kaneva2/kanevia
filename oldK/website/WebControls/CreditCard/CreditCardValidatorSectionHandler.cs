///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Configuration;
using System.Xml;

namespace KlausEnt.KEP.Kaneva.WebControls
{
	/// <summary>
	/// Creates a configuration section for the ASP.NET web.config and machine.config files.
	/// Allows users to add new card types to the validation control. 
	/// </summary>
	public class CreditCardValidatorSectionHandler : IConfigurationSectionHandler
	{
		public CreditCardValidatorSectionHandler()
		{
		}

		public object Create(object parent, object configContext, System.Xml.XmlNode section)
		{
			XmlNodeList cardTypes;
			CardTypeCollection al = new CardTypeCollection();

			cardTypes = section.SelectNodes("cardTypes//cardType");

			foreach( XmlNode cardType in cardTypes )
			{
				String name = cardType.Attributes.GetNamedItem("name").Value;
				String regExp = cardType.Attributes.GetNamedItem("regExp").Value;

				//Add them to the card types collection
				CardType ct = new CardType(name,regExp);

				al.Add(ct);
			}

			return al;
		}
	}

	public class CardType
	{
		private String _name;
		private String _regExp;

		public CardType( String Name, String RegExp )
		{
			_name = Name;
			_regExp = RegExp;
		}

		public string Name
		{
			get {
				return _name;
			}
		}

		public string RegExp
		{
			get {
				return _regExp;
			}
		}
	}
}