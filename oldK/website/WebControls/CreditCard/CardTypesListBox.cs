///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.ComponentModel;

namespace KlausEnt.KEP.Kaneva.WebControls
{
	[
	Designer( typeof(CardTypesListBoxDesigner) )
	]
	public class CardTypesListBox : ListBox
	{
		private CardTypeCollection _cardTypes;
		private string _acceptedCardTypes;
        private string _selectItemByName = "";

		public CardTypesListBox()
		{
		}

		protected override void OnPreRender(System.EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				// Remove anything in there already before re-filling it.
				Items.Clear();

				// If the user has specified which card types to accept, limit the contents
				// of the drop-down to just those.
				if (_acceptedCardTypes != null)
					LimitAcceptedCardTypes ();

				foreach (CardType ct in _cardTypes)
				{
					ListItem li = new ListItem(ct.Name,ct.RegExp);
					Items.Add( li );
				}

                if (_selectItemByName != "")
                {
                    try
                    {
                        SelectedIndex = Items.IndexOf (Items.FindByText (_selectItemByName));
                    }
                    catch { }

                    _selectItemByName = "";
                }
			}

			base.OnPreRender(e);
		}

		public string AcceptedCardTypes
		{
			get
			{
				return _acceptedCardTypes;
			}
			set
			{
				_acceptedCardTypes = value;
			}
		}

        public string SelectItemByName
        {
            get
            {
                return _selectItemByName;
            }
            set
            {
                _selectItemByName = value;
            }
        }

		protected void LimitAcceptedCardTypes()
		{
			CardTypeCollection acceptedTypes = new CardTypeCollection();

			foreach(CardType ct in _cardTypes)
			{
				if ( Regex.IsMatch(_acceptedCardTypes, ".*" + ct.Name + ".*") )
					acceptedTypes.Add( ct );
			}

			_cardTypes = acceptedTypes;
		}

		protected override void OnInit(System.EventArgs e)
		{
			if (Site != null)
			{
				if (Site.DesignMode)
					// In design mode so just add a quick dummy item.
					Items.Add( new ListItem("CardTypes Here") );
			}
			else
			{
				CardTypeCollection ct = (CardTypeCollection)ConfigurationManager.GetSection( "KlausEnt.KEP.Kaneva.WebControls" );

				if (ct != null)
					_cardTypes = new CardTypeCollection(ct);
				else
					throw new NullReferenceException("CardTypeListBox requires card types in the web.config in the KlausEnt.KEP.Kaneva.WebControls section.");
			}

			base.OnInit(e);
		}
	}
}
