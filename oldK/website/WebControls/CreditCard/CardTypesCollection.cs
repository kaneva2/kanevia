///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;

namespace KlausEnt.KEP.Kaneva.WebControls
{
	/// <summary>
	/// A collection of elements of type CardType
	/// </summary>
	public class CardTypeCollection: System.Collections.CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the CardTypeCollection class.
		/// </summary>
		public CardTypeCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the CardTypeCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new CardTypeCollection.
		/// </param>
		public CardTypeCollection(CardType[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the CardTypeCollection class, containing elements
		/// copied from another instance of CardTypeCollection
		/// </summary>
		/// <param name="items">
		/// The CardTypeCollection whose elements are to be added to the new CardTypeCollection.
		/// </param>
		public CardTypeCollection(CardTypeCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this CardTypeCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this CardTypeCollection.
		/// </param>
		public virtual void AddRange(CardType[] items)
		{
			foreach (CardType item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another CardTypeCollection to the end of this CardTypeCollection.
		/// </summary>
		/// <param name="items">
		/// The CardTypeCollection whose elements are to be added to the end of this CardTypeCollection.
		/// </param>
		public virtual void AddRange(CardTypeCollection items)
		{
			foreach (CardType item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type CardType to the end of this CardTypeCollection.
		/// </summary>
		/// <param name="value">
		/// The CardType to be added to the end of this CardTypeCollection.
		/// </param>
		public virtual void Add(CardType value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic CardType value is in this CardTypeCollection.
		/// </summary>
		/// <param name="value">
		/// The CardType value to locate in this CardTypeCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this CardTypeCollection;
		/// false otherwise.
		/// </returns>
		public virtual bool Contains(CardType value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this CardTypeCollection
		/// </summary>
		/// <param name="value">
		/// The CardType value to locate in the CardTypeCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public virtual int IndexOf(CardType value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the CardTypeCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the CardType is to be inserted.
		/// </param>
		/// <param name="value">
		/// The CardType to insert.
		/// </param>
		public virtual void Insert(int index, CardType value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the CardType at the given index in this CardTypeCollection.
		/// </summary>
		public virtual CardType this[int index]
		{
			get
			{
				return (CardType) this.List[index];
			}
			set
			{
				this.List[index] = value;
			}
		}

		/// <summary>
		/// Removes the first occurrence of a specific CardType from this CardTypeCollection.
		/// </summary>
		/// <param name="value">
		/// The CardType value to remove from this CardTypeCollection.
		/// </param>
		public virtual void Remove(CardType value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by CardTypeCollection.GetEnumerator.
		/// </summary>
		public class Enumerator: System.Collections.IEnumerator
		{
			private System.Collections.IEnumerator wrapped;

			public Enumerator(CardTypeCollection collection)
			{
				this.wrapped = ((System.Collections.CollectionBase)collection).GetEnumerator();
			}

			public CardType Current
			{
				get
				{
					return (CardType) (this.wrapped.Current);
				}
			}

			object System.Collections.IEnumerator.Current
			{
				get
				{
					return (CardType) (this.wrapped.Current);
				}
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this CardTypeCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		public new virtual CardTypeCollection.Enumerator GetEnumerator()
		{
			return new CardTypeCollection.Enumerator(this);
		}
	}
}