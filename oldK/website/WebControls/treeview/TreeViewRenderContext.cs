///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////



namespace KlausEnt.KEP.Kaneva.WebControls {

    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;


    /// <summary>
    /// </summary>
    internal class TreeViewRenderContext {

        public HyperLink hyperLink;
        public Image image;
        public Label label;

        public string target;
        public string treeViewID;
        public string expandedStateField;

        public ImageUrlCollection imageUrls;

        public string expandedNodeImageUrl;
        public string collapsedNodeImageUrl;
        public string nodeImageUrl;

        public bool designMode;


        /// <summary>
        /// </summary>
        public TreeViewRenderContext(TreeView treeView, System.Web.UI.Page Page) {

            // Whether or not we are in design mode
            this.designMode = (treeView.Site != null && treeView.Site.DesignMode);

            // Create one of each of the controls
            hyperLink = new HyperLink();
            image = new Image();
            image.ImageAlign = ImageAlign.AbsMiddle;
            label = new Label();

            this.target = treeView.Target;
            this.treeViewID = treeView.ClientID;
            this.imageUrls = treeView.Images;

            // UNDONE: Waiting for RAID bug 42123 (beta 2) to efficiently get Form's ClientID
            Control form = null;
            for (int i = 0; i < treeView.Page.Controls.Count; i++) {
                if (treeView.Page.Controls[i] is HtmlForm) {
                    form = treeView.Page.Controls[i];
                    break;
                }
            }

            // REVIEW: Is this exception necessary? What kind of specific exception should be thrown?
            if (form != null) {
                this.expandedStateField = "document." + form.ClientID + "." + treeViewID + "_expandedstate";
            }
            else {
                this.expandedStateField = String.Empty;
            }

            string imageUrl;

            // TODO: Get images path from config.web
            imageUrl = treeView.ExpandedNodeImageUrl;
            if ((imageUrl == null) || (imageUrl.Length == 0)) {
                this.expandedNodeImageUrl = Page.ResolveUrl("~/images/minus.gif"); //"/aspnet_client/advancedwebcontrols/minus.gif";
            }
            else {
                this.expandedNodeImageUrl = imageUrl;
            }

            imageUrl = treeView.CollapsedNodeImageUrl;
            if ((imageUrl == null) || (imageUrl.Length == 0)) {
                this.collapsedNodeImageUrl  = Page.ResolveUrl("~/images/plus.gif"); //"/aspnet_client/advancedwebcontrols/plus.gif";
            }
            else {
                this.collapsedNodeImageUrl  = imageUrl;
            }

            imageUrl = treeView.NodeImageUrl;
            if ((imageUrl == null) || (imageUrl.Length == 0)) {
                this.nodeImageUrl  = Page.ResolveUrl("~/images/node.gif"); // "/aspnet_client/advancedwebcontrols/node.gif";
            }
            else {
                this.nodeImageUrl  = imageUrl;
            }
        }

    }
}

