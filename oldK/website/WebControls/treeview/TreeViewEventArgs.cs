///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////



namespace KlausEnt.KEP.Kaneva.WebControls {
    
    /// <summary>
    /// <para>Provides data for events of a <see cref='AdvancedWebControls.TreeView'/>.
    /// </para>
    /// </summary>
    public class TreeViewEventArgs {     
        private TreeNode node;


        public TreeViewEventArgs(TreeNode node) {
            this.node = node;
        }
        
        public TreeNode Node {
            get {
                return node;
            }
        } 
    }
}
