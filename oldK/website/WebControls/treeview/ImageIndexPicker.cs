///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////



namespace KlausEnt.KEP.Kaneva.WebControls {

    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.Drawing.Design;
    using System.Windows.Forms;
    using System.Windows.Forms.Design;


    /// <summary>
    /// </summary>
    [
    ToolboxItem(false)
    ]
    public class ImageIndexPicker : ListBox {

        private IWindowsFormsEditorService edSvc;

        private bool keyDown = false;
        private bool mouseClicked = false;


        public ImageIndexPicker() {
            this.BorderStyle = BorderStyle.None;
        }


        // Extra property that "converts" from the -1 based ImageIndex and the 0 based ListBox
        public int SelectedImageIndex {
            get {
                return SelectedIndex - 1;
            }
        }


        public void End() {
            Items.Clear();
            edSvc = null;
        }

        protected void FillData(TreeView treeView) {
            Items.Clear();

            Items.Add(SR.GetString("ImageIndexPicker_NoImage"));

            Debug.Assert(treeView != null, "treeView == null");

            int count = treeView.Images.Count;
            for (int i = 0; i < count; i++) {
                Items.Add(i.ToString() + " - " + treeView.Images[i].Url);
            }
        }

        protected override void OnKeyUp(KeyEventArgs e) {
            base.OnKeyUp(e);

            keyDown = true;
            mouseClicked = false;

            if (e.KeyData == Keys.Return) {
                keyDown = false;
                edSvc.CloseDropDown();
            }
        }

        protected override void OnMouseDown(MouseEventArgs e) {
            base.OnMouseDown(e);
            mouseClicked = true;
        }

        protected override void OnMouseUp(MouseEventArgs e) {
            base.OnMouseUp(e);
            mouseClicked = false;
        }

        protected override void OnSelectedIndexChanged(EventArgs e) {
            base.OnSelectedIndexChanged(e);

            // selecting an item w/ the keyboard is done via 
            // OnKeyDown. we will select an item w/ the mouse,
            // if this was the last thing that the user did
            if (mouseClicked && !keyDown) {
                mouseClicked = false;
                keyDown = false;
                edSvc.CloseDropDown();
            }

            return;
        }

        public void Start(IWindowsFormsEditorService edSvc, TreeView treeView, int selectedImageIndex) {
            this.edSvc = edSvc;

            // Fill in data....
            FillData(treeView);

            if (selectedImageIndex < -1 || selectedImageIndex >= treeView.Images.Count)
                selectedImageIndex = -1;

            this.SelectedIndex = selectedImageIndex + 1;
        }

    }
}
