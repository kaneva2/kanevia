///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////



namespace KlausEnt.KEP.Kaneva.WebControls {

    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Drawing.Design;
    using System.Windows.Forms.Design;


    /// <summary>
    /// </summary>
    public class ImageIndexEditor: UITypeEditor {

        private ImageIndexPicker imageIndexPicker;


        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value) {
            if (provider != null) {
                IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

                if (edSvc != null && context.Instance != null) {
                    if (imageIndexPicker == null) {
                        imageIndexPicker = new ImageIndexPicker();
                    }

                    // Grab TreeView property from the TreeNode so we have access to the Images property
                    TreeView treeView = ((TreeNode)context.Instance).TreeView;

                    // Call the editor
                    int previousIndex = (int)value;
                    imageIndexPicker.Start(edSvc, treeView, (int)value);
                    edSvc.DropDownControl(imageIndexPicker);
                    if (imageIndexPicker.SelectedIndex < -1)
                        value = previousIndex;
                    else
                        value = imageIndexPicker.SelectedImageIndex;
                    imageIndexPicker.End();
                }
            }

            return value;
        }

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context) {
            return UITypeEditorEditStyle.DropDown;
        }

    }    
}
