///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////



namespace KlausEnt.KEP.Kaneva.WebControls.Design {

    using System;
    using System.ComponentModel;
    using System.ComponentModel.Design;
    using System.Diagnostics;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Web.UI.Design;
    using System.Web.UI.WebControls;
    using System.Windows.Forms;

    using WebTreeView = KlausEnt.KEP.Kaneva.WebControls.TreeView;


    /// <summary>
    ///    <para>
    ///       The designer for a treeview control.
    ///    </para>
    /// </summary>
    public class TreeViewDesigner : ControlDesigner {

        private WebTreeView treeView;

        private DesignerVerbCollection designerVerbs;

        private const string emptyDesignTimeHtml =
            @"
                <table cellpadding=4 cellspacing=0 style=""font-family:Tahoma;font-size:8pt;color:buttontext;background-color:buttonface"">
                  <tr><td><span style=""font-weight:bold"">TreeView</span> - {0}</td></tr>
                  <tr><td>{1}</td></tr>
                </table>
             ";

        private const string errorDesignTimeHtml =
            @"
                <table cellpadding=4 cellspacing=0 style=""font-family:Tahoma;font-size:8pt;color:buttontext;background-color:buttonface;border: solid 1px;border-top-color:buttonhighlight;border-left-color:buttonhighlight;border-bottom-color:buttonshadow;border-right-color:buttonshadow"">
                  <tr><td><span style=""font-weight:bold"">TreeView</span> - {0}</td></tr>
                  <tr><td>{1}</td></tr>
                </table>
             ";


        /// <summary>
        ///    <para>
        ///       The designer's collection of verbs.
        ///    </para>
        /// </summary>
        /// <value>
        ///    <para>
        ///       An array of type <see cref='DesignerVerb'/> containing the verbs available to the
        ///       designer.
        ///    </para>
        /// </value>
        public override DesignerVerbCollection Verbs {
            // Stolen from BaseDataListDesigner
            get {
                if (designerVerbs == null) {
                    designerVerbs = new DesignerVerbCollection();
                    designerVerbs.Add(new DesignerVerb(SR.GetString("TreeViewDesigner_EditNodes"),
                                                        new EventHandler(this.OnTreeNodeCollectionEditor)));
                }

                designerVerbs[0].Enabled = true;

                return designerVerbs;
            }
        }

        /// <summary>
        ///    <para>
        ///       Gets the HTML to be used for the design time representation
        ///       of the control.
        ///    </para>
        /// </summary>
        /// <returns>
        ///    <para>
        ///       The design time HTML.
        ///    </para>
        /// </returns>
        public override string GetDesignTimeHtml() {
            if (treeView.Nodes.Count != 0) {

                // Get physical path of assembly so we can reference it in a res:// path
                string moduleName;
                ITypeResolutionService ts = (ITypeResolutionService)GetService(typeof(ITypeResolutionService));
                if (ts != null) {
                    moduleName = new Uri(ts.GetPathOfAssembly(this.GetType().Assembly.GetName())).LocalPath;
                }
                else {
                    moduleName = this.GetType().Module.FullyQualifiedName;
                }

                string resourcePath = "res://" + moduleName;

                bool expandedNodeImageUrlChanged = false;
                bool collapsedNodeImageUrlChanged = false;
                bool nodeImageUrlChanged = false;

                // Use design-time images if the URLs were not specified
                if (treeView.ExpandedNodeImageUrl.Length ==0) {
                    treeView.ExpandedNodeImageUrl = resourcePath + "//TREEVIEW_DESIGNTIME_IMAGE_EXPANDED";
                    expandedNodeImageUrlChanged = true;
                }

                if (treeView.CollapsedNodeImageUrl.Length ==0) {
                    treeView.CollapsedNodeImageUrl = resourcePath + "//TREEVIEW_DESIGNTIME_IMAGE_COLLAPSED";
                    collapsedNodeImageUrlChanged = true;
                }

                if (treeView.NodeImageUrl.Length ==0) {
                    treeView.NodeImageUrl = resourcePath + "//TREEVIEW_DESIGNTIME_IMAGE_NODE";
                    nodeImageUrlChanged = true;
                }

                string designTimeHtml;

                try {
                    // Save the HTML temporarily
                    designTimeHtml = base.GetDesignTimeHtml();
                }
                finally {
                    // Clear the URLs if they were changed
                    if (expandedNodeImageUrlChanged)
                        treeView.ExpandedNodeImageUrl = String.Empty;
                    if (collapsedNodeImageUrlChanged)
                        treeView.CollapsedNodeImageUrl = String.Empty;
                    if (nodeImageUrlChanged)
                        treeView.NodeImageUrl = String.Empty;
                }

                return designTimeHtml;
            }

            return GetEmptyDesignTimeHtml();
        }

        /// <summary>
        /// </summary>
        protected override string GetEmptyDesignTimeHtml() {
            string name = treeView.Site.Name;
            return String.Format(emptyDesignTimeHtml, name, SR.GetString("TreeViewDesigner_ToAddNodes"));
        }

        /// <summary>
        /// </summary>
        protected override string GetErrorDesignTimeHtml(Exception e) {
            string name = treeView.Site.Name;
            return String.Format(errorDesignTimeHtml, name, SR.GetString("TreeViewDesigner_ErrorRendering"));
        }

        public override void Initialize(IComponent component) {
            Debug.Assert(component is WebTreeView, "TreeViewDesigner::Initialize - Invalid TreeView");
            base.Initialize(component);

            treeView = (WebTreeView)component;
        }

        public void InvokeTreeNodeCollectionEditor() {
            OnTreeNodeCollectionEditor(null, null);
        }

        protected void OnTreeNodeCollectionEditor(object sender, EventArgs e) {
            IServiceProvider site = treeView.Site;
            IComponentChangeService changeService = null;

            if (site != null) {
                changeService = (IComponentChangeService)site.GetService(typeof(IComponentChangeService));
                if (changeService != null) {
                    try {
                        changeService.OnComponentChanging(treeView, null);
                    }
                    catch (CheckoutException ex) {
                        if (ex == CheckoutException.Canceled)
                            return;
                        throw ex;
                    }
                }
            }

            DialogResult result = DialogResult.Cancel;
            try {
                TreeNodeCollectionEditorDialog dlg = new TreeNodeCollectionEditorDialog(treeView);
                result = dlg.ShowDialog();
            }
            finally {
                if ((result == DialogResult.OK) && (changeService != null)) {
                    changeService.OnComponentChanged(treeView, null, null, null);
                }
            }
        }

    }

}

