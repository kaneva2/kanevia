///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////



namespace KlausEnt.KEP.Kaneva.WebControls.Design {

    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.ComponentModel.Design;
    using System.Diagnostics;
    using System.Drawing.Design;
    using System.Web.UI.WebControls;

    /// <summary>
    ///    <para>
    ///       The editor for column collections.
    ///    </para>
    /// </summary>
    public class TreeNodeCollectionEditor : UITypeEditor {

        /// <summary>
        ///    <para>
        ///       Edits the value specified.
        ///    </para>
        /// </summary>
        /// <param name='context'>
        ///    An <see cref='System.ComponentModel.ITypeDescriptorContext'/> that specifies the context of the value to edit.
        /// </param>
        /// <param name=' provider'>
        ///    An IServiceObjectProvider.
        /// </param>
        /// <param name=' value'>
        ///    The object to edit.
        /// </param>
        /// <returns>
        ///    <para>
        ///       The updated value.
        ///    </para>
        /// </returns>
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value) {
            IDesignerHost designerHost = (IDesignerHost)context.GetService(typeof(IDesignerHost));
            Debug.Assert(designerHost != null, "Did not get DesignerHost service.");

            Debug.Assert(context.Instance is TreeView, "Expected treeview");
            TreeView treeView = (TreeView)context.Instance;

            TreeViewDesigner designer = (TreeViewDesigner)designerHost.GetDesigner(treeView);
            Debug.Assert(designer != null, "Did not get designer for component");

            designer.InvokeTreeNodeCollectionEditor();
            return value;
        }

        /// <summary>
        ///    <para>
        ///       Gets the edit style.
        ///    </para>
        /// </summary>
        /// <param name='context'>
        ///    An <see cref='System.ComponentModel.ITypeDescriptorContext'/> that specifies the associated context.
        /// </param>
        /// <returns>
        ///    <para>
        ///       A <see cref='System.Drawing.Design.UITypeEditorEditStyle'/> that represents the edit style.
        ///    </para>
        /// </returns>
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context) {
            return UITypeEditorEditStyle.Modal;
        }
    }
}

