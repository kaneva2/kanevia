///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////



namespace KlausEnt.KEP.Kaneva.WebControls.Design {

    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.ComponentModel.Design;
    using System.Diagnostics;
    using System.Drawing;
    using System.Runtime.InteropServices;
    using System.Web.UI.WebControls;
    using System.Windows.Forms;
    using System.Windows.Forms.Design;

    using WebTreeNode = KlausEnt.KEP.Kaneva.WebControls.TreeNode;
    using WebTreeNodeCollection = KlausEnt.KEP.Kaneva.WebControls.TreeNodeCollection;
    using WebTreeView = KlausEnt.KEP.Kaneva.WebControls.TreeView;

    using WinTreeNode = System.Windows.Forms.TreeNode;
    using WinTreeNodeCollection = System.Windows.Forms.TreeNodeCollection;
    using WinTreeView = System.Windows.Forms.TreeView;
    using WinButton = System.Windows.Forms.Button;
    using WinLabel = System.Windows.Forms.Label;


    /// <summary>
    /// </summary>
    public class TreeNodeCollectionEditorDialog : Form {

        private class DesignTimeTreeNode : WinTreeNode {
            public WebTreeNode runTimeTreeNode;

            public DesignTimeTreeNode(WebTreeView owner) : base() {
                runTimeTreeNode = new WebTreeNode();

                // Must set TreeView property or else the ImageIndexEditor will not work, because it needs to get the Images
                // property from the TreeView.
                runTimeTreeNode.SetTreeView(owner);
            }

            public bool HasAncestor(WinTreeNode node) {
                if (Parent == null)
                    return false;

                if (node == Parent)
                    return true;

                return ((DesignTimeTreeNode)Parent).HasAncestor(node);
            }
        }

        // Components added to the property page
        private System.ComponentModel.Container components;

        private WinButton buttonOK;
        private WinButton helpButton;
        private WinButton cancelButton;

        private WinButton removeNodeButton;
        private WinButton addChildNodeButton;
        private WinButton addRootNodeButton;
        private WinButton upButton;
        private WinButton downButton;
        private WinButton unindentButton;
        private WinButton indentButton;
        private WinLabel nodesLabel;
        private WinTreeView winTreeView;
        private WinLabel propertiesLabel;
        private PropertyGrid propertyGrid;


        // Fields for internal use
        private WebTreeView webTreeView;
        private DesignTimeTreeNode currentNode;


        public TreeNodeCollectionEditorDialog(WebTreeView treeView) : base() {
            this.webTreeView = treeView;
            InitForm();
        }
        
        /// <summary>
        ///    Initialize the UI of this page
        /// </summary>
        /// <internalonly/>
        private void InitForm() {

            this.components = new System.ComponentModel.Container();
            this.removeNodeButton = new WinButton();
            this.nodesLabel = new WinLabel();
            this.propertiesLabel = new WinLabel();
            this.cancelButton = new WinButton();
            this.winTreeView = new WinTreeView();
            this.addChildNodeButton = new WinButton();
            this.addRootNodeButton = new WinButton();
            this.helpButton = new WinButton();
            this.downButton = new WinButton();
            this.upButton = new WinButton();
            this.unindentButton = new WinButton();
            this.indentButton = new WinButton();
            this.buttonOK = new WinButton();
            this.propertyGrid = new PropertyGrid();


            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.Text = SR.GetString("TreeNodeCollectionEditorDialog_NodesEditor");
            this.CancelButton = cancelButton;
            this.AcceptButton = buttonOK;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.ClientSize = new System.Drawing.Size(660, 550);
            this.ShowInTaskbar = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.MinimumSize = new Size(668, 450);

            Font f = Control.DefaultFont;
            ISite site = webTreeView.Site;
            IUIService uiService = (IUIService)site.GetService(typeof(IUIService));
            // REVIEW: If null, should we use RichControl.DefaultFont? M:\src\Designer\WebForms\System\Web\UI\Design\WebControls\ListControls\AutoFormatDialog.cs does not use it.
            if (uiService != null)
                f = (Font)uiService.Styles["DialogFont"];
            this.Font = f;


            nodesLabel.Location = new System.Drawing.Point(16, 8);
            nodesLabel.Text = SR.GetString("TreeNodeCollectionEditorDialog_Nodes");
            nodesLabel.Size = new System.Drawing.Size(104, 16);
            nodesLabel.TabIndex = 0;

            winTreeView.Location = new System.Drawing.Point(16, 24);
            winTreeView.Text = "treeView";
            winTreeView.Size = new System.Drawing.Size(340, 474);
            winTreeView.HideSelection = false;
            winTreeView.TabIndex = 1;
            winTreeView.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            winTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.OnAfterSelectTreeNode);
            winTreeView.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.OnBeforeSelectTreeNode);



            addRootNodeButton.Location = new System.Drawing.Point(16, 506);
            addRootNodeButton.Size = new System.Drawing.Size(108, 23);
            addRootNodeButton.TabIndex = 2;
            addRootNodeButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            addRootNodeButton.Text = SR.GetString("TreeNodeCollectionEditorDialog_AddRootNode");
            addRootNodeButton.Click += new EventHandler(this.OnClickAddRootNode);

            addChildNodeButton.Location = new System.Drawing.Point(132, 506);
            addChildNodeButton.Size = new System.Drawing.Size(108, 23);
            addChildNodeButton.TabIndex = 3;
            addChildNodeButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            addChildNodeButton.Text = SR.GetString("TreeNodeCollectionEditorDialog_AddChildNode");
            addChildNodeButton.Click += new EventHandler(this.OnClickAddChildNode);

            removeNodeButton.Location = new System.Drawing.Point(248, 506);
            removeNodeButton.Size = new System.Drawing.Size(108, 23);
            removeNodeButton.TabIndex = 4;
            removeNodeButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            removeNodeButton.Text = SR.GetString("TreeNodeCollectionEditorDialog_RemoveNode");
            removeNodeButton.Click += new EventHandler(this.OnClickRemoveNode);



            upButton.Location = new System.Drawing.Point(364, 40);
            upButton.Size = new System.Drawing.Size(23, 23);
            upButton.TabIndex = 5;
            upButton.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            upButton.Image = new Icon(this.GetType(), "SortUp.ico").ToBitmap();
            upButton.Click += new EventHandler(this.OnClickUp);

            downButton.Location = new System.Drawing.Point(364, 69);
            downButton.Size = new System.Drawing.Size(23, 23);
            downButton.TabIndex = 6;
            downButton.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            downButton.Image = new Icon(this.GetType(), "SortDown.ico").ToBitmap();
            downButton.Click += new EventHandler(this.OnClickDown);

            indentButton.Location = new System.Drawing.Point(364, 104);
            indentButton.Size = new System.Drawing.Size(23, 23);
            indentButton.TabIndex = 7;
            indentButton.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            indentButton.Image = new Icon(this.GetType(), "Indent.ico").ToBitmap();
            indentButton.Click += new EventHandler(this.OnClickIndent);

            unindentButton.Location = new System.Drawing.Point(364, 133);
            unindentButton.Size = new System.Drawing.Size(23, 23);
            unindentButton.TabIndex = 8;
            unindentButton.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            unindentButton.Image = new Icon(this.GetType(), "Unindent.ico").ToBitmap();
            unindentButton.Click += new EventHandler(this.OnClickUnindent);



            propertiesLabel.Location = new System.Drawing.Point(412, 8);
            propertiesLabel.Text = SR.GetString("TreeNodeCollectionEditorDialog_Properties");
            propertiesLabel.Size = new System.Drawing.Size(200, 16);
            propertiesLabel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            propertiesLabel.TabIndex = 10;

            propertyGrid.Location = new System.Drawing.Point(412, 24);
            propertyGrid.Size = new System.Drawing.Size(232, 474);
            propertyGrid.HelpVisible = true;
            propertyGrid.ToolbarVisible = false;
            propertyGrid.CommandsVisibleIfAvailable = false;
            propertyGrid.PropertySort = PropertySort.Categorized;
            propertyGrid.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            propertyGrid.TabIndex = 11;
            propertyGrid.TabStop = true;
            propertyGrid.PropertyValueChanged += new PropertyValueChangedEventHandler(OnPropertyValueChanged);



            helpButton.Location = new System.Drawing.Point(420, 506);
            helpButton.Size = new System.Drawing.Size(70, 23);
            helpButton.TabIndex = 20;
            helpButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            helpButton.Text = SR.GetString("TreeNodeCollectionEditorDialog_Help");
            helpButton.Enabled = false;

            buttonOK.Location = new System.Drawing.Point(496, 506);
            buttonOK.Size = new System.Drawing.Size(70, 23);
            buttonOK.TabIndex = 21;
            buttonOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonOK.Text = SR.GetString("TreeNodeCollectionEditorDialog_OK");
            buttonOK.DialogResult = DialogResult.OK;
            buttonOK.Click += new EventHandler(this.OnClickOK);

            cancelButton.Location = new System.Drawing.Point(572, 506);
            cancelButton.Size = new System.Drawing.Size(70, 23);
            cancelButton.TabIndex = 22;
            cancelButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            cancelButton.Text = SR.GetString("TreeNodeCollectionEditorDialog_Cancel");
            cancelButton.DialogResult = DialogResult.Cancel;



            this.Controls.Add(propertiesLabel);
            this.Controls.Add(nodesLabel);
            this.Controls.Add(addChildNodeButton);
            this.Controls.Add(addRootNodeButton);
            this.Controls.Add(indentButton);
            this.Controls.Add(unindentButton);
            this.Controls.Add(downButton);
            this.Controls.Add(upButton);
            this.Controls.Add(buttonOK);
            this.Controls.Add(helpButton);
            this.Controls.Add(cancelButton);
            this.Controls.Add(winTreeView);
            this.Controls.Add(removeNodeButton);
            this.Controls.Add(propertyGrid);


            // Load existing TreeView nodes into WinForms tree
            LoadTreeView(webTreeView.Nodes, winTreeView.Nodes);
            winTreeView.SelectedNode = winTreeView.Nodes.Count > 0 ? winTreeView.Nodes[0] : null;
            currentNode = (DesignTimeTreeNode)winTreeView.SelectedNode;
            UpdateUIElements();
        }


        private void LoadTreeView(WebTreeNodeCollection sourceNodes, WinTreeNodeCollection destNodes) {

            for (int i = 0; i < sourceNodes.Count; i++) {
                DesignTimeTreeNode node = new DesignTimeTreeNode(webTreeView);
                destNodes.Add(node);
                node.Text                                  = sourceNodes[i].Text;
                node.runTimeTreeNode.Text                  = sourceNodes[i].Text;
                node.runTimeTreeNode.NavigateUrl           = sourceNodes[i].NavigateUrl;
                node.runTimeTreeNode.Enabled               = sourceNodes[i].Enabled;
                node.runTimeTreeNode.Expandable            = sourceNodes[i].Expandable;
                node.runTimeTreeNode.ExpandedImageIndex    = sourceNodes[i].ExpandedImageIndex;
                node.runTimeTreeNode.ImageIndex            = sourceNodes[i].ImageIndex;
                node.runTimeTreeNode.NodeStyle.Reset();
                node.runTimeTreeNode.NodeStyle.CopyFrom(sourceNodes[i].NodeStyle);
                node.runTimeTreeNode.Target                = sourceNodes[i].Target;
                node.runTimeTreeNode.ToolTip               = sourceNodes[i].ToolTip;

                if (sourceNodes[i].Nodes.Count > 0) {
                    LoadTreeView(sourceNodes[i].Nodes, node.Nodes);
                }
                node.Expand();
            }
        }

        private void MoveChildNodes(WinTreeNode sourceNode, WinTreeNode destNode) {
            while (sourceNode.Nodes.Count > 0) {
                WinTreeNode tempNode = sourceNode.Nodes[0];
                sourceNode.Nodes[0].Remove();
                destNode.Nodes.Add(tempNode);
            }
        }

        private void OnAfterSelectTreeNode(object sender, System.Windows.Forms.TreeViewEventArgs e) {
            currentNode = (DesignTimeTreeNode)(e.Node);

            UpdateUIElements();
        }

        private void OnBeforeSelectTreeNode(object sender, System.Windows.Forms.TreeViewCancelEventArgs e) {
//            currentNode.Text = currentNode.runTimeTreeNode.Text;
        }

        private void OnClickAddChildNode(object sender, EventArgs e) {
            // Add a default node
            DesignTimeTreeNode tnp = new DesignTimeTreeNode(webTreeView);
            currentNode.Nodes.Add(tnp);
            string nodeText = SR.GetString("TreeNodeCollectionEditorDialog_NewNode");
            tnp.Text = nodeText;
            tnp.runTimeTreeNode.Text = nodeText;

            // If this is the first node in this branch, expand its parent
            if (currentNode.Nodes.Count > 0)
                currentNode.Expand();
        }

        private void OnClickAddRootNode(object sender, EventArgs e) {
            // Add a default node
            DesignTimeTreeNode tnp = new DesignTimeTreeNode(webTreeView);
            winTreeView.Nodes.Add(tnp);

            // If this is the first root node, select it
            if (winTreeView.Nodes.Count == 1)
                winTreeView.SelectedNode = tnp;

            string nodeText = SR.GetString("TreeNodeCollectionEditorDialog_NewNode");
            tnp.Text = nodeText;
            tnp.runTimeTreeNode.Text = nodeText;
        }

        private void OnClickDown(object sender, EventArgs e) {
            DesignTimeTreeNode tempNode = currentNode;
            int index1 = tempNode.Index;
            int index2 = tempNode.NextNode.Index;

            DesignTimeTreeNode tempNode1;
            DesignTimeTreeNode tempNode2;

            // Different cases if root node of regular child node
            if (tempNode.Parent != null) {
                tempNode1 = (DesignTimeTreeNode)tempNode.Parent.Nodes[index1];
                tempNode2 = (DesignTimeTreeNode)tempNode.Parent.Nodes[index2];
            }
            else {
                tempNode1 = (DesignTimeTreeNode)winTreeView.Nodes[index1];
                tempNode2 = (DesignTimeTreeNode)winTreeView.Nodes[index2];
            }

            SwapDesignTimeTreeNodes(tempNode1, tempNode2);

            // Set selection to what used to be the selected node
            winTreeView.SelectedNode = tempNode2;
            UpdateUIElements();
        }

        private void OnClickHelp(object sender, EventArgs e) {
        }

        private void OnClickIndent(object sender, EventArgs e) {
            DesignTimeTreeNode oldNode = currentNode;

            DesignTimeTreeNode newNode = new DesignTimeTreeNode(webTreeView);
            currentNode.PrevNode.Nodes.Add(newNode);

            // TODO: Only need to copy one-way, not swap
            SwapDesignTimeTreeNodes(oldNode, newNode);

            oldNode.Remove();

            // Set selection to what used to be the selected node
            winTreeView.SelectedNode = newNode;
            UpdateUIElements();
        }

        private void OnClickOK(object source, EventArgs e) {
            webTreeView.Nodes.Clear();
            SaveTreeView(winTreeView.Nodes, webTreeView.Nodes);
        }

        private void OnClickRemoveNode(object sender, EventArgs e) {
            DesignTimeTreeNode previousNode = currentNode;

            // Figure out where to place the new selected node since we're deleting the current selection
            if (currentNode.Parent != null) {   // If Parent is a WebTreeNode...
                if (currentNode.Parent.Nodes.Count == 1)
                    winTreeView.SelectedNode = currentNode.Parent;
                else if (currentNode.Index < currentNode.Parent.Nodes.Count - 1)
                    winTreeView.SelectedNode = currentNode.NextNode;
                else
                    winTreeView.SelectedNode = currentNode.PrevNode;
            }
            else {  // If Parent is null then it is a root node...
                if (winTreeView.Nodes.Count == 1)
                    winTreeView.SelectedNode = null;
                else if (currentNode.Index < winTreeView.Nodes.Count - 1)
                    winTreeView.SelectedNode = currentNode.NextNode;
                else
                    winTreeView.SelectedNode = currentNode.PrevNode;
            }

            previousNode.Remove();

            currentNode = (DesignTimeTreeNode)winTreeView.SelectedNode;

            UpdateUIElements();
        }

        private void OnClickUnindent(object sender, EventArgs e) {
            DesignTimeTreeNode oldNode = currentNode;

            DesignTimeTreeNode newNode = new DesignTimeTreeNode(webTreeView);

            // If node will become a root node, must add directly to TreeView, otherwise add in the normal fashion
            if (currentNode.Parent.Parent == null)
                winTreeView.Nodes.Add(newNode);
            else
                currentNode.Parent.Parent.Nodes.Add(newNode);

            // TODO: Only need to copy one-way, not swap
            SwapDesignTimeTreeNodes(oldNode, newNode);

            oldNode.Remove();

            // Set selection to what used to be the selected node
            winTreeView.SelectedNode = newNode;
            UpdateUIElements();
        }

        private void OnClickUp(object sender, EventArgs e) {
            DesignTimeTreeNode tempNode = currentNode;
            int index1 = tempNode.Index;
            int index2 = tempNode.PrevNode.Index;

            DesignTimeTreeNode tempNode1;
            DesignTimeTreeNode tempNode2;

            // Different cases if root node of regular child node
            if (tempNode.Parent != null) {
                tempNode1 = (DesignTimeTreeNode)tempNode.Parent.Nodes[index1];
                tempNode2 = (DesignTimeTreeNode)tempNode.Parent.Nodes[index2];
            }
            else {
                tempNode1 = (DesignTimeTreeNode)winTreeView.Nodes[index1];
                tempNode2 = (DesignTimeTreeNode)winTreeView.Nodes[index2];
            }

            SwapDesignTimeTreeNodes(tempNode1, tempNode2);

            // Set selection to what used to be the selected node
            winTreeView.SelectedNode = tempNode2;
            UpdateUIElements();
        }

        protected void OnPropertyValueChanged(object sender, PropertyValueChangedEventArgs e) {
            if (e.ChangedItem.Label == "Text")
                currentNode.Text = (string)e.ChangedItem.Value;
        }

        private void SwapDesignTimeTreeNodes(DesignTimeTreeNode node1, DesignTimeTreeNode node2) {
            // Swap child nodes
            WinTreeNode moveNode = new WinTreeNode();
            MoveChildNodes(node1, moveNode);
            MoveChildNodes(node2, node1);
            MoveChildNodes(moveNode, node2);

            // Swap expanded state
            bool expanded = node1.IsExpanded;
            if (node2.IsExpanded)
                node1.Expand();
            else
                node1.Collapse();

            if (expanded)
                node2.Expand();
            else
                node2.Collapse();

            string text = node1.Text;
            node1.Text = node2.Text;
            node2.Text = text;

            // Swap AdvancedWebControls properties
            WebTreeNode tempNode = node1.runTimeTreeNode;
            node1.runTimeTreeNode = node2.runTimeTreeNode;
            node2.runTimeTreeNode = tempNode;
        }

        private void SaveTreeView(WinTreeNodeCollection sourceNodes, WebTreeNodeCollection destNodes) {
            for (int i = 0; i < sourceNodes.Count; i++) {
                destNodes.Add(new WebTreeNode());
                DesignTimeTreeNode node = (DesignTimeTreeNode)sourceNodes[i];
                destNodes[i].Text                  = node.runTimeTreeNode.Text;
                destNodes[i].NavigateUrl           = node.runTimeTreeNode.NavigateUrl;
                destNodes[i].Enabled               = node.runTimeTreeNode.Enabled;
                destNodes[i].Expandable            = node.runTimeTreeNode.Expandable;
                destNodes[i].ExpandedImageIndex    = node.runTimeTreeNode.ExpandedImageIndex;
                destNodes[i].ImageIndex            = node.runTimeTreeNode.ImageIndex;
                destNodes[i].NodeStyle.Reset();
                destNodes[i].NodeStyle.CopyFrom(node.runTimeTreeNode.NodeStyle);
                destNodes[i].Target                = node.runTimeTreeNode.Target;
                destNodes[i].ToolTip               = node.runTimeTreeNode.ToolTip;

                if (node.Nodes.Count > 0)
                    SaveTreeView(node.Nodes, destNodes[i].Nodes);
            }
        }

        private void UpdateUIElements() {
            if (currentNode != null) {
                // Only enable the proper buttons
                upButton.Enabled = currentNode.PrevNode != null;
                downButton.Enabled = currentNode.NextNode != null;
                unindentButton.Enabled = currentNode.Parent != null;
                indentButton.Enabled = currentNode.PrevNode != null;

                removeNodeButton.Enabled = true;
                addChildNodeButton.Enabled = true;

                // Tell the property grid the control to use
                propertyGrid.SelectedObject = currentNode.runTimeTreeNode;
            }
            else {
                // Disable all buttons
                upButton.Enabled = false;
                downButton.Enabled = false;
                unindentButton.Enabled = false;
                indentButton.Enabled = false;

                removeNodeButton.Enabled = false;
                addChildNodeButton.Enabled = false;

                // Empty the property grid
                propertyGrid.SelectedObject = null;
            }
        }

    }
}

