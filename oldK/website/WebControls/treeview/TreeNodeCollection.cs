///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////



namespace KlausEnt.KEP.Kaneva.WebControls {

    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    /// <summary>
    ///   Represents a collection of TreeNodes in a TreeView control or within another TreeNode.
    /// </summary>
    /// <seealso cref="TreeView"/>
    /// <seealso cref="TreeNode"/>
    public class TreeNodeCollection : ICollection, IStateManager {

        protected TreeNode owner;
        private bool marked;
        private ArrayList nodes;

        /// <summary>
        ///   Creates a new instance of TreeNodeCollection.
        /// </summary>
        public TreeNodeCollection(TreeNode owner) {
            this.owner = owner;
            marked = false;
        }


        /// <summary>
        ///   The number of child nodes in the collection.
        /// </summary>
        public int Count {
            get {
                if (nodes != null)
                    return nodes.Count;
                else
                    return 0;
            }
        }

        public bool IsReadOnly {
            get {
                return false;
            }
        }

        public bool IsSynchronized {
            get {
                return false;
            }
        }

        bool IStateManager.IsTrackingViewState {
            get {
                return marked;
            }
        }

        public object SyncRoot {
            get {
                return this;
            }
        }

        public TreeNode this[int index] {
            get {
                if (nodes != null)
                    return (TreeNode)(nodes[index]);
                else
                    return null;
            }
        }

        /// <summary>
        ///   Adds a child node to the collection.
        /// </summary>
        public void Add(TreeNode node) {
            AddAt(-1, node);
        }

        /// <summary>
        ///   Adds a child node to the collection at a specific position.
        /// </summary>
        public void AddAt(int index, TreeNode node) {
            if (nodes == null)
                nodes =  new ArrayList();

            if (index == -1)
                nodes.Add(node);
            else
                nodes.Insert(index, node);

            // Set Parent and TreeView properties of new node in collection
            UpdateChildParentStatus(node);
        }

        /// <summary>
        ///   Removes all child nodes from the collection.
        /// </summary>
        public void Clear() {
            if (nodes != null) {
                while (nodes.Count > 0)
                    RemoveAt(0);
            }
        }

        public void CopyTo(Array array, int index) {
            for (IEnumerator e = this.GetEnumerator(); e.MoveNext();)
                array.SetValue(e.Current, index++);
        }

        /// <summary>
        ///   Returns an enumerator that enumerates over the child nodes in a TreeNode in order.
        /// </summary>
        public IEnumerator GetEnumerator() {
            if (nodes == null) {
                nodes = new ArrayList();
            }

            return nodes.GetEnumerator(0, Count);
        }

        /// <summary>
        ///   Index of a child node in the collection.
        /// </summary>
        public int IndexOf(TreeNode node) {
            if (node != null && nodes != null)
                return nodes.IndexOf(node, 0, Count);

            return -1;
        }

        /// <summary>
        /// </summary>
        public void Remove(TreeNode node) {
            int index = IndexOf(node);
            if (index >= 0)
                RemoveAt(index);
        }

        /// <summary>
        ///   Removes the node at the specified index in the collection.
        /// </summary>
        /// <remarks>
        ///   You cannot remove parsed nodes.
        /// </remarks>
        public void RemoveAt(int index) {
            TreeNode node = this[index];
            if (node != null) {
                // REVIEW: Should we check for DesignMode here?
                // This is necessary because in DesignTime we ARE allowed to remove parsed nodes

                if (node.parsedNode &&
                    owner != null &&
                    owner.TreeView != null &&
                    owner.TreeView.Site != null &&
                    !owner.TreeView.Site.DesignMode) {
                    throw new InvalidOperationException(String.Format(SR.GetString("TreeNodeCollection_CannotRemoveStaticNode"), node.Text));
                }
                nodes.RemoveAt(index);
            }
        }

        private void UpdateChildParentStatus(TreeNode n) {
            n.SetParent(owner);
            n.SetTreeView(owner.TreeView);

            for (int i = 0; i < n.Nodes.Count; i++) {
                UpdateChildParentStatus(n.Nodes[i]);
            }
        }

        /// <summary>
        ///  Load previously saved state.
        ///  Method of private interface, IStateManager.
        /// </summary>
        void IStateManager.LoadViewState(object state) {
            if (state != null) {
                object[] nodeStates = (object[])state;

                // Go through parsed nodes and load their states
                int parsedNodeCount = 0;
                for (int i=0; i < nodeStates.Length; i++) {
                    object[] stateItem = (object[])nodeStates[i];
                    if (stateItem.Length == 1) {
                        if (this[parsedNodeCount] != null) {
                            object nodeState = stateItem[0];
                            if (nodeState != null) {
                                ((IStateManager)this[parsedNodeCount]).LoadViewState(nodeState);
                            }
                            this[parsedNodeCount].parsedNode = true;
                        }

                        parsedNodeCount++;
                    }
                    // null state for this node... This can only happen with parsed nodes so we count it
                    if (stateItem.Length == 0) {
                        parsedNodeCount++;
                    }
                }

                // Go through dynamic nodes, create and insert them into tree, and load their states
                for (int i=0; i < nodeStates.Length; i++) {
                    object[] stateItem = (object[])nodeStates[i];
                    if (stateItem.Length == 2) {
                        object nodeState = stateItem[0];
                        int nodeIndex = (int)stateItem[1];

                        TreeNode newNode = new TreeNode();
                        AddAt(nodeIndex, newNode);

                        // Mark the dynamic node immediately to ensure it is entirely "dirty"
                        ((IStateManager)this[nodeIndex]).TrackViewState();
                        ((IStateManager)this[nodeIndex]).LoadViewState(nodeState);
                    }
                }
            }
        }

        /// <summary>
        ///  Return object containing state changes.
        ///  Method of private interface, IStateManager.
        /// </summary>
        object IStateManager.SaveViewState() {

            if (Count == 0)
                return null;

            // save all nodes
            object[] items = new object[Count];

            // Only return state array if some node has state, assume false at first
            bool somethingHasState = false;

            for (int i=0; i < Count; i++) {
                if (this[i].parsedNode) {
                    // For parsed nodes, we just use the state itself
                    object currentNodeState = ((IStateManager)this[i]).SaveViewState();
                    items[i] = new object[1] {currentNodeState};
                    // Set flag if there is a state
                    if (currentNodeState != null)
                        somethingHasState = true;
                }
                else {
                    // If it is a dynamic node, store the state AND the index to insert into
                    items[i] = new object[2] {((IStateManager)this[i]).SaveViewState(), i};
                    // State from dynamic nodes must always be saved so we automatically set the flag to true
                    somethingHasState = true;
                }
            }

            return somethingHasState ? items : null;
        }

        /// <summary>
        ///  Start tracking state changes.
        ///  Method of private interface, IStateManager.
        /// </summary>
        void IStateManager.TrackViewState() {
            marked = true;
            for (int i=0; i < Count; i++)
                ((IStateManager)this[i]).TrackViewState();
        }
    }
}

