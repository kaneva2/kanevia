///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////



namespace KlausEnt.KEP.Kaneva.WebControls {

    using System;

    /// <summary>
    ///    <para>Represents the method that will handle events of a <see cref='AdvancedWebControls.TreeView'/>.</para>
    /// </summary>
    /// <param name='source'>The source of the event.</param>
    /// <param name='e'>An <see cref='AdvancedWebControls.TreeViewEventArgs'/> that contains the event data.</param>
    public delegate void TreeViewEventHandler(object source, TreeViewEventArgs e);
}
