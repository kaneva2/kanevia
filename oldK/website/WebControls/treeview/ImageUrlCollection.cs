///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////



namespace KlausEnt.KEP.Kaneva.WebControls {

    using System;
    using System.Collections;
    using System.Web.UI;

    /// <summary>
    ///   A collection of ImageUrl objects.
    /// </summary>
    public class ImageUrlCollection : ICollection, IList, IStateManager {

        private ArrayList imageUrls;
        private bool marked;
        private bool saveAll;


        /// <summary>
        /// </summary>
        public ImageUrlCollection() {
            imageUrls = new ArrayList();
            marked = false;
            saveAll = false;
        }

        /// <summary>
        ///    The number of ImageUrls in this collection.
        /// </summary>
        public int Count {
            get {
                if (imageUrls != null)
                    return imageUrls.Count;
                else
                    return 0;
            }
        }

        bool IList.IsFixedSize {
            get {
                return false;
            }
        }

        public bool IsReadOnly {
            get {
                return false;
            }
        }

        public bool IsSynchronized {
            get {
                return false;
            }
        }

        bool IStateManager.IsTrackingViewState {
            get {
                return marked;
            }
        }

        public object SyncRoot {
            get {
                return this;
            }
        }

        public ImageUrl this[int index] {
            get {
                if (imageUrls != null)
                    return (ImageUrl)(imageUrls[index]);
                else
                    return null;
            }
        }

        object IList.this[int index] {
            get {
                return imageUrls[index];
            }
            set {
                imageUrls[index] = (ImageUrl)value;
            }
        }


        int IList.Add(object item) {
            ImageUrl newItem = (ImageUrl)item;
            int returnValue = imageUrls.Add(newItem);
            if (marked) {
                newItem.dirty = true;
            }
            return returnValue;
        }

        /// <summary>
        ///    Adds a new ImageUrl to the collection from a string.
        /// </summary>
        public void Add(string url) {
            Add(new ImageUrl(url));
        }

        /// <summary>
        ///   Adds an existing ImageUrl to the collection.
        /// </summary>
        public void Add(ImageUrl url) {
            imageUrls.Add(url);
            if (marked)
                url.dirty = true;
        }

        /// <summary>
        ///   Clears the collection.
        /// </summary>
        public void Clear() {
            imageUrls.Clear();
            if (marked)
                saveAll = true;
        }

        public bool Contains(ImageUrl item) {
            return imageUrls.Contains(item);
        }

        bool IList.Contains(object item) {
            return Contains((ImageUrl) item);
        }

        /// <summary>
        /// </summary>
        public void CopyTo(Array array, int index) {
            imageUrls.CopyTo(array, index);
        }

        /// <summary>
        /// </summary>
        public IEnumerator GetEnumerator() {
            return imageUrls.GetEnumerator();
        }

        /// <summary>
        /// </summary>
        public int IndexOf(ImageUrl url) {
            return imageUrls.IndexOf(url);
        }

        int IList.IndexOf(object item) {
            return IndexOf((ImageUrl) item);
        }

        /// <summary>
        /// </summary>
        public void Insert(int index, string url) {
            Insert(index, new ImageUrl(url));
        }

        /// <summary>
        /// </summary>
        public void Insert(int index, ImageUrl url) {
            imageUrls.Insert(index, url);
            if (marked)
                saveAll = true;
        }

        void IList.Insert(int index, object item) {
            Insert(index, (ImageUrl)item);
        }

        /// <summary>
        /// </summary>
        public void RemoveAt(int index) {
            imageUrls.RemoveAt(index);
            if (marked)
                saveAll = true;
        }

        /// <summary>
        /// </summary>
        public void Remove(string url) {
            Remove(new ImageUrl(url));
        }

        /// <summary>
        /// </summary>
        public void Remove(ImageUrl url) {
            int index = IndexOf(url);
            if (index >= 0) {
                RemoveAt(index);
            }
        }

        void IList.Remove(object item) {
            Remove((ImageUrl)item);
        }

        /// <summary>
        ///  Load previously saved state.
        ///  Method of private interface, IStateManager.
        /// </summary>
        void IStateManager.LoadViewState(object state) {
            if (state != null) {
                object[] changes = (object[])state;
                if (changes.Length == 1) {
                    // all URLs were saved
                    object[] urls = (object[])changes[0];
                    Clear();
                    for (int i=0; i < urls.Length; i++) {
                        string url = (string)urls[i];
                        Add(new ImageUrl(url));
                    }
                }
                else {
                    // only changed URLs were saved
                    int[] indices = (int[])changes[0];
                    object[] urls = (object[])changes[1];
                    for (int i=0; i < urls.Length; i++) {
                        int index = indices[i];
                        if (index < Count) {
                            ((IStateManager)this[indices[i]]).LoadViewState(urls[i]);
                        }
                        else {
                            string url = (string)urls[i];
                            Add(new ImageUrl(url));
                        }
                    }
                }
            }
        }

        /// <summary>
        ///  Start tracking state changes.
        ///  Method of private interface, IStateManager.
        /// </summary>
        void IStateManager.TrackViewState() {
            marked = true;
            for (int i=0; i < Count; i++)
                ((IStateManager)this[i]).TrackViewState();
        }

        /// <summary>
        ///  Return object containing state changes.
        ///  Method of private interface, IStateManager.
        /// </summary>
        object IStateManager.SaveViewState() {
            object[] changes = null;
            if (saveAll == true) { 
                // save all URLs
                object[] urls = new object[Count];
                for (int i=0; i < Count; i++)
                    urls[i] = this[i].Url;
                changes = new object[1] { urls };
            }
            else { 
                // save only the changed urls
                int n = 0;
                int[] tIndices = new int[3];
                object[] tUrls = new object[3];
                for (int i=0; i < Count; i++) {
                    object url = ((IStateManager)this[i]).SaveViewState();
                    if (url != null) {
                        if (n == tIndices.Length) {
                            int[] t1 = new int[n+n];
                            tIndices.CopyTo(t1,0);
                            tIndices = t1;
                            object[] t2 = new object[n+n];
                            tUrls.CopyTo(t2,0);
                            tUrls = t2;
                        }
                        tIndices[n] = i;
                        tUrls[n] = this[i].Url;
                        n++;
                    }
                }
                if (n > 0) {
                    // some URL has changed
                    int[] indices = new int[n];
                    Array.Copy(tIndices, 0, indices, 0, n);
                    object[] urls = new object[n];
                    Array.Copy(tUrls, 0, urls, 0, n);
                    changes = new object[2] { indices, urls };
                }
            }
            return changes;
        }

    }
}
