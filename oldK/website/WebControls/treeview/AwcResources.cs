///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Reflection;
using System.Globalization;
using System.Resources;
using System.Text;
using System.Threading;
using System.ComponentModel;

namespace KlausEnt.KEP.Kaneva.WebControls
{
	/// <summary>
	/// Summary description for AwcDescriptionAttribute.
	/// </summary>
    [AttributeUsage(AttributeTargets.All)]
    internal sealed class AwcDescriptionAttribute : DescriptionAttribute
	{
		public AwcDescriptionAttribute(string description) : base(description) 
		{
		}

        private bool replaced = false;

        public override string Description 
        {
            get 
            {
                if (!replaced) 
                {
                    replaced = true;
                    DescriptionValue = SR.GetString(base.Description);
                }
                return base.Description;
            }
        }

    }

    [AttributeUsage(AttributeTargets.All)]
    internal sealed class AwcCategoryAttribute : CategoryAttribute 
    {

        public AwcCategoryAttribute(string category) : base(category) 
        {
        }

        protected override string GetLocalizedString(string value) 
        {
            return SR.GetString(value);
        }
    }

    internal sealed class SR 
    {
        static SR loader = null;
        ResourceManager resources;

        private SR() 
        {
            resources = new System.Resources.ResourceManager("AdvancedWebControls", this.GetType().Module.Assembly);
        }

        private static SR GetLoader() 
        {
            if (loader == null) 
            {
                lock(typeof(SR)) 
                {
                    if (loader == null) 
                    {
                        loader = new SR();
                    }
                }
            }

            return loader;
        }

        public static string GetString(string name) 
        {
            return GetString(null, name);
        }

        public static string GetString(CultureInfo culture, string name) 
        {
            SR sys = GetLoader();
            if (sys == null)
                return null;
            return sys.resources.GetString(name, culture);
        }

        public static object GetObject(string name) 
        {
            return GetObject(null, name);
        }

        public static object GetObject(CultureInfo culture, string name) 
        {
            SR sys = GetLoader();
            if (sys == null)
                return null;
            return sys.resources.GetObject(name, culture);
        }
    }

}
