///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////



namespace KlausEnt.KEP.Kaneva.WebControls {

    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.ComponentModel.Design;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.Drawing.Design;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;


    public class TreeViewBuilder : ControlBuilder {
        public override Type GetChildControlType(string tagName, IDictionary attribs) {
            // override to allow TreeNode without runat=server to be added
            if (tagName.ToLower().EndsWith("treenode"))
                return typeof(TreeNode);

            return null;
        }

        public override void AppendLiteralString(string s) {
            // override to ignore literals between items
        }
    }

    /// <summary>
    ///   Displays data in a hierarchical fashion.
    /// </summary>
    [
    ControlBuilderAttribute(typeof(TreeViewBuilder)),
    DefaultEvent("Expand"),
    DefaultProperty("Nodes"),
    Designer(typeof(KlausEnt.KEP.Kaneva.WebControls.Design.TreeViewDesigner)),
    ]
    public class TreeView : WebControl, IPostBackEventHandler {

        internal const char EVENTSPECIFIER_COLLAPSE = 'c';
        internal const char EVENTSPECIFIER_EXPAND = 'e';


        private Style nodeStyle;
        private Style disabledNodeStyle;
        private TreeNode root;

        protected static readonly object EventExpand = new object();
        protected static readonly object EventCollapse = new object();

        private ImageUrlCollection images;

        private bool renderUplevel;
        private TreeViewRenderContext renderContext;


        // constants for Validation script library
        private const string TreeViewScriptFileName = "TreeViewScript.js";
        private const string TreeViewScriptVersion = "1912";
        private const string TreeViewScriptKey = "TreeViewScript";
        private const string TreeViewScript = @"
            <script language=""javascript"" src=""{0}{1}"">
            </script>
        ";


        /// <summary>
        /// </summary>
        public TreeView() : base("div") {
        }


        /// <summary>
        ///   The image displayed when a node with children is collapsed. When the user clicks on the image it will expand the node and display its children.
        /// </summary>
        [
        AwcCategory("Appearance"),
        AwcDescription("TreeView_CollapsedNodeImageUrl"),
        Bindable(true),
        DefaultValue(""),
        Editor("System.Web.UI.Design.ImageUrlEditor, System.Web.UI.Design", typeof(UITypeEditor)),
        PersistenceMode(PersistenceMode.Attribute),
        ]
        public string CollapsedNodeImageUrl {
            get {
                object s = ViewState["CollapsedNodeImageUrl"];
                return ((s == null) ? String.Empty : (string)s);
            }
            set {
                ViewState["CollapsedNodeImageUrl"] = value;
            }
        }

        /// <summary>
        ///   The style of a disabled node in the tree.
        /// </summary>
        [
        AwcCategory("Style"),
        AwcDescription("TreeView_DisabledNodeStyle"),
        PersistenceMode(PersistenceMode.InnerProperty),
        ]
        public Style DisabledNodeStyle {
            get {
                if (disabledNodeStyle == null) {
                    disabledNodeStyle = new Style();
                    if (IsTrackingViewState)
                        ((IStateManager)disabledNodeStyle).TrackViewState();
                }
                return disabledNodeStyle;
            }
        }

        /// <summary>
        ///   The image displayed when a node with children is expanded. When the user clicks on the image it will collapse the node and hide its children.
        /// </summary>
        [
        AwcCategory("Appearance"),
        AwcDescription("TreeView_ExpandedNodeImageUrl"),
        Bindable(true),
        DefaultValue(""),
        Editor("System.Web.UI.Design.ImageUrlEditor, System.Web.UI.Design", typeof(UITypeEditor)),
        PersistenceMode(PersistenceMode.Attribute),
        ]
        public string ExpandedNodeImageUrl {
            get {
                object s = ViewState["ExpandedNodeImageUrl"];
                return ((s == null) ? String.Empty : (string)s);
            }
            set {
                ViewState["ExpandedNodeImageUrl"] = value;
            }
        }

        /// <summary>
        ///   List of images to be used for nodes.
        /// </summary>
        [
        AwcCategory("Appearance"),
        AwcDescription("TreeView_Images"),
        Browsable(true),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        ]
        public ImageUrlCollection Images {
            get {
                if (images == null) {
                    images = new ImageUrlCollection();
                    if (IsTrackingViewState)
                        ((IStateManager)images).TrackViewState();
                }
                return images;
            }
        }

        /// <summary>
        ///   The image displayed when a node has no children.
        /// </summary>
        [
        AwcCategory("Appearance"),
        AwcDescription("TreeView_NodeImageUrl"),
        Bindable(true),
        DefaultValue(""),
        Editor("System.Web.UI.Design.ImageUrlEditor, System.Web.UI.Design", typeof(UITypeEditor)),
        PersistenceMode(PersistenceMode.Attribute),
        ]
        public string NodeImageUrl {
            get {
                object s = ViewState["NodeImageUrl"];
                return ((s == null) ? String.Empty : (string)s);
            }
            set {
                ViewState["NodeImageUrl"] = value;
            }
        }

        /// <summary>
        ///   The collection of root nodes of the tree.
        /// </summary>
        [
        AwcCategory("Data"),
        AwcDescription("TreeView_Nodes"),
        Browsable(true),
        DefaultValue(null),
        Editor(typeof(KlausEnt.KEP.Kaneva.WebControls.Design.TreeNodeCollectionEditor), typeof(UITypeEditor)),
        PersistenceMode(PersistenceMode.InnerProperty),
        ]
        public TreeNodeCollection Nodes {
            get {
                return Root.Nodes;
            }
        }

        /// <summary>
        ///   The style applied to all nodes in the tree.
        /// </summary>
        [
        AwcCategory("Style"),
        AwcDescription("TreeView_NodeStyle"),
        PersistenceMode(PersistenceMode.InnerProperty),
        ]
        public Style NodeStyle {
            get {
                if (nodeStyle == null) {
                    nodeStyle = new Style();
                    if (IsTrackingViewState)
                        ((IStateManager)nodeStyle).TrackViewState();
                }
                return nodeStyle;
            }
        }

        /// <summary>
        ///   Gets the RenderUplevel property.
        /// </summary>
        protected bool RenderUplevel {
            get {
                return renderUplevel;
            }
        }

        /// <summary>
        /// </summary>
        [
        Browsable(false),
        ]
        private TreeNode Root {
            get {
                if (root == null) {
                    root = new TreeNode("root");
                    root.SetTreeView(this);
                    root.parsedNode = true;
                    root.ViewState["IsExpanded"] = true;
                    root.SetNodeID("root");
                }
                return root;
            }
        }

        /// <summary>
        ///   The default hyperlink target for links in the tree. This can be overridden in the TreeNode Target property for an individual node.
        /// </summary>
        [
        AwcCategory("Behavior"),
        AwcDescription("TreeView_Target"),
        Bindable(true),
        DefaultValue(""),
        PersistenceMode(PersistenceMode.Attribute),
        TypeConverter("System.Web.UI.WebControls.TargetConverter, System.Web"),
        ]
        public string Target {
            get {
                object s = ViewState["Target"];
                return ((s == null) ? String.Empty : (string)s);
            }
            set {
                ViewState["Target"] = value;
            }
        }


        [
        AwcCategory("Action"),
        AwcDescription("TreeView_Collapse"),
        ]
        public event TreeViewEventHandler Collapse {
            add {
                Events.AddHandler(EventCollapse, value);
            }
            remove {
                Events.RemoveHandler(EventCollapse, value);
            }
        }

        [
        AwcCategory("Action"),
        AwcDescription("TreeView_Expand"),
        ]
        public event TreeViewEventHandler Expand {
            add {
                Events.AddHandler(EventExpand, value);
            }
            remove {
                Events.RemoveHandler(EventExpand, value);
            }
        }


        protected override void AddAttributesToRender(HtmlTextWriter writer) {
            base.AddAttributesToRender(writer);

            // REVIEW: Add an ID tag? Bug when tree is created dynamically... No ID property
            // on main <div> causes JavaScript to fail.

            renderContext = new TreeViewRenderContext(this, Page);
            if (RenderUplevel) {
                writer.AddAttribute("expandedNodeImageUrl", renderContext.expandedNodeImageUrl);
                writer.AddAttribute("collapsedNodeImageUrl", renderContext.collapsedNodeImageUrl);
            }
        }

        /// <summary>
        ///   Collapses all nodes recursively.
        /// </summary>
        public void CollapseAll() {
            int count = Nodes.Count;
            for (int i = 0; i < count; i++)
                Nodes[i].Collapse(true);
        }

        private bool DetermineRenderUplevel() {

            // must be on a page
            Page page = Page;
            if (page == null || page.Request == null) {
                return false;
            }

            // Check the browser capabilities 
            return (page.Request.Browser.MSDomVersion.Major >= 4 &&
                    page.Request.Browser.EcmaScriptVersion.CompareTo(new Version(1, 2)) >= 0);
        }

        /// <summary>
        ///   Expands all nodes recursively.
        /// </summary>
        public void ExpandAll() {
            int count = Nodes.Count;
            for (int i = 0; i < count; i++)
                Nodes[i].Expand(true);
        }

        private void LoadExpandedState(string expandedState) {
            Stack nodes = new Stack();
            Stack counts = new Stack();

            TreeNode currentNode = Root;
            int currentCount = 0;

            nodes.Push(currentNode);
            counts.Push(currentCount);

            for (int i = 0; i < expandedState.Length; i++) {
                switch (expandedState[i]) {
                    case '[':
                        // Save current node and count states and work on child nodes
                        nodes.Push(currentNode);
                        counts.Push(currentCount);
                        currentNode = currentNode.Nodes[currentCount - 1];
                        currentCount = 0;
                        break;

                    case ']':
                        // Restore current node and count states
                        currentNode = (TreeNode)nodes.Pop();
                        currentCount = (int)counts.Pop();
                        break;

                    case 'n':
                        // Skip expandable nodes without children
                        currentCount++;
                        break;

                    case 'e':
                        // Expand node
                        currentNode.Nodes[currentCount].Expand(false);
                        currentCount++;
                        break;

                    case 'c':
                        // Collapse node
                        currentNode.Nodes[currentCount].Collapse(false);
                        currentCount++;
                        break;

                    default:
                        // We should never ever get here because the only 5 possible characters are checked for
                        Debug.Fail("Unknown state specifier: " + expandedState[i]);
                        break;
                }
            }
        }

        /// <summary>
        /// </summary>
        protected override void LoadViewState(object savedState) {
            if (savedState != null) {
                object[] myState = (object[])savedState;

                if (myState[0] != null)
                    base.LoadViewState(myState[0]);
                if (myState[1] != null)
                    ((IStateManager)NodeStyle).LoadViewState(myState[1]);
                if (myState[2] != null)
                    ((IStateManager)DisabledNodeStyle).LoadViewState(myState[2]);
                if (myState[3] != null)
                    ((IStateManager)Root).LoadViewState(myState[3]);
                if (myState[4] != null)
                    ((IStateManager)Images).LoadViewState(myState[4]);

                // Retrieve expanded state from the request, and chop off the encasing brackets
                string expandedState = Context.Request[ClientID + "_expandedstate"];
                LoadExpandedState(expandedState.Substring(1, expandedState.Length - 1));
            }
        }

        /// <summary>
        ///   Called when a TreeNode is collapsed.
        /// </summary>
        /// <param name='e'>
        /// </param>
        public virtual void OnCollapse(TreeViewEventArgs e) {
            TreeViewEventHandler handler = (TreeViewEventHandler)Events[EventCollapse];
            if (handler != null) handler(this, e);
        }

        /// <summary>
        ///   Called when a TreeNode is expanded.
        /// </summary>
        /// <param name='e'>
        /// </param>
        public virtual void OnExpand(TreeViewEventArgs e) {
            TreeViewEventHandler handler = (TreeViewEventHandler)Events[EventExpand];
            if (handler != null) handler(this, e);
        }

        protected override void OnInit(EventArgs e) {
            base.OnInit(e);

            // Set all children to be parsed nodes and set their parent status
            UpdateChildParsedStatus(Root);
        }

        /// <summary>
        ///  This method is invoked just prior to rendering.
        ///  Register client script for handling postback.
        /// </summary>
        protected override void OnPreRender(EventArgs e) {
            base.OnPreRender(e);

            if (Page != null) {
                // REVIEW: Change #395 in http://net/change.aspx says RegisterPostBackScript no longer necessary
                //Page.RegisterPostBackScript();
                StringBuilder expandedState = new StringBuilder();
                SaveExpandedState(Root, expandedState);
                Page.ClientScript.RegisterHiddenField(ClientID + "_expandedstate", expandedState.ToString());
            }

            // work out uplevelness now
            // This is stolen from the Validator controls
            renderUplevel = DetermineRenderUplevel();

            if (Site != null && Site.DesignMode) {
                // Design Time
                // Render downlevel
                renderUplevel = false;
            }


            if (RenderUplevel) {
                RegisterTreeViewScript();
            }

        }

        /// <summary>
        ///    <para>
        ///       Method of IPostBackEventHandler interface to raise events on post back.
        ///    </para>
        /// </summary>
        /// <param name='eventArgument'>
        /// </param>
        public virtual void RaisePostBackEvent(string eventArgument) {

            // Create full name of control (i.e. remove the event specifier character)
            char eventSpecifier = eventArgument[0];
            string fullNodeID = eventArgument.Substring(1);

            // Get actual TreeNode from NodeID - but SKIP the "root:" part!
            TreeNode sender = Root.FindNode(fullNodeID, 5);

            if (sender == null)
                return;

            switch (eventSpecifier) {
                case EVENTSPECIFIER_EXPAND:
                    sender.Expand(false);
                    break;
                case EVENTSPECIFIER_COLLAPSE:
                    sender.Collapse(false);
                    break;
            }

        }

        protected void RegisterTreeViewScript() {

            if (Page.ClientScript.IsStartupScriptRegistered(GetType(), TreeViewScriptKey))
            {
                return;
            }

            // prepare script
            // UNDONE: Get real client script location in a standard way
            // since GetScriptLocation() is internal.
            //string location = Util.GetScriptLocation(Context);
            //string location = "/aspnet_client/advancedwebcontrols/";
			string location = Page.ResolveUrl("~/jscript/");

            string script = String.Format(TreeViewScript, new object [] {
                                              location, 
                                              TreeViewScriptFileName,
                                              TreeViewScriptVersion
                                          });

            Page.ClientScript.RegisterStartupScript(GetType (), TreeViewScriptKey, script);
        }

        protected override void RenderContents(HtmlTextWriter writer) {

            // If uplevel, output images to hidden span to ensure that IE downloads them
            if (RenderUplevel) {
                writer.WriteLine();
                writer.AddStyleAttribute("display", "none");
                writer.RenderBeginTag("span");

                System.Web.UI.WebControls.Image image = new System.Web.UI.WebControls.Image();

                // Output images array
                if ((images != null) && (images.Count > 0)) {
                    for (int i = 0; i < images.Count; i++) {
                        writer.WriteLine();
                        image.ImageUrl = images[i].Url;
                        image.RenderControl(writer);
                    }
                }

                // Output the three standard images (expanded, collapsed, node)
                writer.WriteLine();
                image.ImageUrl = renderContext.expandedNodeImageUrl;
                image.RenderControl(writer);

                writer.WriteLine();
                image.ImageUrl = renderContext.collapsedNodeImageUrl;
                image.RenderControl(writer);

                writer.WriteLine();
                image.ImageUrl = renderContext.nodeImageUrl;
                image.RenderControl(writer);

                writer.WriteLine();
                writer.RenderEndTag();
            }

            writer.WriteLine();
            writer.RenderBeginTag("dl");
            writer.Indent++;
            for (int i = 0; i < Nodes.Count; i++) {
                Nodes[i].RenderNode(writer, renderContext, RenderUplevel);
            }
            writer.Indent--;
            writer.RenderEndTag();
            writer.WriteLine();
        }

        private void SaveExpandedState(TreeNode node, StringBuilder expandedState) {
            expandedState.Append('[');
            for (int i = 0; i < node.Nodes.Count; i++) {
                TreeNode n = node.Nodes[i];

                // Record in the TreeNode the index of the expanded state
                n.expandedStateIndex = expandedState.Length;

                // Expandable nodes that have not been filled yet get marked as "n" so we know to skip them when we load the state
                if (n.Expandable && !n.HasNodes()) {
                    expandedState.Append('n');
                }
                else {
                    expandedState.Append(n.IsExpanded ? 'e' : 'c');
                }

                if (n.HasNodes())
                    SaveExpandedState(n, expandedState);
            }
            expandedState.Append(']');
        }

        /// <summary>
        /// </summary>
        protected override object SaveViewState() {

            object baseState = base.SaveViewState();
            object nodeStyleState = (nodeStyle != null) ? ((IStateManager)nodeStyle).SaveViewState() : null;
            object disabledNodeStyleState = (disabledNodeStyle != null) ? ((IStateManager)disabledNodeStyle).SaveViewState() : null;
            object treeState = ((IStateManager)Root).SaveViewState();
            object imagesState = (images != null) ? ((IStateManager)images).SaveViewState() : null;

            object[] myState = new object[5];
            myState[0] = baseState;
            myState[1] = nodeStyleState;
            myState[2] = disabledNodeStyleState;
            myState[3] = treeState;
            myState[4] = imagesState;

            return myState;
        }

        protected override void TrackViewState() {
            base.TrackViewState();

            if (nodeStyle != null)
                ((IStateManager)nodeStyle).TrackViewState();
            if (disabledNodeStyle != null)
                ((IStateManager)disabledNodeStyle).TrackViewState();
            ((IStateManager)Root).TrackViewState();
            if (images != null)
                ((IStateManager)images).TrackViewState();
        }

        private void UpdateChildParsedStatus(TreeNode n) {
            for (int i = 0; i < n.Nodes.Count; i++) {
                n.Nodes[i].SetParent(n);
                n.Nodes[i].SetTreeView(this);
                n.Nodes[i].parsedNode = true;
                UpdateChildParsedStatus(n.Nodes[i]);
            }
        }
    }
}

