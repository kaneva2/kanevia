///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////



namespace KlausEnt.KEP.Kaneva.WebControls {

    using System;
    using System.ComponentModel;
    using System.Drawing.Design;
    using System.Web.UI;


    /// <summary>
    ///   Represents a URL of an image.
    /// </summary>
    [
    TypeConverterAttribute(typeof(ExpandableObjectConverter))
    ]
    public class ImageUrl : IStateManager {

        private string url;
        private bool marked;
        internal bool dirty;


        /// <summary>
        /// </summary>
        public ImageUrl() {
        }

        /// <summary>
        ///   Creates a new ImageUrl object representing a URL of an image.
        /// </summary>
        public ImageUrl(string url) {
            this.url = url;
        }


        bool IStateManager.IsTrackingViewState {
            get {
                return marked;
            }
        }

        /// <summary>
        ///     The URL of an image.
        /// </summary>
        [
        DefaultValue(""),
        Editor("System.Web.UI.Design.ImageUrlEditor, System.Web.UI.Design.dll", typeof(UITypeEditor)),
        //Persistable(PersistableSupport.Declarative),
        AwcDescription("ImageUrl_Url")
        ]
        public string Url {
            get {
                if (url != null)
                    return url;
                return String.Empty;
            }
            set {
                url = value;
                if (((IStateManager)this).IsTrackingViewState)
                    dirty = true;
            }
        }


        /// <summary>
        ///  Return true if tracking state changes.
        ///  Method of private interface, IStateManager.
        /// </summary>
        public override bool Equals(object o) {
            if (!(o is ImageUrl))
                return false;
            if (this == o)
                return true;
            return (this.Url == ((ImageUrl)o).Url);
        }

        public override int GetHashCode() 
        {
            return base.GetHashCode();
        }

        /// <summary>
        ///  Load previously saved state.
        ///  Method of private interface, IStateManager.
        /// </summary>
        void IStateManager.LoadViewState(object state) 
        {
            if (state != null) {
                url = (string)state;
            }
        }

        /// <summary>
        ///  Start tracking state changes.
        ///  Method of private interface, IStateManager.
        /// </summary>
        void IStateManager.TrackViewState() {
            marked = true;
        }

        /// <summary>
        ///  Return object containing state changes.
        ///  Method of private interface, IStateManager.
        /// </summary>
        object IStateManager.SaveViewState() {
            if (dirty)
                return Url;
            return null;
        }
    }
}

