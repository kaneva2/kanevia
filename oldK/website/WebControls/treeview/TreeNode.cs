///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////



namespace KlausEnt.KEP.Kaneva.WebControls {

    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Drawing.Design;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;


    internal class TreeNodeBuilder : ControlBuilder {
        public override Type GetChildControlType(string tagName, IDictionary attribs) {
            // override to allow TreeNode without runat=server to be added
            if (tagName.ToLower().EndsWith("treenode"))
                return typeof(TreeNode);

            return null;
        }

        public override void AppendLiteralString(string s) {
            // override to ignore literals between items
        }
    }


    /// <summary>
    ///   Represents a node in a TreeView.
    /// </summary>
    [
    ControlBuilderAttribute(typeof(TreeNodeBuilder)),
    TypeConverterAttribute(typeof(ExpandableObjectConverter))
    ]
    public sealed class TreeNode : IStateManager {

        private static readonly char ID_SEPARATOR = '_';


        private TreeNodeCollection nodesCollection;
        private TreeView treeView;
        private TreeNode parent = null;
        private Style nodeStyle;
        private StateBag state;

        private bool marked;
        internal bool parsedNode;

        // Index of node in expandedstate hidden form field
        internal int expandedStateIndex;


        // Cache for the node ID
        private string nodeID = null;


        /// <summary>
        ///     Creates a TreeNode object.
        /// </summary>
        public TreeNode() {
        }

        /// <summary>
        ///     Creates a TreeNode object.
        /// </summary>
        /// <param name='text'>
        ///     Label of new node
        /// </param>
        public TreeNode(string text) {
            ((IStateManager)this).TrackViewState();
            this.Text = text;
        }

        /// <summary>
        ///     Creates a TreeNode object.
        /// </summary>
        /// <param name='text'>
        ///     Label of new node
        /// </param>
        /// <param name='navigateUrl'>
        ///     URL to navigate to on click.
        /// </param>
        public TreeNode(string text, string navigateUrl) {
            ((IStateManager)this).TrackViewState();
            this.Text = text;
            this.NavigateUrl = navigateUrl;
        }

        /// <summary>
        ///     Creates a TreeNode object.
        /// </summary>
        /// <param name='text'>
        ///     Label of new node
        /// </param>
        /// <param name='navigateUrl'>
        ///     URL to navigate to on click.
        /// </param>
        /// <param name='imageIndex'>
        ///     Index of image in TreeView.ImageList to use when node is not selected.
        /// </param>
        public TreeNode(string text, string navigateUrl, int imageIndex) {
            ((IStateManager)this).TrackViewState();
            this.Text = text;
            this.NavigateUrl = navigateUrl;
            this.ImageIndex = imageIndex;
        }

        /// <summary>
        ///     Creates a TreeNode object.
        /// </summary>
        /// <param name='text'>
        ///     Label of new node
        /// </param>
        /// <param name='navigateUrl'>
        ///     URL to navigate to on click.
        /// </param>
        /// <param name='imageIndex'>
        ///     Index of image in TreeView.ImageList to use when node is not selected.
        /// </param>
        /// <param name='expandedImageIndex'>
        ///     ExpandedIndex of image in TreeView.ImageList to use when node is not selected.
        /// </param>
        public TreeNode(string text, string navigateUrl, int imageIndex, int expandedImageIndex) {
            ((IStateManager)this).TrackViewState();
            this.Text = text;
            this.NavigateUrl = navigateUrl;
            this.ImageIndex = imageIndex;
            this.ExpandedImageIndex = expandedImageIndex;
        }


        /// <summary>
        ///   When a node is not enabled, it will not render as a HyperLink. When true, the DisabledNodeStyle attribute will be applied to the node.
        /// </summary>
        [
        AwcCategory("Behavior"),
        AwcDescription("TreeNode_Enabled"),
        Bindable(true),
        DefaultValue(true),
        PersistenceMode(PersistenceMode.Attribute),
        ]
        public bool Enabled {
            get {
                object b = ViewState["Enabled"];
                return((b == null) ? true : (bool)b);
            }
            set {
                ViewState["Enabled"] = value;
            }
        }

        /// <summary>
        ///   Indicates whether this node is expandable (even if it has no children). If this node has children, then this field is ignored.
        /// </summary>
        [
        AwcCategory("Behavior"),
        AwcDescription("TreeNode_Expandable"),
        Bindable(true),
        DefaultValue(false),
        PersistenceMode(PersistenceMode.Attribute),
        ]
        public bool Expandable {
            get {
                object b = ViewState["Expandable"];
                return((b == null) ? false : (bool)b);
            }
            set {
                ViewState["Expandable"] = value;
            }
        }

        /// <summary>
        ///   The index of an image in the TreeView's Images property to use when the node is expanded.
        ///   If this property is not set and the node is expanded, the image indicated in ImageIndex will be used.
        /// </summary>
        [
        AwcCategory("Appearance"),
        AwcDescription("TreeNode_ExpandedImageIndex"),
        Bindable(true),
        DefaultValue(-1),
        Editor(typeof(KlausEnt.KEP.Kaneva.WebControls.ImageIndexEditor), typeof(System.Drawing.Design.UITypeEditor)),
        PersistenceMode(PersistenceMode.Attribute),
        ]
        public int ExpandedImageIndex {
            get {
                object o = ViewState["ExpandedImageIndex"];
                return((o == null) ? -1 : (int)o);
            }
            set {
                ViewState["ExpandedImageIndex"] = value;
            }
        }

        /// <summary>
        ///   The index of an image in the TreeView's Images property to use when the node is not expanded.
        /// </summary>
        [
        AwcCategory("Appearance"),
        AwcDescription("TreeNode_ImageIndex"),
        Bindable(true),
        DefaultValue(-1),
        Editor(typeof(KlausEnt.KEP.Kaneva.WebControls.ImageIndexEditor), typeof(System.Drawing.Design.UITypeEditor)),
        PersistenceMode(PersistenceMode.Attribute),
        ]
        public int ImageIndex {
            get {
                object o = ViewState["ImageIndex"];
                return((o == null) ? -1 : (int)o);
            }
            set {
                ViewState["ImageIndex"] = value;
            }
        }

        /// <summary>
        ///   True when node is expanded.
        /// </summary>
        [
        AwcCategory("Appearance"),
        AwcDescription("TreeNode_IsExpanded"),
        Browsable(false),
        DefaultValue(false),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        ]
        public bool IsExpanded {
            get {
                object b = ViewState["IsExpanded"];
                return((b == null) ? false : (bool)b);
            }
        }

        private bool IsTrackingViewState {
            get {
                return marked;
            }
        }

        /// <summary>
        ///   The URL to navigate to when the user clicks on the node. Leaving this blank will cause the node to be rendered as plaintext.
        /// </summary>
        [
        AwcCategory("Navigation"),
        AwcDescription("TreeNode_NavigateUrl"),
        Bindable(true),
        DefaultValue(""),
        Editor("System.Web.UI.Design.UrlEditor, System.Web.UI.Design", typeof(UITypeEditor)),
        PersistenceMode(PersistenceMode.Attribute),
        ]
        public string NavigateUrl {
            get {
                string s = (string)ViewState["NavigateUrl"];
                return((s == null) ? String.Empty : s);
            }
            set {
                ViewState["NavigateUrl"] = value;
            }
        }

        /// <summary>
        ///   A unique ID for this node.
        /// </summary>
        [
        Browsable(false),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        ]
        public string NodeID {
            get {
                if (nodeID != null && nodeID.Length > 0)
                    return nodeID;

                nodeID = Parent.NodeID + ID_SEPARATOR + "node" + Parent.Nodes.IndexOf(this);
                return nodeID;
            }
        }

        /// <summary>
        ///     The collection of child nodes associated with this TreeNode.
        /// </summary>
        [
        AwcDescription("TreeNode_Nodes"),
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        ]
        public TreeNodeCollection Nodes {
            get {
                if (nodesCollection == null) {
                    nodesCollection = new TreeNodeCollection(this);

                    if (IsTrackingViewState)
                        ((IStateManager)nodesCollection).TrackViewState();
                }
                return nodesCollection;
            }
        }

        /// <summary>
        ///   The style the node. This is applied after the TreeView's NodeStyle is applied.
        /// </summary>
        [
        AwcCategory("Style"),
        AwcDescription("TreeNode_NodeStyle"),
        PersistenceMode(PersistenceMode.InnerProperty),
        ]
        public Style NodeStyle {
            get {
                if (nodeStyle == null) {
                    nodeStyle = new Style(ViewState);
                    if (IsTrackingViewState)
                        ((IStateManager)nodeStyle).TrackViewState();
                }
                return nodeStyle;
            }
        }

        /// <summary>
        ///   The parent TreeNode of this node. This value is null if the node is not part of a tree, or it is a root node.
        /// </summary>
        [
        Browsable(false),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        ]
        public TreeNode Parent {
            get {
                return parent;
            }
        }

        /// <summary>
        ///   The target frame for the HyperLink. This property overrides the TreeView's Target property when set.
        /// </summary>
        [
        AwcCategory("Navigation"),
        AwcDescription("TreeNode_Target"),
        Bindable(true),
        DefaultValue(""),
        PersistenceMode(PersistenceMode.Attribute),
        TypeConverter("System.Web.UI.WebControls.TargetConverter, System.Web")
        ]
        public string Target {
            get {
                string s = (string)ViewState["Target"];
                return((s == null) ? String.Empty : s);
            }
            set {
                ViewState["Target"] = value;
            }
        }

        /// <summary>
        ///   The text to display for the node.
        /// </summary>
        [
        AwcCategory("Appearance"),
        AwcDescription("TreeNode_Text"),
        Bindable(true),
        DefaultValue(""),
        PersistenceMode(PersistenceMode.Attribute),
        ]
        public string Text {
            get {
                string s = (string)ViewState["Text"];
                return((s == null) ? String.Empty : s);
            }
            set {
                ViewState["Text"] = value;
            }
        }

        /// <summary>
        ///   The tooltip to display when the mouse is hovering over the node.
        /// </summary>
        [
        AwcCategory("Appearance"),
        AwcDescription("TreeNode_ToolTip"),
        Bindable(true),
        DefaultValue(""),
        PersistenceMode(PersistenceMode.Attribute),
        ]
        public string ToolTip {
            get {
                string s = (string)ViewState["ToolTip"];
                return((s == null) ? String.Empty : s);
            }
            set {
                ViewState["ToolTip"] = value;
            }
        }

        /// <summary>
        ///   The TreeView to which this TreeNode belongs.
        /// </summary>
        [
        Browsable(false),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        ]
        public TreeView TreeView {
            get {
                if (treeView == null) {
                    if (Parent != null)
                        treeView = ((TreeNode)Parent).TreeView;
                }
                return treeView;
            }
        }

        /// <summary>
        ///    <para>
        ///       Dictionary of state information that allows developers to save and restore
        ///       the state of a control across multiple requests for the same page.
        ///    </para>
        /// </summary>
        /// <value>
        ///    <para>
        ///       The dictionary of state information.
        ///    </para>
        /// </value>
        [
        Browsable(false),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        ]
        internal StateBag ViewState {
            get {
                if (state == null) {   // create a StateBag on demand; TreeNode makes its case sensitive
                    state = new StateBag(true);
                    if (IsTrackingViewState)
                        ((IStateManager)state).TrackViewState();
                }
                return state;
            }
        }


        /// <summary>
        ///   Collapse children.
        /// </summary>
        /// <param name='recursive'>
        ///     When true, children will be collapsed recursively.
        /// </param>
        public void Collapse(bool recursive) {
            if (Expandable || HasNodes()) {
                ViewState["IsExpanded"] = false;
                TreeView.OnCollapse(new TreeViewEventArgs(this));

                if (recursive) {
                    int count = Nodes.Count;
                    for (int i = 0; i < count; i++)
                        Nodes[i].Collapse(true);
                }
            }
        }

        /// <summary>
        ///   Expand children.
        /// </summary>
        /// <param name='recursive'>
        ///     When true, children will be expanded recursively.
        /// </param>
        public void Expand(bool recursive) {
            if (Expandable || HasNodes()) {
                ViewState["IsExpanded"] = true;
                TreeView.OnExpand(new TreeViewEventArgs(this));

                if (recursive) {
                    int count = Nodes.Count;
                    for (int i = 0; i < count; i++)
                        Nodes[i].Expand(true);
                }
            }
        }

        /// <summary>
        ///   Finds a node in the tree based on its NodeID.
        /// </summary>
        /// <param name='id'>
        ///     The full ID of the node.
        /// </param>
        /// <param name='pathOffset'>
        ///     The offset in the ID from which we are searching.
        /// </param>
        internal TreeNode FindNode(string id, int pathOffset) {

            // Get location of next part of path
            int nextChildPathOffset = id.IndexOf(ID_SEPARATOR, pathOffset);

            int count = Nodes.Count;

            // Is it an immediate child of ours? If so, find it and return it
            if (nextChildPathOffset == -1) {
                for (int i = 0; i < count; i++) {
                    if (Nodes[i].NodeID == id)
                        return Nodes[i];
                }
                return null;
            }

            // Not an immediate child of ours, must find out which of our children to ask

            // Get the name of the child, and try to locate it
            string childID = id.Substring(pathOffset, nextChildPathOffset - pathOffset);

            TreeNode child = null;

            for (int i = 0; i < count; i++) {
                string nodeID = Nodes[i].NodeID;
                if (nodeID.Substring(nodeID.LastIndexOf(ID_SEPARATOR) + 1) == childID)
                    child = Nodes[i];
            }

            // Child doesn't exist: fail
            if (child == null) {
                return null;
            }

            return child.FindNode(id, nextChildPathOffset + 1);
        }

        /// <summary>
        ///   Returns true when this node has children.
        /// </summary>
        public bool HasNodes() {
            return nodesCollection != null && nodesCollection.Count > 0;
        }

        /// <summary>
        ///   Renders this node.
        /// </summary>
        internal void RenderNode(HtmlTextWriter writer, TreeViewRenderContext renderContext, bool renderUplevel) {
            // Hide (using styles), but render a node that is collapsed
            if (renderUplevel && !this.IsExpanded) {
                writer.AddStyleAttribute("display", "none");
            }

            writer.RenderBeginTag("dt");
            writer.WriteLine();
            writer.Indent++;


            // Clear out old style
            renderContext.hyperLink.ControlStyle.Reset();

            // Clear out old properties
            renderContext.hyperLink.Text = String.Empty;
            renderContext.hyperLink.ImageUrl = String.Empty;
            renderContext.hyperLink.Target = String.Empty;
            renderContext.hyperLink.NavigateUrl = String.Empty;

            bool hasNodes = HasNodes();

            // Render proper node icon (plus/minus/node)
            if (hasNodes || Expandable) {
                if (IsExpanded)
                    renderContext.hyperLink.ImageUrl = renderContext.collapsedNodeImageUrl;
                else
					renderContext.hyperLink.ImageUrl = renderContext.expandedNodeImageUrl;

                // If we are rendering up-level and already have all our nodes then use JavaScript + DHTML
                if (renderUplevel && hasNodes) {
                    // Create Expand/Collapse script (only if TreeNode is enabled)
                    if (Enabled && TreeView.Enabled) {
                        string scriptCode = "TreeViewShowHide(" + renderContext.treeViewID + ", this, " + NodeID + ", " + renderContext.expandedStateField + ")";
                        writer.AddAttribute("onClick", scriptCode);

                        // Get URLs for images
                        string imageUrl = String.Empty;
                        string expandedImageUrl = String.Empty;
                        if ((ImageIndex >= 0) && (ImageIndex < renderContext.imageUrls.Count)) {
                            imageUrl = renderContext.imageUrls[ImageIndex].Url;

                            if ((ExpandedImageIndex >= 0) && (ExpandedImageIndex < renderContext.imageUrls.Count))
                                expandedImageUrl = renderContext.imageUrls[ExpandedImageIndex].Url;
                            else
                                expandedImageUrl = renderContext.imageUrls[ImageIndex].Url;
                        }
                        writer.AddAttribute("imageUrl", imageUrl);
                        writer.AddAttribute("expandedImageUrl", expandedImageUrl);
                        writer.AddAttribute("expandedStateIndex", expandedStateIndex.ToString());

                        renderContext.hyperLink.NavigateUrl = "javascript:void(0)";
                        renderContext.hyperLink.RenderControl(writer);
                    }
                    else {
                        renderContext.image.ImageUrl = renderContext.hyperLink.ImageUrl;
                        renderContext.image.RenderControl(writer);
                    }
                }
                else {
                    // If we are rendering down-level, or we are up-level but are not populated yet, do a postback
                    // We also have to check for design-time rendering
                    if (renderContext.designMode) 
                    {
                        renderContext.image.ImageUrl = renderContext.collapsedNodeImageUrl;
                        renderContext.image.RenderControl(writer);
                    }
                    else {
                        // Create PostBack script
                        string scriptCode = "javascript:";
                        char eventSpecifier;
                        if (IsExpanded)
                            eventSpecifier = TreeView.EVENTSPECIFIER_COLLAPSE;
                        else
                            eventSpecifier = TreeView.EVENTSPECIFIER_EXPAND;

                        scriptCode += TreeView.Page.ClientScript.GetPostBackEventReference(TreeView, eventSpecifier + NodeID);

                        if (renderUplevel) 
                        {
                            writer.AddAttribute("expandedStateIndex", expandedStateIndex.ToString());
                        }
                        if (Enabled && TreeView.Enabled)
                            renderContext.hyperLink.NavigateUrl = scriptCode;
                        renderContext.hyperLink.RenderControl(writer);
                    }
                }

                renderContext.hyperLink.ImageUrl = String.Empty;
            }
            else {
                renderContext.image.ImageUrl = renderContext.nodeImageUrl;
                renderContext.image.RenderControl(writer);
            }

            writer.Write("&nbsp;");

            // Render image from ImageList
            if ((ImageIndex >= 0) && (ImageIndex < renderContext.imageUrls.Count)) {
                if (IsExpanded && (ExpandedImageIndex >= 0) && (ExpandedImageIndex < renderContext.imageUrls.Count))
                    renderContext.image.ImageUrl = (string)renderContext.imageUrls[ExpandedImageIndex].Url;
                else
                    renderContext.image.ImageUrl = (string)renderContext.imageUrls[ImageIndex].Url;
                renderContext.image.RenderControl(writer);
                writer.Write("&nbsp;");
            }


            // Render actual node as either HyperLink or Label
            if (NavigateUrl != String.Empty && Enabled) {
                renderContext.hyperLink.Text = Text;
                renderContext.hyperLink.NavigateUrl = NavigateUrl;
                renderContext.hyperLink.Target = (Target == String.Empty) ? renderContext.target : Target;

                // UNDONE: Efficiently apply styles by checking for NULL
                // Apply TreeView NodeStyle, then TreeNode NodeStyle
                renderContext.hyperLink.ApplyStyle(TreeView.NodeStyle);
                renderContext.hyperLink.ApplyStyle(NodeStyle);

                renderContext.hyperLink.ToolTip = ToolTip;
                renderContext.hyperLink.RenderControl(writer);
            }
            else {
                // Clear out old style
                renderContext.label.ControlStyle.Reset();
                renderContext.label.Text = Text;

                // UNDONE: Efficiently apply styles by checking for NULL
                // Apply TreeView NodeStyle, then TreeNode NodeStyle, and when applicable, apply the disabled style
                renderContext.label.ApplyStyle(TreeView.NodeStyle);
                renderContext.label.ApplyStyle(NodeStyle);
                // If disabled, apply disabled style on top of regular style
                if (!Enabled)
                    renderContext.label.ApplyStyle(TreeView.DisabledNodeStyle);

                renderContext.label.ToolTip = ToolTip;
                renderContext.label.RenderControl(writer);
            }

            writer.Indent--;
            writer.WriteLine();
            writer.RenderEndTag();
            writer.WriteLine();

            // Render children if node is expanded or we are uplevel
            if (hasNodes && (IsExpanded || renderUplevel)) {
                // Hide (using styles), but render a node that is collapsed
                if (renderUplevel && !Parent.IsExpanded) {
                    writer.AddStyleAttribute("display", "none");
                }
                writer.RenderBeginTag("dd");
                writer.WriteLine();
                writer.Indent++;

                writer.AddAttribute("id", NodeID);
                writer.RenderBeginTag("dl");
                int count = Nodes.Count;
                for (int i = 0; i < count; i++) {

                    // Render the child node
                    Nodes[i].RenderNode(writer, renderContext, renderUplevel);
                }

                writer.RenderEndTag();
                writer.WriteLine();

                writer.Indent--;
                writer.RenderEndTag();
                writer.WriteLine();
            }

        }

        internal void SetNodeID(string nodeID) {
            this.nodeID = nodeID;
        }

        internal void SetParent(TreeNode parent) {
            this.parent = parent;
        }

        internal void SetTreeView(TreeView treeView) {
            this.treeView = treeView;
        }

        bool IStateManager.IsTrackingViewState {
            get {
                return IsTrackingViewState;
            }
        }

        /// <summary>
        ///  Load previously saved state.
        ///  Method of private interface, IStateManager.
        /// </summary>
        void IStateManager.LoadViewState(object savedState) {

            if (savedState != null) {
                object[] myState = (object[])savedState;

                if (myState[0] != null)
                    ((IStateManager)ViewState).LoadViewState(myState[0]);
                if (myState[1] != null)
                    ((IStateManager)Nodes).LoadViewState(myState[1]);
            }
        }

        /// <summary>
        ///  Return object containing state changes.
        ///  Method of private interface, IStateManager.
        /// </summary>
        object IStateManager.SaveViewState() {
            object stateState = (state != null) ? ((IStateManager)state).SaveViewState() : null;
            object nodesState = (nodesCollection != null) ? ((IStateManager)nodesCollection).SaveViewState() : null;

            if (stateState == null &&
                nodesState == null) {
                return null;
            }

            object[] myState = new object[2];

            myState[0] = stateState;
            myState[1] = nodesState;

            return myState;
        }

        /// <summary>
        ///  Start tracking state changes.
        ///  Method of private interface, IStateManager.
        /// </summary>
        void IStateManager.TrackViewState() {
            marked = true;

            if (state != null)
                ((IStateManager)state).TrackViewState();
            if (nodeStyle != null)
                ((IStateManager)nodeStyle).TrackViewState();
            if (nodesCollection != null)
                ((IStateManager)nodesCollection).TrackViewState();
        }
    }
}

