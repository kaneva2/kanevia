///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Resources;


namespace KlausEnt.KEP.Kaneva.WebControls {
   
	/// <summary>
	/// Displays an image link on the page.
	/// When the mouse is over it, the images changes to a different one.
	/// </summary>
	/// <remarks>
	/// This behaves exactly like a <see cref="System.Web.UI.WebControls.HyperLink"/> unless both <see cref="System.Web.UI.WebControls.HyperLink.ImageUrl"/> and <see cref="RollOverLink.RollOverImageUrl"/> are specified.
	/// </remarks>
	/// <example>
	/// The following example demonstrates how to create a image which does a roll-over.
	/// <code>
	/// <![CDATA[
	/// <html>
	///		<body>
	///		<form runat="server">
	///			<h3>RollOver Example</h3>
	///
	///			Put your mouse over the image.<br><br>
	///
	///			<kaneva:RollOverLink id="myLink"
	///				Text="google"
	///				ImageUrl="/normalImage.png"
	///				RollOverImageUrl="/activeImage.png"
	///				NavigateUrl="http://www.google.com"
	///				runat="server"/>
	///       
	///		</form>
	///		</body>
	/// </html>
	/// ]]>
	/// </code>
	/// </example>
	public class RollOverLink: HyperLink {

		/// <summary>
		/// Initializes a new instance of the <see cref="RollOverLink"/> class.
		/// </summary>
		/// <remarks>
		/// Use this constructor to create and initialize a new instance of the <see cref="RollOverLink"/> class.
		/// </remarks>
		public RollOverLink(): base() {
		}
	
		/// <summary>
		/// Gets or sets the path to an image to display for the <see cref="RollOverLink"/> control during a mouse-over.
		/// </summary>
		/// <value>
		/// The path to the image to display for the <see cref="RollOverLink"/> control. The default value is <see cref="String.Empty"/>.
		/// </value>
		/// <remarks>
		/// The <see cref="RollOverImageUrl"/> is only used if <see cref="HyperLink.ImageUrl"/> is also set.
		/// The image will be shown when the user puts the mouse arrow over the image.
		/// </remarks>
		[
			Bindable(true),
			Category("Appearance"),
			Editor(typeof(System.Web.UI.Design.ImageUrlEditor), typeof(System.Drawing.Design.UITypeEditor)),
			DefaultValue("")
		]
		public virtual String RollOverImageUrl {
			get { 
				object savedState;
                
				savedState = this.ViewState["RollOverImageUrl"];
				if (savedState != null) {
					return (String) savedState;
				}
				return "";
			}
			set {
				ViewState["RollOverImageUrl"] = value;
			}
		}

		/// <summary>
		/// Raises the PreRender event, which notifies the server control that is about to be rendered to the page.
		/// </summary>
		/// <remarks>Overridden to register client script with the <see cref="Page"/></remarks>
		/// <param name="e">An <see cref="EventArgs"/> object that contains the event data. </param>
		protected override void OnPreRender( EventArgs e ) {

			this.RegisterClientScript();
			base.OnPreRender(e);
		}

		/// <summary>
		/// Emits the client scripts which support the rollover behavior.
		/// </summary>
		protected virtual void RegisterClientScript() {
			if( RollOverImageUrl != String.Empty && this.ImageUrl != String.Empty ) {
				this.Attributes[ "OnMouseOver" ] = "RollOverLink_swapImage('"+ this.ClientID + "_img" +"','','"+ this.ResolveUrl( RollOverImageUrl ) +"',1);";
				this.Attributes[ "OnMouseOut" ]  = "RollOverLink_swapImgRestore();";

				#region Script Registration

                if (!this.Page.ClientScript.IsClientScriptBlockRegistered(GetType(), "RollOverLink_Library"))
                {
					ResourceManager manager = new ResourceManager( this.GetType() );
					String script = manager.GetResourceSet(System.Globalization.CultureInfo.CurrentCulture, true, true).GetString("LibraryScript");
                    this.Page.ClientScript.RegisterClientScriptBlock(GetType(), "RollOverLink_Library", script);
				}

                if (!Page.ClientScript.IsStartupScriptRegistered(GetType(), "RollOverLink_Init"))
                {
					ResourceManager manager = new ResourceManager( this.GetType() );
					String script = manager.GetResourceSet(System.Globalization.CultureInfo.CurrentCulture, true, true).GetString("InitScript");
                    this.Page.ClientScript.RegisterStartupScript(GetType(), "RollOverLink_Init", script);
				}
			
				Page.ClientScript.RegisterArrayDeclaration( "RollOverLink_Images", "'" + this.ResolveUrl( RollOverImageUrl ) + "'" );
				#endregion
			}
		}

		/// <summary>
		/// Renders the contents of the control into the specified writer. This method is primarily used by control developers.
		/// </summary>
		/// <param name="writer">The output stream that renders HTML content to the client.</param>
		/// <remarks>
		/// Overridden to allow script to access the inner image.
		/// </remarks>
		protected override void RenderContents( HtmlTextWriter writer ) {
			
	        if ( this.ImageUrl != String.Empty ) {
	        	System.Web.UI.WebControls.Image innerImage = new System.Web.UI.WebControls.Image();
	        	innerImage.ImageUrl = this.ResolveUrl( this.ImageUrl );
	        	innerImage.Attributes["name"] = this.ClientID + "_img";
	        	innerImage.ToolTip = this.ToolTip;
	        	innerImage.AlternateText = this.Text;
				innerImage.Height = this.Height;
				innerImage.Width = this.Width;
	        	innerImage.RenderControl( writer );
	        	return;
	        }
	        if (this.HasControls()) {
	        	base.RenderContents(writer);
	        	return;
	        }
	        writer.Write(this.Text);
			
		}
	}
}