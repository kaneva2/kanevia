///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjectTests
{
    public class UserDaoTests
    {
        private IUserDao m_userDao = DataAccess.UserDao;
        private const string TEST_SYSTEM_ID = "00096378-fc39-11e5-87e1-002219919c2a";
        private const string TEST_SYSTEM_CONFIG_ID = "de26e910-7b71-11e6-84f2-a3cf223dc93b";
        private const string TEST_NET_HASH = "24b077ca-b87a-4402-8a56-7459b81c1e0f";

        [Fact]
        public void InsertUserSystem_BasicInsert()
        {
            string systemId = Guid.NewGuid().ToString();
            Assert.True(m_userDao.InsertUserSystem(systemId));
        }

        [Fact]
        public void InsertUserSystemConfiguration_BasicInsert_NewSystem()
        {
            UserSystemConfiguration config = m_userDao.InsertUserSystemConfiguration(null, 1, 1, 1, 1, 1, 1, 1, 1, "", "", "", "", 
                "", "", "", Guid.NewGuid().ToString(), "", 1, "", "", 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "");
            Assert.NotNull(config);
            Assert.False(string.IsNullOrWhiteSpace(config.SystemConfigurationId));
            Assert.False(string.IsNullOrWhiteSpace(config.SystemId));
        }

        [Fact]
        public void InsertUserSystemConfiguration_BasicInsert_OldSystem()
        {
            UserSystemConfiguration config = m_userDao.InsertUserSystemConfiguration(TEST_SYSTEM_ID, 1, 1, 1, 1, 1, 1, 1, 1, "", "", "", "",
                "", "", "", Guid.NewGuid().ToString(), "", 1, "", "", 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "");
            Assert.NotNull(config);
            Assert.False(string.IsNullOrWhiteSpace(config.SystemConfigurationId));
            Assert.Equal(TEST_SYSTEM_ID, config.SystemId);
        }

        [Fact]
        public void InsertUserSystemConfiguration_BasicUpdate_OldSystem()
        {
            UserSystemConfiguration config = m_userDao.InsertUserSystemConfiguration(TEST_SYSTEM_ID, 1, 1, 1, 1, 1, 1, 1, 1, "", "", "", "",
                "", "", "", TEST_NET_HASH, "", 1, "", "", 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "");
            Assert.NotNull(config);
            Assert.Equal(TEST_SYSTEM_CONFIG_ID, config.SystemConfigurationId);
            Assert.Equal(TEST_SYSTEM_ID, config.SystemId);
        }
    }
}
