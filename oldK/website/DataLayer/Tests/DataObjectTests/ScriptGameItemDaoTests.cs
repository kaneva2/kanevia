///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjectTests
{
    public class ScriptGameItemDaoTests
    {
        private IUserDao m_userDao = DataAccess.UserDao;
        private IScriptGameItemDao m_scriptGameItemDao = DataAccess.ScriptGameItemDao;
        private ScriptGameItem m_testGameItem;

        private const string TEST_USERNAME = "kaneva";
        private const int TEST_USER_ID = 4230696;
        private const int TEST_PLAYER_ID = 842177;
        // Test zone/template info for the blank "Game World Template".
        private const int TEST_TEMPLATE_ID = 11;
        private const int TEST_TEMPLATE_GLID = 4190772; 
        private const int TEST_ZONE_INDEX = 1610612756;
        private const int TEST_ZONE_INSTANCE_ID = 1005734572;
        private const int TEST_ZONE_TYPE = 6;
        // Test game item global config values
        private const string TEST_GAME_ITEM_CONFIG = "TestConfig";
        private const string TEST_GAME_ITEM_CONFIG_VALUE = "TestConfigValue";

        #region Game Item Inserts

        [Fact]
        public void InsertSnapshotScriptGameItems_ReturnsFalseOnEmpty()
        {
            List<ScriptGameItem> sgiList = new List<ScriptGameItem>();
            bool result = m_scriptGameItemDao.InsertSnapshotScriptGameItems(ref sgiList, 0);
            Assert.Equal(false, result);
        }

        #endregion

        //public bool AddSnapshotScriptGameItemsToCustomDeed(IEnumerable<ScriptGameItem> sgItems, int templateGlobalId)
        //{
        //    throw new NotImplementedException();
        //}
        
        [Fact]
        public void GetInventoryScriptGameItems_BasicSelect()
        {
            PagedList<ScriptGameItem> sgitems = m_scriptGameItemDao.GetInventoryScriptGameItems(TEST_PLAYER_ID, "P", string.Empty, 
                new List<ScriptGameItemCategory>(), string.Empty, 1, 10);
            Assert.InRange(sgitems.Count, 1, 10);
        }

        [Fact]
        public void GetSnapshotScriptGameItem_SelectByGIGLID()
        {
            ScriptGameItem sgItem = m_scriptGameItemDao.GetSnapshotScriptGameItem(1000000);
            Assert.NotNull(sgItem);
            Assert.Equal(1000000, sgItem.GIGlid);
        }

        [Fact]
        public void GetSnapshotScriptGameItem_SelectByGIID()
        {
            ScriptGameItem sgItem = m_scriptGameItemDao.GetSnapshotScriptGameItem(TEST_ZONE_INSTANCE_ID, TEST_ZONE_TYPE, 1);
            Assert.NotNull(sgItem);
            Assert.Equal(1, sgItem.GIId);
        }

        [Theory]
        [InlineData("Pet Food")]
        [InlineData("Bogus Game Item Name")]
        public void GetSuggestedNameSuffixNum(string name)
        {
            string suffix = m_scriptGameItemDao.GetSuggestedNameSuffixNum(TEST_PLAYER_ID, "P", name, new List<ScriptGameItemCategory>());
            if (name.Equals("Bogus Game Item Name"))
            {
                Assert.Equal("Bogus Game Item Name", suffix);
            }
            else
            {
                Assert.Equal("Pet Food(1)", suffix);
            }
        }

        [Fact]
        public void GetTemplateScriptGameItems_BasicSelect()
        {
            PagedList<ScriptGameItem> sgitems = m_scriptGameItemDao.GetTemplateScriptGameItems(TEST_TEMPLATE_ID, string.Empty, null, string.Empty, 1, 10);
            Assert.InRange(sgitems.Count, 1, 10);
        }

        [Fact]
        public void GetDefaultScriptGameItems_BasicSelect()
        {
            PagedList<ScriptGameItem> sgitems = m_scriptGameItemDao.GetDefaultScriptGameItems(string.Empty, string.Empty, 1, 10);
            Assert.InRange(sgitems.Count, 1, 10);
        }

        [Fact]
        public void GetWorldScriptGameItems_BasicSelect()
        {
            PagedList<ScriptGameItem> sgitems = m_scriptGameItemDao.GetWorldScriptGameItems(TEST_ZONE_INSTANCE_ID, TEST_ZONE_TYPE, string.Empty, string.Empty, 1, 10);
            Assert.InRange(sgitems.Count, 1, 10);
        }

        [Fact]
        public void GetDeedScriptGameItems_BasicSelect()
        {
            PagedList<ScriptGameItem> sgitems = m_scriptGameItemDao.GetDeedScriptGameItems(TEST_TEMPLATE_GLID, string.Empty, null, string.Empty, 1, 10);
            Assert.InRange(sgitems.Count, 1, 10);
        }

        [Fact]
        public void GetSuggestedGIGLID()
        {
            int suggestedGIGLID = m_scriptGameItemDao.GetSuggestedGIGLID();
            Assert.InRange(suggestedGIGLID, ScriptGameItem.GIGLID_LOWER_LIMIT, ScriptGameItem.GIGLID_UPPER_LIMIT);
        }

        [Fact]
        public void GetSuggestedBundleGlid()
        {
            int suggestedGlid = m_scriptGameItemDao.GetSuggestedBundleGlid();
            Assert.InRange(suggestedGlid, ScriptGameItem.GI_BUNDLE_GLID_LOWER_LIMIT, ScriptGameItem.GI_BUNDLE_GLID_UPPER_LIMIT);
        }

        [Fact]
        public void GetSuggestedGIID()
        {
            int suggestedGiid = m_scriptGameItemDao.GetSuggestedGIID();
            Assert.True(suggestedGiid > 0);
        }

        [Fact]
        public void WorldHasScriptGameItems_BasicSelect()
        {
            bool worldHasGameItems = m_scriptGameItemDao.WorldHasScriptGameItems(TEST_ZONE_INDEX, TEST_ZONE_INSTANCE_ID);
            Assert.Equal(true, worldHasGameItems);
        }

        [Theory]
        [InlineData("ammo")]
        [InlineData("gembox")]
        public void IsTypeInventoryCompatible(string itemType)
        {
            bool compatible = m_scriptGameItemDao.IsTypeInventoryCompatible(itemType);

            if (itemType.Equals("ammo"))
            {
                Assert.Equal(true, compatible);
            }
            else
            {
                Assert.Equal(false, compatible);
            }
        }

        #region SGI Global Configs

        [Fact]
        public void GetSGIGlobalConfigValue_BasicSelect()
        {
            Dictionary<string, string> configs = m_scriptGameItemDao.GetSGIGlobalConfigValues();
            configs[TEST_GAME_ITEM_CONFIG] = TEST_GAME_ITEM_CONFIG_VALUE;
            m_scriptGameItemDao.UpdateSGIGlobalConfig(configs);
            string value = m_scriptGameItemDao.GetSGIGlobalConfigValue(TEST_GAME_ITEM_CONFIG);
            Assert.Equal(TEST_GAME_ITEM_CONFIG_VALUE, value);
        }

        [Fact]
        public void GetSGIGlobalConfigValues_BasicSelect()
        {
            Dictionary<string, string> configs = m_scriptGameItemDao.GetSGIGlobalConfigValues();
            Assert.NotNull(configs);
            Assert.True(configs.Count > 0);
        }

        [Fact]
        public void UpdateSGIGlobalConfig_BasicUpdate()
        {
            if (!string.IsNullOrWhiteSpace(m_scriptGameItemDao.GetSGIGlobalConfigValue(TEST_GAME_ITEM_CONFIG)))
            {
                m_scriptGameItemDao.DeleteSGIGlobalConfig(TEST_GAME_ITEM_CONFIG);
            }
            Dictionary<string, string> configs = m_scriptGameItemDao.GetSGIGlobalConfigValues();
            configs[TEST_GAME_ITEM_CONFIG] = TEST_GAME_ITEM_CONFIG_VALUE;
            bool success = m_scriptGameItemDao.UpdateSGIGlobalConfig(configs);
            Assert.Equal(true, success);
        }

        [Fact]
        public void DeleteSGIGlobalConfig_BasicDelete()
        {
            if (string.IsNullOrWhiteSpace(m_scriptGameItemDao.GetSGIGlobalConfigValue(TEST_GAME_ITEM_CONFIG)))
            {
                Dictionary<string, string> configs = m_scriptGameItemDao.GetSGIGlobalConfigValues();
                configs[TEST_GAME_ITEM_CONFIG] = TEST_GAME_ITEM_CONFIG_VALUE;
                m_scriptGameItemDao.UpdateSGIGlobalConfig(configs);
            }
            bool success = m_scriptGameItemDao.DeleteSGIGlobalConfig(TEST_GAME_ITEM_CONFIG);
            Assert.Equal(true, success);
        }

        #endregion

        //public bool UpdateSnapshotScriptGameItem(ScriptGameItem sgItem, int userId)
        //{
        //    throw new NotImplementedException();
        //}

        //public bool UpdateWorldGIGLIDs(int zoneInstanceId, int zoneType, List<ScriptGameItem> sgItems)
        //{
        //    throw new NotImplementedException();
        //}

        //public bool DeleteStartingScriptGameItem(ScriptGameItem sgItem, int templateId = 0)
        //{
        //    throw new NotImplementedException();
        //}

        //public bool DeleteAllSGItemsInCustomDeed(int templateGlobalId)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
