///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjectTests
{
    public class MetricsDaoTests
    {
        private IUserDao m_userDao = DataAccess.UserDao;
        private IMetricsDao m_metricsDao = DataAccess.MetricsDao;

        private const string KANEVA_USERNAME = "kaneva";
        private const string TEST_RUNTIME_ID = "test_runtime";
        private const string TEST_SYSTEM_ID = "test_system";
        private const string TEST_SYSTEM_NAME = "test_system_name";
        private const string TEST_APP_VERSION = "test_version";
        private const string TEST_IP_ADDRESS = "111.111.1.111";
        private const string TEST_WEBCALL_TYPE = "test_call";
        private const int TEST_USER_ID = 4230696;
        private const ulong TEST_RUNTIME_ID_LONG = 1111111111111111;

        #region Helpers

        /// <summary>
        /// For use in tests that require user_systems or user_system_configuration data for setup.
        /// Ensures that predictable records exist in both tables.
        /// </summary>
        private UserSystemConfiguration InitTestSystemConfiguration()
        {
            UserSystemConfiguration config = m_userDao.InsertUserSystemConfiguration(TEST_SYSTEM_ID, 0, 0, 0, 0, 0, 0, 0, 0, "1", "1",
                "1", TEST_SYSTEM_NAME, "1", "1", "1", "1", "1", 
                1, "TestVendor", "TestBrand", 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "");
            Assert.False(string.IsNullOrWhiteSpace(config.SystemConfigurationId), "Unable to confirm requisite user system configuration.");
            return config;
        }

        /// <summary>
        /// For use in tests that require client_runtime_performance_log data for setup.
        /// Ensures that predictable records exist in client_runtime_performance_log, user_systems, 
        /// and user_system_configurations.
        /// </summary>
        private string InitTestClientRuntimeId()
        {
            UserSystemConfiguration config = InitTestSystemConfiguration();
            string runtimeId = TEST_RUNTIME_ID;
            if (!m_metricsDao.ClientRuntimeExists(runtimeId))
            {
                m_metricsDao.InsertClientRuntimePerformanceLog(runtimeId, TEST_USER_ID, config.SystemConfigurationId,
                    TEST_APP_VERSION, null, null, null, null);
                Assert.True(m_metricsDao.ClientRuntimeExists(runtimeId), "Unable to confirm requisite client runtimeId.");
            }
            return runtimeId;
        }

        /// <summary>
        /// For use in tests that require launcher_runtime_performance_log data for setup.
        /// Ensures that predictable records exist in launcher_runtime_performance_log, user_systems, 
        /// and user_system_configurations.
        /// </summary>
        private ulong InitTestLauncherRuntimeId()
        {
            UserSystemConfiguration config = InitTestSystemConfiguration();
            ulong runtimeId = TEST_RUNTIME_ID_LONG;
            if (!m_metricsDao.LauncherRuntimeExists(runtimeId))
            {
                m_metricsDao.SaveLauncherRuntimePerformanceLog(runtimeId, "Running", config.SystemConfigurationId, TEST_APP_VERSION,
                    0, 0, 0, 0, 0, 0, 0);
                Assert.True(m_metricsDao.LauncherRuntimeExists(runtimeId), "Unable to confirm requisite launcher runtimeId.");
            }
            return runtimeId;
        }

        /// <summary>
        /// Generates the current unix timestamp. Useful for testing updates where you need to be sure
        /// that the record changes.
        /// </summary>
        public long UnixTimeNow()
        {
            var timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)timeSpan.TotalSeconds;
        }

        #endregion

        [Fact]
        public void MetricsPageTiming_BasicInsert()
        {
            int rowsInserted = m_metricsDao.InsertMetricsPageTiming("testpage", 1.0);
            Assert.Equal(1, rowsInserted);
        }

        #region Client Performance Metrics

        [Fact]
        public void ClientMtbfLog_BasicInsert()
        {
            string runtimeId = InitTestClientRuntimeId();
            int rowsInserted = m_metricsDao.InsertClientMtbfLog(TEST_USER_ID, 1.0, 1, 1, TEST_APP_VERSION, runtimeId);
            Assert.Equal(1, rowsInserted);
        }

        [Fact]
        public void ClientRuntimeSystemId_BasicSelect()
        {
            string runtimeId = InitTestClientRuntimeId();
            string systemId = m_metricsDao.GetClientRuntimeSystemId(runtimeId);
            Assert.False(string.IsNullOrWhiteSpace(systemId));
        }

        [Fact]
        public void ClientRuntimePerformanceLog_BasicInsert()
        {
            UserSystemConfiguration config = InitTestSystemConfiguration();
            string runtimeId = m_metricsDao.InsertClientRuntimePerformanceLog(null, TEST_USER_ID, config.SystemConfigurationId, TEST_APP_VERSION,
                null, null, null, null);
            Assert.False(string.IsNullOrWhiteSpace(runtimeId));
        }

        [Fact]
        public void ClientRuntimeIP_BasicInsert()
        {
            // This test needs a fresh client_runtime_performance_log record
            UserSystemConfiguration config = InitTestSystemConfiguration();
            string runtimeId = m_metricsDao.InsertClientRuntimePerformanceLog(null, TEST_USER_ID, config.SystemConfigurationId, TEST_APP_VERSION,
                null, null, null, null);
            Assert.False(string.IsNullOrWhiteSpace(runtimeId), "Unable to confirm requisite client runtimeId.");

            int recordsAffected = m_metricsDao.InsertClientRuntimeIP(runtimeId, TEST_IP_ADDRESS);
            Assert.Equal(1, recordsAffected);
        }

        [Fact]
        public void ClientRuntimePerformanceLog_BasicUpdate()
        {
            string runtimeId = InitTestClientRuntimeId();
            int recordsAffected = m_metricsDao.UpdateClientRuntimePerformanceLog(runtimeId, TEST_USER_ID, 0, 
                (ulong?)UnixTimeNow(), 0, 0, 0, 0, 0, 0, 0, 0, "Running", 0, null);
            Assert.Equal(1, recordsAffected);
        }

        [Fact]
        public void ClientRuntimePerformanceLogUserId_BasicUpdate()
        {
            string runtimeId = InitTestClientRuntimeId();
            m_metricsDao.UpdateClientRuntimePerformanceLogUserId(runtimeId, 1);
            int recordsAffected = m_metricsDao.UpdateClientRuntimePerformanceLogUserId(runtimeId, TEST_USER_ID);
            Assert.Equal(1, recordsAffected);
        }

        [Fact]
        public void ClientWebCallPerformanceLog_BasicInsert()
        {
            string runtimeId = InitTestClientRuntimeId();
            m_metricsDao.DeleteClientWebCallPerformanceLog(runtimeId, TEST_WEBCALL_TYPE);
            int recordsAffected = m_metricsDao.InsertClientWebCallPerformanceLog(runtimeId, TEST_WEBCALL_TYPE, 1, 1, 1, 1, 1);
            Assert.Equal(1, recordsAffected);
        }

        [Fact]
        public void ClientZonePerformanceLog_BasicInsert()
        {
            string runtimeId = InitTestClientRuntimeId();
            int recordsAffected = m_metricsDao.InsertClientZonePerformanceLog(runtimeId, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            Assert.Equal(1, recordsAffected);
        }

        #endregion

        #region Launcher Performance Metrics

        [Fact]
        public void LauncherMtbfLog_BasicInsert()
        {
            ulong runtimeId = InitTestLauncherRuntimeId();
            int rowsInserted = m_metricsDao.InsertLauncherMtbfLog(0, 0, 0, runtimeId);
            Assert.Equal(1, rowsInserted);
        }

        [Fact]
        public void LauncherRuntimePerformanceLog_BasicInsert()
        {
            UserSystemConfiguration config = InitTestSystemConfiguration();
            ulong newRuntimeId = m_metricsDao.SaveLauncherRuntimePerformanceLog(0, "Running", config.SystemConfigurationId, TEST_APP_VERSION,
                0, 0, 0, 0, 0, 0, 0);
            Assert.True(newRuntimeId > 0);
        }

        [Fact]
        public void LauncherRuntimePerformanceLog_BasicUpdate()
        {
            UserSystemConfiguration config = InitTestSystemConfiguration();
            ulong runtimeId = InitTestLauncherRuntimeId();
            ulong newRuntimeId = m_metricsDao.SaveLauncherRuntimePerformanceLog(runtimeId, "Running", config.SystemConfigurationId, TEST_APP_VERSION,
                0, UnixTimeNow(), 0, 0, 0, 0, 0);
            Assert.Equal(runtimeId, newRuntimeId);
        }

        [Fact]
        public void LauncherRuntimeIP_BasicInsert()
        {
            ulong runtimeId = InitTestLauncherRuntimeId();
            m_metricsDao.DeleteLauncherRuntimeIP(runtimeId);
            int rowsInserted = m_metricsDao.InsertLauncherRuntimeIP(runtimeId, TEST_IP_ADDRESS);
            Assert.Equal(1, rowsInserted);
        }

        [Fact]
        public void LauncherPatchPerformanceLog_BasicInsert()
        {
            ulong runtimeId = InitTestLauncherRuntimeId();
            m_metricsDao.DeleteLauncherPatchPerformanceLog(runtimeId);
            int rowsInserted = m_metricsDao.InsertLauncherPatchPerformanceLog(runtimeId, "test_patch_url", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            Assert.Equal(1, rowsInserted);
        }

        [Fact]
        public void LauncherWebCallPerformanceLog_BasicInsert()
        {
            ulong runtimeId = InitTestLauncherRuntimeId();
            m_metricsDao.DeleteLauncherWebCallPerformanceLog(runtimeId);
            int rowsInserted = m_metricsDao.InsertLauncherWebCallPerformanceLog(runtimeId, TEST_WEBCALL_TYPE, 0, 0, 0, 0, 0);
            Assert.Equal(1, rowsInserted);
        }

        #endregion

        #region Client Version History

        [Fact]
        public void ClientVersionHistory_BasicInsert()
        {
            m_metricsDao.DeleteClientVersionHistory(TEST_APP_VERSION);
            int rowsInserted = m_metricsDao.InsertClientVersionHistory(TEST_APP_VERSION, DateTime.Now, DateTime.Now, "Test");
            Assert.Equal(1, rowsInserted);
        }

        [Fact]
        public void ClientVersionHistory_BasicUpdate()
        {
            m_metricsDao.DeleteClientVersionHistory(TEST_APP_VERSION);
            m_metricsDao.InsertClientVersionHistory(TEST_APP_VERSION, DateTime.Now, DateTime.Now, "Test");
            int rowsUpdated = m_metricsDao.UpdateClientVersionHistory(TEST_APP_VERSION, DateTime.Now, DateTime.Now, "New Test Comment");
            Assert.Equal(1, rowsUpdated);
        }

        [Fact]
        public void ClientVersionHistory_BasicDelete()
        {
            m_metricsDao.InsertClientVersionHistory(TEST_APP_VERSION, DateTime.Now, DateTime.Now, "Test");
            int rowsDeleted = m_metricsDao.DeleteClientVersionHistory(TEST_APP_VERSION);
            Assert.Equal(1, rowsDeleted);
        }

        [Fact]
        public void ClientVersionHistory_BasicSelect()
        {
            m_metricsDao.DeleteClientVersionHistory(TEST_APP_VERSION);
            m_metricsDao.InsertClientVersionHistory(TEST_APP_VERSION, DateTime.Now, DateTime.Now, "Test");
            PagedDataTable dt = m_metricsDao.GetClientVersionHistory(1, 10);
            Assert.NotNull(dt);
            Assert.NotEmpty(dt.Rows);
        }

        #endregion

        #region Game Item Metrics

        [Fact]
        public void ScriptGameItemPlacementLog_BasicInsert()
        {
            int recordsInserted = m_metricsDao.InsertScriptGameItemPlacementLog(0, 0, KANEVA_USERNAME, "test", 0, 0, "test", "test");
            Assert.Equal(1, recordsInserted);
        }

        [Fact]
        public void ScriptGameItemLootingLog_BasicInsert()
        {
            int recordsInserted = m_metricsDao.InsertScriptGameItemLootingLog(0, 0, KANEVA_USERNAME, "test", 0, 0, "test", "test");
            Assert.Equal(1, recordsInserted);
        }

        [Fact]
        public void FrameworkActivationLog_BasicInsert()
        {
            int recordsInserted = m_metricsDao.InsertFrameworkActivationLog(0, 0, KANEVA_USERNAME, "Activate");
            Assert.Equal(1, recordsInserted);
        }

        #endregion

        [Fact]
        public void RecordNewUserFunnelProgression_BasicInsert()
        {
            m_metricsDao.ResetNewUserFunnelProgression(TEST_USER_ID);
            int recordsInserted = m_metricsDao.RecordNewUserFunnelProgression(TEST_USER_ID, "web_signup");
            Assert.Equal(1, recordsInserted);
        }

        [Fact]
        public void RecordNewUserFunnelProgression_BasicUpdate()
        {
            m_metricsDao.ResetNewUserFunnelProgression(TEST_USER_ID);
            m_metricsDao.RecordNewUserFunnelProgression(TEST_USER_ID, "web_signup");
            int recordsUpdated = m_metricsDao.RecordNewUserFunnelProgression(TEST_USER_ID, "kim_gamelogin");
            Assert.True(recordsUpdated > 0);
        }

        [Fact]
        public void RecordNewUserFunnelProgression_BasicDelete()
        {
            m_metricsDao.RecordNewUserFunnelProgression(TEST_USER_ID, "web_signup");
            int recordsDeleted = m_metricsDao.ResetNewUserFunnelProgression(TEST_USER_ID);
            Assert.Equal(1, recordsDeleted);
        }
    }
}
