///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System.IO;
using Sphinx.Client.Network;

namespace Sphinx.Client.IO
{
    /// <summary>
    /// Interface for binary formatters factory
    /// </summary>
    public interface IBinaryFormatterFactory
    {
		IBinaryReader CreateReader(IStreamAdapter stream);
		IBinaryWriter CreateWriter(IStreamAdapter stream);
    }
}