///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sphinx.Client.Network
{
	public interface IStreamAdapter
	{
		int OperationTimeout { get; set; }
		bool CanRead { get; }
		bool CanWrite { get; }

		int ReadBytes(byte[] buffer, int count);
		void WriteBytes(byte[] buffer, int count);

		void Flush();
	}
}
