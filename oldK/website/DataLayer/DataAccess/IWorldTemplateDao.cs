///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IWorldTemplateDao
    {
        //--Create--
        int InsertWorldTemplate(WorldTemplate template);

        //--Read--
        WorldTemplate GetWorldTemplate(int templateId);
        WorldTemplate GetDefaultHomeWorldTemplate();
        PagedList<WorldTemplate> GetWorldTemplates(int[] status, int[] passGroupId, DateTime? userSignupDate, bool excludeIncubatorHosted, int pageNumber, int pageSize, string orderBy);
        PagedList<WorldTemplate> GetWorldTemplates(List<int> templateIds, int pageNumber, int pageSize, string orderBy);
        PagedList<WorldTemplate> GetGamingWorldTemplates(int pageNumber, int pageSize, string orderBy);
        PagedList<WorldTemplate> SearchWorldTemplates(string searchString, int pageNumber, int pageSize, string orderBy);
        DataTable GetWorldTemplateDefaultItemIds(int templateId);
        PagedList<WOKItem> GetWorldTemplateDefaultItems(int templateId, int pageNumber, int pageSize);
        PagedList<WorldTemplate> GetWorldTemplatesByExperimentGroups(List<string> experimentGroupIds, string action);

        //--Update--
        int UpdateWorldTemplate(WorldTemplate template);

        //--Delete--
        int DeleteWorldTemplate(WorldTemplate template);

        //--Relationships
		int InsertWorldsToTemplate(int communityId, int templateId);
        int DeleteWorldsToTemplate(int communityId, int templateId);
        int InsertWorldTemplateDefaultItemIds(int templateId, int[] globalIds);
        int DeleteWorldTemplateDefaultItemIds(int templateId, int[] globalIds);

        int GetWorldTemplateId(int communityId);
	}
}
