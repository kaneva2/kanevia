///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface ISubscriptionDao
    {
        List<Subscription> GetUnpurchasedSubscriptionsList(int userID);
        List<Subscription> GetUnpurchasedSubscriptionsList(int userID, string filter);
        Subscription GetSubscription(uint SubscriptionId);
        Subscription GetSubscriptionByPromotionId(uint promotionId);
        int InsertSubscription(Subscription subscription);
        int InsertSubscription2Promotion(int promotion_id, int subscription_id);
        int DeleteAllSubscription2PromotionByPromoID(int promotion_id);
        int DeleteAllSubscription2PromotionBySubscptID(int subscription_id);
        int DeleteSubscription(int subscriptionId);
        void UpdateSubscription(Subscription subscription);
        DataTable GetSubscriptionTerms();
        DataTable GetSubscriptionStatus();
        DataTable GetAvailableSubscriptions();

        uint InsertUserSubscription(UserSubscription userSubscription);
        UserSubscription GetUserSubscription(uint userSubscriptionId);
        UserSubscription GetUserSubscription(uint SubscriptionId, uint userID);
        List<UserSubscription> GetUserSubscriptions(int userId);
        void UpdateUserSubscription (UserSubscription userSubscription);
        List<Subscription> GetSubscriptionsByPromotionId(uint promotionId);

        List<SubscriptionEntitlement> GetSubscriptionEntitlements(int subscriptionId, string gender);
        List<UserSubscription> GetActiveUserSubscriptions(int userId);
        bool HasValidSubscription(int userId, int subscriptionId);
        bool HasValidSubscriptionByPassType(uint userId, uint passGroupId);

        Subscription GetSubscriptionByUserSubscriptionId(uint userSubscriptionId);
        List<Subscription> GetSubscriptionsByPassGroupIds(string passGroupIds);
        UserSubscription GetUserSubscriptionByPromotionId(uint promotionID, uint userID);
        List<UserSubscription> GetUserSubscriptionsByPromotionIds(string promotionIDList, uint userID);
        List<Subscription> GetUserActiveSubscriptions (int userId);

        UserSubscription GetUserSubscriptionByPassGroupId(uint passGroupId, uint userID);
        DataTable GetUnpurchasedSubscriptions(int userID, string passGroupFilter);
        DataTable GetPassGroupsToSubscription();
        DataTable GetPassGroupsAssociatedWSubscription(uint subscriptionId);
        List<Subscription> GetAllSubscriptionsList();
        DataTable GetAllSubscriptions();
        int InsertSubscription2PassGroup(uint pass_group_id, uint subscription_id);
        int DeleteAllSubscription2PassGroupByPassGroupID(uint pass_group_id);
        int DeleteAllSubscription2PassGroupBySubscptID(uint subscription_id);

        int InsertUserSubscriptionOrder(uint usersubscriptionId, int orderId);

        DateTime GetNextEndDate(DateTime purchaseDate, DateTime currentEndDate, Subscription.SubscriptionTerm subscriptionTerm);
    }
}
