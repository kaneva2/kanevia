///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface ICommentsDao
    {
        //standard asset comments
        //Comment GetComment(int commentId);
        int InsertComment(Comment comment);
        void DeleteComment(Comment comment);
        int UpdateComment(Comment comment);

        Comment GetComment(int commentId);
        PagedList<Comment> GetChannelComments(int channelId, int pageNumber, int pageSize);
        IList<Comment> GetAssetComments(int assetId, int pageNumber, int pageSize);

        int GetNumPeopleForChannelComments(int channelId);
        int GetNumPeopleForAssetComments(int assetId);
        bool CommentsBOTCheck(int userId);

        //wok item reviews
        int InsertReview(Comment review);
        void DeleteReview(Comment review);
        int UpdateReview(Comment review);

        //game item reviews
        int InsertGameReview(Comment review);
        int DeleteGameReview(Comment review);
        int UpdateGameReview(Comment review);

        Comment GetReview(int reviewId);
        Comment GetGameReview(int reviewId);
        IList<Comment> GetWOKItemReviews(int assetId, int pageNumber, int pageSize);
        IList<Comment> GetGameReviews(int gameId, int pageNumber, int pageSize);
    }
}
