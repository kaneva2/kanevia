///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IConfigurationDao
    {
        IList<WebOption> GetWebOptions(string group, int pageNumber, int pageSize, bool bIncludeNullValues);
        IList<WebOptionTemplate> GetAvailableWebOptions(string group);
        IList<string> GetAvailableWebOptionGroups();

        string GetWebOptionValueByUserRole(string group, string key, int role);

        WebOption GetWebOption(string group, string key, int role);
        void SetWebOption(string group, string key, int role, string value, int origRole);

        void DeleteWebOptions();
        void DeleteWebOptions(string group);
        void DeleteWebOptions(string group, string key);
        void DeleteWebOption(string group, string key, int role);
    }
}
