///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLEventDao : IEventDao
    {

        /// <summary>
        /// InsertEvent
        /// </summary>
        public int InsertEvent (int communityId, int userId, string title, string details, string location, int zoneIndex, int zoneInstanceId, DateTime startTime, DateTime endTime, int typeId, int timeZoneId, int premium, int privacy, bool isAPEvent, string trackingRequestGUID)
        {
            string query = " INSERT INTO events " +
              " ( community_id, user_id, title, details, " +
              " location, zone_index, zone_instance_id, start_time," +
              " end_time, type_id, time_zone_id, premium, privacy, created_date, last_update, is_AP_event, tracking_request_GUID) " +
              " VALUES( @community_id, @user_id, @title, @details, " +
              " @location, @zone_index, @zone_instance_id, @start_time, " +
              " @end_time, @type_id, @time_zone_id, @premium, @privacy, NOW(), NOW(), @is_AP_event, @tracking_request_GUID) ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@community_id", communityId));
            parameters.Add (new MySqlParameter ("@user_id", userId));
            parameters.Add (new MySqlParameter ("@title", title));
            parameters.Add (new MySqlParameter ("@details", details));
            parameters.Add (new MySqlParameter ("@location", location));
            parameters.Add (new MySqlParameter ("@zone_index", zoneIndex));
            parameters.Add (new MySqlParameter ("@zone_instance_id", zoneInstanceId));
            parameters.Add (new MySqlParameter ("@start_time", startTime));
            parameters.Add (new MySqlParameter ("@end_time", endTime));
            parameters.Add (new MySqlParameter ("@type_id", typeId));
            parameters.Add (new MySqlParameter ("@time_zone_id", timeZoneId));
            parameters.Add (new MySqlParameter ("@premium", premium));
            parameters.Add (new MySqlParameter ("@privacy", privacy));
            parameters.Add (new MySqlParameter ("@is_AP_event", isAPEvent ? "Y" : "N"));
            parameters.Add(new MySqlParameter("@tracking_request_GUID", trackingRequestGUID));

            return Db.Master.ExecuteIdentityInsert (query, parameters, 0);
        }

        /// <summary>
        /// InsertEvent
        /// </summary>
        public int InsertEvent (Event evt, string trackingRequestGUID)
        {
            string query = " INSERT INTO events " +
              " ( community_id, user_id, title, details, " +
              " location, start_time," +
              " end_time, type_id, premium, privacy, created_date, last_update, is_AP_event, tracking_request_GUID, " +
              " invite_friends, recurring_status_id, recurring_parent_id, obj_placement_id)" +
              " VALUES( @community_id, @user_id, @title, @details, " +
              " @location, @start_time, " +
              " @end_time, @type_id, @premium, @privacy, NOW(), NOW(), @is_AP_event, @tracking_request_GUID, " +
              " @invite_friends, @recurring_status_id, @recurring_parent_id, @obj_placement_id )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@community_id", evt.CommunityId));
            parameters.Add (new MySqlParameter ("@user_id", evt.UserId));
            parameters.Add (new MySqlParameter ("@title", evt.Title));
            parameters.Add (new MySqlParameter ("@details", evt.Details));
            parameters.Add (new MySqlParameter ("@location", evt.Location));
            parameters.Add (new MySqlParameter ("@start_time", evt.StartTime));
            parameters.Add (new MySqlParameter ("@end_time", evt.EndTime));
            parameters.Add (new MySqlParameter ("@type_id", evt.TypeId));
            parameters.Add (new MySqlParameter ("@premium", evt.Priority));
            parameters.Add (new MySqlParameter ("@privacy", evt.IsPrivate ? 1 : 0));
            parameters.Add (new MySqlParameter ("@is_AP_event", evt.IsAP ? "Y" : "N"));
            parameters.Add (new MySqlParameter ("@tracking_request_GUID", trackingRequestGUID));
            parameters.Add (new MySqlParameter ("@invite_friends", evt.InviteFriends ? "Y" : "N"));
            parameters.Add (new MySqlParameter ("@recurring_status_id", evt.RecurringStatusId));
            parameters.Add (new MySqlParameter ("@recurring_parent_id", evt.RecurringEventParentId));
            parameters.Add (new MySqlParameter ("@obj_placement_id", evt.ObjPlacementId));

            return Db.Master.ExecuteIdentityInsert (query, parameters, 0);
        }

        /// <summary>
        /// InsertEvent
        /// </summary>
        public int UpdateEvent (Event evt)
        {
            string sqlString = " UPDATE events " +
                " SET community_id = @community_id, " +
                " title= @title, " +
                " details = @details, " +
                " location = @location, " +
                " start_time = @start_time, " +
                " end_time = @end_time, " +
                " type_id = @type_id, " +
                " privacy = @privacy, " +
                " premium = @premium, " +
                " last_update = NOW(), " +
                " is_AP_event = @is_AP_event, " +
                " recurring_status_id = @recurring_status_id, " +
                " invite_friends = @invite_friends " +
                " WHERE event_id = @id ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@id", evt.EventId));
            parameters.Add (new MySqlParameter ("@community_id", evt.CommunityId));
            parameters.Add (new MySqlParameter ("@title", evt.Title));
            parameters.Add (new MySqlParameter ("@details", evt.Details));
            parameters.Add (new MySqlParameter ("@location", evt.Location));
            parameters.Add (new MySqlParameter ("@start_time", evt.StartTime));
            parameters.Add (new MySqlParameter ("@end_time", evt.EndTime));
            parameters.Add (new MySqlParameter ("@type_id", evt.TypeId));
            parameters.Add (new MySqlParameter ("@privacy", evt.IsPrivate ? 1 : 0));
            parameters.Add (new MySqlParameter ("@premium", evt.Priority));
            parameters.Add (new MySqlParameter ("@is_AP_event", evt.IsAP ? "Y" : "N"));
            parameters.Add (new MySqlParameter ("@recurring_status_id", evt.RecurringStatusId));
            parameters.Add (new MySqlParameter ("@invite_friends", evt.InviteFriends ? "Y" : "N"));

            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// UpdateEvent
        /// </summary>
        public int UpdateEvent (int eventId, int communityId, string title, string details, string location,
          DateTime startTime, DateTime endTime, int typeId, int privacy, int premium, int timeZoneId, bool isAPEvent)
        {
            string sqlString = " UPDATE events " +
                " SET community_id = @community_id, " +
                " title= @title, " +
                " details = @details, " +
                " location = @location, " +
                " start_time = @start_time, " +
                " end_time = @end_time, " +
                " type_id = @type_id, " +
                " privacy = @privacy, " +
                " premium = @premium, " +
                " time_zone_id = @time_zone_id, " +
                " last_update = NOW(), " +
                " is_AP_event = @is_AP_event " +
                " WHERE event_id = @id ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@id", eventId));
            parameters.Add (new MySqlParameter ("@community_id", communityId));
            parameters.Add (new MySqlParameter ("@title", title));
            parameters.Add (new MySqlParameter ("@details", details));
            parameters.Add (new MySqlParameter ("@location", location));
            parameters.Add (new MySqlParameter ("@start_time", startTime));
            parameters.Add (new MySqlParameter ("@end_time", endTime));
            parameters.Add (new MySqlParameter ("@type_id", typeId));
            parameters.Add (new MySqlParameter ("@privacy", privacy));
            parameters.Add (new MySqlParameter ("@premium", premium));
            parameters.Add (new MySqlParameter ("@time_zone_id", timeZoneId));
            parameters.Add (new MySqlParameter ("@is_AP_event", isAPEvent ? "Y" : "N"));

            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// GetLocationDetail
        /// </summary>
        public LocationDetail GetLocationDetail (int zone_index, int zone_instance_id, int creator_id, int community_id)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            string strWhere = " ";

            if (!zone_index.Equals (0))
            {
                strWhere += " AND  zone_index = @zone_index ";
                parameters.Add (new MySqlParameter ("@zone_index", zone_index));
            }

            if (!zone_instance_id.Equals (0))
            {
                strWhere += " AND  zone_instance_id = @zone_instance_id ";
                parameters.Add (new MySqlParameter ("@zone_instance_id", zone_instance_id));
            }


            if (!creator_id.Equals (0))
            {
                strWhere += " AND  creator_id = @creator_id ";
                parameters.Add (new MySqlParameter ("@creator_id", creator_id));
            }

            if (!community_id.Equals (0))
            {
                strWhere += " AND  community_id = @community_id ";
                parameters.Add (new MySqlParameter ("@community_id", community_id));
            }




            string strSqlSelect = "SELECT name, community_id, zone_index, zone_instance_id " +
              " FROM wok.worlds_cache  " +
              " WHERE community_id > 0 AND game_id = 0 " +
              strWhere;

            DataRow dr = Db.Master.GetDataRow (strSqlSelect, parameters);

            if (dr == null)
            {
                return new LocationDetail ();
            }

            return new LocationDetail (dr["name"].ToString (), Convert.ToInt32 (dr["community_id"]), Convert.ToInt32 (dr["zone_index"]), Convert.ToInt32 (dr["zone_instance_id"]));
        }

        /// <summary>
        /// GetUserEvents
        /// </summary>
        public PagedDataTable GetUserCreatedEvents (int userId, int startTime, string orderby, int pageNumber, int pageSize)
        {
            string strSQL = "SELECT event_id, start_time, end_time, zone_index, zone_instance_id " +
                 " FROM events e " +
                 " WHERE e.user_id = @userId " +
                 " AND e.event_status_id = @eventStatusId";

            switch (startTime)
            {
                case (int) Event.eSTART_TIME_FILTER.PAST:
                    strSQL += " AND e.start_time < NOW()";
                    break;
                case (int) Event.eSTART_TIME_FILTER.UPCOMING:
                    strSQL += " AND e.start_time >= NOW() ";
                    break;
                case (int) Event.eSTART_TIME_FILTER.NOW:
                    strSQL += " AND e.end_time > NOW() " +
                        " AND e.start_time <= NOW() ";
                    break;
                case (int) Event.eSTART_TIME_FILTER.ALL:
                    break;
                default:
                    break;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@eventStatusId", (int)Event.eSTATUS_TYPE.ACTIVE));
            return Db.Slave2.GetPagedDataTable (strSQL, "", parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// GetEvent
        /// </summary>
        public Event GetEvent (int eventId)
        {
            string strSQL = "SELECT event_id, community_id, user_id, title, details, location, start_time, end_time, " +
                " premium, privacy, created_date, is_AP_event, tracking_request_GUID, event_status_id, " +
                " recurring_status_id, invite_friends, recurring_parent_id, obj_placement_id " +
                " FROM events e " +
                " WHERE event_id = @eventId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@eventId", eventId));
            DataRow dr = Db.Slave.GetDataRow (strSQL, parameters);

            if (dr == null)
            {
                return new Event ();
            }

            return new Event (Convert.ToInt32 (dr["event_id"]), Convert.ToInt32 (dr["community_id"]), Convert.ToInt32 (dr["user_id"]),
                dr["title"].ToString (), dr["details"].ToString (), dr["location"].ToString (), Convert.ToDateTime (dr["start_time"]),
                Convert.ToDateTime (dr["end_time"]), Convert.ToInt32 (dr["premium"]), Convert.ToBoolean (dr["privacy"]),
                Convert.ToDateTime(dr["created_date"]), dr["is_AP_event"].ToString().Equals("Y"),
                dr["tracking_request_GUID"].ToString (), Convert.ToInt32 (dr["event_status_id"]), Convert.ToInt32 (dr["recurring_status_id"]),
                dr["invite_friends"].ToString().Equals("Y"), Convert.ToInt32(dr["recurring_parent_id"]), Convert.ToInt32(dr["obj_placement_id"])); 
        }

        /// <summary>
        /// GetEventsByCommunity
        /// </summary>
        public PagedList<Event> GetWorldEvents (int communityId, int startTimeFilter, DateTime startTime, string orderBy, int pageNumber, int pageSize, int recurringEventParentId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strSQL = "SELECT event_id, community_id, user_id, title, details, location, start_time, end_time, " +
                " premium, privacy, created_date, is_AP_event, tracking_request_GUID, event_status_id, " +
                " recurring_status_id, invite_friends, recurring_parent_id " +
                " FROM events e " +
                " WHERE community_id = @communityId " +
                " AND event_status_id = @eventStatusId";

            switch (startTimeFilter)
            {
                case (int) Event.eSTART_TIME_FILTER.PAST:
                    strSQL += " AND e.start_time < NOW()";
                    break;
                case (int) Event.eSTART_TIME_FILTER.UPCOMING:
                    strSQL += " AND e.start_time >= NOW() ";
                    break;
                case (int) Event.eSTART_TIME_FILTER.NOW:
                    strSQL += " AND e.end_time > NOW() " +
                        " AND e.start_time <= NOW() ";
                    break;
                case (int) Event.eSTART_TIME_FILTER.TODAY:
                    strSQL += " AND DATE(e.start_time) = DATE(NOW()) ";
                        break;
                case(int) Event.eSTART_TIME_FILTER.RECURRING_PAST:      
                        strSQL += " AND ( " +
                            " ((DATE(e.start_time) = DATE(DATE_ADD(@startTime, INTERVAL -7 DAY)) " + // need only recurring
                            "   OR DATE(e.start_time) = DATE(DATE_ADD(@startTime, INTERVAL -14 DAY))) " +
                            "   AND recurring_status_id = " + (int) Event.eRECURRING_STATUS_TYPE.INVITES_SENT + ") " +
                            " OR " +
                            "   DATE(e.start_time) = DATE(@startTime)) ";
                        parameters.Add (new MySqlParameter ("@startTime", startTime));
                        break;
                case (int) Event.eSTART_TIME_FILTER.RECURRING_ALL:
                        // Will get events 7 and 14 days in the past and in the future as well as the current day
                        strSQL += " AND ( " +
                            " ((DATE(e.start_time) = DATE(DATE_ADD(@startTime, INTERVAL -7 DAY)) " + // need only recurring
                            "   OR DATE(e.start_time) = DATE(DATE_ADD(@startTime, INTERVAL -14 DAY))) " +
			                "   AND recurring_status_id = " + (int) Event.eRECURRING_STATUS_TYPE.INVITES_SENT + ") " +
		                    " OR " +
                            "   DATE(e.start_time) = DATE(@startTime) " + // need both recurring and non-recurring
		                    " OR " +
                            "   DATE(e.start_time) = DATE(DATE_ADD(@startTime, INTERVAL 7 DAY)) " +	// need both recurring and non-recurring
                            "   OR DATE(e.start_time) = DATE(DATE_ADD(@startTime, INTERVAL 14 DAY))) ";

                        parameters.Add (new MySqlParameter ("@startTime", startTime));
                        break;
                case (int) Event.eSTART_TIME_FILTER.ALL:      
                    break;
                default:
                    break;
            }

            if (recurringEventParentId > 0)
            {
                strSQL += " AND event_id <> @recurringEventParentId ";
                parameters.Add (new MySqlParameter ("@recurringEventParentId", recurringEventParentId));
            }

            parameters.Add (new MySqlParameter ("@communityId", communityId));
            parameters.Add (new MySqlParameter ("@eventStatusId", (int) Event.eSTATUS_TYPE.ACTIVE));
            PagedDataTable pdt = Db.Master.GetPagedDataTable (strSQL, orderBy, parameters, pageNumber, pageSize);

            PagedList<Event> list = new PagedList<Event> ();
            //assigns total count
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                Event e = new Event (Convert.ToInt32 (row["event_id"]), Convert.ToInt32 (row["community_id"]), Convert.ToInt32 (row["user_id"]),
                row["title"].ToString (), row["details"].ToString (), row["location"].ToString (), Convert.ToDateTime (row["start_time"]),
                Convert.ToDateTime (row["end_time"]), Convert.ToInt32 (row["premium"]), Convert.ToBoolean (row["privacy"]),
                Convert.ToDateTime(row["created_date"]), row["is_AP_event"].ToString().Equals("Y"),
                row["tracking_request_GUID"].ToString (), Convert.ToInt32 (row["event_status_id"]), Convert.ToInt32 (row["recurring_status_id"]),
                row["invite_friends"].ToString ().Equals ("Y"), Convert.ToInt32 (row["recurring_parent_id"]));

                list.Add (e);
            }

            return list;
        }

        /// <summary>
        /// GetEvents
        /// </summary>
        public PagedList<Event> GetEvents (string filter, string orderBy, int pageNumber, int pageSize)
        {
            string strSQL = "SELECT event_id, community_id, user_id, title, details, location, start_time, end_time, " +
                " premium, privacy, created_date, is_AP_event, tracking_request_GUID, event_status_id, " +
                " recurring_status_id, invite_friends, recurring_parent_id " +
                " FROM events e ";

            if (filter.Length > 0)
            {
                strSQL += " WHERE " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            PagedDataTable pdt = Db.Master.GetPagedDataTable (strSQL, orderBy, parameters, pageNumber, pageSize);

            PagedList<Event> list = new PagedList<Event> ();
            //assigns total count
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                Event e = new Event (Convert.ToInt32 (row["event_id"]), Convert.ToInt32 (row["community_id"]), Convert.ToInt32 (row["user_id"]),
                row["title"].ToString (), row["details"].ToString (), row["location"].ToString (), Convert.ToDateTime (row["start_time"]),
                Convert.ToDateTime (row["end_time"]), Convert.ToInt32 (row["premium"]), Convert.ToBoolean (row["privacy"]),
                Convert.ToDateTime (row["created_date"]), row["is_AP_event"].ToString ().Equals ("Y"),
                row["tracking_request_GUID"].ToString (), Convert.ToInt32 (row["event_status_id"]), Convert.ToInt32 (row["recurring_status_id"]),
                row["invite_friends"].ToString ().Equals ("Y"), Convert.ToInt32 (row["recurring_parent_id"]));

                list.Add (e);
            }

            return list;
        }

        public bool DoesCommunityHaveActiveEvent(int communityId)
        {
            string strSQL = " SELECT event_id " +
            " FROM kaneva.events e " +
            " WHERE e.end_time > NOW() AND e.start_time < NOW() " +
            " AND e.community_id = @communityId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            DataRow drResult = Db.Master.GetDataRow(strSQL, parameters);

            return (drResult != null);
        }

        /// <summary>
        /// GetEventsByTimestampOffset
        /// </summary>
        public PagedList<Event> GetEventsByTimestampOffset(DateTime currentTime, int minuteOffset, string orderBy, int pageNumber, int pageSize)
        {
            int highOffset = minuteOffset + 5;
            int lowOffset = minuteOffset - 5;

            string strSQL = "SELECT event_id, community_id, user_id, title, details, location, start_time, end_time, " +
                " premium, privacy, created_date, is_AP_event, tracking_request_GUID, e.event_status_id " +
                " FROM events e " +
                " WHERE e.start_time BETWEEN DATE_ADD(@currentTime, INTERVAL @lowoffset MINUTE) AND DATE_ADD(@currentTime, INTERVAL @highoffset MINUTE)";


            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@lowoffset", lowOffset));
            parameters.Add(new MySqlParameter("@highoffset", highOffset));
            parameters.Add(new MySqlParameter("@currentTime", currentTime));
            parameters.Add (new MySqlParameter ("@eventStatusId", (int) Event.eSTATUS_TYPE.ACTIVE));
            PagedDataTable pdt = Db.Master.GetPagedDataTable(strSQL, orderBy, parameters, pageNumber, pageSize);

            PagedList<Event> list = new PagedList<Event>();
            //assigns total count
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                Event e = new Event(Convert.ToInt32(row["event_id"]), Convert.ToInt32(row["community_id"]), Convert.ToInt32(row["user_id"]),
                row["title"].ToString(), row["details"].ToString(), row["location"].ToString(), Convert.ToDateTime(row["start_time"]),
                Convert.ToDateTime(row["end_time"]), Convert.ToInt32(row["premium"]), Convert.ToBoolean(row["privacy"]),
                Convert.ToDateTime(row["created_date"]), row["is_AP_event"].ToString().Equals("Y"),
                row["tracking_request_GUID"].ToString (), Convert.ToInt32 (row["event_status_id"]));

                list.Add(e);
            }

            return list;
        }

        /// <summary>
        /// GetEventsByTimestampOffset
        /// </summary>
        public PagedList<EventReminder> GetEventReminders(string orderBy, int pageNumber, int pageSize)
        {
            string strSQL = "SELECT er.reminder_id, er.reminder_time_offset, COALESCE(et.type_name, '') AS email_type " +
                " FROM event_reminders er " +
                " LEFT OUTER JOIN viral_email.email_type et ON er.email_type_id = et.email_type_id";


            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            PagedDataTable pdt = Db.Master.GetPagedDataTable(strSQL, orderBy, parameters, pageNumber, pageSize);

            PagedList<EventReminder> list = new PagedList<EventReminder>();
            //assigns total count
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                EventReminder er = new EventReminder(Convert.ToInt32(row["reminder_id"]), Convert.ToInt32(row["reminder_time_offset"]), Convert.ToString(row["email_type"]));

                list.Add(er);
            }

            return list;
        }

        /// <summary>
        /// LogEventReminderNotification
        /// </summary>
        public int LogEventReminderNotification(int eventId, int reminderId, DateTime sentDate)
        {
            string strSQL = "INSERT INTO event_reminder_notifications (event_id, reminder_id, reminder_sent_date) " +
                " VALUES(@eventId, @reminderId, @sentDate) ";


            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@eventId", eventId));
            parameters.Add(new MySqlParameter("@reminderId", reminderId));
            parameters.Add(new MySqlParameter("@sentDate", sentDate));

            return Db.Master.Insert(strSQL, parameters, 0);
        }

        public int InsertEventInvitees (int communityId, int userId, int eventId, int inviteStatusId, bool includeFriends)
        {
            string strSQL = "INSERT INTO event_invites " + 
                "(" +
                "SELECT " + eventId + ", u.user_id, " + inviteStatusId + ", NOW(), NOW(), NULL " +
	            "FROM community_members cm " + 
	            "INNER JOIN users u ON u.user_id = cm.user_id " +
                "WHERE community_id = @communityId " +
                "AND u.user_id <> @userId ";

            // Include user's friends
            if (includeFriends)
            {
                strSQL += ") UNION (" +
                    "SELECT " + eventId + ", u.user_id, " + inviteStatusId + ", NOW(), NOW(), NULL " +
                    "FROM friends f " +
                    "INNER JOIN users u ON f.friend_id = u.user_id " +
                    "WHERE f.user_id = @userId " +
                    "AND u.active = 'Y'";
            }
            strSQL += ")";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            parameters.Add (new MySqlParameter ("@userId", userId));

            return Db.Master.Insert (strSQL, parameters, 0);
        }

        public int InsertEventInvitee (int eventId, int userId, int eventStatusId)
        {
            string strSQL = "INSERT INTO event_invites " +
                "(event_id, user_id, invite_status_id, date_created, date_last_modified) " +
                "VALUES " +
                "(@eventId, @userId, @eventStatusId, NOW(), NOW())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@eventId", eventId));
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@eventStatusId", eventStatusId));

            return Db.Master.Insert (strSQL, parameters, 0);
        }

        public PagedList<EventInvitee> GetEventInviteesByStatus (int eventId, int inviteStatusId, bool includeEventOwner, string orderBy, int pageNumber, int pageSize)
        {
            string strSQL = "SELECT u.user_id, u.username, u.email, u.status_id, u.gender, ei.invite_status_id, " +
                "IFNULL(unp.notify_event_invites, 1) AS notify_event_invites, " +
                "com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path, ei.attended_event " +
                "FROM event_invites ei " +
                "INNER JOIN users u ON u.user_id = ei.user_id " +
                "INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
                "LEFT OUTER JOIN user_notification_preferences unp ON unp.user_id = u.user_id " +
                "WHERE ei.event_id = @eventId " +
                "AND ei.invite_status_id = @inviteStatusId";

            if (includeEventOwner)
            {
                strSQL += " UNION " +
                    "SELECT e.user_id, u.username, u.email, u.status_id, u.gender, 2 AS invite_status_id, " +
                    "IFNULL(unp.notify_event_invites, 1) AS notify_event_invites, " +
                    "com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, " +
                    "com.thumbnail_square_path, NULL " +
                    "FROM events e " +
                    "INNER JOIN users u ON u.user_id = e.user_id " +
                    "INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
                    "LEFT OUTER JOIN user_notification_preferences unp ON unp.user_id = u.user_id " +
                    "WHERE e.event_id = @eventId "; 
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@eventId", eventId));
            parameters.Add (new MySqlParameter ("@inviteStatusId", inviteStatusId));
            PagedDataTable pdt = Db.Master.GetPagedDataTable (strSQL, orderBy, parameters, pageNumber, pageSize);

            PagedList<EventInvitee> list = new PagedList<EventInvitee> ();
            //assigns total count
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                EventInvitee ei = new EventInvitee (Convert.ToInt32 (row["user_id"]), row["username"].ToString (),
                    row["email"].ToString (), Convert.ToInt32 (row["status_id"]), (EventInvitee.Invite_Status)Convert.ToInt32 (row["invite_status_id"]),
                    Convert.ToBoolean (row["notify_event_invites"]), row["gender"].ToString (), row["thumbnail_small_path"].ToString (),
                    row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (),
                    row["thumbnail_square_path"].ToString(), (row["attended_event"].ToString() == "Y"));

                list.Add (ei);
            }

            return list;
        }

        public PagedList<EventInvitee> GetAutomatedEventInvitees(string orderBy, int pageNumber, int pageSize)
        {
            string strSQL = "SELECT active_eligible.user_id, udata.username, udata.email, udata.status_id, udata.gender, 0 AS invite_status_id, " +
	                        "    IFNULL(unp.notify_event_invites, 1) AS notify_event_invites, com.thumbnail_small_path, com.thumbnail_medium_path, " +
	                        "    com.thumbnail_large_path, com.thumbnail_square_path, 'N' AS attended_event, COUNT(eit.event_id) AS invite_count " +
                            "FROM ( " +
	                        // KIM users
	                        "    SELECT DISTINCT u.user_id " +
	                        "    FROM users u " +
	                        "    INNER JOIN users_stats us ON u.user_id = us.user_id " +
	                        "    INNER JOIN im_users imu ON u.user_id = imu.user_id AND imu.online = 1 " +
                            "    INNER JOIN fame.onetime_history oth ON u.user_id = oth.user_id AND oth.packet_id = @packetId " +
	                        "    LEFT OUTER JOIN events_rewards ei ON u.user_id = ei.user_id " +
                            "    WHERE (ei.user_id IS NULL OR us.number_of_friends < @friendThreshold) " +
	                        // World users
	                        "    UNION " +
	                        "    SELECT DISTINCT u.user_id " +
	                        "    FROM users u " +
	                        "    INNER JOIN users_stats us ON u.user_id = us.user_id " +
	                        "    INNER JOIN developer.active_logins al ON u.user_id = al.user_id " +
                            "    INNER JOIN fame.onetime_history oth ON u.user_id = oth.user_id AND oth.packet_id = @packetId " +
                            "    LEFT OUTER JOIN events_rewards ei ON u.user_id = ei.user_id " +
                            "    WHERE (ei.user_id IS NULL OR us.number_of_friends < @friendThreshold) " +
	                        // WoK users
	                        "    UNION " +
	                        "    SELECT DISTINCT u.user_id " +
	                        "    FROM users u " +
	                        "    INNER JOIN users_stats us ON u.user_id = us.user_id " +
	                        "    INNER JOIN wok.player_zones pz ON u.wok_player_id = pz.player_id AND pz.server_id <> 0 " +
                            "    INNER JOIN fame.onetime_history oth ON u.user_id = oth.user_id AND oth.packet_id = @packetId " +
                            "    LEFT OUTER JOIN events_rewards ei ON u.user_id = ei.user_id " +
                            "    WHERE (ei.user_id IS NULL OR us.number_of_friends < @friendThreshold) " +
                            ") AS active_eligible " +
                            "INNER JOIN wok.game_users gu ON active_eligible.user_id = gu.kaneva_user_id " +
                            "   AND TIMESTAMPDIFF(DAY, DATE(gu.created_date), NOW()) < @numDaysToShowThreshold " +
                            "INNER JOIN users udata ON udata.user_id = active_eligible.user_id  " +
                            "INNER JOIN communities_personal com ON com.creator_id = active_eligible.user_id  " +
                            "LEFT OUTER JOIN user_notification_preferences unp ON unp.user_id = active_eligible.user_id " +
                            "LEFT OUTER JOIN event_invites eit ON active_eligible.user_id = eit.user_id  " +
	                        "    AND DATE(eit.date_created) = DATE(NOW()) " +
                            "GROUP BY active_eligible.user_id " +
                            "HAVING invite_count < @inviteDailyLimit";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@friendThreshold", 10));
            parameters.Add(new MySqlParameter("@packetId", (int)PacketId.WELCOME_KANEVA));
            parameters.Add(new MySqlParameter("@inviteDailyLimit", 3));
            parameters.Add(new MySqlParameter("@numDaysToShowThreshold", 4));
            PagedDataTable pdt = Db.Master.GetPagedDataTable(strSQL, orderBy, parameters, pageNumber, pageSize);

            PagedList<EventInvitee> list = new PagedList<EventInvitee>();
            //assigns total count
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                EventInvitee ei = new EventInvitee(Convert.ToInt32(row["user_id"]), row["username"].ToString(),
                    row["email"].ToString(), Convert.ToInt32(row["status_id"]), (EventInvitee.Invite_Status)Convert.ToInt32(row["invite_status_id"]),
                    Convert.ToBoolean(row["notify_event_invites"]), row["gender"].ToString(), row["thumbnail_small_path"].ToString(),
                    row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(),
                    row["thumbnail_square_path"].ToString(), (row["attended_event"].ToString() == "Y"));

                list.Add(ei);
            }

            return list;
        }

        public EventInvitee GetEventInvitee (int userId, int eventId)
        {
            string strSQL = "SELECT u.user_id, u.username, u.email, u.status_id, u.gender, ei.invite_status_id, " +
                "IFNULL(unp.notify_event_invites, 1) AS notify_event_invites, " +
                "com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path, ei.attended_event " +
                "FROM event_invites ei " +
                "INNER JOIN users u ON u.user_id = ei.user_id " +
                "INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
                "LEFT OUTER JOIN user_notification_preferences unp ON unp.user_id = u.user_id " +
                "WHERE ei.event_id = @eventId " +
                "AND ei.user_id = @userId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@eventId", eventId));
            parameters.Add (new MySqlParameter ("@userId", userId));
            DataRow row = Db.Slave.GetDataRow (strSQL, parameters);

            if (row == null)
            {
                return new EventInvitee ();
            }

            return new EventInvitee (Convert.ToInt32 (row["user_id"]), row["username"].ToString (),
                row["email"].ToString (), Convert.ToInt32 (row["status_id"]), (EventInvitee.Invite_Status)Convert.ToInt32 (row["invite_status_id"]),
                Convert.ToBoolean (row["notify_event_invites"]), row["gender"].ToString (), row["thumbnail_small_path"].ToString (),
                row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (),
                row["thumbnail_square_path"].ToString(), (row["attended_event"].ToString() == "Y"));
        }

        public int UpdateEventStatus (int eventId, int statusId)
        {
            // Delete the event invites
            string sqlString = "UPDATE events SET event_status_id = @statusId " +
                " WHERE event_id = @eventId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@eventId", eventId));
            parameters.Add (new MySqlParameter ("@statusId", statusId));
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int InsertEventAudit (int eventId, int userId, string desc)
        {
            string strSQL = "INSERT INTO events_audit " +
                "(event_audit_id, event_id, user_id,description, date_modified) " + 
                "VALUES (UUID(), @eventId, @userId, @desc, NOW())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@eventId", eventId));
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@desc", desc));

            return Db.Master.ExecuteIdentityInsert (strSQL, parameters, 0);
        }


        /// <summary>
        /// InsertComment
        /// </summary>
        public int InsertComment (int eventId, string eventBlastId, int userId, string comment)
        {
            string sqlString = "INSERT INTO event_blast_comments ( " +
                " event_blast_comment_id, event_id, event_blast_id, comment, user_id, created_date " +
                " ) VALUES (" +
                " UUID(), @eventId, @eventBlastId, @comment, @userId, NOW())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@eventId", eventId));
            parameters.Add (new MySqlParameter ("@comment", comment));
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@eventBlastId", eventBlastId));

            int rc = Db.Master.ExecuteNonQuery (sqlString, parameters);

            if (rc > 0)
            {
                UpdateCommentCount (eventId, eventBlastId, 1);
            }

            return rc;
        }

        /// <summary>
        /// InsertEventBlast
        /// </summary>
        public int InsertEventBlast (int eventId, int userId, string comment)
        {
            string sqlString = "INSERT INTO event_blasts ( " +
                " event_blast_id, event_id, user_id, created_date, comment " +
                " ) VALUES (" +
                " UUID(), @eventId, @userId, NOW(), @comment)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@eventId", eventId));
            parameters.Add (new MySqlParameter ("@comment", comment));
            parameters.Add (new MySqlParameter ("@userId", userId));
            
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// GetEventBlast
        /// </summary> 
        public EventBlast GetEventBlast (string eventBlastId)
        {
            string strQuery = "SELECT event_blast_id, eb.event_id, e.user_id AS event_owner_user_id, eb.comment, e.user_id, eb.created_date, " +
                " eb.num_comments, u.username, u.gender, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path,NOW() as curr_date, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture " +
                " FROM event_blasts eb " +
                " INNER JOIN events e ON e.event_id = eb.event_id " +
                " INNER JOIN users u ON u.user_id = eb.user_id " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
                " WHERE eb.event_blast_id = @eventBlastId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@eventBlastId", eventBlastId));

            DataRow row = Db.Slave.GetDataRow (strQuery, parameters);

            if (row == null)
            {
                return new EventBlast ();
            }

            return new EventBlast (Convert.ToInt32 (row["event_id"]), Convert.ToInt32 (row["event_owner_user_id"]),
                row["event_blast_id"].ToString(), Convert.ToDateTime (row["created_date"]),
                row["comment"].ToString (), Convert.ToInt32 (row["num_comments"]), Convert.ToInt32 (row["user_id"]), row["username"].ToString (),
                row["gender"].ToString (), row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (),
                row["thumbnail_large_path"].ToString (), row["thumbnail_square_path"].ToString (), Convert.ToDateTime (row["curr_date"]));
        }

        /// <summary>
        /// EventBlast
        /// </summary> 
        public PagedList<EventBlast> GetEventBlasts (int eventId, int pageNumber, int pageSize, string sortBy)
        {
            string strQuery = "SELECT event_blast_id, eb.event_id, e.user_id AS event_owner_user_id, eb.comment, e.user_id, eb.created_date, " +
                " eb.num_comments, u.username, u.gender, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path, NOW() as curr_date, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture " +
                " FROM event_blasts eb " +
                " INNER JOIN events e ON e.event_id = eb.event_id " +
                " INNER JOIN users u ON u.user_id = eb.user_id " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
                " WHERE eb.event_id = @eventId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@eventId", eventId));
           
            PagedDataTable pdt = Db.Master.GetPagedDataTable (strQuery, sortBy, parameters, pageNumber, pageSize);

            PagedList<EventBlast> list = new PagedList<EventBlast> ();
            list.TotalCount = pdt.TotalCount;

            // reverse the order so the comments will display in correct order
            for (int i = pdt.Rows.Count - 1; i >= 0; i--)
            {
                EventBlast eb = new EventBlast (Convert.ToInt32 (pdt.Rows[i]["event_id"]), Convert.ToInt32 (pdt.Rows[i]["event_owner_user_id"]),
                    pdt.Rows[i]["event_blast_id"].ToString(), Convert.ToDateTime (pdt.Rows[i]["created_date"]),
                    pdt.Rows[i]["comment"].ToString (), Convert.ToInt32 (pdt.Rows[i]["num_comments"]), Convert.ToInt32 (pdt.Rows[i]["user_id"]), pdt.Rows[i]["username"].ToString (),
                    pdt.Rows[i]["gender"].ToString (), pdt.Rows[i]["thumbnail_small_path"].ToString (), pdt.Rows[i]["thumbnail_medium_path"].ToString (),
                    pdt.Rows[i]["thumbnail_large_path"].ToString (), pdt.Rows[i]["thumbnail_square_path"].ToString (), Convert.ToDateTime (pdt.Rows[i]["curr_date"]));

                eb.Creator.FacebookSettings.FacebookUserId = Convert.ToUInt64 (pdt.Rows[i]["fb_user_id"]);
                eb.Creator.FacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (pdt.Rows[i]["use_facebook_profile_picture"]);

                list.Add (eb);
            }
            return list;
        }

        /// <summary>
        /// GetEventComments
        /// </summary> 
        public PagedList<EventComment> GetEventComments (int eventId, string eventBlastId, int pageNumber, int pageSize, string sortBy, string groupBy)
        {
            string strQuery = "SELECT event_blast_comment_id, ebc.event_id, e.user_id AS event_owner_user_id, event_blast_id, comment, ebc.user_id, ebc.created_date, " +
                " u.username, u.gender, u.status_id, u.email, COALESCE(unp.notify_event_invites,0) AS notify_event_invites, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path, NOW() as curr_date, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture " +
                " FROM event_blast_comments ebc " +
                " INNER JOIN events e ON e.event_id = ebc.event_id " +
                " INNER JOIN users u ON u.user_id = ebc.user_id " +
                " LEFT OUTER JOIN user_notification_preferences unp ON unp.user_id = u.user_id " + 
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
                " WHERE ebc.event_id = @eventId " +
                " AND event_blast_id = @eventBlastId";

            if (groupBy != "")
            {
                strQuery += " GROUP BY " + groupBy;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@eventId", eventId));
            parameters.Add (new MySqlParameter ("@eventBlastId", eventBlastId));

            PagedDataTable pdt = Db.Master.GetPagedDataTable (strQuery, sortBy, parameters, pageNumber, pageSize);

            PagedList<EventComment> list = new PagedList<EventComment> ();
            list.TotalCount = pdt.TotalCount;

            // reverse the order so the comments will display in correct order
            for (int i = pdt.Rows.Count - 1; i >= 0; i--)
            {
                EventComment ec = new EventComment (pdt.Rows[i]["event_blast_comment_id"].ToString(), Convert.ToInt32 (pdt.Rows[i]["event_id"]), Convert.ToInt32 (pdt.Rows[i]["event_owner_user_id"]),
                    pdt.Rows[i]["event_blast_id"].ToString(), Convert.ToDateTime (pdt.Rows[i]["created_date"]),
                    pdt.Rows[i]["comment"].ToString (), Convert.ToInt32 (pdt.Rows[i]["user_id"]), pdt.Rows[i]["username"].ToString (),
                    pdt.Rows[i]["gender"].ToString (), pdt.Rows[i]["thumbnail_small_path"].ToString (), pdt.Rows[i]["thumbnail_medium_path"].ToString (),
                    pdt.Rows[i]["thumbnail_large_path"].ToString (), pdt.Rows[i]["thumbnail_square_path"].ToString (), Convert.ToDateTime (pdt.Rows[i]["curr_date"]));

                ec.Creator.FacebookSettings.FacebookUserId = Convert.ToUInt64 (pdt.Rows[i]["fb_user_id"]);
                ec.Creator.FacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (pdt.Rows[i]["use_facebook_profile_picture"]);
                ec.Creator.EmailNotifications = Convert.ToBoolean (pdt.Rows[i]["notify_event_invites"]);
                ec.Creator.UserStatusId = Convert.ToInt32 (pdt.Rows[i]["status_id"]);
                ec.Creator.Email = pdt.Rows[i]["email"].ToString ();

                list.Add (ec);
            }
            return list;
        }

        /// <summary>
        /// DeleteEventBlast
        /// </summary> 
        public int DeleteEventBlast (int eventId, string eventBlastId)
        {
            string sqlString = "DELETE FROM event_blasts " +
                " WHERE event_blast_id = @eventBlastId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@eventBlastId", eventBlastId));
            parameters.Add (new MySqlParameter ("@eventId", eventId));

            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// DeleteComment
        /// </summary> 
        public int DeleteEventComment (int eventId, string eventBlastId, string eventBlastCommentId)
        {
            string sqlString = "DELETE FROM event_blast_comments " +
                " WHERE event_blast_id = @eventBlastId AND event_blast_comment_id = @eventBlastCommentId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@eventBlastId", eventBlastId));
            parameters.Add (new MySqlParameter ("@eventBlastCommentId", eventBlastCommentId));

            int rc = Db.Master.ExecuteNonQuery (sqlString, parameters);

            if (rc > 0)
            {
                UpdateCommentCount (eventId, eventBlastId, -1);
            }

            return rc;
        }

        private int UpdateCommentCount (int eventId, string eventBlastId, int amount)
        {
            string sqlString = "UPDATE event_blasts SET num_comments = num_comments + " + amount.ToString () +
                " WHERE event_blast_id = @eventBlastId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@eventBlastId", eventBlastId));

            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int UpdateEventInviteStatus (int eventId, int inviteeUserId, int inviteStatus)
        {
            string sqlString = "UPDATE event_invites " +
                "SET invite_status_id = @inviteStatus " +
                "WHERE event_id = @eventId " +
                "AND user_id = @inviteeUserId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@eventId", eventId));
            parameters.Add (new MySqlParameter ("@inviteeUserId", inviteeUserId));
            parameters.Add (new MySqlParameter ("@inviteStatus", inviteStatus));

            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int UpdateEventInviteeAttendance(int eventId, int inviteeUserId, int inviteStatusId)
        {
            string sqlString = "INSERT INTO event_invites (event_id, user_id, invite_status_id, date_created, date_last_modified, attended_event) " +
                "VALUES (@eventId, @inviteeUserId, @inviteStatusId, NOW(), NOW(), 'Y') " +
                "ON DUPLICATE KEY UPDATE attended_event = 'Y' ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@eventId", eventId));
            parameters.Add(new MySqlParameter("@inviteeUserId", inviteeUserId));
            parameters.Add(new MySqlParameter("@inviteStatusId", inviteStatusId));

            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        public bool IsUserAlreadyRewardedToday(int userId)
        {
            string sqlString = " Select * from events_rewards " +
                " WHERE user_id = @userId " +
                " AND DATE(reward_date) = DATE(NOW()) ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            DataRow drResult = Db.Master.GetDataRow(sqlString, parameters);

            return (drResult != null);
        }

        public int InsertEventReward(int userId, int eventId, double rewardAmount)
        {
            string sqlString = "INSERT INTO events_rewards ( " +
              " event_id, user_id, reward_date, reward_amount " +
              " ) VALUES (" +
              " @eventId, @userId, NOW(), @rewardAmount)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@eventId", eventId));
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@rewardAmount", rewardAmount));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// GetEventInviteCounts
        /// </summary>
        public EventInviteCounts GetEventInviteCounts (int eventId)
        {
            string strSQL = "SELECT event_id, " + 
                "COUNT(CASE WHEN invite_status_id = 1 THEN 1 END) AS invited, " +
                "COUNT(CASE WHEN invite_status_id = 2 THEN 1 END) AS accepted, " +
                "COUNT(CASE WHEN invite_status_id = 3 THEN 1 END) AS declined " + 
                "FROM kaneva.event_invites " + 
                "WHERE event_id=@eventId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@eventId", eventId));
            DataRow row = Db.Slave.GetDataRow (strSQL, parameters);

            if (row == null)
            {
                return new EventInviteCounts ();
            }

            return new EventInviteCounts (eventId, Convert.ToInt32 (row["invited"]),
                Convert.ToInt32 (row["accepted"]), Convert.ToInt32 (row["declined"]));
        }
    
        public DataTable GetEventsUserAcceptedByStartTimeOffset(int userId, string startTimeOffset)
        {
            string strSelect = "SELECT e.event_id, e.user_id, e.community_id, e.title, e.title as Name, e.location, " +
                " e.start_time, e.end_time, e.premium, ei.invite_status_id, e.tracking_request_GUID, " +
                  " c.thumbnail_small_path, COALESCE(wc.game_id,wc.game_id,0) as game_id, COALESCE(wc.population,wc.population,0) as population, " +
                  " IF(end_time > NOW() AND start_time < NOW(),'Y', 'N') as happening_now, IF(premium in (1,2),'Y', 'N') as is_premium, " + 
                  " IF(wc.game_id IS NULL, wok.concat_vw_url(environment.getWOKGameId(),e.location,6), wok.concat_vw_url(e.location,'',0)) AS STPURL " +
                " FROM events e " +
                " INNER JOIN event_invites ei ON ei.event_id = e.event_id " +
                " INNER JOIN communities c ON c.community_id = e.community_id " +
                " LEFT JOIN wok.worlds_cache wc ON wc.community_id = e.community_id " +
                " WHERE ei.user_id = @userId " +
                  " AND e.user_id <> @userId " +
                  " AND e.event_status_id = " + (int)Event.eSTATUS_TYPE.ACTIVE +
                  " AND DATE_ADD(NOW(), INTERVAL " + startTimeOffset + ") >= e.start_time " +
                  " AND e.end_time > NOW() " +
                  " AND ei.invite_status_id = " + (int)EventInvitee.Invite_Status.ACCEPTED;

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
        //    parameters.Add(new MySqlParameter("@startTimeOffset", startTimeOffset));

            return Db.Master.GetDataTable(strSelect, parameters);
        }

        /// <summary>
        /// GetWorldEventsWithEventFlags
        /// </summary>
        public List<Event> GetWorldEventsWithEventFlags(int communityId, string orderBy)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string strSQL = "SELECT event_id, community_id, user_id, title, details, location, start_time, end_time, " +
                " premium, privacy, e.created_date, is_AP_event, tracking_request_GUID, event_status_id, " +
                " recurring_status_id, invite_friends, recurring_parent_id, e.obj_placement_id " +
                " FROM events e " +
                " INNER JOIN wok.dynamic_objects o ON o.obj_placement_id = e.obj_placement_id " +
                " WHERE community_id = @communityId " +
                " AND event_status_id = @eventStatusId";

            if (!orderBy.Equals(string.Empty))
            {
                strSQL += " ORDER BY " + orderBy;   
            }

            parameters.Add(new MySqlParameter("@communityId", communityId));
            parameters.Add(new MySqlParameter("@eventStatusId", (int)Event.eSTATUS_TYPE.ACTIVE));
            DataTable dt = Db.Master.GetDataTable(strSQL, parameters);

            List<Event> list = new List<Event>();

            foreach (DataRow row in dt.Rows)
            {
                Event e = new Event(Convert.ToInt32(row["event_id"]), Convert.ToInt32(row["community_id"]), Convert.ToInt32(row["user_id"]),
                row["title"].ToString(), row["details"].ToString(), row["location"].ToString(), Convert.ToDateTime(row["start_time"]),
                Convert.ToDateTime(row["end_time"]), Convert.ToInt32(row["premium"]), Convert.ToBoolean(row["privacy"]),
                Convert.ToDateTime(row["created_date"]), row["is_AP_event"].ToString().Equals("Y"),
                row["tracking_request_GUID"].ToString(), Convert.ToInt32(row["event_status_id"]), Convert.ToInt32(row["recurring_status_id"]),
                row["invite_friends"].ToString().Equals("Y"), Convert.ToInt32(row["recurring_parent_id"]), Convert.ToInt32(row["obj_placement_id"]));

                list.Add(e);
            }

            return list;
        }

        /// <summary>
        /// GetEvent
        /// </summary>
        public Event GetEventByEventFlagPlacementId(int objPlacementId)
        {
            string strSQL = "SELECT event_id, community_id, user_id, title, details, location, start_time, end_time, " +
                " premium, privacy, created_date, is_AP_event, tracking_request_GUID, event_status_id, " +
                " recurring_status_id, invite_friends, recurring_parent_id, obj_placement_id " +
                " FROM events e " +
                " WHERE obj_placement_id = @objPlacementId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@objPlacementId", objPlacementId));
            DataRow dr = Db.Slave.GetDataRow(strSQL, parameters);

            if (dr == null)
            {
                return new Event();
            }

            return new Event(Convert.ToInt32(dr["event_id"]), Convert.ToInt32(dr["community_id"]), Convert.ToInt32(dr["user_id"]),
                dr["title"].ToString(), dr["details"].ToString(), dr["location"].ToString(), Convert.ToDateTime(dr["start_time"]),
                Convert.ToDateTime(dr["end_time"]), Convert.ToInt32(dr["premium"]), Convert.ToBoolean(dr["privacy"]),
                Convert.ToDateTime(dr["created_date"]), dr["is_AP_event"].ToString().Equals("Y"),
                dr["tracking_request_GUID"].ToString(), Convert.ToInt32(dr["event_status_id"]), Convert.ToInt32(dr["recurring_status_id"]),
                dr["invite_friends"].ToString().Equals("Y"), Convert.ToInt32(dr["recurring_parent_id"]), Convert.ToInt32(dr["obj_placement_id"]));
        }
    }
}
