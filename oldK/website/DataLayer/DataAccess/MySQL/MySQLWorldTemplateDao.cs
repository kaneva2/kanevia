///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Devart.Data.MySql;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLWorldTemplateDao : IWorldTemplateDao
    {
        private int _zoneIndexPlainPTID = 209;

        /// <summary>
        /// Called to get a WorldTemplate
        /// </summary>
        /// <param name="active">Return active or inactive templates</param>
        /// <param name="pageNumber">Current page number</param>
        /// <param name="pageSize">Number of items per page</param>
        /// <returns>The desired game template, or an empty template if not found.</returns>
        public WorldTemplate GetWorldTemplate(int templateId)
        {
            string sql = " SELECT gt.template_id, gt.name, gt.description, gt.default_category_id, gt.sort_order, gt.status_id, " +
                         " gt.preview_image_path_sm, gt.preview_image_path_me, gt.preview_image_path_lg, gt.kep_thumbnail_id, " +
                         " IF(gtd.default_key IS NULL, 0, 1) AS is_default_home, default_thumbnail, feature_list, incubator_hosted, display_pass_group_id, upsell_pass_group_id, gid.global_id, " +
                         " ip.value AS zone_index_plain, grandfather_date, first_public_release_date, private_on_creation " +
                         " FROM world_templates gt " +
                         " LEFT OUTER JOIN default_home_template gtd ON gt.template_id = gtd.template_id " +
                         " LEFT OUTER JOIN world_template_global_id gid ON gt.template_id = gid.template_id " +
                         " LEFT OUTER JOIN wok.item_parameters ip ON gid.global_id = ip.global_id AND ip.param_type_id = @paramTypeId " +
                         " WHERE gt.template_id = @templateId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@templateId", templateId));
            parameters.Add(new MySqlParameter("@paramTypeId", _zoneIndexPlainPTID));
            DataRow row = Db.WOK.GetDataRow(sql, parameters);

            if (row == null || string.IsNullOrEmpty(row["template_id"].ToString()))
            {
                return new WorldTemplate();
            }

            WorldTemplate template = new WorldTemplate(Convert.ToInt32(row["template_id"]), Convert.ToString(row["name"]), Convert.ToString(row["description"]), Convert.ToString(row["feature_list"]),
                    Convert.ToInt32((row["default_category_id"] == DBNull.Value ? 0 : row["default_category_id"])), Convert.ToInt32(row["sort_order"]), Convert.ToInt32(row["status_id"]),
                    Convert.ToString(row["preview_image_path_sm"]), Convert.ToString(row["preview_image_path_me"]), Convert.ToString(row["preview_image_path_lg"]), Convert.ToInt32(row["kep_thumbnail_id"]),
                    Convert.ToBoolean(row["is_default_home"]), Convert.ToString(row["default_thumbnail"]), (Convert.ToString(row["incubator_hosted"]).Equals("Y") ? true : false),
                    Convert.ToInt32(row["display_pass_group_id"]), Convert.ToInt32(row["upsell_pass_group_id"]), Convert.ToInt32(row["global_id"] == DBNull.Value ? 0 : row["global_id"]), Convert.ToInt32(row["zone_index_plain"] == DBNull.Value ? 0 : row["zone_index_plain"]),
                    (row["grandfather_date"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(row["grandfather_date"])),
                    Convert.ToDateTime(row["first_public_release_date"]), (Convert.ToString(row["private_on_creation"]).Equals("Y") ? true : false));

            return template;
        }

        /// <summary>
        /// Called to get the default world template to be applied to newly created home worlds
        /// </summary>
        public WorldTemplate GetDefaultHomeWorldTemplate()
        {
            string sql = " SELECT gt.template_id, gt.name, gt.description, gt.default_category_id, gt.sort_order, gt.status_id, " +
                         " gt.preview_image_path_sm, gt.preview_image_path_me, gt.preview_image_path_lg, gt.kep_thumbnail_id, " +
                         " IF(gtd.default_key IS NULL, 0, 1) AS is_default_home, default_thumbnail, feature_list, incubator_hosted, display_pass_group_id, upsell_pass_group_id, " +
                         " gid.global_id, ip.value AS zone_index_plain, grandfather_date, first_public_release_date, private_on_creation " +
                         " FROM world_templates gt " +
                         " INNER JOIN default_home_template gtd ON gt.template_id = gtd.template_id " +
                         " LEFT OUTER JOIN world_template_global_id gid ON gt.template_id = gid.template_id " +
                         " LEFT OUTER JOIN wok.item_parameters ip ON gid.global_id = ip.global_id AND ip.param_type_id = @paramTypeId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@paramTypeId", _zoneIndexPlainPTID));
            DataRow row = Db.WOK.GetDataRow(sql, parameters);

            if (row == null || string.IsNullOrEmpty(row["template_id"].ToString()))
            {
                return new WorldTemplate();
            }

            return new WorldTemplate(Convert.ToInt32(row["template_id"]), Convert.ToString(row["name"]), Convert.ToString(row["description"]), Convert.ToString(row["feature_list"]),
                    Convert.ToInt32((row["default_category_id"] == DBNull.Value ? 0 : row["default_category_id"])), Convert.ToInt32(row["sort_order"]), Convert.ToInt32(row["status_id"]),
                    Convert.ToString(row["preview_image_path_sm"]), Convert.ToString(row["preview_image_path_me"]), Convert.ToString(row["preview_image_path_lg"]), Convert.ToInt32(row["kep_thumbnail_id"]),
                    Convert.ToBoolean(row["is_default_home"]), Convert.ToString(row["default_thumbnail"]), (Convert.ToString(row["incubator_hosted"]).Equals("Y") ? true : false),
                    Convert.ToInt32(row["display_pass_group_id"]), Convert.ToInt32(row["upsell_pass_group_id"]), Convert.ToInt32(row["global_id"] == DBNull.Value ? 0 : row["global_id"]), Convert.ToInt32(row["zone_index_plain"] == DBNull.Value ? 0 : row["zone_index_plain"]),
                    (row["grandfather_date"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(row["grandfather_date"])),
                    Convert.ToDateTime(row["first_public_release_date"]), (Convert.ToString(row["private_on_creation"]).Equals("Y") ? true : false));
        }

        /// <summary>
        /// Called to get a paged list of WorldTemplates.
        /// </summary>
        /// <param name="status">Array of values for the status field</param>
        /// <param name="pageNumber">Current page number</param>
        /// <param name="pageSize">Number of items per page</param>
        /// <returns></returns>
        public PagedList<WorldTemplate> GetWorldTemplates(int[] status, int[] passGroupId, DateTime? userSignupDate, bool excludeIncubatorHosted, int pageNumber, int pageSize, string orderBy)
        {
            string sql = " SELECT gt.template_id, gt.name, gt.description, gt.default_category_id, gt.sort_order, gt.status_id, " +
                         " gt.preview_image_path_sm, gt.preview_image_path_me, gt.preview_image_path_lg, gt.kep_thumbnail_id, " +
                         " IF(gtd.default_key IS NULL, 0, 1) AS is_default_home, default_thumbnail, feature_list, incubator_hosted, display_pass_group_id, upsell_pass_group_id, " +
                         " gid.global_id, ip.value AS zone_index_plain, grandfather_date, first_public_release_date, private_on_creation " +
                         " FROM world_templates gt " +
                         " LEFT OUTER JOIN default_home_template gtd ON gt.template_id = gtd.template_id " +
                         " LEFT OUTER JOIN world_template_global_id gid ON gt.template_id = gid.template_id " +
                         " LEFT OUTER JOIN wok.item_parameters ip ON gid.global_id = ip.global_id AND ip.param_type_id = @paramTypeId " +
                         " WHERE 1 = 1 ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@paramTypeId", _zoneIndexPlainPTID));

            if (status.Length > 0)
            {
                sql += " AND ( ";
                for (int i = 0; i < status.Length; i++ )
                {
                    sql += (" status_id = @status" + i + (i >= status.Length - 1 ? "" : " OR "));
                    parameters.Add(new MySqlParameter("@status" + i, Convert.ToInt32(status[i])));
                }

                // If not including admin only templates by default, check that grandfather date is after the user's signup
                if (userSignupDate != null && !status.Contains((int)WorldTemplate.WorldTemplateStatus.ADMIN_ONLY))
                {
                    sql += " OR (status_id = " + (int)WorldTemplate.WorldTemplateStatus.ADMIN_ONLY + " AND grandfather_date IS NOT NULL AND grandfather_date >= @userSignupDate) ";
                    parameters.Add(new MySqlParameter("@userSignupDate", userSignupDate));
                }

                sql += " ) ";
            }

            if (passGroupId.Length > 0)
            {
                sql += " AND ( ";
                for (int i = 0; i < passGroupId.Length; i++)
                {
                    sql += (" display_pass_group_id = @passGroupId" + i + (i >= passGroupId.Length - 1 ? "" : " OR "));
                    parameters.Add(new MySqlParameter("@passGroupId" + i, Convert.ToInt32(passGroupId[i])));
                }
                sql += " ) ";
            }

            if (excludeIncubatorHosted)
            {
                sql += " AND incubator_hosted = 'N' ";
            }

            PagedDataTable pdt = Db.WOK.GetPagedDataTable(sql, orderBy, parameters, pageNumber, pageSize);

            PagedList<WorldTemplate> list = new PagedList<WorldTemplate>();
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                WorldTemplate template = new WorldTemplate(Convert.ToInt32(row["template_id"]), Convert.ToString(row["name"]), Convert.ToString(row["description"]), Convert.ToString(row["feature_list"]),
                    Convert.ToInt32((row["default_category_id"] == DBNull.Value ? 0 : row["default_category_id"])), Convert.ToInt32(row["sort_order"]), Convert.ToInt32(row["status_id"]),
                    Convert.ToString(row["preview_image_path_sm"]), Convert.ToString(row["preview_image_path_me"]), Convert.ToString(row["preview_image_path_lg"]), Convert.ToInt32(row["kep_thumbnail_id"]),
                    Convert.ToBoolean(row["is_default_home"]), Convert.ToString(row["default_thumbnail"]), (Convert.ToString(row["incubator_hosted"]).Equals("Y") ? true : false),
                    Convert.ToInt32(row["display_pass_group_id"]), Convert.ToInt32(row["upsell_pass_group_id"]), Convert.ToInt32(row["global_id"] == DBNull.Value ? 0 : row["global_id"]), Convert.ToInt32(row["zone_index_plain"] == DBNull.Value ? 0 : row["zone_index_plain"]),
                    (row["grandfather_date"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(row["grandfather_date"])),
                    Convert.ToDateTime(row["first_public_release_date"]), (Convert.ToString(row["private_on_creation"]).Equals("Y") ? true : false));
                list.Add(template);
            }

            return list;
        }

        public PagedList<WorldTemplate> GetWorldTemplates(List<int> templateIds, int pageNumber, int pageSize, string orderBy)
        {
            if (templateIds.Count == 0)
            {
                return new PagedList<WorldTemplate>();
            }

            string sql = " SELECT gt.template_id, gt.name, gt.description, gt.default_category_id, gt.sort_order, gt.status_id, " +
                         " gt.preview_image_path_sm, gt.preview_image_path_me, gt.preview_image_path_lg, gt.kep_thumbnail_id, " +
                         " IF(gtd.default_key IS NULL, 0, 1) AS is_default_home, default_thumbnail, feature_list, incubator_hosted, display_pass_group_id, upsell_pass_group_id, " +
                         " gid.global_id, ip.value AS zone_index_plain, grandfather_date, first_public_release_date, private_on_creation " +
                         " FROM world_templates gt " +
                         " LEFT OUTER JOIN default_home_template gtd ON gt.template_id = gtd.template_id " +
                         " LEFT OUTER JOIN world_template_global_id gid ON gt.template_id = gid.template_id " +
                         " LEFT OUTER JOIN wok.item_parameters ip ON gid.global_id = ip.global_id AND ip.param_type_id = @paramTypeId " +
                         " WHERE gt.template_id IN (" + string.Join(",", templateIds) + ") ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@paramTypeId", _zoneIndexPlainPTID));

            PagedDataTable pdt = Db.WOK.GetPagedDataTable(sql, orderBy, parameters, pageNumber, pageSize);

            PagedList<WorldTemplate> list = new PagedList<WorldTemplate>();
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                WorldTemplate template = new WorldTemplate(Convert.ToInt32(row["template_id"]), Convert.ToString(row["name"]), Convert.ToString(row["description"]), Convert.ToString(row["feature_list"]),
                    Convert.ToInt32((row["default_category_id"] == DBNull.Value ? 0 : row["default_category_id"])), Convert.ToInt32(row["sort_order"]), Convert.ToInt32(row["status_id"]),
                    Convert.ToString(row["preview_image_path_sm"]), Convert.ToString(row["preview_image_path_me"]), Convert.ToString(row["preview_image_path_lg"]), Convert.ToInt32(row["kep_thumbnail_id"]),
                    Convert.ToBoolean(row["is_default_home"]), Convert.ToString(row["default_thumbnail"]), (Convert.ToString(row["incubator_hosted"]).Equals("Y") ? true : false),
                    Convert.ToInt32(row["display_pass_group_id"]), Convert.ToInt32(row["upsell_pass_group_id"]), Convert.ToInt32(row["global_id"] == DBNull.Value ? 0 : row["global_id"]), Convert.ToInt32(row["zone_index_plain"] == DBNull.Value ? 0 : row["zone_index_plain"]),
                    (row["grandfather_date"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(row["grandfather_date"])),
                    Convert.ToDateTime(row["first_public_release_date"]), (Convert.ToString(row["private_on_creation"]).Equals("Y") ? true : false));
                list.Add(template);
            }

            return list;
        }

        public PagedList<WorldTemplate> GetGamingWorldTemplates(int pageNumber, int pageSize, string orderBy)
        {
            string sql = " SELECT gt.template_id, gt.name, gt.description, gt.default_category_id, gt.sort_order, gt.status_id, " +
                         " gt.preview_image_path_sm, gt.preview_image_path_me, gt.preview_image_path_lg, gt.kep_thumbnail_id, " +
                         " IF(gtd.default_key IS NULL, 0, 1) AS is_default_home, default_thumbnail, feature_list, incubator_hosted, display_pass_group_id, upsell_pass_group_id, " +
                         " gid.global_id, ip.value AS zone_index_plain, grandfather_date, first_public_release_date, private_on_creation " +
                         " FROM world_templates gt " +
                         " INNER JOIN world_template_global_id gid ON gt.template_id = gid.template_id " +
                         " INNER JOIN starting_script_game_custom_data sgi ON sgi.template_glid = gid.global_id AND sgi.attribute = 'WorldSettings' AND sgi.value LIKE '%\"frameworkEnabled\":true%' " +
                         " LEFT OUTER JOIN wok.item_parameters ip ON gid.global_id = ip.global_id AND ip.param_type_id = @paramTypeId " +
                         " LEFT OUTER JOIN default_home_template gtd ON gt.template_id = gtd.template_id " +
                         " WHERE gt.incubator_hosted = 'N' ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@paramTypeId", _zoneIndexPlainPTID));
            PagedDataTable pdt = Db.WOK.GetPagedDataTable(sql, orderBy, parameters, pageNumber, pageSize);

            PagedList<WorldTemplate> list = new PagedList<WorldTemplate>();
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                WorldTemplate template = new WorldTemplate(Convert.ToInt32(row["template_id"]), Convert.ToString(row["name"]), Convert.ToString(row["description"]), Convert.ToString(row["feature_list"]),
                    Convert.ToInt32((row["default_category_id"] == DBNull.Value ? 0 : row["default_category_id"])), Convert.ToInt32(row["sort_order"]), Convert.ToInt32(row["status_id"]),
                    Convert.ToString(row["preview_image_path_sm"]), Convert.ToString(row["preview_image_path_me"]), Convert.ToString(row["preview_image_path_lg"]), Convert.ToInt32(row["kep_thumbnail_id"]),
                    Convert.ToBoolean(row["is_default_home"]), Convert.ToString(row["default_thumbnail"]), (Convert.ToString(row["incubator_hosted"]).Equals("Y") ? true : false),
                    Convert.ToInt32(row["display_pass_group_id"]), Convert.ToInt32(row["upsell_pass_group_id"]), Convert.ToInt32(row["global_id"] == DBNull.Value ? 0 : row["global_id"]), Convert.ToInt32(row["zone_index_plain"] == DBNull.Value ? 0 : row["zone_index_plain"]),
                    (row["grandfather_date"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(row["grandfather_date"])),
                    Convert.ToDateTime(row["first_public_release_date"]), (Convert.ToString(row["private_on_creation"]).Equals("Y") ? true : false));
                list.Add(template);
            }

            return list;
        }

        /// <summary>
        /// Called to search a paged list of WorldTemplates.
        /// </summary>
        /// <param name="status">Array of values for the status field</param>
        /// <param name="pageNumber">Current page number</param>
        /// <param name="pageSize">Number of items per page</param>
        /// <returns></returns>
        public PagedList<WorldTemplate> SearchWorldTemplates(string searchString, int pageNumber, int pageSize, string orderBy)
        {
            string sql = " SELECT gt.template_id, gt.name, gt.description, gt.default_category_id, gt.sort_order, gt.status_id, " +
                         " gt.preview_image_path_sm, gt.preview_image_path_me, gt.preview_image_path_lg, gt.kep_thumbnail_id, " +
                         " IF(gtd.default_key IS NULL, 0, 1) AS is_default_home, default_thumbnail, feature_list, incubator_hosted, " +
                         " display_pass_group_id, upsell_pass_group_id, gid.global_id, ip.value AS zone_index_plain, grandfather_date, first_public_release_date, private_on_creation " +
                         " FROM world_templates gt " +
                         " LEFT OUTER JOIN default_home_template gtd ON gt.template_id = gtd.template_id " +
                         " LEFT OUTER JOIN world_template_global_id gid ON gt.template_id = gid.template_id " +
                         " LEFT OUTER JOIN wok.item_parameters ip ON gid.global_id = ip.global_id AND ip.param_type_id = @paramTypeId " +
                         " WHERE 1 = 1 ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@paramTypeId", _zoneIndexPlainPTID));

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                sql += " AND gt.name LIKE @searchString";
                parameters.Add(new MySqlParameter("@searchString", "%" + searchString + "%"));
            }

            PagedDataTable pdt = Db.WOK.GetPagedDataTable(sql, orderBy, parameters, pageNumber, pageSize);

            PagedList<WorldTemplate> list = new PagedList<WorldTemplate>();
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                WorldTemplate template = new WorldTemplate(Convert.ToInt32(row["template_id"]), Convert.ToString(row["name"]), Convert.ToString(row["description"]), Convert.ToString(row["feature_list"]),
                    Convert.ToInt32((row["default_category_id"] == DBNull.Value ? 0 : row["default_category_id"])), Convert.ToInt32(row["sort_order"]), Convert.ToInt32(row["status_id"]),
                    Convert.ToString(row["preview_image_path_sm"]), Convert.ToString(row["preview_image_path_me"]), Convert.ToString(row["preview_image_path_lg"]), Convert.ToInt32(row["kep_thumbnail_id"]),
                    Convert.ToBoolean(row["is_default_home"]), Convert.ToString(row["default_thumbnail"]), (Convert.ToString(row["incubator_hosted"]).Equals("Y") ? true : false),
                    Convert.ToInt32(row["display_pass_group_id"]), Convert.ToInt32(row["upsell_pass_group_id"]), Convert.ToInt32(row["global_id"] == DBNull.Value ? 0 : row["global_id"]), Convert.ToInt32(row["zone_index_plain"] == DBNull.Value ? 0 : row["zone_index_plain"]),
                    (row["grandfather_date"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(row["grandfather_date"])),
                    Convert.ToDateTime(row["first_public_release_date"]), (Convert.ToString(row["private_on_creation"]).Equals("Y") ? true : false));
                list.Add(template);
            }

            return list;
        }

        /// <summary>
        /// Called to get the default item glids for the game template.
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public DataTable GetWorldTemplateDefaultItemIds(int templateId)
        {
            string sql = "SELECT template_id, global_id, COALESCE(bi.bundle_global_id, 0) as bundle_id " +
                " FROM world_template_default_items gtdi " +
                " LEFT OUTER JOIN bundle_items bi ON bi.bundle_global_id = gtdi.global_id " +
                " WHERE template_id = @templateId " +
                " GROUP BY global_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@templateId", templateId));
            return Db.WOK.GetDataTable(sql, parameters);
        }

        public PagedList<WOKItem> GetWorldTemplateDefaultItems(int templateId, int pageNumber, int pageSize)
        {
            string strSelect = "SELECT DISTINCT(i.global_id) as global_id,name,i.description,market_cost, " +
	               " selling_price, use_type, " +
	               " iw.display_name, COALESCE(pass_group_id,0) as passTypeId, iw.date_added, " +
	               " item_creator_id, iw.item_active, " +
	               " inventory_type, i.base_global_id, iw.template_path, iw.texture_path, " +
	               " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
	               " (i.market_cost + iw.designer_price + COALESCE(iw2.designer_price,0)) as WebPrice, " +
	               " iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, username as owner_username, " +
	               " COALESCE(iw2.designer_price,0) as TemplateDesignerPrice, " +
	               " iw.category_id1, iw.category_id2, iw.category_id3, COALESCE(ia.item_id,0) as has_animation " +
                " FROM wok.world_template_default_items wtdi " +
                " INNER JOIN wok.items i ON wtdi.global_id = i.global_id " +
                " INNER JOIN shopping.items_web iw ON i.global_id = iw.global_id " +
                " LEFT OUTER JOIN shopping.items_web iw2 ON iw.base_global_id = iw2.global_id " +
                " LEFT JOIN wok.item_parameters ip ON i.global_id = ip.global_id AND i.use_type = ip.param_type_id AND use_type <= 13 " +
                " LEFT OUTER JOIN kaneva.users u ON i.item_creator_id = u.user_id " +
                " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id AND pgi.pass_group_id = 1 " +
                " LEFT OUTER JOIN wok.item_animations ia ON ia.item_id = i.global_id " +
                " WHERE wtdi.template_id = @templateId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@templateId", templateId));

            PagedDataTable dt = Db.WOK.GetPagedDataTable(strSelect, "wtdi.global_id ASC", parameters, pageNumber, pageSize);

            PagedList<WOKItem> list = new PagedList<WOKItem>();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                WOKItem wokItem = new WOKItem(Convert.ToInt32(row["global_id"]), row["name"].ToString(), row["description"].ToString(), Convert.ToInt32(row["market_cost"]),
                Convert.ToInt32(row["selling_price"]), 0, 0, Convert.ToInt32(row["use_type"]),
                row["display_name"].ToString(), Convert.ToInt32(row["passTypeId"]),
                Convert.ToDateTime(row["date_added"]), Convert.ToUInt32(row["item_creator_id"]), Convert.ToInt32(row["item_active"]), 0,
                0, 0, 0,
                Convert.ToInt32(row["inventory_type"]), Convert.ToUInt32(row["base_global_id"]), row["template_path"].ToString(), row["texture_path"].ToString(),
                row["thumbnail_path"].ToString(), row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_assetdetails_path"].ToString(), Convert.ToUInt32(row["designer_price"]),
                row["keywords"].ToString(), Convert.ToInt32(row["WebPrice"]), 0, Convert.ToUInt32(row["number_sold_on_web"]), Convert.ToUInt32(row["number_of_raves"]),
                Convert.ToUInt32(row["number_of_views"]), 0, 0, 0, WOKItem.DRL_UNKNOWN, Convert.ToUInt32(row["TemplateDesignerPrice"]),
                Convert.ToUInt32(row["category_id1"]), Convert.ToUInt32(row["category_id2"]), Convert.ToUInt32(row["category_id3"]),
                0,
                Convert.ToBoolean(row["has_animation"])
                );

                if (wokItem.UseType == WOKItem.USE_TYPE_BUNDLE)
                {
                    wokItem.BundleWebPrice = Convert.ToInt32(row["market_cost"]);
                }

                list.Add(wokItem);
            }
            return list;
        }

        /// <summary>
        /// Called to associate default global_ids with a template.
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="globalIds"></param>
        /// <returns></returns>
        public int InsertWorldTemplateDefaultItemIds(int templateId, int[] globalIds)
        {
            int rowsAffected = 0;

            if (globalIds.Length > 0)
            {
                string sql = " INSERT INTO world_template_default_items (template_id, global_id) VALUES ";
                List<IDbDataParameter> parameters = new List<IDbDataParameter>();

                for (int x = 0; x < globalIds.Length; x++)
                {
                    sql += string.Format(" (@templateId{0}, @globalId{0}),", x);
                    parameters.Add(new MySqlParameter("@templateId" + x, templateId));
                    parameters.Add(new MySqlParameter("@globalId" + x, globalIds[x]));
                }

                sql = sql.TrimEnd(',');

                rowsAffected = Db.WOK.ExecuteNonQuery(sql, parameters);
            }

            return rowsAffected;
        }

        /// <summary>
        /// Called to disassociate default global_ids with a template.
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="globalIds"></param>
        /// <returns></returns>
        public int DeleteWorldTemplateDefaultItemIds(int templateId, int[] globalIds)
        {
            int rowsAffected = 0;

            if (globalIds.Length > 0)
            {
                string sql = " DELETE FROM world_template_default_items WHERE template_id = @templateId AND global_id IN (" + string.Join(", ", globalIds) + ")";
                
                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@templateId", templateId));

                rowsAffected = Db.WOK.ExecuteNonQuery(sql, parameters);
            }

            return rowsAffected;
        }

        /// <summary>
        /// Insert the game template
        /// </summary>
        /// <param name="template"></param>
        /// <returns>int</returns>
        public int InsertWorldTemplate(WorldTemplate template)
        {
            int rowsAffected = 0;
            MySQLExperimentDao experimentDao = new MySQLExperimentDao();

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sql =
                        " INSERT INTO world_templates (template_id, name, description, feature_list, default_category_id, sort_order, status_id, " +
                        "     preview_image_path_sm, preview_image_path_me, preview_image_path_lg, kep_thumbnail_id, default_thumbnail, incubator_hosted, display_pass_group_id, upsell_pass_group_id, grandfather_date, first_public_release_date, private_on_creation) " +
                        " VALUES(@templateId, @name, @description, @featureList, @defaultCategoryId, @sortOrder, @statusId, @previewImagePathSmall, " +
                        "     @previewImagePathMedium, @previewImagePathLarge, @kepThumbnailId, @defaultThumbnail, @incubatorHosted, @passGroupId, @upsellPassGroupId, @grandfatherDate, @firstPublicReleaseDate, @privateOnCreation)";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@templateId", template.TemplateId));
                    parameters.Add(new MySqlParameter("@name", template.Name));
                    parameters.Add(new MySqlParameter("@description", template.Description));
                    parameters.Add(new MySqlParameter("@featureList", template.Features));
                    parameters.Add(new MySqlParameter("@defaultCategoryId", (template.DefaultCategoryId > 0 ? (object)template.DefaultCategoryId : DBNull.Value)));
                    parameters.Add(new MySqlParameter("@sortOrder", template.SortOrder));
                    parameters.Add(new MySqlParameter("@statusId", template.Status));
                    parameters.Add(new MySqlParameter("@previewImagePathSmall", template.PreviewImagePathSmall));
                    parameters.Add(new MySqlParameter("@previewImagePathMedium", template.PreviewImagePathMedium));
                    parameters.Add(new MySqlParameter("@previewImagePathLarge", template.PreviewImagePathLarge));
                    parameters.Add(new MySqlParameter("@kepThumbnailId", template.KEPThumbnailId));
                    parameters.Add(new MySqlParameter("@defaultThumbnail", template.DefaultThumbnail));
                    parameters.Add(new MySqlParameter("@incubatorHosted", (template.IncubatorHosted ? "Y" : "N")));
                    parameters.Add(new MySqlParameter("@passGroupId", template.DisplayPassGroupId));
                    parameters.Add(new MySqlParameter("@upsellPassGroupId", template.UpsellPassGroupId));
                    parameters.Add(new MySqlParameter("@grandfatherDate", (template.GrandfatherDate != null ? (object)template.GrandfatherDate : DBNull.Value)));
                    parameters.Add(new MySqlParameter("@firstPublicReleaseDate", template.FirstPublicReleaseDate));
                    parameters.Add(new MySqlParameter("@privateOnCreation", (template.PrivateOnCreation ? "Y" : "N")));

                    rowsAffected = Db.WOK.ExecuteNonQuery(sql, parameters, true);

                    if (rowsAffected > 0)
                    {
                        // Associate with global id
                        UpdateWorldTemplateGlobalId(template.TemplateId, template.GlobalId, true);

                        // Check default
                        if (template.IsDefaultHomeTemplate)
                        {
                            SetDefaultHomeWorldTemplate(template, true);
                        }
                    }

                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                rowsAffected = 0;
                m_logger.Error("MySQLWorldTemplateDao.InsertWorldTemplate() - Transaction aborted: " + e.ToString());
            }

            return rowsAffected;
        }

        /// <summary>
        /// Update the game template
        /// </summary>
        /// <param name="template"></param>
        /// <returns>int</returns>
        public int UpdateWorldTemplate(WorldTemplate template)
        {
            int rowsAffected = 0;
            MySQLExperimentDao experimentDao = new MySQLExperimentDao();

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sql =
                        " UPDATE world_templates " +
                        " SET name                  = @name, " +
                        "     description           = @description, " +
                        "     feature_list          = @featureList, " +
                        "     default_category_id   = @defaultCategoryId, " +
                        "     sort_order            = @sortOrder, " +
                        "     status_id             = @statusId, " +
                        "     preview_image_path_sm = @previewImagePathSmall, " +
                        "     preview_image_path_me = @previewImagePathMedium, " +
                        "     preview_image_path_lg = @previewImagePathLarge, " +
                        "     kep_thumbnail_id      = @kepThumbnailId, " +
                        "     default_thumbnail     = @defaultThumbnail, " +
                        "     incubator_hosted      = @incubatorHosted, " +
                        "     display_pass_group_id = @passGroupId, " +
                        "     upsell_pass_group_id  = @upsellPassGroupId, " +
                        "     grandfather_date      = @grandfatherDate, " +
                        "     first_public_release_date = @firstPublicReleaseDate, " +
                        "     private_on_creation   = @privateOnCreation " +
                        " WHERE template_id         = @templateId";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@templateId", template.TemplateId));
                    parameters.Add(new MySqlParameter("@name", template.Name));
                    parameters.Add(new MySqlParameter("@description", template.Description));
                    parameters.Add(new MySqlParameter("@featureList", template.Features));
                    parameters.Add(new MySqlParameter("@defaultCategoryId", (template.DefaultCategoryId > 0 ? (object)template.DefaultCategoryId : DBNull.Value)));
                    parameters.Add(new MySqlParameter("@sortOrder", template.SortOrder));
                    parameters.Add(new MySqlParameter("@statusId", template.Status));
                    parameters.Add(new MySqlParameter("@previewImagePathSmall", template.PreviewImagePathSmall));
                    parameters.Add(new MySqlParameter("@previewImagePathMedium", template.PreviewImagePathMedium));
                    parameters.Add(new MySqlParameter("@previewImagePathLarge", template.PreviewImagePathLarge));
                    parameters.Add(new MySqlParameter("@kepThumbnailId", template.KEPThumbnailId));
                    parameters.Add(new MySqlParameter("@defaultThumbnail", template.DefaultThumbnail));
                    parameters.Add(new MySqlParameter("@incubatorHosted", (template.IncubatorHosted ? "Y" : "N")));
                    parameters.Add(new MySqlParameter("@passGroupId", template.DisplayPassGroupId));
                    parameters.Add(new MySqlParameter("@upsellPassGroupId", template.UpsellPassGroupId));
                    parameters.Add(new MySqlParameter("@grandfatherDate", (template.GrandfatherDate != null ? (object)template.GrandfatherDate : DBNull.Value)));
                    parameters.Add(new MySqlParameter("@firstPublicReleaseDate", template.FirstPublicReleaseDate));
                    parameters.Add(new MySqlParameter("@privateOnCreation", (template.PrivateOnCreation ? "Y" : "N")));

                    rowsAffected = Db.WOK.ExecuteNonQuery(sql, parameters, true);

                    // Associate with global id
                    UpdateWorldTemplateGlobalId(template.TemplateId, template.GlobalId, true);

                    // Check default
                    if (template.IsDefaultHomeTemplate)
                    {
                        SetDefaultHomeWorldTemplate(template, true);
                    }

                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                rowsAffected = 0;
                m_logger.Error("MySQLWorldTemplateDao.UpdateWorldTemplate() - Transaction aborted: " + e.ToString());
            }

            return rowsAffected;
        }

        /// <summary>
        /// Delete the game template
        /// </summary>
        /// <param name="template"></param>
        /// <returns>int</returns>
        public int DeleteWorldTemplate(WorldTemplate template)
        {
            int rowsAffected = 0;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sql = " DELETE FROM world_templates WHERE template_id = @templateId";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@templateId", template.TemplateId));

                    // Make sure we always have a valid default
                    if (template.IsDefaultHomeTemplate)
                    {
                        int[] template_statuses = { Convert.ToInt32(WorldTemplate.WorldTemplateStatus.ACTIVE), Convert.ToInt32(WorldTemplate.WorldTemplateStatus.ADMIN_ONLY), Convert.ToInt32(WorldTemplate.WorldTemplateStatus.ARCHIVED) };
                        int[] templatePassGroups = new int[] { 0 };
                        PagedList<WorldTemplate> templates = GetWorldTemplates(template_statuses, templatePassGroups, null, true, 1, 1, "sort_order ASC");
                        templates[0].IsDefaultHomeTemplate = true;
                        SetDefaultHomeWorldTemplate(templates[0], true);
                    }

                    rowsAffected = Db.WOK.ExecuteNonQuery(sql, parameters, true);

                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                rowsAffected = 0;
                m_logger.Error("MySQLWorldTemplateDao.DeleteWorldTemplate() - Transaction aborted: " + e.ToString());
            }

            return rowsAffected;
        }

        /// <summary>
        /// Sets the default game template on update, insert, or delete
        /// </summary>
        /// <param name="template"></param>
        /// <returns>int</returns>
        private int SetDefaultHomeWorldTemplate(WorldTemplate template, bool propagateTransExceptions = false)
        {
            int rowsAffected = 0;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    // Only non-incubator templates can be set as the default
                    if (template.IsDefaultHomeTemplate &&
                        template.GlobalId > 0 &&
                        !template.IncubatorHosted)
                    {
                        string sql =
                            " UPDATE default_home_template " +
                            " SET template_id = @templateId";

                        List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                        parameters.Add(new MySqlParameter("@templateId", template.TemplateId));

                        rowsAffected = Db.WOK.ExecuteNonQuery(sql, parameters, true);
                    }
                    else
                    {
                        throw new Exception("Unable to set template as default home world template.");
                    }

                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                rowsAffected = 0;
                m_logger.Error("MySQLWorldTemplateDao.SetDefaultHomeWorldTemplate() - Transaction aborted: " + e.ToString());

                if (propagateTransExceptions)
                    throw e;
            }

            return rowsAffected;
        }

        /// <summary>
        /// Add the template to global_id
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="globalId"></param>
        /// <returns>int</returns>
        private int UpdateWorldTemplateGlobalId(int templateId, int globalId, bool propagateTransExceptions = false)
        {
            int rowsAffected = 0;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sql = string.Empty;

                    if (globalId > 0)
                    {
                        sql = " INSERT INTO world_template_global_id (template_id, global_id) VALUES(@templateId, @globalId) " +
                              " ON DUPLICATE KEY UPDATE global_id = @globalId ";
                    }
                    else
                    {
                        sql = " DELETE FROM world_template_global_id WHERE template_id = @templateId ";
                    }

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@templateId", templateId));
                    parameters.Add(new MySqlParameter("@globalId", globalId));

                    rowsAffected = Db.WOK.ExecuteNonQuery(sql, parameters, true);

                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                rowsAffected = 0;
                m_logger.Error("MySQLWorldTemplateDao.UpdateWorldTemplateGlobalId() - Transaction aborted: " + e.ToString());

                if (propagateTransExceptions)
                    throw e;
            }

            return rowsAffected;
        }

        /// <summary>
		/// Add the game to template record
		/// </summary>
		/// <param name="communityId"></param>
		/// <param name="templateId"></param>
		/// <returns>int</returns>
		public int InsertWorldsToTemplate(int communityId, int templateId)
		{
            string sql = " INSERT IGNORE INTO worlds_to_templates (community_id, template_id) VALUES(@communityId, @templateId)";

			List<IDbDataParameter> parameters = new List<IDbDataParameter>();
			parameters.Add(new MySqlParameter("@communityId",     communityId));
			parameters.Add(new MySqlParameter("@templateId", templateId));

			return Db.WOK.Insert(sql, parameters, 0);
		}

		/// <summary>
		/// Delete the game to template record
		/// </summary>
		/// <param name="communityId"></param>
		/// <param name="templateId"></param>
		/// <returns>int</returns>
		public int DeleteWorldsToTemplate(int communityId, int templateId)
		{
            string sql = " DELETE FROM worlds_to_templates WHERE community_id = @communityId AND template_id = @templateId";

			List<IDbDataParameter> parameters = new List<IDbDataParameter>();
			parameters.Add(new MySqlParameter("@communityId", communityId));
			parameters.Add(new MySqlParameter("@templateId", templateId));

			return Db.WOK.ExecuteNonQuery(sql, parameters);
		}

        public PagedList<WorldTemplate> GetWorldTemplatesByExperimentGroups(List<string> experimentGroupIds, string action)
        {
            if (experimentGroupIds.Count == 0)
            {
                return new PagedList<WorldTemplate>();
            }

            string strSelect = "SELECT wte.template_id, wte.experiment_group_id, ege.effect, e.name AS experiment_name, eg.name AS experiment_group_name " +
                               "FROM experiment.world_template_experiment_groups wte " +
                               "INNER JOIN experiment.experiment_groups eg ON wte.experiment_group_id = eg.experiment_group_id " +
                               "INNER JOIN experiment.experiments e ON eg.experiment_id = e.experiment_id " +
                               "INNER JOIN experiment.experiment_group_effects ege ON eg.experiment_group_id = ege.experiment_group_id " +
                               "WHERE wte.experiment_group_id IN ('" + string.Join("','", experimentGroupIds) + "') " +
                               "AND ege.effect = @action ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@action", action));

            DataTable dt = Db.WOK.GetDataTable(strSelect, parameters);

            List<int> list = new List<int>();

            foreach (DataRow dtRow in dt.Rows)
            {
                list.Add(Convert.ToInt32(dtRow["template_id"]));
            }

            return GetWorldTemplates(list, 1, Int32.MaxValue, string.Empty);
        }

        public int GetWorldTemplateId (int communityId)
        {
            string sql = "SELECT template_id " +
                " FROM wok.worlds_to_templates " +
                " WHERE community_id = @communityId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            return Convert.ToInt32(Db.Master.GetScalar(sql, parameters) ?? 0);
        }


        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
	}
}
