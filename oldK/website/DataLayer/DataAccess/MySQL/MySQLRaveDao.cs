///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLRaveDao : IRaveDao
    {
        public RaveType GetRaveType (UInt32 raveTypeId)                            
        {
            RaveType raveType = (RaveType)CentralCache.Get(CentralCache.keyKanevaRaveTypes + raveTypeId.ToString());

            if (raveType != null)
            {
                // Found in cache
                return raveType;
            }

            string sqlSelect = "SELECT " + 
                " rave_type_id, name, description, mega_rave_value, price_single_rave, " +
                " price_mega_rave, number_allowed_free, percent_commission " +
                " FROM rave_types " +
                " WHERE rave_type_id = @raveTypeId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@raveTypeId", raveTypeId));
            DataRow row = Db.Slave.GetDataRow (sqlSelect, parameters);              

            if (row == null)
            {
                return null;
            }

            raveType = new RaveType(Convert.ToUInt32(row["rave_type_id"]), row["name"].ToString(),
            row["description"].ToString (), Convert.ToInt32 (row["mega_rave_value"]), Convert.ToInt32 (row["price_single_rave"]),
            Convert.ToInt32 (row["price_mega_rave"]), Convert.ToInt32 (row["number_allowed_free"]), Convert.ToInt32 (row["percent_commission"]));

            CentralCache.Store(CentralCache.keyKanevaRaveTypes + raveTypeId.ToString(), raveType, TimeSpan.FromMinutes(60));

            return raveType;
        }

        public IList<RaveType> GetRaveTypes ()                                      
        {
            string strQuery = "SELECT " +
                " rave_type_id, name, description, mega_rave_value, price_single_rave, " +
                " price_mega_rave, number_allowed_free, percent_commission " +
                " FROM rave_types ";

            DataTable dt = Db.Slave.GetDataTable (strQuery);                        

            IList<RaveType> list = new List<RaveType> ();
            foreach (DataRow row in dt.Rows)
            {
                list.Add (new RaveType (Convert.ToUInt32 (row["rave_type_id"]), row["name"].ToString (),
                    row["description"].ToString (), Convert.ToInt32 (row["mega_rave_value"]), Convert.ToInt32 (row["price_single_rave"]),
                    Convert.ToInt32 (row["price_mega_rave"]), Convert.ToInt32 (row["number_allowed_free"]), Convert.ToInt32 (row["percent_commission"])
                ));
            }
            return list;
        }

        public int InsertMegaRaveHistory (UInt32 raveTypeId, int channelId, int zoneInstanceId, int zoneType, int assetId,
        int raveValue, int userIdRaver, int pricePaid, int commissionReceived)
        {
            string sql = "INSERT INTO mega_rave_history " +
                "(rave_type_id, channel_id, zone_instance_id, zone_type, asset_id, " +
                " rave_value, user_id_raver, price_paid, commission_received, create_datetime " +
                ") VALUES (" +
                " @raveTypeId, @channelId, @zoneInstanceId, @zoneType, @assetId, " +
                " @raveValue, @userIdRaver, @pricePaid, @commissionReceived, @createDate)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@raveTypeId", raveTypeId));
            parameters.Add (new MySqlParameter ("@channelId", channelId));
            parameters.Add (new MySqlParameter ("@zoneInstanceId", zoneInstanceId));
            parameters.Add (new MySqlParameter ("@zoneType", zoneType));
            parameters.Add (new MySqlParameter ("@assetId", assetId));
            parameters.Add (new MySqlParameter ("@raveValue", raveValue));
            parameters.Add (new MySqlParameter ("@userIdRaver", userIdRaver));
            parameters.Add (new MySqlParameter ("@pricePaid", pricePaid));
            parameters.Add (new MySqlParameter ("@commissionReceived", commissionReceived));
            parameters.Add (new MySqlParameter ("@createDate", DateTime.Now));

            return Db.Master.ExecuteIdentityInsert (sql, parameters);
        }

        public int UpdateMegaRaveHistoryCommission (int megaRaveHistoryId, int commissionReceived)
        {
            string sql = "UPDATE mega_rave_history " +
              " SET commission_received = @commissionReceived " +
              " WHERE mega_rave_history_id = @megaRaveHistoryId;";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@megaRaveHistoryId", megaRaveHistoryId));
            parameters.Add (new MySqlParameter ("@commissionReceived", commissionReceived));

            return Db.Master.ExecuteNonQuery (sql, parameters);
        }

        #region UGC Raves

        public void AdjustUGCRaveTotal (int globalId, int numRaves)
        {
            string sql = "UPDATE items_web " +
               " SET number_of_raves = number_of_raves  + @numRaves " +
               " WHERE global_id = @globalId ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));
            parameters.Add (new MySqlParameter ("@numRaves", numRaves));

            Db.Shopping.ExecuteNonQuery (sql, parameters);
        }

        public int InsertUGCRave (int userId, int globalId)
        {
            string sql = "INSERT INTO item_diggs " +
                "(user_id, global_id, created_date)" +
                " VALUES " +
                "(@userId, @globalId, @raveDate)";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@raveDate", DateTime.Now));

            return Db.Shopping.ExecuteNonQuery (sql, parameters);
        }

        public int GetUGCRaveCountByUser (int userId, int globalId)
        {
            int numRaves = 0;

            string sql = "SELECT COUNT(*) as raves";
            sql += " FROM item_diggs ";
            sql += " WHERE global_id=@globalId AND user_id=@userId ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));
            parameters.Add (new MySqlParameter ("@userId", userId));

            DataRow dr = Db.Shopping.GetDataRow (sql, parameters);

            if (dr != null)
            {
                numRaves = Convert.ToInt32 (dr["raves"]);
            }

            return numRaves;
        }

        public int GetUGCRaveCount (int globalId)
        {
            int numRaves = 0;

            string sql = "SELECT number_of_raves ";
            sql += " FROM items_web ";
            sql += " WHERE global_id=@global_id ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@global_id", globalId));

            DataRow dr = Db.Shopping.GetDataRow (sql, parameters);

            if (dr != null)
            {
                numRaves = Convert.ToInt32 (dr["number_of_raves"]);
            }

            return numRaves;
        }

        #endregion UGC Raves


        #region Community/User Raves


        public DataRow GetCommunityRave (int userId, int communityId)
        {
            string sqlSelect = "select d.digg_id " +
                " FROM channel_diggs d " +
                " WHERE user_id = @userId " +
                " AND channel_id = @channelId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@channelId", communityId));
            return Db.Master.GetDataRow (sqlSelect, parameters);
        }

        public int GetCommunityRaveCountByUser (int userId, int channelId)
        {
            int numRaves = 0;

            string sql = "SELECT COALESCE(SUM(rave_count),0) as raves ";
            sql += " FROM channel_diggs ";
            sql += " WHERE user_id=@userId AND channel_id=@channelId ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@channelId", channelId));

            DataRow dr = Db.Master.GetDataRow (sql, parameters);

            if (dr != null)
            {
                numRaves = Convert.ToInt32 (dr["raves"]);
            }

            return numRaves;
        }

        public int RaveCommunityAndAdjustStats(int ravingUserId, int communityId, int numRaves)
        {
            string sql = "CALL add_community_rave(@communityId, @numRaves, @ravingUserId) ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            parameters.Add(new MySqlParameter("@numRaves", numRaves));
            parameters.Add(new MySqlParameter("@ravingUserId", ravingUserId));

            return Convert.ToInt32(Db.Master.GetScalar(sql, parameters));
        }

        public void AdjustCommunityRaveTotal(int communityId, int numRaves)
        {
            string sql = "UPDATE channel_stats ";
            sql += " SET number_of_diggs = number_of_diggs + @numRaves ";
            sql += " WHERE channel_id=@communityId ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@numRaves", numRaves));
            parameters.Add(new MySqlParameter("@communityId", communityId));

            Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// InsertCommunityRave
        /// </summary>
        public int InsertCommunityRave(int userId, int communityId, int numRaves)
        {
            string sql = "CALL add_channel_diggs (@userId, @communityId, @raveCount)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            parameters.Add(new MySqlParameter("@raveDate", DateTime.Now));
            parameters.Add(new MySqlParameter("@raveCount", numRaves));

            return Db.Master.ExecuteNonQuery (sql, parameters);
        }

        public int GetUserMegaRaveCount (int personalChannelId)
        {
            int numRaves = 0;

            string sql = "SELECT COALESCE(COUNT(mega_rave_history_id),0) as raves ";
            sql += " FROM mega_rave_history ";
            sql += " WHERE channel_id=@personalChannelId ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@personalChannelId", personalChannelId));

            DataRow dr = Db.Master.GetDataRow (sql, parameters);

            if (dr != null)
            {
                numRaves = Convert.ToInt32 (dr["raves"]);
            }

            return numRaves;
        }

        #endregion Community/User Raves


        #region Asset Raves

        // Formerly StoreUtility.InsertDigg
        public int InsertAssetRave (int userId, int assetId)
        {
            string sql = "CALL add_asset_diggs(@userId, @assetId, @diggId);  SELECT CAST(@diggId as UNSIGNED INT) as diggId;";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@assetId", assetId));
            return Db.Master.ExecuteNonQuery (sql, parameters);
        }

        public DataRow GetAssetRave (int userId, int assetId)
        {
            string sqlSelect = "select d.digg_id " +
                " FROM asset_diggs d " +
                " WHERE user_id = @userId " +
                " AND asset_id = @assetId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@assetId", assetId));
            return Db.Master.GetDataRow (sqlSelect, parameters);
        }

        public int GetAssetRaveCountByUser (int userId, int assetId)
        {
            int numRaves = 0;

            string sql = "SELECT COALESCE(COUNT(digg_id),0) as raves ";
            sql += " FROM asset_diggs ";
            sql += " WHERE user_id=@userId AND asset_id=@assetId ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@assetId", assetId));

            DataRow dr = Db.Master.GetDataRow (sql, parameters);

            if (dr != null)
            {
                numRaves = Convert.ToInt32 (dr["raves"]);
            }

            return numRaves;
        }

        public void AdjustAssetRaveTotal (int assetId, int numRaves)
        {
            string sql = "UPDATE assets_stats ";
            sql += " SET number_of_diggs = number_of_diggs + @numRaves ";
            sql += " WHERE asset_id=@assetId ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@numRaves", numRaves));
            parameters.Add (new MySqlParameter ("@assetId", assetId));

            Db.Master.ExecuteNonQuery (sql, parameters);
        }

        #endregion Asset Raves

    }
}
