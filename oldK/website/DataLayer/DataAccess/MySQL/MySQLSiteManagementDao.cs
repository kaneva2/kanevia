///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLSiteManagementDao : ISiteManagementDao
    {
        #region Available Website Queries

        /// <summary>
        /// Gets all the available site that this tool manages
        /// </summary>
        /// <returns>IList<WebSite></returns>
        public IList<WebSite> GetAvailableWebsites()
        {
            //create query string to get game by user id
            string sql = "SELECT website_id, website_name, connection_string, website_url FROM sm_available_websites";

            //perform query returning single data row from the developer database
            DataTable dt = Db.SiteManagement.GetDataTable(sql.ToString());

            IList<WebSite> list = new List<WebSite>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new WebSite(Convert.ToInt32(row["website_id"]), row["website_name"].ToString(),
                     row["connection_string"].ToString(), row["website_url"].ToString()));
            }

            return list;
        }

        /// <summary>
        /// Add A New Available Website
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int AddAvailableWebsite(WebSite website)
        {
            // Send the message
            string sql = "CALL add_available_website(@WebsiteName, @ConnectionString, @WebsiteURL)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@WebsiteName", website.WebsiteName));
            parameters.Add(new MySqlParameter("@ConnectionString", website.ConnectionString));
            parameters.Add(new MySqlParameter("@WebsiteURL", website.WebsiteURL));

            return Db.SiteManagement.ExecuteIdentityInsert(sql, parameters);
        }

        /// <summary>
        /// Update Available Website
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int UpdateAvailableWebsite(WebSite website)
        {
            // Send the message
            string sql = "CALL update_available_website(@WebsiteName, @ConnectionString, @WebsiteURL, @WebsiteId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@WebsiteName", website.WebsiteName));
            parameters.Add(new MySqlParameter("@ConnectionString", website.ConnectionString));
            parameters.Add(new MySqlParameter("@WebsiteURL", website.WebsiteURL));
            parameters.Add(new MySqlParameter("@WebsiteId", website.WebsiteId));

            //use the get scalar since the stored proc returns an integer value
            return Convert.ToInt32(Db.SiteManagement.GetScalar(sql, parameters));
        }

        /// <summary>
        /// Delete Available Website
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="userId"></param>
        /// <returns>int</returns>
        public int DeleteAvailableWebsite(int websiteId)
        {
            string sql = "CALL delete_available_website(@websiteId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@websiteId", websiteId));

            return Convert.ToInt32(Db.SiteManagement.GetScalar(sql, parameters));
            //return Db.SiteManagement.ExecuteNonQuery(sql, parameters);
        }

        #endregion

        #region Main Menu

        /// <summary>
        /// Gets the top level (main) menu based on the select website, gets all available menus
        /// </summary>
        /// <param name="owner_id">Unique user identifier.</param>
        /// <returns>IList<SM_MenuItem></returns>
        public IList<SM_MenuItem> GetMainMenu(int websiteId)
        {
            //create query string to get game by user id
            string sql = "SELECT csr_mainnav_id, csr_mainnav_title, privilege_id, website_id FROM sm_mainnav WHERE website_id = @websiteId";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@websiteId", websiteId));

            //perform query returning single data row from the developer database
            DataTable dt = Db.SiteManagement.GetDataTable(sql, parameters);

            IList<SM_MenuItem> list = new List<SM_MenuItem>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SM_MenuItem(Convert.ToInt32(row["csr_mainnav_id"]), row["csr_mainnav_title"].ToString(), Convert.ToInt32(row["privilege_id"]),
                    Convert.ToInt32(row["website_id"]), 0, 0));
            }

            return list;
        }

        /// <summary>
        /// Gets the top level (main) menu based on the select website, gets all available menus
        /// </summary>
        /// <param name="owner_id">Unique user identifier.</param>
        /// <returns>IList<SM_MenuItem></returns>
        public IList<SM_MenuItem> GetMainMenuFiltered(int websiteId, string privilegeList)
        {
            //create query string to get game by user id
            string sql = "SELECT csr_mainnav_id, csr_mainnav_title, privilege_id, website_id FROM sm_mainnav WHERE website_id = @websiteId" + 
            " AND privilege_id IN (" + privilegeList + ")";
            
            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@websiteId", websiteId));

            //perform query returning single data row from the developer database
            DataTable dt = Db.SiteManagement.GetDataTable(sql, parameters);

            IList<SM_MenuItem> list = new List<SM_MenuItem>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SM_MenuItem(Convert.ToInt32(row["csr_mainnav_id"]), row["csr_mainnav_title"].ToString(), Convert.ToInt32(row["privilege_id"]),
                    Convert.ToInt32(row["website_id"]), 0, 0));
            }

            return list;
        }

        /// <summary>
        /// Add A New Available Website
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int AddMainMenuItem(SM_MenuItem menuItem)
        {
            // Send the message
            string sql = "CALL add_main_menu_item(@NavigationTitle, @PrivilegeId, @WebsiteId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@NavigationTitle", menuItem.NavigationTitle));
            parameters.Add(new MySqlParameter("@PrivilegeId", menuItem.PrivilegeId));
            parameters.Add(new MySqlParameter("@WebsiteId", menuItem.WebsiteId));

            return Db.SiteManagement.ExecuteIdentityInsert(sql, parameters);
        }

        /// <summary>
        /// Update Available Website
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int UpdateMainMenuItem(SM_MenuItem menuItem)
        {
            // Send the message
            string sql = "CALL update_main_menu_item(@NavigationTitle, @PrivilegeId, @WebsiteId, @NavigationId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@NavigationTitle", menuItem.NavigationTitle));
            parameters.Add(new MySqlParameter("@PrivilegeId", menuItem.PrivilegeId));
            parameters.Add(new MySqlParameter("@WebsiteId", menuItem.WebsiteId));
            parameters.Add(new MySqlParameter("@NavigationId", menuItem.NavigationId));

            //use the get scalar since the stored proc returns an integer value
            return Convert.ToInt32(Db.SiteManagement.GetScalar(sql, parameters));
        }

        /// <summary>
        /// Delete Available Website
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="userId"></param>
        /// <returns>int</returns>
        public int DeleteMainMenuItem(int menuItemId)
        {
            string sql = "CALL delete_main_menu_item(@menuItemId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@menuItemId", menuItemId));

            return Convert.ToInt32(Db.SiteManagement.GetScalar(sql, parameters));
        }

        #endregion

        #region Submenu

        /// <summary>
        /// Gets the top level (main) menu based on the select 
        /// </summary>
        /// <param name="owner_id">Unique user identifier.</param>
        /// <returns>IList<SM_MenuItem></returns>
        public IList<SM_MenuItem> GetSubMenu(int mainNavId)
        {
            //create query string to get game by user id
            string sql = "SELECT csr_subnav_id, csr_mainnav_id, csr_subnav_title, user_control_id, privilege_id FROM sm_subnav WHERE csr_mainnav_id = @mainNavId";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@mainNavId", mainNavId));

            //perform query returning single data row from the developer database
            DataTable dt = Db.SiteManagement.GetDataTable(sql, parameters);

            IList<SM_MenuItem> list = new List<SM_MenuItem>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SM_MenuItem(Convert.ToInt32(row["csr_subnav_id"]), row["csr_subnav_title"].ToString(), Convert.ToInt32(row["privilege_id"]),
                    0, Convert.ToInt32(row["user_control_id"]), Convert.ToInt32(row["csr_mainnav_id"])));
            }

            return list;
        }

        public IList<SM_MenuItem> GetSubMenuFiltered(int mainNavId, string privilegeList)
        {
            //create query string to get game by user id
            string sql = "SELECT csr_subnav_id, csr_mainnav_id, csr_subnav_title, user_control_id, privilege_id FROM sm_subnav WHERE csr_mainnav_id = @mainNavId" + 
            " AND privilege_id IN (" + privilegeList + ")";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@mainNavId", mainNavId));

            //perform query returning single data row from the developer database
            DataTable dt = Db.SiteManagement.GetDataTable(sql, parameters);

            IList<SM_MenuItem> list = new List<SM_MenuItem>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SM_MenuItem(Convert.ToInt32(row["csr_subnav_id"]), row["csr_subnav_title"].ToString(), Convert.ToInt32(row["privilege_id"]),
                    0, Convert.ToInt32(row["user_control_id"]), Convert.ToInt32(row["csr_mainnav_id"])));
            }

            return list;
        }

        /// <summary>
        /// Add A New submenu item
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int AddSubMenuItem(SM_MenuItem subMenuItem)
        {
            // Send the message
            string sql = "CALL add_submenu_item(@ParentNavId, @NavigationTitle, @UserControlId, @PrivilegeId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@ParentNavId", subMenuItem.ParentNavId));
            parameters.Add(new MySqlParameter("@NavigationTitle", subMenuItem.NavigationTitle));
            parameters.Add(new MySqlParameter("@UserControlId", subMenuItem.UserControlId));
            parameters.Add(new MySqlParameter("@PrivilegeId", subMenuItem.PrivilegeId));

            return Db.SiteManagement.ExecuteIdentityInsert(sql, parameters);
        }

        /// <summary>
        /// Update submenu item
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int UpdateSubMenuItem(SM_MenuItem subMenuItem)
        {
            // Send the message
            string sql = "CALL update_submenu_item(@ParentNavId, @NavigationTitle, @UserControlId, @PrivilegeId, @NavigationId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@ParentNavId", subMenuItem.ParentNavId));
            parameters.Add(new MySqlParameter("@NavigationTitle", subMenuItem.NavigationTitle));
            parameters.Add(new MySqlParameter("@UserControlId", subMenuItem.UserControlId));
            parameters.Add(new MySqlParameter("@PrivilegeId", subMenuItem.PrivilegeId));
            parameters.Add(new MySqlParameter("@NavigationId", subMenuItem.NavigationId));

            //use the get scalar since the stored proc returns an integer value
            return Convert.ToInt32(Db.SiteManagement.GetScalar(sql, parameters));
        }

        /// <summary>
        /// Delete sub menu item
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="userId"></param>
        /// <returns>int</returns>
        public int DeleteSubMenuItem(int subMenuItemId)
        {
            string sql = "CALL delete_submenu_item(@subMenuItemId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@subMenuItemId", subMenuItemId));

            return Convert.ToInt32(Db.SiteManagement.GetScalar(sql, parameters));
        }

        #endregion

        #region User Controls
        /// <summary>
        /// Add A New submenu item
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int AddUserControl(KanevaUserControl userControl)
        {
            // Send the message
            string sql = "CALL add_usercontrol(@userControlName, @controlTypeId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userControlName", userControl.UserControlName));
            parameters.Add(new MySqlParameter("@controlTypeId", userControl.ControlTypeId));

            return Db.SiteManagement.ExecuteIdentityInsert(sql, parameters);
        }

        /// <summary>
        /// Update submenu item
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int UpdateUserControl(KanevaUserControl userControl)
        {
            // Send the message
            string sql = "CALL update_usercontrol(@userControlName, @controlTypeId, @userControlId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userControlName", userControl.UserControlName));
            parameters.Add(new MySqlParameter("@controlTypeId", userControl.ControlTypeId));
            parameters.Add(new MySqlParameter("@userControlId", userControl.UserControlId));

            //use the get scalar since the stored proc returns an integer value
            return Convert.ToInt32(Db.SiteManagement.GetScalar(sql, parameters));
        }

        /// <summary>
        /// Delete user control
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="userId"></param>
        /// <returns>int</returns>
        public int DeleteUserControl(int userControlId)
        {
            string sql = "CALL delete_usercontrol(@userControlId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userControlId", userControlId));

            return Convert.ToInt32(Db.SiteManagement.GetScalar(sql, parameters));
        }

        /// <summary>
        /// Gets the top level (main) menu based on the select 
        /// </summary>
        /// <param name="owner_id">Unique user identifier.</param>
        /// <returns>DataRow</returns>
        public KanevaUserControl GetUserControlByID(int userControlId)
        {
            //get's list of results (should only be one result)
            IList<KanevaUserControl> usercontrol = GetUserControls(userControlId,null);

            //retrieve control
            return (KanevaUserControl)usercontrol[0];
        }

        /// <summary>
        /// Gets the top level (main) menu based on the select 
        /// </summary>
        /// <param name="owner_id">Unique user identifier.</param>
        /// <returns>DataRow</returns>
        public KanevaUserControl GetUserControlByName(string name)
        {
            //get's list of results (should only be one result)
            IList<KanevaUserControl> usercontrol = GetUserControls(0,name);

            //retrieve control
            return (KanevaUserControl)usercontrol[0];
        }

        /// <summary>
        /// Gets the top level (main) menu based on the select 
        /// </summary>
        /// <param name="owner_id">Unique user identifier.</param>
        /// <returns>DataRow</returns>
        public IList<KanevaUserControl> GetUserControls(int userControlId, string name)
        {
            //create query string to get game by user id
            string sql = "SELECT user_control_id, user_control_name, control_type_id FROM user_controls";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (userControlId > 0)
            {
                sql += " WHERE user_control_id = @userControlId";
                parameters.Add(new MySqlParameter("@userControlId", userControlId));
            }
            else if ((name != null) && (name.Length  >0 ))
            {
                sql += " WHERE user_control_name = @user_control_name";
                parameters.Add(new MySqlParameter("@user_control_name", name));
            }

            //perform query returning single data row from the developer database
            DataTable dt = Db.SiteManagement.GetDataTable(sql, parameters);

            IList<KanevaUserControl> list = new List<KanevaUserControl>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new KanevaUserControl(Convert.ToInt32(row["user_control_id"]), Convert.ToInt32(row["control_type_id"]), row["user_control_name"].ToString(), ""));
            }

            return list;
        }

        /// <summary>
        /// Gets the top level (main) menu based on the select 
        /// </summary>
        /// <param name="owner_id">Unique user identifier.</param>
        /// <returns>string</returns>
        public string GetUserControlPath(int userControlId)
        {
            string usercontrol = "";

            //create query string to get game by user id
            string sql = "SELECT user_control_name FROM user_controls WHERE user_control_id = @userControlId";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userControlId", userControlId));

            //perform query returning single data row from the developer database
            DataRow dr = Db.SiteManagement.GetDataRow(sql, parameters);

            if (!dr.IsNull("user_control_path"))
            {
                usercontrol = dr["user_control_path"].ToString();
            }
            return usercontrol;
        }

        /// <summary>
        /// Gets the types of user controls
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable GetUserControlTypes()
        {
            //create query string to get game by user id
            string sql = "SELECT control_type_id, control_type FROM user_control_type";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            return Db.SiteManagement.GetDataTable(sql, parameters);
        }

        #endregion

        #region Misc

        /// <summary>
        /// InsertCSRLog
        /// </summary>
        public int InsertCSRLog(int userId, string activity, int assetId, int updatedUserId)
        {
            string sAssetId = (assetId.Equals(0)) ? "NULL" : assetId.ToString();
            string sUserId = (updatedUserId.Equals(0)) ? "NULL" : updatedUserId.ToString();

            string sqlSelect = "INSERT INTO csr_logs " +
                " (user_id, activity, asset_id, updated_user_id, created_datetime ) " +
                " VALUES (@userId, @activity, " + sAssetId + "," + sUserId + ", NOW())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@activity", activity));
            return Db.Audit.ExecuteIdentityInsert(sqlSelect, parameters);
        }

        /// <summary>
        /// GetNumberChannelShares
        /// </summary>
        public int GetNumberChannelShares(DateTime dtStartDate, DateTime dtEndDate)
        {
            string sqlSelect = sqlSelect = "SELECT COALESCE(COUNT(share_id),0) as count " +
                " FROM community_shares " +
                " WHERE created_date >= @startDate" +
                " AND created_date <= @endDate";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@startDate", (dtStartDate)));
            parameters.Add(new MySqlParameter("@endDate", (dtEndDate)));
            return Convert.ToInt32 (Db.Audit.GetScalar (sqlSelect, parameters));
        }

        /// <summary>
        /// GetNumberChannelShares
        /// </summary>
        /// <param name="dtStartDate"></param>
        /// <param name="dtEndDate"></param>
        /// <returns></returns>
        public int GetNumberPublicChannelShares(DateTime dtStartDate, DateTime dtEndDate)
        {
            string sqlSelect = sqlSelect = "SELECT COALESCE(COUNT(share_id),0) as count " +
                " FROM community_shares cs " +
                " INNER JOIN kaneva.communities_public cp " +
                " ON cs.community_id = cp.community_id " +
                " WHERE cs.created_date >= @startDate" +
                " AND cs.created_date <= @endDate";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@startDate", (dtStartDate)));
            parameters.Add(new MySqlParameter("@endDate", (dtEndDate)));
            return Convert.ToInt32 (Db.Audit.GetScalar (sqlSelect, parameters));
        }

        /// <summary>
        /// GetNumberChannelShares
        /// </summary>
        /// <param name="dtStartDate"></param>
        /// <param name="dtEndDate"></param>
        /// <returns></returns>
        public int GetNumberPersonalChannelShares(DateTime dtStartDate, DateTime dtEndDate)
        {
            string sqlSelect = "SELECT COALESCE(COUNT(share_id),0) as count " +
                " FROM community_shares cs " +
                " INNER JOIN kaneva.communities_personal cp " +
                " ON cs.community_id = cp.community_id " +
                " WHERE cs.created_date >= @startDate" +
                " AND cs.created_date <= @endDate";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@startDate", (dtStartDate)));
            parameters.Add(new MySqlParameter("@endDate", (dtEndDate)));
            return Convert.ToInt32 (Db.Audit.GetScalar (sqlSelect, parameters));
        }


        /// <summary>
        /// GetNumberAssetShares
        /// </summary>
        public int GetNumberAssetShares(DateTime dtStartDate, DateTime dtEndDate)
        {
            string sqlSelect = "SELECT COALESCE(COUNT(share_id),0) as count " +
                    " FROM asset_shares " +
                    " WHERE created_date >= @startDate" +
                    " AND created_date <= @endDate";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@startDate", (dtStartDate)));
            parameters.Add(new MySqlParameter("@endDate", (dtEndDate)));
            return Convert.ToInt32(Db.Audit.GetScalar(sqlSelect, parameters));
        }

        #endregion

        /*      /// <summary>
                      /// Gets a Game.
                      /// </summary>
                      /// <param name="gameName">Unique user identifier.</param>
                      /// <returns>Game.</returns>
                      public Game GetGame(string gameName)
                      {
                          //create query string to get game by user id
                          string sql = "SELECT g.game_id, g.owner_id, g.game_rating_id, g.game_status_id, g.game_name, g.game_description, " +
                              " g.game_synopsis, g.game_creation_date, g.game_access_id, g.game_image_path, g.game_keywords, " +
                              " g.last_updated, g.modifiers_id, gs.number_of_comments, gs.number_of_raves, gs.number_of_views, g.game_encryption_key " +
                              " FROM games g " +
                              " INNER JOIN game_stats gs ON g.game_id = gs.game_id " +
                              " WHERE g.game_name = @gameName ";

                          //indicate any parameters for place holder variables in query string
                          List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                          parameters.Add(new MySqlParameter("@gameName", gameName));

                          //perform query returning single data row from the developer database
                          DataRow row = Db.Developer.GetDataRow(sql.ToString(), parameters);

                          //populate new instance of Game Object
                          return new Game(Convert.ToInt32(row["game_id"]), Convert.ToInt32(row["owner_id"]), Convert.ToInt32(row["game_rating_id"]),
                              Convert.ToInt32(row["game_status_id"]), Convert.ToInt32(row["game_access_id"]), Convert.ToInt32(row["modifiers_id"]),
                              Convert.ToInt32(row["number_of_comments"]), Convert.ToInt32(row["number_of_raves"]), Convert.ToInt32(row["number_of_views"]),
                              Convert.ToInt32(row["number_of_players"]), row["game_name"].ToString(), row["game_description"].ToString(), row["game_synopsis"].ToString(),
                              row["game_image_path"].ToString(), row["game_keywords"].ToString(),
                              Convert.ToDateTime(row["game_creation_date"]), Convert.ToDateTime(row["last_updated"]), row["game_encryption_key"].ToString());
                      }

                      /// <summary>
                      /// Gets all registered games.
                      /// </summary>
                      /// <param name="filter">filtering on query</param>
                      /// <param name="orderby">ordering on query</param>
                      /// <returns>IList<Game></returns>
                      public IList<Game> GetAllGames(string filter, string orderby)
                      {
                          return GetGamesByOwner(0, filter, orderby);
                      }

                      /// <summary>
                      /// Gets all registered games by an owner.
                      /// </summary>
                      /// <param name="ownerId">limit results to specific user</param>
                      /// <param name="filter">filtering on query</param>
                      /// <param name="orderby">ordering on query</param>
                      /// <param name="pageNumber">page number for record set</param>
                      /// <param name="pageSize">how many records in page</param>
                      /// <returns>Game.</returns>
                      public IList<Game> GetGamesByOwner(int ownerId, string filter, string orderby)
                      {
                          // UNION WAY
                          string strQuery = "";
                          string strWhere = "";
                          string strOrderBy = "";

                          List<IDbDataParameter> parameters = new List<IDbDataParameter>();

                          // Friends I invited
                          strQuery = "SELECT g.game_id, g.owner_id, g.game_rating_id, g.game_status_id, g.game_name, g.game_description, " +
                              " g.game_synopsis, g.game_creation_date, g.game_access_id, g.game_image_path, g.game_keywords, " +
                              " g.last_updated, g.modifiers_id, gs.number_of_comments, gs.number_of_raves, gs.number_of_views, gs.number_of_players, g.game_encryption_key ";

                          strQuery += " FROM games g " +
                              " INNER JOIN game_stats gs ON g.game_id = gs.game_id ";

                          if (ownerId > 0)
                          {
                              strWhere += " WHERE g.owner_id = @ownerId ";
                              parameters.Add(new MySqlParameter("@ownerId", ownerId));
                          }

                          if (filter.Length > 0)
                          {
                              if (strWhere.Length > 0)
                              {
                                  strWhere += " AND " + filter;
                              }
                              else
                              {
                                  strWhere += " WHERE " + filter;
                              }
                          }

                          if (orderby != null && orderby.Length > 0)
                          {
                              strOrderBy = " ORDER BY " + orderby;
                          }

                          strQuery = strQuery + strWhere + strOrderBy;

                          DataTable dt = Db.Developer.GetDataTable(strQuery, parameters);

                          IList<Game> list = new List<Game>();
                          foreach (DataRow row in dt.Rows)
                          {
                              list.Add(new Game(Convert.ToInt32(row["game_id"]), Convert.ToInt32(row["owner_id"]), Convert.ToInt32(row["game_rating_id"]),
                              Convert.ToInt32(row["game_status_id"]), Convert.ToInt32(row["game_access_id"]), Convert.ToInt32(row["modifiers_id"]),
                              Convert.ToInt32(row["number_of_comments"]), Convert.ToInt32(row["number_of_raves"]), Convert.ToInt32(row["number_of_views"]),
                              Convert.ToInt32(row["number_of_players"]), row["game_name"].ToString(), row["game_description"].ToString(), row["game_synopsis"].ToString(), row["game_image_path"].ToString(),
                              row["game_keywords"].ToString(), Convert.ToDateTime(row["game_creation_date"]), Convert.ToDateTime(row["last_updated"]), row["game_encryption_key"].ToString()));
                          }
                          return list;
                      }

                      /// <summary>
                      /// Gets all games matching the search criteria
                      /// </summary>
                      /// <param name="keywords">limit results to specific keywords</param>
                      /// <param name="filter">filtering on query</param>
                      /// <param name="orderby">ordering on query</param>
                      /// <param name="pageNumber">page number for record set</param>
                      /// <param name="pageSize">how many records in page</param>
                      /// <returns>PagedDataTable</returns>
                      public PagedDataTable SearchForGames(string searchString, string filter, string categories, bool bGetMature, bool bThumbnailRequired, int pastDays, int ownerId, string orderBy, int pageNumber, int pageSize)
                      {
                          // UNION WAY
                          string strQuery = "";
                          string strFROM = "";
                          string strWhere = "";
                          string strOrderBy = "";

                          List<IDbDataParameter> parameters = new List<IDbDataParameter>();

                          // 
                          strQuery = "SELECT DISTINCT(g.game_id), g.owner_id, g.game_rating_id, g.game_status_id, g.game_name, g.game_description, " +
                              " g.game_synopsis, g.game_creation_date, g.game_access_id, g.game_image_path, g.game_keywords, " +
                              " g.last_updated, g.modifiers_id, gs.number_of_comments, gs.number_of_raves, gs.number_of_views, g.game_encryption_key, " +
                              " gs.number_of_players, dc.company_name as owner_display_name, dc.company_name as owner_username ";

                          strFROM += " FROM games g " +
                              " INNER JOIN game_stats gs ON g.game_id = gs.game_id " +
                              " INNER JOIN development_companies dc ON g.owner_id = dc.company_id ";

                          strWhere += "WHERE g.game_access_id = " + GAME_IS_PUBLIC;

                          if (bThumbnailRequired)
                          {
                              strWhere += " AND g.game_image_path IS NOT NULL ";
                          }

                          if (ownerId > 0)
                          {
                              strWhere += " AND g.owner_id = @ownerId ";
                              parameters.Add(new MySqlParameter("@ownerId", ownerId));
                          }

                          // Only get last x number of days?
                          if (pastDays > 0)
                          {
                              strWhere += " AND g.game_creation_date > SUBDATE(NOW(),INTERVAL " + pastDays + " DAY) ";
                          }

                          // Mature
                          if (!bGetMature)
                          {
                              strWhere += " AND gr.is_mature = 0 ";
                              strFROM += " INNER JOIN game_ratings gr ON g.game_rating_id = gr.game_rating_id ";
                          }

                          if ((categories != null) && (categories.Trim().Length > 0))
                          {
                              strWhere += " AND (gtc.category_id IN (" + categories + "))";
                              strFROM += " LEFT JOIN games_to_categories gtc ON g.game_id = gtc.game_id ";
                          }

                          // Search
                          if ((filter != null) && (filter.Trim().Length > 0))
                          {
                              strWhere += " AND " + filter + " ";
                          }

                          // Search
                          if ((searchString != null) && (searchString.Trim().Length > 0))
                          {
                              char[] splitter = { ' ' };
                              string[] arKeywords = null;

                              // Did they enter multiples?
                              arKeywords = searchString.Split(splitter);

                              if (arKeywords.Length > 1)
                              {
                                  searchString = "";
                                  for (int j = 0; j < arKeywords.Length; j++)
                                  {
                                      searchString += arKeywords[j].ToString() + "* ";
                                  }
                              }
                              else
                              {
                                  searchString = searchString + "*";
                              }

                              strWhere += " AND MATCH (game_name, game_description, game_synopsis, game_keywords) AGAINST (@searchString IN BOOLEAN MODE) ";
                              parameters.Add(new MySqlParameter("@searchString", searchString));

                              // If no order by, do relevancy
                              if (orderBy.Length.Equals(0))
                              {
                                  parameters.Add(new MySqlParameter("@searchStringOrig", searchString));
                                  strWhere += ", MATCH (game_name, game_description, game_synopsis, game_keywords) AGAINST (@searchStringOrig) as rel";
                                  orderBy = " rel DESC ";
                              }
                          }

                          if (orderBy != null && orderBy.Length > 0)
                          {
                              strOrderBy = orderBy + " DESC ";
                          }

                          strQuery = strQuery + strFROM + strWhere;

                          return Db.Developer.GetPagedDataTable(strQuery, strOrderBy, parameters, pageNumber, pageSize);

                      }

                      /// <summary>
                      /// Gets all registered games by an owner.
                      /// </summary>
                      /// <param name="ownerId">limit results to specific user</param>
                      /// <param name="filter">filtering on query</param>
                      /// <param name="orderby">ordering on query</param>
                      /// <param name="pageNumber">page number for record set</param>
                      /// <param name="pageSize">how many records in page</param>
                      /// <returns>PagedDataTable</returns>
                      public PagedDataTable GetGamesByOwner(int ownerId, string filter, string orderby, int pageNumber, int pageSize)
                      {
                          // UNION WAY
                          string strQuery = "";
                          string strWhere = "";
                          string strOrderBy = "";

                          List<IDbDataParameter> parameters = new List<IDbDataParameter>();

                          // Friends I invited
                          strQuery = "SELECT g.game_id, g.owner_id, g.game_rating_id, g.game_status_id, g.game_name, g.game_description, " +
                              " g.game_synopsis, g.game_creation_date, g.game_access_id, g.game_image_path, g.game_keywords, " +
                              " g.last_updated, g.modifiers_id, gs.number_of_comments, gs.number_of_raves, gs.number_of_views, gs.number_of_players, g.game_encryption_key ";

                          strQuery += " FROM games g " +
                              " INNER JOIN game_stats gs ON g.game_id = gs.game_id ";

                          if (ownerId > 0)
                          {
                              strWhere += " WHERE g.owner_id = @ownerId ";
                              parameters.Add(new MySqlParameter("@ownerId", ownerId));
                          }

                          if (filter.Length > 0)
                          {
                              if (strWhere.Length > 0)
                              {
                                  strWhere += " AND " + filter;
                              }
                              else
                              {
                                  strWhere += " WHERE " + filter;
                              }
                          }

                          if (orderby != null && orderby.Length > 0)
                          {
                              strOrderBy = orderby;
                          }

                          strQuery = strQuery + strWhere;

                          return Db.Developer.GetPagedDataTable(strQuery, strOrderBy, parameters, pageNumber, pageSize);
                      }

                      //--manage game functions---
                      /// <summary>
                      /// Add A New Game
                      /// </summary>
                      /// <param name="game"></param>
                      /// <returns>int</returns>
                      public int AddNewGame(Game game)
                      {
                          // Send the message
                          string sql = "INSERT INTO games " +
                              "(owner_id, game_rating_id, game_status_id, game_name, game_description, " +
                              " game_synopsis, game_creation_date, game_access_id, game_image_path, game_keywords, game_encryption_key, " +
                              " last_updated, modifiers_id) VALUES (@owner_id, @game_rating_id, @game_status_id, @game_name, @game_description, " +
                              " @game_synopsis, @game_creation_date, @game_access_id, @game_image_path, @game_keywords, @game_encryption_key, " +
                              " @last_updated, @modifiers_id)";

                          List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                          parameters.Add(new MySqlParameter("@owner_id", game.OwnerId));
                          parameters.Add(new MySqlParameter("@game_rating_id", game.GameRatingId));
                          parameters.Add(new MySqlParameter("@game_status_id", game.GameStatusId));
                          parameters.Add(new MySqlParameter("@game_name", game.GameName));
                          parameters.Add(new MySqlParameter("@game_description", game.GameDescription));
                          parameters.Add(new MySqlParameter("@game_synopsis", game.GameSynopsis));
                          parameters.Add(new MySqlParameter("@game_creation_date", game.GameCreationDate));
                          parameters.Add(new MySqlParameter("@game_access_id", game.GameAccessId));
                          parameters.Add(new MySqlParameter("@game_image_path", (game.GameImagePath.Equals("") ? null : game.GameImagePath)));
                          parameters.Add(new MySqlParameter("@game_keywords", game.GameKeyWords));
                          parameters.Add(new MySqlParameter("@game_encryption_key", game.GameEncryptionKey));
                          parameters.Add(new MySqlParameter("@last_updated", game.LastUpdated));
                          parameters.Add(new MySqlParameter("@modifiers_id", game.GameModifiersId));

                          return Db.Developer.ExecuteIdentityInsert(sql, parameters);
                      }

                      //--manage game functions---
                      /// <summary>
                      /// Update Game
                      /// </summary>
                      /// <param name="game"></param>
                      /// <returns>int</returns>
                      public int UpdateGame(Game game)
                      {
                          // Send the message
                          string sql = "UPDATE games SET " +
                              "owner_id = @owner_id, game_rating_id = @game_rating_id, game_status_id = @game_status_id, game_name = @game_name, game_description = @game_description, " +
                              " game_synopsis = @game_synopsis, game_creation_date = @game_creation_date, game_access_id = @game_access_id, game_image_path = @game_image_path, " +
                              " game_keywords = @game_keywords, last_updated = @last_updated, modifiers_id = @modifiers_id WHERE game_id =  @game_id";

                          List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                          parameters.Add(new MySqlParameter("@owner_id", game.OwnerId));
                          parameters.Add(new MySqlParameter("@game_rating_id", game.GameRatingId));
                          parameters.Add(new MySqlParameter("@game_status_id", game.GameStatusId));
                          parameters.Add(new MySqlParameter("@game_name", game.GameName));
                          parameters.Add(new MySqlParameter("@game_description", game.GameDescription));
                          parameters.Add(new MySqlParameter("@game_synopsis", game.GameSynopsis));
                          parameters.Add(new MySqlParameter("@game_creation_date", game.GameCreationDate));
                          parameters.Add(new MySqlParameter("@game_access_id", game.GameAccessId));
                          parameters.Add(new MySqlParameter("@game_image_path", (game.GameImagePath.Equals("") ? null : game.GameImagePath)));
                          parameters.Add(new MySqlParameter("@game_keywords", game.GameKeyWords));
                          parameters.Add(new MySqlParameter("@last_updated", game.LastUpdated));
                          parameters.Add(new MySqlParameter("@modifiers_id", game.GameModifiersId));
                          parameters.Add(new MySqlParameter("@game_id", game.GameId));

                          return Db.Developer.ExecuteNonQuery(sql, parameters);
                      }
                      /// <summary>
                      /// Update Game
                      /// </summary>
                      /// <param name="game"></param>
                      /// <returns>int</returns>
                      public int UpdateGameThumbnail(int gameId, string imagePath)
                      {
                          // Send the message
                          string sql = "UPDATE games SET game_image_path = @imagePath WHERE game_id = @gameId";

                          List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                          parameters.Add(new MySqlParameter("@gameId", gameId));
                          parameters.Add(new MySqlParameter("@imagePath", imagePath));

                          return Db.Developer.ExecuteNonQuery(sql, parameters);
                      }

                      //--manage game functions---
                      /// <summary>
                      /// Delete Game
                      /// </summary>
                      /// <param name="gameId"></param>
                      /// <param name="userId"></param>
                      /// <returns>int</returns>
                      public int DeleteGame(int gameId, int userId)
                      {
                          string sql = "CALL delete_game (@game_id, @user_id)";

                          List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                          parameters.Add(new MySqlParameter("@game_id", gameId));
                          parameters.Add(new MySqlParameter("@user_id", userId));
                          return Db.Developer.ExecuteNonQuery(sql, parameters);
                      }

                      /// <summary>
                      /// GetGameRatings
                      /// </summary>
                      /// <returns></returns>
                      public DataTable GetGameRatings()
                      {
                          string strQuery = "SELECT game_rating_id, game_rating, description, minimum_age, is_mature " +
                              " FROM game_ratings ";

                          return Db.Developer.GetDataTable(strQuery);
                      }

                      //pull down choice functions
                      /// <summary>
                      /// Get Game Status Options
                      /// </summary>
                      /// <returns></returns>
                      public DataTable GetGameStatus()
                      {
                          string strQuery = "SELECT game_status_id, game_status " +
                              " FROM game_status ";

                          return Db.Developer.GetDataTable(strQuery);
                      }

                      /// <summary>
                      /// Get Game Access Options
                      /// </summary>
                      /// <returns></returns>
                      public DataTable GetGameAccess()
                      {
                          string strQuery = "SELECT game_access_id, game_access" +
                              " FROM game_access ";

                          return Db.Developer.GetDataTable(strQuery);
                      }

                      /// <summary>
                      /// Get Game Category options
                      /// </summary>
                      /// <returns></returns>
                      public DataTable GetGameCategories()
                      {
                          string strQuery = "SELECT category_id, description " +
                              " FROM game_categories ";

                          return Db.Developer.GetDataTable(strQuery);
                      }

                      /// <summary>
                      /// Get Categories A Game Is In
                      /// </summary>
                      /// <returns></returns>
                      public DataTable GetGameCategories(int gameId)
                      {
                          string strQuery = "SELECT gc.category_id, gc.description" +
                              " FROM games_to_categories g2c INNER JOIN game_categories gc ON g2c.category_id = gc.category_id WHERE game_id = @gameId";

                          List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                          parameters.Add(new MySqlParameter("@gameId", gameId));

                          return Db.Developer.GetDataTable(strQuery, parameters);
                      }

                      //--manage game functions---
                      /// <summary>
                      /// Delete a Games's categories
                      /// </summary>
                      /// <param name="gameId"></param>
                      /// <returns>int</returns>
                      public int DeleteGameCategories(int gameId)
                      {
                          // Send the message
                          string sql = "DELETE FROM games_to_categories WHERE game_id =  @gameId";

                          List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                          parameters.Add(new MySqlParameter("@gameId", gameId));

                          return Db.Developer.ExecuteNonQuery(sql, parameters);
                      }

                      //--manage game functions---
                      /// <summary>
                      /// Add new category for a game
                      /// </summary>
                      /// <param name="game"></param>
                      /// <returns>int</returns>
                      public int InsertGameCategories(int gameId, int categoryId)
                      {
                          // Send the message
                          string sql = "INSERT INTO games_to_categories " +
                              "(game_id, category_id) VALUES (@gameId, @categoryId)";

                          List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                          parameters.Add(new MySqlParameter("@gameId", gameId));
                          parameters.Add(new MySqlParameter("@categoryId", categoryId));

                          return Db.Developer.ExecuteNonQuery(sql, parameters);
                      }

                      /// <summary>
                      /// Insert a rave
                      /// </summary>
                      /// <param name="userId"></param>
                      /// <param name="gameId"></param>
                      /// <returns>int</returns>
                      public int InsertGameRave(int userId, int gameId)
                      {
                          string sql = "INSERT INTO game_raves " +
                              "(user_id, game_id, rave_date " +
                              ") VALUES (" +
                              "@userId, @gameId, @raveDate)";

                          List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                          parameters.Add(new MySqlParameter("@userId", userId));
                          parameters.Add(new MySqlParameter("@gameId", gameId));
                          parameters.Add(new MySqlParameter("@raveDate", DateTime.Now));

                          return Db.Developer.ExecuteNonQuery(sql, parameters);
                      }*/
    }
}
