///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;


namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MYSQLPromotionsDao : IPromotionsDao
    {
        /// <summary>
        /// Get the List of point buckets
        /// </summary>
        /// <returns>List<Promotion></returns>
        public List<Promotion> GetActivePromotions(string promoOfferTypeIDList)
        {
            return GetActivePromotions(promoOfferTypeIDList, Promotion.CURR_KPOINT);
        }

        /// <summary>
        /// Get the List of point buckets
        /// </summary>
        /// <returns>List<Promotion></returns>
        public List<Promotion> GetActivePromotions(string promoOfferTypeIDList, string keiPointType)
        {
            //minimum point amount is 0.0 
            return GetActivePromotions(promoOfferTypeIDList, keiPointType, 0.0);
        }

        /// <summary>
        /// Get the List of point buckets
        /// </summary>
        /// <returns></returns>
        public List<Promotion> GetActivePromotions(string promoOfferTypeIDList, string keiPointType, Double minimumPointAmount)
        {
            return GetActivePromotions(promoOfferTypeIDList, keiPointType, minimumPointAmount, DateTime.Now);
        }

        /// <summary>
        /// Get the List of point buckets
        /// </summary>
        /// <returns></returns>
        public List<Promotion> GetActivePromotions(string promoOfferTypeIDList, string keiPointType, Double minimumPointAmount, DateTime dateNOW)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sqlSelect = "SELECT promotion_id, kei_point_id, dollar_amount, kei_point_amount, free_points_awarded_amount, free_kei_point_ID," +
                " is_special, promotion_description, promotional_offers_type_id, wok_pass_group_id,  value_of_credits, modifiers_id, sku, bundle_title," +
                " bundle_subheading1, bundle_subheading2, promotion_start, promotion_end, promotion_list_heading, highlight_color, original_price, adrotator_xml_path, " +
                " special_background_color, special_background_image, special_sticker_image, promotional_package_label, special_font_color, use_display_options " +
                " FROM promotional_offers " +
                " WHERE kei_point_id = @keiPointType " +
                " AND promotional_offers_type_id IN (" + promoOfferTypeIDList + ") " +
                " AND (@dateNOW between promotion_start AND promotion_end) ";

            if (minimumPointAmount > 0)
            {
                parameters.Add(new MySqlParameter("@minimumPointAmount", minimumPointAmount));
                sqlSelect += " AND ((kei_point_amount + free_points_awarded_amount) >= @minimumPointAmount)";
            }

            sqlSelect += " ORDER BY dollar_amount ";

            parameters.Add(new MySqlParameter("@keiPointType", keiPointType));
            //parameters.Add(new MySqlParameter("@promoOfferTypeIDList", promoOfferTypeIDList));
            parameters.Add(new MySqlParameter("@dateNOW", dateNOW));

            DataTable dt = Db.Master.GetDataTable(sqlSelect, parameters);

            List<Promotion> list = new List<Promotion>();

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Promotion(Convert.ToUInt32(row["promotion_id"]), row["kei_point_id"].ToString(), Convert.ToDecimal(row["dollar_amount"]),
                Convert.ToDecimal(row["kei_point_amount"]), Convert.ToDecimal(row["free_points_awarded_amount"]), row["free_kei_point_ID"].ToString(),
                Convert.ToUInt32(row["is_special"]), row["promotion_description"].ToString(), Convert.ToUInt32(row["promotional_offers_type_id"]),
                Convert.ToUInt32(row["wok_pass_group_id"]), Convert.ToDecimal(row["value_of_credits"]), Convert.ToUInt32(row["modifiers_id"]),
                Convert.ToUInt32(row["sku"]), row["bundle_title"].ToString(), row["bundle_subheading1"].ToString(), row["bundle_subheading2"].ToString(),
                Convert.ToDateTime(row["promotion_start"]), Convert.ToDateTime(row["promotion_end"]), row["promotion_list_heading"].ToString(),
                row["highlight_color"].ToString(), row["special_background_color"].ToString(), row["special_background_image"].ToString(),
                row["special_sticker_image"].ToString(), row["promotional_package_label"].ToString(), row["special_font_color"].ToString(),
                Convert.ToUInt32(row["use_display_options"]), Convert.ToDecimal(row["original_price"]), row["adrotator_xml_path"].ToString()));
            }

            return list;

        }


        /// <summary>
        /// Get promotion by promotion id
        /// </summary>
        /// <returns></returns>
        public Promotion GetPromotionsByPromoId(int promotionalID)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sqlSelect = "SELECT promotion_id, kei_point_id, dollar_amount, kei_point_amount, free_points_awarded_amount, free_kei_point_ID," +
                " is_special, promotion_description, promotional_offers_type_id, wok_pass_group_id,  value_of_credits, modifiers_id, sku, bundle_title," +
                " bundle_subheading1, bundle_subheading2, promotion_start, promotion_end, promotion_list_heading, highlight_color, original_price, adrotator_xml_path, " +
                " special_background_color, special_background_image, special_sticker_image, promotional_package_label, special_font_color, use_display_options " +
                " FROM promotional_offers " +
                " WHERE promotion_id = @promotionalID";

            parameters.Add(new MySqlParameter("@promotionalID", promotionalID));

            DataRow row = Db.Master.GetDataRow(sqlSelect, parameters);

            return new Promotion(Convert.ToUInt32(row["promotion_id"]), row["kei_point_id"].ToString(), Convert.ToDecimal(row["dollar_amount"]),
                Convert.ToDecimal(row["kei_point_amount"]), Convert.ToDecimal(row["free_points_awarded_amount"]), row["free_kei_point_ID"].ToString(),
                Convert.ToUInt32(row["is_special"]), row["promotion_description"].ToString(), Convert.ToUInt32(row["promotional_offers_type_id"]),
                Convert.ToUInt32(row["wok_pass_group_id"]), Convert.ToDecimal(row["value_of_credits"]), Convert.ToUInt32(row["modifiers_id"]),
                Convert.ToUInt32(row["sku"]), row["bundle_title"].ToString(), row["bundle_subheading1"].ToString(), row["bundle_subheading2"].ToString(),
                Convert.ToDateTime(row["promotion_start"]), Convert.ToDateTime(row["promotion_end"]), row["promotion_list_heading"].ToString(),
                row["highlight_color"].ToString(), row["special_background_color"].ToString(), row["special_background_image"].ToString(),
                row["special_sticker_image"].ToString(), row["promotional_package_label"].ToString(), row["special_font_color"].ToString(),
                Convert.ToUInt32(row["use_display_options"]), Convert.ToDecimal(row["original_price"]), row["adrotator_xml_path"].ToString());
        }

        /// <summary>
        /// Gets all of the promotions of the provided promotion type that have a subcription
        /// </summary>
        /// <returns></returns>
        public List<Promotion> GetPromotionsWithSubscriptionByPromoType(int promotionTypeID)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sqlSelect = "SELECT po.promotion_id, kei_point_id, dollar_amount, kei_point_amount, free_points_awarded_amount, free_kei_point_ID," +
                " is_special, promotion_description, promotional_offers_type_id, wok_pass_group_id,  value_of_credits, modifiers_id, sku, bundle_title," +
                " bundle_subheading1, bundle_subheading2, promotion_start, promotion_end, promotion_list_heading, highlight_color, original_price, adrotator_xml_path, " +
                " special_background_color, special_background_image, special_sticker_image, promotional_package_label, special_font_color, use_display_options " +
                " FROM promotional_offers po INNER JOIN promotions_to_subscriptions pts ON po.promotion_id = pts.promotion_id" +
                " WHERE promotional_offers_type_id = @promotionTypeID";

            parameters.Add(new MySqlParameter("@promotionTypeID", promotionTypeID));

            DataTable dt = Db.Master.GetDataTable(sqlSelect, parameters);

            List<Promotion> list = new List<Promotion>();

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Promotion(Convert.ToUInt32(row["promotion_id"]), row["kei_point_id"].ToString(), Convert.ToDecimal(row["dollar_amount"]),
                Convert.ToDecimal(row["kei_point_amount"]), Convert.ToDecimal(row["free_points_awarded_amount"]), row["free_kei_point_ID"].ToString(),
                Convert.ToUInt32(row["is_special"]), row["promotion_description"].ToString(), Convert.ToUInt32(row["promotional_offers_type_id"]),
                Convert.ToUInt32(row["wok_pass_group_id"]), Convert.ToDecimal(row["value_of_credits"]), Convert.ToUInt32(row["modifiers_id"]),
                Convert.ToUInt32(row["sku"]), row["bundle_title"].ToString(), row["bundle_subheading1"].ToString(), row["bundle_subheading2"].ToString(),
                Convert.ToDateTime(row["promotion_start"]), Convert.ToDateTime(row["promotion_end"]), row["promotion_list_heading"].ToString(),
                row["highlight_color"].ToString(), row["special_background_color"].ToString(), row["special_background_image"].ToString(),
                row["special_sticker_image"].ToString(), row["promotional_package_label"].ToString(), row["special_font_color"].ToString(),
                Convert.ToUInt32(row["use_display_options"]), Convert.ToDecimal(row["original_price"]), row["adrotator_xml_path"].ToString()));
            }

            return list;
        }
        /// <summary>
        /// Gets all of the active subscription based promotions
        /// </summary>
        /// <returns></returns>
        public List<Promotion> GetAllActiveSubscriptionBasedPromotions(string excludedPasses, string includedPasses)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sqlSelect = "SELECT po.promotion_id, kei_point_id, dollar_amount, kei_point_amount, free_points_awarded_amount, free_kei_point_ID," +
                " is_special, promotion_description, promotional_offers_type_id, wok_pass_group_id,  value_of_credits, modifiers_id, sku, bundle_title," +
                " bundle_subheading1, bundle_subheading2, promotion_start, promotion_end, promotion_list_heading, highlight_color, original_price, adrotator_xml_path, " +
                " special_background_color, special_background_image, special_sticker_image, promotional_package_label, special_font_color, use_display_options " +
                " FROM promotional_offers po INNER JOIN promotions_to_subscriptions pts ON po.promotion_id = pts.promotion_id" +
                " WHERE promotion_end >= Now()";

            if ((excludedPasses != null) && (excludedPasses != ""))
            {
                sqlSelect += " AND pts.subscription_id NOT IN (SELECT subscription_id FROM subscriptions_to_passgroups WHERE pass_group_id IN (" + excludedPasses + "))";
            }
            else if ((includedPasses != null) && (includedPasses != ""))
            {
                sqlSelect += " AND pts.subscription_id IN (SELECT subscription_id FROM subscriptions_to_passgroups WHERE pass_group_id IN (" + includedPasses + "))";
            }

            DataTable dt = Db.Master.GetDataTable(sqlSelect, parameters);

            List<Promotion> list = new List<Promotion>();

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Promotion(Convert.ToUInt32(row["promotion_id"]), row["kei_point_id"].ToString(), Convert.ToDecimal(row["dollar_amount"]),
                Convert.ToDecimal(row["kei_point_amount"]), Convert.ToDecimal(row["free_points_awarded_amount"]), row["free_kei_point_ID"].ToString(),
                Convert.ToUInt32(row["is_special"]), row["promotion_description"].ToString(), Convert.ToUInt32(row["promotional_offers_type_id"]),
                Convert.ToUInt32(row["wok_pass_group_id"]), Convert.ToDecimal(row["value_of_credits"]), Convert.ToUInt32(row["modifiers_id"]),
                Convert.ToUInt32(row["sku"]), row["bundle_title"].ToString(), row["bundle_subheading1"].ToString(), row["bundle_subheading2"].ToString(),
                Convert.ToDateTime(row["promotion_start"]), Convert.ToDateTime(row["promotion_end"]), row["promotion_list_heading"].ToString(),
                row["highlight_color"].ToString(), row["special_background_color"].ToString(), row["special_background_image"].ToString(),
                row["special_sticker_image"].ToString(), row["promotional_package_label"].ToString(), row["special_font_color"].ToString(),
                Convert.ToUInt32(row["use_display_options"]), Convert.ToDecimal(row["original_price"]), row["adrotator_xml_path"].ToString()));
            }

            return list;
        }

        /// <summary>
        ///  Gets all of the active subscription based promotions the user hasnt bought
        /// </summary>
        /// <returns></returns>
        public List<Promotion> GetAllActiveUnpurchasedSubscriptionBasedPromotions(int userId, string passGroupFilter)
        {
            // This was added to header so cache it
            List<Promotion> promotions = (List<Promotion>) CentralCache.Get(CentralCache.keyUnpurchasedSubscription + userId.ToString() + passGroupFilter);

            if (promotions != null)
            {
                // Found in cache
                return promotions;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userID", userId));

            string sqlSelect = "SELECT po.promotion_id, kei_point_id, dollar_amount, kei_point_amount, free_points_awarded_amount, free_kei_point_ID," +
                " is_special, promotion_description, promotional_offers_type_id, wok_pass_group_id,  value_of_credits, modifiers_id, sku, bundle_title," +
                " bundle_subheading1, bundle_subheading2, promotion_start, promotion_end, promotion_list_heading, highlight_color, original_price, adrotator_xml_path, " +
                " special_background_color, special_background_image, special_sticker_image, promotional_package_label, special_font_color, use_display_options " +
                " FROM promotional_offers po INNER JOIN promotions_to_subscriptions pts ON po.promotion_id = pts.promotion_id ";

            string sqlWhere = "WHERE po.promotion_end >= now() AND ";

            if ((passGroupFilter != null) && (passGroupFilter != ""))
            {
                sqlWhere += " pts.promotion_id NOT IN (" +
                "SELECT promotion_id FROM promotions_to_subscriptions pts INNER JOIN subscriptions_to_passgroups stp ON pts.subscription_id = stp.subscription_id " +
                "WHERE pass_group_id IN (" + passGroupFilter + ") UNION SELECT promotion_id FROM user_subscriptions us WHERE user_id = @userID AND status_id IN (" +
                (int)Subscription.SubscriptionStatus.Active + ", " + (int)Subscription.SubscriptionStatus.Cancelled + ") AND end_date > NOW())";
            }
            else
            {
                sqlWhere += "pts.promotion_id NOT IN (SELECT promotion_id FROM user_subscriptions us WHERE user_id = @userID AND status_id IN (" +
                (int)Subscription.SubscriptionStatus.Active + ", " + (int)Subscription.SubscriptionStatus.Cancelled + ") AND end_date > NOW())";
            }

            DataTable dt = Db.Master.GetDataTable(sqlSelect + sqlWhere, parameters);

            List<Promotion> list = new List<Promotion>();

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Promotion(Convert.ToUInt32(row["promotion_id"]), row["kei_point_id"].ToString(), Convert.ToDecimal(row["dollar_amount"]),
                Convert.ToDecimal(row["kei_point_amount"]), Convert.ToDecimal(row["free_points_awarded_amount"]), row["free_kei_point_ID"].ToString(),
                Convert.ToUInt32(row["is_special"]), row["promotion_description"].ToString(), Convert.ToUInt32(row["promotional_offers_type_id"]),
                Convert.ToUInt32(row["wok_pass_group_id"]), Convert.ToDecimal(row["value_of_credits"]), Convert.ToUInt32(row["modifiers_id"]),
                Convert.ToUInt32(row["sku"]), row["bundle_title"].ToString(), row["bundle_subheading1"].ToString(), row["bundle_subheading2"].ToString(),
                Convert.ToDateTime(row["promotion_start"]), Convert.ToDateTime(row["promotion_end"]), row["promotion_list_heading"].ToString(),
                row["highlight_color"].ToString(), row["special_background_color"].ToString(), row["special_background_image"].ToString(),
                row["special_sticker_image"].ToString(), row["promotional_package_label"].ToString(), row["special_font_color"].ToString(),
                Convert.ToUInt32(row["use_display_options"]), Convert.ToDecimal(row["original_price"]), row["adrotator_xml_path"].ToString()));
            }

            // Cache user data for 5 minutes.  We do not invalidate cache for number_of_diggs, number_of_views etc.
            CentralCache.Store(CentralCache.keyUnpurchasedSubscription + userId.ToString() + passGroupFilter, list, TimeSpan.FromMinutes(5));
            return list;
        }



        /// <summary>
        /// Gets all of the promotions of the provided promotion type that have a subcription
        /// </summary>
        /// <returns></returns>
        public List<Promotion> GetPromotionsByPromotionAndPassType(int promotionTypeID, int passTypeID)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sqlSelect = "SELECT po.promotion_id, kei_point_id, dollar_amount, kei_point_amount, free_points_awarded_amount, free_kei_point_ID," +
                " is_special, promotion_description, promotional_offers_type_id, wok_pass_group_id,  value_of_credits, modifiers_id, sku, bundle_title," +
                " bundle_subheading1, bundle_subheading2, promotion_start, promotion_end, promotion_list_heading, highlight_color, original_price, adrotator_xml_path, " +
                " special_background_color, special_background_image, special_sticker_image, promotional_package_label, special_font_color, use_display_options " +
                " FROM promotional_offers po WHERE promotional_offers_type_id = @promotionTypeID AND wok_pass_group_id = @passTypeID";

            parameters.Add(new MySqlParameter("@promotionTypeID", promotionTypeID));
            parameters.Add(new MySqlParameter("@passTypeID", passTypeID));

            DataTable dt = Db.Master.GetDataTable(sqlSelect, parameters);

            List<Promotion> list = new List<Promotion>();

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Promotion(Convert.ToUInt32(row["promotion_id"]), row["kei_point_id"].ToString(), Convert.ToDecimal(row["dollar_amount"]),
                Convert.ToDecimal(row["kei_point_amount"]), Convert.ToDecimal(row["free_points_awarded_amount"]), row["free_kei_point_ID"].ToString(),
                Convert.ToUInt32(row["is_special"]), row["promotion_description"].ToString(), Convert.ToUInt32(row["promotional_offers_type_id"]),
                Convert.ToUInt32(row["wok_pass_group_id"]), Convert.ToDecimal(row["value_of_credits"]), Convert.ToUInt32(row["modifiers_id"]),
                Convert.ToUInt32(row["sku"]), row["bundle_title"].ToString(), row["bundle_subheading1"].ToString(), row["bundle_subheading2"].ToString(),
                Convert.ToDateTime(row["promotion_start"]), Convert.ToDateTime(row["promotion_end"]), row["promotion_list_heading"].ToString(),
                row["highlight_color"].ToString(), row["special_background_color"].ToString(), row["special_background_image"].ToString(),
                row["special_sticker_image"].ToString(), row["promotional_package_label"].ToString(), row["special_font_color"].ToString(),
                Convert.ToUInt32(row["use_display_options"]), Convert.ToDecimal(row["original_price"]), row["adrotator_xml_path"].ToString()));
            }

            return list;
        }

        /// <summary>
        /// GetPassGroupsAssociatedWPromotion
        /// </summary>
        public DataTable GetPassGroupsAssociatedWPromotion(uint promotionId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sqlString = "SELECT pass_group_id, subscription_id FROM subscriptions_to_passgroups " +
                " WHERE subscription_id IN (SELECT subscription_id FROM promotions_to_subscriptions WHERE promotion_id = @promotionId )";

            //string sqlString = "SELECT stp.pass_group_id, stp.subscription_id FROM subscriptions_to_passgroups stp INNER JOIN " +
            //    " promotions_to_subscriptions pts ON stp.subscription_id = pts.subscription_id WHERE promotion_id = @promotionId )";

            parameters.Add(new MySqlParameter("@promotionId", promotionId));

            return Db.Master.GetDataTable(sqlString, parameters);

        }


        /// <summary>
        /// GetUserTransactionCount
        /// </summary>
        /// <returns></returns>
        public int GetUserTransactionCount(int userId, string logicalOperator, string promotionTypeFilter)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sqlString = "SELECT COUNT(ppt.user_id) FROM purchase_point_transactions ppt " +
                " INNER JOIN orders o ON ppt.order_id = o.order_id";

            if (promotionTypeFilter != string.Empty)
            {
                sqlString += " INNER JOIN promotional_offers po ON ppt.point_bucket_id = po.promotion_id " +
                    " WHERE ppt.user_id = @userId AND o.transaction_status_id=2 AND promotional_offers_type_id " + logicalOperator + " (@promotionTypeFilter)";
            }
            else
            {
                sqlString += " WHERE ppt.user_id = @userId AND o.transaction_status_id=2";
            }

            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@promotionTypeFilter", promotionTypeFilter));

            return Convert.ToInt32(Db.Master.GetScalar(sqlString, parameters));
        }

        /// <summary>
        /// GetUserTransactionCount
        /// </summary>
        /// <returns></returns>
        public int GetBestSellingPromotion(string promoOfferTypeIDList)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sqlString = "SELECT point_bucket_id FROM (SELECT COUNT(point_bucket_id)AS total_purchases, point_bucket_id FROM" +
                " purchase_point_transactions ppt INNER JOIN promotional_offers po ON ppt.point_bucket_id = po.promotion_id WHERE transaction_status = 3" +
                " AND po.promotion_end >= Now() AND promotional_offers_type_id IN (" + promoOfferTypeIDList + ") " +
                " GROUP BY point_bucket_id ORDER BY total_purchases DESC LIMIT 1) as x1";
            
            return Convert.ToInt32(Db.Master.GetScalar(sqlString, parameters));
        }

        /// <summary>
        /// GetUserTransactionCount
        /// </summary>
        /// <returns></returns>
        public DataTable GetUserPurchasesByType(int userId, int promotionType)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sqlString = "SELECT po.promotion_id FROM promotional_offers po" +
                " INNER JOIN purchase_point_transactions ppt ON po.promotion_id = ppt.point_bucket_id" + 
                " WHERE ppt.user_id = @userId AND po.promotional_offers_type_id = @promotionType ";

            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@promotionType", promotionType));

            return Db.Master.GetDataTable(sqlString, parameters);
        }

        public DataTable GetPromotionalOfferItemsByGender(int promoId, string genderList)
        {
            string sqlSelect = "SELECT promotion_offer_id, market_price, promotion_id, gift_card_id, community_id, quantity, wok_item_id," +
                " item_description, alternate_description, gender " +
                " FROM promotional_offer_items poi " +
                " WHERE promotion_id = @promoId  AND gender IN (" + genderList + ")";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@promoId", promoId));
            //parameters.Add(new MySqlParameter("@genderList", genderList));

            return Db.Master.GetDataTable(sqlSelect, parameters);
        }
    }
}
