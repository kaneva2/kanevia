///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLDevelopmentCompanyDao : IDevelopmentCompanyDao
    {
        /// <summary>
        /// Gets a user.
        /// </summary>
        /// <param name="companyId">Unique game developer identifier.</param>
        /// <returns>DevelopmentCompany.</returns>
        public DevelopmentCompany GetDevelopmentCompany(int companyId)
        {
            string sql = "SELECT contacts_firstname, contacts_lastname, contacts_email, contacts_phone, company_name, company_addressI, " +
                "company_addressII, city, state_code, province, country_id, zip_code, website_URL, annual_revenue, vworld_involvment_id, " +
                "project_name, project_description, project_startdate_id, budget, client_platform_id, project_status_id, current_team_size, " +
                "projected_team_size, company_id, date_submitted, approved, authorized_tester FROM development_companies WHERE company_id = @companyId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@companyId", companyId));

            DataRow row = Db.Developer.GetDataRow(sql.ToString(), parameters);

            return new DevelopmentCompany(Convert.ToInt32(row["company_id"]), row["contacts_firstname"].ToString(),
                row["contacts_lastname"].ToString(), row["contacts_phone"].ToString(), row["company_name"].ToString(),
                row["contacts_email"].ToString(), row["company_addressI"].ToString(), row["company_addressII"].ToString(), row["state_code"].ToString(),
                row["city"].ToString(), row["province"].ToString(), row["country_id"].ToString(), row["zip_code"].ToString(),
                row["website_URL"].ToString(), Convert.ToInt32(row["annual_revenue"]), Convert.ToInt32(row["vworld_involvment_id"]),
                row["project_name"].ToString(), row["project_description"].ToString(), Convert.ToInt32(row["project_startdate_id"]),
                Convert.ToInt32(row["budget"]), Convert.ToInt32(row["client_platform_id"]), Convert.ToInt32(row["project_status_id"]),
                Convert.ToInt32(row["current_team_size"]), Convert.ToInt32(row["projected_team_size"]), Convert.ToDateTime(row["date_submitted"]),
                Convert.ToBoolean(row["approved"]), Convert.ToBoolean(row["authorized_tester"])
            );
        }

        /// <summary>
        /// Gets a development company.
        /// </summary>
        /// <param name="gameDeveloperId">Unique game developer identifier.</param>
        /// <returns>DevelopmentCompany.</returns>
        public DevelopmentCompany GetCompanyByDeveloperId(int gameDeveloperId)
        {
            string sql = "SELECT contacts_firstname, contacts_lastname, contacts_email, contacts_phone, company_name, company_addressI, " +
                "company_addressII, city, state_code, province, country_id, zip_code, website_URL, annual_revenue, vworld_involvment_id, " +
                "project_name, project_description, project_startdate_id, budget, client_platform_id, project_status_id, current_team_size, " +
                "projected_team_size, dc.company_id, date_submitted, approved, authorized_tester FROM development_companies dc INNER JOIN " +
                "company_developers cd ON dc.company_id = cd.company_id WHERE user_id = @gameDeveloperId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@gameDeveloperId", gameDeveloperId));

            DataRow row = Db.Developer.GetDataRow(sql.ToString(), parameters);

            if (row == null)
            {
                return new DevelopmentCompany();
            }

            return new DevelopmentCompany(Convert.ToInt32(row["company_id"]), row["contacts_firstname"].ToString(),
                row["contacts_lastname"].ToString(), row["contacts_phone"].ToString(), row["company_name"].ToString(),
                row["contacts_email"].ToString(), row["company_addressI"].ToString(), row["company_addressII"].ToString(), row["state_code"].ToString(),
                row["city"].ToString(), row["province"].ToString(), row["country_id"].ToString(), row["zip_code"].ToString(),
                row["website_URL"].ToString(), Convert.ToInt32(row["annual_revenue"]), Convert.ToInt32(row["vworld_involvment_id"]),
                row["project_name"].ToString(), row["project_description"].ToString(), Convert.ToInt32(row["project_startdate_id"]),
                Convert.ToInt32(row["budget"]), Convert.ToInt32(row["client_platform_id"]), Convert.ToInt32(row["project_status_id"]),
                Convert.ToInt32(row["current_team_size"]), Convert.ToInt32(row["projected_team_size"]), Convert.ToDateTime(row["date_submitted"]),
                Convert.ToBoolean(row["approved"]), Convert.ToBoolean(row["authorized_tester"])
            );
        }

        /// <summary>
        /// Get the friends for a user
        /// </summary>
        public PagedList<DevelopmentCompany> GetDevelopmentCompanies(int companyId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // UNION WAY
            string strQuery = "";

            // Friends I invited
            strQuery = "SELECT contacts_firstname, contacts_lastname, contacts_email, contacts_phone, company_name, company_addressI, " +
                "company_addressII, city, state_code, province, country_id, zip_code, website_URL, annual_revenue, vworld_involvment_id, " +
                "project_name, project_description, project_startdate_id, budget, client_platform_id, project_status_id, current_team_size, " +
                "projected_team_size, company_id, date_submitted, approved, authorized_tester ";

            strQuery += " FROM development_companies ";

            if (companyId > 0)
            {
                strQuery += " WHERE company_id = @companyId";
            }

            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@companyId", companyId));

            PagedDataTable dt = Db.Developer.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<DevelopmentCompany> list = new PagedList<DevelopmentCompany>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new DevelopmentCompany(Convert.ToInt32(row["company_id"]), row["contacts_firstname"].ToString(),
                row["contacts_lastname"].ToString(), row["contacts_phone"].ToString(), row["company_name"].ToString(),
                row["contacts_email"].ToString(), row["company_addressI"].ToString(), row["company_addressII"].ToString(), row["state_code"].ToString(),
                row["city"].ToString(), row["province"].ToString(), row["country_id"].ToString(), row["zip_code"].ToString(),
                row["website_URL"].ToString(), Convert.ToInt32(row["annual_revenue"]), Convert.ToInt32(row["vworld_involvment_id"]),
                row["project_name"].ToString(), row["project_description"].ToString(), Convert.ToInt32(row["project_startdate_id"]),
                Convert.ToInt32(row["budget"]), Convert.ToInt32(row["client_platform_id"]), Convert.ToInt32(row["project_status_id"]),
                Convert.ToInt32(row["current_team_size"]), Convert.ToInt32(row["projected_team_size"]), Convert.ToDateTime(row["date_submitted"]),
                Convert.ToBoolean(row["approved"]), Convert.ToBoolean(row["authorized_tester"])));
            }
            return list;
        }

        /// <summary>
        /// Insert a Game Developer
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public int InsertDevelopmentCompany(DevelopmentCompany company)
        {
            int companyId = 0;

            company.CompanyId = companyId;

            // Send the message
            string sql = "INSERT INTO development_companies " +
                "(contacts_firstname, contacts_lastname, contacts_email, contacts_phone, company_name, company_addressI, company_addressII, " +
                "city, state_code, province, country_id, zip_code, website_URL, annual_revenue, vworld_involvment_id, project_name, project_description, " +
                "project_startdate_id, budget, client_platform_id, project_status_id, current_team_size, projected_team_size, date_submitted, approved, authorized_tester " +
                ") VALUES (" +
                " @contactsFirstname, @contactsLastname, @contactsEmail, @contactsPhone, @companyName, @companyAddressI, @companyAddressII, " +
                "@city, @stateCode, @province, @countryId, @zipCode, @websiteURL, @annualRevenue, @vworldInvolvmentId, @projectName, @projectDescription, " +
                "@projectStartdateId, @budget, @clientPlatformId, @projectStatusId, @currentTeamSize, @projectedTeamSize, @dateSubmitted, @approved, @authorized )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@contactsFirstname", company.ContactsFirstName));
            parameters.Add(new MySqlParameter("@contactsLastname", company.ContactsLastName));
            parameters.Add(new MySqlParameter("@contactsEmail", company.ContactsEmail));
            parameters.Add(new MySqlParameter("@contactsPhone", company.ContactsPhone));
            parameters.Add(new MySqlParameter("@companyName", company.CompanyName));
            parameters.Add(new MySqlParameter("@companyAddressI", company.CompanyAddressI));
            parameters.Add(new MySqlParameter("@companyAddressII", company.CompanyAddressII));
            parameters.Add(new MySqlParameter("@city", company.City));
            parameters.Add(new MySqlParameter("@stateCode", company.StateCode));
            parameters.Add(new MySqlParameter("@province", company.Province));
            parameters.Add(new MySqlParameter("@countryId", company.CountryId));
            parameters.Add(new MySqlParameter("@zipCode", company.ZipCode));
            parameters.Add(new MySqlParameter("@websiteURL", company.WebsiteURL));
            parameters.Add(new MySqlParameter("@annualRevenue", company.AnnualRevenue));
            parameters.Add(new MySqlParameter("@vworldInvolvmentId", company.VworldInvolvmentId));
            parameters.Add(new MySqlParameter("@projectName", company.ProjectName));
            parameters.Add(new MySqlParameter("@projectDescription", company.ProjectDescription));
            parameters.Add(new MySqlParameter("@projectStartdateId", company.ProjectStartdateId));
            parameters.Add(new MySqlParameter("@budget", company.Budget));
            parameters.Add(new MySqlParameter("@clientPlatformId", company.ClientPlatformId));
            parameters.Add(new MySqlParameter("@projectStatusId", company.ProjectStatusId));
            parameters.Add(new MySqlParameter("@currentTeamSize", company.CurrentTeamSize));
            parameters.Add(new MySqlParameter("@projectedTeamSize", company.ProjectedTeamSize));
            parameters.Add(new MySqlParameter("@dateSubmitted", company.DateSubmitted));
            parameters.Add(new MySqlParameter("@approved", company.Approved ? 1 : 0));
            parameters.Add(new MySqlParameter("@authorized", company.AuthorizedTester ? 1 : 0));

            return Db.Developer.ExecuteIdentityInsert(sql, parameters);
        }


        // **********************************************************************************************
        // Misc
        // **********************************************************************************************
        #region Misc Functions

        /// <summary>
        /// Get Virtual World involvement options
        /// </summary>
        /// <returns></returns>
        public DataTable GetVWorldInvolvment()
        {
            string strQuery = "SELECT vworld_involvement_id , vworld_involvement " +
                " FROM vworld_involvement ";

            return Db.Developer.GetDataTable(strQuery);
        }

        /// <summary>
        /// Get Project Start Date options
        /// </summary>
        /// <returns></returns>
        public DataTable GetProjectStartDates()
        {
            string strQuery = "SELECT project_startdate_id, project_startdate " +
                " FROM project_startdates ";

            return Db.Developer.GetDataTable(strQuery);
        }

        /// <summary>
        /// Get Client Platform options
        /// </summary>
        /// <returns></returns>
        public DataTable GetClientPlatforms()
        {
            string strQuery = "SELECT client_platform_id, client_platform " +
                " FROM client_platforms ";

            return Db.Developer.GetDataTable(strQuery);
        }

        /// <summary>
        /// Get Project Status options
        /// </summary>
        /// <returns></returns>
        public DataTable GetProjectStatus()
        {
            string strQuery = "SELECT project_status_id, project_status " +
                " FROM project_status ";

            return Db.Developer.GetDataTable(strQuery);
        }

        /// <summary>
        /// Gets a List of all company roles
        /// </summary>
        public IList<SiteRole> GetCompaniesRoles(int companyId)
        {

            string strQuery = "SELECT site_roles_id, role_id, company_id, roles_description " +
                " FROM site_roles ";

            DataTable dt = Db.SiteManagement.GetDataTable(strQuery);

            IList<SiteRole> list = new List<SiteRole>();
            foreach (DataRow row in dt.Rows)
            {
                int id = int.Parse(row["user_id"].ToString());

                list.Add(new SiteRole(Convert.ToInt32(row["site_roles_id"]), Convert.ToInt32(row["role_id"]),
                    Convert.ToInt32(row["company_id"]), row["roles_description"].ToString()));
            }
            return list;
        }

        /// <summary>
        /// Insert default company roles
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public int CreateDefaultCompanyRoles(int companyId)
        {
            // Send the message
            string sql = "CALL add_default_game_roles(@companyId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@companyId", companyId));

            return Db.SiteManagement.ExecuteNonQuery(sql, parameters);
        }

        #endregion

    }
}
