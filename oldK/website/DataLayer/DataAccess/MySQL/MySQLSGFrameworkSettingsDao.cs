///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using Devart.Data.MySql;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;
using System.Text;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLSGFrameworkSettingsDao : ISGFrameworkSettingsDao
    {
        #region Create

        /// <summary>
        /// Adds starting_script_game_framework_settings to a deed. 
        /// </summary>
        /// <returns>False if an error occurred in processing, True otherwise.</returns>
        public bool AddSGFrameworkSettingsToCustomDeed(int zoneInstanceId, int zoneType, int templateGlobalId)
        {
            bool success = false;

            SGFrameworkSettings sgFrameworkSettings = GetWorldSGFrameworkSettings(zoneInstanceId, zoneType);

            // Return true if there were no items to process.
            if (sgFrameworkSettings == null)
            {
                return true;
            }

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sql = "INSERT INTO starting_script_game_framework_settings (template_glid, framework_enabled) " +
                                 "VALUES (@templateGlobalId, @frameworkEnabled) " +
                                 "ON DUPLICATE KEY UPDATE framework_enabled = @frameworkEnabled ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@templateGlobalId", templateGlobalId));
                    parameters.Add(new MySqlParameter("@frameworkEnabled", sgFrameworkSettings.FrameworkEnabled ? "T" : "F"));

                    Db.WOK.ExecuteNonQuery(sql, parameters, true);

                    transaction.Complete();
                    success = true;
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySGCustomDataDao.AddSGCustomDataToCustomDeed() - Transaction aborted: " + e.ToString());
            }

            return success;
        }

        #endregion

        #region Read

        /// <summary>
        /// GetWorldSGFrameworkSettings
        /// </summary>
        public SGFrameworkSettings GetWorldSGFrameworkSettings(int zoneInstanceId, int zoneType)
        {
            string sql = "SELECT sgi.framework_enabled " +
                         "FROM script_game_framework_settings sgi " +
                         "WHERE zone_instance_id = @zoneInstanceId " +
                         "AND zone_type = @zoneType ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));

            DataRow row = Db.WOK.GetDataRow(sql, parameters);

            if (row == null)
                return null;

            return new SGFrameworkSettings(row["framework_enabled"].ToString().Equals("T"));
        }

        /// <summary>
        /// GetDeedSGFrameworkSettings
        /// </summary>
        public SGFrameworkSettings GetDeedSGFrameworkSettings(int templateGlid)
        {
            string sql = "SELECT sgi.framework_enabled " +
                         "FROM starting_script_game_framework_settings sgi " +
                         "WHERE template_glid = @templateGlid ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@templateGlid", templateGlid));

            DataRow row = Db.WOK.GetDataRow(sql, parameters);

            if (row == null)
                return null;

            return new SGFrameworkSettings(row["framework_enabled"].ToString().Equals("T"));
        }

        #endregion

        #region Delete

        public bool DeleteSGFrameworkSettingsFromCustomDeed(int deedTemplateId)
        {
            bool success = false;

            try
            {
                // Do deletes in a transaction
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "DELETE FROM starting_script_game_framework_settings WHERE template_glid = @deedTemplateId";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@deedTemplateId", deedTemplateId));

                    Db.WOK.ExecuteNonQuery(sqlString, parameters, true);

                    transaction.Complete();
                    success = true;
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLSGCustomDataDao.DeleteSGFrameworkSettingsFromCustomDeed() - Transaction aborted: " + e.ToString());
            }
            return success;
        }

        #endregion

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
    }
}
