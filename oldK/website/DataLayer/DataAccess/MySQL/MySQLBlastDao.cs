///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLBlastDao : IBlastDao
    {
        /// <summary>
        /// SendAdminBlast
        /// </summary>
        public int SendAdminBlast (UInt64 entryId, int userId, DateTime dtExpires, string subject, string blastText, string cssHightLight, string thumbnailPath, bool sticky)
        {
            string sqlString = "CALL send_sticky_blast (@entryId, @thumbnailSmall, @userId, @cssHightLight, @dtExpires, @subject, @diaryText, @sticky )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@entryId", entryId));
            parameters.Add(new MySqlParameter("@dtExpires", dtExpires));
            parameters.Add(new MySqlParameter("@subject", subject));
            parameters.Add(new MySqlParameter("@diaryText", blastText));
            parameters.Add(new MySqlParameter("@sticky", (sticky ? 'Y' : 'N')));
            parameters.Add(new MySqlParameter("@cssHightLight", cssHightLight));
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@thumbnailSmall", thumbnailPath));
            return Db.DiaryMaster.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// InsertUserEvent
        /// </summary>
        public int InsertUserEvent(UInt64 entryId, string subject, string diaryText, int userId, int communityId, DateTime dtEventDate, string highlight_css, string eventName, string location)
        {
            string sqlString = "INSERT INTO user_events  ( " +
            " entry_id, date_created, _o, date_event, " +
            " sender, subj, diary_entry, highlight_css, blast_type, community_id, event_name, location " +
            " ) VALUES (" + 
            " @entryId, NOW(), kaneva.get_o(), @dtEventDate, " +
            " @userId, @subject, @diary_entry, @highlight_css, @blast_type, @community_id, @event_name, @location )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@entryId", entryId));
            parameters.Add(new MySqlParameter("@dtEventDate", dtEventDate));
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@subject", subject));
            parameters.Add(new MySqlParameter("@diary_entry", diaryText));
            parameters.Add(new MySqlParameter("@highlight_css", highlight_css));
            parameters.Add(new MySqlParameter("@blast_type", (int) BlastTypes.EVENT));
            parameters.Add(new MySqlParameter("@community_id", communityId));
            parameters.Add(new MySqlParameter("@event_name", eventName));
            parameters.Add(new MySqlParameter("@location", location));
            return Db.DiaryMaster.ExecuteNonQuery (sqlString, parameters);
        }
        
        /// <summary>
        /// Send Friend Active Blast
        /// </summary>
		public int SendFriendActiveBlast(BlastTypes blastType, int userId, string userName, string subject, string diaryText, string cssHightLight, string thumbnailPath, int recipientId)
        {
            string sqlString = "CALL add_active_blast (@thumbnailPath, @blastType, @cssHightLight, @userId, @userName, @subject, @diaryText, @recipientId )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@blastType", (int) blastType));
            parameters.Add (new MySqlParameter ("@cssHightLight", cssHightLight));
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@userName", userName));
            parameters.Add (new MySqlParameter ("@subject", subject));
            parameters.Add (new MySqlParameter ("@diaryText", diaryText));
            parameters.Add (new MySqlParameter ("@thumbnailPath", thumbnailPath));
			parameters.Add(new MySqlParameter("@recipientId", recipientId));
            return Db.DiaryMaster.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// Send Friend Active Blast
        /// </summary>
		public int SendFriendActiveBlast(UInt64 entryId, BlastTypes blastType, int userId, string userName, string subject, string diaryText, string cssHightLight, string thumbnailPath, int recipientId)
        {
			string sqlString = "CALL add_active_blast2 (@blastType, @cssHightLight, @entryId, @userId, @userName, @subject, @diaryText, @thumbnailPath, @recipientId  )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@blastType", (int) blastType));
            parameters.Add (new MySqlParameter ("@entryId", entryId));
            parameters.Add (new MySqlParameter ("@cssHightLight", cssHightLight));
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@userName", userName));
            parameters.Add (new MySqlParameter ("@subject", subject));
            parameters.Add (new MySqlParameter ("@diaryText", diaryText));
            parameters.Add (new MySqlParameter ("@thumbnailPath", thumbnailPath));
			parameters.Add (new MySqlParameter("@recipientId", recipientId));
            return Db.DiaryMaster.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// Send Channel Active Blast
        /// </summary>
        public int SendChannelActiveBlast (BlastTypes blastType, int userId, string userName, 
            int communityId, string communityName, bool isCommunityPersonal, string subject,
			string diaryText, string cssHightLight, string thumbnailPath, int recipientId)
        {
			string sqlString = "CALL add_channel_blast_entry (@thumbnailPath, @userId, @userName, @blastType, @cssHightLight, @communityId, @subject, @diaryText, @communityName, @isCommunityPersonal, @recipientId  )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@userName", userName));
            parameters.Add (new MySqlParameter ("@blastType", (int) blastType));
            parameters.Add (new MySqlParameter ("@cssHightLight", cssHightLight));
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            parameters.Add (new MySqlParameter ("@communityName", communityName));
            parameters.Add (new MySqlParameter ("@isCommunityPersonal", isCommunityPersonal ? 1 : 0));
            parameters.Add (new MySqlParameter ("@subject", subject));
            parameters.Add (new MySqlParameter ("@diaryText", diaryText));
            parameters.Add (new MySqlParameter ("@thumbnailPath", thumbnailPath));
			parameters.Add (new MySqlParameter("@recipientId", recipientId));
            return Db.DiaryMaster.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// Send Channel Active Blast
        /// </summary>
        public int SendChannelActiveBlast (UInt64 entryId, BlastTypes blastType, int userId, string userName, 
            int communityId, string communityName, bool isCommunityPersonal, string subject, string diaryText,
			string cssHightLight, string thumbnailPath, int recipientId)
        {
			string sqlString = "CALL add_channel_blast_entry2 (@userId, @userName, @blastType, @cssHightLight, @entryId, @communityId, @subject, @diaryText, @thumbnailPath, @communityName, @isCommunityPersonal, @recipientId  )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@userName", userName));
            parameters.Add (new MySqlParameter ("@entryId", entryId));
            parameters.Add (new MySqlParameter ("@blastType", (int) blastType));
            parameters.Add (new MySqlParameter ("@cssHightLight", cssHightLight));
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            parameters.Add (new MySqlParameter ("@communityName", communityName));
            parameters.Add (new MySqlParameter ("@isCommunityPersonal", isCommunityPersonal ? 1 : 0));
            parameters.Add (new MySqlParameter ("@subject", subject));
            parameters.Add (new MySqlParameter ("@diaryText", diaryText));
            parameters.Add (new MySqlParameter ("@thumbnailPath", thumbnailPath));
			parameters.Add (new MySqlParameter("@recipientId", recipientId));
            return Db.DiaryMaster.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// Send Friend Passive Blast
        /// </summary>
        public int SendFriendPassiveBlast (BlastTypes blastType, int userId, string userName, string subject, string diaryText)
        {
            string sqlString = "CALL add_passive_blast ( @blastType, @userId, @userName, @subject, @diaryText)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@blastType", (int) blastType));
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@userName", userName));
            parameters.Add (new MySqlParameter ("@subject", subject));
            parameters.Add (new MySqlParameter ("@diaryText", diaryText));
            return Db.DiaryMaster.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// Send Friend Passive Blast
        /// </summary>
        public int SendFriendPassiveBlast (UInt64 entryId, BlastTypes blastType, int userId, string userName, string subject, string diaryText)
        {
            string sqlString = "CALL add_passive_blast2 (@blastType, @entryId, @userId, @userName, @subject, @diaryText)";
                                                             
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@blastType", (int) blastType));
            parameters.Add (new MySqlParameter ("@entryId", entryId));
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@userName", userName));
            parameters.Add (new MySqlParameter ("@subject", subject));
            parameters.Add (new MySqlParameter ("@diaryText", diaryText));
            return Db.DiaryMaster.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// GetBlast
        /// </summary>
        public Blast GetBlast(UInt64 entryId)
        {
            string sqlSelect = "SELECT entry_id, date_created, sender_id, sender_name, subj, " +
                " diary_entry, highlight_css, ab.community_id, display_thumbnail, com.thumbnail_small_path, " +
                " name_no_spaces, number_of_replies, blast_type, now() as ctime, community_name, is_community_personal, " +
                " recipient_id, originator_id, originator_name " +
                " FROM active_blasts ab " +
                " INNER JOIN kaneva.communities com on ab.sender_id = com.creator_id AND com.is_personal = 1 " +
                " WHERE ab.entry_id = @entryId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add(new MySqlParameter("@entryId", entryId));
            DataRow row = Db.Diary.GetDataRow(sqlSelect, parameters);

            if (row == null)
            {
                return null;
            }

            return new Blast(Convert.ToUInt64(row["entry_id"]), Convert.ToDateTime(row["date_created"]), Convert.ToUInt32(row["sender_id"]), row["sender_name"].ToString(),
                row["subj"].ToString(), row["diary_entry"].ToString(), row["highlight_css"].ToString(), Convert.ToInt32(row["blast_type"]),
                Convert.ToUInt32(row["community_id"]), Convert.ToInt32(row["number_of_replies"]), row["display_thumbnail"].ToString(),
                row["name_no_spaces"].ToString(), row["thumbnail_small_path"].ToString(), Convert.ToDateTime(row["ctime"]),
			    row["community_name"].ToString(), Convert.ToBoolean(row["is_community_personal"]), Convert.ToInt32(row["recipient_id"]),
                row["originator_id"] == DBNull.Value ? 0 : Convert.ToUInt32 (row["originator_id"]),
                row["originator_name"] == DBNull.Value ? "" : row["originator_name"].ToString ()
            );
        }

        /// <summary>
        /// GetAdminBlast
        /// </summary>
        public Blast GetAdminBlast(UInt64 entryId)
        {
            //get stick blast
            string sqlSelect = "SELECT " + // Admin Blast
                " 0 as community_id, com.name_no_spaces, entry_id, sender, date_created, subj, diary_entry, highlight_css, " +
                " '' as thumbnail_small_path, '' as display_thumbnail, 12 as blast_type, now() as ctime, " +
                " 0 as number_of_replies, '' as community_name, 0 as is_community_personal" +
                " FROM sticky_blasts sb " +
                " INNER JOIN kaneva.communities com on sb.sender = com.creator_id AND com.is_personal = 1 " +
                " WHERE entry_id = @entryId ORDER BY sticky DESC, _o ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@entryId", entryId));
            DataRow row = Db.Diary.GetDataRow(sqlSelect, parameters);

            if (row == null)
            {
                return null;
            }

            return new Blast(Convert.ToUInt64(row["entry_id"]), Convert.ToDateTime(row["date_created"]), Convert.ToUInt32(row["sender"]), "",
            row["subj"].ToString(), row["diary_entry"].ToString(), row["highlight_css"].ToString(), Convert.ToInt32(row["blast_type"]),
            Convert.ToUInt32(row["community_id"]), Convert.ToInt32(row["number_of_replies"]), row["display_thumbnail"].ToString(),
            row["name_no_spaces"].ToString (), row["thumbnail_small_path"].ToString (), Convert.ToDateTime (row["ctime"]),
			row["community_name"].ToString(), Convert.ToBoolean(row["is_community_personal"]), 0);
        }

        /// <summary>
        /// GetBlasts
        /// </summary>
        public IList<Blast> GetAdminBlasts()
        {
            //get stick blast
            string strQuery = "SELECT " + // Admin Blasts
                " sb.entry_id, sb.date_created, sb.subj, sb.diary_entry, sb.highlight_css, '' as thumbnail_small_path, " +
                " 12 as blast_type, now() as ctime, 0 as number_of_replies, '' as community_name, 0 as is_community_personal " +
                " FROM sticky_blasts sb " +
                " LEFT OUTER JOIN user_events ue ON ue.entry_id = sb.entry_id " +
                " WHERE ue.entry_id IS NULL " +
                " ORDER BY sb.sticky DESC, sb._o ";

            DataTable dt = Db.Diary.GetDataTable(strQuery);

            IList<Blast> list = new List<Blast>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Blast(Convert.ToUInt64(row["entry_id"]), Convert.ToDateTime(row["date_created"]), 0, "",
                    row["subj"].ToString(), row["diary_entry"].ToString(), row["highlight_css"].ToString(), Convert.ToInt32(row["blast_type"]),
                    0, Convert.ToInt32(row["number_of_replies"]), "",
                    "", row["thumbnail_small_path"].ToString(), Convert.ToDateTime(row["ctime"]),
					row["community_name"].ToString(), Convert.ToBoolean(row["is_community_personal"]), 0
                ));
            }
            return list;
        }

        /// <summary>
        /// GetPassiveBlasts
        /// </summary>
        public List<Blast> GetPassiveBlasts(int userId, int pageSize, string userVisibilityFilter, int pageNumber)
        {
            string strQuery = " SELECT DISTINCT entry_id, date_created, sender_id, sender_name, subj, diary_entry, blast_type, now() as ctime, _o " +
                    " FROM passive_blasts pb, kaneva.friends f " +
                    " WHERE sender_id = f.friend_id " +
                    " AND f.user_id = @userId " +
                    " AND blast_type IN(" + userVisibilityFilter + ")" +
           //     " UNION " +
           //         " SELECT entry_id, date_created, sender_id, sender_name, subj, diary_entry, blast_type, now() as ctime, _o " +
           //         " FROM passive_blasts pb " +
           //         " WHERE sender_id = @userId " +
           //         " AND blast_type IN(" + userVisibilityFilter + ")" +
                " ORDER BY _o LIMIT " + pageSize;

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            DataTable dt = Db.Diary.GetDataTable (strQuery, parameters);

            List<Blast> list = new List<Blast>();
            
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Blast(Convert.ToUInt64(row["entry_id"]), Convert.ToDateTime(row["date_created"]),
                    Convert.ToUInt32(row["sender_id"]), row["sender_name"].ToString(),
                    row["subj"].ToString(), row["diary_entry"].ToString(), "", Convert.ToInt32(row["blast_type"]),
                    0, 0, "", "", "", Convert.ToDateTime(row["ctime"]), "", false, 0
                ));
            }
            return list;
        }

        /// <summary>
        /// GetMyPassiveBlasts
        /// </summary>
        public List<Blast> GetMyPassiveBlasts (int userId, int pageSize, string userVisibilityFilter, int pageNumber)
        {
            string strQuery = " SELECT entry_id, date_created, sender_id, sender_name, " +
                " subj, diary_entry, blast_type, now() as ctime, _o " +
                " FROM passive_blasts pb " +
                " WHERE sender_id = @userId " +
                " AND blast_type IN(" + userVisibilityFilter + ")" +
                " ORDER BY _o LIMIT " + pageSize;

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            DataTable dt = Db.Diary.GetDataTable (strQuery, parameters);

            List<Blast> list = new List<Blast> ();
            
            foreach (DataRow row in dt.Rows)
            {
                list.Add (new Blast (Convert.ToUInt64 (row["entry_id"]), Convert.ToDateTime (row["date_created"]),
                    Convert.ToUInt32 (row["sender_id"]), row["sender_name"].ToString (),
                    row["subj"].ToString (), row["diary_entry"].ToString (), "", Convert.ToInt32 (row["blast_type"]),
                    0, 0, "", "", "", Convert.ToDateTime (row["ctime"]), "", false, 0
                ));
            }
            return list;
        }

        ///// <summary>
        ///// GetPassiveBlastsManyFriends
        ///// </summary>
        //public PagedList<Blast> GetPassiveBlastsManyFriends (int userId, int pageSize, string userVisibilityFilter, int pageNumber, uint maxValue)
        //{
            //int offset = (pageNumber - 1) * pageSize;
            //string limit = " LIMIT " + offset + ", " + pageSize;

            //// It is too expensive to do a count on the total number of blasts and do a filter if a user has many friends.
            //string strQuery = " SELECT entry_id, date_created, sender_id, sender_name, subj, diary_entry, blast_type, now() as ctime" +
            //    " FROM passive_blasts pb " +
            //    " WHERE " +
            //    " (" +
            //        " sender_id in (SELECT friend_id FROM kaneva.friends WHERE user_id = @userId) " +
            //        " OR sender_id = @userId " +
            //    ")" +
            //    " AND blast_type IN(" + userVisibilityFilter + ")" +
            //    " ORDER BY _o " +
            //    limit;

            //List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            //parameters.Add(new MySqlParameter("@userId", userId));
            //DataTable dt = Db.Diary.GetDataTable (strQuery, parameters);

            //PagedList<Blast> list = new PagedList<Blast>();
            //list.TotalCount = maxValue;

            //foreach (DataRow row in dt.Rows)
            //{
            //    list.Add(new Blast(Convert.ToUInt64(row["entry_id"]), Convert.ToDateTime(row["date_created"]),
            //        Convert.ToUInt32(row["sender_id"]), row["sender_name"].ToString(),
            //        row["subj"].ToString(), row["diary_entry"].ToString(), "", Convert.ToInt32(row["blast_type"]),
            //        0, 0, "", "", "", Convert.ToDateTime(row["ctime"]),"",false
            //    ));
            //}
            //return list;
   //     }

        /// <summary>
        /// GetActivityBlasts
        /// </summary>
        public PagedList<Blast> GetActiveBlasts(int userId, int currentUserId, int pageSize, int pageNumber)
        {
            string strQuery = " SELECT entry_id, date_created, sender_id, sender_name, subj, diary_entry, highlight_css, " +
                    // get friend blasts, and admin blasts
					" blast_type, ab.community_id, now() as ctime, number_of_replies, display_thumbnail as thumbnail_small_path, _o, " +
                    " ab.community_name, ab.is_community_personal, recipient_id, originator_id, originator_name " +
                    " FROM active_blasts ab, kaneva.friends f " +
                    " WHERE sender_id = f.friend_id " +
                    " AND f.user_id= @userId " +
                    " AND ab.community_id = 0 " +
                    " AND blast_type IN (" + (int) BlastTypes.BASIC + "," + (int) BlastTypes.LINK + "," + (int) BlastTypes.PHOTO + "," + (int) BlastTypes.VIDEO + "," + (int) BlastTypes.PLACE + "," + (int) BlastTypes.SHOP_UPLOAD + "," + (int) BlastTypes.SHOP_PURCHASE + "," + (int) BlastTypes.ACHIEVEMENT_3DAPP + "," + (int) BlastTypes.CONTEST + ") " +

                " UNION " +
                    // get achievement blasts for friends
                    " SELECT entry_id, date_created, sender_id, sender_name, subj, diary_entry, highlight_css, " +
                    " blast_type, ab.community_id, now() as ctime, number_of_replies, display_thumbnail as thumbnail_small_path, _o, " +
                    " ab.community_name, ab.is_community_personal, recipient_id, originator_id, originator_name " +
                    " FROM active_blasts ab, kaneva.friends f " +
                    " WHERE sender_id = f.friend_id " +
                    " AND f.user_id= @userId " +
                    " AND blast_type = " + (int) BlastTypes.ACHIEVEMENT_3DAPP +

                " UNION " +

                // get 3d app rave blasts for friends, limit 2 per hour, only one per world per hour
                // IMPORTANT: Do NOT define the following as parameters, they are used as MySQL session variables to
                // overcome MySQL's unforgivable lack of a 'Row_Number' function: @ROWNUM, @CURDATE, @CURHOUR.
                // Select only blasts numbered as #1
                   " SELECT entry_id, date_created, sender_id, sender_name, subj, diary_entry, highlight_css, " +
                   " blast_type, community_id, ctime, number_of_replies, thumbnail_small_path, _o, " +
                   " community_name, is_community_personal, recipient_id, originator_id, originator_name " +
                   " FROM " +
                   " (SELECT @ROWNUM:=0, @CURDATE:='', @BLASTTYPE:=0) var_set, " +
                   " ( " +
                       //Number blasts within their 3 hour timespan window
                       " SELECT entry_id, date_created, sender_id, sender_name, subj, diary_entry, highlight_css, " +
                       " blast_type, community_id, ctime, number_of_replies, thumbnail_small_path, _o, " +
                       " community_name, is_community_personal, recipient_id, originator_id, originator_name, " +
                       " @ROWNUM:=IF(@BLASTTYPE = grouped_data.blast_type AND ABS(TIMESTAMPDIFF(HOUR, @CURDATE, grouped_data.date_created)) < 3, @ROWNUM + 1, 1) AS row_number, " +
                       " @CURDATE:=grouped_data.date_created AS dummy1, " +
                       " @BLASTTYPE:=grouped_data.blast_type AS dummy2 " +
                       " FROM " +
                       " ( " +
                           // Select a single blast per hour, per world
                           " SELECT * FROM ( " +
                               " SELECT entry_id, date_created, sender_id, sender_name, subj, diary_entry, highlight_css, " +
                               " blast_type, ab.community_id, now() as ctime, number_of_replies, display_thumbnail as thumbnail_small_path, _o, " +
                               " ab.community_name, ab.is_community_personal, recipient_id, originator_id, originator_name " +
                               " FROM active_blasts ab, kaneva.friends f " +
                               " WHERE sender_id = f.friend_id " +
                                   " AND f.user_id= @userId " +
                                   " AND blast_type IN (" + (int)BlastTypes.RAVE_3D_APP + ", " + (int)BlastTypes.CAMERA_SCREENSHOT + ") " +
                               " ORDER BY date_created DESC, blast_type DESC, ab.community_id DESC " +
                           " ) AS ordered_list " +
                           " GROUP BY DATE(date_created) DESC, HOUR(date_created) DESC, blast_type DESC, community_id DESC " +
                        " ) AS grouped_data " +
                   " ) AS numbered_data " +
                   " WHERE numbered_data.row_number <= 1 " +

                " UNION " +
					// get community blasts and personal blasts
                    " SELECT entry_id, date_created, sender_id, sender_name, subj, diary_entry, highlight_css, " +
                    " blast_type, ab.community_id, now() as ctime, number_of_replies, display_thumbnail as thumbnail_small_path, _o, " +
                    " ab.community_name, ab.is_community_personal, recipient_id, originator_id, originator_name " +
                    " FROM active_blasts ab, kaneva.community_members cm " +
                    " WHERE ab.community_id = cm.community_id " +
                    " AND cm.user_id= @userId " +
                    " AND cm.status_id = " + (int)CommunityMember.CommunityMemberStatus.ACTIVE +
                    " AND blast_type IN (" + (int) BlastTypes.BASIC + "," + (int) BlastTypes.LINK + "," + (int) BlastTypes.PHOTO + "," + (int) BlastTypes.VIDEO + "," + (int) BlastTypes.PLACE + "," + (int) BlastTypes.SHOP_UPLOAD + "," + (int) BlastTypes.SHOP_PURCHASE + "," + (int) BlastTypes.ACHIEVEMENT_3DAPP + "," + (int) BlastTypes.CONTEST + ") " +
				
				" UNION " +
				// get 3D App Requests
					" SELECT entry_id, date_created, sender_id, sender_name, subj, diary_entry, highlight_css, " +
					" blast_type, ab.community_id, now() as ctime, number_of_replies, display_thumbnail as thumbnail_small_path, _o, " +
                    " ab.community_name, ab.is_community_personal, recipient_id, originator_id, originator_name " +
					" FROM active_blasts ab " +
					" WHERE recipient_id = @userId " +
					" AND recipient_id = @currentUserId " +
					" AND blast_type IN (" + (int)BlastTypes.REQUEST_3DAPP + ") " +					
				
				" UNION " +
					//get own blasts
                    " SELECT entry_id, date_created, sender_id, sender_name, subj, diary_entry, highlight_css, " +
                    " blast_type, ab.community_id, now() as ctime, number_of_replies, display_thumbnail as thumbnail_small_path, _o, " +
                    " ab.community_name, ab.is_community_personal, recipient_id, originator_id, originator_name " +
                    " FROM active_blasts ab " +
                    " WHERE sender_id = @userId " +
                    " AND blast_type IN (" + (int)BlastTypes.BASIC + "," + (int)BlastTypes.LINK + "," + (int)BlastTypes.PHOTO + "," + (int)BlastTypes.VIDEO + "," + (int)BlastTypes.PLACE + "," + (int)BlastTypes.SHOP_UPLOAD + "," + (int)BlastTypes.SHOP_PURCHASE + "," + (int)BlastTypes.ACHIEVEMENT_3DAPP + "," + (int)BlastTypes.CONTEST + "," + (int)BlastTypes.RAVE_3D_APP + "," + (int)BlastTypes.CAMERA_SCREENSHOT + ") " +				

                " ORDER BY _o ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
			parameters.Add(new MySqlParameter("@currentUserId", currentUserId));
            PagedDataTable dt = Db.Diary.GetPagedDataTable(strQuery, "", parameters, pageNumber, pageSize);

            PagedList<Blast> list = new PagedList<Blast>();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Blast(Convert.ToUInt64(row["entry_id"]), Convert.ToDateTime(row["date_created"]),
                    Convert.ToUInt32 (row["sender_id"]), row["sender_name"].ToString (),
                    row["subj"].ToString(), row["diary_entry"].ToString(), row["highlight_css"].ToString(), Convert.ToInt32(row["blast_type"]),
                    Convert.ToUInt32 (row["community_id"]), Convert.ToInt32 (row["number_of_replies"]), "",
                    "", row["thumbnail_small_path"].ToString(), Convert.ToDateTime(row["ctime"]),
                    row["community_name"].ToString(), Convert.ToBoolean(row["is_community_personal"]), 0,
                    row["originator_id"] == DBNull.Value ? 0 : Convert.ToUInt32 (row["originator_id"]),
                    row["originator_name"] == DBNull.Value ? "" : row["originator_name"].ToString ()
                ));
            }
            return list;

        }

        /// <summary>
        /// GetFriendActiveBlasts
        /// </summary>
        public PagedList<Blast> GetFriendActiveBlasts (int userId, int pageSize, int pageNumber)
        {
            string strQuery =
                " ( " +
                " SELECT entry_id, date_created, sender_id, sender_name, subj, diary_entry, highlight_css," +
                " blast_type, now() as ctime, number_of_replies, display_thumbnail as thumbnail_small_path," +
                " community_id, community_name, is_community_personal, originator_id, originator_name" +
                " FROM active_blasts ab " +
                " WHERE " +
                " ( " +		// Blasts my friends sent
                " 	(sender_id in (SELECT friend_id FROM kaneva.friends WHERE user_id = @userId)) AND ab.community_id = 0 AND blast_type IN (" + (int) BlastTypes.BASIC + "," + (int) BlastTypes.LINK + "," + (int) BlastTypes.PHOTO + "," + (int) BlastTypes.VIDEO + "," + (int) BlastTypes.PLACE + "," + (int) BlastTypes.SHOP_UPLOAD + "," + (int) BlastTypes.SHOP_PURCHASE + "," + (int) BlastTypes.CONTEST + ") " +
                " OR " +	// Blasts from a channel
                " 	ab.community_id in (SELECT cm.community_id FROM kaneva.community_members cm WHERE user_id= @userId AND cm.active_member = 'Y') AND blast_type IN (" + (int) BlastTypes.BASIC + "," + (int) BlastTypes.LINK + "," + (int) BlastTypes.PHOTO + "," + (int) BlastTypes.VIDEO + "," + (int) BlastTypes.PLACE + "," + (int) BlastTypes.SHOP_UPLOAD + "," + (int) BlastTypes.SHOP_PURCHASE + "," + (int) BlastTypes.CONTEST + ") " +
                " ) " +
                " ORDER BY _o " +
                " )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            PagedDataTable dt = Db.Diary.GetPagedDataTable (strQuery, "", parameters, pageNumber, pageSize);

            PagedList<Blast> list = new PagedList<Blast> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new Blast (Convert.ToUInt64 (row["entry_id"]), Convert.ToDateTime (row["date_created"]),
                    Convert.ToUInt32 (row["sender_id"]), row["sender_name"].ToString (),
                    row["subj"].ToString (), row["diary_entry"].ToString (), row["highlight_css"].ToString (), Convert.ToInt32 (row["blast_type"]),
                    Convert.ToUInt32 (row["community_id"]), Convert.ToInt32 (row["number_of_replies"]), "",
                    "", row["thumbnail_small_path"].ToString (), Convert.ToDateTime (row["ctime"]),
                    row["community_name"].ToString (), Convert.ToBoolean (row["is_community_personal"]), 0,
                    row["originator_id"] == DBNull.Value ? 0 : Convert.ToUInt32 (row["originator_id"]),
                    row["originator_name"] == DBNull.Value ? "" : row["originator_name"].ToString ()
                ));
            }
            
            return list;
        }

        /// <summary>
        /// GetMyActiveBlasts
        /// </summary>
        public PagedList<Blast> GetMyActiveBlasts (int userId, int pageSize, int pageNumber)
        {
            string strQuery =
                " ( " +
                " SELECT entry_id, date_created, sender_id, sender_name, subj, diary_entry, highlight_css, " +
                " blast_type, now() as ctime, community_id, number_of_replies, display_thumbnail as thumbnail_small_path," +
                " community_name, is_community_personal, originator_id, originator_name" +
                " FROM active_blasts ab " +
                " WHERE " +
                " ( " +		// Blasts I sent
                " (sender_id = @userId AND blast_type IN (" + (int) BlastTypes.BASIC + "," + (int) BlastTypes.LINK + "," + (int) BlastTypes.PHOTO + "," + (int) BlastTypes.VIDEO + "," + (int) BlastTypes.PLACE + "," + (int) BlastTypes.SHOP_UPLOAD + "," + (int) BlastTypes.SHOP_PURCHASE + "," + (int) BlastTypes.CONTEST + "))  " +
                " ) " +
                " ORDER BY _o " +
                " )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            PagedDataTable dt = Db.Diary.GetPagedDataTable(strQuery, "", parameters, pageNumber, pageSize);

            PagedList<Blast> list = new PagedList<Blast>();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new Blast (Convert.ToUInt64 (row["entry_id"]), Convert.ToDateTime (row["date_created"]),
                    Convert.ToUInt32 (row["sender_id"]), row["sender_name"].ToString (),
                    row["subj"].ToString(), row["diary_entry"].ToString(), row["highlight_css"].ToString(), Convert.ToInt32(row["blast_type"]),
                    Convert.ToUInt32 (row["community_id"]), Convert.ToInt32 (row["number_of_replies"]), "",
                    "", row["thumbnail_small_path"].ToString(), Convert.ToDateTime(row["ctime"]),
                    row["community_name"].ToString (), Convert.ToBoolean (row["is_community_personal"]), 0,
                    row["originator_id"] == DBNull.Value ? 0 : Convert.ToUInt32 (row["originator_id"]),
                    row["originator_name"] == DBNull.Value ? "" : row["originator_name"].ToString ()
                ));
            }
            return list;
        }

        /// <summary>
        /// GetCommunityActiveBlasts
        /// </summary>
        public PagedList<Blast> GetCommunityActiveBlasts (int communityId, int senderId, bool isPersonalCommunity, int pageSize, int pageNumber)
        {
            string strQuery =
                " SELECT DISTINCT ab.entry_id, ab.date_created, ab.sender_id, ab.sender_name, ab.subj, ab.diary_entry, ab.highlight_css, " +
                " ab.blast_type, now() as ctime, ab.community_id, ab.number_of_replies, ab.display_thumbnail as thumbnail_small_path," +
                " ab.community_name, ab.is_community_personal" +
                " FROM active_blasts ab " +	
		        " INNER JOIN kaneva.communities c ON ab.community_id = c.community_id " +
                " LEFT OUTER JOIN kaneva.community_members cm ON c.community_id = cm.community_id " +
                " WHERE " +
                " (ab.community_id = @communityId AND ab.blast_type IN (" + (int)BlastTypes.BASIC + "," + (int)BlastTypes.LINK + "," + (int)BlastTypes.PHOTO + "," + (int)BlastTypes.VIDEO + "," + (int)BlastTypes.PLACE + "," + (int)BlastTypes.RAVE_3D_APP + ")) " +
                " OR (ab.community_id = @communityId AND ab.blast_type IN (" + (int)BlastTypes.CAMERA_SCREENSHOT + ") AND ab.sender_id = cm.user_id) ";

            if (senderId > 0)
            {
                if (isPersonalCommunity)
                {
                    strQuery += " OR (ab.sender_id = @senderId AND ab.community_id = 0) " +
                        " OR (ab.sender_id = @senderId AND ab.blast_type IN (" + (int)BlastTypes.ACHIEVEMENT_3DAPP + "," + (int)BlastTypes.RAVE_3D_APP + "," + (int)BlastTypes.CAMERA_SCREENSHOT + ")) ";
                }
                else
                {
                    strQuery += " AND ab.sender_id = @senderId ";
                }
            }

            strQuery += " ORDER BY ab._o ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            parameters.Add (new MySqlParameter ("@senderId", senderId));
            PagedDataTable dt = Db.Diary.GetPagedDataTable (strQuery, "", parameters, pageNumber, pageSize);

            PagedList<Blast> list = new PagedList<Blast> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new Blast (Convert.ToUInt64 (row["entry_id"]), Convert.ToDateTime (row["date_created"]),
                    Convert.ToUInt32 (row["sender_id"]), row["sender_name"].ToString (),
                    row["subj"].ToString (), row["diary_entry"].ToString (), row["highlight_css"].ToString (), Convert.ToInt32 (row["blast_type"]),
                    Convert.ToUInt32 (row["community_id"]), Convert.ToInt32 (row["number_of_replies"]), "",
                    "", row["thumbnail_small_path"].ToString (), Convert.ToDateTime (row["ctime"]),
                    row["community_name"].ToString (), Convert.ToBoolean (row["is_community_personal"]), 0
                ));
            }
            return list;
        }

        /// <summary>
        /// GetEvents
        /// </summary>
        public IList<Blast> GetUpcomingEvents(int userId, int pageSize)
        {
            string strQuery = " SELECT entry_id, date_created, date_event, subj, event_name, diary_entry, highlight_css, com.name_no_spaces, " +
                    " com.thumbnail_small_path, location, blast_type, now() as ctime " +
                    " FROM user_events ud " +
                    " INNER JOIN kaneva.communities com on ud.sender = com.creator_id AND com.is_personal = 1, kaneva.friends f " +
                    " WHERE sender = f.friend_id " +
                    " AND user_id = @userId " +
                    " AND ud.community_id = 0 " +
                    " AND date_event > ADDDATE(NOW(),INTERVAL -30 MINUTE) " +
                " UNION " +
                    " SELECT entry_id, date_created, date_event, subj, event_name, diary_entry, highlight_css, com.name_no_spaces, " +
                    " com.thumbnail_small_path, location, blast_type, now() as ctime " +
                    " FROM user_events ud " +
                    " INNER JOIN kaneva.communities com on ud.sender = com.creator_id AND com.is_personal = 1 " +
                    " WHERE sender = @userId " +
                    " AND ud.community_id = 0 " +
                    " AND date_event > ADDDATE(NOW(),INTERVAL -30 MINUTE) " +
                " UNION " +
                    " SELECT entry_id, date_created, date_event, subj, event_name, diary_entry, highlight_css, com.name_no_spaces, " +
                    " com.thumbnail_small_path, location, blast_type, now() as ctime " +
                    " FROM user_events ud " +
                    " INNER JOIN kaneva.communities com on ud.sender = com.creator_id AND com.is_personal = 1, kaneva.community_members cm " +
                    " WHERE ud.community_id = cm.community_id " +
                    " AND user_id= @userId " +
                    " AND cm.active_member = 'Y' " +
                    " AND date_event > ADDDATE(NOW(),INTERVAL -30 MINUTE) " +
                " ORDER BY date_event";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            DataTable dt = Db.Diary.GetDataTable(strQuery, parameters);

            IList<Blast> list = new List<Blast>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Blast(Convert.ToUInt64(row["entry_id"]), Convert.ToDateTime(row["date_created"]), 0, "",
                    row["subj"].ToString(), row["diary_entry"].ToString(), row["highlight_css"].ToString(), Convert.ToInt32(row["blast_type"]),
                    0, 0, "", Convert.ToDateTime(row["date_event"]), row["location"].ToString (), row["event_name"].ToString (),
                    row["name_no_spaces"].ToString (), row["thumbnail_small_path"].ToString (), Convert.ToDateTime (row["ctime"]),"",false
                ));
            }
            return list;
        }

        /// <summary>
        /// GetEvents
        /// </summary>
        public IList<Blast> GetAdminEvents ()
        {
            string strQuery =
                " SELECT sb.entry_id, sb.date_created, date_event, sb.subj, event_name, sb.diary_entry, " +
                " sb.highlight_css, ue.location, blast_type, now() as ctime, u.username, u.username " +
                " FROM user_events ue " +
                " INNER JOIN sticky_blasts sb ON sb.entry_id = ue.entry_id " +
                " INNER JOIN kaneva.users u ON u.user_id = sb.sender " +
                " WHERE ue.blast_type = " + (int) BlastTypes.EVENT +  
                " AND date_event > ADDDATE(NOW(),INTERVAL -30 MINUTE) " +
                " ORDER BY date_event;";

            DataTable dt = Db.Diary.GetDataTable (strQuery);

            IList<Blast> list = new List<Blast> ();
            foreach (DataRow row in dt.Rows)
            {
                list.Add (new Blast (Convert.ToUInt64 (row["entry_id"]), Convert.ToDateTime (row["date_created"]), 0, "",
                    row["subj"].ToString (), row["diary_entry"].ToString (), row["highlight_css"].ToString (), Convert.ToInt32 (row["blast_type"]),
                    0, 0, "", Convert.ToDateTime (row["date_event"]), row["location"].ToString (), row["event_name"].ToString (),
                    row["username"].ToString (), "", Convert.ToDateTime (row["ctime"]),"",false
                ));
            }
            return list;
        }

        /// <summary>
        /// InsertBlastReply
        /// </summary>
        public int InsertBlastReply(UInt64 entryId, int userId, string message, string ipAddress)
        {
            string sqlString = "INSERT INTO active_blasts_replies ( " +
                " diary_reply_id, reply_to_id, date_created, message, user_id " +
                " ) VALUES (" +
                " diary.get_id(), @entryId,NOW(),@message, @userId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@entryId", entryId));
            parameters.Add(new MySqlParameter("@message", message));
            parameters.Add(new MySqlParameter("@userId", userId));
            return Db.DiaryMaster.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// GetBlast
        /// </summary> 
        public PagedList<BlastReply> GetBlastReplies(UInt64 entryId, int pageNumber, int pageSize, string sortBy)
        {
            string strQuery = "SELECT diary_reply_id, reply_to_id, abr.user_id, date_created, message, " +
                " com.name_no_spaces, com.thumbnail_small_path, now() as ctime, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture, users.gender " +
                " FROM active_blasts_replies abr " +
                " INNER JOIN kaneva.communities com on abr.user_id = com.creator_id AND com.is_personal = 1 " +
                " INNER JOIN kaneva.users users on abr.user_id = users.user_id " +
                " LEFT OUTER JOIN kaneva.user_facebook_settings ufs ON ufs.user_id = com.creator_id " +
                " WHERE abr.reply_to_id = @entryId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@entryId", entryId));

            PagedDataTable dt = Db.Diary.GetPagedDataTable(strQuery, sortBy, parameters, pageNumber, pageSize);

            PagedList<BlastReply> list = new PagedList<BlastReply>();
            list.TotalCount = dt.TotalCount;

            // reverse the order so the comments will display in correct order
            for (int i = dt.Rows.Count-1; i >= 0; i--)
            {
                BlastReply br = new BlastReply (Convert.ToUInt64 (dt.Rows[i]["diary_reply_id"]), Convert.ToUInt64 (dt.Rows[i]["reply_to_id"]), 
                    Convert.ToDateTime (dt.Rows[i]["date_created"]), dt.Rows[i]["message"].ToString (),
                    Convert.ToInt32 (dt.Rows[i]["user_id"]), dt.Rows[i]["name_no_spaces"].ToString (),
                    dt.Rows[i]["thumbnail_small_path"].ToString(), Convert.ToDateTime(dt.Rows[i]["ctime"]));

                br.Gender = dt.Rows[i]["gender"].ToString();

                br.OwnerFacebookSettings.FacebookUserId = Convert.ToUInt64 (dt.Rows[i]["fb_user_id"]);
                br.OwnerFacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (dt.Rows[i]["use_facebook_profile_picture"]);

                list.Add (br);
            }
            return list;
        }

        /// <summary>
        /// GetNewEntryId
        /// </summary>
        public UInt64 GetNewEntryId()
        {
            string sqlSelect = "SELECT diary.get_id() as entryId ";

            DataRow drEntryId = Db.DiaryMaster.GetDataRow(sqlSelect);

            if (drEntryId != null)
            {
                return Convert.ToUInt64(drEntryId["entryId"]);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// DeleteAdminBlast
        /// </summary>
        /// <param name="id">the id of the blast</param>
        /// <returns></returns>
        public int DeleteAdminBlast (UInt64 entryId)
        {
            string sqlString = "CALL delete_admin_blast (@entryId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@entryId", entryId));
            return Db.DiaryMaster.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// DeleteBlast
        /// </summary>
        /// <param name="id">the id of the blast</param>
        /// <returns></returns>
        public int DeleteBlast (UInt64 entryId)
        {
            string sqlString = "CALL delete_blast (@entryId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@entryId", entryId));
            return Db.DiaryMaster.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// Update number of blasts sent
        /// </summary>
        public void IncrementBlastsSent (int userId)
        {
            //delete from contest table
            string sqlString = "UPDATE users_stats SET number_blasts_sent = number_blasts_sent + 1 WHERE user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            Db.Master.ExecuteNonQuery(sqlString, parameters, 1);
        }

        public int SendReblast (int blastType, int userId, string userName, string subject, string diaryEntry, 
            string cssHightlight, string thumbnailPath, uint originatorId, string originatorName)
        {
            string sqlString = "INSERT INTO active_blasts (entry_id, date_created, _o, sender_id, sender_name, subj, " +
                "diary_entry, highlight_css, blast_type, display_thumbnail, originator_id, originator_name) " +
                " VALUES (get_id(), NOW(), kaneva.get_o(), @senderId, @senderName, @subj, @diaryEntry, @highlightCSS, " +
                "@blastType, @displayThumbnail, @originatorId, @originatorName)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@senderId", userId));
            parameters.Add (new MySqlParameter ("@senderName", userName));
            parameters.Add (new MySqlParameter ("@subj", subject));
            parameters.Add (new MySqlParameter ("@diaryEntry", diaryEntry));
            parameters.Add (new MySqlParameter ("@highlightCSS", cssHightlight));
            parameters.Add (new MySqlParameter ("@blastType", blastType));
            parameters.Add (new MySqlParameter ("@displayThumbnail", thumbnailPath));
            parameters.Add (new MySqlParameter ("@originatorId", originatorId));
            parameters.Add (new MySqlParameter ("@originatorName", originatorName));
            return Db.DiaryMaster.ExecuteNonQuery (sqlString, parameters);
        }
    }
}
