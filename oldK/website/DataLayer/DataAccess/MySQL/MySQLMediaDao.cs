///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using log4net;
using Sphinx.Client.Commands.Search;
using Sphinx.Client.Connections;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLMediaDao : IMediaDao
    {
        private enum eASSET_PERMISSION
        {
            PUBLIC = 1,
            PRIVATE = 2,
            FRIENDS = 3,
            KANEVA_MEMBERS = 4
        }

        private enum eASSET_TYPE
        {
            ALL = 0,
            GAME = 1,
            VIDEO = 2,
            ASSET = 3,
            MUSIC = 4,
            PICTURE = 5,
            PATTERN = 6,
            TV = 7,
            WIDGET = 8
        }

        /// <summary>
        /// Get an Asset
        /// </summary>
        public Asset GetAsset(int assetId, int userId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            string strSelect;
            string strJoin = string.Empty;

            if (userId > 0)
            {
                strSelect = " COALESCE(d.digg_id, 0) as digg_id,";
                strJoin = " LEFT OUTER JOIN asset_diggs d ON d.asset_id = a.asset_id AND d.user_id = @userId";
                parameters.Add(new MySqlParameter("@userId", userId));
            }
            else
            {
                strSelect = " 0 as digg_id,";
            }

            string sqlSelect = " SELECT a.asset_id, a.asset_type_id, a.is_kaneva_game, a.asset_sub_type_id, a.media_path, " +
                " a.asset_rating_id, a.owner_id, file_size, " +
                " a.run_time_seconds, a.category1_id, a.category2_id, a.category3_id, " +
                " a.amount, a.kei_point_id, a.image_path, " +
                " a.image_full_path, a.thumbnail_assetdetails_path, a.thumbnail_xlarge_path, a.thumbnail_large_path, " +
                " a.thumbnail_medium_path, a.thumbnail_small_path,  a.thumbnail_gen, a.image_caption, " +
                " a.status_id, a.created_date, a.last_updated_date, " +
                " a.last_updated_user_id, a.license_cc, a.license_URL, " +
                " a.license_name, a.license_additional, a.license_type, a.publish_status_id, " +
                " a.percent_complete, a.ip_address, a.game_encryption_key, " +
                " a.require_login, a.content_extension, a.target_dir, a.permission, COALESCE(a.permission_group, 0) as permission_group, " +
                " a.published, a.mature, a.public,  " +
                " a.playlistable, a.over_21_required, a.asset_offsite_id, " +
                " a.sort_order, a.name, a.body_text, a.short_description, " +
                " a.teaser, a.owner_username, a.keywords, " +
                strSelect +
                " CONCAT_WS(',', ac1.name, ac2.name, ac3.name) as categories, " +
                "   " +
                "  u.zip_code, " +
                " ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs, ass.number_of_channels " +
                " FROM assets a " +
                " INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id " +
                " INNER JOIN users u ON u.user_id = a.owner_id      " +
                strJoin +
                " LEFT OUTER JOIN asset_categories ac1 ON ac1.category_id = a.category1_id " +
                " LEFT OUTER JOIN asset_categories ac2 ON ac2.category_id = a.category2_id " +
                " LEFT OUTER JOIN asset_categories ac3 ON ac3.category_id = a.category3_id " +
                " WHERE a.asset_id = @assetId" +
                " AND a.status_id IN (" + (int)Asset.eASSET_STATUS.ACTIVE + "," + (int)Asset.eASSET_STATUS.NEW + ")";

            
            parameters.Add(new MySqlParameter("@assetId", assetId));
            DataRow row = Db.Slave2.GetDataRow(sqlSelect, parameters);

            if (row == null)
            {
                Asset assetBlank = new Asset();
                assetBlank.Stats = new AssetStats();
                return assetBlank;
            }

            Asset asset = new Asset(Convert.ToInt32(row["asset_id"]), Convert.ToInt32(row["asset_type_id"]), row["is_kaneva_game"].ToString(), Convert.ToInt32(row["asset_sub_type_id"]),
                Convert.ToInt32(row["asset_rating_id"]), Convert.ToInt32(row["owner_id"]), Convert.ToUInt64(row["file_size"]),
                Convert.ToInt32(row["run_time_seconds"]), Convert.ToInt32(row["category1_id"]), Convert.ToInt32(row["category2_id"]), Convert.ToInt32(row["category3_id"]), 
                Convert.ToDouble(row["amount"]), row["kei_point_id"].ToString(), row["image_path"].ToString(),
                row["image_full_path"].ToString(), row["thumbnail_assetdetails_path"].ToString(), row["thumbnail_xlarge_path"].ToString(), row["thumbnail_large_path"].ToString(),
                row["thumbnail_medium_path"].ToString(), row["thumbnail_small_path"].ToString(), Convert.ToInt32(row["thumbnail_gen"]), row["image_caption"].ToString(),
                Convert.ToInt32(row["status_id"]), Convert.ToDateTime(row["created_date"]), Convert.ToDateTime(row["last_updated_date"]), 
                Convert.ToInt32(row["last_updated_user_id"]), row["license_cc"].ToString(), row["license_URL"].ToString(),
                row["license_type"].ToString(), row["license_name"].ToString(), row["license_additional"].ToString(), Convert.ToInt32(row["publish_status_id"]),
                Convert.ToInt32(row["percent_complete"]), row["ip_address"].ToString(), row["game_encryption_key"].ToString(), 
                row["require_login"].ToString(), row["content_extension"].ToString(),
                row["target_dir"].ToString(), row["media_path"].ToString(), Convert.ToInt32(row["permission"]), Convert.ToInt32(row["permission_group"]),
                row["published"].ToString().Equals("Y"), row["mature"].ToString().Equals("Y"), row["public"].ToString().Equals("Y"),
                row["playlistable"].ToString().Equals("Y"), row["over_21_required"].ToString().Equals("Y"), row["asset_offsite_id"].ToString(),
                Convert.ToUInt32(row["sort_order"]), row["name"].ToString(), row["body_text"].ToString(), row["short_description"].ToString(),
                row["teaser"].ToString(), row["owner_username"].ToString(), row["keywords"].ToString(),
                row["categories"].ToString(), row["zip_code"].ToString(), Convert.ToInt32(row["digg_id"]));

            asset.Stats = new AssetStats (Convert.ToInt32(row["asset_id"]), Convert.ToInt32(row["number_of_comments"]), Convert.ToInt32(row["number_of_downloads"]), 
                Convert.ToUInt32(row["number_of_shares"]), Convert.ToUInt32(row["number_of_channels"]), Convert.ToUInt32(row["number_of_diggs"]));
            
            return asset;
        }

        /// <summary>
        /// Get Asset information from the main database without any associated stat data
        /// </summary>
        public Asset GetNewAssetInformation(int assetId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            string sqlSelect = " SELECT a.asset_id, a.asset_type_id, a.is_kaneva_game, a.asset_sub_type_id, a.media_path, " +
                " a.asset_rating_id, a.owner_id, file_size, " +
                " a.run_time_seconds, a.category1_id, a.category2_id, a.category3_id, " +
                " a.amount, a.kei_point_id, a.image_path, " +
                " a.image_full_path, a.thumbnail_assetdetails_path, a.thumbnail_xlarge_path, a.thumbnail_large_path, " +
                " a.thumbnail_medium_path, a.thumbnail_small_path,  a.thumbnail_gen, a.image_caption, " +
                " a.status_id, a.created_date, a.last_updated_date, " +
                " a.last_updated_user_id, a.license_cc, a.license_URL, " +
                " a.license_name, a.license_additional, a.license_type, a.publish_status_id, " +
                " a.percent_complete, a.ip_address, a.game_encryption_key, " +
                " a.require_login, a.content_extension, a.target_dir, a.permission, COALESCE(a.permission_group, 0) as permission_group, " +
                " a.published, a.mature, a.public,  " +
                " a.playlistable, a.over_21_required, a.asset_offsite_id, " +
                " a.sort_order, a.name, a.body_text, a.short_description, " +
                " a.teaser, a.owner_username, a.keywords " +
                " FROM assets a " +
                " WHERE a.asset_id = @assetId";

            parameters.Add(new MySqlParameter("@assetId", assetId));
            DataRow row = Db.Master.GetDataRow(sqlSelect, parameters);

            if (row == null)
            {
                log4net.ILog m_logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                m_logger.Error("MySQLMediaDao.cs - sqlSelect: " + sqlSelect + " assetId: " + assetId);

                Asset assetBlank = new Asset();
                assetBlank.Stats = new AssetStats();
                return assetBlank;
            }

            Asset asset = new Asset(Convert.ToInt32(row["asset_id"]), Convert.ToInt32(row["asset_type_id"]), row["is_kaneva_game"].ToString(), Convert.ToInt32(row["asset_sub_type_id"]),
                Convert.ToInt32(row["asset_rating_id"]), Convert.ToInt32(row["owner_id"]), Convert.ToUInt64(row["file_size"]),
                Convert.ToInt32(row["run_time_seconds"]), Convert.ToInt32(row["category1_id"]), Convert.ToInt32(row["category2_id"]), Convert.ToInt32(row["category3_id"]),
                Convert.ToDouble(row["amount"]), row["kei_point_id"].ToString(), row["image_path"].ToString(),
                row["image_full_path"].ToString(), row["thumbnail_assetdetails_path"].ToString(), row["thumbnail_xlarge_path"].ToString(), row["thumbnail_large_path"].ToString(),
                row["thumbnail_medium_path"].ToString(), row["thumbnail_small_path"].ToString(), Convert.ToInt32(row["thumbnail_gen"]), row["image_caption"].ToString(),
                Convert.ToInt32(row["status_id"]), Convert.ToDateTime(row["created_date"]), Convert.ToDateTime(row["last_updated_date"]),
                Convert.ToInt32(row["last_updated_user_id"]), row["license_cc"].ToString(), row["license_URL"].ToString(),
                row["license_type"].ToString(), row["license_name"].ToString(), row["license_additional"].ToString(), Convert.ToInt32(row["publish_status_id"]),
                Convert.ToInt32(row["percent_complete"]), row["ip_address"].ToString(), row["game_encryption_key"].ToString(),
                row["require_login"].ToString(), row["content_extension"].ToString(),
                row["target_dir"].ToString(), row["media_path"].ToString(), Convert.ToInt32(row["permission"]), Convert.ToInt32(row["permission_group"]),
                row["published"].ToString().Equals("Y"), row["mature"].ToString().Equals("Y"), row["public"].ToString().Equals("Y"),
                row["playlistable"].ToString().Equals("Y"), row["over_21_required"].ToString().Equals("Y"), row["asset_offsite_id"].ToString(),
                Convert.ToUInt32(row["sort_order"]), row["name"].ToString(), row["body_text"].ToString(), row["short_description"].ToString(),
                row["teaser"].ToString(), row["owner_username"].ToString(), row["keywords"].ToString(),
                "", "", 0);
            return asset;
        }

        /// <summary>
        /// Get the Asset Categories
        /// </summary>
        /// <returns></returns>
        public int GetAssetCategoryIdByName(int assetTypeId, string catName)
        {
            string sqlSelect = "SELECT ac.category_id " +
                " FROM asset_categories ac " +
                " WHERE asset_type_id = @assetTypeId " +
                " AND name = @name ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetTypeId", assetTypeId));
            parameters.Add(new MySqlParameter("@name", catName));

            Object result = Db.Master.GetScalar(sqlSelect, parameters);

            return (result == null ? -1 : Convert.ToInt32(result));
        }


        /// <summary>
        /// Update Asset Status 
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int UpdateAssetStatus(Asset asset, int statusId)
        {
            // Send the message
            string sql = "call update_asset_status(@assetId, @statusId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetId", asset.AssetId));
            parameters.Add(new MySqlParameter("@statusId", statusId));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Update Asset Restrictions
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int UpdateAssetRestriction(int assetId, int assetRatingId, bool isMature)
        {
            // Send the message
            string sql = "call update_asset_restriction(@assetId, @assetRatingId, @mature)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetId", assetId));
            parameters.Add(new MySqlParameter("@assetRatingId", assetRatingId));
            parameters.Add(new MySqlParameter("@assetRatingId", isMature ? 'Y':'N'));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Update Asset On Search Box
        /// </summary>
        /// <param name="assetid"></param>
        /// <returns>int</returns>
        public int UpdateAssetInSearch(int assetId)
        {
            // Send the message
            string sql = "CALL flag_search_asset( @assetId )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetId", assetId));

            return Db.SearchMaster.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Delete Asset
        /// </summary>
        /// <param name="asset"></param>
        /// <returns>int</returns>
        public int DeleteAsset(Asset asset)
        {
            string sql = "CALL delete_asset(@assetId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetId", asset.AssetId));

            int result = Db.Master.ExecuteNonQuery(sql, parameters);

            return 1;//temporary until able to return value from stored procedure
        }

        /// <summary>
        /// Delete Asset From Channels
        /// </summary>
        /// <param name="asset"></param>
        /// <returns>int</returns>
        public int DeleteAssetFromChannel(Asset asset)
        {
            string sql = "CALL delete_asset_from_channels(@assetId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetId", asset.AssetId));

            int result = Db.Master.ExecuteNonQuery(sql, parameters);

            return 1;//temporary until able to return value from stored procedure
        }

        /// <summary>
        /// Update Asset On Search Box
        /// </summary>
        /// <param name="asset"></param>
        /// <returns>int</returns>
        public int DeleteAssetFromSearch(Asset asset)
        {
            // Send the message
            string sql = "CALL delete_search_asset( @assetId )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetId", asset.AssetId));

            return Db.SearchMaster.ExecuteNonQuery(sql, parameters);
        }


        /// <summary>
        /// UpdateMediaViews
        /// </summary>
        public int UpdateMediaViews(int assetId, string userIpAddress, int userId)
        {
            // Build unique cache key from this IP to the community
            string cacheKey = CentralCache.keyRecentAssetView + userIpAddress + assetId.ToString();
            int iSecondsToCountNextView = 60;

            // Have they viewd in last X seconds?
            String view = (String)CentralCache.Get(cacheKey);

            if (view == null)
            {    
                // Not found in cache means not views in last X seconds, Record as a view
                // Update download counts for the assets
                string sqlUpdate = "UPDATE assets_stats " +
                    " SET number_of_downloads = number_of_downloads + 1 " +
                    " WHERE asset_id = @assetId";
                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@assetId", assetId));
                Db.Master.ExecuteNonQuery(sqlUpdate, parameters);

                // Insert the asset_views record
                string sqlInsert = "INSERT INTO asset_views " +
                    " (user_id, asset_id, created_date, ip_address) " +
                    " VALUES " +
                    " (@userId, @assetId, NOW(), INET_ATON(@ipAddress))";

                parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@userId", userId));
                parameters.Add(new MySqlParameter("@assetId", assetId));
                parameters.Add(new MySqlParameter("@ipAddress", userIpAddress));
                Db.Audit.ExecuteNonQuery(sqlInsert, parameters);

                // This was in asset_view trigger in kaneva, now do in code a better way
                sqlUpdate = " INSERT INTO summary_assets_viewed VALUES(@assetId,DATE(NOW()),1) " +
                    " ON DUPLICATE KEY UPDATE count = count+1 ";
                parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@assetId", assetId));
                Db.Master.ExecuteNonQuery(sqlUpdate, parameters);

                // Store it in cache
                CentralCache.Store(cacheKey, userIpAddress, TimeSpan.FromSeconds(iSecondsToCountNextView));
            }

            return 0;
        }

        /// <summary>
        /// GetUserViewHistory
        /// </summary>
        public PagedList<AssetView> GetUserViewHistory(int userId, string orderBy, int pageNumber, int pageSize)
        {
            string sqlSelectList = " SELECT av.view_id, av.user_id, av.created_date, INET_NTOA(av.ip_address) as ip_address, a.asset_id, " +
                " a.name, a.asset_rating_id, a.thumbnail_small_path, a.thumbnail_gen, a.asset_type_id " +
                " FROM asset_views av, kaneva.assets a " +
                " WHERE av.user_id = @userId " +
                " AND av.asset_id = a.asset_id ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            PagedDataTable dt = Db.Audit.GetPagedDataTable(sqlSelectList, orderBy, parameters, pageNumber, pageSize);

            PagedList<AssetView> list = new PagedList<AssetView>();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new AssetView(Convert.ToUInt32(row["view_id"]) , Convert.ToUInt32(row["user_id"]) , 
                    Convert.ToUInt32(row["asset_id"]) , Convert.ToDateTime(row["created_date"]) ,
                    row["ip_address"].ToString(), row["name"].ToString (), Convert.ToInt32(row["asset_rating_id"]),
                    row["thumbnail_small_path"].ToString (), Convert.ToInt32 (row["thumbnail_gen"]), Convert.ToInt32 (row["asset_type_id"])
                   ));
            }
            return list;
        }

        /// <summary>
        /// ShareAsset
        /// </summary>
        public void ShareAsset(int assetId, int fromUserId, string fromIp, string toEmail, int toUserId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" INSERT INTO asset_shares ");
            sb.Append(" (from_user_id, asset_id, to_email, created_date, from_ip, ");
            sb.Append(" to_user_id) VALUES (");
            sb.Append(" @fromUserId, @asset_id, @to_email, NOW(), @from_ip, ");
            sb.Append(" @to_user_id) ");

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@fromUserId", fromUserId > 0 ? fromUserId : 0));
            parameters.Add(new MySqlParameter("@asset_id", assetId));
            parameters.Add(new MySqlParameter("@to_email", toEmail != null ? toEmail : "NULL"));
            parameters.Add(new MySqlParameter("@from_ip", fromIp != null ? fromIp : "NULL"));
            parameters.Add(new MySqlParameter("@to_user_id", toUserId > 0 ? toUserId : 0));
            Db.Audit.ExecuteNonQuery(sb.ToString(), parameters);

            if (fromUserId > 0)
            {
                string query = "UPDATE users_stats " +
                    " SET number_asset_shares = number_asset_shares+1 " +
                    " WHERE user_id = @fromUserId ";
                parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@fromUserId", fromUserId));
                Db.Master.ExecuteNonQuery(query, parameters);
            }

            // This was in asset_share trigger in kaneva, now do in code a better way
            string sqlUpdate = " INSERT INTO summary_assets_shared VALUES(@assetId,DATE(NOW()),1) " +
                " ON DUPLICATE KEY UPDATE count = count+1 ";
            parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetId", assetId));
            Db.Master.ExecuteNonQuery(sqlUpdate, parameters);

            // What happened to this trigger?
            // Update assets_stats set number_of_shares=number_of_shares+1 where asset_id=aid;
        }


        /// <summary>
        /// when a link sent from "share an asset" email is clicked
        /// </summary>
        /// <param name="keyValue"></param>
        public void SharedAssetClicked(string keyValue)
        {
            string query = " UPDATE asset_shares " +
                " SET clicked = 1 " +
                " WHERE key_value =@keyValue ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@key_value", keyValue));
            Db.Audit.ExecuteNonQuery(query, parameters);
        }

        public PagedList<Asset> SearchMedia(bool bGetMature, int assetTypeId, bool bGetPrivate, bool bThumbnailRequired, string searchString,
            string categories, int pastDays, string orderBy, int pageNumber, int pageSize, bool onlyAccessPass, List<int> iAssetList)
        {
            return SearchMedia(bGetMature, new List<int> {assetTypeId}, bGetPrivate, bThumbnailRequired, searchString, categories, pastDays,
                orderBy, pageNumber, pageSize, onlyAccessPass, iAssetList);
        }

        /// <summary>
        /// Search SearchMedia
        /// </summary>
        public PagedList<Asset> SearchMedia(bool bGetMature, List<int> assetTypeId, bool bGetPrivate, bool bThumbnailRequired, string searchString,
            string categories, int pastDays, string orderBy, int pageNumber, int pageSize, bool onlyAccessPass, List<int> iAssetList)
        {
            Sphinx sphinx = new Sphinx();
            string newSearchString = "";
            string assetIds = "";
            int totalCount = 0;
            string sphinxCategoryPrefix = "cat_e_gory_";
            assetTypeId = assetTypeId ?? new List<int>();

            searchString = sphinx.StripSphinxSearchString(searchString);

            using (ConnectionBase connection = new PersistentTcpConnection(sphinx.Host, sphinx.Port))
            {
                connection.ConnectionTimeout = 5000;

               // Build search string.
                 char[] splitter = { ' ' };
                string[] arKeywords = null;

                if (searchString.Trim ().Length > 0)
                {
                    // Escape some strings sphinx has issues with \()|-!@~/^$*
                    searchString = sphinx.CleanSphinxSearchString(searchString);

                    // Did they enter multiples?
                    arKeywords = searchString.Split(splitter);

                    if (arKeywords.Length > 1)
                    {
                        newSearchString = "";
                        for (int j = 0; j < arKeywords.Length; j++)
                        {
                            // Do a sphinx OR for now
                            if (j < arKeywords.Length - 1)
                            {
                                newSearchString += "@(asset_name,teaser,body_text,owner_name,short_desc,keywords) " + arKeywords[j].ToString() + " | ";
                            }
                            else
                            {
                                newSearchString += "@(asset_name,teaser,body_text,owner_name,short_desc,keywords) " + arKeywords[j].ToString();
                            }
                        }
                    }
                    else
                    {
                        newSearchString = "@(asset_name,teaser,body_text,owner_name,short_desc,keywords) " + searchString;
                    }
                }

                // Jeff R built category into FT index, not filtering
                if (categories.Trim().Length > 0)
                {
                    if (newSearchString.Trim ().Length > 0)
                    {
                        // Category is an AND (@categories means only search categories index 
                        // for the next search term, sphinx extended mode)
                        newSearchString += " & @categories " + sphinxCategoryPrefix + categories.ToString();
                    }
                    else
                    {
                        // @categories means only search categories index
                        newSearchString = "@categories " + sphinxCategoryPrefix + categories.ToString();
                    }
                }

                // Mature
                if (!bGetMature)
                {
                    if (newSearchString.Trim().Length > 0)
                    {

                        newSearchString += " & @mature N";
                    }
                    else
                    {
                        // @categories means only search categories index
                        newSearchString = "@mature N";
                    }
                }


                if (bGetMature && onlyAccessPass)
                {
                    if (newSearchString.Trim().Length > 0)
                    {

                        newSearchString += " & @mature Y";
                    }
                    else
                    {
                        // @categories means only search categories index
                        newSearchString = "@mature Y";
                    }
                }

                // Thumbnail
                if (bThumbnailRequired && !assetTypeId.Contains((int)Asset.eASSET_TYPES.PICTURE) && !assetTypeId.Contains((int)Asset.eASSET_TYPES.PATTERN))
                {
                    if (newSearchString.Trim().Length > 0)
                    {

                        newSearchString += " & @has_image Y";
                    }
                    else
                    {
                        // @categories means only search categories index
                        newSearchString = "@has_image Y";
                    }
                }

                SearchQuery searchQuery = new SearchQuery(newSearchString);

                // Add field weighting
                if (searchString.Trim().Length > 0)
                {
                    searchQuery.FieldWeights.Add("asset_name", 100);
                }

                // Set match mode to SPH_MATCH_EXTENDED2
                searchQuery.MatchMode = MatchMode.Extended2;

                // Add Sphinx index name to list
                searchQuery.Indexes.Add("assets_srch_idx");

                // Set up paging
                searchQuery.Limit = pageSize;
                searchQuery.Offset = (pageNumber - 1) * pageSize;

                // Create search command object
                SearchCommand searchCommand = new SearchCommand(connection);

                // Add newly created search query object to query list
                searchCommand.QueryList.Add(searchQuery);


                // Add some filtering
                if (assetTypeId.Count > 0)
                {
                    searchQuery.AttributeFilters.Add("asset_type_id", assetTypeId, false);
                }

                // Don't show private
                if (!bGetPrivate)
                {
                    searchQuery.AttributeFilters.Add("permission", (uint)1, false);
                }

                // Only get a certain group of assets
                if (iAssetList != null && iAssetList.Count > 0)
                {
                    searchQuery.AttributeFilters.Add("asset_id_filter", iAssetList, false);
                }

                // Only get last x number of days?
                if (pastDays > 0)
                {
                    searchQuery.AttributeFilters.Add("date_created", (uint)ConvertToTimestamp(DateTime.Now.AddDays(-pastDays)), (uint)ConvertToTimestamp(DateTime.Now.AddYears(1)), false);
                }


                //// Sorting
                if (orderBy.Trim().Length.Equals(0))
                {
                    searchQuery.SortMode = ResultsSortMode.Relevance;
                }
                else
                {
                    if (orderBy.ToUpper().Contains("ASC"))
                    {
                        searchQuery.SortMode = ResultsSortMode.AttributeAscending;
                    }
                    else
                    {
                        searchQuery.SortMode = ResultsSortMode.AttributeDescending;
                    }

                    // Strip ASC, DESC
                    searchQuery.SortBy = orderBy.Replace("ASC", "").Replace("DESC", "").Trim();
                }

                searchQuery.MaxMatches = 10000;

                // Execute command on server and obtain results
                try
                {
                    searchCommand.Execute();
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error calling Sphinx " + sphinx.Host + ":" + sphinx.Port, exc);
                }

                // Build result IN clause for MySQL
                foreach (SearchQueryResult result in searchCommand.Result.QueryResults)
                {
                    foreach (Match match in result.Matches)
                    {
                        if (assetIds.Length > 0)
                        {
                            assetIds = assetIds + "," + match.DocumentId.ToString();
                        }
                        else
                        {
                            assetIds = match.DocumentId.ToString();
                        }
                    }

                    // Note this will grab the last total count in collection, ok for this case, but not best place to do this
                    totalCount = (result.TotalFound > searchQuery.MaxMatches ? searchQuery.MaxMatches : result.TotalFound);
                }

                // Nothing found in Sphinx
                if (assetIds.Length.Equals(0))
                {
                    PagedList<Asset> emptyList = new PagedList<Asset>();
                    emptyList.TotalCount = 0;
                    return emptyList;
                }

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();

                string strSelect = "SELECT a.asset_id, file_size, " +
                    " a.content_extension, a.asset_type_id, a.asset_sub_type_id, a.name, " +
                    " COALESCE(a.short_description,'') as short_description, a.created_date, a.asset_rating_id, a.amount, " +
                    " a.status_id, a.mature, a.public, a.over_21_required, " +
                    " a.run_time_seconds, a.owner_id, " +
                    " a.owner_id as user_id, a.owner_username, a.owner_username as username, " +
                    " a.teaser, a.keywords, " +
                    " a.permission, COALESCE(a.permission_group,0) AS permission_group, " +
                    " a.image_full_path, a.thumbnail_small_path, thumbnail_medium_path, " +
                    " thumbnail_large_path, thumbnail_xlarge_path, thumbnail_gen, " +
                    " ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs, a.asset_offsite_id " +
                    " FROM assets a INNER JOIN assets_stats as ass ON a.asset_id = ass.asset_id " +
                    " where a.asset_id in " +
                    " (" + assetIds + ")" +
                    " ORDER BY FIELD(a.asset_id," + assetIds + ")";

                 DataTable dt = Db.Slave.GetDataTable(strSelect, parameters);

                PagedList<Asset> list = new PagedList<Asset>();
                list.TotalCount = Convert.ToUInt32(totalCount);

                foreach (DataRow row in dt.Rows)
                {
                    Asset asset = new Asset(Convert.ToInt32 (row["asset_id"]), Convert.ToInt32 (row["asset_type_id"]), 
                    "0", Convert.ToInt32 (row["asset_sub_type_id"]),
                    Convert.ToInt32 (row["asset_rating_id"]), Convert.ToInt32 (row["owner_id"]), 
                    0, 0, 0, 0, 0, 0, "KPOINT", "",
                    row["image_full_path"].ToString (), "", row["thumbnail_xlarge_path"].ToString (), 
                    row["thumbnail_large_path"].ToString (), row["thumbnail_medium_path"].ToString (), 
                    row["thumbnail_small_path"].ToString (), Convert.ToInt32 (row["thumbnail_gen"]), 
                    "", Convert.ToInt32 (row["status_id"]), Convert.ToDateTime (row["created_date"]), 
                    DateTime.Now, 0, "", "", "", "", "", 0, 0, "", "", "", row["content_extension"].ToString (),
                    "", "", Convert.ToInt32 (row["permission"]), Convert.ToInt32 (row["permission_group"]),
                    true, row["mature"].ToString ().Equals ("Y"), row["public"].ToString ().Equals ("Y"),
                    false, row["over_21_required"].ToString ().Equals ("Y"),
                    row["asset_offsite_id"].ToString(), 0, row["name"].ToString(), "", row["short_description"].ToString(),
                    row["teaser"].ToString (), row["owner_username"].ToString (), row["keywords"].ToString (),
                    "", "", 0);
                
                asset.Stats = new AssetStats (Convert.ToInt32 (row["asset_id"]), Convert.ToInt32 (row["number_of_comments"]), Convert.ToInt32 (row["number_of_downloads"]),
                   Convert.ToUInt32 (row["number_of_shares"]), 0, Convert.ToUInt32 (row["number_of_diggs"]));

                list.Add (asset);
                }

                return list;

            }
        }

        /// <summary>
        /// Search BrowseMedia
        /// </summary>
        public PagedList<Asset> BrowseMedia(string sphinxIndex, bool bGetMature, int assetTypeId, bool bGetPrivate, bool bThumbnailRequired,
            string categories, int pastDays, string orderBy, int pageNumber, int pageSize, bool onlyAccessPass)
        {
            Sphinx sphinx = new Sphinx();
            string newSearchString = "";
            string assetIds = "";
            int totalCount = 0;
            string sphinxCategoryPrefix = "cat_e_gory_";

            using (ConnectionBase connection = new PersistentTcpConnection(sphinx.Host, sphinx.Port))
            {
                connection.ConnectionTimeout = 5000;

            
                // Jeff R built category into FT index, not filtering
                if (categories.Trim().Length > 0)
                {
                    if (newSearchString.Trim().Length > 0)
                    {
                        // Category is an AND (@categories means only search categories index 
                        // for the next search term, sphinx extended mode)
                        newSearchString += " & @categories " + sphinxCategoryPrefix + categories.ToString();
                    }
                    else
                    {
                        // @categories means only search categories index
                        newSearchString = "@categories " + sphinxCategoryPrefix + categories.ToString();
                    }
                }

                // Mature
                if (!bGetMature)
                {
                    if (newSearchString.Trim().Length > 0)
                    {

                        newSearchString += " & @mature N";
                    }
                    else
                    {
                        // @categories means only search categories index
                        newSearchString = "@mature N";
                    }
                }


                if (bGetMature && onlyAccessPass)
                {
                    if (newSearchString.Trim().Length > 0)
                    {

                        newSearchString += " & @mature Y";
                    }
                    else
                    {
                        // @categories means only search categories index
                        newSearchString = "@mature Y";
                    }
                }

                //// Thumbnail
                //if (bThumbnailRequired && !assetTypeId.Equals((int)Asset.eASSET_TYPES.PICTURE) && !assetTypeId.Equals((int)Asset.eASSET_TYPES.PATTERN))
                //{
                //    if (newSearchString.Trim().Length > 0)
                //    {

                //        newSearchString += " & @has_image Y";
                //    }
                //    else
                //    {
                //        // @categories means only search categories index
                //        newSearchString = "@has_image Y";
                //    }
                //}



                SearchQuery searchQuery = new SearchQuery(newSearchString);

                // Set match mode to SPH_MATCH_EXTENDED2
                searchQuery.MatchMode = MatchMode.Extended2;

                // Add Sphinx index name to list
                searchQuery.Indexes.Add(sphinxIndex);

                // Set up paging
                searchQuery.Limit = pageSize;
                searchQuery.Offset = (pageNumber - 1) * pageSize;

                // Create search command object
                SearchCommand searchCommand = new SearchCommand(connection);

                // Add newly created search query object to query list
                searchCommand.QueryList.Add(searchQuery);

                // Add some filtering
                if (assetTypeId > 0)
                {
                    searchQuery.AttributeFilters.Add("asset_type_id", (uint)assetTypeId, false);
                }

                // Don't show private
                searchQuery.AttributeFilters.Add("permission", (uint)1, false);


                // Only get last x number of days?
                if (pastDays > 0)
                {
                    searchQuery.AttributeFilters.Add("date_created", (uint) ConvertToTimestamp(DateTime.Now.AddDays(-pastDays)), (uint) ConvertToTimestamp(DateTime.Now.AddYears(1)), false);
                }

                ////// Sorting
                //if (orderBy.Trim().Length.Equals(0))
                //{
                //    searchQuery.SortMode = ResultsSortMode.Relevance;
                //}
                //else
                //{
                //    if (orderBy.ToUpper().Contains("ASC"))
                //    {
                //        searchQuery.SortMode = ResultsSortMode.AttributeAscending;
                //    }
                //    else
                //    {
                //        searchQuery.SortMode = ResultsSortMode.AttributeDescending;
                //    }

                //    // Strip ASC, DESC
                //    searchQuery.SortBy = orderBy.Replace("ASC", "").Replace("DESC", "").Trim();
                //}


                searchQuery.GroupBy = "asset_id";
                searchQuery.GroupFunc = ResultsGroupFunction.Attribute;
                searchQuery.GroupSort = "@count desc";

                searchQuery.MaxMatches = 10000;

                // Execute command on server and obtain results
                try
                {
                    searchCommand.Execute();
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error calling Sphinx " + sphinx.Host + ":" + sphinx.Port, exc);
                }

                string currentAssetId = "";

                // Build result IN clause for MySQL
                foreach (SearchQueryResult result in searchCommand.Result.QueryResults)
                {
                    foreach (Match match in result.Matches)
                    {
                        // Get Asset Id
                        currentAssetId = match.AttributesValues["asset_id"].GetValue().ToString ();

                        if (assetIds.Length > 0)
                        {
                            assetIds = assetIds + "," + currentAssetId;
                        }
                        else
                        {
                            assetIds = currentAssetId;
                        }
                    }

                    // Note this will grab the last total count in collection, ok for this case, but not best place to do this
                    totalCount = (result.TotalFound > searchQuery.MaxMatches ? searchQuery.MaxMatches : result.TotalFound);
                }


                // Nothing found in Sphinx
                if (assetIds.Length.Equals(0))
                {
                    PagedList<Asset> emptyList = new PagedList<Asset>();
                    emptyList.TotalCount = 0;
                    return emptyList;
                }

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();

                string strSelect = "SELECT a.asset_id, file_size, " +
                    " a.content_extension, a.asset_type_id, a.asset_sub_type_id, a.name, " +
                    " COALESCE(a.short_description,'') as short_description, a.created_date, a.asset_rating_id, a.amount, " +
                    " a.status_id, a.mature, a.public, a.over_21_required, " +
                    " a.run_time_seconds, a.owner_id, " +
                    " a.owner_id as user_id, a.owner_username, a.owner_username as username, " +
                    " a.teaser, a.keywords, " +
                    " a.permission, COALESCE(a.permission_group,0) AS permission_group, " +
                    " a.image_full_path, a.thumbnail_small_path, thumbnail_medium_path, " +
                    " thumbnail_large_path, thumbnail_xlarge_path, thumbnail_gen, " +
                    " ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs " +
                    " FROM assets a INNER JOIN assets_stats as ass ON a.asset_id = ass.asset_id " +
                    " where a.asset_id in " +
                    " (" + assetIds + ")" +
                    " ORDER BY FIELD(a.asset_id," + assetIds + ")";

                DataTable dt = Db.Slave.GetDataTable(strSelect, parameters);

                PagedList<Asset> list = new PagedList<Asset>();
                list.TotalCount = Convert.ToUInt32(totalCount);

                foreach (DataRow row in dt.Rows)
                {
                    Asset asset = new Asset(Convert.ToInt32(row["asset_id"]), Convert.ToInt32(row["asset_type_id"]),
                    "0", Convert.ToInt32(row["asset_sub_type_id"]),
                    Convert.ToInt32(row["asset_rating_id"]), Convert.ToInt32(row["owner_id"]),
                    0, 0, 0, 0, 0, 0, "KPOINT", "",
                    row["image_full_path"].ToString(), "", row["thumbnail_xlarge_path"].ToString(),
                    row["thumbnail_large_path"].ToString(), row["thumbnail_medium_path"].ToString(),
                    row["thumbnail_small_path"].ToString(), Convert.ToInt32(row["thumbnail_gen"]),
                    "", Convert.ToInt32(row["status_id"]), Convert.ToDateTime(row["created_date"]),
                    DateTime.Now, 0, "", "", "", "", "", 0, 0, "", "", "", row["content_extension"].ToString(),
                    "", "", Convert.ToInt32(row["permission"]), Convert.ToInt32(row["permission_group"]),
                    true, row["mature"].ToString().Equals("Y"), row["public"].ToString().Equals("Y"),
                    false, row["over_21_required"].ToString().Equals("Y"),
                    "", 0, row["name"].ToString(), "", row["short_description"].ToString(),
                    row["teaser"].ToString(), row["owner_username"].ToString(), row["keywords"].ToString(),
                    "", "", 0);

                    asset.Stats = new AssetStats(Convert.ToInt32(row["asset_id"]), Convert.ToInt32(row["number_of_comments"]), Convert.ToInt32(row["number_of_downloads"]),
                       Convert.ToUInt32(row["number_of_shares"]), 0, Convert.ToUInt32(row["number_of_diggs"]));

                    list.Add(asset);
                }

                return list;

            }
        }

        /// <summary>
        /// method for converting a System.DateTime value to a UNIX Timestamp
        /// </summary>
        /// <param name="value">date to convert</param>
        /// <returns></returns>
        private double ConvertToTimestamp(DateTime value)
        {
            //create Timespan by subtracting the value provided from
            //the Unix Epoch
            TimeSpan span = (value - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());

            //return the total seconds (which is a UNIX timestamp)
            return (double)span.TotalSeconds;
        }



        /// <summary>
        /// Get a list of Assets accessible by user
        /// </summary>
        public PagedList<Asset> GetAccessibleAssetsInCommunity (int communityId, int assetGroupId, bool bGetMature,
            int assetTypeId, int userId, string filter, string orderBy, int pageNumber, int pageSize)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string sqlSelect = "SELECT a.asset_id, file_size, " +
                " a.content_extension, a.asset_type_id, a.asset_sub_type_id, a.name, " +
                " COALESCE(a.short_description,'') as short_description, a.created_date, a.asset_rating_id, a.amount, " +
                " a.status_id, a.mature, a.public, a.over_21_required, " +
                " a.run_time_seconds, a.owner_id, " +
                " a.owner_id as user_id, a.owner_username, a.owner_username as username, " +
                " a.teaser, a.keywords, " +
                " a.permission, COALESCE(a.permission_group,0) AS permission_group, " +
                " a.image_full_path, a.thumbnail_small_path, thumbnail_medium_path, " +
                " thumbnail_large_path, thumbnail_xlarge_path, thumbnail_gen, asset_offsite_id, " +
                " ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs " +
                " FROM asset_channels ac";

            if (bGetMature)
            {
                sqlSelect += ", vw_published_mature_assets  a ";
            }
            else
            {
                sqlSelect += ", vw_published_assets  a ";
            }

            sqlSelect += " INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id ";

            if (assetGroupId > 0)
            {
                sqlSelect += " ,asset_groups ag, asset_group_assets aga ";
            }

            // Build the basic WHERE Clause
            sqlSelect += " WHERE ac.channel_id=@communityId " +
                " AND ac.asset_id=a.asset_id ";

            // Specific asset type
            if (assetTypeId > 0)
            {
                parameters.Add (new MySqlParameter ("@assetTypeId", assetTypeId));
                sqlSelect += " AND a.asset_type_id = @assetTypeId ";
            }

            if (filter.Length > 0)
            {
                sqlSelect += " AND " + filter;
            }

            if (assetGroupId > 0)
            {
                parameters.Add (new MySqlParameter ("@assetGroupId", assetGroupId));
                sqlSelect += " AND ag.asset_group_id = @assetGroupId " +
                    " AND ag.channel_id = @communityId " +
                    " AND ag.asset_group_id = aga.asset_group_id " +
                    " AND aga.asset_id = a.asset_id ";
            }

            sqlSelect += " AND a.permission= " + ((int) eASSET_PERMISSION.PUBLIC);

            parameters.Add (new MySqlParameter ("@communityId", communityId));
            PagedDataTable dt = Db.Slave2.GetPagedDataTable (sqlSelect, orderBy, parameters, pageNumber, pageSize);

            PagedList<Asset> list = new PagedList<Asset> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                Asset asset = new Asset(Convert.ToInt32 (row["asset_id"]), Convert.ToInt32 (row["asset_type_id"]), 
                    "0", Convert.ToInt32 (row["asset_sub_type_id"]),
                    Convert.ToInt32 (row["asset_rating_id"]), Convert.ToInt32 (row["owner_id"]), 
                    0, 0, 0, 0, 0, 0, "KPOINT", "",
                    row["image_full_path"].ToString (), "", row["thumbnail_xlarge_path"].ToString (), 
                    row["thumbnail_large_path"].ToString (), row["thumbnail_medium_path"].ToString (), 
                    row["thumbnail_small_path"].ToString (), Convert.ToInt32 (row["thumbnail_gen"]), 
                    "", Convert.ToInt32 (row["status_id"]), Convert.ToDateTime (row["created_date"]), 
                    DateTime.Now, 0, "", "", "", "", "", 0, 0, "", "", "", row["content_extension"].ToString (),
                    "", "", Convert.ToInt32 (row["permission"]), Convert.ToInt32 (row["permission_group"]),
                    true, row["mature"].ToString ().Equals ("Y"), row["public"].ToString ().Equals ("Y"),
                    false, row["over_21_required"].ToString ().Equals ("Y"),
                    row["asset_offsite_id"].ToString (), 0, row["name"].ToString (), "", row["short_description"].ToString (),
                    row["teaser"].ToString (), row["owner_username"].ToString (), row["keywords"].ToString (),
                    "", "", 0);
                
                asset.Stats = new AssetStats (Convert.ToInt32 (row["asset_id"]), Convert.ToInt32 (row["number_of_comments"]), Convert.ToInt32 (row["number_of_downloads"]),
                   Convert.ToUInt32 (row["number_of_shares"]), 0, Convert.ToUInt32 (row["number_of_diggs"]));

                list.Add (asset);
            }

            return list;
        }



        /// <summary>
        /// Get the asset groups for a channel
        /// </summary>
        public PagedList<AssetGroup> GetAssetGroups(int channelId, string filter, string orderby, int pageNumber, int pageSize)
        {
            string sqlString = " SELECT asset_group_id, name, created_date, last_updated, channel_id, asset_count, description, shuffle " +
                " FROM asset_groups ag " +
                " WHERE ag.channel_id = @channelId ";

            // Filter it?
            if (filter.Length > 0)
            {
                sqlString += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@channelId", channelId));
            PagedDataTable dt = Db.Slave2.GetPagedDataTable(sqlString, "", parameters, pageNumber, pageSize);

            PagedList<AssetGroup> list = new PagedList<AssetGroup>();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new AssetGroup(Convert.ToInt32(row["asset_group_id"]), row["name"].ToString(), Convert.ToDateTime(row["created_date"]), row["last_updated"].ToString(), Convert.ToInt32(row["channel_id"]),
                Convert.ToUInt32(row["asset_count"]), row["description"].ToString(), 0, 0, Convert.ToInt32(row["shuffle"])));
            }

            return list;
        }

        /// <summary>
        /// Get the asset groups for a channel
        /// </summary>
        public PagedList<AssetGroup> GetAssetGroups(int channelId, string assetGroupIds)
        {
            string sqlString = " SELECT asset_group_id, name, created_date, last_updated, channel_id, asset_count, description " +
                " FROM asset_groups ag " +
                " WHERE ag.channel_id = @channelId " +
                " AND ag.asset_group_id IN (" + assetGroupIds + ")" +
                " ORDER BY ag.name ASC ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@channelId", channelId));

            // Limit to to prevent users from abusing system adding groups to channels
            PagedDataTable dt = Db.Slave2.GetPagedDataTable(sqlString, "", parameters, 1, 10);

            PagedList<AssetGroup> list = new PagedList<AssetGroup>();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new AssetGroup(Convert.ToInt32(row["asset_group_id"]), row["name"].ToString(), Convert.ToDateTime(row["created_date"]), row["last_updated"].ToString(), Convert.ToInt32(row["channel_id"]),
                Convert.ToUInt32(row["asset_count"]), row["description"].ToString()));
            }

            return list;
        }

        /// <summary>
        /// GetGroupCategoryAssetGroups
        /// </summary>
        public PagedList<AssetGroup> GetGroupCategoryAssetGroups(int groupCategoryId, int page, int pageSize)
        {
            string sqlString = " SELECT gcag.group_category_id, gcag.asset_group_id, gcag.sort_order, ag.name, ag.created_date, ag.last_updated, ag.channel_id, ag.asset_count, ag.description, ag.shuffle " +
                " FROM asset_groups ag " +
                " INNER JOIN group_category_asset_groups gcag ON gcag.asset_group_id = ag.asset_group_id AND gcag.group_category_id = @groupCategoryId" +
                " ORDER BY gcag.sort_order ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@groupCategoryId", groupCategoryId));

            // Limit to to prevent users from abusing system adding groups to channels
            PagedDataTable dt = Db.Slave2.GetPagedDataTable(sqlString, "", parameters, page, pageSize);

            PagedList<AssetGroup> list = new PagedList<AssetGroup>();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new AssetGroup(Convert.ToInt32(row["asset_group_id"]), row["name"].ToString(), Convert.ToDateTime(row["created_date"]), row["last_updated"].ToString(), Convert.ToInt32(row["channel_id"]),
                Convert.ToUInt32(row["asset_count"]), row["description"].ToString(), Convert.ToInt32(row["group_category_id"]), Convert.ToInt32(row["sort_order"]), Convert.ToInt32(row["shuffle"])));
            }

            return list;
        }

        /// <summary>
        /// IsAssetInGroup
        /// </summary>
        public bool IsAssetInGroup(int assetGroupId, int assetId)
        {
            string sqlSelect = "SELECT asset_group_id " +
                " FROM asset_group_assets " +
                " WHERE asset_group_id = @assetGroupId" +
                " AND asset_id = @assetId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetGroupId", assetGroupId));
            parameters.Add(new MySqlParameter("@assetId", assetId));
            DataRow drAssetGroup = Db.Master.GetDataRow (sqlSelect, parameters);

            return (drAssetGroup != null);
        }

        /// <summary>
        /// GetGroupsWithAsset
        /// </summary>
        public List<int> GetGroupIdsWithAsset(int assetId)
        {
            string sqlSelect = "SELECT asset_group_id " +
                " FROM asset_group_assets " +
                " WHERE asset_id = @assetId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetId", assetId));
            DataTable dt = Db.Master.GetDataTable(sqlSelect, parameters);

            List<int> assetGroupIds = new List<int>();

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    assetGroupIds.Add(Convert.ToInt32(row["asset_group_id"]));
                }
            }

            return assetGroupIds;
        }

        public List<int> GetGroupIdsWithAssetByChannel(int assetId, int channelId)
        {
            string sqlSelect = "SELECT ag.asset_group_id " +
                " FROM asset_groups ag " +
                " INNER JOIN asset_group_assets aga ON aga.asset_group_id = ag.asset_group_id " +
                " AND aga.asset_id = @assetId " +
                " WHERE ag.channel_id = @channelId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@channelId", channelId));
            parameters.Add(new MySqlParameter("@assetId", assetId));
            DataTable dt = Db.Master.GetDataTable(sqlSelect, parameters);

            List<int> assetGroupIds = new List<int>();

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    assetGroupIds.Add(Convert.ToInt32(row["asset_group_id"]));
                }
            }

            return assetGroupIds;
        }

        /// <summary>
        /// Insert a asset group
        /// </summary>
        /// <returns></returns>
        public int InsertAssetGroup(int channelId, string groupName, string groupDescription, int shuffle)
        {
            string sql = "INSERT INTO asset_groups " +
                "(channel_id, name, created_date, description, shuffle " +
                ") VALUES (" +
                "@channelId, @groupName, NOW(), @groupDescription, @shuffle)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@channelId", channelId));
            parameters.Add(new MySqlParameter("@groupName", groupName));
            parameters.Add(new MySqlParameter("@groupDescription", groupDescription));
            parameters.Add(new MySqlParameter("@shuffle", shuffle));
            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Delete a friend group
        /// </summary>
        /// <returns></returns>
        public int DeleteAssetGroup(int channelId, int assetGroupId)
        {
            // Delete all friends from group
            DeleteAllAssetsFromGroup(assetGroupId);

            // Delete the friend group
            string sql = "DELETE FROM asset_groups " +
                " WHERE channel_id = @channelId " +
                " AND asset_group_id = @assetGroupId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@channelId", channelId));
            parameters.Add(new MySqlParameter("@assetGroupId", assetGroupId));
            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// DeleteAllAssetsFromGroup
        /// </summary>
        /// <returns></returns>
        public int DeleteAllAssetsFromGroup(int assetGroupId)
        {
            // Delete any friends in the group
            string sql = "DELETE FROM asset_group_assets " +
                " WHERE asset_group_id = @assetGroupId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetGroupId", assetGroupId));
            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// GetAssetGroup
        /// </summary>
        public AssetGroup GetAssetGroup(int assetGroupId, int channelId)
        {
            string sqlSelect = "SELECT fg.asset_group_id, fg.name, fg.created_date, fg.last_updated, fg.channel_id, fg.asset_count, fg.description, fg.shuffle " +
                " FROM asset_groups fg " +
                " WHERE fg.asset_group_id = @assetGroupId" +
                " AND fg.channel_id = @channelId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@channelId", channelId));
            parameters.Add(new MySqlParameter("@assetGroupId", assetGroupId));
            DataRow row = Db.Slave.GetDataRow(sqlSelect, parameters);

            if (row == null)
            {
                return new AssetGroup();
            }

            return new AssetGroup(Convert.ToInt32(row["asset_group_id"]), row["name"].ToString(), Convert.ToDateTime(row["created_date"]), row["last_updated"].ToString(), Convert.ToInt32(row["channel_id"]),
                Convert.ToUInt32(row["asset_count"]), row["description"].ToString(), 0, 0, Convert.ToInt32(row["shuffle"]));

        }

        /// <summary>
        /// Insert an asset into a group
        /// </summary>
        public int InsertAssetInGroup(int channelId, int assetGroupId, int assetId)
        {
            // Make sure they are not already in the group
            if (IsAssetInGroup(assetGroupId, assetId))
            {
                return 0;
            }

            string sql = "CALL add_asset_group_assets(@assetGroupId, @assetId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetId", assetId));
            parameters.Add(new MySqlParameter("@assetGroupId", assetGroupId));
            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Update the Asset Asset Sort order
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="avatar"></param>
        public int UpdateAssetGroupAssetSortOrder(int assetId, int sortOrder, int assetGroupId)
        {
            string sqlString = "UPDATE asset_group_assets " +
                " SET sort_order = @sortOrder " +
                " WHERE asset_id = @assetId" +
                " AND asset_group_id = @assetGroupId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@sortOrder", sortOrder));
            parameters.Add(new MySqlParameter("@assetId", assetId));
            parameters.Add(new MySqlParameter("@assetGroupId", assetGroupId));

            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        public int MoveAssetToTopOfGroup(int assetGroupId, int assetId)
        {
            string sqlSelect = "SELECT MIN(sort_order) - 1 new_sort_order FROM asset_group_assets WHERE asset_group_id = @assetGroupId";
            int newSortOrder = 0;

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetGroupId", assetGroupId));
            DataRow drResult = Db.Slave.GetDataRow(sqlSelect, parameters);

            if (drResult != null && !drResult["new_sort_order"].Equals(DBNull.Value))
            {
                newSortOrder = Convert.ToInt32(drResult["new_sort_order"]);
            }

            return UpdateAssetGroupAssetSortOrder(assetId, newSortOrder, assetGroupId);
        }

        public int MoveAssetToBottomOfGroup(int assetGroupId, int assetId)
        {
            string sqlSelect = "SELECT MAX(sort_order) + 1 new_sort_order FROM asset_group_assets WHERE asset_group_id = @assetGroupId";
            int newSortOrder = 0;

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetGroupId", assetGroupId));
            DataRow drResult = Db.Slave.GetDataRow(sqlSelect, parameters);

            if (drResult != null && !drResult["new_sort_order"].Equals(DBNull.Value))
            {
                newSortOrder = Convert.ToInt32(drResult["new_sort_order"]);
            }

            return UpdateAssetGroupAssetSortOrder(assetId, newSortOrder, assetGroupId);
        }

        /// <summary>
        /// Remove an asset from a group
        /// </summary>
        /// <returns></returns>
        public int RemoveAssetFromGroup(int assetGroupId, int assetId)
        {
            // Make sure they are not already in the group
            if (!IsAssetInGroup(assetGroupId, assetId))
            {
                return 0;
            }

            string sql = "DELETE FROM asset_group_assets " +
                " WHERE asset_id = @assetId " +
                " AND asset_group_id = @assetGroupId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetId", assetId));
            parameters.Add(new MySqlParameter("@assetGroupId", assetGroupId));
            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Remove an asset from a group
        /// </summary>
        /// <returns></returns>
        public int RemoveAssetFromAllGroups(int assetId)
        {
            string sql = "DELETE FROM asset_group_assets " +
                " WHERE asset_id = @assetId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetId", assetId));
            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Remove an asset from a group
        /// </summary>
        /// <returns></returns>
        public int RemoveAssetFromGroupsByChannel(int channelId, int assetId)
        {
           string sqlSelect = "SELECT asset_group_id " +
                " FROM asset_groups ag " +
                " WHERE ag.channel_id = @channelId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@channelId", channelId));
            DataTable dtAssetGroupIds = Db.Master.GetDataTable(sqlSelect, parameters);

            //if no play list found abort
            if (dtAssetGroupIds.Rows.Count <= 0)
            {
                return 0;
            }

            string inValues = "";
            int i;
            for (i = 0; i < dtAssetGroupIds.Rows.Count - 1; i++)
            {
                inValues += (dtAssetGroupIds.Rows[i].ItemArray.GetValue(0).ToString() + ", ");
            }
            inValues += dtAssetGroupIds.Rows[i].ItemArray.GetValue(0).ToString();

            //select asset group ids separately due to trigger conflicts
            string sql = "DELETE FROM asset_group_assets " +
                " WHERE asset_id = @assetId " +
                " AND asset_group_id IN (" + inValues + ") ";

            parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetId", assetId));
            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// GetWokPlaylist
        /// </summary>
        public PagedList<AssetGroupAsset> GetWokPlaylist (int assetGroupId)
        {
            string sql = "CALL wok.get_playlist(0,0,@assetGroupId,0)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetGroupId", assetGroupId));
            DataTable dt = Db.Master.GetDataTable(sql, parameters);

            PagedList<AssetGroupAsset> list = new PagedList<AssetGroupAsset>();
            list.TotalCount = (uint) dt.Rows.Count;
            int i = 0;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new AssetGroupAsset(Convert.ToInt32(row["asset_id"]), row["name"].ToString (), row["asset_offsite_id"].ToString (), Convert.ToInt32(row["asset_type_id"]), Convert.ToInt32(row["asset_sub_type_id"]),
                     Convert.ToInt32(row["run_time_seconds"]), row["thumbnail_medium_path"].ToString(), row["mature"].ToString().Equals("Y"),
                     row["public"].ToString().Equals("Y"), row["over_21_required"].ToString().Equals("Y"),
                     Convert.ToInt32(row["number_of_diggs"]), row["media_path"] != null ? row["media_path"].ToString() : "", i));

                i++;
            }

            return list;
            
        }

        public List<int> GetUserUploadedAssetIds(int userId)
        {
            List<int> ids = new List<int>();
            
            string sql = "SELECt asset_id FROM kaneva.assets WHERE owner_id = @userId";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            
            DataTable dt = Db.Master.GetDataTable(sql, parameters);
            foreach (DataRow row in dt.Rows)
            {
                ids.Add(Convert.ToInt32(row["asset_id"]));
            }

            return ids;
        }

        public PagedList<Asset> GetUserUploadedAssets(int userId, List<int> assetTypeIds, string orderBy, int pageNum, int itemsPerPage)
        {
            PagedList<Asset> list = new PagedList<Asset>();

            string strSelect = "SELECT a.asset_id, file_size, " +
                " a.content_extension, a.asset_type_id, a.asset_sub_type_id, a.name, " +
                " COALESCE(a.short_description,'') as short_description, a.created_date, a.asset_rating_id, a.amount, " +
                " a.status_id, a.mature, a.public, a.over_21_required, " +
                " a.run_time_seconds, a.owner_id, " +
                " a.owner_id as user_id, a.owner_username, a.owner_username as username, " +
                " a.teaser, a.keywords, " +
                " a.permission, COALESCE(a.permission_group,0) AS permission_group, " +
                " a.image_full_path, a.thumbnail_small_path, thumbnail_medium_path, " +
                " thumbnail_large_path, thumbnail_xlarge_path, thumbnail_gen, " +
                " ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs, a.asset_offsite_id " +
                " FROM assets a INNER JOIN assets_stats as ass ON a.asset_id = ass.asset_id " +
                " where a.owner_id = @userId ";

            if (assetTypeIds != null && assetTypeIds.Count > 0)
            {
                strSelect += " AND a.asset_type_id IN (" + string.Join(",", assetTypeIds) + ") ";
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            
            DataTable dt = Db.Slave.GetPagedDataTable(strSelect, orderBy, parameters, pageNum, itemsPerPage);
            if (dt == null)
                return list;

            foreach (DataRow row in dt.Rows)
            {
                Asset asset = new Asset(Convert.ToInt32(row["asset_id"]), Convert.ToInt32(row["asset_type_id"]),
                "0", Convert.ToInt32(row["asset_sub_type_id"]),
                Convert.ToInt32(row["asset_rating_id"]), Convert.ToInt32(row["owner_id"]),
                0, 0, 0, 0, 0, 0, "KPOINT", "",
                row["image_full_path"].ToString(), "", row["thumbnail_xlarge_path"].ToString(),
                row["thumbnail_large_path"].ToString(), row["thumbnail_medium_path"].ToString(),
                row["thumbnail_small_path"].ToString(), Convert.ToInt32(row["thumbnail_gen"]),
                "", Convert.ToInt32(row["status_id"]), Convert.ToDateTime(row["created_date"]),
                DateTime.Now, 0, "", "", "", "", "", 0, 0, "", "", "", row["content_extension"].ToString(),
                "", "", Convert.ToInt32(row["permission"]), Convert.ToInt32(row["permission_group"]),
                true, row["mature"].ToString().Equals("Y"), row["public"].ToString().Equals("Y"),
                false, row["over_21_required"].ToString().Equals("Y"),
                row["asset_offsite_id"].ToString(), 0, row["name"].ToString(), "", row["short_description"].ToString(),
                row["teaser"].ToString(), row["owner_username"].ToString(), row["keywords"].ToString(),
                "", "", 0);

                asset.Stats = new AssetStats(Convert.ToInt32(row["asset_id"]), Convert.ToInt32(row["number_of_comments"]), Convert.ToInt32(row["number_of_downloads"]),
                   Convert.ToUInt32(row["number_of_shares"]), 0, Convert.ToUInt32(row["number_of_diggs"]));

                list.Add(asset);
            }

            return list;
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    }
}
