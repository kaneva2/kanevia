///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;
using System.Collections.Specialized;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MYSQLGameDeveloperDao : IGameDeveloperDao
    {

        #region developers

        /// <summary>
        /// Gets a GameDeveloper By kaneva userId.
        /// </summary>
        /// <param name="userId">Unique game developer identifier.</param>
        /// <returns>GameDeveloper.</returns>
        public GameDeveloper GetGameDeveloperByUserId(int userId)
        {
            string sql = "SELECT company_id, user_id, role_id FROM company_developers WHERE user_id = @userId";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            DataRow row = Db.Developer.GetDataRow(sql.ToString(), parameters);

            return (new GameDeveloper(Convert.ToInt32(row["user_id"]), Convert.ToInt32(row["company_id"]), Convert.ToInt32(row["role_id"]), new User(), new NameValueCollection()));
        }

        /// <summary>
        /// Gets a GameDeveloper By kaneva userId without the user information
        /// Can be used to see if they exist in the developer database without querying the kaneva database
        /// </summary>
        /// <param name="userId">Unique game developer identifier.</param>
        /// <returns>GameDeveloper.</returns>
        public GameDeveloper GetGameDeveloperNoUserInfo(int userId)
        {
            string sql = "SELECT company_id, user_id, role_id FROM company_developers WHERE user_id = @userId";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            DataRow row = Db.Developer.GetDataRow(sql.ToString(), parameters);

            GameDeveloper gameDeveloper = new GameDeveloper(Convert.ToInt32(row["user_id"]), Convert.ToInt32(row["company_id"]), Convert.ToInt32(row["role_id"]), new User(), new NameValueCollection());

            return gameDeveloper;

        }

        /// <summary>
        /// Gets a GameDeveloper By kaneva userId without the user information
        /// Can be used to see if they exist in the developer database without querying the kaneva database
        /// </summary>
        /// <param name="userId">Unique game developer identifier.</param>
        /// <returns>GameDeveloper.</returns>
        public GameDeveloper GetGameDeveloperNoUserInfo(int userId, int companyId)
        {
            string sql = "SELECT company_id, user_id, role_id FROM company_developers WHERE user_id = @userId AND company_id = @companyId";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@companyId", companyId));
            DataRow row = Db.Developer.GetDataRow(sql.ToString(), parameters);

            GameDeveloper gameDeveloper = new GameDeveloper(Convert.ToInt32(row["user_id"]), Convert.ToInt32(row["company_id"]), Convert.ToInt32(row["role_id"]), new User(), new NameValueCollection());

            return gameDeveloper;

        }

        /// <summary>
        /// Get the friends for a user
        /// </summary>
        public PagedList<GameDeveloper> GetGameDevelopersList(int company_id, string filter, string orderby, int pageNumber, int pageSize)
        {
            // UNION WAY
            string strQuery = "";

            // Friends I invited
            strQuery = "SELECT company_id, user_id, role_id FROM company_developers WHERE company_id = @company_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@company_id", company_id));

            if ((filter != null) && (filter != ""))
            {
                strQuery += " AND " + filter;
            }

            if ((orderby != null) && (orderby != ""))
            {
                strQuery += " ORDER BY " + orderby;
            }

            PagedDataTable dt = Db.Developer.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<GameDeveloper> list = new PagedList<GameDeveloper>();
            //assigns total count
            list.TotalCount = dt.TotalCount;
            GameDeveloper gameDeveloper;
            parameters.Clear();

            foreach (DataRow row in dt.Rows)
            {
                gameDeveloper = new GameDeveloper(Convert.ToInt32(row["user_id"]), Convert.ToInt32(row["company_id"]), Convert.ToInt32(row["role_id"]), new User(), new NameValueCollection());

                //temporary and inefficient way to get the user information 
                //it is done this way to prevent a join across database in case the databases are ever
                //spilt across different servers

                //get user data
                string strQuery2 = "SELECT u.user_id, u.age, u.username, u.role, u.first_name, u.last_name, u.gender, u.homepage, u.email, u.show_mature, u.status_id, u.wok_player_id, " +
                " com.thumbnail_path, com.name_no_spaces, com.url, com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_xlarge_path " +
                " FROM users u INNER JOIN communities_personal com ON com.creator_id = u.user_id WHERE u.user_id = @userId ";

                parameters.Clear();
                parameters.Add(new MySqlParameter("@userId", gameDeveloper.UserId));
                DataRow row2 = Db.Master.GetDataRow(strQuery2.ToString(), parameters);

                // Set the owner information
                gameDeveloper.UserInfo.Age = Convert.ToInt32(row2["age"]);
                gameDeveloper.UserInfo.UserId = Convert.ToInt32(row2["user_id"]);
                gameDeveloper.UserInfo.Username = row2["username"].ToString();
                gameDeveloper.UserInfo.Role = Convert.ToInt32(row2["role"]);
                gameDeveloper.UserInfo.FirstName = row2["first_name"].ToString();
                gameDeveloper.UserInfo.LastName = row2["last_name"].ToString();
                gameDeveloper.UserInfo.Gender = row2["gender"].ToString();
                gameDeveloper.UserInfo.Homepage = row2["homepage"].ToString();
                gameDeveloper.UserInfo.Email = row2["email"].ToString();
                gameDeveloper.UserInfo.ShowMature = Convert.ToInt32(row2["show_mature"]).Equals(1) ? true : false;
                gameDeveloper.UserInfo.StatusId = Convert.ToInt32(row2["status_id"]);
                gameDeveloper.UserInfo.WokPlayerId = Convert.ToInt32(row2["wok_player_id"]);
                gameDeveloper.UserInfo.ThumbnailPath = row2["thumbnail_path"].ToString();
                gameDeveloper.UserInfo.NameNoSpaces = row2["name_no_spaces"].ToString();
                gameDeveloper.UserInfo.URL = row2["url"].ToString();
                gameDeveloper.UserInfo.ThumbnailSmallPath = row2["thumbnail_small_path"].ToString();
                gameDeveloper.UserInfo.ThumbnailMediumPath = row2["thumbnail_medium_path"].ToString();
                gameDeveloper.UserInfo.ThumbnailLargePath = row2["thumbnail_large_path"].ToString();
                gameDeveloper.UserInfo.ThumbnailXlargePath = row2["thumbnail_xlarge_path"].ToString();

                list.Add(gameDeveloper);
            }

            return list;
        }

        /// <summary>
        /// Get Paged DataTable of Developers and some of their personal information
        /// </summary>
        /*public PagedDataTable GetGameDevelopers(int company_id, string filter, string orderby, int pageNumber, int pageSize)
        {
            // UNION WAY
            string strQuery = "";

            // Friends I invited
            strQuery = "SELECT company_id, user_id, role_id FROM company_developers WHERE company_id = @company_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@company_id", company_id));
            PagedDataTable dt = Db.Developer.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<GameDeveloper> list = new PagedList<GameDeveloper>();
            GameDeveloper gameDeveloper;
            parameters.Clear();

            foreach (DataRow row in dt.Rows)
            {
                gameDeveloper = new GameDeveloper(Convert.ToInt32(row["user_id"]), Convert.ToInt32(row["company_id"]), Convert.ToInt32(row["role_id"]), new User());

                //temporary and inefficient way to get the user information 
                //it is done this way to prevent a join across database in case the databases are ever
                //spilt across different servers

                //get user data
                string strQuery2 = "SELECT u.user_id, u.age, u.username, u.role, u.first_name, u.last_name, u.gender, u.homepage, u.email, u.show_mature, u.status_id, u.wok_player_id, " +
                " com.thumbnail_path, com.name_no_spaces, com.url, com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_xlarge_path " +
                " FROM users u INNER JOIN communities_personal com ON com.creator_id = u.user_id WHERE u.user_id = @userId ";

                parameters.Clear();
                parameters.Add(new MySqlParameter("@userId", gameDeveloper.UserId));
                DataRow row2 = Db.Master.GetDataRow(strQuery2.ToString(), parameters);

                // Set the owner information
                gameDeveloper.UserInfo.Age = Convert.ToInt32(row2["age"]);
                gameDeveloper.UserInfo.UserId = Convert.ToInt32(row2["user_id"]);
                gameDeveloper.UserInfo.Username = row2["username"].ToString();
                gameDeveloper.UserInfo.Role = Convert.ToInt32(row2["role"]);
                gameDeveloper.UserInfo.FirstName = row2["first_name"].ToString();
                gameDeveloper.UserInfo.LastName = row2["last_name"].ToString();
                gameDeveloper.UserInfo.Gender = row2["gender"].ToString();
                gameDeveloper.UserInfo.Homepage = row2["homepage"].ToString();
                gameDeveloper.UserInfo.Email = row2["email"].ToString();
                gameDeveloper.UserInfo.ShowMature = Convert.ToInt32(row2["show_mature"]).Equals(1) ? true : false;
                gameDeveloper.UserInfo.StatusId = Convert.ToInt32(row2["status_id"]);
                gameDeveloper.UserInfo.WokPlayerId = Convert.ToInt32(row2["wok_player_id"]);
                gameDeveloper.UserInfo.ThumbnailPath = row2["thumbnail_path"].ToString();
                gameDeveloper.UserInfo.NameNoSpaces = row2["name_no_spaces"].ToString();
                gameDeveloper.UserInfo.URL = row2["url"].ToString();
                gameDeveloper.UserInfo.ThumbnailSmallPath = row2["thumbnail_small_path"].ToString();
                gameDeveloper.UserInfo.ThumbnailMediumPath = row2["thumbnail_medium_path"].ToString();
                gameDeveloper.UserInfo.ThumbnailLargePath = row2["thumbnail_large_path"].ToString();
                gameDeveloper.UserInfo.ThumbnailXlargePath = row2["thumbnail_xlarge_path"].ToString();

                list.Add(gameDeveloper);
            }

            return list;
        }*/

        /// <summary>
        /// Insert a Game Developer
        /// </summary>
        /// <param name="gameDev"></param>
        /// <returns></returns>
        public int InsertGameDeveloper(GameDeveloper gameDev)
        {
            // Send the message
            string sql = "INSERT INTO company_developers " +
                "(company_id, user_id, role_id) VALUES (@companyId, @userId, @developerRoleId )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@companyId", gameDev.CompanyId));
            parameters.Add(new MySqlParameter("@userId", gameDev.UserId));
            parameters.Add(new MySqlParameter("@developerRoleId", gameDev.DeveloperRoleId));

            return Db.Developer.ExecuteNonQuery(sql, parameters);
        }

        //--manage game functions---
        /// <summary>
        /// Update Game
        /// </summary>
        /// <param name="game"></param>
        /// <returns>int</returns>
        public int UpdateGameDeveloper(GameDeveloper gameDev)
        {
            // Send the message
            string sql = "UPDATE company_developers SET " +
                "role_id = @developerRoleId WHERE company_id = @companyId AND user_id = @userId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@developerRoleId", gameDev.DeveloperRoleId));
            parameters.Add(new MySqlParameter("@companyId", gameDev.CompanyId));
            parameters.Add(new MySqlParameter("@userId", gameDev.UserId));

            return Db.Developer.ExecuteNonQuery(sql, parameters);
        }

        //--manage game functions---
        /// <summary>
        /// Update Game
        /// </summary>
        /// <param name="game"></param>
        /// <returns>int</returns>
        public int DeleteGameDeveloper(int gameDevId)
        {
            // Send the message
            string sql = "DELETE FROM company_developers WHERE user_id = @userId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", gameDevId));

            return Db.Developer.ExecuteNonQuery(sql, parameters);
        }

        public int InsertGameDeveloperEmail (string email)
        {
            // Send the message
            string sql = "INSERT IGNORE INTO developer_emails " +
                "(email, signup_date) VALUES (@email, NOW())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@email", email));

            return Db.Developer.ExecuteNonQuery (sql, parameters);
        }

        #endregion

        #region Developer Invitations

        /// <summary>
        /// Get an invite by the invitation Id
        /// </summary>
        /// <returns>DeveloperInvitation</returns>
        public DeveloperInvitation GetCompanyInvitation(int inviteId, int companyId)
        {
            string strQuery = "SELECT invitations_id, validation_key, email, company_id, invitation_date, message, role_id, name FROM invitations " +
                "WHERE company_id = @companyId AND invitations_id = @inviteId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@companyId", companyId));
            parameters.Add(new MySqlParameter("@inviteId", inviteId));

            DataRow row = Db.Developer.GetDataRow(strQuery.ToString(), parameters);

            return new DeveloperInvitation(Convert.ToInt32(row["invitations_id"]), Convert.ToInt32(row["company_id"]), Convert.ToInt32(row["role_id"]),
                     row["validation_key"].ToString(), row["email"].ToString(), row["message"].ToString(),
                     row["name"].ToString(), Convert.ToDateTime(row["invitation_date"]));
        }

        /// <summary>
        /// Get an invite by the validation key
        /// </summary>
        /// <returns>DeveloperInvitation</returns>
        public DeveloperInvitation GetCompanyInvitation(string validationKey)
        {
            string strQuery = "SELECT invitations_id, validation_key, email, company_id, invitation_date, message, role_id, name FROM invitations " +
                "WHERE validation_key = @validationKey";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@validationKey", validationKey));

            DataRow row = Db.Developer.GetDataRow(strQuery.ToString(), parameters);

            return new DeveloperInvitation(Convert.ToInt32(row["invitations_id"]), Convert.ToInt32(row["company_id"]), Convert.ToInt32(row["role_id"]),
                     row["validation_key"].ToString(), row["email"].ToString(), row["message"].ToString(),
                     row["name"].ToString(), Convert.ToDateTime(row["invitation_date"]));
        }

        /// <summary>
        /// Get a companies current invitations in a paged list
        /// </summary>
        /// <returns>PagedList</returns>
        public PagedList<DeveloperInvitation> GetCompanyInvitationsList(int companyId, string filter, string orderby, int pageNumber, int pageSize)
        {

            string strQuery = "SELECT invitations_id, validation_key, email, company_id, invitation_date, message, role_id, name FROM invitations " +
                "WHERE company_id = @companyId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@companyId", companyId));

            //add on where if a company is specified
            if ((filter != null) && (filter != ""))
            {
                strQuery += " AND " + filter;
            }

            PagedDataTable dt = Db.Developer.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<DeveloperInvitation> list = new PagedList<DeveloperInvitation>();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new DeveloperInvitation(Convert.ToInt32(row["invitations_id"]), Convert.ToInt32(row["company_id"]), Convert.ToInt32(row["role_id"]),
                     row["validation_key"].ToString(), row["email"].ToString(), row["message"].ToString(),
                     row["name"].ToString(), Convert.ToDateTime(row["invitation_date"])));
            }

            return list;
        }

        /// <summary>
        /// Insert a Game Developer
        /// </summary>
        /// <param name="gameDev"></param>
        /// <returns></returns>
        public int InsertInvitation(DeveloperInvitation invite)
        {
            // Send the message
            string sql = "INSERT INTO invitations " +
                "(validation_key, email, company_id, invitation_date, message, name, role_id) VALUES (@key, @email, @companyId, @inviteDate, @message, @name, @roleId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@key", invite.ValidationKey));
            parameters.Add(new MySqlParameter("@email", invite.Email));
            parameters.Add(new MySqlParameter("@companyId", invite.CompanyId));
            parameters.Add(new MySqlParameter("@inviteDate", invite.InviteDate));
            parameters.Add(new MySqlParameter("@message", invite.Message));
            parameters.Add(new MySqlParameter("@name", invite.Name));
            parameters.Add(new MySqlParameter("@roleId", invite.RoleId));

            return Db.Developer.ExecuteNonQuery(sql, parameters);
        }


        //--manage game functions---
        /// <summary>
        /// Update Game
        /// </summary>
        /// <param name="game"></param>
        /// <returns>int</returns>
        public int DeleteInvitation(DeveloperInvitation invite)
        {
            // Send the message
            string sql = "DELETE FROM invitations WHERE invitations_id = @inviteId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@inviteId", invite.InvitationId));

            return Db.Developer.ExecuteNonQuery(sql, parameters);
        }

        #endregion

        #region Tracking

        /// <summary>
        /// Insert a Game Developer
        /// </summary>
        /// <param name="gameDev"></param>
        /// <returns></returns>
        public int TrackDownLoads(User user, string itemDownLoaded)
        {
            // Send the message
            string sql = "INSERT INTO download_log " +
                "(kaneva_user_id, item_downloaded, download_date) VALUES (@userId, @itemDL, @date )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", user.UserId));
            parameters.Add(new MySqlParameter("@itemDL", itemDownLoaded));
            parameters.Add(new MySqlParameter("@date", DateTime.Now));

            return Db.Developer.ExecuteNonQuery(sql, parameters);
        }

        #endregion
    }
}
