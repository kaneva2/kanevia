///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLBlogDao : IBlogDao
    {
        /// <summary>
        /// Get the blog for the given blogId
        /// </summary>
        public Blog GetBlog(int blogId)
        {
            string sqlSelectBlog = "SELECT b.blog_id, b.subject, COALESCE(b.community_id, 0) as community_id, " +
                " b.subject, b.body_text, b.created_date, b.created_user_id, b.number_of_comments, number_of_views, " +
                " b.status_id, b.last_updated_date, b.last_updated_user_id, b.ip_address, b.keywords, " +
                " u.username, com.name_no_spaces " +
                " FROM users u, communities com, blogs b " +
                " where b.status_id = " + (int) BlogStatus.Active +
                " AND b.created_user_id = u.user_id" +
                " AND b.blog_id = @blogId " +
                " AND is_personal=1 " +
                " AND u.user_id = com.creator_id ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@blogId", blogId));
            DataRow row = Db.Slave2.GetDataRow(sqlSelectBlog, parameters);

            if (row == null)
            {
                return new Blog ();
            }

            return new Blog(Convert.ToInt32(row["blog_id"]), Convert.ToInt32(row["community_id"]), row["subject"].ToString(), row["body_text"].ToString(),
                Convert.ToInt32(row["created_user_id"]), Convert.ToInt32(row["number_of_comments"]), Convert.ToInt32(row["number_of_views"]), Convert.ToDateTime(row["created_date"]),
                Convert.ToDateTime(row["last_updated_date"]), Convert.ToInt32(row["last_updated_user_id"]), row["ip_address"].ToString(), Convert.ToInt32(row["status_id"]),
                row["keywords"].ToString(), row["username"].ToString(), row["name_no_spaces"].ToString());
        }


        /// <summary>
        /// Get a list of blogs
        /// </summary>
        public PagedList<Blog> GetBlogs(int communityId, string filter, string orderby, int pageNumber, int pageSize)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sqlString = "SELECT b.blog_id, b.subject, COALESCE(b.community_id, 0) as community_id, " +
                " b.subject, b.body_text, b.created_date, b.created_user_id, b.number_of_comments, number_of_views, " +
                " b.status_id, b.last_updated_date, b.last_updated_user_id, b.ip_address, b.keywords, " +
                " u.username " +
                " FROM users u, blogs b " +
                " WHERE b.status_id = " + (int) BlogStatus.Active +
                " AND b.created_user_id = u.user_id";

            // Do they want a specific community?
            if (communityId == 0)
            {
                sqlString += " AND b.community_id IS NULL";
            }
            else
            {
                sqlString += " AND b.community_id = @communityId ";
                parameters.Add(new MySqlParameter("@communityId", communityId));
            }

            // Filter it?
            if (filter.Trim().Length > 0)
            {
                sqlString += " AND " + filter;
            }

            PagedDataTable dt = Db.Slave2.GetPagedDataTable(sqlString, orderby, parameters, pageNumber, pageSize);

            PagedList<Blog> list = new PagedList<Blog>();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Blog(Convert.ToInt32(row["blog_id"]), Convert.ToInt32(row["community_id"]), row["subject"].ToString(), row["body_text"].ToString(),
                    Convert.ToInt32(row["created_user_id"]), Convert.ToInt32(row["number_of_comments"]), Convert.ToInt32(row["number_of_views"]), Convert.ToDateTime(row["created_date"]),
                    Convert.ToDateTime(row["last_updated_date"]), Convert.ToInt32(row["last_updated_user_id"]), row["ip_address"].ToString(), Convert.ToInt32(row["status_id"]),
                    row["keywords"].ToString(), row["username"].ToString(), "")
                );
            }
            return list;
        }

        /// <summary>
        /// Get a list of all a users blogs, including personal and ones in a channel
        /// </summary>
        /// <returns></returns>
        public PagedList<Blog> GetAllUserBlogs(int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            string sqlString = "SELECT b.blog_id, b.subject, COALESCE(b.community_id, 0) as community_id, " +
                " b.subject, b.body_text, b.created_date, b.created_user_id, b.number_of_comments, number_of_views, " +
                " b.status_id, b.last_updated_date, COALESCE(b.last_updated_user_id, 0) as last_updated_user_id, b.ip_address, b.keywords " +
                " FROM blogs b " +
                " WHERE b.status_id = " + (int)BlogStatus.Active +
                " AND b.created_user_id = @userId ";

            if (filter.Trim().Length > 0)
            {
                sqlString += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            PagedDataTable dt = Db.Slave.GetPagedDataTable(sqlString, orderby, parameters, pageNumber, pageSize);

            PagedList<Blog> list = new PagedList<Blog>();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Blog(Convert.ToInt32(row["blog_id"]), Convert.ToInt32(row["community_id"]), row["subject"].ToString(), row["body_text"].ToString(),
                    Convert.ToInt32(row["created_user_id"]), Convert.ToInt32(row["number_of_comments"]), Convert.ToInt32(row["number_of_views"]), Convert.ToDateTime(row["created_date"]),
                    Convert.ToDateTime(row["last_updated_date"]), Convert.ToInt32(row["last_updated_user_id"]), row["ip_address"].ToString(), Convert.ToInt32(row["status_id"]),
                    row["keywords"].ToString(), "", "")
                );
            }
            return list;
        }

        /// <summary>
        /// Insert a new blog
        /// </summary>
        public int InsertBlog(int communityId, string subject, string bodyText, int userId, string createdUsername, string ipAddress, string keywords)
        {
            string comId = (communityId == 0) ? "NULL" : communityId.ToString();

            string sqlString = "INSERT INTO blogs ( " +
                " community_id, created_user_id,  " +
                " subject, body_text, " +
                " created_date, last_updated_user_id, last_updated_date, " +
                " status_id, ip_address, keywords" +
                " ) VALUES (" +
                comId + ", @userId, @subject, @bodyText, NOW(), @userId, NOW()," +
                (int)BlogStatus.Active + ", @ipAddress, @keywords)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add (new MySqlParameter("@userId", userId));
            parameters.Add (new MySqlParameter("@ipAddress", ipAddress));
            parameters.Add (new MySqlParameter ("@subject", subject));
            parameters.Add (new MySqlParameter ("@bodyText", bodyText));

            int blogId = Db.Master.Insert(sqlString, parameters,1);

            if (blogId > 0)
            {
                // Update the blog count
                UpdateBlogCount(communityId, 1);
            }

            return blogId;
        }

        /// <summary>
        /// Update a blog
        /// </summary>
        public int UpdateBlog(int userId, int blogId, string subject, string bodyText, int statusId, string keywords)
        {
            string sqlString = "UPDATE blogs " +
                " SET " +
                " last_updated_user_id = @userId," +
                " last_updated_date = NOW(), " +
                " status_id = @statusId, " +
                " subject = @subject," +
                " body_text = @bodyText, " +
                " keywords = @keywords " +
                " WHERE blog_id = @blogId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@blogId", blogId));
            parameters.Add(new MySqlParameter("@statusId", statusId));
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@subject", subject));
            parameters.Add(new MySqlParameter("@bodyText", bodyText));

            Db.Master.ExecuteNonQuery(sqlString, parameters);
            return 0;
        }

        /// <summary>
        /// DeleteBlog
        /// </summary>
        /// <param name="blogId"></param>
        /// <param name="userId"></param>
        public void DeleteBlog(int blogId, int userId, bool bPermanent)
        {
            if (!IsBlogOwner(userId, blogId))
            {
                return;
            }

            if (bPermanent)
            {
                Blog blog = GetBlog(blogId);
                
                string sqlString = "DELETE FROM blogs " +
                    " WHERE blog_id = @blogId";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            
                parameters.Add(new MySqlParameter("@blogId", blogId));
                Db.Master.ExecuteNonQuery(sqlString, parameters);

                if (blog.StatusId == (int)BlogStatus.Active)
                {
                    UpdateBlogCount(blog.CommunityId, -1);
                }
            }
            else
            {
                DeleteBlog(blogId, userId);
            }
        }

        /// <summary>
        /// DeleteBlog
        /// </summary>
        /// <param name="blogId"></param>
        /// <param name="userId"></param>
        public void DeleteBlog(int blogId, int userId)
        {
            Blog blog = GetBlog(blogId);
            
            string sqlString = "UPDATE blogs " +
                " SET status_id = " + (int)BlogStatus.Deleted +
                " , last_updated_user_id = @userId " +
                " , last_updated_date = NOW() " +
                " WHERE blog_id = @blogId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@blogId", blogId));
            parameters.Add(new MySqlParameter("@userId", userId));
            Db.Master.ExecuteNonQuery(sqlString, parameters);

            if (blog.StatusId == (int)BlogStatus.Active)
            {
                UpdateBlogCount(blog.CommunityId, -1);
            }
        }

        private void UpdateBlogCount(int communityId, int updateValue)
        {
            string sqlString = "UPDATE channel_stats SET number_of_blogs = number_of_blogs + @updateValue WHERE channel_id = @communityId;";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            parameters.Add(new MySqlParameter("@updateValue", updateValue));
            Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// IsBlogOwner
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="blogId"></param>
        /// <returns></returns>
        public bool IsBlogOwner(int userId, int blogId)
        {
            string sqlSelect = "SELECT COUNT(*) " +
                " FROM blogs " +
                " WHERE created_user_id = @userId " +
                " AND blog_id = @blogId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@blogId", blogId));
            parameters.Add(new MySqlParameter("@userId", userId));
            int result = Convert.ToInt32(Db.Slave2.GetScalar(sqlSelect, parameters));
            return (result > 0);
        }

        /// <summary>
        /// GetBlogKeywords
        /// </summary>
        public DataTable GetBlogKeywords(int channelId)
        {
           string sqlSelect = " SELECT DISTINCT keywords FROM blogs " +
                " WHERE community_id = @channelId AND status_id = " +
                (int)BlogStatus.Active;

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@channelId", channelId));
            return Db.Slave2.GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// Insert a blog comment
        /// </summary>
        public int InsertBlogComment(int blogId, int userId, string bodyText, string ipAddress)
        {
            string sqlString = "INSERT INTO blog_comments ( " +
                " blog_id, " +
                " user_id, ip_address, body_text, " +
                " created_date, last_updated_date, status_id" +
                " ) VALUES (" +
                " @blogId," +
                "@userId, @ipAddress, @bodyText, " +
                " NOW(), NOW(), " + (int)BlogStatus.Active + ")";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@blogId", blogId));
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@ipAddress", ipAddress));
            parameters.Add(new MySqlParameter("@bodyText", bodyText));

            int blogCommentId = Db.Master.Insert(sqlString, parameters, 1);

            // Update the comments count
            sqlString = "UPDATE blogs " +
                " SET number_of_comments = (number_of_comments + 1) " +
                " WHERE blog_id = @blogId ";

            parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@blogId", blogId));
            Db.Master.ExecuteNonQuery(sqlString, parameters);

            return 0;
        }

        /// <summary>
        /// UpdateBlogComment
        /// </summary>
        public int UpdateBlogComment(int blogCommentId, int userId, string bodyText, string ipAddress)
        {
            string sqlString = "UPDATE blog_comments SET " +
                " ip_address = @ipAddress," +
                " body_text = @bodyText," +
                " last_updated_date = NOW()," +
                " last_updated_user_id = @userId " +
                " WHERE blog_comment_id = @blogCommentId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@blogCommentId", blogCommentId));
            parameters.Add(new MySqlParameter("@ipAddress", ipAddress));
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@bodyText", bodyText));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// Get a blog comment
        /// </summary>
        public BlogComment GetBlogComment(int blogCommentId)
        {
            string sqlSelect = "SELECT bc.blog_comment_id, bc.blog_id, bc.user_id, bc.subject, " +
                " bc.body_text, bc.ip_address, bc.created_date, bc.last_updated_date, " +
                " COALESCE(bc.last_updated_user_id, 0) as last_updated_user_id, bc.status_id " +
                " FROM blog_comments bc " +
                " WHERE blog_comment_id = @blogCommentId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@blogCommentId", blogCommentId));
            DataRow row = Db.Slave2.GetDataRow(sqlSelect, parameters);

            if (row == null)
            {
                return new BlogComment();
            }

            return new BlogComment(Convert.ToInt32(row["blog_comment_id"]), Convert.ToInt32(row["blog_id"]), Convert.ToInt32(row["user_id"]), row["subject"].ToString(), 
                row["body_text"].ToString(), row["ip_address"].ToString(), Convert.ToDateTime(row["created_date"]), Convert.ToDateTime(row["last_updated_date"]), 
                Convert.ToInt32(row["last_updated_user_id"]), Convert.ToInt32(row["status_id"]), "", "");
        }

        /// <summary>
        /// Get a list of blog comments
        /// </summary>
        public PagedList<BlogComment> GetBlogComments(int blogId, int pageNumber, int pageSize)
        {
            string sqlQuery = "SELECT bc.blog_comment_id, bc.blog_id, bc.subject, " +
                " bc.body_text, bc.ip_address, bc.created_date, bc.last_updated_date, COALESCE(bc.last_updated_user_id, 0) as last_updated_user_id, bc.status_id, " +
                " u.username, u.user_id, u2.username as updatedUser "+
                " FROM users u, blog_comments bc LEFT OUTER JOIN users u2 ON bc.last_updated_user_id = u2.user_id " +
                " WHERE bc.blog_id = @blogId " +
                " AND bc.user_id = u.user_id" +
                " AND bc.status_id = " + (int)BlogStatus.Active;

            string orderBy = "bc.blog_comment_id DESC";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@blogId", blogId));
            PagedDataTable pdt = Db.Slave2.GetPagedDataTable(sqlQuery, orderBy, parameters, pageNumber, pageSize);

            PagedList<BlogComment> list = new PagedList<BlogComment>();
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                list.Add(new BlogComment(Convert.ToInt32(row["blog_comment_id"]), Convert.ToInt32(row["blog_id"]), Convert.ToInt32(row["user_id"]), row["subject"].ToString(),
                row["body_text"].ToString(), row["ip_address"].ToString(), Convert.ToDateTime(row["created_date"]), Convert.ToDateTime(row["last_updated_date"]),
                Convert.ToInt32(row["last_updated_user_id"]), Convert.ToInt32(row["status_id"]), row["username"].ToString(), row["updatedUser"].ToString() 
                )
                );
            }
            return list;
        }


        /// <summary>
        /// Get a list of blog comments from a certain date
        /// </summary>
        public PagedList<BlogComment> GetBlogCommentsByCommunity(int communityId, DateTime sinceDate, int pageNumber, int pageSize)
        {
            string sqlQuery = "SELECT th.blog_comment_id, t.subject, th.body_text, th.ip_address, th.created_date, th.last_updated_date, COALESCE(th.last_updated_user_id, 0) as last_updated_user_id, th.status_id, " +
                " u.username, u.user_id, u2.username as updatedUser, t.blog_id " +
                " FROM users u, blogs t, blog_comments th LEFT OUTER JOIN users u2 ON th.last_updated_user_id = u2.user_id " +
                " WHERE th.blog_id = t.blog_id " +
                " AND t.community_id = @communityId " +
                " AND th.created_date > @sinceDate " +
                " AND th.user_id = u.user_id" +
                " AND th.status_id = " + (int)BlogStatus.Active;

            string orderBy = "th.blog_comment_id DESC";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            parameters.Add(new MySqlParameter("@sinceDate", sinceDate));
            PagedDataTable pdt = Db.Slave2.GetPagedDataTable(sqlQuery, orderBy, parameters, pageNumber, pageSize);

            PagedList<BlogComment> list = new PagedList<BlogComment>();
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                list.Add(new BlogComment(Convert.ToInt32(row["blog_comment_id"]), Convert.ToInt32(row["blog_id"]), Convert.ToInt32(row["user_id"]), row["subject"].ToString(),
                row["body_text"].ToString(), row["ip_address"].ToString(), Convert.ToDateTime(row["created_date"]), Convert.ToDateTime(row["last_updated_date"]),
                Convert.ToInt32(row["last_updated_user_id"]), Convert.ToInt32(row["status_id"]), row["username"].ToString(), row["updatedUser"].ToString()
                ));
            }
            return list;
        }

        /// <summary>
        /// DeleteBlogComment
        /// </summary>
        /// <param name="blogId"></param>
        /// <param name="userId"></param>
        public void DeleteBlogComment(int blogCommentId, int userId)
        {
            string sqlString = "DELETE FROM blog_comments " +
                " WHERE blog_comment_id = @blogCommentId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@blogCommentId", blogCommentId));
            Db.Master.ExecuteNonQuery(sqlString, parameters);
        }
    }
}
