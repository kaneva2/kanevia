///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLTransactionDao : ITransactionDao
    {
        public PagedList<FameTransaction> GetFameTransactionsByUser (int userId, string gender, string filter, string orderby, int pageNumber, int pageSize)
        {
            string sql = "SELECT user_id, level_date, rewards_awarded, lh.fame_type_id, level_number, lh.level_id, " +
                " rewards, ft.name as fame_type_name " +
                " FROM level_history lh " +
                " INNER JOIN levels l ON l.level_id = lh.level_id " +
                " INNER JOIN fame_type ft ON ft.fame_type_id = lh.fame_type_id " +
                " WHERE user_id = @userId";

            // Filter it?
            if (filter.Length > 0)
            {
                sql += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            PagedDataTable pdt = Db.Fame.GetPagedDataTable (sql, orderby, parameters, pageNumber, pageSize);

            PagedList<FameTransaction> list = new PagedList<FameTransaction> ();
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                int id = int.Parse(row["user_id"].ToString());

                FameTransaction ft = new FameTransaction (id, int.Parse (row["fame_type_id"].ToString ()), 
                    row["fame_type_name"].ToString (), Convert.ToDateTime (row["level_date"]), 
                    int.Parse (row["level_number"].ToString ()), int.Parse (row["rewards_awarded"].ToString ()));

                sql = "SELECT t1.level_award_id, t1.level_id, t1.name, t1.global_id, t1.animation_id, " +
                    " t1.gender, t1.quantity, t2.name as new_title_name, t3.fame_type_id " +
                    " FROM fame.level_awards AS t1 LEFT JOIN wok.titles AS t2 " +
                    " ON t1.new_title_id = t2.title_id LEFT JOIN fame.levels AS t3" +
                    " ON t1.level_id = t3.level_id" +
                    " WHERE t1.level_id = @levelId AND t1.gender = @gender ";
                parameters.Clear ();

                parameters.Add (new MySqlParameter ("@levelId", int.Parse (row["level_id"].ToString ())));
                parameters.Add (new MySqlParameter ("@gender", gender));
                
                DataTable dt = Db.Fame.GetDataTable (sql, parameters);

                if (dt.Rows.Count > 0)
                {
                    ft.LevelAwards = new List<LevelAwards> ();
                    foreach (DataRow dr in dt.Rows)
                    {
                        ft.LevelAwards.Add(new LevelAwards(int.Parse(dr["level_award_id"].ToString()), int.Parse(dr["level_id"].ToString()), int.Parse(dr["fame_type_id"].ToString()),
                            dr["name"].ToString (), int.Parse (dr["global_id"].ToString ()), int.Parse (dr["animation_id"].ToString ()),
                            dr["gender"].ToString (), int.Parse (dr["quantity"].ToString ()), dr["new_title_name"].ToString ()));
                    }
                }

                list.Add (ft);
            }

            return list;
        }

        public PagedList<RewardTransaction> GetRewardTransactionsByUser (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            string sql = "SELECT user_id, date_created, rl.transaction_type as transaction_type_id , rl.reward_event_id, " +
                "wok_transaction_log_id, reward_log_id, amount, tt.trans_desc, re.event_name " +
                "FROM kaneva.reward_log rl " +
                "INNER JOIN kaneva.reward_events re ON re.reward_event_id = rl.reward_event_id " +
                "INNER JOIN kaneva.transaction_type tt ON tt.transaction_type = rl.transaction_type " +
                "WHERE user_id = @userId " +
                "AND rl.kei_point_id = 'GPOINT' " +
                "AND rl.return_code = 0 ";
                
            // Filter it?
            if (filter.Length > 0)
            {
                sql += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            PagedDataTable dt = Db.Fame.GetPagedDataTable (sql, orderby, parameters, pageNumber, pageSize);

            PagedList<RewardTransaction> list = new PagedList<RewardTransaction> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new RewardTransaction (userId, row["trans_desc"].ToString (),
                    int.Parse (row["amount"].ToString ()), Convert.ToDateTime (row["date_created"]),
                    Int64.Parse (row["transaction_type_id"].ToString ()), int.Parse (row["reward_event_id"].ToString ()),
                    row["event_name"].ToString (), int.Parse (row["wok_transaction_log_id"].ToString ()))
                );
            }
            return list;
        }

        public PagedList<PurchaseTransaction> GetPurchasesTransactionsByUser (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            string sql = "SELECT i.global_id, i.user_id, purchase_date, i.purchase_price as item_price, i.kei_point_id, " +
                " wi.name, wi.description, wi.name as display_name, 1 as quantity, i.purchase_price as transaction_amount, 3 as transaction_type, txn_type " +
                " FROM shopping.item_purchases i " +
                " INNER JOIN wok.items wi ON wi.global_id = i.global_id " +
                " WHERE parent_purchase_id = 0 "+
                " AND user_id = @userId ";
                
            // Filter it?
            if (filter.Length > 0)
            {
                sql += " AND " + filter;
            }

                sql += " UNION " +

                " SELECT i.global_id, user_id, created_date as purchase_date, market_cost as item_price, kei_point_id, " +
                " i.name, description, wd.name as display_name, quantity, transaction_amount, transaction_type, 0 AS txn_type " +
                " FROM kaneva.wok_transaction_log w " +
                " INNER JOIN kaneva.wok_transaction_log_details wd ON wd.trans_id = w.trans_id " +
                " INNER JOIN wok.items i ON i.global_id = wd.global_id " +
                " WHERE transaction_type IN (3,4) AND user_id = @userId";

            // Filter it?
            if (filter.Length > 0)
            {
                sql += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            PagedDataTable dt = Db.Search.GetPagedDataTable (sql, orderby, parameters, pageNumber, pageSize);

            PagedList<PurchaseTransaction> list = new PagedList<PurchaseTransaction> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                PurchaseTransaction pt = new PurchaseTransaction(int.Parse(row["global_id"].ToString()),
                    row["name"].ToString(), row["description"].ToString(), row["display_name"].ToString(),
                    uint.Parse(row["item_price"].ToString()), uint.Parse(row["quantity"].ToString()),
                    Convert.ToDateTime(row["purchase_date"]), row["kei_point_id"].ToString(), int.Parse(row["transaction_type"].ToString()));

                pt.ItemPurchaseTransactionType = int.Parse(row["txn_type"].ToString());
                list.Add(pt);
            }                                                     
            return list;
        }

        public PagedList<PurchaseTransaction> GetPurchasesRaveTransactionsByUser (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            string sql = " SELECT COALESCE(channel_id, 0) as global_id, user_id, created_date as purchase_date, ABS(transaction_amount) as item_price, kei_point_id, " +
                " 'Rave' as name, 'Rave' as description, 'Rave' as display_name, 1 as quantity, transaction_amount, transaction_type " +
                " FROM kaneva.wok_transaction_log wtl" +
                " LEFT JOIN kaneva.mega_rave_history mrh ON wtl.user_id=mrh.user_id_raver " +
                " AND mrh.create_datetime = wtl.created_date " +
                " WHERE transaction_type IN (28,29) AND user_id = @userId";

            // Filter it?
            if (filter.Length > 0)
            {
                sql += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            PagedDataTable dt = Db.Search.GetPagedDataTable (sql, orderby, parameters, pageNumber, pageSize);

            PagedList<PurchaseTransaction> list = new PagedList<PurchaseTransaction> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new PurchaseTransaction (int.Parse (row["global_id"].ToString ()), 
                    row["name"].ToString (), row["description"].ToString (), row["display_name"].ToString (), 
                    uint.Parse (row["item_price"].ToString ()), uint.Parse (row["quantity"].ToString ()), 
                    Convert.ToDateTime (row["purchase_date"]), row["kei_point_id"].ToString (), int.Parse(row["transaction_type"].ToString ())));
            }                                                     
            return list;
        }
        
        public PagedList<TradeTransaction> GetTradeTransactionsByUser (int userId, string orderby, int pageNumber, int pageSize)
        {
            string sql = "SELECT w.trans_id, w2.trans_id, w.created_date, w2.created_date, w.user_id as from_id, " +
                "w2.user_id as to_id, w.transaction_amount, w.kei_point_id, w.transaction_type, " +
                "'' as item_name, 0 as global_id, 1 as quantity " +
                "FROM kaneva.wok_transaction_log w, kaneva.wok_transaction_log w2 " +
                "WHERE w.transaction_type = 2 AND w2.transaction_type = 2 AND w.user_id = @userId AND w.transaction_amount <> 0 " +
                "AND w2.transaction_amount = (-1)*w.transaction_amount AND w.created_date = w2.created_date "; // +
            
            // ** Add this union once code is added to Wok to track giver_id in inventories_transaction_log table

            /*  
                "UNION " +
                "SELECT trans_id, trans_id, itl.created_date, itl.created_date, giver_id as from_id, " + 
			    "itl.player_id as to_id, 0 as transaction_amount, '' as kei_point_id, 2 as transaction_type, " +
			    "i.name as item_name, itl.global_id as item_id, ABS(quantity_delta) as quantity " +
	            "FROM wok.inventories_transaction_log itl " +
                "INNER JOIN wok.items i ON i.global_id = itl.global_id " +
                "INNER JOIN wok.players p ON p.player_id = itl.player_id " +
                "WHERE p.user_id = @userId " +
                "OR itl.giver_id = p.player_id;
            */

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            PagedDataTable dt = Db.Search.GetPagedDataTable (sql, orderby, parameters, pageNumber, pageSize);

            PagedList<TradeTransaction> list = new PagedList<TradeTransaction> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new TradeTransaction (Convert.ToDateTime (row["created_date"]),
                    int.Parse (row["global_id"].ToString ()), row["item_name"].ToString (), int.Parse (row["quantity"].ToString ()),
                    int.Parse (row["to_id"].ToString ()), int.Parse (row["from_id"].ToString ()),
                    int.Parse (row["transaction_amount"].ToString ()), row["kei_point_id"].ToString () ));
            }
            return list;
        }

        public PagedList<GiftTransaction> GetGiftTransactionsByUser (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {                                                                                               
            string sql = "SELECT from_id, to_id, message_date, mg.gift_id, g.name, 1 as quantity " +
                "FROM kaneva.messages m " +
                "INNER JOIN kaneva.message_gifts mg ON mg.message_id = m.message_id " +
                //"INNER JOIN wok.items i ON i.global_id = mg.gift_id " +
                "INNER JOIN wok.gift_catalog_3d_items g ON g.gift_id = mg.gift_id " +
                "WHERE type = 4 AND (from_id = @userId or to_id = @userId) ";

            // Filter it?
            if (filter.Length > 0)
            {
                sql += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            PagedDataTable dt = Db.Search.GetPagedDataTable (sql, orderby, parameters, pageNumber, pageSize);

            PagedList<GiftTransaction> list = new PagedList<GiftTransaction> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new GiftTransaction (Convert.ToDateTime (row["message_date"]),
                    int.Parse (row["gift_id"].ToString ()), row["name"].ToString (), int.Parse (row["quantity"].ToString ()),
                    int.Parse (row["to_id"].ToString ()), int.Parse (row["from_id"].ToString ())));
            }
            return list;
        }

        public PagedList<PurchaseTransactionPremiumItem> GetPurchaseTransactionsByGame (int communityId, string filter, string orderby, int pageNumber, int pageSize)
        {
            string sql = "SELECT ip.purchase_id, i.name, i.description, ip.purchase_date, u.username, " +
                "ip.global_id, iw.display_name, ip.purchase_price, ip.quantity, " +
                "ip.purchase_price, ip.item_owner_id, u.user_id, ip.kei_point_id, ip.txn_type, picr.redemption_date " +
                "FROM item_purchases ip " +
                "INNER JOIN items_web iw ON iw.global_id = ip.global_id " +
                "INNER JOIN kaneva.users u ON u.user_id = ip.user_id " +
                "INNER JOIN wok.items i ON i.global_id = iw.global_id " +
                "INNER JOIN kaneva.premium_item_credit_redemption picr ON picr.purchase_id = ip.purchase_id  " +
                "WHERE i.use_type = @useType " +
                "AND picr.community_id = @communityId ";
                
                //"ORDER BY ip.purchase_id DESC";

            // Filter it?
            if (filter.Length > 0)
            {
                sql += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            parameters.Add (new MySqlParameter ("@useType", WOKItem.USE_TYPE_PREMIUM));

            PagedDataTable dt = Db.Shopping.GetPagedDataTable (sql, orderby, parameters, pageNumber, pageSize);

            PagedList<PurchaseTransactionPremiumItem> list = new PagedList<PurchaseTransactionPremiumItem> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new PurchaseTransactionPremiumItem (int.Parse (row["global_id"].ToString ()),
                    row["name"].ToString (), row["description"].ToString (), row["display_name"].ToString (),
                    Convert.ToUInt32 (row["purchase_price"]), uint.Parse (row["quantity"].ToString ()),
                    Convert.ToDateTime (row["purchase_date"]), row["kei_point_id"].ToString (),
                    int.Parse (row["txn_type"].ToString ()), Convert.ToInt32 (row["purchase_id"]),
                    Convert.ToInt32 (row["user_id"]), row["username"].ToString (), row["redemption_date"] == DBNull.Value ? DateTime.MaxValue : Convert.ToDateTime (row["redemption_date"])));
            }
            return list;
        }

        public int GetTotalTransactionAmountByGame (int gameId)
        {
            string sql ="SELECT SUM(purchase_price) AS TotalAmount " +
                "FROM item_purchases ip " +
                "INNER JOIN items_web iw ON iw.global_id = ip.global_id " +
                "INNER JOIN wok.items i ON i.global_id = iw.global_id " +
                "INNER JOIN wok.game_items gi ON gi.global_id = ip.global_id " +
                "WHERE i.use_type = @useType " +
                "AND gi.game_id = @gameId ";
            
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@gameId", gameId));
            parameters.Add (new MySqlParameter ("@useType", WOKItem.USE_TYPE_PREMIUM));

            return Convert.ToInt32 (Db.Shopping.GetScalar (sql, parameters));
        }

        public PremiumItemRedeemAmounts GetTotalTransactionCreditAmountsByGame (int communityId, int daysPendingInterval)
        {
            try
            {
                string strQuery = "SELECT picr.community_id, " +
                "SUM(CASE WHEN ip.purchase_date < DATE_SUB(CURDATE(), INTERVAL @daysPendingInterval DAY) THEN purchase_price END) AS available, " +
                "SUM(CASE WHEN ip.purchase_date >= DATE_SUB(CURDATE(), INTERVAL @daysPendingInterval DAY) THEN purchase_price END) AS pending " +
                "FROM item_purchases ip " +
                "INNER JOIN items_web iw ON iw.global_id = ip.global_id " +
                "INNER JOIN wok.items i ON i.global_id = iw.global_id " +
                "INNER JOIN kaneva.premium_item_credit_redemption picr ON picr.purchase_id = ip.purchase_id " +
                "WHERE i.use_type = @useType " +
                "AND picr.community_id = @communityId " +
                "AND picr.redemption_date IS NULL";

                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
                parameters.Add (new MySqlParameter ("@communityId", communityId));
                parameters.Add (new MySqlParameter ("@useType", WOKItem.USE_TYPE_PREMIUM));
                parameters.Add (new MySqlParameter ("@daysPendingInterval", daysPendingInterval));

                DataRow row = Db.Shopping.GetDataRow (strQuery, parameters);

                if (row == null)
                {
                    return new PremiumItemRedeemAmounts ();
                }

                return new PremiumItemRedeemAmounts (Convert.ToInt32 (row["community_id"]), row["pending"] == DBNull.Value ? 0 : Convert.ToInt32 (row["pending"]),
                    row["available"] == DBNull.Value ? 0 : Convert.ToInt32 (row["available"]));
            }
            catch
            {
                return new PremiumItemRedeemAmounts ();
            }
        }

        public PremiumItemRedeemAmounts GetTotalTransactionCreditAmountsByUser(int userId, int daysPendingInterval)
        {
            try
            {
                string strQuery = "SELECT 0 AS community_id, " +
                "SUM(CASE WHEN ip.purchase_date < DATE_SUB(CURDATE(), INTERVAL @daysPendingInterval DAY) THEN purchase_price END) AS available, " +
                "SUM(CASE WHEN ip.purchase_date >= DATE_SUB(CURDATE(), INTERVAL @daysPendingInterval DAY) THEN purchase_price END) AS pending " +
                "FROM item_purchases ip " +
                "INNER JOIN items_web iw ON iw.global_id = ip.global_id " +
                "INNER JOIN wok.items i ON i.global_id = iw.global_id " +
                "INNER JOIN kaneva.premium_item_credit_redemption picr ON picr.purchase_id = ip.purchase_id " +
                "INNER JOIN kaneva.communities c ON c.community_id = picr.community_id AND c.creator_id = @userId " +
                "WHERE i.use_type = @useType " +
                "AND picr.redemption_date IS NULL";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@userId", userId));
                parameters.Add(new MySqlParameter("@useType", WOKItem.USE_TYPE_PREMIUM));
                parameters.Add(new MySqlParameter("@daysPendingInterval", daysPendingInterval));

                DataRow row = Db.Shopping.GetDataRow(strQuery, parameters);

                if (row == null)
                {
                    return new PremiumItemRedeemAmounts();
                }

                return new PremiumItemRedeemAmounts(Convert.ToInt32(row["community_id"]), row["pending"] == DBNull.Value ? 0 : Convert.ToInt32(row["pending"]),
                    row["available"] == DBNull.Value ? 0 : Convert.ToInt32(row["available"]));
            }
            catch
            {
                return new PremiumItemRedeemAmounts();
            }
        }

        public PagedList<RaveTransaction> GetAssetRaveTransactionsByUser (int userId, bool isUserTheRaveGiver, string filter, string orderby, int pageNumber, int pageSize)
        {
            string sql = "SELECT ad.user_id, u.username, ad.asset_id, ad.created_date as create_date, a.owner_id, " +
                "a.owner_username, a.name, a.asset_type_id " +
                "FROM asset_diggs ad " +
                "INNER JOIN assets a ON a.asset_id = ad.asset_id " +
                "INNER JOIN users u ON u.user_id = ad.user_id ";

            if (isUserTheRaveGiver)
            {
                sql += "WHERE ad.user_id = @userId ";
            }
            else
            {
                sql += "WHERE a.owner_id = @userId ";
            }

            // Filter it?
            if (filter.Length > 0)
            {
                sql += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            PagedDataTable dt = Db.Slave.GetPagedDataTable (sql, orderby, parameters, pageNumber, pageSize);

            PagedList<RaveTransaction> list = new PagedList<RaveTransaction> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new RaveTransaction (int.Parse (row["user_id"].ToString ()), row["username"].ToString (), int.Parse (row["asset_id"].ToString ()),
                    row["name"].ToString (), int.Parse (row["owner_id"].ToString ()), row["owner_username"].ToString (),
                    int.Parse (row["asset_type_id"].ToString ()), false, Convert.ToDateTime (row["create_date"]))); 
            }
            return list;
        }

        public PagedList<RaveTransaction> GetCommunityRaveTransactionsByUser(int userId, bool isUserTheRaveGiver, string filter1, string filter2, string orderby, int pageNumber, int pageSize)
        {
            string sql = "SELECT cd.user_id as raverId, u.username as raverName, " +
                "cd.channel_id as itemId, c.name as itemName, " +
                "c.creator_id as itemOwnerId, c.creator_username as itemOwnerName, " +
                "cd.created_date as create_date, c.is_personal, 0 as is_mega_rave " +
                "FROM channel_diggs cd " +
                "INNER JOIN communities c ON c.community_id = cd.channel_id " +
                "INNER JOIN users u ON u.user_id = cd.user_id ";

            string sql1 = "";
            string sql2 = "";
            
            if (isUserTheRaveGiver)
            {
             //   sql += "WHERE cd.user_id = @userId ";

                sql1 = " WHERE cd.user_id = @userId ";
                sql2 = " WHERE mrh.user_id_raver = @userId ";
            }
            else
            {
             //   sql += "WHERE c.creator_id = @userId ";

                sql1 = " WHERE c.creator_id = @userId ";
                sql2 = " WHERE c.creator_id = @userId ";
            }

            // Filter it?
            if (filter1.Length > 0)
            {
                sql1 += " AND " + filter1;
            }

            // Filter it?
            if (filter2.Length > 0)
            {
                sql2 += " AND " + filter2;
            }

            sql += sql1 + " UNION " +
                " SELECT mrh.user_id_raver as raverId, u.username as raverName, " +
                " c.community_id as itemId, c.name as itemName, " +
                " c.creator_id as itemOwnerId, c.creator_username as itemOwnerName, " +
                " mrh.create_datetime as create_date, c.is_personal, 1 as is_mega_rave " +
                " FROM kaneva.mega_rave_history mrh " +
                " INNER JOIN communities c ON c.community_id = mrh.channel_id " +
                " INNER JOIN users u ON u.user_id = mrh.user_id_raver " +
                sql2;

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            PagedDataTable dt = Db.Slave.GetPagedDataTable (sql, orderby, parameters, pageNumber, pageSize);

            PagedList<RaveTransaction> list = new PagedList<RaveTransaction> ();
            list.TotalCount = dt.TotalCount;

            RaveTransaction.eItemType eItemType;

            foreach (DataRow row in dt.Rows)
            {
                eItemType = (Convert.ToBoolean (row["is_personal"])) ? RaveTransaction.eItemType.WEB_PROFILE : RaveTransaction.eItemType.WEB_COMMUNITY;

                list.Add (new RaveTransaction (int.Parse (row["raverId"].ToString ()), row["raverName"].ToString (),
                    int.Parse (row["itemId"].ToString ()), row["itemName"].ToString (),
                    int.Parse (row["itemOwnerId"].ToString ()), row["itemOwnerName"].ToString (),
                    eItemType, Convert.ToBoolean (row["is_mega_rave"]), Convert.ToDateTime (row["create_date"])));
            }
            return list;
        }

        public PagedDataTable GetSalesOrderTransactions(DateTime dtStartDate, DateTime dtEndDate, string filter, string orderby, int pageNumber, int pageSize)
        {
            string select = "SELECT SUM(ppta.amount) as kpoints, pm.name, pm.description, ppt.point_transaction_id, " +
	        "ppt.user_id, ppt.description as ppt_description, ppt.order_id, billing_information_id, " +
	        "ppt.payment_method_id, ppt.amount_debited, ppt.amount_credited, ppt.transaction_status, ppt.transaction_date, " +
            "ppt.error_description, ppts.transaction_status_id, ppts.description as status_description, com.name_no_spaces, " +
            "com.name as com_name, com.thumbnail_small_path FROM purchase_point_transactions ppt  INNER JOIN communities_personal com " +
            "ON ppt.user_id = com.creator_id, purchase_point_transaction_amounts ppta, payment_methods pm, " +
            "purchase_point_transactions_status ppts " ;

            string where = "WHERE ppt.point_transaction_id = ppta.point_transaction_id " +
            " AND pm.payment_method_id = ppt.payment_method_id AND ppt.transaction_status = ppts.transaction_status_id " +
            " AND ppt.transaction_status = 3 AND ppt.amount_debited > 0 " +
            " AND ppt.transaction_date >= @startDate " +
            " AND ppt.transaction_date <= @endDate ";

            string groupBy = "GROUP BY ppt.point_transaction_id";

            // Filter it?
            if (filter.Length > 0)
            {
                where += " AND " + filter;
            }

            string sql = select + where + groupBy;
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@startDate", (dtStartDate)));
            parameters.Add(new MySqlParameter("@endDate", (dtEndDate)));
            return Db.Master.GetPagedDataTable(sql, orderby, parameters, pageNumber, pageSize);
        }

        public PagedList<RaveTransaction> Get3DHomeRaveTransactionsByUser (int userId, bool isUserTheRaveGiver, string filter, string orderby, int pageNumber, int pageSize)
        {
            string sql = "";
                
            if (isUserTheRaveGiver)
            {
                sql = "SELECT zr.kaneva_user_id as raverId, u.username as raverName, " +
                    "zr.zone_instance_id as itemId, cz.name as itemName, " +
                    "cz.kaneva_user_id as itemOwnerId, u2.username as itemOwnerName, " +
                    "zr.zone_type, zr.created_date as create_date " +
                    "FROM wok.zone_raves zr " +
                    "INNER JOIN wok.channel_zones cz ON cz.zone_instance_id = zr.zone_instance_id " +
                    "INNER JOIN kaneva.users u2 ON u2.user_id = cz.kaneva_user_id " +
                    "INNER JOIN kaneva.users u ON u.user_id = zr.kaneva_user_id " +
                    "WHERE zr.kaneva_user_id = @userId ";
            }
            else
            {
                sql = "SELECT zr.kaneva_user_id as raverId, u2.username as raverName, " +
                    "zr.zone_instance_id as itemId, cz.name as itemName, " +
                    "cz.kaneva_user_id as itemOwnerId, u.username as itemOwnerName, " +
                    "zr.zone_type, zr.created_date as create_date " +
                    "FROM wok.zone_raves zr " +
                    "INNER JOIN wok.channel_zones cz ON cz.zone_instance_id = zr.zone_instance_id " +
                    "INNER JOIN kaneva.users u ON u.user_id = cz.kaneva_user_id " +
                    "INNER JOIN kaneva.users u2 ON u2.user_id = zr.kaneva_user_id " +
                    "WHERE cz.kaneva_user_id = @userId ";
            }

            // NOTE:  This code will be used when we add the ability to user 
            //        a Mega Rave on a 3d home or 3d hangout

            //sql += sql1 + "UNION " +
            //    "SELECT mrh.user_id_raver as raverId, u.username as raverName, " +
            //    "cz.zone_instance_id as itemId, cz.name as itemName, " +
            //    "cz.kaneva_user_id as itemOwnerId, u2.username as itemOwnerName, " +
            //    "cz.zone_type, mrh.create_datetime as create_date " +
            //    "FROM kaneva.mega_rave_history mrh " +
            //    "INNER JOIN wok.channel_zones cz ON cz.channel_zone_id = mrh.channel_zone_id " +
            //    sql2;

            // Filter it?
            if (filter.Length > 0)
            {
                sql += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            PagedDataTable dt = Db.Slave.GetPagedDataTable (sql, orderby, parameters, pageNumber, pageSize);

            PagedList<RaveTransaction> list = new PagedList<RaveTransaction> ();
            list.TotalCount = dt.TotalCount;
            
            RaveTransaction.eItemType eItemType;

            foreach (DataRow row in dt.Rows)
            {
                switch (int.Parse (row["zone_type"].ToString ()))
                {
                    case 3: // Apartment
                        eItemType = RaveTransaction.eItemType.WOK_HOME;
                        break;
                    case 4: // Perm Space
                        eItemType = RaveTransaction.eItemType.WOK_HANGOUT;
                        break;
                    case 6: // Hangout
                        eItemType = RaveTransaction.eItemType.WOK_HANGOUT;
                        break;
                    default:
                        eItemType = RaveTransaction.eItemType.WOK_HOME;
                        break;
                }

                list.Add (new RaveTransaction (int.Parse (row["raverId"].ToString ()), row["raverName"].ToString (), int.Parse (row["itemId"].ToString ()),
                    row["itemName"].ToString (), int.Parse (row["itemOwnerId"].ToString ()), row["itemOwnerName"].ToString (),
                    eItemType, false, Convert.ToDateTime (row["create_date"])));
            }
            return list;
        }

        public PagedList<RaveTransaction> GetUGCRaveTransactionsByUser (int userId, bool isUserTheRaveGiver, string filter, string orderby, int pageNumber, int pageSize)
        {
            string sql = "SELECT id.user_id AS raverId, raver.username AS raverName, " +
                "id.global_id AS itemId, i.name AS itemName, " +
                "i.item_creator_id AS itemOwnerId, creator.username AS itemOwnerName, id.created_date AS create_date " +
                "FROM shopping.item_diggs id " +
                "INNER JOIN wok.items i ON i.global_id = id.global_id " +
                "INNER JOIN kaneva.users raver ON raver.user_id = id.user_id ";

            if (isUserTheRaveGiver)
            {
                sql += "LEFT JOIN kaneva.users creator ON creator.user_id = i.item_creator_id " +
                    "WHERE id.user_id = @userId ";
            }
            else
            {
                sql += "INNER JOIN kaneva.users creator ON creator.user_id = i.item_creator_id " +
                    "WHERE i.item_creator_id = @userId ";
            }

            // Filter it?
            if (filter.Length > 0)
            {
                sql += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            PagedDataTable dt = Db.Slave.GetPagedDataTable (sql, orderby, parameters, pageNumber, pageSize);

            PagedList<RaveTransaction> list = new PagedList<RaveTransaction> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new RaveTransaction (int.Parse (row["raverId"].ToString ()), row["raverName"].ToString (),
                    int.Parse (row["itemId"].ToString ()), row["itemName"].ToString (),
                    int.Parse (row["itemOwnerId"].ToString ()), row["itemOwnerName"].ToString (),
                    RaveTransaction.eItemType.UGC, false, Convert.ToDateTime (row["create_date"]))); 
            }
            return list;
        }

        public PagedList<RaveTransaction> GetCommunityRaveTransactions (int communityId, string filter1, string filter2, string orderby, int pageNumber, int pageSize)
        {
            string sql = "SELECT 0 as raverId, '' as raverName, " +
                " cd.channel_id as itemId, c.name as itemName, " +
                " c.creator_id as itemOwnerId, c.creator_username as itemOwnerName, " +
                " cd.created_date as create_date, c.is_personal, 0 as is_mega_rave " +
                " FROM channel_diggs cd " +
                " INNER JOIN communities c ON c.community_id = cd.channel_id " +
             //   " INNER JOIN users u ON u.user_id = cd.user_id " +
                " WHERE c.community_id = @communityId ";

            string sql1 = "";
            string sql2 = "";

    //        if (isUserTheRaveGiver)
    //        {
                //   sql += "WHERE cd.user_id = @userId ";

    //            sql1 = " WHERE cd.user_id = @userId ";
    //            sql2 = " WHERE mrh.user_id_raver = @userId ";
    //        }
    //        else
    //        {
                //   sql += "WHERE c.creator_id = @userId ";

    //            sql1 = " WHERE c.creator_id = @userId ";
    //            sql2 = " WHERE c.creator_id = @userId ";
    //        }

            // Filter it?
            if (filter1.Length > 0)
            {
                sql1 += " AND " + filter1;
            }

            // Filter it?
            if (filter2.Length > 0)
            {
                sql2 += " AND " + filter2;
            }

            sql += sql1 + " UNION " +
                " SELECT 0 as raverId, '' as raverName, " +
                " c.community_id as itemId, c.name as itemName, " +
                " c.creator_id as itemOwnerId, c.creator_username as itemOwnerName, " +
                " mrh.create_datetime as create_date, c.is_personal, 1 as is_mega_rave " +
                " FROM kaneva.mega_rave_history mrh " +
                " INNER JOIN communities c ON c.community_id = mrh.channel_id " +
            //    " INNER JOIN users u ON u.user_id = mrh.user_id_raver " +
                " WHERE c.community_id = @communityId " +
                sql2;

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));

            PagedDataTable dt = Db.Slave.GetPagedDataTable (sql, orderby, parameters, pageNumber, pageSize);

            PagedList<RaveTransaction> list = new PagedList<RaveTransaction> ();
            list.TotalCount = dt.TotalCount;

            RaveTransaction.eItemType eItemType;

            foreach (DataRow row in dt.Rows)
            {
                eItemType = (Convert.ToBoolean (row["is_personal"])) ? RaveTransaction.eItemType.WEB_PROFILE : RaveTransaction.eItemType.WEB_COMMUNITY;

                list.Add (new RaveTransaction (int.Parse (row["raverId"].ToString ()), row["raverName"].ToString (),
                    int.Parse (row["itemId"].ToString ()), row["itemName"].ToString (),
                    int.Parse (row["itemOwnerId"].ToString ()), row["itemOwnerName"].ToString (),
                    eItemType, Convert.ToBoolean (row["is_mega_rave"]), Convert.ToDateTime (row["create_date"])));
            }
            return list;
        }

        /// <summary>
        /// return a list of reward events for a specified transaction type
        /// </summary>
        /// <param name="transactionTypeId"></param>
        /// <returns></returns>
        public DataTable GetKanevaEventsByTransactionType(int transactionTypeId)
        {
            string sql = "SELECT reward_event_id, event_name, transaction_type_id " +
                " FROM reward_events " +
                " WHERE transaction_type_id = @transactionTypeId " +
                " ORDER BY event_name ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            return Db.Master.GetDataTable(sql.ToString(), parameters);
        }

        /// <summary>
        /// return a list of reward events
        /// </summary>
        /// <returns></returns>
        public DataTable GetKanevaEvents()
        {
            string sql = "SELECT reward_event_id, event_name, transaction_type_id, trans_desc " +
                " FROM reward_events re " +
                " INNER JOIN transaction_type tt ON tt.transaction_type = re.transaction_type_id " +
                " ORDER BY event_name ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            return Db.Master.GetDataTable(sql.ToString(), parameters);
        }

        /// <summary>
        /// inserts a new reward event
        /// </summary>
        /// <param name="eventName"></param>
        /// <param name="transactionTypeId"></param>
        /// <returns></returns>
        public int AddKanevaRewardEvent(string eventName, int transactionTypeId)
        {
            string sqlString = " INSERT INTO reward_events (event_name, transaction_type_id) " +
                               " VALUES (@eventName, @transactionTypeId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@eventName", eventName));
            parameters.Add(new MySqlParameter("@transactionTypeId", transactionTypeId));

            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// updates a reward event
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="eventName"></param>
        /// <param name="transactionTypeId"></param>
        /// <returns></returns>
        public int UpdateKanevaRewardEvent(int eventId, string eventName, int transactionTypeId)
        {
            string sqlString = "UPDATE reward_events " +
                " SET event_name = @eventName, " +
                " transaction_type_id = @transactionTypeId " +
                " WHERE reward_event_id = @eventId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@eventId", eventId));
            parameters.Add(new MySqlParameter("@eventName", eventName));
            parameters.Add(new MySqlParameter("@transactionTypeId", transactionTypeId));

            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// Gets listo of Transaction Types
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public DataTable GetTransactionTypes(string filter, string orderBy)
        {
            string sqlString = "SELECT transaction_type, trans_desc, trans_text " +
                " FROM transaction_type ";

            if (filter.Length > 0)
            {
                sqlString += " WHERE " + filter;
            }

            if (orderBy.Length > 0)
            {
                sqlString += " ORDER BY " + orderBy;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            return Db.Master.GetDataTable(sqlString, parameters);
        }

        /// <summary>
        /// Call stored proc to reward credits to user account
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="keiPointId"></param>
        /// <param name="rewardEventId"></param>
        /// <param name="transTypeId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public int RewardCredits(double amount, string keiPointId, int rewardEventId, int transTypeId, DateTime startDate, DateTime endDate)
        {
            string sqlString = "CALL kaneva.rewardCredits (@amount, @keiPointId, @startDate, @endDate, @rewardEventId, @transTypeId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@amount", amount));
            parameters.Add(new MySqlParameter("@keiPointId", keiPointId));
            parameters.Add(new MySqlParameter("@startDate", startDate));
            parameters.Add(new MySqlParameter("@endDate", endDate));
            parameters.Add(new MySqlParameter("@rewardEventId", rewardEventId));
            parameters.Add(new MySqlParameter("@transTypeId", transTypeId));

            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// Call stored proc to reward credits to multiple users
        /// </summary>
        /// <param name="userIdList"></param>
        /// <param name="userEmailList"></param>
        /// <param name="amount"></param>
        /// <param name="keiPointId"></param>
        /// <param name="rewardEventId"></param>
        /// <param name="transTypeId"></param>
        /// <param name="failedUserIdList"></param>
        /// <returns></returns>
        public int RewardCreditsToMultipleUsers(string userIdList, string userEmailList, double amount, int transTypeId, string keiPointId, int rewardEventId, ref string failedUserIdList)
        {
            string sql = "CALL kaneva.apply_transaction_to_multiple_user_balances2 (@userIdList, @userEmailList, " +
                "@amount, @transTypeId, @keiPointId, @rewardEventId, @success_count, @_failed_user_id_list); select CAST(@success_count as UNSIGNED INT) as success_count, CAST(@failed_user_id_list as UNSIGNED INT) as failed_user_id_list;";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userIdList", userIdList));
            parameters.Add(new MySqlParameter("@userEmailList", userEmailList));
            parameters.Add(new MySqlParameter("@amount", amount));
            parameters.Add(new MySqlParameter("@transTypeId", transTypeId));
            parameters.Add(new MySqlParameter("@keiPointId", keiPointId));
            parameters.Add(new MySqlParameter("@rewardEventId", rewardEventId));

            DataRow dr = Db.Master.GetDataRow(sql, parameters);

            if (dr["failed_user_id_list"] != null)
            {
                failedUserIdList = dr["failed_user_id_list"].ToString();
            }

            return Convert.ToInt32(dr["success_count"]);
        }

        /// <summary>
        /// inserts a new reward event
        /// </summary>
        /// <param name="eventName"></param>
        /// <param name="transactionTypeId"></param>
        /// <returns></returns>
        public int LogSuperRewardsTransaction(SuperRewardsTransaction swTrans)
        {
            string sqlString = " CALL add_new_superrewards_transaction (@srTransactionId, @creditsAwarded, @creditsAllTime, @userID, @offerID, @security_key, @awardStatus, @WokTransactionLogID, @RequestingIPAddress) ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@srTransactionId", swTrans.SuperRewardsTransactionID));
            parameters.Add(new MySqlParameter("@creditsAwarded", swTrans.CreditsAwarded));
            parameters.Add(new MySqlParameter("@creditsAllTime", swTrans.CreditsAllTime));
            parameters.Add(new MySqlParameter("@userID", swTrans.UserID));
            parameters.Add(new MySqlParameter("@offerID", swTrans.SuperRewardsOfferID));
            parameters.Add(new MySqlParameter("@security_key", swTrans.SecurityKey));
            parameters.Add(new MySqlParameter("@awardStatus", swTrans.AwardStatus));
            parameters.Add(new MySqlParameter("@WokTransactionLogID", swTrans.WokTransactionLogID));
            parameters.Add(new MySqlParameter("@RequestingIPAddress", swTrans.RequestingIPAddress));

            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// checks to see if the user has already been rewarded for that offer
        /// </summary>
        /// <param name="eventName"></param>
        /// <param name="transactionTypeId"></param>
        /// <returns></returns>
        public int SuperRewardsCreditAlreadyGiven(uint srTransactionId)
        {
            string sqlString = " SELECT COUNT(transaction_id) AS reward_count FROM superrewards_log WHERE sr_transaction_id = @srTransactionId " +
                " AND award_status = " + (int)SuperRewardsTransaction.eSR_AWARD_STATUS.SUCCEED;

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@srTransactionId", srTransactionId));

            return Convert.ToInt32 (Db.Master.GetScalar(sqlString, parameters));
        }

        /// <summary>
        /// GetConversionRateToDollar
        /// </summary>
        public Currency GetConversionRateToDollar (string currencyType)
        {
            string cacheKey = "currency." + currencyType + ".convratetodollar";
            Currency currency = (Currency) DbCache.Cache[cacheKey];

            if (currency == null)
            {
                // Add to the cache
                string strQuery = "SELECT kei_point_id, conversion_rate_to_dollar, description, " +
                    " currency_symbol, user_purchaseable, COALESCE(display_format, '') as display_format " +
                    " FROM kei_points " +
                    " WHERE kei_point_id = @currencyType ";

                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
                parameters.Add (new MySqlParameter ("@currencyType", currencyType));
                DataRow row = Db.Master.GetDataRow (strQuery, parameters);

                if (row == null)
                {
                    return new Currency ();
                }

                currency = new Currency (currencyType, Convert.ToDecimal (row["conversion_rate_to_dollar"]),
                    row["conversion_rate_to_dollar"].ToString (), row["currency_symbol"].ToString (),
                    row["user_purchaseable"].ToString ().Equals ("Y"), row["display_format"].ToString ());

                DbCache.Cache.Insert (cacheKey, currency, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return currency;
        }
    
        /// <summary>
        /// updates a reward event
        /// </summary>
        /// <param name="communityId"></param>
        /// <returns></returns>
        public int RedeemPremiumItemCredits (int communityId, int daysPendingBeforeRedeem)
        {
            string sqlString = "UPDATE premium_item_credit_redemption picr " +
                " INNER JOIN shopping.item_purchases ip ON ip.purchase_id = picr.purchase_id" +
                " SET picr.redemption_date = NOW() " +
                " WHERE picr.community_id = @communityId " +
                " AND picr.redemption_date IS NULL " +
                " AND ip.purchase_date < DATE_SUB(CURDATE(), INTERVAL " + daysPendingBeforeRedeem + " DAY)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add (new MySqlParameter ("@communityId", communityId));
 
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }
    }
}


