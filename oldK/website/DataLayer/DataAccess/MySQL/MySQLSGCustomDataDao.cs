///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using Devart.Data.MySql;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;
using System.Text;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLSGCustomDataDao : ISGCustomDataDao
    {
        #region Create

        /// <summary>
        /// SnapshotScriptGameItems
        /// </summary>
        /// <returns>True if the game items were successfully added.</returns>
        public bool ModifyDefaultSGCustomDataItem(SGCustomDataItem sgCustomDataItem, bool updateTemplates = false)
        {
            bool result = false;
            string worldTemplateGlids = string.Empty;

            try
            {
                if (updateTemplates)
                {
                    PagedList<WorldTemplate> gamingTemplates = (new MySQLWorldTemplateDao()).GetGamingWorldTemplates(1, Int32.MaxValue, "");
                    worldTemplateGlids = string.Join(", ", gamingTemplates.Select(t => t.GlobalId).ToArray());
                }

                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "INSERT INTO default_script_game_custom_data (attribute, value) " +
                                       "VALUES (@attribute, @value) " +
                                       "ON DUPLICATE KEY UPDATE value = @value ";

                    List<IDbDataParameter>  parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@attribute", sgCustomDataItem.Attribute));
                    parameters.Add(new MySqlParameter("@value", sgCustomDataItem.Value));
                    Db.WOK.ExecuteNonQuery(sqlString, parameters, true);

                    if (updateTemplates && ! string.IsNullOrWhiteSpace(worldTemplateGlids))
                    {
                        sqlString = "INSERT INTO starting_script_game_custom_data (template_glid, attribute, value) " +
                                    "SELECT global_id, @attribute, @value " +
                                    "FROM world_template_global_id " +
                                    "WHERE global_id IN (" + worldTemplateGlids + ") " +
                                    "ON DUPLICATE KEY UPDATE value = @value ";

                        parameters = new List<IDbDataParameter>();
                        parameters.Add(new MySqlParameter("@attribute", sgCustomDataItem.Attribute));
                        parameters.Add(new MySqlParameter("@value", sgCustomDataItem.Value));
                        Db.WOK.ExecuteNonQuery(sqlString, parameters, true);
                    }      

                    // If we reach this point, the transaction succeeded
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLSGCustomDataDao.InsertSGCustomDataItem() - Transaction aborted: " + e.ToString());
            }

            return result;
        }

        /// <summary>
        /// Adds starting_script_game_custom_data to a deed. 
        /// </summary>
        /// <returns>False if an error occurred in processing, True otherwise.</returns>
        public bool AddSGCustomDataToCustomDeed(int zoneInstanceId, int zoneType, int templateGlobalId)
        {
            bool success = false;

            PagedList<SGCustomDataItem> dtCustomData = GetWorldSGCustomData(zoneInstanceId, zoneType, " AND attribute NOT LIKE 'DNC_%' AND attribute <> 'Metrics' ", string.Empty, 1, Int32.MaxValue);

            // Return true if there were no items to process.
            if (dtCustomData == null || dtCustomData.Count == 0)
            {
                return true;
            }

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    StringBuilder queryBuilder = new StringBuilder("INSERT INTO starting_script_game_custom_data (template_glid, attribute, value) VALUES ");

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@templateGlobalId", templateGlobalId));

                    // Add insert for each dynamic object parameter.
                    for (int i = 0; i < dtCustomData.Count; i++)
                    {
                        queryBuilder.Append("(@templateGlobalId, @attribute" + i + ", @value" + i + "), ");

                        SGCustomDataItem dataItem = dtCustomData[i];
                        parameters.Add(new MySqlParameter("@attribute" + i, dataItem.Attribute));
                        parameters.Add(new MySqlParameter("@value" + i, (dataItem.Value == null ? DBNull.Value : (object)dataItem.Value)));
                    }
                    queryBuilder.Remove(queryBuilder.Length - 2, 2);

                    Db.WOK.ExecuteNonQuery(queryBuilder.ToString(), parameters, true);

                    transaction.Complete();
                    success = true;
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySGCustomDataDao.AddSGCustomDataToCustomDeed() - Transaction aborted: " + e.ToString());
            }

            return success;
        }

        #endregion

        #region Read

        /// <summary>
        /// GetDefaultSGCustomData
        /// </summary>
        public PagedList<SGCustomDataItem> GetDefaultSGCustomData(string orderBy, int pageNumber, int pageSize)
        {
            PagedList<SGCustomDataItem> sgCustomData = new PagedList<SGCustomDataItem>();

            string sql = "SELECT sgi.attribute, sgi.value " +
                         "FROM default_script_game_custom_data sgi ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            PagedDataTable pdt = Db.WOK.GetPagedDataTable(sql, orderBy, parameters, pageNumber, pageSize);

            if (pdt == null)
                return sgCustomData;

            foreach (DataRow row in pdt.Rows)
            {
                sgCustomData.Add(new SGCustomDataItem(row["attribute"].ToString(), (row["value"] == DBNull.Value ? null : row["value"].ToString())));
            }

            // Make sure total count makes sense
            sgCustomData.TotalCount = pdt.TotalCount;

            return sgCustomData;
        }

        /// <summary>
        /// GetWorldSGCustomData
        /// </summary>
        public PagedList<SGCustomDataItem> GetWorldSGCustomData(int zoneInstanceId, int zoneType, string filter, string orderBy, int pageNumber, int pageSize)
        {
            PagedList<SGCustomDataItem> sgCustomData = new PagedList<SGCustomDataItem>();

            string sql = "SELECT sgi.attribute, sgi.value " +
                         "FROM script_game_custom_data sgi " +
                         "WHERE zone_instance_id = @zoneInstanceId " +
                         "AND zone_type = @zoneType ";

            if (!string.IsNullOrWhiteSpace(filter))
            {
                sql += filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));

            PagedDataTable pdt = Db.WOK.GetPagedDataTable(sql, orderBy, parameters, pageNumber, pageSize);

            if (pdt == null)
                return sgCustomData;

            foreach (DataRow row in pdt.Rows)
            {
                sgCustomData.Add(new SGCustomDataItem(row["attribute"].ToString(), (row["value"] == DBNull.Value ? null : row["value"].ToString())));
            }

            // Make sure total count makes sense
            sgCustomData.TotalCount = pdt.TotalCount;

            return sgCustomData;
        }

        /// <summary>
        /// GetDefaultSGCustomDataItem
        /// </summary>
        public SGCustomDataItem GetDefaultSGCustomDataItem(string attribute)
        {
            SGCustomDataItem sgCustomDataItem = new SGCustomDataItem();

            string sql = "SELECT sgi.attribute, sgi.value " +
                         "FROM default_script_game_custom_data sgi " +
                         "WHERE sgi.attribute = @attribute ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@attribute", attribute));

            DataRow row = Db.WOK.GetDataRow(sql, parameters);

            if (row != null)
            {
                sgCustomDataItem = new SGCustomDataItem(row["attribute"].ToString(), (row["value"] == DBNull.Value ? null : row["value"].ToString()));
            }

            return sgCustomDataItem;
        }

        #endregion

        #region Update

        /// <summary>
        /// Adds script_game_custom_data to a world. 
        /// </summary>
        /// <returns>False if an error occurred in processing, True otherwise.</returns>
        public bool AddSGCustomDataToWorld(int zoneInstanceId, int zoneType, string attribute, string value)
        {
            bool success = false;

            // Check attribute and value before we bother with the update.
            if (string.IsNullOrWhiteSpace(attribute) || string.IsNullOrWhiteSpace(value))
            {
                m_logger.Error("MySGCustomDataDao.AddSGCustomDataToWorld() - Custom data invalid. attribute=" + (attribute ?? "NULL") + " value=" + (value ?? "NULL"));
                return false;
            }

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sql = "INSERT INTO script_game_custom_data (zone_instance_id, zone_type, attribute, value) " +
                                 "VALUES (@zoneInstanceId, @zoneType, @attribute, @value) " +
                                 "ON DUPLICATE KEY UPDATE value = @value ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
                    parameters.Add(new MySqlParameter("@zoneType", zoneType));
                    parameters.Add(new MySqlParameter("@attribute", attribute));
                    parameters.Add(new MySqlParameter("@value", value));

                    Db.WOK.ExecuteNonQuery(sql, parameters, true);

                    transaction.Complete();
                    success = true;
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySGCustomDataDao.AddSGCustomDataToWorld() - Transaction aborted: " + e.ToString());
            }

            return success;
        }

        #endregion

        #region Delete

        public bool DeleteDefaultSGCustomDataItem(SGCustomDataItem sgCustomDataItem, bool removeFromTemplates = false)
        {
            bool success = false;
            string worldTemplateGlids = string.Empty;

            try
            {
                // Prior to transaction, grab template glids, if necessary
                if (removeFromTemplates)
                {
                    PagedList<WorldTemplate> gamingTemplates = (new MySQLWorldTemplateDao()).GetGamingWorldTemplates(1, Int32.MaxValue, "");
                    worldTemplateGlids = string.Join(", ", gamingTemplates.Select(t => t.GlobalId).ToArray());
                }

                // Do deletes in a transaction
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "DELETE FROM default_script_game_custom_data WHERE attribute = @attribute";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@attribute", sgCustomDataItem.Attribute));
                    Db.WOK.ExecuteNonQuery(sqlString, parameters, true);

                    if (removeFromTemplates && !string.IsNullOrWhiteSpace(worldTemplateGlids))
                    {
                        sqlString = "DELETE FROM starting_script_game_custom_data WHERE attribute = @attribute AND template_glid IN (" + worldTemplateGlids + ")";

                        parameters = new List<IDbDataParameter>();
                        parameters.Add(new MySqlParameter("@attribute", sgCustomDataItem.Attribute));
                        Db.WOK.ExecuteNonQuery(sqlString, parameters, true);
                    }
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLSGCustomDataDao.DeleteSGCustomDataItem() - Transaction aborted: " + e.ToString());
            }
            return success;
        }

        public bool DeleteSGCustomDataFromCustomDeed(int deedTemplateId)
        {
            bool success = false;

            try
            {
                // Do deletes in a transaction
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "DELETE FROM starting_script_game_custom_data WHERE template_glid = @deedTemplateId";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@deedTemplateId", deedTemplateId));

                    Db.WOK.ExecuteNonQuery(sqlString, parameters, true);

                    transaction.Complete();
                    success = true;
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLSGCustomDataDao.DeleteSGCustomDataFromCustomDeed() - Transaction aborted: " + e.ToString());
            }
            return success;
        }

        #endregion

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
    }
}
