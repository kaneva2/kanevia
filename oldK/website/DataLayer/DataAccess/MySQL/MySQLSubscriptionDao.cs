///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;
using System.Collections.Specialized;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLSubscriptionDao : ISubscriptionDao
    {
        #region Subscrpriptions

        /// <summary>
        /// Get the List of point buckets
        /// </summary>
        /// <returns></returns>
        public List<Subscription> GetUnpurchasedSubscriptionsList(int userID)
        {
            return GetUnpurchasedSubscriptionsList(userID, null);
        }

        /// <summary>
        /// Get the List of subscriptions the user doesn't own buckets
        /// </summary>
        /// <returns></returns>
        public DataTable GetUnpurchasedSubscriptions(int userID, string passGroupFilter)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userID", userID));

            string sqlSelect = "SELECT po.promotion_id, upsell_image_path, bundle_title, name " +
            "FROM subscriptions s INNER JOIN promotions_to_subscriptions pts ON s.subscription_id = pts.subscription_id " +
            "INNER JOIN promotional_offers po ON pts.promotion_id = po.promotion_id ";
 
            string sqlWhere = "WHERE po.promotion_end >= now() AND ";

            if ((passGroupFilter != null) && (passGroupFilter != ""))
            {
                sqlWhere += " pts.promotion_id NOT IN (" +
                "SELECT promotion_id FROM promotions_to_subscriptions pts INNER JOIN subscriptions_to_passgroups stp ON pts.subscription_id = stp.subscription_id " +
                "WHERE pass_group_id IN (" + passGroupFilter + ") UNION SELECT promotion_id FROM user_subscriptions us WHERE user_id = @userID AND status_id IN (" +
                (int)Subscription.SubscriptionStatus.Active + ", " + (int)Subscription.SubscriptionStatus.Cancelled + ") AND end_date > NOW())";
            }
            else
            {
                sqlWhere += "pts.promotion_id NOT IN (SELECT promotion_id FROM user_subscriptions us WHERE user_id = @userID AND status_id IN (" +
                (int)Subscription.SubscriptionStatus.Active + ", " + (int)Subscription.SubscriptionStatus.Cancelled + ") AND end_date > NOW())";
            }

            return Db.Master.GetDataTable(sqlSelect + sqlWhere, parameters);
        }

        /// <summary>
        /// Get the List of subscriptions the user doesn't own buckets
        /// </summary>
        /// <returns></returns>
        public List<Subscription> GetUnpurchasedSubscriptionsList(int userID, string filter)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userID", userID));

            string sqlSelect = "SELECT po.promotion_id, s.subscription_id, name, price, introductory_price, introductory_end_date, grandfather_price, " +
            "term, days_free, learn_more_image_path, upsell_image_path, learn_more_content, discount_percent, keep_subscription_reasons, monthly_allowance " +
            "FROM subscriptions s LEFT JOIN promotions_to_subscriptions pts ON s.subscription_id = pts.subscription_id " +
            "LEFT JOIN promotional_offers po ON pts.promotion_id = po.promotion_id WHERE po.promotion_end >= now() AND " +
            "pts.promotion_id NOT IN (SELECT promotion_id FROM user_subscriptions us WHERE user_id = @userID AND status_id IN (" + 
            (int)Subscription.SubscriptionStatus.Active + ", " + (int)Subscription.SubscriptionStatus.Cancelled + ") AND end_date > NOW())";

            if ((filter != null) && (filter != ""))
            {
                sqlSelect += " AND " + filter;
            }

            DataTable dt = Db.Master.GetDataTable(sqlSelect, parameters);

            List<Subscription> list = new List<Subscription>();

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Subscription(Convert.ToUInt32(row["subscription_id"]), row["name"].ToString(), Convert.ToDouble(row["price"]), Convert.ToDouble(row["introductory_price"]),
                Convert.ToDateTime(row["introductory_end_date"]), row["grandfather_price"].ToString().Equals("Y"), (Subscription.SubscriptionTerm)Convert.ToInt32(row["term"]), Convert.ToInt32(row["days_free"]),
                row["upsell_image_path"].ToString(), row["learn_more_image_path"].ToString(), row["learn_more_content"].ToString(), Convert.ToUInt32(row["discount_percent"]),
                row["keep_subscription_reasons"].ToString(), Convert.ToUInt32(row["monthly_allowance"])));
            }

            return list;

        }

        /// <summary>
        /// GetAllSubscriptions
        /// </summary>
        public DataTable GetAllSubscriptions()
        {
            string sqlString = "SELECT subscription_id, name, price, introductory_price, introductory_end_date, grandfather_price, monthly_allowance, " +
                " term, days_free, learn_more_image_path, upsell_image_path, learn_more_content, discount_percent, keep_subscription_reasons" +
                " FROM subscriptions";

            return Db.Master.GetDataTable(sqlString);
        }


        /// <summary>
        /// GetAllSubscriptionsList
        /// </summary>
        public List<Subscription> GetAllSubscriptionsList()
        {
            DataTable dt = GetAllSubscriptions();

            List<Subscription> list = new List<Subscription>();

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Subscription(Convert.ToUInt32(row["subscription_id"]), row["name"].ToString(), Convert.ToDouble(row["price"]), Convert.ToDouble(row["introductory_price"]),
                Convert.ToDateTime(row["introductory_end_date"]), row["grandfather_price"].ToString().Equals("Y"), (Subscription.SubscriptionTerm)Convert.ToInt32(row["term"]), Convert.ToInt32(row["days_free"]),
                row["upsell_image_path"].ToString(), row["learn_more_image_path"].ToString(), row["learn_more_content"].ToString(), Convert.ToUInt32(row["discount_percent"]),
                row["keep_subscription_reasons"].ToString(), Convert.ToUInt32(row["monthly_allowance"])));
            }

            return list;
        }

        /// <summary>
        /// GetSubscription
        /// </summary>
        public Subscription GetSubscription(uint SubscriptionId)
        {
            string sqlString = "SELECT subscription_id, name, price, introductory_price, introductory_end_date, grandfather_price, monthly_allowance, " +
                " term, days_free, learn_more_image_path, upsell_image_path, learn_more_content, discount_percent, keep_subscription_reasons" +
                " FROM subscriptions " +
                " WHERE subscription_id = @SubscriptionId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@SubscriptionId", SubscriptionId));
            DataRow row = Db.Master.GetDataRow(sqlString, parameters);

            if (row == null)
            {
                return new Subscription();
            }

            return new Subscription(Convert.ToUInt32(row["subscription_id"]), row["name"].ToString(), Convert.ToDouble(row["price"]), Convert.ToDouble(row["introductory_price"]),
                Convert.ToDateTime(row["introductory_end_date"]), row["grandfather_price"].ToString().Equals("Y"), (Subscription.SubscriptionTerm)Convert.ToInt32(row["term"]), Convert.ToInt32(row["days_free"]),
                row["upsell_image_path"].ToString(), row["learn_more_image_path"].ToString(), row["learn_more_content"].ToString(), Convert.ToUInt32(row["discount_percent"]),
                row["keep_subscription_reasons"].ToString(), Convert.ToUInt32(row["monthly_allowance"]));
        }

        /// <summary>
        /// Get Subscription by the UserSubscriptionId
        /// </summary>
        public Subscription GetSubscriptionByUserSubscriptionId(uint userSubscriptionId)
        {
            string sqlString = "SELECT promotion_id, s.subscription_id, name, s.price, introductory_price, introductory_end_date, grandfather_price, " +
                " term, days_free, learn_more_image_path, upsell_image_path, learn_more_content, discount_percent, keep_subscription_reasons, monthly_allowance " +
                " FROM subscriptions s INNER JOIN user_subscriptions us ON s.subscription_id = us.subscription_id " +
                " WHERE user_subscription_id = @userSubscriptionId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userSubscriptionId", userSubscriptionId));
            DataRow row = Db.Master.GetDataRow(sqlString, parameters);

            if (row == null)
            {
                return new Subscription();
            }

            return new Subscription(Convert.ToUInt32(row["subscription_id"]), row["name"].ToString(), Convert.ToDouble(row["price"]), Convert.ToDouble(row["introductory_price"]),
                Convert.ToDateTime(row["introductory_end_date"]), row["grandfather_price"].ToString().Equals("Y"), (Subscription.SubscriptionTerm)Convert.ToInt32(row["term"]), Convert.ToInt32(row["days_free"]),
                row["upsell_image_path"].ToString(), row["learn_more_image_path"].ToString(), row["learn_more_content"].ToString(), Convert.ToUInt32(row["discount_percent"]),
                row["keep_subscription_reasons"].ToString(), Convert.ToUInt32(row["monthly_allowance"]));
        }

        /// <summary>
        /// Get Subscription by the UserSubscriptionId
        /// </summary>
        public List<Subscription> GetUserActiveSubscriptions (int userId)
        {
            string sqlString = "SELECT promotion_id, s.subscription_id, name, s.price, introductory_price, introductory_end_date, grandfather_price, " +
                " term, days_free, learn_more_image_path, upsell_image_path, learn_more_content, s.discount_percent, keep_subscription_reasons, monthly_allowance " +
                " FROM subscriptions s INNER JOIN user_subscriptions us ON s.subscription_id = us.subscription_id " +
                " WHERE us.user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            DataTable dt = Db.Master.GetDataTable (sqlString, parameters);

            List<Subscription> list = new List<Subscription> ();

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new Subscription (Convert.ToUInt32 (row["subscription_id"]), row["name"].ToString (), Convert.ToDouble (row["price"]), Convert.ToDouble (row["introductory_price"]),
                Convert.ToDateTime (row["introductory_end_date"]), row["grandfather_price"].ToString ().Equals ("Y"), (Subscription.SubscriptionTerm) Convert.ToInt32 (row["term"]), Convert.ToInt32 (row["days_free"]),
                row["upsell_image_path"].ToString (), row["learn_more_image_path"].ToString (), row["learn_more_content"].ToString (), Convert.ToUInt32 (row["discount_percent"]),
                row["keep_subscription_reasons"].ToString (), Convert.ToUInt32 (row["monthly_allowance"])));
            }

            return list;
        }

        /// <summary>
        /// GetSubscriptionByPromotionId
        /// </summary>
        public Subscription GetSubscriptionByPromotionId(uint promotionId)
        {
            string sqlString = "SELECT promotion_id, s.subscription_id, name, price, introductory_price, introductory_end_date, grandfather_price, " +
                " term, days_free, learn_more_image_path, upsell_image_path, learn_more_content, discount_percent, keep_subscription_reasons, monthly_allowance " +
                " FROM subscriptions s INNER JOIN promotions_to_subscriptions pts ON s.subscription_id = pts.subscription_id " +
                " WHERE promotion_id = @promotionId LIMIT 1";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@promotionId", promotionId));
            DataRow row = Db.Master.GetDataRow(sqlString, parameters);

            if (row == null)
            {
                return new Subscription();
            }

            return new Subscription(Convert.ToUInt32(row["subscription_id"]), row["name"].ToString(), Convert.ToDouble(row["price"]), Convert.ToDouble(row["introductory_price"]),
                Convert.ToDateTime(row["introductory_end_date"]), row["grandfather_price"].ToString().Equals("Y"), (Subscription.SubscriptionTerm)Convert.ToInt32(row["term"]), Convert.ToInt32(row["days_free"]),
                row["upsell_image_path"].ToString(), row["learn_more_image_path"].ToString(), row["learn_more_content"].ToString(), Convert.ToUInt32(row["discount_percent"]),
                row["keep_subscription_reasons"].ToString(), Convert.ToUInt32(row["monthly_allowance"]));
        }

        /// <summary>
        /// GetSubscriptionsByPromotionId
        /// </summary>
        public List<Subscription> GetSubscriptionsByPromotionId(uint promotionId)
        {
            string sqlString = "SELECT subscription_id, name, price, introductory_price, introductory_end_date, grandfather_price, monthly_allowance, " +
                " term, days_free, learn_more_image_path, upsell_image_path, learn_more_content, discount_percent, keep_subscription_reasons" +
                " FROM subscriptions WHERE subscription_id IN (SELECT subscription_id FROM promotions_to_subscriptions WHERE promotion_id = @promotionId) " ;

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@promotionId", promotionId));
            DataTable dt = Db.Master.GetDataTable(sqlString, parameters);

            List<Subscription> list = new List<Subscription>();

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Subscription(Convert.ToUInt32(row["subscription_id"]), row["name"].ToString(), Convert.ToDouble(row["price"]), Convert.ToDouble(row["introductory_price"]),
                Convert.ToDateTime(row["introductory_end_date"]), row["grandfather_price"].ToString().Equals("Y"), (Subscription.SubscriptionTerm)Convert.ToInt32(row["term"]), Convert.ToInt32(row["days_free"]),
                row["upsell_image_path"].ToString(), row["learn_more_image_path"].ToString(), row["learn_more_content"].ToString(), Convert.ToUInt32(row["discount_percent"]),
                row["keep_subscription_reasons"].ToString(), Convert.ToUInt32(row["monthly_allowance"])));
            }

            return list;
        }

        /// <summary>
        /// GetSubscriptionsByPassGroupIds
        /// </summary>
        public List<Subscription> GetSubscriptionsByPassGroupIds(string passGroupIds)
        {
            string sqlString = "SELECT subscription_id, name, price, introductory_price, introductory_end_date, grandfather_price, monthly_allowance, " +
                " term, days_free, learn_more_image_path, upsell_image_path, learn_more_content, discount_percent, keep_subscription_reasons" +
                " FROM subscriptions WHERE subscription_id IN (SELECT subscription_id FROM subscriptions_to_passgroups WHERE pass_group_id IN(@passGroupIds) ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@passGroupIds", passGroupIds));
            DataTable dt = Db.Master.GetDataTable(sqlString, parameters);

            List<Subscription> list = new List<Subscription>();

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Subscription(Convert.ToUInt32(row["subscription_id"]), row["name"].ToString(), Convert.ToDouble(row["price"]), Convert.ToDouble(row["introductory_price"]),
                Convert.ToDateTime(row["introductory_end_date"]), row["grandfather_price"].ToString().Equals("Y"), (Subscription.SubscriptionTerm)Convert.ToInt32(row["term"]), Convert.ToInt32(row["days_free"]),
                row["upsell_image_path"].ToString(), row["learn_more_image_path"].ToString(), row["learn_more_content"].ToString(), Convert.ToUInt32(row["discount_percent"]),
                row["keep_subscription_reasons"].ToString(), Convert.ToUInt32(row["monthly_allowance"])));
            }

            return list;
        }

        /// <summary>
        /// GetAvailableSubscriptions
        /// </summary>
        public DataTable GetAvailableSubscriptions()
        {
            string sqlString = "SELECT subscription_id, name FROM subscriptions ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            return Db.Master.GetDataTable(sqlString, parameters);

        }

        /// <summary>
        /// GetSubscriptionTerms
        /// </summary>
        public DataTable GetSubscriptionTerms()
        {
            string sqlString = "SELECT subscription_term_id, subscription_term FROM subscription_terms";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            return Db.Master.GetDataTable(sqlString, parameters);

        }

        /// <summary>
        /// GetSubscriptionStatus
        /// </summary>
        public DataTable GetSubscriptionStatus()
        {
            string sqlString = "SELECT name, status_id, description FROM subscription_status";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            return Db.Master.GetDataTable(sqlString, parameters);

        }

        /// <summary>
        /// InsertSubscription2PassGroup
        /// </summary>
        public int InsertSubscription2PassGroup(uint pass_group_id, uint subscription_id)
        {
            string sqlString = "INSERT INTO subscriptions_to_passgroups (pass_group_id, subscription_id) VALUES (@pass_group_id, @subscription_id)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@pass_group_id", pass_group_id));
            parameters.Add(new MySqlParameter("@subscription_id", subscription_id));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// DeleteAllSubscription2PassGroupByPassGroupID
        /// </summary>
        public int DeleteAllSubscription2PassGroupByPassGroupID(uint pass_group_id)
        {
            string sqlString = "DELETE FROM subscriptions_to_passgroups WHERE pass_group_id = @pass_group_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@pass_group_id", pass_group_id));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// DeleteAllSubscription2PassGroupBySubscptID
        /// </summary>
        public int DeleteAllSubscription2PassGroupBySubscptID(uint subscription_id)
        {
            string sqlString = "DELETE FROM subscriptions_to_passgroups WHERE subscription_id = @subscription_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@subscription_id", subscription_id));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }


        /// <summary>
        /// InsertSubscription2Promotion
        /// </summary>
        public int InsertSubscription2Promotion(int promotion_id, int subscription_id)
        {
            string sqlString = "INSERT INTO promotions_to_subscriptions (promotion_id, subscription_id) VALUES (@promotion_id, @subscription_id)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@promotion_id", promotion_id));
            parameters.Add(new MySqlParameter("@subscription_id", subscription_id));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// DeleteAllSubscription2PromotionByPromoID
        /// </summary>
        public int DeleteAllSubscription2PromotionByPromoID(int promotion_id)
        {
            string sqlString = "DELETE FROM promotions_to_subscriptions WHERE promotion_id = @promotion_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@promotion_id", promotion_id));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summaryDeleteAllSubscription2PromotionBySubscptID
        /// DeleteAllSubscription2PromotionByPromoID
        /// </summary>
        public int DeleteAllSubscription2PromotionBySubscptID(int subscription_id)
        {
            string sqlString = "DELETE FROM promotions_to_subscriptions WHERE subscription_id = @subscription_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@subscription_id", subscription_id));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// DeleteSubscription
        /// </summary>
        public int DeleteSubscription(int subscriptionId)
        {
            string sqlString = "DELETE FROM subscriptions WHERE subscription_id = @subscription_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@subscription_id", subscriptionId));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// InsertSubscription
        /// </summary>
        public int InsertSubscription(Subscription subscription)
        {
            string sqlString = "INSERT INTO subscriptions (" +
                " subscription_id, name, price, introductory_price, introductory_end_date, grandfather_price, monthly_allowance, " +
                " term, days_free, learn_more_image_path, upsell_image_path, learn_more_content, discount_percent, keep_subscription_reasons" +
                ") VALUES (" +
                " @subscription_id,  @name,  @price,  @introductory_price,  @introductory_end_date,  @grandfather_price, @monthly_allowance, " +
                "  @term,  @days_free, @learn_more_image_path, @upsell_image_path, @learn_more_content, @discount_percent, @keep_subscription_reasons" +
                ")";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@name", subscription.Name));
            parameters.Add(new MySqlParameter("@price", subscription.Price));
            parameters.Add(new MySqlParameter("@introductory_price", subscription.IntroductoryPrice));
            parameters.Add(new MySqlParameter("@introductory_end_date", subscription.IntroductoryEndDate));
            parameters.Add(new MySqlParameter("@grandfather_price", subscription.GrandfatherPrice ? "Y" : "N"));
            parameters.Add(new MySqlParameter("@term", (int) subscription.Term));
            parameters.Add(new MySqlParameter("@days_free", subscription.DaysFree));
            parameters.Add(new MySqlParameter("@learn_more_image_path", subscription.LearnMoreImage));
            parameters.Add(new MySqlParameter("@upsell_image_path", subscription.UpsellImage));
            parameters.Add(new MySqlParameter("@learn_more_content", subscription.LearnMoreContent));
            parameters.Add(new MySqlParameter("@discount_percent", subscription.DiscountPercent));
            parameters.Add(new MySqlParameter("@keep_subscription_reasons", subscription.Reasons2Keep));
            parameters.Add(new MySqlParameter("@monthly_allowance", subscription.MonthlyAllowance));

            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// UpdateSubscription
        /// </summary>
        public void UpdateSubscription(Subscription subscription)
        {
            string sqlString = "UPDATE subscriptions SET" +
                " subscription_id=@SubscriptionId, " +
                " name=@name, " +
                " price=@price, " +
                " introductory_price=@introductory_price, " +
                " introductory_end_date=@introductory_end_date, " +
                " grandfather_price=@grandfather_price, " +
                " term=@term, " +
                " learn_more_image_path=@learn_more_image_path, " +
                " upsell_image_path=@upsell_image_path, " +
                " learn_more_content=@learn_more_content, " +
                " discount_percent=@discount_percent, " +
                " keep_subscription_reasons=@keep_subscription_reasons, " +
                " days_free=@days_free, " +
                " monthly_allowance=@monthly_allowance" +
                " WHERE subscription_id = @SubscriptionId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@SubscriptionId", subscription.SubscriptionId));
            parameters.Add(new MySqlParameter("@name", subscription.Name));
            parameters.Add(new MySqlParameter("@price", subscription.Price));
            parameters.Add(new MySqlParameter("@introductory_price", subscription.IntroductoryPrice));
            parameters.Add(new MySqlParameter("@introductory_end_date", subscription.IntroductoryEndDate));
            parameters.Add(new MySqlParameter("@grandfather_price", subscription.GrandfatherPrice ? "Y" : "N"));
            parameters.Add(new MySqlParameter("@term", (int) subscription.Term));
            parameters.Add(new MySqlParameter("@learn_more_image_path", subscription.LearnMoreImage));
            parameters.Add(new MySqlParameter("@upsell_image_path", subscription.UpsellImage));
            parameters.Add(new MySqlParameter("@days_free", subscription.DaysFree));
            parameters.Add(new MySqlParameter("@learn_more_content", subscription.LearnMoreContent));
            parameters.Add(new MySqlParameter("@discount_percent", subscription.DiscountPercent));
            parameters.Add(new MySqlParameter("@keep_subscription_reasons", subscription.Reasons2Keep));
            parameters.Add(new MySqlParameter("@monthly_allowance", subscription.MonthlyAllowance));

            Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// InsertUserSubscription
        /// </summary>
        public uint InsertUserSubscription(UserSubscription userSubscription)
        {
            string sqlString = "INSERT INTO user_subscriptions (" +
                " subscription_id, promotion_id, user_id, price, auto_renew, purchase_date, status_id, " +
                " billing_type, subscription_identifier, user_cancelled, cancelled_date, inital_term, " +
                " renewal_term, end_date, free_trial_end_date, user_cancellation_reason, " +
                " discount_percent, monthly_allowance" +
                ") VALUES (" +
                " @subscription_id, @promotion_id, @user_id, @price,  @auto_renew,  @purchase_date,  @status_id, " +
                "  @billing_type,  @subscription_identifier,  @user_cancelled,  @cancelled_date,  @inital_term, " +
                "  @renewal_term,  @end_date,  @free_trial_end_date, @user_cancellation_reason, " +
                " @discount_percent, @monthly_allowance)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@subscription_id", userSubscription.SubscriptionId));
            parameters.Add(new MySqlParameter("@promotion_id", userSubscription.PromotionId));
            parameters.Add(new MySqlParameter("@user_id", userSubscription.UserId));
            parameters.Add(new MySqlParameter("@price", userSubscription.Price));
            parameters.Add(new MySqlParameter("@auto_renew", userSubscription.AutoRenew ? "Y" : "N"));
            parameters.Add(new MySqlParameter("@purchase_date", userSubscription.PurchaseDate));
            parameters.Add(new MySqlParameter("@status_id", (uint) userSubscription.StatusId));
            parameters.Add(new MySqlParameter("@billing_type", userSubscription.BillingType));
            parameters.Add(new MySqlParameter("@subscription_identifier", userSubscription.SubscriptionIdentifier));
            parameters.Add(new MySqlParameter("@user_cancelled", userSubscription.UserCancelled ? "Y" : "N"));
            parameters.Add(new MySqlParameter("@cancelled_date", userSubscription.CancelledDate));
            parameters.Add(new MySqlParameter("@inital_term", (int) userSubscription.InitalTerm));
            parameters.Add(new MySqlParameter("@renewal_term", (int) userSubscription.RenewalTerm));
            parameters.Add(new MySqlParameter("@end_date", userSubscription.EndDate));
            parameters.Add(new MySqlParameter("@free_trial_end_date", userSubscription.FreeTrialEndDate));
            parameters.Add(new MySqlParameter("@user_cancellation_reason", userSubscription.UserCancellationReason));
            parameters.Add(new MySqlParameter("@discount_percent", userSubscription.DiscountPercent));
            parameters.Add(new MySqlParameter("@monthly_allowance", userSubscription.MontlyAllowance));
            uint result = (uint)Db.Master.ExecuteIdentityInsert(sqlString, parameters);

            // Clear the cache 
            CentralCache.Remove (CentralCache.keyKanevaUserSubscriptionValid + userSubscription.UserId.ToString () + userSubscription.SubscriptionId.ToString ());
            CentralCache.Remove (CentralCache.keyUserSubscriptionsActive + userSubscription.UserId.ToString ());

            return result;
        }

        /// <summary>
        /// GetUserSubscription
        /// </summary>
        public UserSubscription GetUserSubscription(uint userSubscriptionId)
        {
            string sqlString = "SELECT user_subscription_id, subscription_id, promotion_id, user_id, price, auto_renew, purchase_date, status_id, " +
                " billing_type, subscription_identifier, user_cancelled, cancelled_date, inital_term, " +
                " renewal_term, end_date, free_trial_end_date, user_cancellation_reason, discount_percent, monthly_allowance, is_legacy " +
                " FROM user_subscriptions" +
                " WHERE user_subscription_id = @UserSubscriptionId"; 

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@UserSubscriptionId", userSubscriptionId));
            DataRow row = Db.Master.GetDataRow(sqlString, parameters);

            if (row == null)
            {
                return new UserSubscription(0, 0, 0, 0, 0.0, false, DateTime.Now, 0, 0, "",
                    false, DateTime.Now, 0, 0, DateTime.Now, DateTime.Now,"", 0, 0, false);
            }

            return new UserSubscription(Convert.ToUInt32(row["user_subscription_id"]), Convert.ToUInt32(row["subscription_id"]), Convert.ToUInt32(row["promotion_id"]), Convert.ToInt32(row["user_id"]), Convert.ToDouble(row["price"]), row["auto_renew"].ToString().Equals("Y"), Convert.ToDateTime(row["purchase_date"]),
            (Subscription.SubscriptionStatus)Convert.ToUInt32(row["status_id"]), Convert.ToInt32(row["billing_type"]), row["subscription_identifier"].ToString(), row["user_cancelled"].ToString().Equals("Y"),
            Convert.ToDateTime(row["cancelled_date"]), (Subscription.SubscriptionTerm)Convert.ToInt32(row["inital_term"]), (Subscription.SubscriptionTerm)Convert.ToInt32(row["renewal_term"]), Convert.ToDateTime(row["end_date"]),
            Convert.ToDateTime(row["free_trial_end_date"]), row["user_cancellation_reason"].ToString(), Convert.ToUInt32(row["discount_percent"]), Convert.ToUInt32(row["monthly_allowance"]), row["is_legacy"].ToString().Equals("Y"));
        }

        /// <summary>
        /// GetUserSubscription
        /// </summary>
        public UserSubscription GetUserSubscription(uint SubscriptionId, uint userID)
        {
            string sqlString = "SELECT user_subscription_id, subscription_id, promotion_id, user_id, price, auto_renew, purchase_date, status_id, " +
                " billing_type, subscription_identifier, user_cancelled, cancelled_date, inital_term, " +
                " renewal_term, end_date, free_trial_end_date, user_cancellation_reason, discount_percent, monthly_allowance, is_legacy " +
                " FROM user_subscriptions" +
                " WHERE subscription_id = @SubscriptionId AND user_id = @userID AND subscription_identifier > 0 " +
                " ORDER BY user_subscription_id DESC LIMIT 1"; //makes sure it gets the most recent one in case there have been many

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@SubscriptionId", SubscriptionId));
            parameters.Add(new MySqlParameter("@userID", userID));
            DataRow row = Db.Master.GetDataRow(sqlString, parameters);

            if (row == null)
            {
                return new UserSubscription(0, 0, 0, 0, 0.0, false, DateTime.Now, 0, 0, "",
                    false, DateTime.Now, 0, 0, DateTime.Now, DateTime.Now, "",0,0, false);
            }

            return new UserSubscription(Convert.ToUInt32(row["user_subscription_id"]), Convert.ToUInt32(row["subscription_id"]), Convert.ToUInt32(row["promotion_id"]), Convert.ToInt32(row["user_id"]), Convert.ToDouble(row["price"]), row["auto_renew"].ToString().Equals("Y"), Convert.ToDateTime(row["purchase_date"]),
            (Subscription.SubscriptionStatus)Convert.ToUInt32(row["status_id"]), Convert.ToInt32(row["billing_type"]), row["subscription_identifier"].ToString(), row["user_cancelled"].ToString().Equals("Y"),
            Convert.ToDateTime(row["cancelled_date"]), (Subscription.SubscriptionTerm)Convert.ToInt32(row["inital_term"]), (Subscription.SubscriptionTerm)Convert.ToInt32(row["renewal_term"]), Convert.ToDateTime(row["end_date"]),
            Convert.ToDateTime(row["free_trial_end_date"]), row["user_cancellation_reason"].ToString(), Convert.ToUInt32(row["discount_percent"]), Convert.ToUInt32(row["monthly_allowance"]), row["is_legacy"].ToString().Equals("Y"));
        }

        /// <summary>
        /// GetUserSubscriptionByPromotionId
        /// </summary>
        public UserSubscription GetUserSubscriptionByPromotionId(uint promotionID, uint userID)
        {
            string sqlString = "SELECT user_subscription_id, subscription_id, promotion_id, user_id, price, auto_renew, purchase_date, status_id, " +
                " billing_type, subscription_identifier, user_cancelled, cancelled_date, inital_term, " +
                " renewal_term, end_date, free_trial_end_date, user_cancellation_reason, discount_percent, monthly_allowance, is_legacy " +
                " FROM user_subscriptions" +
                " WHERE promotion_id = @promotionID AND user_id = @userID AND subscription_identifier > 0 " +
                " ORDER BY user_subscription_id DESC LIMIT 1"; //makes sure it gets the most recent one in case there have been many

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@promotionID", promotionID));
            parameters.Add(new MySqlParameter("@userID", userID));
            DataRow row = Db.Master.GetDataRow(sqlString, parameters);

            if (row == null)
            {
                return new UserSubscription(0, 0, 0, 0, 0.0, false, DateTime.Now, 0, 0, "",
                    false, DateTime.Now, 0, 0, DateTime.Now, DateTime.Now, "", 0, 0, false);
            }

            return new UserSubscription(Convert.ToUInt32(row["user_subscription_id"]), Convert.ToUInt32(row["subscription_id"]), Convert.ToUInt32(row["promotion_id"]), Convert.ToInt32(row["user_id"]), Convert.ToDouble(row["price"]), row["auto_renew"].ToString().Equals("Y"), Convert.ToDateTime(row["purchase_date"]),
            (Subscription.SubscriptionStatus)Convert.ToUInt32(row["status_id"]), Convert.ToInt32(row["billing_type"]), row["subscription_identifier"].ToString(), row["user_cancelled"].ToString().Equals("Y"),
            Convert.ToDateTime(row["cancelled_date"]), (Subscription.SubscriptionTerm)Convert.ToInt32(row["inital_term"]), (Subscription.SubscriptionTerm)Convert.ToInt32(row["renewal_term"]), Convert.ToDateTime(row["end_date"]),
            Convert.ToDateTime(row["free_trial_end_date"]), row["user_cancellation_reason"].ToString(), Convert.ToUInt32(row["discount_percent"]), Convert.ToUInt32(row["monthly_allowance"]), row["is_legacy"].ToString().Equals("Y"));
        }

        /// <summary>
        /// GetUserSubscriptionsByPromotionIds
        /// </summary>
        public List<UserSubscription> GetUserSubscriptionsByPromotionIds(string promotionIDList, uint userID)
        {
            string sqlString = "SELECT user_subscription_id, subscription_id, promotion_id, user_id, price, auto_renew, purchase_date, status_id, " +
                " billing_type, subscription_identifier, user_cancelled, cancelled_date, inital_term, " +
                " renewal_term, end_date, free_trial_end_date, user_cancellation_reason, discount_percent, monthly_allowance, is_legacy " +
                " FROM user_subscriptions" +
                " WHERE promotion_id IN (" + promotionIDList + ") AND user_id = @userID AND subscription_identifier > 0 AND end_date > NOW() " +
                " ORDER BY user_subscription_id DESC"; 

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            //parameters.Add(new MySqlParameter("@promotionIDList", promotionIDList));
            parameters.Add(new MySqlParameter("@userID", userID));
            DataTable dt = Db.Master.GetDataTable(sqlString, parameters);

            List<UserSubscription> list = new List<UserSubscription>();

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new UserSubscription(Convert.ToUInt32(row["user_subscription_id"]), Convert.ToUInt32(row["subscription_id"]), Convert.ToUInt32(row["promotion_id"]), Convert.ToInt32(row["user_id"]), Convert.ToDouble(row["price"]), row["auto_renew"].ToString().Equals("Y"), Convert.ToDateTime(row["purchase_date"]),
                    (Subscription.SubscriptionStatus)Convert.ToUInt32(row["status_id"]), Convert.ToInt32(row["billing_type"]), row["subscription_identifier"].ToString(), row["user_cancelled"].ToString().Equals("Y"),
                    Convert.ToDateTime(row["cancelled_date"]), (Subscription.SubscriptionTerm)Convert.ToInt32(row["inital_term"]), (Subscription.SubscriptionTerm)Convert.ToInt32(row["renewal_term"]), Convert.ToDateTime(row["end_date"]),
                    Convert.ToDateTime(row["free_trial_end_date"]), row["user_cancellation_reason"].ToString(), Convert.ToUInt32(row["discount_percent"]), Convert.ToUInt32(row["monthly_allowance"]), row["is_legacy"].ToString().Equals("Y")));
            }
            return list;
        }


        /// <summary>
        /// GetUserSubscriptionByPassGroupId
        /// </summary>
        public UserSubscription GetUserSubscriptionByPassGroupId(uint passGroupId, uint userID)
        {
            string sqlString = "SELECT user_subscription_id, subscription_id, promotion_id, user_id, price, auto_renew, purchase_date, status_id, " +
                " billing_type, subscription_identifier, user_cancelled, cancelled_date, inital_term, " +
                " renewal_term, end_date, free_trial_end_date, user_cancellation_reason, discount_percent, monthly_allowance, is_legacy " +
                " FROM user_subscriptions" +
                " WHERE user_id = @userID " +
                " AND subscription_id IN (SELECT s.subscription_id FROM subscriptions s INNER JOIN subscriptions_to_passgroups stp ON s.subscription_id = stp.subscription_id WHERE pass_group_id = @passGroupId)" +
                " AND status_id in (" + (int)Subscription.SubscriptionStatus.Active + ", " + (int)Subscription.SubscriptionStatus.Cancelled + ") " +
                " AND end_date > NOW() " + 
                " ORDER BY user_subscription_id DESC LIMIT 1"; //makes sure it gets the most recent one in case there have been many

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userID));
            parameters.Add(new MySqlParameter("@passGroupId", passGroupId));
            DataRow row = Db.Master.GetDataRow(sqlString, parameters);

            if (row == null)
            {
                return new UserSubscription(0, 0, 0, 0, 0.0, false, DateTime.Now, 0, 0, "",
                    false, DateTime.Now, 0, 0, DateTime.Now, DateTime.Now, "", 0,0, false);
            }

            return new UserSubscription(Convert.ToUInt32(row["user_subscription_id"]), Convert.ToUInt32(row["subscription_id"]), Convert.ToUInt32(row["promotion_id"]), Convert.ToInt32(row["user_id"]), Convert.ToDouble(row["price"]), row["auto_renew"].ToString().Equals("Y"), Convert.ToDateTime(row["purchase_date"]),
            (Subscription.SubscriptionStatus)Convert.ToUInt32(row["status_id"]), Convert.ToInt32(row["billing_type"]), row["subscription_identifier"].ToString(), row["user_cancelled"].ToString().Equals("Y"),
            Convert.ToDateTime(row["cancelled_date"]), (Subscription.SubscriptionTerm)Convert.ToInt32(row["inital_term"]), (Subscription.SubscriptionTerm)Convert.ToInt32(row["renewal_term"]), Convert.ToDateTime(row["end_date"]),
            Convert.ToDateTime(row["free_trial_end_date"]), row["user_cancellation_reason"].ToString(), Convert.ToUInt32(row["discount_percent"]), Convert.ToUInt32(row["monthly_allowance"]), row["is_legacy"].ToString().Equals("Y"));
        }

        /// <summary>
        /// GetUserSubscriptions
        /// </summary>
        public List<UserSubscription> GetUserSubscriptions(int userId)
        {
            string sqlString = "SELECT user_subscription_id, subscription_id, promotion_id, user_id, price, auto_renew, purchase_date, status_id, " +
                " billing_type, subscription_identifier, user_cancelled, cancelled_date, inital_term, " +
                " renewal_term, end_date, free_trial_end_date, user_cancellation_reason, discount_percent, monthly_allowance, is_legacy " +
                " FROM user_subscriptions" +
                " WHERE user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            DataTable dt = Db.Master.GetDataTable(sqlString, parameters);

            List<UserSubscription> list = new List<UserSubscription>();

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new UserSubscription(Convert.ToUInt32(row["user_subscription_id"]), Convert.ToUInt32(row["subscription_id"]), Convert.ToUInt32(row["promotion_id"]), Convert.ToInt32(row["user_id"]), Convert.ToDouble(row["price"]), row["auto_renew"].ToString().Equals("Y"), Convert.ToDateTime(row["purchase_date"]),
                    (Subscription.SubscriptionStatus)Convert.ToUInt32(row["status_id"]), Convert.ToInt32(row["billing_type"]), row["subscription_identifier"].ToString(), row["user_cancelled"].ToString().Equals("Y"),
                    Convert.ToDateTime(row["cancelled_date"]), (Subscription.SubscriptionTerm)Convert.ToInt32(row["inital_term"]), (Subscription.SubscriptionTerm)Convert.ToInt32(row["renewal_term"]), Convert.ToDateTime(row["end_date"]),
                    Convert.ToDateTime(row["free_trial_end_date"]), row["user_cancellation_reason"].ToString(), Convert.ToUInt32(row["discount_percent"]), Convert.ToUInt32(row["monthly_allowance"]), row["is_legacy"].ToString().Equals("Y")));
            }
            return list;
        }

        /// <summary>
        /// GetUserSubscriptions
        /// </summary>
        public List<UserSubscription> GetActiveUserSubscriptions(int userId)
        {
            string cacheKey = CentralCache.keyUserSubscriptionsActive + userId.ToString();
            List<UserSubscription> userSubs = (List<UserSubscription>)CentralCache.Get(cacheKey);

            if (userSubs != null)
            {
                // Found in cache
                return userSubs;
            }

            string sqlString = "SELECT user_subscription_id, subscription_id, promotion_id, user_id, price, auto_renew, purchase_date, status_id, " +
                " billing_type, subscription_identifier, user_cancelled, cancelled_date, inital_term, " +
                " renewal_term, end_date, free_trial_end_date, user_cancellation_reason, discount_percent, monthly_allowance, is_legacy " +
                " FROM user_subscriptions" +
                //" WHERE user_id = @userId AND (status_id<>" + (int)Subscription.SubscriptionStatus.Cancelled + " AND status_id<>" + (int)Subscription.SubscriptionStatus.Expired + ")";
                " WHERE user_id = @userId AND end_date > Now() AND status_id in (" + (int)Subscription.SubscriptionStatus.Active + ", " + (int)Subscription.SubscriptionStatus.Cancelled + ") ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            DataTable dt = Db.Master.GetDataTable(sqlString, parameters);

            userSubs = new List<UserSubscription>();

            foreach (DataRow row in dt.Rows)
            {
                userSubs.Add(new UserSubscription(Convert.ToUInt32(row["user_subscription_id"]), Convert.ToUInt32(row["subscription_id"]), Convert.ToUInt32(row["promotion_id"]), Convert.ToInt32(row["user_id"]), Convert.ToDouble(row["price"]), row["auto_renew"].ToString().Equals("Y"), Convert.ToDateTime(row["purchase_date"]),
                    (Subscription.SubscriptionStatus)Convert.ToUInt32(row["status_id"]), Convert.ToInt32(row["billing_type"]), row["subscription_identifier"].ToString(), row["user_cancelled"].ToString().Equals("Y"),
                    Convert.ToDateTime(row["cancelled_date"]), (Subscription.SubscriptionTerm)Convert.ToInt32(row["inital_term"]), (Subscription.SubscriptionTerm)Convert.ToInt32(row["renewal_term"]), Convert.ToDateTime(row["end_date"]),
                    Convert.ToDateTime(row["free_trial_end_date"]), row["user_cancellation_reason"].ToString(), Convert.ToUInt32(row["discount_percent"]), Convert.ToUInt32(row["monthly_allowance"]), row["is_legacy"].ToString().Equals("Y")));
            }

            CentralCache.Store(cacheKey, userSubs, TimeSpan.FromMinutes(15));

            return userSubs;
        }

        /// <summary>
        /// UpdateUserSubscription
        /// </summary>
        public void UpdateUserSubscription (UserSubscription userSubscription)
        {
            string sqlString = "UPDATE user_subscriptions SET " +
                " user_subscription_id=@user_subscription_id, " +
                " subscription_id=@subscription_id, " +
                " promotion_id=@promotion_id, " +
                " price=@price, " +
                " auto_renew=@auto_renew, " +
                " purchase_date=@purchase_date, " +
                " status_id=@status_id, " +
                " billing_type=@billing_type, " +
                " subscription_identifier=@subscription_identifier, " +
                " user_cancelled=@user_cancelled, " +
                " cancelled_date=@cancelled_date, " +
                " inital_term=@inital_term, " +
                " renewal_term=@renewal_term, " +
                " end_date=@end_date, " +
                " free_trial_end_date=@free_trial_end_date," +
                " user_cancellation_reason=@user_cancellation_reason," +
                " discount_percent=@discount_percent," +
                " monthly_allowance=@monthly_allowance" +
                " WHERE user_subscription_id = @user_subscription_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@user_subscription_id", userSubscription.UserSubscriptionId));
            parameters.Add(new MySqlParameter("@subscription_id", userSubscription.SubscriptionId));
            parameters.Add(new MySqlParameter("@promotion_id", userSubscription.PromotionId));
            parameters.Add(new MySqlParameter("@price", userSubscription.Price));
            parameters.Add(new MySqlParameter("@auto_renew", userSubscription.AutoRenew ? "Y" : "N"));
            parameters.Add(new MySqlParameter("@purchase_date", userSubscription.PurchaseDate));
            parameters.Add(new MySqlParameter("@status_id", (uint) userSubscription.StatusId));
            parameters.Add(new MySqlParameter("@billing_type", userSubscription.BillingType));
            parameters.Add(new MySqlParameter("@subscription_identifier", userSubscription.SubscriptionIdentifier));
            parameters.Add(new MySqlParameter("@user_cancelled", userSubscription.UserCancelled ? "Y" : "N"));
            parameters.Add(new MySqlParameter("@cancelled_date", userSubscription.CancelledDate));
            parameters.Add(new MySqlParameter("@inital_term", (int) userSubscription.InitalTerm));
            parameters.Add(new MySqlParameter("@renewal_term", (int) userSubscription.RenewalTerm));
            parameters.Add(new MySqlParameter("@end_date", userSubscription.EndDate));
            parameters.Add(new MySqlParameter("@free_trial_end_date", userSubscription.FreeTrialEndDate));
            parameters.Add(new MySqlParameter("@user_cancellation_reason", userSubscription.UserCancellationReason));
            parameters.Add(new MySqlParameter("@discount_percent", userSubscription.DiscountPercent));
            parameters.Add(new MySqlParameter("@monthly_allowance", userSubscription.MontlyAllowance));
            Db.Master.ExecuteNonQuery(sqlString, parameters);

            // Invalidate from cache
            CentralCache.Remove(CentralCache.keyKanevaUserSubscriptionValid + userSubscription.UserId.ToString() + userSubscription.SubscriptionId.ToString());
            CentralCache.Remove(CentralCache.keyUserSubscriptionsActive + userSubscription.UserId.ToString());
        }

        /// <summary>
        /// GetSubscriptionEntitlements
        /// </summary>
        public List<SubscriptionEntitlement> GetSubscriptionEntitlements(int subscriptionId, string gender)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sqlString = "SELECT entitlement_id, subscription_id, points, kei_point_id, " +
                " global_id, quantity, " +
                " keep_on_cancel" +
                " FROM subscription_entitlements" +
                " WHERE subscription_id = @subscriptionId ";
            // " AND gender = @gender ";

            //parameters.Add(new MySqlParameter("@gender", gender));
            parameters.Add(new MySqlParameter("@subscriptionId", subscriptionId));
            DataTable dt = Db.Master.GetDataTable(sqlString, parameters);

            List<SubscriptionEntitlement> list = new List<SubscriptionEntitlement>();

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SubscriptionEntitlement(Convert.ToUInt32(row["entitlement_id"]), Convert.ToUInt32(row["subscription_id"]), Convert.ToDouble(row["points"]), row["kei_point_id"].ToString(), Convert.ToInt32(row["global_id"]),
                    Convert.ToUInt32(row["quantity"]), row["keep_on_cancel"].ToString().Equals("Y")));
            }
            return list;
        }

        /// <summary>
        /// GetPassGroupsAssociatedWSubscription
        /// </summary>
        public DataTable GetPassGroupsAssociatedWSubscription(uint subscriptionId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sqlString = "SELECT pass_group_id, subscription_id FROM subscriptions_to_passgroups " +
                " WHERE subscription_id = @subscriptionId ";

            parameters.Add(new MySqlParameter("@subscriptionId", subscriptionId));

            return Db.Master.GetDataTable(sqlString, parameters);

        }

        /// <summary>
        /// GetPassGroupsToSubscription
        /// </summary>
        public DataTable GetPassGroupsToSubscription()
        {
            string sqlString = "SELECT pass_group_id, subscription_id FROM subscriptions_to_passgroups";

            return Db.Master.GetDataTable(sqlString);
        }

        /// <summary>
        /// HasValidSubscription
        /// </summary>
        public bool HasValidSubscription(int userId, int subscriptionId)
        {
            string cacheKey = CentralCache.keyKanevaUserSubscriptionValid + userId.ToString() + subscriptionId.ToString();
            String hasValidSubscription = (String)CentralCache.Get(cacheKey);

            if (hasValidSubscription != null)
            {
                // Found in cache
                return Convert.ToBoolean(hasValidSubscription);
            }

            string sqlString = "SELECT user_subscription_id " +
                " FROM user_subscriptions" +
                " WHERE user_id = @userId " +
                " AND subscription_id = @subscriptionId " +
                " AND status_id in (" + (int)Subscription.SubscriptionStatus.Active + ", " + (int)Subscription.SubscriptionStatus.Cancelled + ") " +
                " AND end_date > NOW() ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@subscriptionId", subscriptionId));
            DataRow row = Db.Master.GetDataRow(sqlString, parameters);

            hasValidSubscription = (row == null ? "false" : "true");
            CentralCache.Store(cacheKey, hasValidSubscription, TimeSpan.FromMinutes(5));
            return Convert.ToBoolean (hasValidSubscription);
        }

        /// <summary>
        /// HasValidSubscriptionByPassType
        /// </summary>
        public bool HasValidSubscriptionByPassType(uint userId, uint passGroupId)
        {
            // Cache it here
            string cacheKey = CentralCache.keyKanevaUserSubscriptionValidByPassGroup + userId.ToString() + passGroupId.ToString();
            String hasValidSubscription = (String)CentralCache.Get(cacheKey);

            if (hasValidSubscription != null)
            {
                // Found in cache
                return Convert.ToBoolean(hasValidSubscription);
            }

            string sqlString = "SELECT user_subscription_id " +
                " FROM user_subscriptions" +
                " WHERE user_id = @userId " +
                " AND subscription_id IN (SELECT s.subscription_id FROM subscriptions s INNER JOIN subscriptions_to_passgroups stp ON s.subscription_id = stp.subscription_id WHERE pass_group_id = @passGroupId)" +
                " AND status_id in (" + (int)Subscription.SubscriptionStatus.Active + ", " + (int)Subscription.SubscriptionStatus.Cancelled + ") " +
                " AND end_date > NOW() ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@passGroupId", passGroupId));
            DataRow row = Db.Master.GetDataRow(sqlString, parameters);

            hasValidSubscription = (row == null ? "false" : "true");
            CentralCache.Store(cacheKey, hasValidSubscription, TimeSpan.FromMinutes(5));
            return Convert.ToBoolean(hasValidSubscription);
        }

        /// <summary>
        /// InsertUserSubscriptionOrder
        /// </summary>
        /// <param name="usersubscriptionId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public int InsertUserSubscriptionOrder(uint usersubscriptionId, int orderId)
        {
            string sqlString = "INSERT INTO user_subscription_orders " +
                " (user_subscription_id, order_id) VALUES " +
                " (@usersubscriptionId, @orderId) ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@usersubscriptionId", usersubscriptionId));
            parameters.Add(new MySqlParameter("@orderId", orderId));
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// GetNextEndDate
        /// </summary>
        public DateTime GetNextEndDate(DateTime purchaseDate, DateTime currentEndDate, Subscription.SubscriptionTerm subscriptionTerm)
        {
            string sqlString = "";

            switch ((int)subscriptionTerm)
            {
                case (int)Subscription.SubscriptionTerm.None:
                    return new DateTime(currentEndDate.Year, currentEndDate.Month, currentEndDate.Day, 23, 59, 59);

                case (int)Subscription.SubscriptionTerm.BiMonthly:
                    {
                        // This calculates bi-monthly date very accurately 
                        // For example, if purchase is on the 31st day of month, it always extends to last day of month.
                        sqlString = "SELECT DATE_ADD(@purchaseDate, INTERVAL PERIOD_DIFF(DATE_FORMAT(@currentEndDate,'%Y%m'),DATE_FORMAT(@purchaseDate,'%Y%m')) + 2 MONTH)";
                        List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                        parameters.Add(new MySqlParameter("@purchaseDate", purchaseDate));
                        parameters.Add(new MySqlParameter("@currentEndDate", currentEndDate));
                        DateTime dtResult = Convert.ToDateTime(Db.Master.GetDataRow(sqlString, parameters)[0]);
                        return new DateTime(dtResult.Year, dtResult.Month, dtResult.Day, 23, 59, 59);
                    }

                case (int)Subscription.SubscriptionTerm.BiWeekly:
                    return currentEndDate.AddDays(14);

                case (int)Subscription.SubscriptionTerm.Monthly:
                {
                    // This calculates monthly date very accurately 
                    // For example, if purchase is on the 31st day of month, it always extends to last day of month.
                    sqlString = "SELECT DATE_ADD(@purchaseDate, INTERVAL PERIOD_DIFF(DATE_FORMAT(@currentEndDate,'%Y%m'),DATE_FORMAT(@purchaseDate,'%Y%m')) + 1 MONTH)";
                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@purchaseDate", purchaseDate));
                    parameters.Add(new MySqlParameter("@currentEndDate", currentEndDate));
                    DateTime dtResult = Convert.ToDateTime (Db.Master.GetDataRow(sqlString, parameters)[0]);
                    return new DateTime(dtResult.Year, dtResult.Month, dtResult.Day, 23, 59, 59);
                }

                case (int)Subscription.SubscriptionTerm.Weekly:
                    return currentEndDate.AddDays(7);

                case (int)Subscription.SubscriptionTerm.Yearly:
                    {
                        // This calculates monthly date very accurately 
                        // For example, if purchase is on the 31st day of month, it always extends to last day of month.
                        sqlString = "SELECT DATE_ADD(@purchaseDate, INTERVAL PERIOD_DIFF(DATE_FORMAT(@currentEndDate,'%Y%m'),DATE_FORMAT(@purchaseDate,'%Y%m')) + 12 MONTH)";
                        List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                        parameters.Add(new MySqlParameter("@purchaseDate", purchaseDate));
                        parameters.Add(new MySqlParameter("@currentEndDate", currentEndDate));
                        DateTime dtResult = Convert.ToDateTime(Db.Master.GetDataRow(sqlString, parameters)[0]);
                        return new DateTime(dtResult.Year, dtResult.Month, dtResult.Day, 23, 59, 59);
                    }

                case (int)Subscription.SubscriptionTerm.Quarterly:
                    {
                        // This calculates quarterly date very accurately 
                        // For example, if purchase is on the 31st day of month, it always extends to last day of month.
                        sqlString = "SELECT DATE_ADD(@purchaseDate, INTERVAL PERIOD_DIFF(DATE_FORMAT(@currentEndDate,'%Y%m'),DATE_FORMAT(@purchaseDate,'%Y%m')) + 3 MONTH)";
                        List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                        parameters.Add(new MySqlParameter("@purchaseDate", purchaseDate));
                        parameters.Add(new MySqlParameter("@currentEndDate", currentEndDate));
                        DateTime dtResult = Convert.ToDateTime(Db.Master.GetDataRow(sqlString, parameters)[0]);
                        return new DateTime(dtResult.Year, dtResult.Month, dtResult.Day, 23, 59, 59);
                    }

                case (int)Subscription.SubscriptionTerm.SemiAnnually:
                    {
                        // This calculates SemiAnnually date very accurately 
                        // For example, if purchase is on the 31st day of month, it always extends to last day of month.
                        sqlString = "SELECT DATE_ADD(@purchaseDate, INTERVAL PERIOD_DIFF(DATE_FORMAT(@currentEndDate,'%Y%m'),DATE_FORMAT(@purchaseDate,'%Y%m')) + 6 MONTH)";
                        List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                        parameters.Add(new MySqlParameter("@purchaseDate", purchaseDate));
                        parameters.Add(new MySqlParameter("@currentEndDate", currentEndDate));
                        DateTime dtResult = Convert.ToDateTime(Db.Master.GetDataRow(sqlString, parameters)[0]);
                        return new DateTime(dtResult.Year, dtResult.Month, dtResult.Day, 23, 59, 59);
                    }

                default:
                    return DateTime.Now;
            }
        }


        #endregion
    }
}
