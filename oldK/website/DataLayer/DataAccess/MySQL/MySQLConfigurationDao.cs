///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLConfigurationDao: IConfigurationDao
    {
        public IList<WebOption> GetWebOptions(string group, int pageNumber, int pageSize, bool bIncludeNullValues)
        {
            string sql = "SELECT wo.option_group, wo.option_key, wo.role, wo.option_value, wot.option_desc, wot.default_value, wot.help_examples, wot.help_references, wot.help_see_also " +
                         "FROM web_options wo INNER JOIN web_option_templates wot ON wo.option_group=wot.option_group AND wo.option_key=wot.option_key WHERE 1";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (group != null)
            {
                sql = sql + " AND option_group=@group";
                parameters.Add(new MySqlParameter("@group", group));
            }

            if (!bIncludeNullValues)
                sql = sql + " AND NOT ISNULL(option_value)";

            string sortString = "option_group, option_key, role DESC";

            PagedDataTable pdt = Db.Master.GetPagedDataTable(sql, sortString, parameters, pageNumber, pageSize);

            PagedList<WebOption> list = new PagedList<WebOption>();
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
                list.Add(new WebOption(Convert.ToString(row["option_group"]), Convert.ToString(row["option_key"]), Convert.ToInt32(row["role"]), 
                    Convert.IsDBNull(row["option_value"]) ? null : Convert.ToString(row["option_value"]), 
                    Convert.ToString(row["option_desc"]), 
                    Convert.IsDBNull(row["default_value"])?null:Convert.ToString(row["default_value"]), 
                    Convert.IsDBNull(row["help_examples"])?null:Convert.ToString(row["help_examples"]), 
                    Convert.IsDBNull(row["help_references"])?null:Convert.ToString(row["help_references"]), 
                    Convert.IsDBNull(row["help_see_also"])?null:Convert.ToString(row["help_see_also"])));
            return list;
        }

        public IList<WebOptionTemplate> GetAvailableWebOptions(string group)
        {
            string sql = "SELECT option_group, option_key, option_desc, enabled, support_complex_values, default_value, help_examples, help_references, help_see_also FROM web_option_templates";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (group != null)
            {
                sql = sql + " WHERE option_group=@group";
                parameters.Add(new MySqlParameter("@group", group));
            }

            sql = sql + " GROUP BY option_group, option_key";

            DataTable dt = Db.Master.GetDataTable(sql, parameters);

            List<WebOptionTemplate> list = new List<WebOptionTemplate>();

            foreach (DataRow row in dt.Rows)
                list.Add(new WebOptionTemplate(
                    Convert.ToString(row["option_group"]), 
                    Convert.ToString(row["option_key"]), 
                    Convert.ToString(row["option_desc"]),
                    Convert.ToBoolean(row["enabled"]),
                    Convert.ToBoolean(row["support_complex_values"]),
                    Convert.IsDBNull(row["default_value"]) ? null : Convert.ToString(row["default_value"]),
                    Convert.IsDBNull(row["help_examples"]) ? null : Convert.ToString(row["help_examples"]),
                    Convert.IsDBNull(row["help_references"]) ? null : Convert.ToString(row["help_references"]),
                    Convert.IsDBNull(row["help_see_also"]) ? null : Convert.ToString(row["help_see_also"])));
            return list;
        }

        public IList<string> GetAvailableWebOptionGroups()
        {
            string sql = "SELECT DISTINCT option_group FROM web_option_templates ORDER BY option_group";

            DataTable dt = Db.Master.GetDataTable(sql);

            List<string> list = new List<string>();

            foreach (DataRow row in dt.Rows)
                list.Add(Convert.ToString(row["option_group"]));
            return list;
        }

        public string GetWebOptionValueByUserRole(string group, string key, int role)
        {
            string sql = "SELECT option_value FROM web_options " +
                         "WHERE option_group=@group AND option_key=@key AND (role=@role OR role=0) ORDER BY role DESC LIMIT 1";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@group", group));
            parameters.Add(new MySqlParameter("@key", key));
            parameters.Add(new MySqlParameter("@role", role));

            DataRow row = Db.Master.GetDataRow(sql, parameters);
            if (row != null)
                return Convert.IsDBNull(row["option_value"]) ? null : Convert.ToString(row["option_value"]);
            else
                return null;
        }

        public WebOption GetWebOption(string group, string key, int role)
        {
            string sql = "SELECT wo.option_group, wo.option_key, wo.role, wo.option_value, wot.option_desc, wot.default_value, wot.help_examples, wot.help_references, wot.help_see_also " + 
                         "FROM web_options wo INNER JOIN web_option_templates wot ON wo.option_group=wot.option_group AND wo.option_key=wot.option_KEY " +
                         "WHERE wo.option_group=@group AND wo.option_key=@key AND wo.role=@role";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@group", group));
            parameters.Add(new MySqlParameter("@key", key));
            parameters.Add(new MySqlParameter("@role", role));

            DataRow row = Db.Master.GetDataRow(sql, parameters);
            if (row != null)
                return new WebOption(Convert.ToString(row["option_group"]), Convert.ToString(row["option_key"]), Convert.ToInt32(row["role"]), 
                    Convert.IsDBNull(row["option_value"]) ? null : Convert.ToString(row["option_value"]), 
                    Convert.ToString(row["option_desc"]), 
                    Convert.IsDBNull(row["default_value"])?null:Convert.ToString(row["default_value"]), 
                    Convert.IsDBNull(row["help_examples"])?null:Convert.ToString(row["help_examples"]), 
                    Convert.IsDBNull(row["help_references"])?null:Convert.ToString(row["help_references"]), 
                    Convert.IsDBNull(row["help_see_also"])?null:Convert.ToString(row["help_see_also"]));
            else
                return null;
        }

        public void SetWebOption(string group, string key, int role, string value, int origRole)
        {
            if( origRole!=-1 && origRole!=role )
            {
                // Key changed, delete existing record
                DeleteWebOption(group, key, origRole);
            }

            string sql = "INSERT INTO web_options(option_group, option_key, role, option_value) VALUES ( @group, @key, @role, @value ) " +
                         "ON DUPLICATE KEY UPDATE role = @role, option_value = @value";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@group", group));
            parameters.Add(new MySqlParameter("@key", key));
            parameters.Add(new MySqlParameter("@role", role));
            parameters.Add(new MySqlParameter("@value", value));

            Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public void DeleteWebOptions()
        {
            DeleteWebOptions(null);
        }

        public void DeleteWebOptions(string group)
        {
            DeleteWebOptions(group, null);
        }

        public void DeleteWebOptions(string group, string key)
        {
            string sql = "DELETE FROM web_options " +
                         "WHERE 1";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (group != null)
            {
                sql = sql + " AND option_group=@group";
                parameters.Add(new MySqlParameter("@group", group));
            }
            if (key != null)
            {
                sql = sql + " AND option_key=@key";
                parameters.Add(new MySqlParameter("@key", key));
            }

            Db.Master.ExecuteNonQuery(sql, parameters);
        }
        
        public void DeleteWebOption(string group, string key, int role)
        {
            string sql = "DELETE FROM web_options " +
                         "WHERE option_group=@group AND option_key=@key AND role=@role";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(new MySqlParameter("@group", group));
            parameters.Add(new MySqlParameter("@key", key));
            parameters.Add(new MySqlParameter("@role", role));

            Db.Master.ExecuteNonQuery(sql, parameters);
        }
    }
}
