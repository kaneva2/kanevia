///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLErrorLoggingDao : IErrorLoggingDao
    {

        /// <summary>
        /// Insert an Error Log
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public int InsertErrorLog(int errorType, string description, DateTime errorTime, int userId, int errorCode)
        {
            // Send the message
            string sql = "INSERT INTO error_log (error_type_id, description, error_time, user_id, error_code, report_time) " +
                " VALUES (@errorType, @description, @errorTime, @userId, @errorCode, Now())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@errorType", errorType));
            parameters.Add(new MySqlParameter("@description", description));
            parameters.Add(new MySqlParameter("@errorTime", errorTime));
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@errorCode", errorCode));

            return Db.WOK.ExecuteNonQuery(sql, parameters);
        }
    }
}
