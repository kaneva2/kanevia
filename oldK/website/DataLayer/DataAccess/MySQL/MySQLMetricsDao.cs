///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;
using log4net;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLMetricsDao : IMetricsDao
    {
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Counter Data

        /// <summary>
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public int InsertMetricsCounterData_v0(    string counterName
                                                , double? a1
                                                , double? a2
                                                , double? b1
                                                , double? b2 )
        {
            // Send the message
            string sql =    "INSERT INTO metrics.counter_data ("
                                                                            + "guid, ndx, counter_guid, capture_dt"
                                                                            + ", formatted_value, a1, a2, b1, b2 ) "
                            + "SELECT "
                            +       "uuid()"
                            +       ", NULL"
                            +       ", counter_guid"
                            +       ", now()"
                            +       ", NULL"
                            +       ",@a1"
                            +       ",@a2"
                            +       ",@b1"
                            +       ",@b2 "
                            + "FROM "
                            +       "metrics.counters "
                            + "WHERE "
                            +       "counters.name = @counterName";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@counterName", counterName));
            parameters.Add(new MySqlParameter("@a1", a1));
            parameters.Add(new MySqlParameter("@a2", a2));
            parameters.Add(new MySqlParameter("@b1", b1));
            parameters.Add(new MySqlParameter("@b2", b2));

            return Db.Metrics.ExecuteNonQuery(sql, parameters);
        }

        public int InsertMetricsCounterData_v1( string      counterName
                                                , string    k1
                                                , string    k2
                                                , string    k3
                                                , string    k4
                                                , double?   a1
                                                , double?   a2
                                                , double?   b1
                                                , double?   b2  )               {

            string counterQuery = "SELECT counter_guid from counters where name = @name and enabled = 'Y'";
            List<IDbDataParameter> counterParams = new List<IDbDataParameter>();
            counterParams.Add(  new MySqlParameter("@name"
                                , counterName == null 
                                    ? "<novalue>" 
                                    : counterName ));
            DataRow counterRow = Db.Metrics.GetDataRow(counterQuery, counterParams);
            if (counterRow == null)
            {
                // missing or disabled counter.
                return 0;
            }

            // Insure that the keys are present.
            //
            string guidQuery = "SELECT guid from v1_counter_keys where k1=@k1 and k2=@k2 and k3=@k3 and k4=@k4";
            List<IDbDataParameter> keyParams = new List<IDbDataParameter>();
            keyParams.Add(new MySqlParameter("@k1", k1 == null ? "<novalue>" : k1));
            keyParams.Add(new MySqlParameter("@k2", k2 == null ? "<novalue>" : k2));
            keyParams.Add(new MySqlParameter("@k3", k3 == null ? "<novalue>" : k3));
            keyParams.Add(new MySqlParameter("@k4", k4 == null ? "<novalue>" : k4));

            DataRow row = Db.Metrics.GetDataRow(guidQuery, keyParams);
            if (row == null)            {
                // Key doesn't exist...add it.
                //
                string keyInsert = "insert into v1_counter_keys ( guid,ndx,k1,k2,k3,k4 ) values ( uuid(), null, @k1, @k2,@k3,@k4 )";
                List<IDbDataParameter> insertParams = new List<IDbDataParameter>();
                insertParams.Add(new MySqlParameter("@k1", k1 == null ? "<novalue>" : k1));
                insertParams.Add(new MySqlParameter("@k2", k2 == null ? "<novalue>" : k2));
                insertParams.Add(new MySqlParameter("@k3", k3 == null ? "<novalue>" : k3));
                insertParams.Add(new MySqlParameter("@k4", k4 == null ? "<novalue>" : k4));
                Db.Metrics.ExecuteNonQuery(keyInsert, insertParams);

                List<IDbDataParameter> retryParams = new List<IDbDataParameter>();
                retryParams.Add(new MySqlParameter("@k1", k1 == null ? "<novalue>" : k1));
                retryParams.Add(new MySqlParameter("@k2", k2 == null ? "<novalue>" : k2));
                retryParams.Add(new MySqlParameter("@k3", k3 == null ? "<novalue>" : k3));
                retryParams.Add(new MySqlParameter("@k4", k4 == null ? "<novalue>" : k4));
                row = Db.Metrics.GetDataRow(guidQuery, retryParams);
            }
            string keyGuid = row["guid"].ToString();


            // Send the message
            string sql = "INSERT INTO metrics.v1_counter_data ("
                                                                            + "guid, ndx, counter_guid, key_guid, capture_dt"
                                                                            + ", formatted_value, a1, a2, b1, b2 ) "
                            + "SELECT "
                            + "uuid()"
                            + ", NULL"
                            + ", counter_guid"
                            + ",@key_guid"
                            + ",now()"
                            + ",NULL"
                            + ",@a1"
                            + ",@a2"
                            + ",@b1"
                            + ",@b2 "
                            + "FROM "
                            + "metrics.counters "
                            + "WHERE "
                            + "counters.name = @counterName and counters.enabled='Y'";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@counterName", counterName));
            parameters.Add(new MySqlParameter("@key_guid", keyGuid));
            parameters.Add(new MySqlParameter("@a1", a1));
            parameters.Add(new MySqlParameter("@a2", a2));
            parameters.Add(new MySqlParameter("@b1", b1));
            parameters.Add(new MySqlParameter("@b2", b2));

            return Db.Metrics.ExecuteNonQuery(sql, parameters);
        }

        #endregion

        #region Website Performance

        public int InsertMetricsPageTiming(string pageName, double timeInMS)
        {
            // Send the message
            string sql = "INSERT INTO metrics.page_timing (page_name, time_in_ms) VALUES "
                            + "(@pageName, @timeInMS)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@pageName", pageName));
            parameters.Add(new MySqlParameter("@timeInMS", timeInMS));
            return Db.Metrics.ExecuteNonQuery(sql, parameters);
        }

        #endregion

        # region Client Performance

        public bool ClientRuntimeExists(string runtimeId)
        {
            string sql = "SELECT runtime_id FROM metrics.client_runtime_performance_log WHERE runtime_id = @runtimeId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));

            return Db.Master.GetScalar(sql, parameters) != null;
        }

        public int InsertClientMtbfLog(int userId, double totalRuntime, uint totalRuns, uint totalCrashes, string appVersion, string runtimeId)
        {
            string sql = "INSERT INTO metrics.client_mtbf_log (report_time, user_id, total_runtime_sec, total_runs, total_crashes, kep_client_version, runtime_id ) " +
                         "VALUES ( NOW(), @userId, @totalRuntime, @totalRuns, @totalCrashes, @appVersion, @runtimeId ) " +
                         "ON DUPLICATE KEY UPDATE total_runtime_sec = @totalRuntime, total_runs = @totalRuns, total_crashes = @totalCrashes";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@totalRuntime", totalRuntime));
            parameters.Add(new MySqlParameter("@totalRuns", totalRuns));
            parameters.Add(new MySqlParameter("@totalCrashes", totalCrashes));
            parameters.Add(new MySqlParameter("@appVersion", appVersion));
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));
            
            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public string GetClientRuntimeSystemId(string runtimeId)
        {
            string sql = " SELECT usc.system_id " +
                         " FROM metrics.client_runtime_performance_log crpl " +
                         " INNER JOIN kaneva.user_system_configurations usc ON crpl.system_configuration_id = usc.system_configuration_id " +
                         " WHERE crpl.runtime_id = @runtimeId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));

            DataRow row = Db.Master.GetDataRow(sql.ToString(), parameters);

            if (row == null || row["system_id"] == null)
            {
                return string.Empty;
            }

            return row["system_id"].ToString();
        }

        public string InsertClientRuntimePerformanceLog(string runtimeId, int userId, string systemConfigurationId, string appVersion, uint? testGroup, ulong? diskAvail, double? pingMs, uint? ripperDetected)
        {
            // Do a bit of cleanup before the insert
            if (!string.IsNullOrWhiteSpace(systemConfigurationId))
            {
                string sqlUpdate = "UPDATE metrics.client_runtime_performance_log " +
                                   "SET runtime_state = 'Aborted' " +
                                   "WHERE system_configuration_id = @systemConfigurationId AND runtime_state = 'Running' ";
                List<IDbDataParameter> updateParams = new List<IDbDataParameter>();
                updateParams.Add(new MySqlParameter("@systemConfigurationId", systemConfigurationId));
                Db.Master.ExecuteNonQuery(sqlUpdate, updateParams);
            }

            if (string.IsNullOrWhiteSpace(runtimeId))
                runtimeId = (Db.Master.GetScalar("SELECT uuid()") ?? string.Empty).ToString();

            string sql = "INSERT INTO metrics.client_runtime_performance_log (runtime_id, report_time, user_id, system_configuration_id, kep_client_version, test_group, initial_disk_space, initial_ping, ripper_detected) " +
                         "VALUES ( @runtimeId, NOW(), @userId, @systemConfigurationId, @appVersion, @testGroup, @diskAvail, @pingMs, @ripperDetected )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@systemConfigurationId", (string.IsNullOrWhiteSpace(systemConfigurationId) ? DBNull.Value : (object)systemConfigurationId)));
            parameters.Add(new MySqlParameter("@appVersion", appVersion));
            parameters.Add(new MySqlParameter("@testGroup", (testGroup == null ? DBNull.Value : (object)testGroup)));
            parameters.Add(new MySqlParameter("@diskAvail", (diskAvail == null ? DBNull.Value : (object)diskAvail)));
            parameters.Add(new MySqlParameter("@pingMs", (pingMs == null ? DBNull.Value : (object)pingMs)));
            parameters.Add(new MySqlParameter("@ripperDetected", (ripperDetected == null ? DBNull.Value : (object)ripperDetected)));

            // If insert fails, blank out runtimeId to indicate error
            runtimeId = (Db.Master.ExecuteNonQuery(sql, parameters) <= 0 ? string.Empty : runtimeId);

            return runtimeId;
        }

        public int InsertClientRuntimeIP(string runtimeId, string ipAddress)
        {
            if (string.IsNullOrWhiteSpace(ipAddress))
            {
                return 0;
            }

            string sql = "INSERT INTO metrics.client_runtime_ip_info (runtime_id, report_time, ip_address) VALUES (@runtimeId, NOW(), @ipAddress) " +
                         "ON DUPLICATE KEY UPDATE ip_address = @ipAddress, last_update = NULL ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));
            parameters.Add(new MySqlParameter("@ipAddress", ipAddress));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int UpdateClientRuntimePerformanceLog(string runtimeId, int userId, double? appCpuTime, ulong? appMemNum, ulong? appMemAvg, ulong? appMemMin, ulong? appMemMax,
            ulong? texMemNum, ulong? texMemAvg, ulong? texMemMin, ulong? texMemMax, double? fps, string shutDownType, uint? ripperDetected, double? msgMoveEnabledTimeSec)
        {
            string sql = "UPDATE metrics.client_runtime_performance_log " +
                         "SET user_id = @userId, " +
                         "total_cpu_time = @appCpuTime, " +
                         "total_mem_checks = @appMemNum, " +
                         "avg_mem_used = @appMemAvg, " +
                         "min_mem_used = @appMemMin, " +
                         "max_mem_used = @appMemMax, " +
                         "total_tex_mem_checks = @texMemNum, " +
                         "avg_tex_mem_avail = @texMemAvg, " +
                         "min_tex_mem_avail = @texMemMin, " +
                         "max_tex_mem_avail = @texMemMax, " +
                         "avg_fps = @fps, " +
                         "runtime_state = @shutDownType, " +
                         "ripper_detected = @ripperDetected, " +
                         "msg_move_enabled_time_sec = @msgMoveEnabledTimeSec " +
                         "WHERE runtime_id = @runtimeId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@appCpuTime", (appCpuTime == null ? DBNull.Value : (object)appCpuTime)));
            parameters.Add(new MySqlParameter("@appMemNum", (appMemNum == null ? DBNull.Value : (object)appMemNum)));
            parameters.Add(new MySqlParameter("@appMemAvg", (appMemAvg == null ? DBNull.Value : (object)appMemAvg)));
            parameters.Add(new MySqlParameter("@appMemMin", (appMemMin == null ? DBNull.Value : (object)appMemMin)));
            parameters.Add(new MySqlParameter("@appMemMax", (appMemMax == null ? DBNull.Value : (object)appMemMax)));
            parameters.Add(new MySqlParameter("@texMemNum", (texMemNum == null ? DBNull.Value : (object)texMemNum)));
            parameters.Add(new MySqlParameter("@texMemAvg", (texMemAvg == null ? DBNull.Value : (object)texMemAvg)));
            parameters.Add(new MySqlParameter("@texMemMin", (texMemMin == null ? DBNull.Value : (object)texMemMin)));
            parameters.Add(new MySqlParameter("@texMemMax", (texMemMax == null ? DBNull.Value : (object)texMemMax)));
            parameters.Add(new MySqlParameter("@fps", (fps == null ? DBNull.Value : (object)fps)));
            parameters.Add(new MySqlParameter("@shutDownType", (string.IsNullOrWhiteSpace(shutDownType) ? DBNull.Value : (object)shutDownType)));
            parameters.Add(new MySqlParameter("@ripperDetected", (ripperDetected == null ? DBNull.Value : (object)ripperDetected)));
            parameters.Add(new MySqlParameter("@msgMoveEnabledTimeSec", (msgMoveEnabledTimeSec == null ? DBNull.Value : (object)msgMoveEnabledTimeSec)));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int UpdateClientRuntimePerformanceLogUserId(string runtimeId, int userId)
        {
            int ret = 0;

            if (userId > 0)
            {
                string sqlUpdate = " UPDATE metrics.client_runtime_performance_log " +
                                    " SET user_id = @userId " +
                                    " WHERE runtime_id = @runtimeId ";

                List<IDbDataParameter> updateParameters = new List<IDbDataParameter>();
                updateParameters.Add(new MySqlParameter("@userId", userId));
                updateParameters.Add(new MySqlParameter("@runtimeId", runtimeId));

                ret = Db.Master.ExecuteNonQuery(sqlUpdate, updateParameters);
            }

            return ret;
        }

        public int InsertClientWebCallPerformanceLog(string runtimeId, string webCallType, uint webCalls, double webCallsMs, double webCallsResponseMs, uint webCallsBytes, uint webCallsErrored)
        {
            string sql = "INSERT INTO metrics.client_webcall_performance_log (runtime_id, webcall_type, report_time, total_calls_completed, avg_response_time, avg_web_response_time, total_response_size, total_calls_errored) " +
                         "VALUES ( @runtimeId, @webCallType, NOW(), @webCalls, @webCallsMs, @webCallsResponseMs, @webCallsBytes, @webCallsErrored )" +
                         "ON DUPLICATE KEY UPDATE total_calls_completed = @webCalls, avg_response_time = @webCallsMs, avg_web_response_time = @webCallsResponseMs, total_response_size = @webCallsBytes, total_calls_errored = @webCallsErrored ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));
            parameters.Add(new MySqlParameter("@webCallType", webCallType));
            parameters.Add(new MySqlParameter("@webCalls", webCalls));
            parameters.Add(new MySqlParameter("@webCallsMs", webCallsMs));
            parameters.Add(new MySqlParameter("@webCallsResponseMs", webCallsResponseMs));
            parameters.Add(new MySqlParameter("@webCallsBytes", webCallsBytes));
            parameters.Add(new MySqlParameter("@webCallsErrored", webCallsErrored));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int DeleteClientWebCallPerformanceLog(string runtimeId, string webCallType)
        {
            string sql = "DELETE FROM metrics.client_webcall_performance_log " +
                "WHERE runtime_id = @runtimeId " +
                "AND webcall_type = @webCallType ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));
            parameters.Add(new MySqlParameter("@webCallType", webCallType));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int InsertClientZonePerformanceLog(string runtimeId, uint gameId, uint zoneIndex, uint zoneInstanceId, uint zoneType, double? pingMs, double? fps,
            double? zoneTimeMs, double? avgDo, double? minDo, double? maxDo, double? avgPoly, double? minPoly, double? maxPoly, uint mediaCount, uint texMem)
        {
            string sql = "INSERT INTO metrics.client_zone_performance_log (runtime_id, report_time, game_id, zone_index, zone_instance_id, zone_type, ping, avg_fps, " +
                            " zone_time, avg_do, min_do, max_do, avg_poly, min_poly, max_poly, media_count, tex_mem) " +
                         "VALUES ( @runtimeId, NOW(), @gameId, @zoneIndex, @zoneInstanceId, @zoneType, @pingMs, @fps, " +
                            " @zoneTimeMs, @avgDo, @minDo, @maxDo, @avgPoly, @minPoly, @maxPoly, @mediaCount, @texMem) " +
                          "ON DUPLICATE KEY UPDATE ping = @pingMs, avg_fps = @fps, zone_time = @zoneTimeMs, avg_do = @avgDo, " +
                            " min_do = @minDo, max_do = @maxDo, avg_poly = @avgPoly, min_poly = @minPoly, max_poly = @maxPoly, " +
                            " media_count = @mediaCount, tex_mem = @texMem";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));
            parameters.Add(new MySqlParameter("@gameId", gameId));
            parameters.Add(new MySqlParameter("@zoneIndex", zoneIndex));
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));
            parameters.Add(new MySqlParameter("@pingMs", (pingMs == null ? DBNull.Value : (object)pingMs)));
            parameters.Add(new MySqlParameter("@fps", (fps == null ? DBNull.Value : (object)fps)));
            parameters.Add(new MySqlParameter("@zoneTimeMs", (zoneTimeMs == null ? DBNull.Value : (object)zoneTimeMs)));
            parameters.Add(new MySqlParameter("@avgDo", (avgDo == null ? DBNull.Value : (object)avgDo)));
            parameters.Add(new MySqlParameter("@minDo", (minDo == null ? DBNull.Value : (object)minDo)));
            parameters.Add(new MySqlParameter("@maxDo", (maxDo == null ? DBNull.Value : (object)maxDo)));
            parameters.Add(new MySqlParameter("@avgPoly", (avgPoly == null ? DBNull.Value : (object)avgPoly)));
            parameters.Add(new MySqlParameter("@minPoly", (minPoly == null ? DBNull.Value : (object)minPoly)));
            parameters.Add(new MySqlParameter("@maxPoly", (maxPoly == null ? DBNull.Value : (object)maxPoly)));
            parameters.Add(new MySqlParameter("@mediaCount", mediaCount));
            parameters.Add(new MySqlParameter("@texMem", texMem));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        #endregion

        #region Launcher Reports

        public bool LauncherRuntimeExists(ulong runtimeId)
        {
            string sql = "SELECT runtime_id FROM metrics.launcher_runtime_performance_log_v2 WHERE runtime_id = @runtimeId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));

            return Db.Master.GetScalar(sql, parameters) != null;
        }

        public int InsertLauncherMtbfLog(double totalRuntime, ushort totalRuns, ushort totalCrashes, ulong runtimeId)
        {
            string sql = "INSERT INTO metrics.launcher_mtbf_log_v2 (report_time, total_runtime_sec, total_runs, total_crashes, runtime_id ) " +
                         "VALUES ( NOW(), @totalRuntime, @totalRuns, @totalCrashes, @runtimeId ) " +
                         "ON DUPLICATE KEY UPDATE total_runtime_sec = @totalRuntime, total_runs = @totalRuns, total_crashes = @totalCrashes";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@totalRuntime", totalRuntime));
            parameters.Add(new MySqlParameter("@totalRuns", totalRuns));
            parameters.Add(new MySqlParameter("@totalCrashes", totalCrashes));
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public ulong SaveLauncherRuntimePerformanceLog(ulong runtimeId, string runtimeState, string systemConfigurationId, string appVersion, ulong? diskAvail, double? appCpuTime, uint? appMemNum,
            ulong? appMemAvg, ulong? appMemMin, ulong? appMemMax, ushort? patchRv)
        {
            // Make sure runtimeId is valid.
            runtimeId = GetTimestampPrimaryKey(runtimeId);
            if (runtimeId == 0)
            {
                return 0;
            }

            string sql = "INSERT INTO metrics.launcher_runtime_performance_log_v2 (runtime_id, runtime_state, report_time, system_configuration_id, app_version, initial_disk_space, " +
                         "  total_cpu_time, total_mem_checks, avg_mem_used, min_mem_used, max_mem_used, patch_rv) " +
                         "VALUES ( @runtimeId, @runtimeState, NOW(), @systemConfigurationId, @appVersion, @diskAvail, " +
                         "  @appCpuTime, @appMemNum, @appMemAvg, @appMemMin, @appMemMax, @patchRv) " +
                         "ON DUPLICATE KEY UPDATE runtime_state = @runtimeState," +
                         "  total_cpu_time = @appCpuTime, " +
                         "  total_mem_checks = @appMemNum, " +
                         "  avg_mem_used = @appMemAvg, " +
                         "  min_mem_used = @appMemMin, " +
                         "  max_mem_used = @appMemMax, " +
                         "  patch_rv = @patchRv ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));
            parameters.Add(new MySqlParameter("@runtimeState", (string.IsNullOrWhiteSpace(runtimeState) ? DBNull.Value : (object)runtimeState)));
            parameters.Add(new MySqlParameter("@systemConfigurationId", (string.IsNullOrWhiteSpace(systemConfigurationId) ? DBNull.Value : (object)systemConfigurationId)));
            parameters.Add(new MySqlParameter("@appVersion", appVersion));
            parameters.Add(new MySqlParameter("@diskAvail", (diskAvail == null ? DBNull.Value : (object)diskAvail)));
            parameters.Add(new MySqlParameter("@appCpuTime", (appCpuTime == null ? DBNull.Value : (object)appCpuTime)));
            parameters.Add(new MySqlParameter("@appMemNum", (appMemNum == null ? DBNull.Value : (object)appMemNum)));
            parameters.Add(new MySqlParameter("@appMemAvg", (appMemAvg == null ? DBNull.Value : (object)appMemAvg)));
            parameters.Add(new MySqlParameter("@appMemMin", (appMemMin == null ? DBNull.Value : (object)appMemMin)));
            parameters.Add(new MySqlParameter("@appMemMax", (appMemMax == null ? DBNull.Value : (object)appMemMax)));
            parameters.Add(new MySqlParameter("@patchRv", (patchRv == null ? DBNull.Value : (object)patchRv)));

            if (Db.Master.ExecuteNonQuery(sql, parameters) < 1)
            {
                return 0;
            }

            return runtimeId;
        }

        public int InsertLauncherRuntimeIP(ulong runtimeId, string ipAddress)
        {
            if (string.IsNullOrWhiteSpace(ipAddress))
            {
                return 0;
            }

            string sql = "INSERT INTO metrics.launcher_runtime_ip_info (runtime_id, report_time, ip_address) VALUES (@runtimeId, NOW(), @ipAddress) " +
                         "ON DUPLICATE KEY UPDATE ip_address = @ipAddress, last_update = NULL ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));
            parameters.Add(new MySqlParameter("@ipAddress", ipAddress));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int DeleteLauncherRuntimeIP(ulong runtimeId)
        {
            string sql = "DELETE FROM metrics.launcher_runtime_ip_info WHERE runtime_id = @runtimeId";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int InsertLauncherPatchPerformanceLog(ulong runtimeId, string patchUrl, double? patchMs, ushort dl_NumOk, ushort dl_NumErr, double dl_Ms, uint dl_Bytes,
          ushort dl_Waits, ushort dl_Retries, ushort dl_RetriesErr,
          ushort zip_NumOk, ushort zip_NumErr, double zip_Ms, uint zip_Bytes,
          ushort pak_NumOk, ushort pak_NumErr, double pak_Ms, uint pak_Bytes)
        {
            string sql = "INSERT INTO metrics.launcher_patch_performance_log_v2 (runtime_id, report_time, patch_url, patch_ms, dl_num_ok, dl_num_err, dl_ms, dl_bytes, " +
                         "  dl_waits, dl_retries, dl_retries_err, " +
                         "  zip_num_ok, zip_num_err, zip_ms, zip_bytes, pak_num_ok, pak_num_err, pak_ms, pak_bytes) " +
                         "VALUES ( @runtimeId, NOW(), @patchUrl, @patchMs, @dl_NumOk, @dl_NumErr, @dl_Ms, @dl_Bytes, " +
                         "  @dl_Waits, @dl_Retries, @dl_RetriesErr, " +
                         "  @zip_NumOk, @zip_NumErr, @zip_Ms, @zip_Bytes, @pak_NumOk, @pak_NumErr, @pak_Ms, @pak_Bytes) " +
                         "ON DUPLICATE KEY UPDATE patch_url = @patchUrl, " +
                         "  patch_ms = @patchMs, " +
                         "  dl_num_ok = @dl_NumOk, " +
                         "  dl_num_err = @dl_NumErr, " +
                         "  dl_ms = @dl_Ms, " +
                         "  dl_bytes = @dl_Bytes, " +
                         "  dl_waits = @dl_Waits, " +
                         "  dl_retries = @dl_Retries, " +
                         "  dl_retries_err = @dl_RetriesErr, " +
                         "  zip_num_ok = @zip_NumOk, " +
                         "  zip_num_err = @zip_NumErr, " +
                         "  zip_ms = @zip_Ms, " +
                         "  zip_bytes = @zip_Bytes, " +
                         "  pak_num_ok = @pak_NumOk, " +
                         "  pak_num_err = @pak_NumErr, " +
                         "  pak_ms = @pak_Ms, " +
                         "  pak_bytes = @pak_Bytes ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));
            parameters.Add(new MySqlParameter("@patchUrl", patchUrl));
            parameters.Add(new MySqlParameter("@patchMs", (patchMs == null ? DBNull.Value : (object)patchMs)));
            parameters.Add(new MySqlParameter("@dl_NumOk", dl_NumOk));
            parameters.Add(new MySqlParameter("@dl_NumErr", dl_NumErr));
            parameters.Add(new MySqlParameter("@dl_Ms", dl_Ms));
            parameters.Add(new MySqlParameter("@dl_Bytes", dl_Bytes));
            parameters.Add(new MySqlParameter("@dl_Waits", dl_Waits));
            parameters.Add(new MySqlParameter("@dl_Retries", dl_Retries));
            parameters.Add(new MySqlParameter("@dl_RetriesErr", dl_RetriesErr));
            parameters.Add(new MySqlParameter("@zip_NumOk", zip_NumOk));
            parameters.Add(new MySqlParameter("@zip_NumErr", zip_NumErr));
            parameters.Add(new MySqlParameter("@zip_Ms", zip_Ms));
            parameters.Add(new MySqlParameter("@zip_Bytes", zip_Bytes));
            parameters.Add(new MySqlParameter("@pak_NumOk", pak_NumOk));
            parameters.Add(new MySqlParameter("@pak_NumErr", pak_NumErr));
            parameters.Add(new MySqlParameter("@pak_Ms", pak_Ms));
            parameters.Add(new MySqlParameter("@pak_Bytes", pak_Bytes));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int DeleteLauncherPatchPerformanceLog(ulong runtimeId)
        {
            string sql = "DELETE FROM metrics.launcher_patch_performance_log_v2 WHERE runtime_id = @runtimeId";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int InsertLauncherWebCallPerformanceLog(ulong runtimeId, string webCallType, uint webCalls, double webCallsMs, double webCallsResponseMs, uint webCallsBytes, uint webCallsErrored)
        {
            string sql = "INSERT INTO metrics.launcher_webcall_performance_log_v2 (runtime_id, webcall_type, report_time, total_calls_completed, avg_response_time, avg_web_response_time, total_response_size, total_calls_errored) " +
                         "VALUES ( @runtimeId, @webCallType, NOW(), @webCalls, @webCallsMs, @webCallsResponseMs, @webCallsBytes, @webCallsErrored )" +
                         "ON DUPLICATE KEY UPDATE total_calls_completed = @webCalls, avg_response_time = @webCallsMs, avg_web_response_time = @webCallsResponseMs, total_response_size = @webCallsBytes, total_calls_errored = @webCallsErrored ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));
            parameters.Add(new MySqlParameter("@webCallType", webCallType));
            parameters.Add(new MySqlParameter("@webCalls", webCalls));
            parameters.Add(new MySqlParameter("@webCallsMs", webCallsMs));
            parameters.Add(new MySqlParameter("@webCallsResponseMs", webCallsResponseMs));
            parameters.Add(new MySqlParameter("@webCallsBytes", webCallsBytes));
            parameters.Add(new MySqlParameter("@webCallsErrored", webCallsErrored));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int DeleteLauncherWebCallPerformanceLog(ulong runtimeId)
        {
            string sql = "DELETE FROM metrics.launcher_webcall_performance_log_v2 WHERE runtime_id = @runtimeId";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@runtimeId", runtimeId));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        #endregion

        #region Client Version History

        public int InsertClientVersionHistory(string clientVersion, DateTime releaseDate, DateTime deprecatedDate, string comment)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sql = "INSERT INTO metrics.client_version_history (kep_client_version, release_date, deprecated_date, comment) " +
                         "VALUES ( @clientVersion, @releaseDate, @deprecatedDate, @comment )";

            parameters.Add(new MySqlParameter("@clientVersion", clientVersion));
            parameters.Add(new MySqlParameter("@releaseDate", releaseDate));
            
            // Optional deprecation date
            if (deprecatedDate != null && !deprecatedDate.Equals(DateTime.MaxValue))
            {
                parameters.Add(new MySqlParameter("@deprecatedDate", deprecatedDate));
            }
            else
            {
                parameters.Add(new MySqlParameter("@deprecatedDate", DBNull.Value));
            }

            // Optional comment
            if (!string.IsNullOrWhiteSpace(comment))
            {
                parameters.Add(new MySqlParameter("@comment", comment));
            }
            else
            {
                parameters.Add(new MySqlParameter("@comment", DBNull.Value));
            }

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int UpdateClientVersionHistory(string clientVersion, DateTime releaseDate, DateTime deprecatedDate, string comment)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sql = "UPDATE metrics.client_version_history " +
                         "SET release_date = @releaseDate, deprecated_date = @deprecatedDate, comment = @comment " +
                         "WHERE kep_client_version = @clientVersion";

            parameters.Add(new MySqlParameter("@clientVersion", clientVersion));
            parameters.Add(new MySqlParameter("@releaseDate", releaseDate));

            // Optional deprecation date
            if (deprecatedDate != null && !deprecatedDate.Equals(DateTime.MaxValue))
            {
                parameters.Add(new MySqlParameter("@deprecatedDate", deprecatedDate));
            }
            else
            {
                parameters.Add(new MySqlParameter("@deprecatedDate", DBNull.Value));
            }

            // Optional comment
            if (!string.IsNullOrWhiteSpace(comment))
            {
                parameters.Add(new MySqlParameter("@comment", comment));
            }
            else
            {
                parameters.Add(new MySqlParameter("@comment", DBNull.Value));
            }

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int DeleteClientVersionHistory(string clientVersion)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sql = "DELETE FROM metrics.client_version_history " +
                         "WHERE kep_client_version = @clientVersion";

            parameters.Add(new MySqlParameter("@clientVersion", clientVersion));
            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public PagedDataTable GetClientVersionHistory(int page, int itemsPerPage, string clientVersion = "")
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sql = "SELECT kep_client_version, release_date, deprecated_date, comment " +
                         "FROM metrics.client_version_history ";

            if (!string.IsNullOrWhiteSpace(clientVersion))
            {
                sql += "WHERE kep_client_version = @clientVersion ";
                parameters.Add(new MySqlParameter("@clientVersion", clientVersion));
            }

            return Db.Master.GetPagedDataTable(sql, "release_date ASC", parameters, page, itemsPerPage);
        }

        #endregion

        #region Game Item Metrics

        public int InsertScriptGameItemPlacementLog(int zoneInstanceId, int zoneType, string username, string gameplayMode, int gameItemId, int gameItemGlid, string itemType, string behavior)
        {
            string sql = "INSERT INTO metrics.sgitem_placement_log (uuid, report_time, zone_instance_id, zone_type, username, gameplay_mode, game_item_id, game_item_glid, item_type, behavior) " +
                         "VALUES ( UUID(), NOW(), @zoneInstanceId, @zoneType, @username, @gameplayMode, @gameItemId, @gameItemGlid, @itemType, @behavior )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));
            parameters.Add(new MySqlParameter("@username", username));
            parameters.Add(new MySqlParameter("@gameplayMode", gameplayMode));
            parameters.Add(new MySqlParameter("@gameItemId", gameItemId));
            parameters.Add(new MySqlParameter("@gameItemGlid", (gameItemGlid <= 0 ? DBNull.Value : (object)gameItemGlid)));
            parameters.Add(new MySqlParameter("@itemType", itemType));
            parameters.Add(new MySqlParameter("@behavior", (behavior == null ? DBNull.Value : (object)behavior)));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int InsertScriptGameItemLootingLog(int zoneInstanceId, int zoneType, string username, string lootSource, int gameItemId, int gameItemGlid, string itemType, string behavior)
        {
            string sql = "INSERT INTO metrics.sgitem_loot_log (uuid, report_time, zone_instance_id, zone_type, username, loot_source, game_item_id, game_item_glid, item_type, behavior) " +
                         "VALUES ( UUID(), NOW(), @zoneInstanceId, @zoneType, @username, @lootSource, @gameItemId, @gameItemGlid, @itemType, @behavior )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));
            parameters.Add(new MySqlParameter("@username", username));
            parameters.Add(new MySqlParameter("@lootSource", (lootSource == null ? DBNull.Value : (object)lootSource)));
            parameters.Add(new MySqlParameter("@gameItemId", gameItemId));
            parameters.Add(new MySqlParameter("@gameItemGlid", (gameItemGlid <= 0 ? DBNull.Value : (object)gameItemGlid)));
            parameters.Add(new MySqlParameter("@itemType", itemType));
            parameters.Add(new MySqlParameter("@behavior", (behavior == null ? DBNull.Value : (object)behavior)));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int InsertFrameworkActivationLog(int zoneInstanceId, int zoneType, string username, string activationAction)
        {
            string sql = "INSERT INTO metrics.sgitem_framework_activation_log (uuid, report_time, zone_instance_id, zone_type, username, activation_action) " +
                         "VALUES ( UUID(), NOW(), @zoneInstanceId, @zoneType, @username, @activationAction )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));
            parameters.Add(new MySqlParameter("@username", username));
            parameters.Add(new MySqlParameter("@activationAction", activationAction));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        #endregion

        #region Misc

        /// <summary>
        /// Helper method to generate a unsigned integer primary key based upon the current timestamp
        /// with a random number suffix to alleviate collisions.
        /// </summary>
        /// <returns>The passed value of primaryKey if it's valid (non-zero), otherwise a newly generated key.</returns>
        private ulong GetTimestampPrimaryKey(ulong primaryKey)
        {
            if (primaryKey == 0)
            {
                object objPrimaryKey = Db.Master.GetScalar("SELECT metrics.generate_timestamp_pk();");
                primaryKey = (objPrimaryKey == null ? 0 : Convert.ToUInt64(objPrimaryKey));
            }

            return primaryKey;
        }

        /// <summary>
        /// Records new user progression through key points in the Kaneva user experience.
        /// </summary>
        /// <param name="userId">user_id of the user to record.</param>
        /// <param name="funnelStep">Column name of the step in metrics.new_user_funnel to update.</param>
        public int RecordNewUserFunnelProgression(int userId, string funnelStep)
        {
            funnelStep = (funnelStep ?? string.Empty).ToLower();

            List<string> stepColumns = new List<string> 
            { 
                "web_signup", "kim_gamelogin", "kim_validateuser", "entered_avatar_select", "completed_avatar_select", "entered_home", "completed_tutorial", "entered_game_world" 
            };

            if (string.IsNullOrWhiteSpace(funnelStep) || !stepColumns.Contains(funnelStep))
            {
                throw new ArgumentException("funnelStep is invalid.");
            }

            string sql = "INSERT INTO metrics.new_user_funnel_progression (user_id, @funnelStep) " +
                         "VALUES ( @userId, NOW() ) " +
                         "ON DUPLICATE KEY UPDATE @funnelStep = IF (@funnelStep IS NULL, NOW(), @funnelStep)";

            sql = sql.Replace("@funnelStep", funnelStep);

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int ResetNewUserFunnelProgression(int userId)
        {
            string sql = "DELETE FROM metrics.new_user_funnel_progression " +
                         "WHERE user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        #endregion
    }
}

