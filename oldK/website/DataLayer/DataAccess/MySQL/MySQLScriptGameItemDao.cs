///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using Devart.Data.MySql;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;
using System.Text;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLScriptGameItemDao : IScriptGameItemDao
    {
        #region Private Helpers

        /// <summary>
        /// GetScriptGameItems
        /// </summary>
        private PagedList<ScriptGameItem> GetSnapshotScriptGameItems(List<string> joins, List<string> whereClauses, List<IDbDataParameter> parameters,
            string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false)
        {
            PagedList<ScriptGameItem> scriptGameItems = new PagedList<ScriptGameItem>();
            ILookup<int, ScriptGameItem> gameItemLookup = null;

            // Starting game item names override snapshot names.
            string gameItemNameColumn;
            int templateGlid = 0;

            if (joins.Where(j => j.Contains("starting_script_game_items")).Count() > 0)
            {
                gameItemNameColumn = "ssgi.name, ssgi.game_item_id";

                IDbDataParameter templateGlidParam = parameters.Where(p => p.ParameterName.Equals("@templateGlid")).SingleOrDefault();
                templateGlid = (templateGlidParam == null ? 0 : Convert.ToInt32(templateGlidParam.Value));
            }
            else
            {
                gameItemNameColumn = "sgi.name, null as game_item_id";
            }

            //create query string to get game by user id
            string sql = "SELECT DISTINCT sgi.game_item_glid, sgi.glid, " + gameItemNameColumn + ", sgi.item_type, sgi.level, sgi.rarity, iw.global_id AS shop_global_id, dids.game_item_id AS default_game_item_id, ip.VALUE AS bundle_glid,  " +
                         "dids.automatic_insertion_start, dids.automatic_insertion_order, sgit.inventory_compatible " +
                         "FROM snapshot_script_game_items sgi " +
                         "INNER JOIN script_game_item_types sgit ON sgi.item_type = sgit.item_type " + 
                         string.Join(" ", joins.ToArray()) + " " +
                         "LEFT OUTER JOIN shopping.items_web iw ON sgi.game_item_glid = iw.global_id AND iw.item_active = 1 " +
                         "LEFT OUTER JOIN default_script_game_items dids ON sgi.game_item_glid = dids.game_item_glid " +
                         "LEFT OUTER JOIN item_parameters ip ON sgi.game_item_glid = ip.global_id AND ip.param_type_id = 34 " +
                         "WHERE 1=1 " +
                         string.Join(" ", whereClauses.ToArray());

            //perform query returning single data row from the wok database
            PagedDataTable pdt = Db.WOK.GetPagedDataTable(sql, orderBy, parameters, pageNumber, pageSize);

            if (pdt == null)
                return scriptGameItems;

            foreach (DataRow row in pdt.Rows)
            {
                ScriptGameItem scriptGameItem = new ScriptGameItem();
                scriptGameItem.GIGlid = Convert.ToInt32(row["game_item_glid"]);
                scriptGameItem.GIId = (row["default_game_item_id"] == DBNull.Value ? 0 : Convert.ToInt32(row["default_game_item_id"]));

                if (row["game_item_id"] != null && row["game_item_id"] != DBNull.Value)
                {
                    scriptGameItem.GIId = Convert.ToInt32(row["game_item_id"]);
                }

                scriptGameItem.Glid = (row["glid"] == DBNull.Value ? 0 : Convert.ToInt32(row["glid"]));
                scriptGameItem.Name = row["name"].ToString();
                scriptGameItem.ItemType = row["item_type"].ToString();
                scriptGameItem.Level = Convert.ToInt32(row["level"]);
                scriptGameItem.Rarity = row["rarity"].ToString();
                scriptGameItem.ExistsOnShop = (row["shop_global_id"] != DBNull.Value);
                scriptGameItem.BundleGlid = (row["bundle_glid"] == DBNull.Value ? 0 : Convert.ToInt32(row["bundle_glid"]));
                scriptGameItem.AutomaticInsertionStart = (row["automatic_insertion_start"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(row["automatic_insertion_start"]));
                scriptGameItem.AutomaticInsertionOrder = (row["automatic_insertion_order"] == DBNull.Value ? null : (int?)Convert.ToInt32(row["automatic_insertion_order"]));
                scriptGameItem.InventoryCompatible = (row["inventory_compatible"].ToString() == "T");

                scriptGameItems.Add(scriptGameItem);
            }

            // Pull and include the game item properties if necessary
            if (includeProperties && scriptGameItems.Count > 0)
            {
                sql = "SELECT game_item_glid, property_name, property_value " +
                      "FROM snapshot_script_game_item_properties " +
                      "WHERE game_item_glid IN (" + string.Join(",", scriptGameItems.Select(i => i.GIGlid)) + ") " +
                      "ORDER BY game_item_glid";

                DataTable dtProp = Db.WOK.GetDataTable(sql.ToString());
                if (dtProp != null && dtProp.Rows.Count > 0)
                {
                    // Create lookup
                    gameItemLookup = (gameItemLookup ?? scriptGameItems.ToLookup(i => i.GIGlid));

                    foreach (DataRow rowProp in dtProp.Rows)
                    {
                        foreach (ScriptGameItem gameItem in gameItemLookup[Convert.ToInt32(rowProp["game_item_glid"])])
                        {
                            gameItem.Properties.Add(rowProp["property_name"].ToString(), rowProp["property_value"].ToString());
                        }
                    }
                }
            }

            // Pull and include bundled glids if necessary
            if (includeBundledGlids && scriptGameItems.Count > 0)
            {
                sql = "SELECT sgi.game_item_glid, bi.item_global_id " +
                      "FROM snapshot_script_game_items sgi " +
                      "INNER JOIN item_parameters ip ON sgi.game_item_glid = ip.global_id AND ip.param_type_id = 34 " +
                      "INNER JOIN bundle_items bi ON CAST(ip.VALUE AS UNSIGNED) = bi.bundle_global_id " +
                      "WHERE sgi.game_item_glid IN (" + string.Join(",", scriptGameItems.Select(i => i.GIGlid)) + ") ";

                DataTable dtBundleItems = Db.WOK.GetDataTable(sql.ToString());
                if (dtBundleItems != null && dtBundleItems.Rows.Count > 0)
                {
                    // Create lookup
                    gameItemLookup = (gameItemLookup ?? scriptGameItems.ToLookup(i => i.GIGlid));

                    foreach (DataRow rowProp in dtBundleItems.Rows)
                    {
                        foreach (ScriptGameItem gameItem in gameItemLookup[Convert.ToInt32(rowProp["game_item_glid"])])
                        {
                            gameItem.BundledGlids.Add(Convert.ToInt32(rowProp["item_global_id"]));
                        }
                    }
                }
            }

            // Pull and include placement info if necessary
            if (templateGlid > 0 && scriptGameItems.Count > 0)
            {
                sql = "SELECT do.id, dop.value, st.game_item_glid " +
                      "FROM starting_dynamic_objects do " +
                      "INNER JOIN starting_dynamic_object_parameters dop ON do.id = dop.id " +
                      "    AND dop.param_type_id = " + ItemParameter.PARAM_TYPE_GAME_ITEM_ID + " " +
                      "INNER JOIN starting_script_game_items st ON dop.value = st.game_item_id " +
                      "    AND st.template_glid = do.apartment_template_id " +
                      "WHERE do.apartment_template_id = " + templateGlid + " " +
                      "AND dop.value IN (" + string.Join(",", scriptGameItems.Select(i => i.GIId)) + ")";

                DataTable dtProp = Db.WOK.GetDataTable(sql.ToString());
                if (dtProp != null && dtProp.Rows.Count > 0)
                {
                    // Create lookup
                    gameItemLookup = (gameItemLookup ?? scriptGameItems.ToLookup(i => i.GIGlid));

                    foreach (DataRow rowProp in dtProp.Rows)
                    {
                        foreach (ScriptGameItem gameItem in gameItemLookup[Convert.ToInt32(rowProp["game_item_glid"])])
                        {
                            gameItem.NumPlacedInTemplate++;
                        }
                    }
                }
            }

            // Make sure total count makes sense
            scriptGameItems.TotalCount = pdt.TotalCount;

            return scriptGameItems;
        }

        /// <summary>
        /// Called to save a snapshot of a ScriptGameItem.  Automatically passes on any query exceptions.
        /// </summary>
        private bool InsertSnapshotScriptGameItem(ScriptGameItem sgItem)
        {
            bool success = false;

            using (TransactionDecorator transaction = new TransactionDecorator())
            {
                // Insert game item snapshot
                string sqlString = "INSERT INTO snapshot_script_game_items (game_item_glid, glid, name, item_type, level, rarity) " +
                                    "VALUES (@gameItemGlid, @glid, @name, @itemType, @level, @rarity)";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@gameItemGlid", sgItem.GIGlid));
                parameters.Add(new MySqlParameter("@glid", (sgItem.Glid > 0 ? (object)sgItem.Glid : DBNull.Value)));
                parameters.Add(new MySqlParameter("@name", sgItem.Name));
                parameters.Add(new MySqlParameter("@itemType", sgItem.ItemType));
                parameters.Add(new MySqlParameter("@level", sgItem.Level));
                parameters.Add(new MySqlParameter("@rarity", sgItem.Rarity));
                Db.WOK.ExecuteNonQuery(sqlString, parameters, true);

                // Insert game item properties
                string propValue = string.Empty;
                foreach (KeyValuePair<string, string> property in sgItem.Properties)
                {
                    sqlString = "INSERT INTO snapshot_script_game_item_properties (game_item_glid, property_name, property_value) " +
                                        "VALUES (@gameItemGlid, @propertyName, @propertyValue)";

                    parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@gameItemGlid", sgItem.GIGlid));
                    parameters.Add(new MySqlParameter("@propertyName", property.Key));
                    parameters.Add(new MySqlParameter("@propertyValue", property.Value));
                    Db.WOK.ExecuteNonQuery(sqlString, parameters, true);
                }

                transaction.Complete();
                success = true;
            }

            return success;
        }

        /// <summary>
        /// UpdateDefaultScriptGameItem
        /// </summary>
        private bool UpdateDefaultScriptGameItem(ScriptGameItem sgItem)
        {
            bool success = false;

            using (TransactionDecorator transaction = new TransactionDecorator())
            {
                // Insert default script game item record
                string sqlString = "INSERT INTO default_script_game_items (game_item_id, game_item_glid, automatic_insertion_start, automatic_insertion_order) " +
                                    "VALUES (@gameItemId, @gameItemGlid, @automaticInsertionStart, @automaticInsertionOrder) " +
                                    "ON DUPLICATE KEY UPDATE automatic_insertion_start = @automaticInsertionStart, automatic_insertion_order = @automaticInsertionOrder ";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@gameItemId", sgItem.GIId));
                parameters.Add(new MySqlParameter("@gameItemGlid", sgItem.GIGlid));
                parameters.Add(new MySqlParameter("@automaticInsertionStart", (sgItem.InsertAutomatically ? (object)sgItem.AutomaticInsertionStart : DBNull.Value)));
                parameters.Add(new MySqlParameter("@automaticInsertionOrder", (sgItem.InsertAutomatically && sgItem.AutomaticInsertionOrder != null ? (object)sgItem.AutomaticInsertionOrder : DBNull.Value)));
                Db.WOK.ExecuteNonQuery(sqlString, parameters, true);

                // Add it to all templates that already have game items if automatic_insertion_start
                if (sgItem.InsertAutomatically)
                {
                    sqlString = "INSERT INTO starting_script_game_items (template_glid, game_item_id, game_item_glid, name) " +
                                "SELECT sgi.template_glid, @gameItemId, @gameItemGlid, @name " +
                                "FROM world_templates t " +
                                "INNER JOIN world_template_global_id tg ON t.template_id = tg.template_id " +
                                "INNER JOIN starting_script_game_custom_data sgi ON sgi.template_glid = tg.global_id AND sgi.attribute = 'WorldSettings' AND sgi.value LIKE '%\"frameworkEnabled\":true%' " +
                                "WHERE t.incubator_hosted = 'N' " +
                                "ON DUPLICATE KEY UPDATE game_item_glid = @gameItemGlid ";
                }
                else // If not automatic insertion, remove it from the templates
                {
                    sqlString = "DELETE FROM starting_script_game_items " +
                                "WHERE game_item_glid = @gameItemGlid ";
                }

                parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@gameItemId", sgItem.GIId));
                parameters.Add(new MySqlParameter("@gameItemGlid", sgItem.GIGlid));
                parameters.Add(new MySqlParameter("@name", sgItem.Name));
                Db.WOK.ExecuteNonQuery(sqlString, parameters, true);

                transaction.Complete();
                success = true;
            }

            return success;
        }

        /// <summary>
        /// AddGameItemBundledGlids
        /// </summary>
        private bool AddGameItemBundledGlids(int bundleGlid, List<int> bundledGlids, int userId, int gameItemGlid = 0, string gameItemName = null, bool newBundle = false)
        {
            bool success = false;
            MySQLShoppingDao shoppingDao = new MySQLShoppingDao();

            using (TransactionDecorator transaction = new TransactionDecorator())
            {
                // Add the bundles of associated WOKItems
                if (bundledGlids.Count > 0)
                {
                    if (newBundle)
                    {
                        // Add a new bundle if necessary
                        WOKItem bundle = new WOKItem();
                        bundle.GlobalId = bundleGlid;
                        bundle.Name = string.Format("{0} Bundled Items", gameItemName);
                        bundle.Description = string.Format("{0} Bundled Items", gameItemName);
                        bundle.UseType = WOKItem.USE_TYPE_BUNDLE;
                        bundle.ItemCreatorId = (uint)userId;
                        bundle.IsDerivable = 0;
                        bundle.DerivationLevel = -1;
                        bundle.PassTypeId = 0;

                        // Bundles need items_web records, but don't need to be listed on shop. Default them to deleted.
                        bundle.ItemActive = 0;

                        shoppingDao.AddCustomItem(bundle, false, true, 0, true);

                        shoppingDao.UpdateItemParameter(gameItemGlid, ItemParameter.PARAM_TYPE_SCRIPT_GAME_ITEM_BUNDLE_GLID, bundle.GlobalId.ToString(), true);
                    }

                    foreach (int bundleItemGlid in bundledGlids)
                    {
                        shoppingDao.AddItemToBundle(bundleGlid, bundleItemGlid, 1, true);
                    }
                }

                transaction.Complete();
                success = true;
            }

            return success;
        }

        #endregion

        #region Create

        private ScriptGameItemPrice GetScriptGameItemPrice(WOKItem gameItem, IEnumerable<int> bundleGlids, Dictionary<int, WOKItem> bundledItems)
        {
            WOKItem bundle = new WOKItem();
            bundle.UseType = WOKItem.USE_TYPE_BUNDLE;
            bundle.BundleItems = new List<WOKItem>();

            WOKItem iOut;
            foreach (int glid in bundleGlids)
            {
                if (bundledItems.TryGetValue(glid, out iOut))
                {
                    bundle.BundleItems.Add(iOut);
                }
            }

            return new ScriptGameItemPrice(gameItem, bundle);
        }

        /// <summary>
        /// SnapshotScriptGameItems
        /// </summary>
        /// <returns>True if the game items were successfully added.</returns>
        public bool InsertSnapshotScriptGameItems(ref List<ScriptGameItem> sgItems, int userId, bool addToInventory = false, bool itemsAreDefault = false)
        {
            bool result = false;
            MySQLUserDao userDao = new MySQLUserDao();
            MySQLShoppingDao shoppingDao = new MySQLShoppingDao();
            Dictionary<int, WOKItem> glidItemsByGIGLID = new Dictionary<int, WOKItem>();
            Dictionary<int, WOKItem> glidItemsByGIID = new Dictionary<int, WOKItem>();

            if (sgItems == null || sgItems.Count == 0)
                return result;

            // If there are any invalid game items in this collection, go ahead and return an error
            if (itemsAreDefault && sgItems.Count(i => i.GIGlid == 0 || i.GIId == 0) > 0)
                return result;

            // List of bundled glids to add to the inventory
            List<int> bundledItemGlids = sgItems.SelectMany(i => i.BundledGlids).Distinct().ToList();

            // List of bundled items for calculating price
            Dictionary<int, WOKItem> bundledItems = (bundledItemGlids.Count > 0 ? 
                shoppingDao.GetItems(bundledItemGlids, false, "").ToDictionary(i => i.GlobalId) : new Dictionary<int, WOKItem>());

            // Number of bundle glids to allocate
            int bundleGlidCount = sgItems.Count(i => i.BundledGlids.Count > 0);

            Stack<int> sgGlids = new Stack<int>();
            Stack<int> bundleGlids = new Stack<int>();

            // Pre-allocate a block of globalIds for game items and bundles.
            // Per discussion with D.Hicks, this is done outside of the transaction to prevent deadlocks.
            try
            {
                if (itemsAreDefault)
                {
                    for (int i = 0; i < sgItems.Count; i++)
                    {
                        sgGlids.Push(sgItems[i].GIGlid);

                        // Get the wokitem associated with the "glid" field of the game item
                        if (sgItems[i].Glid > 0)
                        {
                            glidItemsByGIGLID.Add(sgItems[i].GIGlid, shoppingDao.GetItem(sgItems[i].Glid, false));
                        }
                    }

                    for (int i = 0; i < bundleGlidCount; i++)
                    {
                        bundleGlids.Push(sgItems[i].BundleGlid);
                    }
                }
                else
                {
                    for (int i = 0; i < sgItems.Count; i++)
                    {
                        sgGlids.Push(Int32.Parse(Db.Shard.GetScalar("SELECT shard_info.get_items_custom_id()", 2).ToString()));

                        // Get the wokitem associated with the "glid" field of the game item
                        if (sgItems[i].GIId > 0 && sgItems[i].Glid > 0)
                        {
                            glidItemsByGIID.Add(sgItems[i].GIId, shoppingDao.GetItem(sgItems[i].Glid, false));
                        }
                    }

                    for (int i = 0; i < bundleGlidCount; i++)
                    {
                        bundleGlids.Push(Int32.Parse(Db.Shard.GetScalar("SELECT shard_info.get_items_custom_id()", 2).ToString()));
                    }
                }
            }
            catch (FormatException) { }

            // If all globalIds were allocated successfully, snapshot game items in a transaction
            if (sgGlids.Count == sgItems.Count && bundleGlids.Count == bundleGlidCount)
            {
                try
                {
                    string sOut = string.Empty;

                    foreach (ScriptGameItem sgItem in sgItems)
                    {
                        using (TransactionDecorator transaction = new TransactionDecorator())
                        {
                            // Add the WOKItem representing the game item
                            WOKItem wokItem = new WOKItem();
                            wokItem.GlobalId = sgGlids.Pop();
                            wokItem.Name = sgItem.Name;
                            wokItem.Description = (sgItem.Properties.TryGetValue("description", out sOut) ? sOut : "");
                            wokItem.UseType = WOKItem.USE_TYPE_SCRIPT_GAME_ITEM;
                            wokItem.ItemCreatorId = (uint)userId;
                            wokItem.IsDerivable = 0;
                            wokItem.DerivationLevel = -1;
                            wokItem.PassTypeId = 0;
                            wokItem.DesignerPrice = 0;
                            wokItem.DisplayName = sgItem.Name;

                            // If there is an item associated with the game item's "glid" field, we need to use it's thumbnails for the game item.
                            WOKItem glidItem;
                            if (glidItemsByGIGLID.TryGetValue(sgItem.GIGlid, out glidItem) || glidItemsByGIID.TryGetValue(sgItem.GIId, out glidItem))
                            {
                                wokItem.ThumbnailPath = glidItem.ThumbnailPath;
                                wokItem.ThumbnailSmallPath = glidItem.ThumbnailSmallPath;
                                wokItem.ThumbnailMediumPath = glidItem.ThumbnailMediumPath;
                                wokItem.ThumbnailLargePath = glidItem.ThumbnailLargePath;
                                wokItem.ThumbnailAssetdetailsPath = glidItem.ThumbnailAssetdetailsPath;
                            }

                            // Default items to "Deleted" state on shop, unless they're one of ours and inventory_compatible
                            wokItem.ItemActive = (itemsAreDefault && sgItem.InventoryCompatible ? 1 : 0);

                            wokItem.InventoryType = BundleItem.CURRENCY_TYPE_CREDITS;

                            sgItem.GIGlid = wokItem.GlobalId;

                            // Store web price in market cost for game items.
                            ScriptGameItemPrice price = GetScriptGameItemPrice(wokItem, sgItem.BundledGlids, bundledItems);
                            price.BasePrice = ScriptGameItemPrice.BASE_GAME_ITEM_PRICE;
                            wokItem.MarketCost = price.WebPrice;

                            shoppingDao.AddCustomItem(wokItem, false, true, 0, true);

                            // Add the bundles of associated WOKItems
                            if (sgItem.BundledGlids.Count > 0)
                            {
                                int bundleGlid = bundleGlids.Pop();

                                AddGameItemBundledGlids(bundleGlid, sgItem.BundledGlids, userId, wokItem.GlobalId, sgItem.Name, true);

                                sgItem.BundleGlid = bundleGlid;
                            }

                            if (addToInventory)
                            {
                                // Add the WOKItem representing the game item to the user's inventory
                                userDao.AddItemToPendingInventory(userId, "P", sgItem.GIGlid, 1, 512, true, true);
                            }

                            // Snapshot the game item properties
                            InsertSnapshotScriptGameItem(sgItem);

                            // Insert the default
                            if (itemsAreDefault)
                            {
                                UpdateDefaultScriptGameItem(sgItem);
                            }

                            // Store the web price as market cost


                            // If we reach this point, the transaction succeeded
                            transaction.Complete();
                        }
                    }

                    if (addToInventory)
                    {
                        // Add associated bundle glids to the user's inventory
                        foreach (int bundleItemGlid in bundledItemGlids)
                        {
                            userDao.AddItemToPendingInventory(userId, "B", bundleItemGlid, 1, 512, true, true);
                        }
                    }

                    // If we get here, the entire thing worked.
                    result = true;
                }
                catch (Exception e)
                {
                    m_logger.Error("MySQLScriptGameItemDao.SnapshotScriptGameItems() - Transaction aborted: " + e.ToString());
                }
            }

            return result;
        }

        /// <summary>
        /// AddSnapshotScriptGameItemsToTemplate
        /// </summary>
        /// <returns>True if the game items were successfully added.</returns>
        public bool AddSnapshotScriptGameItemsToCustomDeed(IEnumerable<ScriptGameItem> sgItems, int templateGlobalId)
        {
            bool success = false;

            if (sgItems == null || sgItems.Count() == 0)
            {
                return success;
            }

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    StringBuilder queryBuilder = new StringBuilder("INSERT IGNORE INTO starting_script_game_items (template_glid, game_item_id, game_item_glid, name) VALUES ");

                    // Add insert for each game item.
                    foreach (ScriptGameItem sgItem in sgItems)
                    {
                        queryBuilder.Append("(" + templateGlobalId + ", " + sgItem.GIId + ", " + sgItem.GIGlid + ", '" + sgItem.Name.Replace("'", "''") + "'), ");
                    }
                    queryBuilder.Remove(queryBuilder.Length - 2, 2);

                    Db.WOK.ExecuteNonQuery(queryBuilder.ToString(), new List<IDbDataParameter>(), true);

                    transaction.Complete();
                    success = true;
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLScriptGameItemDao.AddSnapshotScriptGameItemsToCustomDeed() - Transaction aborted: " + e.ToString());
            }

            return success;
        }

        #endregion

        #region Read

        /// <summary>
        /// GetInventoryScriptGameItems
        /// </summary>
        public PagedList<ScriptGameItem> GetDefaultScriptGameItems(string searchString, string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false)
        {
            List<string> joins = new List<string>();
            List<string> whereClauses = new List<string> { "AND dids.game_item_id IS NOT NULL" };

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                whereClauses.Add("AND sgi.name LIKE @name ");
                parameters.Add(new MySqlParameter("@name", string.Format("%{0}%", searchString)));
            }

            return GetSnapshotScriptGameItems(joins, whereClauses, parameters, orderBy, pageNumber, pageSize, includeProperties, includeBundledGlids);
        }

        /// <summary>
        /// GetGlobalConfigValue
        /// </summary>
        public string GetSGIGlobalConfigValue(string propertyName)
        {
            string sql = "SELECT property_value " +
                         "FROM script_game_item_global_configs " +
                         "WHERE property_name = @propertyName ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@propertyName", propertyName));

            return (Db.WOK.GetScalar(sql, parameters) ?? string.Empty).ToString();
        }

        public Dictionary<string, string> GetSGIGlobalConfigValues()
        {
            Dictionary<string, string> config = new Dictionary<string, string>();

            string sql = "SELECT property_name, property_value " +
                         "FROM script_game_item_global_configs " +
                         "ORDER BY property_name DESC ";

            DataTable dt = Db.WOK.GetDataTable(sql);

            foreach (DataRow row in dt.Rows)
            {
                config.Add(row["property_name"].ToString(), row["property_value"].ToString());
            }

            return config;
        }

        /// <summary>
        /// GetInventoryScriptGameItems
        /// </summary>
        public PagedList<ScriptGameItem> GetInventoryScriptGameItems(int playerId, string inventoryType, string searchString, IList<ScriptGameItemCategory> itemTypes,
            string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false)
        {
            List<string> joins = new List<string> { "INNER JOIN wok.inventories inv ON sgi.game_item_glid = inv.global_id AND inv.player_id = @playerId" };

            // If we're filtering game items by behavior/type combinations, add properties to joins.
            if (itemTypes.Where(s => !string.IsNullOrWhiteSpace(s.Behavior)).Count() > 0)
            {
                joins.Add("INNER JOIN wok.snapshot_script_game_item_properties sgip ON sgi.game_item_glid = sgip.game_item_glid AND sgip.property_name = 'behavior'");
            }

            List<string> whereClauses = new List<string>();

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@playerId", playerId));
            
            if (inventoryType != "All")
            {
                joins[0] += " AND inv.inventory_type = @inventoryType";
                parameters.Add(new MySqlParameter("@inventoryType", inventoryType));
            }

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                whereClauses.Add("AND sgi.name LIKE @name ");
                parameters.Add(new MySqlParameter("@name", string.Format("%{0}%", searchString)));
            }

            if (itemTypes.Count > 0)
            {
                string itemTypeSql = "AND ( ";
                for (int i = 0, lastIndex = itemTypes.Count - 1; i <= lastIndex; i++)
                {
                    itemTypeSql += "(";
                    itemTypeSql += "sgi.item_type = @itemType" + i;
                    itemTypeSql += (string.IsNullOrWhiteSpace(itemTypes[i].Behavior) ? "" : " AND sgip.property_value = @behavior" + i);
                    itemTypeSql += ")";
                    itemTypeSql += (i < lastIndex ? " OR " : "");

                    parameters.Add(new MySqlParameter("@itemType" + i, itemTypes[i].ItemType));

                    if (!string.IsNullOrWhiteSpace(itemTypes[i].Behavior))
                    {
                        parameters.Add(new MySqlParameter("@behavior" + i, itemTypes[i].Behavior));
                    }
                }
                itemTypeSql += ")";

                whereClauses.Add(itemTypeSql);
            }

            return GetSnapshotScriptGameItems(joins, whereClauses, parameters, orderBy, pageNumber, pageSize, includeProperties, includeBundledGlids);
        }

        public ScriptGameItem GetSnapshotScriptGameItem(int zoneInstanceId, int zoneType, int gameItemId, bool includeProperties = false, bool includeBundledGlids = false)
        {
            //create query string to get game by user id
            string sql = "SELECT sgi.game_item_glid " +
                         "FROM script_game_items sgi " +
                         "WHERE sgi.zone_instance_id = @zoneInstanceId " +
                         "AND sgi.zone_type = @zoneType " +
                         "AND sgi.game_item_id = @gameItemId ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));
            parameters.Add(new MySqlParameter("@gameItemId", gameItemId));

            object result = (Db.WOK.GetScalar(sql, parameters) ?? 0);

            return GetSnapshotScriptGameItem(Convert.ToInt32(result), includeProperties, includeBundledGlids);
        }

        /// <summary>
        /// GetScriptGameItemSnapshot
        /// </summary>
        /// <returns>A new script game item snapshot, and optionally, its properties.</returns>
        public ScriptGameItem GetSnapshotScriptGameItem(int gIGlid, bool includeProperties = false, bool includeBundledGlids = false)
        {
            //create query string to get game by user id
            string sql = "SELECT sgi.game_item_glid, sgi.glid, sgi.name, sgi.item_type, sgi.level, sgi.rarity, iw.global_id AS shop_global_id, dids.game_item_id AS default_game_item_id, ip.VALUE AS bundle_glid,  " +
                         "dids.automatic_insertion_start, dids.automatic_insertion_order, sgit.inventory_compatible " +
                         "FROM snapshot_script_game_items sgi " +
                         "INNER JOIN script_game_item_types sgit ON sgi.item_type = sgit.item_type " +
                         "LEFT OUTER JOIN shopping.items_web iw ON sgi.game_item_glid = iw.global_id AND iw.item_active = 1 " +
                         "LEFT OUTER JOIN default_script_game_items dids ON sgi.game_item_glid = dids.game_item_glid " +
                         "LEFT OUTER JOIN item_parameters ip ON sgi.game_item_glid = ip.global_id AND ip.param_type_id = 34 " +
                         "WHERE sgi.game_item_glid = @gIGlid ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@gIGlid", gIGlid));

            //perform query returning single data row from the wok database
            DataRow row = Db.WOK.GetDataRow(sql.ToString(), parameters);
            if (row == null)
                return new ScriptGameItem();

            ScriptGameItem scriptGameItem = new ScriptGameItem();
            scriptGameItem.GIGlid = Convert.ToInt32(row["game_item_glid"]);
            scriptGameItem.GIId = (row["default_game_item_id"] == DBNull.Value ? 0 : Convert.ToInt32(row["default_game_item_id"]));
            scriptGameItem.Glid = (row["glid"] == DBNull.Value ? 0 : Convert.ToInt32(row["glid"]));
            scriptGameItem.Name = row["name"].ToString();
            scriptGameItem.ItemType = row["item_type"].ToString();
            scriptGameItem.Level = Convert.ToInt32(row["level"]);
            scriptGameItem.Rarity = row["rarity"].ToString();
            scriptGameItem.ExistsOnShop = (row["shop_global_id"] != DBNull.Value);
            scriptGameItem.BundleGlid = (row["bundle_glid"] == DBNull.Value ? 0 : Convert.ToInt32(row["bundle_glid"]));
            scriptGameItem.AutomaticInsertionStart = (row["automatic_insertion_start"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(row["automatic_insertion_start"]));
            scriptGameItem.AutomaticInsertionOrder = (row["automatic_insertion_order"] == DBNull.Value ? null : (int?)Convert.ToInt32(row["automatic_insertion_order"]));
            scriptGameItem.InventoryCompatible = (row["inventory_compatible"].ToString() == "T");

            // Pull and include the game item properties if necessary
            if (includeProperties)
            {
                sql = "SELECT game_item_glid, property_name, property_value " +
                      "FROM snapshot_script_game_item_properties " +
                      "WHERE game_item_glid = @gIGlid ";

                parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@gIGlid", gIGlid));

                DataTable dtProp = Db.WOK.GetDataTable(sql.ToString(), parameters);
                if (dtProp != null && dtProp.Rows.Count > 0)
                {
                    foreach (DataRow rowProp in dtProp.Rows)
                    {
                        scriptGameItem.Properties.Add(rowProp["property_name"].ToString(), rowProp["property_value"].ToString());
                    }
                }
            }

            // Pull and include bundled glids if necessary
            if (includeBundledGlids && scriptGameItem.BundleGlid > 0)
            {
                sql = "SELECT DISTINCT item_global_id " +
                      "FROM bundle_items " +
                      "WHERE bundle_global_id = @bundleGlid ";

                parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@bundleGlid", scriptGameItem.BundleGlid));

                DataTable dtBundleItems = Db.WOK.GetDataTable(sql.ToString(), parameters);
                if (dtBundleItems != null && dtBundleItems.Rows.Count > 0)
                {
                    foreach (DataRow rowProp in dtBundleItems.Rows)
                    {
                        scriptGameItem.BundledGlids.Add(Convert.ToInt32(rowProp["item_global_id"]));
                    }
                }
            }

            return scriptGameItem;
        }

        /// <summary>
        /// GetSuggestedNameSuffixNum
        /// </summary>
        public string GetSuggestedNameSuffixNum(int playerId, string inventoryType, string name, IList<ScriptGameItemCategory> itemTypes)
        {
            // Pull the suffix out and prepare the regex for mysql
            Regex regex = new Regex("(\\([0-9]+\\))?$");
            string passedSuffix = (regex.IsMatch(name) ? regex.Match(name).Groups[0].ToString() : string.Empty);
            string baseName = regex.Replace(name, string.Empty);
            string regexName = string.Format("^{0}(\\([0-9]+\\))?$", baseName);

            string sql = "SELECT DISTINCT CAST(COALESCE(TRIM(TRAILING ')' FROM SUBSTRING_INDEX(ssgi.name, '(', -1)), '0') AS SIGNED) AS num_suffix " +
                         "FROM wok.inventories i " +
                         "INNER JOIN snapshot_script_game_items ssgi ON i.global_id = ssgi.game_item_glid " +
                         "WHERE i.player_id = @playerId " +
                         "AND ssgi.name REGEXP @name ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@playerId", playerId));
            parameters.Add(new MySqlParameter("@inventoryType", inventoryType));
            parameters.Add(new MySqlParameter("@name", regexName));

            if (itemTypes.Count > 0)
            {
                sql += "AND ssgi.item_type IN ( ";
                for (int i = 0, lastIndex = itemTypes.Count - 1; i <= lastIndex; i++)
                {
                    sql += " @itemType" + i;
                    sql += (i < lastIndex ? ", " : "");

                    parameters.Add(new MySqlParameter("@itemType" + i, itemTypes[i].ItemType));
                }
                sql += ")";
            }

            sql += " ORDER BY num_suffix ASC ";

            // Start the next number suffix as the passed suffix number, or 0 if none was passed
            int nextNum = Int32.TryParse(passedSuffix.Replace("(", string.Empty).Replace(")", string.Empty), out nextNum) ? nextNum : 0;

            // Find the spot for the next number. 
            DataTable dt = Db.WOK.GetDataTable(sql, parameters);

            if (dt != null && dt.Rows.Count > 0)
            {
                int rowNum;
                foreach (DataRow row in dt.Rows)
                {
                    rowNum = Convert.ToInt32(row["num_suffix"]);
                    if (nextNum > rowNum)
                        continue;
                    else if (rowNum == nextNum)
                        nextNum++;
                    else
                        break;
                }
            }

            return (nextNum == 0 ? baseName : string.Format("{0}({1})", baseName, nextNum));
        }

        /// <summary>
        /// GetSuggestedGIGLID
        /// </summary>
        public int GetSuggestedGIGLID()
        {
            //create query string to get game by user id
            string sql = "SELECT MAX(global_id) AS suggestedGIGLID " +
                         "FROM items " +
                         "WHERE global_id BETWEEN @lowerLimit AND @upperLimit";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@lowerLimit", ScriptGameItem.GIGLID_LOWER_LIMIT));
            parameters.Add(new MySqlParameter("@upperLimit", ScriptGameItem.GIGLID_UPPER_LIMIT));

            object result = Db.WOK.GetScalar(sql, parameters);

            if (result == null)
                return 0;

            return Convert.ToInt32(result) + 1;
        }

        /// <summary>
        /// GetSuggestedBundleGlid
        /// </summary>
        public int GetSuggestedBundleGlid()
        {
            //create query string to get game by user id
            string sql = "SELECT MAX(global_id) AS suggestedBundleGlid " +
                         "FROM items " +
                         "WHERE global_id BETWEEN @lowerLimit AND @upperLimit";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@lowerLimit", ScriptGameItem.GI_BUNDLE_GLID_LOWER_LIMIT));
            parameters.Add(new MySqlParameter("@upperLimit", ScriptGameItem.GI_BUNDLE_GLID_UPPER_LIMIT));

            object result = Db.WOK.GetScalar(sql, parameters);

            if (result == null)
                return 0;

            return Convert.ToInt32(result) + 1;
        }

        /// <summary>
        /// GetSuggestedGIID
        /// </summary>
        public int GetSuggestedGIID()
        {
            //create query string to get game by user id
            string sql = "SELECT MAX(game_item_id) AS suggestedGIGLID " +
                         "FROM default_script_game_items ";

            object result = Db.WOK.GetScalar(sql);

            if (result == null)
                return 1;

            return Convert.ToInt32(result) + 1;
        }

        /// <summary>
        /// WorldHasGameItems
        /// </summary>
        public bool WorldHasScriptGameItems(int zoneIndex, int zoneInstanceId)
        {
            string sql = "SELECT 'T' " +
                         "FROM script_game_items " +
                         "WHERE zone_instance_id = @zoneInstanceId " +
                         "AND zone_type = zoneType(@zoneIndex) " +
                         "LIMIT 1";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneIndex", zoneIndex));
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));

            object result = Db.WOK.GetScalar(sql, parameters);

            if (result == null)
                return false;

            return true;
        }

        public PagedList<ScriptGameItem> GetWorldScriptGameItems(int zoneInstanceId, int zoneType, string filter, string orderBy, int pageNumber, int pageSize, bool includeProperties = false)
        {
            PagedList<ScriptGameItem> scriptGameItems = new PagedList<ScriptGameItem>();
            Dictionary<int, ScriptGameItem> gameItemLookup = null;

            //create query string to get game by user id
            string sql = "SELECT DISTINCT sgi.game_item_id, sgi.game_item_glid, sgi.glid, sgi.name, sgi.item_type, sgi.level, sgi.rarity, iw.global_id AS shop_global_id, ip.VALUE AS bundle_glid,  " +
                         "dids.automatic_insertion_start, dids.automatic_insertion_order, sgit.inventory_compatible " +
                         "FROM script_game_items sgi " +
                         "INNER JOIN script_game_item_types sgit ON sgi.item_type = sgit.item_type " +
                         "LEFT OUTER JOIN shopping.items_web iw ON sgi.game_item_glid = iw.global_id AND iw.item_active = 1 " +
                         "LEFT OUTER JOIN default_script_game_items dids ON sgi.game_item_glid = dids.game_item_glid " +
                         "LEFT OUTER JOIN item_parameters ip ON sgi.game_item_glid = ip.global_id AND ip.param_type_id = 34 " +
                         "WHERE zone_instance_id = @zoneInstanceId " +
                         "AND zone_type = @zoneType ";

            if (!string.IsNullOrWhiteSpace(filter))
            {
                sql += ("AND " + filter);
            }

            //perform query returning single data row from the wok database
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));
            PagedDataTable pdt = Db.WOK.GetPagedDataTable(sql, orderBy, parameters, pageNumber, pageSize);

            if (pdt == null)
                return scriptGameItems;

            foreach (DataRow row in pdt.Rows)
            {
                ScriptGameItem scriptGameItem = new ScriptGameItem();
                scriptGameItem.GIGlid = (row["game_item_glid"] == DBNull.Value ? 0 : Convert.ToInt32(row["game_item_glid"]));
                scriptGameItem.GIId = (Convert.ToInt32(row["game_item_id"]));
                scriptGameItem.Glid = (row["glid"] == DBNull.Value ? 0 : Convert.ToInt32(row["glid"]));
                scriptGameItem.Name = row["name"].ToString();
                scriptGameItem.ItemType = row["item_type"].ToString();
                scriptGameItem.Level = Convert.ToInt32(row["level"]);
                scriptGameItem.Rarity = row["rarity"].ToString();
                scriptGameItem.ExistsOnShop = (row["shop_global_id"] != DBNull.Value);
                scriptGameItem.BundleGlid = (row["bundle_glid"] == DBNull.Value ? 0 : Convert.ToInt32(row["bundle_glid"]));
                scriptGameItem.AutomaticInsertionStart = (row["automatic_insertion_start"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(row["automatic_insertion_start"]));
                scriptGameItem.AutomaticInsertionOrder = (row["automatic_insertion_order"] == DBNull.Value ? null : (int?)Convert.ToInt32(row["automatic_insertion_order"]));
                scriptGameItem.InventoryCompatible = (row["inventory_compatible"].ToString() == "T");

                scriptGameItems.Add(scriptGameItem);
            }

            // Pull and include the game item properties if necessary
            if (includeProperties && scriptGameItems.Count > 0)
            {
                // Get properties from non-snapshots
                sql = "SELECT game_item_id, property_name, property_value " +
                      "FROM script_game_item_properties " +
                      "WHERE game_item_id IN (" + string.Join(",", scriptGameItems.Select(i => i.GIId)) + ") " +
                      "AND zone_instance_id = @zoneInstanceId " +
                      "AND zone_type = @zoneType " +
                      "ORDER BY game_item_id";

                parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
                parameters.Add(new MySqlParameter("@zoneType", zoneType));

                DataTable dtProp = Db.WOK.GetDataTable(sql.ToString(), parameters);
                if (dtProp != null && dtProp.Rows.Count > 0)
                {
                    // Create lookup
                    gameItemLookup = (gameItemLookup ?? scriptGameItems.ToDictionary(i => i.GIId));

                    foreach (DataRow rowProp in dtProp.Rows)
                    {
                        gameItemLookup[Convert.ToInt32(rowProp["game_item_id"])].Properties.Add(rowProp["property_name"].ToString(), rowProp["property_value"].ToString());
                    }
                }

                // Get snapshot properties
                sql = "SELECT sgi.game_item_id, sgip.game_item_glid, sgip.property_name, sgip.property_value " +
                      "FROM snapshot_script_game_item_properties sgip " +
                      "INNER JOIN script_game_items sgi ON sgi.game_item_glid = sgip.game_item_glid " +
                      "AND sgi.zone_instance_id = @zoneInstanceId " +
                      "AND sgi.zone_type = @zoneType " +
                      "AND sgi.game_item_id IN (" + string.Join(",", scriptGameItems.Select(i => i.GIId)) + ") " +
                      "ORDER BY game_item_id";

                parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
                parameters.Add(new MySqlParameter("@zoneType", zoneType));

                dtProp = Db.WOK.GetDataTable(sql.ToString(), parameters);
                if (dtProp != null && dtProp.Rows.Count > 0)
                {
                    // Create lookup
                    gameItemLookup = (gameItemLookup ?? scriptGameItems.ToDictionary(i => i.GIId));

                    foreach (DataRow rowProp in dtProp.Rows)
                    {
                        gameItemLookup[Convert.ToInt32(rowProp["game_item_id"])].Properties.Add(rowProp["property_name"].ToString(), rowProp["property_value"].ToString());
                    }
                }
            }

            // Make sure total count makes sense
            scriptGameItems.TotalCount = pdt.TotalCount;

            return scriptGameItems;
        }

        /// <summary>
        /// GetDeedScriptGameItems
        /// </summary>
        public PagedList<ScriptGameItem> GetDeedScriptGameItems(int templateGlid, string searchString, string[] itemTypes, 
            string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false)
        {
            List<string> joins = new List<string> { "INNER JOIN starting_script_game_items ssgi ON sgi.game_item_glid = ssgi.game_item_glid" };
            List<string> whereClauses = new List<string> { "AND ssgi.template_glid = @templateGlid" };

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@templateGlid", templateGlid));

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                whereClauses.Add("AND ssgi.name LIKE @name ");
                parameters.Add(new MySqlParameter("@name", string.Format("%{0}%", searchString)));
            }

            if (itemTypes != null && itemTypes.Length > 0)
            {
                string itemTypeSql = "AND sgi.item_type IN ( ";
                for (int i = 0, lastIndex = itemTypes.Length - 1; i <= lastIndex; i++)
                {
                    itemTypeSql += " @itemType" + i;
                    itemTypeSql += (i < lastIndex ? ", " : "");

                    parameters.Add(new MySqlParameter("@itemType" + i, itemTypes[i]));
                }
                itemTypeSql += ")";

                whereClauses.Add(itemTypeSql);
            }

            return GetSnapshotScriptGameItems(joins, whereClauses, parameters, orderBy, pageNumber, pageSize, includeProperties, includeBundledGlids);
        }

        /// <summary>
        /// GetTemplateScriptGameItems
        /// </summary>
        public PagedList<ScriptGameItem> GetTemplateScriptGameItems(int templateId, string searchString, string[] itemTypes,
            string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false)
        {
            string sql = "SELECT global_id " +
                         "FROM world_template_global_id " +
                         "WHERE template_id = @templateId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@templateId", templateId));
            int templateGlid = Convert.ToInt32(Db.WOK.GetScalar(sql, parameters));

            return GetDeedScriptGameItems(templateGlid, searchString, itemTypes, orderBy, pageNumber, pageSize, includeProperties, includeBundledGlids);
        }

        public bool IsTypeInventoryCompatible(string itemType)
        {
            string sql = "SELECT inventory_compatible " +
                         "FROM script_game_item_types " +
                         "WHERE item_type = @itemType ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@itemType", itemType));

            object result = Db.WOK.GetScalar(sql, parameters);

            return (result != null && result.ToString().Equals("T"));
        }

        #endregion

        #region Update

        public bool UpdateSGIGlobalConfig(Dictionary<string, string> config)
        {
            bool success = false;

            try
            {
                if (config.Keys.Count > 0)
                {
                    using (TransactionDecorator transaction = new TransactionDecorator())
                    {
                        foreach (KeyValuePair<string, string> property in config)
                        {
                            string sqlString = "INSERT INTO script_game_item_global_configs (property_name, property_value) " +
                                               "VALUES (@propertyName, @propertyValue) " +
                                               "ON DUPLICATE KEY UPDATE property_value = @propertyValue";

                            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                            parameters.Add(new MySqlParameter("@propertyName", property.Key));
                            parameters.Add(new MySqlParameter("@propertyValue", (string.IsNullOrWhiteSpace(property.Value) ? (object)DBNull.Value : property.Value)));
                            Db.WOK.ExecuteNonQuery(sqlString, parameters, true);
                        }

                        transaction.Complete();
                        success = true;
                    }
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLScriptGameItemDao.UpdateSGIGlobalConfig() - Transaction aborted: " + e.ToString());
            }

            return success;
        }

        /// <summary>
        /// Called to update the snapshot of a ScriptGameItem
        /// </summary>
        public bool UpdateSnapshotScriptGameItem(ScriptGameItem sgItem, int userId)
        {
            bool result = false;
            MySQLShoppingDao shoppingDao = new MySQLShoppingDao();

            try
            {
                // Quick check to make sure that none of the required fields are invalid
                if (sgItem.GIGlid <= 0 ||
                    string.IsNullOrWhiteSpace(sgItem.Name) ||
                    string.IsNullOrWhiteSpace(sgItem.ItemType) ||
                    sgItem.Level <= 0 ||
                    string.IsNullOrWhiteSpace(sgItem.Rarity) ||
                    (sgItem.BundleGlid <= 0 && sgItem.BundledGlids.Count > 0))
                {
                    throw new ArgumentException("Invalid game item input.");
                }

                // Get the item in its current state. We'll use it to avoid making more expensive updates if we can.
                ScriptGameItem dbItem = GetSnapshotScriptGameItem(sgItem.GIGlid);

                // Get glid item
                WOKItem glidItem = shoppingDao.GetItem(sgItem.Glid, false);

                // Bundled items
                Dictionary<int, WOKItem> bundledItemOld = (dbItem.BundledGlids.Count > 0 ?
                    shoppingDao.GetItems(dbItem.BundledGlids, false, "").ToDictionary(i => i.GlobalId) : new Dictionary<int, WOKItem>());

                Dictionary<int, WOKItem> bundledItemNew = (sgItem.BundledGlids.Count > 0 ?
                    shoppingDao.GetItems(sgItem.BundledGlids, false, "").ToDictionary(i => i.GlobalId) : new Dictionary<int, WOKItem>());

                // Store web price in market cost for game items.
                WOKItem wokItem = shoppingDao.GetItem(dbItem.GIGlid, false);
                ScriptGameItemPrice oldPrice = GetScriptGameItemPrice(wokItem, dbItem.BundledGlids, bundledItemOld);
                ScriptGameItemPrice newPrice = GetScriptGameItemPrice(wokItem, sgItem.BundledGlids, bundledItemNew);

                // Recalculate price using existing base price and new bundled items.
                newPrice.BasePrice = oldPrice.BasePrice;
                wokItem.MarketCost = newPrice.WebPrice;
                shoppingDao.UpdateCustomItem(wokItem);

                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    // Insert game item snapshot
                    string sqlString = "UPDATE snapshot_script_game_items " +
                                       "SET glid = @glid, " +
                                       "name = @name, " +
                                       "item_type = @itemType, " +
                                       "level = @level, " +
                                       "rarity = @rarity " +
                                       "WHERE game_item_glid = @gameItemGlid ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@gameItemGlid", sgItem.GIGlid));
                    parameters.Add(new MySqlParameter("@glid", (sgItem.Glid > 0 ? (object)sgItem.Glid : DBNull.Value)));
                    parameters.Add(new MySqlParameter("@name", sgItem.Name));
                    parameters.Add(new MySqlParameter("@itemType", sgItem.ItemType));
                    parameters.Add(new MySqlParameter("@level", sgItem.Level));
                    parameters.Add(new MySqlParameter("@rarity", sgItem.Rarity));
                    Db.WOK.ExecuteNonQuery(sqlString, parameters, true);

                    // Delete old properties
                    sqlString = "DELETE FROM snapshot_script_game_item_properties WHERE game_item_glid = @gameItemGlid";
                    parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@gameItemGlid", sgItem.GIGlid));
                    Db.WOK.ExecuteNonQuery(sqlString, parameters, true);

                    // Insert new game item properties
                    string propValue = string.Empty;
                    foreach (KeyValuePair<string, string> property in sgItem.Properties)
                    {
                        sqlString = "INSERT INTO snapshot_script_game_item_properties (game_item_glid, property_name, property_value) " +
                                           "VALUES (@gameItemGlid, @propertyName, @propertyValue)";

                        parameters = new List<IDbDataParameter>();
                        parameters.Add(new MySqlParameter("@gameItemGlid", sgItem.GIGlid));
                        parameters.Add(new MySqlParameter("@propertyName", property.Key));
                        parameters.Add(new MySqlParameter("@propertyValue", property.Value));
                        Db.WOK.ExecuteNonQuery(sqlString, parameters, true);
                    }

                    // Add bundled items
                    if (dbItem.BundleGlid > 0)
                    {
                        // Make sure entered bundled glid matches saved glid
                        if (dbItem.BundleGlid != sgItem.BundleGlid)
                        {
                            throw new ArgumentException("Mismatch between input and saved BundleGlid");
                        }

                        // If a bundle already existed for this item, clean bundled items out and re-add
                        shoppingDao.RemoveAllFromBundle(sgItem.BundleGlid, true);
                        AddGameItemBundledGlids(sgItem.BundleGlid, sgItem.BundledGlids, userId);
                    }
                    else
                    {
                        AddGameItemBundledGlids(sgItem.BundleGlid, sgItem.BundledGlids, userId, sgItem.GIGlid, sgItem.Name, true);
                    }

                    // Update automatic insertion start
                    UpdateDefaultScriptGameItem(sgItem);

                    // Only update script game items records if necessary
                    if (dbItem.Glid != sgItem.Glid ||
                        dbItem.Level != sgItem.Level ||
                        !dbItem.Name.Equals(sgItem.Name) ||
                        !dbItem.ItemType.Equals(sgItem.ItemType) ||
                        !dbItem.Rarity.Equals(sgItem.Rarity))
                    {
                        sqlString = "UPDATE script_game_items " +
                                        "SET glid = @glid, " +
                                        "name = @name, " +
                                        "item_type = @itemType, " +
                                        "level = @level, " +
                                        "rarity = @rarity " +
                                        "WHERE game_item_glid = @gameItemGlid ";

                        parameters = new List<IDbDataParameter>();
                        parameters.Add(new MySqlParameter("@gameItemGlid", sgItem.GIGlid));
                        parameters.Add(new MySqlParameter("@glid", (sgItem.Glid > 0 ? (object)sgItem.Glid : DBNull.Value)));
                        parameters.Add(new MySqlParameter("@name", sgItem.Name));
                        parameters.Add(new MySqlParameter("@itemType", sgItem.ItemType));
                        parameters.Add(new MySqlParameter("@level", sgItem.Level));
                        parameters.Add(new MySqlParameter("@rarity", sgItem.Rarity));
                        Db.WOK.ExecuteNonQuery(sqlString, parameters, true);

                        // Make sure name of wok item matches name of game item
                        if (!dbItem.Name.Equals(sgItem.Name))
                        {
                            sqlString = "UPDATE items " +
                                        "SET name = @name " +
                                        "WHERE global_id = @gameItemGlid ";

                            parameters = new List<IDbDataParameter>();
                            parameters.Add(new MySqlParameter("@name", sgItem.Name));
                            parameters.Add(new MySqlParameter("@gameItemGlid", sgItem.GIGlid));
                            Db.WOK.ExecuteNonQuery(sqlString, parameters, true);
                        }
                    }

                    transaction.Complete();
                    result = true;
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLScriptGameItemDao.UpdateSnapshotScriptGameItems() - Transaction aborted: " + e.ToString());
            }

            return result;
        }

        /// <summary>
        /// Called to update the GIGLIDs of items within a game world.
        /// </summary>
        public bool UpdateWorldGIGLIDs(int zoneInstanceId, int zoneType, List<ScriptGameItem> sgItems)
        {
            bool result = false;
            MySQLShoppingDao shoppingDao = new MySQLShoppingDao();

            try
            {
                // Quick check to make sure that none of the required fields are missing
                if (sgItems == null ||
                    sgItems.Count == 0 ||
                    sgItems.Where(sg => sg.GIGlid <= 0).Count() > 0 ||
                    sgItems.Where(sg => sg.GIId <= 0).Count() > 0 ||
                    zoneInstanceId <= 0 ||
                    zoneType <= 0)
                {
                    throw new ArgumentException("Invalid input.");
                }

                foreach (ScriptGameItem sgItem in sgItems)
                {
                    // It's only necessary that processing each item be atomic. The process of setting a GIGLID
                    // on a game item in a world is harmless as long as the process is complete per item.
                    using (TransactionDecorator transaction = new TransactionDecorator())
                    {
                        // Insert game item snapshot
                        string sqlString = "UPDATE script_game_items " +
                                            "SET game_item_glid = @gameItemGlid " +
                                            "WHERE game_item_id = @gameItemId " +
                                            "AND zone_instance_id = @zoneInstanceId " +
                                            "AND zone_type = @zoneType";

                        List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                        parameters.Add(new MySqlParameter("@gameItemGlid", sgItem.GIGlid));
                        parameters.Add(new MySqlParameter("@gameItemId", sgItem.GIId));
                        parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
                        parameters.Add(new MySqlParameter("@zoneType", zoneType));
                        Db.WOK.ExecuteNonQuery(sqlString, parameters, true);

                        // Delete old properties
                        sqlString = "DELETE FROM script_game_item_properties " +
                                    "WHERE game_item_id = @gameItemId " +
                                    "AND zone_instance_id = @zoneInstanceId " +
                                    "AND zone_type = @zoneType";
                        parameters = new List<IDbDataParameter>();
                        parameters.Add(new MySqlParameter("@gameItemId", sgItem.GIId));
                        parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
                        parameters.Add(new MySqlParameter("@zoneType", zoneType));
                        Db.WOK.ExecuteNonQuery(sqlString, parameters, true);

                        transaction.Complete();
                        result = true;
                    }
                }  
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLScriptGameItemDao.UpdateWorldGIGLIDs() - Transaction aborted: " + e.ToString());
            }

            return result;
        }

        #endregion

        #region Delete

        public bool DeleteSGIGlobalConfig(string propName)
        {
            bool success = false;

            if (!string.IsNullOrWhiteSpace(propName))
            {
                string sqlString = "DELETE FROM script_game_item_global_configs WHERE property_name = @propertyName";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@propertyName", propName));
                success = Db.WOK.ExecuteNonQuery(sqlString, parameters) > 0;
            }

            return success;
        }

        /// <summary>
        /// Called to delete the snapshot of a ScriptGameItem
        /// </summary>
        public bool DeleteStartingScriptGameItem(ScriptGameItem sgItem, int templateId = 0)
        {
            bool success = false;

            try
            {
                // Delete game item snapshot
                string sqlString = "DELETE sgi.* FROM starting_script_game_items sgi " +
                                    (templateId > 0 ? "INNER JOIN world_template_global_id wtg ON wtg.global_id = sgi.template_glid AND wtg.template_id = @templateId " : "") +
                                    "WHERE game_item_glid = @gameItemGlid ";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@gameItemGlid", sgItem.GIGlid));

                if (templateId > 0)
                    parameters.Add(new MySqlParameter("@templateId", templateId));

                Db.WOK.ExecuteNonQuery(sqlString, parameters, true);

                success = true;
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLScriptGameItemDao.DeleteStartingScriptGameItems() - Transaction aborted: " + e.ToString());
            }

            return success;
        }

        public bool DeleteAllSGItemsInCustomDeed(int templateGlobalId)
        {
            bool success = false;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sql = "DELETE FROM starting_script_game_items WHERE template_glid = @templateGlobalId";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@templateGlobalId", templateGlobalId));

                    Db.WOK.ExecuteNonQuery(sql, parameters, true);

                    transaction.Complete();
                    success = true;
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLScriptGameItemDao.DeleteAllSGItemsInCustomDeed() - Transaction aborted: " + e.ToString());
            }

            return success;
        }

        #endregion

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
    }
}
