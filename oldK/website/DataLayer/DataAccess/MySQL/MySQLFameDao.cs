///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLFameDao : IFameDao
    {

        /// <summary>
        /// Get User Fame
        /// </summary>
        public UserFame GetUserFame (int userId, int fameTypeId)
        {
            string cacheKey = CentralCache.keyFameUserFame + userId.ToString() + fameTypeId.ToString();
            UserFame userFame = (UserFame)CentralCache.Get(cacheKey);

            if (userFame != null)
            {
                // Found in cache
                return userFame;
            }

            string strQuery = "SELECT u.username, uf.user_id, uf.fame_type_id, uf.level_id, uf.points " +
                " FROM user_fame uf, kaneva.users u " +
                " WHERE uf.user_id = @userId " +
                " AND uf.user_id = u.user_id " +
                " AND fame_type_id = @fameTypeId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@fameTypeId", fameTypeId));
            DataRow row = Db.Fame.GetDataRow (strQuery, parameters);

            if (row == null)
            {
                return new UserFame (userId, fameTypeId, 0, 0, "");
            }

            userFame = new UserFame(Convert.ToInt32(row["user_id"]), Convert.ToInt32(row["fame_type_id"]), Convert.ToInt32(row["level_id"]), Convert.ToInt32(row["points"]), row["username"].ToString());

            CentralCache.Store(cacheKey, userFame, TimeSpan.FromMinutes(15));

            return userFame; 
        }

         /// <summary>
        /// Get FameType
        /// </summary>
        public FameType GetFameType (int fameTypeId)
        {
            string cacheKey = "fame.fametype." + fameTypeId;
            FameType fameType = (FameType) DbCache.Cache[cacheKey];

            if (fameType == null)
            {
                // Add to the cache
                string strQuery = "SELECT fame_type_id, name " +
                " FROM fame_type " +
                " WHERE fame_type_id = @fameTypeId ";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@fameTypeId", fameTypeId));
                DataRow row = Db.Fame.GetDataRow(strQuery, parameters);

                if (row == null)
                {
                    return new FameType(fameTypeId, "", 0);
                }

                // Get the Max Level
                string strQuery2 = "SELECT max(level_number) max_level_number FROM levels WHERE fame_type_id = @fameTypeId";

                if (fameTypeId == 2)
                {
                    strQuery2 = "SELECT max(level_number) max_level_number FROM wok.skill_levels WHERE skill_id = @fameTypeId";
                }

                List<IDbDataParameter> parameters2 = new List<IDbDataParameter>();
                parameters2.Add(new MySqlParameter("@fameTypeId", fameTypeId));
                DataRow row2 = Db.Fame.GetDataRow(strQuery2, parameters2);

                fameType = new FameType(Convert.ToInt32(row["fame_type_id"]), row["name"].ToString(), Convert.ToInt32(row2["max_level_number"]));

                DbCache.Cache.Insert(cacheKey, fameType, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return fameType;
        }

        /// <summary>
        /// Get Dance Fame
        /// </summary>
        [Obsolete ("This is here until Dance Fame gets moved from skills to Fame")]
        public UserFame GetDanceFame (int userId, int fameTypeId, string username, int playerId)
        {
            string strQuery = "SELECT player_id, skill_id, current_level, current_experience " +
                " FROM wok.player_skills " +
                " WHERE player_id = @playerId " +
                " AND skill_id = @skillId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@playerId", playerId));
            parameters.Add (new MySqlParameter ("@skillId", fameTypeId));
            DataRow row = Db.WOK.GetDataRow (strQuery, parameters);

            if (row == null)
            {
                return new UserFame (userId, fameTypeId, 1, 0, username);
            }

            int iExperience = 0;

            try
            {
                string sCurrentExperience = row["current_experience"].ToString ();
                int iPeriod = sCurrentExperience.IndexOf (".");

                if (iPeriod > 0)
                {
                    sCurrentExperience = sCurrentExperience.Substring (0, iPeriod);
                }

                iExperience = Convert.ToInt32 (sCurrentExperience);
            }
            catch (Exception) { }

            return new UserFame (userId, fameTypeId, Convert.ToInt32 (row["current_level"]), iExperience, username);
        }

        /// <summary>
        /// Get Dance Level
        /// </summary>
        [Obsolete ("This is here until Dance Fame gets moved from skills to Fame")]
        public Level GetDanceLevel (int levelId, int fameTypeId)
        {
            string cacheKey = "wok.skill_levels." + levelId + fameTypeId;
            Level level = (Level) DbCache.Cache[cacheKey];

            if (level == null)
            {
                string strQuery = "SELECT level_id, skill_id, level_number, start_exp, end_exp " +
                    " FROM wok.skill_levels " +
                    " WHERE level_number = @levelId" +
                    " AND skill_id = @fameTypeId ";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@levelId", levelId - 1));
                parameters.Add(new MySqlParameter("@fameTypeId", fameTypeId));
                DataRow row = Db.WOK.GetDataRow(strQuery, parameters);

                if (row == null)
                {
                    return new Level(levelId, fameTypeId, 0, 0, 0, 0, 0, 0, false);
                }

                // Get max level
                string strQuery2 = "SELECT max(level_number) max_level_number FROM wok.skill_levels WHERE skill_id = @fameTypeId";
                List<IDbDataParameter> parameters2 = new List<IDbDataParameter>();
                parameters2.Add(new MySqlParameter("@fameTypeId", fameTypeId));

                DataRow row2 = Db.Fame.GetDataRow(strQuery2, parameters2);

                level = new Level(levelId, fameTypeId, Convert.ToInt32(row["level_number"]), Convert.ToInt32(row["start_exp"]),
                    Convert.ToInt32(row["end_exp"]), 0, 0, Convert.ToInt32(row2["max_level_number"]), false);

                DbCache.Cache.Insert(cacheKey, level, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return level;
        }

        /// <summary>
        /// Get User Fame
        /// </summary>
        public PagedList<UserFame> GetUserFame (int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strQuery = "SELECT u.username, uf.user_id, uf.fame_type_id, uf.level_id, uf.points " +
                " FROM user_fame uf, kaneva.users u " +
                " WHERE uf.user_id = u.user_id ";

            if (fameTypeId > 0)
            {
                strQuery += " AND fame_type_id = @fameTypeid ";
                parameters.Add (new MySqlParameter ("@fameTypeid", fameTypeId));
            }

            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            PagedDataTable dt = Db.Fame.GetPagedDataTable (strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<UserFame> list = new PagedList<UserFame> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new UserFame (Convert.ToInt32 (row["user_id"]), Convert.ToInt32 (row["fame_type_id"]), Convert.ToInt32 (row["level_id"]), Convert.ToInt32 (row["points"]), row["username"].ToString ()
                ));
            }
            return list;
        }

        /// <summary>
        /// Gets level
        /// </summary>
        public Level GetLevel(int levelId)
        {
            string cacheKey = "fame.level." + levelId;
            Level level = (Level) DbCache.Cache[cacheKey];

            if (level == null)
            {
                string strQuery = "SELECT level_id, fame_type_id, level_number, start_points, " +
                    " end_points, rewards, send_kmail " +
                    " FROM levels " +
                    " WHERE level_id = @levelId ";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@levelId", levelId));

                DataRow row = Db.Fame.GetDataRow(strQuery, parameters);

                if (row == null)
                {
                    return new Level(levelId, 0, 0, 0, 5000, 0, 0, 0, false);
                }

                // Get max level
                string strQuery2 = "SELECT max(level_number) max_level_number FROM levels WHERE fame_type_id = @fameTypeId";
                List<IDbDataParameter> parameters2 = new List<IDbDataParameter>();
                parameters2.Add(new MySqlParameter("@fameTypeId", Convert.ToInt32(row["fame_type_id"])));

                DataRow row2 = Db.Fame.GetDataRow(strQuery2, parameters2);

                //Get next level
                int nextLevelNumber = Convert.ToInt32(row["level_number"]) + 1;
                string strQuery3 = "SELECT level_id FROM fame.levels WHERE fame_type_id = @fameTypeId AND level_number = @levelNumber";
                List<IDbDataParameter> parameters3 = new List<IDbDataParameter>();
                parameters3.Add(new MySqlParameter("@fameTypeId", Convert.ToInt32(row["fame_type_id"])));
                parameters3.Add(new MySqlParameter("@levelNumber", nextLevelNumber));

                DataRow row3 = Db.Fame.GetDataRow(strQuery3, parameters3);

                int nextLevelId = 0;

                if (row3 != null)
                {
                    nextLevelId = Convert.ToInt32(row3["level_id"]);
                }

                level = new Level(Convert.ToInt32(row["level_id"]), Convert.ToInt32(row["fame_type_id"]), Convert.ToInt32(row["level_number"]), Convert.ToInt32(row["start_points"]),
                        Convert.ToInt32(row["end_points"]), Convert.ToInt32(row["rewards"]), nextLevelId, Convert.ToInt32(row2["max_level_number"]), (row["send_kmail"].ToString().Equals("Y")));

                DbCache.Cache.Insert(cacheKey, level, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return level;
        }

        /// <summary>
        /// Forces a player's fame up to a specific level.
        /// Intended to be used in automated testing.
        /// </summary>
        public int SetLevel(int userId, int fameTypeId, int level)
        {
            int newLevelId = 0;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    // Get the level id based on the level number.
                    string sqlString =  "SELECT level_id, start_points " +
                                        "FROM fame.levels " +
                                        "WHERE fame_type_id = @fameTypeId " +
                                        "AND level_number = @level ";
                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@fameTypeId", fameTypeId));
                    parameters.Add(new MySqlParameter("@level", level));
                    DataRow row = Db.Fame.GetDataRow(sqlString, parameters);

                    if (row != null && row["level_id"] != null && row["start_points"] != null)
                    {
                        int levelId = Convert.ToInt32(row["level_id"]);
                        int points = Convert.ToInt32(row["start_points"]) + 1;

                        // Set the user's points total to match the lower-limit.
                        sqlString = "INSERT INTO fame.user_fame (user_id, fame_type_id, level_id, points) " +
                                    "VALUES (@userId, @fameTypeId, 0, @points) " +
                                    "ON DUPLICATE KEY UPDATE points = @points ";
                        parameters.Clear();
                        parameters.Add(new MySqlParameter("@userId", userId));
                        parameters.Add(new MySqlParameter("@fameTypeId", fameTypeId));
                        parameters.Add(new MySqlParameter("@points", points));
                        Db.Fame.ExecuteNonQuery(sqlString, parameters, true);

                        // Call the level up stored proc.
                        sqlString = "CALL fame.LevelUp(@userId, @fameTypeId, 0, @levelId); SELECT @levelId;";
                        parameters.Clear();
                        parameters.Add(new MySqlParameter("@userId", userId));
                        parameters.Add(new MySqlParameter("@fameTypeId", fameTypeId));
                        newLevelId = Convert.ToInt32(Db.Fame.GetScalar(sqlString, parameters) ?? (object)0);

                        // If we get here, we're all good.
                        if (newLevelId == levelId)
                        {
                            transaction.Complete();
                            newLevelId = level;
                        }
                        else
                        {
                            newLevelId = 0;
                        }
                    } 
                }
            }
            catch (Exception) { }

            return newLevelId;
        }

        /// <summary>
        /// Gets list of levels
        /// </summary>
        public PagedList<Level> GetLevels (int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strQuery = "SELECT level_id, fame_type_id, level_number, start_points, " +
                " end_points, rewards, send_kmail " +
                " FROM levels ";

            if (fameTypeId > 0)
            {
                strQuery += " WHERE fame_type_id = @fameTypeid ";
                parameters.Add (new MySqlParameter ("@fameTypeid", fameTypeId));
            }

            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            PagedDataTable dt = Db.Fame.GetPagedDataTable (strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<Level> list = new PagedList<Level> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new Level (Convert.ToInt32 (row["level_id"]), Convert.ToInt32 (row["fame_type_id"]), Convert.ToInt32 (row["level_number"]), Convert.ToInt32 (row["start_points"]),
                    Convert.ToInt32(row["end_points"]), Convert.ToInt32(row["rewards"]), 0, 0, (row["send_kmail"].ToString().Equals("Y"))
                ));
            }
            return list;
        }

        /// <summary>
        /// GetLevelAwards
        /// </summary>
        public List<LevelAwards> GetLevelAwards (int levelId, string gender)
        {
            string cacheKey = "fame.level_awards." + levelId + gender;
            List<LevelAwards> list = (List<LevelAwards>)DbCache.Cache[cacheKey];

            if (list == null)
            {

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();

                string strQuery = "SELECT t1.level_award_id, t1.level_id, t1.name, t1.global_id, t1.animation_id, " +
                    " t1.gender, t1.quantity, t2.name as new_title_name, t3.fame_type_id " +
                    " FROM fame.level_awards AS t1 LEFT JOIN wok.titles AS t2 " +
                    " ON t1.new_title_id = t2.title_id LEFT JOIN fame.levels AS t3" +
                    " ON t1.level_id = t3.level_id" +
                    " WHERE t1.level_id = @levelId AND t1.gender = @gender ";

                parameters.Add(new MySqlParameter("@gender", gender));
                parameters.Add(new MySqlParameter("@levelId", levelId));
                DataTable dt = Db.Fame.GetDataTable(strQuery, parameters);

                list = new List<LevelAwards>();

                foreach (DataRow row in dt.Rows)
                {
                    list.Add(new LevelAwards(Convert.ToInt32(row["level_award_id"]), Convert.ToInt32(row["level_id"]), Convert.ToInt32(row["fame_type_id"]), row["name"].ToString(), Convert.ToInt32(row["global_id"]), Convert.ToInt32(row["animation_id"]),
                        row["gender"].ToString(), Convert.ToInt32(row["quantity"]), row["new_title_name"].ToString()
                    ));
                }
                if (dt.Rows.Count == 0) //no awards for that level
                {
                    List<IDbDataParameter> parameters2 = new List<IDbDataParameter>();

                    string strQuery2 = "SELECT fame_type_id FROM fame.levels WHERE level_id = @levelId";
                    parameters2.Add(new MySqlParameter("@levelId", levelId));
                    DataTable dtFameType = Db.Fame.GetDataTable(strQuery2, parameters2);
                    if (dtFameType.Rows.Count > 0)
                    {
                        DataRow row2 = dtFameType.Rows[0];
                        list.Add(new LevelAwards(0, levelId, Convert.ToInt32(row2["fame_type_id"]), "", 0, 0, gender, 0, ""));
                    }
                    else
                    {
                        list.Add(new LevelAwards(0, levelId, 0, "", 0, 0, gender, 0, ""));
                    }
                }

                DbCache.Cache.Insert(cacheKey, list, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return list;
        }

        /// <summary>
        /// GetLevelAwards
        /// </summary>
        public List<LevelAwards> GetAllLevelAwards(int fameTypeId)
        {
            string cacheKey = "fame.Alllevel_awards." + fameTypeId;
            List<LevelAwards> list = (List<LevelAwards>)DbCache.Cache[cacheKey];

            if (list == null)
            {

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();

                string strQuery = "SELECT t1.level_award_id, t1.level_id, t1.name, t1.global_id, t1.animation_id, " +
                    " t1.gender, t1.quantity, t2.name as new_title_name, t3.fame_type_id " +
                    " FROM fame.level_awards AS t1 " +
                    " LEFT JOIN wok.titles AS t2 ON t1.new_title_id = t2.title_id " +
                    " INNER JOIN fame.levels AS t3 " +
                    " ON t1.level_id = t3.level_id" +
                    " WHERE t3.fame_type_id = @fameTypeId ";

                parameters.Add(new MySqlParameter("@fameTypeId", fameTypeId));
                DataTable dt = Db.Fame.GetDataTable(strQuery, parameters);

                list = new List<LevelAwards>();

                foreach (DataRow row in dt.Rows)
                {
                    list.Add(new LevelAwards(Convert.ToInt32(row["level_award_id"]), Convert.ToInt32(row["level_id"]), Convert.ToInt32(row["fame_type_id"]), row["name"].ToString(), Convert.ToInt32(row["global_id"]), Convert.ToInt32(row["animation_id"]),
                        row["gender"].ToString(), Convert.ToInt32(row["quantity"]), row["new_title_name"].ToString()
                    ));
                }

                DbCache.Cache.Insert(cacheKey, list, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return list;
        }

        /// <summary>
        /// GetPacket
        /// </summary>
        public Packet GetPacket (int packetId)
        {
            string cacheKey = "fame.packet." + packetId;
            Packet packet = (Packet) DbCache.Cache[cacheKey];

            if (packet == null)
            {
                string strQuery = "SELECT packet_id, fame_type_id, name, points_earned, " +
                    " rewards, time_interval, reward_interval, max_per_day, status_id, badge_id, is_server_only " +
                    " FROM packets " +
                    " WHERE packet_id = @packetId ";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@packetId", packetId));
                DataRow row = Db.Fame.GetDataRow(strQuery, parameters);

                if (row == null)
                {
                    return new Packet(packetId, 0, "", 0, 0, 0, 0, 0, PacketStatus.InActive, 0, true);
                }

                packet = new Packet(Convert.ToInt32(row["packet_id"]), Convert.ToInt32(row["fame_type_id"]), row["name"].ToString(), Convert.ToInt32(row["points_earned"]),
                        Convert.ToInt32(row["rewards"]), Convert.ToInt32(row["time_interval"]), Convert.ToInt32(row["reward_interval"]), Convert.ToInt32(row["max_per_day"]),
                        Convert.ToInt32(row["status_id"]).Equals (1) ? PacketStatus.Active : PacketStatus.InActive,
                        Convert.ToInt32(row["badge_id"]), Convert.ToInt32(row["is_server_only"]).Equals (1)
                        );

                DbCache.Cache.Insert(cacheKey, packet, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return packet;
        }

        /// <summary>
        /// Gets list of packets
        /// </summary>
        public PagedList<Packet> GetPackets (int fameTypeId, string filter, string orderBy, string groupBy, int pageNumber, int pageSize)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strQuery = "SELECT p.packet_id, p.fame_type_id, name, " +
                " points_earned, rewards, " +
                " time_interval, reward_interval, max_per_day, status_id, badge_id, is_server_only " +
                " FROM packets p ";

            if (groupBy.Length > 0)
            {
                strQuery = "SELECT p.packet_id, p.fame_type_id, name, " +
                " SUM(points_earned) AS points_earned, SUM(rewards) AS rewards, " +
                " time_interval, reward_interval, max_per_day, status_id, badge_id, is_server_only " +
                " FROM packets p " +
                " INNER JOIN packet_history ph ON ph.packet_id = p.packet_id ";
            }
            

            if (fameTypeId > 0)
            {
                strQuery += " WHERE p.fame_type_id = @fameTypeid ";
                parameters.Add (new MySqlParameter ("@fameTypeid", fameTypeId));
            }

            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            if (groupBy.Length > 0)
            {
                strQuery += " GROUP BY " + groupBy;
            }

            PagedDataTable dt = Db.Fame.GetPagedDataTable (strQuery, orderBy, parameters, pageNumber, pageSize);

            PagedList<Packet> list = new PagedList<Packet> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new Packet (Convert.ToInt32 (row["packet_id"]), Convert.ToInt32 (row["fame_type_id"]), row["name"].ToString (), Convert.ToInt32 (row["points_earned"]),
                    Convert.ToInt32(row["rewards"]), Convert.ToInt32(row["time_interval"]), Convert.ToInt32(row["reward_interval"]), Convert.ToInt32(row["max_per_day"]),
                    Convert.ToInt32(row["status_id"]).Equals(1) ? PacketStatus.Active : PacketStatus.InActive,
                    Convert.ToInt32(row["badge_id"]), Convert.ToInt32(row["is_server_only"]).Equals(1)
                ));
            }
            return list;
        }

        /// <summary>
        /// Get a users packet history
        /// </summary>
        public PagedList<PacketHistory> GetPacketHistory (int userId, int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strQuery = "SELECT ph.packet_history_id, ph.user_id, ph.packet_id, ph.date_rewarded, " +
                " ph.points_awarded, ph.rewards_awarded, ph.fame_type_id, " +
                " p.name " +
                " FROM packet_history ph, packets p " +
                " WHERE ph.packet_id = p.packet_id " +
                " AND user_id = @userId ";

            if (fameTypeId > 0)
            {
                strQuery += " AND ph.fame_type_id = @fameTypeid ";
                parameters.Add (new MySqlParameter ("@fameTypeid", fameTypeId));
            }

            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            parameters.Add (new MySqlParameter ("@userId", userId));
            PagedDataTable dt = Db.Fame.GetPagedDataTable (strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<PacketHistory> list = new PagedList<PacketHistory> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new PacketHistory (Convert.ToInt64 (row["packet_history_id"]), Convert.ToInt32 (row["user_id"]), Convert.ToInt32 (row["packet_id"]), Convert.ToDateTime (row["date_rewarded"]),
                    Convert.ToInt32 (row["points_awarded"]), Convert.ToInt32 (row["rewards_awarded"]), Convert.ToInt32 (row["fame_type_id"]), row["name"].ToString ()
                ));
            }
            return list;
        }

        /// <summary>
        /// Get a users daily history
        /// </summary>
        public PagedList<DailyHistory> GetDailyHistoryForUser(int userId, int packetId, int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
        {
            string strQuery = "SELECT user_id, fame_type_id, packet_id, date_rewarded, packet_count " +
                " FROM fame.daily_history " +
                " WHERE user_id = @userId " +
                " AND fame_type_id = @fameTypeId " +
                " AND packet_id = @packetId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@fameTypeId", fameTypeId));
            parameters.Add(new MySqlParameter("@packetId", packetId));

            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            PagedDataTable dt = Db.Fame.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<DailyHistory> list = new PagedList<DailyHistory>();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new DailyHistory(Convert.ToInt32(row["user_id"]), Convert.ToInt32(row["packet_id"]), Convert.ToDateTime(row["date_rewarded"]),
                    Convert.ToInt32(row["fame_type_id"]), Convert.ToInt32(row["packet_count"])
                ));
            }
            return list;
        }

        /// <summary>
        /// Get a users level history
        /// </summary>
        public PagedList<LevelHistory> GetLevelHistory (int userId, int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strQuery = "SELECT lh.user_id, lh.fame_type_id, lh.level_id, lh.level_date, lh.seconds_to_level, l.level_number " +
                " FROM level_history lh, levels l " +
                " WHERE lh.level_id = l.level_id " +
                " AND lh.user_id = @userId ";

            if (fameTypeId > 0)
            {
                strQuery += " AND lh.fame_type_id = @fameTypeId ";
                parameters.Add (new MySqlParameter ("@fameTypeId", fameTypeId));
            }

            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            parameters.Add (new MySqlParameter ("@userId", userId));
            PagedDataTable dt = Db.Fame.GetPagedDataTable (strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<LevelHistory> list = new PagedList<LevelHistory> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new LevelHistory (Convert.ToInt32 (row["user_id"]), Convert.ToInt32 (row["fame_type_id"]), Convert.ToInt32 (row["level_id"]), Convert.ToDateTime (row["level_date"]),
                    Convert.ToInt64 (row["seconds_to_level"]), Convert.ToInt32 (row["level_number"])
                ));
            }
            return list;
        }

        /// <summary>
        /// GetOneTimeHistory
        /// </summary>
        public OnetimeHistory GetOneTimeHistory (int userId, int packetId)
        {
            // If found cache for a while
            string cacheKey = "fame.onetime_history." + userId + "." + packetId;
            OnetimeHistory otHistory = (OnetimeHistory)DbCache.Cache[cacheKey];

            if (otHistory == null)
            {
                string strQuery = "SELECT user_id, fame_type_id, packet_id, date_rewarded " +
                 " FROM onetime_history " +
                 " WHERE packet_id = @packetId " +
                 " AND user_id = @userId ";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@userId", userId));
                parameters.Add(new MySqlParameter("@packetId", packetId));
                DataRow row = Db.Fame.GetDataRow(strQuery, parameters);

                if (row == null)
                {
                    return null;
                }

                otHistory = new OnetimeHistory (Convert.ToInt32 (row["user_id"]), Convert.ToInt32 (row["fame_type_id"]), Convert.ToInt32 (row["packet_id"]),
                    Convert.ToDateTime (row["date_rewarded"]));

                DbCache.Cache.Insert(cacheKey, otHistory, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return otHistory;
        }

        /// <summary>
        /// AwardFame
        /// </summary>
        public void AwardFame (int packetId, int userId, ref int returnCode, ref int levelId)
        {

            string strQuery = "CALL AwardFame(@packetId, @userId, @returnCode, @levelId);SELECT CAST(@returnCode as UNSIGNED INT) as returnCode, CAST(@levelId as UNSIGNED INT) as levelId;";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@packetId", packetId));
            DataRow row = Db.Fame.GetDataRow (strQuery, parameters);

            returnCode = Convert.ToInt32 (row["returnCode"]);
            levelId = Convert.ToInt32 (row["levelId"]);

            // Invalidate it
            Packet packet = GetPacket (packetId);
            CentralCache.Remove(CentralCache.keyFameUserFame + userId.ToString() + packet.FameTypeId.ToString());
        }

        /// <summary>
        /// Gets list of packets
        /// </summary>
        public PagedList<Nudge> GetChecklist (int userId, int checklistId, string filter, string orderby, int pageNumber, int pageSize)
        {            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strQuery = "SELECT p.packet_id, p.rewards, n.name, n.nudge_id, n.title, n.description, n.url_text, n.url, n.impressions, " +
                " oh.date_rewarded, ncp.name as checklist_text, n.status_id " +
                " FROM packets p " +
                " INNER JOIN nudge_checklist_packets ncp ON ncp.packet_id = p.packet_id " +
                " INNER JOIN nudges n ON n.packet_id = p.packet_id " +
                " LEFT JOIN onetime_history oh ON oh.packet_id = p.packet_id AND oh.user_id = @userId " +
                " WHERE ncp.nudge_checklist_id = @checklistId ";

            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@checklistId", checklistId));

            PagedDataTable dt = Db.Fame.GetPagedDataTable (strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<Nudge> list = new PagedList<Nudge> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new Nudge (Convert.ToInt32 (row["nudge_id"]), row["name"].ToString (), row["title"].ToString (),
                    row["description"].ToString (), row["url_text"].ToString (), row["url"].ToString (),
                    Convert.ToInt32(row["packet_id"]), (row["date_rewarded"] != DBNull.Value), row["checklist_text"].ToString(),
                    Convert.ToInt32(row["rewards"]), Convert.ToInt32(row["impressions"]),
                    Convert.ToInt32(row["status_id"]).Equals (1) ? NudgeStatus.Active : NudgeStatus.InActive
                ));
            }
            return list;
        }

        /// <summary>
        /// Get a checklist Id
        /// </summary>
        public int GetChecklistIdByPacketId (int packetId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strQuery = "SELECT ncp.nudge_checklist_id " +
                " FROM nudge_checklist_packets ncp " +
                " WHERE packet_id = @packetId " +
                " LIMIT 1 ";

            parameters.Add (new MySqlParameter ("@packetId", packetId));
            DataRow row = Db.Fame.GetDataRow (strQuery, parameters);

            if (row == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32 (row["nudge_checklist_id"]);
            }
        }

        /// <summary>
        /// Gets list of nudges
        /// </summary>
        public PagedList<Nudge> GetNudges (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strQuery = "SELECT nudge_id, name, title, description, url_text, url, packet_id, impressions, status_id " +
                " FROM nudges n " +
                " WHERE n.packet_id NOT IN (SELECT packet_id FROM onetime_history WHERE user_id = @userId); ";

            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            parameters.Add (new MySqlParameter ("@userId", userId));

            PagedDataTable dt = Db.Fame.GetPagedDataTable (strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<Nudge> list = new PagedList<Nudge> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new Nudge (Convert.ToInt32 (row["nudge_id"]), row["name"].ToString (),
                    row["title"].ToString (), row["description"].ToString (), row["url_text"].ToString (),
                    row["url"].ToString(), Convert.ToInt32(row["packet_id"]), false, "",
                    -1, Convert.ToInt32 (row["impressions"]),
                    Convert.ToInt32(row["status_id"]).Equals (1) ? NudgeStatus.Active : NudgeStatus.InActive
                ));
            }
            return list;
        }

        

        /// <summary>
        /// Gets whether user has completed checklist
        /// </summary>
        public bool IsChecklistComplete (int userId, int checklistId)
        {
            string sqlSelect = "SELECT user_id " +
                " FROM checklist_completions " +
                "WHERE user_id = @userId " +
                " AND nudge_checklist_id = @checklistId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@checklistId", checklistId));

            DataRow drCompleteChecklist = Db.Fame.GetDataRow (sqlSelect, parameters);
            return (drCompleteChecklist != null);
        }

        /// <summary>
        /// Gets whether user has completed checklist
        /// </summary>
        public bool IsChecklistCompleteLongWay (int userId, int checklistId)
        {
            string strQuery = "SELECT p.packet_id, n.name, n.nudge_id, oh.date_rewarded " +
               " FROM packets p " +
               " INNER JOIN nudge_checklist_packets ncp ON ncp.packet_id = p.packet_id " +
               " INNER JOIN nudges n ON n.packet_id = p.packet_id " +
               " LEFT JOIN onetime_history oh ON oh.packet_id = p.packet_id AND oh.user_id = @userId " +
               " WHERE ncp.nudge_checklist_id = @checklistId " +
               " AND date_rewarded IS NULL ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@checklistId", checklistId));

            PagedDataTable dt = Db.Fame.GetPagedDataTable (strQuery, "", parameters, 1, 1);
            return (dt.TotalCount.Equals (0));
        }

        /// <summary>
        /// InsertChecklistComplete
        /// </summary>
        public int InsertChecklistComplete (int userId, int checklistId)
        {
            string sqlString = "INSERT IGNORE INTO checklist_completions ( " +
               " user_id, nudge_checklist_id, date_completed " +
               " ) VALUES (" +
               " @userId, @checklistId, NOW())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@checklistId", checklistId));
            return Db.Fame.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// UpdateDailyHistory
        /// </summary>
        public void UpdateDailyHistory (int userId, int packetId, int fameTypeId)
        {
            string sqlString = "";

            // Only used for login to world so we can redeem 1,2,3+ day login packets
            if (packetId.Equals ((int) PacketId.WORLD_DAILY_SIGNIN))
            {
                sqlString = " INSERT IGNORE INTO fame.daily_history " +
                    " (user_id, fame_type_id, packet_id, date_rewarded, packet_count, date_last_rewarded) " +
                    " VALUES " +
                    " (@userId, @fameTypeId, @packetId, DATE(NOW()), 1, NOW())";

                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
                parameters.Add (new MySqlParameter ("@userId", userId));
                parameters.Add (new MySqlParameter ("@fameTypeId", fameTypeId));
                parameters.Add (new MySqlParameter ("@packetId", packetId));
                Db.Fame.ExecuteNonQuery (sqlString, parameters);
            }
        }
        
        /// <summary>
        /// GetRewardInterval
        /// </summary>
        public int GetRewardInterval (int userId, int packetId)
        {
            string cacheKey = CentralCache.keyFameUserFame + ".RIT." + userId.ToString() + packetId.ToString();
            String RewardIntervalTracking = (String)CentralCache.Get(cacheKey);

            if (RewardIntervalTracking != null)
            {
                return Convert.ToInt32(RewardIntervalTracking);
            }
            else
            {
                string sqlSelect = "SELECT user_id, packet_id, created_date, packet_count " +
                   " FROM fame.reward_interval_tracking " +
                   " WHERE user_id = @userId " +
                   " AND packet_id = @packetId ";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@userId", userId));
                parameters.Add(new MySqlParameter("@packetId", packetId));

                DataRow result = Db.Fame.GetDataRow(sqlSelect, parameters);

                if (result != null)
                {
                    CentralCache.Store(cacheKey, Convert.ToInt32(result["packet_count"]).ToString (), TimeSpan.FromMinutes(30));
                    return Convert.ToInt32(result["packet_count"]);
                }
                else
                {
                    // Do this first time so the update can just update cache and save a query next time
                    CentralCache.Store(cacheKey, "0", TimeSpan.FromMinutes(30));
                    return 0;
                }
            }


            //DataRow drRewardInterval = GetRewardIntervalTracking(userId, packetId);

            //if (drRewardInterval != null)
            //{
            //    return Convert.ToInt32(drRewardInterval["packet_count"]);
            //}
            //else
            //{
            //    return 0;
            //}
        }

        ///// <summary>
        ///// GetRewardInterval
        ///// </summary>
        //private DataRow GetRewardIntervalTracking (int userId, int packetId)
        //{
        //    string cacheKey = CentralCache.keyFameUserFame + ".RIT." + userId.ToString() + packetId.ToString();
        //    DataRow RewardIntervalTracking = (DataRow) CentralCache.Get(cacheKey);

        //    if (RewardIntervalTracking != null)
        //    {
        //        // Found in cache
        //        return RewardIntervalTracking;
        //    }

        //    string sqlSelect = "SELECT user_id, packet_id, created_date, packet_count " +
        //        " FROM fame.reward_interval_tracking " +
        //        " WHERE user_id = @userId " +
        //        " AND packet_id = @packetId ";

        //    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
        //    parameters.Add(new MySqlParameter("@userId", userId));
        //    parameters.Add(new MySqlParameter("@packetId", packetId));
            
        //    DataRow result = Db.Fame.GetDataRow(sqlSelect, parameters);

        //    if (result != null)
        //    {
        //        CentralCache.Store(cacheKey, result, TimeSpan.FromMinutes(30));
        //    }

        //    return result;
        //}

        /// <summary>
        /// UpdateRewardInterval
        /// </summary>
        public void UpdateRewardInterval (int userId, int packetId)
        {
            string sqlString = "";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            // Optimize to take out a read
            // Insert
            sqlString = " INSERT INTO fame.reward_interval_tracking " +
              " (user_id, packet_id, created_date, packet_count) " +
              " VALUES " +
              " (@userId, @packetId, DATE(NOW()), 1) " +
              " ON DUPLICATE KEY UPDATE packet_count = packet_count + 1 ";
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@packetId", packetId));
            Db.Fame.ExecuteNonQuery(sqlString, parameters);


            // Update cache to save a db call later, and don't invalidate cache since we just updated it
            string cacheKey = CentralCache.keyFameUserFame + ".RIT." + userId.ToString() + packetId.ToString();
            String RewardIntervalTracking = (String)CentralCache.Get(cacheKey);

            if (RewardIntervalTracking != null)
            {
                int newCount = Convert.ToInt32(RewardIntervalTracking) + 1;
                CentralCache.Store(cacheKey, newCount.ToString(), TimeSpan.FromMinutes(30));
            }
            //CentralCache.Remove(cacheKey);

            //// Does record already exist?
            //if (GetRewardIntervalTracking(userId, packetId) != null)
            //{
            //    // Update count
            //    sqlString = " UPDATE fame.reward_interval_tracking " +
            //      " SET packet_count = packet_count + 1 " +
            //      " WHERE user_id = @userId " +
            //      " AND packet_id = @packetId ";
            //    parameters.Add(new MySqlParameter("@userId", userId));
            //    parameters.Add(new MySqlParameter("@packetId", packetId));
            //    Db.Fame.ExecuteNonQuery(sqlString, parameters);
            //}
            //else
            //{
            //    // Insert
            //    sqlString = " INSERT INTO fame.reward_interval_tracking " +
            //      " (user_id, packet_id, created_date, packet_count) " +
            //      " VALUES " +
            //      " (@userId, @packetId, DATE(NOW()), 1)";
            //    parameters.Add(new MySqlParameter("@userId", userId));
            //    parameters.Add(new MySqlParameter("@packetId", packetId));
            //    Db.Fame.ExecuteNonQuery(sqlString, parameters);
            //}

        }

        /// <summary>
        /// GetRewardIntervalDaily
        /// </summary>
        public int GetRewardIntervalDaily (int userId, int packetId)
        {
            string cacheKey = CentralCache.keyFameDailyRewardTracking + "." + userId + "." + packetId;

            // Handle bad cache values as a quick fix
            try
            {
                String strDailyCount = (String)CentralCache.Get(cacheKey);

                if (strDailyCount != null)
                {
                    // Found in cache
                    return Convert.ToInt32(strDailyCount);
                }
            }
            catch {}

            return 0;
        }

        /// <summary>
        /// UpdateRewardIntervalDaily
        /// </summary>
        public void UpdateRewardIntervalDaily (int userId, int packetId)
        {
            DateTime dtNow = DateTime.Now;

            string cacheKey = CentralCache.keyFameDailyRewardTracking + "." + userId + "." + packetId;

            int currentCount = GetRewardIntervalDaily (userId, packetId);
            currentCount++;

            // Update count in the cache
            // Store daily interval untill midnight tonight
            DateTime dtMidnight = new DateTime (dtNow.Year, dtNow.Month, dtNow.Day, 23, 59, 59);
            TimeSpan tsDifference = dtMidnight - dtNow;

            int totalMinutes = tsDifference.Minutes + tsDifference.Hours * 60;

            // Just in case they are last minute of the day, don't set expires to 0!!!
            if (totalMinutes < 1)
            {
                totalMinutes = 1;
            }

            CentralCache.Store(cacheKey, currentCount.ToString(), TimeSpan.FromMinutes(totalMinutes));
        }

        /// <summary>
        /// InsertDanceToLevel10
        /// </summary>
        public int InsertDanceToLevel10 (int userId)
        {
            string sqlString = "REPLACE INTO wok.daily_dance_levels ( " +
               " user_id, level_id, created_date " +
               " ) VALUES (" +
               " @userId, 10, NOW())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            return Db.WOK.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// GetYesterdayWebLogins
        /// </summary>
        public List<User> GetYesterdayWebLogins ()
        {
            string strQuery = "SELECT user_id FROM users WHERE last_login > DATE(DATE_ADD(NOW(), INTERVAL -1 DAY)) AND last_login < DATE(NOW())";
            DataTable dt = Db.Stats.GetDataTable (strQuery);

            User user;
            List<User> list = new List<User> ();

            foreach (DataRow row in dt.Rows)
            {
                user = new User ();
                user.UserId = Convert.ToInt32 (row["user_id"]);
                list.Add (user);
            }
            return list;
        }

        /// <summary>
        /// GetYesterdayWOKLogins
        /// </summary>
        public List<User> GetYesterdayWOKLogins ()
        {
            string strQuery = "SELECT user_id as kaneva_user_id" +
                " FROM developer.login_log ll" +
                " WHERE ll.created_date > DATE(DATE_ADD(NOW(), INTERVAL -1 DAY)) AND ll.created_date < DATE(NOW()) " +
                " GROUP BY user_id";
            DataTable dt = Db.Stats.GetDataTable (strQuery);

            User user;
            List<User> list = new List<User> ();

            foreach (DataRow row in dt.Rows)
            {
                user = new User ();
                user.UserId = Convert.ToInt32 (row["kaneva_user_id"]);
                list.Add (user);
            }
            return list;
        }

        /// <summary>
        /// GetYesterday10MinsInWorld
        /// </summary>
        public List<User> GetYesterday10MinsInWorld ()
        {
            //string strQuery = "SELECT user_id FROM wok.time_played " +
            //    " WHERE date_created > DATE(DATE_ADD(NOW(), INTERVAL -1 DAY)) AND date_created < DATE(NOW()) AND time_played > 10 " +
            //    " GROUP BY user_id ";
            string strQuery = "SELECT user_id as kaneva_user_id, SUM(TIME_TO_SEC(TIMEDIFF(ll.logout_date,ll.created_date))) as time_played " +
                " FROM developer.login_log ll" +
                " WHERE logout_date is not null " +
                " AND ll.created_date > DATE(DATE_ADD(NOW(), INTERVAL -1 DAY)) " +
                " AND ll.created_date < DATE(NOW()) " +
                " GROUP BY ll.user_id " +
                " HAVING time_played > 600 ";
            DataTable dt = Db.Stats.GetDataTable (strQuery);

            User user;
            List<User> list = new List<User> ();

            foreach (DataRow row in dt.Rows)
            {
                user = new User ();
                user.UserId = Convert.ToInt32(row["kaneva_user_id"]);
                list.Add (user);
            }
            return list;
        }

        public List<User> GetYesterday3HoursInWorld ()
        {
            string strQuery = "SELECT user_id as kaneva_user_id, SUM(TIME_TO_SEC(TIMEDIFF(ll.logout_date,ll.created_date))) as time_played " +
                " FROM developer.login_log ll" +
                " WHERE logout_date is not null " +
                " AND ll.created_date > DATE(DATE_ADD(NOW(), INTERVAL -1 DAY)) " +
                " AND ll.created_date < DATE(NOW()) " +
                " GROUP BY ll.user_id " +
                " HAVING time_played > 10800 ";
            DataTable dt = Db.Stats.GetDataTable (strQuery);

            User user;
            List<User> list = new List<User> ();

            foreach (DataRow row in dt.Rows)
            {
                user = new User ();
                user.UserId = Convert.ToInt32(row["kaneva_user_id"]);
                list.Add (user);
            }
            return list;
        }

        /// <summary>
        /// GetYesterdayDanceLevel10
        /// </summary>
        public List<User> GetYesterdayDanceLevel10 ()
        {
            string strQuery = "SELECT user_id FROM wok.daily_dance_levels dfl WHERE " +
                " level_id > 9 AND created_date > DATE(DATE_ADD(NOW(), INTERVAL -2 DAY)) AND created_date < DATE(NOW())";
            DataTable dt = Db.Stats.GetDataTable (strQuery);

            User user;
            List<User> list = new List<User> ();

            foreach (DataRow row in dt.Rows)
            {
                user = new User ();
                user.UserId = Convert.ToInt32 (row["user_id"]);
                list.Add (user);
            }
            return list;
        }

        /// <summary>
        /// GetYesterdayVisitForum
        /// </summary>
        public List<User> GetYesterdayVisitForum ()
        {
            string strQuery = "SELECT user_id " +
                " FROM kaneva.forum_visits " +
                " WHERE created_date > DATE(DATE_ADD(NOW(), INTERVAL -1 DAY)) AND created_date < DATE(NOW())";
            DataTable dt = Db.Stats.GetDataTable (strQuery);

            User user;
            List<User> list = new List<User> ();

            foreach (DataRow row in dt.Rows)
            {
                user = new User ();
                user.UserId = Convert.ToInt32 (row["user_id"]);
                list.Add (user);
            }
            return list;
        }

        /// <summary>
        /// GetYesterdayRead3Posts
        /// </summary>
        public List<User> GetYesterdayRead3Posts ()
        {
            string strQuery = "SELECT user_id " +
                " FROM kaneva.forum_post_views " +
                " WHERE created_date > DATE(DATE_ADD(NOW(), INTERVAL -1 DAY)) AND created_date < DATE(NOW()) AND number_of_views > 2";
            DataTable dt = Db.Stats.GetDataTable (strQuery);

            User user;
            List<User> list = new List<User> ();

            foreach (DataRow row in dt.Rows)
            {
                user = new User ();
                user.UserId = Convert.ToInt32 (row["user_id"]);
                list.Add (user);
            }
            return list;
        }

        /// <summary>
        /// GetYesterdayView3Media
        /// </summary>
        public List<User> GetYesterdayView3Media ()
        {
            string strQuery = "SELECT user_id, COUNT(asset_id) as view_count FROM kaneva.asset_views " +
                " WHERE created_date > DATE(DATE_ADD(NOW(), INTERVAL -1 DAY)) AND created_date < DATE(NOW()) " +
                " GROUP BY user_id HAVING view_count > 2";

            DataTable dt = Db.Audit.GetDataTable (strQuery);

            User user;
            List<User> list = new List<User> ();

            foreach (DataRow row in dt.Rows)
            {
                user = new User ();
                user.UserId = Convert.ToInt32 (row["user_id"]);
                list.Add (user);
            }
            return list;
        }

        /// <summary>
        /// GetFamePointsFromInvitingFriends
        /// </summary>
        public DataRow GetFamePointsFromInvitingFriends (int userId)
        {
            string strQuery = "SELECT COALESCE(SUM(points_awarded), 0) AS total_points, COALESCE(SUM(rewards_awarded), 0) AS total_rewards " + 
                "FROM fame.packet_history " +
                "WHERE user_id = @userId " +
                "AND fame_type_id = 1 " +
                "AND packet_id in (12,13,14,32,33,34); ";

            return Db.Slave.GetDataRow (strQuery);
        }

        public DataRow GetUserLastDailyLoginPacketRedemption (int userId)
        {
            string sqlSelect = "SELECT user_id, fame_type_id, packet_id, date_rewarded, packet_count, date_last_rewarded " +
                " FROM fame.daily_history " +
                " WHERE user_id = @userId " +
                " AND packet_id = " + (int) PacketId.WORLD_DAILY_SIGNIN +
                " AND date_last_rewarded < NOW() " +
                " ORDER BY date_rewarded DESC LIMIT 1";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            
            return Db.Master.GetDataRow (sqlSelect.ToString (), parameters);
        }

        /// <summary>
        /// Gets list of packets
        /// </summary>
        public Checklist GetChecklist (int checklistId, int userId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strQuery = "SELECT nc.nudge_checklist_id, name, reward_packet_id, IF(cc.nudge_checklist_id>0,1,0) AS completed " +
                " FROM nudge_checklists nc " +
                " LEFT OUTER JOIN checklist_completions cc ON cc.nudge_checklist_id = nc.nudge_checklist_id AND cc.user_id = @userId " +
                " WHERE nc.nudge_checklist_id = @checklistId;";

            parameters.Add (new MySqlParameter ("@checklistId", checklistId));
            parameters.Add (new MySqlParameter ("@userId", userId));

            DataRow row = Db.Fame.GetDataRow (strQuery, parameters);

            if (row == null)
            {
                return null;
            }

            return new Checklist(Convert.ToInt32 (row["nudge_checklist_id"]),
                row["name"].ToString (), Convert.ToInt32 (row["reward_packet_id"]), (Convert.ToInt32 (row["completed"])).Equals (1)
                );
        }

        public List<ChecklistPacket> GetChecklistPackets (int userId, int checklistId, string orderBy)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strQuery = "SELECT ncp.packet_id, p.fame_type_id, ncp.name, points_earned, rewards, time_interval, " +
                " reward_interval, max_per_day, status_id, badge_id, is_server_only, " +
                " percentage, IF(oh.packet_id>0,1,0) AS completed, sort_order " +
                " FROM fame.nudge_checklist_packets ncp " +
                " INNER JOIN packets p ON p.packet_id = ncp.packet_id " +
                " LEFT OUTER JOIN fame.onetime_history oh ON oh.packet_id = ncp.packet_id AND oh.user_id = @userId " +
                " WHERE nudge_checklist_id = @checklistId ";

            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@checklistId", checklistId));

            PagedDataTable dt = Db.Fame.GetPagedDataTable (strQuery, orderBy, parameters, 1, 100);

            List<ChecklistPacket> list = new List<ChecklistPacket> ();

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new ChecklistPacket (Convert.ToInt32 (row["packet_id"]), Convert.ToInt32 (row["fame_type_id"]),
                    row["name"].ToString (), Convert.ToInt32 (row["points_earned"]), Convert.ToInt32 (row["rewards"]),
                    Convert.ToInt32 (row["time_interval"]), Convert.ToInt32 (row["reward_interval"]), Convert.ToInt32 (row["max_per_day"]),
                    (PacketStatus) Convert.ToInt32 (row["status_id"]), Convert.ToInt32 (row["badge_id"]), (Convert.ToInt32 (row["is_server_only"])).Equals (1),
                    Convert.ToDecimal (row["percentage"]), (Convert.ToInt32 (row["completed"])).Equals (1)
                ));
            }
            return list;
        }

        public PagedList<Nudge> GetChecklistNudges(int userId, int checklistId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string strQuery = "SELECT n.nudge_id, n.name, n.title, n.description, n.url_text, n.url, n.packet_id, n.impressions, n.status_id, COALESCE(oh.packet_id, 0, 1) as completed  " +
                " FROM fame.nudge_checklist_packets ncp, fame.nudges n  " +
                " LEFT OUTER JOIN fame.onetime_history oh ON oh.packet_id = n.packet_id AND oh.user_id = @userId " +
                " WHERE ncp.packet_id = n.packet_id " +
                " AND nudge_checklist_id = @checklistId ";

            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@checklistId", checklistId));

            PagedDataTable dt = Db.Fame.GetPagedDataTable(strQuery, "ncp.sort_order desc", parameters, 1, 100);

            PagedList<Nudge> list = new PagedList<Nudge>();

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Nudge(Convert.ToInt32(row["nudge_id"]), row["name"].ToString(),
                   row["title"].ToString(), row["description"].ToString(), row["url_text"].ToString(),
                   row["url"].ToString(), Convert.ToInt32(row["packet_id"]), Convert.ToInt32(row["completed"]) > 0 ? true : false, "",
                   -1, Convert.ToInt32(row["impressions"]),
                   Convert.ToInt32(row["status_id"]).Equals(1) ? NudgeStatus.Active : NudgeStatus.InActive
               ));
            }
            return list;
        }

    }
}
