///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
	class MySQLMailDao : IMailDao
	{
		public IList<EmailTemplate> GetTemplatesByTypeId(int emailTypeId, int enabled)
		{			
			// Check cache
			List<EmailTemplate> list = (List<EmailTemplate>)CentralCache.Get(CentralCache.keyMailerEnabledTemplates + emailTypeId);			
			if (list != null)
			{
				return list;
			}
			
			// Go fetch it
			string sqlSelect = "SELECT etm.email_template_id, ety.email_type_id, ety.type_name, etm.template_name, etm.template_version, etm.template_subversion,  " +
			  " etm.reply_address, etm.template_text,	etm.template_html, etm.email_subject, etm.language_code, etm.enabled,  " +
			  " etm.footer_template_id, etm.header_template_id, etm.rotation_weight, etm.date_created, etm.description, etm.comments  " +
			  " FROM email_template etm  " +
			  " LEFT JOIN email_type ety ON etm.email_type_id = ety.email_type_id " +
			  " WHERE etm.email_type_id=@email_type_id AND etm.enabled = @enabled AND etm.deleted = 0 " +
			  " ORDER BY etm.email_type_id DESC, etm.template_version DESC";

			List<IDbDataParameter> parameters = new List<IDbDataParameter>();
			parameters.Add(new MySqlParameter("@email_type_id", emailTypeId));
			parameters.Add(new MySqlParameter("@enabled", enabled));

			DataTable dt = Db.EmailViral.GetDataTable(sqlSelect, parameters);


			list = new List<EmailTemplate>();

			int emailTemplateId;
			string emailSubject;
			string templateHtml;
			int rotationWeight;
			
			foreach (DataRow row in dt.Rows)
			{
				emailTemplateId = Convert.ToInt32(row["email_template_id"]);
				emailSubject = row["email_subject"].ToString();
				templateHtml = row["template_html"].ToString();
				rotationWeight = Convert.ToInt32(row["rotation_weight"]);

				EmailTemplate emailTempl = new EmailTemplate(emailTemplateId, emailSubject, templateHtml, "", "", "", "", "", rotationWeight);
				try
				{
					list.Add(emailTempl);
				}
				catch
				{

				}

			}

			// Cache the templates
			CentralCache.Store(CentralCache.keyMailerEnabledTemplates + emailTypeId, list, TimeSpan.FromMinutes(5));

			return list;
		}

	}
}
