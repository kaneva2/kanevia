///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using log4net;
using Sphinx.Client.Commands.Search;
using Sphinx.Client.Connections;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLCommunityDao : ICommunityDao
    {

        /// <summary>
        /// GetCommunity
        /// </summary>
        public Community GetCommunity(int communityId)
        {
            string sqlSelect = " SELECT c.community_id, c.place_type_id, c.name_no_spaces, c.url, c.creator_id, " +
                " c.created_date, c.last_edit, c.community_type_id, c.status_id, c.email, c.is_public, c.is_adult, c.thumbnail_path, " +
                " c.thumbnail_small_path, c.thumbnail_medium_path, c.thumbnail_large_path, c.thumbnail_xlarge_path, " +
                " c.thumbnail_type, c.allow_member_events, c.is_personal, c.template_id, c.over_21_required, c.has_thumbnail, " +
                " c.name, c.description, c.background_rgb, c.background_image, c.creator_username, c.keywords, c.allow_publishing, " +
                " u.zip_code, u.age, u.gender, u.country, u.location, u.mature_profile, us.number_of_friends " +
                " FROM communities c " +
                " INNER JOIN users u ON u.user_id = c.creator_id " +
                " INNER JOIN users_stats us ON us.user_id = c.creator_id " +
                " WHERE c.status_id = @statusId " +
                " AND c.community_id = @communityId ";

			List<IDbDataParameter> parameters = new List<IDbDataParameter>();
	        parameters.Add(new MySqlParameter("@communityId", communityId));
            parameters.Add(new MySqlParameter("@statusId", (int) CommunityStatus.ACTIVE));
            DataRow row = Db.Master.GetDataRow(sqlSelect, parameters);

            if (row == null)
            {
                Community communityBlank = new Community();
                communityBlank.Stats = new CommunityStats();
                communityBlank.Preferences = new CommunityPreferences();
                communityBlank.WOK3App = new WOK3DApp ();
                return communityBlank;
            }
            
            Community community = new Community (Convert.ToInt32(row["community_id"]), row["name_no_spaces"].ToString(), 
                row["url"].ToString(), Convert.ToInt32(row["creator_id"]), Convert.ToDateTime(row["created_date"]), Convert.ToDateTime(row["last_edit"]), 
                Convert.ToInt32(row["status_id"]), row["email"].ToString(), row["is_public"].ToString(), row["is_adult"].ToString(), row["thumbnail_path"].ToString(), 
                row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_xlarge_path"].ToString(),
                row["thumbnail_type"].ToString(), Convert.ToInt32(row["allow_publishing"]), Convert.ToInt32(row["allow_member_events"]), Convert.ToInt32(row["is_personal"]), Convert.ToUInt32(row["template_id"]), 
                row["over_21_required"].ToString().Equals("Y"), row["has_thumbnail"].ToString().Equals("Y"), row["name"].ToString(), row["description"].ToString(), 
                row["background_rgb"].ToString(), row["background_image"].ToString(), row["creator_username"].ToString(), row["keywords"].ToString(), row["zip_code"].ToString(), 
                Convert.ToInt32(row["age"]), row["gender"].ToString(), row["country"].ToString(), row["location"].ToString(), row["mature_profile"].ToString().Equals("Y"),
                Convert.ToInt32(row["number_of_friends"]), Convert.ToInt32(row["place_type_id"]), Convert.ToInt32(row["community_type_id"]));

            return community;
        }

		/// <summary>
        /// GetCommunities
        /// </summary>
		public PagedList<Community> GetCommunities(int userId, int[] communityTypeIds, string orderBy, string filter, int pageNumber, int pageSize)
		{
			return GetCommunities( userId,  communityTypeIds,  orderBy,  filter,  pageNumber,  pageSize, false, "");
		}

        /// <summary>
        /// GetCommunities
        /// </summary>
		public PagedList<Community> GetCommunities(int userId, int[] communityTypeIds, string orderBy, string filter, int pageNumber, int pageSize, bool with3dAppInfo, string nameFilter)
        {
            string sqlSelect = " SELECT c.community_id, c.place_type_id, c.name_no_spaces, c.url, c.creator_id, " +
                " c.created_date, c.last_edit, c.community_type_id, c.status_id, c.email, c.is_public, c.is_adult, c.thumbnail_path, " +
                " c.thumbnail_small_path, c.thumbnail_medium_path, c.thumbnail_large_path, c.thumbnail_xlarge_path, " +
                " c.thumbnail_type, c.allow_member_events, c.is_personal, c.template_id, c.over_21_required, c.has_thumbnail, " +
                " c.name, c.description, c.background_rgb, c.background_image, c.creator_username, c.keywords, c.allow_publishing, " +
                " cs.number_of_views, cs.number_of_members, cs.number_of_diggs, " +
                " cs.number_times_shared, cs.number_of_pending_members, cs.number_posts_7_days, " +
                " u.zip_code, u.age, u.gender, u.country, u.location, u.mature_profile, us.number_of_friends";
			
            string sqlTables = " FROM communities c " +
                " INNER JOIN channel_stats cs ON c.community_id = cs.channel_id " +
                " INNER JOIN users u ON u.user_id = c.creator_id " +
                " INNER JOIN users_stats us ON us.user_id = c.creator_id ";

            string sqlWhere = " WHERE c.status_id = @statusId ";
			


            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@statusId", (int)CommunityStatus.ACTIVE));
            
            if (communityTypeIds.Length > 0)
            {
                sqlWhere += " AND c.community_type_id IN (";
                for(int i = 0; i < communityTypeIds.Length; i++)
                {
                    sqlWhere += "@communityTypeId" + i + ",";
                    parameters.Add(new MySqlParameter("@communityTypeId" + i, communityTypeIds[i]));
                }
                sqlWhere = sqlWhere.TrimEnd(',') + ") ";
            }

            //limits results to be just for a particular user and active memberships
            if (userId > 0)
            {
                sqlSelect += ", cm.account_type_id ";
                sqlTables += " INNER JOIN community_members cm ON c.community_id = cm.community_id ";
                sqlWhere += " AND cm.user_id = @userId AND cm.status_id = @memberStatus ";

                parameters.Add(new MySqlParameter("@memberStatus", (UInt32)CommunityMember.CommunityMemberStatus.ACTIVE));
                parameters.Add(new MySqlParameter("@userId", userId));
            }


			if (with3dAppInfo)
			{
                sqlSelect += ", COALESCE(g.game_status_id,0) as game_status_id,  COALESCE(g.game_id,0) as game_id,  COALESCE(g.game_access_id,0) as game_access_id, COALESCE(gs.server_status_id,0), COALESCE( TIMESTAMPDIFF(MINUTE,`gs`.`last_ping_datetime`,NOW()),0 ) AS minutes_from_last_ping ";
                sqlSelect += ", SUM(CASE gs.server_status_id WHEN 1 THEN 1 ELSE 0 END) AS game_server_is_on_sum, COALESCE(g.incubator_hosted,0) as incubator_hosted ";

				sqlTables += " LEFT JOIN kaneva.communities_game cg on c.community_id = cg.community_id " +
				" LEFT JOIN developer.games g on g.game_id = cg.game_id " +
				" LEFT JOIN developer.game_servers gs on g.game_id = gs.game_id ";

			}

            if (nameFilter.Length > 0)
            {
                sqlWhere += " AND (name_no_spaces LIKE @nameFilter OR name LIKE @nameFilter) ";
                parameters.Add(new MySqlParameter("@nameFilter", nameFilter));
            }

            string sql = sqlSelect + sqlTables + sqlWhere;

            if (filter.Length > 0)
            {
                sql += " AND " + filter;
            }

			if (with3dAppInfo)
			{
				sql += " GROUP BY c.community_id ";
			}


            PagedDataTable pdt = Db.Master.GetPagedDataTable(sql, orderBy, parameters, pageNumber, pageSize);

            PagedList<Community> list = new PagedList<Community>();
            //assigns total count
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                Community community = new Community(Convert.ToInt32(row["community_id"]), row["name_no_spaces"].ToString(),
                    row["url"].ToString(), Convert.ToInt32(row["creator_id"]), Convert.ToDateTime(row["created_date"]), Convert.ToDateTime(row["last_edit"]),
                    Convert.ToInt32(row["status_id"]), row["email"].ToString(), row["is_public"].ToString(), row["is_adult"].ToString(), row["thumbnail_path"].ToString(),
                    row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_xlarge_path"].ToString(),
                    row["thumbnail_type"].ToString(), Convert.ToInt32(row["allow_publishing"]), Convert.ToInt32(row["allow_member_events"]), Convert.ToInt32(row["is_personal"]), Convert.ToUInt32(row["template_id"]),
                    row["over_21_required"].ToString().Equals("Y"), row["has_thumbnail"].ToString().Equals("Y"), row["name"].ToString(), row["description"].ToString(),
                    row["background_rgb"].ToString(), row["background_image"].ToString(), row["creator_username"].ToString(), row["keywords"].ToString(), row["zip_code"].ToString(),
                    Convert.ToInt32(row["age"]), row["gender"].ToString(), row["country"].ToString(), row["location"].ToString(), row["mature_profile"].ToString().Equals("Y"),
                    Convert.ToInt32(row["number_of_friends"]), Convert.ToInt32(row["place_type_id"]), Convert.ToInt32(row["community_type_id"]));

                //added/left here due to idea that in a list the proxy would create too many query calls
                CommunityStats communityStats = new CommunityStats();
                communityStats.NumberOfViews = Convert.ToInt32(row["number_of_views"]);
                communityStats.NumberOfMembers = Convert.ToInt32(row["number_of_members"]);
                communityStats.NumberOfDiggs = Convert.ToInt32(row["number_of_diggs"]);
                communityStats.NumberTimesShared = Convert.ToInt32(row["number_times_shared"]);
                communityStats.NumberOfPendingMembers = Convert.ToInt32(row["number_of_pending_members"]);
                communityStats.NumberPosts7Days = Convert.ToUInt32(row["number_posts_7_days"]);

                community.Stats = communityStats;

                //community.Stats = new CommunityStats(Convert.ToInt32(row["community_id"]), Convert.ToInt32(row["number_of_views"]), Convert.ToInt32(row["number_of_members"]), Convert.ToInt32(row["number_of_diggs"]),
                //    Convert.ToInt32(row["number_times_shared"]), Convert.ToInt32(row["number_of_pending_members"]), Convert.ToUInt32(row["number_posts_7_days"]),
                //    Convert.ToUInt32(row["game_count"]), Convert.ToUInt32(row["video_count"]), Convert.ToUInt32(row["music_count"]),
                //    Convert.ToUInt32(row["photo_count"]), Convert.ToUInt32(row["pattern_count"]), Convert.ToUInt32(row["tv_count"]),
                //    Convert.ToUInt32(row["widget_count"]));

				if (with3dAppInfo)
				{
					WOK3DApp wok3DApp = new WOK3DApp();
					wok3DApp.GameStatusId = Convert.ToInt32(row["game_status_id"]);
                    wok3DApp.GameAccessId = Convert.ToInt32(row["game_access_id"]);

					wok3DApp.ServerStatusId = 0;
					// if game_server_is_on_sum is more than 0 at least one sever is up and running
					if (Convert.ToInt32(row["game_server_is_on_sum"]) > 0)
					{
						wok3DApp.ServerStatusId = 1;
					}
					
					wok3DApp.MinutesFromLastPing = Convert.ToInt32(row["minutes_from_last_ping"]);
                    wok3DApp.GameId = Convert.ToInt32(row["game_id"]);
                    wok3DApp.IsIncubatorHosted = Convert.ToInt32(row["incubator_hosted"]).Equals(1);

					community.WOK3App = wok3DApp;

				}

                if (userId > 0)
                {
                    //create new member object to hold the member information
                    CommunityMember member = new CommunityMember();

                    //populate the queried member data
                    member.AccountTypeId = Convert.ToInt32(row["account_type_id"]);

                    //create the member list and add this member to the list
                    List<CommunityMember> memberList = new List<CommunityMember>();
                    memberList.Add(member);

                    //add list to the community
                    community.MemberList = memberList;
                }

                list.Add(community);
            }

            return list;
        }

        /// <summary>
        /// GetApartmentCommunity
        /// </summary>
        public Community GetApartmentCommunity(int zoneInstanceId)
        {
            string sqlSelect = " SELECT c.community_id, c.place_type_id, c.name_no_spaces, c.url, c.creator_id, " +
                " c.created_date, c.last_edit, c.community_type_id, c.status_id, c.email, c.is_public, c.is_adult, c.thumbnail_path, " +
                " c.thumbnail_small_path, c.thumbnail_medium_path, c.thumbnail_large_path, c.thumbnail_xlarge_path, " +
                " c.thumbnail_type, c.allow_member_events, c.is_personal, c.template_id, c.over_21_required, c.has_thumbnail, " +
                " c.name, c.description, c.background_rgb, c.background_image, c.creator_username, c.keywords, c.allow_publishing, " +
                " u.zip_code, u.age, u.gender, u.country, u.location, u.mature_profile, us.number_of_friends " +
                " FROM communities c " +
                " INNER JOIN users u ON u.user_id = c.creator_id AND u.wok_player_id = @zoneInstanceId " +
                " INNER JOIN users_stats us ON us.user_id = c.creator_id " +
                " WHERE c.status_id = @statusId " +
                " AND c.community_type_id = @communityTypeId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@statusId", (int)CommunityStatus.ACTIVE));
            parameters.Add(new MySqlParameter("@communityTypeId", (int)CommunityType.HOME));
            DataRow row = Db.Master.GetDataRow(sqlSelect, parameters);

            if (row == null)
            {
                Community communityBlank = new Community();
                communityBlank.Stats = new CommunityStats();
                communityBlank.Preferences = new CommunityPreferences();
                communityBlank.WOK3App = new WOK3DApp();
                return communityBlank;
            }

            Community community = new Community(Convert.ToInt32(row["community_id"]), row["name_no_spaces"].ToString(),
                row["url"].ToString(), Convert.ToInt32(row["creator_id"]), Convert.ToDateTime(row["created_date"]), Convert.ToDateTime(row["last_edit"]),
                Convert.ToInt32(row["status_id"]), row["email"].ToString(), row["is_public"].ToString(), row["is_adult"].ToString(), row["thumbnail_path"].ToString(),
                row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_xlarge_path"].ToString(),
                row["thumbnail_type"].ToString(), Convert.ToInt32(row["allow_publishing"]), Convert.ToInt32(row["allow_member_events"]), Convert.ToInt32(row["is_personal"]), Convert.ToUInt32(row["template_id"]),
                row["over_21_required"].ToString().Equals("Y"), row["has_thumbnail"].ToString().Equals("Y"), row["name"].ToString(), row["description"].ToString(),
                row["background_rgb"].ToString(), row["background_image"].ToString(), row["creator_username"].ToString(), row["keywords"].ToString(), row["zip_code"].ToString(),
                Convert.ToInt32(row["age"]), row["gender"].ToString(), row["country"].ToString(), row["location"].ToString(), row["mature_profile"].ToString().Equals("Y"),
                Convert.ToInt32(row["number_of_friends"]), Convert.ToInt32(row["place_type_id"]), Convert.ToInt32(row["community_type_id"]));

            return community;
        }


        /// <summary>
        /// Search Communities
        /// </summary>
        public PagedList<Community> SearchCommunities(bool onlyAccessPass, bool bGetMature, string searchString,
            bool bOnlyWithPhotos, string country, int pastDays, int[] communityTypeIds, 
            string orderBy, int pageNumber, int pageSize, int iPlaceTypeId)
        {
            Sphinx sphinx = new Sphinx();
            string newSearchString = "";
            string communityIds = "";
            int totalCount = 0;

            searchString = sphinx.StripSphinxSearchString(searchString);

            using (ConnectionBase connection = new PersistentTcpConnection(sphinx.Host, sphinx.Port))
            {
                connection.ConnectionTimeout = 5000;

                // Passed in search string
                if (searchString.Length > 0)
                {
                    // Escape some strings sphinx has issues with \()|-!@~/^$*
                    searchString = sphinx.CleanSphinxSearchString(searchString);

                    char[] splitter = { ' ' };
                    string[] arKeywords = null;

                    // Did they enter multiples?
                    arKeywords = searchString.Split(splitter);

                    if (arKeywords.Length > 1)
                    {
                        newSearchString = "";
                        for (int j = 0; j < arKeywords.Length; j++)
                        {
                            // Do a sphinx OR for now
                            if (j < arKeywords.Length - 1)
                            {
                                newSearchString += "@(name,description,creator_username,keywords) " + arKeywords[j].ToString() + " | ";
                            }
                            else
                            {
                                newSearchString += "@(name,description,creator_username,keywords) " + arKeywords[j].ToString();
                            }
                        }
                    }
                    else
                    {
                        newSearchString = "@(name,description,creator_username,keywords) " + searchString;
                    }
                }

                SearchQuery searchQuery = new SearchQuery(newSearchString);

                // Set match mode to SPH_MATCH_EXTENDED2
                searchQuery.MatchMode = MatchMode.Extended2;

                // Add Sphinx index name to list
                searchQuery.Indexes.Add("communities_srch_idx");

                // Set up paging
                searchQuery.Limit = pageSize;
                searchQuery.Offset = (pageNumber - 1) * pageSize;

                // Create search command object
                SearchCommand searchCommand = new SearchCommand(connection);

                // Add newly created search query object to query list
                searchCommand.QueryList.Add(searchQuery);

                // Place type filter
                if (iPlaceTypeId > 0)
                {
                    searchQuery.AttributeFilters.Add("place_type_id", (uint)iPlaceTypeId, false);
                }

                // Mature
                if (!bGetMature)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }

                    searchQuery.Query = searchQuery.Query + " @is_adult N";
                }

                if (bGetMature && onlyAccessPass)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }

                    searchQuery.Query = searchQuery.Query + " @is_adult Y";
                }

                // Filter by owner country
                if (country.Length > 0)
                {
                    // Country
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @country " + country;
                }

                // Filter by owner country
                if (bOnlyWithPhotos)
                {
                    // Country
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @has_thumbnail Y";
                }

                // Community type
                if (communityTypeIds.Length > 0)
                {
                    searchQuery.AttributeFilters.Add("community_type_id", communityTypeIds, false);
                }

                // New in last x number of days?
                if (pastDays > 0)
                {
                    searchQuery.AttributeFilters.Add("created_date", (uint)ConvertToTimestamp(DateTime.Now.AddDays(-pastDays)), (uint)ConvertToTimestamp(DateTime.Now.AddYears(1)), false);
                }

                // Sorting
                if (orderBy.Trim().Length.Equals(0))
                {
                    searchQuery.SortMode = ResultsSortMode.Relevance;

                    //name,description,creator_username,keywords   
                    //searchQuery.SortMode = ResultsSortMode.Expression;
                    searchQuery.FieldWeights.Add("name", 5);
                    searchQuery.FieldWeights.Add("description", 2);
                    searchQuery.FieldWeights.Add("creator_username", 1);
                    searchQuery.FieldWeights.Add("keywords", 1);

                }
                else
                {
                    if (orderBy.ToUpper().Contains("ASC"))
                    {
                        searchQuery.SortMode = ResultsSortMode.AttributeAscending;
                    }
                    else
                    {
                        searchQuery.SortMode = ResultsSortMode.AttributeDescending;
                    }

                    // Strip ASC, DESC
                    searchQuery.SortBy = orderBy.Replace("ASC", "").Replace("DESC", "").Trim();
                }


                // Execute command on server and obtain results
                try
                {
                    searchCommand.Execute();
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error calling Sphinx " + sphinx.Host + ":" + sphinx.Port, exc);
                }

                // Build result IN clause for MySQL
                foreach (SearchQueryResult result in searchCommand.Result.QueryResults)
                {
                    foreach (Match match in result.Matches)
                    {
                        if (communityIds.Length > 0)
                        {
                            communityIds = communityIds + "," + match.DocumentId.ToString();
                        }
                        else
                        {
                            communityIds = match.DocumentId.ToString();
                        }
                    }

                    // Note this will grab the last total count in collection, ok for this case, but not best place to do this
                    totalCount = (result.TotalFound > searchQuery.MaxMatches ? searchQuery.MaxMatches : result.TotalFound);
                }

                // Nothing found in Sphinx
                if (communityIds.Length.Equals(0))
                {
                    PagedList<Community> emptyList = new PagedList<Community>();
                    emptyList.TotalCount = 0;
                    return emptyList;
                }

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();

                string strSelect = "SELECT c.community_id, " +
                    " c.name, c.description, c.is_public, c.is_adult, c.keywords, c.over_21_required, " +
                    " c.creator_id, u.username, c.created_date, " +
                    " c.name_no_spaces, c.thumbnail_small_path, " +
                    " creator_c.name_no_spaces as creator_name_no_spaces, creator_c.thumbnail_small_path as creator_thumbnail_small_path, " +
                    " cs.number_of_members, cs.number_of_views, cs.number_times_shared, cs.number_of_diggs" +
                    " FROM communities c " +
                    " INNER JOIN kaneva.communities_personal creator_c ON c.creator_id = creator_c.creator_id AND creator_c.is_personal = 1 " +
                    " INNER JOIN channel_stats cs ON c.community_id = cs.channel_id " +
                    " INNER JOIN users u ON u.user_id = c.creator_id " +
                    " where c.community_id in " +
                    " (" + communityIds + ")" +
                    " ORDER BY FIELD(c.community_id," + communityIds + ")";

                DataTable dt = Db.Slave.GetDataTable(strSelect, parameters);

                PagedList<Community> list = new PagedList<Community>();
                list.TotalCount = Convert.ToUInt32(totalCount);
                Community community;

                foreach (DataRow row in dt.Rows)
                {
                    community = new Community();
                    community.CommunityId = Convert.ToInt32(row["community_id"]);
                    community.Name = row["name"].ToString();
                    community.Description = row["description"].ToString();
                    community.IsPublic = row["is_public"].ToString();
                    community.IsAdult = row["is_adult"].ToString();
                    community.Keywords = row["keywords"].ToString();
                    community.Over21Required = (bool) row["over_21_required"].ToString ().Equals ("Y");
                    community.CreatorId = Convert.ToInt32(row["creator_id"]);
                    community.CreatorUsername = row["username"].ToString();
                    community.CreatedDate = Convert.ToDateTime(row["created_date"]);
                    community.NameNoSpaces = row["name_no_spaces"].ToString();
                    community.ThumbnailSmallPath = row["thumbnail_small_path"].ToString();
                    community.CreatorNameNoSpaces = row["creator_name_no_spaces"].ToString();
                    community.CreatorThumbnailSmallPath = row["creator_thumbnail_small_path"].ToString();

                    CommunityStats communityStats = new CommunityStats();
                    communityStats.NumberOfMembers = Convert.ToInt32(row["number_of_members"]);
                    communityStats.NumberOfViews = Convert.ToInt32(row["number_of_views"]);
                    communityStats.NumberTimesShared = Convert.ToInt32(row["number_times_shared"]);
                    communityStats.NumberOfDiggs = Convert.ToInt32(row["number_of_diggs"]);
                    community.Stats = communityStats;

                    list.Add(community);
                }

                return list;

            }
        }

        /// <summary>
        /// method for converting a System.DateTime value to a UNIX Timestamp
        /// </summary>
        /// <param name="value">date to convert</param>
        /// <returns></returns>
        private double ConvertToTimestamp(DateTime value)
        {
            //create Timespan by subtracting the value provided from
            //the Unix Epoch
            TimeSpan span = (value - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());

            //return the total seconds (which is a UNIX timestamp)
            return (double)span.TotalSeconds;
        }


        /// <summary>
        /// GetCommunityStats
        /// </summary>
        public CommunityStats GetCommunityStats(int communityId)
        {

            CommunityStats communityStats = new CommunityStats();
            
            string sql = "SELECT cs.number_of_views, cs.number_of_members, cs.number_of_diggs, cs.number_times_shared, " +
                "cs.number_of_pending_members, cs.number_posts_7_days, cs.channel_id, cs.number_of_blogs " +
                " FROM channel_stats cs WHERE cs.channel_id = @communityId";

            List < IDbDataParameter > parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));

            DataRow row = Db.Master.GetDataRow(sql.ToString(), parameters);

            if (row != null)
            {
                communityStats.NumberOfViews = Convert.ToInt32(row["number_of_views"]);
                communityStats.NumberOfMembers = Convert.ToInt32(row["number_of_members"]);
                communityStats.NumberOfDiggs = Convert.ToInt32(row["number_of_diggs"]);
                communityStats.NumberTimesShared = Convert.ToInt32(row["number_times_shared"]);
                communityStats.NumberOfPendingMembers = Convert.ToInt32(row["number_of_pending_members"]);
                communityStats.NumberPosts7Days = Convert.ToUInt32(row["number_posts_7_days"]);
                communityStats.BlogCount = Convert.ToUInt32(row["number_of_blogs"]);
            }

            string sql2 = "SELECT games as game_count, " +
                " videos as video_count, " +
                " music as music_count, " +
                " photos as photo_count, " +
                " patterns as pattern_count, " +
                " TV as tv_count, " +
                " Widgets as widget_count, " +
                " Shares as share_count " +
                " FROM communities_assets_counts " +
                " WHERE community_id = @communityId ";

            parameters.Clear();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            DataRow row2 = Db.Master.GetDataRow(sql2.ToString(), parameters);

             if (row2 != null)
             {
                communityStats.GameCount = Convert.ToUInt32(row2["game_count"]);
                communityStats.VideoCount = Convert.ToUInt32(row2["video_count"]);
                communityStats.MusicCount = Convert.ToUInt32(row2["music_count"]);
                communityStats.PhotoCount = Convert.ToUInt32(row2["photo_count"]);
                communityStats.PatternCount = Convert.ToUInt32(row2["pattern_count"]);
                communityStats.TvCount = Convert.ToUInt32(row2["tv_count"]);
                communityStats.WidgetCount = Convert.ToUInt32(row2["widget_count"]);
             }

             communityStats.ChannelId = communityId;

             return communityStats;
        }

        /// <summary>
        /// GetCommunityPreferences
        /// </summary>
        public CommunityPreferences GetCommunityPreferences(int communityId)
        {
            CommunityPreferences communityPreferences = new CommunityPreferences();
            
            string sql = "SELECT cp.community_preference_id, cp.show_gender, cp.show_location, cp.show_age, cp.community_id " +
                           " FROM community_preferences cp WHERE cp.community_id = @communityId";

            List < IDbDataParameter > parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));

            DataRow row = Db.Master.GetDataRow(sql.ToString(), parameters);

            if (row != null)
            {
                communityPreferences.CommunityPreferenceId = Convert.ToInt32(row["community_preference_id"]);
                communityPreferences.ShowGender = Convert.ToBoolean(row["show_gender"]);
                communityPreferences.ShowLocation = Convert.ToBoolean(row["show_location"]);
                communityPreferences.ShowAge = Convert.ToBoolean(row["show_age"]);
            }

            communityPreferences.CommunityId = communityId;

            return communityPreferences;
        }

        /// <summary>
        /// Get a community member
        /// </summary>
        public CommunityMember GetCommunityMember (int communityId, int userId)
        {
            string sqlString = "SELECT cm.community_id, cm.user_id, u.username, u.display_name, cm.account_type_id, cm.added_date, cm.status_id, " +
                " cm.newsletter, COALESCE(cm.invited_by_user_id, 0) as invited_by_user_id, cm.allow_asset_uploads, cm.allow_forum_use, " +
                " cm.keywords, cm.notifications, cm.item_notify, u.gender, (u.online & u.show_online) as online, u.mature_profile, u.email, " +
                " cm.item_review_notify, cm.blog_notify, cm.blog_comment_notify, cm.post_notify, " +
                " cm.reply_notify, cm.has_edit_rights, cm.active_member, com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_square_path " +
                " FROM community_members cm " +
                " INNER JOIN users u ON u.user_id = cm.user_id " +
                " INNER JOIN communities com ON com.creator_id = u.user_id " +
                " WHERE cm.user_id = @userId " +
                " AND cm.community_id = @communityId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            parameters.Add (new MySqlParameter ("@userId", userId));
            DataRow row = Db.Slave2.GetDataRow (sqlString, parameters);

            if (row == null)
            {
                return new CommunityMember ();
            }

            return new CommunityMember (Convert.ToInt32 (row["community_id"]), Convert.ToInt32 (row["user_id"]), row["username"].ToString (),
                row["display_name"].ToString (), Convert.ToInt32 (row["account_type_id"]), Convert.ToDateTime (row["added_date"]),
                Convert.ToUInt32 (row["status_id"]), row["newsletter"].ToString (), Convert.ToInt32 (row["invited_by_user_id"]), row["allow_asset_uploads"].ToString (),
                row["allow_forum_use"].ToString (), row["keywords"].ToString (), Convert.ToInt32 (row["notifications"]), Convert.ToInt32 (row["item_notify"]),
                Convert.ToInt32 (row["item_review_notify"]), Convert.ToInt32 (row["blog_notify"]), Convert.ToInt32 (row["blog_comment_notify"]), Convert.ToInt32 (row["post_notify"]),
                Convert.ToInt32 (row["reply_notify"]), row["has_edit_rights"].ToString ().Equals ("Y"), row["active_member"].ToString ().Equals ("Y"),
                row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_square_path"].ToString (),
                row["gender"].ToString (), Convert.ToInt32 (row["online"]), Convert.ToInt32 (row["mature_profile"]).Equals (1), row["email"].ToString ()
                );
        }

        /// <summary>
        /// Get a community members
        /// </summary>
        public PagedList<CommunityMember> GetCommunityMembers (int communityId, UInt32 communityMemberStatusId, bool bIncludeSuspended, bool bIncludePending, bool bIncludeDeleted,
            bool bIncludeRejected, string filter, string orderby, int pageNumber, int pageSize, bool bShowMature, string userNameFilter, bool includeNotificationPref)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string selectList = "SELECT cm.community_id, cm.account_type_id, cm.added_date, cm.status_id, " +
                " cm.notifications, cm.item_notify, cm.item_review_notify, cm.blog_notify, cm.blog_comment_notify, " +
                " cm.post_notify, cm.reply_notify, u.gender, u.birth_date, cm.newsletter, COALESCE(cm.invited_by_user_id, 0) as invited_by_user_id, " +
                " cm.allow_asset_uploads, cm.allow_forum_use, cm.keywords, cm.has_edit_rights, cm.active_member, " +
                " u.username, u.email, u.show_email, u.user_id, u.email, u.show_online, " +
                " (u.online & u.show_online) as online, u.location, u.username, u.display_name, u.gender, u.mature_profile, u.email, " +
                " com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_square_path, " +
                " cs.number_of_members, cs.number_of_views, cs.number_times_shared, cs.number_of_diggs, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture ";

            string fromList = " FROM users u " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id," +
                " community_members cm, communities_personal com " +
                " INNER JOIN channel_stats cs ON com.community_id = cs.channel_id ";

            string whereClause = " WHERE u.user_id = cm.user_id " +
                " AND cm.community_id = @communityId " +
                " AND com.creator_id = u.user_id ";

            if (includeNotificationPref)
            {
                selectList += ", IFNULL(unp.notify_world_blasts, 1) AS notify_world_blasts ";
                fromList += "LEFT JOIN user_notification_preferences unp ON u.user_id = unp.user_id ";
            }

            // Did they want a specific status id?
            if (communityMemberStatusId > 0)
            {
                if (bIncludeSuspended)
                {
                    whereClause += "AND (cm.status_id = @statusId OR cm.status_id = @statusId2) ";
                    parameters.Add (new MySqlParameter ("@statusId", communityMemberStatusId));
                    parameters.Add (new MySqlParameter ("@statusId2", (UInt32) CommunityMember.CommunityMemberStatus.LOCKED));
                }
                else
                {
                    whereClause += " AND cm.status_id = @statusId ";
                    parameters.Add (new MySqlParameter ("@statusId", communityMemberStatusId));
                }
            }
            else
            {
                // All Status's, but maybe not deleted or pending
                // Suspended?
                if (!bIncludeSuspended)
                {
                    whereClause += " AND cm.status_id <> " + (UInt32) CommunityMember.CommunityMemberStatus.LOCKED;
                }
                // Deleted?
                if (!bIncludeDeleted)
                {
                    whereClause += " AND cm.status_id <> " + (UInt32) CommunityMember.CommunityMemberStatus.DELETED;
                }
                // Pending?
                if (!bIncludePending)
                {
                    whereClause += " AND cm.status_id <> " + (UInt32) CommunityMember.CommunityMemberStatus.PENDING;
                }
                // Rejected?
                if (!bIncludeRejected)
                {
                    whereClause += " AND cm.status_id <> " + (UInt32) CommunityMember.CommunityMemberStatus.REJECTED;
                }
            }

            if (!bShowMature)
            {
                whereClause += " AND mature_profile = 0 ";
            }

            // Username search
            if (userNameFilter.Length > 0)
            {
                whereClause += " AND u.username LIKE @userNameFilter";
                parameters.Add(new MySqlParameter("@userNameFilter", userNameFilter));
            }

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

            string strQuery = selectList + fromList + whereClause;

            parameters.Add (new MySqlParameter ("@communityId", communityId));
            PagedDataTable dt = Db.Slave2.GetPagedDataTable (strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<CommunityMember> list = new PagedList<CommunityMember> ();
            //assigns total count
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                CommunityMember cm = new CommunityMember (Convert.ToInt32 (row["community_id"]), Convert.ToInt32 (row["user_id"]), row["username"].ToString (),
                    row["display_name"].ToString (), Convert.ToInt32 (row["account_type_id"]), Convert.ToDateTime (row["added_date"]),
                    Convert.ToUInt32 (row["status_id"]), row["newsletter"].ToString (), Convert.ToInt32 (row["invited_by_user_id"]), row["allow_asset_uploads"].ToString (),
                    row["allow_forum_use"].ToString (), row["keywords"].ToString (), Convert.ToInt32 (row["notifications"]), Convert.ToInt32 (row["item_notify"]),
                    Convert.ToInt32 (row["item_review_notify"]), Convert.ToInt32 (row["blog_notify"]), Convert.ToInt32 (row["blog_comment_notify"]), Convert.ToInt32 (row["post_notify"]),
                    Convert.ToInt32 (row["reply_notify"]), row["has_edit_rights"].ToString ().Equals ("Y"), row["active_member"].ToString ().Equals ("Y"),
                    row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_square_path"].ToString (),
                    row["gender"].ToString (), Convert.ToInt32 (row["online"]), Convert.ToInt32 (row["mature_profile"]).Equals (1), row["email"].ToString ()
                );
                cm.FacebookSettings.FacebookUserId = Convert.ToUInt64 (row["fb_user_id"]);
                cm.FacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (row["use_facebook_profile_picture"]);

                if (includeNotificationPref)
                {
                    cm.NotificationPreference.NotifyWorldBlasts = Convert.ToBoolean (row["notify_world_blasts"]);
                }

                list.Add (cm);
            }

            return list;
        }

        /// <summary>
        /// GetCommunityCategories
        /// </summary>
        public List<CommunityCategory> GetCommunityCategories ()
        {
            string cacheKey = "kaneva.community_categories";
            List<CommunityCategory> list = (List<CommunityCategory>)DbCache.Cache[cacheKey];

            if (list == null)
            {

                string sqlSelect = "SELECT * FROM community_categories ORDER BY category_id ASC";
                DataTable dt = Db.Slave2.GetDataTable(sqlSelect, new List<IDbDataParameter>());

                list = new List<CommunityCategory>();

                foreach (DataRow row in dt.Rows)
                {
                    list.Add(new CommunityCategory( Convert.ToInt32(row["category_id"]) , row["category_name"].ToString() , row["description"].ToString()));
                }

                DbCache.Cache.Insert(cacheKey, list, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return list;
        }

        /// <summary>
        /// GetCommunityCategories
        /// </summary>
        public List<CommunityCategory> GetCommunityCategories(int communityId)
        {
            string sqlSelect =  " SELECT c.category_id, c.category_name, c.description " +
                                " FROM communities_to_categories ctc " +
                                " INNER JOIN community_categories c ON ctc.category_id = c.category_id " +
                                " WHERE community_id = @communityId ORDER BY category_id ASC ";
            
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            DataTable dt = Db.Slave2.GetDataTable(sqlSelect, parameters);

            List<CommunityCategory> list = new List<CommunityCategory>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new CommunityCategory(Convert.ToInt32(row["category_id"]), row["category_name"].ToString(), row["description"].ToString()));
            }

            return list;
        }

        public int InsertCommunityCategory(int communityId, int categoryId)
        {
            // Send the message
            string sql = " INSERT INTO kaneva.communities_to_categories (community_id, category_id) " +
                         " VALUES (@communityId, @categoryId) ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            parameters.Add(new MySqlParameter("@categoryId", categoryId));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int DeleteCommunityCategories(int communityId)
        {
            string sql = " DELETE FROM kaneva.communities_to_categories " +
                         " WHERE community_id = @communityId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// GetCommunityPlaceTypes
        /// </summary>
        public List<CommunityPlaceType> GetCommunityPlaceTypes ()
        {
            string cacheKey = "kaneva.community_place_types";
            List<CommunityPlaceType> list = (List<CommunityPlaceType>)DbCache.Cache[cacheKey];

            if (list == null)
            {

                string sqlSelect = "SELECT * FROM community_place_types";
                DataTable dt = Db.Slave2.GetDataTable(sqlSelect, new List<IDbDataParameter>());

                list = new List<CommunityPlaceType>();

                foreach (DataRow row in dt.Rows)
                {
                    list.Add(new CommunityPlaceType(Convert.ToInt32(row["place_type_id"]), row["name"].ToString(), row["description"].ToString()
                    ));
                }

                DbCache.Cache.Insert(cacheKey, list, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }
            return list;
        }

        /// <summary>
        /// GetCommunityTabsByCommunity
        /// </summary>
        public List<CommunityTab> GetCommunityTabs (int communityId, int communityTypeId)
        {
            string cacheKey = "kaneva.community_tabs." + communityId.ToString();
            List<CommunityTab> list = (List<CommunityTab>) DbCache.Cache[cacheKey];

            if (list == null)
            {
                string sqlSelect = "SELECT ct.community_id, ctt.tab_id, ctt.community_type_id, " +
                    " ctt.name, ctt.is_configurable, ctt.display_order, ct.is_default " +
                    " FROM community_types_tabs ctt " +
                    " LEFT OUTER JOIN community_tabs ct ON ctt.community_type_id = ct.community_type_id and ctt.tab_id = ct.tab_id " +
                    " AND community_id = @communityId " +
                    " WHERE ctt.community_type_id = @communityTypeId " +
                    " ORDER BY display_order";

                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
                parameters.Add (new MySqlParameter ("@communityId", communityId));
                parameters.Add (new MySqlParameter ("@communityTypeId", communityTypeId));
                DataTable dt = Db.Slave2.GetDataTable (sqlSelect, parameters);

                list = new List<CommunityTab> ();

                foreach (DataRow row in dt.Rows)
                {
                    list.Add (new CommunityTab (Convert.ToInt32 (row["tab_id"]), row["name"].ToString (),
                        Convert.ToInt32 (row["display_order"]), row["community_id"] == DBNull.Value ? 0 : Convert.ToInt32 (row["community_id"]),
                        Convert.ToInt32 (row["community_type_id"]), row["is_configurable"].ToString ().Equals ("Y"),
                        row["is_default"] == DBNull.Value ? false : row["is_default"].ToString ().Equals ("Y")
                    ));
                }

                DbCache.Cache.Insert (cacheKey, list, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
            }
            return list;
        }

        /// <summary>
        /// GetCommunityTabsByCommunityType
        /// </summary>
        public List<CommunityTab> GetCommunityTabsByCommunityType (int CommunityTypeId)
        {
            string cacheKey = "kaneva.community_type_tabs." + CommunityTypeId.ToString();
            List<CommunityTab> list = (List<CommunityTab>) DbCache.Cache[cacheKey];

            if (list == null)
            {
                string sqlSelect = "SELECT tab_id, community_type_id, " +
                    " name, is_configurable, display_order " +
                    " FROM community_types_tabs" +
                    " WHERE community_type_id = @CommunityTypeId " +
                    " ORDER BY display_order";
                
                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
                parameters.Add (new MySqlParameter ("@CommunityTypeId", CommunityTypeId));
                DataTable dt = Db.Slave2.GetDataTable (sqlSelect, parameters);

                list = new List<CommunityTab> ();

                foreach (DataRow row in dt.Rows)
                {
                    list.Add (new CommunityTab (Convert.ToInt32 (row["tab_id"]), row["name"].ToString (), 
                        Convert.ToInt32 (row["display_order"]), 0, Convert.ToInt32 (row["community_type_id"]),
                        row["is_configurable"].ToString ().Equals ("Y")
                    ));
                }

                DbCache.Cache.Insert (cacheKey, list, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
            }
            return list;
        }

        /// <summary>
        /// UpdateCommunityTabs
        /// </summary>
        public int UpdateCommunityTabs (int tabId, int communityId, int communityTypeId, bool isSelected, bool isDefault)
        {
            // Remove the tabs to display cache for this community
            DbCache.Cache.Remove ("kaneva.community_tabs." + communityId.ToString ());

            string sqlString = "";
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            if (isSelected)
            {
                sqlString = "REPLACE INTO community_tabs (community_id, tab_id, community_type_id, is_default) " + 
                    " VALUES (@communityId, @tabId, @communityTypeId, @isDefault)";

                parameters.Add (new MySqlParameter ("@isDefault", isDefault ? "Y" : "N"));
            }
            else
            {
                sqlString = "DELETE FROM community_tabs WHERE " +
                    " community_id = @communityId AND " +
                    " tab_id = @tabId AND " +
                    " community_type_id = @communityTypeId";
            }

            parameters.Add (new MySqlParameter ("@communityId", communityId));
            parameters.Add (new MySqlParameter ("@tabId", tabId));
            parameters.Add (new MySqlParameter ("@communityTypeId", communityTypeId));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// GetCommunityAboutText
        /// </summary>
        public string GetCommunityAboutText (int communityId)
        {
            string cacheKey = "kaneva.community_about_text." + communityId.ToString();
            string text = (string) DbCache.Cache[cacheKey];

            if (text == null)
            {
                string sqlSelect = "SELECT about_text " +
                    " FROM community_preferences " +
                    " WHERE community_id = @communityId";

                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
                parameters.Add (new MySqlParameter ("@communityId", communityId));

                DataRow drAbout = Db.Slave2.GetDataRow (sqlSelect, parameters);
                text = "";
                if (drAbout != null)
                {
                    text = drAbout["about_text"].ToString ();
                }
                
                DbCache.Cache.Insert (cacheKey, text, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return text;
        }

        public int UpdateCommunityAboutText (int communityId, string aboutText)
        {
            // Remove the tabs to display cache for this community
            DbCache.Cache.Remove ("kaneva.community_about_text." + communityId.ToString ());

            string sqlString = "";
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            sqlString = "UPDATE community_preferences " +
                " SET about_text = @aboutText " +
                " WHERE community_id=@communityId ";

            parameters.Add (new MySqlParameter ("@aboutText", aboutText));
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// Get a 3d Place
        /// </summary>
        public WOK3DApp GetWOK3DApp (int communityId)
        {
            string sqlString = "SELECT community_id, cg.game_id, game_status_id " +
                " FROM communities_game cg " +
                " INNER JOIN developer.games g ON cg.game_id = g.game_id " +
                " WHERE cg.community_id = @communityId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            DataRow row = Db.Master.GetDataRow (sqlString, parameters);

            if (row == null)
            {
                return new WOK3DApp ();
            }

            return new WOK3DApp (Convert.ToInt32 (row["game_id"]), Convert.ToInt32 (row["game_status_id"]));
        }

        /// <summary>
        /// Get a 3d Place
        /// </summary>
        public WOK3DPlace Get3DPlace (int zoneInstanceId, int zoneType)
        {
            string sqlString = 
                "SELECT cz.channel_zone_id, cz.kaneva_user_id, cz.zone_index, cz.zone_instance_id, cz.zone_type, cz.name, cz.raves, cz.server_id, cz.tied_to_server, " +
                "       COALESCE(sz.spin_down_delay_minutes, -1) spin_down_delay_minutes, COALESCE(sz.server_id, 0) script_zone_server_id " +
                " FROM channel_zones cz " +
                " LEFT OUTER JOIN script_zones sz ON cz.zone_instance_id=sz.zone_instance_id AND cz.zone_type=sz.zone_type " +
                "WHERE cz.zone_instance_id = @zoneInstanceId AND cz.zone_type = @zoneType";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@zoneInstanceId", zoneInstanceId));
            parameters.Add (new MySqlParameter ("@zoneType", zoneType));
            DataRow row = Db.WOK.GetDataRow (sqlString, parameters);

            if (row == null)
            {
                return new WOK3DPlace ();
            }

            return new WOK3DPlace (Convert.ToInt32 (row["channel_zone_id"]), Convert.ToInt32 (row["zone_index"]), Convert.ToInt32 (row["zone_instance_id"]),
                Convert.ToInt32 (row["zone_type"]), Convert.ToInt32 (row["kaneva_user_id"]),
                row["name"].ToString(), Convert.ToInt32(row["raves"]), Convert.ToInt32(row["server_id"]), row["tied_to_server"].ToString(),
                Convert.ToInt32(row["spin_down_delay_minutes"]), Convert.ToInt32(row["script_zone_server_id"]));
        }

        public WOK3DPlace Get3DPlace(int channelZoneId)
        {
            string sqlString = 
                "SELECT cz.channel_zone_id, cz.kaneva_user_id, cz.zone_index, cz.zone_instance_id, cz.zone_type, cz.name, cz.raves, cz.server_id, cz.tied_to_server, " +
                "       COALESCE(sz.spin_down_delay_minutes, -1) spin_down_delay_minutes, COALESCE(sz.server_id, 0) script_zone_server_id " +
                " FROM channel_zones cz " +
                " LEFT OUTER JOIN script_zones sz ON cz.zone_instance_id=sz.zone_instance_id AND cz.zone_type=sz.zone_type " +
                "WHERE cz.channel_zone_id = @channelZoneId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add(new MySqlParameter("@channelZoneId", channelZoneId));
            DataRow row = Db.WOK.GetDataRow (sqlString, parameters);

            if (row == null)
            {
                return new WOK3DPlace ();
            }

            return new WOK3DPlace (Convert.ToInt32 (row["channel_zone_id"]), Convert.ToInt32 (row["zone_index"]), Convert.ToInt32 (row["zone_instance_id"]),
                Convert.ToInt32 (row["zone_type"]), Convert.ToInt32 (row["kaneva_user_id"]),
                row["name"].ToString(), Convert.ToInt32(row["raves"]), Convert.ToInt32(row["server_id"]), row["tied_to_server"].ToString(),
                Convert.ToInt32(row["spin_down_delay_minutes"]), Convert.ToInt32(row["script_zone_server_id"]));
        }

        public WOK3DPlace Get3DPlace(string worldName)
        {
            string sqlString = 
                "SELECT cz.channel_zone_id, cz.kaneva_user_id, cz.zone_index, cz.zone_instance_id, cz.zone_type, cz.name, cz.raves, cz.server_id, cz.tied_to_server, " +
                "       COALESCE(sz.spin_down_delay_minutes, -1) spin_down_delay_minutes, COALESCE(sz.server_id, 0) script_zone_server_id " +
                " FROM channel_zones cz " +
                " INNER JOIN wok.unified_world_ids u ON u.zone_index = cz.zone_index AND u.zone_instance_id = cz.zone_instance_id " +
                " INNER JOIN kaneva.communities c ON u.community_id = c.community_id AND c.name = @worldName " +
                " LEFT OUTER JOIN script_zones sz ON cz.zone_instance_id=sz.zone_instance_id AND cz.zone_type=sz.zone_type ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add(new MySqlParameter("@worldName", worldName));
            DataRow row = Db.WOK.GetDataRow (sqlString, parameters);

            if (row == null)
            {
                return new WOK3DPlace ();
            }

            return new WOK3DPlace (Convert.ToInt32 (row["channel_zone_id"]), Convert.ToInt32 (row["zone_index"]), Convert.ToInt32 (row["zone_instance_id"]),
                Convert.ToInt32 (row["zone_type"]), Convert.ToInt32 (row["kaneva_user_id"]),
                row["name"].ToString(), Convert.ToInt32(row["raves"]), Convert.ToInt32(row["server_id"]), row["tied_to_server"].ToString(),
                Convert.ToInt32(row["spin_down_delay_minutes"]), Convert.ToInt32(row["script_zone_server_id"]));
        }

        public PagedList<WOK3DPlace> Search3DPlaceByName(string name, int page, int itemsPerPage)
        {
            string sqlString = " SELECT cz.channel_zone_id AS channel_zone_id, " +
	                           "     cz.kaneva_user_id AS kaneva_user_id, " +
	                           "     cz.zone_index AS zone_index, " +
	                           "     cz.zone_instance_id AS zone_instance_id, " +
	                           "     cz.zone_type AS zone_type, " +
	                           "     c.name, " +
	                           "     cz.raves AS raves, " +
                               "     cz.server_id, " +
                               "     cz.tied_to_server, " +
                               "     COALESCE(sz.spin_down_delay_minutes, -1) spin_down_delay_minutes, " +
                               "     COALESCE(sz.server_id, 0) script_zone_server_id " +
                               " FROM kaneva.communities c " +
                               " INNER JOIN wok.channel_zones cz ON c.community_id = cz.zone_instance_id AND cz.zone_type = 6 " +
                               " LEFT OUTER JOIN wok.script_zones sz ON cz.zone_instance_id=sz.zone_instance_id AND cz.zone_type=sz.zone_type " +
                               " WHERE c.name LIKE '" + name + "%' AND c.is_personal = 0 " +
                               " UNION ALL " +
                               " SELECT cz.channel_zone_id AS channel_zone_id, " +
	                           "     cz.kaneva_user_id AS kaneva_user_id,  " +
	                           "     cz.zone_index AS zone_index,  " +
	                           "     cz.zone_instance_id AS zone_instance_id,  " +
	                           "     cz.zone_type AS zone_type, " +
	                           "     c.name, " +
                               "     cz.raves AS raves, " +
                               "     cz.server_id, " +
                               "     cz.tied_to_server, " +
                               "     COALESCE(sz.spin_down_delay_minutes, -1) spin_down_delay_minutes, " +
                               "     COALESCE(sz.server_id, 0) script_zone_server_id " +
                               " FROM kaneva.communities c " +
                               " INNER JOIN kaneva.users u ON c.creator_id = u.user_id " +
                               " INNER JOIN wok.channel_zones cz ON u.wok_player_id = cz.zone_instance_id AND cz.zone_type = 3 " +
                               " LEFT OUTER JOIN wok.script_zones sz ON cz.zone_instance_id=sz.zone_instance_id AND cz.zone_type=sz.zone_type " +
                               " WHERE c.name LIKE '" + name + "%' AND c.is_personal = 0 AND c.community_type_id = 5 " +
                               " UNION ALL " +
                               " SELECT cz.channel_zone_id AS channel_zone_id,  " +
	                           "     cz.kaneva_user_id AS kaneva_user_id,  " +
	                           "     cz.zone_index AS zone_index,  " +
	                           "     cz.zone_instance_id AS zone_instance_id,  " +
	                           "     cz.zone_type AS zone_type, " +
	                           "     cz.name, " +
                               "     cz.raves AS raves, " +
                               "     cz.server_id, " +
                               "     cz.tied_to_server, " +
                               "     COALESCE(sz.spin_down_delay_minutes, -1) spin_down_delay_minutes, " +
                               "     COALESCE(sz.server_id, 0) script_zone_server_id " +
                               " FROM wok.channel_zones cz " +
                               " LEFT OUTER JOIN wok.script_zones sz ON cz.zone_instance_id=sz.zone_instance_id AND cz.zone_type=sz.zone_type " +
                               " WHERE cz.name LIKE '" + name + "%' AND cz.zone_type = 4 ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            PagedDataTable pdt = Db.Master.GetPagedDataTable (sqlString, "", parameters, page, itemsPerPage);

            PagedList<WOK3DPlace> list = new PagedList<WOK3DPlace> ();
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                list.Add(new WOK3DPlace(Convert.ToInt32(row["channel_zone_id"]), Convert.ToInt32(row["zone_index"]), Convert.ToInt32(row["zone_instance_id"]),
                    Convert.ToInt32(row["zone_type"]), Convert.ToInt32(row["kaneva_user_id"]),
                    row["name"].ToString(), Convert.ToInt32(row["raves"]), Convert.ToInt32(row["server_id"]), row["tied_to_server"].ToString(),
                    Convert.ToInt32(row["spin_down_delay_minutes"]), Convert.ToInt32(row["script_zone_server_id"])));
            }

            return list;
        }

        public int Set3DPlaceTiedStatus(int channelZoneId, int serverId, string tiedToServer)
        {
            string sqlString =  " UPDATE channel_zones " +
                                " SET tied_to_server = @tiedToServer, " +
                                " server_id = @serverId " + 
                                " WHERE channel_zone_id = @channelZoneId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@channelZoneId", channelZoneId));
            parameters.Add(new MySqlParameter("@serverId", (tiedToServer == "NOT_TIED" ? 0 : serverId)));
            parameters.Add(new MySqlParameter("@tiedToServer", tiedToServer));
            
            return Db.WOK.ExecuteNonQuery(sqlString, parameters);
        }

        public int Set3DPlaceSpinDownDelayMinutes(int zoneInstanceId, int zoneType, int spinDownDelayMinutes)
        {
            string sqlString = "INSERT INTO script_zones(zone_instance_id, zone_type, spin_down_delay_minutes) VALUES " +
                               "(@zoneInstanceId, @zoneType, @spinDownDelayMinutes) " +
                               "ON DUPLICATE KEY UPDATE spin_down_delay_minutes = @spinDownDelayMinutes";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));
            parameters.Add(new MySqlParameter("@spinDownDelayMinutes", spinDownDelayMinutes));

            return Db.WOK.ExecuteNonQuery(sqlString, parameters);
        }

        public bool IsUserWOK3DPlaceOwner(int userId, int zoneIndex, int zoneInstanceId)
        {
            string sql = "SELECT cz.* " +
                        " FROM wok.channel_zones cz " +
                        " INNER JOIN kaneva.communities c ON cz.zone_instance_id = c.community_id and c.is_personal = 0 " +
                        " WHERE cz.zone_type = 6 " +
                        " AND cz.zone_index = @zoneIndex " +
                        " AND cz.zone_instance_id = @zoneInstanceId " +
                        " AND c.creator_id = @userId " +
                        " UNION " +
                        " SELECT cz.* " +
                        " FROM wok.channel_zones cz " +
                        " INNER JOIN wok.players p ON p.player_id = cz.zone_instance_id " +
                        " WHERE cz.zone_type = 3 " +
                        " AND cz.zone_index = @zoneIndex " +
                        " AND cz.zone_instance_id = @zoneInstanceId " +
                        " AND p.kaneva_user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@zoneIndex", zoneIndex));
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));

            DataRow drResult = Db.WOK.GetDataRow(sql, parameters);
            return (drResult != null);
        }

        private void ConvertCommunityInfoToZoneInfo(int communityId, int communityType, out int zoneInstanceId, out int zoneType)
        {
            zoneInstanceId = 0;
            zoneType = 0;

            if (communityType == (int)CommunityType.COMMUNITY)
            {
                zoneInstanceId = communityId;
                zoneType = 6;
            }
            else if (communityType == (int)CommunityType.HOME)
            {
                string strQuery = " SELECT COALESCE(u.wok_player_id, 0) AS player_id " +
                                  " FROM users u WHERE u.home_community_id = @communityId";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@communityId", communityId));

                zoneInstanceId = Convert.ToInt32(Db.Master.GetScalar(strQuery, parameters));
                zoneType = 3;
            }
        }

        /// <summary>
        /// Get community Passes
        /// </summary>
        /// <returns></returns>
        public DataTable GetCommunityPasses(int communityId, int communityType)
        {
            int zoneInstanceId;
            int zoneType;
            ConvertCommunityInfoToZoneInfo(communityId, communityType, out zoneInstanceId, out zoneType);

            string strQuery = " SELECT pg.name, pgcz.pass_group_id, pgcz.set_by_admin " +
                                " FROM wok.pass_group_channel_zones pgcz, wok.pass_groups pg " +
                                " WHERE pgcz.pass_group_id = pg.pass_group_id " +
                                " AND pgcz.zone_instance_id = @zoneInstanceId " +
                                " AND pgcz.zone_type = @zoneType";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));

            return Db.Master.GetDataTable(strQuery, parameters);
        }

        public DataTable GetWorldPasses(int wokGameId, int gameId, int zoneInstanceId, int zoneType)
        {
            string strQuery = string.Empty;
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (wokGameId == gameId)
            {
                strQuery =  " SELECT pg.name, pgcz.pass_group_id, pgcz.set_by_admin " +
                            " FROM wok.pass_group_channel_zones pgcz " +
                            " INNER JOIN wok.pass_groups pg ON pgcz.pass_group_id = pg.pass_group_id " +
                            " WHERE pgcz.zone_instance_id = @zoneInstanceId " +
                            " AND pgcz.zone_type = @zoneType";

                parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
                parameters.Add(new MySqlParameter("@zoneType", zoneType));
            }
            else
            {
                strQuery =  " SELECT pg.name, gpg.pass_group_id " +
                            " FROM developer.game_pass_groups gpg " +
                            " INNER JOIN wok.pass_groups pg ON gpg.pass_group_id = pg.pass_group_id " +
                            " WHERE gpg.game_id = @gameId";

                parameters.Add(new MySqlParameter("@gameId", gameId));
            }

            return Db.Master.GetDataTable(strQuery, parameters);
        }

        /// <summary>
        /// Add community Pass
        /// </summary>
        /// <returns></returns>
        public bool AddCommunityPass(int userId, bool isAdmin, int communityId, int communityType, int passId)
        {
            int zoneInstanceId;
            int zoneType;
            ConvertCommunityInfoToZoneInfo(communityId, communityType, out zoneInstanceId, out zoneType);

            CommunityMember communityMember = GetCommunityMember(communityId, userId);
            if (isAdmin || communityMember.AccountTypeId == (int)CommunityMember.CommunityMemberAccountType.OWNER)
            {
                MySQLSubscriptionDao subDao = new MySQLSubscriptionDao();
                if (isAdmin || subDao.HasValidSubscriptionByPassType((uint)userId, (uint)passId))
                {
                    // Send the message
                    string sql = "INSERT INTO wok.pass_group_channel_zones (zone_instance_id, zone_type, pass_group_id, set_by_admin) VALUES (@zoneInstanceId, @zoneType, @passId, @setByAdmin)";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
                    parameters.Add(new MySqlParameter("@zoneType", zoneType));
                    parameters.Add(new MySqlParameter("@passId", passId));
                    parameters.Add(new MySqlParameter("@setByAdmin", (isAdmin ? "Y" : "N")));

                    return (Db.Master.ExecuteNonQuery(sql, parameters) != 0);
                }
            }

            return false;
        }

        /// <summary>
        /// Delete community Pass
        /// </summary>
        /// <returns></returns>
        public bool DeleteCommunityPass(int userId, bool isAdmin, int communityId, int communityType, int passId)
        {
            int zoneInstanceId;
            int zoneType;
            ConvertCommunityInfoToZoneInfo(communityId, communityType, out zoneInstanceId, out zoneType);

            CommunityMember communityMember = GetCommunityMember(communityId, userId);
            if (isAdmin || communityMember.AccountTypeId == (int)CommunityMember.CommunityMemberAccountType.OWNER)
            {
                // Send the message
                string sql = "DELETE FROM wok.pass_group_channel_zones WHERE zone_instance_id = @zoneInstanceId AND zone_type = @zoneType AND pass_group_id = @passId";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
                parameters.Add(new MySqlParameter("@zoneType", zoneType));
                parameters.Add(new MySqlParameter("@passId", passId));

                return (Db.Master.ExecuteNonQuery(sql, parameters) != 0);
            }

            return false;
        }

        public Community GetPersonalCommunityFromPlayerId (int playerId)
        {
            string sqlString = "SELECT p.kaneva_user_id, com.name, com.name_no_spaces, com.is_adult, com.creator_username " +
                " FROM wok.players p INNER JOIN kaneva.communities_personal com ON com.creator_id = p.kaneva_user_id " +
                " WHERE p.player_id = @playerId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@playerId", playerId));
            DataRow row = Db.Slave2.GetDataRow (sqlString, parameters);

            Community community = new Community ();
            if (row == null)
            {
                return community;
            }

            community.CreatorId = Convert.ToInt32 (row["kaneva_user_id"]);
            community.Name = row["name"].ToString ();
            community.NameNoSpaces = row["name_no_spaces"].ToString ();
            community.IsAdult = row["is_adult"].ToString ();
            community.CreatorUsername = row["creator_username"].ToString ();

            return community;
        }

        /// <summary>
        /// GetNumberOfCommunities
        /// </summary>
        /// <param name="creatorId"></param>
        /// <param name="statusId"></param>
        /// <returns>int</returns>
        public int GetNumberOfCommunities(int creatorId, int statusId, int[] communityTypes)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@creatorId", creatorId));
            parameters.Add(new MySqlParameter("@statusId", statusId));

            string sql = "SELECT count(*) FROM communities " +
				" WHERE creator_id = @creatorId " +
                " AND status_id = @statusId" +
                " AND place_type_id < 99 ";

            char[] charsToTrim = { ',' };
            if (communityTypes.Length > 0)
            {
                sql += " AND community_type_id IN ( ";
                for (int i = 0; i < communityTypes.Length; i++)
                {
                    sql += " @communityTypeId" + i + ",";
                    parameters.Add(new MySqlParameter("@communityTypeId" + i, communityTypes[i]));
                }
                sql = sql.TrimEnd(charsToTrim) + ") ";
            }

            return Convert.ToInt32(Db.Master.GetScalar(sql, parameters));
        }

        /// <summary>
        /// GetNumberOf3DApps
        /// </summary>
        /// <param name="creatorId"></param>
        /// <param name="statusId"></param>
        /// <returns>int</returns>
        public int GetNumberOf3DApps(int creatorId, int statusId)
        {
            string sql = "SELECT count(*) FROM developer.games " +
				" WHERE owner_id = @creatorId " +
                " AND incubator_hosted = 1 " +
                " AND game_status_id = 1 ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@creatorId", creatorId));
            parameters.Add(new MySqlParameter("@statusId", statusId));

            return Convert.ToInt32(Db.Master.GetScalar(sql, parameters));
        }


        /// <summary>
        /// ShareCommunity
        /// </summary>
        public void ShareCommunity(int communityId, int userId, int sentUserId, string email)
		{
			string sqlString = "INSERT INTO community_shares " +
				" (user_id, sent_user_id, community_id, email, created_date) VALUES (" +
				"@userId, @sentUserId, @communityId, @email, NOW())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@sentUserId", sentUserId));
            parameters.Add (new MySqlParameter ("@communityId", communityId));

			if (email.Trim ().Length.Equals (0))
			{
                parameters.Add (new MySqlParameter ("@email", DBNull.Value));
			}
			else
			{
                parameters.Add (new MySqlParameter ("@email", email));
			}
			Db.Audit.ExecuteNonQuery (sqlString, parameters);

            sqlString = "UPDATE channel_stats SET number_times_shared=number_times_shared+1 WHERE channel_id=@communityId;";
            parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            Db.Master.ExecuteNonQuery(sqlString, parameters);
            
            sqlString = "UPDATE users_stats SET number_community_shares=number_community_shares+1 WHERE user_id = @userId;";
            parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// IsCommunityValid - returns true if community exists
        /// </summary>
        /// <param name="communityId"></param>
        /// <returns></returns>
        public bool IsCommunityValid(int communityId)
        {
            string sqlSelect = " SELECT COUNT(*) " +
                " FROM communities " +
                " WHERE community_id = @community_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@community_id", communityId));

            return (Convert.ToInt32(Db.Master.GetScalar(sqlSelect, parameters)) > 0);
        }

        /// <summary>
        /// Update a community
        /// </summary>
        public int UpdateCommunityURL(int communityId,string url)
        {
            string sqlString = "CALL update_communities_url (@community_id, @url);";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@community_id", communityId));
            parameters.Add(new MySqlParameter("@url", url));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// IsCommunityURLTaken
        /// </summary>
        /// <param name="url"></param>
        /// <param name="communityId"></param>
        /// <returns>int</returns>
        public int IsCommunityURLTaken(string url, int communityId)
        {
            string sql = "SELECT COUNT(*) FROM communities where url = @url " +
                " AND community_id <> @communityId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@url", url));
            parameters.Add(new MySqlParameter("@communityId", communityId));

            return Convert.ToInt32(Db.Master.GetScalar(sql, parameters));
        }

        /// <summary>
        /// IsCommunityNameTaken
        /// </summary>
        /// <param name="isPersonal"></param>
        /// <param name="communityName"></param>
        /// <returns>int</returns>
        public int IsCommunityNameTaken(bool isPersonal, string communityName, int communityId)
        {
            string sql = "SELECT COUNT(*) FROM " + (isPersonal ? "communities_personal" : "communities_public") +
                " WHERE name_no_spaces = @nameNoSpaces AND community_id <> @communityId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@nameNoSpaces", communityName));
            parameters.Add(new MySqlParameter("@communityId", communityId));

            return Convert.ToInt32(Db.Master.GetScalar(sql, parameters));
        }

        /// <summary>
        /// Update number of channel views
        /// </summary>
        /// <param name="user_id"></param>
        public void UpdateChannelViews(int userId, int communityId, bool browseAnon, string ipAddress)
        {
            // Build unique cache key from this IP to the community
            string cacheKey = CentralCache.keyRecentCommunityView + ipAddress + communityId.ToString();
            int iSecondsToCountNextView = 60;

            // Have they viewd in last X seconds?
            String view = (String)CentralCache.Get(cacheKey);

            if (view == null)
            {
                // Not found in cache means not views in last X seconds
                // Record as a browse
                string sqlInsertView = "INSERT INTO community_views " +
                    " (user_id, community_id, created_date, anonymous, ip_address)" +
                    " VALUES (@userId, @communityId, NOW(), @anonymous, INET_ATON(@ipAddress))";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@userId", userId));
                parameters.Add(new MySqlParameter("@communityId", communityId));
                parameters.Add(new MySqlParameter("@anonymous", browseAnon ? 1 : 0));
                parameters.Add(new MySqlParameter("@ipAddress", ipAddress));
                Db.Master.ExecuteNonQuery(sqlInsertView, parameters);

                // Code moved out of triggers
                parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@communityId", communityId));
                sqlInsertView = "UPDATE channel_stats SET number_of_views=number_of_views+1 WHERE channel_id = @communityId";
                Db.Master.ExecuteNonQuery(sqlInsertView, parameters);

                // Store it in cache
                CentralCache.Store(cacheKey, ipAddress, TimeSpan.FromSeconds(iSecondsToCountNextView));
            }
        }

        /// <summary>
        /// Update a community
        /// </summary>
        public int UpdateCommunity(int communityId, int ownerId, bool IsPersonal, string thumbnailPath, string thumbnailType, bool hasThumbnail)
        {
            string sqlString = "UPDATE communities " +
                " SET " +
                " thumbnail_path = @thumbnailPath," +
                " thumbnail_type = @thumbnailType, " +
                " has_thumbnail = @hasThumbnail " +
                " WHERE community_id = @communityId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (thumbnailPath.Trim().Length.Equals(0))
            {
                parameters.Add(new MySqlParameter("@thumbnailPath", DBNull.Value));
                parameters.Add(new MySqlParameter("@hasThumbnail", 'N'));
            }
            else if (!hasThumbnail) // this case added so Dance Party 3D default avatars don't show up in search as having thumbnail
            {
                parameters.Add(new MySqlParameter("@thumbnailPath", thumbnailPath));
                parameters.Add(new MySqlParameter("@hasThumbnail", 'N'));
            }
            else
            {
                parameters.Add(new MySqlParameter("@thumbnailPath", thumbnailPath));
                parameters.Add(new MySqlParameter("@hasThumbnail", 'Y'));
            }

            parameters.Add(new MySqlParameter("@thumbnailType", thumbnailType));
            parameters.Add(new MySqlParameter("@communityId", communityId));

            // If this is a personal channel, invalidate cache so user sees correct profile image next hit
            if (IsPersonal)
            {
                // Invalidate user from cache
                CentralCache.Remove(CentralCache.keyKanevaUser + ownerId);
            }


            Db.Master.ExecuteNonQuery(sqlString, parameters);
            return 0;
        }


        /// <summary>
        /// Update a community
        /// </summary>
        public int UpdateCommunityThumb(int communityId, string thumbnailPath, string dbColumn)
        {
            string sqlString = "UPDATE communities " +
                " SET " +
                dbColumn + " = @thumbnailPath " +
                " WHERE community_id = @communityId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (thumbnailPath.Trim().Length.Equals(0))
            {
                parameters.Add(new MySqlParameter("@thumbnailPath", DBNull.Value));
            }
            else
            {
                parameters.Add(new MySqlParameter("@thumbnailPath", thumbnailPath));
            }

            parameters.Add(new MySqlParameter("@communityId", communityId));

            Db.Master.ExecuteNonQuery(sqlString, parameters);
            return 0;
        }

        /// <summary>
        /// Update the community
        /// </summary>
        /// <param name="community"></param>
        /// <returns>int</returns>
        public int UpdateCommunity(Community community)
        {
            string sql = "CALL update_community(@communityId,@canPublish,@backgroundImage,@backgroundRGB,@description,@isAdult," +
                "@isPublic,@keywords,@name,@nameNoSpaces,@over21Required,@placeTypeId,@isPersonal,@email,@url,@communityTypeId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", community.CommunityId));
            parameters.Add(new MySqlParameter("@canPublish", community.AllowPublishing));
            parameters.Add(new MySqlParameter("@backgroundImage", community.BackgroundImage));
            parameters.Add(new MySqlParameter("@backgroundRGB", community.BackgroundRGB));
            parameters.Add(new MySqlParameter("@description", community.Description));
            parameters.Add(new MySqlParameter("@isAdult", community.IsAdult));
            parameters.Add(new MySqlParameter("@isPublic", community.IsPublic));
            parameters.Add(new MySqlParameter("@keywords", community.Keywords));
            parameters.Add(new MySqlParameter("@name", community.Name));
            parameters.Add(new MySqlParameter("@nameNoSpaces", community.NameNoSpaces));
            parameters.Add(new MySqlParameter("@over21Required", community.Over21Required ? 'Y' : 'N'));
            parameters.Add(new MySqlParameter("@placeTypeId", community.PlaceTypeId));
            parameters.Add(new MySqlParameter("@isPersonal", community.IsPersonal));
            parameters.Add(new MySqlParameter("@email", community.Email));
            if ((community.Url == null) || (community.Url.Length <= 0))
            {
                parameters.Add(new MySqlParameter("@url", DBNull.Value));
            }
            else
            {
                parameters.Add(new MySqlParameter("@url", community.Url));
            }
            parameters.Add(new MySqlParameter("@communityTypeId", community.CommunityTypeId));

            int result = Convert.ToInt32(Db.Master.GetScalar(sql, parameters));

            if ((result > 0) && community.IsPersonal.Equals(1))
            {
                // Invalidate user from cache
                CentralCache.Remove(CentralCache.keyKanevaUser + community.CreatorId);
            }

            return result;
        }

        /// <summary>
        /// Insert a new community
        /// </summary>
        /// <param name="community"></param>
        /// <returns>int</returns>
        public int InsertCommunity(Community community)
        {
            string sqlString = "CALL add_communities (" +
              " @placeTypeId, @nameNoSpaces, " +
              " @url, @creatorId, @statusId, @email, " +
              " @isPublic, @isAdult, '', " +
              " '', '', " +
              " '', '', " +
              " '', @canPublish, 0, " +
              " @isPersonal, @communityTypeId, 0, " +
              " @over21Required, 'N', @name, " +
              " @description, @backgroundRGB, " +
              " @backgroundImage, @creatorUsername, @keywords, " +
              " @communityId); SELECT CAST(@communityId as UNSIGNED INT) as communityId; ";

            int communityId = 0;
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@canPublish", community.AllowPublishing));
            parameters.Add(new MySqlParameter("@backgroundImage", community.BackgroundImage));
            parameters.Add(new MySqlParameter("@backgroundRGB", community.BackgroundRGB));
            parameters.Add(new MySqlParameter("@description", community.Description));
            parameters.Add(new MySqlParameter("@isAdult", community.IsAdult));
            parameters.Add(new MySqlParameter("@isPublic", community.IsPublic));
            parameters.Add(new MySqlParameter("@keywords", community.Keywords));
            parameters.Add(new MySqlParameter("@name", community.Name));
            parameters.Add(new MySqlParameter("@nameNoSpaces", community.NameNoSpaces));
            parameters.Add(new MySqlParameter("@over21Required", community.Over21Required ? 'Y' : 'N'));
            parameters.Add(new MySqlParameter("@placeTypeId", community.PlaceTypeId));
            parameters.Add(new MySqlParameter("@isPersonal", community.IsPersonal));
            parameters.Add(new MySqlParameter("@statusId", community.StatusId));
            parameters.Add(new MySqlParameter("@email", community.Email));
            parameters.Add(new MySqlParameter("@creatorId", community.CreatorId));
            parameters.Add(new MySqlParameter("@creatorUsername", community.CreatorUsername));
            parameters.Add(new MySqlParameter("@communityTypeId", community.CommunityTypeId));

            if ((community.Url == null) || (community.Url.Length <= 0))
            {
                parameters.Add(new MySqlParameter("@url", DBNull.Value));
            }
            else
            {
                parameters.Add(new MySqlParameter("@url", community.Url));
            }

            DataRow row = Db.Master.GetDataRow(sqlString, parameters);

            if (row != null)
            {
                communityId = Convert.ToInt32(row["communityId"]);
            }

            return communityId;
        }

        /// <summary>
        /// Insert a new community preference
        /// </summary>
        /// <param name="community"></param>
        /// <returns>int</returns>
        public int InsertCommunityPreferences(Community community)
        {
            string sql = "CALL add_community_preferences(@communityId, @showGender, @showLocation, @showAge)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", community.Preferences.CommunityId));
            parameters.Add(new MySqlParameter("@showGender", community.Preferences.ShowGender ? 1 : 0));
            parameters.Add(new MySqlParameter("@showLocation", community.Preferences.ShowLocation ? 1 : 0));
            parameters.Add(new MySqlParameter("@showAge", community.Preferences.ShowAge ? 1 : 0));

            return Convert.ToInt32(Db.Master.GetScalar(sql, parameters));
        }

        /// <summary>
        /// updates community preference
        /// </summary>
        /// <param name="community"></param>
        /// <returns>int</returns>
        public int UpdateCommunityPreferences(Community community)
        {
            string sql = "CALL update_community_preferences(@communityPreferenceId, @communityId, @showGender, @showLocation, @showAge)"; 

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", community.Preferences.CommunityId));
            parameters.Add(new MySqlParameter("@showGender", community.Preferences.ShowGender ? 1 : 0));
            parameters.Add(new MySqlParameter("@showLocation", community.Preferences.ShowLocation ? 1 : 0));
            parameters.Add(new MySqlParameter("@showAge", community.Preferences.ShowAge ? 1 : 0));
            parameters.Add(new MySqlParameter("@communityPreferenceId", community.Preferences.CommunityPreferenceId));

            return Convert.ToInt32(Db.Master.GetScalar(sql, parameters));
        }

        /// <summary>
        /// InsertCommunityMember
        /// </summary>
        /// <param name="community"></param>
        /// <returns>int</returns>
        public int InsertCommunityMember(CommunityMember communityMember)
        {
            string sql = "CALL add_community_members (@communityId, @userId, @accountTypeId, @statusId, " +
                " @newsletter, @invitedByUserId, @allowAssetUploads, @allowForumUse," +
	            " NULL, 0, 0, 0," +
	            " 0, 0, 0, 0)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityMember.CommunityId));
            parameters.Add(new MySqlParameter("@userId", communityMember.UserId));
            parameters.Add(new MySqlParameter("@accountTypeId", communityMember.AccountTypeId));
            parameters.Add(new MySqlParameter("@statusId", communityMember.StatusId));
            parameters.Add(new MySqlParameter("@newsletter", communityMember.Newsletter));
            parameters.Add(new MySqlParameter("@invitedByUserId", communityMember.InvitedByUserId));
            parameters.Add(new MySqlParameter("@allowAssetUploads", communityMember.AllowAssetUploads));
            parameters.Add(new MySqlParameter("@allowForumUse", communityMember.AllowForumUse));
            Db.Master.ExecuteNonQuery (sql, parameters);
            return 0;
        }

        /// <summary>
        /// UpdateCommunityMember status
        /// </summary>
        public int UpdateCommunityMember(int communityId, int userId, UInt32 statusId)
        {
            string sql = "CALL update_community_members_status (@communityId, @userId, @statusId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@statusId", statusId));
            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// UpdateCommunityMember status
        /// </summary>
        public int UpdateCommunityMember(int communityId, int userId, int accountTypeId, string newsletter,
            string allowAssetUploads, string allowForumUse)
        {
            string sql = "CALL update_community_members_preferences (@communityId, @userId, " +
                    " @accountTypeId, @newsletter, @allowAssetUploads, " +
                    " @allowForumUse );";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@accountTypeId", accountTypeId));
            parameters.Add(new MySqlParameter("@newsletter", newsletter));
            parameters.Add(new MySqlParameter("@allowAssetUploads", allowAssetUploads));
            parameters.Add(new MySqlParameter("@allowForumUse", allowForumUse));
            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// UpdateCommunityMember status
        /// </summary>
        public int UpdateCommunityMember(int communityId, int userId, CommunityMember.CommunityMemberAccountType accountType)
        {
            string sql = "CALL update_community_members_account_type (@communityId, @userId, @accountTypeId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@accountTypeId", (int)accountType));
            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// RemoveMemberFromAllOwnersWorlds status
        /// </summary>
        public int RemoveMemberFromAllOwnersWorlds (int ownerId, int memberId)
        {
            string sqlString = "UPDATE community_members cm " +
                " INNER JOIN communities c ON c.community_id = cm.community_id " +
                " SET cm.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.DELETED +
                " WHERE cm.user_id = @memberId " +
                " AND c.creator_id = @ownerId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@ownerId", ownerId));
            parameters.Add (new MySqlParameter ("@memberId", memberId));

            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// DeleteAllCommunityAssets - Delete All assets from community
        /// </summary>
        /// <param name="communityId"></param>
        /// <returns>int</returns>
        public int DeleteAllCommunityAssets(int communityId)
        {
            string sql = "CALL delete_assets_from_community(@communityId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));

            return Convert.ToInt32(Db.Master.GetScalar(sql, parameters));
        }

        /// <summary>
        /// DeleteAllCommunityMembers - Delete All members from community
        /// </summary>
        /// <param name="communityId"></param>
        /// <returns>int</returns>
        public int DeleteAllCommunityMembers(int communityId)
        {
            string sql = "CALL delete_members_from_community(@communityId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));

            return Convert.ToInt32(Db.Master.GetScalar(sql, parameters));
        }

        /// <summary>
        /// Deletes a community in a recoverable way, preserving all assets,
        /// members, etc.
        /// </summary>
        /// <param name="communityId"></param>
        /// <returns>int</returns>
        public int DeleteCommunityRecoverable(int communityId)
        {
            string sql = "UPDATE communities SET status_id = @status, last_update = NOW() WHERE community_id = @communityId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@status", (int)CommunityStatus.DELETED));
            parameters.Add(new MySqlParameter("@communityId", communityId));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Restores a community to "Active" status.
        /// </summary>
        public int RestoreDeletedCommunity(int communityId)
        {
            string sql = "UPDATE communities SET status_id = @status, last_update = NOW() WHERE community_id = @communityId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@status", (int)CommunityStatus.ACTIVE));
            parameters.Add(new MySqlParameter("@communityId", communityId));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// DeleteCommunityThumbs - Delete All thumbnail url/paths
        /// </summary>
        /// <param name="communityId"></param>
        /// <returns>int</returns>
        public int DeleteCommunityThumbs(int communityId)
        {
            string sql = "CALL delete_community_thumbs(@communityId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));

            return Convert.ToInt32(Db.Master.GetScalar(sql, parameters));
        }

        /// <summary>
        /// IsActiveCommunityMember - Is user a member of community
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="userId"></param>
        /// <param name="communityId"></param>
        /// <returns>int</returns>
        public bool IsActiveCommunityMember (int communityId, int userId)
        {
            string sqlSelect = "SELECT COUNT(*) " +
                " FROM community_members " +
                " WHERE user_id = @userId " +
                " AND community_id = @communityId " +
                " AND status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE;

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            parameters.Add (new MySqlParameter ("@userId", userId));

            return (Convert.ToInt32 (Db.Master.GetScalar (sqlSelect, parameters)) > 0);
        }

        /// <summary>
        /// GetCommunityGameId - Gets game id for a community
        /// </summary>
        /// <param name="communityId"></param>
        /// <returns>int</returns>
        public int GetCommunityGameId (int communityId)
        {
            string cacheKey = CentralCache.keyCommunityId + "GIFC." + communityId + ".gameId";
            string gameId = (string)CentralCache.Get(cacheKey);

            if (gameId != null)
            {
                // Found in cache
                return Convert.ToInt32(gameId);
            }

            string sqlSelect = "SELECT game_id  " +
                " FROM communities_game " +
                " WHERE community_id = @communityId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));

            int returnId = Convert.ToInt32(Db.Master.GetScalar(sqlSelect, parameters));
            CentralCache.Store(cacheKey, returnId.ToString(), TimeSpan.FromMinutes(15));

            return returnId;
        }

        /// <summary>
        /// GetCommunityIdFromGameId - Gets community id for a game (3D App)
        /// </summary>
        /// <param name="gameId"></param>
        /// <returns>int</returns>
        public int GetCommunityIdFromGameId (int gameId)
        {
            string cacheKey = CentralCache.keyCommunityId + "CIFG." + gameId + ".communityId";
            string communityId = (string)CentralCache.Get(cacheKey);

            if (communityId != null)
            {
                // Found in cache
                return Convert.ToInt32(communityId);
            }


            string sqlSelect = "SELECT community_id  " +
                " FROM communities_game " +
                " WHERE game_id = @gameId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@gameId", gameId));

            int returnId = Convert.ToInt32(Db.Master.GetScalar(sqlSelect, parameters));
            CentralCache.Store(cacheKey, returnId.ToString(), TimeSpan.FromMinutes(15));

            return returnId;
        }

        /// <summary>
        /// GetCommunityIdFromZoneIndex - Gets community id for a zone index (3D App)
        /// </summary>
        /// <param name="gameId"></param>
        /// <returns>int</returns>
        public uint GetCommunityIdFromFaux3DAppZoneIndex (int zoneIndex)
        {
            string sqlSelect = "SELECT community_id " +
                " FROM kaneva.communities_game cg " +
                " INNER JOIN developer.faux_3d_app fa ON fa.game_id = cg.game_id " +
                " WHERE zone_index = @zoneIndex";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@zoneIndex", zoneIndex));

            return Convert.ToUInt32 (Db.Master.GetScalar (sqlSelect, parameters));
        }

        public int GetWorldOwnerFromWorldIds(int wokGameId, int gameId, int zoneIndex, int zoneInstanceId, int zoneType)
        {
            int ownerId = 0;
            int communityId = GetCommunityIdFromWorldIds(wokGameId, gameId, zoneIndex, zoneInstanceId, zoneType);

            if (communityId > 0)
            {
                string sql = " SELECT creator_id " +
                             " FROM kaneva.communities " +
                             " WHERE community_id = @communityId";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@communityId", communityId));

                ownerId = Convert.ToInt32(Db.Master.GetScalar(sql, parameters) ?? 0);
            }

            return ownerId;
        }

        /// <summary>
        /// GetCommunityIdFromWorldIds - Gets the community id for a world
        /// </summary>
        /// <returns>int</returns>
        public int GetCommunityIdFromWorldIds(int wokGameId, int gameId, int zoneIndex, int zoneInstanceId, int zoneType)
        {
            int communityId = -1;

            string sqlSelect = string.Empty;
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            // If we have a valid gameId, default to treating this as a 3dApp
            if (gameId > 0 && gameId != wokGameId)
            {
                sqlSelect = " SELECT community_id  " +
                            " FROM communities_game " +
                            " WHERE game_id = @gameId";

                parameters.Add(new MySqlParameter("@gameId", gameId));
            }
            // Process Homes
            else if (zoneType == 3 && zoneInstanceId > 0)
            {
                sqlSelect = " SELECT c.community_id " +
                            " FROM users u " +
                            " INNER JOIN communities c ON c.creator_id = u.user_id " +
                            "     AND c.community_type_id = 5 " +
                            " WHERE u.wok_player_id = @playerId";

                parameters.Add(new MySqlParameter("@playerId", zoneInstanceId));
            }
            // Process Permanent Zones
            else if (zoneType == 4 && zoneIndex > 0)
            {
                sqlSelect = " SELECT cg.community_id " +
                            " FROM developer.faux_3d_app f3d " +
                            " INNER JOIN communities_game cg ON f3d.game_id = cg.game_id " +
                            " WHERE f3d.zone_index = @zoneIndex";

                parameters.Add(new MySqlParameter("@zoneIndex", zoneIndex));
            }
            // Process Communities
            else if (zoneType == 6 && zoneInstanceId > 0)
            {
                communityId = zoneInstanceId;
            }

            // Run the query, if necessary
            if (!string.IsNullOrWhiteSpace(sqlSelect) && parameters.Count > 0)
            {
                communityId = Convert.ToInt32(Db.Master.GetScalar(sqlSelect, parameters) ?? -1);
            }

            return communityId;
        }

        /// <summary>
        /// Get a community Id
        /// </summary>
        public int GetCommunityIdFromName(string communityName, bool isPersonal)
        {
            communityName = communityName.Replace(" ", "");
            string cacheKey = CentralCache.keyCommunityId + communityName.ToString() + (isPersonal ? "Y" : "N").ToString();
            string communityId = (string) CentralCache.Get(cacheKey);

            if (communityId != null)
            {
                // Found in cache
                return Convert.ToInt32(communityId);
            }

            StringBuilder sqlSelect = new StringBuilder();
            sqlSelect.Append(" SELECT c.community_id ");
            sqlSelect.Append(" FROM communities c ");
            sqlSelect.Append(" WHERE c.name_no_spaces = @communityName ");

            if (isPersonal)
            {
                sqlSelect.Append (" AND is_personal = 1 ");    
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityName", communityName));
            DataRow drCommunity = Db.Slave2.GetDataRow(sqlSelect.ToString(), parameters);

            if (drCommunity != null && drCommunity["community_id"] != DBNull.Value)
            {
//                CentralCache.Store(cacheKey, drCommunity["community_id"].ToString (), TimeSpan.FromMinutes(15));
                return Convert.ToInt32(drCommunity["community_id"]);
            }

//            CentralCache.Store(cacheKey, "0", TimeSpan.FromMinutes(5));
            return 0;
        }

        public void InsertAPIAuth(int communityId, string consumerKey, string consumerSecret)
        {
            // Send the message
            string sql = "INSERT INTO api_authentication " +
                "(community_id, consumer_key, consumer_secret) VALUES (@communityId, @consumerKey, @consumerSecret) " +
                " ON DUPLICATE KEY UPDATE consumer_key =  @consumerKey, consumer_secret = @consumerSecret ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            parameters.Add(new MySqlParameter("@consumerKey", consumerKey));
            parameters.Add(new MySqlParameter("@consumerSecret", consumerSecret));
            Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// APIAuthentication
        /// </summary>
        /// <param name="consumerKey"></param>
        /// <returns></returns>
        public APIAuthentication GetAPIAuth(int communityId)
        {
            string sql = "SELECT aa.community_id, consumer_key, consumer_secret, game_id, send_blast, admin_override_blast " +
               " FROM kaneva.api_authentication aa " +
               " LEFT OUTER JOIN kaneva.communities_game cg ON aa.community_id = cg.community_id " +
               " WHERE aa.community_id = @communityId ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));

            //perform query returning single data row from the developer database
            DataRow row = Db.Master.GetDataRow(sql.ToString(), parameters);

            if (row == null)
            {
                return new APIAuthentication();
            }

            //populate new instance of Game Object
            return new APIAuthentication(Convert.ToInt32(row["community_id"]), row["consumer_key"].ToString(), row["consumer_secret"].ToString()
                , Convert.ToInt32(row["game_id"] == DBNull.Value ? "0" : row["game_id"]), row["admin_override_blast"].ToString().Equals("Y"), row["send_blast"].ToString().Equals("Y")
                );
        }

        /// <summary>
        /// APIAuthentication
        /// </summary>
        /// <param name="consumerKey"></param>
        /// <returns></returns>
        public APIAuthentication GetAPIAuth(string consumerKey, string consumerSecret)
        {
            APIAuthentication apiAuth = (APIAuthentication)CentralCache.Get(CentralCache.keyAPIAuthAuthentication + "wS" + consumerKey.ToString());

            if (apiAuth != null)
            {
                // Found in cache
                return apiAuth;
            }

            string sql = "SELECT aa.community_id, consumer_key, consumer_secret, game_id, send_blast, admin_override_blast " +
                " FROM kaneva.api_authentication aa " +
                " LEFT OUTER JOIN kaneva.communities_game cg ON aa.community_id = cg.community_id " +
                " WHERE consumer_key = @consumerKey  " +
                " AND consumer_secret = @consumerSecret";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@consumerKey", consumerKey));
            parameters.Add(new MySqlParameter("@consumerSecret", consumerSecret));

            //perform query returning single data row from the developer database
            DataRow row = Db.Master.GetDataRow(sql.ToString(), parameters);

            if (row == null)
            {
                return new APIAuthentication();
            }

            //populate new instance of Game Object
            apiAuth = new APIAuthentication(Convert.ToInt32(row["community_id"]), row["consumer_key"].ToString(), row["consumer_secret"].ToString()
                , Convert.ToInt32(row["game_id"] == DBNull.Value ? "0" : row["game_id"]), row["admin_override_blast"].ToString().Equals("Y"), row["send_blast"].ToString().Equals("Y")
                );

            CentralCache.Store(CentralCache.keyAPIAuthAuthentication + "wS" + consumerKey.ToString(), apiAuth, TimeSpan.FromMinutes(5));
            return apiAuth;
        }

        /// <summary>
        /// APIAuthentication
        /// </summary>
        /// <param name="consumerKey"></param>
        /// <returns></returns>
        public APIAuthentication GetAPIAuth(string consumerKey)
        {
            APIAuthentication apiAuth = (APIAuthentication)CentralCache.Get(CentralCache.keyAPIAuthAuthentication + consumerKey.ToString());

            if (apiAuth != null)
            {
                // Found in cache
                return apiAuth;
            }

            string sql = "SELECT aa.community_id, consumer_key, consumer_secret, game_id, send_blast, admin_override_blast " +
                " FROM kaneva.api_authentication aa " +
                " LEFT OUTER JOIN kaneva.communities_game cg ON aa.community_id = cg.community_id " +
                " WHERE consumer_key = @consumerKey  ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@consumerKey", consumerKey));

            //perform query returning single data row from the developer database
            DataRow row = Db.Master.GetDataRow(sql.ToString(), parameters);

            if (row == null)
            {
                return new APIAuthentication();
            }


            //populate new instance of Game Object
            apiAuth = new APIAuthentication(Convert.ToInt32(row["community_id"]), row["consumer_key"].ToString(), row["consumer_secret"].ToString()
                , Convert.ToInt32(row["game_id"] == DBNull.Value ? "0" : row["game_id"]), row["admin_override_blast"].ToString().Equals("Y"), row["send_blast"].ToString().Equals("Y")
                );

            CentralCache.Store(CentralCache.keyAPIAuthAuthentication + consumerKey.ToString(), apiAuth, TimeSpan.FromMinutes(5));
            return apiAuth;
        }

        /// <summary>
        /// GetCommunities
        /// </summary>
        public PagedList<Community> GetUserTopWorlds (int userId, int[] communityTypeIds, string orderBy, string filter, int pageNumber, int pageSize, bool includeCreditBalance, int premiumItemDaysPendingBeforeRedeem)
        {
            string sqlSelect = " SELECT c.community_id, c.place_type_id, c.name_no_spaces, c.url, c.creator_id, " +
                " c.created_date, c.last_edit, c.community_type_id, c.status_id, c.email, c.is_public, c.is_adult, c.thumbnail_path, " +
                " c.thumbnail_small_path, c.thumbnail_medium_path, c.thumbnail_large_path, c.thumbnail_xlarge_path, " +
                " c.thumbnail_type, c.allow_member_events, c.is_personal, c.template_id, c.over_21_required, c.has_thumbnail, " +
                " c.name, c.description, c.background_rgb, c.background_image, c.creator_username, c.keywords, c.allow_publishing, " +
                " cs.number_of_views, cs.number_of_members, cs.number_of_diggs, cm.account_type_id, " +
                " cs.number_times_shared, cs.number_of_pending_members, cs.number_posts_7_days, " +
                " u.zip_code, u.age, u.gender, u.country, u.location, u.mature_profile, us.number_of_friends, " +
                " IF (last_visited_at > created_date, last_visited_at, created_date) AS last_touched, " +
                " COALESCE(g.game_id,0) as game_id ";

            string sqlTables = " FROM community_members cm " +
                " INNER JOIN communities c ON c.community_id = cm.community_id " +
                " INNER JOIN channel_stats cs ON c.community_id = cs.channel_id " +
                " INNER JOIN users u ON u.user_id = c.creator_id " +
                " INNER JOIN users_stats us ON us.user_id = c.creator_id " +
                " LEFT JOIN communities_game cg ON cg.community_id = c.community_id " + 
                " LEFT JOIN developer.games g on g.game_id = cg.game_id ";

            string sqlWhere = " WHERE cm.user_id = @userId " + 
                " AND c.place_type_id < 99 " +
                " AND cm.status_id = @memberStatus " +
                " AND c.status_id = @statusId ";

            if (includeCreditBalance)          // ONLY GET CREDIT AMT FOR WORLDS USER OWNS
            {
                sqlSelect += ", COALESCE((SELECT SUM(ip.purchase_price) " +  
	                " FROM premium_item_credit_redemption picr " + 
	                " INNER JOIN shopping.item_purchases ip ON ip.purchase_id = picr.purchase_id " +
                    " WHERE picr.community_id = cm.community_id " +
                    " AND picr.redemption_date IS NULL " +
                    " AND ip.purchase_date < DATE_SUB(NOW(), INTERVAL " + premiumItemDaysPendingBeforeRedeem + " DAY)),0) AS world_credit_balance ";
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@statusId", (int) CommunityStatus.ACTIVE));
            parameters.Add (new MySqlParameter ("@memberStatus", (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE));
            parameters.Add (new MySqlParameter ("@userId", userId));

            if (communityTypeIds.Length > 0)
            {
                sqlWhere += " AND c.community_type_id IN (";
                for (int i = 0; i < communityTypeIds.Length; i++)
                {
                    sqlWhere += "@communityTypeId" + i + ",";
                    parameters.Add (new MySqlParameter ("@communityTypeId" + i, communityTypeIds[i]));
                }
                sqlWhere = sqlWhere.TrimEnd (',') + ") ";
            }

            string sql = sqlSelect + sqlTables + sqlWhere;

            if (filter.Length > 0)
            {
                sql += " AND " + filter;
            }

            PagedDataTable pdt = Db.Master.GetPagedDataTable (sql, orderBy, parameters, pageNumber, pageSize);

            PagedList<Community> list = new PagedList<Community> ();
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                Community community = new Community (Convert.ToInt32 (row["community_id"]), row["name_no_spaces"].ToString (),
                    row["url"].ToString (), Convert.ToInt32 (row["creator_id"]), Convert.ToDateTime (row["created_date"]), Convert.ToDateTime (row["last_edit"]),
                    Convert.ToInt32 (row["status_id"]), row["email"].ToString (), row["is_public"].ToString (), row["is_adult"].ToString (), row["thumbnail_path"].ToString (),
                    row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (), row["thumbnail_xlarge_path"].ToString (),
                    row["thumbnail_type"].ToString (), Convert.ToInt32 (row["allow_publishing"]), Convert.ToInt32 (row["allow_member_events"]), Convert.ToInt32 (row["is_personal"]), Convert.ToUInt32 (row["template_id"]),
                    row["over_21_required"].ToString ().Equals ("Y"), row["has_thumbnail"].ToString ().Equals ("Y"), row["name"].ToString (), row["description"].ToString (),
                    row["background_rgb"].ToString (), row["background_image"].ToString (), row["creator_username"].ToString (), row["keywords"].ToString (), row["zip_code"].ToString (),
                    Convert.ToInt32 (row["age"]), row["gender"].ToString (), row["country"].ToString (), row["location"].ToString (), row["mature_profile"].ToString ().Equals ("Y"),
                    Convert.ToInt32 (row["number_of_friends"]), Convert.ToInt32 (row["place_type_id"]), Convert.ToInt32 (row["community_type_id"]));

                //added/left here due to idea that in a list the proxy would create too many query calls
                CommunityStats communityStats = new CommunityStats ();
                communityStats.NumberOfViews = Convert.ToInt32 (row["number_of_views"]);
                communityStats.NumberOfMembers = Convert.ToInt32 (row["number_of_members"]);
                communityStats.NumberOfDiggs = Convert.ToInt32 (row["number_of_diggs"]);
                communityStats.NumberTimesShared = Convert.ToInt32 (row["number_times_shared"]);
                communityStats.NumberOfPendingMembers = Convert.ToInt32 (row["number_of_pending_members"]);
                communityStats.NumberPosts7Days = Convert.ToUInt32 (row["number_posts_7_days"]);

                community.Stats = communityStats;

                WOK3DApp wok3DApp = new WOK3DApp ();
                wok3DApp.GameId = Convert.ToInt32 (row["game_id"]);
                community.WOK3App = wok3DApp;

                if (userId > 0)
                {
                    //create new member object to hold the member information
                    CommunityMember member = new CommunityMember ();

                    //populate the queried member data
                    member.AccountTypeId = Convert.ToInt32 (row["account_type_id"]);

                    //create the member list and add this member to the list
                    List<CommunityMember> memberList = new List<CommunityMember> ();
                    memberList.Add (member);

                    //add list to the community
                    community.MemberList = memberList;
                }

                list.Add (community);
            }
      
            return list;
        }

        /// <summary>
        /// Update a community
        /// </summary>
        public int UpdateCommunityParentId(int communityId, int parentId)
        {
            string sqlString = "UPDATE communities " +
                " SET " +
                " parent_community_id = @parentId " +
                " WHERE community_id = @communityId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            parameters.Add(new MySqlParameter("@parentId", parentId));

            Db.Master.ExecuteNonQuery(sqlString, parameters);
            return 0;
        }

        public int InsertHomeCommunityIdAsChildWorld(int communityId)
        {
            // Send the message
            string sql = "INSERT INTO child_home_worlds " +
                "(community_id) VALUES (@communityId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int RemoveHomeCommunityIdAsChildWorld(int communityId)
        {
            // Send the message
            string sql = "DELETE FROM child_home_worlds " +
                " WHERE community_id = @communityId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            return Db.Master.ExecuteNonQuery(sql, parameters);
        }


        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    }
}
