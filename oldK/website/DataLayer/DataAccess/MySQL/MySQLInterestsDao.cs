///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLInterestsDao : IInterestsDao
    {
        /// <summary>
        /// GetInterestCategories
        /// </summary>
        /// <returns></returns>
        public IList<InterestCategory> GetInterestCategories()
        {
            string strQuery = "SELECT ic.ic_id, ic.category_text FROM interest_categories ic ORDER BY category_text" ;

            //List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            DataTable dt = Db.Master.GetDataTable(strQuery);

            IList<InterestCategory> list = new List<InterestCategory>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new InterestCategory(Convert.ToUInt32(row["ic_id"]),row["category_text"].ToString()));
            }
            return list;
        }

        /// <summary>
        /// GetInterestCategories
        /// </summary>
        /// <returns></returns>
        public DataTable GetCommonInterests(int userId, int userId2)
        {
            string sqlSelect = "SELECT ui.interest_id, i.interest, ic.category_text " +
                " FROM kaneva.user_interests ui " +
                " INNER JOIN interests i ON i.interest_id = ui.interest_id " +
                " INNER JOIN interest_categories ic ON ic.ic_id = ui.ic_id " +
                " WHERE user_id = @userId " +
                " AND (ui.interest_id IN (SELECT interest_id FROM kaneva.user_interests WHERE user_id = @userId2)) " +
                " AND ic.ic_id NOT IN (21) " +
                " ORDER BY category_text, interest;";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@userId2", userId2));

            return Db.Master.GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// GetUserInterests
        /// </summary>
        /// <returns></returns>
        public DataTable GetUserInterests(int userId, string filter, string orderBy)
        {
            string sqlSelect = "SELECT ui.interest_id, i.interest, i.user_count, ui.ic_id, ic.category_text, ui.z_order " +
                " FROM user_interests ui " +
                " INNER JOIN interests i ON i.interest_id = ui.interest_id " +
                " INNER JOIN interest_categories ic ON ic.ic_id = ui.ic_id " +
                " WHERE ui.user_id = @userId " +
                " AND ic.ic_id NOT IN (21) ";

            if (filter.Length > 0)
            {
                sqlSelect += " AND " + filter;
            }

            if (orderBy.Length > 0)
            {
                sqlSelect += " ORDER BY " + orderBy + ";";
            }
            else
            {
                sqlSelect += " ORDER BY category_text, interest; ";
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            return Db.Master.GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// GetUserInterests
        /// </summary>
        /// <returns></returns>
        public DataTable GetInterests(int userId)
        {
            string sqlSelect = "SELECT ic.ic_id, ic.category_text, X.interest, X.interest_id, X.user_count, X.z_order " +
                " FROM interest_categories ic " +
                " LEFT JOIN " +
                " (SELECT ui.ic_id, i.interest, i.interest_id, i.user_count, ui.z_order FROM interests i " +
                " INNER JOIN user_interests ui on ui.interest_id = i.interest_id AND ui.user_id = @userId " +
                " ) X on X.ic_id = ic.ic_id " +
                " WHERE ic.ic_id NOT IN (21) " +
                " ORDER BY category_text, X.interest; ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            return Db.Master.GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// GetUserInterests
        /// </summary>
        /// <returns></returns>
        public DataTable GetInterestByCategory(int userId, int icId)
        {
            string sqlSelect = "SELECT ic.ic_id, ic.category_text, X.interest, X.interest_id, X.user_count, X.z_order " +
                " FROM interest_categories ic " +
                " LEFT JOIN " +
                " (SELECT ui.ic_id, i.interest, i.interest_id, i.user_count, ui.z_order FROM interests i " +
                " INNER JOIN user_interests ui on ui.interest_id = i.interest_id AND ui.user_id = @userId " +
                " ) X on X.ic_id = ic.ic_id " +
                " WHERE ic.ic_id = @icId " +
                " ORDER BY X.interest; ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@icId", icId));

            return Db.Master.GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// GetCommonInterestsByCategory
        /// </summary>
        /// <returns></returns>
        public DataTable GetCommonInterestsByCategory(int userId, int userId2, int icId)
        {
            string sqlSelect = "SELECT ui.interest_id, i.interest, ic.category_text " +
                " FROM kaneva.user_interests ui " +
                " INNER JOIN interests i ON i.interest_id = ui.interest_id " +
                " INNER JOIN interest_categories ic ON ic.ic_id = ui.ic_id " +
                " WHERE user_id = @userId " +
                " AND (ui.interest_id IN (SELECT interest_id FROM kaneva.user_interests WHERE user_id = @userId2)) " +
                " AND ic.ic_id = @icId " +
                " ORDER BY category_text, interest;";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@userId2", userId2));
            parameters.Add(new MySqlParameter("@icId", icId));

            return Db.Master.GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// GetCommonInterestsByCategory
        /// </summary>
        /// <returns></returns>
        public DataTable GetUserInterestsByCategory(int userId, int icId)
        {
            string sqlSelect = "SELECT ui.interest_id, i.interest, i.user_count, ui.ic_id, ic.category_text, ui.z_order " +
                " FROM user_interests ui " +
                " INNER JOIN interests i ON i.interest_id = ui.interest_id " +
                " INNER JOIN interest_categories ic ON ic.ic_id = ui.ic_id " +
                " WHERE ui.user_id = @userId " +
                " AND ic.ic_id = @icId " +
                " ORDER BY category_text, interest; ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@icId", icId));

            return Db.Master.GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// GetCommonSchools
        /// </summary>
        /// <returns></returns>
        public DataTable GetCommonSchools(int userId, int userId2)
        {
            string sqlSelect = "SELECT us.school_id, s.name " +
                " FROM user_schools us " +
                " INNER JOIN user_schools us2 ON us.school_id = us2.school_id " +
                " INNER JOIN schools s ON us.school_id = s.school_id " +
                " WHERE us.user_id=@userId AND us2.user_id=@userId2 " +
                " ORDER BY s.name;";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@userId2", userId2));

            return Db.Master.GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// GetCommonSchools
        /// </summary>
        /// <returns></returns>
        public DataTable GetUserSchools(int userId, string filter, string orderBy)
        {
            string sqlSelect = "SELECT s.school_id, s.name, s.city, s.state " +
                " FROM schools s " +
                " INNER JOIN user_schools us ON us.school_id = s.school_id " +
                " WHERE us.user_id = @userId ";

            if (filter.Length > 0)
            {
                sqlSelect += " AND " + filter;
            }

            if (orderBy.Length > 0)
            {
                sqlSelect += " ORDER BY " + orderBy + ";";
            }
            else
            {
                sqlSelect += " ORDER BY s.name; ";
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            return Db.Master.GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// remove the interest from all users.
        /// </summary>
        /// <param name="interestId"></param>
        public int RemoveInterestFromAllUsers(int interestId)
        {
            string sqlString = "CALL remove_interest_from_all_users (@interestId);";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@interestId", interestId));

            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// remove the interest from all users.
        /// </summary>
        /// <param name="interestId"></param>
        public int RemoveInterestFromUser(int userId, int interestId)
        {
            string sqlString = "CALL remove_interest_from_user (@userId, @interestId);";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@interestId", interestId));

            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// remove the interest from all users.
        /// </summary>
        /// <param name="interestId"></param>
        public int RemoveFromInterestCount(int interestId)
        {
            string sqlSubtractProc = "CALL remove_from_interest_count(@interestId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@interestId", interestId));

            return Db.Master.ExecuteNonQuery(sqlSubtractProc, parameters);
        }

        /// <summary>
        /// remove the interest from all users.
        /// </summary>
        /// <param name="interestId"></param>
        public int AssignInterestToUser(int userId, int interestId, int icId)
        {
            string sqlSelect = "CALL assign_interest_to_user(@userId, @interestId, @icId, @rc); Select CAST(@rc as UNSIGNED INT) as rc;";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@interestId", interestId));
            parameters.Add(new MySqlParameter("@icId", icId));

            DataRow dr = Db.Master.GetDataRow(sqlSelect, parameters);

            return Convert.ToInt32(dr["rc"]); // 0 = ok, -1 = interest already exists for this user
        }

        /// <summary>
        /// update the user's searchable interest.
        /// </summary>
        /// <param name="interestId"></param>
        public int UpdateUsersSearchableInterests(int userId)
        {
            string sqlString = "CALL update_my_searchable_interests(@userId);";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// update the user's searchable interest.
        /// </summary>
        /// <param name="interestId"></param>
        public int AddInterestsToInterestCount(int interestId)
        {
            string sqlAddProc = "CALL add_to_interest_count(@interestId)";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@interestId", interestId));

            return Db.Master.ExecuteNonQuery (sqlAddProc, parameters);
        }

        /// <summary>
        /// associates a school to a user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="schoolId"></param>
        public int AssociateSchoolToUser(int userId, int schoolId)
        {
            string sqlSelect = "CALL associate_school(@userId, @schoolId, @rc); Select CAST(@rc as UNSIGNED INT) as rc;";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@schoolId", schoolId));

            DataRow dr = Db.Master.GetDataRow(sqlSelect, parameters);

            return Convert.ToInt32(dr["rc"]); // 0 = ok, -1 = interest already exists for this user
        }

        /// <summary>
        /// checks to see if the provided school already exists
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="schoolId"></param>
        public int SchoolExists(string name)
        {
            string sqlSelect = "SELECT school_exists(@name);";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@name", name));

            DataRow drSchool = Db.Master.GetDataRow(sqlSelect, parameters);

            return Convert.ToInt32(drSchool[0]); // 0 = does not exist, > 0 is interest id 
        }

        /// <summary>
        /// update the user's searchable interest.
        /// </summary>
        /// <param name="interestId"></param>
        public int AddToSchoolCount(int schoolId)
        {
            string sqlAddSelect = "CALL add_to_schoolscount(@schoolId)";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@schoolId", schoolId));

            return Db.Master.ExecuteNonQuery (sqlAddSelect, parameters);
        }

        /// <summary>
        /// add a new school.
        /// </summary>
        /// <param name="interestId"></param>
        public int AddSchool(string name, string city, string state)
        {
            string sqlSelect = "CALL add_school(@name, @city, @state, @rc); SELECT CAST(@rc as UNSIGNED INT) as rc;";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@name", name));
            parameters.Add(new MySqlParameter("@city", city));
            parameters.Add(new MySqlParameter("@state", state));

            DataRow dr = Db.Master.GetDataRow(sqlSelect, parameters);
            
            return Convert.ToInt32(dr["rc"]);
        }

        /// <summary>
        /// add an interest to 
        /// </summary>
        /// <param name="interestId"></param>
        public int AddInterest(int icId, string interest)
        {
            string sqlSelect = "CALL add_interest(@icId, @interest, @rc); SELECT CAST(@rc as UNSIGNED INT) as rc;";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@interest", interest));
            parameters.Add(new MySqlParameter("@icId", icId));

            DataRow dr = Db.Master.GetDataRow(sqlSelect, parameters);

            return Convert.ToInt32(dr["rc"]);
        }

        /// <summary>
        /// add an interest to 
        /// </summary>
        /// <param name="interestId"></param>
        public int InterestExists(int icId, string interest)
        {
            string sqlSelect = "SELECT interest_exists(@icId, @interest);";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@interest", interest));
            parameters.Add(new MySqlParameter("@icId", icId));

            DataRow drInterest = Db.Master.GetDataRow(sqlSelect, parameters);

            return Convert.ToInt32(drInterest[0]); // 0 = does not exist, > 0 is interest id 
        }

        /// <summary>
        /// remove the school from the users.
        /// </summary>
        /// <param name="interestId"></param>
        public int RemoveSchoolFromUser(int userId, int schoolId)
        {
            string sqlString = "CALL remove_associated_school (@userId, @schoolId);";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@schoolId", schoolId));

            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// remove the interest from all users.
        /// </summary>
        /// <param name="interestId"></param>
        public int RemoveFromSchoolCount(int schoolId)
        {
            string sqlSubtractProc = "CALL remove_from_schoolscount(@schoolId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@schoolId", schoolId));

            return Db.Master.ExecuteNonQuery(sqlSubtractProc, parameters);
        }


    }
}
