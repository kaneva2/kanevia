///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    /// <summary>
    /// Sql Server specific factory that creates Sql Server specific data access objects.
    /// 
    /// GoF Design Pattern: Factory.
    /// </summary>
    public class MySQLDaoFactory : DaoFactory
    {

        /// <summary>
        /// Gets a Sql server specific Event access object.
        /// </summary>
        public override IEventDao EventDao
        {
            get { return new MySQLEventDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific violation access object.
        /// </summary>
        public override IViolationsDao ViolationsDao
        {
            get { return new MySQLViolationsDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific User data access object.
        /// </summary>
        public override IPromotionsDao PromotionsDao
        {
            get { return new MYSQLPromotionsDao(); }
        }

        /// <summary>
        /// Gets a Sql specific interest access object.
        /// </summary>
        public override IInterestsDao InterestsDao
        {
            get { return new MySQLInterestsDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific User data access object.
        /// </summary>
        public override IDevelopmentCompanyDao DevelopmentCompanyDao
        {
            get { return new MySQLDevelopmentCompanyDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific Kaching data access object.
        /// </summary>
        public override IKachingDao KachingDao
        {
            get { return new MySQLKachingDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific Kaching data access object.
        /// </summary>
        public override IMarketingDao MarketingDao
        {
            get { return new MySQLMarketingDao(); }
        }


        /// <summary>
        /// Gets a Sql server specific User data access object.
        /// </summary>
        public override ISiteManagementDao SiteManagementDao
        {
            get { return new MySQLSiteManagementDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific User data access object.
        /// </summary>
        public override ISiteSecurityDao SiteSecurityDao
        {
            get { return new MySQLSiteSecurityDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific User data access object.
        /// </summary>
        public override IGameDeveloperDao GameDeveloperDao
        {
            get { return new MYSQLGameDeveloperDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific User data access object.
        /// </summary>
        public override IUserDao UserDao
        {
            get { return new MySQLUserDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific User data access object.
        /// </summary>
        public override IGameDao GameDao
        {
            get { return new MYSQLGameDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific User data access object.
        /// </summary>
        public override IGameServerDao GameServerDao
        {
            get { return new MYSQLGameServerDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific User data access object.
        /// </summary>
        public override IGameLicenseDao GameLicenseDao
        {
            get { return new MYSQLGameLicenseDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific User data access object.
        /// </summary>
        public override IWorldTemplateDao WorldTemplateDao
        {
            get { return new MySQLWorldTemplateDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific Comment data access object.
        /// </summary>
        public override ICommentsDao CommentsDao
        {
            get { return new MySQLCommentDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific Blast data access object.
        /// </summary>
        public override IBlastDao BlastDao
        {
            get { return new MySQLBlastDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific Community data access object.
        /// </summary>
        public override ICommunityDao CommunityDao
        {
            get { return new MySQLCommunityDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific Contest data access object.
        /// </summary>
        public override IContestDao ContestDao
        {
            get { return new MySQLContestDao (); }
        }

        /// <summary>
        /// Gets a Sql server specific Blog data access object.
        /// </summary>
        public override IBlogDao BlogDao
        {
            get { return new MySQLBlogDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific Forum data access object.
        /// </summary>
        public override IForumDao ForumDao
        {
            get { return new MySQLForumDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific Fame data access object.
        /// </summary>
        public override IFameDao FameDao
        {
            get { return new MySQLFameDao(); }
        }

        /// <summary>
        /// Gets a Error Logging data access object.
        /// </summary>
        public override IErrorLoggingDao ErrorLoggingDao 
        {
            get
            {
                return new MySQLErrorLoggingDao();
            }
        }


        /// <summary>
        /// Gets a Metrics data access object.
        /// </summary>
        public override IMetricsDao MetricsDao
        {
            get
            {
                return new MySQLMetricsDao();
            }
        }


        /// <summary>
        /// Gets a Sql server specific Media data access object.
        /// </summary>
        public override IMediaDao MediaDao
        {
            get { return new MySQLMediaDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific Media data access object.
        /// </summary>
        public override IShoppingDao ShoppingDao
        {
            get { return new MySQLShoppingDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific ScriptGameItem data access object.
        /// </summary>
        public override IScriptGameItemDao ScriptGameItemDao
        {
            get { return new MySQLScriptGameItemDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific SGCustomDataItem data access object.
        /// </summary>
        public override ISGCustomDataDao SGCustomDataDao
        {
            get { return new MySQLSGCustomDataDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific SGCustomDataItem data access object.
        /// </summary>
        public override ISGFrameworkSettingsDao SGFrameworkSettingsDao
        {
            get { return new MySQLSGFrameworkSettingsDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific Transactions data access object.
        /// </summary>
        public override ITransactionDao TransactionDao
        {
            get { return new MySQLTransactionDao (); }
        }

        /// <summary>
        /// Gets a Sql server specific Subcription data access object.
        /// </summary>
        public override ISubscriptionDao SubscriptionDao
        {
            get { return new MySQLSubscriptionDao(); }
        }

        /// <summary>
        /// Gets a Sql server specific Rave data access object.
        /// </summary>
        public override IRaveDao RaveDao
        {
            get { return new MySQLRaveDao (); }
        }

        /// <summary>
        /// Gets a Sql server specific Configuration data access object.
        /// </summary>
        public override IConfigurationDao ConfigurationDao
        {
            get { return new MySQLConfigurationDao(); }
        }

		/// <summary>
		/// Gets a Sql server specific Mail data access object.
		/// </summary>
		public override IMailDao MailDao
		{
			get { return new MySQLMailDao(); }
		}

        /// <summary>
        /// Gets a Sql server specific Sitemap data access object.
        /// </summary>
        public override ISitemapDao SitemapDao
        {
            get { return new MySQLSitemapDao (); }
        }

        /// <summary>
        /// Gets a Sql server specific Experiment data access object.
        /// </summary>
        public override IExperimentDao ExperimentDao
        {
            get { return new MySQLExperimentDao(); }
        }
    }
}
