///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLViolationsDao : IViolationsDao
    {
        #region Violation Reports

        /// <summary>
        /// Gets all violation reports in a paged format
        /// </summary>
        /// <returns>IList<WebSite></returns>
        public PagedList<ViolationReport> GetAllViolationReports(string filter, string orderby, int pageNumber, int pageSize)
        {
            // get all roles in pages datatable
            string strQuery = "SELECT violation_id, asset_id, wok_item_id, owner_id, reporter_id, violation_type_id, violation_action_type_id, " +
            " member_comments, csr_notes, report_date, last_updated, modifiers_id FROM violation_reports";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (filter.Length > 0)
            {
                strQuery += " WHERE " + filter;
            }

            PagedDataTable dt = Db.Master.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<ViolationReport> list = new PagedList<ViolationReport>();
            //assigns total count
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new ViolationReport(Convert.ToUInt32(row["violation_id"]), Convert.ToUInt32(row["asset_id"]), Convert.ToUInt32(row["wok_item_id"]),
                    Convert.ToUInt32(row["owner_id"]), Convert.ToUInt32(row["reporter_id"]), Convert.ToUInt32(row["violation_type_id"]), 
                    Convert.ToUInt32(row["violation_action_type_id"]), row["member_comments"].ToString(),
                    row["csr_notes"].ToString(), Convert.ToDateTime(row["report_date"]), Convert.ToDateTime(row["last_updated"]), Convert.ToUInt32(row["modifiers_id"])));
            }
            return list;
        }

        /// <summary>
        /// Gets all violation reports in a paged format
        /// </summary>
        /// <returns>IList<WebSite></returns>
        public PagedList<ViolationReport> GetAllViolationReportsJoined(string filter, string orderby, int pageNumber, int pageSize)
        {
            // get all roles in pages datatable
            string strQuery = "SELECT violation_id, vr.asset_id, wok_item_id, vr.owner_id, reporter_id, violation_type_id, violation_action_type_id, " +
            " member_comments, csr_notes, report_date, last_updated, vr.modifiers_id, u1.username AS owner_name, u2.username AS reporter_name, " +
            " a.name AS asset_name, i.name AS wok_item_name FROM violation_reports vr INNER JOIN users u1 ON owner_id = u1.user_id" +
            " INNER JOIN users u2 ON reporter_id = u2.user_id LEFT JOIN assets a ON vr.asset_id = a.asset_id LEFT JOIN " + 
            Db.WOK.Name.ToLower() + ".items i ON vr.wok_item_id = i.global_id" ;

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (filter.Length > 0)
            {
                strQuery += " WHERE " + filter;
            }

            PagedDataTable dt = Db.Master.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<ViolationReport> list = new PagedList<ViolationReport>();
            //assigns total count
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new ViolationReport(Convert.ToUInt32(row["violation_id"]), Convert.ToUInt32(row["asset_id"]), Convert.ToUInt32(row["wok_item_id"]),
                    Convert.ToUInt32(row["owner_id"]), Convert.ToUInt32(row["reporter_id"]), Convert.ToUInt32(row["violation_type_id"]),
                    Convert.ToUInt32(row["violation_action_type_id"]), row["member_comments"].ToString(),
                    row["csr_notes"].ToString(), Convert.ToDateTime(row["report_date"]), Convert.ToDateTime(row["last_updated"]), Convert.ToUInt32(row["modifiers_id"])));
            }
            return list;
        }

        /// <summary>
        /// Gets all violation reports in a paged format grouped by item id ordered by Date descing
        /// </summary>
        /// <returns>IList<WebSite></returns>
        public PagedList<ViolationReport> GetAllViolationReportsGrouped(string filter, int pageNumber, int pageSize)
        {
            // get all roles in pages datatable
            string strQuery = "SELECT violation_id, asset_id, wok_item_id, owner_id, reporter_id, violation_type_id, violation_action_type_id, " +
            " member_comments, csr_notes, report_date, last_updated, modifiers_id FROM violation_reports";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (filter.Length > 0)
            {
                strQuery += " WHERE " + filter;
            }

            strQuery += " GROUP BY wok_item_id, asset_id, violation_type_id, reporter_id";

            PagedDataTable dt = Db.Master.GetPagedDataTable(strQuery, "", parameters, pageNumber, pageSize);

            PagedList<ViolationReport> list = new PagedList<ViolationReport>();
            //assigns total count
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new ViolationReport(Convert.ToUInt32(row["violation_id"]), Convert.ToUInt32(row["asset_id"]), Convert.ToUInt32(row["wok_item_id"]),
                    Convert.ToUInt32(row["owner_id"]), Convert.ToUInt32(row["reporter_id"]), Convert.ToUInt32(row["violation_type_id"]),
                    Convert.ToUInt32(row["violation_action_type_id"]), row["member_comments"].ToString(),
                    row["csr_notes"].ToString(), Convert.ToDateTime(row["report_date"]), Convert.ToDateTime(row["last_updated"]), Convert.ToUInt32(row["modifiers_id"])));
            }
            return list;
        }

        public DataTable GetViolationCounts(uint assetId, uint wokItemId)
        {
            //create query string to get game by user id
            string sql = "SELECT COUNT(wok_item_id) as number_violations, asset_id, wok_item_id FROM violation_reports ";
            
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (assetId > 0)
            {
                parameters.Add(new MySqlParameter("@assetId", assetId));
                sql += " WHERE asset_id = @assetId ";
            }
            else if (wokItemId > 0)
            {
                parameters.Add(new MySqlParameter("@wokItemId", wokItemId));
                sql += " WHERE wok_item_id = @wokItemId ";
            }

            sql += " GROUP BY wok_item_id, asset_id";

            return Db.Master.GetDataTable(sql.ToString(),parameters);
        }

        /// <summary>
        /// Get the Violation Report for an item based on the Violation Id
        /// </summary>
        /// <param name="report"></param>
        /// <returns>ViolationReport</returns>
        public ViolationReport GetViolationReportByReportId(ViolationReport report)
        {
            ViolationReport violation = new ViolationReport();

            // get all roles in pages datatable
            string strQuery = "SELECT violation_id, asset_id, wok_item_id, owner_id, reporter_id, violation_type_id, violation_action_type_id, " +
            " member_comments, csr_notes, report_date, last_updated, modifiers_id FROM violation_reports WHERE violation_id = @violation_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@violation_id", report.ViolationID));

            DataRow row = Db.Master.GetDataRow(strQuery, parameters);

            if (row != null)
            {
                violation = new ViolationReport(Convert.ToUInt32(row["violation_id"]), Convert.ToUInt32(row["asset_id"]), Convert.ToUInt32(row["wok_item_id"]),
                    Convert.ToUInt32(row["owner_id"]), Convert.ToUInt32(row["reporter_id"]), Convert.ToUInt32(row["violation_type_id"]),
                    Convert.ToUInt32(row["violation_action_type_id"]), row["member_comments"].ToString(),
                    row["csr_notes"].ToString(), Convert.ToDateTime(row["report_date"]), Convert.ToDateTime(row["last_updated"]), Convert.ToUInt32(row["modifiers_id"]));
            }

            return violation;
        }

        /// <summary>
        /// Get the Violation Report for an item based on the Violation Type and the Reporting user
        /// </summary>
        /// <param name="report"></param>
        /// <returns>ViolationReport</returns>
        public ViolationReport GetViolationReportByTypeANDReporter(ViolationReport report)
        {
            ViolationReport violation = new ViolationReport();

            // get all roles in pages datatable
            string strQuery = "SELECT violation_id, asset_id, wok_item_id, owner_id, reporter_id, violation_type_id, violation_action_type_id, " +
            " member_comments, csr_notes, report_date, last_updated, modifiers_id FROM violation_reports WHERE reporter_id = @reporter_id AND " +
            " violation_type_id = @violation_type_id ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@reporter_id", report.ReportingUserId));
            parameters.Add(new MySqlParameter("@violation_type_id", report.ViolationTypeId));

            //determine if checking for a WOK item or user uploaded asset
            //should be one or the other
            if (report.AssetID > 0)
            {
                strQuery += " AND asset_id = @asset_id";
                parameters.Add(new MySqlParameter("@asset_id", report.AssetID));
            }
            else
            {
                strQuery += " AND wok_item_id = @wok_item_id";
                parameters.Add(new MySqlParameter("@wok_item_id", report.WokItemId));
            }

            DataRow row = Db.Master.GetDataRow(strQuery, parameters);

            if (row != null)
            {
                violation = new ViolationReport(Convert.ToUInt32(row["violation_id"]), Convert.ToUInt32(row["asset_id"]), Convert.ToUInt32(row["wok_item_id"]),
                    Convert.ToUInt32(row["owner_id"]), Convert.ToUInt32(row["reporter_id"]), Convert.ToUInt32(row["violation_type_id"]),
                    Convert.ToUInt32(row["violation_action_type_id"]), row["member_comments"].ToString(),
                    row["csr_notes"].ToString(), Convert.ToDateTime(row["report_date"]), Convert.ToDateTime(row["last_updated"]), Convert.ToUInt32(row["modifiers_id"]));
            }

            return violation;
        }

        /// <summary>
        /// Get the Violation Report for an item based on the Violation Type
        /// </summary>
        /// <param name="report"></param>
        /// <returns>ViolationReport</returns>
        public ViolationReport GetViolationReportByViolationType(ViolationReport report)
        {
            ViolationReport violation = new ViolationReport();

            // get all roles in pages datatable
            string strQuery = "SELECT violation_id, asset_id, wok_item_id, owner_id, reporter_id, violation_type_id, violation_action_type_id, " +
            " member_comments, csr_notes, report_date, last_updated, modifiers_id FROM violation_reports WHERE violation_type_id = @violation_type_id ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@violation_type_id", report.ViolationTypeId));

            //determine if checking for a WOK item or user uploaded asset
            //should be one or the other
            if (report.AssetID > 0)
            {
                strQuery += " AND asset_id = @asset_id";
                parameters.Add(new MySqlParameter("@asset_id", report.AssetID));
            }
            else
            {
                strQuery += " AND wok_item_id = @wok_item_id";
                parameters.Add(new MySqlParameter("@wok_item_id", report.WokItemId));
            }

            DataRow row = Db.Master.GetDataRow(strQuery, parameters);

            if (row != null)
            {
                violation = new ViolationReport(Convert.ToUInt32(row["violation_id"]), Convert.ToUInt32(row["asset_id"]), Convert.ToUInt32(row["wok_item_id"]),
                    Convert.ToUInt32(row["owner_id"]), Convert.ToUInt32(row["reporter_id"]), Convert.ToUInt32(row["violation_type_id"]),
                    Convert.ToUInt32(row["violation_action_type_id"]), row["member_comments"].ToString(),
                    row["csr_notes"].ToString(), Convert.ToDateTime(row["report_date"]), Convert.ToDateTime(row["last_updated"]), Convert.ToUInt32(row["modifiers_id"]));
            }

            return violation;
        }


        /// <summary>
        /// Add A New Violation Report
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int AddNewViolationReport(ViolationReport report)
        {
            // Send the message
            string sql = "CALL add_new_violation_report(@asset_id, @wok_item_id, @owner_id, @reporter_id, @violation_type_id, @violation_action_type_id," +
                " @member_comments, @csr_notes, @report_date, @last_updated, @modifiers_id)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@asset_id", report.AssetID));
            parameters.Add(new MySqlParameter("@wok_item_id", report.WokItemId));
            parameters.Add(new MySqlParameter("@owner_id", report.OwnerId));
            parameters.Add(new MySqlParameter("@reporter_id", report.ReportingUserId));
            parameters.Add(new MySqlParameter("@violation_type_id", report.ViolationTypeId));
            parameters.Add(new MySqlParameter("@violation_action_type_id", report.ViolationActionTypeId));
            parameters.Add(new MySqlParameter("@member_comments", report.MemberComments));
            parameters.Add(new MySqlParameter("@csr_notes", report.CSRNotes));
            parameters.Add(new MySqlParameter("@report_date", report.ReportDate));
            parameters.Add(new MySqlParameter("@last_updated", report.LastUpdated));
            parameters.Add(new MySqlParameter("@modifiers_id", report.ModifiersID));

            return Db.Master.ExecuteIdentityInsert(sql, parameters);
        }

        //--manage game functions---
        /// <summary>
        /// Update Role
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int UpdateViolationReport(ViolationReport report)
        {
            // Send the message
            string sql = "call update_violation_report(@violation_id, @asset_id, @wok_item_id, @owner_id, @reporter_id, @violation_type_id, @violation_action_type_id," +
                " @member_comments, @csr_notes, @report_date, @last_updated, @modifiers_id)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@asset_id", report.AssetID));
            parameters.Add(new MySqlParameter("@wok_item_id", report.WokItemId));
            parameters.Add(new MySqlParameter("@owner_id", report.OwnerId));
            parameters.Add(new MySqlParameter("@reporter_id", report.ReportingUserId));
            parameters.Add(new MySqlParameter("@violation_type_id", report.ViolationTypeId));
            parameters.Add(new MySqlParameter("@violation_action_type_id", report.ViolationActionTypeId));
            parameters.Add(new MySqlParameter("@member_comments", report.MemberComments));
            parameters.Add(new MySqlParameter("@csr_notes", report.CSRNotes));
            parameters.Add(new MySqlParameter("@report_date", report.ReportDate));
            parameters.Add(new MySqlParameter("@last_updated", report.LastUpdated));
            parameters.Add(new MySqlParameter("@modifiers_id", report.ModifiersID));
            parameters.Add(new MySqlParameter("@violation_id", report.ViolationID));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        //--manage game functions---
        /// <summary>
        /// ProcessAssetViolation
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="userId"></param>
        /// <returns>int</returns>
        public int ProcessAssetViolation(ViolationReport report)
        {
            string sql = "CALL process_asset_violation(@assetId, @violationActionId, @modifiers_id)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetId", report.AssetID));
            parameters.Add(new MySqlParameter("@violationActionId", report.ViolationActionTypeId));
            parameters.Add(new MySqlParameter("@modifiers_id", report.ModifiersID));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        //--manage game functions---
        /// <summary>
        /// ProcessWokItemViolation
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="userId"></param>
        /// <returns>int</returns>
        public int ProcessWokItemViolation(ViolationReport report)
        {
            string sql = "CALL process_wokItem_violation(@wokItemId, @violationActionId, @modifiers_id)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@wokItemId", report.WokItemId));
            parameters.Add(new MySqlParameter("@violationActionId", report.ViolationActionTypeId));
            parameters.Add(new MySqlParameter("@modifiers_id", report.ModifiersID));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        //--manage game functions---
        /// <summary>
        /// Delete Game
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="userId"></param>
        /// <returns>int</returns>
        public int DeleteViolationReport(ViolationReport report)
        {
            string sql = "CALL delete_violation_report(@violation_id)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@violation_id", report.ViolationID));

            Db.Master.ExecuteNonQuery(sql, parameters);

            return 1;//temporary until able to return value from stored procedure
        }

        //--manage game functions---
        /// <summary>
        /// Delete Game
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="userId"></param>
        /// <returns>int</returns>
        public int DeleteViolationReportByItem(uint assetId, uint itemId)
        {
            string sql = "";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (assetId > 0)
            {
                sql = "CALL delete_violation_report_by_assetid(@assetId)";
                parameters.Add(new MySqlParameter("@assetId", assetId));
           }
            else if (itemId > 0)
            {
                sql = "CALL delete_violation_report_by_itemid(@itemId)";
                parameters.Add(new MySqlParameter("@itemId", itemId));
            }
            else
            {
                return 0;
            }

            Db.Master.ExecuteNonQuery(sql, parameters);

            return 1;//temporary until able to return value from stored procedure
        }

        #endregion

        /// <summary>
        /// Gets all the available violation action types
        /// </summary>
        /// <returns>IList<WebSite></returns>
        public IList<ViolationActionType> GetAllViolationActionTypes()
        {
            //create query string to get game by user id
            string sql = "SELECT violation_action_type_id, violation_action_description FROM violation_action_types";

            DataTable dt = Db.Master.GetDataTable(sql.ToString());

            IList<ViolationActionType> list = new List<ViolationActionType>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new ViolationActionType(Convert.ToUInt32(row["violation_action_type_id"]), row["violation_action_description"].ToString()));
            }

            return list;
        }

        /// <summary>
        /// Gets a list of all possible violation actions
        /// </summary>
        /// <returns>IList<WebSite></returns>
        public IList<ViolationAction> GetAllViolationActions()
        {
            //create query string to get game by user id
            string sql = "SELECT violation_action_id, violation_type_id, violation_action_type_id, violation_trigger_count FROM violation_actions";

            DataTable dt = Db.Master.GetDataTable(sql.ToString());

            IList<ViolationAction> list = new List<ViolationAction>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new ViolationAction(Convert.ToUInt32(row["violation_action_id"]), Convert.ToUInt32(row["violation_type_id"]),
                    Convert.ToUInt32(row["violation_action_type_id"]), Convert.ToUInt32(row["violation_trigger_count"])));
            }

            return list;
        }

        /// <summary>
        /// Gets a list of all violation types
        /// </summary>
        /// <returns>IList<WebSite></returns>
        public IList<ViolationType> GetAllViolationTypes()
        {
            //create query string to get game by user id
            string sql = "SELECT violation_type_id, violation_type_description FROM violation_types";

            DataTable dt = Db.Master.GetDataTable(sql.ToString());

            IList<ViolationType> list = new List<ViolationType>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new ViolationType(Convert.ToUInt32(row["violation_type_id"]), row["violation_type_description"].ToString()));
            }

            return list;
        }

        /// <summary>
        /// Gets violation action based on violation type id
        /// </summary>
        /// <returns>ViolationAction<WebSite></returns>
        public ViolationAction GetViolationAction(int violationTypeId)
        {
            //create query string to get game by user id
            string sql = "SELECT violation_action_id, violation_type_id, violation_action_type_id, violation_trigger_count FROM violation_actions" +
            " WHERE violation_type_id = @violationTypeId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            IList<ViolationAction> list = new List<ViolationAction>();
            parameters.Add(new MySqlParameter("@violationTypeId", violationTypeId));

            //perform query returning single data row from the developer database
            DataRow row = Db.Master.GetDataRow(sql.ToString(), parameters);

            return new ViolationAction(Convert.ToUInt32(row["violation_action_id"]), Convert.ToUInt32(row["violation_type_id"]),
                    Convert.ToUInt32(row["violation_action_type_id"]), Convert.ToUInt32(row["violation_trigger_count"]));

        }

    }
}
