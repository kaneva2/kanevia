///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using Devart.Data.MySql;
using System.Transactions;
using log4net;
using Sphinx.Client.Commands.Search;
using Sphinx.Client.Connections;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLExperimentDao : IExperimentDao
    {
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Create / Update

        /// <summary>
        /// Saves an Experiment object to the database, updating if exists, inserting otherwise.
        /// </summary>
        /// <returns>True if the Experiment was successfully saved.</returns>
        public bool SaveExperiment(Experiment experiment, bool propagateTransactionExceptions = false)
        {
            bool result = false;

            // Generate a UUID for a primary key if not provided. Do so outside the context of a transaction.
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                if (string.IsNullOrWhiteSpace(experiment.ExperimentId))
                {
                    experiment.ExperimentId = (Db.Master.GetScalar("SELECT uuid()") ?? string.Empty).ToString();
                }
            }

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "INSERT INTO experiment.experiments (experiment_id, name, description, status, winning_group_id, creation_date, " +
                                       "start_date, end_date, assign_groups_on, category, creator_id, assignment_target, cleanup_jira) " +
                                       "VALUES (@experimentId, @name, @description, @status, @winningGroupId, NOW(), " +
                                       "@startDate, @endDate, @assignGroupsOn, @category, @creatorId, @assignmentTarget, @cleanupJIRA) " +
                                       "ON DUPLICATE KEY UPDATE name = @name, description = @description, status = @status, winning_group_id = @winningGroupId, " +
                                       "start_date = @startDate, end_date = @endDate, assign_groups_on = @assignGroupsOn, category = @category, assignment_target = @assignmentTarget, " +
                                       "cleanup_jira = @cleanupJIRA ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@experimentId", experiment.ExperimentId));
                    parameters.Add(new MySqlParameter("@name", experiment.Name));
                    parameters.Add(new MySqlParameter("@description", experiment.Description));
                    parameters.Add(new MySqlParameter("@status", experiment.Status));
                    parameters.Add(new MySqlParameter("@winningGroupId", string.IsNullOrWhiteSpace(experiment.WinningGroupId)
                        ? DBNull.Value : (object)experiment.WinningGroupId));
                    parameters.Add(new MySqlParameter("@startDate", experiment.StartDate == null
                        ? DBNull.Value : (object)experiment.StartDate));
                    parameters.Add(new MySqlParameter("@endDate", experiment.EndDate == null
                        ? DBNull.Value : (object)experiment.EndDate));
                    parameters.Add(new MySqlParameter("@assignGroupsOn", experiment.AssignGroupsOn));
                    parameters.Add(new MySqlParameter("@category", string.IsNullOrWhiteSpace(experiment.Category)
                       ? DBNull.Value : (object)experiment.Category));
                    parameters.Add(new MySqlParameter("@creatorId", experiment.CreatorId));
                    parameters.Add(new MySqlParameter("@assignmentTarget", experiment.AssignmentTarget));
                    parameters.Add(new MySqlParameter("@cleanupJIRA", string.IsNullOrWhiteSpace(experiment.CleanupJIRA)
                       ? DBNull.Value : (object)experiment.CleanupJIRA));

                    Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.SaveExperiment() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        /// <summary>
        /// Saves an ExperimentCategory object to the database, updating if exists, inserting otherwise.
        /// </summary>
        /// <returns>True if the ExperimentCategory was successfully saved.</returns>
        public bool SaveExperimentCategory(ExperimentCategory category, bool propagateTransactionExceptions = false)
        {
            bool result = false;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "INSERT INTO experiment.experiment_categories (category, description) " +
                                       "VALUES (@category, @description) " +
                                       "ON DUPLICATE KEY UPDATE description = @description ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@category", category.Name));
                    parameters.Add(new MySqlParameter("@description", category.Description));

                    Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.SaveExperimentCategory() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        /// <summary>
        /// Saves an ExperimentEffect object to the database, updating if exists, inserting otherwise.
        /// </summary>
        /// <returns>True if the ExperimentEffect was successfully saved.</returns>
        public bool SaveExperimentEffect(ExperimentEffect effect, bool propagateTransactionExceptions = false)
        {
            bool result = false;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "INSERT INTO experiment.effects (effect, description) " +
                                       "VALUES (@effect, @description) " +
                                       "ON DUPLICATE KEY UPDATE description = @description ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@effect", effect.Effect));
                    parameters.Add(new MySqlParameter("@description", effect.Description));

                    Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.SaveExperimentEffect() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        /// <summary>
        /// Saves multiple ExperimentGroups in a single transaction.
        /// </summary>
        public bool SaveExperimentGroups(Experiment experiment, bool propagateTransactionExceptions = false)
        {
            bool result = false;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    foreach(ExperimentGroup group in experiment.ExperimentGroups)
                    {
                        SaveExperimentGroup(group, true);
                    }

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.SaveExperimentGroups() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        /// <summary>
        /// Saves an ExperimentGroup object to the database, updating if exists, inserting otherwise.
        /// </summary>
        /// <returns>True if the ExperimentGroup was successfully saved.</returns>
        private bool SaveExperimentGroup(ExperimentGroup group, bool propagateTransactionExceptions = false)
        {
            bool result = false;
            ExperimentGroup origGroup = null;

            // Generate a UUID for a primary key if not provided. Do so outside of transaction context.
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                if (string.IsNullOrWhiteSpace(group.GroupId))
                {
                    group.GroupId = (Db.Master.GetScalar("SELECT uuid()") ?? string.Empty).ToString();
                }
                else
                {
                    origGroup = GetExperimentGroup(group.GroupId);
                }
            }

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "INSERT INTO experiment.experiment_groups (experiment_group_id, experiment_id, name, description, creation_date, label, assignment_percentage) " +
                                        "VALUES (@groupId, @experimentId, @name, @description, NOW(), @label, @assignmentPercentage) " +
                                        "ON DUPLICATE KEY UPDATE name = @name, description = @description, label = @label, assignment_percentage = @assignmentPercentage ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@groupId", group.GroupId));
                    parameters.Add(new MySqlParameter("@experimentId", group.ExperimentId));
                    parameters.Add(new MySqlParameter("@name", group.Name));
                    parameters.Add(new MySqlParameter("@description", string.IsNullOrWhiteSpace(group.Description)
                            ? DBNull.Value : (object)group.Description));
                    parameters.Add(new MySqlParameter("@label", string.IsNullOrWhiteSpace(group.Label)
                            ? DBNull.Value : (object)group.Label));
                    parameters.Add(new MySqlParameter("@assignmentPercentage", group.AssignmentPercentage));

                    Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // Delete experiment group effects, if necessary
                    if (origGroup != null && origGroup.Effects.Count > 0)
                    {
                        HashSet<string> effectsToRemove;

                        if (group.Effects.Count > 0)
                        {
                            IEnumerable<string> currentGroupIds = group.Effects.Select(g => g.Effect);
                            effectsToRemove = new HashSet<string>(origGroup.Effects.Select(g => g.Effect).Except(currentGroupIds));
                        }
                        else
                        {
                            effectsToRemove = new HashSet<string>(origGroup.Effects.Select(g => g.Effect));
                        }

                        foreach (ExperimentEffect effect in origGroup.Effects)
                        {
                            if (effectsToRemove.Contains(effect.Effect))
                            {
                                DeleteExperimentGroupEffect(origGroup, effect, true);
                            }
                        }
                    }

                    // Add experiment groups if necessary
                    foreach (ExperimentEffect effect in group.Effects)
                    {
                        SaveExperimentGroupEffect(group, effect, true);
                    }

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.SaveExperimentGroup() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        /// <summary>
        /// Saves an ExperimentEffect for a specific group object to the database, updating if exists, inserting otherwise.
        /// </summary>
        /// <returns>True if the ExperimentEffect was successfully saved.</returns>
        public bool SaveExperimentGroupEffect(ExperimentGroup group, ExperimentEffect effect, bool propagateTransactionExceptions = false)
        {
            bool result = false;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "INSERT IGNORE INTO experiment.experiment_group_effects (experiment_group_id, effect) " +
                                       "VALUES (@experimentGroupId, @effect) ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@experimentGroupId", group.GroupId));
                    parameters.Add(new MySqlParameter("@effect", effect.Effect));

                    Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.SaveExperimentGroupEffect() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        /// <summary>
        /// Saves a UserExperimentGroup object to the database, updating if exists, inserting otherwise.
        /// </summary>
        /// <returns>True if the UserExperimentGroup was successfully saved.</returns>
        public bool SaveUserExperimentGroup(UserExperimentGroup participant, bool propagateTransactionExceptions = false)
        {
            bool result = false;

            // Generate a UUID for a primary key if not provided. Do so outside the context of a transaction.
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                if (string.IsNullOrWhiteSpace(participant.UserGroupId))
                {
                    participant.UserGroupId = (Db.Master.GetScalar("SELECT uuid()") ?? string.Empty).ToString();
                }
            }

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "INSERT INTO experiment.user_groups (user_group_id, user_id, experiment_group_id, assignment_date, conversion_date, assigned_via_url) " +
                                       "VALUES (@userGroupId, @userId, @experimentGroupId, NOW(), @conversionDate, @assignedViaUrl) " +
                                       "ON DUPLICATE KEY UPDATE experiment_group_id = @experimentGroupId, conversion_date = @conversionDate, assigned_via_url = @assignedViaUrl ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@userGroupId", participant.UserGroupId));
                    parameters.Add(new MySqlParameter("@userId", participant.UserId));
                    parameters.Add(new MySqlParameter("@experimentGroupId", participant.GroupId));
                    parameters.Add(new MySqlParameter("@conversionDate", participant.ConversionDate == null
                        ? DBNull.Value : (object)participant.ConversionDate));
                    parameters.Add(new MySqlParameter("@assignedViaUrl", participant.AssignedViaUrl ? "T" : "F"));

                    Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.SaveUserExperimentGroup() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        /// <summary>
        /// Saves a WorldExperimentGroup object to the database, updating if exists, inserting otherwise.
        /// </summary>
        /// <returns>True if the WorldExperimentGroup was successfully saved.</returns>
        public bool SaveWorldExperimentGroup(WorldExperimentGroup participant, bool propagateTransactionExceptions = false)
        {
            bool result = false;

            // Generate a UUID for a primary key if not provided. Do so outside the context of a transaction.
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                if (string.IsNullOrWhiteSpace(participant.WorldGroupId))
                {
                    participant.WorldGroupId = (Db.Master.GetScalar("SELECT uuid()") ?? string.Empty).ToString();
                }
            }

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "INSERT INTO experiment.world_groups (world_group_id, zone_instance_id, zone_type, experiment_group_id, assignment_date, conversion_date) " +
                                       "VALUES (@worldGroupId, @zoneInstanceId, @zoneType, @experimentGroupId, NOW(), @conversionDate) " +
                                       "ON DUPLICATE KEY UPDATE conversion_date = @conversionDate ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@worldGroupId", participant.WorldGroupId));
                    parameters.Add(new MySqlParameter("@zoneInstanceId", participant.ZoneInstanceId));
                    parameters.Add(new MySqlParameter("@zoneType", participant.ZoneType));
                    parameters.Add(new MySqlParameter("@experimentGroupId", participant.GroupId));
                    parameters.Add(new MySqlParameter("@conversionDate", participant.ConversionDate == null
                        ? DBNull.Value : (object)participant.ConversionDate));

                    Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.SaveWorldExperimentGroup() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        /// <summary>
        /// Saves a WorldTemplateExperimentGroup object to the database, updating if exists, inserting otherwise.
        /// </summary>
        /// <returns>True if the WorldTemplateExperimentGroup was successfully saved.</returns>
        public bool SaveWorldTemplateExperimentGroup(WorldTemplateExperimentGroup participant, bool propagateTransactionExceptions = false)
        {
            bool result = false;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "INSERT IGNORE INTO experiment.world_template_experiment_groups (template_id, experiment_group_id) " +
                                       "VALUES (@templateId, @experimentGroupId) ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@templateId", participant.TemplateId));
                    parameters.Add(new MySqlParameter("@experimentGroupId", participant.GroupId));

                    Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.SaveWorldTemplateExperimentGroup() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes an Experiment object from the database.
        /// </summary>
        /// <remarks>
        /// Deletion assumes proper referential integrity constraints will cascade the change appropriately.
        /// If this is NOT the case, this method must be adjusted to make the associated changes manually.
        /// </remarks>
        /// <returns>True if the Experiment was successfully deleted.</returns>
        public bool DeleteExperiment(Experiment experiment, bool propagateTransactionExceptions = false)
        {
            bool result = false;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "DELETE FROM experiment.experiments WHERE experiment_id = @experimentId ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@experimentId", experiment.ExperimentId));

                    Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.DeleteExperiment() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        /// <summary>
        /// Deletes an ExperimentCategory object from the database.
        /// </summary>
        /// <remarks>
        /// Deletion assumes proper referential integrity constraints will cascade the change appropriately.
        /// If this is NOT the case, this method must be adjusted to make the associated changes manually.
        /// </remarks>
        /// <returns>True if the ExperimentCategory was successfully deleted.</returns>
        public bool DeleteExperimentCategory(ExperimentCategory category, bool propagateTransactionExceptions = false)
        {
            bool result = false;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "DELETE FROM experiment.experiment_categories WHERE category = @category ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@category", category.Name));

                    Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.DeleteExperimentCategory() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        /// <summary>
        /// Deletes an ExperimentEffect object from the database.
        /// </summary>
        /// <remarks>
        /// Deletion assumes proper referential integrity constraints will cascade the change appropriately.
        /// If this is NOT the case, this method must be adjusted to make the associated changes manually.
        /// </remarks>
        /// <returns>True if the ExperimentEffect was successfully deleted.</returns>
        public bool DeleteExperimentEffect(ExperimentEffect effect, bool propagateTransactionExceptions = false)
        {
            bool result = false;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "DELETE FROM experiment.effects WHERE effect = @effect ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@effect", effect.Effect));

                    Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.DeleteExperimentEffect() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        /// <summary>
        /// Deletes an ExperimentGroup object from the database.
        /// </summary>
        /// <remarks>
        /// Deletion assumes proper referential integrity constraints will cascade the change appropriately.
        /// If this is NOT the case, this method must be adjusted to make the associated changes manually.
        /// </remarks>
        /// <returns>True if the ExperimentGroup was successfully deleted.</returns>
        public bool DeleteExperimentGroup(ExperimentGroup group, bool propagateTransactionExceptions = false)
        {
            bool result = false;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "DELETE FROM experiment.experiment_groups WHERE experiment_group_id = @groupId ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@groupId", group.GroupId));

                    Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.DeleteExperimentGroup() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        /// <summary>
        /// Deletes an ExperimentEffect object from the database for a specific group.
        /// </summary>
        /// <remarks>
        /// Deletion assumes proper referential integrity constraints will cascade the change appropriately.
        /// If this is NOT the case, this method must be adjusted to make the associated changes manually.
        /// </remarks>
        /// <returns>True if the ExperimentEffect was successfully deleted.</returns>
        public bool DeleteExperimentGroupEffect(ExperimentGroup group, ExperimentEffect effect, bool propagateTransactionExceptions = false)
        {
            bool result = false;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "DELETE FROM experiment.experiment_group_effects WHERE experiment_group_id = @experimentGroupId AND effect = @effect ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@experimentGroupId", group.GroupId));
                    parameters.Add(new MySqlParameter("@effect", effect.Effect));

                    Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.DeleteExperimentGroupEffect() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        /// <summary>
        /// Deletes an ExperimentGroup object from the database.
        /// </summary>
        /// <remarks>
        /// Deletion assumes proper referential integrity constraints will cascade the change appropriately.
        /// If this is NOT the case, this method must be adjusted to make the associated changes manually.
        /// </remarks>
        /// <returns>True if the ExperimentGroup was successfully deleted.</returns>
        public bool DeleteUserExperimentGroup(UserExperimentGroup participant, bool propagateTransactionExceptions = false)
        {
            bool result = false;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "DELETE FROM experiment.user_groups WHERE user_group_id = @userGroupId ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@userGroupId", participant.UserGroupId));

                    Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.DeleteUserExperimentGroup() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        /// <summary>
        /// Deletes an WorldExperimentGroup object from the database.
        /// </summary>
        /// <remarks>
        /// Deletion assumes proper referential integrity constraints will cascade the change appropriately.
        /// If this is NOT the case, this method must be adjusted to make the associated changes manually.
        /// </remarks>
        /// <returns>True if the WorldExperimentGroup was successfully deleted.</returns>
        public bool DeleteWorldExperimentGroup(WorldExperimentGroup participant, bool propagateTransactionExceptions = false)
        {
            bool result = false;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "DELETE FROM experiment.world_groups " +
                                       "WHERE world_group_id = @worldGroupId ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@worldGroupId", participant.WorldGroupId));

                    Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.DeleteWorldExperimentGroup() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        /// <summary>
        /// Deletes an WorldTemplateExperimentGroup object from the database.
        /// </summary>
        /// <remarks>
        /// Deletion assumes proper referential integrity constraints will cascade the change appropriately.
        /// If this is NOT the case, this method must be adjusted to make the associated changes manually.
        /// </remarks>
        /// <returns>True if the WorldTemplateExperimentGroup was successfully deleted.</returns>
        public bool DeleteWorldTemplateExperimentGroup(WorldTemplateExperimentGroup participant, bool propagateTransactionExceptions = false)
        {
            bool result = false;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "DELETE FROM experiment.world_template_experiment_groups " +
                                       "WHERE template_id = @templateId AND experiment_group_id = @experimentGroupId ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@templateId", participant.TemplateId));
                    parameters.Add(new MySqlParameter("@experimentGroupId", participant.GroupId));

                    Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.DeleteWorldTemplateExperimentGroup() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        /// <summary>
        /// Deletes all WorldTemplateExperimentGroup objects for a given template from the database.
        /// </summary>
        /// <remarks>
        /// Deletion assumes proper referential integrity constraints will cascade the change appropriately.
        /// If this is NOT the case, this method must be adjusted to make the associated changes manually.
        /// </remarks>
        /// <returns>True if the WorldTemplateExperimentGroup was successfully deleted.</returns>
        public bool ClearWorldTemplateExperimentGroups(WorldTemplate template, bool propagateTransactionExceptions = false)
        {
            bool result = false;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "DELETE FROM experiment.world_template_experiment_groups " +
                                       "WHERE template_id = @templateId ";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@templateId", template.TemplateId));

                    Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // If we reach this point, the transaction succeeded.
                    result = true;
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLExperimentDao.ClearWorldTemplateExperimentGroups() - Transaction aborted: " + e.ToString());

                if (propagateTransactionExceptions)
                {
                    throw e;
                }
            }

            return result;
        }

        #endregion

        #region Read

        /// <summary>
        /// Helper function used to pull lists of Experiments.
        /// </summary>
        /// <returns>PagedList of Experiment objects.</returns>
        private PagedList<Experiment> GetExperiments(List<string> joins, List<string> whereClauses, List<IDbDataParameter> parameters,
            string orderBy, int pageNumber, int pageSize, bool includeExperimentGroups = true)
        {
            PagedList<Experiment> experiments = new PagedList<Experiment>();
            Dictionary<string, Experiment> experimentLookup = null;

            // Grab Experiments.
            string sql = "SELECT DISTINCT e.experiment_id, " + 
                         " e.name, " + 
                         " e.description, " + 
                         " e.status, " + 
                         " e.winning_group_id, " + 
                         " e.creation_date, " + 
                         " e.start_date, " + 
                         " e.end_date, " + 
                         " e.assign_groups_on, " +
                         " e.category, " +
                         " e.creator_id, " +
                         " e.assignment_target, " +
                         " e.cleanup_jira " +
                         " FROM experiment.experiments e " +
                         string.Join(" ", joins) + " " +
                         "WHERE 1=1 " +
                         string.Join(" ", whereClauses);

            PagedDataTable pdtExperiments = Db.Master.GetPagedDataTable(sql, orderBy, parameters, pageNumber, pageSize);
            if (pdtExperiments == null)
            {
                return experiments;
            }

            foreach (DataRow row in pdtExperiments.Rows)
            {
                Experiment exp = new Experiment(row["experiment_id"].ToString(), row["name"].ToString(), (row["description"] == DBNull.Value ? null : row["description"].ToString()),
                    row["status"].ToString(), (row["winning_group_id"] == DBNull.Value ? null : row["winning_group_id"].ToString()), Convert.ToDateTime(row["creation_date"]),
                    (row["start_date"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(row["start_date"])), 
                    (row["end_date"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(row["end_date"])),
                    row["assign_groups_on"].ToString(), (row["category"] == DBNull.Value ? null : row["category"].ToString()), Convert.ToInt32(row["creator_id"]),
                    row["assignment_target"].ToString(), row["cleanup_jira"].ToString());

                experiments.Add(exp);
            }

            // Pull associated experiment groups if necessary.
            if (includeExperimentGroups && experiments.Count > 0)
            {
                sql = "SELECT eg.experiment_group_id, " +
                      " eg.experiment_id, " +
                      " eg.name, " +
                      " eg.description, " +
                      " eg.creation_date, " +
                      " eg.label, " +
                      " eg.assignment_percentage, " +
                      " ef.effect, " +
                      " ef.description AS effect_description " +
                      "FROM experiment.experiment_groups eg " +
                      "LEFT OUTER JOIN experiment.experiment_group_effects egf ON eg.experiment_group_id = egf.experiment_group_id " +
                      "LEFT OUTER JOIN experiment.effects ef ON egf.effect = ef.effect " +
                      "WHERE eg.experiment_id IN ('" + string.Join("', '", experiments.Select(e => e.ExperimentId)) + "') ";

                DataTable dtGroups = Db.Master.GetDataTable(sql);
                if (dtGroups != null && dtGroups.Rows.Count > 0)
                {
                    // Make sure lookups are initialized.
                    experimentLookup = (experimentLookup ?? experiments.ToDictionary(e => e.ExperimentId));
                    Dictionary<string, ExperimentGroup> groupLookup = new Dictionary<string, ExperimentGroup>();

                    foreach (DataRow row in dtGroups.Rows)
                    {
                        Experiment experiment = experimentLookup[row["experiment_id"].ToString()];
                        ExperimentGroup group;

                        if (!groupLookup.TryGetValue(row["experiment_group_id"].ToString(), out group))
                        {
                            group = new ExperimentGroup(row["experiment_group_id"].ToString(),
                                row["experiment_id"].ToString(), row["name"].ToString(), (row["description"] == DBNull.Value ? null : row["description"].ToString()),
                                Convert.ToDateTime(row["creation_date"]), (row["label"] == DBNull.Value ? null : row["label"].ToString()), Convert.ToInt32(row["assignment_percentage"]),
                                experiment);

                            groupLookup[group.GroupId] = group;
                            experiment.ExperimentGroups.Add(group);
                        }

                        // Add group effects if necessary.
                        if (row["effect"] != DBNull.Value)
                        {
                            group.Effects.Add(new ExperimentEffect(row["effect"].ToString(), row["description"].ToString()));
                        }
                    }
                }
            }

            // Make sure total count makes sense.
            experiments.TotalCount = pdtExperiments.TotalCount;

            return experiments;
        }

        /// <summary>
        /// Pulls the data for an experiment by id.
        /// </summary>
        public Experiment GetExperiment(string experimentId, bool includeExperimentGroups = true)
        {
            List<string> whereClauses = new List<string> 
            { 
                "AND e.experiment_id = @experimentId"
            };

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@experimentId", experimentId));

            PagedList<Experiment> experiments = GetExperiments(new List<string>(), whereClauses, parameters, string.Empty, 1, 1, includeExperimentGroups);

            if (experiments.Count == 1)
            {
                return experiments[0];
            }

            return null;
        }

        /// <summary>
        /// Searches experiments by name.
        /// </summary>
        /// <returns>PagedList of Experiment objects.</returns>
        public PagedList<Experiment> SearchExperiments(string name, bool exactMatch, string orderBy, int pageNumber, int pageSize, bool includeExperimentGroups = true)
        {
            List<string> whereClauses = new List<string>();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (!string.IsNullOrWhiteSpace(name))
            {
                string nameOperator = "=";
                if (!exactMatch)
                {
                    name = "%" + name + "%";
                    nameOperator = "LIKE";
                }

                whereClauses.Add("AND e.name " + nameOperator + " @name");
                parameters.Add(new MySqlParameter("@name", name));
            }

            return GetExperiments(new List<string>(), whereClauses, parameters, orderBy, pageNumber, pageSize, includeExperimentGroups);
        }

        /// <summary>
        /// Searches experiments by category and name.
        /// </summary>
        /// <returns>PagedList of Experiment objects.</returns>
        public PagedList<Experiment> SearchExperiments(string name, bool exactMatch, string category, string status, string orderBy, int pageNumber, int pageSize, bool includeExperimentGroups = true)
        {
            List<string> whereClauses = new List<string>();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (!string.IsNullOrWhiteSpace(name))
            {
                string nameOperator = "=";
                if (!exactMatch)
                {
                    name = "%" + name + "%";
                    nameOperator = "LIKE";
                }

                whereClauses.Add("AND e.name " + nameOperator + " @name");
                parameters.Add(new MySqlParameter("@name", name));
            }

            if (!string.IsNullOrWhiteSpace(category))
            {
                whereClauses.Add("AND e.category = @category");
                parameters.Add(new MySqlParameter("@category", category));
            }
            else if (category != null && category.Trim() == string.Empty)
            {
                whereClauses.Add("AND e.category IS NULL");
            }

            if (!string.IsNullOrWhiteSpace(status))
            {
                whereClauses.Add(" AND e.status = @status ");
                parameters.Add(new MySqlParameter("@status", status));
            }

            return GetExperiments(new List<string>(), whereClauses, parameters, orderBy, pageNumber, pageSize, includeExperimentGroups);
        }

        /// <summary>
        /// Pulls a list of Experiments that are available for the specified user to join.
        /// </summary>
        /// <returns>PagedList of Experiment objects.</returns>
        public IList<Experiment> GetAvailableExperimentsByUserContext(int userId, string context = "Request", bool includeExperimentGroups = true)
        {
            List<string> whereClauses = new List<string> 
            { 
                "AND e.status IN ('" + EXPERIMENT_STATUS_TYPE.STARTED + "', '" + EXPERIMENT_STATUS_TYPE.ENDED + "')",
                "AND e.assign_groups_on = @context ",
                "AND e.assignment_target = 'Users' ",
                "AND e.experiment_id NOT IN ( SELECT experiment_id FROM (" +
                "                SELECT  e.experiment_id " +
                "                FROM    experiment.user_groups ep, " +
                "                        experiment.experiment_groups eg, " +
                "                        experiment.experiments e " +
                "                WHERE   ep.experiment_group_id = eg.experiment_group_id " +
                "                AND     e.experiment_id = eg.experiment_id " +
                "                AND     ep.user_id = @userId ) AS X ) "
            };

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@context", context));

            return GetExperiments(new List<string>(), whereClauses, parameters, string.Empty, 1, Int32.MaxValue, includeExperimentGroups);
        }

        /// <summary>
        /// Helper function used to pull lists of ExperimentCategories.
        /// </summary>
        /// <returns>PagedList of ExperimentCategories objects.</returns>
        private PagedList<ExperimentCategory> GetExperimentCategories(List<string> joins, List<string> whereClauses, List<IDbDataParameter> parameters,
            string orderBy, int pageNumber, int pageSize)
        {
            PagedList<ExperimentCategory> categories = new PagedList<ExperimentCategory>();

            // Grab Experiments.
            string sql = "SELECT c.category, c.description " +
                         "FROM experiment.experiment_categories c " +
                         string.Join(" ", joins) + " " +
                         "WHERE 1=1 " +
                         string.Join(" ", whereClauses);

            PagedDataTable pdtCategories = Db.Master.GetPagedDataTable(sql, orderBy, parameters, pageNumber, pageSize);
            if (pdtCategories == null)
            {
                return categories;
            }

            foreach (DataRow row in pdtCategories.Rows)
            {
                ExperimentCategory category = new ExperimentCategory(row["category"].ToString(), row["description"].ToString());
                categories.Add(category);
            }

            // Make sure total count makes sense.
            categories.TotalCount = pdtCategories.TotalCount;

            return categories;
        }

        /// <summary>
        /// Returns an ExperimentCategory object.
        /// </summary>
        /// <returns>Specified ExperimentCategory or null if not found.</returns>
        public ExperimentCategory GetExperimentCategory(string name)
        {
            List<string> whereClauses = new List<string> 
            { 
                "AND c.category = @name"
            };

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@name", name));

            PagedList<ExperimentCategory> categories = GetExperimentCategories(new List<string>(), whereClauses, parameters, string.Empty, 1, 1);

            if (categories.Count == 1)
            {
                return categories[0];
            }

            return null;
        }

        /// <summary>
        /// Returns a list of ExperimentCategory objects.
        /// </summary>
        /// <returns>PagedList of ExperimentCategory objects.</returns>
        public PagedList<ExperimentCategory> SearchExperimentCategories(string name, string sortBy, int pageNumber, int pageSize)
        {
            List<string> whereClauses = new List<string>();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (!string.IsNullOrWhiteSpace(name))
            {
                name = "%" + name + "%";
                whereClauses.Add("AND c.category LIKE @name");
                parameters.Add(new MySqlParameter("@name", name));
            }

            return GetExperimentCategories(new List<string>(), whereClauses, parameters, sortBy, pageNumber, pageSize);
        } 

        /// <summary>
        /// Returns a list of ExperimentEffects.
        /// </summary>
        /// <returns>PagedList of ExperimentEffect objects.</returns>
        public PagedList<ExperimentEffect> GetExperimentEffects(string sortBy, int pageNumber, int pageSize)
        {
            string strQuery = "SELECT effect, description " +
                              "FROM experiment.effects ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            PagedDataTable dt = Db.Master.GetPagedDataTable(strQuery, sortBy, parameters, pageNumber, pageSize);

            PagedList<ExperimentEffect> list = new PagedList<ExperimentEffect>();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new ExperimentEffect(row["effect"].ToString(), row["description"].ToString()));
            }

            return list;
        } 

        /// <summary>
        /// Gets an ExperimentGroup by id.
        /// </summary>
        public ExperimentGroup GetExperimentGroup(string experimentGroupId)
        {
            List<string> joinClauses = new List<string>
            {
                "INNER JOIN experiment.experiment_groups eg ON e.experiment_id = eg.experiment_id AND eg.experiment_group_id = @experimentGroupId"
            };

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@experimentGroupId", experimentGroupId));

            PagedList<Experiment> experiments = GetExperiments(joinClauses, new List<string>(), parameters, string.Empty, 1, 1);

            if (experiments.Count == 1)
            {
                return experiments[0].ExperimentGroups.SingleOrDefault(eg => eg.GroupId == experimentGroupId);
            }          

            return null;
        }

        /// <summary>
        /// Helper function used to pull lists of UserExperimentGroups.
        /// </summary>
        /// <returns>PagedList of UserExperimentGroup objects.</returns>
        private PagedList<UserExperimentGroup> GetUserExperimentGroups(List<string> joins, List<string> whereClauses, List<IDbDataParameter> parameters,
            string orderBy, int pageNumber, int pageSize, bool includeExperimentGroups = true)
        {
            PagedList<UserExperimentGroup> ueGroups = new PagedList<UserExperimentGroup>();
            HashSet<string> experimentIds = new HashSet<string>();

            // Grab user experiment groups.
            string strQuery = "SELECT ep.user_group_id, " +
                              " ep.user_id AS user_id, " +
                              " e.experiment_id AS experiment_id, " +
                              " IF(e.status='E', IFNULL(e.winning_group_id, ep.experiment_group_id), ep.experiment_group_id) AS group_id, " +
                              " ep.assignment_date AS assignment_date, " +
                              " ep.conversion_date AS conversion_date, " +
                              " ep.assigned_via_url, " +
                              " u.username " +
                              "FROM experiment.experiments e " +
                              "INNER JOIN experiment.experiment_groups eg ON eg.experiment_id = e.experiment_id " +
                              "INNER JOIN experiment.user_groups ep ON ep.experiment_group_id = eg.experiment_group_id " +
                              "INNER JOIN kaneva.users u ON ep.user_id = u.user_id " +
                              string.Join(" ", joins) + " " +
                              "WHERE 1 = 1 " +
                              string.Join(" ", whereClauses);

            PagedDataTable pdt = Db.Master.GetPagedDataTable(strQuery, orderBy, parameters, pageNumber, pageSize);
            if (pdt == null)
            {
                return ueGroups;
            }
 
            foreach (DataRow row in pdt.Rows)
            {
                ueGroups.Add(new UserExperimentGroup(row["user_group_id"].ToString(), Convert.ToInt32(row["user_id"]), row["group_id"].ToString(), 
                    Convert.ToDateTime(row["assignment_date"]), (row["conversion_date"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(row["conversion_date"])),
                    (row["assigned_via_url"].ToString() == "T" ? true : false), row["username"].ToString()));

                if (!experimentIds.Contains(row["experiment_id"].ToString()))
                {
                    experimentIds.Add(row["experiment_id"].ToString());
                }
            }

            // Grab ExperimentGroups and Experiments, if necessary.
            if (includeExperimentGroups && ueGroups.Count > 0)
            {
                IList<Experiment> experiments = GetExperiments(new List<string>(), new List<string> { "AND e.experiment_id IN ('" + string.Join("', '", experimentIds) + "')" }, 
                    new List<IDbDataParameter>(), string.Empty, 1, Int32.MaxValue);

                Dictionary<string, ExperimentGroup> groupLookup = experiments.SelectMany(e => e.ExperimentGroups).ToDictionary(g => g.GroupId);

                foreach (UserExperimentGroup ueGroup in ueGroups)
                {
                    ExperimentGroup group;
                    if (groupLookup.TryGetValue(ueGroup.GroupId, out group))
                    {
                        ueGroup.ExperimentGroup = group;
                    }
                }
            }

            ueGroups.TotalCount = pdt.TotalCount;

            return ueGroups;
        }

        /// <summary>
        /// Returns  UserExperimentGroup by username.
        /// </summary>
        public PagedList<UserExperimentGroup> SearchUserExperimentGroups(string username, bool bExactMatch, string experimentId, string experimentStatus, string orderBy, int pageNum, int pageSize, bool includeExperimentGroups = true)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                return new PagedList<UserExperimentGroup>();
            }

            // Default to a LIKE search
            string strComparison = "LIKE";
            string strUsernameParam = username + "%";

            if (bExactMatch)
            {
                strComparison = "=";
                strUsernameParam = username;
            }

            List<string> whereClauses = new List<string> 
            {
                "AND u.username " + strComparison + " @username"
            };

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@username", strUsernameParam));

            if (!string.IsNullOrWhiteSpace(experimentId))
            {
                whereClauses.Add("AND e.experiment_id = @experimentId");
                parameters.Add(new MySqlParameter("@experimentId", experimentId));
            }

            if (!string.IsNullOrWhiteSpace(experimentStatus))
            {
                whereClauses.Add("AND e.status = @experimentStatus");
                parameters.Add(new MySqlParameter("@experimentStatus", experimentStatus));
            }

            return GetUserExperimentGroups(new List<string>(), whereClauses, parameters, orderBy, pageNum, pageSize, includeExperimentGroups);
        }

        /// <summary>
        /// Returns a list of active UserExperimentGroups for a given user.
        /// </summary>
        /// <returns>IList of UserExperimentGroup objects.</returns>
        public IList<UserExperimentGroup> GetActiveUserExperimentGroups(int userId, bool includeExperimentGroups = true)
        {
            List<string> whereClauses = new List<string> 
            {
                "AND e.status IN ('" + EXPERIMENT_STATUS_TYPE.STARTED + "', '" + EXPERIMENT_STATUS_TYPE.ENDED + "') ",
                "AND ep.user_id = @userId"
            };

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            return GetUserExperimentGroups(new List<string>(), whereClauses, parameters, string.Empty, 1, Int32.MaxValue, includeExperimentGroups);
        }

        /// <summary>
        /// Returns a UserExperimentGroup.
        /// </summary>
        public UserExperimentGroup GetUserExperimentGroup(string userGroupId, bool includeExperimentGroups = true)
        {
            List<string> whereClauses = new List<string> 
            {
                "AND ep.user_group_id = @userGroupId",
            };

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userGroupId", userGroupId));

            PagedList<UserExperimentGroup> ueGroups = GetUserExperimentGroups(new List<string>(), whereClauses, parameters, string.Empty, 1, 1, includeExperimentGroups);

            if (ueGroups.Count == 1)
            {
                return ueGroups[0];
            }

            return null;
        }

        /// <summary>
        /// Returns a UserExperimentGroup.
        /// </summary>
        public UserExperimentGroup GetUserExperimentGroupByExperimentId(int userId, string experimentId, bool includeExperimentGroups = true)
        {
            List<string> whereClauses = new List<string> 
            {
                "AND ep.user_id = @userId",
                "AND e.experiment_id = @experimentId"
            };

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@experimentId", experimentId));

            PagedList<UserExperimentGroup> ueGroups = GetUserExperimentGroups(new List<string>(), whereClauses, parameters, string.Empty, 1, 1, includeExperimentGroups);

            if (ueGroups.Count == 1)
            {
                return ueGroups[0];
            }

            return null;
        }

        /// <summary>
        /// Helper function used to pull lists of WorldExperimentGroups.
        /// </summary>
        /// <returns>PagedList of WorldExperimentGroup objects.</returns>
        private PagedList<WorldExperimentGroup> GetWorldExperimentGroups(List<string> joins, List<string> whereClauses, List<IDbDataParameter> parameters,
            string orderBy, int pageNumber, int pageSize, bool includeExperimentGroups = true)
        {
            PagedList<WorldExperimentGroup> weGroups = new PagedList<WorldExperimentGroup>();
            HashSet<string> experimentIds = new HashSet<string>();

            // Grab user experiment groups.
            string strQuery = "SELECT ep.world_group_id, " +
                              " ep.zone_instance_id, " +
                              " ep.zone_type, " +
                              " e.experiment_id AS experiment_id, " +
                              " IF(e.status='E', IFNULL(e.winning_group_id, ep.experiment_group_id), ep.experiment_group_id) AS group_id, " +
                              " ep.assignment_date AS assignment_date, " +
                              " ep.conversion_date AS conversion_date " +
                              "FROM experiment.experiments e " +
                              "INNER JOIN experiment.experiment_groups eg ON eg.experiment_id = e.experiment_id " +
                              "INNER JOIN experiment.world_groups ep ON ep.experiment_group_id = eg.experiment_group_id " +
                              string.Join(" ", joins) + " " +
                              "WHERE 1 = 1 " +
                              string.Join(" ", whereClauses);

            PagedDataTable pdt = Db.Master.GetPagedDataTable(strQuery, orderBy, parameters, pageNumber, pageSize);
            if (pdt == null)
            {
                return weGroups;
            }

            foreach (DataRow row in pdt.Rows)
            {
                weGroups.Add(new WorldExperimentGroup(row["world_group_id"].ToString(), Convert.ToInt32(row["zone_instance_id"]), Convert.ToInt32(row["zone_type"]), row["group_id"].ToString(),
                    Convert.ToDateTime(row["assignment_date"]), (row["conversion_date"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(row["conversion_date"]))));

                if (!experimentIds.Contains(row["experiment_id"].ToString()))
                {
                    experimentIds.Add(row["experiment_id"].ToString());
                }
            }

            // Grab ExperimentGroups and Experiments, if necessary.
            if (includeExperimentGroups && weGroups.Count > 0)
            {
                IList<Experiment> experiments = GetExperiments(new List<string>(), new List<string> { "AND e.experiment_id IN ('" + string.Join("', '", experimentIds) + "')" },
                    new List<IDbDataParameter>(), string.Empty, 1, Int32.MaxValue);

                Dictionary<string, ExperimentGroup> groupLookup = experiments.SelectMany(e => e.ExperimentGroups).ToDictionary(g => g.GroupId);

                foreach (WorldExperimentGroup weGroup in weGroups)
                {
                    ExperimentGroup group;
                    if (groupLookup.TryGetValue(weGroup.GroupId, out group))
                    {
                        weGroup.ExperimentGroup = group;
                    }
                }
            }

            weGroups.TotalCount = pdt.TotalCount;

            return weGroups;
        }

        /// <summary>
        /// Returns a WorldExperimentGroup.
        /// </summary>
        public WorldExperimentGroup GetWorldExperimentGroup(int zoneInstanceId, int zoneType, string groupId, bool includeExperimentGroups = true)
        {
            List<string> whereClauses = new List<string> 
            {
                "AND ep.zone_instance_id = @zoneInstanceId",
                "AND ep.zone_type = @zoneType",
                "AND ep.experiment_group_id = @groupId"
            };

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));
            parameters.Add(new MySqlParameter("@groupId", groupId));

            PagedList<WorldExperimentGroup> ueGroups = GetWorldExperimentGroups(new List<string>(), whereClauses, parameters, string.Empty, 1, 1, includeExperimentGroups);

            if (ueGroups.Count == 1)
            {
                return ueGroups[0];
            }

            return null;
        }

        /// <summary>
        /// Returns a WorldExperimentGroup.
        /// </summary>
        public WorldExperimentGroup GetWorldExperimentGroupByExperimentId(int zoneInstanceId, int zoneType, string experimentId, bool includeExperimentGroups = true)
        {
            List<string> whereClauses = new List<string> 
            {
                "AND ep.zone_instance_id = @zoneInstanceId",
                "AND ep.zone_type = @zoneType",
                "AND e.experiment_id = @experimentId"
            };

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));
            parameters.Add(new MySqlParameter("@experimentId", experimentId));

            PagedList<WorldExperimentGroup> ueGroups = GetWorldExperimentGroups(new List<string>(), whereClauses, parameters, string.Empty, 1, 1, includeExperimentGroups);

            if (ueGroups.Count == 1)
            {
                return ueGroups[0];
            }

            return null;
        }

        /// <summary>
        /// Pulls a list of Experiments that are available for the specified world to join.
        /// </summary>
        /// <returns>PagedList of Experiment objects.</returns>
        public IList<Experiment> GetAvailableExperimentsByWorldContext(int zoneInstanceId, int zoneType, string context = "Request", bool includeExperimentGroups = true)
        {
            List<string> whereClauses = new List<string> 
            { 
                "AND e.status IN ('" + EXPERIMENT_STATUS_TYPE.STARTED + "', '" + EXPERIMENT_STATUS_TYPE.ENDED + "')",
                "AND e.assign_groups_on = @context ",
                "AND e.assignment_target = 'Worlds' ",
                "AND e.experiment_id NOT IN ( SELECT experiment_id FROM (" +
                "                SELECT  e.experiment_id " +
                "                FROM    experiment.world_groups ep, " +
                "                        experiment.experiment_groups eg, " +
                "                        experiment.experiments e " +
                "                WHERE   ep.experiment_group_id = eg.experiment_group_id " +
                "                AND     e.experiment_id = eg.experiment_id " +
                "                AND     ep.zone_instance_id = @zoneInstanceId " +
                "                AND     ep.zone_type = @zoneType) AS X ) "
            };

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));
            parameters.Add(new MySqlParameter("@context", context));

            return GetExperiments(new List<string>(), whereClauses, parameters, string.Empty, 1, Int32.MaxValue, includeExperimentGroups);
        }

        /// <summary>
        /// Returns a list of active WorldExperimentGroups for a given world.
        /// </summary>
        /// <returns>IList of WorldExperimentGroup objects.</returns>
        public IList<WorldExperimentGroup> GetActiveWorldExperimentGroups(int zoneInstanceId, int zoneType, bool includeExperimentGroups = true)
        {
            List<string> whereClauses = new List<string> 
            {
                "AND e.status IN ('" + EXPERIMENT_STATUS_TYPE.STARTED + "', '" + EXPERIMENT_STATUS_TYPE.ENDED + "') ",
                "AND ep.zone_instance_id = @zoneInstanceId",
                "AND ep.zone_type = @zoneType"
            };

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));

            return GetWorldExperimentGroups(new List<string>(), whereClauses, parameters, string.Empty, 1, Int32.MaxValue, includeExperimentGroups);
        }

        /// <summary>
        /// Helper function used to pull lists of WorldTemplateExperimentGroup.
        /// </summary>
        /// <returns>PagedList of WorldTemplateExperimentGroup objects.</returns>
        private PagedList<WorldTemplateExperimentGroup> GetWorldTemplateExperimentGroups(List<string> joins, List<string> whereClauses, List<IDbDataParameter> parameters,
            string orderBy, int pageNumber, int pageSize, bool includeExperimentGroups = true)
        {
            PagedList<WorldTemplateExperimentGroup> weGroups = new PagedList<WorldTemplateExperimentGroup>();
            HashSet<string> experimentIds = new HashSet<string>();

            // Grab user experiment groups.
            string strQuery = "SELECT ep.template_id, " +
                              " ep.experiment_group_id, " +
                              " e.experiment_id " +
                              "FROM experiment.experiments e " +
                              "INNER JOIN experiment.experiment_groups eg ON eg.experiment_id = e.experiment_id " +
                              "INNER JOIN experiment.world_template_experiment_groups ep ON ep.experiment_group_id = eg.experiment_group_id " +
                              string.Join(" ", joins) + " " +
                              "WHERE 1 = 1 " +
                              string.Join(" ", whereClauses);

            PagedDataTable pdt = Db.Master.GetPagedDataTable(strQuery, orderBy, parameters, pageNumber, pageSize);
            if (pdt == null)
            {
                return weGroups;
            }

            foreach (DataRow row in pdt.Rows)
            {
                weGroups.Add(new WorldTemplateExperimentGroup(Convert.ToInt32(row["template_id"]), row["experiment_group_id"].ToString()));

                if (!experimentIds.Contains(row["experiment_id"].ToString()))
                {
                    experimentIds.Add(row["experiment_id"].ToString());
                }
            }

            // Grab ExperimentGroups and Experiments, if necessary.
            if (includeExperimentGroups && weGroups.Count > 0)
            {
                IList<Experiment> experiments = GetExperiments(new List<string>(), new List<string> { "AND e.experiment_id IN ('" + string.Join("', '", experimentIds) + "')" },
                    new List<IDbDataParameter>(), string.Empty, 1, Int32.MaxValue);

                Dictionary<string, ExperimentGroup> groupLookup = experiments.SelectMany(e => e.ExperimentGroups).ToDictionary(g => g.GroupId);

                foreach (WorldTemplateExperimentGroup weGroup in weGroups)
                {
                    ExperimentGroup group;
                    if (groupLookup.TryGetValue(weGroup.GroupId, out group))
                    {
                        weGroup.ExperimentGroup = group;
                    }
                }
            }

            weGroups.TotalCount = pdt.TotalCount;

            return weGroups;
        }

        /// <summary>
        /// Returns a list of template experiment groups.
        /// </summary>
        public PagedList<WorldTemplateExperimentGroup> GetWorldTemplateExperimentGroups(WorldTemplate template, string orderBy, int pageNumber, int pageSize, bool includeExperimentGroups = true)
        {
            List<string> whereClauses = new List<string> 
            {
                "AND ep.template_id = @templateId"
            };

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@templateId", template.TemplateId));

            return GetWorldTemplateExperimentGroups(new List<string>(), whereClauses, parameters, orderBy, pageNumber, pageSize, includeExperimentGroups);
        }

        #endregion
    }
}
