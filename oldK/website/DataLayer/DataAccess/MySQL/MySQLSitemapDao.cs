///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLSitemapDao : ISitemapDao
    {
        /// <summary>
        /// GetCommunities
        /// </summary>
        public PagedList<string> GetCommunities (int communityTypeId, string filter, string orderby, int pageNumber, int pageSize)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string sqlSelect = " SELECT c.community_id, c.category_id, c.place_type_id, c.name_no_spaces, " +
                " c.community_type_id, c.status_id, c.is_public, c.is_adult, c.is_personal, c.over_21_required, c.name " +
                " FROM communities c ";

            string sqlWhere = " WHERE c.status_id = @statusId ";

            // Limit by particular community type
            if (communityTypeId > 0)
            {
                sqlWhere += " AND c.community_type_id = @communityTypeId ";
                parameters.Add (new MySqlParameter ("@communityTypeId", communityTypeId));
            }

            if (filter.Length > 0)
            {
                sqlWhere += " AND " + filter;
            }

            string sql = sqlSelect + sqlWhere;

            parameters.Add (new MySqlParameter ("@statusId", (int) CommunityStatus.ACTIVE));
            DataTable dt = Db.Slave.GetPagedDataTable (sql, orderby, parameters, pageNumber, pageSize);

            PagedList<string> list = new PagedList<string> ();

            foreach (DataRow row in dt.Rows)
            {
                list.Add (row["name_no_spaces"].ToString ());
            }

            return list;
        }

        /// <summary>
        /// Get a list of users
        /// </summary>
        public PagedList<string> GetUsers (string filter, string orderby, int pageNumber, int pageSize)
        {
            string sqlSelect = "SELECT user_id, username, status_id, age " +
                " FROM users u ";

            string sqlWhere = "";
            if ((filter != null) && (filter != ""))
            {
                sqlWhere += " WHERE " + filter;       
            }

            string sql = sqlSelect + sqlWhere;

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            DataTable dt = Db.Slave.GetPagedDataTable (sql, orderby, parameters, pageNumber, pageSize);

            PagedList<string> list = new PagedList<string> ();

            foreach (DataRow row in dt.Rows)
            {
                list.Add (row["username"].ToString());
            }

            return list;
        }

        /// <summary>
        /// Get an Asset
        /// </summary>
        public PagedList <Int32> GetAssets (string filter, string orderby, int pageNumber, int pageSize)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string sqlSelect = " SELECT a.asset_id, a.asset_type_id, a.status_id, a.mature, a.public, " +
                " a.over_21_required, a.name " +
                " FROM assets a ";

            string sqlWhere = " WHERE a.status_id IN (" + (int) Asset.eASSET_STATUS.ACTIVE + "," + (int) Asset.eASSET_STATUS.NEW + ") ";

            if ((filter != null) && (filter != ""))
            {
                sqlWhere += " AND " + filter;
            }

            string sql = sqlSelect + sqlWhere;

            DataTable dt = Db.Slave.GetPagedDataTable (sql, orderby, parameters, pageNumber, pageSize);

            PagedList<Int32> list = new PagedList<Int32> ();

            foreach (DataRow row in dt.Rows)
            {
                list.Add (Convert.ToInt32 (row["asset_id"]));
            }

            return list;
        }

        /// <summary>
        /// Get Shopping Itmes
        /// </summary>
        public PagedList<Int32> GetShopItems (string filter, string orderby, int pageNumber, int pageSize)
        {
            //attach primary key to columns and attach items table to columns to form SQL string
            string sqlSelect = "SELECT global_id, item_active " +
                 " FROM shopping.items_web ";

            string sqlWhere = " WHERE item_active = 1 ";

            if ((filter != null) && (filter != ""))
            {
                sqlWhere += " AND " + filter;
            }

            string sql = sqlSelect + sqlWhere;

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            DataTable dt = Db.Slave.GetPagedDataTable (sql, orderby, parameters, pageNumber, pageSize);

            PagedList<Int32> list = new PagedList<Int32> ();

            foreach (DataRow row in dt.Rows)
            {
                list.Add (Convert.ToInt32 (row["global_id"]));
            }

            return list;
        }
    }
}
