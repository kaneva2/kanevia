///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;
using System.Diagnostics;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Linq;

using log4net;
using Sphinx.Client.Commands.Search;
using Sphinx.Client.Connections;
using System.Transactions;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLShoppingDao : IShoppingDao
    {
        /// <summary>
        /// AddCustomItem
        /// </summary>
        public int AddCustomItem (WOKItem item, UInt32 animTargetActorGLID)
        {
            return AddCustomItem (item, false, true, animTargetActorGLID);
        }

        /// <summary>
        /// AddCustomItem
        /// </summary>
        public int AddCustomItem (WOKItem item, bool useUGCNumbering, bool isWebItem)
        {
            return AddCustomItem (item, useUGCNumbering, isWebItem, 0);
        }

        /// <summary>
        /// AddCustomItem
        /// </summary>
        public int AddCustomItem (WOKItem item, bool useUGCNumbering, bool isWebItem, UInt32 animTargetActorGLID, bool propagateTransExceptions = false)
        {
            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    // If the globalId is not already specified, grab it
                    if (item.GlobalId == 0)
                    {
                        if (useUGCNumbering)
                            item.GlobalId = int.Parse(Db.Shard.GetScalar("SELECT shard_info.get_items_ugc_id()", 2).ToString());
                        else
                            item.GlobalId = int.Parse(Db.Shard.GetScalar("SELECT shard_info.get_items_custom_id()", 2).ToString());
                    }

                    // Only process if a globalId has been selected
                    if (item.GlobalId > 0)
                    {
                        string sqlString = "INSERT INTO wok.items ( " +
                            " global_id, base_global_id, name, description, market_cost, selling_price, " +
                            " required_skill, required_skill_level, use_type, " +
                            " item_creator_id, " +
                            " arm_anywhere, disarmable, stackable, destroy_when_used, " +
                            " inventory_type, expired_duration, is_derivable, derivation_level, actor_group, search_refresh_needed " +
                            " ) VALUES (" +
                            " @globalId, @base_global_id, @name, @description, @market_cost, @selling_price, " +
                            " @required_skill, @required_skill_level, @use_type, @item_creator_id, " +
                            " @arm_anywhere, @disarmable, @stackable, @destroy_when_used, " +
                            " @inventory_type, @expired_duration, @isDerivable, @derivationLevel, @actorGroup, NOW())";

                        List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                        parameters.Add(new MySqlParameter("@globalId", item.GlobalId));
                        parameters.Add(new MySqlParameter("@base_global_id", item.BaseGlobalId));
                        parameters.Add(new MySqlParameter("@name", item.Name));
                        parameters.Add(new MySqlParameter("@description", item.Description));
                        parameters.Add(new MySqlParameter("@market_cost", item.MarketCost));
                        parameters.Add(new MySqlParameter("@selling_price", item.SellingPrice));
                        parameters.Add(new MySqlParameter("@required_skill", item.RequiredSkill));
                        parameters.Add(new MySqlParameter("@required_skill_level", item.RequiredSkillLevel));
                        parameters.Add(new MySqlParameter("@use_type", item.UseType));
                        parameters.Add(new MySqlParameter("@item_creator_id", item.ItemCreatorId));
                        parameters.Add(new MySqlParameter("@arm_anywhere", item.ArmAnywhere));
                        parameters.Add(new MySqlParameter("@disarmable", item.Disarmable));
                        parameters.Add(new MySqlParameter("@stackable", item.Stackable));
                        parameters.Add(new MySqlParameter("@destroy_when_used", item.DestroyWhenUsed));
                        parameters.Add(new MySqlParameter("@inventory_type", item.InventoryType));
                        parameters.Add(new MySqlParameter("@expired_duration", item.ExpiredDuration));
                        parameters.Add(new MySqlParameter("@isDerivable", item.IsDerivable));
                        parameters.Add(new MySqlParameter("@derivationLevel", item.DerivationLevel));
                        parameters.Add(new MySqlParameter("@actorGroup", item.ActorGroup));
                        int result = Db.WOK.ExecuteNonQuery(sqlString, parameters, true);

                        if (isWebItem)
                        {
                            result = AddWebItem(item, true);
                        }

                        // Set Access Pass
                        // Only able to set here, per Jim, he is adding code to server to read
                        // Access Pass flag, but it will only be read once, changes don't get re-read by server.
                        //do not insert any pass type as general pass
                        //given current wok architecture this would cause problems
                        if ((item.GlobalId > 0) && (item.PassTypeId > 0))
                        {
                            sqlString = " INSERT INTO wok.pass_group_items " +
                                 " (global_id, pass_group_id) " +
                                 " VALUES " +
                                 " (@global_id, @pass_group_id) ";
                            parameters = new List<IDbDataParameter>();
                            parameters.Add(new MySqlParameter("@global_id", item.GlobalId));
                            parameters.Add(new MySqlParameter("@pass_group_id", item.PassTypeId));
                            Db.WOK.ExecuteNonQuery(sqlString, parameters, true);
                        }

                        // If animTargetActorGLID > 0 then this is a DO animation and we need
                        // to add this to the item animation relationships table so we know what 
                        // item this animation is assigned to
                        if (animTargetActorGLID > 0)
                        {
                            sqlString = " INSERT INTO wok.item_animations " +
                                 " (item_id, animation_id) " +
                                 " VALUES " +
                                 " (@item_id, @animation_id) ";
                            parameters = new List<IDbDataParameter>();
                            parameters.Add(new MySqlParameter("@item_id", animTargetActorGLID));
                            parameters.Add(new MySqlParameter("@animation_id", item.GlobalId));
                            Db.WOK.ExecuteNonQuery(sqlString, parameters, true);
                        }

                        // Insert item_parameters records based on item's use type
                        if ((item.GlobalId > 0) && ((item.UseType == WOKItem.USE_TYPE_PREMIUM) || (item.UseType == WOKItem.USE_TYPE_SOUND)))
                        {
                            UpdateItemParameter(item.GlobalId, item.UseType, item.GlobalId.ToString(), true);
                        }
                        else if (item.UseType != WOKItem.USE_TYPE_PREMIUM)
                        {
                            UpdateItemParameter(item.GlobalId, item.UseType, item.UseValue.ToString(), true);
                        }

                        // If we reach this point, the transaction was successful
                        transaction.Complete();
                    }
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLShoppingDao.AddCustomItem() - Transaction aborted: " + e.ToString());

                if (propagateTransExceptions)
                    throw e;
            }

            // If globalId is still 0, we have an set to -1
            item.GlobalId = (item.GlobalId > 0 ? item.GlobalId : -1);

            return item.GlobalId;
        }

        /// <summary>
        /// AddWebItem - Adds a given wokitem to shop. Updates if already exists.
        /// </summary>
        public int AddWebItem(WOKItem item, bool propagateTransExceptions = false)
        {
            int result = 0;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    string sqlString = "INSERT INTO shopping.items_web ( " +
                                     " global_id, date_added, base_global_id, template_path, texture_path, " +
                                     " thumbnail_path, thumbnail_small_path, thumbnail_medium_path, thumbnail_large_path, thumbnail_assetdetails_path, designer_price, keywords, " +
                                     " item_active, display_name, search_refresh_needed " +
                                     " ) VALUES (" +
                                     " @globalId, NOW(), @base_global_id, @template_path, @texture_path, " +
                                     " @thumbnail_path, @thumbnail_small_path, @thumbnail_medium_path, @thumbnail_large_path, @thumbnail_assetdetails_path, @designer_price, @keywords, " +
                                     " @item_active, @display_name, NOW()) " +
                                     " ON DUPLICATE KEY UPDATE base_global_id = @base_global_id, template_path = @template_path, texture_path = @texture_path, " +
                                     " thumbnail_path = @thumbnail_path, thumbnail_small_path = @thumbnail_small_path, thumbnail_medium_path = @thumbnail_medium_path, thumbnail_large_path = @thumbnail_large_path, " +
                                     " thumbnail_assetdetails_path = @thumbnail_assetdetails_path, designer_price = @designer_price, keywords = @keywords, " +
                                     " item_active = @item_active, display_name = @display_name";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@globalId", item.GlobalId));
                    parameters.Add(new MySqlParameter("@base_global_id", item.BaseGlobalId));
                    parameters.Add(new MySqlParameter("@template_path", item.TemplatePath));
                    parameters.Add(new MySqlParameter("@texture_path", item.TexturePath));
                    parameters.Add(new MySqlParameter("@thumbnail_path", item.ThumbnailPath));
                    parameters.Add(new MySqlParameter("@thumbnail_small_path", item.ThumbnailSmallPath));
                    parameters.Add(new MySqlParameter("@thumbnail_medium_path", item.ThumbnailMediumPath));
                    parameters.Add(new MySqlParameter("@thumbnail_large_path", item.ThumbnailLargePath));
                    parameters.Add(new MySqlParameter("@thumbnail_assetdetails_path", item.ThumbnailAssetdetailsPath));
                    parameters.Add(new MySqlParameter("@designer_price", item.DesignerPrice));
                    parameters.Add(new MySqlParameter("@keywords", item.Keywords));
                    parameters.Add(new MySqlParameter("@item_active", item.ItemActive));
                    parameters.Add(new MySqlParameter("@display_name", item.DisplayName));
                    result = Db.Shopping.ExecuteNonQuery(sqlString, parameters, true);

                    // If we reach this point, the transaction was successful
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLShoppingDao.AddWebItem() - Transaction aborted: " + e.ToString());

                if (propagateTransExceptions)
                    throw e;
            }

            return result;
        }

        public void AddItemToGame (int globalId, int gameId, int approvalStatus)
        {
            string sqlString = "INSERT INTO wok.game_items (game_id, global_id, approval_status) VALUES (@game_id, @global_id, @approval_status)";
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@game_id", gameId));
            parameters.Add (new MySqlParameter ("@global_id", globalId));
            parameters.Add(new MySqlParameter("@approval_status", approvalStatus));
            Db.WOK.ExecuteNonQuery (sqlString, parameters);
        }

        public bool IsItemAccessPass (int globalId)
        {
            //create query string to get game by user id
            string sql = "SELECT global_id " +
                " FROM wok.pass_group_items " +
                " WHERE global_id = @global_id " +
                " AND pass_group_id = @pass_group_id";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@global_id", globalId));
            parameters.Add (new MySqlParameter ("@pass_group_id", 1));

            //perform query returning single data row from the wok database
            DataRow row = Db.WOK.GetDataRow (sql.ToString (), parameters);
            return (row != null);
        }

        /// <summary>
        /// UpdateItemTextureAsInactive
        /// </summary>
        public int UpdateItemTextureActiveState (int globalId, int itemActiveState)
        {
            // Mark as inactive
            string sql = "CALL update_item_active_state(@globalId, @itemActive)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));
            parameters.Add (new MySqlParameter ("@itemActive", itemActiveState));
            return Db.WOK.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// DeleteItemFromPendingAdds
        /// </summary>
        public int DeleteItemFromPendingAdds (int globalId)
        {
            // Remove it from everyones inventory
            string sql = "CALL delete_item_from_pending_adds(@globalId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));
            return Db.WOK.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// DeleteItemFromDynamicObjects
        /// </summary>
        public int DeleteItemFromDynamicObjects (int globalId)
        {
            // Remove it from everyones inventory
            string sql = "CALL delete_item_from_dynamic_objects(@globalId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));
            return Db.WOK.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// DeleteItemFromAllInventory
        /// </summary>
        public int DeleteItemFromAllInventory (int globalId)
        {
            // Remove it from everyones inventory
            string sql = "CALL delete_item_from_inventories(@globalId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));
            return Db.WOK.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// UpdateCustomItemTexture
        /// </summary>
        public int UpdateCustomItemTexture (WOKItem item)
        {
            // Send the message
            string sql = "UPDATE items_web SET " +
                " texture_path = @texture_path " +
                " WHERE global_id  =  @global_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@global_id", item.GlobalId));
            parameters.Add (new MySqlParameter ("@texture_path", item.TexturePath));
            return Db.Shopping.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// UpdateCustomItem
        /// </summary>
        public int UpdateCustomItem (WOKItem item)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            
            // Update sql
            string sql = "UPDATE items SET " +
                " name = @name, " +
                " description = @description, " +
                " inventory_type = @inventory_type, " +
                " search_refresh_needed = NOW() ";

            if ((item.UseType == WOKItem.USE_TYPE_BUNDLE || item.UseType == WOKItem.USE_TYPE_CUSTOM_DEED || item.UseType == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM) 
                && item.MarketCost > 0)
            {
                sql += ", market_cost = @market_cost ";
                parameters.Add (new MySqlParameter ("@market_cost", item.MarketCost));
            }

            sql += " WHERE global_id  =  @global_id";

            parameters.Add (new MySqlParameter ("@global_id", item.GlobalId));
            parameters.Add (new MySqlParameter ("@name", item.Name));
            parameters.Add (new MySqlParameter ("@description", item.Description));
            parameters.Add (new MySqlParameter ("@inventory_type", item.InventoryType));
            Db.WOK.ExecuteNonQuery (sql, parameters);

            sql = "UPDATE items_web SET " +
                " item_active = @item_active, " +
                " display_name = @display_name " +
                " WHERE global_id  =  @global_id";

            parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@item_active", item.ItemActive));
            parameters.Add (new MySqlParameter ("@global_id", item.GlobalId));
            parameters.Add (new MySqlParameter ("@display_name", item.DisplayName));
            Db.Shopping.ExecuteNonQuery (sql, parameters);

            //do not update any pass type to general pass
            //given current wok architecture this would cause problems
            //also users are currently not allowed to update pass types anyway
            if (item.PassTypeId > 0)
            {
                //update the items pass group
                sql = "INSERT INTO pass_group_items (global_id, pass_group_id) " +
                      "VALUES (@global_id, @pass_group_id) " +
                      "ON DUPLICATE KEY UPDATE pass_group_id = @pass_group_id";

                parameters = new List<IDbDataParameter> ();
                parameters.Add (new MySqlParameter ("@global_id", item.GlobalId));
                parameters.Add (new MySqlParameter ("@pass_group_id", item.PassTypeId));
                Db.WOK.ExecuteNonQuery (sql, parameters);
            }

            //update items on web
            bool bUpdateTemplateDesignerPrice = false;
            if (item.TemplateDesignerPrice != UInt32.MaxValue)
            {
                bUpdateTemplateDesignerPrice = true;
                sql = "UPDATE items_web iw LEFT JOIN items_web iw2 on iw.base_global_id=iw2.global_id SET " +
                    " iw.designer_price = @designerPrice, " +
                    " iw.keywords = @keywords, " +
                    " iw2.designer_price = @templateDesignerPrice " +       // Update designer price for template item 
                    " WHERE iw.global_id  =  @global_id";
            }
            else
            {
                sql = "UPDATE items_web SET " +
                    " designer_price = @designerPrice, " +
                    " keywords = @keywords " +
                    " WHERE global_id  =  @global_id";
            }

            parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@global_id", item.GlobalId));
            parameters.Add (new MySqlParameter ("@designerPrice", item.DesignerPrice));
            parameters.Add (new MySqlParameter ("@keywords", item.Keywords));
            if (bUpdateTemplateDesignerPrice)
                parameters.Add (new MySqlParameter ("@templateDesignerPrice", item.TemplateDesignerPrice));
            return Db.Shopping.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// AdminUpdateCustomItem
        /// </summary>
        public int UpdateShopItem (WOKItem modifiedItem, WOKItem origItem, int modifyingUserId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            // Update sql
            string sql = "UPDATE items SET " +
                " name = @name, " +
                " description = @description, " +
                " inventory_type = @inventory_type, " +
                " market_cost = @market_cost, " +
                " selling_price = @selling_price, " +
                " use_type = @use_type, " +
                " item_creator_id = @item_creator_id, " +
                " search_refresh_needed = NOW() " +
                " WHERE global_id  =  @global_id";

            parameters.Add (new MySqlParameter ("@global_id", modifiedItem.GlobalId));
            parameters.Add (new MySqlParameter ("@name", modifiedItem.Name));
            parameters.Add (new MySqlParameter ("@description", modifiedItem.Description));
            parameters.Add (new MySqlParameter ("@inventory_type", modifiedItem.InventoryType));
            parameters.Add (new MySqlParameter ("@market_cost", modifiedItem.MarketCost));
            parameters.Add (new MySqlParameter ("@selling_price", modifiedItem.SellingPrice));
            parameters.Add (new MySqlParameter ("@use_type", modifiedItem.UseType));
            parameters.Add (new MySqlParameter ("@item_creator_id", modifiedItem.ItemCreatorId));
            Db.WOK.ExecuteNonQuery (sql, parameters);

            sql = "UPDATE items_web SET " +
                " item_active = @item_active, " +
                " display_name = @display_name, " +
                " date_added = @date_added, " +
                " keywords = @keywords " +
                " WHERE global_id  =  @global_id";

            parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@item_active", modifiedItem.ItemActive));
            parameters.Add (new MySqlParameter ("@display_name", modifiedItem.DisplayName));
            parameters.Add (new MySqlParameter ("@date_added", modifiedItem.DateAdded));
            parameters.Add (new MySqlParameter ("@keywords", modifiedItem.Keywords));
            parameters.Add (new MySqlParameter ("@global_id", modifiedItem.GlobalId));
            Db.Shopping.ExecuteNonQuery (sql, parameters);

            if (modifiedItem.PassTypeId > 0 && origItem.PassTypeId == 0)
            {
                //add item to pass group
                sql = "CALL add_pass_group_items(@pass_group_id, @global_id, @modifying_user_id)";

                parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@global_id", modifiedItem.GlobalId));
                parameters.Add(new MySqlParameter("@pass_group_id", modifiedItem.PassTypeId));
                parameters.Add(new MySqlParameter("@modifying_user_id", modifyingUserId));
                Db.WOK.ExecuteNonQuery(sql, parameters);
            }
            else if (modifiedItem.PassTypeId == 0 && origItem.PassTypeId > 0)
            {
                // Remove item from pass group
                sql = "CALL delete_pass_group_items(@pass_group_id, @global_id, @modifying_user_id)";

                parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@global_id", modifiedItem.GlobalId));
                parameters.Add(new MySqlParameter("@pass_group_id", origItem.PassTypeId));
                parameters.Add(new MySqlParameter("@modifying_user_id", modifyingUserId));
                Db.WOK.ExecuteNonQuery(sql, parameters);
            }

            //update items on web
            bool bUpdateTemplateDesignerPrice = false;
            if (modifiedItem.TemplateDesignerPrice != origItem.TemplateDesignerPrice)
            {
                bUpdateTemplateDesignerPrice = true;
                sql = "UPDATE items_web iw LEFT JOIN items_web iw2 on iw.base_global_id=iw2.global_id SET " +
                    " iw.designer_price = @designerPrice, " +
                    " iw2.designer_price = @templateDesignerPrice " +       // Update designer price for template item 
                    " WHERE iw.global_id  =  @global_id";
            }
            else
            {
                sql = "UPDATE items_web SET " +
                    " designer_price = @designerPrice " +
                    " WHERE global_id  =  @global_id";
            }

            parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@global_id", modifiedItem.GlobalId));
            parameters.Add (new MySqlParameter ("@designerPrice", modifiedItem.DesignerPrice));
            if (bUpdateTemplateDesignerPrice)
                parameters.Add (new MySqlParameter ("@templateDesignerPrice", modifiedItem.TemplateDesignerPrice));
            return Db.Shopping.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// InsertIntoStoreInventories - inserts an item into an itemset
        /// </summary>
        public int InsertIntoStoreInventories(int storeId, int itemId)
        {
            // Record User Asset Subscriptions
            string sqlInsert = "INSERT INTO store_inventories (store_id, global_id) " +
                               "VALUES (@storeId, @itemId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter ("@storeId", storeId));
            parameters.Add(new MySqlParameter ("@itemId", itemId));
            return Db.WOK.ExecuteIdentityInsert(sqlInsert, parameters);
        }

        /// <summary>
        /// DeleteFromStoreInventories - removes an item from the store inventory
        /// </summary>
        public int DeleteFromStoreInventories(int storeId, int itemId)
        {
            //delete from contest table
            string sqlString = "DELETE FROM store_inventories " +
                               "WHERE store_id = @storeId AND global_id = @itemId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@storeId", storeId));
            parameters.Add(new MySqlParameter("@itemId", itemId));

            return Db.WOK.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// DeleteFromStoreInventories - removes an item from the store inventory
        /// </summary>
        public int DeleteFromStoreInventories(int itemId)
        {
            //delete from contest table
            string sqlString = "DELETE FROM store_inventories " +
                               "WHERE global_id = @itemId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@itemId", itemId));

            return Db.WOK.ExecuteNonQuery(sqlString, parameters);
        }
        
        /// <summary>
        /// UpdateCustomItemTextureEncryption
        /// </summary>
        public int UpdateCustomItemTextureEncryption (WOKItem item, string template_path_encrypted)
        {
            // Update sql
            string sql = "UPDATE items_web SET " +
                " template_path_encrypted = @template_path_encrypted " +
                " WHERE global_id = @global_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@global_id", item.GlobalId));
            parameters.Add (new MySqlParameter ("@template_path_encrypted", template_path_encrypted));
            return Db.Shopping.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// DeleteCustomItemThumbnail
        /// </summary>
        public int DeleteCustomItemThumbnail (int globalId)
        {
            // Mark as inactive
            string sql = "UPDATE items_web SET " +
                " thumbnail_path = null, " +
                " thumbnail_small_path = null, " +
                " thumbnail_medium_path = null, " +
                " thumbnail_large_path = null, " +
                " thumbnail_assetdetails_path = null " +
                " WHERE global_id = @globalId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));
            return Db.Shopping.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// GetItem
        /// </summary>
        public WOKItem GetItem (int globalId, bool bAllowUGCTemplate)
        {
            //create query string to get game by user id
            string sql = "SELECT i.global_id,name,description,market_cost, " +
                " selling_price, required_skill, required_skill_level, use_type, " +
                " iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId, " +
                " iw.date_added, " +
                " item_creator_id, iw.item_active, arm_anywhere, " +
                " disarmable, stackable, destroy_when_used, " +
                " inventory_type, actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " +
                " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
                " (i.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice, " +
                " expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total, " +
                " i.is_derivable, i.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice, " +
                " iw.category_id1, iw.category_id2, iw.category_id3, IFNULL(gi.game_id,0) as game_id, IFNULL(gi.approval_status, 0) as approval_status " +
                " FROM items i INNER JOIN shopping.items_web iw ON i.global_id = iw.global_id " +
                " LEFT OUTER JOIN shopping.items_web iw2 on iw.base_global_id=iw2.global_id " + // get base item info
                " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id " +
                " LEFT OUTER JOIN wok.game_items gi ON i.global_id = gi.global_id " +
                " WHERE i.global_id = @globalId ";

            if (!bAllowUGCTemplate)
                sql = sql + " AND wok.isUGCMeshTemplate(i.global_id)=0 ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));

            //perform query returning single data row from the wok database
            DataRow row = Db.WOK.GetDataRow (sql.ToString (), parameters);
            if (row == null)
                return null;

            return new WOKItem (Convert.ToInt32 (row["global_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToInt32 (row["market_cost"]),
                   Convert.ToInt32 (row["selling_price"]), Convert.ToInt32 (row["required_skill"]), Convert.ToInt32 (row["required_skill_level"]), Convert.ToInt32 (row["use_type"]),
                   row["display_name"].ToString (), Convert.ToInt32 (row["passTypeId"]),
                   Convert.ToDateTime (row["date_added"]),
                   Convert.ToUInt32 (row["item_creator_id"]), Convert.ToInt32 (row["item_active"]), Convert.ToInt32 (row["arm_anywhere"]),
                   Convert.ToInt32 (row["disarmable"]), Convert.ToInt32 (row["stackable"]), Convert.ToInt32 (row["destroy_when_used"]),
                   Convert.ToInt32 (row["inventory_type"]), Convert.ToUInt32 (row["base_global_id"]), row["template_path"].ToString (), row["texture_path"].ToString (),
                   row["thumbnail_path"].ToString (), row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (), row["thumbnail_assetdetails_path"].ToString (), Convert.ToUInt32 (row["designer_price"]),
                   row["keywords"].ToString (), Convert.ToInt32 (row["WebPrice"]), Convert.ToInt32 (row["expired_duration"]), Convert.ToUInt32 (row["number_sold_on_web"]), Convert.ToUInt32 (row["number_of_raves"]),
                   Convert.ToUInt32 (row["number_of_views"]), Convert.ToUInt32 (row["sales_total"]), Convert.ToUInt32 (row["sales_designer_total"]),
                   Convert.ToInt32 (row["is_derivable"]), Convert.ToInt32 (row["derivation_level"]), Convert.ToUInt32 (row["TemplateDesignerPrice"]),
                   Convert.ToUInt32 (row["category_id1"]), Convert.ToUInt32 (row["category_id2"]), Convert.ToUInt32 (row["category_id3"]), Convert.ToInt32 (row["actor_group"]), Convert.ToInt32 (row["game_id"]), (WOKItem.ItemApprovalStatus) Convert.ToInt32 (row["approval_status"])
                   );
        }

        /// <summary>
        /// GetItems
        /// </summary>
        public List<WOKItem> GetItems(IEnumerable<int> globalIds, bool bAllowUGCTemplate, string orderBy)
        {
            // Make sure item list is not null
            if (globalIds == null || globalIds.Count() == 0)
                return new List<WOKItem>();

            //create query string to get game by user id
            string sql = "SELECT i.global_id,name,description,market_cost, " +
                " selling_price, required_skill, required_skill_level, use_type, " +
                " iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId, " +
                " iw.date_added, " +
                " item_creator_id, iw.item_active, arm_anywhere, " +
                " disarmable, stackable, destroy_when_used, " +
                " inventory_type, actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " +
                " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
                " (i.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice, " +
                " expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total, " +
                " i.is_derivable, i.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice, " +
                " iw.category_id1, iw.category_id2, iw.category_id3, IFNULL(gi.game_id,0) as game_id, IFNULL(gi.approval_status, 0) as approval_status " +
                " FROM items i INNER JOIN shopping.items_web iw ON i.global_id = iw.global_id " +
                " LEFT OUTER JOIN shopping.items_web iw2 on iw.base_global_id=iw2.global_id " + // get base item info
                " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id " +
                " LEFT OUTER JOIN wok.game_items gi ON i.global_id = gi.global_id " +
                " WHERE i.global_id IN (" + string.Join(",", globalIds) + ") ";

            if (!bAllowUGCTemplate)
                sql = sql + " AND wok.isUGCMeshTemplate(i.global_id)=0 ";

            if (!string.IsNullOrWhiteSpace(orderBy))
                sql = sql + " ORDER BY " + orderBy;

            //Get data
            DataTable dt = Db.WOK.GetDataTable(sql.ToString());

            if (dt == null || dt.Rows.Count == 0)
                return new List<WOKItem>();

            List<WOKItem> items = new List<WOKItem>();
            foreach (DataRow row in dt.Rows)
            {
                items.Add(new WOKItem(Convert.ToInt32(row["global_id"]), row["name"].ToString(), row["description"].ToString(), Convert.ToInt32(row["market_cost"]),
                       Convert.ToInt32(row["selling_price"]), Convert.ToInt32(row["required_skill"]), Convert.ToInt32(row["required_skill_level"]), Convert.ToInt32(row["use_type"]),
                       row["display_name"].ToString(), Convert.ToInt32(row["passTypeId"]),
                       Convert.ToDateTime(row["date_added"]),
                       Convert.ToUInt32(row["item_creator_id"]), Convert.ToInt32(row["item_active"]), Convert.ToInt32(row["arm_anywhere"]),
                       Convert.ToInt32(row["disarmable"]), Convert.ToInt32(row["stackable"]), Convert.ToInt32(row["destroy_when_used"]),
                       Convert.ToInt32(row["inventory_type"]), Convert.ToUInt32(row["base_global_id"]), row["template_path"].ToString(), row["texture_path"].ToString(),
                       row["thumbnail_path"].ToString(), row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_assetdetails_path"].ToString(), Convert.ToUInt32(row["designer_price"]),
                       row["keywords"].ToString(), Convert.ToInt32(row["WebPrice"]), Convert.ToInt32(row["expired_duration"]), Convert.ToUInt32(row["number_sold_on_web"]), Convert.ToUInt32(row["number_of_raves"]),
                       Convert.ToUInt32(row["number_of_views"]), Convert.ToUInt32(row["sales_total"]), Convert.ToUInt32(row["sales_designer_total"]),
                       Convert.ToInt32(row["is_derivable"]), Convert.ToInt32(row["derivation_level"]), Convert.ToUInt32(row["TemplateDesignerPrice"]),
                       Convert.ToUInt32(row["category_id1"]), Convert.ToUInt32(row["category_id2"]), Convert.ToUInt32(row["category_id3"]), Convert.ToInt32(row["actor_group"]), Convert.ToInt32(row["game_id"]), (WOKItem.ItemApprovalStatus)Convert.ToInt32(row["approval_status"])
                       ));
            }

            return items;
        }

        #region Custom Deeds and Deed Items

        /// <summary>
        /// Retrieves a list of deed items representing dynamic objects placed in a world.
        /// </summary>
        public List<DeedItem> GetWorldDODeedItems(int zoneIndex, int zoneInstanceId, int worldOwnerId)
        {
            List<DeedItem> itemList = new List<DeedItem>();

            string sql = "SELECT do.global_id, name, description, market_cost, " +
	                     "    selling_price, required_skill, required_skill_level, use_type, " +
	                     "    iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId, " +
	                     "    iw.date_added, item_creator_id, iw.item_active, arm_anywhere, " +
	                     "    disarmable, stackable, destroy_when_used, " +
	                     "    inventory_type, actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " +
	                     "    iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, " +
	                     "    iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
	                     "    (i.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice, " +
	                     "    expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total, " +
	                     "    i.is_derivable, i.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice, " +
	                     "    iw.category_id1, iw.category_id2, iw.category_id3, " +
                         "    COUNT(*) AS quantity " +
                         "FROM dynamic_objects do " +
                         "INNER JOIN items i ON i.global_id = do.global_id " +
                         "INNER JOIN shopping.items_web iw ON iw.global_id = do.global_id " +
                         "LEFT OUTER JOIN shopping.items_web iw2 ON iw.base_global_id = iw2.global_id " +
                         "LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = do.global_id " +
                         "WHERE do.zone_index = @zoneIndex " +
                         "    AND do.zone_instance_id = @zoneInstanceId " +
	                     "    AND do.expired_date IS NULL " +
                         "GROUP BY do.global_id " +
                         "ORDER BY NULL";

            //perform query returning multiple data rows from the wok database
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneIndex", zoneIndex));
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));

            DataTable dt = Db.WOK.GetDataTable(sql, parameters);

            if (dt != null)
            {
                DeedItem dItem;
                foreach (DataRow row in dt.Rows)
                {
                    dItem = new DeedItem(worldOwnerId, new WOKItem(Convert.ToInt32(row["global_id"]), row["name"].ToString(), row["description"].ToString(), Convert.ToInt32(row["market_cost"]),
                           Convert.ToInt32(row["selling_price"]), Convert.ToInt32(row["required_skill"]), Convert.ToInt32(row["required_skill_level"]), Convert.ToInt32(row["use_type"]),
                           row["display_name"].ToString(), Convert.ToInt32(row["passTypeId"]),
                           Convert.ToDateTime(row["date_added"]),
                           Convert.ToUInt32(row["item_creator_id"]), Convert.ToInt32(row["item_active"]), Convert.ToInt32(row["arm_anywhere"]),
                           Convert.ToInt32(row["disarmable"]), Convert.ToInt32(row["stackable"]), Convert.ToInt32(row["destroy_when_used"]),
                           Convert.ToInt32(row["inventory_type"]), Convert.ToUInt32(row["base_global_id"]), row["template_path"].ToString(), row["texture_path"].ToString(),
                           row["thumbnail_path"].ToString(), row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_assetdetails_path"].ToString(), Convert.ToUInt32(row["designer_price"]),
                           row["keywords"].ToString(), Convert.ToInt32(row["WebPrice"]), Convert.ToInt32(row["expired_duration"]), Convert.ToUInt32(row["number_sold_on_web"]), Convert.ToUInt32(row["number_of_raves"]),
                           Convert.ToUInt32(row["number_of_views"]), Convert.ToUInt32(row["sales_total"]), Convert.ToUInt32(row["sales_designer_total"]),
                           Convert.ToInt32(row["is_derivable"]), Convert.ToInt32(row["derivation_level"]), Convert.ToUInt32(row["TemplateDesignerPrice"]),
                           Convert.ToUInt32(row["category_id1"]), Convert.ToUInt32(row["category_id2"]), Convert.ToUInt32(row["category_id3"]), Convert.ToInt32(row["actor_group"]), 0, WOKItem.ItemApprovalStatus.Pending
                           ));
                    dItem.Quantity = Convert.ToInt32(row["quantity"]);
                    itemList.Add(dItem);
                }
            }

            return itemList;
        }

        /// <summary>
        /// Retrieves a list of deed items representing animations and particles attached to dynamic objects placed in a world.
        /// </summary>
        public List<DeedItem> GetWorldDOPDeedItems(int zoneIndex, int zoneInstanceId, int worldOwnerId)
        {
            List<DeedItem> itemList = new List<DeedItem>();

            string sql = "SELECT i.global_id, name, description, market_cost, " +
	                     "    selling_price, required_skill, required_skill_level, use_type, " +
	                     "    iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId, " +
	                     "    iw.date_added, item_creator_id, iw.item_active, arm_anywhere, " +
	                     "    disarmable, stackable, destroy_when_used, " +
	                     "    inventory_type, actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " +
	                     "    iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, " +
	                     "    iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
	                     "    (i.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice, " +
	                     "    expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total, " +
	                     "    i.is_derivable, i.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice, " +
	                     "    iw.category_id1, iw.category_id2, iw.category_id3, " +
                         "    COUNT(*) AS quantity " +
                         "FROM dynamic_object_parameters dop " +
                         "INNER JOIN dynamic_objects do ON do.obj_placement_id = dop.obj_placement_id " +
                         "INNER JOIN items i ON i.global_id = dop.value " +
                         "INNER JOIN shopping.items_web iw ON iw.global_id = i.global_id " +
                         "LEFT OUTER JOIN shopping.items_web iw2 ON iw.base_global_id = iw2.global_id " +
                         "LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id " +
                         "WHERE dop.param_type_id in (32, 206) " +
                         "    AND do.zone_index = @zoneIndex " +
                         "    AND do.zone_instance_id = @zoneInstanceId " +
	                     "    AND do.expired_date IS NULL " +
                         "GROUP BY i.global_id " +
                         "ORDER BY NULL";

            //perform query returning multiple data rows from the wok database
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneIndex", zoneIndex));
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));

            DataTable dt = Db.WOK.GetDataTable(sql, parameters);

            if (dt != null)
            {
                DeedItem dItem;
                foreach (DataRow row in dt.Rows)
                {
                    dItem = new DeedItem(worldOwnerId, new WOKItem(Convert.ToInt32(row["global_id"]), row["name"].ToString(), row["description"].ToString(), Convert.ToInt32(row["market_cost"]),
                           Convert.ToInt32(row["selling_price"]), Convert.ToInt32(row["required_skill"]), Convert.ToInt32(row["required_skill_level"]), Convert.ToInt32(row["use_type"]),
                           row["display_name"].ToString(), Convert.ToInt32(row["passTypeId"]),
                           Convert.ToDateTime(row["date_added"]),
                           Convert.ToUInt32(row["item_creator_id"]), Convert.ToInt32(row["item_active"]), Convert.ToInt32(row["arm_anywhere"]),
                           Convert.ToInt32(row["disarmable"]), Convert.ToInt32(row["stackable"]), Convert.ToInt32(row["destroy_when_used"]),
                           Convert.ToInt32(row["inventory_type"]), Convert.ToUInt32(row["base_global_id"]), row["template_path"].ToString(), row["texture_path"].ToString(),
                           row["thumbnail_path"].ToString(), row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_assetdetails_path"].ToString(), Convert.ToUInt32(row["designer_price"]),
                           row["keywords"].ToString(), Convert.ToInt32(row["WebPrice"]), Convert.ToInt32(row["expired_duration"]), Convert.ToUInt32(row["number_sold_on_web"]), Convert.ToUInt32(row["number_of_raves"]),
                           Convert.ToUInt32(row["number_of_views"]), Convert.ToUInt32(row["sales_total"]), Convert.ToUInt32(row["sales_designer_total"]),
                           Convert.ToInt32(row["is_derivable"]), Convert.ToInt32(row["derivation_level"]), Convert.ToUInt32(row["TemplateDesignerPrice"]),
                           Convert.ToUInt32(row["category_id1"]), Convert.ToUInt32(row["category_id2"]), Convert.ToUInt32(row["category_id3"]), Convert.ToInt32(row["actor_group"]), 0, WOKItem.ItemApprovalStatus.Pending
                           ));
                    dItem.Quantity = Convert.ToInt32(row["quantity"]);
                    itemList.Add(dItem);
                }
            }

            return itemList;
        }

        /// <summary>
        /// Retrieves a list of deed items representing dynamic objects placed in a Shop world.
        /// </summary>
        public List<DeedItem> GetDODeedItems(int deedTemplateId, int deedOwnerId)
        {
            List<DeedItem> itemList = new List<DeedItem>();

            string sql = "SELECT do.global_id, name, description, market_cost, " +
                         "   selling_price, required_skill, required_skill_level, use_type, " +
                         "   iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId, " + 
                         "   iw.date_added, item_creator_id, iw.item_active, arm_anywhere, " +
                         "   disarmable, stackable, destroy_when_used, " +
                         "   inventory_type, actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " + 
                         "   iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, " +
                         "   iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
                         "   (i.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice, " +
                         "   expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total, " +
                         "   i.is_derivable, i.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice, " +
                         "   iw.category_id1, iw.category_id2, iw.category_id3, " +
                         "   COUNT(*) AS quantity " +
                         "FROM starting_dynamic_objects do " +
                         "INNER JOIN items i ON i.global_id = do.global_id " +
                         "INNER JOIN shopping.items_web iw ON iw.global_id = do.global_id " +
                         "LEFT OUTER JOIN shopping.items_web iw2 ON iw.base_global_id = iw2.global_id " +
                         "LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = do.global_id " +
                         "WHERE do.apartment_template_id = @deedTemplateId " +
                         "GROUP BY do.global_id " +
                         "ORDER BY NULL";

            //perform query returning multiple data rows from the wok database
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@deedTemplateId", deedTemplateId));

            DataTable dt = Db.WOK.GetDataTable(sql, parameters);

            if (dt != null)
            {
                DeedItem dItem;
                foreach (DataRow row in dt.Rows)
                {
                    dItem = new DeedItem(deedOwnerId, new WOKItem(Convert.ToInt32(row["global_id"]), row["name"].ToString(), row["description"].ToString(), Convert.ToInt32(row["market_cost"]),
                           Convert.ToInt32(row["selling_price"]), Convert.ToInt32(row["required_skill"]), Convert.ToInt32(row["required_skill_level"]), Convert.ToInt32(row["use_type"]),
                           row["display_name"].ToString(), Convert.ToInt32(row["passTypeId"]),
                           Convert.ToDateTime(row["date_added"]),
                           Convert.ToUInt32(row["item_creator_id"]), Convert.ToInt32(row["item_active"]), Convert.ToInt32(row["arm_anywhere"]),
                           Convert.ToInt32(row["disarmable"]), Convert.ToInt32(row["stackable"]), Convert.ToInt32(row["destroy_when_used"]),
                           Convert.ToInt32(row["inventory_type"]), Convert.ToUInt32(row["base_global_id"]), row["template_path"].ToString(), row["texture_path"].ToString(),
                           row["thumbnail_path"].ToString(), row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_assetdetails_path"].ToString(), Convert.ToUInt32(row["designer_price"]),
                           row["keywords"].ToString(), Convert.ToInt32(row["WebPrice"]), Convert.ToInt32(row["expired_duration"]), Convert.ToUInt32(row["number_sold_on_web"]), Convert.ToUInt32(row["number_of_raves"]),
                           Convert.ToUInt32(row["number_of_views"]), Convert.ToUInt32(row["sales_total"]), Convert.ToUInt32(row["sales_designer_total"]),
                           Convert.ToInt32(row["is_derivable"]), Convert.ToInt32(row["derivation_level"]), Convert.ToUInt32(row["TemplateDesignerPrice"]),
                           Convert.ToUInt32(row["category_id1"]), Convert.ToUInt32(row["category_id2"]), Convert.ToUInt32(row["category_id3"]), Convert.ToInt32(row["actor_group"]), 0, WOKItem.ItemApprovalStatus.Pending
                           ));
                    dItem.Quantity = Convert.ToInt32(row["quantity"]);
                    itemList.Add(dItem);
                }
            }

            return itemList;
        }

        /// <summary>
        /// Retrieves a list of deed items representing animations and particles attached to dynamic objects placed in a Shop world.
        /// </summary>
        public List<DeedItem> GetDOPDeedItems(int deedTemplateId, int deedOwnerId)
        {
            List<DeedItem> itemList = new List<DeedItem>();

            string sql = "SELECT i.global_id, name, description, market_cost, " +
                         "    selling_price, required_skill, required_skill_level, use_type, " +
                         "    iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId, " +
                         "    iw.date_added, item_creator_id, iw.item_active, arm_anywhere, " +
                         "    disarmable, stackable, destroy_when_used, " +
                         "    inventory_type, actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " +
                         "    iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, " +
                         "    iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
                         "    (i.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice, " +
                         "    expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total, " +
                         "   i.is_derivable, i.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice, " +
                         "    iw.category_id1, iw.category_id2, iw.category_id3, " +
                         "    COUNT(*) AS quantity " +
                         "FROM starting_dynamic_object_parameters dop " +
                         "INNER JOIN starting_dynamic_objects do ON do.id = dop.id " +
                         "INNER JOIN items i ON i.global_id = dop.value " +
                         "INNER JOIN shopping.items_web iw ON iw.global_id = i.global_id " +
                         "LEFT OUTER JOIN shopping.items_web iw2 ON iw.base_global_id = iw2.global_id " +
                         "LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id " +
                         "WHERE do.apartment_template_id = @deedTemplateId " +
                         "AND dop.param_type_id in (32, 206) " +
                         "GROUP BY i.global_id " +
                         "ORDER BY NULL";

            //perform query returning multiple data rows from the wok database
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@deedTemplateId", deedTemplateId));

            DataTable dt = Db.WOK.GetDataTable(sql, parameters);

            if (dt != null)
            {
                DeedItem dItem;
                foreach (DataRow row in dt.Rows)
                {
                    dItem = new DeedItem(deedOwnerId, new WOKItem(Convert.ToInt32(row["global_id"]), row["name"].ToString(), row["description"].ToString(), Convert.ToInt32(row["market_cost"]),
                           Convert.ToInt32(row["selling_price"]), Convert.ToInt32(row["required_skill"]), Convert.ToInt32(row["required_skill_level"]), Convert.ToInt32(row["use_type"]),
                           row["display_name"].ToString(), Convert.ToInt32(row["passTypeId"]),
                           Convert.ToDateTime(row["date_added"]),
                           Convert.ToUInt32(row["item_creator_id"]), Convert.ToInt32(row["item_active"]), Convert.ToInt32(row["arm_anywhere"]),
                           Convert.ToInt32(row["disarmable"]), Convert.ToInt32(row["stackable"]), Convert.ToInt32(row["destroy_when_used"]),
                           Convert.ToInt32(row["inventory_type"]), Convert.ToUInt32(row["base_global_id"]), row["template_path"].ToString(), row["texture_path"].ToString(),
                           row["thumbnail_path"].ToString(), row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_assetdetails_path"].ToString(), Convert.ToUInt32(row["designer_price"]),
                           row["keywords"].ToString(), Convert.ToInt32(row["WebPrice"]), Convert.ToInt32(row["expired_duration"]), Convert.ToUInt32(row["number_sold_on_web"]), Convert.ToUInt32(row["number_of_raves"]),
                           Convert.ToUInt32(row["number_of_views"]), Convert.ToUInt32(row["sales_total"]), Convert.ToUInt32(row["sales_designer_total"]),
                           Convert.ToInt32(row["is_derivable"]), Convert.ToInt32(row["derivation_level"]), Convert.ToUInt32(row["TemplateDesignerPrice"]),
                           Convert.ToUInt32(row["category_id1"]), Convert.ToUInt32(row["category_id2"]), Convert.ToUInt32(row["category_id3"]), Convert.ToInt32(row["actor_group"]), 0, WOKItem.ItemApprovalStatus.Pending
                           ));
                    dItem.Quantity = Convert.ToInt32(row["quantity"]);
                    itemList.Add(dItem);
                }
            }

            return itemList;
        }

        /// <summary>
        /// GetDeedsItemBelongsTo
        /// </summary>
        public List<WOKItem> GetDeedsItemBelongsTo(int itemId, string filter, bool onlyPublic = true)
        {
            string itemActive = (onlyPublic ? ((int)WOKItem.ItemActiveStates.Public).ToString() : (int)WOKItem.ItemActiveStates.Public + "," + (int)WOKItem.ItemActiveStates.Private);

            string sql = "SELECT distinct wi.global_id,wi.name,wi.description,wi.market_cost, " +
                " wi.selling_price, wi.required_skill, wi.required_skill_level, wi.use_type, " +
                " iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId, " +
                " iw.date_added, " +
                " wi.item_creator_id, iw.item_active, wi.arm_anywhere, " +
                " wi.disarmable, wi.stackable, wi.destroy_when_used, " +
                " wi.inventory_type, wi.actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " +
                " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
                " (wi.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice, " +
                " wi.expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total, " +
                " wi.is_derivable, wi.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice, " +
                " iw.category_id1, iw.category_id2, iw.category_id3, IFNULL(gi.game_id,0) as game_id, IFNULL(gi.approval_status, 0) as approval_status " +
                " FROM wok.starting_dynamic_objects do " +
                " INNER JOIN shopping.items_web iw ON iw.global_id = do.apartment_template_id  " +
                " INNER JOIN wok.items wi ON wi.global_id = iw.global_id " +
                " LEFT OUTER JOIN shopping.items_web iw2 on iw.base_global_id=iw2.global_id " + // get base item info
                " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = wi.global_id " +
                " LEFT OUTER JOIN wok.game_items gi ON wi.global_id = gi.global_id " +
                " WHERE do.global_id = @itemId " +
                " AND wi.use_type = " + (int)WOKItem.USE_TYPE_CUSTOM_DEED +
                " AND iw.item_active IN (" + itemActive + ") " +
                " UNION " +
                " SELECT distinct wi.global_id,wi.name,wi.description,wi.market_cost, " +
                " wi.selling_price, wi.required_skill, wi.required_skill_level, wi.use_type, " +
                " iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId,  " +
                " iw.date_added,  " +
                " wi.item_creator_id, iw.item_active, wi.arm_anywhere,  " +
                " wi.disarmable, wi.stackable, wi.destroy_when_used,  " +
                " wi.inventory_type, wi.actor_group, iw.base_global_id, iw.template_path, iw.texture_path,  " +
                " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords,  " +
                " (wi.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice,  " +
                " wi.expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total,  " +
                " wi.is_derivable, wi.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice,  " +
                " iw.category_id1, iw.category_id2, iw.category_id3, IFNULL(gi.game_id,0) as game_id, IFNULL(gi.approval_status, 0) as approval_status  " +
                " FROM wok.starting_dynamic_object_parameters dop " +
                " INNER JOIN wok.starting_dynamic_objects do ON dop.id = do.id " +
                " INNER JOIN shopping.items_web iw ON iw.global_id = do.apartment_template_id " +
                " INNER JOIN wok.items wi ON wi.global_id = iw.global_id  " +
                " LEFT OUTER JOIN shopping.items_web iw2 on iw.base_global_id=iw2.global_id  " +
                " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = wi.global_id  " +
                " LEFT OUTER JOIN wok.game_items gi ON wi.global_id = gi.global_id  " +
                " WHERE dop.value = @itemId  " +
                " AND dop.param_type_id in (32, 206)  " +
                " AND wi.use_type = " + (int)WOKItem.USE_TYPE_CUSTOM_DEED +
                " AND iw.item_active IN (" + itemActive + ") " +
                " UNION " +
                " SELECT distinct wi.global_id,wi.name,wi.description,wi.market_cost, " +
                " wi.selling_price, wi.required_skill, wi.required_skill_level, wi.use_type, " +
                " iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId, " +
                " iw.date_added, " +
                " wi.item_creator_id, iw.item_active, wi.arm_anywhere, " +
                " wi.disarmable, wi.stackable, wi.destroy_when_used, " +
                " wi.inventory_type, wi.actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " +
                " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
                " (wi.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice, " +
                " wi.expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total, " +
                " wi.is_derivable, wi.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice, " +
                " iw.category_id1, iw.category_id2, iw.category_id3, IFNULL(gi.game_id,0) as game_id, IFNULL(gi.approval_status, 0) as approval_status " +
                " FROM wok.starting_script_game_items sgi " +
                " INNER JOIN wok.item_parameters ip ON sgi.game_item_glid = ip.global_id " +
	            " AND ip.param_type_id = 34 " +
                " INNER JOIN wok.bundle_items bi ON ip.value = bi.bundle_global_id " +
                " INNER JOIN shopping.items_web iw ON iw.global_id = sgi.template_glid  " +
                " INNER JOIN wok.items wi ON wi.global_id = iw.global_id " +
                " LEFT OUTER JOIN shopping.items_web iw2 on iw.base_global_id=iw2.global_id " + // get base item info
                " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = wi.global_id " +
                " LEFT OUTER JOIN wok.game_items gi ON wi.global_id = gi.global_id " +
                " WHERE bi.item_global_id = @itemId " +
                " AND wi.use_type = " + (int)WOKItem.USE_TYPE_CUSTOM_DEED +
                " AND iw.item_active IN (" + itemActive + ") ";

            if (filter.Length > 0)
            {
                sql += " AND " + filter;
            }

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@itemId", itemId));

            DataTable dt = Db.Shopping.GetDataTable(sql, parameters);

            List<WOKItem> list = new List<WOKItem>();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    list.Add(new WOKItem(Convert.ToInt32(row["global_id"]), row["name"].ToString(), row["description"].ToString(), Convert.ToInt32(row["market_cost"]),
                    Convert.ToInt32(row["selling_price"]), Convert.ToInt32(row["required_skill"]), Convert.ToInt32(row["required_skill_level"]), Convert.ToInt32(row["use_type"]),
                    row["display_name"].ToString(), Convert.ToInt32(row["passTypeId"]),
                    Convert.ToDateTime(row["date_added"]),
                    Convert.ToUInt32(row["item_creator_id"]), Convert.ToInt32(row["item_active"]), Convert.ToInt32(row["arm_anywhere"]),
                    Convert.ToInt32(row["disarmable"]), Convert.ToInt32(row["stackable"]), Convert.ToInt32(row["destroy_when_used"]),
                    Convert.ToInt32(row["inventory_type"]), Convert.ToUInt32(row["base_global_id"]), row["template_path"].ToString(), row["texture_path"].ToString(),
                    row["thumbnail_path"].ToString(), row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_assetdetails_path"].ToString(), Convert.ToUInt32(row["designer_price"]),
                    row["keywords"].ToString(), Convert.ToInt32(row["WebPrice"]), Convert.ToInt32(row["expired_duration"]), Convert.ToUInt32(row["number_sold_on_web"]), Convert.ToUInt32(row["number_of_raves"]),
                    Convert.ToUInt32(row["number_of_views"]), Convert.ToUInt32(row["sales_total"]), Convert.ToUInt32(row["sales_designer_total"]),
                    Convert.ToInt32(row["is_derivable"]), Convert.ToInt32(row["derivation_level"]), Convert.ToUInt32(row["TemplateDesignerPrice"]),
                    Convert.ToUInt32(row["category_id1"]), Convert.ToUInt32(row["category_id2"]), Convert.ToUInt32(row["category_id3"]), Convert.ToInt32(row["actor_group"])
                    ));
                }
            }
            return list;

        }

        /// <summary>
        /// Checks for derived items included in deeds.
        /// </summary>
        public List<WOKItem> GetDerivedItemsIncludedInDeeds(int globalId)
        {
            List<WOKItem> list = new List<WOKItem>();

            // Get items from starting dynamic objects first
            string sql = "SELECT DISTINCT(i.global_id),name,description,market_cost, " +
                            " selling_price, required_skill, required_skill_level, use_type, " +
                            " iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId, " +
                            " iw.date_added, " +
                            " item_creator_id, iw.item_active, arm_anywhere, " +
                            " disarmable, stackable, destroy_when_used, " +
                            " inventory_type, actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " +
                            " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
                            " (i.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice, " +
                            " expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total, " +
                            " i.is_derivable, i.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice, " +
                            " iw.category_id1, iw.category_id2, iw.category_id3, IFNULL(gi.game_id,0) as game_id, IFNULL(gi.approval_status, 0) as approval_status " +
                        " FROM wok.items i " +
                        " INNER JOIN shopping.items_web iw ON i.global_id = iw.global_id " +
                        " INNER JOIN wok.starting_dynamic_objects sdo ON sdo.global_id = i.global_id " +
                        " LEFT OUTER JOIN shopping.items_web iw2 on iw.base_global_id=iw2.global_id " +
                        " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id " +
                        " LEFT OUTER JOIN wok.game_items gi ON i.global_id = gi.global_id " +
                        " WHERE i.base_global_id = @globalId " +
                            " AND i.global_id <> @globalId " +
                        " UNION " +
                        " SELECT DISTINCT(i.global_id),name,description,market_cost, " +
                            " selling_price, required_skill, required_skill_level, use_type, " +
                            " iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId,  " +
                            " iw.date_added,  " +
                            " item_creator_id, iw.item_active, arm_anywhere,  " +
                            " disarmable, stackable, destroy_when_used,  " +
                            " inventory_type, actor_group, iw.base_global_id, iw.template_path, iw.texture_path,  " +
                            " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords,  " +
                            " (i.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice,  " +
                            " expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total,  " +
                            " i.is_derivable, i.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice,  " +
                            " iw.category_id1, iw.category_id2, iw.category_id3, IFNULL(gi.game_id,0) as game_id, IFNULL(gi.approval_status, 0) as approval_status  " +
                        " FROM wok.items i  " +
                        " INNER JOIN shopping.items_web iw ON i.global_id = iw.global_id  " +
                        " INNER JOIN wok.starting_dynamic_object_parameters sdop ON sdop.value = i.global_id  " +
                            " AND sdop.param_type_id IN (32, 206) " +
                        " LEFT OUTER JOIN shopping.items_web iw2 on iw.base_global_id=iw2.global_id  " +
                        " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id  " +
                        " LEFT OUTER JOIN wok.game_items gi ON i.global_id = gi.global_id  " +
                        " WHERE i.base_global_id = @globalId " +
                            " AND i.global_id <> @globalId " +
                        " UNION " +
                        " SELECT DISTINCT(i.global_id),i.name,i.description,i.market_cost,   " +
                        " i.selling_price, i.required_skill, i.required_skill_level, i.use_type,   " +
                        " iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId,   " +
                        " iw.date_added,   " +
                        " i.item_creator_id, iw.item_active, i.arm_anywhere,   " +
                        " i.disarmable, i.stackable, i.destroy_when_used,   " +
                        " i.inventory_type, i.actor_group, iw.base_global_id, iw.template_path, iw.texture_path,   " +
                        " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords,   " +
                        " (i.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice,   " +
                        " i.expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total,   " +
                        " i.is_derivable, i.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice,   " +
                        " iw.category_id1, iw.category_id2, iw.category_id3, IFNULL(gi.game_id,0) as game_id, IFNULL(gi.approval_status, 0) as approval_status   " +
                        " FROM wok.items i   " +
                        " INNER JOIN shopping.items_web iw ON i.global_id = iw.global_id " +
                        " INNER JOIN wok.bundle_items bi ON bi.item_global_id = i.global_id  " + 
                        " INNER JOIN wok.item_parameters ip ON bi.bundle_global_id = ip.value " +
	                    "   AND ip.param_type_id = 34 " +
                        " INNER JOIN wok.items gi1 ON gi1.global_id = ip.global_id " +
                        " INNER JOIN wok.starting_script_game_items sgi ON sgi.game_item_glid = gi1.global_id   " +
                        " LEFT OUTER JOIN shopping.items_web iw2 on iw.base_global_id=iw2.global_id   " +
                        " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id   " +
                        " LEFT OUTER JOIN wok.game_items gi ON i.global_id = gi.global_id   " +
                        " WHERE i.base_global_id = @globalId   " +
                        " AND i.global_id <> @globalId ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@globalId", globalId));

            DataTable dt = Db.Shopping.GetDataTable(sql, parameters);

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    list.Add(new WOKItem(Convert.ToInt32(row["global_id"]), row["name"].ToString(), row["description"].ToString(), Convert.ToInt32(row["market_cost"]),
                    Convert.ToInt32(row["selling_price"]), Convert.ToInt32(row["required_skill"]), Convert.ToInt32(row["required_skill_level"]), Convert.ToInt32(row["use_type"]),
                    row["display_name"].ToString(), Convert.ToInt32(row["passTypeId"]),
                    Convert.ToDateTime(row["date_added"]),
                    Convert.ToUInt32(row["item_creator_id"]), Convert.ToInt32(row["item_active"]), Convert.ToInt32(row["arm_anywhere"]),
                    Convert.ToInt32(row["disarmable"]), Convert.ToInt32(row["stackable"]), Convert.ToInt32(row["destroy_when_used"]),
                    Convert.ToInt32(row["inventory_type"]), Convert.ToUInt32(row["base_global_id"]), row["template_path"].ToString(), row["texture_path"].ToString(),
                    row["thumbnail_path"].ToString(), row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_assetdetails_path"].ToString(), Convert.ToUInt32(row["designer_price"]),
                    row["keywords"].ToString(), Convert.ToInt32(row["WebPrice"]), Convert.ToInt32(row["expired_duration"]), Convert.ToUInt32(row["number_sold_on_web"]), Convert.ToUInt32(row["number_of_raves"]),
                    Convert.ToUInt32(row["number_of_views"]), Convert.ToUInt32(row["sales_total"]), Convert.ToUInt32(row["sales_designer_total"]),
                    Convert.ToInt32(row["is_derivable"]), Convert.ToInt32(row["derivation_level"]), Convert.ToUInt32(row["TemplateDesignerPrice"]),
                    Convert.ToUInt32(row["category_id1"]), Convert.ToUInt32(row["category_id2"]), Convert.ToUInt32(row["category_id3"]), Convert.ToInt32(row["actor_group"])
                    ));
                }
            }
            return list;
        }

        /// <summary>
        /// AddWOPSettingsToCustomDeed
        /// </summary>
        /// <returns>False if an error occurred during processing, True otherwise.</returns>
        public bool AddWOPSettingsToCustomDeed(int zoneIndex, int zoneInstanceId, int templateGlobalId)
        {
            bool success = false;

            DataTable dtWOPSettings = GetSpaceWorldObjectSettings(0, zoneIndex, zoneInstanceId);

            if (dtWOPSettings == null || dtWOPSettings.Rows.Count == 0)
            {
                return true;
            }

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    StringBuilder queryBuilder = new StringBuilder("INSERT INTO starting_world_object_player_settings (active, zone_index, world_object_id, " +
                                                                   "asset_id, texture_url, apartment_template_id) VALUES ");

                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@templateGlobalId", templateGlobalId));

                    // Add insert for each dynamic object parameter.
                    for (int i = 0; i < dtWOPSettings.Rows.Count; i++)
                    {
                        queryBuilder.Append("(@active" + i + ", @zone_index" + i + ", @world_object_id" + i + ", @asset_id" + i + ", @texture_url" + i + ", @templateGlobalId), ");

                        DataRow row = dtWOPSettings.Rows[i];
                        parameters.Add(new MySqlParameter("@active" + i, 1));
                        parameters.Add(new MySqlParameter("@zone_index" + i, row["zone_index"]));
                        parameters.Add(new MySqlParameter("@world_object_id" + i, row["world_object_id"]));
                        parameters.Add(new MySqlParameter("@asset_id" + i, row["asset_id"]));
                        parameters.Add(new MySqlParameter("@texture_url" + i, row["texture_url"]));
                    }
                    queryBuilder.Remove(queryBuilder.Length - 2, 2);

                    Db.WOK.ExecuteNonQuery(queryBuilder.ToString(), parameters, true);

                    transaction.Complete();
                    success = true;
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLShoppingDao.AddWOPSettingsToCustomDeed() - Transaction aborted: " + e.ToString());
            }

            return success;
        }

        /// <summary>
        /// AddWorldDOsToCustomDeed
        /// </summary>
        /// <returns>False if an error occurred during processing, True otherwise.</returns>
        public bool AddWorldDOsToCustomDeed(int zoneIndex, int zoneInstanceId, int templateGlobalId)
        {
            bool success = false;

            try
            {
                string sql = "CALL snapshot_zone_dynamic_objects (@zoneIndex, @zoneInstanceId, @templateGlobalId)";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@zoneIndex", zoneIndex));
                parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
                parameters.Add(new MySqlParameter("@templateGlobalId", templateGlobalId));

                Db.WOK.ExecuteNonQuery(sql, parameters, true);

                success = true;
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLShoppingDao.AddWorldDOsToCustomDeed() - Transaction aborted: " + e.ToString());
            }

            return success;
        }

        public int DeleteAllDOsFromCustomDeed(int deedTemplateId)
        {
            int deletions = 0;
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    // Delete template dynamic objects and associated playlists with this glid
                    string sqlDeedDOs = "DELETE starting_dynamic_objects.*, " +
                                        "    starting_dynamic_object_parameters.*, " +
                                        "    starting_dynamic_object_playlists.* " +
                                        "FROM starting_dynamic_objects " +
                                        "LEFT JOIN starting_dynamic_object_parameters " +
                                        "    ON starting_dynamic_object_parameters.id = starting_dynamic_objects.id " +
                                        "LEFT JOIN starting_dynamic_object_playlists " +
                                        "    ON starting_dynamic_object_playlists.id = starting_dynamic_objects.id " +
                                        "WHERE starting_dynamic_objects.apartment_template_id = @deedTemplateId ";

                    parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@deedTemplateId", deedTemplateId));
                    deletions += Db.WOK.ExecuteNonQuery(sqlDeedDOs, parameters, true);

                    // If we reach this point, the transaction was successful
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                deletions = 0;
                m_logger.Error("MySQLShoppingDao.DeleteAllDOsFromCustomDeed() - Transaction aborted: " + e.ToString());
            }

            return deletions;
        }

        public int DeleteAllWOPSettingsFromCustomDeed(int deedTemplateId)
        {
            int deletions = 0;
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    // Delete template dynamic objects and associated playlists with this glid
                    string sqlDeedWOPs = "DELETE FROM starting_world_object_player_settings WHERE apartment_template_id = @deedTemplateId ";

                    parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@deedTemplateId", deedTemplateId));
                    deletions += Db.WOK.ExecuteNonQuery(sqlDeedWOPs, parameters, true);

                    // If we reach this point, the transaction was successful
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                deletions = 0;
                m_logger.Error("MySQLShoppingDao.DeleteAllDOsFromCustomDeed() - Transaction aborted: " + e.ToString());
            }

            return deletions;
        }

        /// <summary>
        /// Called to remove a list of items from a deed on shop.
        /// </summary>
        public int RemoveItemsFromDeed(int deedTemplateId, List<int> itemGlobalIds)
        {
            if (itemGlobalIds == null || itemGlobalIds.Count == 0)
            {
                return 0;
            }

            int deletions = 0;
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            // Grab the associated "Try On" zone
            WOK3DPlace tryOnZone = GetDeedTryOnZone(deedTemplateId);

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    // Delete template dynamic objects and associated playlists with this glid
                    string sqlDeedDOs = "DELETE starting_dynamic_objects.*, " +
                                        "    starting_dynamic_object_parameters.*, " +
                                        "    starting_dynamic_object_playlists.* " +
                                        "FROM starting_dynamic_objects " +
                                        "LEFT JOIN starting_dynamic_object_parameters " +
                                        "    ON starting_dynamic_object_parameters.id = starting_dynamic_objects.id " +
                                        "LEFT JOIN starting_dynamic_object_playlists " +
                                        "    ON starting_dynamic_object_playlists.id = starting_dynamic_objects.id " +
                                        "WHERE starting_dynamic_objects.apartment_template_id = @deedTemplateId " +
                                        "    AND starting_dynamic_objects.global_id IN (" + string.Join(",", itemGlobalIds) + ")";

                    parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@deedTemplateId", deedTemplateId));
                    deletions += Db.WOK.ExecuteNonQuery(sqlDeedDOs, parameters, true);

                    // Delete template dynamic object parameters
                    string sqlDeedDOParams = "DELETE starting_dynamic_object_parameters.* " +
                                             "FROM starting_dynamic_objects " +
                                             "INNER JOIN starting_dynamic_object_parameters " +
                                             "    ON starting_dynamic_object_parameters.id = starting_dynamic_objects.id " +
                                             "    AND starting_dynamic_object_parameters.param_type_id IN (32, 206) " +
                                             "    AND starting_dynamic_object_parameters.value IN (" + string.Join(",", itemGlobalIds) + ") " +
                                             "WHERE starting_dynamic_objects.apartment_template_id = @deedTemplateId ";

                    parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@deedTemplateId", deedTemplateId));
                    deletions += Db.WOK.ExecuteNonQuery(sqlDeedDOParams, parameters, true);

                    if (tryOnZone.ChannelZoneId > 0)
                    {
                        // Delete "Try On" zone's playlists
                        string sqlTryOnPlaylists = "DELETE dynamic_object_playlists.* " +
                                                   "FROM dynamic_object_playlists " +
                                                   "WHERE zone_index = @zoneIndex " +
                                                   "    AND zone_instance_id = @zoneInstanceId " +
                                                   "    AND global_id IN (" + string.Join(",", itemGlobalIds) + ")";

                        parameters = new List<IDbDataParameter>();
                        parameters.Add(new MySqlParameter("@zoneIndex", tryOnZone.ZoneIndex));
                        parameters.Add(new MySqlParameter("@zoneInstanceId", tryOnZone.ZoneInstanceId));
                        deletions += Db.WOK.ExecuteNonQuery(sqlTryOnPlaylists, parameters, true);

                        // Delete "Try On" zone's dynamic objects
                        string sqlTryOnDOs = "DELETE dynamic_objects.*, " +
                                             "    dynamic_object_parameters.* " +
                                             "FROM dynamic_objects " +
                                             "LEFT JOIN dynamic_object_parameters " +
                                             "    ON dynamic_object_parameters.obj_placement_id = dynamic_objects.obj_placement_id " +
                                             "WHERE dynamic_objects.zone_index = @zoneIndex " +
                                             "    AND dynamic_objects.zone_instance_id = @zoneInstanceId " +
                                             "    AND dynamic_objects.global_id IN (" + string.Join(",", itemGlobalIds) + ")";

                        parameters = new List<IDbDataParameter>();
                        parameters.Add(new MySqlParameter("@zoneIndex", tryOnZone.ZoneIndex));
                        parameters.Add(new MySqlParameter("@zoneInstanceId", tryOnZone.ZoneInstanceId));
                        deletions += Db.WOK.ExecuteNonQuery(sqlTryOnDOs, parameters, true);

                        // Delete "Try On" zone's dynamic object parameters
                        string sqlTryOnDOParams = "DELETE dynamic_object_parameters.* " +
                                                  "FROM dynamic_objects " +
                                                  "INNER JOIN dynamic_object_parameters " +
                                                  "    ON dynamic_object_parameters.obj_placement_id = dynamic_objects.obj_placement_id " +
                                                  "    AND dynamic_object_parameters.param_type_id IN (32, 206) " +
                                                  "    AND dynamic_object_parameters.value IN (" + string.Join(",", itemGlobalIds) + ") " +
                                                  "WHERE dynamic_objects.zone_index = @zoneIndex " +
                                                  "    AND dynamic_objects.zone_instance_id = @zoneInstanceId";

                        parameters = new List<IDbDataParameter>();
                        parameters.Add(new MySqlParameter("@zoneIndex", tryOnZone.ZoneIndex));
                        parameters.Add(new MySqlParameter("@zoneInstanceId", tryOnZone.ZoneInstanceId));
                        deletions += Db.WOK.ExecuteNonQuery(sqlTryOnDOParams, parameters, true);
                    }

                    // If we reach this point, the transaction was successful
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                deletions = 0;
                m_logger.Error("MySQLShoppingDao.RemoveItemsFromDeed() - Transaction aborted: " + e.ToString());
            }

            return deletions;
        }

        /// <summary>
        /// Gets the usage of a custom deed
        /// </summary>
        /// <returns></returns>
        public DataRow GetCustomDeedUsage(int deedId)
        {
            string sql = "SELECT cz.* " +
                        " FROM wok.custom_deed_usage cdu, wok.channel_zones cz " +
                        " WHERE cdu.apartment_template_id = @deedId " +
                        " AND cdu.zone_index = cz.zone_index " +
                        " AND cdu.zone_instance_id = cz.zone_instance_id " +
                        " AND cz.kaneva_user_id = 1";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@deedId", deedId));

            return Db.WOK.GetDataRow(sql, parameters);
        }

        /// <summary>
        /// CanZoneBeCustomDeed
        /// </summary>
        /// <returns>True if the zone is not based on an existing custom deed, and is thus eligible for shop upload.</returns>
        public bool IsOriginalUGCZone(int zoneIndex, int zoneInstanceId)
        {
            string sql = "SELECT cdu.*, IF(wtg.template_id IS NULL, 'F', 'T') AS is_world_template " +
                        " FROM wok.custom_deed_usage cdu " +
                        " LEFT OUTER JOIN wok.world_template_global_id wtg ON wtg.global_id = cdu.apartment_template_id " +
                        " WHERE cdu.zone_index = @zoneIndex " +
                        " AND cdu.zone_instance_id = @zoneInstanceId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneIndex", zoneIndex));
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));

            DataRow drResult = Db.WOK.GetDataRow(sql, parameters);

            return (drResult == null || drResult["is_world_template"].ToString() == "T");
        }

        /// <summary>
        /// Gets the "Try On" zone for a deed
        /// </summary>
        public WOK3DPlace GetDeedTryOnZone(int deedTemplateId)
        {
            // Join cz to get admin record, which ensures it is the try on deed
            string sql = "SELECT cz.channel_zone_id, cz.kaneva_user_id, cz.zone_index, cz.zone_instance_id, " +
                         "  cz.zone_type, cz.name, cz.raves, cz.server_id, cz.tied_to_server " +
                         "FROM custom_deed_usage cdu " +
                         "INNER JOIN channel_zones cz ON cdu.zone_index = cz.zone_index " +
                         "  AND cdu.zone_instance_id = cz.zone_instance_id " +
                         "WHERE cz.kaneva_user_id = 1 " +
                         "  AND cdu.apartment_template_id = @deedTemplateId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@deedTemplateId", deedTemplateId));

            DataRow row = Db.WOK.GetDataRow(sql, parameters);

            if (row == null)
            {
                return new WOK3DPlace();
            }

            return new WOK3DPlace(Convert.ToInt32(row["channel_zone_id"]), Convert.ToInt32(row["zone_index"]), Convert.ToInt32(row["zone_instance_id"]),
                Convert.ToInt32(row["zone_type"]), Convert.ToInt32(row["kaneva_user_id"]),
                row["name"].ToString(), Convert.ToInt32(row["raves"]), Convert.ToInt32(row["server_id"]), row["tied_to_server"].ToString(), -1, 0);
        }

        /// <summary>
        /// Called to get the community_id of a try on zone community
        /// </summary>
        public int GetCommunityIdFromDeedTemplateId(int deedTemplateId)
        {
            WOK3DPlace tryOnZone = GetDeedTryOnZone(deedTemplateId);

            // Try-On zones link community_id to zone_instance_id
            return tryOnZone.ZoneInstanceId;
        }

        #endregion


        public List<WOKItem> GetItemAnimations (int globalId, string filter, bool showPrivate)
        {
            string sql = "SELECT i.global_id,name,description,market_cost, " +
                " selling_price, required_skill, required_skill_level, use_type, " +
                " iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId, " +
                " iw.date_added, " +
                " item_creator_id, iw.item_active, arm_anywhere, " +
                " disarmable, stackable, destroy_when_used, " +
                " inventory_type, actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " +
                " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
                " (i.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice, " +
                " expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total, " +
                " i.is_derivable, i.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice, " +
                " iw.category_id1, iw.category_id2, iw.category_id3, IFNULL(gi.game_id,0) as game_id, IFNULL(gi.approval_status, 0) as approval_status " +
                " FROM wok.item_animations ia " +
                " INNER JOIN wok.items i ON i.global_id = ia.animation_id " +
                " INNER JOIN shopping.items_web iw ON i.global_id = iw.global_id " +
                " LEFT OUTER JOIN shopping.items_web iw2 on iw.base_global_id=iw2.global_id " + // get base item info
                " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id " +
                " LEFT OUTER JOIN wok.game_items gi ON i.global_id = gi.global_id " +
                " WHERE ia.item_id = @globalId ";

            if (!showPrivate)
            {
                sql += " AND iw.item_active = " + (int) WOKItem.ItemActiveStates.Public;
            }
            else
            {
                sql += " AND iw.item_active <> " + (int) WOKItem.ItemActiveStates.Deleted;
            }

            if (filter.Length > 0)
            {
                sql += " AND " + filter;
            }

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));
            
            DataTable dt = Db.Search.GetDataTable (sql, parameters);

            List<WOKItem> list = new List<WOKItem> ();

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new WOKItem (Convert.ToInt32 (row["global_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToInt32 (row["market_cost"]),
                Convert.ToInt32 (row["selling_price"]), Convert.ToInt32 (row["required_skill"]), Convert.ToInt32 (row["required_skill_level"]), Convert.ToInt32 (row["use_type"]),
                row["display_name"].ToString (), Convert.ToInt32 (row["passTypeId"]),
                Convert.ToDateTime (row["date_added"]),
                Convert.ToUInt32 (row["item_creator_id"]), Convert.ToInt32 (row["item_active"]), Convert.ToInt32 (row["arm_anywhere"]),
                Convert.ToInt32 (row["disarmable"]), Convert.ToInt32 (row["stackable"]), Convert.ToInt32 (row["destroy_when_used"]),
                Convert.ToInt32 (row["inventory_type"]), Convert.ToUInt32 (row["base_global_id"]), row["template_path"].ToString (), row["texture_path"].ToString (),
                row["thumbnail_path"].ToString (), row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (), row["thumbnail_assetdetails_path"].ToString (), Convert.ToUInt32 (row["designer_price"]),
                row["keywords"].ToString (), Convert.ToInt32 (row["WebPrice"]), Convert.ToInt32 (row["expired_duration"]), Convert.ToUInt32 (row["number_sold_on_web"]), Convert.ToUInt32 (row["number_of_raves"]),
                Convert.ToUInt32 (row["number_of_views"]), Convert.ToUInt32 (row["sales_total"]), Convert.ToUInt32 (row["sales_designer_total"]),
                Convert.ToInt32 (row["is_derivable"]), Convert.ToInt32 (row["derivation_level"]), Convert.ToUInt32 (row["TemplateDesignerPrice"]),
                Convert.ToUInt32 (row["category_id1"]), Convert.ToUInt32 (row["category_id2"]), Convert.ToUInt32 (row["category_id3"]), Convert.ToInt32 (row["actor_group"])
                ));
            }
            return list;
        }

        public WOKItem GetAnimationActor (int globalId)
        {
            //create query string to get game by user id
            string sql = "SELECT i.global_id,name,description,market_cost, " +
                " selling_price, required_skill, required_skill_level, use_type, " +
                " iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId, " +
                " iw.date_added, " +
                " item_creator_id, iw.item_active, arm_anywhere, " +
                " disarmable, stackable, destroy_when_used, " +
                " inventory_type, actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " +
                " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
                " (i.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice, " +
                " expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total, " +
                " i.is_derivable, i.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice, " +
                " iw.category_id1, iw.category_id2, iw.category_id3, IFNULL(gi.game_id,0) as game_id, IFNULL(gi.approval_status, 0) as approval_status " +
                " FROM items i " +
                " INNER JOIN wok.item_animations ia ON i.global_id = ia.item_id " +
                " INNER JOIN shopping.items_web iw ON i.global_id = iw.global_id " +
                " LEFT OUTER JOIN shopping.items_web iw2 on iw.base_global_id=iw2.global_id " + // get base item info
                " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id " +
                " LEFT OUTER JOIN wok.game_items gi ON i.global_id = gi.global_id " +
                " WHERE ia.animation_id = @globalId ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));

            //perform query returning single data row from the wok database
            DataRow row = Db.WOK.GetDataRow (sql.ToString (), parameters);
            if (row == null)
                return new WOKItem();

            return new WOKItem (Convert.ToInt32 (row["global_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToInt32 (row["market_cost"]),
                   Convert.ToInt32 (row["selling_price"]), Convert.ToInt32 (row["required_skill"]), Convert.ToInt32 (row["required_skill_level"]), Convert.ToInt32 (row["use_type"]),
                   row["display_name"].ToString (), Convert.ToInt32 (row["passTypeId"]),
                   Convert.ToDateTime (row["date_added"]),
                   Convert.ToUInt32 (row["item_creator_id"]), Convert.ToInt32 (row["item_active"]), Convert.ToInt32 (row["arm_anywhere"]),
                   Convert.ToInt32 (row["disarmable"]), Convert.ToInt32 (row["stackable"]), Convert.ToInt32 (row["destroy_when_used"]),
                   Convert.ToInt32 (row["inventory_type"]), Convert.ToUInt32 (row["base_global_id"]), row["template_path"].ToString (), row["texture_path"].ToString (),
                   row["thumbnail_path"].ToString (), row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (), row["thumbnail_assetdetails_path"].ToString (), Convert.ToUInt32 (row["designer_price"]),
                   row["keywords"].ToString (), Convert.ToInt32 (row["WebPrice"]), Convert.ToInt32 (row["expired_duration"]), Convert.ToUInt32 (row["number_sold_on_web"]), Convert.ToUInt32 (row["number_of_raves"]),
                   Convert.ToUInt32 (row["number_of_views"]), Convert.ToUInt32 (row["sales_total"]), Convert.ToUInt32 (row["sales_designer_total"]),
                   Convert.ToInt32 (row["is_derivable"]), Convert.ToInt32 (row["derivation_level"]), Convert.ToUInt32 (row["TemplateDesignerPrice"]),
                   Convert.ToUInt32 (row["category_id1"]), Convert.ToUInt32 (row["category_id2"]), Convert.ToUInt32 (row["category_id3"]), Convert.ToInt32 (row["actor_group"]), Convert.ToInt32 (row["game_id"]), (WOKItem.ItemApprovalStatus) Convert.ToInt32 (row["approval_status"])
                   );
        }

        /// <summary>
        /// GetOwnerItems
        /// </summary>
        public PagedList<WOKItem> GetOwnerItems (string searchString, int ownerId, UInt32 itemCategoryId, int pageSize, int pageNumber, string orderBy)
        {
            return GetOwnerItems (searchString, ownerId, itemCategoryId, pageSize, pageNumber, orderBy, false);
        }

        /// <summary>
        /// GetOwnerItems
        /// </summary>
        public PagedList<WOKItem> GetOwnerItems (string searchString, int ownerId, UInt32 itemCategoryId, int pageSize, int pageNumber, string orderBy, bool bIncludeUGCTemplate)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strSelect = "SELECT i.global_id,name,description,market_cost, " +
                " selling_price, required_skill, required_skill_level, use_type, " +
                " iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId, iw.date_added, " +
                " item_creator_id, iw.item_active, arm_anywhere, " +
                " disarmable, stackable, destroy_when_used, " +
                " inventory_type,actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " +
                " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
                " (i.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice, " +
                " expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total, " +
                " i.is_derivable, i.derivation_level, IFNULL(iw2.designer_price, 0) TemplateDesignerPrice, " +
                " iw.category_id1, iw.category_id2, iw.category_id3 ";

            string strTables = " FROM wok.items i INNER JOIN shopping.items_web iw ON i.global_id = iw.global_id LEFT OUTER JOIN shopping.items_web iw2 ON iw.base_global_id=iw2.global_id " +
                                " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id ";
            string strWhere = " WHERE iw.item_active IN (" + (int) WOKItem.ItemActiveStates.Public + ", " + (int) WOKItem.ItemActiveStates.Private + ")";
            strWhere = strWhere + " AND use_type <> " + WOKItem.USE_TYPE_PREMIUM;

            if (!bIncludeUGCTemplate)
                strWhere = strWhere + " AND wok.isUGCMeshTemplate(i.global_id)=0 ";

            if (ownerId > 0)
            {
                strWhere += " AND item_creator_id = @itemCreatorId  ";
                parameters.Add (new MySqlParameter ("@itemCreatorId", ownerId));
            }

            if (itemCategoryId > 0)
            {
                strWhere += " AND (iw.category_id1 = @itemCategoryId  OR iw.category_id2 = @itemCategoryId OR iw.category_id3 = @itemCategoryId) ";
                parameters.Add (new MySqlParameter ("@itemCategoryId", itemCategoryId));
            }

            if (searchString.Length > 0)
            {
                strWhere += " AND name LIKE @name ";
                parameters.Add (new MySqlParameter ("@name", searchString));
            }

            if (orderBy.Length.Equals (0))
            {
                // Default to newest first
                orderBy = " global_id DESC ";
            }

            PagedDataTable dt = Db.Search.GetPagedDataTable (strSelect + strTables + strWhere, orderBy, parameters, pageNumber, pageSize);

            PagedList<WOKItem> list = new PagedList<WOKItem> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new WOKItem (Convert.ToInt32 (row["global_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToInt32 (row["market_cost"]),
                Convert.ToInt32 (row["selling_price"]), Convert.ToInt32 (row["required_skill"]), Convert.ToInt32 (row["required_skill_level"]), Convert.ToInt32 (row["use_type"]),
                row["display_name"].ToString (), Convert.ToInt32 (row["passTypeId"]),
                Convert.ToDateTime (row["date_added"]),
                Convert.ToUInt32 (row["item_creator_id"]), Convert.ToInt32 (row["item_active"]), Convert.ToInt32 (row["arm_anywhere"]),
                Convert.ToInt32 (row["disarmable"]), Convert.ToInt32 (row["stackable"]), Convert.ToInt32 (row["destroy_when_used"]),
                Convert.ToInt32 (row["inventory_type"]), Convert.ToUInt32 (row["base_global_id"]), row["template_path"].ToString (), row["texture_path"].ToString (),
                row["thumbnail_path"].ToString (), row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (), row["thumbnail_assetdetails_path"].ToString (), Convert.ToUInt32 (row["designer_price"]),
                row["keywords"].ToString (), Convert.ToInt32 (row["WebPrice"]), Convert.ToInt32 (row["expired_duration"]), Convert.ToUInt32 (row["number_sold_on_web"]), Convert.ToUInt32 (row["number_of_raves"]),
                Convert.ToUInt32 (row["number_of_views"]), Convert.ToUInt32 (row["sales_total"]), Convert.ToUInt32 (row["sales_designer_total"]),
                Convert.ToInt32 (row["is_derivable"]), Convert.ToInt32 (row["derivation_level"]), Convert.ToUInt32 (row["TemplateDesignerPrice"]),
                Convert.ToUInt32 (row["category_id1"]), Convert.ToUInt32 (row["category_id2"]), Convert.ToUInt32 (row["category_id3"]), Convert.ToInt32 (row["actor_group"])
                ));
            }
            return list;

        }

        /// <summary>
        /// SearchWOKItems
        /// </summary>
        public PagedList<WOKItem> SearchItems (string searchString, int ownerId, UInt32[] itemCategoryIds, int pageSize, int pageNumber, string orderBy, bool showRestricted, bool onlyAccessPass, int showPrivate, bool onlyAnimated)
        {
            // Support Sphinx search
            string sphinxCategoryPrefix = "cat_e_gory_";
            string newSearchString = "";
            int totalCount = 0;
            string glids = "";

            Sphinx sphinx = new Sphinx ();

            searchString = sphinx.StripSphinxSearchString(searchString);

            using (ConnectionBase connection = new PersistentTcpConnection (sphinx.Host, sphinx.Port))
            {
                connection.ConnectionTimeout = 5000;

                // Format search string for sphinx for OR searching
                if (searchString.Trim ().Length > 0)
                {
                    // Escape some strings sphinx has issues with \()|-!@~/^$*
                    searchString = sphinx.CleanSphinxSearchString (searchString);

                    char[] splitter = { ' ' };
                    string[] arKeywords = null;

                    // Did they enter multiples?
                    arKeywords = searchString.Split (splitter);

                    if (arKeywords.Length > 1)
                    {
                        searchString = "";
                        for (int j = 0; j < arKeywords.Length; j++)
                        {
                            if (j < arKeywords.Length - 1)
                            {
                                searchString += arKeywords[j].ToString () + " | ";
                            }
                            else
                            {
                                searchString += arKeywords[j].ToString ();
                            }
                        }
                    }
                }

                // Jeff R built category into FT index, not filtering
                if (itemCategoryIds[0] > 0)
                {
                    if (searchString.Trim ().Length > 0)
                    {
                        // Category is an AND (@categories means only search categories index 
                        // for the next search term, sphinx extended mode)
                        newSearchString = searchString + " & (@categories " + sphinxCategoryPrefix + itemCategoryIds[0].ToString () + ")";
                    }
                    else
                    {
                        // @categories means only search categories index
 //                       newSearchString = "@categories " + sphinxCategoryPrefix + itemCategoryId.ToString ();
                        newSearchString = "@categories " ;
                        for (int i = 0; i < itemCategoryIds.Length; i++)
                        {
                            if (i > 0) newSearchString += " | ";
                            newSearchString += sphinxCategoryPrefix + itemCategoryIds[i].ToString () + " ";
                        }
                    }
                }
                else
                {
                    newSearchString = searchString;
                }

                SearchQuery searchQuery = new SearchQuery (newSearchString);

                // Set match mode to SPH_MATCH_EXTENDED2
                searchQuery.MatchMode = MatchMode.Extended2;

                // Add Sphinx index name to list
                searchQuery.Indexes.Add ("wokitems_srch_idx");

                //Filtering

                // Active states
                if (showPrivate == 1)
                {
                    searchQuery.AttributeFilters.Add ("item_active", (int) WOKItem.ItemActiveStates.Private, false);
                }
                else if (showPrivate == 2)
                {
                    List<int> ilist = new List<int> (2);
                    ilist.Add ((int) WOKItem.ItemActiveStates.Private);
                    ilist.Add ((int) WOKItem.ItemActiveStates.Public);
                    searchQuery.AttributeFilters.Add ("item_active", ilist, false);
                }
                else
                {
                    // Only public
                    searchQuery.AttributeFilters.Add ("item_active", (int) WOKItem.ItemActiveStates.Public, false);
                }

                // Filter by owner
                if (ownerId > 0)
                {
                    searchQuery.AttributeFilters.Add ("item_creator_id", ownerId, false);
                }
                else if (ownerId.Equals (-1))
                {
                    // Don't show kanenva, only UGC (item_creator_id > 0)
                    searchQuery.AttributeFilters.Add ("item_creator_id", 1, UInt32.MaxValue, false);
                }

                // Don't show AP if not appropriate
                if (!showRestricted)
                {
                    searchQuery.AttributeFilters.Add ("pass_group_id", 0, false);
                }

                // Show AP and Only want AP
                if (showRestricted && onlyAccessPass)
                {
                    // pass_group_id > 0
                    searchQuery.AttributeFilters.Add ("pass_group_id", 1, UInt32.MaxValue, false);
                }

                // Show only items with animations
                if (onlyAnimated)
                {
                    // ia.item_id > 0
                    searchQuery.AttributeFilters.Add ("has_animation", 1, UInt32.MaxValue, false);
                }

                // Sorting
                if (orderBy.Trim ().Length.Equals (0))
                {
                    searchQuery.SortMode = ResultsSortMode.Relevance;
                }
                else
                {
                    // Handle extended mode
                    if (orderBy.Contains(","))
                    {
                        searchQuery.SortMode = ResultsSortMode.Extended;
                        searchQuery.SortBy = orderBy;
                    }
                    else
                    {
                        if (orderBy.ToUpper().Contains("ASC"))
                        {
                            searchQuery.SortMode = ResultsSortMode.AttributeAscending;
                        }
                        else
                        {
                            searchQuery.SortMode = ResultsSortMode.AttributeDescending;
                        }

                        searchQuery.SortBy = orderBy.Replace("ASC", "").Replace("DESC", "").Trim();
                    }
                }

                // Set up paging
                searchQuery.Limit = pageSize;
                searchQuery.Offset = (pageNumber - 1) * pageSize;

                // Create search command object
                SearchCommand searchCommand = new SearchCommand (connection);

                // Add newly created search query object to query list
                searchCommand.QueryList.Add (searchQuery);

                // Execute command on server and obtain results
                try
                {
                    searchCommand.Execute ();
                }
                catch (Exception exc)
                {
                    m_logger.Error ("Error calling Sphinx " + sphinx.Host + ":" + sphinx.Port, exc);
                }

                // Build result IN clause for MySQL
                foreach (SearchQueryResult result in searchCommand.Result.QueryResults)
                {
                    foreach (Match match in result.Matches)
                    {
                        if (glids.Length > 0)
                        {
                            glids = glids + "," + match.DocumentId.ToString ();
                        }
                        else
                        {
                            glids = match.DocumentId.ToString ();
                        }
                    }

                    // Note this will grab the last total count in collection, ok for this case, but not best place to do this
                    totalCount = (result.TotalFound > searchQuery.MaxMatches ? searchQuery.MaxMatches : result.TotalFound);
                }
            }

            // Nothing found in Sphinx
            if (glids.Length.Equals (0))
            {
                PagedList<WOKItem> emptyList = new PagedList<WOKItem> ();
                emptyList.TotalCount = 0;
                return emptyList;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strSelect = "SELECT DISTINCT(i.global_id) as global_id,name,i.description,market_cost, " +
                " selling_price, use_type, " +
                " iw.display_name, COALESCE(pass_group_id,0) as passTypeId, iw.date_added, " +
                " item_creator_id, iw.item_active, " +
                " inventory_type, i.base_global_id, iw.template_path, iw.texture_path, " +
                " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
                " (i.market_cost + iw.designer_price + COALESCE(iw2.designer_price,0)) as WebPrice, " +
                " iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, username as owner_username, " +
                " COALESCE(iw2.designer_price,0) as TemplateDesignerPrice, " +
                " iw.category_id1, iw.category_id2, iw.category_id3, COALESCE(ia.item_id,0) as has_animation " +
                " FROM wok.items i " +
                " INNER JOIN shopping.items_web iw ON i.global_id = iw.global_id " +
                " LEFT OUTER JOIN shopping.items_web iw2 ON iw.base_global_id = iw2.global_id " +
                " LEFT JOIN wok.item_parameters ip ON i.global_id = ip.global_id AND i.use_type = ip.param_type_id AND use_type <= 13 " +
                " LEFT OUTER JOIN kaneva.users u ON i.item_creator_id = u.user_id " +
                " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id AND pgi.pass_group_id = 1 " +
                " LEFT OUTER JOIN wok.item_animations ia ON ia.item_id = i.global_id " +
                " where i.global_id in " +
                " (" + glids + ")" +
                " ORDER BY FIELD(i.global_id," + glids + ")";

            DataTable dt = Db.Search.GetDataTable (strSelect, parameters);

            PagedList<WOKItem> list = new PagedList<WOKItem> ();
            list.TotalCount = Convert.ToUInt32 (totalCount);

            foreach (DataRow row in dt.Rows)
            {
                WOKItem wokItem = new WOKItem (Convert.ToInt32 (row["global_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToInt32 (row["market_cost"]),
                Convert.ToInt32 (row["selling_price"]), 0, 0, Convert.ToInt32 (row["use_type"]),
                row["display_name"].ToString (), Convert.ToInt32 (row["passTypeId"]),
                Convert.ToDateTime (row["date_added"]), Convert.ToUInt32 (row["item_creator_id"]), Convert.ToInt32 (row["item_active"]), 0,
                0, 0, 0,
                Convert.ToInt32 (row["inventory_type"]), Convert.ToUInt32 (row["base_global_id"]), row["template_path"].ToString (), row["texture_path"].ToString (),
                row["thumbnail_path"].ToString (), row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (), row["thumbnail_assetdetails_path"].ToString (), Convert.ToUInt32 (row["designer_price"]),
                row["keywords"].ToString (), Convert.ToInt32 (row["WebPrice"]), 0, Convert.ToUInt32 (row["number_sold_on_web"]), Convert.ToUInt32 (row["number_of_raves"]),
                Convert.ToUInt32 (row["number_of_views"]), 0, 0, 0, WOKItem.DRL_UNKNOWN, Convert.ToUInt32 (row["TemplateDesignerPrice"]),
                Convert.ToUInt32 (row["category_id1"]), Convert.ToUInt32 (row["category_id2"]), Convert.ToUInt32 (row["category_id3"]),
                0, //actor_group is not in swi so give it the default value.
                Convert.ToBoolean (row["has_animation"])
                );

                if (wokItem.UseType == WOKItem.USE_TYPE_BUNDLE)
                {
                    wokItem.BundleWebPrice = Convert.ToInt32 (row["market_cost"]);
                }
                
                list.Add (wokItem);
            }
            return list;
        }

        public List<WOKItem> GetBundlesItemBelongsTo (int itemId, string filter, bool onlyPublic = true)
        {
            string itemActive = (onlyPublic ? ((int)WOKItem.ItemActiveStates.Public).ToString() : (int)WOKItem.ItemActiveStates.Public + "," + (int)WOKItem.ItemActiveStates.Private);

            string sql = "SELECT i.global_id,name,description,market_cost, " +
                " selling_price, required_skill, required_skill_level, use_type, " +
                " iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId, " +
                " iw.date_added, " +
                " item_creator_id, iw.item_active, arm_anywhere, " +
                " disarmable, stackable, destroy_when_used, " +
                " inventory_type, actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " +
                " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
                " (i.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice, " +
                " expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total, " +
                " i.is_derivable, i.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice, " +
                " iw.category_id1, iw.category_id2, iw.category_id3, IFNULL(gi.game_id,0) as game_id, IFNULL(gi.approval_status, 0) as approval_status, " +
                " bi.quantity " +
                " FROM shopping.items_web iw INNER JOIN wok.items i ON i.global_id = iw.global_id " +
                " INNER JOIN wok.bundle_items bi ON iw.global_id = bi.bundle_global_id " +
                " LEFT OUTER JOIN shopping.items_web iw2 on iw.base_global_id=iw2.global_id " + // get base item info
                " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id " +
                " LEFT OUTER JOIN wok.game_items gi ON i.global_id = gi.global_id " +
                " WHERE bi.item_global_id = @itemId " +
                " AND iw.item_active IN (" + itemActive + ") ";

            if (filter.Length > 0)
            {
                sql += " AND " + filter;
            }

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@itemId", itemId));

            DataTable dt = Db.Shopping.GetDataTable (sql, parameters);

            List<WOKItem> list = new List<WOKItem> ();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    list.Add (new WOKItem (Convert.ToInt32 (row["global_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToInt32 (row["market_cost"]),
                    Convert.ToInt32 (row["selling_price"]), Convert.ToInt32 (row["required_skill"]), Convert.ToInt32 (row["required_skill_level"]), Convert.ToInt32 (row["use_type"]),
                    row["display_name"].ToString (), Convert.ToInt32 (row["passTypeId"]),
                    Convert.ToDateTime (row["date_added"]),
                    Convert.ToUInt32 (row["item_creator_id"]), Convert.ToInt32 (row["item_active"]), Convert.ToInt32 (row["arm_anywhere"]),
                    Convert.ToInt32 (row["disarmable"]), Convert.ToInt32 (row["stackable"]), Convert.ToInt32 (row["destroy_when_used"]),
                    Convert.ToInt32 (row["inventory_type"]), Convert.ToUInt32 (row["base_global_id"]), row["template_path"].ToString (), row["texture_path"].ToString (),
                    row["thumbnail_path"].ToString (), row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (), row["thumbnail_assetdetails_path"].ToString (), Convert.ToUInt32 (row["designer_price"]),
                    row["keywords"].ToString (), Convert.ToInt32 (row["WebPrice"]), Convert.ToInt32 (row["expired_duration"]), Convert.ToUInt32 (row["number_sold_on_web"]), Convert.ToUInt32 (row["number_of_raves"]),
                    Convert.ToUInt32 (row["number_of_views"]), Convert.ToUInt32 (row["sales_total"]), Convert.ToUInt32 (row["sales_designer_total"]),
                    Convert.ToInt32 (row["is_derivable"]), Convert.ToInt32 (row["derivation_level"]), Convert.ToUInt32 (row["TemplateDesignerPrice"]),
                    Convert.ToUInt32 (row["category_id1"]), Convert.ToUInt32 (row["category_id2"]), Convert.ToUInt32 (row["category_id3"]), Convert.ToInt32 (row["actor_group"])
                    ));
                }
            }
            return list;

        }

        public List<WOKItem> GetBundleItems (int bundleId, string filter, bool onlyPublic = true)
        {
            string sql = "SELECT i.global_id,name,description,market_cost, " +
                " selling_price, required_skill, required_skill_level, use_type, " +
                " iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId, " +
                " iw.date_added, " +
                " item_creator_id, iw.item_active, arm_anywhere, " +
                " disarmable, stackable, destroy_when_used, " +
                " inventory_type, actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " +
                " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
                " (i.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice, " +
                " expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total, " +
                " i.is_derivable, i.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice, " +
                " iw.category_id1, iw.category_id2, iw.category_id3, IFNULL(gi.game_id,0) as game_id, IFNULL(gi.approval_status, 0) as approval_status, " +
                " bi.quantity " +
                " FROM shopping.items_web iw INNER JOIN wok.items i ON i.global_id = iw.global_id " +
                " INNER JOIN wok.bundle_items bi ON iw.global_id = bi.item_global_id " +
                " LEFT OUTER JOIN shopping.items_web iw2 on iw.base_global_id=iw2.global_id " + // get base item info
                " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id " +
                " LEFT OUTER JOIN wok.game_items gi ON i.global_id = gi.global_id " +
                " WHERE bi.bundle_global_id = @bundleId ";

            if (onlyPublic)
            {
                sql += " AND iw.item_active = " + (int)WOKItem.ItemActiveStates.Public;
            }
            else
            {
                sql += " AND iw.item_active <> " + (int)WOKItem.ItemActiveStates.Deleted;
            }
            
            if (filter.Length > 0)
            {
                sql += " AND " + filter;
            }

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@bundleId", bundleId));

            DataTable dt = Db.Shopping.GetDataTable (sql, parameters);

            List<WOKItem> list = new List<WOKItem> ();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    WOKItem wokItem = new WOKItem (Convert.ToInt32 (row["global_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToInt32 (row["market_cost"]),
                    Convert.ToInt32 (row["selling_price"]), Convert.ToInt32 (row["required_skill"]), Convert.ToInt32 (row["required_skill_level"]), Convert.ToInt32 (row["use_type"]),
                    row["display_name"].ToString (), Convert.ToInt32 (row["passTypeId"]),
                    Convert.ToDateTime (row["date_added"]),
                    Convert.ToUInt32 (row["item_creator_id"]), Convert.ToInt32 (row["item_active"]), Convert.ToInt32 (row["arm_anywhere"]),
                    Convert.ToInt32 (row["disarmable"]), Convert.ToInt32 (row["stackable"]), Convert.ToInt32 (row["destroy_when_used"]),
                    Convert.ToInt32 (row["inventory_type"]), Convert.ToUInt32 (row["base_global_id"]), row["template_path"].ToString (), row["texture_path"].ToString (),
                    row["thumbnail_path"].ToString (), row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (), row["thumbnail_assetdetails_path"].ToString (), Convert.ToUInt32 (row["designer_price"]),
                    row["keywords"].ToString (), Convert.ToInt32 (row["WebPrice"]), Convert.ToInt32 (row["expired_duration"]), Convert.ToUInt32 (row["number_sold_on_web"]), Convert.ToUInt32 (row["number_of_raves"]),
                    Convert.ToUInt32 (row["number_of_views"]), Convert.ToUInt32 (row["sales_total"]), Convert.ToUInt32 (row["sales_designer_total"]),
                    Convert.ToInt32 (row["is_derivable"]), Convert.ToInt32 (row["derivation_level"]), Convert.ToUInt32 (row["TemplateDesignerPrice"]),
                    Convert.ToUInt32 (row["category_id1"]), Convert.ToUInt32 (row["category_id2"]), Convert.ToUInt32 (row["category_id3"]), Convert.ToInt32 (row["actor_group"])
                    );

                    wokItem.Quantity = Convert.ToUInt32(row["quantity"]);
                    list.Add(wokItem);
                }
            }
            return list;
        }

        public int AddItemToBundle (int bundleGlobalId, int itemGlobalId, int quantity, bool propagateQueryExceptions = false)
        {
            string sqlString = "INSERT IGNORE INTO bundle_items ( " +
                " bundle_global_id, item_global_id, quantity " +
                " ) VALUES (" +
                " @bundleGlobalId, @itemGlobalId, @quantity )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@bundleGlobalId", bundleGlobalId));
            parameters.Add (new MySqlParameter ("@itemGlobalId", itemGlobalId));
            parameters.Add (new MySqlParameter ("@quantity", quantity));
            return Db.WOK.ExecuteNonQuery(sqlString, parameters, propagateQueryExceptions);
        }

        public int RemoveItemsFromBundle(int bundleGlobalId, List<int> itemGlobalIds)
        {
            if (itemGlobalIds == null || itemGlobalIds.Count == 0)
            {
                return 0;
            }

            string sqlString = "DELETE FROM bundle_items " +
                               "WHERE bundle_global_id = @bundleGlobalId " +
                               "AND item_global_id IN (" + string.Join(",", itemGlobalIds) + ")";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@bundleGlobalId", bundleGlobalId));
            return Db.WOK.ExecuteNonQuery(sqlString, parameters);
        }

        public int RemoveAllFromBundle(int bundleGlobalId, bool propagateQueryExceptions = false)
        {
            string sqlString = "DELETE FROM bundle_items WHERE bundle_global_id = @bundleGlobalId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@bundleGlobalId", bundleGlobalId));
            return Db.WOK.ExecuteNonQuery(sqlString, parameters, propagateQueryExceptions);
        }

        public List<WOKItem> GetDerivedItemsIncludedInBundles (int globalId, string filter)
        {
            string sql = "SELECT DISTINCT(i.global_id),name,description,market_cost, " +
                " selling_price, required_skill, required_skill_level, use_type, " +
                " iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId, " +
                " iw.date_added, " +
                " item_creator_id, iw.item_active, arm_anywhere, " +
                " disarmable, stackable, destroy_when_used, " +
                " inventory_type, actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " +
                " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
                " (i.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice, " +
                " expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total, " +
                " i.is_derivable, i.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice, " +
                " iw.category_id1, iw.category_id2, iw.category_id3, IFNULL(gi.game_id,0) as game_id, IFNULL(gi.approval_status, 0) as approval_status " +
                " FROM wok.items i INNER JOIN shopping.items_web iw ON i.global_id = iw.global_id " +
                " INNER JOIN wok.bundle_items bi ON bi.item_global_id = i.global_id " +
                " LEFT OUTER JOIN shopping.items_web iw2 on iw.base_global_id=iw2.global_id " + // get base item info
                " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id " +
                " LEFT OUTER JOIN wok.game_items gi ON i.global_id = gi.global_id " +
                " WHERE i.base_global_id = @globalId " +
                " AND i.global_id <> @globalId";

            if (filter.Length > 0)
            {
                sql += " AND " + filter;
            }

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));

            DataTable dt = Db.Shopping.GetDataTable (sql, parameters);

            List<WOKItem> list = new List<WOKItem> ();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    list.Add (new WOKItem (Convert.ToInt32 (row["global_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToInt32 (row["market_cost"]),
                    Convert.ToInt32 (row["selling_price"]), Convert.ToInt32 (row["required_skill"]), Convert.ToInt32 (row["required_skill_level"]), Convert.ToInt32 (row["use_type"]),
                    row["display_name"].ToString (), Convert.ToInt32 (row["passTypeId"]),
                    Convert.ToDateTime (row["date_added"]),
                    Convert.ToUInt32 (row["item_creator_id"]), Convert.ToInt32 (row["item_active"]), Convert.ToInt32 (row["arm_anywhere"]),
                    Convert.ToInt32 (row["disarmable"]), Convert.ToInt32 (row["stackable"]), Convert.ToInt32 (row["destroy_when_used"]),
                    Convert.ToInt32 (row["inventory_type"]), Convert.ToUInt32 (row["base_global_id"]), row["template_path"].ToString (), row["texture_path"].ToString (),
                    row["thumbnail_path"].ToString (), row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (), row["thumbnail_assetdetails_path"].ToString (), Convert.ToUInt32 (row["designer_price"]),
                    row["keywords"].ToString (), Convert.ToInt32 (row["WebPrice"]), Convert.ToInt32 (row["expired_duration"]), Convert.ToUInt32 (row["number_sold_on_web"]), Convert.ToUInt32 (row["number_of_raves"]),
                    Convert.ToUInt32 (row["number_of_views"]), Convert.ToUInt32 (row["sales_total"]), Convert.ToUInt32 (row["sales_designer_total"]),
                    Convert.ToInt32 (row["is_derivable"]), Convert.ToInt32 (row["derivation_level"]), Convert.ToUInt32 (row["TemplateDesignerPrice"]),
                    Convert.ToUInt32 (row["category_id1"]), Convert.ToUInt32 (row["category_id2"]), Convert.ToUInt32 (row["category_id3"]), Convert.ToInt32 (row["actor_group"])
                    ));
                }
            }
            return list;
        }  

        public int GetBundleStoredWebPrice (int globalId)
        {
            string strSQL = "SELECT market_cost as WebPrice " +
                " FROM items i " +
                " WHERE global_id = " + globalId.ToString();

            DataRow dr = Db.WOK.GetDataRow (strSQL);
            if (dr != null)
                return Convert.ToInt32 (dr["WebPrice"]);

            return 0;
        }

        /// <summary>
        /// GetItemCategory
        /// </summary>
        public ItemCategory GetItemCategoryByName (string name)
        {
            //create query string to get game by user id
            string sql = "SELECT item_category_id, name, description, parent_category_id, sort_order, marketing_path, upload_type, actor_group  " +
                 " FROM item_categories i " +
                 " WHERE i.name = @name";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@name", name));

            //perform query returning single data row from the wok database
            DataRow row = Db.Shopping.GetDataRow (sql.ToString (), parameters);

            if (row == null)
            {
                return new ItemCategory ();
            }

            return new ItemCategory (Convert.ToUInt32 (row["item_category_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToUInt32 (row["parent_category_id"]),
                Convert.ToInt32 (row["sort_order"]), row["marketing_path"].ToString (), Convert.ToInt32 (row["upload_type"]), Convert.ToInt32 (row["actor_group"]));
        }

        /// <summary>
        /// GetItemCategoryByTree
        /// </summary>
        public ItemCategory GetItemCategoryByTree(string parentCategory, string subCategory)
        {
            string sql = "SELECT ic2.item_category_id, ic2.name, ic2.description, ic2.parent_category_id, ic2.sort_order, ic2.marketing_path, ic2.upload_type, ic2.actor_group  " +
                 " FROM shopping.item_categories ic " +
                 " INNER JOIN shopping.item_categories ic2 ON ic2.parent_category_id = ic.item_category_id " +
                 " WHERE ic.description = @parentCategory " +
                 " AND ic2.description = @subCategory ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@parentCategory", parentCategory));
            parameters.Add(new MySqlParameter("@subCategory", subCategory));

            //perform query returning single data row from the wok database
            DataRow row = Db.Shopping.GetDataRow(sql.ToString(), parameters);

            if (row == null)
            {
                return new ItemCategory();
            }

            return new ItemCategory(Convert.ToUInt32(row["item_category_id"]), row["name"].ToString(), row["description"].ToString(), Convert.ToUInt32(row["parent_category_id"]),
                Convert.ToInt32(row["sort_order"]), row["marketing_path"].ToString(), Convert.ToInt32(row["upload_type"]), Convert.ToInt32(row["actor_group"]));
        }
 
        /// <summary>
        /// GetItemCategory
        /// </summary>
        public ItemCategory GetItemCategoryParentByUploadType (int uploadType)
        {
            //create query string to get game by user id
            string sql = "SELECT item_category_id, name, description, parent_category_id, sort_order, marketing_path, upload_type, actor_group " +
                " FROM item_categories i " +
                " WHERE i.upload_type = @upload_type and parent_category_id = 0 ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@upload_type", uploadType));

            //perform query returning single data row from the wok database
            DataRow row = Db.Shopping.GetDataRow (sql.ToString (), parameters);

            if (row == null)
            {
                return new ItemCategory ();
            }

            return new ItemCategory (Convert.ToUInt32 (row["item_category_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToUInt32 (row["parent_category_id"]),
                Convert.ToInt32 (row["sort_order"]), row["marketing_path"].ToString (), Convert.ToInt32 (row["upload_type"]), Convert.ToInt32 (row["actor_group"]));
        }

        /// <summary>
        /// GetUploadTypeByItemCategoryId
        /// </summary>
        public int GetUploadTypeByItemCategoryId (int categoryId, int retry)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strSQL = "SELECT upload_type, parent_category_id " +
                " FROM item_categories i " +
                " WHERE i.item_category_id = @categoryId";

            parameters.Add (new MySqlParameter ("@categoryId", categoryId));

            DataRow row = Db.Shopping.GetDataRow (strSQL.ToString (), parameters);

            int uploadType = 0;

            try
            {
                uploadType = Convert.ToInt32 (row["upload_type"]);
            }
            catch { }

            if (uploadType != 0)
            {
                return uploadType;
            }
            else
            {
                if (retry == 0 && row != null)
                {
                    return GetUploadTypeByItemCategoryId (Convert.ToInt32 (row["parent_category_id"]), 1);
                }
                else
                {
                    return 0;
                }
            }

        }
 
        /// <summary>
        /// GetItemCategory
        /// </summary>
        public ItemCategory GetItemCategory (UInt32 categoryId)
        {
            if (categoryId.Equals (0))
            {
                return new ItemCategory();
            }

             // If found cache for a while
            string cacheKey = "item_categories." + categoryId;
            ItemCategory itemCategory = (ItemCategory) DbCache.Cache[cacheKey];

            if (itemCategory == null)
            {
                //create query string to get game by user id
                string sql = "SELECT item_category_id, name, description, parent_category_id, sort_order, marketing_path, upload_type, actor_group " +
                     " FROM item_categories i " +
                     " WHERE i.item_category_id = @categoryId ";

                //indicate any parameters for place holder variables in query string
                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@categoryId", categoryId));

                //perform query returning single data row from the wok database
                DataRow row = Db.Shopping.GetDataRow(sql.ToString(), parameters);

                if (row == null)
                {
                    return new ItemCategory();
                }

                itemCategory = new ItemCategory(Convert.ToUInt32(row["item_category_id"]), row["name"].ToString(), row["description"].ToString(), Convert.ToUInt32(row["parent_category_id"]),
                    Convert.ToInt32(row["sort_order"]), row["marketing_path"].ToString(), Convert.ToInt32(row["upload_type"]), Convert.ToInt32(row["actor_group"]));

                DbCache.Cache.Insert (cacheKey, itemCategory, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return itemCategory;
        }

        /// <summary>
        /// GetItemCategoriesByParentCategoryId
        /// </summary>
        public List<ItemCategory> GetItemCategoriesByParentCategoryId (UInt32[] parentCategoryIds)
        {
            // If found cache for a while
            string cacheId = "";
            for (int i=0; i < parentCategoryIds.Length; i++)
            {
                cacheId += parentCategoryIds[i].ToString();
            }
            
            string cacheKey = "item_categories_by_parent." + cacheId;
            List<ItemCategory> categoryList = (List<ItemCategory>) DbCache.Cache[cacheKey];

            if (categoryList == null)
            {
                //create query string to get game by user id
                string sql = "SELECT item_category_id, name, description, parent_category_id, sort_order, marketing_path, upload_type, actor_group " +
                     " FROM item_categories i " +
                     " WHERE i.parent_category_id IN (" + string.Join (",", Array.ConvertAll<UInt32, string> (parentCategoryIds, Convert.ToString)) + ") " +
                     " ORDER BY sort_order ASC";

                //indicate any parameters for place holder variables in query string
                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
                //           parameters.Add (new MySqlParameter ("@parentCategoryId", parentCategoryId));

                //parameters.Add(new MySqlParameter("@userId", userId));
                DataTable dt = Db.Shopping.GetDataTable (sql, parameters);

                categoryList = new List<ItemCategory> ();

                foreach (DataRow row in dt.Rows)
                {
                    categoryList.Add (new ItemCategory (Convert.ToUInt32 (row["item_category_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToUInt32 (row["parent_category_id"]),
                    Convert.ToInt32 (row["sort_order"]), row["marketing_path"].ToString (), Convert.ToInt32 (row["upload_type"]), Convert.ToInt32 (row["actor_group"])));
                }

                DbCache.Cache.Insert (cacheKey, categoryList, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return categoryList;
        }
 
        /// <summary>
        /// GetItemCatByUploadType
        /// </summary>
        public DataTable GetItemCatsByUploadType (uint uploadType)
        {
            //create query string to get game by user id
            string sql = "SELECT ic.item_category_id, ic2.name as parent, ic.name, CONCAT(ic2.name,  ' - ', ic.name) AS listName  FROM item_categories ic " +
                  " JOIN item_categories ic2 ON ic.parent_category_id = ic2.item_category_id " +
                  " WHERE ic.upload_type = @upload_type  " +
                  " AND ic.parent_category_id !=0 " +
                  " ORDER BY ic.parent_category_id";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@upload_type", uploadType));
            DataTable dt = Db.Shopping.GetDataTable (sql, parameters);

            return dt;
        }

        /// <summary>
        /// GetItemCatByUploadTypeActorGroup
        /// </summary>
        public List<ItemCategory> GetItemCatByUploadTypeActorGroup (uint uploadType, int actorGroup)
        {
            //create query string to get game by user id
            string sql = "SELECT item_category_id, name, description, parent_category_id, sort_order, marketing_path, upload_type, actor_group " +
                 " FROM item_categories i " +
                 " WHERE i.upload_type = @upload_type " +
                 " and i.actor_group = @actor_group " +
                 " ORDER BY sort_order ASC";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@upload_type", uploadType));
            parameters.Add (new MySqlParameter ("@actor_group", actorGroup));

            DataTable dt = Db.Shopping.GetDataTable (sql, parameters);

            List<ItemCategory> list = new List<ItemCategory> ();

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new ItemCategory (Convert.ToUInt32 (row["item_category_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToUInt32 (row["parent_category_id"]),
                Convert.ToInt32 (row["sort_order"]), row["marketing_path"].ToString (), Convert.ToInt32 (row["upload_type"]), Convert.ToInt32 (row["actor_group"])));
            }
            return list;
        }

        /// <summary>
        /// SearchWOKItems
        /// </summary>
        public PagedList<ItemCategory> GetItemCategories (UInt32 itemCategoryId, string filter, int pageSize, int pageNumber, string orderBy)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strSelect = "SELECT item_category_id, name, description, parent_category_id, sort_order, marketing_path, upload_type, actor_group " +
                " FROM item_categories i ";

            if (itemCategoryId > 0)
            {
                strSelect += " WHERE i.item_category_id = @itemCategoryId ";
                parameters.Add (new MySqlParameter ("@itemCategoryId", itemCategoryId));
            }

            if ((filter != null) && (filter != ""))
            {
                strSelect += filter;
            }

            PagedDataTable dt = Db.Shopping.GetPagedDataTable (strSelect, orderBy, parameters, pageNumber, pageSize);

            PagedList<ItemCategory> list = new PagedList<ItemCategory> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new ItemCategory (Convert.ToUInt32 (row["item_category_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToUInt32 (row["parent_category_id"]),
                Convert.ToInt32 (row["sort_order"]), row["marketing_path"].ToString (), Convert.ToInt32 (row["upload_type"]), Convert.ToInt32 (row["actor_group"])));
            }
            return list;

        }

        /// <summary>
        /// GetItemCategories
        /// </summary>
        public List<ItemCategory> GetItemCategoriesByFilter (string filter)
        {
            //create query string to categorites by global_id
            string strSelect = "SELECT item_category_id, name, description, parent_category_id, sort_order, marketing_path, upload_type, actor_group " +
                " FROM item_categories i ";

            if ((filter != null) && (filter != ""))
            {
                strSelect += "WHERE " + filter;
            }

            //parameters.Add(new MySqlParameter("@userId", userId));
            DataTable dt = Db.Shopping.GetDataTable (strSelect);

            List<ItemCategory> list = new List<ItemCategory> ();

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new ItemCategory (Convert.ToUInt32 (row["item_category_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToUInt32 (row["parent_category_id"]),
                Convert.ToInt32 (row["sort_order"]), row["marketing_path"].ToString (), Convert.ToInt32 (row["upload_type"]), Convert.ToInt32 (row["actor_group"])));
            }
            return list;
        }

        /// <summary>
        /// GetCategoriesThatHaveItems - temporary
        /// works on assumption that production has no bottom level categories that don't have items associated
        /// with it
        /// </summary>
        public List<ItemCategory> GetCategoriesThatHaveItems (string filter)
        {
            //create query string to categorites by global_id
            string strSelect = "SELECT ic.item_category_id, name, description, parent_category_id, sort_order, marketing_path, upload_type, actor_group " +
                " FROM item_categories ic ";


            if ((filter != null) && (filter != ""))
            {
                strSelect += "WHERE " + filter;
            }

            strSelect += " GROUP BY ic.item_category_id ";

            //parameters.Add(new MySqlParameter("@userId", userId));
            DataTable dt = Db.Shopping.GetDataTable (strSelect);

            List<ItemCategory> list = new List<ItemCategory> ();

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new ItemCategory (Convert.ToUInt32 (row["item_category_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToUInt32 (row["parent_category_id"]),
                Convert.ToInt32 (row["sort_order"]), row["marketing_path"].ToString (), Convert.ToInt32 (row["upload_type"]), Convert.ToInt32 (row["actor_group"])));
            }
            return list;
        }
 
        /// <summary>
        /// GetItemCategories
        /// </summary>
        public List<ItemCategory> GetItemCategoriesByItemId (int globalId)
        {
            //create query string to categorites by global_id
            string sql = "SELECT i.item_category_id, name, description, parent_category_id, sort_order, marketing_path, upload_type, actor_group, " +
                " IF(i.parent_category_id = 0, 1, " +
                "   IF(i.item_category_id = iw.category_id2, 2, " +
                "     IF(i.item_category_id = iw.category_id3, 3, 5) " +
                "   ) " +
                " ) AS cat_sort " +
                " FROM item_categories i, items_web iw " +
                " WHERE (i.item_category_id = iw.category_id1 OR i.item_category_id = iw.category_id2 OR i.item_category_id = iw.category_id3) " +
                " AND iw.global_id = @globalId " +
                " ORDER BY cat_sort ASC";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));

            //parameters.Add(new MySqlParameter("@userId", userId));
            DataTable dt = Db.Shopping.GetDataTable (sql, parameters);

            List<ItemCategory> list = new List<ItemCategory> ();

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new ItemCategory (Convert.ToUInt32 (row["item_category_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToUInt32 (row["parent_category_id"]),
                Convert.ToInt32 (row["sort_order"]), row["marketing_path"].ToString (), Convert.ToInt32 (row["upload_type"]), Convert.ToInt32 (row["actor_group"])));
            }
            return list;
        }

        /// <summary>
        /// AddItemCategory
        /// </summary>
        public int AddItemCategoryItem (int globalId, ItemCategory itemCategory)
        {
            string sqlString = "INSERT INTO item_category_items ( " +
                " global_id, item_category_id " +
                " ) VALUES (" +
                " @globalId, @itemCategoryId )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));
            parameters.Add (new MySqlParameter ("@itemCategoryId", itemCategory.ItemCategoryId));
            return Db.Shopping.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// GetItemPromotions
        /// </summary>
        public PagedList<WOKItem> GetItemPromotions (UInt32 itemCategoryId, int pageSize)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strSQL = "SELECT i.global_id,name,description,market_cost, " +
                " selling_price, required_skill, required_skill_level, use_type, " +
                " iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId, iw.date_added, " +
                " item_creator_id, iw.item_active, arm_anywhere, " +
                " disarmable, stackable, destroy_when_used, " +
                " inventory_type, actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " +
                " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
                " (i.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice, " +
                " expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total, " +
                " i.is_derivable, i.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice, " +
                " iw.category_id1, iw.category_id2, iw.category_id3 " +
                " FROM wok.items i " +
                " INNER JOIN shopping.items_web iw ON i.global_id = iw.global_id " +
                " LEFT OUTER JOIN shopping.items_web iw2 ON iw.base_global_id = iw2.global_id " +
                " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = swi.global_id AND pgi.pass_group_id = 1 " +
                " INNER JOIN item_promotions ip ON i.global_id = ip.global_id AND ip.item_category_id = @itemCategoryId ";

            parameters.Add (new MySqlParameter ("@itemCategoryId", itemCategoryId));

            //parameters.Add(new MySqlParameter("@userId", userId));
            PagedDataTable dt = Db.Shopping.GetPagedDataTable (strSQL, "", parameters, 1, pageSize);

            PagedList<WOKItem> list = new PagedList<WOKItem> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new WOKItem (Convert.ToInt32 (row["global_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToInt32 (row["market_cost"]),
                Convert.ToInt32 (row["selling_price"]), Convert.ToInt32 (row["required_skill"]), Convert.ToInt32 (row["required_skill_level"]), Convert.ToInt32 (row["use_type"]),
                row["display_name"].ToString (), Convert.ToInt32 (row["passTypeId"]),
                Convert.ToDateTime (row["date_added"]),
                Convert.ToUInt32 (row["item_creator_id"]), Convert.ToInt32 (row["item_active"]), Convert.ToInt32 (row["arm_anywhere"]),
                Convert.ToInt32 (row["disarmable"]), Convert.ToInt32 (row["stackable"]), Convert.ToInt32 (row["destroy_when_used"]),
                Convert.ToInt32 (row["inventory_type"]), Convert.ToUInt32 (row["base_global_id"]), row["template_path"].ToString (), row["texture_path"].ToString (),
                row["thumbnail_path"].ToString (), row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (), row["thumbnail_assetdetails_path"].ToString (), Convert.ToUInt32 (row["designer_price"]),
                row["keywords"].ToString (), Convert.ToInt32 (row["WebPrice"]), Convert.ToInt32 (row["expired_duration"]), Convert.ToUInt32 (row["number_sold_on_web"]), Convert.ToUInt32 (row["number_of_raves"]),
                Convert.ToUInt32 (row["number_of_views"]), Convert.ToUInt32 (row["sales_total"]), Convert.ToUInt32 (row["sales_designer_total"]),
                Convert.ToInt32 (row["is_derivable"]), Convert.ToInt32 (row["derivation_level"]), Convert.ToUInt32 (row["TemplateDesignerPrice"]),
                Convert.ToUInt32 (row["category_id1"]), Convert.ToUInt32 (row["category_id2"]), Convert.ToUInt32 (row["category_id3"]), Convert.ToInt32 (row["actor_group"])
                ));
            }
            return list;

        }

        /// <summary>
        /// GetItemReviews
        /// </summary>
        public PagedList<ItemReview> GetItemReviews (int globalId, int pageSize, int pageNumber)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strSQL = "SELECT ir.review_id, ir.user_id, ir.global_id, ir.comment, INET_NTOA(ir.ip_address) as ip_address, " +
                " ir.last_updated_date, ir.created_date, last_updated_user_id, ir.status_id, u.username, c.thumbnail_small_path " +
                " FROM shopping.item_reviews ir, kaneva.users u " +
                " INNER JOIN kaneva.communities_personal c ON c.creator_id = u.user_id " +
                " WHERE ir.user_id = u.user_id " +
                " AND global_id = @globalId " +
                " AND ir.status_id = @statusId ";

            parameters.Add (new MySqlParameter ("@globalId", globalId));
            parameters.Add(new MySqlParameter("@statusId", (int)ItemReview.ItemReviewStatus.Active));

            //parameters.Add(new MySqlParameter("@userId", userId));
            PagedDataTable dt = Db.Shopping.GetPagedDataTable (strSQL, "review_id DESC", parameters, 1, pageSize);

            PagedList<ItemReview> list = new PagedList<ItemReview> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new ItemReview (Convert.ToUInt32 (row["review_id"]), Convert.ToInt32 (row["global_id"]), Convert.ToInt32 (row["user_id"]), row["comment"].ToString (), row["ip_address"].ToString (),
                Convert.ToDateTime (row["last_updated_date"]), Convert.ToDateTime (row["created_date"]), Convert.ToInt32 (row["last_updated_user_id"]), Convert.ToInt32 (row["status_id"]),
                row["username"].ToString (), row["thumbnail_small_path"].ToString ()
                ));
            }
            return list;
        }

        /// <summary>
        /// AddItemReview
        /// </summary>
        public int AddItemReview (ItemReview itemReview)
        {
            string sqlString = "INSERT INTO item_reviews ( " +
                            " global_id, user_id, comment, ip_address, " +
                            " last_updated_date, created_date, last_updated_user_id, status_id " +
                            " ) VALUES (" +
                             " @globalId, @userId, @comment, INET_ATON(@ipAddress), " +
                            " NOW(), NOW(), 0, @statusId )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", itemReview.GlobalId));
            parameters.Add (new MySqlParameter ("@userId", itemReview.UserId));
            parameters.Add (new MySqlParameter ("@comment", itemReview.Comment));
            parameters.Add (new MySqlParameter ("@ipAddress", itemReview.IpAddress));
            parameters.Add (new MySqlParameter ("@statusId", itemReview.StatusId));
            return Db.Shopping.ExecuteNonQuery (sqlString, parameters);
        }

        public int DeleteItemReview(uint reviewId)
        {
            string sqlString = "UPDATE item_reviews " +
                               "SET status_id = @statusId " +
                               "WHERE review_id = @reviewId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@reviewId", reviewId));
            parameters.Add(new MySqlParameter("@statusId", (int)ItemReview.ItemReviewStatus.Deleted));
            return Db.Shopping.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// GetRelatedItems
        /// </summary>
        public PagedList<WOKItem> GetRelatedItems (WOKItem item, UInt32 itemCategoryId, int pageSize, int pageNumber, bool showRestricted)
        {
            PagedList<WOKItem> plRelated = SearchItems ("", Convert.ToInt32 (item.ItemCreatorId), new uint[] { 0 }, pageSize, pageNumber, "date_added DESC", showRestricted, false, 0, false);

            if (plRelated.Count < pageSize)
            {
                // Get more items from same category
                PagedList<WOKItem> plRelatedMore = SearchItems ("", 0, new uint[] { itemCategoryId }, pageSize - plRelated.Count, pageNumber, "date_added DESC", showRestricted, false, 0, false);

                foreach (WOKItem wokItem in plRelatedMore)
                {
                    plRelated.Add (wokItem);
                }
            }

            return plRelated;
        }

        /// <summary>
        /// AddItemPurchase
        /// </summary>
        public int AddItemPurchase(int globalId, int txnType, int userId, UInt32 itemOwnerId, int purchasePrice, UInt32 ownerPrice, string ipAddress, string keiPointId, int quantity, int parentPurchaseId)
        {
            string sqlString = "";
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            // Per Jeff P, don't increment these counts if paid via rewards or if commision
            // per Jeff P, "This is to cut down on support tickets", but has side affect of cuasing mismatch between item_purchase data and web_items summary total. 
            // Of course this will created a mismatch between summary number and item_purchases numbers
            // Do not increase number_sold_on_web unless it's a purchase record
            if ((txnType != (int) ItemPurchase.eItemTxnType.COMMISSION) && !keiPointId.Equals (Currency.CurrencyType.GPOINT))
            {
                // Update number of times sold
                sqlString = " UPDATE items_web " +
                     " SET number_sold_on_web = (number_sold_on_web + @quantity), " +
                     " sales_total = (sales_total + @purchase_price), " +
                     " sales_designer_total = (sales_designer_total + @owner_price) " +
                     " WHERE global_id = @global_id ";
                parameters.Add (new MySqlParameter ("@global_id", globalId));
                parameters.Add (new MySqlParameter ("@purchase_price", purchasePrice));
                parameters.Add (new MySqlParameter ("@owner_price", ownerPrice));
                parameters.Add (new MySqlParameter ("@quantity", quantity));  // Increase number_sold_on_web by quantity (bug fix)        
                Db.Shopping.ExecuteNonQuery (sqlString, parameters);
            }

            // Add the purchase history
            sqlString = "INSERT INTO item_purchases ( " +
                " global_id, txn_type, user_id, item_owner_id, purchase_price, kei_point_id, owner_price, ip_address, purchase_date, quantity, parent_purchase_id " +
                " ) VALUES (" +
                " @global_id, @txn_type, @user_id, @item_owner_id, @purchase_price, @keiPointId, @owner_price, INET_ATON(@ip_address), NOW(), @quantity, @parent_purchase_id)";

            parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@global_id", globalId));
            parameters.Add (new MySqlParameter ("@txn_type", txnType));
            parameters.Add (new MySqlParameter ("@user_id", userId));
            parameters.Add (new MySqlParameter ("@item_owner_id", itemOwnerId));
            parameters.Add (new MySqlParameter ("@purchase_price", purchasePrice));
            parameters.Add (new MySqlParameter ("@keiPointId", keiPointId));
            parameters.Add (new MySqlParameter ("@owner_price", ownerPrice));
            parameters.Add (new MySqlParameter ("@ip_address", ipAddress));
            parameters.Add (new MySqlParameter ("@quantity", quantity));
            parameters.Add(new MySqlParameter("@parent_purchase_id", parentPurchaseId));

            return Db.Shopping.ExecuteIdentityInsert (sqlString, parameters);
        }

        /// <summary>
        /// GetItemPurchases
        /// </summary>
        public PagedList<ItemPurchase> GetItemPurchases (int userId, int pageSize, int pageNumber)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strSQL = "SELECT ip.purchase_id, ip.txn_type, ip.global_id, ip.user_id, ip.item_owner_id," +
                    " ip.purchase_price, ip.ip_address, ip.purchase_date, ip.owner_price, i.name, ip.quantity " +
                " FROM shopping.item_purchases ip INNER JOIN wok.items i ON ip.global_id = i.global_id " +
                " WHERE ip.item_owner_id = @userId ";

            parameters.Add (new MySqlParameter ("@userId", userId));
            PagedDataTable dt = Db.Shopping.GetPagedDataTable (strSQL, "purchase_date DESC", parameters, pageNumber, pageSize);

            PagedList<ItemPurchase> list = new PagedList<ItemPurchase> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new ItemPurchase (Convert.ToUInt32 (row["purchase_id"]), Convert.ToInt32 (row["txn_type"]), Convert.ToInt32 (row["global_id"]), row["name"].ToString (), Convert.ToInt32 (row["user_id"]), Convert.ToUInt32 (row["item_owner_id"]),
                    Convert.ToInt32 (row["purchase_price"]), Convert.ToInt32 (row["owner_price"]), Convert.ToString (row["ip_address"]), Convert.ToDateTime (row["purchase_date"]), Convert.ToInt32 (row["quantity"])
                ));
            }
            return list;
        }

        /// <summary>
        /// GetWeeklySales
        /// </summary>
        public int GetWeeklySales (int userId, ref int credits, ref int numberOfSales)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strSQL = "SELECT SUM(owner_price) as ownerCredits, COUNT(purchase_id) as itemCount " +
                " FROM item_purchases " +
                " WHERE item_owner_id = @userId " +
                " AND purchase_date > DATE_ADD(NOW(), INTERVAL -7 DAY) " +
                " GROUP BY item_owner_id ";

            parameters.Add (new MySqlParameter ("@userId", userId));
            DataTable dt = Db.Shopping.GetDataTable (strSQL, parameters);

            if (dt != null && dt.Rows.Count > 0)
            {
                credits = Convert.ToInt32 (dt.Rows[0]["ownerCredits"]);
                numberOfSales = Convert.ToInt32 (dt.Rows[0]["itemCount"]);
            }

            return 0;
        }

        /// <summary>
        /// Insert a digg
        /// </summary>
        public int InsertItemDigg (ItemDigg itemDigg)
        {
            // Must be logged in to digg someone
            if (itemDigg.UserId < 1)
            {
                //return 2;
                return 1;
            }

            // Make sure they have not already digged
            ItemDigg previousDigg = GetItemDigg (itemDigg.UserId, itemDigg.GlobalId);

            // If they already dugg
            if (previousDigg.GlobalId.Equals (itemDigg.GlobalId))
            {
                // Already dugg
                return 1;
            }

            string sql = "INSERT INTO item_diggs " +
                "(user_id, global_id, created_date " +
                ") VALUES (" +
                "@userId, @globalId,NOW())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", itemDigg.UserId));
            parameters.Add (new MySqlParameter ("@globalId", itemDigg.GlobalId));
            Db.Shopping.ExecuteNonQuery (sql.ToString (), parameters);

            string sqlUpdate = "UPDATE items_web " +
                " SET number_of_raves = number_of_raves + 1 " +
                " WHERE global_id = @globalId";
            parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", itemDigg.GlobalId));
            Db.Shopping.ExecuteNonQuery (sqlUpdate, parameters);

            return 0;
        }

        /// <summary>
        /// Get a digg for an item
        /// </summary>
        public ItemDigg GetItemDigg (int userId, int globalId)
        {
            //create query string to get game by user id
            string sql = "SELECT d.digg_id, user_id, global_id, created_date " +
                " FROM item_diggs d " +
                " WHERE user_id = @userId " +
                " AND global_id = @globalId ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@globalId", globalId));

            //perform query returning single data row from the wok database
            DataRow row = Db.Shopping.GetDataRow (sql.ToString (), parameters);

            if (row == null)
            {
                return new ItemDigg ();
            }

            return new ItemDigg (Convert.ToUInt32 (row["digg_id"]), Convert.ToInt32 (row["user_id"]), Convert.ToInt32 (row["global_id"]),
                Convert.ToDateTime (row["created_date"]));
        }

        /// <summary>
        /// GetItemTemplates
        /// </summary>
        public List<ItemTemplate> GetItemTemplates ()
        {
            //create query string to categorites by global_id
            string sql = "SELECT i.global_id, name, thumbnail_small_path, thumbnail_medium_path, thumbnail_large_path, height, width " +
                " FROM active_item_templates ait, wok.items i, shopping.items_web iw " +
                " WHERE ait.global_id = i.global_id " +
                " AND ait.global_id = iw.global_id " +
                " ORDER BY ait.sort_order ASC";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            DataTable dt = Db.Shopping.GetDataTable (sql, parameters);

            List<ItemTemplate> list = new List<ItemTemplate> ();

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new ItemTemplate (Convert.ToInt32 (row["global_id"]), row["name"].ToString (),
                    row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (),
                    Convert.ToInt32 (row["height"]), Convert.ToInt32 (row["width"])
                ));
            }
            return list;
        }

        public int CreateItemTemplate (WOKItem item)
        {
            return AddCustomItem (item, true, true);
        }

        /// <summary>
        /// AddItemPath - store relative path of item data file into wok.item_paths table
        /// </summary>
        public void AddItemPath (ItemPath itemPath)
        {
            string sqlString = "INSERT INTO `item_paths`(`global_id`, `type_id`, `ordinal`, `path`, `file_size`, `file_hash`, `compressed_size`, `unique_id`) " +
                               "SELECT @globalId as `global_id`, `type_id`, @ordinal as `ordinal`, @path as `path`, @fileSize as `file_size`, @fileHash as `file_hash`, @compressedSize as `compressed_size`, @uniqueAssetId as `unique_id` " +
                               "FROM `item_path_types` WHERE `type`=@pathType LIMIT 1";
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add(new MySqlParameter("@globalId", itemPath.GlobalId));
            parameters.Add(new MySqlParameter("@pathType", itemPath.PathType));
            parameters.Add(new MySqlParameter("@ordinal", itemPath.Ordinal));
            parameters.Add(new MySqlParameter("@path", itemPath.Path));
            parameters.Add(new MySqlParameter("@fileSize", itemPath.FileSize));
            parameters.Add(new MySqlParameter("@fileHash", itemPath.FileHash));
            parameters.Add(new MySqlParameter("@compressedSize", itemPath.CompressedSize));
            parameters.Add(new MySqlParameter("@uniqueAssetId", itemPath.UniqueId));
            Db.WOK.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// GetItemPaths - retrieve a list of relative paths from wok.item_paths table
        /// </summary>
        public List<ItemPath> GetItemPaths (int globalId)
        {
            string sqlString = "SELECT `global_id`, `type`, `ordinal`, `path`, `file_size`, `file_hash`, `compressed_size`, `unique_id` " +
                              "FROM `item_paths` ip INNER JOIN `item_path_types` ipt ON ip.`type_id`=ipt.`type_id` " +
                              "WHERE `global_id`=@globalId";
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));

            DataTable dt = Db.WOK.GetDataTable (sqlString, parameters);

            List<ItemPath> list = new List<ItemPath> ();

            foreach (DataRow row in dt.Rows)
            {
                list.Add (
                    new ItemPath(
                        Convert.ToInt32(row["global_id"]),
                        row["type"].ToString(),
                        Convert.ToInt64(row["ordinal"]),
                        row["path"].ToString(),
                        Convert.ToInt32(row["file_size"]),
                        row["file_hash"].ToString(),
                        Convert.ToInt32(row["compressed_size"]),
                        Convert.ToUInt32(row["unique_id"])
                    )
                );
            }
            return list;
        }

        /// <summary>
        /// AddItemPathTexture - store texture metadata in wok.item_path_textures table
        /// </summary>
        public void AddItemPathTexture(ItemPathTexture itemPathTex)
        {
            string sqlString = "INSERT INTO item_path_textures(global_id, ordinal, width, height, opacity, color) " +
                               "VALUES (@globalId, @ordinal, @width, @height, @opacity, @color)";
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add(new MySqlParameter("@globalId", itemPathTex.GlobalId));
            parameters.Add(new MySqlParameter("@ordinal", itemPathTex.Ordinal));
            parameters.Add(new MySqlParameter("@width", itemPathTex.Width));
            parameters.Add(new MySqlParameter("@height", itemPathTex.Height));
            parameters.Add(new MySqlParameter("@opacity", (int) itemPathTex.Opacity));
            parameters.Add(new MySqlParameter("@color", itemPathTex.AverageColor));
            Db.WOK.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// AddItemPathFileStore - add record for per-item file store override for unique asset stores
        /// </summary>
        public void AddItemPathFileStore(int globalId, ItemPathType pathTypeInfo, string filestore)
        {
            string sqlString = "INSERT INTO `item_path_filestores`(`global_id`, `item_path_type_id`, `filestore`) " +
                               "VALUES (@globalId, @itemPathTypeId, @filestore)";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@globalId", globalId));
            parameters.Add(new MySqlParameter("@itemPathTypeId", pathTypeInfo.TypeId));
            parameters.Add(new MySqlParameter("@filestore", filestore));
            Db.WOK.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// GetItemPathFileStore - get record for per-item file store override for unique asset stores
        /// </summary>
        public string GetItemPathFileStore(int globalId, ItemPathType pathTypeInfo)
        {
            string sqlString = "SELECT `filestore` FROM `item_path_filestores` WHERE `global_id`=@globalId AND `item_path_type_id`=@itemPathTypeId";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@globalId", globalId));
            parameters.Add(new MySqlParameter("@itemPathTypeId", pathTypeInfo.TypeId));

            Object res = Db.WOK.GetScalar(sqlString, parameters);
            if (res != null)
            {
                return Convert.ToString(res);
            }

            return null;
        }

        /// <summary>
        /// GetItemPathType - select record in item_path_type table
        /// </summary>
        public ItemPathType GetItemPathType(string pathType)
        {
            string sqlString = "SELECT type_id, `type`, asset_version, file_format, path_regex, path_format, store_type+0 store_type, primary_type_id, sha256_table FROM item_path_types WHERE `type`=@pathType";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@pathType", pathType));

            DataRow dr = Db.WOK.GetDataRow(sqlString, parameters);
            if (dr == null)
            {
                return null;
            }

            return new ItemPathType(
                Convert.ToInt32(dr["type_id"]),
                Convert.ToString(dr["type"]),
                Convert.ToInt32(dr["asset_version"]),
                Convert.ToString(dr["file_format"]),
                Convert.ToString(dr["path_regex"]),
                Convert.ToString(dr["path_format"]),
                (ItemPathType.AssetStoreType)Convert.ToInt32(dr["store_type"]),
                Convert.IsDBNull(dr["primary_type_id"]) ? ItemPathType.INVALID_PATH_TYPE_ID : Convert.ToInt32(dr["primary_type_id"]),
                Convert.ToString(dr["sha256_table"])
            );
        }

        /// <summary>
        /// AllocNextUniqueAssetId - allocate and return next unique asset ID for the specified path type
        /// </summary>
        private uint AllocNextUniqueAssetId(int pathTypeId)
        {
            string sqlString = "UPDATE unique_asset_ids SET `last_assigned_value` = @uniqueAssetId := `last_assigned_value` + 1 WHERE `item_path_type_id` = @itemPathTypeId; SELECT CAST(@uniqueAssetId as unsigned);";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@itemPathTypeId", pathTypeId));

            Object res = Db.WOK.GetScalar(sqlString, parameters);
            if (res != null)
            {
                try
                {
                    return Convert.ToUInt32(res);
                }
                catch (Exception ex) 
                {
                    m_logger.Error("GetNextUniqueAssetId: " + ex.ToString() + " while parsing result, pathTypeId=" + pathTypeId.ToString());
                }
            }
            else
            {
                m_logger.Error("GetNextUniqueAssetId: null result returned from DB, pathTypeId=" + pathTypeId.ToString());
            }

            return 0;
        }

        /// <summary>
        /// GetExistingUniqueAssetIdBySHA256 - return existing asset ID for matching hash. Return 0 if not found.
        /// </summary>
        public uint GetExistingUniqueAssetIdBySHA256(ItemPathType pathTypeInfo, byte[] hashSHA256)
        {
            string sqlString = "SELECT `id` FROM `" + pathTypeInfo.SHA256Table + "` WHERE `sha256` = @sha256 and `item_path_type_id` = @itemPathTypeId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@sha256", new MySqlBinaryString(hashSHA256)));
            parameters.Add(new MySqlParameter("@itemPathTypeId", pathTypeInfo.TypeId));

            Object res = Db.WOK.GetScalar(sqlString, parameters);
            if (res == null)
            {
                // Does not exist
                return 0;
            }

            try
            {
                return Convert.ToUInt32(res);
            }
            catch (Exception ex)
            {
                m_logger.Error("GetExistingUniqueAssetIdBySHA256: " + ex.ToString() + " while parsing result, pathTypeId=" + pathTypeInfo.TypeId.ToString());
            }

            return 0;
        }

        /// <summary>
        /// AllocUniqueAssetIdBySHA256 - allocate unique asset ID if hash is new, otherwise return existing asset ID
        /// </summary>
        public uint AllocUniqueAssetIdBySHA256(ItemPathType pathTypeInfo, byte[] hashSHA256, int size, int compressedSize, out bool isNew)
        {
            isNew = false;

            // Check if record exists. No locking is used here, assuming chance of race condition is low. Under worse case scenario, we lose a unique ID if race occurs.
            uint uniqueAssetId = GetExistingUniqueAssetIdBySHA256(pathTypeInfo, hashSHA256);
            if (uniqueAssetId != 0)
            {
                // Already exists
                return uniqueAssetId;
            }

            // Allocate new asset ID
            uniqueAssetId = AllocNextUniqueAssetId(pathTypeInfo.TypeId);
            Debug.Assert(uniqueAssetId > 0);
            if (uniqueAssetId <= 0)
            {
                m_logger.Error("AllocUniqueAssetIdBySHA256: error allocating next unique ID, pathTypeId=" + pathTypeInfo.TypeId.ToString());
                return 0;
            }

            string sqlString = 
                "SET @existingAssetId = 0; " +
                "INSERT INTO `" + pathTypeInfo.SHA256Table + "`(`item_path_type_id`, `id`, `sha256`, `size`, `compressed_size`) " + 
                "VALUES (@itemPathTypeId, @uniqueAssetId, @sha256, @size, @compressedSize) " +                // Attempt to insert hash
                "ON DUPLICATE KEY UPDATE `id` = @existingAssetId := `id`; " +   // If hash already exists, make a dummy update to get the existing id
                "SELECT CAST(@existingAssetId as unsigned)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@itemPathTypeId", pathTypeInfo.TypeId));
            parameters.Add(new MySqlParameter("@uniqueAssetId", uniqueAssetId));
            parameters.Add(new MySqlParameter("@sha256", new MySqlBinaryString(hashSHA256)));
            parameters.Add(new MySqlParameter("@size", size));
            parameters.Add(new MySqlParameter("@compressedSize", compressedSize));

            Object res = Db.WOK.GetScalar(sqlString, parameters);
            if (res == null)
            {
                m_logger.Error("AllocUniqueAssetIdBySHA256: null result returned, pathTypeId=" + pathTypeInfo.TypeId.ToString());
                return 0;
            }

            uint existingAssetId = 0;
            try
            {
                existingAssetId = Convert.ToUInt32(res);
            }
            catch (Exception ex) 
            {
                m_logger.Error("AllocUniqueAssetIdBySHA256: " + ex.ToString() + " while parsing existing asset ID, pathTypeId=" + pathTypeInfo.TypeId.ToString());
                return 0;
            }

            if (existingAssetId > 0)
            {
                // Race condition occurred - use existing asset ID
                return existingAssetId;
            }

            // New asset ID
            isNew = true;
            return uniqueAssetId;
        }

        /// <summary>
        /// GetItemParameter - select record in item parameter table
        /// </summary>
        public ItemParameter GetItemParameter (int globalId, int paramTypeId)
        {
            string sqlString = "SELECT value FROM item_parameters WHERE global_id=@globalId AND param_type_id=@paramTypeId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));
            parameters.Add (new MySqlParameter ("@paramTypeId", paramTypeId));

            DataRow dr = Db.WOK.GetDataRow (sqlString, parameters);
            if (dr != null)
            {
                return new ItemParameter (globalId, paramTypeId, Convert.ToString (dr["value"]));
            }

            return null;
        }

        /// <summary>
        /// UpdateItemParameter - add or update record in item parameter table
        /// </summary>
        public void UpdateItemParameter (int globalId, int paramTypeId, string value, bool propagateQueryExceptions = false)
        {
            string sqlString = "INSERT INTO item_parameters(global_id, param_type_id, value) VALUES (@globalId, @paramTypeId, @value) " +
                " ON DUPLICATE KEY UPDATE value=@value";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));
            parameters.Add (new MySqlParameter ("@paramTypeId", paramTypeId));
            parameters.Add (new MySqlParameter ("@value", value));

            Db.WOK.ExecuteNonQuery (sqlString, parameters, propagateQueryExceptions);
        }

        /// <summary>
        /// GetItemAnimations - Get animations belonging to an item
        /// </summary>
        public DataTable GetItemAnimations (int globalId, bool showPrivate)
        {
            string sql = "SELECT i.global_id,name,description,market_cost, " +
                " selling_price, required_skill, required_skill_level, use_type, " +
                " iw.display_name, IFNULL(pgi.pass_group_id,0) as passTypeId, " +
                " iw.date_added, " +
                " item_creator_id, iw.item_active, arm_anywhere, " +
                " disarmable, stackable, destroy_when_used, " +
                " inventory_type, actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " +
                " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
                " (i.market_cost + iw.designer_price + IFNULL(iw2.designer_price, 0) ) as WebPrice, " +
                " expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, iw.sales_total, iw.sales_designer_total, " +
                " i.is_derivable, i.derivation_level, IFNULL(iw2.designer_price, 0) as TemplateDesignerPrice, " +
                " iw.category_id1, iw.category_id2, iw.category_id3, IFNULL(gi.game_id,0) as game_id, IFNULL(gi.approval_status, 0) as approval_status " +
                " FROM wok.item_animations ia " +
                " INNER JOIN wok.items i ON i.global_id = ia.animation_id " +
                " INNER JOIN shopping.items_web iw ON i.global_id = iw.global_id " +
                " LEFT OUTER JOIN shopping.items_web iw2 on iw.base_global_id=iw2.global_id " + // get base item info
                " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id " +
                " LEFT OUTER JOIN wok.game_items gi ON i.global_id = gi.global_id " +
                " WHERE ia.item_id = @globalId ";

            if (!showPrivate)
            {
                sql += " AND iw.item_active = " + (int) WOKItem.ItemActiveStates.Public;
            } 
       // Commented out code so deleted items would be returned also so WOK can let the player use it but not buy it.
       //     else
       //     {
       //         sql += " AND iw.item_active <> " + (int) WOKItem.ItemActiveStates.Deleted;
       //     }

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));

            return Db.Search.GetDataTable (sql, parameters);
        }

        public DataTable GetAnimations (string globalIds)
        {
            string sql = "SELECT item_id as global_id, animation_id " +
                " FROM wok.item_animations " +
                " WHERE item_id IN (" + globalIds + ")";

            return Db.WOK.GetDataTable (sql);
        }

        public DataTable GetPasses(string globalIds)
        {
            string sql = "SELECT global_id, pass_group_id " +
                " FROM wok.pass_group_items " +
                " WHERE global_id IN (" + globalIds + ")";

            return Db.WOK.GetDataTable(sql);
        }

        public DataTable GetExclusionGroupIds(string globalIds)
        {
            string sql = "SELECT global_id, VALUE as exclusion_group_id " +
                         "  FROM wok.item_parameters " +
                         " WHERE global_id IN (" + globalIds + ") AND param_type_id = " + ItemParameter.PARAM_TYPE_EXCLUSION_GROUPS;

            return Db.WOK.GetDataTable(sql);
        }

        public DataTable GetSpaceDynamicObjectParams (string ids)
        {
            string sql = "SELECT obj_placement_id, param_type_id, value " +
                " FROM wok.dynamic_object_parameters " +
                " WHERE obj_placement_id IN (" + ids + ")";

            return Db.WOK.GetDataTable (sql);
        }

        public DataTable GetDynamicObjectParams (int objPlacementId)
        {
            string sql = "SELECT obj_placement_id, param_type_id, value " +
                " FROM wok.dynamic_object_parameters " +
                " WHERE obj_placement_id = @objPlacementId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@objPlacementId", objPlacementId));

            return Db.WOK.GetDataTable (sql, parameters);
        }

        public DataTable GetDynamicObjectPlaylists (int objPlacementId, int zoneIndex, int zoneInstanceId, int globalId)
        {
            string sql = "SELECT obj_placement_id, global_id, asset_group_id " +
                " FROM wok.dynamic_object_playlists " +
                " WHERE obj_placement_id = @objPlacementId " +
                " AND zone_index = @zoneIndex " +
                " AND zone_instance_id = @zoneInstanceId " +
                " AND global_id = @globalId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@objPlacementId", objPlacementId));
            parameters.Add(new MySqlParameter("@zoneIndex", zoneIndex));
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@globalId", globalId));

            return Db.WOK.GetDataTable (sql, parameters);
        }

        public DataRow GetDynamicObjectSoundCustomizations(int objPlacementId)
        {
            string sql = "SELECT obj_placement_id, pitch, gain, max_distance, roll_off, `loop`, loop_delay, " +
                " dir_x, dir_y, dir_z, cone_outer_gain, cone_inner_angle, cone_outer_angle " + 
                " FROM wok.sound_customizations " +
                " WHERE obj_placement_id = @objPlacementId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@objPlacementId", objPlacementId));

            return Db.WOK.GetDataRow(sql, parameters);
        }

        #region Premium Items

        /// <summary>
        /// GetPremiumItems
        /// </summary>
        public PagedList<WOKItem> GetPremiumItems (int gameId, int pageSize, int pageNumber, string orderBy, bool showPrivate, bool showNonApproved)
        {
            string strSelect = "SELECT i.global_id,name,description,market_cost, " +
                " selling_price, required_skill, required_skill_level, use_type, " +
                " iw.display_name, IF(ISNULL(pgi.pass_group_id),0,pass_group_id) as passTypeId, iw.date_added, " +
                " item_creator_id, iw.item_active, arm_anywhere, " +
                " disarmable, stackable, destroy_when_used, " +
                " inventory_type,actor_group, iw.base_global_id, iw.template_path, iw.texture_path, " +
                " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, " +
                " iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, selling_price as WebPrice, " +
                " selling_price as TemplateDesignerPrice, expired_duration, iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, " +
                " iw.sales_total, iw.sales_designer_total, i.is_derivable, i.derivation_level, " +
                " iw.category_id1, iw.category_id2, iw.category_id3, IFNULL(gi.game_id,0) as game_id, IFNULL(gi.approval_status, 0) as approval_status ";

            string strTables = " FROM wok.items i " +
                " INNER JOIN shopping.items_web iw ON i.global_id = iw.global_id " +
                " INNER JOIN wok.game_items gi ON gi.global_id = iw.global_id " +
                " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id ";

            string activeState = ((int) WOKItem.ItemActiveStates.Public).ToString ();
            if (showPrivate)
            {
                activeState += "," + ((int) WOKItem.ItemActiveStates.Private).ToString ();
            }

            string approvedState = ((int) WOKItem.ItemApprovalStatus.Approved).ToString ();
            if (showNonApproved)
            {
                approvedState += "," + ((int) WOKItem.ItemApprovalStatus.Pending).ToString () +
                    ", " + ((int) WOKItem.ItemApprovalStatus.Requested).ToString () +
                    ", " + ((int) WOKItem.ItemApprovalStatus.Denied).ToString ();
            }

            string strWhere = " WHERE iw.item_active IN (" + activeState + ") " +
                " AND iw.base_global_id > 0 " +
                " AND gi.approval_status IN (" + approvedState + ") " +
                " AND gi.game_id = @gameId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@gameId", gameId));

            PagedDataTable dt = Db.Search.GetPagedDataTable (strSelect + strTables + strWhere, orderBy, parameters, pageNumber, pageSize);

            PagedList<WOKItem> list = new PagedList<WOKItem> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new WOKItem (Convert.ToInt32 (row["global_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToInt32 (row["market_cost"]),
                Convert.ToInt32 (row["selling_price"]), Convert.ToInt32 (row["required_skill"]), Convert.ToInt32 (row["required_skill_level"]), Convert.ToInt32 (row["use_type"]),
                row["display_name"].ToString (), Convert.ToInt32 (row["passTypeId"]), Convert.ToDateTime (row["date_added"]),
                Convert.ToUInt32 (row["item_creator_id"]), Convert.ToInt32 (row["item_active"]), Convert.ToInt32 (row["arm_anywhere"]),
                Convert.ToInt32 (row["disarmable"]), Convert.ToInt32 (row["stackable"]), Convert.ToInt32 (row["destroy_when_used"]),
                Convert.ToInt32 (row["inventory_type"]), Convert.ToUInt32 (row["base_global_id"]), row["template_path"].ToString (), row["texture_path"].ToString (),
                row["thumbnail_path"].ToString (), row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (), row["thumbnail_assetdetails_path"].ToString (), Convert.ToUInt32 (row["designer_price"]),
                row["keywords"].ToString (), Convert.ToInt32 (row["WebPrice"]), Convert.ToInt32 (row["expired_duration"]), Convert.ToUInt32 (row["number_sold_on_web"]), Convert.ToUInt32 (row["number_of_raves"]),
                Convert.ToUInt32 (row["number_of_views"]), Convert.ToUInt32 (row["sales_total"]), Convert.ToUInt32 (row["sales_designer_total"]),
                Convert.ToInt32 (row["is_derivable"]), Convert.ToInt32 (row["derivation_level"]), Convert.ToUInt32 (row["TemplateDesignerPrice"]),
                Convert.ToUInt32 (row["category_id1"]), Convert.ToUInt32 (row["category_id2"]), Convert.ToUInt32 (row["category_id3"]),
                Convert.ToInt32 (row["actor_group"]), Convert.ToInt32 (row["game_id"]), (WOKItem.ItemApprovalStatus) Convert.ToInt16 (row["approval_status"])
                ));
            }
            return list;
        }

        /// <summary>
        /// DeletePremiumItem
        /// </summary>
        public int DeletePremiumItem (int globalId)
        {
            // Mark as inactive
            string sql = "UPDATE items_web SET " +
                " item_active = @itemActive " +
                " WHERE global_id = @globalId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));
            parameters.Add (new MySqlParameter ("@itemActive", (int) WOKItem.ItemActiveStates.Deleted));
            return Db.Shopping.ExecuteNonQuery (sql, parameters);
        }

        public int UpdatePremiumItemApprovalStatus (WOKItem item)
        {
            string sql = "UPDATE game_items SET " +
                " approval_status = @approval_status " +
                " WHERE global_id  =  @global_id " +
                " AND game_id = @game_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@approval_status", (int) item.ApprovalStatus));
            parameters.Add (new MySqlParameter ("@global_id", item.GlobalId));
            parameters.Add (new MySqlParameter ("@game_id", item.GameId));
            return Db.WOK.ExecuteNonQuery (sql, parameters);
        }

        public int UpdateItemDeniedReason (int globalId, string desc)
        {
            string sql = "INSERT INTO item_approval_denials " +
                " VALUES (@global_id, @desc) " +
                " ON DUPLICATE KEY UPDATE reason_description=@desc";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@global_id", globalId));
            parameters.Add (new MySqlParameter ("@desc", desc));
            return Db.WOK.ExecuteNonQuery (sql, parameters);
        }

        public string GetItemDeniedReason (int globalId)
        {
            string sql = "SELECT global_id, reason_description " +
                " FROM item_approval_denials " +
                " WHERE global_id = @globalId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalid", globalId));
            return Db.WOK.GetDataRow (sql, parameters)["reason_description"].ToString ();
        }

        public int InsertPremiumItemCreditRedemption (int purchaseId, int communityId)
        {
            string sqlString = "INSERT INTO premium_item_credit_redemption ( purchase_id, community_id ) " +
                " VALUES ( @purchaseId, @communityId )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@purchaseId", purchaseId));
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        #endregion Premium Items


        #region Category Management

        /// <summary>
        /// get a list of all WOK items. Limited data returned
        /// </summary>
        public List<WOKItem> GetWOKItemsList ()
        {
            //attach primary key to columns and attach items table to columns to form SQL string
            string sql = "SELECT i.global_id, iw.display_name, i.name, i.description " +
                 " FROM items i INNER JOIN shopping.items_web iw ON i.global_id = iw.global_id " +
                 " ORDER BY iw.display_name ASC";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            DataTable dt = Db.WOK.GetDataTable (sql, parameters);

            List<WOKItem> list = new List<WOKItem> ();
            foreach (DataRow row in dt.Rows)
            {
                WOKItem item = new WOKItem ();
                item.GlobalId = Convert.ToInt32 (row["global_id"]);
                item.DisplayName = row["display_name"].ToString ();
                item.Name = row["name"].ToString ();
                item.Description = row["description"].ToString ();

                list.Add (item);
            }

            return list;
        }

        public DataTable GetWOKCategoryItemsByCategoryId (int categoryId)
        {
            return GetWOKCategoryItems (-1, categoryId);
        }

        /// <summary>
        /// GetWOKCategoryItemsByCategoryIdList - temporary solution until  a descision is made on how best
        /// place the items_to_Categories and item_categories tables
        /// </summary>
        /// <param name="categoryIdList"></param>
        /// <returns></returns>
        public DataTable GetWOKCategoryItems (int itemID, int categoryId)
        {
            string sql = "SELECT global_id, item_category_id FROM item_category_items itc ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            //limit by item id if one is provided
            if (itemID > 0)
            {
                sql += " WHERE itc.global_id = @itemId ";
                parameters.Add (new MySqlParameter ("@itemId", itemID));
            }

            //limit by category id if one is provided
            if ((categoryId > 0) && (itemID <= 0))
            {
                sql += " WHERE itc.item_category_id = @categoryId ";
                parameters.Add (new MySqlParameter ("@categoryId", categoryId));
            }
            else if (((categoryId > 0) && (itemID > 0)))
            {
                sql += " AND itc.item_category_id = @categoryId ";
                parameters.Add (new MySqlParameter ("@categoryId", categoryId));
            }

            sql += " ORDER BY item_category_id, global_id ASC";


            return Db.Shopping.GetDataTable (sql, parameters);
        }

        /// <summary>
        /// AddCategoryToItem
        /// </summary>
        public int AddCategoryToItem (int globalId, uint ItemCategoryId, int categoryIndex)
        {
            string sqlString = "UPDATE items_web " +
            " SET category_id" + categoryIndex.ToString () + " =  @ItemCategoryId " +
            " WHERE global_id = @globalId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));
            parameters.Add (new MySqlParameter ("@ItemCategoryId", ItemCategoryId));
            return Db.Shopping.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// InsertWOKCategory - inserts a new category
        /// </summary>
        /// <returns></returns>
        public int InsertWOKCategory (string category, int parentCategoryId, string description, int userId, string marketingPath)
        {
            // Record User Asset Subscriptions
            /*string sqlInsert = "INSERT INTO item_categories " +
                "(name, description, parent_category_id, modifiers_id) VALUES (@category, @parentCategoryId, @description, @modifiers_id)";*/
            string sqlInsert = "INSERT INTO item_categories " +
                "(name, description, parent_category_id, marketing_path) VALUES (@category, @description, @parentCategoryId, @marketingPath)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@category", category));
            parameters.Add (new MySqlParameter ("@parentCategoryId", parentCategoryId));
            parameters.Add (new MySqlParameter ("@description", description));
            parameters.Add (new MySqlParameter ("@marketingPath", marketingPath));

            return Db.Shopping.ExecuteIdentityInsert (sqlInsert, parameters);
        }

        /// <summary>
        /// UpdateWOKCategory - updates WOK categories 
        /// </summary>
        /// <param name="featuredAssetId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public int UpdateWOKCategory (int categoryId, string category, int parentCategoryId, string description, int userId, string marketingPath)
        {
            string sqlUpdate = "UPDATE item_categories ic SET " +
                "name = @category, " +
                "parent_category_id = @parentCategoryId, " +
                "description = @description, " +
                "marketing_path = @marketingPath " +
                " WHERE item_category_id = @categoryId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@category", category));
            parameters.Add (new MySqlParameter ("@parentCategoryId", parentCategoryId));
            parameters.Add (new MySqlParameter ("@description", description));
            parameters.Add (new MySqlParameter ("@marketingPath", marketingPath));
            parameters.Add (new MySqlParameter ("@categoryId", categoryId));
            return Db.Shopping.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// MoveWOKCategoryItems - moves WOK Category Items from one category to another 
        /// </summary>
        /// <param name="oldCategoryId"></param>
        /// <param name="newCategoryId"></param>
        /// <returns></returns>
        public int MoveWOKCategoryItem (int oldCategoryId, int newCategoryId, int itemId)
        {
            string sql = "UPDATE items_web SET " +
                "category_id1 = @newCategoryId " +
                "WHERE category_id1 = @oldCategoryId AND global_id = @itemId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@oldCategoryId", oldCategoryId));
            parameters.Add (new MySqlParameter ("@newCategoryId", newCategoryId));
            parameters.Add (new MySqlParameter ("@itemId", itemId));
            Db.Shopping.ExecuteNonQuery (sql, parameters);

            sql = "UPDATE items_web SET " +
                "category_id2 = @newCategoryId " +
                "WHERE category_id2 = @oldCategoryId AND global_id = @itemId ";
            Db.Shopping.ExecuteNonQuery (sql, parameters);

            sql = "UPDATE items_web SET " +
                "category_id3 = @newCategoryId " +
                "WHERE category_id3 = @oldCategoryId AND global_id = @itemId ";
            return Db.Shopping.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// UpdateCategorysParent
        /// </summary>
        public int UpdateCategorysParent (int categoryId, int parentCategoryId)
        {
            // Send the message
            string sql = "UPDATE item_categories ic SET " +
                "parent_category_id = @parentCategoryId " +
                " WHERE parent_category_id = @categoryId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@parentCategoryId", parentCategoryId));
            parameters.Add (new MySqlParameter ("@categoryId", categoryId));
            return Db.Shopping.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// DeleteWOKCategoryItems - deletes all items from the category
        /// table that are under a certain category
        /// </summary>
        public int DeleteWOKCategoryItems (int categoryId)
        {
            //delete from items_to_categories table
            string sql = "UPDATE items_web SET category_id1 = 0 WHERE category_id1 = @categoryId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@categoryId", categoryId));
            Db.Shopping.ExecuteNonQuery (sql, parameters);

            //delete from items_to_categories table
            sql = "UPDATE items_web SET category_id2 = 0 WHERE category_id2 = @categoryId ";
            Db.Shopping.ExecuteNonQuery (sql, parameters);

            //delete from items_to_categories table
            sql = "UPDATE items_web SET category_id3 = 0 WHERE category_id3 = @categoryId ";
            return Db.Shopping.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// DeleteCategoryFromItem
        /// </summary>
        public int DeleteCategoryFromItem (int globalId, int categoryIndex)
        {
            string sql = "UPDATE items_web " +
              " SET category_id" + categoryIndex.ToString () + " =  0 " +
              " WHERE global_id = @globalId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@globalId", globalId));
            return Db.Shopping.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// DeleteCategory
        /// </summary>
        public int DeleteCategory (int categoryId)
        {
            string sql = "DELETE FROM item_categories WHERE item_category_id = @categoryId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@categoryId", categoryId));
            return Db.Shopping.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// GetOriginalItem - Get original item which a derived item is based on
        /// </summary>
        public int GetBaseItemGlobalId (int globalId)
        {
            string sql = "SELECT i2.global_id FROM items i, items i2 WHERE i.base_global_id=i2.base_global_id AND i.base_global_id<>0 AND i.global_id=" + globalId.ToString () + " AND i2.derivation_level=0 LIMIT 1";
            DataRow dr = Db.WOK.GetDataRow (sql);
            if (dr != null)
                return Convert.ToInt32 (dr["global_id"]);

            return 0;
        }

        /// <summary>
        /// used by STAR for syncing items
        /// GetItems for sync, if only globalId non-zero it gets only that item
        /// otherwise it gets all items greater than each id
        /// the ugcId and templateId should be validated for range before calling
        /// </summary>
        /// <param name="globalId"></param>
        /// <param name="ugcId"></param>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public DataTable GetItems (int id, int ugcId, int templateId, int count)
        {

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@id", id));

            string sql = "SELECT *, IFNULL((SELECT VALUE FROM wok.item_parameters WHERE param_type_id = 10 AND global_id = @id), 0) misc_use_value " +
                         " FROM  wok.items";

            // first case, they tell us highest in each range, so will be one less than min values in ranges
            // the ugcId and templateId should be validated before calling
            if (id > 0 && ugcId > 0 && templateId > 0)
            {
                Debug.Assert (ugcId >= WOKItem.UGC_BASE_GLID - 1);
                Debug.Assert (templateId >= WOKItem.UGC_BASE_TEMPLATE_GLID - 1);

                parameters.Add (new MySqlParameter ("@ugcId", ugcId));
                parameters.Add (new MySqlParameter ("@templateId", templateId));
                parameters.Add (new MySqlParameter ("@count", count));
                sql += " WHERE global_id > @id and global_id < " + WOKItem.UGC_BASE_GLID.ToString () + " or " +
                         "       global_id > @ugcId and global_id < " + WOKItem.UGC_BASE_TEMPLATE_GLID.ToString () + " or " +
                         "       global_id > @templateId" +
                         " LIMIT @count";
            }
            else if (count > 1) // they want a block of items starting with count
            {
                parameters.Add (new MySqlParameter ("@count", count));
                sql += " WHERE global_id > @id" +
                        " LIMIT @count";
            }
            else // just want 1 item, send it and base item, if it has it
            {
                sql += " WHERE global_id = @id" +
                        " UNION" +
                        " SELECT i.*, IFNULL((SELECT VALUE FROM wok.item_parameters WHERE param_type_id = 10 AND global_id = @id), 0) misc_use_value  " +
                        " FROM wok.items i" +
                        " INNER JOIN wok.items i2 ON i2.base_global_id = i.global_id" +
                        " WHERE i2.global_id =  @id AND i2.base_global_id <> 0;";
            }



            return Db.Shopping.GetDataTable (sql, parameters);
        }

        
        /// <summary>
        /// get space based on the playerId, zoneIndex, and instanceId
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public DataTable GetSpaceDynamicObjects (int playerId, int zoneIndex, int instanceId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string sql = "SELECT * FROM wok.dynamic_objects " + 
             " WHERE  zone_index = @zoneIndex AND zone_instance_id = @instanceId AND expired_date IS NULL";

            if (playerId > 0)
            {
                sql += " AND player_id = @playerId";
                parameters.Add (new MySqlParameter ("@playerId", playerId));
            }
            
            parameters.Add (new MySqlParameter ("@zoneIndex", zoneIndex));
            parameters.Add (new MySqlParameter ("@instanceId", instanceId));

            return Db.Shopping.GetDataTable (sql, parameters);
        }
        /// <summary>
        /// get space world object settings based on the userId, zoneIndex and instanceId
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public DataTable GetSpaceWorldObjectSettings (int userId, int zoneIndex, int instanceId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string sql = "SELECT wo.* " +
                "FROM wok.world_object_player_settings wo " +
                "INNER JOIN wok.channel_zones cz ON cz.zone_index=wo.zone_index AND cz.zone_instance_id=wo.zone_instance_id " +
                "INNER JOIN wok.players p ON cz.kaneva_user_id=p.kaneva_user_id " +
                "WHERE wo.zone_index = @zoneIndex AND wo.zone_instance_id = @instanceId";

            if (userId > 0)
            {
                sql += " AND p.player_id = @userId";
                parameters.Add (new MySqlParameter ("@userId", userId));
            }

           
            parameters.Add (new MySqlParameter ("@zoneIndex", zoneIndex));
            parameters.Add (new MySqlParameter ("@instanceId", instanceId));

            return Db.Shopping.GetDataTable (sql, parameters);
        }

        /// <summary>
        /// CreateCustomZoneTempate
        /// <param name="isGM">This is TEMPORARY, put in place to allow game item copying for templating without having to address UGC zones.</param>
        /// </summary>
        public int CreateCustomZoneTemplate(int zoneIndex, int instanceId, int templateId, bool isAdmin)
        {
            string sql = "CALL create_custom_zone_template(@zoneIndex, @instanceId,@templateId,@isAdmin)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneIndex", zoneIndex));
            parameters.Add(new MySqlParameter("@instanceId", instanceId));
            parameters.Add(new MySqlParameter("@templateId", templateId));
            parameters.Add(new MySqlParameter("@isAdmin", isAdmin));
            return Db.WOK.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// ChangeZoneMap
        /// </summary>
        public int ChangeZoneMap(int oldZoneIndex, int zoneInstanceId, int newZoneIndex, string playerName, int templateId,
            string gameNameNoSpaces, ref int result, ref string url)
        {
            string sql = "CALL change_zone_map (@oldZoneIndex, @zoneInstanceId,@newZoneIndex, @playerName, @templateId, " +
            "@gameNameNoSpaces, @result, @url); SELECT CAST(@result as UNSIGNED INT) as result, @url;";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@oldZoneIndex", oldZoneIndex));
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@newZoneIndex", newZoneIndex));
            parameters.Add(new MySqlParameter("@playerName", playerName));
            parameters.Add(new MySqlParameter("@templateId", templateId));
            parameters.Add(new MySqlParameter("@gameNameNoSpaces", gameNameNoSpaces));
            DataRow row = Db.WOK.GetDataRow (sql, parameters);

            if (row == null)
            {
                // Logging for https://kaneva.atlassian.net/browse/ED-2033
                m_logger.Error(string.Format("change_zone_map failed. Parameters: @oldZoneIndex = {0}, @zoneInstanceId = {1}, @newZoneIndex = {2}, " +
                    "@playerName = {3}, @templateId = {4}, @gameNameNoSpaces = {5}", oldZoneIndex, zoneInstanceId, newZoneIndex,
                    playerName, templateId, gameNameNoSpaces));

                return -1;
            }

            result = Convert.ToInt32(row["result"]);
            url = row["@url"].ToString();

            return result;
        }

        /// <summary>
        /// ChangeZoneMap
        /// </summary>
        public int ImportZone (int playerId, int oldZoneIndex, int oldZoneInstanceId, int baseZoneIndexPlain, 
            int srcZoneIndex, int srcZoneInstanceId, bool maintainScripts, ref int result, ref int newZoneIndex)
        {
            string sql = "CALL import_channel (@playerId, @oldZoneIndex, @oldZoneInstanceId, @baseZoneIndexPlain, " +
            "@srcZoneIndex, @srcZoneInstanceId, @maintainScripts, @result, @newZoneIndex); " +
            "SELECT CAST(@result as UNSIGNED INT) as result, CAST(@newZoneIndex as UNSIGNED INT) as newZoneIndex;";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@playerId", playerId));
            parameters.Add (new MySqlParameter ("@oldZoneIndex", oldZoneIndex));
            parameters.Add (new MySqlParameter ("@oldZoneInstanceId", oldZoneInstanceId));
            parameters.Add (new MySqlParameter ("@baseZoneIndexPlain", baseZoneIndexPlain));
            parameters.Add (new MySqlParameter ("@srcZoneIndex", srcZoneIndex));
            parameters.Add (new MySqlParameter ("@srcZoneInstanceId", srcZoneInstanceId));
            parameters.Add (new MySqlParameter ("@maintainScripts", maintainScripts));
            DataRow row = Db.WOK.GetDataRow (sql, parameters);

            result = Convert.ToInt32 (row["result"]);
            newZoneIndex = Convert.ToInt32 (row["newZoneIndex"]);

            return result;
        }

        /// <summary>
        /// Inserts a new channel zone record. Replaces WOKStoreUtility.InsertChannelZone.
        /// </summary>
        /// <returns></returns>
        public int InsertChannelZone(int userId, int communityId, int zoneIndex, int instanceId, int zoneType, string name, int serverId, string tiedToServer, string country, int age, int scriptServerId)
        {
            int recordsInserted = 0;

            string sqlInsert = "INSERT INTO wok.channel_zones (kaneva_user_id, zone_index, zone_index_plain, zone_type, zone_instance_id, name, server_id, tied_to_server, created_date, country, max_age, script_server_id ) " +
                               "VALUES ( @kanevaUserId, @zoneIndex, wok.zoneIndex(@zoneIndex), wok.zoneType(@zoneIndex), @instanceId,  @friendlyName, @serverId, @tiedToServer,  NOW(), @country, wok.calc_max_age_group(@age), @scriptServerId); ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter ("@kanevaUserId", userId));
            parameters.Add(new MySqlParameter ("@zoneIndex", zoneIndex));
            parameters.Add(new MySqlParameter ("@instanceId", instanceId));
            parameters.Add(new MySqlParameter ("@friendlyName", name));
            parameters.Add(new MySqlParameter ("@serverId", serverId));
            parameters.Add(new MySqlParameter ("@tiedToServer", tiedToServer));
            parameters.Add(new MySqlParameter ("@country", country));
            parameters.Add(new MySqlParameter ("@age", age));
            parameters.Add(new MySqlParameter ("@scriptServerId", scriptServerId));

            if (communityId > 0)
            {
                sqlInsert += "INSERT INTO wok.unified_world_ids (community_id, world_type, zone_index, zone_index_plain, zone_type, zone_instance_id) " +
                             "VALUES (@communityId, @worldType, @zoneIndex, wok.zoneIndex(@zoneIndex), wok.zoneType(@zoneIndex), @instanceId);";
                parameters.Add(new MySqlParameter("@communityId", communityId));
                parameters.Add(new MySqlParameter("@worldType", (zoneType == (int)WOK3DPlace.eZoneType.HOUSING ? "Home" : "Community")));
            }

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    recordsInserted = Db.WOK.ExecuteNonQuery(sqlInsert, parameters, true);
                    transaction.Complete();
                }
            }
            catch(Exception e)
            {
                m_logger.Error("MySQLShoppingDao.InsertChannelZone(): Error inserting channel_zones record.", e);
            }

            return recordsInserted;
        }

        public int CopyGlid(int originalGlid)
        {
            string sql = "CALL copy_item_new(@originalGlid)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@originalGlid", originalGlid));

            DataSet ds = Db.WOK.GetDataSet(sql, parameters);

            // If copy failed, return error
            if (ds == null || ds.Tables.Count != 2 || ds.Tables[1].Rows.Count != 1)
            {
                return 0;
            }

            // First result is not used
            return Convert.ToInt32(ds.Tables[1].Rows[0]["newcopy"]);
        }

        public DataRow GetItemByPlacementId(int objPlacementId, int zoneInstanceId, int playerId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sql = "SELECT * " +
                "FROM wok.dynamic_objects " +
                "WHERE obj_placement_id = @objPlacementId AND zone_instance_id = @zoneInstanceId" +
                " AND player_id = @playerId";

            parameters.Add(new MySqlParameter("@playerId", playerId));
            parameters.Add(new MySqlParameter("@objPlacementId", objPlacementId));
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));

            return Db.Shopping.GetDataRow(sql, parameters);
        }

        #endregion

        /// <summary>
        /// CreateItemPreloadList - create a new record in item_preloads table and return list ID. Return value is -1 if creation failed.
        /// </summary>
        public int CreateItemPreloadList(string name)
        {
            const uint numRetries = 3;
            for (uint i = 0; i<numRetries; i++)
            {
                int maxListId = GetMaxItemPreloadListId();
                if (maxListId == -1)
                {
                    // Error occurred
                    m_logger.Error("CreateItemPreloadList: error obtaining max list ID");
                    return -1;
                }

                int newListId = maxListId + 1;  // Minimum valid list ID is 1

                string sql = "INSERT IGNORE INTO `item_preloads`(`list_id`, `name`) VALUES (@ListId, @Name)";
                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@ListId", newListId));
                parameters.Add(new MySqlParameter("@Name", name));

                int rowsAffected = Db.WOK.ExecuteNonQuery(sql, parameters);
                if (rowsAffected > 0)
                {
                    return newListId;
                }
            }

            m_logger.Error("CreateItemPreloadList: error creating new list ID for \"" + name.ToString() + "\"");
            return -1;
        }

        /// <summary>
        /// UpdateItemPreloadList
        /// </summary>
        public bool UpdateItemPreloadList(int listId, string newName)
        {
            string sql = "UPDATE `item_preloads` SET `name`=@Name WHERE `list_id`=@ListId";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@ListId", listId));
            parameters.Add(new MySqlParameter("@Name", newName));

            int rowsAffected = Db.WOK.ExecuteNonQuery(sql, parameters);
            return rowsAffected > 0;
        }

        /// <summary>
        /// PublishItemPreloadList - update a existing item_preloads record and assign it with a valid version number. Return value is -1 if assignment failed.
        /// </summary>
        public int PublishItemPreloadList(int listId)
        {
            const uint numRetries = 3;
            for (uint i = 0; i < numRetries; i++)
            {
                int maxVer = GetLatestItemPreloadListVersion();
                if (maxVer < 0)
                {
                    // Error occurred
                    m_logger.Error("PublishItemPreloadList: error obtaining latest list version");
                    return -1;
                }

                int newVer = maxVer + 1;

                // UPDATE with a left join to make sure the new version is not used by other existing records due to race condition
                string sql = "UPDATE `item_preloads` ipl LEFT OUTER JOIN `item_preloads` ipl2 ON ipl2.`ver`=@Ver " + 
                             "SET ipl.`ver` = @Ver, ipl.published_date = now() " + 
                             "WHERE ipl.`list_id` = @ListId AND ipl2.`list_id` IS NULL";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@ListId", listId));
                parameters.Add(new MySqlParameter("@Ver", newVer));

                int rowsAffected = Db.WOK.ExecuteNonQuery(sql, parameters);
                if (rowsAffected > 0)
                {
                    return newVer;
                }
            }

            m_logger.Error("PublishItemPreloadList: error assigning new version number for list " + listId.ToString());
            return -1;
        }

        /// <summary>
        /// GetAllItemPreloadLists()
        /// </summary>
        public DataTable GetAllItemPreloadLists()
        {
            string sql = "SELECT `list_id`, `name`, `ver`, `created_date`, `published_date` FROM `item_preloads` ORDER BY `list_id` DESC";
            return Db.WOK.GetDataTable(sql);
        }

        /// <summary>
        /// GetItemPreloadList
        /// </summary>
        public DataRow GetItemPreloadList(int listId)
        {
            string sql = "SELECT `list_id`, `name`, `ver`, `created_date`, `published_date` FROM `item_preloads` WHERE `list_id` = @ListId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@ListId", listId));

            return Db.WOK.GetDataRow(sql, parameters);
        }

        /// <summary>
        /// GetMaxItemPreloadListId
        /// </summary>
        public int GetMaxItemPreloadListId()
        {
            string sql = "SELECT COALESCE(max(`list_id`), 0) `max_id` FROM `item_preloads`";
            var res = Db.WOK.GetScalar(sql);
            if (Convert.IsDBNull(res))
            {
                m_logger.Error("GetMaxItemPreloadListId: SQL query failed");
                return -1;
            }

            try
            {
                int maxListId = Convert.ToInt32(res);
                if (maxListId < 0)
                {
                    m_logger.Error("GetMaxItemPreloadListId: current max list ID is negative: " + maxListId.ToString());
                    return -1;
                }

                return maxListId;
            }
            catch (Exception)
            {
                m_logger.Error("GetMaxItemPreloadListId: error converting max list ID to int: " + res.ToString());
                return -1;
            }
        }

        /// <summary>
        /// GetLatestItemPreloadListVersion
        /// </summary>
        public int GetLatestItemPreloadListVersion()
        {
            string sql = "SELECT COALESCE(max(`ver`), 0) `max_ver` FROM `item_preloads`";
            var res = Db.WOK.GetScalar(sql);
            if (Convert.IsDBNull(res))
            {
                m_logger.Error("GetLatestItemPreloadListVersion: SQL query failed");
                return -1;
            }

            try
            {
                int maxListId = Convert.ToInt32(res);
                if (maxListId < 0)
                {
                    return 0;
                }

                return maxListId;
            }
            catch (Exception)
            {
                m_logger.Error("GetLatestItemPreloadListVersion: error converting max list ID to int: " + res.ToString());
                return -1;
            }
        }

        /// <summary>
        /// AddItemPreloadListItem - add a new global ID to a preload list
        /// </summary>
        public void AddItemPreloadListItem(int listId, int globalId)
        {
            // Validate arguments
            string sql = 
                "SELECT ipl.`list_id`, i.`global_id` `item_global_id`, ipi.`global_id` `list_global_id` " + 
                "FROM `item_preloads` ipl " + 
                "LEFT OUTER JOIN `items` i ON i.`global_id`=@GlobalId " +
                "LEFT OUTER JOIN `item_preload_items` ipi ON ipl.`list_id` = ipi.`list_id` AND ipi.`global_id`=@GlobalId " + 
                "WHERE ipl.`list_id` = @ListId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@ListId", listId));
            parameters.Add(new MySqlParameter("@GlobalId", globalId));

            var row = Db.WOK.GetDataRow(sql, parameters);
            if (row == null)
            {
                string msg = "Invalid list ID: " + listId.ToString();
                m_logger.Error("AddItemPreloadListItem: " + msg);
                throw new Exception(msg);
            }

            if (Convert.IsDBNull(row["item_global_id"]))
            {
                // Global ID not found in items table
                string msg = "Item " + globalId.ToString() + " is invalid";
                m_logger.Error("AddItemPreloadListItem: " + msg);
                throw new Exception(msg);
            }

            if (!Convert.IsDBNull(row["list_global_id"]))
            {
                // Global ID already exists in item_preload_items table
                string msg = "Item " + globalId.ToString() + " already exists in the list: " + listId.ToString();
                m_logger.Error("AddItemPreloadListItem: " + msg);
                throw new Exception(msg);
            }

            // Have to recreate MySqlParameter even if they are the same as previous query
            parameters.Clear();
            parameters.Add(new MySqlParameter("@ListId", listId));
            parameters.Add(new MySqlParameter("@GlobalId", globalId));

            sql = "INSERT IGNORE INTO `item_preload_items`(`list_id`, `global_id`) VALUES (@ListId, @GlobalId)";
            int rowsAffected = Db.WOK.ExecuteNonQuery(sql, parameters);
            if (rowsAffected == 0)
            {
                // Other unknown error
                string msg = "Error adding item " + globalId.ToString() + " into item preload list: " + listId.ToString();
                m_logger.Error("AddItemPreloadListItem: " + msg);
                throw new Exception(msg);
            }
        }

        /// <summary>
        /// RemoveItemPreloadListItem - remove a new global ID from a preload list. Throws one of three possible exceptions: listId is invalid, globalId does not exists in the list or globalId is invalid.
        /// </summary>
        public void RemoveItemPreloadListItem(int listId, int globalId)
        {
            // Validate arguments
            string sql = 
                "SELECT ipl.`list_id`, i.`global_id` `item_global_id`, ipi.`global_id` `list_global_id` " + 
                "FROM `item_preloads` ipl " + 
                "LEFT OUTER JOIN `items` i ON i.`global_id`=@GlobalId " +
                "LEFT OUTER JOIN `item_preload_items` ipi ON ipl.`list_id` = ipi.`list_id` AND ipi.`global_id`=@GlobalId " + 
                "WHERE ipl.`list_id` = @ListId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@ListId", listId));
            parameters.Add(new MySqlParameter("@GlobalId", globalId));

            var row = Db.WOK.GetDataRow(sql, parameters);
            if (row == null)
            {
                string msg = "Invalid list ID: " + listId.ToString();
                m_logger.Error("RemoveItemPreloadListItem: " + msg);
                throw new Exception(msg);
            }

            if (Convert.IsDBNull(row["item_global_id"]))
            {
                // Global ID not found in items table
                string msg = "Item " + globalId.ToString() + " is invalid";
                m_logger.Error("RemoveItemPreloadListItem: " + msg);
                throw new Exception(msg);
            }

            if (Convert.IsDBNull(row["list_global_id"]))
            {
                // Global ID already exists in item_preload_items table
                string msg = "Item " + globalId.ToString() + " does not exists in the list: " + listId.ToString();
                m_logger.Error("RemoveItemPreloadListItem: " + msg);
                throw new Exception(msg);
            }

            // Have to recreate MySqlParameter even if they are the same as previous query
            parameters.Clear();
            parameters.Add(new MySqlParameter("@ListId", listId));
            parameters.Add(new MySqlParameter("@GlobalId", globalId));

            sql = "DELETE FROM `item_preload_items` WHERE `list_id` = @ListId AND `global_id` = @GlobalId";
            int rowsAffected = Db.WOK.ExecuteNonQuery(sql, parameters);
            if (rowsAffected == 0)
            {
                // Other unknown error
                string msg = "Error removing item " + globalId.ToString() + " from item preload list: " + listId.ToString();
                m_logger.Error("RemoveItemPreloadListItem: " + msg);
                throw new Exception(msg);
            }
        }

        /// <summary>
        /// MergeItemPreloadListItems - merge all items from another list
        /// </summary>
        public int MergeItemPreloadListItems(int dstListId, int srcListId)
        {
            string sql = "INSERT IGNORE INTO `item_preload_items`(`list_id`, `global_id`) SELECT @DstListId, `global_id` FROM `item_preload_items` WHERE `list_id`=@SrcListId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@DstListId", dstListId));
            parameters.Add(new MySqlParameter("@SrcListId", srcListId));

            return Db.WOK.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// GetItemPreloadListItems()
        /// </summary>
        public DataTable GetItemPreloadListItems(int listId)
        {
            string sql = "SELECT i.`global_id`, i.`name`, i.`item_creator_id`, i.`use_type` FROM `item_preload_items` ipi INNER JOIN items i ON ipi.`global_id` = i.`global_id` WHERE ipi.`list_id` = @ListId ORDER BY i.`global_id`";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@ListId", listId));

            return Db.WOK.GetDataTable(sql, parameters);
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
    }
}
