///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLKachingDao : IKachingDao
    {
        /// <summary>
        /// GetLeaderboard
        /// </summary>
        /// <returns></returns>
        public DataTable GetLeaderboardDay (string sortOrder, int numResults)
        {   
            string strQuery = "SELECT p.kaneva_user_id, skl.player_id, p.name " +
                    " ,skl.wins, skl.kills, skl.deaths, skl.coins " +
                    " , ratio_display_fraction as ratio " +
                    " , kills_to_deaths_ratio as KillsToDeathsRatio " +
                    " FROM report_kaneva.summary_kaching_leader_by_day skl " +
                    " INNER JOIN wok.players p on skl.player_id = p.player_id " +
                    " WHERE cur_date > ADDDATE(NOW(),INTERVAL -1 DAY) " +
                    sortOrder + " LIMIT @numResults ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@numResults", numResults));

            return Db.Stats.GetDataTable(strQuery, parameters);
        }
         
        /// <summary>
        /// GetLeaderboard
        /// </summary>
        /// <returns></returns>
        public DataTable GetLeaderboardWeek (string sortOrder, int numResults)
        {
            string strQuery = "SELECT p.kaneva_user_id, skl.player_id, p.name " +
                    " ,skl.wins, skl.kills, skl.deaths, skl.coins " +
                    " , ratio_display_fraction as ratio " +
                    " , kills_to_deaths_ratio as KillsToDeathsRatio " +
                    " FROM report_kaneva.summary_kaching_leader_by_last7days skl " +
                    " INNER JOIN wok.players p on skl.player_id = p.player_id " +
                    sortOrder + " LIMIT @numResults ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@numResults", numResults));

            return Db.Stats.GetDataTable(strQuery, parameters);
        }

        /// <summary>
        /// GetLeaderboard
        /// </summary>
        /// <returns></returns>
        public DataTable GetLeaderboardAllTime (string sortOrder, int numResults)
        {
            string strQuery = "SELECT p.kaneva_user_id, skl.player_id, p.name " +
                    " ,skl.wins, skl.kills, skl.deaths, skl.coins " +
                    " , ratio_display_fraction as ratio " +
                    " , kills_to_deaths_ratio as KillsToDeathsRatio " +
                    " FROM report_kaneva.summary_kaching_leader_by_allTime skl " +
                    " INNER JOIN wok.players p on skl.player_id = p.player_id " +
                    sortOrder + " LIMIT @numResults ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@numResults", numResults));

            return Db.Stats.GetDataTable(strQuery, parameters);
        }

        /// <summary>
        /// GetUserStats
        /// </summary>
        /// <returns></returns>
        public DataTable GetUserStats(DateTime dtStartDate, DateTime dtEndDate, int userId)
        {
            string strQuery = "SELECT p.kaneva_user_id, pg.player_id, p.name " +
                    " ,(SELECT count(win_lose) FROM kaching.player_game pg2 WHERE win_lose = 1 and pg2.player_id = pg.player_id) as 'wins'  " +
                    " , sum(num_kills) as kills, sum(num_deaths) as deaths " +
                    " ,(SELECT sum(c.coin_scores) FROM kaching.player_game_coin_stats c WHERE c.player_id = pg.player_id) as coins " +
                    " , concat(num_kills, '/',num_deaths) as ratio " +
                    " , CASE WHEN (num_deaths > 0 AND num_kills = 0) THEN num_deaths " +
                    "                   WHEN (num_kills = 0) THEN 0 " +
                    "                   ELSE num_kills/num_deaths END as KillsToDeathsRatio " +
                    " FROM kaching.game g " +
                    " INNER JOIN kaching.player_game pg on g.game_id = pg.game_id " +                    
                    " INNER JOIN wok.players p on pg.player_id = p.player_id " +
                    " INNER JOIN kaneva.users ku on p.kaneva_user_id = ku.user_id " + 
                    " WHERE g.datetime BETWEEN  @startDate  AND @endDate AND ku.user_id = " + userId +
                    " GROUP BY pg.player_id LIMIT 1  ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@startDate", dtStartDate.Date));
            parameters.Add(new MySqlParameter("@endDate", dtEndDate.Date));            

            return Db.Stats.GetDataTable(strQuery, parameters);
        }

    }
}






