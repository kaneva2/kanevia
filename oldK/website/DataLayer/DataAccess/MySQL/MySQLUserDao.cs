///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using Devart.Data.MySql;
using System.Transactions;
using log4net;
using Sphinx.Client.Commands.Search;
using Sphinx.Client.Connections;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLUserDao : IUserDao
    {
        static private int PLAYER_ADMIN_CACHE_LIMIT_MIN = 5;

        private void CacheUserObject(User user, TimeSpan span )
        {
            // Add to the primary key
            CentralCache.Store(CentralCache.keyKanevaUser + user.UserId.ToString(), user, span );

            // Add to the usename => userid map
            CentralCache.Store(CentralCache.keyUserByUserName(user.Username), user.UserId, span );
        }

        private User GetCachedUser(int userId)
        {
            return (User)CentralCache.Get(CentralCache.keyKanevaUser + userId.ToString());
        }

        private User GetCachedUser(string userName)
        {
            Object IdObj = CentralCache.Get(CentralCache.keyUserByUserName(userName));
            if (IdObj != null)
            {
                User cachedUser = (User)CentralCache.Get(CentralCache.keyKanevaUser + IdObj.ToString());
                if (cachedUser != null)
                {
                    // Found in cache
                    return cachedUser;
                }
            }
            return null;
        }

        /// <summary>
        /// InvalidateKanevaUserCache
        /// </summary>
        /// <param name="userId"></param>
        public void InvalidateKanevaUserCache(int userId)
        {
            // Invalidate user from cache
            CentralCache.Remove(CentralCache.keyKanevaUser + userId.ToString());
            CentralCache.Remove(CentralCache.keyKanevaUser + userId.ToString() + "Obsolete");
        }

        /// <summary>
        /// Gets a user.
        /// </summary>
        /// <param name="userId">Unique user identifier.</param>
        /// <returns>User.</returns>
        public User GetUser(int userId)
        {
            User user = GetCachedUser(userId);
            if (user != null)
            {
                // Found in cache
                return user;
            }

            string sql = "SELECT u.user_id, u.username, display_name, TIMESTAMPDIFF(year,DATE(birth_date),now()) AS age, role, account_type, first_name, last_name, registration_key, key_value, " +
                " u.description, gender, homepage, u.email, show_mature, u.status_id, u.email_status, signup_date, birth_date, newsletter, u.public_profile, " +
                " u.country,u.ip_address, u.last_ip_address, zip_code, u.location, u.over_21, u.own_mod, u.active, " +
                " u.last_login, u.second_to_last_login, u.blast_privacy_permissions, u.blast_show_permissions, " +
                " u.browse_anonymously, u.show_online, u.online, u.ustate, u.notify_blog_comments, " +
                " u.notify_profile_comments, u.notify_friend_messages, u.notify_anyone_messages, " +
                " u.notify_friend_requests, u.notify_new_friends, u.friends_can_comment, u.everyone_can_comment, u.mature_profile, " +
                " u.salt, u.password, " +
                //" u.wok_player_id, u.join_source_community_id, u.receive_updates, " +
                // THIS IS A TEMPORARY HACK
                // Using master_user_id for receive_updates until we implement schema changes.
                " u.wok_player_id, u.join_source_community_id, coalesce(u.master_user_id, 1) as receive_updates, " +
                " com.thumbnail_path, com.community_id, com.template_id, com.name_no_spaces, com.url, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_xlarge_path, " +
                " com.thumbnail_square_path, cs.number_of_diggs, cs.number_of_views, cs.number_times_shared, u.home_community_id, gu.created_date AS first_wok_login " +
                " FROM users u " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
                " INNER JOIN channel_stats cs ON cs.channel_id = com.community_id " +
                " LEFT JOIN wok.game_users gu ON u.user_id = gu.kaneva_user_id " +
                " WHERE u.user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add (new MySqlParameter ("@userId", userId));

            DataRow row = Db.Master.GetDataRow(sql.ToString(), parameters);

            if (row == null)
            {
                User userBlank = new User ();
                userBlank.Stats = new UserStats ();
                return userBlank;
            }

            user = new User(Convert.ToInt32(row["user_id"]), row["username"].ToString(), row["display_name"].ToString(),
                Convert.ToInt32(row["role"]), row["registration_key"].ToString(), Convert.ToInt32(row["account_type"]),
                row["first_name"].ToString(), row["last_name"].ToString(), row["description"].ToString(), row["gender"].ToString(),
                row["homepage"].ToString(), row["email"].ToString(),
                Convert.ToInt32(row["status_id"]),Convert.ToInt32(row["email_status"]), row["key_value"].ToString(), Convert.ToDateTime(row["last_login"]), Convert.ToDateTime(row["signup_date"]),
                row["birth_date"].ToString(), row["newsletter"].ToString(),
                row["public_profile"].ToString().Equals("Y"), row["zip_code"].ToString(), row["country"].ToString(), row["ip_address"].ToString(), row["last_ip_address"].ToString(), 
                Convert.ToInt32(row["show_mature"]).Equals (1), Convert.ToInt32(row["browse_anonymously"]).Equals (1),
                Convert.ToInt32(row["show_online"]).Equals (1), row["notify_blog_comments"].ToString(), row["notify_profile_comments"].ToString(), row["notify_friend_messages"].ToString(),
                row["notify_anyone_messages"].ToString (), row["notify_friend_requests"].ToString (), Convert.ToInt16 (row["notify_new_friends"]).Equals (1), Convert.ToInt32 (row["friends_can_comment"]), Convert.ToInt32 (row["everyone_can_comment"]),
                Convert.ToInt32(row["mature_profile"]).Equals (1),  Convert.ToInt32(row["online"]), Convert.ToDateTime(row["second_to_last_login"]), Convert.ToInt32(row["age"]),
                row["location"].ToString(), row["own_mod"].ToString().Equals("Y"), row["active"].ToString().Equals("Y"),
                Convert.ToUInt32(row["blast_show_permissions"]), Convert.ToUInt32(row["blast_privacy_permissions"]), row["ustate"].ToString(), row["over_21"].ToString().Equals("Y"),
                Convert.ToInt32(row["wok_player_id"]), Convert.ToInt32(row["join_source_community_id"]), row["receive_updates"].ToString(),
                Convert.ToInt32(row["community_id"]), Convert.ToInt32(row["template_id"]),
                row["name_no_spaces"].ToString(), row["url"].ToString(), row["thumbnail_path"].ToString(),
                row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_xlarge_path"].ToString(),
                row["thumbnail_square_path"].ToString (), Convert.ToInt32 (row["number_of_diggs"]), Convert.ToInt32 (row["number_of_views"]), Convert.ToInt32 (row["number_times_shared"]),
                row["salt"].ToString(), row["password"].ToString(), Convert.ToInt32(row["home_community_id"] == DBNull.Value ? "0" : row["home_community_id"])
            );

            // Add in the date the user first logged into WoK, if present.
            if (row["first_wok_login"] != null && row["first_wok_login"] != DBNull.Value)
                user.FirstWoKLogin = Convert.ToDateTime(row["first_wok_login"]);

            // Cache user data for 5 minutes.  We do not invalidate cache for number_of_diggs, number_of_views etc.
            CacheUserObject(user, TimeSpan.FromMinutes(5));
            return user;
        }

        /// <summary>
        /// GetUserObsoleteForWok
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Obsolete("Use new call above")]
        public DataRow GetUserObsoleteForWok(int userId)
        {
            string cacheKey = CentralCache.keyKanevaUser + userId.ToString() + "Obsolete";
            DataTable dtUser = (DataTable) CentralCache.Get(cacheKey);

            if (dtUser != null)
            {
                if (dtUser.Rows.Count > 0)
                {
                    // Found in cache
                    return dtUser.Rows [0];
                }
            }

            string sqlSelect = "SELECT u.user_id, username, TIMESTAMPDIFF(year,DATE(birth_date),now()) AS age, role, account_type, first_name, last_name, registration_key, key_value, " +
               " u.description, gender, homepage, u.email, show_email, show_mature, u.status_id, signup_date, u.wok_player_id, " +
               " birth_date, newsletter, public_profile, " +
               " u.country, zip_code, u.location, u.over_21, u.own_mod, " +
               " u.last_login, u.second_to_last_login, u.blast_privacy_permissions, u.blast_show_permissions, " +
               " u.browse_anonymously, u.show_online, u.online, u.ustate, u.notify_blog_comments, " +
               " u.notify_profile_comments, u.notify_friend_messages, u.notify_anyone_messages, " +
               " u.notify_friend_requests, u.friends_can_comment, u.everyone_can_comment, u.mature_profile, coalesce(u.master_user_id, 1) as receive_updates, " +
               " us.number_of_logins, us.number_inbox_messages, us.number_of_new_messages, us.number_outbox_messages, us.number_forum_posts, " +
               " us.number_of_friends, us.number_of_pending, us.number_of_requests, us.number_of_trashed_messages, " +
               " us.number_of_blogs, us.number_of_comments, us.number_of_asset_diggs, " +
               " us.i_belong_to, us.i_own, us.i_moderate, us.number_asset_shares, us.number_community_shares, us.number_invites_sent, us.number_invites_accepted, us.number_invites_in_world, " +
               " com.thumbnail_path, com.community_id, com.template_id, com.name_no_spaces, com.url, " +
               " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_xlarge_path, " +
               " cs.number_of_diggs, cs.number_of_views, cs.number_times_shared, u.display_name " +
               " FROM users u " +
               " INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
               " INNER JOIN users_stats us ON u.user_id = us.user_id " +
               " INNER JOIN channel_stats cs ON cs.channel_id = com.community_id " +
               " WHERE u.user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            dtUser = Db.Master.GetDataTable(sqlSelect, parameters);

            if (dtUser.Rows.Count > 0)
            {
                CentralCache.Store(cacheKey, dtUser, TimeSpan.FromMinutes (5));
                return dtUser.Rows [0];
            }

            return null;
        }

        /// <summary>
        /// Gets a user.
        /// </summary>
        /// <param name="userId">Unique user identifier.</param>
        /// <returns>User.</returns>
        public User GetUser(string username)
        {
            // The primary entries for User objects in the cache are keyed by Id.  However, this method is called as the result of KEPAuthV# call.  In this context, the only key data
            // we have is the user's username.  To insure that we don't throw away the work, 
            //
            //  1) Add method cacheUserObject().  This method will place the object in memcached keyed by the user id AND it will store a map from username to id.
            //  2) This method will attempt to acquire the User object with user name by first acquring the userid via the username to userid map, and second
            //     by looking for the User object with this id.  If either of these steps fail, this method will build the user object and cache it with a call to
            //     if it fails, it build the user object and call cacheUserObject()
            //  3) Code for invalidating the userobject will remain unchanged.  If a UserObject is removed from the cache (invalidated) the acquisition process described in
            //     step two will fail and this method will insure that the object is added.
            //
            // Look for this username in memcache to acquire the userid for this user
            //

            //Object IdObj = CentralCache.Get(CentralCache.keyUserByUserName(username));
            //if ( IdObj != null )
            //{
            //    User cachedUser = (User)CentralCache.Get(CentralCache.keyKanevaUser + IdObj.ToString());
            //    if (cachedUser != null)
            //    {
            //        // Found in cache
            //        return cachedUser;
            //    }
            //}
            User cachedUser = GetCachedUser(username);
            if ( cachedUser != null )
            {
                return cachedUser;
            }



            string sql = "SELECT u.user_id, username, display_name, TIMESTAMPDIFF(year,DATE(birth_date),now()) AS age, role, account_type, first_name, last_name, registration_key, key_value, " +
                " u.description, gender, homepage, u.email, show_mature, u.status_id, u.email_status, signup_date, birth_date, newsletter, u.public_profile, " +
                " u.country,u.ip_address, u.last_ip_address, zip_code, u.location, u.over_21, u.own_mod, u.active, " +
                " u.last_login, u.second_to_last_login, u.blast_privacy_permissions, u.blast_show_permissions, " +
                " u.browse_anonymously, u.show_online, u.online, u.ustate, u.notify_blog_comments, " +
                " u.notify_profile_comments, u.notify_friend_messages, u.notify_anyone_messages, " +
                " u.notify_friend_requests, u.notify_new_friends, u.friends_can_comment, u.everyone_can_comment, u.mature_profile, " +
                " u.salt, u.password, " +
                //" u.wok_player_id, u.join_source_community_id, u.receive_updates, " +
                // THIS IS A TEMPORARY HACK
                // Using master_user_id for receive_updates until we implement schema changes.
                " u.wok_player_id, u.join_source_community_id, coalesce(u.master_user_id, 1) as receive_updates, " +
                " com.thumbnail_path, com.community_id, com.template_id, com.name_no_spaces, com.url, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_xlarge_path, " +
                " com.thumbnail_square_path, cs.number_of_diggs, cs.number_of_views, cs.number_times_shared, u.home_community_id " +
                " FROM users u " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
                " INNER JOIN channel_stats cs ON cs.channel_id = com.community_id " +
                " WHERE u.username = @username ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@username", username));

            DataRow row = Db.Master.GetDataRow(sql.ToString(), parameters);

            if (row == null)
            {
                User blankUser = new User();
                blankUser.Stats = new UserStats();
                return blankUser;
            }

            User user = new User(Convert.ToInt32(row["user_id"]), row["username"].ToString(), row["display_name"].ToString(),
                Convert.ToInt32(row["role"]), row["registration_key"].ToString(), Convert.ToInt32(row["account_type"]),
                row["first_name"].ToString(), row["last_name"].ToString(), row["description"].ToString(), row["gender"].ToString(),
                row["homepage"].ToString(), row["email"].ToString(),
                Convert.ToInt32(row["status_id"]), Convert.ToInt32(row["email_status"]), row["key_value"].ToString(), Convert.ToDateTime(row["last_login"]), Convert.ToDateTime(row["signup_date"]),
                row["birth_date"].ToString(), row["newsletter"].ToString(),
                row["public_profile"].ToString().Equals("Y"), row["zip_code"].ToString(), row["country"].ToString(), row["ip_address"].ToString(), row["last_ip_address"].ToString(),
                Convert.ToInt32(row["show_mature"]).Equals(1), Convert.ToInt32(row["browse_anonymously"]).Equals(1),
                Convert.ToInt32(row["show_online"]).Equals(1), row["notify_blog_comments"].ToString(), row["notify_profile_comments"].ToString(), row["notify_friend_messages"].ToString(),
                row["notify_anyone_messages"].ToString(), row["notify_friend_requests"].ToString(), Convert.ToInt16(row["notify_new_friends"]).Equals(1), Convert.ToInt32(row["friends_can_comment"]), Convert.ToInt32(row["everyone_can_comment"]),
                Convert.ToInt32(row["mature_profile"]).Equals(1), Convert.ToInt32(row["online"]), Convert.ToDateTime(row["second_to_last_login"]), Convert.ToInt32(row["age"]),
                row["location"].ToString(), row["own_mod"].ToString().Equals("Y"), row["active"].ToString().Equals("Y"),
                Convert.ToUInt32(row["blast_show_permissions"]), Convert.ToUInt32(row["blast_privacy_permissions"]), row["ustate"].ToString(), row["over_21"].ToString().Equals("Y"),
                Convert.ToInt32(row["wok_player_id"]), Convert.ToInt32(row["join_source_community_id"]), row["receive_updates"].ToString(),
                Convert.ToInt32(row["community_id"]), Convert.ToInt32(row["template_id"]),
                row["name_no_spaces"].ToString(), row["url"].ToString(), row["thumbnail_path"].ToString(),
                row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_xlarge_path"].ToString(),
                row["thumbnail_square_path"].ToString (), Convert.ToInt32 (row["number_of_diggs"]), Convert.ToInt32 (row["number_of_views"]), Convert.ToInt32 (row["number_times_shared"]),
                row["salt"].ToString(), row["password"].ToString(), Convert.ToInt32(row["home_community_id"] == DBNull.Value ? "0" : row["home_community_id"])
            );

            CacheUserObject(user, TimeSpan.FromMinutes(5));
            return user;
        }

        /// <summary>
        /// Gets a user by their email.
        /// </summary>
        /// <param name="email">email</param>
        /// <returns>User.</returns>
        public User GetUserByEmail(string email)
        {
            string sql = "SELECT u.user_id, username, display_name, TIMESTAMPDIFF(year,DATE(birth_date),now()) AS age, role, account_type, first_name, last_name, registration_key, key_value, " +
               " u.description, gender, homepage, u.email, show_mature, u.status_id, u.email_status, signup_date, birth_date, newsletter, u.public_profile, " +
               " u.country,u.ip_address, u.last_ip_address, zip_code, u.location, u.over_21, u.own_mod, u.active, " +
               " u.last_login, u.second_to_last_login, u.blast_privacy_permissions, u.blast_show_permissions, " +
               " u.browse_anonymously, u.show_online, u.online, u.ustate, u.notify_blog_comments, " +
               " u.notify_profile_comments, u.notify_friend_messages, u.notify_anyone_messages, " +
               " u.notify_friend_requests, u.notify_new_friends, u.friends_can_comment, u.everyone_can_comment, u.mature_profile, " +
                " u.salt, u.password, " +
                //" u.wok_player_id, u.join_source_community_id, u.receive_updates, " +
                // THIS IS A TEMPORARY HACK
                // Using master_user_id for receive_updates until we implement schema changes.
               " u.wok_player_id, u.join_source_community_id, coalesce(u.master_user_id, 1) as receive_updates, " +
               " com.thumbnail_path, com.community_id, com.template_id, com.name_no_spaces, com.url, " +
               " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_xlarge_path, " +
               " com.thumbnail_square_path, cs.number_of_diggs, cs.number_of_views, cs.number_times_shared, u.home_community_id " +
               " FROM users u " +
               " INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
               " INNER JOIN channel_stats cs ON cs.channel_id = com.community_id " +
               " WHERE u.email = @email ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@email", email));

            DataRow row = Db.Master.GetDataRow(sql.ToString(), parameters);

            if (row == null)
            {
                User userBlank = new User ();
                userBlank.Stats = new UserStats ();
                return userBlank;
            }

            return new User(Convert.ToInt32(row["user_id"]), row["username"].ToString(), row["display_name"].ToString(),
                Convert.ToInt32(row["role"]), row["registration_key"].ToString(), Convert.ToInt32(row["account_type"]),
                row["first_name"].ToString(), row["last_name"].ToString(), row["description"].ToString(), row["gender"].ToString(),
                row["homepage"].ToString(), row["email"].ToString(),
                Convert.ToInt32(row["status_id"]), Convert.ToInt32(row["email_status"]), row["key_value"].ToString(), Convert.ToDateTime(row["last_login"]), Convert.ToDateTime(row["signup_date"]),
                row["birth_date"].ToString(), row["newsletter"].ToString(),
                row["public_profile"].ToString().Equals("Y"), row["zip_code"].ToString(), row["country"].ToString(), row["ip_address"].ToString(), row["last_ip_address"].ToString(),
                Convert.ToInt32(row["show_mature"]).Equals(1), Convert.ToInt32(row["browse_anonymously"]).Equals(1),
                Convert.ToInt32(row["show_online"]).Equals(1), row["notify_blog_comments"].ToString(), row["notify_profile_comments"].ToString(), row["notify_friend_messages"].ToString(),
                row["notify_anyone_messages"].ToString (), row["notify_friend_requests"].ToString (), Convert.ToInt16 (row["notify_new_friends"]).Equals (1), Convert.ToInt32 (row["friends_can_comment"]), Convert.ToInt32 (row["everyone_can_comment"]),
                Convert.ToInt32(row["mature_profile"]).Equals(1), Convert.ToInt32(row["online"]), Convert.ToDateTime(row["second_to_last_login"]), Convert.ToInt32(row["age"]),
                row["location"].ToString(), row["own_mod"].ToString().Equals("Y"), row["active"].ToString().Equals("Y"),
                Convert.ToUInt32(row["blast_show_permissions"]), Convert.ToUInt32(row["blast_privacy_permissions"]), row["ustate"].ToString(), row["over_21"].ToString().Equals("Y"),
                Convert.ToInt32(row["wok_player_id"]), Convert.ToInt32(row["join_source_community_id"]), row["receive_updates"].ToString(),
                Convert.ToInt32(row["community_id"]), Convert.ToInt32(row["template_id"]),
                row["name_no_spaces"].ToString(), row["url"].ToString(), row["thumbnail_path"].ToString(),
                row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_xlarge_path"].ToString(),
                row["thumbnail_square_path"].ToString (), Convert.ToInt32 (row["number_of_diggs"]), Convert.ToInt32 (row["number_of_views"]), Convert.ToInt32 (row["number_times_shared"]),
                row["salt"].ToString(), row["password"].ToString(), Convert.ToInt32(row["home_community_id"] == DBNull.Value ? "0" : row["home_community_id"])
            );
        }

        public PagedList<User> GetUsersByEmail (List<string> emails)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string sql = "SELECT u.user_id, username, display_name, TIMESTAMPDIFF(year,DATE(birth_date),now()) AS age, role, account_type, first_name, last_name, registration_key, key_value, " +
               " u.description, gender, homepage, u.email, show_mature, u.status_id, u.email_status, signup_date, birth_date, newsletter, u.public_profile, " +
               " u.country,u.ip_address, u.last_ip_address, zip_code, u.location, u.over_21, u.own_mod, u.active, " +
               " u.last_login, u.second_to_last_login, u.blast_privacy_permissions, u.blast_show_permissions, " +
               " u.browse_anonymously, u.show_online, u.online, u.ustate, u.notify_blog_comments, " +
               " u.notify_profile_comments, u.notify_friend_messages, u.notify_anyone_messages, " +
               " u.notify_friend_requests, u.notify_new_friends, u.friends_can_comment, u.everyone_can_comment, u.mature_profile, " +
                " u.salt, u.password, " +
                //" u.wok_player_id, u.join_source_community_id, u.receive_updates, " +
                // THIS IS A TEMPORARY HACK
                // Using master_user_id for receive_updates until we implement schema changes.
               " u.wok_player_id, u.join_source_community_id, coalesce(u.master_user_id, 1) as receive_updates, " +
               " com.thumbnail_path, com.community_id, com.template_id, com.name_no_spaces, com.url, " +
               " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_xlarge_path, " +
               " com.thumbnail_square_path, cs.number_of_diggs, cs.number_of_views, cs.number_times_shared, u.home_community_id " +
               " FROM users u " +
               " INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
               " INNER JOIN channel_stats cs ON cs.channel_id = com.community_id " +
               " WHERE ";

            sql += " u.email IN (";
            for (int i = 0; i < emails.Count; i++)
            {
                sql += "@email" + i + ",";
                parameters.Add (new MySqlParameter ("@email" + i, emails[i]));
            }
            sql = sql.TrimEnd (',') + ") ";




   //         PagedDataTable dt = Db.Master.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);
            PagedDataTable dt = Db.Master.GetPagedDataTable(sql, "", parameters, 1, 1000);

            PagedList<User> list = new PagedList<User>();
            //assigns total count
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new User(Convert.ToInt32 (row["user_id"]), row["username"].ToString (), row["display_name"].ToString (),
                    Convert.ToInt32 (row["role"]), row["registration_key"].ToString (), Convert.ToInt32 (row["account_type"]),
                    row["first_name"].ToString (), row["last_name"].ToString (), row["description"].ToString (), row["gender"].ToString (),
                    row["homepage"].ToString (), row["email"].ToString (),
                    Convert.ToInt32 (row["status_id"]), Convert.ToInt32 (row["email_status"]), row["key_value"].ToString (), Convert.ToDateTime (row["last_login"]), Convert.ToDateTime (row["signup_date"]),
                    row["birth_date"].ToString (), row["newsletter"].ToString (),
                    row["public_profile"].ToString ().Equals ("Y"), row["zip_code"].ToString (), row["country"].ToString (), row["ip_address"].ToString (), row["last_ip_address"].ToString (),
                    Convert.ToInt32 (row["show_mature"]).Equals (1), Convert.ToInt32 (row["browse_anonymously"]).Equals (1),
                    Convert.ToInt32 (row["show_online"]).Equals (1), row["notify_blog_comments"].ToString (), row["notify_profile_comments"].ToString (), row["notify_friend_messages"].ToString (),
                    row["notify_anyone_messages"].ToString (), row["notify_friend_requests"].ToString (), Convert.ToInt16 (row["notify_new_friends"]).Equals (1), Convert.ToInt32 (row["friends_can_comment"]), Convert.ToInt32 (row["everyone_can_comment"]),
                    Convert.ToInt32 (row["mature_profile"]).Equals (1), Convert.ToInt32 (row["online"]), Convert.ToDateTime (row["second_to_last_login"]), Convert.ToInt32 (row["age"]),
                    row["location"].ToString (), row["own_mod"].ToString ().Equals ("Y"), row["active"].ToString ().Equals ("Y"),
                    Convert.ToUInt32 (row["blast_show_permissions"]), Convert.ToUInt32 (row["blast_privacy_permissions"]), row["ustate"].ToString (), row["over_21"].ToString ().Equals ("Y"),
                    Convert.ToInt32 (row["wok_player_id"]), Convert.ToInt32 (row["join_source_community_id"]), row["receive_updates"].ToString (),
                    Convert.ToInt32 (row["community_id"]), Convert.ToInt32 (row["template_id"]),
                    row["name_no_spaces"].ToString (), row["url"].ToString (), row["thumbnail_path"].ToString (),
                    row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (), row["thumbnail_xlarge_path"].ToString (),
                    row["thumbnail_square_path"].ToString (), Convert.ToInt32 (row["number_of_diggs"]), Convert.ToInt32 (row["number_of_views"]), Convert.ToInt32 (row["number_times_shared"]),
                    row["salt"].ToString (), row["password"].ToString (), Convert.ToInt32 (row["home_community_id"] == DBNull.Value ? "0" : row["home_community_id"])));
            }

            return list;
        }

        /// <summary>
        /// Gets a user by their email.
        /// </summary>
        /// <param name="email">email</param>
        /// <returns>User.</returns>
        public User GetCommunityOwnerByCommunityId (int communityId)
        {
            string sql = "SELECT u.user_id, username, display_name, TIMESTAMPDIFF(year,DATE(birth_date),now()) AS age, role, account_type, first_name, last_name, registration_key, key_value, " +
               " u.description, gender, homepage, u.email, show_mature, u.status_id, u.email_status, signup_date, birth_date, newsletter, u.public_profile, " +
               " u.country,u.ip_address, u.last_ip_address, zip_code, u.location, u.over_21, u.own_mod, u.active, " +
               " u.last_login, u.second_to_last_login, u.blast_privacy_permissions, u.blast_show_permissions, " +
               " u.browse_anonymously, u.show_online, u.online, u.ustate, u.notify_blog_comments, " +
               " u.notify_profile_comments, u.notify_friend_messages, u.notify_anyone_messages, " +
               " u.notify_friend_requests, u.notify_new_friends, u.friends_can_comment, u.everyone_can_comment, u.mature_profile, " +
               " u.wok_player_id, u.join_source_community_id, coalesce(u.master_user_id, 1) as receive_updates, " +
               " com.thumbnail_path, com.community_id, com.template_id, com.name_no_spaces, com.url, " +
               " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_xlarge_path, " +
               " com.thumbnail_square_path, cs.number_of_diggs, cs.number_of_views, cs.number_times_shared, u.home_community_id " +
               " FROM communities com1 " +
               " INNER JOIN users u ON com1.creator_id = u.user_id " +
               " INNER JOIN communities com ON com.creator_id = u.user_id " +
               " INNER JOIN channel_stats cs ON cs.channel_id = com1.community_id " +
               " WHERE com1.community_id = @communityId " +
               " AND com.is_personal = 1;";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));

            DataRow row = Db.Master.GetDataRow (sql.ToString (), parameters);

            if (row == null)
            {
                User userBlank = new User ();
                userBlank.Stats = new UserStats ();
                return userBlank;
            }

            return new User (Convert.ToInt32 (row["user_id"]), row["username"].ToString (), row["display_name"].ToString (),
                Convert.ToInt32 (row["role"]), row["registration_key"].ToString (), Convert.ToInt32 (row["account_type"]),
                row["first_name"].ToString (), row["last_name"].ToString (), row["description"].ToString (), row["gender"].ToString (),
                row["homepage"].ToString (), row["email"].ToString (),
                Convert.ToInt32 (row["status_id"]), Convert.ToInt32 (row["email_status"]), row["key_value"].ToString (), Convert.ToDateTime (row["last_login"]), Convert.ToDateTime (row["signup_date"]),
                row["birth_date"].ToString (), row["newsletter"].ToString (),
                row["public_profile"].ToString ().Equals ("Y"), row["zip_code"].ToString (), row["country"].ToString (), row["ip_address"].ToString (), row["last_ip_address"].ToString (),
                Convert.ToInt32 (row["show_mature"]).Equals (1), Convert.ToInt32 (row["browse_anonymously"]).Equals (1),
                Convert.ToInt32 (row["show_online"]).Equals (1), row["notify_blog_comments"].ToString (), row["notify_profile_comments"].ToString (), row["notify_friend_messages"].ToString (),
                row["notify_anyone_messages"].ToString (), row["notify_friend_requests"].ToString (), Convert.ToInt16 (row["notify_new_friends"]).Equals (1), Convert.ToInt32 (row["friends_can_comment"]), Convert.ToInt32 (row["everyone_can_comment"]),
                Convert.ToInt32 (row["mature_profile"]).Equals (1), Convert.ToInt32 (row["online"]), Convert.ToDateTime (row["second_to_last_login"]), Convert.ToInt32 (row["age"]),
                row["location"].ToString (), row["own_mod"].ToString ().Equals ("Y"), row["active"].ToString ().Equals ("Y"),
                Convert.ToUInt32 (row["blast_show_permissions"]), Convert.ToUInt32 (row["blast_privacy_permissions"]), row["ustate"].ToString (), row["over_21"].ToString ().Equals ("Y"),
                Convert.ToInt32 (row["wok_player_id"]), Convert.ToInt32 (row["join_source_community_id"]), row["receive_updates"].ToString (),
                Convert.ToInt32 (row["community_id"]), Convert.ToInt32 (row["template_id"]),
                row["name_no_spaces"].ToString (), row["url"].ToString (), row["thumbnail_path"].ToString (),
                row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (), row["thumbnail_xlarge_path"].ToString (),
                row["thumbnail_square_path"].ToString (), Convert.ToInt32 (row["number_of_diggs"]), Convert.ToInt32 (row["number_of_views"]), Convert.ToInt32 (row["number_times_shared"]),
                Convert.ToInt32(row["home_community_id"] == DBNull.Value ? "0" : row["home_community_id"])
            );
        }

        /// <summary>
        /// GetUsers
        /// </summary>
        public PagedList<User> GetUsersList(string username, string email, string firstName, string lastName, int userStatus, string filter, string orderby, int pageNumber, int pageSize)
        {
            string strQuery = "SELECT u.user_id, username, display_name, TIMESTAMPDIFF(year,DATE(birth_date),now()) AS age, role, account_type, first_name, last_name, registration_key, key_value, " +
                " u.description, gender, homepage, u.email, show_mature, u.status_id, u.email_status, signup_date, birth_date, newsletter, u.public_profile, " +
                " u.country,u.ip_address, u.last_ip_address, zip_code, u.location, u.over_21, u.own_mod, u.active, " +
                " u.last_login, u.second_to_last_login, u.blast_privacy_permissions, u.blast_show_permissions, " +
                " u.browse_anonymously, u.show_online, u.online, u.ustate, u.notify_blog_comments, " +
                " u.notify_profile_comments, u.notify_friend_messages, u.notify_anyone_messages, " +
                " u.notify_friend_requests, u.notify_new_friends, u.friends_can_comment, u.everyone_can_comment, u.mature_profile, " +
                //" u.wok_player_id, u.join_source_community_id, u.receive_updates, " +
                // THIS IS A TEMPORARY HACK
                // Using master_user_id for receive_updates until we implement schema changes.
                " u.wok_player_id, u.join_source_community_id, coalesce(u.master_user_id, 1) as receive_updates, " +
                " com.thumbnail_path, com.community_id, com.template_id, com.name_no_spaces, com.url, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_xlarge_path, " +
                " com.thumbnail_square_path, cs.number_of_diggs, cs.number_of_views, cs.number_times_shared, u.home_community_id " +
                " FROM users u " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
                " INNER JOIN channel_stats cs ON cs.channel_id = com.community_id ";

            string whereClause = "";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (userStatus > 0)
            {
                whereClause += " AND u.status_id = " + userStatus;
            }

            if (username.Trim().Length > 0)
            {
                parameters.Add(new MySqlParameter("@username", username.Trim() + "%"));
                whereClause += " AND u.username like @username";
            }

            if (email.Trim().Length > 0)
            {
                parameters.Add(new MySqlParameter("@email", email.Trim() + "%"));
                whereClause += " AND u.email like @email";
            }

            if (firstName.Trim().Length > 0)
            {
                parameters.Add(new MySqlParameter("@firstName", firstName.Trim() + "%"));
                whereClause += " AND u.first_name like @firstName";
            }

            if (lastName.Trim().Length > 0)
            {
                parameters.Add(new MySqlParameter("@lastName", lastName.Trim() + "%"));
                whereClause += " AND u.last_name like @lastName";
            }

            // Filter it?
            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

            if (whereClause.Length > 0)
            {
                strQuery = strQuery + " WHERE " + whereClause.Substring(4,whereClause.Length - 4);
            }

            PagedDataTable dt = Db.Master.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<User> list = new PagedList<User>();
            //assigns total count
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new User(Convert.ToInt32(row["user_id"]), row["username"].ToString(), row["display_name"].ToString(),
                Convert.ToInt32(row["role"]), row["registration_key"].ToString(), Convert.ToInt32(row["account_type"]),
                row["first_name"].ToString(), row["last_name"].ToString(), row["description"].ToString(), row["gender"].ToString(),
                row["homepage"].ToString(), row["email"].ToString(),
                Convert.ToInt32(row["status_id"]), Convert.ToInt32(row["email_status"]), row["key_value"].ToString(), Convert.ToDateTime(row["last_login"]), Convert.ToDateTime(row["signup_date"]),
                row["birth_date"].ToString(), row["newsletter"].ToString(),
                row["public_profile"].ToString().Equals("Y"), row["zip_code"].ToString(), row["country"].ToString(), row["ip_address"].ToString(), row["last_ip_address"].ToString(),
                Convert.ToInt32(row["show_mature"]).Equals(1), Convert.ToInt32(row["browse_anonymously"]).Equals(1),
                Convert.ToInt32(row["show_online"]).Equals(1), row["notify_blog_comments"].ToString(), row["notify_profile_comments"].ToString(), row["notify_friend_messages"].ToString(),
                row["notify_anyone_messages"].ToString(), row["notify_friend_requests"].ToString(), Convert.ToInt16(row["notify_new_friends"]).Equals(1), Convert.ToInt32(row["friends_can_comment"]), Convert.ToInt32(row["everyone_can_comment"]),
                Convert.ToInt32(row["mature_profile"]).Equals(1), Convert.ToInt32(row["online"]), Convert.ToDateTime(row["second_to_last_login"]), Convert.ToInt32(row["age"]),
                row["location"].ToString(), row["own_mod"].ToString().Equals("Y"), row["active"].ToString().Equals("Y"),
                Convert.ToUInt32(row["blast_show_permissions"]), Convert.ToUInt32(row["blast_privacy_permissions"]), row["ustate"].ToString(), row["over_21"].ToString().Equals("Y"),
                Convert.ToInt32(row["wok_player_id"]), Convert.ToInt32(row["join_source_community_id"]), row["receive_updates"].ToString(),
                Convert.ToInt32(row["community_id"]), Convert.ToInt32(row["template_id"]),
                row["name_no_spaces"].ToString(), row["url"].ToString(), row["thumbnail_path"].ToString(),
                row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_xlarge_path"].ToString(),
                row["thumbnail_square_path"].ToString (), Convert.ToInt32 (row["number_of_diggs"]), Convert.ToInt32 (row["number_of_views"]), Convert.ToInt32 (row["number_times_shared"]),
                Convert.ToInt32(row["home_community_id"] == DBNull.Value ? "0" : row["home_community_id"])));
            }

            return list;
        }

        /// <summary>
        /// Get a list of users
        /// </summary>
        public PagedList<User> GetUsersList(string filter, string orderby, int pageNumber, int pageSize)
        {
            string strQuery = "SELECT u.user_id, username, display_name, TIMESTAMPDIFF(year,DATE(birth_date),now()) AS age, role, account_type, first_name, last_name, registration_key, key_value, " +
                " u.description, gender, homepage, u.email, show_mature, u.status_id, u.email_status, signup_date, birth_date, newsletter, u.public_profile, " +
                " u.country,u.ip_address, u.last_ip_address, zip_code, u.location, u.over_21, u.own_mod, u.active, " +
                " u.last_login, u.second_to_last_login, u.blast_privacy_permissions, u.blast_show_permissions, " +
                " u.browse_anonymously, u.show_online, u.online, u.ustate, u.notify_blog_comments, " +
                " u.notify_profile_comments, u.notify_friend_messages, u.notify_anyone_messages, " +
                " u.notify_friend_requests, u.notify_new_friends, u.friends_can_comment, u.everyone_can_comment, u.mature_profile, " +
                //" u.wok_player_id, u.join_source_community_id, u.receive_updates, " +
                // THIS IS A TEMPORARY HACK
                // Using master_user_id for receive_updates until we implement schema changes.
                " u.wok_player_id, u.join_source_community_id, coalesce(u.master_user_id, 1) as receive_updates, " +
                " com.thumbnail_path, com.community_id, com.template_id, com.name_no_spaces, com.url, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_xlarge_path, " +
                " com.thumbnail_square_path, cs.number_of_diggs, cs.number_of_views, cs.number_times_shared, u.home_community_id " +
                " FROM users u " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
                " INNER JOIN channel_stats cs ON cs.channel_id = com.community_id ";


            if ((filter != null) && (filter != ""))
            {
                strQuery += " WHERE " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            PagedDataTable dt = Db.Master.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<User> list = new PagedList<User>();
            //assigns total count
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new User(Convert.ToInt32(row["user_id"]), row["username"].ToString(), row["display_name"].ToString(),
                Convert.ToInt32(row["role"]), row["registration_key"].ToString(), Convert.ToInt32(row["account_type"]),
                row["first_name"].ToString(), row["last_name"].ToString(), row["description"].ToString(), row["gender"].ToString(),
                row["homepage"].ToString(), row["email"].ToString(),
                Convert.ToInt32(row["status_id"]), Convert.ToInt32(row["email_status"]), row["key_value"].ToString(), Convert.ToDateTime(row["last_login"]), Convert.ToDateTime(row["signup_date"]),
                row["birth_date"].ToString(), row["newsletter"].ToString(),
                row["public_profile"].ToString().Equals("Y"), row["zip_code"].ToString(), row["country"].ToString(), row["ip_address"].ToString(), row["last_ip_address"].ToString(),
                Convert.ToInt32(row["show_mature"]).Equals(1), Convert.ToInt32(row["browse_anonymously"]).Equals(1),
                Convert.ToInt32(row["show_online"]).Equals(1), row["notify_blog_comments"].ToString(), row["notify_profile_comments"].ToString(), row["notify_friend_messages"].ToString(),
                row["notify_anyone_messages"].ToString (), row["notify_friend_requests"].ToString (), Convert.ToInt16 (row["notify_new_friends"]).Equals (1), Convert.ToInt32 (row["friends_can_comment"]), Convert.ToInt32 (row["everyone_can_comment"]),
                Convert.ToInt32(row["mature_profile"]).Equals(1), Convert.ToInt32(row["online"]), Convert.ToDateTime(row["second_to_last_login"]), Convert.ToInt32(row["age"]),
                row["location"].ToString(), row["own_mod"].ToString().Equals("Y"), row["active"].ToString().Equals("Y"),
                Convert.ToUInt32(row["blast_show_permissions"]), Convert.ToUInt32(row["blast_privacy_permissions"]), row["ustate"].ToString(), row["over_21"].ToString().Equals("Y"),
                Convert.ToInt32(row["wok_player_id"]), Convert.ToInt32(row["join_source_community_id"]), row["receive_updates"].ToString(),
                Convert.ToInt32(row["community_id"]), Convert.ToInt32(row["template_id"]),
                row["name_no_spaces"].ToString(), row["url"].ToString(), row["thumbnail_path"].ToString(),
                row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_xlarge_path"].ToString(),
                row["thumbnail_square_path"].ToString(), Convert.ToInt32(row["number_of_diggs"]), Convert.ToInt32(row["number_of_views"]), Convert.ToInt32(row["number_times_shared"]), Convert.ToInt32(row["home_community_id"] == DBNull.Value ? "0" : row["home_community_id"])));
            }

            return list;
        }

        /// <summary>
        /// GetUserStats
        /// </summary>
        public UserStats GetUserStats(int userId)
        {
            string sql = "SELECT us.number_of_logins, us.number_inbox_messages, us.number_of_new_messages, us.number_outbox_messages, us.number_forum_posts, " +
               " us.number_of_friends, us.number_of_pending, us.number_of_requests, us.number_of_trashed_messages, " +
               " us.number_of_blogs, us.number_of_comments, us.number_of_asset_diggs, us.interest_count, us.number_blasts_sent, " +
               " us.i_belong_to, us.i_own, us.i_moderate, us.number_asset_shares, us.number_community_shares, us.number_invites_sent, " +
               " us.number_invites_accepted, us.number_invites_in_world, us.number_friend_invite_rewards, us.number_3DApp_requests, us.number_community_invites " +
               " FROM users_stats us " +
               " WHERE us.user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            DataRow row = Db.Master.GetDataRow(sql.ToString(), parameters);
            return new UserStats(Convert.ToInt32(row["number_of_logins"]), Convert.ToInt32(row["number_forum_posts"]), Convert.ToInt32(row["number_of_friends"]),
                Convert.ToInt32(row["number_of_pending"]), Convert.ToInt32(row["number_of_requests"]), Convert.ToInt32(row["number_inbox_messages"]), Convert.ToInt32(row["number_of_new_messages"]),
                Convert.ToInt32 (row["number_of_blogs"]), Convert.ToInt32 (row["number_of_comments"]), Convert.ToInt32 (row["number_of_asset_diggs"]), Convert.ToInt32 (row["number_blasts_sent"]),
                Convert.ToInt32(row["i_own"]), Convert.ToInt32(row["i_moderate"]), Convert.ToInt32(row["i_belong_to"]),
                Convert.ToUInt32(row["interest_count"]), Convert.ToUInt32(row["number_asset_shares"]), Convert.ToUInt32(row["number_community_shares"]),
                Convert.ToUInt32 (row["number_invites_sent"]), Convert.ToUInt32 (row["number_invites_accepted"]), 
                Convert.ToUInt32 (row["number_invites_in_world"]), Convert.ToDouble (row["number_friend_invite_rewards"]),
                Convert.ToInt32 (row["number_3DApp_requests"]), Convert.ToInt32 (row["number_community_invites"]));
        }

        /// <summary>
        /// GetUserPreferences
        /// </summary>
        public Preferences GetUserPreferences(int userId)
        {
            Preferences pref = (Preferences)CentralCache.Get(CentralCache.keyUserPref(userId));

            if (pref != null)
            {
                // Found in cache
                return pref;
            }

            string sql = "SELECT user_id, kei_point_id_purchase_type, filter_by_country " +
                " FROM user_preferences " +
                " WHERE user_id = @UserId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            DataRow row = Db.Slave2.GetDataRow(sql.ToString(), parameters);

            if (row == null)
            {
                pref = new Preferences();
                pref.UserId = userId;
            }
            else
            {
                pref = new Preferences(
                    Convert.ToInt32(row["user_id"]), row["kei_point_id_purchase_type"].ToString(),
                        row["filter_by_country"].ToString().Equals("Y")
                        );
            }

            CentralCache.Store(CentralCache.keyUserPref(userId), pref, TimeSpan.FromMinutes(30));
            return pref;
        }

		/// <summary>
		/// GetUserNotificationPreferences
		/// </summary>
		public NotificationPreferences GetUserNotificationPreferences(int userId)
		{
			NotificationPreferences notificationPref = (NotificationPreferences)CentralCache.Get(CentralCache.keyUserNotificationPref(userId));

			if (notificationPref != null)
			{
				// Found in cache
				return notificationPref;
			}

            string sql = "SELECT user_id, notify_blast_comments, notify_media_comments, notify_shop_comments_purchases, " +
                " notify_event_invites, notify_friend_birthdays, notify_world_blasts, notify_world_requests " +
				" FROM user_notification_preferences " +
				" WHERE user_id = @UserId ";

			List<IDbDataParameter> parameters = new List<IDbDataParameter>();
			parameters.Add(new MySqlParameter("@userId", userId));

			DataRow row = Db.Slave2.GetDataRow(sql.ToString(), parameters);

			if (row == null)
			{
				notificationPref = new NotificationPreferences();
				notificationPref.UserId = userId;
			}
			else
			{
				notificationPref = new NotificationPreferences(
					Convert.ToInt32(row["user_id"]), Convert.ToInt32(row["notify_blast_comments"]).Equals(1),
						Convert.ToInt32(row["notify_media_comments"]).Equals(1), Convert.ToInt32(row["notify_shop_comments_purchases"]).Equals(1),
                        Convert.ToInt32 (row["notify_event_invites"]).Equals (1), Convert.ToInt32 (row["notify_friend_birthdays"]).Equals (1),
                        Convert.ToInt32(row["notify_world_blasts"]).Equals(1), Convert.ToInt32(row["notify_world_requests"]).Equals(1)
						);
			}

			CentralCache.Store(CentralCache.keyUserNotificationPref(userId), notificationPref, TimeSpan.FromMinutes(30));
			return notificationPref;
		}

        /// <summary>
        /// GetUserBalancesCached
        /// </summary>
        public UserBalances GetUserBalancesCached(int userId)
        {
            UserBalances userBalances = (UserBalances)CentralCache.Get(CentralCache.keyKanevaUserBalances + userId.ToString());

            if (userBalances != null)
            {
                // Found in cache
                return userBalances;
            }

            userBalances = GetUserBalances(userId);

            CentralCache.Store(CentralCache.keyKanevaUserBalances + userId.ToString (), userBalances, TimeSpan.FromMinutes(10));
            return userBalances;

        }

        /// <summary>
        /// GetUserBalances
        /// </summary>
        public UserBalances GetUserBalances (int userId)
        {
            string sqlSelect = "SELECT COALESCE(ub.balance, 0) as balance, kei_point_id " +
                " FROM user_balances ub " +
                " WHERE ub.user_id = @userId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            DataTable dt = Db.Master.GetDataTable (sqlSelect, parameters);

            UserBalances ub = new UserBalances ();

            if (dt == null)
            {
                return ub;
            }

            foreach (DataRow row in dt.Rows)
            {
                switch (row["kei_point_id"].ToString ())
                {
                    case Currency.CurrencyType.CREDITS:
                        ub.KPoint = Convert.ToDouble (row["balance"]);
                        break;

                    case Currency.CurrencyType.REWARDS:
                        ub.GPoint = Convert.ToDouble (row["balance"]);
                        break;

                    case Currency.CurrencyType.MPOINT:
                        ub.MPoint = Convert.ToDouble (row["balance"]);
                        break;

                    case Currency.CurrencyType.DOLLAR:
                        ub.Dollar = Convert.ToDouble (row["balance"]);
                        break;
                }
            }

            return ub;
        }

        /// <summary>
        /// Get a friend for a user
        /// </summary>
        /// <returns></returns>
        public Friend GetFriend(int userId, int friendId)
        {
            string strQuery = "SELECT u.username, u.user_id, u.display_name, u.gender, f.glued_date, u.ustate, u.location, u.last_login, u.status_id, " +
                " com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path, " +
                " cs.number_of_views, cs.number_of_diggs, TIMESTAMPDIFF(year,DATE(u.birth_date),now()) AS age, u.mature_profile ";

            strQuery += " FROM users u, friends f, communities_personal com " +
                " INNER JOIN channel_stats cs ON com.community_id = cs.channel_id ";

            strQuery += " WHERE f.user_id = @userId " +
                " AND f.friend_id = @friendId " +
                " AND u.user_id = f.friend_id " +
                " AND u.active = 'Y' " +
                " AND u.user_id = com.creator_id ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@friendId", friendId));
            DataRow row = Db.Master.GetDataRow(strQuery.ToString(), parameters);

            if (row == null)
            {
                return new Friend ();
            }

            return new Friend (Convert.ToInt32 (row["user_id"]), row["username"].ToString(), row["display_name"].ToString(), row["gender"].ToString(), Convert.ToDateTime(row["glued_date"]),
                    row["ustate"].ToString(), row["location"].ToString(), row["name_no_spaces"].ToString(),
                    row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(),
                    row["thumbnail_square_path"].ToString (), int.Parse (row["number_of_views"].ToString ()), int.Parse (row["number_of_diggs"].ToString ()),
                    int.Parse (row["status_id"].ToString ()), Convert.ToInt32 (row["age"]), Convert.ToInt32(row["mature_profile"]).Equals (1));
        }

        public PagedList<FriendRequest> GetOutgoingPendingFriendRequests (int userId, string filter, string orderby,
            bool bGetPending, int pageNumber, int pageSize)
        {
            string strQuery = " SELECT u.username, u.user_id, u.gender, u.age, u.location, " +
                " u.birth_date, u.zip_code, " +
                " com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, " +
                " fr.request_date, NOW() as CurrentDate " +
                " FROM users u, friend_requests fr, communities_personal com " +
                " WHERE fr.user_id = @userId AND u.user_id = fr.friend_id " +
                " AND u.user_id = com.creator_id ";

            // Filter it?
            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }
            if ((orderby != null) && (orderby != ""))
            {
                strQuery += " ORDER BY " + orderby;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            PagedDataTable dt = Db.Master.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<FriendRequest> list = new PagedList<FriendRequest>();
            //assigns total count
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new FriendRequest (Convert.ToInt32 (row["user_id"]), row["username"].ToString (), row["display_name"].ToString (), row["gender"].ToString (),
                    Convert.ToDateTime (row["birth_date"]), row["location"].ToString (), row["name_no_spaces"].ToString (),
                    row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (),
                    row["zip_code"].ToString (), Convert.ToInt32 (row["age"]),
                    Convert.ToDateTime (row["request_date"]), Convert.ToDateTime (row["CurrentDate"])
                ));
            }
            return list;
        }

        /// <summary>
        /// AcceptFriend
        /// </summary>
        public int AcceptFriend(int userId, int friendId)
        {
            int result = 0;

            // Are they already friends?
            Friend friend = GetFriend(userId, friendId);
            if (friend.UserId > 0)
            {
                return -2;
            }

            // Delete the request
            DeleteFriendRequest(userId, friendId);

            // Insert into the friend table
            string sql = "CALL add_friends (@userId, @friendId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@friendId", friendId));
            result = Db.Master.ExecuteNonQuery (sql, parameters);

            // Remove all cache entries for friend Transactions
            ClearFriendTransactionCache();

            return result;
        }

        /// <summary>
        /// AcceptFUpdateFriendRequestSentDateriend
        /// </summary>
        public int UpdateFriendRequestSentDate (int userId, int friendId)
        {
            string sqlString = "UPDATE friend_requests " +
                            " SET request_date = NOW() " +
                            " WHERE user_id = @userId " +
                            " AND friend_id = @friendId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@friendId", friendId));
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// DenyFriend
        /// </summary>
        public int DenyFriend(int userId, int friendId)
        {
            // Delete the request
            DeleteFriendRequest (userId, friendId);

            // Insert into the friend_declined table
            string sql = "INSERT IGNORE INTO friends_declined " +
                "(user_id, friend_id, date_declined " +
                ") VALUES (" +
                "@userId, @friendId, NOW())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@friendId", friendId));
            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Delete a friend
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="friendId"></param>
        /// <returns></returns>
        public int DeleteFriendRequest(int userId, int friendId)
        {
            // Get the friend to match the userId, friendId correctly
            DataRow drFriendRequest = GetFriendRequest(userId, friendId);

            if (drFriendRequest != null)
            {
                string sql = "CALL remove_friend_request(@userID, @friendId)";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@userId", Convert.ToInt32(drFriendRequest["user_id"])));
                parameters.Add(new MySqlParameter("@friendId", Convert.ToInt32(drFriendRequest["friend_id"])));
                return Db.Master.ExecuteNonQuery(sql, parameters);
            }

            return 0;
        }

        /// <summary>
        /// Delete friend
        /// </summary>
        public int DeleteFriend (int userId, int friendId)
        {
            // Get the friend to match the userId, friendId correctly
            Friend friend = GetFriend (userId, friendId);

            if (friend.UserId > 0)
            {
                string sqlUpdate = "CALL remove_friends (@userId, @friendId)";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@userId", userId));
                parameters.Add(new MySqlParameter("@friendId", friendId));
                int result = Db.Master.ExecuteNonQuery(sqlUpdate, parameters);

                // Remove all cache entries for friend Transactions
                ClearFriendTransactionCache();

                return result;
            }

            return -1;
        }

        /// <summary>
        /// ClearFriendTransactionCache
        /// </summary>
        public void ClearFriendTransactionCache ()
        {
            string friendTransactionsCachedKey = CentralCache.keyFriendTransactionsCached();
            string ftCached = (string)CentralCache.Get(friendTransactionsCachedKey);

            string[] keys;
            char[] splitter = { ';' };

            if (ftCached != null)
            {
                // Remove each spawn point
                keys = ftCached.Split(splitter);

                foreach (string key in keys)
                {
                    CentralCache.Remove(key);
                }

                // Remove the stored keys
                CentralCache.Remove(friendTransactionsCachedKey);
            }
        }


        /// <summary>
        /// UpdateUser
        /// </summary>
        /// <param name="user"></param>
        public int UpdateUserSecurity(User user)
        {
            string sqlString = "UPDATE users " +
                " SET role = @roldID " +
                " WHERE user_id = @userID";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@roldID", user.Role));
            parameters.Add(new MySqlParameter("@userID", user.UserId));
            int result = Db.Master.ExecuteNonQuery(sqlString, parameters);

            // Invalidate user from cache
            InvalidateKanevaUserCache(user.UserId);

            return result;
         }



        /// <summary>
        /// UpdateUserPreferences
        /// </summary>
        /// <param name="user_id"></param>
        public int UpdateUserPreferences(Preferences preferences)
        {
            int ret = 0;
            // Does a preferences already exist?
            Preferences prefExist = GetUserPreferences (preferences.UserId);

            if (prefExist.UserId.Equals (0))
            {
                // Insert new
                string sqlString = "INSERT INTO user_preferences (" +
                " user_id, kei_point_id_purchase_type, filter_by_country" +
                ") VALUES (" +
                " @user_id,  @kei_point_id_purchase_type,  @filter_by_country" +
                ")";

                List < IDbDataParameter > parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@user_id", preferences.UserId));
                parameters.Add(new MySqlParameter("@kei_point_id_purchase_type", preferences.AlwaysPurchaseWithCurrencyType));
                parameters.Add(new MySqlParameter("@filter_by_country", preferences.FilterByCountry ? "Y" : "N"));
                ret = Db.Master.ExecuteNonQuery(sqlString, parameters);
            }
            else
            {  
                string sqlString = "UPDATE user_preferences SET " +
                    " kei_point_id_purchase_type = @kei_point_id_purchase_type, " +
                    " filter_by_country=@filter_by_country" +
                    " WHERE user_id = @user_id";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@user_id", preferences.UserId));
                parameters.Add(new MySqlParameter("@kei_point_id_purchase_type", preferences.AlwaysPurchaseWithCurrencyType));
                parameters.Add(new MySqlParameter("@filter_by_country", preferences.FilterByCountry ? "Y" : "N"));
                ret =  Db.Master.ExecuteNonQuery(sqlString, parameters);
            }
            CentralCache.Remove(CentralCache.keyUserPref(preferences.UserId));
            return ret;
        }

		/// <summary>
		/// UpdateUserNotificationPreferences
		/// </summary>
		/// <param name="user_id"></param>
		public int UpdateUserNotificationPreferences(NotificationPreferences notificationPreferences)
		{
			int ret = 0;
			// Does a preferences already exist?
			NotificationPreferences prefExist = GetUserNotificationPreferences(notificationPreferences.UserId);

			string sqlString = "";

			// Insert new or update existing
			sqlString = "INSERT INTO user_notification_preferences (" +
                " user_id, notify_blast_comments, notify_media_comments, notify_shop_comments_purchases, notify_event_invites, " +
                " notify_friend_birthdays, notify_world_blasts, notify_world_requests " +
                ") VALUES (" +
                " @user_id,  @notify_blast_comments,  @notify_media_comments, @notify_shop_comments_purchases, @notify_event_invites, " +
                " @notify_friend_birthdays, @notify_world_blasts, @notify_world_requests " +
                ") ON DUPLICATE KEY UPDATE notify_blast_comments = @notify_blast_comments, notify_media_comments = @notify_media_comments, " +
                    " notify_shop_comments_purchases = @notify_shop_comments_purchases, notify_event_invites = @notify_event_invites, " +
                    " notify_friend_birthdays = @notify_friend_birthdays, notify_world_blasts = @notify_world_blasts, notify_world_requests = @notify_world_requests";


			List<IDbDataParameter> parameters = new List<IDbDataParameter>();
			parameters.Add (new MySqlParameter ("@user_id", notificationPreferences.UserId));
			parameters.Add (new MySqlParameter ("@notify_blast_comments", (notificationPreferences.NotifyBlastComments ? 1 : 0)));
			parameters.Add (new MySqlParameter ("@notify_media_comments", (notificationPreferences.NotifyMediaComments ? 1 : 0)));
			parameters.Add (new MySqlParameter ("@notify_shop_comments_purchases", (notificationPreferences.NotifyShopCommentsPurchases ? 1 : 0)));
            parameters.Add (new MySqlParameter ("@notify_event_invites", (notificationPreferences.NotifyEventInvites ? 1 : 0)));
            parameters.Add (new MySqlParameter ("@notify_friend_birthdays", (notificationPreferences.NotifyFriendBirthdays ? 1 : 0)));
            parameters.Add (new MySqlParameter ("@notify_world_blasts", (notificationPreferences.NotifyWorldBlasts ? 1 : 0)));
            parameters.Add (new MySqlParameter ("@notify_world_requests", (notificationPreferences.NotifyWorldRequests ? 1 : 0)));
			
			ret = Db.Master.ExecuteNonQuery(sqlString, parameters);
		
			CentralCache.Remove(CentralCache.keyUserNotificationPref(notificationPreferences.UserId));
			return ret;
		}

        /// <summary>
        /// adjust the user balance by calling the stored proc that logs messages and returns id of record
        /// inserted into the wok_transaction_log table (wok_transaction_log_id)
        /// </summary>
        public int AdjustUserBalance(int userId, string keiPointId, Double amount, UInt16 transType, ref int wok_transaction_log_id)
        {
            string sql = "CALL apply_transaction_to_user_balance( @userId, @amount, @transType, @keiPointId, @rc, @newBalance, @tranId ); SELECT CAST(@rc as UNSIGNED INT) as rc, CAST(@newBalance as DECIMAL) as newBalance, CAST(@tranId as UNSIGNED INT) as tranId;";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@keiPointId", keiPointId));
            parameters.Add(new MySqlParameter("@amount", amount));
            parameters.Add(new MySqlParameter("@transType", transType));

            DataRow row = Db.Master.GetDataRow(sql.ToString(), parameters);

            if (Convert.ToInt32(row["rc"]) != 0) // 0 = ok, 1 = insufficient funds, -1 = error
            {
                throw new Exception(Convert.ToString(row["rc"]));
            }

            //return the wok transaction id for use
            wok_transaction_log_id = Convert.ToInt32(row["tranId"]);

            // Invalidate cache
            CentralCache.Remove(CentralCache.keyKanevaUserBalances + userId.ToString());

            return 0;
        }

        /// <summary>
        /// adjust the user balance by calling the stored proc that logs messages and returns id of record
        /// inserted into the wok_transaction_log table (wok_transaction_log_id) as well as the wok_transaction_log_detials table
        /// </summary>
        public int AdjustUserBalanceWithDetails(int userId, string keiPointId, Double amount, UInt16 transType, int globalId, int quantity, ref int wok_transaction_log_id)
        {
            string sql = "CALL apply_transaction_and_details_to_user_balance ( @userId, @amount, @transType, @keiPointId, @globalId, @quantity, @rc, @newBalance, @tranId ); SELECT CAST(@rc as UNSIGNED INT) as rc, CAST(@newBalance as DECIMAL) as newBalance, CAST(@tranId as UNSIGNED INT) as tranId;";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@keiPointId", keiPointId));
            parameters.Add(new MySqlParameter("@amount", amount));
            parameters.Add(new MySqlParameter("@transType", transType));
            parameters.Add(new MySqlParameter("@globalId", globalId));
            parameters.Add(new MySqlParameter("@quantity", quantity));

            DataRow row = Db.Master.GetDataRow(sql.ToString(), parameters);

            if (Convert.ToInt32(row["rc"]) != 0) // 0 = ok, 1 = insufficient funds, -1 = error
            {
                throw new Exception("Not enough currency in account!");
            }

            //return the wok transaction id for use in back up
            wok_transaction_log_id = Convert.ToInt32(row["tranId"]);

            // Invalidate cache
            CentralCache.Remove(CentralCache.keyKanevaUserBalances + userId.ToString());
            
            return 0;
        }

        public FraudDetect GetFraudDetect (int userId)
        {
            // Read 24 hour attempts
            FraudDetect fraudDetect = (FraudDetect)CentralCache.Get(CentralCache.keyKanevaFraudDetect + userId.ToString());

            if (fraudDetect == null)
            {
                fraudDetect = new FraudDetect();
                fraudDetect.Expires = DateTime.Now.AddDays(1);
            }
            else
            {
                // Do this since Memcached Replace updates the expired flag for another 24 hours
                if (fraudDetect.Expires < DateTime.Now)
                {
                    // Reset it this one has expired
                    CentralCache.Remove(CentralCache.keyKanevaFraudDetect + userId.ToString());
                    fraudDetect = new FraudDetect();
                    fraudDetect.Expires = DateTime.Now.AddDays(1);
                }
            }

            return fraudDetect;
        }

        public void SaveFraudDetect(int userId, FraudDetect fraudDetect)
        {
            //FraudDetect fraudDetectTemp = (FraudDetect)CentralCache.Get(CentralCache.keyKanevaFraudDetect + userId.ToString());

            //if (fraudDetectTemp == null)
            //{
                CentralCache.Store(CentralCache.keyKanevaFraudDetect + userId.ToString(), fraudDetect, TimeSpan.FromHours (24));
            //}
            //else
            //{
            //    // Use replace to save teh 24 hour timeframe
            //    CentralCache.Replace( CentralCache.keyKanevaFraudDetect + userId.ToString(), fraudDetect);
            //}
        }

        /// <summary>
        /// Insert a new user
        /// </summary>
        /// <returns></returns>
        public int InsertUser(string username, string password, string salt, int role, int accountType,
            string first_name, string last_name, string display_name, string gender, string homepage, string email, DateTime birthdate,
            string key_value, string country, string zipCode, string registrationKey, string ipAddress, int validationStatus, int joinSourceId)
        {
            // NOTE - int validationStatus. Jeff did not implement validatation status in SP?

            string sqlString = "CALL add_user ( " +
                " @username, @password, @salt, @role, @accountType," +
                " @first_name, @last_name, @display_name, @gender," +
                " @homepage, @email, 0, @key_value, @birthdate, " +
                " @country, @zipCode, @registrationKey, " +
                " INET_ATON(@ipAddress), INET_ATON(@ipAddress), @joinSourceId, @userId); SELECT CAST(@userId as UNSIGNED INT) as userId;";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@username", username));
            parameters.Add(new MySqlParameter("@password", password));
            parameters.Add(new MySqlParameter("@salt", salt));
            parameters.Add(new MySqlParameter("@role", role));
            parameters.Add(new MySqlParameter("@accountType", accountType));

            parameters.Add(new MySqlParameter("@first_name", first_name));
            parameters.Add(new MySqlParameter("@last_name", last_name));
            parameters.Add(new MySqlParameter("@display_name", display_name));
            parameters.Add(new MySqlParameter("@gender", gender));

            parameters.Add(new MySqlParameter("@homepage", homepage));
            parameters.Add(new MySqlParameter("@email", email));
            parameters.Add(new MySqlParameter("@key_value", key_value));
            parameters.Add(new MySqlParameter("@birthdate", birthdate));

            parameters.Add(new MySqlParameter("@country", country));
            parameters.Add(new MySqlParameter("@zipCode", zipCode));
            parameters.Add(new MySqlParameter("@registrationKey", registrationKey.Substring(0, 49)));
            parameters.Add(new MySqlParameter("@ipAddress", ipAddress));
            parameters.Add(new MySqlParameter("@joinSourceId", joinSourceId));

            //Db.Master.ExecuteNonQuery(sqlString, parameters);
            DataRow dr = Db.Master.GetDataRow(sqlString, parameters);
            return Convert.ToInt32 (dr["userId"]);
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user_id"></param>
        public void UpdateUserAccount(int userId, string firstName, string lastName, string email)
        {
            string sqlUpdate = "CALL update_user_account " +
                " (@userId, @firstName," +
                " @lastName, @email)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@firstName", firstName));
            parameters.Add(new MySqlParameter("@lastName", lastName));
            parameters.Add(new MySqlParameter("@email", email));
            Db.Master.ExecuteNonQuery(sqlUpdate, parameters);

            // Invalidate user from cache
            InvalidateKanevaUserCache(userId);
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user_id"></param>
        public void UpdateUserAccount(int userId, string displayName, string email, string country, string zipCode)
        {
            string sqlUpdate = "CALL update_user_account2 " +
                " (@userId, @displayName, " +
                " @email, @country, @zipCode)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@displayName", displayName));
            parameters.Add(new MySqlParameter("@email", email));
            parameters.Add(new MySqlParameter("@country", country));
            parameters.Add(new MySqlParameter("@zipCode", zipCode));
            Db.Master.ExecuteNonQuery(sqlUpdate, parameters);

            // Invalidate user from cache
            InvalidateKanevaUserCache(userId);
        }

        /// <summary>
        /// UpdateUserName
        /// </summary>
        public int UpdateUserName(int userId, string oldName, string newName)
        {
            try
            {
                string sqlSelect = "CALL rename_user(@oldName, @newName, @rc); SELECT CAST(@rc as UNSIGNED INT) as rc;";
                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@oldName", oldName));
                parameters.Add(new MySqlParameter("@newName", newName));
                DataRow dr = Db.Master.GetDataRow(sqlSelect, parameters);

                // Remove entry from cache
                InvalidateKanevaUserCache(userId);
                return Convert.ToInt32(dr["rc"]);
            }
            catch
            {
                return -1;
            }
        }

        
        public int UpdateUserHomeCommunityId(int userId, int communityId)
        {
            string sqlString = "UPDATE users " +
                " SET home_community_id = @communityId " +
                " WHERE user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            parameters.Add(new MySqlParameter("@userId", userId));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// Delete User By UserName
        /// </summary>
        public int CleanUpRegistrationIssue(string username)
        {
            string sql = "DELETE FROM users where username = @username";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@username", username));
            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Get user Id
        /// </summary>
        /// <returns></returns>
        public int GetUserIdFromUsername(string username)
        {
            string sql = "SELECT user_id FROM users WHERE username = @username";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@username", username));

            DataRow drUser = Db.Slave2.GetDataRow(sql, parameters);

            if (drUser != null)
            {
                return (int)drUser["user_id"];
            }
            
            return 0;
        }

        /// <summary>
        /// Get user home community id
        /// </summary>
        /// <returns></returns>
        public int GetUserHomeCommunityIdByUserId(int userId)
        {
            string sql = "SELECT home_community_id FROM users WHERE user_id = @userId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            DataRow drUser = Db.Slave2.GetDataRow(sql, parameters);

            if (drUser != null && drUser["home_community_id"] != DBNull.Value)
            {
                return (int)drUser["home_community_id"];
            }

            return 0;
        }

        /// <summary>
        /// Get user home community id
        /// </summary>
        /// <returns></returns>
        public int GetUserHomeCommunityIdByPlayerId(int playerId)
        {
            string sql = "SELECT home_community_id FROM users WHERE wok_player_id = @playerId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@playerId", playerId));

            DataRow drUser = Db.Slave2.GetDataRow(sql, parameters);

            if (drUser != null)
            {
                return (int)drUser["home_community_id"];
            }

            return 0;
        }

        /// <summary>
        /// Get user name
        /// </summary>
        /// <returns></returns>
        public string GetUserName (int userId)
        {
            string sql = "SELECT username FROM users WHERE user_id = @userid";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userid", userId));

            DataRow drUser = Db.Slave2.GetDataRow (sql, parameters);

            if (drUser != null)
            {
                return drUser["username"].ToString ();
            }

            return "";
        }

        // **********************************************************************************************
        // Friends
        // **********************************************************************************************
        #region Friend Functions

        /// <summary>
        /// Get the friends for a user
        /// </summary>
        public PagedList<Friend> GetFriends(int userId, int friendId, string filter, string orderby, int pageNumber, int pageSize, bool bShowMature, string userNameFilter )
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            // Friends
            string strQuery =   "SELECT u.username, u.user_id, u.display_name, u.gender, f.glued_date, u.ustate, u.location, u.last_login, u.status_id, " +
                                " com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path," +
                                " cs.number_of_views, cs.number_of_diggs, TIMESTAMPDIFF(year, DATE(u.birth_date), now()) AS age, u.mature_profile, " +
                                " IF(TIMESTAMPDIFF(year, DATE(u.birth_date), now()) < 18, 1, 0) AS under18, cp.show_age, cp.show_gender, cp.show_location, " +
                                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture ";

            strQuery += " FROM users u " +
                        " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id, " + 
                        " friends f, communities_personal com " +
                        " INNER JOIN channel_stats cs ON com.community_id = cs.channel_id " +
                        " INNER JOIN community_preferences cp ON cp.community_id = com.community_id ";

            strQuery += " WHERE f.user_id = @userId " +
                        " AND u.user_id = f.friend_id " +
                        " AND u.active = 'Y' " +
                        " AND u.user_id = com.creator_id ";

            if (!bShowMature)
            {
                strQuery += " AND u.mature_profile = 0 ";
            }

            if (userNameFilter.Length > 0)
            {
                strQuery += " AND username LIKE @userNameFilter ";
                parameters.Add(new MySqlParameter("@userNameFilter", userNameFilter));
            }

            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            if (friendId > 0)
            {
                strQuery += " AND u.user_id = @friendId ";
                parameters.Add(new MySqlParameter("@friendId", friendId));
            }

           
            parameters.Add (new MySqlParameter ("@userId", userId));

            PagedDataTable dt = Db.Slave2.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<Friend> list = new PagedList<Friend> ();
            list.TotalCount = dt.TotalCount; 
            
            foreach (DataRow row in dt.Rows)
            {
                int id = int.Parse(row["user_id"].ToString());

                Friend friend = new Friend(id, row["username"].ToString(), row["display_name"].ToString(), row["gender"].ToString(), Convert.ToDateTime(row["glued_date"]),
                    row["ustate"].ToString(), row["location"].ToString(), row["name_no_spaces"].ToString(),
                    row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(),
                    row["thumbnail_square_path"].ToString (), int.Parse (row["number_of_views"].ToString ()), int.Parse (row["number_of_diggs"].ToString ()),
                    int.Parse (row["status_id"].ToString ()), Convert.ToInt32 (row["age"]), Convert.ToInt32 (row["mature_profile"]).Equals (1),
                    Convert.ToInt32(row["under18"]).Equals(1), 
                    Convert.ToInt32(row["show_age"]).Equals(1), 
                    Convert.ToInt32(row["show_gender"]).Equals(1), 
                    Convert.ToInt32(row["show_location"]).Equals(1)                    
                );

                friend.FacebookSettings.FacebookUserId = Convert.ToUInt64 (row["fb_user_id"]);
                friend.FacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (row["use_facebook_profile_picture"]);

                list.Add (friend);
            }
            return list;
        }

        /// <summary>
        /// Get the friends for a user
        /// </summary>
        public List<Friend> GetFriendsFast (int userId, int friendId, string filter, string orderby, int pageNumber, int pageSize)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            // Friends
            string strQuery = "SELECT u.username, u.user_id, u.display_name, u.gender, f.glued_date, u.ustate, u.location, u.last_login, u.status_id, " +
                                " com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, " +
                                " cs.number_of_views, cs.number_of_diggs, TIMESTAMPDIFF(year, DATE(u.birth_date), now()) AS age, u.mature_profile, " +
                                " IF(TIMESTAMPDIFF(year, DATE(u.birth_date), now()) < 18, 1, 0) AS under18, cp.show_age, cp.show_gender, cp.show_location ";

            strQuery += " FROM users u, friends f, communities_personal com " +
                        " INNER JOIN channel_stats cs ON com.community_id = cs.channel_id " +
                        " INNER JOIN community_preferences cp ON cp.community_id = com.community_id";

            strQuery += " WHERE f.user_id = @userId " +
                        " AND u.user_id = f.friend_id " +
                        " AND u.active = 'Y' " +
                        " AND u.user_id = com.creator_id ";

            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            if (friendId > 0)
            {
                strQuery += " AND u.user_id = @friendId ";
                parameters.Add(new MySqlParameter("@friendId", friendId));
            }

            if (orderby.Length > 0)
            {
                strQuery += " ORDER BY " + orderby;
            }

            int offset = (pageNumber - 1) * pageSize;
            strQuery += " LIMIT " + offset + ", " + pageSize;

            parameters.Add(new MySqlParameter("@userId", userId));

            DataTable dt = Db.Slave2.GetDataTable (strQuery, parameters);

            List<Friend> list = new List<Friend>();
            //list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                int id = int.Parse(row["user_id"].ToString());

                list.Add(new Friend(id, row["username"].ToString(), row["display_name"].ToString(), row["gender"].ToString(), Convert.ToDateTime(row["glued_date"]),
                    row["ustate"].ToString(), row["location"].ToString(), row["name_no_spaces"].ToString(),
                    row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(),
                    int.Parse(row["number_of_views"].ToString()), int.Parse(row["number_of_diggs"].ToString()),
                    int.Parse(row["status_id"].ToString()), Convert.ToInt32(row["age"]), Convert.ToInt32(row["mature_profile"]).Equals(1),
                    Convert.ToInt32(row["under18"]).Equals(1),
                    Convert.ToInt32(row["show_age"]).Equals(1),
                    Convert.ToInt32(row["show_gender"]).Equals(1),
                    Convert.ToInt32(row["show_location"]).Equals(1)
                ));
            }
            return list;
        }

         /// <summary>
        /// GetFriendGroup
        /// </summary>
        public FriendGroup GetFriendGroup (int friendGroupId, int ownerId)
        {
            string sql = "SELECT fg.friend_group_id, fg.name, fg.owner_id, fg.created_datetime, fg.friend_count " +
				" FROM friend_groups fg " +
				" WHERE fg.friend_group_id = @friendGroupId" +
				" AND fg.owner_id = @ownerId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@ownerId", ownerId));
            parameters.Add(new MySqlParameter("@friendGroupId", friendGroupId));

            DataRow row = Db.Slave2.GetDataRow(sql.ToString(), parameters);

            if (row == null)
            {
                return new FriendGroup();
            }

            return new FriendGroup(Convert.ToInt32(row["friend_group_id"]), Convert.ToInt32(row["owner_id"]),
                row["name"].ToString(), Convert.ToDateTime(row["created_datetime"]), Convert.ToUInt32(row["friend_count"])
                );                
        }

        /// <summary>
        /// GetFriendsInGroup
        /// </summary>
        public PagedList<Friend> GetFriendsInGroup (int friendGroupId, int ownerId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // Friends I invited
            string strQuery = "SELECT fgf.friend_id, u.username, u.user_id, " +
                " u.ustate, u.location, u.gender, u.last_login, u.display_name, u.status_id, " +
                " com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path, " +
                " f.glued_date, kcs.number_of_diggs, kcs.number_of_views, TIMESTAMPDIFF(year,DATE(u.birth_date),now()) AS age, u.mature_profile, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture " +
                " FROM friend_groups fg, friend_group_friends fgf, communities_personal com " +
                " INNER JOIN channel_stats kcs ON com.community_id = kcs.channel_id, " +
                " users u " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id, " +
                " friends f " +
                " WHERE fgf.friend_group_id = @friendGroupId" +
                " AND fg.owner_id = @ownerId " +
                " AND fg.friend_group_id = fgf.friend_group_id " +
                " AND fgf.friend_id = u.user_id " +
                " AND u.active = 'Y' " +
                " AND u.user_id = com.creator_id " +
                " AND f.user_id = @ownerId " +
                " AND u.user_id = f.friend_id ";

            // Filter it?
            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@ownerId", ownerId));
            parameters.Add(new MySqlParameter("@friendGroupId", friendGroupId));

            PagedDataTable dt = Db.Slave2.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<Friend> list = new PagedList<Friend> ();
            list.TotalCount = dt.TotalCount; 
            
            foreach (DataRow row in dt.Rows)
            {
                int id = int.Parse(row["user_id"].ToString());

                Friend friend = new Friend (id, row["username"].ToString (), row["display_name"].ToString (), row["gender"].ToString (), Convert.ToDateTime (row["glued_date"]),
                    row["ustate"].ToString(), row["location"].ToString(), row["name_no_spaces"].ToString(),
                    row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(),
                    row["thumbnail_square_path"].ToString (), int.Parse (row["number_of_views"].ToString ()), int.Parse (row["number_of_diggs"].ToString ()),
                    int.Parse (row["status_id"].ToString ()), Convert.ToInt32 (row["age"]), Convert.ToInt32 (row["mature_profile"]).Equals (1)
                );
                friend.FacebookSettings.FacebookUserId = Convert.ToUInt64 (row["fb_user_id"]);
                friend.FacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (row["use_facebook_profile_picture"]);

                list.Add (friend);
            }
            return list;
        }

        /// <summary>
        /// Search Users
        /// </summary>
        public PagedList<User> SearchUsers(bool onlyAccessPass, bool showMature, string searchString, bool male, bool female, bool bOnlyPhoto,
            int ageFrom, int ageTo,
            string country, string zipCode, int zipMiles, int newWithinDays, int updatedWithinDays,
            int heightFromFeet, int heightFromInches, int heightToFeet, int heightToInches,
            string ethnicity, string religion,
            string relationship, string orientation, string education,
            string drink, string smoke,
            string interest, bool bOnlineNow, string hometown,
            string filter, string orderBy, int pageNumber, int pageSize)
        {
            Sphinx sphinx = new Sphinx();
            string newSearchString = "";
            string userIds = "";
            int totalCount = 0;

            // Do some cleaning
            interest = sphinx.StripSphinxSearchString(interest);
            searchString = sphinx.StripSphinxSearchString(searchString);

            using (ConnectionBase connection = new PersistentTcpConnection(sphinx.Host, sphinx.Port))
            {
                connection.ConnectionTimeout = 5000;

                // Search interests
                if (interest.Trim().Length > 0)
                {
                    newSearchString = "@interests " + interest;
                }
                // Search by username
                else if (searchString.Length > 0)
                {
                    // Escape some strings sphinx has issues with \()|-!@~/^$*
                    searchString = sphinx.CleanSphinxSearchString(searchString);

                    // Try an exact match on EMAIL?
                    if (searchString.IndexOf("@") > 0)
                    {
                        newSearchString = "@email " + searchString;
                    }
                    else
                    {
                        char[] splitter = { ' ' };
                        string[] arKeywords = null;

                        // Did they enter multiples?
                        arKeywords = searchString.Split(splitter);

                        if (arKeywords.Length > 1)
                        {
                            newSearchString = "";
                            for (int j = 0; j < arKeywords.Length; j++)
                            {
                                // Do a sphinx OR for now
                                if (j < arKeywords.Length - 1)
                                {
                                    newSearchString += "@(username,display_name,first_name,last_name,location) " + arKeywords[j].ToString() + " | ";
                                }
                                else
                                {
                                    newSearchString += "@(username,display_name,first_name,last_name,location) " + arKeywords[j].ToString();
                                }
                            }
                        }
                        else
                        {
                            newSearchString = "@(username,display_name,first_name,last_name,location) " + searchString;
                        }

                    }
                }

                SearchQuery searchQuery = new SearchQuery(newSearchString);

                // Set match mode to SPH_MATCH_EXTENDED2
                searchQuery.MatchMode = MatchMode.Extended2;

                // Add Sphinx index name to list
                //searchQuery.Indexes.Add("people_srch_idx");
                searchQuery.Indexes.Add("people_srch_idx");

                // Set up paging
                searchQuery.Limit = pageSize;
                searchQuery.Offset = (pageNumber - 1) * pageSize;

                // Create search command object
                SearchCommand searchCommand = new SearchCommand(connection);

                // Add newly created search query object to query list
                searchCommand.QueryList.Add(searchQuery);

                if (bOnlineNow)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @ustate !" + User.ONLINE_USTATE_OFF; ;
                }

                // Filtering
                if (male)
                {
                    if (female)
                    {
                        // both no need to add it, get them all
                        // sqlWhereClause += " AND su.gender IN ('M','F')";
                    }
                    else
                    {
                        if (searchQuery.Query.Length > 0)
                        {
                            searchQuery.Query = searchQuery.Query + " & ";
                        }
                        searchQuery.Query = searchQuery.Query + " @gender M ";
                    }
                }
                else
                {
                    if (female)
                    {
                        if (searchQuery.Query.Length > 0)
                        {
                            searchQuery.Query = searchQuery.Query + " & ";
                        }
                        searchQuery.Query = searchQuery.Query + " @gender F ";
                    }
                    else
                    {
                        //sqlWhereClause += " AND su.gender NOT IN ('M','F')";
                    }
                }

                if (hometown.Length > 0)
                {
                    // hometown
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @hometown " + hometown;
                }

                if (country.Length > 0)
                {
                    // Country
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @country " + country;
                }

                if (!showMature)
                {
                    searchQuery.AttributeFilters.Add("mature_profile", (uint) 0, false);
                }
                if (showMature && onlyAccessPass)
                {
                    searchQuery.AttributeFilters.Add("mature_profile", (uint)0, true);
                }

                if (bOnlyPhoto)
                {
                    //if (searchQuery.Query.Length > 0)
                    //{
                    //    searchQuery.Query = searchQuery.Query + " & ";
                    //}
                    //searchQuery.Query = searchQuery.Query + " @has_thumbnail Y ";
                    searchQuery.AttributeFilters.Add("has_thumbnail", (uint)1, false);
                }

                if (ethnicity.Length > 0)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @ethnicity " + ethnicity;
                }

                if (religion.Length > 0)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @religion " + religion;
                }

                if (drink.Length > 0)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @drink " + drink;
                }

                if (smoke.Length > 0)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @smoke " + smoke;
                }

                //// Distance or country
                if (country.Length == 0 || country.ToLower() == "us")
                {
                    // Is it a numeric zip?
                    if (zipCode.Length > 0 && IsNumeric(zipCode))
                    {
                        // Zip
                        if (zipMiles > 0 && zipCode.Length > 0)
                        {
                            string selectZip = " SELECT latitude, longitude " +
                                " FROM zip_codes " +
                                " WHERE zip_code = @zipCode ";

                            List<IDbDataParameter> zipParameters = new List<IDbDataParameter>();
                            zipParameters.Add(new MySqlParameter("@zipCode", zipCode));
                            DataRow drZip = Db.Slave.GetDataRow(selectZip, zipParameters);

                            if (drZip != null)
                            {
                                if (drZip["longitude"] != null || drZip["latitude"] != null)
                                {
                                    //double upperLat = 0.0, upperLong = 0.0, lowerLat = 0.0, lowerLong = 0.0;
                                    //StoreUtility.GetCoordinates(zipMiles, Convert.ToDouble(drZip["latitude"]), Convert.ToDouble(drZip["longitude"]), ref upperLat, ref upperLong, ref lowerLat, ref lowerLong);

                                    //sqlTableList += " INNER JOIN kaneva.zip_codes zip ON su.zip_code = zip.zip_code " +
                                    //    " AND latitude BETWEEN @lowerLat AND @upperLat " +
                                    //    " AND longitude BETWEEN @lowerLong AND @upperLong ";

                                    //parameters.Add("@lowerLat", lowerLat);
                                    //parameters.Add("@upperLat", upperLat);
                                    //parameters.Add("@lowerLong", lowerLong);
                                    //parameters.Add("@upperLong", upperLon();
                                    float latInDegree = (float)Convert.ToDecimal(drZip["latitude"]);
                                    float longInDegree = (float)Convert.ToDecimal(drZip["longitude"]);

                                    // Radian = Math.Pi * Degree / 180 or get rid of extra operation Radian = Degree * 0.01745
                                    //searchQuery.GeoAnchor = new GeoAnchor("latitude", (float)(latInDegree * 0.01745), "longitude", (float) (longInDegree * 0.01745));
                                    searchQuery.GeoAnchor = new GeoAnchor("latitude", (float)(Math.PI * latInDegree / 180), "longitude", (float)(Math.PI * longInDegree / 180));

                                    // 1 Miles = 1609.344 meters
                                    searchQuery.AttributeFilters.Add("@geodist", (float)0.0, (float)(zipMiles * 1609), false);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (zipCode.Length > 0)
                        {
                            if (searchQuery.Query.Length > 0)
                            {
                                searchQuery.Query = searchQuery.Query + " & ";
                            }
                            // Assume zip code field is city or state
                            searchQuery.Query = searchQuery.Query + " @location " + zipCode;
                        }
                    }
                }

                if (ageFrom > 0 && ageTo > 0)
                {
                    searchQuery.AttributeFilters.Add("age", (uint)ageFrom, (uint)ageTo, false);
                }
                else if (ageFrom > 0)
                {
                    searchQuery.AttributeFilters.Add("age", (uint)ageFrom, (uint)150, false);
                }

                long minHeight = 0;
                long maxHeight = 0;

                if (heightFromFeet > 0 && heightFromInches > -1)
                {
                    minHeight = heightFromFeet * 12 + heightFromInches;
                }

                if (heightToFeet > 0 && heightToInches > -1)
                {
                    maxHeight = heightToFeet * 12 + heightToInches; 
                }

                if (minHeight > 0)
                {
                    if (maxHeight > 0)
                    {
                        searchQuery.AttributeFilters.Add("height", (long)minHeight, (long)maxHeight, false);
                    }
                    else
                    {
                        searchQuery.AttributeFilters.Add("height", (long)minHeight, (long)200, false);
                    }
                }
                else if (maxHeight > 0)
                {
                    searchQuery.AttributeFilters.Add("height", (long)0, (long)maxHeight, false);
                }

                if (relationship.Length > 0)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @relationship " + relationship;
                }

                if (orientation.Length > 0)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }

                    searchQuery.Query = searchQuery.Query + " @orientation " + orientation;
                }

                if (education.Length > 0)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }

                    searchQuery.Query = searchQuery.Query + " @education " + education;
                }

                if (filter.Length > 0)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }

                    searchQuery.Query = searchQuery.Query + filter;
                }


                // New in last x number of days?
                if (newWithinDays > 0)
                {
                    //searchQuery.AttributeFilters.Add("signup_date", (long)DateTime.Now.AddDays(-newWithinDays).Ticks, (long)DateTime.Now.AddYears(1).Ticks, false);
                    searchQuery.AttributeFilters.Add("signup_date", DateTime.Now.AddDays(-newWithinDays), DateTime.Now.AddYears(1), false);
                    //searchQuery.AttributeFilters.Add("signup_date", (long)DateTime.Now.AddDays(-newWithinDays).Ticks, (long)DateTime.Now.AddYears(1).Ticks, false);
                }

                //// Updated in last x number of days?
                //if (updatedWithinDays > 0)
                //{
                //    sqlWhereClause += " AND su.last_login > " + dbUtility.GetDatePlusDays(-updatedWithinDays);
                //}

                // Always only get active
                if (searchQuery.Query.Length > 0)
                {
                    searchQuery.Query = searchQuery.Query + " & ";
                }
                searchQuery.Query = searchQuery.Query + " @active Y";


                // Sorting
                if (orderBy.Trim().Length.Equals(0))
                {
                    searchQuery.SortMode = ResultsSortMode.Relevance;
                }
                else
                {
                    if (orderBy.ToUpper().Contains("ASC"))
                    {
                        searchQuery.SortMode = ResultsSortMode.AttributeAscending;
                    }
                    else
                    {
                        searchQuery.SortMode = ResultsSortMode.AttributeDescending;
                    }

                    // Strip ASC, DESC
                    searchQuery.SortBy = orderBy.Replace("ASC", "").Replace("DESC", "").Trim();
                }


                // Execute command on server and obtain results
                try
                {
                    searchCommand.Execute();
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error calling Sphinx " + sphinx.Host + ":" + sphinx.Port, exc);
                }

                // Build result IN clause for MySQL
                foreach (SearchQueryResult result in searchCommand.Result.QueryResults)
                {
                    foreach (Match match in result.Matches)
                    {
                        if (userIds.Length > 0)
                        {
                            userIds = userIds + "," + match.DocumentId.ToString();
                        }
                        else
                        {
                            userIds = match.DocumentId.ToString();
                        }
                    }

                    // Note this will grab the last total count in collection, ok for this case, but not best place to do this
                    totalCount = (result.TotalFound > searchQuery.MaxMatches ? searchQuery.MaxMatches : result.TotalFound);
                }

                // Nothing found in Sphinx
                if (userIds.Length.Equals(0))
                {
                    PagedList<User> emptyList = new PagedList<User>();
                    emptyList.TotalCount = 0;
                    return emptyList;
                }

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();

                string strSelect = "SELECT u.user_id, u.username, u.display_name, " +
                        " u.gender, u.status_id, u.signup_date, u.last_login, " +
                        " u.location, u.mature_profile, u.ustate, " +
                        " com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_square_path, " +
                        " cs.number_of_diggs, cs.number_of_views, cs.number_times_shared, " +
                        " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture " +
                    " FROM users u " +
                    " INNER JOIN communities com ON u.user_id = com.creator_id AND is_personal = 1 " +
                    " INNER JOIN channel_stats cs ON com.community_id = cs.channel_id " +
                    " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id " +
                    " where u.user_id in " +
                    " (" + userIds + ")" +
                    " ORDER BY FIELD(u.user_id," + userIds + ")";

                DataTable dt = Db.Slave.GetDataTable(strSelect, parameters);

                PagedList<User> list = new PagedList<User>();
                list.TotalCount = Convert.ToUInt32(totalCount);
                User user;

                foreach (DataRow row in dt.Rows)
                {
                    user = new User();
                    user.UserId = Convert.ToInt32(row["user_id"]);
                    user.Username = row["username"].ToString();
                    user.DisplayName = row["display_name"].ToString();
                    user.Gender = row["gender"].ToString();
                    user.StatusId = Convert.ToInt32(row["status_id"]);
                    user.SignupDate = Convert.ToDateTime(row["signup_date"]);
                    user.LastLogin = Convert.ToDateTime(row["last_login"]);
                    user.Location = row["location"].ToString();
                    user.MatureProfile = row["mature_profile"].ToString().Equals("Y");
                    user.Ustate = row["ustate"].ToString();
                    user.NameNoSpaces = row["name_no_spaces"].ToString();
                    user.ThumbnailSquarePath = row["thumbnail_square_path"].ToString ();
                    user.ThumbnailSmallPath = row["thumbnail_small_path"].ToString();
                    user.ThumbnailMediumPath = row["thumbnail_medium_path"].ToString();
                    user.NumberOfDiggs = Convert.ToInt32(row["number_of_diggs"]);
                    user.NumberOfViews = Convert.ToInt32(row["number_of_views"]);
                    user.NumberTimesShared = Convert.ToInt32(row["number_times_shared"]);
                    user.FacebookSettings.UserId = Convert.ToInt32 (row["user_id"]);
                    user.FacebookSettings.FacebookUserId = Convert.ToUInt64 (row["fb_user_id"]);
                    user.FacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (row["use_facebook_profile_picture"]);

                    list.Add(user);
                }

                return list;

            }
        }

        /// <summary>
        /// Search the friends for a user
        /// </summary>
        public PagedList<Friend> SearchFriends (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // Friends I invited
            string strQuery = "SELECT u.username, u.user_id, u.display_name, u.gender, f.glued_date, u.ustate, u.location, u.last_login, u.status_id, " +
                " com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path, " +
                " cs.number_of_views, cs.number_of_diggs, TIMESTAMPDIFF(year,DATE(u.birth_date),now()) AS age, u.mature_profile ";

            strQuery += " FROM kaneva.users u, kaneva.friends f, kaneva.communities_personal com " +
                " INNER JOIN kaneva.channel_stats cs ON com.community_id = cs.channel_id ";

            strQuery += " WHERE f.user_id = @userId " +
                " AND u.user_id = f.friend_id " +
                " AND u.active = 'Y' " +
                " AND u.user_id = com.creator_id ";

            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            PagedDataTable dt = Db.Search.GetPagedDataTable (strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<Friend> list = new PagedList<Friend> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                int id = int.Parse (row["user_id"].ToString ());

                list.Add (new Friend (id, row["username"].ToString (), row["display_name"].ToString (), row["gender"].ToString (), Convert.ToDateTime (row["glued_date"]),
                    row["ustate"].ToString (), row["location"].ToString (), row["name_no_spaces"].ToString (),
                    row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (),
                    row["thumbnail_square_path"].ToString (), int.Parse (row["number_of_views"].ToString ()), int.Parse (row["number_of_diggs"].ToString ()),
                    int.Parse (row["status_id"].ToString ()), Convert.ToInt32 (row["age"]), Convert.ToInt32 (row["mature_profile"]).Equals (1)
                ));
            }
            return list;
        }

        /// <summary>
        /// Search the friends for a user
        /// </summary>
        public PagedList<Friend> GetFriendsRecommended(string strCountry, string strZipCode, int age, string orderBy, int pageNumber, int pageSize)
        {
            Sphinx sphinx = new Sphinx();
            string userIds = "";
            int totalCount = 0;

            using (ConnectionBase connection = new PersistentTcpConnection(sphinx.Host, sphinx.Port))
            {
                connection.ConnectionTimeout = 5000;

                SearchQuery searchQuery = new SearchQuery("");

                // Set match mode to SPH_MATCH_EXTENDED2
                searchQuery.MatchMode = MatchMode.Extended2;

                // Add Sphinx index name to list
                //searchQuery.Indexes.Add("people_srch_idx");
                searchQuery.Indexes.Add("people_srch_idx");

                // Set up paging
                searchQuery.Limit = pageSize;
                searchQuery.Offset = (pageNumber - 1) * pageSize;

                // Create search command object
                SearchCommand searchCommand = new SearchCommand(connection);

                // Add newly created search query object to query list
                searchCommand.QueryList.Add(searchQuery);

                if (strCountry.Length > 0)
                {
                    // Country
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @country " + strCountry;
                }

                if (strZipCode.Length > 0)
                {
                    // Country
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @zip_code " + strZipCode;
                }

                // Suggest 18+ to 18+, and 18- to 18-, filter. (JL)
                if (age > 17)
                {
                    searchQuery.AttributeFilters.Add("age", (uint) 18, (uint) 100, false);
                }
                else
                {
                    searchQuery.AttributeFilters.Add("age", (uint)0, (uint)17, false);
                }

                // Always only get active
                if (searchQuery.Query.Length > 0)
                {
                    searchQuery.Query = searchQuery.Query + " & ";
                }
                searchQuery.Query = searchQuery.Query + " @active Y";

                // Sorting
                if (orderBy.Trim().Length.Equals(0))
                {
                    searchQuery.SortMode = ResultsSortMode.AttributeDescending;
                    searchQuery.SortBy = "last_login";
                }
                else
                {
                    searchQuery.SortMode = ResultsSortMode.AttributeDescending;
                    searchQuery.SortBy = orderBy;
                }

                // Execute command on server and obtain results
                try
                {
                    searchCommand.Execute();
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error calling Sphinx " + sphinx.Host + ":" + sphinx.Port, exc);
                }

                // Build result IN clause for MySQL
                foreach (SearchQueryResult result in searchCommand.Result.QueryResults)
                {
                    foreach (Match match in result.Matches)
                    {
                        if (userIds.Length > 0)
                        {
                            userIds = userIds + "," + match.DocumentId.ToString();
                        }
                        else
                        {
                            userIds = match.DocumentId.ToString();
                        }
                    }

                    // Note this will grab the last total count in collection, ok for this case, but not best place to do this
                    totalCount = (result.TotalFound > searchQuery.MaxMatches ? searchQuery.MaxMatches : result.TotalFound);
                }

                // Nothing found in Sphinx
                if (userIds.Length.Equals(0))
                {
                    PagedList<Friend> emptyList = new PagedList<Friend>();
                    emptyList.TotalCount = 0;
                    return emptyList;
                }

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();

                string strSelect = "SELECT u.username, u.user_id, u.display_name, u.gender, " +
                    " u.ustate, u.location, u.last_login, u.status_id, " +
                    " com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_medium_path as thumbnail_large_path, " +
                    " com.thumbnail_square_path, cs.number_of_views, cs.number_of_diggs, u.age, u.mature_profile " +
                    " FROM users u " +
                    " INNER JOIN communities com ON u.user_id = com.creator_id AND is_personal = 1 " +
                    " INNER JOIN channel_stats cs ON com.community_id = cs.channel_id " +
                    " where u.user_id in " +
                    " (" + userIds + ")" +
                    " ORDER BY FIELD(u.user_id," + userIds + ")";

                DataTable dt = Db.Slave.GetDataTable(strSelect, parameters);

                PagedList<Friend> list = new PagedList<Friend>();
                list.TotalCount = Convert.ToUInt32(totalCount);

                foreach (DataRow row in dt.Rows)
                {
                    int id = int.Parse(row["user_id"].ToString());

                    list.Add(new Friend(id, row["username"].ToString(), row["display_name"].ToString(), row["gender"].ToString(), new DateTime(),
                        row["ustate"].ToString(), row["location"].ToString(), row["name_no_spaces"].ToString(),
                        row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(),
                        row["thumbnail_square_path"].ToString (), int.Parse (row["number_of_views"].ToString ()), int.Parse (row["number_of_diggs"].ToString ()),
                        int.Parse(row["status_id"].ToString()), Convert.ToInt32(row["age"]), Convert.ToInt32(row["mature_profile"]).Equals(1)
                    ));
                }

                return list;

            }
        }

        /// <summary>
        /// Search Users
        /// </summary>
        public PagedList<User> SearchUsers(bool onlyAccessPass, bool showMature, string searchString, bool male, bool female, bool bOnlyPhoto,
            int ageFrom, int ageTo,
            string country, string zipCode, int zipMiles, int newWithinDays, int updatedWithinDays,
            int heightFromFeet, int heightFromInches, int heightToFeet, int heightToInches,
            string ethnicity, string religion,
            string relationship, string orientation, string education,
            string drink, string smoke,
            string interest,
            string filter, string orderBy, int pageNumber, int pageSize)
        {
            Sphinx sphinx = new Sphinx();
            string newSearchString = "";
            string userIds = "";
            int totalCount = 0;

            searchString = sphinx.StripSphinxSearchString(searchString);

            using (ConnectionBase connection = new PersistentTcpConnection(sphinx.Host, sphinx.Port))
            {
                connection.ConnectionTimeout = 5000;

                // Search interests
                if (interest.Trim().Length > 0)
                {
                    newSearchString = "@interests " + searchString;
                }
                // Search by username
                else if (searchString.Length > 0)
                {
                    // Escape some strings sphinx has issues with \()|-!@~/^$*
                    searchString = sphinx.CleanSphinxSearchString(searchString);

                    // Try an exact match on EMAIL?
                    if (searchString.IndexOf("@") > 0)
                    {
                        newSearchString = "@email " + searchString;
                    }
                    else
                    {
                        char[] splitter = { ' ' };
                        string[] arKeywords = null;

                        // Did they enter multiples?
                        arKeywords = searchString.Split(splitter);

                        if (arKeywords.Length > 1)
                        {
                            newSearchString = "";
                            for (int j = 0; j < arKeywords.Length; j++)
                            {
                                 // Do a sphinx OR for now
                                if (j < arKeywords.Length - 1)
                                {
                                    newSearchString += "@(username,display_name,first_name,last_name,location) " + arKeywords[j].ToString() + " | ";
                                }
                                else
                                {
                                    newSearchString += "@(username,display_name,first_name,last_name,location) " + arKeywords[j].ToString();
                                }
                            }
                        }
                        else
                        {
                            newSearchString = "@(username,display_name,first_name,last_name,location) " + searchString;
                        }

                    }
                }

                SearchQuery searchQuery = new SearchQuery(newSearchString);
                    
                // Set match mode to SPH_MATCH_EXTENDED2
                searchQuery.MatchMode = MatchMode.Extended2;

                // Add Sphinx index name to list
                //searchQuery.Indexes.Add("people_srch_idx");
                searchQuery.Indexes.Add("people_srch_idx");

                // Set up paging
                searchQuery.Limit = pageSize;
                searchQuery.Offset = (pageNumber - 1) * pageSize;

                // Create search command object
                SearchCommand searchCommand = new SearchCommand(connection);

                // Add newly created search query object to query list
                searchCommand.QueryList.Add(searchQuery);

                // Filtering
                if (male)
                {
                    if (female)
                    {
                        // both no need to add it, get them all
                        // sqlWhereClause += " AND su.gender IN ('M','F')";
                    }
                    else
                    {
                        if (searchQuery.Query.Length > 0)
                        {
                            searchQuery.Query = searchQuery.Query + " & ";
                        }
                        searchQuery.Query = searchQuery.Query + " @gender M ";
                    }
                }
                else
                {
                    if (female)
                    {
                        if (searchQuery.Query.Length > 0)
                        {
                            searchQuery.Query = searchQuery.Query + " & ";
                        }
                        searchQuery.Query = searchQuery.Query + " @gender F ";
                    }
                    else
                    {
                        //sqlWhereClause += " AND su.gender NOT IN ('M','F')";
                    }
                }

                if (country.Length > 0)
                {
                    // Country
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @country " + country;
                }

                if (!showMature)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @mature_profile 0 ";
                }
                if (showMature && onlyAccessPass)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @mature_profile !0 ";
                }

                if (bOnlyPhoto)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @has_thumbnail Y ";
                }

                //if (ethnicity.Length > 0)
                //{
                //    sqlWhereClause += " AND su.ethnicity = @ethnicity ";
                //    parameters.Add("@ethnicity", ethnicity);
                //}

                //if (religion.Length > 0)
                //{
                //    sqlWhereClause += " AND su.religion = @religion ";
                //    parameters.Add("@religion", religion);
                //}

                //if (drink.Length > 0)
                //{
                //    sqlWhereClause += " AND su.drink = @drink ";
                //    parameters.Add("@drink", drink);
                //}

                //if (smoke.Length > 0)
                //{
                //    sqlWhereClause += " AND su.smoke = @smoke ";
                //    parameters.Add("@smoke", smoke);
                //}

                //// Distance or country
                if (country.Length == 0 || country.ToLower() == "us")
                {
                    // Is it a numeric zip?
                    if (zipCode.Length > 0 && IsNumeric(zipCode))
                    {
                        // Zip
                        if (zipMiles > 0 && zipCode.Length > 0)
                        {
                            string selectZip = " SELECT latitude, longitude " +
                                " FROM zip_codes " +
                                " WHERE zip_code = @zipCode ";

                            List<IDbDataParameter> zipParameters = new List<IDbDataParameter>();
                            zipParameters.Add(new MySqlParameter("@zipCode", zipCode));
                            DataRow drZip = Db.Slave.GetDataRow(selectZip, zipParameters);

                            if (drZip != null)
                            {
                                if (drZip["longitude"] != null || drZip["latitude"] != null)
                                {
                                    //double upperLat = 0.0, upperLong = 0.0, lowerLat = 0.0, lowerLong = 0.0;
                                    //StoreUtility.GetCoordinates(zipMiles, Convert.ToDouble(drZip["latitude"]), Convert.ToDouble(drZip["longitude"]), ref upperLat, ref upperLong, ref lowerLat, ref lowerLong);

                                    //sqlTableList += " INNER JOIN kaneva.zip_codes zip ON su.zip_code = zip.zip_code " +
                                    //    " AND latitude BETWEEN @lowerLat AND @upperLat " +
                                    //    " AND longitude BETWEEN @lowerLong AND @upperLong ";

                                    //parameters.Add("@lowerLat", lowerLat);
                                    //parameters.Add("@upperLat", upperLat);
                                    //parameters.Add("@lowerLong", lowerLong);
                                    //parameters.Add("@upperLong", upperLon();
                                    float latInDegree = (float)Convert.ToDecimal(drZip["latitude"]);
                                    float longInDegree = (float)Convert.ToDecimal(drZip["longitude"]);

                                    // Radian = Math.Pi * Degree / 180 or get rid of extra operation Radian = Degree * 0.01745
                                    //searchQuery.GeoAnchor = new GeoAnchor("latitude", (float)(latInDegree * 0.01745), "longitude", (float) (longInDegree * 0.01745));
                                    searchQuery.GeoAnchor = new GeoAnchor("latitude", (float)(Math.PI * latInDegree / 180), "longitude", (float)(Math.PI * longInDegree / 180));

                                    // 1 Miles = 1609.344 meters
                                    searchQuery.AttributeFilters.Add("@geodist", (float) 0.0, (float) (zipMiles * 1609), false);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (zipCode.Length > 0)
                        {
                            if (searchQuery.Query.Length > 0)
                            {
                                searchQuery.Query = searchQuery.Query + " & ";
                            }
                            // Assume zip code field is city or state
                            searchQuery.Query = searchQuery.Query + " @location " + zipCode;
                        }
                    }
                }

                if (ageFrom > 0 && ageTo > 0)
                {
                    searchQuery.AttributeFilters.Add("age", (uint)ageFrom, (uint)ageTo, false);
                }
                else if (ageFrom > 0)
                {
                    searchQuery.AttributeFilters.Add("age", (uint)ageFrom, (uint)150, false);
                }

                //if (heightFromFeet > 0 && heightFromInches > -1)
                //{
                //    sqlWhereClause += " AND su.height > @heightFromInches ";
                //    parameters.Add("@heightFromInches", heightFromFeet * 12 + heightFromInches);
                //}

                //if (heightToFeet > 0 && heightToInches > -1)
                //{
                //    sqlWhereClause += " AND su.height <= @heightToInches ";
                //    parameters.Add("@heightToInches", heightToFeet * 12 + heightToInches);
                //}

                if (relationship.Length > 0)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @relationship " + relationship;
                }

                if (orientation.Length > 0)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }

                    searchQuery.Query = searchQuery.Query + " @orientation " + orientation;
                }

                if (education.Length > 0)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }

                    searchQuery.Query = searchQuery.Query + " @education " + education;
                }


                // New in last x number of days?
                if (newWithinDays > 0)
                {
                    //searchQuery.AttributeFilters.Add("signup_date", (long)DateTime.Now.AddDays(-newWithinDays).Ticks, (long)DateTime.Now.AddYears(1).Ticks, false);
                    searchQuery.AttributeFilters.Add("signup_date",  DateTime.Now.AddDays(-newWithinDays), DateTime.Now.AddYears(1), false);
                    //searchQuery.AttributeFilters.Add("signup_date", (long)DateTime.Now.AddDays(-newWithinDays).Ticks, (long)DateTime.Now.AddYears(1).Ticks, false);
                }

                //// Updated in last x number of days?
                //if (updatedWithinDays > 0)
                //{
                //    sqlWhereClause += " AND su.last_login > " + dbUtility.GetDatePlusDays(-updatedWithinDays);
                //}

                // Always only get active
                if (searchQuery.Query.Length > 0)
                {
                    searchQuery.Query = searchQuery.Query + " & ";
                }
                searchQuery.Query = searchQuery.Query + " @active Y";


                // Sorting
                if (orderBy.Trim().Length.Equals(0))
                {
                    searchQuery.SortMode = ResultsSortMode.Relevance;
                }
                else
                {
                    if (orderBy.ToUpper().Contains("ASC"))
                    {
                        searchQuery.SortMode = ResultsSortMode.AttributeAscending;
                    }
                    else
                    {
                        searchQuery.SortMode = ResultsSortMode.AttributeDescending;
                    }

                    // Strip ASC, DESC
                    searchQuery.SortBy = orderBy.Replace("ASC", "").Replace("DESC", "").Trim();
                }


                // Execute command on server and obtain results
                try
                {
                    searchCommand.Execute();
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error calling Sphinx " + sphinx.Host + ":" + sphinx.Port, exc);
                }

                // Build result IN clause for MySQL
                foreach (SearchQueryResult result in searchCommand.Result.QueryResults)
                {
                    foreach (Match match in result.Matches)
                    {
                        if (userIds.Length > 0)
                        {
                            userIds = userIds + "," + match.DocumentId.ToString();
                        }
                        else
                        {
                            userIds = match.DocumentId.ToString();
                        }
                    }

                    // Note this will grab the last total count in collection, ok for this case, but not best place to do this
                    totalCount = (result.TotalFound > searchQuery.MaxMatches ? searchQuery.MaxMatches : result.TotalFound);
                }

                // Nothing found in Sphinx
                if (userIds.Length.Equals(0))
                {
                    PagedList<User> emptyList = new PagedList<User>();
                    emptyList.TotalCount = 0;
                    return emptyList;
                }

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();

                string strSelect = "SELECT u.user_id, u.username, u.display_name, " +
                        " u.gender, u.status_id, u.signup_date, u.last_login, " +
                        " u.location, u.mature_profile, u.ustate, " +
                        " com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_square_path, " +
                        " cs.number_of_diggs, cs.number_of_views, cs.number_times_shared, " +
                        " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture " + 
                    " FROM users u " +
                    " INNER JOIN communities com ON u.user_id = com.creator_id AND is_personal = 1 " +
                    " INNER JOIN channel_stats cs ON com.community_id = cs.channel_id " +
                    " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id " +
                    " where u.user_id in " +
                    " (" + userIds + ")" +
                    " ORDER BY FIELD(u.user_id," + userIds + ")";

                DataTable dt = Db.Slave.GetDataTable(strSelect, parameters);

                PagedList<User> list = new PagedList<User>();
                list.TotalCount = Convert.ToUInt32(totalCount);
                User user;

                foreach (DataRow row in dt.Rows)
                {
                    user = new User();
                    user.UserId = Convert.ToInt32(row["user_id"]);
                    user.Username = row["username"].ToString ();
                    user.DisplayName = row["display_name"].ToString ();
                    user.Gender = row["gender"].ToString();
                    user.StatusId = Convert.ToInt32(row["status_id"]);
                    user.SignupDate = Convert.ToDateTime(row["signup_date"]);
                    user.LastLogin = Convert.ToDateTime(row["last_login"]);
                    user.Location = row["location"].ToString();
                    user.MatureProfile = row["mature_profile"].ToString().Equals ("Y");
                    user.Ustate = row["ustate"].ToString();
                    user.NameNoSpaces = row["name_no_spaces"].ToString();
                    user.ThumbnailSquarePath = row["thumbnail_square_path"].ToString ();
                    user.ThumbnailSmallPath = row["thumbnail_small_path"].ToString();
                    user.ThumbnailMediumPath = row["thumbnail_medium_path"].ToString();
                    user.NumberOfDiggs = Convert.ToInt32(row["number_of_diggs"]);
                    user.NumberOfViews = Convert.ToInt32(row["number_of_views"]);
                    user.NumberTimesShared = Convert.ToInt32(row["number_times_shared"]);
                    user.FacebookSettings.UserId = Convert.ToInt32 (row["user_id"]);
                    user.FacebookSettings.FacebookUserId = Convert.ToUInt64 (row["fb_user_id"]);
                    user.FacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (row["use_facebook_profile_picture"]);

                    list.Add(user);
                }

                return list;

            }


            //string sqlWhereClause = " su.active = 'Y' ";

          

            

            //        sqlWhereClause += " AND MATCH (su.username, su.display_name, su.first_name, su.last_name, su.location) AGAINST (@searchString IN BOOLEAN MODE)";
            //        parameters.Add("@searchString", searchString);
            //    }
            //}

            //// Filter
            //if (filter.Length > 0)
            //{
            //    sqlWhereClause += " AND " + filter;
            //}

            //// Hack to get result set down when ordering by new for performance
            //if (country.Length == 0 && orderBy.Equals("_signup_date"))
            //{
            //    sqlWhereClause += " AND su.signup_date > ADDDATE(NOW(), INTERVAL -30 DAY)";
            //}

            //return dbUtility.GetPagedDataTable(sqlSelectList, sqlTableList, sqlWhereClause, orderBy, parameters, pageNumber, pageSize);
        }


        /// <summary>
        /// Is the number numeric?
        /// </summary>
        /// <param name="candidate"></param>
        /// <returns></returns>
        public static bool IsNumeric(string candidate)
        {
            char[] ca = candidate.ToCharArray();
            for (int i = 0; i < ca.Length; i++)
            {
                if (ca[i] > 57 || ca[i] < 48)
                {
                    // Handle negative number
                    if (i == 0 && ca[0] == '-')
                    {
                        // Staring with a -
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Insert a friend group
        /// </summary>
        /// <param name="group">required only when <c>messageType</c> is
        /// <returns>int</returns>
        public int InsertFriendGroup(FriendGroup group)
        {
            string sql = "INSERT INTO friend_groups (owner_id, name, created_datetime ) " +
                "VALUES (@ownerId, @groupName, NOW())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@ownerId", group.OwnerId));
            parameters.Add(new MySqlParameter("@groupName", group.Name));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Insert a Message
        /// </summary>
        /// <param name="channelId">required only when <c>messageType</c> is
        /// <code>Constants.eMESSAGE_TYPE.MEMBER_REQUEST</code>, pass 0 otherwise </param>
        /// <returns></returns>
        public int InsertMessage(Message message)
        {
            // Make sure the from is not blocked from this user
            if (IsUserBlocked(message.ToId, message.FromId))
            {
                return -99;
            }

            // Send the message
            string sql = "CALL add_messages (@fromId, @toId, @subject, @message, @replied, @messageType, @channelId, @messageId); SELECT CAST(@messageId as UNSIGNED INT) as messageId; ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@fromId", message.FromId));
            parameters.Add(new MySqlParameter("@toId", message.ToId));
            parameters.Add(new MySqlParameter("@subject", message.Subject));
            parameters.Add(new MySqlParameter("@message", message.MessageText));
            parameters.Add(new MySqlParameter("@replied", message.Replied));
            parameters.Add(new MySqlParameter("@messageType", message.Type));

            if (message.ChannelId > 0)
            {
                parameters.Add(new MySqlParameter("@channelId", message.ChannelId));
            }
            else
            {
                parameters.Add(new MySqlParameter("@channelId", DBNull.Value));
            }

            DataRow row = Db.Master.GetDataRow(sql, parameters);

            if (row != null)
            {
                message.MessageId = Convert.ToInt32(row["messageId"]);
            }

            return 0;
        }

        /// <summary>
        /// Is the user blocked
        /// </summary>
        /// <returns></returns>
//        public bool IsUserBlocked (int userId, int profileId)
//        {
//            return (IsUserBlocked (userId, profileId, true) != null);   
//        }

        /// <summary>
        /// Is the user blocked
        /// </summary>
        /// <returns></returns>
        public bool IsUserBlocked (int userId, int blockedUserId)
        {     //**BLOCK -- remove after test
       //     string sqlSelect = "SELECT block_id, user_id, is_comm_blocked, is_follow_blocked, is_zone_blocked " +
       //         " FROM blocked_users " +
       //         "WHERE user_id = @userId " +
       //         " AND profile_id = @profileId " +
       //         " AND zone_type = 3 ";

            string sqlSelect = "SELECT user_id " +
                " FROM blocked_users " +
                "WHERE user_id = @userId " +
                " AND blocked_user_id = @blockedUserId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add (new MySqlParameter ("@blockedUserId", blockedUserId));

            DataRow drBlockedUser = Db.Master.GetDataRow(sqlSelect, parameters);
            return drBlockedUser != null; 
        }

        public bool IsUserBlocked (int userId, int gameId, int communityId)
        {   //**BLOCK -- remove after test
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            string sqlSelect = "SELECT user_id " +
                " FROM blocked_users bu ";

            if (gameId > 0)
            {
                sqlSelect += " INNER JOIN developer.games g ON g.owner_id = bu.user_id " +
                    " WHERE blocked_user_id = @userId " +
                    " AND game_id = @gameId";
                
                parameters.Add (new MySqlParameter ("@gameId", gameId));
            }
            else if (communityId > 0)
            {
                sqlSelect += " INNER JOIN communities c ON c.creator_id = bu.user_id " +
                    " WHERE blocked_user_id = @userId " +
                    " AND community_id = @communityId";
                
                parameters.Add (new MySqlParameter ("@communityId", communityId));
            }

            DataRow drBlockedUser = Db.Master.GetDataRow (sqlSelect, parameters);
            return drBlockedUser != null;
        }

        public bool IsUserOnlineInKIM(int userId)
        {
            string sqlSelect = "SELECT user_id " +
                " FROM im_users " +
                " WHERE user_id = @userId " +
                " AND online = 1";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter ("@userId", userId));

            DataRow drUser = Db.Master.GetDataRow(sqlSelect, parameters);
            return (drUser != null);
        }

        /// <summary>
        /// AddCrew
        /// </summary>
        public int AddCrew (Crew crew)
        {
            string sqlString = "INSERT INTO crews (" +
            " user_id, created_datetime, crew_count" +
            ") VALUES (" +
            " @user_id,  NOW(),  @crew_count" +
            ")";

            List < IDbDataParameter > parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@user_id", crew.UserId));
            parameters.Add(new MySqlParameter("@crew_count", crew.CrewCount));
            return Db.Master.ExecuteIdentityInsert (sqlString, parameters);
        }

        /// <summary>
        /// GetCrew
        /// </summary>
        public Crew GetCrew(int userId)
        {
            string sqlString = "SELECT crew_id, user_id, created_datetime, crew_count" +
                " FROM crews " +
                " WHERE user_id = @user_id ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@user_id", userId));
            DataRow row = Db.Slave2.GetDataRow(sqlString, parameters);

            if (row == null)
            {
                return new Crew(0, userId, DateTime.Now, 0);
            }

            return new Crew (Convert.ToInt32(row["crew_id"]) , Convert.ToInt32(row["user_id"]) , 
                Convert.ToDateTime(row["created_datetime"]) , Convert.ToUInt32(row["crew_count"]));            
        }

        /// <summary>
        /// AddCrewPeep
        /// </summary>
        public int AddCrewPeep(CrewPeep crewPeep)
        {
            string sqlString = "INSERT INTO crew_peeps (" +
                " crew_id, user_id, created_datetime " +
                ") VALUES (" +
                " @crew_id,  @user_id, NOW() " +
                ")";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@crew_id", crewPeep.CrewId));
            parameters.Add(new MySqlParameter("@user_id", crewPeep.UserId));
            int result = Db.Master.ExecuteNonQuery(sqlString, parameters);

            // Update crew count
            sqlString = "UPDATE crews " +
                " SET crew_count = crew_count + 1 " +
                " WHERE crew_id = @crew_id ";

            parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@crew_id", crewPeep.CrewId));
            Db.Master.ExecuteNonQuery(sqlString, parameters);

            return result;
        }

        /// <summary>
        /// InsertFriendRequest
        /// </summary>
        public int InsertFriendRequest (int userId, int friendId)
        {
            // Make sure they are not already friends
            Friend friend = GetFriend (userId, friendId);

            // If they are already a friend, don't do anything
            if (friend.UserId > 0)
            {
                // Already friends
                return 0;
            }

            // Make sure they are not already friends
            DataRow drFriendRequest = GetFriendRequest (userId, friendId);

            // If they are already a friend request, don't do anything
            if (drFriendRequest != null)
            {
                // Already friend requested
                return 2;
            }

            string sql = "CALL add_friend_request(@userId, @friendId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@friendId", friendId));

            int result = Db.Master.ExecuteNonQuery (sql, parameters);

            // Remove all cache entries for friend Transactions
            ClearFriendTransactionCache();

            return result;
        }

        /// <summary>
        /// Get a friend for a user
        /// </summary>
        /// <returns></returns>
        public DataRow GetFriendRequest (int userId, int friendId)
        {
            string sqlSelect = "SELECT fr.user_id, fr.friend_id " +
                " FROM friend_requests fr " +
                " WHERE ((fr.user_id = @userId AND fr.friend_id = @friendId)" +	// Friends I invited that confirmed
                " OR (fr.friend_id = @userId AND fr.user_id = @friendId)" +		// Friends invited by my I confirmed
                " )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@friendId", friendId));
            return Db.Slave2.GetDataRow (sqlSelect, parameters);
        }

        public PagedList<FriendDates> GetFriendBirthdays (int userId, string filter, string orderBy, int pageSize, int pageNumber)
        {
            string strQuery = "SELECT u.user_id, u.username, u.birth_date " + 
                " FROM users_dates ud " +
                " INNER JOIN users u ON u.user_id = ud.user_id " +
                " INNER JOIN friends f ON f.friend_id = u.user_id " +
                " WHERE f.user_id = @userId " + 
                " AND ud.birthday BETWEEN " +
                " DATE_FORMAT(NOW(),'%m%d') AND DATE_FORMAT(ADDDATE(NOW(), INTERVAL 5 DAY),'%m%d')";    

            // Filter it?
            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            PagedDataTable pdt = Db.Slave2.GetPagedDataTable (strQuery, orderBy, parameters, pageNumber, pageSize);

            PagedList<FriendDates> list = new PagedList<FriendDates> ();
            list.TotalCount = pdt.TotalCount; 
            
            foreach (DataRow row in pdt.Rows)
            {
                list.Add (new FriendDates (Convert.ToInt32 (row["user_id"]), row["username"].ToString (),
                    Convert.ToDateTime (row["birth_date"]), DateTime.MinValue));
            }

            return list;
        }

        public PagedList<FriendDates> GetFriendAnniversaries (int userId, string filter, string orderBy, int pageSize, int pageNumber)
        {
            string strQuery = "SELECT u.user_id, u.username, u.signup_date " +
                " FROM users_dates ud " +
                " INNER JOIN users u ON u.user_id = ud.user_id " +
                " INNER JOIN friends f ON f.friend_id = u.user_id " +
                " WHERE f.user_id = @userId " +
                " AND ud.anniversary BETWEEN " +
                " DATE_FORMAT(NOW(),'%m%d') AND DATE_FORMAT(ADDDATE(NOW(), INTERVAL 5 DAY),'%m%d')";

            // Filter it?
            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            PagedDataTable pdt = Db.Slave2.GetPagedDataTable (strQuery, orderBy, parameters, pageNumber, pageSize);

            PagedList<FriendDates> list = new PagedList<FriendDates> ();
            foreach (DataRow row in pdt.Rows)
            {
                list.Add (new FriendDates (Convert.ToInt32 (row["user_id"]), row["username"].ToString (),
                    DateTime.MinValue, Convert.ToDateTime (row["signup_date"])));
            }

            return list;
        }

        public PagedList<FriendDates> GetUsersBirthdaysWithFriendsList (string timeSpanOccuringInNextNumDays, string timeSpanOffsetLastLoggedIn, string orderBy, int pageSize, int pageNumber)
        {
            string strQuery = "SELECT u2.user_id, u2.username, u2.display_name, u2.birth_date, u2.gender, " +
                "c.thumbnail_small_path, c.thumbnail_medium_path, c.thumbnail_square_path, " +
                "u.user_id AS friend_user_id, u.username AS friend_username, u.email AS friend_email, " +
                "u.status_id AS friend_status_id, IFNULL(unp.notify_friend_birthdays, 1) AS notify_friend_birthdays " +
                "FROM friends f " +
                "INNER JOIN users u ON u.user_id = f.user_id " +
                "INNER JOIN users u2 ON u2.user_id = f.friend_id " +
                "INNER JOIN communities c ON c.creator_id = u2.user_id " +
                "LEFT JOIN user_notification_preferences unp ON u.user_id = unp.user_id " +
                "WHERE u.status_id = 1 ";
            if (timeSpanOffsetLastLoggedIn != "")
            {                                                                   // ex. -6 MONTH
                strQuery += "AND u.last_login > ADDDATE(NOW(), INTERVAL " + timeSpanOffsetLastLoggedIn + ") ";
            }
            strQuery += "AND c.is_personal = 1 " +
                "AND f.friend_id IN " +
                "( " +
                "    SELECT ud.user_id " +
                "    FROM users_dates ud " +
                "    INNER JOIN users u on u.user_id = ud.user_id " +
                "    WHERE status_id IN (1,2) " +
                "    AND (birthday BETWEEN DATE_FORMAT(NOW(),'%m%d') " +     // ex. 7 DAY
                "        AND DATE_FORMAT(ADDDATE(NOW(), INTERVAL " + timeSpanOccuringInNextNumDays + "),'%m%d')) ";
            if (timeSpanOffsetLastLoggedIn != "")
            {
                strQuery += "    AND last_login > ADDDATE(NOW(), INTERVAL " + timeSpanOffsetLastLoggedIn + ") ";
            }
            strQuery += ") ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            PagedDataTable pdt = Db.Slave2.GetPagedDataTable (strQuery, orderBy, parameters, pageNumber, pageSize);

            PagedList<FriendDates> list = new PagedList<FriendDates> ();
            foreach (DataRow row in pdt.Rows)
            {
                list.Add (new FriendDates (Convert.ToInt32 (row["user_id"]), row["username"].ToString (),
                    row["display_name"].ToString (), Convert.ToDateTime (row["birth_date"]), row["gender"].ToString (),
                    row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_square_path"].ToString(),
                    Convert.ToInt32 (row["friend_user_id"]), row["friend_username"].ToString (),
                    row["friend_email"].ToString (), Convert.ToInt32 (row["friend_status_id"]), Convert.ToInt32 (row["notify_friend_birthdays"]).Equals (1)
                    ));
            }

            return list;
        }

        #endregion

        /// <summary>
        /// add an item to a users inventory
        /// </summary>
        /// <returns></returns>
        public void AddItemToPendingInventory(int userId, string inventory_type, int global_id, int quantity, int inventorySubType, bool suppressInventoryFetch = false, bool propagateQueryExceptions = false)
        {
            string sql = "CALL wok.add_pending_inventory(@userId, @inventory_type, @global_id, @quantity, @inventorySubType, @suppressInventoryFetch)"; 

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@inventory_type", inventory_type));
            parameters.Add(new MySqlParameter("@global_id", global_id));
            parameters.Add(new MySqlParameter("@quantity", quantity));
            parameters.Add(new MySqlParameter("@inventorySubType", inventorySubType));
            parameters.Add(new MySqlParameter("@suppressInventoryFetch", suppressInventoryFetch));
            Db.Master.ExecuteNonQuery(sql, parameters, propagateQueryExceptions);
        }

        /// <summary>
        /// return a game inventories sync record
        /// </summary>
        public DataRow GetGameInventoriesSync(int gameId, int userId, string inventory_type, int global_id, int inventorySubType)
        {
            string sqlSelect = "SELECT  game_id, global_id, inventory_type, inventory_sub_type, kaneva_user_id, quantity, quantity_purchased, created_date, last_update_date " +
                                "FROM    wok.game_inventories_sync " +
                                "WHERE   game_id = @gameId " +
                                "AND     kaneva_user_id = @userId " +
                                "AND     inventory_type = @inventory_type " +
                                "AND     global_id = @global_id " +
                                "AND     inventory_sub_type = @inventorySubType;";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@gameId", gameId));
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@inventory_type", inventory_type));
            parameters.Add(new MySqlParameter("@global_id", global_id));
            parameters.Add(new MySqlParameter("@inventorySubType", inventorySubType));

            return Db.Master.GetDataRow(sqlSelect.ToString(), parameters);
        }

        /// <summary>
        /// add a premium item to a users game inventory
        /// </summary>
        /// <returns></returns>
        public void AddItemToGameInventoriesSync(int gameId, int userId, string inventory_type, int global_id, int quantity, int inventorySubType)
        {
            string sqlSelect = " INSERT INTO wok.game_inventories_sync " +
                               "        ( game_id, global_id, inventory_type, inventory_sub_type, kaneva_user_id, quantity, quantity_purchased, created_date, last_update_date ) " +
                               " VALUES ( @gameId, @global_id, @inventory_type, @inventorySubType, @userId, 0, @quantity, NOW(), NOW()) " +
                               " ON DUPLICATE KEY UPDATE " +
                               " quantity_purchased = quantity_purchased + @quantity, " + 
                               " last_update_date = NOW();";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@gameId", gameId));
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@inventory_type", inventory_type));
            parameters.Add(new MySqlParameter("@global_id", global_id));
            parameters.Add(new MySqlParameter("@quantity", quantity));
            parameters.Add(new MySqlParameter("@inventorySubType", inventorySubType));
            Db.Master.ExecuteNonQuery(sqlSelect, parameters);
        }

        /// <summary>
        /// Inserts info record into the kaneva.reward_log table
        /// </summary>
        public void InsertRewardLog(int userId, double amount, string keiPointId, int transTypeId, int rewardEventId, int wokTransLogId)
        {
            string sql = "INSERT INTO kaneva.reward_log " +
                "(user_id, date_created, amount, kei_point_id, transaction_type, reward_event_id, wok_transaction_log_id, return_code" +
                ") VALUES (" +
                "@userId, DATE(NOW()), @transactionAmount, @keiPointId, @transTypeId, @rewardEventId, @wokTransLogId, 0)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@transactionAmount", amount));
            parameters.Add(new MySqlParameter("@keiPointId", keiPointId));
            parameters.Add(new MySqlParameter("@transTypeId", transTypeId));
            parameters.Add(new MySqlParameter("@rewardEventId", rewardEventId));
            parameters.Add(new MySqlParameter("@wokTransLogId", wokTransLogId));

            Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// InsertPlayerTitle
        /// </summary>
        public int InsertPlayerTitle(int playerId, string title, int sortOrder)
        {
            string sqlSelect = "INSERT IGNORE INTO player_titles " +
                " (player_id, title_id, sort_order) " +
                " SELECT " + playerId.ToString () + ", title_id, 0 FROM titles where name = @title ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@title", title));
            return Db.WOK.ExecuteNonQuery(sqlSelect, parameters);
        }

        /// <summary>
        /// InsertPlayerTitle
        /// </summary>
        public int ChangePlayerNameColor(int userId, int color)
        {
            string sqlString = "CALL change_player_name_color (@userId, @color)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@color", color));

            return Db.WOK.ExecuteNonQuery(sqlString, parameters);
        }
        
        /// <summary>
        /// Update UpdatePlayerTitle
        /// </summary>
        public void UpdatePlayerTitle(int playerId, string title)
        {
            string sqlUpdate = "UPDATE wok.players " +
                " SET title = @title " +
                " WHERE player_id = @playerId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@playerId", playerId));
            parameters.Add(new MySqlParameter("@title", title));
            Db.WOK.ExecuteNonQuery(sqlUpdate, parameters);
        }

        /// <summary>
        /// GetPlayerTitleByFameType
        /// </summary>
        /// <returns></returns>
        public string GetPlayerTitleByFameType (int userId, int fameTypeId)
        {
            string sql = "SELECT t.name " +
                " FROM wok.titles t " +
                " INNER JOIN wok.player_titles pt ON pt.title_id = t.title_id " +
                " INNER JOIN fame.level_awards la ON la.new_title_id = t.title_id " +
                " INNER JOIN fame.levels l ON l.level_id = la.level_id " +
                " INNER JOIN wok.players p ON p.player_id = pt.player_id " +
                " WHERE p.kaneva_user_id = @userId " +
                " AND l.fame_type_id = @fameTypeId " +
                " ORDER BY level_number DESC LIMIT 1;";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userid", userId));
            parameters.Add (new MySqlParameter ("@fameTypeId", fameTypeId));

            DataRow dr = Db.Slave2.GetDataRow (sql, parameters);

            if (dr != null)
            {
                return dr["name"].ToString ();
            }

            return "";
        }
        
        /// <summary>
        /// Update UpdatePlayerTitle
        /// </summary>
        public int InsertUserSourceAcquisitionUrl (int userId, string fullUrl, string hostName, int sourceTypeId)
        {
            hostName = hostName.ToLower ();

            // First check to see if an acquisition source already exists for the same hostname
            // and source type.  If so, we'll use that acquisition_source_id
            string sql = "SELECT acquisition_source_id " +
                " FROM kaneva.acquisition_source " +
                " WHERE acquisition_source_type_id = @sourceTypeId " +
                " AND description = @hostName";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@sourceTypeId", sourceTypeId));
            parameters.Add (new MySqlParameter ("@hostName", hostName));
            DataRow drSource = Db.Master.GetDataRow (sql, parameters);

            int acqSourceId = 0;
            if (drSource != null)
            {
                acqSourceId = Convert.ToInt32 (drSource["acquisition_source_id"]);    
            }

            // If a source record does not already exists that matches what we need
            // create a new one
            if (acqSourceId == 0)
            {
                sql = "INSERT INTO kaneva.acquisition_source " +
                    " (acquisition_source_type_id, description) " +
                    " VALUES (@sourceTypeId, @hostName)";

                parameters.Clear ();
                parameters.Add (new MySqlParameter ("@sourceTypeId", sourceTypeId));
                parameters.Add (new MySqlParameter ("@hostName", hostName));
                acqSourceId = Db.Master.ExecuteIdentityInsert (sql, parameters);
            }

            // Make user we have a valid acquisition_source_id.  If so, then match the
            // user with the correct acquisition source record
            if (acqSourceId > 0)
            {
                sql = "INSERT INTO kaneva.user_acquisition_source " +
                    " (user_id, acquisition_source_id, full_url) " +
                    " VALUES (@userId, @acqSourceId, @fullUrl) ";

                parameters.Clear ();
                parameters.Add (new MySqlParameter ("@userId", userId));
                parameters.Add (new MySqlParameter ("@acqSourceId", acqSourceId));
                parameters.Add (new MySqlParameter ("@fullUrl", fullUrl));
                return Db.Master.ExecuteNonQuery (sql, parameters);
            }

            return 0;
        }

        /// <summary>
        /// GetPlayerData for a STAR, this is a subset of all user's info
        /// </summary>
        public DataSet GetPlayerData(int kanevaUserId, int flags, int gameId)
        {
            string sql;

            sql = "CALL wok.get_player_config_data(@kanevaUserId,@flags,@gameId);";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@kanevaUserId", kanevaUserId));
            parameters.Add(new MySqlParameter("@flags", flags));
            parameters.Add(new MySqlParameter("@gameId", gameId));
            return Db.WOK.GetDataSet(sql, parameters);
        }

        /// <summary>
        /// </summary>
        public DataSet GetPlayListData (int zoneIndex, int zoneInstanceId, int playListId, int playerId)
        {
            string sql = "CALL wok.get_playlist( @zoneIndex, @zoneInstanceId, @playListId, @playerId);";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@zoneIndex", zoneIndex));
            parameters.Add (new MySqlParameter ("@zoneInstanceId", zoneInstanceId));
            parameters.Add (new MySqlParameter ("@playListId", playListId));
            parameters.Add (new MySqlParameter ("@playerId", playerId));
            return Db.WOK.GetDataSet (sql, parameters);
        }

        public int GetPlayerLoginCount (int userId)
        {
            string sqlString = "SELECT login_count " +
                " FROM wok.game_users " +
                " WHERE kaneva_user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            DataRow drLogin = Db.WOK.GetDataRow (sqlString, parameters);

            if (drLogin != null)
            {
                return (int) drLogin["login_count"];
            }

            return 0;
        }

        public void GetPlayerAdmin (int kanevaUserId, out bool isAdmin, out bool isGm, out DateTime secondTolastLogin)
        {
            string key = CentralCache.keyPlayerAdmin( kanevaUserId );
            isAdmin = isGm = false;
            secondTolastLogin = new DateTime();

            DataTable dt = (DataTable)CentralCache.Get( key );
            if ( dt == null )
            {
                string sql = "SELECT is_gm, is_admin, IFNULL(second_to_last_logon,cast( '2010-1-1' as datetime) ) as second_to_last_logon " +
                             " FROM wok.game_users " +
                             " WHERE kaneva_user_id = @kanevaUserId";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@kanevaUserId", kanevaUserId));
                dt = Db.WOK.GetDataTable(sql, parameters);

                CentralCache.Store(key, dt, TimeSpan.FromMinutes( PLAYER_ADMIN_CACHE_LIMIT_MIN ) );
            }
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                isAdmin = dr["is_admin"].ToString() == "T";
                isGm = dr["is_gm"].ToString() == "T";
                secondTolastLogin = Convert.ToDateTime(dr["second_to_last_logon"]);
            }
        }

        public PagedList<UserMessage> GetUserMessages (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            string strQuery = "SELECT com.thumbnail_small_path, com.name_no_spaces, COALESCE(m.channel_id, 0) as channel_id, m.message_id, m.from_id, " +
                " m.to_id, m.subject, m.message, m.message_date, m.replied, m.type, m.to_viewable, " +
                " m.from_viewable, u.username, m.channel_id, NOW() as CurrentDate, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture ";

            strQuery += " FROM messages m, users u " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id, communities_personal com ";

            strQuery += " WHERE m.from_id = u.user_id " +
                " AND m.to_id = @userId " +
                " AND m.to_viewable != 'D' " +
                " AND u.user_id = com.creator_id ";

            // Filter it?
            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            PagedDataTable dt = Db.Slave2.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<UserMessage> list = new PagedList<UserMessage> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                UserMessage um = new UserMessage (int.Parse (row["message_id"].ToString ()), int.Parse (row["from_id"].ToString ()),
                    int.Parse (row["to_id"].ToString ()), row["subject"].ToString (), row["message"].ToString (), Convert.ToDateTime (row["message_date"]),
                    int.Parse (row["replied"].ToString ()), int.Parse (row["type"].ToString ()), int.Parse (row["channel_id"].ToString ()),
                    row["to_viewable"].ToString (), row["from_viewable"].ToString (), row["thumbnail_small_path"].ToString (),
                    row["name_no_spaces"].ToString (), row["username"].ToString (), Convert.ToDateTime (row["CurrentDate"]));

                um.OwnerFacebookSettings.FacebookUserId = Convert.ToUInt64 (row["fb_user_id"]);
                um.OwnerFacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (row["use_facebook_profile_picture"]);

                list.Add (um);
            }
            return list;
        }

        public PagedList<UserMessage> GetSentMessages (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            string strQuery = "SELECT com.thumbnail_small_path, com.name_no_spaces, COALESCE(m.channel_id, 0) as channel_id, m.message_id, m.from_id, m.to_id, m.subject, " +
                " m.message, m.message_date, m.replied, m.type, m.to_viewable, m.from_viewable, u.username, NOW() as CurrentDate, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture ";

            strQuery += " FROM messages m, users u " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id, communities_personal com ";

            strQuery += " WHERE m.to_id = u.user_id " +
                " AND m.from_id = @userId " +
                " AND m.from_viewable = 'S' " +
                " AND u.user_id = com.creator_id ";


            // Filter it?
            if (filter.Length > 0)
            {
                strQuery += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            PagedDataTable dt = Db.Slave2.GetPagedDataTable (strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<UserMessage> list = new PagedList<UserMessage> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                UserMessage um = new UserMessage (int.Parse (row["message_id"].ToString ()), int.Parse (row["from_id"].ToString ()),
                    int.Parse (row["to_id"].ToString ()), row["subject"].ToString (), row["message"].ToString (), Convert.ToDateTime (row["message_date"]),
                    int.Parse (row["replied"].ToString ()), int.Parse (row["type"].ToString ()), int.Parse (row["channel_id"].ToString ()),
                    row["to_viewable"].ToString (), row["from_viewable"].ToString (), row["thumbnail_small_path"].ToString (),
                    row["name_no_spaces"].ToString (), row["username"].ToString (), Convert.ToDateTime (row["CurrentDate"]));

                um.OwnerFacebookSettings.FacebookUserId = Convert.ToUInt64 (row["fb_user_id"]);
                um.OwnerFacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (row["use_facebook_profile_picture"]);

                list.Add (um);
            }
            return list;
        }

        public UserMessage GetUserMessage (int userId, int messageId)
        {
            string sqlString = "SELECT com.thumbnail_small_path, com.name_no_spaces, COALESCE(m.channel_id, 0) as channel_id, m.message_id, m.from_id, m.to_id, m.subject, " +
                " m.message, m.message_date, m.replied, m.type, m.to_viewable, m.from_viewable, u.username, NOW() as CurrentDate, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture ";

            sqlString += "FROM messages m, users u " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id, communities_personal com ";

            sqlString += "WHERE m.from_id = u.user_id " +
                " AND (" +
                        " (" +
                            " m.to_id = @userId " +
                            " AND m.to_viewable != 'D'" +
                        ")" +
                    " OR (" +
                            " m.from_id = @userId " +
                            " AND m.from_viewable != 'D'" +
                        ")" +
                    ")" +

                " AND m.message_id = @messageId" +
                " AND u.user_id = com.creator_id ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@messageId", messageId));

            DataRow row = Db.Slave2.GetDataRow(sqlString, parameters);

            if (row == null)
            {
                return new UserMessage();
            }

            UserMessage um = new UserMessage (int.Parse (row["message_id"].ToString ()), int.Parse (row["from_id"].ToString ()),
                    int.Parse (row["to_id"].ToString ()), row["subject"].ToString (), row["message"].ToString (), Convert.ToDateTime (row["message_date"]),
                    int.Parse (row["replied"].ToString ()), int.Parse (row["type"].ToString ()), int.Parse (row["channel_id"].ToString ()),
                    row["to_viewable"].ToString (), row["from_viewable"].ToString (), row["thumbnail_small_path"].ToString (),
                    row["name_no_spaces"].ToString (), row["username"].ToString (), Convert.ToDateTime (row["CurrentDate"]));
            
            um.OwnerFacebookSettings.FacebookUserId = Convert.ToUInt64 (row["fb_user_id"]);
            um.OwnerFacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (row["use_facebook_profile_picture"]);
            
            return um;
        }

        // **********************************************************************************************
        // Misc
        // **********************************************************************************************
        #region Misc Functions

        /// <summary>
        /// See if the email already exists in the database
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool EmailExists(string email)
        {
            // Make sure the email does not already exist?
            string sqlString = "SELECT COUNT(*) FROM users WHERE email = @email";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@email", email));
            return (Convert.ToInt32(Db.Master.GetScalar(sqlString, parameters)) > 0);
        }

        /// <summary>
        /// See if the username already exists in the database
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public bool UsernameExists(string username)
        {
            // Make sure the username does not already exist?
            string sqlString = "SELECT COUNT(*) FROM users WHERE username = @username";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@username", username));
            return (Convert.ToInt32(Db.Master.GetScalar(sqlString, parameters)) > 0);
        }

        /// <summary>
        /// GetStates
        /// </summary>
        /// <returns></returns>
        public IList<State> GetStates()
        {
            string strQuery = "SELECT state_code, state " +
                " FROM states " +
                " ORDER BY state ";

            DataTable dt = Db.Slave2.GetDataTable(strQuery);

            IList<State> list = new List<State>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new State(row["state_code"].ToString(), row["state"].ToString()));
            }
            return list;
        }

        /// <summary>
        /// GetCountries
        /// </summary>
        /// <returns></returns>
        public IList<Country> GetCountries()
        {
            string strQuery = "SELECT country_id, country " +
                " FROM countries " +
                " ORDER BY country";

            DataTable dt = Db.Slave2.GetDataTable(strQuery);

            IList<Country> list = new List<Country>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Country(row["country_id"].ToString(), row["country"].ToString()));
            }
            return list;
        }

        /// <summary>
        /// LogSearch
        /// </summary>
        public int LogSearch(int userId, string searchString, int searchType)
        {
            string sql = "INSERT INTO user_searches " +
                "(user_id, search_string, created_datetime, search_type " +
                ") VALUES (" +
                "@userId, @searchString, NOW(), @searchType)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@searchString", searchString));
            parameters.Add(new MySqlParameter("@searchType", searchType));
            return Db.Audit.ExecuteIdentityInsert (sql, parameters);
        }

        public int SearchConvert (int logId)
        {
            string sqlUpdate = "UPDATE user_searches " +
                " SET " +
                " converted = 1 " +
                " WHERE log_id = " + logId;

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            return Db.Audit.ExecuteNonQuery(sqlUpdate, parameters);
        }

        /// <summary>
        /// GetLoginAttempts
        /// </summary>
        public PagedList<UserLoginIssue> GetLoginAttempts(DateTime dtStartDate, DateTime dtEndDate, string orderBy, int pageNumber, int pageSize)
        {
            string sql = "SELECT COALESCE(u.username,'Anonymous') as username, COALESCE(u.user_id,0) as user_id, " +
                " uli.ip_address, uli.description, uli.created_datetime " +
                " FROM user_login_issues uli LEFT OUTER JOIN kaneva.users u ON u.user_id = uli.user_id " +
                " WHERE uli.created_datetime >= @startDate " +
                " AND uli.created_datetime <= @endDate";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@startDate", dtStartDate));
            parameters.Add(new MySqlParameter("@endDate", dtEndDate));

            PagedDataTable dt = Db.Audit.GetPagedDataTable(sql, orderBy, parameters, pageNumber, pageSize);

            PagedList<UserLoginIssue> list = new PagedList<UserLoginIssue>();
            list.TotalCount = dt.TotalCount; 
            
            foreach (DataRow row in dt.Rows)
            {
                int id = int.Parse(row["user_id"].ToString());

                list.Add(new UserLoginIssue(row ["username"].ToString (), Convert.ToInt32 (row["user_id"]),
                    row ["ip_address"].ToString (), row ["description"].ToString (), Convert.ToDateTime (row ["created_datetime"])
                    )
                );
            }
            return list;
        }

        /// <summary>
        /// InsertUserLoginIssue
        /// </summary>
        public int InsertUserLoginIssue(int userId, string ipAddress, string description, string errorType, string serverType)
        {
            string sUserId = (userId.Equals(0)) ? "NULL" : userId.ToString();

            string sqlSelect = "INSERT INTO user_login_issues " +
                " (user_id, ip_address, description, created_datetime, error_type, server_type) " +
                " VALUES (" + sUserId + ", @ipAddress, @description,NOW(),@errorType, @serverType)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@ipAddress", ipAddress));
            parameters.Add(new MySqlParameter("@description", description));
            parameters.Add(new MySqlParameter("@errorType", errorType));
            parameters.Add(new MySqlParameter("@serverType", serverType));
            return Db.Audit.ExecuteIdentityInsert(sqlSelect, parameters);
        }

        /// <summary>
        /// InsertKIMLoginLog
        /// </summary>
        public int InsertKIMLoginLog(int userId, string servername, string ipAddress)
        {
            string sqlSelect = "INSERT INTO kim_login_log " +
                " (user_id, server_name, ip_address, created_date ) " +
                " VALUES (@userId, @server_name, @ipAddress, NOW())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@server_name", servername));
            parameters.Add(new MySqlParameter("@ipAddress", ipAddress));
            return Db.Developer.ExecuteIdentityInsert(sqlSelect, parameters);
        }

        /// <summary>
        /// InsertUserLoginIssue
        /// </summary>
        public PagedDataTable GetUserLoginHistory (int userId, string orderby, int pageNumber, int pageSize)
        {
            string sqlSelect = "SELECT logout_reason_code AS action_type, kaneva_user_id, ll.created_date, ll.logout_date, " +
            "TIMESTAMPDIFF(MINUTE,ll.created_date,logout_date) AS time_played_in_min " +
            "FROM developer.login_log ll INNER JOIN wok.game_users gu ON ll.user_id = gu.kaneva_user_id " +
            "WHERE ll.user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            PagedDataTable pdtResults = Db.Stats.GetPagedDataTable(sqlSelect, orderby, parameters, pageNumber, pageSize);
            if(pdtResults == null)
            {
                pdtResults = new PagedDataTable();
            }

            return pdtResults;
        }

        public DataRow GetUserLastLoginByGame (int userId, int gameId)
        {
            string sqlSelect = "SELECT login_id, user_id, game_id, created_date, " +
                " logout_date, logout_reason_code " +
                " FROM developer.login_log " +
                " WHERE user_id = @userId " + 
                " AND game_id = @gameId " +
                " AND logout_reason_code = 'NORMAL' " +
                " ORDER BY login_id DESC LIMIT 1";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@gameId", gameId));

            return Db.Master.GetDataRow (sqlSelect.ToString (), parameters);
        }

        public DateTime GetUserFirstWokLogin(int userId)
        {
            string sqlSelect = "SELECT created_date " +
                " FROM wok.game_users " +
                " WHERE kaneva_user_id = @userId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            DataRow row = Db.Master.GetDataRow(sqlSelect, parameters);

            if (row == null)
            {
                return DateTime.MaxValue;
            }
            else
            {
                return Convert.ToDateTime(row["created_date"]);
            }
        }

        public Dictionary<int, bool> AreUsersOnlineInWoK(int[] userIds)
        {
            Dictionary<int, bool> statuses = new Dictionary<int, bool>();

            string sqlSelect = "SELECT u.user_id, IF(al.server_id > 0,'T','F') as online " +
                               "FROM kaneva.users u " +
                               "LEFT OUTER JOIN developer.active_logins al ON u.user_id = al.user_id " +
                               "LEFT OUTER JOIN kaneva.im_users imu ON u.user_id = imu.user_id " +
                               "WHERE u.user_id IN (" + string.Join(",", userIds) + ")";

            DataTable dt = Db.Master.GetDataTable(sqlSelect);

            if (dt == null || dt.Rows.Count == 0)
                return statuses;

            bool bOut;
            foreach (DataRow row in dt.Rows)
            {
                if (!statuses.TryGetValue(Convert.ToInt32(row["user_id"]), out bOut))
                {
                    statuses.Add(Convert.ToInt32(row["user_id"]), (row["online"].ToString() == "T"));
                }
            }

            return statuses;
        }

        #endregion

        // **********************************************************************************************
        // Update User Functions
        // **********************************************************************************************
        #region UpdateUser Functions

        public IList<KanevaColor> GetAvailableNameColors()
        {
            string strQuery = "SELECT color_type, color_name, color_description, color_r, color_g, color_b " +
                " FROM colors";

            DataTable dt = Db.WOK.GetDataTable(strQuery);

            IList<KanevaColor> list = new List<KanevaColor>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new KanevaColor(Convert.ToUInt32(row["color_type"]), row["color_name"].ToString(), row["color_description"].ToString(),
                    Convert.ToUInt16(row["color_r"]), Convert.ToUInt16(row["color_g"]), Convert.ToUInt16(row["color_b"])));
            }
            return list;
        }

        public int GetUsersNameColor(int wokPlayerId)
        {
            string strQuery = "SELECT color_type FROM player_presences where player_id = @wokPlayerId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@wokPlayerId", wokPlayerId));

            return Convert.ToInt32(Db.WOK.GetScalar(strQuery, parameters));
        }

        public DataRow GetUsersNameColorRGB(int userId)
        {
            string strQuery =  "SELECT c.color_type, c.color_r, c.color_g, c.color_b " +
                               "FROM wok.players p " +
                               "INNER JOIN wok.player_presences pp ON p.player_id = pp.player_id " +
                               "INNER JOIN wok.colors c ON pp.color_type = c.color_type " +
                               "WHERE p.kaneva_user_id = @userId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            return Db.WOK.GetDataRow(strQuery, parameters);
        }

        /// <summary>
        /// updates a blocked user's status 
        /// </summary>
        public int UpdateUsersNameColor(int wokPlayerId, int nameColorId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sql = "UPDATE player_presences " +
                " SET color_type = @nameColorId " +
                " WHERE player_id = @wokPlayerId ";

            parameters.Add(new MySqlParameter("@nameColorId", nameColorId));
            parameters.Add(new MySqlParameter("@wokPlayerId", wokPlayerId));

            return Db.WOK.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Deletes user from blocked users table
        /// </summary>
        public int DeleteBlockedUser(int userId, int blockedUserId)
        {   //**BLOCK -- remove after test
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            // No other blocking is set so remove the record
            string sql = "DELETE FROM blocked_users " +
                "WHERE user_id = @userId " +
                " AND blocked_user_id = @blockedUserId ";

            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add (new MySqlParameter ("@blockedUserId", blockedUserId));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// updates a blocked user's status 
        /// </summary>
        public int InsertUserToBlock (int userId, int blockedUserId, int blockSourceId)
        {   //**BLOCK -- remove after test
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sql = "INSERT INTO blocked_users " +
                "(user_id, blocked_user_id, created_date, blocked_source_id" +
                ") VALUES (" +
                "@userId, @blockedUserId, NOW(), @blockSourceId)"; 

            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add (new MySqlParameter ("@blockedUserId", blockedUserId));
            parameters.Add (new MySqlParameter ("@blockSourceId", blockSourceId));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public PagedDataTable GetBlockedUsers (int userId, string orderBy, int pageNumber, int pageSize)
        {
            string strQuery = "SELECT bu.user_id AS block_owner, blocked_user_id, blocked_user_id AS proflle_id, " +
                " created_date AS blocked_date, u.username AS username " +
                " FROM blocked_users bu, users u " +
                " WHERE bu.user_id = @userId " +
                " AND blocked_user_id=u.user_id ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            return Db.Master.GetPagedDataTable (strQuery, orderBy, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// Update UpdateUserEmailSecurity
        /// </summary>
        public void UpdateUserEmailSecurity(int userId, string showEmail)
        {
            string sqlUpdate = "UPDATE users " +
                " SET " +
                " show_email = @showEmail " +
                " WHERE user_id = " + userId;

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@showEmail", showEmail));
            Db.Master.ExecuteNonQuery(sqlUpdate, parameters);

            // Invalidate user from cache
            InvalidateKanevaUserCache(userId);
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user_id"></param>
        public void UpdateUser(int userId, int showMature, int browseAnon, int showOnline, string newsletter,
            int notifyBlogComments, int notifyProfileComments, int notifyFriendMessages, int notifyAnyoneMessages,
            int notifyFriendRequests, int notifyNewFriends, int friendsCanComment, int everyoneCanComment, int matureProfile, int receiveUpdates)
        {
            string sqlUpdate = "UPDATE users " +
                " SET browse_anonymously = @browseAnon, " +
                " show_mature = @showMature, " +
                " show_online = @showOnline, " +
                " newsletter = @newsletter, " +
                " notify_blog_comments = @notifyBlogComments, " +
                " notify_profile_comments = @notifyProfileComments, " +
                " notify_friend_messages = @notifyFriendMessages, " +
                " notify_anyone_messages = @notifyAnyoneMessages, " +
                " notify_friend_requests = @notifyFriendRequests, " +
                " notify_new_friends = @notifyNewFriends, " +
                " friends_can_comment = @friendsCanComment, " +
                " everyone_can_comment = @everyoneCanComment, " +
                " mature_profile = @matureProfile, " +
                //" receive_updates = @receiveUpdates" +
                // TEMPORARY HACK
                // using master_user_id for receive_updates until schema changes can be made.
                " master_user_id =  @receiveUpdates" +
                " WHERE user_id = " + userId;

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@browseAnon", browseAnon));
            parameters.Add(new MySqlParameter("@showMature", showMature));
            parameters.Add(new MySqlParameter("@showOnline", showOnline));
            parameters.Add(new MySqlParameter("@newsletter", newsletter));
            parameters.Add(new MySqlParameter("@notifyBlogComments", notifyBlogComments));
            parameters.Add(new MySqlParameter("@notifyProfileComments", notifyProfileComments));
            parameters.Add(new MySqlParameter("@notifyFriendMessages", notifyFriendMessages));
            parameters.Add(new MySqlParameter("@notifyAnyoneMessages", notifyAnyoneMessages));
            parameters.Add(new MySqlParameter("@notifyFriendRequests", notifyFriendRequests));
            parameters.Add(new MySqlParameter("@notifyNewFriends", notifyNewFriends));
            parameters.Add(new MySqlParameter("@friendsCanComment", friendsCanComment));
            parameters.Add(new MySqlParameter("@everyoneCanComment", everyoneCanComment));
            parameters.Add(new MySqlParameter("@matureProfile", matureProfile));
            parameters.Add(new MySqlParameter("@receiveUpdates", receiveUpdates));

            Db.Master.ExecuteNonQuery(sqlUpdate, parameters);

            // Invalidate user from cache
            InvalidateKanevaUserCache(userId);
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user_id"></param>
        public void UpdateUser(int userId, int showMature)
        {
            string sqlUpdate = "UPDATE users " +
                " SET show_mature = @showMature " +
                " WHERE user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@showMature", showMature));
            Db.Master.ExecuteNonQuery(sqlUpdate, parameters);

            // Invalidate user from cache
            InvalidateKanevaUserCache(userId);
        }


        /// <summary>
        /// Update user unsubscribe
        /// </summary>       
        public void UpdateUserNoCommunication(int userId, string keyvalue)
        {
            string sqlUpdate = "UPDATE users " +
                " SET  newsletter = @newsletter," +
                " notify_blog_comments = @notifyBlogComments," +
                " notify_profile_comments = @notifyProfileComments," +
                " notify_friend_messages = @notifyFriendMessages," +
                " notify_anyone_messages = @notifyAnyoneMessages," +
                " notify_friend_requests = @notifyFriendRequests, " +
                // Temporary Hack!
                // master_user_id is temporarily holding receive_updates
                " master_user_id = @receiveUpdates " +
                " WHERE user_id = " + userId + " and key_value = @keyvalue";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@newsletter", "N"));
            parameters.Add(new MySqlParameter("@notifyBlogComments", 0));
            parameters.Add(new MySqlParameter("@notifyProfileComments", 0));
            parameters.Add(new MySqlParameter("@notifyFriendMessages", 0));
            parameters.Add(new MySqlParameter("@notifyAnyoneMessages", 0));
            parameters.Add(new MySqlParameter("@notifyFriendRequests", 0));
            parameters.Add(new MySqlParameter("@receiveUpdates", 0));
            parameters.Add(new MySqlParameter("@keyvalue", keyvalue));
            Db.Master.ExecuteNonQuery(sqlUpdate, parameters);

            // Invalidate user from cache
            InvalidateKanevaUserCache(userId);
        }

        /// <summary>
        /// Update blast preferences
        /// </summary>
        /// <param name="user_id"></param>
        public void UpdateBlastPreferences(int userId, int blastPreferences, int blastShowPreferences)
        {
            string sqlUpdateLogin = "UPDATE users " +
                " SET blast_privacy_permissions = @blastPreferences, " +
                " blast_show_permissions = @blastShowPreferences " +
                " WHERE user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@blastPreferences", blastPreferences));
            parameters.Add(new MySqlParameter("@blastShowPreferences", blastShowPreferences));
            Db.Master.ExecuteNonQuery(sqlUpdateLogin, parameters);

            // Invalidate user from cache
            InvalidateKanevaUserCache(userId);
        }

		public int AddBlastBlock(int userId, int blockedUserId, int blockedCommunityId)
		{
			string sqlAddBlastBlock = "INSERT IGNORE INTO user_preferences_blast_blocks (user_id, blocked_user_id, blocked_community_id, created_date) VALUES " +
				" (@userId, @blockedUserId, @blockedCommunityId, @createdDate)";

			List<IDbDataParameter> parameters = new List<IDbDataParameter>();
			parameters.Add(new MySqlParameter("@userId", userId));
			parameters.Add(new MySqlParameter("@blockedUserId", blockedUserId));
			parameters.Add(new MySqlParameter("@blockedCommunityId", blockedCommunityId));
			parameters.Add(new MySqlParameter("@createdDate", DateTime.Now));
			int ret = Db.Master.ExecuteNonQuery(sqlAddBlastBlock, parameters);

			// Invalidate user from cache
            InvalidateKanevaUserCache(userId);

            return ret;
		}

		public int DeleteBlastBlock(int userId, int blockedUserId, int blockedCommunityId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

			string sql = "DELETE FROM user_preferences_blast_blocks " +
                " WHERE user_id = @userId " +
				" AND blocked_user_id = @blockedUserId " +
				" AND blocked_community_id = @blockedCommunityId ";

            parameters.Add(new MySqlParameter("@userId", userId));
			parameters.Add(new MySqlParameter("@blockedUserId", blockedUserId));
			parameters.Add(new MySqlParameter("@blockedCommunityId", blockedCommunityId));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }


		/// <summary>
		/// Get a users' blast blocks
		/// </summary>
		public PagedList<BlastBlock> GetBlastBlocks(int userId, string filter, string orderby, int pageNumber, int pageSize)
		{
			List<IDbDataParameter> parameters = new List<IDbDataParameter>();

			string strQuery = "SELECT upbb.user_id, blocked_user_id, u.username, blocked_community_id, `name` as blocked_community_name,  blocked_user_id " +				
				" FROM kaneva.user_preferences_blast_blocks upbb " +
				" LEFT JOIN kaneva.users u ON upbb.blocked_user_id = u.user_id " +
				" LEFT JOIN kaneva.communities c ON upbb.blocked_community_id = community_id WHERE upbb.user_id = @userId ";

			parameters.Add(new MySqlParameter("@userId", userId));

			if ((filter != null) && (filter != ""))
			{
				strQuery += " AND " + filter;
			}

			if ((orderby != null) && (orderby != ""))
			{
				strQuery += " ORDER BY " + orderby;
			}

			PagedDataTable dt = Db.Slave2.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);
			if (dt == null)
			{
				dt = new PagedDataTable();
			}


			PagedList<BlastBlock> list = new PagedList<BlastBlock>();
			list.TotalCount = dt.TotalCount;

			foreach (DataRow row in dt.Rows)
			{				
				list.Add(new BlastBlock(userId, Convert.ToInt32(row["blocked_user_id"]), row["username"].ToString(),
					Convert.ToInt32(row["blocked_community_id"]), row["blocked_community_name"].ToString()));									
			}
			return list;
		}

		public bool IsBlastBlocked(int userId, int blockedUserId, int blockedCommunityId)
		{
			List<IDbDataParameter> parameters = new List<IDbDataParameter>();

			string sqlSelect = "SELECT user_id FROM  user_preferences_blast_blocks " +
				" WHERE user_id = @userId " +
				" AND blocked_user_id = @blockedUserId OR blocked_community_id = @blockedCommunityId ";

			parameters.Add(new MySqlParameter("@userId", userId));
			parameters.Add(new MySqlParameter("@blockedUserId", blockedUserId));
			parameters.Add(new MySqlParameter("@blockedCommunityId", blockedCommunityId));

			DataRow drBlockedBlast = Db.Master.GetDataRow(sqlSelect, parameters);
			return (drBlockedBlast != null ); 			
		}


        /// <summary>
        /// Update user identity
        /// </summary>
        /// <param name="user_id"></param>
        public void UpdateUserIdentityCSR(int userId, string firstName, string lastName, string description, string gender,
            string homepage, string email, bool bShowEmail, bool bShowMature, string country, string zipCode, DateTime birthDate, string displayName)
        {
            int iShowEmail = bShowEmail ? 1 : 0;
            int iShowMature = bShowMature ? 1 : 0;

            string sqlUpdate = "CALL update_user_identity_csr(" +
                " @userId," +
                " @firstName," +
                " @lastName," +
                " @description," +
                " @gender," +
                " @homepage," +
                " @email," +
                " @iShowEmail," +
                " @iShowMature," +
                " @birthDate," +
                " @country," +
                " @zipCode, " +
                " @displayName); ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@firstName", firstName));
            parameters.Add(new MySqlParameter("@lastName", lastName));
            parameters.Add(new MySqlParameter("@description", description));
            parameters.Add(new MySqlParameter("@gender", gender));
            parameters.Add(new MySqlParameter("@homepage", homepage));
            parameters.Add(new MySqlParameter("@email", email));
            parameters.Add(new MySqlParameter("@iShowEmail", iShowEmail));
            parameters.Add(new MySqlParameter("@iShowMature", iShowMature));
            parameters.Add(new MySqlParameter("@birthDate", birthDate));
            parameters.Add(new MySqlParameter("@country", country));
            parameters.Add(new MySqlParameter("@zipCode", zipCode));
            parameters.Add(new MySqlParameter("@displayName", displayName));
            Db.Master.ExecuteNonQuery(sqlUpdate, parameters);

            // Invalidate user from cache
            InvalidateKanevaUserCache(userId);
        }

        /// <summary>
        /// Update a user status
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userStatus"></param>
        public void UpdateUserStatus(int userId, int userStatus, bool propagateTransExceptions = false)
        {
            using (TransactionDecorator transaction = new TransactionDecorator())
            {
                string sqlString = "CALL update_user_status_id (@userId, @userStatus); ";
                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@userStatus", userStatus));
                parameters.Add(new MySqlParameter("@userId", userId));
                Db.Master.ExecuteNonQuery(sqlString, parameters, propagateTransExceptions);

                transaction.Complete();
            }

            // Invalidate user from cache
            InvalidateKanevaUserCache(userId);
        }

        /// <summary>
        /// Update a users password
        /// </summary>
        public int UpdatePassword(int userId, string password, string salt)
        {
            string sqlString = "UPDATE users " +
                " SET password = @password, " +
                " salt = @salt " +
                " WHERE user_id = @userId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@password", password));
            parameters.Add(new MySqlParameter("@salt", salt));
            int result = Db.Master.ExecuteNonQuery(sqlString, parameters);

            // Invalidate user from cache
            InvalidateKanevaUserCache(userId);
            return result;
        }


        /// <summary>
        /// Update last login and number of logins
        /// </summary>
        /// <param name="user_id"></param>
        public int UpdateLastLogin(int userId, string ipAddress, string serverName)
        {
            string sqlUpdateLogin = "UPDATE users " +
                " SET second_to_last_login = last_login " +
                " ,last_login = NOW() " +
                " ,last_ip_address = INET_ATON(@ipAddress) " +
                " ,online = @online " +
                " ,ustate = @uState " +
                " WHERE user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@ipAddress", ipAddress));
            parameters.Add(new MySqlParameter("@online", 1));
            parameters.Add(new MySqlParameter("@uState", User.ONLINE_USTATE_ON));
            Db.Master.ExecuteNonQuery(sqlUpdateLogin, parameters);

            // Update number of logins
            sqlUpdateLogin = "UPDATE users_stats " +
                " SET number_of_logins = number_of_logins + 1 " +
                " WHERE user_id = @userId ";

            parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            Db.Master.ExecuteNonQuery(sqlUpdateLogin, parameters);

            // New login log
            int loginId = 0;

            string sqlInsertLoginLog = "INSERT INTO `kaneva`.`login_log`" +
                " (`user_id`,`created_date`,`server_name`,`ip_address`) " +
                " VALUES " +
                " (@userId, NOW(), @serverName, INET_ATON(@ipAddress))";

            parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@ipAddress", ipAddress));
            parameters.Add(new MySqlParameter("@serverName", serverName));
            loginId = Db.Master.ExecuteIdentityInsert(sqlInsertLoginLog, parameters);

            // Invalidate user from cache
            InvalidateKanevaUserCache(userId);

            return loginId;
        }

        /// <summary>
        /// Update online status
        /// </summary>
        /// <param name="user_id"></param>
        public void UpdateOnlineStatus(int userId, int online, string uState, int loginId)
        {
            string sqlUsersOnline = "UPDATE users " +
                " SET online = @online, " +
                " ustate = @uState " +
                " WHERE user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@online", online));
            parameters.Add(new MySqlParameter("@uState", uState));
            Db.Master.ExecuteNonQuery(sqlUsersOnline, parameters);

            if (loginId > 0 && uState == User.ONLINE_USTATE_OFF)
            {
                string sqlUsersLogout = "UPDATE `kaneva`.`login_log` " +
                    " SET " +
                    " `logout_date` = NOW()," +
                    " `logout_reason_code` = 'NORMAL' " +
                    " WHERE login_id = @loginId " ;

                parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@loginId", loginId));
                Db.Master.ExecuteNonQuery(sqlUsersLogout, parameters);
            }

            // Invalidate user from cache
            InvalidateKanevaUserCache(userId);
        }

        /// <summary>
        /// Update last ip of the user
        /// </summary>
        /// <param name="user_id"></param>
        public void UpdateLastIP(int userId, string ipAddress)
        {
            string sqlUpdateLogin = "UPDATE users " +
                " SET last_ip_address = INET_ATON(@ipAddress) " +
                " WHERE user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@ipAddress", ipAddress));
            Db.Master.ExecuteNonQuery(sqlUpdateLogin, parameters);
        }

        /// <summary>
        /// Update number of failed logins
        /// </summary>
        /// <param name="user_id"></param>
        public void UpdateFailedLogins(int userId)
        {
            string sqlUpdateLogin = "UPDATE users_stats " +
                " SET number_of_failed_logins = number_of_failed_logins + 1 " +
                " WHERE user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            Db.Master.ExecuteNonQuery(sqlUpdateLogin, parameters);
        }

        /// <summary>
        /// Update users restriction setting
        /// </summary>
        /// <param name="user_id"></param>
        public void UpdateUserRestriction(int userId, int matureProfile)
        {
            string sqlUpdate = "UPDATE users " +
                " SET mature_profile = @matureProfile" +
                " WHERE user_id = " + userId;

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@matureProfile", matureProfile));
            Db.Master.ExecuteNonQuery(sqlUpdate, parameters);

            // Invalidate user from cache
            InvalidateKanevaUserCache(userId);
        }

        /// <summary>
        /// Update a user's email status
        /// </summary>
        public void UpdateUserEmailStatus(int userId, int emailStatus)
        {
            string sqlString = "UPDATE users " +
                " SET " +
                " email_status = @emailStatus " +
                " WHERE user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@emailStatus", emailStatus));
            parameters.Add(new MySqlParameter("@userId", userId));
            Db.Master.ExecuteNonQuery(sqlString, parameters);

            // Invalidate user from cache
            InvalidateKanevaUserCache(userId);
        }

        /// <summary>
        /// Update user
        /// </summary>
        public void UpdateUser(int userId, string description)
        {
            string sqlUpdate = "UPDATE users " +
                " SET " +
                " description = @description" +
                " WHERE user_id = " + userId;

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@description", description));
            Db.Master.ExecuteNonQuery(sqlUpdate, parameters);

            // Invalidate user from cache
            InvalidateKanevaUserCache(userId);
        }

        #endregion

        #region User Profile/Personal Info

        /// <summary>
        /// Get personal stats for a user
        /// </summary>
        /// <returns></returns>
        public DataRow GetUserPersonalInfo(int userId)
        {
            string sqlSelect = "SELECT up.relationship, up.orientation, up.religion, up.ethnicity, " +
                " up.children, up.education, up.income, up.height_feet, up.height_inches, up.smoke, up.drink, up.hometown " +
                " FROM user_profiles up " +
                " INNER JOIN users u ON u.user_id = up.user_id " +
                " WHERE up.user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            return Db.Master.GetDataRow(sqlSelect.ToString(), parameters);
        }

        /// <summary>
        /// Get personal infor data for a user - none user object
        /// </summary>
        /// <returns></returns>
        public DataRow GetUserProfile(int userId)
        {
            string sqlSelect = "SELECT up.title_blast, u.description, up.hobbies, up.clubs, up.favorite_movies,up.favorite_tv, " +
                " up.favorite_music, up.favorite_sports, up.favorite_games, up.favorite_books, up.favorite_artists,	up.favorite_cars, " +
                " up.favorite_gadgets,   up.favorite_fashion, up.favorite_brands, up.role_models, up.like_to_meet, up.favorite_quotes, " +
                " up.show_favorites, up.show_prof, up.show_org, up.interests, up.prof_title, " +
                " up.prof_position, up.prof_industry, up.prof_company, up.prof_job, up.prof_skills, up.org_current_assoc, up.org_past_assoc, " +
                " up.relationship, up.orientation, up.religion, up.ethnicity, up.children, up.education, up.income, up.height_feet, up.height_inches, up.smoke, up.drink, " +
                " up.show_store, up.show_blog, up.show_games, up.show_bookmarks, up.hometown " +
                " FROM user_profiles up " +
                " INNER JOIN users u ON u.user_id = up.user_id " +
                " WHERE up.user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            return Db.Master.GetDataRow(sqlSelect.ToString(), parameters);
        }

        /// <summary>
        /// See if the username already exists in the database
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public bool DoesUserProfileExist(int userId)
        {
            // Make sure the username does not already exist?
            string sqlSelect = "SELECT COUNT(*) FROM user_profiles WHERE user_id = @userId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            return (Convert.ToInt32(Db.Master.GetScalar(sqlSelect, parameters)) > 0);
        }

        /// <summary>
        /// Adds a new user profile entry
        /// </summary>
        public int AddNewUserProfile(int userId)
        {
            string sqlSelect = "INSERT INTO user_profiles (user_id) VALUES (@userId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            return Db.Audit.ExecuteIdentityInsert(sqlSelect, parameters);
        }


        /// <summary>
        /// UpdateUserProfile
        /// </summary>
        /// <param name="user_id"></param>
        public int UpdateUserProfile(int userId,string relationship, string orientation, string religion, string ethnicity, string children, string education, 
            string income,int heightFeet, int heightInches, string smoke, string drink, string hometown)
        {

            bool includeHometown = (hometown == "false!" ? false : true);

            string sHeightFeet = heightFeet.ToString();
            string sHeightInches = heightInches.ToString();

            if (heightFeet == 0)
            {
                sHeightFeet = "NULL";
                sHeightInches = "NULL";
            }
            else
            {
                if (heightInches == -1)
                {
                    sHeightInches = "0";
                }
            }
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sqlUpdate = "UPDATE user_profiles " +
                " SET " +
                " relationship = @relationship," +
                " orientation = @orientation," +
                " religion = @religion," +
                " ethnicity = @ethnicity," +
                " children = @children," +
                " education = @education," +
                " income = @income," +
                " height_feet = " + sHeightFeet + "," +
                " height_inches =  " + sHeightInches + "," +
                " smoke = @smoke," +
                " drink = @drink";

            if (includeHometown)
            {
                sqlUpdate += ", hometown = @hometown";
                parameters.Add(new MySqlParameter("@hometown", hometown));
            }

            sqlUpdate += " WHERE user_id = @userId";

            parameters.Add(new MySqlParameter("@relationship", relationship));
            parameters.Add(new MySqlParameter("@orientation", orientation));
            parameters.Add(new MySqlParameter("@religion", religion));
            parameters.Add(new MySqlParameter("@ethnicity", ethnicity));
            parameters.Add(new MySqlParameter("@children", children));
            parameters.Add(new MySqlParameter("@education", education));
            parameters.Add(new MySqlParameter("@income", income));
            parameters.Add(new MySqlParameter("@smoke", smoke));
            parameters.Add(new MySqlParameter("@drink", drink));
            parameters.Add(new MySqlParameter("@userId", userId));

            return Db.Master.ExecuteNonQuery(sqlUpdate, parameters);
        }

  
        
        #endregion

        /// <summary>
        /// Get user Id, player id for game if it exists otherwise 0
        /// </summary>
        /// <returns></returns>
        public DataRow GetUserIdPlayerIdFromUsername(string username)
        {
            DataTable dtUserInfo = (DataTable)CentralCache.Get(CentralCache.keyKanevaUserInfo + username.ToString());

            if (dtUserInfo != null)
            {
                // Found in cache
                if (dtUserInfo.Rows.Count > 0)
                {
                    return dtUserInfo.Rows [0];
                }
            }

            string sqlSelectString = "SELECT ku.user_id, COALESCE(ku.wok_player_id, 0) as player_id, thumbnail_medium_path, thumbnail_square_path FROM " +
                "kaneva.users ku INNER JOIN kaneva.communities_personal cb ON ku.user_id = cb.creator_id " +
                " WHERE ku.username = @username";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@username", username));

            dtUserInfo = Db.Slave.GetDataTable(sqlSelectString.ToString(), parameters);

            if (dtUserInfo.Rows.Count > 0)
            {
                // Be safe not sure how wok works here??? I don't think it would be calling this if not wok player id
                if (Convert.ToInt32(dtUserInfo.Rows [0]["player_id"]) > 0)
                {
                    CentralCache.Store(CentralCache.keyKanevaUserInfo + username.ToString(), dtUserInfo, TimeSpan.FromMinutes(15));
                }

                return dtUserInfo.Rows[0];
            }

            return null;
        }

        /// <summary>
        /// GetPersonalChannelId
        /// </summary>
        public int GetPersonalChannelId(int userId)
        {
            if (userId.Equals (0))
            {
                return 0;
            }
            
            string cacheKey = CentralCache.keyCommunitiesPersonalUserId + userId.ToString();
            String strCommunityId = (String)CentralCache.Get(cacheKey);
            
            if (strCommunityId != null)
            {
                // Found in cache
                return Convert.ToInt32(strCommunityId);
            }

            string sqlSelect = "SELECT community_id " +
                " FROM communities_personal " +
                " WHERE creator_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            int communityId = (int) Db.Slave.GetScalar (sqlSelect, parameters);

            CentralCache.Store(cacheKey, communityId.ToString (), TimeSpan.FromMinutes(15));

            return communityId;
        }

        public FacebookSettings GetUserFacebookSettings (int userId)
        {
            FacebookSettings fbs = (FacebookSettings) CentralCache.Get (CentralCache.keyUserFBSettings (userId));

            if (fbs != null)
            {
                // Found in cache
                return fbs;
            }

            string sql = "SELECT fb_user_id, access_token, use_facebook_profile_picture " +
                " FROM user_facebook_settings " +
                " WHERE user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            DataRow row = Db.Slave2.GetDataRow (sql.ToString (), parameters);

            if (row == null)
            {
                fbs = new FacebookSettings ();
            }
            else
            {
                fbs = new FacebookSettings (
                    userId, Convert.ToUInt64 (row["fb_user_id"]), row["access_token"].ToString (),
                    Convert.ToBoolean (row["use_facebook_profile_picture"])
                    );
            }

            CentralCache.Store (CentralCache.keyUserFBSettings (userId), fbs, TimeSpan.FromMinutes (30));
            return fbs;
        }
        
        public int ConnectUserToFacebook (int userId, UInt64 fbUserId, bool useFBProfilePic, string accessToken)
        {
            string sqlString = "REPLACE INTO user_facebook_settings " +
                            " (user_id, fb_user_id, access_token, use_facebook_profile_picture, created_date)" +
                            " VALUES (@userId, @fbUserId, @accessToken, @useFBProfilePic, NOW()); ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@fbUserId", fbUserId));
            parameters.Add (new MySqlParameter ("@accessToken", accessToken));
            parameters.Add (new MySqlParameter ("@useFBProfilePic", useFBProfilePic));
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int DisconnectUserFromFacebook (int userId)
        {
            string sqlString = "DELETE FROM user_facebook_settings " +
                            " WHERE user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int SetFacebookAccessToken (UInt64 fbUserId, string accessToken)
        {
            string sqlString = "UPDATE user_facebook_settings " +
                            " SET access_token = @accessToken " +
                            " WHERE fb_user_id = @fbUserId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@fbUserId", fbUserId));
            parameters.Add (new MySqlParameter ("@accessToken", accessToken));
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int SetUseFacebookProfilePic (int userId, bool useProfilePic)
        {
            string sqlString = "UPDATE user_facebook_settings " +
                " SET use_facebook_profile_picture = @useProfilePic " +            
                " WHERE user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@useProfilePic", useProfilePic));
            parameters.Add (new MySqlParameter ("@userId", userId));
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public bool IsFacebookUserAlreadyConnected (UInt64 fbUserId)
        {
            // Make sure the email does not already exist?
            string sqlString = "SELECT COUNT(*) FROM user_facebook_settings WHERE fb_user_id = @fbUserId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@fbUserId", fbUserId));
            return (Convert.ToInt32 (Db.Master.GetScalar (sqlString, parameters)) > 0);
        }

        public User GetUserByFacebookUserId (UInt64 fbUserId)
        {
            string sql = "SELECT u.user_id, username, display_name, TIMESTAMPDIFF(year,DATE(birth_date),now()) AS age, role, account_type, first_name, last_name, registration_key, key_value, " +
                " u.description, gender, homepage, u.email, show_mature, u.status_id, u.email_status, signup_date, birth_date, newsletter, u.public_profile, " +
                " u.country,u.ip_address, u.last_ip_address, zip_code, u.location, u.over_21, u.own_mod, u.active, " +
                " u.last_login, u.second_to_last_login, u.blast_privacy_permissions, u.blast_show_permissions, " +
                " u.browse_anonymously, u.show_online, u.online, u.ustate, u.notify_blog_comments, " +
                " u.notify_profile_comments, u.notify_friend_messages, u.notify_anyone_messages, " +
                " u.notify_friend_requests, u.notify_new_friends, u.friends_can_comment, u.everyone_can_comment, u.mature_profile, " +
                " u.salt, u.password, " +
                //" u.wok_player_id, u.join_source_community_id, u.receive_updates, " +
                // THIS IS A TEMPORARY HACK
                // Using master_user_id for receive_updates until we implement schema changes.
                " u.wok_player_id, u.join_source_community_id, coalesce(u.master_user_id, 1) as receive_updates, " +
                " com.thumbnail_path, com.community_id, com.template_id, com.name_no_spaces, com.url, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_xlarge_path, " +
                " com.thumbnail_square_path, cs.number_of_diggs, cs.number_of_views, cs.number_times_shared, u.home_community_id " +
                " FROM users u " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
                " INNER JOIN channel_stats cs ON cs.channel_id = com.community_id " +
                " INNER JOIN user_facebook_settings ufbs ON ufbs.user_id = u.user_id " +
                " WHERE ufbs.fb_user_id = @fbUserId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@fbUserId", fbUserId));

            DataRow row = Db.Master.GetDataRow (sql.ToString (), parameters);

            if (row == null)
            {
                User userBlank = new User ();
                userBlank.Stats = new UserStats ();
                return userBlank;
            }

            return new User (Convert.ToInt32 (row["user_id"]), row["username"].ToString (), row["display_name"].ToString (),
                Convert.ToInt32 (row["role"]), row["registration_key"].ToString (), Convert.ToInt32 (row["account_type"]),
                row["first_name"].ToString (), row["last_name"].ToString (), row["description"].ToString (), row["gender"].ToString (),
                row["homepage"].ToString (), row["email"].ToString (),
                Convert.ToInt32 (row["status_id"]), Convert.ToInt32 (row["email_status"]), row["key_value"].ToString (), Convert.ToDateTime (row["last_login"]), Convert.ToDateTime (row["signup_date"]),
                row["birth_date"].ToString (), row["newsletter"].ToString (),
                row["public_profile"].ToString ().Equals ("Y"), row["zip_code"].ToString (), row["country"].ToString (), row["ip_address"].ToString (), row["last_ip_address"].ToString (),
                Convert.ToInt32 (row["show_mature"]).Equals (1), Convert.ToInt32 (row["browse_anonymously"]).Equals (1),
                Convert.ToInt32 (row["show_online"]).Equals (1), row["notify_blog_comments"].ToString (), row["notify_profile_comments"].ToString (), row["notify_friend_messages"].ToString (),
                row["notify_anyone_messages"].ToString (), row["notify_friend_requests"].ToString (), Convert.ToInt16 (row["notify_new_friends"]).Equals (1), Convert.ToInt32 (row["friends_can_comment"]), Convert.ToInt32 (row["everyone_can_comment"]),
                Convert.ToInt32 (row["mature_profile"]).Equals (1), Convert.ToInt32 (row["online"]), Convert.ToDateTime (row["second_to_last_login"]), Convert.ToInt32 (row["age"]),
                row["location"].ToString (), row["own_mod"].ToString ().Equals ("Y"), row["active"].ToString ().Equals ("Y"),
                Convert.ToUInt32 (row["blast_show_permissions"]), Convert.ToUInt32 (row["blast_privacy_permissions"]), row["ustate"].ToString (), row["over_21"].ToString ().Equals ("Y"),
                Convert.ToInt32 (row["wok_player_id"]), Convert.ToInt32 (row["join_source_community_id"]), row["receive_updates"].ToString (),
                Convert.ToInt32 (row["community_id"]), Convert.ToInt32 (row["template_id"]),
                row["name_no_spaces"].ToString (), row["url"].ToString (), row["thumbnail_path"].ToString (),
                row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (), row["thumbnail_xlarge_path"].ToString (),
                row["thumbnail_square_path"].ToString (), Convert.ToInt32 (row["number_of_diggs"]), Convert.ToInt32 (row["number_of_views"]), Convert.ToInt32 (row["number_times_shared"]),
                row["salt"].ToString(), row["password"].ToString(), Convert.ToInt32(row["home_community_id"] == DBNull.Value ? "0" : row["home_community_id"])
            );
        }

        public IList<UInt64> GetFacebookUserIdForConnectedUsers (string fbUserIdList)
        {
            string sql = "SELECT fb_user_id FROM user_facebook_settings " +
                " WHERE fb_user_id IN (" + fbUserIdList + ") ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            DataTable dt = Db.Slave.GetDataTable (sql, parameters);
            IList<UInt64> list = new List<UInt64> ();

            foreach (DataRow row in dt.Rows)
            {
                list.Add (Convert.ToUInt64 (row["fb_user_id"]));
            }

            return list;
        }

        //GetFacebookFriendsOnKanevaButNotMyFriendByFacebookUserId
        public PagedList<User> GetUsersByFacebookUserId (int userId, string idList, string orderby, int pageNumber, int pageSize)
        {
            string sql = "SELECT u.user_id, username, display_name, TIMESTAMPDIFF(year,DATE(birth_date),now()) AS age, role, account_type, first_name, last_name, registration_key, key_value, " +
                " u.description, gender, homepage, u.email, show_mature, u.status_id, u.email_status, signup_date, birth_date, newsletter, u.public_profile, " +
                " u.country,u.ip_address, u.last_ip_address, zip_code, u.location, u.over_21, u.own_mod, u.active, " +
                " u.last_login, u.second_to_last_login, u.blast_privacy_permissions, u.blast_show_permissions, " +
                " u.browse_anonymously, u.show_online, u.online, u.ustate, u.notify_blog_comments, " +
                " u.notify_profile_comments, u.notify_friend_messages, u.notify_anyone_messages, " +
                " u.notify_friend_requests, u.notify_new_friends, u.friends_can_comment, u.everyone_can_comment, u.mature_profile, " +
                " u.salt, u.password, " +
                " u.wok_player_id, u.join_source_community_id, coalesce(u.master_user_id, 1) as receive_updates, " +
                " com.thumbnail_path, com.community_id, com.template_id, com.name_no_spaces, com.url, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_xlarge_path, " +
                " com.thumbnail_square_path, cs.number_of_diggs, cs.number_of_views, cs.number_times_shared, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture, u.home_community_id " +
                " FROM user_facebook_settings ufs " +
                " INNER JOIN users u ON u.user_id = ufs.user_id " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
                " INNER JOIN channel_stats cs ON cs.channel_id = com.community_id " +
                " WHERE ufs.fb_user_id IN (" + idList + ") " +
                " AND ufs.user_id NOT IN " +
                "   (" +
                "   SELECT user_id FROM friends WHERE friend_id = @userId " +
                "   UNION DISTINCT " +
                "   SELECT friend_id FROM friends WHERE user_id = @userId)";
                

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            PagedDataTable dt = Db.Master.GetPagedDataTable(sql, orderby, parameters, pageNumber, pageSize);

            PagedList<User> list = new PagedList<User>();
            //assigns total count
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                User user = new User (Convert.ToInt32 (row["user_id"]), row["username"].ToString (), row["display_name"].ToString (),
                Convert.ToInt32 (row["role"]), row["registration_key"].ToString (), Convert.ToInt32 (row["account_type"]),
                row["first_name"].ToString (), row["last_name"].ToString (), row["description"].ToString (), row["gender"].ToString (),
                row["homepage"].ToString (), row["email"].ToString (),
                Convert.ToInt32 (row["status_id"]), Convert.ToInt32 (row["email_status"]), row["key_value"].ToString (), Convert.ToDateTime (row["last_login"]), Convert.ToDateTime (row["signup_date"]),
                row["birth_date"].ToString (), row["newsletter"].ToString (),
                row["public_profile"].ToString ().Equals ("Y"), row["zip_code"].ToString (), row["country"].ToString (), row["ip_address"].ToString (), row["last_ip_address"].ToString (),
                Convert.ToInt32 (row["show_mature"]).Equals (1), Convert.ToInt32 (row["browse_anonymously"]).Equals (1),
                Convert.ToInt32 (row["show_online"]).Equals (1), row["notify_blog_comments"].ToString (), row["notify_profile_comments"].ToString (), row["notify_friend_messages"].ToString (),
                row["notify_anyone_messages"].ToString (), row["notify_friend_requests"].ToString (), Convert.ToInt16 (row["notify_new_friends"]).Equals (1), Convert.ToInt32 (row["friends_can_comment"]), Convert.ToInt32 (row["everyone_can_comment"]),
                Convert.ToInt32 (row["mature_profile"]).Equals (1), Convert.ToInt32 (row["online"]), Convert.ToDateTime (row["second_to_last_login"]), Convert.ToInt32 (row["age"]),
                row["location"].ToString (), row["own_mod"].ToString ().Equals ("Y"), row["active"].ToString ().Equals ("Y"),
                Convert.ToUInt32 (row["blast_show_permissions"]), Convert.ToUInt32 (row["blast_privacy_permissions"]), row["ustate"].ToString (), row["over_21"].ToString ().Equals ("Y"),
                Convert.ToInt32 (row["wok_player_id"]), Convert.ToInt32 (row["join_source_community_id"]), row["receive_updates"].ToString (),
                Convert.ToInt32 (row["community_id"]), Convert.ToInt32 (row["template_id"]),
                row["name_no_spaces"].ToString (), row["url"].ToString (), row["thumbnail_path"].ToString (),
                row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (), row["thumbnail_xlarge_path"].ToString (),
                row["thumbnail_square_path"].ToString (), Convert.ToInt32 (row["number_of_diggs"]), Convert.ToInt32 (row["number_of_views"]), Convert.ToInt32 (row["number_times_shared"]),
                row["salt"].ToString(), row["password"].ToString(), Convert.ToInt32(row["home_community_id"] == DBNull.Value ? "0" : row["home_community_id"]));

                user.FacebookSettings.FacebookUserId = Convert.ToUInt64 (row["fb_user_id"]);
                user.FacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (row["use_facebook_profile_picture"]);

                list.Add (user);
            }
 
            return list;
        }

        /// <summary>
        /// InvalidateKanevaUserCache
        /// </summary>
        /// <param name="userId"></param>
        public void InvalidateKanevaUserFacebookCache (int userId)
        {
            CentralCache.Remove (CentralCache.keyUserFBSettings (userId));
        }

        // Tracking
        public int InsertTrackingRequest(string requestId, int requestTypeId, int userIdSent, int gameId, int communityId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sql = "INSERT INTO metrics.requests (request_id, request_type_id, user_id_sent, game_id, community_id, request_date) " +
                         "VALUES ( @requestId, @requestTypeId, @userIdSent, @gameId, @communityId, NOW() )";

            parameters.Add(new MySqlParameter("@requestId", requestId));
            parameters.Add(new MySqlParameter("@requestTypeId", requestTypeId));
            parameters.Add(new MySqlParameter("@userIdSent", userIdSent));
            parameters.Add(new MySqlParameter("@gameId", gameId));
            parameters.Add(new MySqlParameter("@communityId", communityId));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int InsertTrackingTypeSent(string requestTypeSentId, string requestId, int messageTypeId, int userId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sql = "INSERT INTO metrics.request_types_sent (request_types_sent_id, request_id, message_type_id, user_id, sent_date) " +
                         "VALUES ( @requestTypeSentId, @requestId, @messageTypeId, @userId, NOW())";

            parameters.Add(new MySqlParameter("@requestTypeSentId", requestTypeSentId));
            parameters.Add(new MySqlParameter("@requestId", requestId));
            parameters.Add(new MySqlParameter("@messageTypeId", messageTypeId));
            parameters.Add(new MySqlParameter("@userId", userId));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int AcceptTrackingRequest(string requestTypeSentId, int userId, int acceptTypeId, char isIE, string userAgent)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sql = "INSERT INTO metrics.request_types_accepted (request_types_sent_id, user_id, accept_date, accept_type_id, is_ie, user_agent) " +
                         "VALUES ( @requestTypeSentId, @userId, NOW(), @acceptType, @isIEParam, @userAgentParam)";

            parameters.Add(new MySqlParameter("@requestTypeSentId", requestTypeSentId));
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@acceptType", acceptTypeId));
            parameters.Add(new MySqlParameter("@isIEParam", isIE));
            parameters.Add(new MySqlParameter("@userAgentParam", userAgent));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public int GetTrackingRequestType(string requestTypeSentId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sql = "SELECT r.request_type_id " +
                         "FROM metrics.request_types_sent rts " +
                         "INNER JOIN metrics.requests r ON rts.request_id = r.request_id " +
                         "WHERE rts.request_types_sent_id = @requestTypeSentId";

            parameters.Add(new MySqlParameter("@requestTypeSentId", requestTypeSentId));

            DataRow row = Db.Master.GetDataRow(sql, parameters);

            if (row == null)
            {
                return -1;
            }
            return (int)row["request_type_id"];
        }

        #region System Configuration

        public Graph<int, User> GetUserAltsGraph(int passedUserId)
        {
            // Building an undirected graph of users, joined by systemId
            Graph<int, User> graph = new Graph<int, User>();

            // Validate username and add as initial node
            User passedUser = GetUser(passedUserId);
            if (passedUser == null || passedUser.UserId != passedUserId)
                return graph;

            graph.AddNode(passedUserId, passedUser);

            // We'll do two queries here: 1 for all the user ids (nodes), 1 for the edges (systems)
            string sqlNodes = "SELECT lvl2.user_id, u.username, display_name, TIMESTAMPDIFF(year,DATE(birth_date),now()) AS age, role, account_type, first_name, last_name, registration_key, key_value, " +
                              "u.description, gender, homepage, u.email, show_mature, u.status_id, u.email_status, signup_date, birth_date, newsletter, u.public_profile, " +
                              "u.country,u.ip_address, u.last_ip_address, zip_code, u.location, u.over_21, u.own_mod, u.active, " +
                              "u.last_login, u.second_to_last_login, u.blast_privacy_permissions, u.blast_show_permissions, " +
                              "u.browse_anonymously, u.show_online, u.online, u.ustate, u.notify_blog_comments, " +
                              "u.notify_profile_comments, u.notify_friend_messages, u.notify_anyone_messages, " +
                              "u.notify_friend_requests, u.notify_new_friends, u.friends_can_comment, u.everyone_can_comment, u.mature_profile, " +
                              "u.salt, u.password, " +
                              "u.wok_player_id, u.join_source_community_id, coalesce(u.master_user_id, 1) as receive_updates, " +
                              "com.thumbnail_path, com.community_id, com.template_id, com.name_no_spaces, com.url, " +
                              "com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_xlarge_path, " +
                              "com.thumbnail_square_path, cs.number_of_diggs, cs.number_of_views, cs.number_times_shared, u.home_community_id " +
                              "FROM kaneva.users_system_ids lvl1 " +
                              "INNER JOIN kaneva.users_system_ids lvl2 ON lvl1.system_id = lvl2.system_id " +
                              "INNER JOIN kaneva.users u ON lvl2.user_id = u.user_id " +
                              "AND lvl2.user_id NOT IN ({0}) " +
                              "INNER JOIN kaneva.communities_personal com ON com.creator_id = u.user_id " +
                              "INNER JOIN kaneva.channel_stats cs ON cs.channel_id = com.community_id " +
                              "WHERE lvl1.user_id IN ({0}) ";

            string sqlEdges = "SELECT u1.user_id AS 'from', u2.user_id AS 'to', lvl1.system_id AS label, 0 AS cost " +
                              "FROM kaneva.users_system_ids lvl1 " +
                              "INNER JOIN kaneva.users u1 ON lvl1.user_id = u1.user_id " +
                              "LEFT OUTER JOIN kaneva.users_system_ids lvl2 ON lvl1.system_id = lvl2.system_id " +
	                          "AND lvl1.user_id <> lvl2.user_id " +
                              "LEFT OUTER JOIN kaneva.users u2 ON lvl2.user_id = u2.user_id " +
                              "WHERE lvl1.user_id IN ({0}) ";

            // Prime the string of users with the passed in id
            StringBuilder sb = new StringBuilder();
            sb.Append(passedUserId);

            // Pull adjacent userIds from the db until no more are found
            bool nodesRemaining = true;
            while (nodesRemaining)
            {
                DataTable dtNodes = Db.Master.GetDataTable(string.Format(sqlNodes, sb.ToString()));

                if (dtNodes != null && dtNodes.Rows.Count > 0)
                {
                    foreach (DataRow row in dtNodes.Rows)
                    {
                        if (!graph.Contains((int)row["user_id"]))
                        {
                            sb.Append(",");
                            sb.Append(row["user_id"]);

                            graph.AddNode((int)row["user_id"], new User(Convert.ToInt32(row["user_id"]), row["username"].ToString(), row["display_name"].ToString(),
                                Convert.ToInt32(row["role"]), row["registration_key"].ToString(), Convert.ToInt32(row["account_type"]),
                                row["first_name"].ToString(), row["last_name"].ToString(), row["description"].ToString(), row["gender"].ToString(),
                                row["homepage"].ToString(), row["email"].ToString(),
                                Convert.ToInt32(row["status_id"]), Convert.ToInt32(row["email_status"]), row["key_value"].ToString(), Convert.ToDateTime(row["last_login"]), Convert.ToDateTime(row["signup_date"]),
                                row["birth_date"].ToString(), row["newsletter"].ToString(),
                                row["public_profile"].ToString().Equals("Y"), row["zip_code"].ToString(), row["country"].ToString(), row["ip_address"].ToString(), row["last_ip_address"].ToString(),
                                Convert.ToInt32(row["show_mature"]).Equals(1), Convert.ToInt32(row["browse_anonymously"]).Equals(1),
                                Convert.ToInt32(row["show_online"]).Equals(1), row["notify_blog_comments"].ToString(), row["notify_profile_comments"].ToString(), row["notify_friend_messages"].ToString(),
                                row["notify_anyone_messages"].ToString(), row["notify_friend_requests"].ToString(), Convert.ToInt16(row["notify_new_friends"]).Equals(1), Convert.ToInt32(row["friends_can_comment"]), Convert.ToInt32(row["everyone_can_comment"]),
                                Convert.ToInt32(row["mature_profile"]).Equals(1), Convert.ToInt32(row["online"]), Convert.ToDateTime(row["second_to_last_login"]), Convert.ToInt32(row["age"]),
                                row["location"].ToString(), row["own_mod"].ToString().Equals("Y"), row["active"].ToString().Equals("Y"),
                                Convert.ToUInt32(row["blast_show_permissions"]), Convert.ToUInt32(row["blast_privacy_permissions"]), row["ustate"].ToString(), row["over_21"].ToString().Equals("Y"),
                                Convert.ToInt32(row["wok_player_id"]), Convert.ToInt32(row["join_source_community_id"]), row["receive_updates"].ToString(),
                                Convert.ToInt32(row["community_id"]), Convert.ToInt32(row["template_id"]),
                                row["name_no_spaces"].ToString(), row["url"].ToString(), row["thumbnail_path"].ToString(),
                                row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_xlarge_path"].ToString(),
                                row["thumbnail_square_path"].ToString(), Convert.ToInt32(row["number_of_diggs"]), Convert.ToInt32(row["number_of_views"]), Convert.ToInt32(row["number_times_shared"]),
                                row["salt"].ToString(), row["password"].ToString(), Convert.ToInt32(row["home_community_id"] == DBNull.Value ? "0" : row["home_community_id"])
                            ));
                        }
                    }
                }
                else
                {
                    nodesRemaining = false;
                }
            }

            // Add all the edges to the graph as directed edges. Adding directed edges in both directions is the same as adding undirected
            DataTable dtEdges = Db.Master.GetDataTable(string.Format(sqlEdges, sb.ToString()));

            if (dtEdges != null && dtEdges.Rows.Count > 0)
            {
                foreach (DataRow row in dtEdges.Rows)
                {
                    // If we don't have a "to" node, connect to the user itself to prevent dropping systems
                    if (row["to"] == DBNull.Value)
                    {
                        graph.AddDirectedEdge((int)row["from"], (int)row["from"], row["label"].ToString());
                    }
                    else
                    {
                        graph.AddDirectedEdge((int)row["from"], (int)row["to"], row["label"].ToString());
                    }
                }
            }

            return graph;
        }

        public UserSystem GetUserSystem(string systemId)
        {
            string sql = " SELECT usc.system_id, usc.date_first_used, usc.date_last_used " +
                         " FROM user_systems usc " +
                         " WHERE usc.system_id = @systemId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@systemId", systemId));

            DataRow row = Db.Master.GetDataRow(sql.ToString(), parameters);

            if (row == null)
            {
                return new UserSystem();
            }

            return new UserSystem(Convert.ToString(row["system_id"]),
                Convert.ToDateTime(row["date_first_used"]), Convert.ToDateTime(row["date_last_used"]));
        }

        public List<UserSystem> GetUserSystems(int userId)
        {
            string sql = " SELECT usc.system_id, usc.date_first_used, usc.date_last_used " +
                         " FROM user_systems usc " +
                         " INNER JOIN users_system_ids ids ON usc.system_id = ids.system_id " +
                         " AND ids.user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            DataTable dt = Db.Master.GetDataTable(sql.ToString(), parameters);

            if (dt == null || dt.Rows.Count == 0)
            {
                return new List<UserSystem>();
            }

            List<UserSystem> systems = new List<UserSystem>();
            foreach(DataRow row in dt.Rows)
            {
                systems.Add(new UserSystem(Convert.ToString(row["system_id"]),
                Convert.ToDateTime(row["date_first_used"]), Convert.ToDateTime(row["date_last_used"])));
            }

            return systems;
        }

        public string GetValidSystemId(string systemId, uint? cpus, uint? cpuMhz, ulong? sysMem, ulong? diskSize, uint? gpuX, uint? gpuY,
            uint? gpuHz, uint? gpuFmt, string dxVer, string cpuVer, string sysVer, string net, string netMAC, string netType, string netHash, string gpu)
        {
            // If passed system id is valid, we can use it
            if (!string.IsNullOrWhiteSpace(systemId))
            {
                UserSystem system = GetUserSystem(systemId);
                if (!string.IsNullOrWhiteSpace(system.SystemId))
                {
                    return system.SystemId;
                }
            }

            // Next try an "exact" match from configs
            List<string> possibleMatches = GetUserSystemIdsMatchExactConfig(cpus, cpuMhz, sysMem, diskSize, dxVer, cpuVer, sysVer, 
                net, netMAC, netType, netHash, gpu);
            
            if (possibleMatches.Count > 0)
            {

                return possibleMatches[0];
            }
            
            // If all else fails, return a new id
            return (Db.Master.GetScalar("SELECT uuid()") ?? string.Empty).ToString();
        }

        public bool InsertUserSystem(string systemId, bool propagateExceptions = false)
        {
            bool result = false;

            try
            {
                string sql = "INSERT INTO user_systems (system_id, date_first_used, date_last_used) " +
                                "VALUES (@systemId, NOW(), NOW()) " +
                                "ON DUPLICATE KEY UPDATE date_last_used = NOW() ";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@systemId", systemId));

                Db.Master.ExecuteNonQuery(sql, parameters, true);

                // If we reach this point, the transaction succeeded
                result = true;
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLUserDao.InsertUserSystem(): " + e.ToString() + "\n" +
                    "@systemId: " + (systemId ?? "NULL"));

                if (propagateExceptions)
                    throw e;
            }

            return result;
        }

        public List<string> GetUserSystemIdsMatchExactConfig(uint? cpus, uint? cpuMhz, ulong? sysMem, ulong? diskSize,
            string dxVer, string cpuVer, string sysVer, string net, string netMAC, string netType, string netHash, string gpu)
        {
            // Check only major versions for OS
            if (!string.IsNullOrWhiteSpace(sysVer))
            {
                string[] sysVerParts = sysVer.Split(new char[] { '.' });
                if (sysVerParts.Length >= 2)
                {
                    sysVer = string.Format("{0}.{1}.%", sysVerParts[0], sysVerParts[1]);
                }
            }

            // Check only major versions for dxVer
            if (!string.IsNullOrWhiteSpace(dxVer))
            {
                string[] dxVerParts = dxVer.Split(new char[] { '.' });
                if (dxVerParts.Length >= 1)
                {
                    dxVer = string.Format("{0}.%", dxVerParts[0]);
                }
            }

            string sql = "SELECT usc.system_id " +
                " FROM user_system_configurations usc " +
                " WHERE usc.system_id IS NOT NULL ";

            sql += (cpus == null ? " AND usc.cpu_count IS @cpus " : " AND usc.cpu_count = @cpus ");
            sql += (cpuMhz == null ? " AND usc.cpu_mhz IS @cpuMhz " : " AND usc.cpu_mhz = @cpuMhz ");
            sql += (sysMem == null ? " AND usc.system_memory_bytes IS @sysMem " : " AND usc.system_memory_bytes = @sysMem ");
            sql += (diskSize == null ? " AND usc.disk_size_bytes IS @diskSize " : " AND usc.disk_size_bytes = @diskSize ");
            sql += (string.IsNullOrWhiteSpace(dxVer) ? " AND usc.dx_version IS @dxVer " : " AND usc.dx_version LIKE @dxVer ");
            sql += (string.IsNullOrWhiteSpace(cpuVer) ? " AND usc.cpu_version IS @cpuVer " : " AND usc.cpu_version = @cpuVer ");
            sql += (string.IsNullOrWhiteSpace(sysVer) ? " AND usc.os_version IS @sysVer " : " AND usc.os_version LIKE @sysVer ");
            sql += (string.IsNullOrWhiteSpace(netMAC) ? " AND usc.net_interface_mac IS @netMAC " : " AND usc.net_interface_mac = @netMAC ");
            sql += (string.IsNullOrWhiteSpace(netType) ? " AND usc.net_interface_type IS @netType " : " AND usc.net_interface_type = @netType ");
            sql += (string.IsNullOrWhiteSpace(netHash) ? " AND usc.net_interface_hash IS @netHash " : " AND usc.net_interface_hash = @netHash ");
            sql += (string.IsNullOrWhiteSpace(gpu) ? " AND usc.gpu_description IS @gpu " : " AND usc.gpu_description = @gpu ");
            sql += " ORDER BY usc.date_last_used DESC ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@cpus", (cpus == null ? DBNull.Value : (object)cpus)));
            parameters.Add(new MySqlParameter("@cpuMhz", (cpuMhz == null ? DBNull.Value : (object)cpuMhz)));
            parameters.Add(new MySqlParameter("@sysMem", (sysMem == null ? DBNull.Value : (object)sysMem)));
            parameters.Add(new MySqlParameter("@diskSize", (diskSize == null ? DBNull.Value : (object)diskSize)));
            parameters.Add(new MySqlParameter("@dxVer", (string.IsNullOrWhiteSpace(dxVer) ? DBNull.Value : (object)dxVer)));
            parameters.Add(new MySqlParameter("@cpuVer", (string.IsNullOrWhiteSpace(cpuVer) ? DBNull.Value : (object)cpuVer)));
            parameters.Add(new MySqlParameter("@sysVer", (string.IsNullOrWhiteSpace(sysVer) ? DBNull.Value : (object)sysVer)));
            parameters.Add(new MySqlParameter("@netMAC", (string.IsNullOrWhiteSpace(netMAC) ? DBNull.Value : (object)netMAC)));
            parameters.Add(new MySqlParameter("@netType", (string.IsNullOrWhiteSpace(netType) ? DBNull.Value : (object)netType)));
            parameters.Add(new MySqlParameter("@netHash", (string.IsNullOrWhiteSpace(netHash) ? DBNull.Value : (object)netHash)));
            parameters.Add(new MySqlParameter("@gpu", (string.IsNullOrWhiteSpace(gpu) ? DBNull.Value : (object)gpu)));

            DataTable dt = Db.Master.GetDataTable(sql.ToString(), parameters);

            if (dt == null || dt.Rows.Count < 1)
            {
                return new List<string>();
            }

            List<string> configs = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                configs.Add(row["system_id"].ToString());
            }

            return configs.Distinct().ToList();
        }

        public bool InsertUsersSystemIds(int userId, string systemId)
        {
            if (string.IsNullOrWhiteSpace(systemId) || userId < 0)
            {
                return false;
            }

            string sql = "INSERT INTO users_system_ids (user_id, system_id, date_first_used, date_last_used) " +
                         "VALUES (@userId, @systemId, NOW(), NOW()) " +
                         "ON DUPLICATE KEY UPDATE date_last_used = NOW() ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@systemId", systemId));

            return Db.Master.ExecuteNonQuery(sql, parameters) > 0;
        }

        /// <summary>
        /// Retrieves a list of users that are associated with a given system.
        /// </summary>
        public List<User> GetSystemUsers(string systemId)
        {
            string sql = "SELECT user_id " +
                         "FROM users_system_ids " +
                         "WHERE system_id = @systemId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@systemId", systemId));

            DataTable dt = Db.Master.GetDataTable(sql, parameters);

            if (dt == null)
            {
                return new List<User>();
            }

            List<User> users = new List<User>();
            foreach (DataRow row in dt.Rows)
            {
                User user = GetCachedUser((int)row["user_id"]);      
                if (user == null)
                {
                    user = GetUser((int)row["user_id"]);
                }
                users.Add(user);
            }

            return users;
        }

        public UserSystemConfiguration GetUserSystemConfiguation(string systemConfigurationId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@systemConfigurationId", systemConfigurationId));
            IList<UserSystemConfiguration> results = GetUserSystemConfiguationsByCondition("usc.system_configuration_id = @systemConfigurationId", parameters);

            if (results != null && results.Count == 1)
            {
                return results[0];
            }

            return new UserSystemConfiguration();
        }

        public IList<UserSystemConfiguration> GetUserSystemConfiguationsBySystemName(string systemName)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@systemName", systemName));
            return GetUserSystemConfiguationsByCondition("usc.system_name = @systemName", parameters);
        }

        private IList<UserSystemConfiguration> GetUserSystemConfiguationsByCondition(string filter, List<IDbDataParameter> parameters)
        {
            string sql = "SELECT usc.system_configuration_id, usc.system_id, usc.os_version, usc.system_name, usc.cpu_count, usc.cpu_version, usc.cpu_mhz, " +
                " usc.system_memory_bytes, usc.disk_size_bytes, usc.gpu_description, usc.gpu_display_mode_width, " +
                " usc.gpu_display_mode_height, usc.gpu_refresh_rate_hz, usc.gpu_display_mode_format, usc.dx_version, usc.net_interface_mac, " +
                " usc.net_interface_description, usc.net_interface_type, usc.net_interface_hash, usc.date_first_used, usc.date_last_used, " +
                " cpu_phys_core, cpu_vendor, cpu_brand, cpu_family, cpu_model, cpu_stepping, cpu_sse3, cpu_ssse3, " +
                " cpu_sse4_1, cpu_sse4_2, cpu_sse4_A, cpu_sse5, cpu_avx, cpu_avx2, cpu_fma3, cpu_fma4, cpu_code_name " +
                " FROM user_system_configurations usc";

            if (filter != null && filter != "") {
                sql = sql + " WHERE " + filter;
            }

            DataTable dt = Db.Master.GetDataTable(sql, parameters);
            if (dt == null)
            {
                return null;
            }

            IList<UserSystemConfiguration> result = new List<UserSystemConfiguration>();

            foreach (DataRow row in dt.Rows) {
                UserSystemConfiguration userSystemConfiguration = new UserSystemConfiguration(row["system_configuration_id"].ToString(),
                            Convert.ToString(row["system_id"] == DBNull.Value ? null : row["system_id"]),
                            (row["cpu_count"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_count"])) as uint?,
                            (row["cpu_mhz"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_mhz"])) as uint?,
                            (row["system_memory_bytes"] == DBNull.Value ? null : (object)Convert.ToUInt64(row["system_memory_bytes"])) as ulong?,
                            (row["disk_size_bytes"] == DBNull.Value ? null : (object)Convert.ToUInt64(row["disk_size_bytes"])) as ulong?,
                            (row["gpu_display_mode_width"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["gpu_display_mode_width"])) as uint?,
                            (row["gpu_display_mode_height"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["gpu_display_mode_height"])) as uint?,
                            (row["gpu_refresh_rate_hz"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["gpu_refresh_rate_hz"])) as uint?,
                            (row["gpu_display_mode_format"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["gpu_display_mode_format"])) as uint?,
                            Convert.ToString(row["dx_version"] == DBNull.Value ? null : row["dx_version"]),
                            Convert.ToString(row["cpu_version"] == DBNull.Value ? null : row["cpu_version"]),
                            Convert.ToString(row["os_version"] == DBNull.Value ? null : row["os_version"]),
                            Convert.ToString(row["system_name"] == DBNull.Value ? null : row["system_name"]),
                            Convert.ToString(row["net_interface_description"] == DBNull.Value ? null : row["net_interface_description"]),
                            Convert.ToString(row["net_interface_mac"] == DBNull.Value ? null : row["net_interface_mac"]),
                            Convert.ToString(row["net_interface_type"] == DBNull.Value ? null : row["net_interface_type"]),
                            Convert.ToString(row["net_interface_hash"] == DBNull.Value ? null : row["net_interface_hash"]),
                            Convert.ToString(row["gpu_description"] == DBNull.Value ? null : row["gpu_description"]),
                            Convert.ToDateTime(row["date_first_used"]),
                            Convert.ToDateTime(row["date_last_used"]),
                            (row["cpu_phys_core"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_phys_core"])) as uint?,
                            Convert.ToString(row["cpu_vendor"] == DBNull.Value ? null : row["cpu_vendor"]),
                            Convert.ToString(row["cpu_brand"] == DBNull.Value ? null : row["cpu_brand"]),
                            (row["cpu_family"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_family"])) as uint?,
                            (row["cpu_model"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_model"])) as uint?,
                            (row["cpu_stepping"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_stepping"])) as uint?,
                            (row["cpu_sse3"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_sse3"])) as uint?,
                            (row["cpu_ssse3"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_ssse3"])) as uint?,
                            (row["cpu_sse4_1"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_sse4_1"])) as uint?,
                            (row["cpu_sse4_2"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_sse4_2"])) as uint?,
                            (row["cpu_sse4_A"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_sse4_A"])) as uint?,
                            (row["cpu_sse5"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_sse5"])) as uint?,
                            (row["cpu_avx"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_avx"])) as uint?,
                            (row["cpu_avx2"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_avx2"])) as uint?,
                            (row["cpu_fma3"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_fma3"])) as uint?,
                            (row["cpu_fma4"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_fma4"])) as uint?,
                            Convert.ToString(row["cpu_code_name"] == DBNull.Value ? null : row["cpu_code_name"])
                        );

                result.Add(userSystemConfiguration);
            }

            return result;
        }

        public UserSystemConfiguration GetUserSystemConfiguation(string systemId, uint? cpus, uint? cpuMhz, ulong? sysMem, ulong? diskSize, uint? gpuX, uint? gpuY,
            uint? gpuHz, uint? gpuFmt, string dxVer, string cpuVer, string sysVer, string sysName, string net, string netMAC, string netType, string netHash, string gpu)
        {
            string sql = "SELECT usc.system_configuration_id, usc.system_id, usc.os_version, usc.system_name, usc.cpu_count, usc.cpu_version, usc.cpu_mhz, " +
                " usc.system_memory_bytes, usc.disk_size_bytes, usc.gpu_description, usc.gpu_display_mode_width, " +
                " usc.gpu_display_mode_height, usc.gpu_refresh_rate_hz, usc.gpu_display_mode_format, usc.dx_version, usc.net_interface_mac, " +
                " usc.net_interface_description, usc.net_interface_type, usc.net_interface_hash, usc.date_first_used, usc.date_last_used, " +
                " cpu_phys_core, cpu_vendor, cpu_brand, cpu_family, cpu_model, cpu_stepping, cpu_sse3, cpu_ssse3, " +
                " cpu_sse4_1, cpu_sse4_2, cpu_sse4_A, cpu_sse5, cpu_avx, cpu_avx2, cpu_fma3, cpu_fma4, cpu_code_name " +
                " FROM user_system_configurations usc ";

            sql += (systemId == null ? " WHERE usc.system_id IS @systemId " : " WHERE usc.system_id = @systemId ");
            sql += (cpus == null ? " AND usc.cpu_count IS @cpus " : " AND usc.cpu_count = @cpus ");
            sql += (cpuMhz == null ? " AND usc.cpu_mhz IS @cpuMhz " : " AND usc.cpu_mhz = @cpuMhz ");
            sql += (sysMem == null ? " AND usc.system_memory_bytes IS @sysMem " : " AND usc.system_memory_bytes = @sysMem ");
            sql += (diskSize == null ? " AND usc.disk_size_bytes IS @diskSize " : " AND usc.disk_size_bytes = @diskSize ");
            sql += (gpuX == null ? " AND usc.gpu_display_mode_width IS @gpuX " : " AND usc.gpu_display_mode_width = @gpuX ");
            sql += (gpuY == null ? " AND usc.gpu_display_mode_height IS @gpuY " : " AND usc.gpu_display_mode_height = @gpuY ");
            sql += (gpuHz == null ? " AND usc.gpu_refresh_rate_hz IS @gpuHz " : " AND usc.gpu_refresh_rate_hz = @gpuHz ");
            sql += (gpuFmt == null ? " AND usc.gpu_display_mode_format IS @gpuFmt " : " AND usc.gpu_display_mode_format = @gpuFmt ");
            sql += (string.IsNullOrWhiteSpace(dxVer) ? " AND usc.dx_version IS @dxVer " : " AND usc.dx_version = @dxVer ");
            sql += (string.IsNullOrWhiteSpace(cpuVer) ? " AND usc.cpu_version IS @cpuVer " : " AND usc.cpu_version = @cpuVer ");
            sql += (string.IsNullOrWhiteSpace(sysVer) ? " AND usc.os_version IS @sysVer " : " AND usc.os_version = @sysVer ");
            sql += (string.IsNullOrWhiteSpace(sysName) ? " AND usc.system_name IS @sysName " : " AND usc.system_name = @sysName ");
            sql += (string.IsNullOrWhiteSpace(net) ? " AND usc.net_interface_description IS @net " : " AND usc.net_interface_description = @net ");
            sql += (string.IsNullOrWhiteSpace(netMAC) ? " AND usc.net_interface_mac IS @netMAC " : " AND usc.net_interface_mac = @netMAC ");
            sql += (string.IsNullOrWhiteSpace(netType) ? " AND usc.net_interface_type IS @netType " : " AND usc.net_interface_type = @netType ");
            sql += (string.IsNullOrWhiteSpace(netHash) ? " AND usc.net_interface_hash IS @netHash " : " AND usc.net_interface_hash = @netHash ");
            sql += (string.IsNullOrWhiteSpace(gpu) ? " AND usc.gpu_description IS @gpu " : " AND usc.gpu_description = @gpu ");

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@systemId", systemId));
            parameters.Add(new MySqlParameter("@cpus", (cpus == null ? DBNull.Value : (object)cpus)));
            parameters.Add(new MySqlParameter("@cpuMhz", (cpuMhz == null ? DBNull.Value : (object)cpuMhz)));
            parameters.Add(new MySqlParameter("@sysMem", (sysMem == null ? DBNull.Value : (object)sysMem)));
            parameters.Add(new MySqlParameter("@diskSize", (diskSize == null ? DBNull.Value : (object)diskSize)));
            parameters.Add(new MySqlParameter("@gpuX", (gpuX == null ? DBNull.Value : (object)gpuX)));
            parameters.Add(new MySqlParameter("@gpuY", (gpuY == null ? DBNull.Value : (object)gpuY)));
            parameters.Add(new MySqlParameter("@gpuHz", (gpuHz == null ? DBNull.Value : (object)gpuHz)));
            parameters.Add(new MySqlParameter("@gpuFmt", (gpuFmt == null ? DBNull.Value : (object)gpuFmt)));
            parameters.Add(new MySqlParameter("@dxVer", (string.IsNullOrWhiteSpace(dxVer) ? DBNull.Value : (object)dxVer)));
            parameters.Add(new MySqlParameter("@cpuVer", (string.IsNullOrWhiteSpace(cpuVer) ? DBNull.Value : (object)cpuVer)));
            parameters.Add(new MySqlParameter("@sysVer", (string.IsNullOrWhiteSpace(sysVer) ? DBNull.Value : (object)sysVer)));
            parameters.Add(new MySqlParameter("@sysName", (string.IsNullOrWhiteSpace(sysName) ? DBNull.Value : (object)sysName)));
            parameters.Add(new MySqlParameter("@net", (string.IsNullOrWhiteSpace(net) ? DBNull.Value : (object)net)));
            parameters.Add(new MySqlParameter("@netMAC", (string.IsNullOrWhiteSpace(netMAC) ? DBNull.Value : (object)netMAC)));
            parameters.Add(new MySqlParameter("@netType", (string.IsNullOrWhiteSpace(netType) ? DBNull.Value : (object)netType)));
            parameters.Add(new MySqlParameter("@netHash", (string.IsNullOrWhiteSpace(netHash) ? DBNull.Value : (object)netHash)));
            parameters.Add(new MySqlParameter("@gpu", (string.IsNullOrWhiteSpace(gpu) ? DBNull.Value : (object)gpu)));

            DataRow row = Db.Master.GetDataRow(sql.ToString(), parameters);

            if (row == null)
            {
                return new UserSystemConfiguration();
            }

            UserSystemConfiguration userSystemConfiguration = new UserSystemConfiguration(row["system_configuration_id"].ToString(),
                    Convert.ToString(row["system_id"] == DBNull.Value ? null : row["system_id"]),
                    (row["cpu_count"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_count"])) as uint?,
                    (row["cpu_mhz"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_mhz"])) as uint?,
                    (row["system_memory_bytes"] == DBNull.Value ? null : (object)Convert.ToUInt64(row["system_memory_bytes"])) as ulong?,
                    (row["disk_size_bytes"] == DBNull.Value ? null : (object)Convert.ToUInt64(row["disk_size_bytes"])) as ulong?,
                    (row["gpu_display_mode_width"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["gpu_display_mode_width"])) as uint?,
                    (row["gpu_display_mode_height"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["gpu_display_mode_height"])) as uint?,
                    (row["gpu_refresh_rate_hz"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["gpu_refresh_rate_hz"])) as uint?,
                    (row["gpu_display_mode_format"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["gpu_display_mode_format"])) as uint?,
                    Convert.ToString(row["dx_version"] == DBNull.Value ? null : row["dx_version"]),
                    Convert.ToString(row["cpu_version"] == DBNull.Value ? null : row["cpu_version"]),
                    Convert.ToString(row["os_version"] == DBNull.Value ? null : row["os_version"]),
                    Convert.ToString(row["system_name"] == DBNull.Value ? null : row["system_name"]),
                    Convert.ToString(row["net_interface_description"] == DBNull.Value ? null : row["net_interface_description"]),
                    Convert.ToString(row["net_interface_mac"] == DBNull.Value ? null : row["net_interface_mac"]),
                    Convert.ToString(row["net_interface_type"] == DBNull.Value ? null : row["net_interface_type"]),
                    Convert.ToString(row["net_interface_hash"] == DBNull.Value ? null : row["net_interface_hash"]),
                    Convert.ToString(row["gpu_description"] == DBNull.Value ? null : row["gpu_description"]),
                    Convert.ToDateTime(row["date_first_used"]),
                    Convert.ToDateTime(row["date_last_used"]),
                    (row["cpu_phys_core"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_phys_core"])) as uint?,
                    Convert.ToString(row["cpu_vendor"] == DBNull.Value ? null : row["cpu_vendor"]),
                    Convert.ToString(row["cpu_brand"] == DBNull.Value ? null : row["cpu_brand"]),
                    (row["cpu_family"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_family"])) as uint?,
                    (row["cpu_model"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_model"])) as uint?,
                    (row["cpu_stepping"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_stepping"])) as uint?,
                    (row["cpu_sse3"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_sse3"])) as uint?,
                    (row["cpu_ssse3"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_ssse3"])) as uint?,
                    (row["cpu_sse4_1"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_sse4_1"])) as uint?,
                    (row["cpu_sse4_2"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_sse4_2"])) as uint?,
                    (row["cpu_sse4_A"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_sse4_A"])) as uint?,
                    (row["cpu_sse5"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_sse5"])) as uint?,
                    (row["cpu_avx"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_avx"])) as uint?,
                    (row["cpu_avx2"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_avx2"])) as uint?,
                    (row["cpu_fma3"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_fma3"])) as uint?,
                    (row["cpu_fma4"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_fma4"])) as uint?,
                    Convert.ToString(row["cpu_code_name"] == DBNull.Value ? null : row["cpu_code_name"])
                );

            return userSystemConfiguration;
        }

        public UserSystemConfiguration GetMostRecentConfigForSystem(string systemId)
        {
            string sql = "SELECT usc.system_configuration_id, usc.system_id, usc.os_version, usc.system_name, usc.cpu_count, usc.cpu_version, usc.cpu_mhz, " +
                " usc.system_memory_bytes, usc.disk_size_bytes, usc.gpu_description, usc.gpu_display_mode_width, " +
                " usc.gpu_display_mode_height, usc.gpu_refresh_rate_hz, usc.gpu_display_mode_format, usc.dx_version, usc.net_interface_mac, " +
                " usc.net_interface_description, usc.net_interface_type, usc.net_interface_hash, usc.date_first_used, usc.date_last_used, " +
                " IF(crpl.runtime_id IS NOT NULL, 'T', 'F') AS is_running, " +
                " cpu_phys_core, cpu_vendor, cpu_brand, cpu_family, cpu_model, cpu_stepping, cpu_sse3, cpu_ssse3, " +
                " cpu_sse4_1, cpu_sse4_2, cpu_sse4_A, cpu_sse5, cpu_avx, cpu_avx2, cpu_fma3, cpu_fma4, cpu_code_name " +
                " FROM user_system_configurations usc " +
                " LEFT OUTER JOIN metrics.client_runtime_performance_log crpl ON usc.system_configuration_id = crpl.system_configuration_id " +
                " AND crpl.runtime_state = 'running' " +
                " WHERE usc.system_id = @systemId " +
                " ORDER BY usc.date_last_used DESC " +
                " LIMIT 1 ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@systemId", systemId));

            DataRow row = Db.Master.GetDataRow(sql.ToString(), parameters);

            if (row == null)
            {
                return new UserSystemConfiguration();
            }

            UserSystemConfiguration userSystemConfiguration = new UserSystemConfiguration(row["system_configuration_id"].ToString(),
                        Convert.ToString(row["system_id"] == DBNull.Value ? null : row["system_id"]),
                        (row["cpu_count"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_count"])) as uint?,
                        (row["cpu_mhz"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_mhz"])) as uint?,
                        (row["system_memory_bytes"] == DBNull.Value ? null : (object)Convert.ToUInt64(row["system_memory_bytes"])) as ulong?,
                        (row["disk_size_bytes"] == DBNull.Value ? null : (object)Convert.ToUInt64(row["disk_size_bytes"])) as ulong?,
                        (row["gpu_display_mode_width"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["gpu_display_mode_width"])) as uint?,
                        (row["gpu_display_mode_height"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["gpu_display_mode_height"])) as uint?,
                        (row["gpu_refresh_rate_hz"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["gpu_refresh_rate_hz"])) as uint?,
                        (row["gpu_display_mode_format"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["gpu_display_mode_format"])) as uint?,
                        Convert.ToString(row["dx_version"] == DBNull.Value ? null : row["dx_version"]),
                        Convert.ToString(row["cpu_version"] == DBNull.Value ? null : row["cpu_version"]),
                        Convert.ToString(row["os_version"] == DBNull.Value ? null : row["os_version"]),
                        Convert.ToString(row["system_name"] == DBNull.Value ? null : row["system_name"]),
                        Convert.ToString(row["net_interface_description"] == DBNull.Value ? null : row["net_interface_description"]),
                        Convert.ToString(row["net_interface_mac"] == DBNull.Value ? null : row["net_interface_mac"]),
                        Convert.ToString(row["net_interface_type"] == DBNull.Value ? null : row["net_interface_type"]),
                        Convert.ToString(row["net_interface_hash"] == DBNull.Value ? null : row["net_interface_hash"]),
                        Convert.ToString(row["gpu_description"] == DBNull.Value ? null : row["gpu_description"]),
                        Convert.ToDateTime(row["date_first_used"]),
                        Convert.ToDateTime(row["date_last_used"]),
                        (row["cpu_phys_core"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_phys_core"])) as uint?,
                        Convert.ToString(row["cpu_vendor"] == DBNull.Value ? null : row["cpu_vendor"]),
                        Convert.ToString(row["cpu_brand"] == DBNull.Value ? null : row["cpu_brand"]),
                        (row["cpu_family"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_family"])) as uint?,
                        (row["cpu_model"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_model"])) as uint?,
                        (row["cpu_stepping"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_stepping"])) as uint?,
                        (row["cpu_sse3"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_sse3"])) as uint?,
                        (row["cpu_ssse3"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_ssse3"])) as uint?,
                        (row["cpu_sse4_1"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_sse4_1"])) as uint?,
                        (row["cpu_sse4_2"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_sse4_2"])) as uint?,
                        (row["cpu_sse4_A"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_sse4_A"])) as uint?,
                        (row["cpu_sse5"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_sse5"])) as uint?,
                        (row["cpu_avx"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_avx"])) as uint?,
                        (row["cpu_avx2"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_avx2"])) as uint?,
                        (row["cpu_fma3"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_fma3"])) as uint?,
                        (row["cpu_fma4"] == DBNull.Value ? null : (object)Convert.ToUInt32(row["cpu_fma4"])) as uint?,
                        Convert.ToString(row["cpu_code_name"] == DBNull.Value ? null : row["cpu_code_name"])
                    );

            if (row["is_running"].ToString() == "T")
            {
                userSystemConfiguration.IsRunning = true;
            }

            return userSystemConfiguration;
        }

        public UserSystemConfiguration InsertUserSystemConfiguration(string systemId, uint? cpus, uint? cpuMhz, ulong? sysMem, ulong? diskSize, uint? gpuX, uint? gpuY,
            uint? gpuHz, uint? gpuFmt, string dxVer, string cpuVer, string sysVer, string sysName, string net, string netMAC, string netType, string netHash, string gpu, 
            uint? cpuPhysCore, string cpuVendor, string cpuBrand, uint? cpuFamily, uint? cpuModel, uint? cpuStepping,
            uint? cpuSSE3, uint? cpuSSSE3, uint? cpuSSE4_1, uint? cpuSSE4_2, uint? cpuSSE4_A, uint? cpuSSE5, uint? cpuAVX, uint? cpuAVX2, uint? cpuFMA3, uint? cpuFMA4, string cpuCodeName)
        {
            // Grab new ids
            string sysConfigId = (Db.Master.GetScalar("SELECT uuid()") ?? string.Empty).ToString();
            string validSystemId = GetValidSystemId(systemId, cpus, cpuMhz, sysMem, diskSize, gpuX, gpuY, gpuHz, gpuFmt,
                dxVer, cpuVer, sysVer, net, netMAC, netType, netHash, gpu);

            // If unable to obtain valid ids, return false
            if (string.IsNullOrWhiteSpace(validSystemId) || string.IsNullOrWhiteSpace(sysConfigId))
            {
                m_logger.Warn("MySQLUserDao.InsertUserSystemConfiguration() - Unable to obtain systemId and sysConfigId");
                return new UserSystemConfiguration();
            }

            // Grab existing system config if exists
            UserSystemConfiguration sysConfig = GetUserSystemConfiguation(validSystemId, cpus, cpuMhz, sysMem, diskSize, gpuX, gpuY, gpuHz,
                    gpuFmt, dxVer, cpuVer, sysVer, sysName, net, netMAC, netType, netHash, gpu);

            try
            {
                // Insert user system (updates if exists)
                InsertUserSystem(validSystemId, true);

                // If config does not exist, insert system and config
                if (string.IsNullOrWhiteSpace(sysConfig.SystemConfigurationId))
                {
                    string sqlInsert = " INSERT INTO user_system_configurations(system_configuration_id, system_id, cpu_count, cpu_mhz, system_memory_bytes, " +
                                    "   disk_size_bytes, gpu_display_mode_width, gpu_display_mode_height, gpu_refresh_rate_hz, gpu_display_mode_format, dx_version, " +
                                    "   cpu_version, os_version, system_name, net_interface_description, net_interface_mac, net_interface_type, net_interface_hash, gpu_description, " +
                                    "   date_first_used, date_last_used, cpu_phys_core, cpu_vendor, cpu_brand, cpu_family, cpu_model, cpu_stepping, cpu_sse3, cpu_ssse3, " +
                                    "   cpu_sse4_1, cpu_sse4_2, cpu_sse4_A, cpu_sse5, cpu_avx, cpu_avx2, cpu_fma3, cpu_fma4, cpu_code_name) " +
                                    " VALUES(@systemConfigurationId, @systemId, @cpus, @cpuMhz, @sysMem, @diskSize, @gpuX, @gpuY, @gpuHz, @gpuFmt, @dxVer, @cpuVer, @sysVer, @sysName, @net, " +
                                    "   @netMAC, @netType, @netHash, @gpu, NOW(), NOW(), @cpuPhysCore, @cpuVendor, @cpuBrand, @cpuFamily, @cpuModel, @cpuStepping, @cpuSSE3, @cpuSSSE3, " +
                                    "   @cpuSSE4_1, @cpuSSE4_2, @cpuSSE4_A, @cpuSSE5, @cpuAVX, @cpuAVX2, @cpuFMA3, @cpuFMA4, @cpuCodeName)";

                    List<IDbDataParameter> insertParameters = new List<IDbDataParameter>();
                    insertParameters.Add(new MySqlParameter("@systemConfigurationId", sysConfigId));
                    insertParameters.Add(new MySqlParameter("@systemId", validSystemId));
                    insertParameters.Add(new MySqlParameter("@cpus", (cpus == null ? DBNull.Value : (object)cpus)));
                    insertParameters.Add(new MySqlParameter("@cpuMhz", (cpuMhz == null ? DBNull.Value : (object)cpuMhz)));
                    insertParameters.Add(new MySqlParameter("@sysMem", (sysMem == null ? DBNull.Value : (object)sysMem)));
                    insertParameters.Add(new MySqlParameter("@diskSize", (diskSize == null ? DBNull.Value : (object)diskSize)));
                    insertParameters.Add(new MySqlParameter("@gpuX", (gpuX == null ? DBNull.Value : (object)gpuX)));
                    insertParameters.Add(new MySqlParameter("@gpuY", (gpuY == null ? DBNull.Value : (object)gpuY)));
                    insertParameters.Add(new MySqlParameter("@gpuHz", (gpuHz == null ? DBNull.Value : (object)gpuHz)));
                    insertParameters.Add(new MySqlParameter("@gpuFmt", (gpuFmt == null ? DBNull.Value : (object)gpuFmt)));
                    insertParameters.Add(new MySqlParameter("@dxVer", (string.IsNullOrWhiteSpace(dxVer) ? DBNull.Value : (object)dxVer)));
                    insertParameters.Add(new MySqlParameter("@cpuVer", (string.IsNullOrWhiteSpace(cpuVer) ? DBNull.Value : (object)cpuVer)));
                    insertParameters.Add(new MySqlParameter("@sysVer", (string.IsNullOrWhiteSpace(sysVer) ? DBNull.Value : (object)sysVer)));
                    insertParameters.Add(new MySqlParameter("@sysName", (string.IsNullOrWhiteSpace(sysName) ? DBNull.Value : (object)sysName)));
                    insertParameters.Add(new MySqlParameter("@net", (string.IsNullOrWhiteSpace(net) ? DBNull.Value : (object)net)));
                    insertParameters.Add(new MySqlParameter("@netMAC", (string.IsNullOrWhiteSpace(netMAC) ? DBNull.Value : (object)netMAC)));
                    insertParameters.Add(new MySqlParameter("@netType", (string.IsNullOrWhiteSpace(netType) ? DBNull.Value : (object)netType)));
                    insertParameters.Add(new MySqlParameter("@netHash", (string.IsNullOrWhiteSpace(netHash) ? DBNull.Value : (object)netHash)));
                    insertParameters.Add(new MySqlParameter("@gpu", (string.IsNullOrWhiteSpace(gpu) ? DBNull.Value : (object)gpu)));
                    // New CPU values.
                    insertParameters.Add(new MySqlParameter("@cpuPhysCore", (cpuPhysCore == null ? DBNull.Value : (object)cpuPhysCore)));
                    insertParameters.Add(new MySqlParameter("@cpuVendor", (string.IsNullOrWhiteSpace(cpuVendor) ? DBNull.Value : (object)cpuVendor)));
                    insertParameters.Add(new MySqlParameter("@cpuBrand", (string.IsNullOrWhiteSpace(cpuBrand) ? DBNull.Value : (object)cpuBrand)));
                    insertParameters.Add(new MySqlParameter("@cpuFamily", (cpuFamily == null ? DBNull.Value : (object)cpuFamily)));
                    insertParameters.Add(new MySqlParameter("@cpuModel", (cpuModel == null ? DBNull.Value : (object)cpuModel)));
                    insertParameters.Add(new MySqlParameter("@cpuStepping", (cpuStepping == null ? DBNull.Value : (object)cpuStepping)));
                    insertParameters.Add(new MySqlParameter("@cpuSSE3", (cpuSSE3 == null ? DBNull.Value : (object)cpuSSE3)));
                    insertParameters.Add(new MySqlParameter("@cpuSSSE3", (cpuSSSE3 == null ? DBNull.Value : (object)cpuSSSE3)));
                    insertParameters.Add(new MySqlParameter("@cpuSSE4_1", (cpuSSE4_1 == null ? DBNull.Value : (object)cpuSSE4_1)));
                    insertParameters.Add(new MySqlParameter("@cpuSSE4_2", (cpuSSE4_2 == null ? DBNull.Value : (object)cpuSSE4_2)));
                    insertParameters.Add(new MySqlParameter("@cpuSSE4_A", (cpuSSE4_A == null ? DBNull.Value : (object)cpuSSE4_A)));
                    insertParameters.Add(new MySqlParameter("@cpuSSE5", (cpuSSE5 == null ? DBNull.Value : (object)cpuSSE5)));
                    insertParameters.Add(new MySqlParameter("@cpuAVX", (cpuAVX == null ? DBNull.Value : (object)cpuAVX)));
                    insertParameters.Add(new MySqlParameter("@cpuAVX2", (cpuAVX2 == null ? DBNull.Value : (object)cpuAVX2)));
                    insertParameters.Add(new MySqlParameter("@cpuFMA3", (cpuFMA3 == null ? DBNull.Value : (object)cpuFMA3)));
                    insertParameters.Add(new MySqlParameter("@cpuFMA4", (cpuFMA4 == null ? DBNull.Value : (object)cpuFMA4)));
                    insertParameters.Add(new MySqlParameter("@cpuCodeName", (cpuCodeName == null ? DBNull.Value : (object)cpuCodeName)));

                    Db.Master.ExecuteNonQuery(sqlInsert, insertParameters, true);

                    sysConfig = new UserSystemConfiguration(sysConfigId, validSystemId, cpus, cpuMhz, sysMem, diskSize, gpuX, gpuY, gpuHz, gpuFmt, dxVer, 
                        cpuVer, sysVer, sysName, net, netMAC, netType, netHash, gpu, DateTime.Now, DateTime.Now, 
                        cpuPhysCore, cpuVendor, cpuBrand, cpuFamily, cpuModel, cpuStepping,
                        cpuSSE3, cpuSSSE3, cpuSSE4_1, cpuSSE4_2, cpuSSE4_A, cpuSSE5, cpuAVX, cpuAVX2, cpuFMA3, cpuFMA4, cpuCodeName);
                }
                else
                {
                    // If config already exists, just mark the date it was last used
                    string sqlUpdate = " UPDATE user_system_configurations " +
                                    " SET date_last_used = NOW(), " +
                                    " cpu_phys_core = IFNULL(@cpuPhysCore, cpu_phys_core), " +
                                    " cpu_vendor = IFNULL(@cpuVendor, cpu_vendor), " +
                                    " cpu_brand = IFNULL(@cpuBrand, cpu_brand), " +
                                    " cpu_family = IFNULL(@cpuFamily, cpu_family), " +
                                    " cpu_model = IFNULL(@cpuModel, cpu_model), " +
                                    " cpu_stepping = IFNULL(@cpuStepping, cpu_stepping), " +
                                    " cpu_sse3 = IFNULL(@cpuSSE3, cpu_sse3), " +
                                    " cpu_ssse3 = IFNULL(@cpuSSSE3, cpu_ssse3), " +
                                    " cpu_sse4_1 = IFNULL(@cpuSSE4_1, cpu_sse4_1), " +
                                    " cpu_sse4_2 = IFNULL(@cpuSSE4_2, cpu_sse4_2), " +
                                    " cpu_sse4_A = IFNULL(@cpuSSE4_A, cpu_sse4_A), " +
                                    " cpu_sse5 = IFNULL(@cpuSSE5, cpu_sse5), " +
                                    " cpu_avx = IFNULL(@cpuAVX, cpu_avx), " +
                                    " cpu_avx2 = IFNULL(@cpuAVX2, cpu_avx2), " +
                                    " cpu_fma3 = IFNULL(@cpuFMA3, cpu_fma3), " +
                                    " cpu_fma4 = IFNULL(@cpuFMA4, cpu_fma4), " +
                                    " cpu_code_name = IFNULL(@cpuCodeName, cpu_code_name) " +
                                    " WHERE system_configuration_id = @systemConfigurationId ";

                    List<IDbDataParameter> updateParameters = new List<IDbDataParameter>();
                    updateParameters.Add(new MySqlParameter("@systemConfigurationId", sysConfig.SystemConfigurationId));
                    updateParameters.Add(new MySqlParameter("@cpuPhysCore", (cpuPhysCore == null ? DBNull.Value : (object)cpuPhysCore)));
                    updateParameters.Add(new MySqlParameter("@cpuVendor", (string.IsNullOrWhiteSpace(cpuVendor) ? DBNull.Value : (object)cpuVendor)));
                    updateParameters.Add(new MySqlParameter("@cpuBrand", (string.IsNullOrWhiteSpace(cpuBrand) ? DBNull.Value : (object)cpuBrand)));
                    updateParameters.Add(new MySqlParameter("@cpuFamily", (cpuFamily == null ? DBNull.Value : (object)cpuFamily)));
                    updateParameters.Add(new MySqlParameter("@cpuModel", (cpuModel == null ? DBNull.Value : (object)cpuModel)));
                    updateParameters.Add(new MySqlParameter("@cpuStepping", (cpuStepping == null ? DBNull.Value : (object)cpuStepping)));
                    updateParameters.Add(new MySqlParameter("@cpuSSE3", (cpuSSE3 == null ? DBNull.Value : (object)cpuSSE3)));
                    updateParameters.Add(new MySqlParameter("@cpuSSSE3", (cpuSSSE3 == null ? DBNull.Value : (object)cpuSSSE3)));
                    updateParameters.Add(new MySqlParameter("@cpuSSE4_1", (cpuSSE4_1 == null ? DBNull.Value : (object)cpuSSE4_1)));
                    updateParameters.Add(new MySqlParameter("@cpuSSE4_2", (cpuSSE4_2 == null ? DBNull.Value : (object)cpuSSE4_2)));
                    updateParameters.Add(new MySqlParameter("@cpuSSE4_A", (cpuSSE4_A == null ? DBNull.Value : (object)cpuSSE4_A)));
                    updateParameters.Add(new MySqlParameter("@cpuSSE5", (cpuSSE5 == null ? DBNull.Value : (object)cpuSSE5)));
                    updateParameters.Add(new MySqlParameter("@cpuAVX", (cpuAVX == null ? DBNull.Value : (object)cpuAVX)));
                    updateParameters.Add(new MySqlParameter("@cpuAVX2", (cpuAVX2 == null ? DBNull.Value : (object)cpuAVX2)));
                    updateParameters.Add(new MySqlParameter("@cpuFMA3", (cpuFMA3 == null ? DBNull.Value : (object)cpuFMA3)));
                    updateParameters.Add(new MySqlParameter("@cpuFMA4", (cpuFMA4 == null ? DBNull.Value : (object)cpuFMA4)));
                    updateParameters.Add(new MySqlParameter("@cpuCodeName", (cpuCodeName == null ? DBNull.Value : (object)cpuCodeName)));

                    Db.Master.ExecuteNonQuery(sqlUpdate, updateParameters, true);

                    // sysConfig already valid, just need to set date
                    sysConfig.DateLastUsed = DateTime.Now;
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLUserDao.InsertUserSystemConfiguration(): " + e.ToString() +
                    "@systemConfigurationId: " + sysConfigId + "\n" +
                    "@systemId: " + validSystemId + "\n" +
                    "@cpus: " + (cpus == null ? "NULL" : cpus.ToString()) + "\n" +
                    "@cpuMhz: " + (cpuMhz == null ? "NULL" : cpuMhz.ToString()) + "\n" +
                    "@sysMem: " + (sysMem == null ? "NULL" : sysMem.ToString()) + "\n" +
                    "@diskSize: " + (diskSize == null ? "NULL" : diskSize.ToString()) + "\n" +
                    "@gpuX: " + (gpuX == null ? "NULL" : gpuX.ToString()) + "\n" +
                    "@gpuY: " + (gpuY == null ? "NULL" : gpuY.ToString()) + "\n" +
                    "@gpuHz: " + (gpuHz == null ? "NULL" : gpuHz.ToString()) + "\n" +
                    "@gpuFmt: " + (gpuFmt == null ? "NULL" : gpuFmt.ToString()) + "\n" +
                    "@dxVer: " + (string.IsNullOrWhiteSpace(dxVer) ? "NULL" : dxVer.ToString()) + "\n" +
                    "@cpuVer: " + (string.IsNullOrWhiteSpace(cpuVer) ? "NULL" : cpuVer.ToString()) + "\n" +
                    "@sysVer: " + (string.IsNullOrWhiteSpace(sysVer) ? "NULL" : sysVer.ToString()) + "\n" +
                    "@sysName: " + (string.IsNullOrWhiteSpace(sysName) ? "NULL" : sysName.ToString()) + "\n" +
                    "@net: " + (string.IsNullOrWhiteSpace(net) ? "NULL" : net.ToString()) + "\n" +
                    "@netMAC: " + (string.IsNullOrWhiteSpace(netMAC) ? "NULL" : netMAC.ToString()) + "\n" +
                    "@netType: " + (string.IsNullOrWhiteSpace(netType) ? "NULL" : netType.ToString()) + "\n" +
                    "@netHash: " + (string.IsNullOrWhiteSpace(netHash) ? "NULL" : netHash.ToString()) + "\n" +
                    "@gpu: " + (string.IsNullOrWhiteSpace(gpu) ? "NULL" : gpu.ToString()) + "\n");
                sysConfig = new UserSystemConfiguration();
            }

            return sysConfig;
        }

        public int UpdateSystemConfigurationLastUserId(string runtimeId)
        {
            int ret = 0;

            string selectSql = " SELECT user_id, system_configuration_id " +
                               " FROM metrics.client_runtime_performance_log " +
                               " WHERE runtime_id = @runtimeId ";

            List<IDbDataParameter> selectParameters = new List<IDbDataParameter>();
            selectParameters.Add(new MySqlParameter("@runtimeId", runtimeId));

            DataRow row = Db.Master.GetDataRow(selectSql.ToString(), selectParameters);

            if (row != null)
            {
                int userId = (row["user_id"] == DBNull.Value ? 0 : (int)row["user_id"]);
                string systemConfigurationId = (row["system_configuration_id"] == DBNull.Value ? string.Empty : row["system_configuration_id"].ToString());

                if (userId > 0 && !string.IsNullOrWhiteSpace(systemConfigurationId))
                {
                    string sqlUpdate =  " UPDATE user_system_configurations " +
                                        " SET user_id_last_used = @userId " +
                                        " WHERE system_configuration_id = @systemConfigurationId ";

                    List<IDbDataParameter> updateParameters = new List<IDbDataParameter>();
                    updateParameters.Add(new MySqlParameter("@userId", userId));
                    updateParameters.Add(new MySqlParameter("@systemConfigurationId", systemConfigurationId));

                    ret = Db.Master.ExecuteNonQuery(sqlUpdate, updateParameters);
                }
            }

            return ret;
        }

        public int InsertUserAgentData (int userId, string os, string browser, string useragent)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string sql = "INSERT INTO user_useragent_data (user_id, os, browser, user_agent) " +
                         "VALUES ( @userId, @os, @browser, @useragent )";

            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@os", os));
            parameters.Add (new MySqlParameter ("@browser", browser));
            parameters.Add (new MySqlParameter ("@useragent", useragent));

            return Db.Master.ExecuteNonQuery (sql, parameters);
        }

        #endregion

        #region Suspensions

        /// <summary>
        /// Retrieves all suspensions associated with the specified user id, optionally returning only
        /// currently active suspensions.
        /// </summary>
        public List<UserSuspension> GetUserSuspensions(int userId, bool activeOnly = false)
        {
            Dictionary<int, List<UserSuspension>> suspensionMap = GetUserSuspensions(new int[] { userId }, null, activeOnly);

            List<UserSuspension> suspensions;
            if (!suspensionMap.TryGetValue(userId, out suspensions))
            {
                suspensions = new List<UserSuspension>();
            }

            return suspensions;
        }

        /// <summary>
        /// Retrieves all suspensions associated with the specified user id, optionally returning only
        /// currently active suspensions.
        /// </summary>
        public Dictionary<int, List<UserSuspension>> GetUserSuspensions(IEnumerable<int> userIds, bool activeOnly = false)
        {
            return GetUserSuspensions(userIds, null, activeOnly);
        }

        /// <summary>
        /// Retrieves a mapping of user ids to suspensions associated with the specified user system id, optionally returning only
        /// currently active suspensions.
        /// </summary>
        public Dictionary<int, List<UserSuspension>> GetUserSuspensionsBySystemBanId(int systemBanId, bool activeOnly = false)
        {
            return GetUserSuspensions(null, new int[] { systemBanId }, activeOnly);
        }

        /// <summary>
        /// Helper method used to retrieve a mapping of user ids to suspensions.
        /// </summary>
        private Dictionary<int, List<UserSuspension>> GetUserSuspensions(IEnumerable<int> userIds, IEnumerable<int> systemBanIds, bool activeOnly)
        {
            Dictionary<int, List<UserSuspension>> suspensions = new Dictionary<int, List<UserSuspension>>();

            // Make sure filter inputs are valid.
            userIds = (userIds ?? new int[0]);
            systemBanIds = (systemBanIds ?? new int[0]);

            if (userIds.Count() == 0 && systemBanIds.Count() == 0)
            {
                return suspensions;
            }

            string sqlSelect = "SELECT ban_id, user_id, ban_date_start, ban_date_end, system_ban_id " +
                               "FROM user_suspensions " +
                               "WHERE 1 = 1 " +
                               (userIds.Count() > 0 ? "AND user_id IN (" + string.Join(", ", userIds) + ") " : "") +
                               (systemBanIds.Count() > 0 ? "AND system_ban_id IN (" + string.Join(", ", systemBanIds) + ") " : "") +
                               (activeOnly ? " AND (ban_date_end IS NULL OR ban_date_end > NOW()) " : "");

            DataTable dt = Db.Master.GetDataTable(sqlSelect);

            if (dt == null || dt.Rows.Count == 0)
            {
                return suspensions;
            }
           
            foreach (DataRow row in dt.Rows)
            {
                List<UserSuspension> susOut;
                int userId = Convert.ToInt32(row["user_id"]);

                if (!suspensions.TryGetValue(userId, out susOut))
                {
                    susOut = new List<UserSuspension>();
                    suspensions.Add(userId, susOut);
                }

                susOut.Add(new UserSuspension(Convert.ToInt32(row["ban_id"]), userId, Convert.ToDateTime(row["ban_date_start"]),
                        (DateTime?)(row["ban_date_end"] == DBNull.Value ? null : row["ban_date_end"]),
                        (row["system_ban_id"] == DBNull.Value ? null : (int?)Convert.ToInt32(row["system_ban_id"]))));
            }

            return suspensions;
        }

        /// <summary>
        /// Save changes to a UserSuspension, banning the user as appropriate.
        /// </summary>
        public int SaveUserSuspension(int userCurrentStatus, UserSuspension suspension, int banningUserId, string banNote, bool propagateTransExceptions = false)
        {
            int recordsAffected = 0;

            string sqlString = " INSERT INTO user_suspensions (ban_id, user_id, ban_date_start, ban_date_end, system_ban_id) " +
                               " VALUES (@banId, @userId, @banStartDate, @banEndDate, @systemBanId) " +
                               " ON DUPLICATE KEY UPDATE ban_date_start = @banStartDate, ban_date_end = @banEndDate; ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            // Passing null for ban_id will cause the auto-increment to kick in and insert a new record.
            parameters.Add(new MySqlParameter("@banId", (suspension.BanId == 0 ? (object)DBNull.Value : suspension.BanId)));
            parameters.Add(new MySqlParameter("@userId", suspension.UserId));
            parameters.Add(new MySqlParameter("@banStartDate", suspension.BanStartDate));
            parameters.Add(new MySqlParameter("@banEndDate", (suspension.BanEndDate ?? (object)DBNull.Value)));
            parameters.Add(new MySqlParameter("@systemBanId", (suspension.SystemBanId ?? (object)DBNull.Value)));

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    // Set user status to banned if the suspension is active.
                    // No need to adjust user status if suspension is inactive. UsersUtility.Authorize() handles this
                    // upon login.
                    if (suspension.IsPermanent || suspension.BanEndDate > DateTime.Now)
                    {
                        if (userCurrentStatus == (int)User.eUSER_STATUS.REGVALIDATED)
                        {
                            UpdateUserStatus(suspension.UserId, (int)User.eUSER_STATUS.LOCKEDVALIDATED, true);
                        }
                        else if (userCurrentStatus == (int)User.eUSER_STATUS.REGNOTVALIDATED)
                        {
                            UpdateUserStatus(suspension.UserId, (int)User.eUSER_STATUS.LOCKED, true);
                        }
                    }

                    recordsAffected = Db.Master.ExecuteNonQuery(sqlString, parameters, true);

                    // Insert a note recording the action that was taken.
                    InsertUserNote(suspension.UserId, banningUserId, banNote);

                    transaction.Complete();
                }
            }
            catch(Exception e)
            {
                recordsAffected = 0;
                m_logger.Error("MySQLUserDao.SaveUserSuspension() - Error saving user suspension.", e);

                if (propagateTransExceptions)
                {
                    throw e;
                }
            }

            return recordsAffected;
        }

        /// <summary>
        /// Deletes a user suspension from the database. 
        /// </summary>
        public int DeleteUserSuspension(int banId, int userId, bool propagateTransExceptions = false)
        {
            int recordsAffected = 0;
            List<UserSuspension> activeSuspensions;
            bool isActiveBan;
            bool resetUserStatus;
            int userStatusId;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                // Grab the user's currently active suspensions. If the suspension we're deleting is active and there are
                // no other active suspensions, the user should be reinstated.
                activeSuspensions = GetUserSuspensions(userId, true);
                isActiveBan = activeSuspensions.Where(s => s.BanId == banId).Count() > 0;
                resetUserStatus = isActiveBan && activeSuspensions.Count == 1;
                userStatusId = GetUser(userId).StatusId;
            }

            string sql = " DELETE FROM user_suspensions WHERE ban_id = @banId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@banId", banId));

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    recordsAffected = Db.Master.ExecuteNonQuery(sql, parameters, true);

                    // If the user should be unbanned, move them from a locked state to an unlocked state.
                    if (resetUserStatus)
                    {
                        if (userStatusId == (int)User.eUSER_STATUS.LOCKEDVALIDATED)
                        {
                            UpdateUserStatus(userId, (int)User.eUSER_STATUS.REGVALIDATED, true);
                        }
                        else if (userStatusId == (int)User.eUSER_STATUS.LOCKED)
                        {
                            UpdateUserStatus(userId, (int)User.eUSER_STATUS.REGNOTVALIDATED, true);
                        }
                    }

                    transaction.Complete();
                }
            }
            catch(Exception e)
            {
                recordsAffected = 0;
                m_logger.Error("MySQLUserDao.DeleteUserSuspension: Error deleting user suspension.", e);

                if (propagateTransExceptions)
                {
                    throw e;
                }
            }

            return recordsAffected;
        }

        /// <summary>
        /// Deletes a user system suspension from the database. 
        /// </summary>
        public int DeleteUserSystemSuspension(int banId)
        {
            int recordsAffected = 0;

            Dictionary<int, List<UserSuspension>> userSuspensions = GetUserSuspensionsBySystemBanId(banId);

            string sql = " DELETE FROM user_system_suspensions WHERE system_ban_id = @banId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@banId", banId));

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    // Delete all user suspensions that are associated with this system suspension.
                    foreach(KeyValuePair<int, List<UserSuspension>> kvp in userSuspensions)
                    {
                        foreach(UserSuspension userSuspension in kvp.Value)
                        {
                            DeleteUserSuspension(userSuspension.BanId, userSuspension.UserId, true);
                        }
                    }

                    recordsAffected = Db.Master.ExecuteNonQuery(sql, parameters, true);
                    transaction.Complete();
                }
            }
            catch (Exception e)
            {
                recordsAffected = 0;
                m_logger.Error("MySQLUserDao.DeleteUserSystemSuspension: Error deleting user system suspension.", e);
            }

            return recordsAffected;
        }

        /// <summary>
        /// Retrieves a list of user system suspensions for a given system, optionally limited
        /// to active suspensions only.
        /// </summary>
        public List<UserSystemSuspension> GetUserSystemSuspensions(string systemId, bool activeOnly = false)
        {
            List<UserSystemSuspension> suspensions = new List<UserSystemSuspension>();

            if (string.IsNullOrWhiteSpace(systemId))
            {
                return suspensions;
            }

            string sql = "SELECT system_ban_id, system_id, start_date, end_date " +
                         "FROM user_system_suspensions " +
                         "WHERE system_id = @systemId " +
                         (activeOnly ? "AND (end_date IS NULL OR end_date > NOW()) " : "");

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@systemId", systemId));

            DataTable dt = Db.Master.GetDataTable(sql, parameters);

            if (dt == null)
            {
                return suspensions;
            }

            foreach (DataRow row in dt.Rows)
            {
                suspensions.Add(new UserSystemSuspension(Convert.ToInt32(row["system_ban_id"]), row["system_id"].ToString(), Convert.ToDateTime(row["start_date"]),
                    (row["end_date"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(row["end_date"]))));
            }

            return suspensions;
        }

        /// <summary>
        /// Saves a user system suspension
        /// </summary>
        public int SaveUserSystemSuspension(UserSystemSuspension systemSuspension, int banningUserId)
        {
            int recordsAffected = 0;

            string userBanNote = (systemSuspension.BanEndDate > DateTime.Now || systemSuspension.IsPermanent ? 
                "User suspended due to suspension for system id: " + systemSuspension.SystemId :
                "User suspension modified due to suspension for system id: " + systemSuspension.SystemId);

            // Grab the users associated with the system and any suspensions tied to the system ban.
            List<User> systemUsers = GetSystemUsers(systemSuspension.SystemId);
            Dictionary<int, List<UserSuspension>> existingUserSuspensions = GetUserSuspensionsBySystemBanId(systemSuspension.SystemBanId);

            // Prepare statement to insert/update the user_system_suspensions record.
            string sql = "INSERT INTO user_system_suspensions (system_ban_id, system_id, start_date, end_date) " +
                         "VALUES (@systemBanId, @systemId, @startDate, @endDate) " +
                         "ON DUPLICATE KEY UPDATE start_date = @startDate, end_date = @endDate; ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            // Passing null for the PK will cause auto-increment to kick in and insert a new record.
            parameters.Add(new MySqlParameter("@systemBanId", systemSuspension.SystemBanId == 0 ? (object)DBNull.Value : systemSuspension.SystemBanId));
            parameters.Add(new MySqlParameter("@systemId", systemSuspension.SystemId));
            parameters.Add(new MySqlParameter("@startDate", systemSuspension.BanStartDate));
            parameters.Add(new MySqlParameter("@endDate", (systemSuspension.BanEndDate == null ? (object)DBNull.Value : systemSuspension.BanEndDate)));

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    // Save the user_system_suspensions record.
                    if (systemSuspension.SystemBanId == 0)
                    {
                        systemSuspension.SystemBanId = Db.Master.ExecuteIdentityInsert(sql, parameters, true);
                        recordsAffected++;
                    }
                    else
                    {
                        recordsAffected += Db.Master.ExecuteNonQuery(sql, parameters, true);
                    }

                    // Insert update suspension records for system user.
                    foreach (User user in systemUsers)
                    {
                        List<UserSuspension> userSuspensions;
                        if (!existingUserSuspensions.TryGetValue(user.UserId, out userSuspensions))
                        {
                            userSuspensions = new List<UserSuspension>();
                            userSuspensions.Add(new UserSuspension(0, user.UserId, systemSuspension.BanStartDate, systemSuspension.BanEndDate, (int?)systemSuspension.SystemBanId));
                        }

                        foreach(UserSuspension userSuspension in userSuspensions)
                        {
                            userSuspension.BanStartDate = systemSuspension.BanStartDate;
                            userSuspension.BanEndDate = systemSuspension.BanEndDate;

                            recordsAffected += SaveUserSuspension(user.StatusId, userSuspension, banningUserId, userBanNote, true);
                        }
                    }

                    transaction.Complete();
                }
            }
            catch(Exception e)
            {
                recordsAffected = 0;
                m_logger.Error("MySQLUserDao.SaveUserSystemSuspension() - Error saving system suspension.", e);
            }

            return recordsAffected;
        }

        #endregion

        /// <summary>
        /// Returns world info about the World the user is currently in on client (if any).
        /// </summary>
        public DataRow GetUserLoggedInWorld(int userId)
        {
            // Check if the user is in a 3D App first
            string sql = " SELECT al.game_id, " +
                         "      IF(wok.is_wok_game(al.game_id) = 1, pz.current_zone_index, 0) AS zone_index, " +
	                     "      IF(wok.is_wok_game(al.game_id) = 1, pz.current_zone_instance_id, 0) AS zone_instance_id, " +
	                     "      IF(wok.is_wok_game(al.game_id) = 1, pz.current_zone_type, 0) AS zone_type " +
                         "  FROM wok.players p " +
                         "  INNER JOIN wok.player_zones pz ON pz.player_id = p.player_id " +
                         "  INNER JOIN developer.active_logins al ON al.user_id = p.kaneva_user_id " +
                         "  WHERE p.kaneva_user_id = @userId " +
	                     "      AND ((wok.is_wok_game(al.game_id) = 1 AND p.in_game = 'T' AND pz.server_id <> 0) OR " +
		                 "           (wok.is_wok_game(al.game_id) = 0 AND p.in_game = 'F')) ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            
            return Db.WOK.GetDataRow(sql, parameters);
        }

        public bool HasUserCompletedAvatarSelect(int wokPlayerId)
        {
            string sqlSelect = "CALL is_avatar_template_applied( @wokPlayerId, @ret ); SELECT @ret";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@wokPlayerId", wokPlayerId));

            return (Convert.ToInt32(Db.WOK.GetScalar(sqlSelect, parameters)) == 1);
        }

        /// <summary>
        /// Get awards data for inviting a user
        /// </summary>
        /// <returns></returns>
        public DataRow GetInvitePointAwards (int invitePointId)
        {
            // Get Invite Point awards
            string sqlSelect = "SELECT invite_point_id, point_award_amount, kei_point_id, ip_limit, award_multiple, description " +
                " FROM invite_point_awards " +
                " WHERE invite_point_id = @invitePointId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@invitePointId", invitePointId));

            return Db.Master.GetDataRow (sqlSelect.ToString (), parameters);
        }

        public UserMetaGameItemBalances GetUserMetaGameItemBalances(int userId)
        {
            UserMetaGameItemBalances balances = new UserMetaGameItemBalances(userId);

            // Get Invite Point awards
            string sqlSelect = "SELECT meta_game_item_name, balance " +
                " FROM user_meta_game_item_balances " +
                " WHERE user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            DataTable dt = Db.Master.GetDataTable(sqlSelect, parameters);

            if (dt == null || dt.Rows.Count == 0)
            {
                return balances;
            }

            foreach (DataRow row in dt.Rows)
            {
                balances.SetMetaGameItemBalance(row["meta_game_item_name"].ToString(), Convert.ToInt32(row["balance"]));
            }

            return balances;
        }

        public bool SaveUserMetaGameItemBalances(UserMetaGameItemBalances balances, bool propagateTransExceptions = false)
        {
            bool success = false;

            if (balances == null || balances.Count() == 0)
            {
                return false;
            }

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    int updateCounter = 0;
                    string sqlUpdate = string.Empty;
                    
                    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                    parameters.Add(new MySqlParameter("@userId", balances.UserId));

                    foreach (KeyValuePair<string, int> balance in balances)
                    {
                        updateCounter++;
                        sqlUpdate += " INSERT INTO user_meta_game_item_balances VALUES" +
                            " (@userId, @itemName" + updateCounter + ", @balance" + updateCounter + ")"+
                            " ON DUPLICATE KEY UPDATE balance = @balance" + updateCounter + ";";
                        parameters.Add(new MySqlParameter("@itemName" + updateCounter, balance.Key));
                        parameters.Add(new MySqlParameter("@balance" + updateCounter, balance.Value));
                    }

                    Db.Master.ExecuteNonQuery(sqlUpdate, parameters, true);

                    // If we reach this point, the transaction was successful
                    transaction.Complete();
                    success = true;
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLUserDao.SaveUserMetaGameItemBalances() - Transaction aborted: " + e.ToString());

                if (propagateTransExceptions)
                    throw e;
            }

            return success;
        }

        public int GetUserTotalMGIConversions(int userId, string itemType, DateTime startDate, DateTime endDate, string currencyType = null)
        {
            // Get Invite Point awards
            string sqlSelect = "SELECT COALESCE(SUM(num_items_converted),0) AS daily_total " +
                " FROM meta_game_item_conversions " +
                " WHERE user_id = @userId " +
                " AND conversion_date BETWEEN @start AND @end " +
                " AND item_type = @itemType ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@start", startDate));
            parameters.Add (new MySqlParameter ("@end", endDate));
            parameters.Add (new MySqlParameter ("@itemType", itemType));

            if (!string.IsNullOrWhiteSpace(currencyType))
            {
                sqlSelect += " AND kei_point_id = @currencyType ";
                parameters.Add(new MySqlParameter("@currencyType", currencyType));
            }

            DataRow row = Db.Master.GetDataRow (sqlSelect, parameters);

            if (row == null)
            {
                return 0;
            }
            return Convert.ToInt32(row["daily_total"]);
        }

        public int GetUserTotalMGIConversionAmount(int userId, string itemType, DateTime startDate, DateTime endDate, string currencyType = null)
        {
            // Get Invite Point awards
            string sqlSelect = "SELECT COALESCE(SUM(currency_amount_received),0) AS daily_total " +
                " FROM meta_game_item_conversions " +
                " WHERE user_id = @userId " +
                " AND conversion_date BETWEEN @start AND @end " +
                " AND item_type = @itemType ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@start", startDate));
            parameters.Add(new MySqlParameter("@end", endDate));
            parameters.Add(new MySqlParameter("@itemType", itemType));

            if (!string.IsNullOrWhiteSpace(currencyType))
            {
                sqlSelect += " AND kei_point_id = @currencyType ";
                parameters.Add(new MySqlParameter("@currencyType", currencyType));
            }

            DataRow row = Db.Master.GetDataRow(sqlSelect, parameters);

            if (row == null)
            {
                return 0;
            }
            return Convert.ToInt32(row["daily_total"]);
        }

        private int AddMetaGameItemConversion(int userId, string itemType, string currencyType, int numItemsConverted, int currencyAmountReceived, int wokTransId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string sql = "INSERT INTO meta_game_item_conversions (conversion_id, user_id, item_type, kei_point_id, num_items_converted, currency_amount_received, conversion_date, trans_id) " +
                         "VALUES (uuid(), @userId, @itemType, @currencyType, @numItemsConverted, @currencyAmountReceived, NOW(), @wokTransId)";

            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@itemType", itemType));
            parameters.Add (new MySqlParameter ("@currencyType", currencyType));
            parameters.Add (new MySqlParameter ("@numItemsConverted", numItemsConverted));
            parameters.Add (new MySqlParameter ("@currencyAmountReceived", currencyAmountReceived));
            parameters.Add (new MySqlParameter ("@wokTransId", wokTransId));

            return Db.Master.ExecuteNonQuery (sql, parameters);
        }

        public bool ConvertMetaGameItemToCurrency(int userId, string itemType, string currencyType, int numItemsConverted, int currencyAmountReceived,
            UserMetaGameItemBalances metaGameItemBalances, bool propagateTransExceptions = false)
        {
            bool success = false;

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator ())
                {
                    // Create a transaction for giving user rewards
                    int wokTransactionLogId = 0;
                    int successfulPayment = -1;

                    successfulPayment = AdjustUserBalance(userId, currencyType, currencyAmountReceived, (ushort)TransactionType.eTRANSACTION_TYPES.CASH_TT_GAMING, ref wokTransactionLogId);

                    if (successfulPayment == 0 && wokTransactionLogId > 0) //0 is success according to AdjustUserBalance notes
                    {
                        // Add record to meta_game_item_conversions
                        int result = AddMetaGameItemConversion (userId, itemType, currencyType, numItemsConverted, currencyAmountReceived, wokTransactionLogId);

                        if (result < 1) // Error
                        {
                            throw new TransactionAbortedException(string.Format("Failed to insert meta game item conversion. userId = {0}, itemType = {1}, currencyType = {2}, numItemsConverted = {3}, currencyAmountReceived = {4}, wokTransId = {5}",
                                userId, itemType, currencyType, numItemsConverted, currencyAmountReceived, wokTransactionLogId));
                        }

                        // Save the user's meta game item balances
                        if (metaGameItemBalances != null && metaGameItemBalances.Count() > 0)
                        {
                            SaveUserMetaGameItemBalances(metaGameItemBalances, true);
                        }
                    }
                    else
                    {
                        throw new TransactionAbortedException(string.Format("Failed to create meta game item conversion transaction calling AdjustUserBalance(). userId = {0}, itemType = {1}, currencyType = {2}, numItemsConverted = {3}, currencyAmountReceived = {4}, wokTransId = {5}",
                            userId, itemType, currencyType, numItemsConverted, currencyAmountReceived, wokTransactionLogId));
                    }

                    // If we reach this point, the transaction was successful
                    transaction.Complete ();
                    success = true;
                }
            }
            catch (Exception e)
            {
                m_logger.Error ("MySQLUserDao.ConvertMetaGameItemToCurrency() - Transaction aborted: " + e.ToString());

                if (propagateTransExceptions)
                    throw e;
            }

            return success;
        }

        private int AddMetaGameItemPurchase(int userId, string itemType, uint numItemsSpent, uint rewardsValue, int wokTransId,
            int zoneInstanceId, int zoneType, string gameItemName, string gameItemType, bool propagateTransExceptions = false)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sql = "INSERT INTO meta_game_item_purchases (user_id, item_type, num_items_spent, rewards_value, purchase_date, " +
                         "zone_instance_id, zone_type, game_item_name, game_item_type, royalty_trans_id) " +
                         "VALUES (@userId, @itemType, @numItemsSpent, @rewardsValue, NOW(), " +
                         "@zoneInstanceId, @zoneType, @gameItemName, @gameItemType, @wokTransId)";

            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@itemType", itemType));
            parameters.Add(new MySqlParameter("@numItemsSpent", numItemsSpent));
            parameters.Add(new MySqlParameter("@rewardsValue", rewardsValue));
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));
            parameters.Add(new MySqlParameter("@gameItemName", gameItemName));
            parameters.Add(new MySqlParameter("@gameItemType", gameItemType));
            parameters.Add(new MySqlParameter("@wokTransId", wokTransId > 0 ? wokTransId : (object)DBNull.Value));

            return Db.Master.ExecuteNonQuery(sql, parameters, propagateTransExceptions);
        }

        private int UpdateUserMetaGameItemRoyalties(UserMetaGameItemRoyalties royalties, bool propagateTransExceptions = false)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sql = "INSERT INTO meta_game_item_royalties (user_id, item_type, items_spent_in_owned_worlds, rewards_received, unconverted_balance) " +
                         "VALUES (@userId, @itemType, @itemsSpentInOwnedWorlds, @rewardsReceived, @unconvertedBalance) " +
                         "ON DUPLICATE KEY UPDATE items_spent_in_owned_worlds = @itemsSpentInOwnedWorlds, rewards_received = @rewardsReceived, unconverted_balance = @unconvertedBalance";

            parameters.Add(new MySqlParameter("@userId", royalties.UserId));
            parameters.Add(new MySqlParameter("@itemType", royalties.ItemType));
            parameters.Add(new MySqlParameter("@itemsSpentInOwnedWorlds", royalties.ItemsSpentInOwnedWorlds));
            parameters.Add(new MySqlParameter("@rewardsReceived", royalties.RewardsReceived));
            parameters.Add(new MySqlParameter("@unconvertedBalance", royalties.UnconvertedBalance));

            return Db.Master.ExecuteNonQuery(sql, parameters, propagateTransExceptions);
        }

        public UserMetaGameItemRoyalties GetUserMetaGameItemRoyalties(int userId, string itemType)
        {
            UserMetaGameItemRoyalties royalties;

            string sql = "SELECT user_id, item_type, items_spent_in_owned_worlds, rewards_received, unconverted_balance " +
                         "FROM meta_game_item_royalties " +
                         "WHERE user_id = @userId " +
                         "AND item_type = @itemType ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@itemType", itemType));

            DataRow row = Db.Master.GetDataRow(sql, parameters);
            if (row != null)
                royalties = new UserMetaGameItemRoyalties(userId, itemType, Convert.ToUInt32(row["items_spent_in_owned_worlds"]),
                    Convert.ToUInt32(row["rewards_received"]), Convert.ToDecimal(row["unconverted_balance"]));
            else
                royalties = new UserMetaGameItemRoyalties(userId, itemType, 0, 0, 0);

            return royalties;
        }

        public bool SpendMetaGameItems(int userId, string itemType, uint numItemsSpent, uint totalRewardsValue,
            int zoneInstanceId, int zoneType, string gameItemName, string gameItemType,
            int worldOwnerId, UserMetaGameItemBalances metaGameItemBalances, bool propagateTransExceptions = false)
        {
            bool success = false;
            string errorFormat = "{0} Params: userId: {1}, itemType: {2}, numItemsSpent: {3}, totalRewardsValue: {4}, zoneInstanceId: {5}, zoneType: {6}, " +
                "gameItemName: {7}, gameItemType: {8}, worldOwnerId: {9}.";

            try
            {
                using (TransactionDecorator transaction = new TransactionDecorator())
                {
                    // Grab the owner's current royalty value.
                    UserMetaGameItemRoyalties royalties = GetUserMetaGameItemRoyalties(worldOwnerId, itemType);
                    uint payout = royalties.AddRoyalty(numItemsSpent, totalRewardsValue);
                    int royaltyTransactionLogId = 0;

                    if (payout > 0)
                    {
                        // Create a transaction to pay the world owner.             
                        int successfulPayment = AdjustUserBalance(worldOwnerId, Currency.CurrencyType.REWARDS, payout, 
                            (ushort)TransactionType.eTRANSACTION_TYPES.CASH_TT_GAMING, ref royaltyTransactionLogId);

                        if (successfulPayment != 0 || royaltyTransactionLogId <= 0)
                            throw new TransactionAbortedException(string.Format(errorFormat, "Failed to pay royalty to world owner.", 
                                userId, itemType, numItemsSpent, totalRewardsValue, zoneInstanceId, zoneType, gameItemName,
                                gameItemType, worldOwnerId));
                    }

                    // Update the world owner's royalty status.
                    UpdateUserMetaGameItemRoyalties(royalties, true);

                    // Log the purchase.
                    AddMetaGameItemPurchase(userId, itemType, numItemsSpent, totalRewardsValue, royaltyTransactionLogId, zoneInstanceId, zoneType,
                        gameItemName, gameItemType, true);

                    // Save the user's meta game item balances
                    SaveUserMetaGameItemBalances(metaGameItemBalances, true);

                    // If we reach this point, the transaction was successful
                    transaction.Complete();
                    success = true;
                }
            }
            catch (Exception e)
            {
                m_logger.Error("MySQLUserDao.SpendMetaGameItems() - Transaction aborted: " + e.ToString());

                if (propagateTransExceptions)
                    throw e;
            }

            return success;
        }

        /// <summary>
        /// InsertUserNotificationProfile
        /// </summary>
        public int InsertUserNotificationProfile (NotificationsProfile notifcationsProfile)
        {   
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sql = "INSERT INTO notifications_profile " +
                "(user_id, notification_type, xml_data, notif_text, param1, param2, param3, param4, added_datetime" +
                ") VALUES (" +
                "@user_id, @notification_type, @xml_data, @notif_text,@param1, @param2, @param3, @param4, NOW())";

            parameters.Add(new MySqlParameter("@user_id", notifcationsProfile.UserId));
            parameters.Add(new MySqlParameter("@notification_type", (int) notifcationsProfile.NotificationType));
            parameters.Add(new MySqlParameter("@xml_data", notifcationsProfile.XMLData));
            parameters.Add(new MySqlParameter("@param1", notifcationsProfile.Param1));
            parameters.Add(new MySqlParameter("@param2", notifcationsProfile.Param2));
            parameters.Add(new MySqlParameter("@param3", notifcationsProfile.Param3));
            parameters.Add(new MySqlParameter("@param4", notifcationsProfile.Param4));

            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        public List<NotificationsProfile> GetNotificationsProfile(int userId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

//3.4.3.1
//Will always show:
//Level ups (also show popup)
//Badges (even if clicked to see the badge - what was earned etc.
//XP earned
//Rewards earned
//Credits received from <username> (new - go with the player trading project)
 
//3.4.3.2
//Will show, but will be removed after being appeased (or after 60 days):
//Friend Requests (also show popup)
//Rave & Rave Back (also show popup)

//All standard fame tips, until appeased  (user_dismissed)
//3.4.3.3
//Will never show:
//Gaming notifications
//An event you were invited
 
            string strQuery = "SELECT idnotifications_profile, user_id, notification_type, xml_data, notif_text, param1, param2, param3, param4, added_datetime " +
                     " FROM kaneva.notifications_profile np " +
                     " WHERE np.user_id = @userId " +
                     " and notification_type IN (12, 13, 16, 6) " +
                     " and user_dismissed = 0 " +
                " UNION " +
                    " SELECT idnotifications_profile, user_id, notification_type, xml_data, notif_text, param1, param2, param3, param4, added_datetime " +
                     " FROM kaneva.notifications_profile np " +
                     " WHERE np.user_id = @userId " +
                     " and notification_type IN (1,2,14) " +
                     " and added_datetime > DATE_ADD(NOW(), INTERVAL -30 DAY) " +
                     " and user_dismissed = 0 " +
                 " ORDER BY idnotifications_profile DESC ";

            // To do, split out friend request so only active

            parameters.Add(new MySqlParameter("@userId", userId));
            DataTable dt = Db.Fame.GetDataTable(strQuery, parameters);

            List<NotificationsProfile> list = new List<NotificationsProfile>();

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new NotificationsProfile(Convert.ToUInt32(row["idnotifications_profile"]), Convert.ToInt32(row["user_id"]), Convert.ToInt32(row["notification_type"]), row["xml_data"].ToString(), row["notif_text"].ToString()
                ));
            }

            return list;
        }

        /// <summary>
        /// DismissNotficationProfile
        /// </summary>
        public int DismissNotficationProfile(int userId, uint idnotifications_profile)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            string sql = "UPDATE notifications_profile " +
                " SET user_dismissed = 1 " +
                " WHERE idnotifications_profile =  @idnotifications_profile and user_id = @user_id ";

            parameters.Add(new MySqlParameter("@user_id", idnotifications_profile));
            parameters.Add(new MySqlParameter("@idnotifications_profile", userId));


            return Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Insert a user note
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public void InsertUserNote(int userId, int createdUserId, string note)
        {
            InsertUserNote(userId, createdUserId, note, 0);
        }

        public void InsertUserNote(int userId, int createdUserId, string note, int wokTransLogId)
        {
            // Send the message
            string sql = "INSERT INTO user_notes " +
                "(user_id, note, created_datetime, created_user_id, wok_transaction_log_id " +
                ") VALUES (" +
                "@userId, @note, NOW(), @createdUserId, @wokTransLogId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@note", note));
            parameters.Add(new MySqlParameter("@createdUserId", createdUserId));
            parameters.Add(new MySqlParameter("@wokTransLogId", wokTransLogId));
            
            Db.Master.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Insert into developer authorization dropbox for developer.dat downloads
        /// </summary>
        public string InsertDeveloperAuthorizationDropBox(int userId, string systemName, byte[] content, uint secondsToLive)
        {
            string sql = "SELECT uuid() id";
            DataRow row = Db.Master.GetDataRow(sql);

            if (row == null)
            {
                return null;
            }

            string uuid = Convert.ToString(row["id"]);
            if (uuid == null)
            {
                return null;
            }

            sql = 
                "INSERT INTO `developer_auth_dropbox`(`id`, `system_name`, `content`, `requester_id`, `requested_date`, `expired_date`) " +
                "VALUES (@id, @systemName, @content, @requesterId, now(), DATE_ADD(now(), INTERVAL @secondsToLive SECOND))";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@id", uuid));
            parameters.Add(new MySqlParameter("@systemName", systemName));
            parameters.Add(new MySqlParameter("@content", new MySqlBinaryString(content)));
            parameters.Add(new MySqlParameter("@requesterId", userId));
            parameters.Add(new MySqlParameter("@secondsToLive", secondsToLive));         

            Db.Master.ExecuteNonQuery(sql, parameters);
            return uuid;
        }

        /// <summary>
        /// Retrieve data for developer.dat downloads
        /// </summary>
        public byte[] GetDeveloperAuthorizationFromDropBox(int userId, string systemName, string uuid)
        {
            string sql = 
                "SELECT `content` FROM `developer_auth_dropbox` " +
                "WHERE `id` = @id AND `system_name`=@systemName AND `requester_id`=@requesterId AND `expired_date` > now()";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@id", uuid));
            parameters.Add(new MySqlParameter("@systemName", systemName));
            parameters.Add(new MySqlParameter("@requesterId", userId));

            DataRow row = Db.Master.GetDataRow(sql, parameters);
            if (row != null)
            {
                return (byte [])row["content"];
            }

            return null;
        }

        /// <summary>
        /// Delete expired developer authorization dropbox entries
        /// </summary>
        public void CleanupDeveloperAuthorizationDropBox()
        {
            string sql = "DELETE FROM `developer_auth_dropbox` WHERE `expired_date` <= now()";
            Db.Master.ExecuteNonQuery(sql, new List<IDbDataParameter>());
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}
