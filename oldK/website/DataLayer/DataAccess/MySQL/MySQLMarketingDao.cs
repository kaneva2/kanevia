///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLMarketingDao : IMarketingDao
    {
        /// <summary>
        /// GetLeaderboard
        /// </summary>
        /// <returns></returns>
        public DataTable GetShareLeaderboard(string sortOrder, int numResults)
        {
            string strQuery = " SELECT mst.user_id, thumbnail_small_path, display_name, name_no_spaces, number_of_clicks as clicks, count(registered_date) as registered, count(completed_checklist_date) as completed " +
                              " FROM marketing_share_tracking mst " +
                              " LEFT JOIN marketing_share_referrals msr on mst.user_id = msr.existing_user_id " +
                              " INNER JOIN users u on mst.user_id = u.user_id " +
                              " INNER JOIN communities_personal cp on u.user_id = cp.creator_id " +
                              " GROUP BY user_id " +
                              " ORDER BY completed DESC, registered DESC, clicks DESC " +
                              " LIMIT @numResults ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@numResults", numResults));
            DataTable dtLB = Db.Master.GetDataTable(strQuery, parameters);

            return dtLB;
        }

        /// <summary>
        /// countIps
        /// </summary>
        /// <returns></returns>
        public int countIps(int referredUserId)
        {
            string strQuery = " SELECT count(referred_user_id) FROM marketing_share_referrals msr " +
                              " INNER JOIN users u on msr.referred_user_id = u.user_id " +
                              " WHERE last_ip_address = (SELECT last_ip_address FROM users WHERE user_id = @referredUserId) ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@referredUserId", referredUserId));

            int result = int.Parse(Db.Master.GetScalar(strQuery, parameters).ToString());

            return result;
        }

        /// <summary>
        /// UpdateShareView
        /// </summary>
        public int UpdateShareView(int userId)
        {
            
            string sqlString = " INSERT INTO marketing_share_tracking (user_id, number_of_views) VALUES (@userId, 1) " +
                               " ON duplicate KEY UPDATE number_of_views = number_of_views + 1 ";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// UpdateShareClick
        /// </summary>
        public int UpdateShareClick(int userId)
        {
            string sqlString = " INSERT INTO marketing_share_tracking (user_id, number_of_clicks) VALUES (@userId, 1) " +
                               " ON duplicate KEY UPDATE number_of_clicks = number_of_clicks + 1 ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }


        /// <summary>
        /// UpdateReferral
        /// </summary>
        public int AddReferral(int referrerUserId, int referredUserId)
        {
            string sqlString = " INSERT INTO marketing_share_referrals (existing_user_id, referred_user_id, registered_date, completed_checklist_date) " +
                               " VALUES ( @existing_user_id, @referred_user_id, now(), null) ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@existing_user_id", referrerUserId));
            parameters.Add(new MySqlParameter("@referred_user_id", referredUserId));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// UpdateReferral
        /// </summary>
        public int CompleteReferral(int referredUserId)
        {
            string sqlString = " UPDATE marketing_share_referrals SET completed_checklist_date = now() " +
                               " WHERE  referred_user_id = @referred_user_id  and completed_checklist_date is null";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@referred_user_id", referredUserId));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// UpdateReferral
        /// </summary>
        public int GetReferrer(int referredUserId)
        {
            string sqlString = "SELECT existing_user_id FROM marketing_share_referrals " +
                               " WHERE  referred_user_id = @referred_user_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@referred_user_id", referredUserId));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }


    }
}
