///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;
using System.Linq;

using log4net;
using Sphinx.Client.Commands.Search;
using Sphinx.Client.Connections;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MYSQLGameDao : IGameDao
    {
        private const int GAME_IS_PUBLIC = 1;
        public const int LOGIN_CACHE_LIMIT_MIN = 5;
        public const int ZONECUSTOMIZATION_CACHE_LIMIT_MIN = 15;
        public const int ZONEDOWNLOADSIZES_CACHE_LIMIT_MIN = 60;
        public const int FAUX_3D_APP_CACHE_LIMIT_MIN = 60;

        /// <summary>
        /// Gets a Game.
        /// </summary>
        /// <param name="owner_id">Unique user identifier.</param>
        /// <returns>Game.</returns>
        public Game GetGame (int gameId)
        {
            //create query string to get game by user id
            string sql = "SELECT g.game_id, COALESCE(c.creator_id,g.owner_id) as owner_id, g.game_rating_id, " +
                " g.game_status_id, c.name as game_name, " +
                " COALESCE(c.description,g.game_description) as game_description, g.game_synopsis, g.game_creation_date, " +
                " g.game_access_id, c.thumbnail_medium_path as game_image_path, " +
                " COALESCE(c.keywords,g.game_keywords) as game_keywords, g.last_updated, g.modifiers_id, gs.number_of_comments, " +
                " COALESCE(cs.number_of_diggs,gs.number_of_raves) as number_of_raves, gs.number_of_visits, gs.number_of_players, " +
                " g.game_encryption_key, g.parent_game_id, COALESCE(c.is_adult,0) as is_adult, " +
                " COALESCE(c.over_21_required,0) as over_21_required, g.ga_account as ga_account, g.incubator_hosted " +
                " FROM games g " +
                " INNER JOIN game_stats gs ON g.game_id = gs.game_id " +
                " INNER JOIN kaneva.communities_game cg ON cg.game_id = g.game_id " +
                " INNER JOIN kaneva.communities c ON c.community_id = cg.community_id " +
                " LEFT JOIN kaneva.channel_stats cs ON cs.channel_id = c.community_id " +
                " WHERE g.game_id = @gameId ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@gameId", gameId));

            //perform query returning single data row from the developer database
            DataRow row = Db.Developer.GetDataRow (sql.ToString (), parameters);

            if (row != null)
            {
                //populate new instance of Game Object
                return new Game (Convert.ToInt32 (row["game_id"]), Convert.ToInt32 (row["owner_id"]), Convert.ToInt32 (row["game_rating_id"]),
                    Convert.ToInt32 (row["game_status_id"]), Convert.ToInt32 (row["game_access_id"]), Convert.ToInt32 (row["modifiers_id"]),
                    Convert.ToInt32 (row["number_of_comments"]), Convert.ToInt32 (row["number_of_raves"]), Convert.ToInt32 (row["number_of_visits"]),
                    Convert.ToInt32 (row["number_of_players"]), row["game_name"].ToString (), row["game_description"].ToString (), row["game_synopsis"].ToString (),
                    row["game_image_path"].ToString (), row["game_keywords"].ToString (),
                    Convert.ToDateTime (row["game_creation_date"]), Convert.ToDateTime (row["last_updated"]), row["game_encryption_key"].ToString (),
                    Convert.ToInt32 (row["parent_game_id"]), row["is_adult"].ToString ().Equals ("Y"), row["over_21_required"].ToString ().Equals ("Y"), row["ga_account"].ToString (), row["incubator_hosted"].ToString ().Equals ("1"));
            }
            else
            {
                return null;
            }
        }

        public Game GetGameByServerId (int serverId)
        {
            //create query string to get game by user id
            string sql = "SELECT g.game_id, COALESCE(c.creator_id,g.owner_id) as owner_id, g.game_rating_id, " +
                " g.game_status_id, COALESCE(c.name,g.game_name) as game_name, COALESCE(c.description,g.game_description) as game_description, " +
                " g.game_synopsis, g.game_creation_date, g.game_access_id, c.thumbnail_medium_path as game_image_path, " +
                " COALESCE(c.keywords,g.game_keywords) as game_keywords, g.last_updated, g.modifiers_id, gs.number_of_comments, " +
                " COALESCE(cs.number_of_diggs,gs.number_of_raves) as number_of_raves, " +
                " COALESCE(cs.number_of_views,gs.number_of_visits) as number_of_views, gs.number_of_players, g.game_encryption_key, " +
                " g.parent_game_id, COALESCE(c.is_adult,0) as is_adult, COALESCE(c.over_21_required,0) as over_21_required, g.ga_account as ga_account, g.incubator_hosted " +
                " FROM games g " +
                " INNER JOIN game_stats gs ON g.game_id = gs.game_id " +
                " INNER JOIN game_servers svr ON g.game_id = svr.game_id " +
                " INNER JOIN kaneva.communities_game cg ON cg.game_id = g.game_id " +
                " INNER JOIN kaneva.communities c ON c.community_id = cg.community_id " +
                " LEFT JOIN kaneva.channel_stats cs ON cs.channel_id = c.community_id " +
                " WHERE svr.server_id = @serverId ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@serverId", serverId));

            //perform query returning single data row from the developer database
            DataRow row = Db.Developer.GetDataRow (sql.ToString (), parameters);

            if (row != null)
            {
                //populate new instance of Game Object
                return new Game (Convert.ToInt32 (row["game_id"]), Convert.ToInt32 (row["owner_id"]), Convert.ToInt32 (row["game_rating_id"]),
                    Convert.ToInt32 (row["game_status_id"]), Convert.ToInt32 (row["game_access_id"]), Convert.ToInt32 (row["modifiers_id"]),
                    Convert.ToInt32 (row["number_of_comments"]), Convert.ToInt32 (row["number_of_raves"]), Convert.ToInt32 (row["number_of_views"]),
                    Convert.ToInt32 (row["number_of_players"]), row["game_name"].ToString (), row["game_description"].ToString (), row["game_synopsis"].ToString (),
                    row["game_image_path"].ToString (), row["game_keywords"].ToString (),
                    Convert.ToDateTime (row["game_creation_date"]), Convert.ToDateTime (row["last_updated"]), row["game_encryption_key"].ToString (),
                    Convert.ToInt32 (row["parent_game_id"]), row["is_adult"].ToString ().Equals ("Y"), row["over_21_required"].ToString ().Equals ("Y"), row["ga_account"].ToString (), row["incubator_hosted"].ToString ().Equals ("1"));
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets a Game.
        /// </summary>
        /// <param name="gameName">Unique user identifier.</param>
        /// <returns>Game.</returns>
        public Game GetGame (string gameName)
        {
            //create query string to get game by user id
            string sql = "SELECT g.game_id, COALESCE(c.creator_id,g.owner_id) as owner_id, g.game_rating_id, " +
                " g.game_status_id, COALESCE(c.name,g.game_name) as game_name, COALESCE(c.description) as game_description, " +
                " g.game_synopsis, g.game_creation_date, g.game_access_id, " +
                " c.thumbnail_medium_path as game_image_path, " +
                " COALESCE(c.keywords,g.game_keywords) as game_keywords, g.last_updated, g.modifiers_id, " +
                " gs.number_of_comments, COALESCE(cs.number_of_diggs,gs.number_of_raves) as number_of_raves, " +
                " COALESCE(cs.number_of_views,gs.number_of_visits) as number_of_views, gs.number_of_players, g.game_encryption_key, " +
                " g.parent_game_id, COALESCE(c.is_adult, 0) as is_adult, COALESCE(c.over_21_required,0) as over_21_required, g.ga_account as ga_account, g.incubator_hosted " +
                " FROM games g " +
                " INNER JOIN game_stats gs ON g.game_id = gs.game_id " +
                " INNER JOIN kaneva.communities_game cg ON cg.game_id = g.game_id " +
                " INNER JOIN kaneva.communities c ON c.community_id = cg.community_id " +
                " LEFT JOIN kaneva.channel_stats cs ON cs.channel_id = c.community_id " +
                " WHERE g.game_name = @gameName ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@gameName", gameName));

            //perform query returning single data row from the developer database
            DataRow row = Db.Developer.GetDataRow (sql.ToString (), parameters);

            //populate new instance of Game Object
            return new Game (Convert.ToInt32 (row["game_id"]), Convert.ToInt32 (row["owner_id"]), Convert.ToInt32 (row["game_rating_id"]),
                Convert.ToInt32 (row["game_status_id"]), Convert.ToInt32 (row["game_access_id"]), Convert.ToInt32 (row["modifiers_id"]),
                Convert.ToInt32 (row["number_of_comments"]), Convert.ToInt32 (row["number_of_raves"]), Convert.ToInt32 (row["number_of_views"]),
                Convert.ToInt32 (row["number_of_players"]), row["game_name"].ToString (), row["game_description"].ToString (), row["game_synopsis"].ToString (),
                row["game_image_path"].ToString (), row["game_keywords"].ToString (),
                Convert.ToDateTime (row["game_creation_date"]), Convert.ToDateTime (row["last_updated"]), row["game_encryption_key"].ToString (),
                Convert.ToInt32 (row["parent_game_id"]), row["is_adult"].ToString ().Equals ("Y"), row["over_21_required"].ToString ().Equals ("Y"), row["ga_account"].ToString (), row["incubator_hosted"].ToString ().Equals ("1"));
        }

        /// <summary>
        /// Gets all registered games.
        /// </summary>
        /// <param name="filter">filtering on query</param>
        /// <param name="orderby">ordering on query</param>
        /// <returns>IList<Game></returns>
        public IList<Game> GetAllGames (string filter, string orderby)
        {
            return GetGamesByOwner (0, filter, orderby);
        }

        /// <summary>
        /// Gets the game id associated with the 3d community id.
        /// </summary>
        /// <param name="communityId">filtering on query</param>
        /// <returns>int<Game></returns>
        public int GetGameIdByCommunityId (int communityId)
        {
            string strQuery = "SELECT game_id FROM communities_game WHERE community_id = @communityId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));

            Object result = Db.Master.GetScalar (strQuery, parameters);

            return (result == null ? 0 : Convert.ToInt32 (result));
        }

        /// <summary>
        /// Gets all registered games by an owner.
        /// </summary>
        /// <param name="ownerId">limit results to specific user</param>
        /// <param name="filter">filtering on query</param>
        /// <param name="orderby">ordering on query</param>
        /// <param name="pageNumber">page number for record set</param>
        /// <param name="pageSize">how many records in page</param>
        /// <returns>Game.</returns>
        public IList<Game> GetGamesByOwner (int ownerId, string filter, string orderby)
        {
            // UNION WAY
            string strQuery = "";
            string strWhere = "";
            string strOrderBy = "";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            // Friends I invited
            strQuery = "SELECT g.game_id, COALESCE(c.creator_id,g.owner_id) as owner_id, g.game_rating_id, " +
                " g.game_status_id, COALESCE(c.name,g.game_name) as game_name, " +
                " COALESCE(c.description,g.game_description) as game_description, " +
                " g.game_synopsis, g.game_creation_date, g.game_access_id, " +
                " c.thumbnail_medium_path as game_image_path, " +
                " COALESCE(c.keywords,g.game_keywords) as game_keywords, g.last_updated, g.modifiers_id, " +
                " gs.number_of_comments, COALESCE(cs.number_of_diggs,gs.number_of_raves) as number_of_raves, " +
                " gs.number_of_visits, gs.number_of_players, g.game_encryption_key, g.parent_game_id, " +
                " COALESCE(c.is_adult,0) as is_adult, COALESCE(c.over_21_required,0) as over_21_required, g.ga_account as ga_account, g.incubator_hosted ";

            strQuery += " FROM games g " +
                " INNER JOIN game_stats gs ON g.game_id = gs.game_id " +
                " INNER JOIN kaneva.communities_game cg ON cg.game_id = g.game_id " +
                " INNER JOIN kaneva.communities c ON c.community_id = cg.community_id " +
                " LEFT JOIN kaneva.channel_stats cs ON cs.channel_id = c.community_id ";


            if (ownerId > 0)
            {
                strWhere += " WHERE c.creator_id = @ownerId ";
                parameters.Add (new MySqlParameter ("@ownerId", ownerId));
            }

            if (filter.Length > 0)
            {
                if (strWhere.Length > 0)
                {
                    strWhere += " AND " + filter;
                }
                else
                {
                    strWhere += " WHERE " + filter;
                }
            }

            if (orderby != null && orderby.Length > 0)
            {
                strOrderBy = " ORDER BY " + orderby;
            }

            strQuery = strQuery + strWhere + strOrderBy;

            DataTable dt = Db.Developer.GetDataTable (strQuery, parameters);

            IList<Game> list = new List<Game> ();
            foreach (DataRow row in dt.Rows)
            {
                list.Add (new Game (Convert.ToInt32 (row["game_id"]), Convert.ToInt32 (row["owner_id"]), Convert.ToInt32 (row["game_rating_id"]),
                Convert.ToInt32 (row["game_status_id"]), Convert.ToInt32 (row["game_access_id"]), Convert.ToInt32 (row["modifiers_id"]),
                Convert.ToInt32 (row["number_of_comments"]), Convert.ToInt32 (row["number_of_raves"]), Convert.ToInt32 (row["number_of_visits"]),
                Convert.ToInt32 (row["number_of_players"]), row["game_name"].ToString (), row["game_description"].ToString (), row["game_synopsis"].ToString (), row["game_image_path"].ToString (),
                row["game_keywords"].ToString (), Convert.ToDateTime (row["game_creation_date"]), Convert.ToDateTime (row["last_updated"]), row["game_encryption_key"].ToString (),
                Convert.ToInt32 (row["parent_game_id"]), row["is_adult"].ToString ().Equals ("Y"), row["over_21_required"].ToString ().Equals ("Y"), row["ga_account"].ToString (), row["incubator_hosted"].ToString ().Equals ("1")));
            }
            return list;
        }

        /// <summary>
        /// Gets all games matching the search criteria
        /// </summary>
        /// <param name="keywords">limit results to specific keywords</param>
        /// <param name="filter">filtering on query</param>
        /// <param name="orderby">ordering on query</param>
        /// <param name="pageNumber">page number for record set</param>
        /// <param name="pageSize">how many records in page</param>
        /// <returns>PagedDataTable</returns>
        public PagedDataTable SearchForGames (string searchString, string filter, string categories, bool bGetMature, bool bThumbnailRequired, int pastDays, int ownerId, string orderBy, int pageNumber, int pageSize)
        {
            // UNION WAY
            string strQuery = "";
            string strFROM = "";
            string strWhere = "";
            string strOrderBy = "";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            // 
            strQuery = "SELECT DISTINCT(g.game_id), c.creator_id as owner_id, g.game_rating_id, g.game_status_id, " +
                " c.name as game_name, c.description as game_description, g.game_synopsis, g.game_creation_date, " +
                " g.game_access_id, c.thumbnail_medium_path as game_image_path, c.keywords as game_keywords, " +
                " g.last_updated, g.modifiers_id, gs.number_of_comments, cs.number_of_diggs as number_of_raves, " +
                " cs.number_of_views, g.game_encryption_key, gs.number_of_players, " +
                " c.creator_username as owner_display_name, c.creator_username as owner_username ";

            strFROM += " FROM games g " +
                " INNER JOIN game_stats gs ON g.game_id = gs.game_id " +
                " INNER JOIN kaneva.communities_game cg ON cg.game_id = g.game_id " +
                " INNER JOIN kaneva.communities c ON c.community_id = cg.community_id " +
                " INNER JOIN kaneva.channel_stats cs ON cs.channel_id = c.community_id ";

            strWhere += "WHERE g.game_access_id = " + GAME_IS_PUBLIC;

            if (bThumbnailRequired)
            {
                strWhere += " AND c.has_thumbnail = 'Y' ";
            }

            if (ownerId > 0)
            {
                strWhere += " AND c.creator_id = @ownerId ";
                parameters.Add (new MySqlParameter ("@ownerId", ownerId));
            }

            // Only get last x number of days?
            if (pastDays > 0)
            {
                strWhere += " AND g.game_creation_date > SUBDATE(NOW(),INTERVAL " + pastDays + " DAY) ";
            }

            // Mature
            if (!bGetMature)
            {
                strWhere += " AND gr.is_mature = 0 ";
                strFROM += " INNER JOIN game_ratings gr ON g.game_rating_id = gr.game_rating_id ";
            }

            if ((categories != null) && (categories.Trim ().Length > 0))
            {
                strWhere += " AND (gtc.category_id IN (" + categories + "))";
                strFROM += " INNER JOIN kaneva.communities_to_categories gtc ON c.community_id = gtc.community_id ";
            }

            // Search
            if ((filter != null) && (filter.Trim ().Length > 0))
            {
                strWhere += " AND " + filter + " ";
            }

            // Search
            if ((searchString != null) && (searchString.Trim ().Length > 0))
            {
                char[] splitter = { ' ' };
                string[] arKeywords = null;

                // Did they enter multiples?
                arKeywords = searchString.Split (splitter);

                if (arKeywords.Length > 1)
                {
                    searchString = "";
                    for (int j = 0; j < arKeywords.Length; j++)
                    {
                        searchString += arKeywords[j].ToString () + "* ";
                    }
                }
                else
                {
                    searchString = searchString + "*";
                }

                strWhere += " AND MATCH (game_name, game_description, game_synopsis, game_keywords) AGAINST (@searchString IN BOOLEAN MODE) ";
                parameters.Add (new MySqlParameter ("@searchString", searchString));

                // If no order by, do relevancy
                if (orderBy.Length.Equals (0))
                {
                    parameters.Add (new MySqlParameter ("@searchStringOrig", searchString));
                    strWhere += ", MATCH (game_name, game_description, game_synopsis, game_keywords) AGAINST (@searchStringOrig) as rel";
                    orderBy = " rel DESC ";
                }
            }

            if (orderBy != null && orderBy.Length > 0)
            {
                strOrderBy = orderBy + " DESC ";
            }

            strQuery = strQuery + strFROM + strWhere;

            return Db.Developer.GetPagedDataTable (strQuery, strOrderBy, parameters, pageNumber, pageSize);

        }

        /// <summary>
        /// Gets all registered games by an owner.
        /// </summary>
        /// <param name="ownerId">limit results to specific user</param>
        /// <param name="filter">filtering on query</param>
        /// <param name="orderby">ordering on query</param>
        /// <param name="pageNumber">page number for record set</param>
        /// <param name="pageSize">how many records in page</param>
        /// <returns>PagedDataTable</returns>
        public PagedDataTable GetGamesByOwner (int ownerId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // UNION WAY
            string strQuery = "";
            string strWhere = "";
            string strOrderBy = "";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            // Friends I invited
            strQuery = "SELECT g.game_id, COALESCE(c.creator_id,g.owner_id) as owner_id, g.game_rating_id, g.game_status_id, " +
                " COALESCE(c.name,g.game_name) as game_name, COALESCE(c.description,g.game_description) as game_description, " +
                " g.game_synopsis, g.game_creation_date, g.game_access_id, " +
                " COALESCE(c.thumbnail_medium_path,g.game_image_path) as game_image_path, " +
                " COALESCE(c.keywords,g.game_keywords) as game_keywords, " +
                " g.last_updated, g.modifiers_id, gs.number_of_comments, COALESCE(cs.number_of_diggs,gs.number_of_raves) as number_of_raves, " +
                " COALESCE(cs.number_of_views,gs.number_of_visits) as number_of_views, gs.number_of_players, g.game_encryption_key ";

            strQuery += " FROM games g " +
                " INNER JOIN game_stats gs ON g.game_id = gs.game_id " +
                " LEFT JOIN kaneva.communities_game cg ON cg.game_id = g.game_id " +
                " LEFT JOIN kaneva.communities c ON c.community_id = cg.community_id " +
                " LEFT JOIN kaneva.channel_stats cs ON cs.channel_id = c.community_id ";

            if (ownerId > 0)
            {
                strWhere += " WHERE c.creator_id = @ownerId ";
                parameters.Add (new MySqlParameter ("@ownerId", ownerId));
            }

            if (filter.Length > 0)
            {
                if (strWhere.Length > 0)
                {
                    strWhere += " AND " + filter;
                }
                else
                {
                    strWhere += " WHERE " + filter;
                }
            }

            if (orderby != null && orderby.Length > 0)
            {
                strOrderBy = orderby;
            }

            strQuery = strQuery + strWhere;

            return Db.Developer.GetPagedDataTable (strQuery, strOrderBy, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// get a list of registered games 
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public PagedDataTable GetGames (int pageNumber, int pageSize, string orderBy)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            // XML conversion barfs on empty strings, so add IFs
            // TODO -- enforce valid values in DB
            string selectList = " SELECT COALESCE(IF(length(c.name) = 0, 'unknown', c.name),IF(length(g.game_name) = 0, 'unknown', g.game_name)) AS name,  " +
                                "        g.game_id, " +
                                "        COALESCE(IF(length(c.description) = 0, 'unknown', c.description),IF(length(g.game_description) = 0, 'unknown', g.game_description))  AS description, " +
                                "        sum(gs.number_of_players) AS population,  " +
                                "        gl.max_game_users AS max_population,  " +
                                "        COALESCE(cs.number_of_diggs,(SELECT count(*) FROM game_raves gr WHERE gr.game_id = g.game_id)) AS raves " +
                                " FROM  games g " +
                                "       INNER JOIN game_servers gs on g.game_id = gs.game_id " +
                                "       INNER JOIN game_licenses gl on gl.game_id = g.game_id " +
                                "       INNER JOIN kaneva.communities_game cg ON cg.game_id = g.game_id " +
                                "       INNER JOIN kaneva.communities c ON c.community_id = cg.community_id " +
                                "       LEFT JOIN kaneva.channel_stats cs ON cs.channel_id = c.community_id " +
                                " WHERE g.game_status_id = 1  " + //-- active
                                "       AND gs.server_status_id = 1  " + //-- 1 is running
                                "       AND timestampdiff (minute, last_ping_datetime, NOW()) < deadServerInterval() " + // -- minute limit  
                                "       AND gs.visibility_id = 1 " + //-- server is public  
                                "       AND g.game_access_id = 1 " + //-- game is public
                                " GROUP BY 1, 2, 3 ";

            string orderByList = orderBy;
            /*
                string selectList = " SELECT IF(length(g.game_name) = 0, 'unknown', g.game_name)  AS name,  " +
                                "          g.game_id, " +
                                "           IF(length(g.game_description) = 0, 'unknown', g.game_description)  AS description, " +
                                "           sum(gs.number_of_players) AS population,  " +
                                "           gl.max_game_users AS max_population,  " +
                                "           (SELECT count(*) FROM game_raves gr WHERE gr.game_id = g.game_id) AS raves " +
                                " FROM  games g " +
                                "       INNER JOIN game_servers gs on g.game_id = gs.game_id " +
                                "       INNER JOIN game_licenses gl on gl.game_id = g.game_id " +
                                " WHERE g.game_status_id = 1  " + //-- active
                                "      AND gs.server_status_id = 1  " + //-- 1 is running
                                "        AND timestampdiff (minute, last_ping_datetime, NOW()) < deadServerInterval() " + // -- minute limit  
                                "        AND gs.visibility_id = 1 " + //-- server is public  
                                "        AND g.game_access_id = 1 " + //-- game is public
                                " GROUP BY 1, 2, 3 ";
            */

            return Db.Developer.GetPagedDataTable (selectList, orderByList, parameters, pageNumber, pageSize);
        }

        /// <summary>
        ///Saves a new game (game, license, and 3d community) in a transaction
        /// returns the games community id if seuccesful (last create done)
        /// </summary>
        /// <param name="game"></param>
        /// <param name="license"></param>
        /// <param name="community"></param>
        /// <returns>int</returns>
        public int TransactionalGameCreation (Game game, GameLicense license, Community community, int daysLeft)
        {
            // Send the message
            string sql = "CALL create_new_game(@ownerId, @gameRatingId, @gameStatusId, @gameName, @gameDescription, @gameSynopsis, NOW(), " +
            "@gameAccessId, @gameImagePath, @gameKeywords, @gameEncryptionKey, NOW(), @modifiersId, @parentGameId, @licenseSubscriptionId, " +
            "@gameKey, NOW(), ADDDATE(NOW(), INTERVAL @daysLeft DAY), NOW(), @licenseStatusId, @maxGameUsers, @canPublish, @communityTypeId, @backgroundImage, " +
            "@backgroundRGB, @description, @isAdult, @isPublic, @keywords, @name, @nameNoSpaces,  @over21Required, @placeTypeId, " +
            "@isPersonal, @statusId, @email, @creatorId, @creatorUsername, @url)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@ownerId", game.OwnerId));
            parameters.Add (new MySqlParameter ("@gameRatingId", game.GameRatingId));
            parameters.Add (new MySqlParameter ("@gameStatusId", game.GameStatusId));
            parameters.Add (new MySqlParameter ("@gameName", game.GameName));
            parameters.Add (new MySqlParameter ("@gameDescription", game.GameDescription));
            parameters.Add (new MySqlParameter ("@gameSynopsis", game.GameSynopsis));
            parameters.Add (new MySqlParameter ("@gameAccessId", game.GameAccessId));
            parameters.Add (new MySqlParameter ("@gameImagePath", (game.GameImagePath.Equals ("") ? null : game.GameImagePath)));
            parameters.Add (new MySqlParameter ("@gameKeywords", game.GameKeyWords));
            parameters.Add (new MySqlParameter ("@gameEncryptionKey", game.GameEncryptionKey));
            parameters.Add (new MySqlParameter ("@modifiersId", game.GameModifiersId));
            parameters.Add (new MySqlParameter ("@parentGameId", game.ParentGameId));

            parameters.Add (new MySqlParameter ("@licenseSubscriptionId", license.LicenseSubscriptionId));
            parameters.Add (new MySqlParameter ("@gameKey", license.GameKey));
            parameters.Add (new MySqlParameter ("@licenseStatusId", license.LicenseStatusId));
            parameters.Add (new MySqlParameter ("@maxGameUsers", license.MaxGameUsers));
            parameters.Add (new MySqlParameter ("@daysLeft", daysLeft));

            parameters.Add (new MySqlParameter ("@canPublish", community.AllowPublishing));
            parameters.Add (new MySqlParameter ("@communityTypeId", community.CommunityTypeId));
            parameters.Add (new MySqlParameter ("@backgroundImage", community.BackgroundImage));
            parameters.Add (new MySqlParameter ("@backgroundRGB", community.BackgroundRGB));
            parameters.Add (new MySqlParameter ("@description", community.Description));
            parameters.Add (new MySqlParameter ("@isAdult", community.IsAdult));
            parameters.Add (new MySqlParameter ("@isPublic", community.IsPublic));
            parameters.Add (new MySqlParameter ("@keywords", community.Keywords));
            parameters.Add (new MySqlParameter ("@name", community.Name));
            parameters.Add (new MySqlParameter ("@nameNoSpaces", community.NameNoSpaces));
            parameters.Add (new MySqlParameter ("@over21Required", community.Over21Required ? 'Y' : 'N'));
            parameters.Add (new MySqlParameter ("@placeTypeId", community.PlaceTypeId));
            parameters.Add (new MySqlParameter ("@isPersonal", community.IsPersonal));
            parameters.Add (new MySqlParameter ("@statusId", community.StatusId));
            parameters.Add (new MySqlParameter ("@email", community.Email));
            parameters.Add (new MySqlParameter ("@creatorId", community.CreatorId));
            parameters.Add (new MySqlParameter ("@creatorUsername", community.CreatorUsername));
            if ((community.Url == null) || (community.Url.Length <= 0))
            {
                parameters.Add (new MySqlParameter ("@url", DBNull.Value));
            }
            else
            {
                parameters.Add (new MySqlParameter ("@url", community.Url));
            }

            //// The ownerId might be a dummy if this is an app-incubator preallocated app. 
            //// So, some dummy user might be getting credit for creating a bunch of apps. No biggie.
            //FameFacade fameFacade = new FameFacade();
            //fameFacade.RedeemPacket(game.OwnerId, (int)PacketId.MAKE_A_GAME, (int)FameTypes.World);

            return Convert.ToInt32 (Db.Developer.GetScalar (sql, parameters));
        }

        //--manage game functions---
        /// <summary>
        /// Add A New Game
        /// </summary>
        /// <param name="game"></param>
        /// <returns>int</returns>
        public int AddNewGame (Game game)
        {
            // Send the message
            string sql = "CALL add_new_game(@owner_id, @game_rating_id, @game_status_id, @game_name, @game_description, " +
                " @game_synopsis, @game_creation_date, @game_access_id, @game_image_path, @game_keywords, @game_encryption_key, " +
                " @last_updated, @modifiers_id, @parent_game_id, @gameId); SELECT CAST(@gameId as UNSIGNED INT) as gameId; ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@owner_id", game.OwnerId));
            parameters.Add (new MySqlParameter ("@game_rating_id", game.GameRatingId));
            parameters.Add (new MySqlParameter ("@game_status_id", game.GameStatusId));
            parameters.Add (new MySqlParameter ("@game_name", game.GameName));
            parameters.Add (new MySqlParameter ("@game_description", game.GameDescription));
            parameters.Add (new MySqlParameter ("@game_synopsis", game.GameSynopsis));
            parameters.Add (new MySqlParameter ("@game_creation_date", game.GameCreationDate));
            parameters.Add (new MySqlParameter ("@game_access_id", game.GameAccessId));
            parameters.Add (new MySqlParameter ("@game_image_path", (game.GameImagePath.Equals ("") ? null : game.GameImagePath)));
            parameters.Add (new MySqlParameter ("@game_keywords", game.GameKeyWords));
            parameters.Add (new MySqlParameter ("@game_encryption_key", game.GameEncryptionKey));
            parameters.Add (new MySqlParameter ("@last_updated", game.LastUpdated));
            parameters.Add (new MySqlParameter ("@modifiers_id", game.GameModifiersId));
            parameters.Add (new MySqlParameter ("@parent_game_id", game.ParentGameId));

            return Convert.ToInt32 (Db.Developer.GetScalar (sql, parameters));
        }

        //--manage game functions---
        /// <summary>
        /// Update Game
        /// </summary>
        /// <param name="game"></param>
        /// <returns>int</returns>
        public int UpdateGame (Game game)
        {
            // Send the message
            string sql = "CALL update_game(@game_id, @owner_id, @game_rating_id, @game_status_id, @game_name, @game_description, " +
                " @game_synopsis, @game_creation_date, @game_access_id, @game_image_path, @game_keywords, @game_encryption_key, " +
                " @last_updated, @modifiers_id, @parent_game_id, @isAdult, @over21Required)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@owner_id", game.OwnerId));
            parameters.Add (new MySqlParameter ("@game_rating_id", game.GameRatingId));
            parameters.Add (new MySqlParameter ("@game_status_id", game.GameStatusId));
            parameters.Add (new MySqlParameter ("@game_name", game.GameName));
            parameters.Add (new MySqlParameter ("@game_description", game.GameDescription));
            parameters.Add (new MySqlParameter ("@game_synopsis", game.GameSynopsis));
            parameters.Add (new MySqlParameter ("@game_creation_date", game.GameCreationDate));
            parameters.Add (new MySqlParameter ("@game_access_id", game.GameAccessId));
            parameters.Add (new MySqlParameter ("@game_image_path", (game.GameImagePath.Equals ("") ? null : game.GameImagePath)));
            parameters.Add (new MySqlParameter ("@game_keywords", game.GameKeyWords));
            parameters.Add (new MySqlParameter ("@last_updated", game.LastUpdated));
            parameters.Add (new MySqlParameter ("@modifiers_id", game.GameModifiersId));
            parameters.Add (new MySqlParameter ("@game_id", game.GameId));
            parameters.Add (new MySqlParameter ("@parent_game_id", game.ParentGameId));
            parameters.Add (new MySqlParameter ("@game_encryption_key", game.GameEncryptionKey));
            parameters.Add (new MySqlParameter ("@isAdult", (game.IsAdult ? "Y" : "N")));
            parameters.Add (new MySqlParameter ("@over21Required", (game.Over21Required ? "Y" : "N")));

            // clean up the cache of serverid/status
            List<int> serverIds = (List<int>) CentralCache.Get (CentralCache.keyGameServerIds (game.GameId));
            if (serverIds != null)
            {
                foreach (int serverId in serverIds)
                    CentralCache.Remove (CentralCache.keyGameServerAccess (serverId));
                CentralCache.Remove (CentralCache.keyGameServerIds (game.GameId));
            }

            return Convert.ToInt32 (Db.Developer.GetScalar (sql, parameters));
        }
        /// <summary>
        /// Update Game
        /// </summary>
        /// <param name="game"></param>
        /// <returns>int</returns>
        public int UpdateGameThumbnail (int gameId, string imagePath)
        {
            // Send the message
            string sql = "UPDATE games SET game_image_path = @imagePath WHERE game_id = @gameId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@gameId", gameId));
            parameters.Add (new MySqlParameter ("@imagePath", imagePath));

            return Db.Developer.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// Update Game Google Analytics Account ID
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="gaAccountID"></param>
        /// <returns>int</returns>
        public int UpdateGameGAAccountID (int gameId, string gaAccountID)
        {
            // Send the message
            string sql = "UPDATE games SET ga_account = @gaAccountID WHERE game_id = @gameId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@gameId", gameId));
            parameters.Add (new MySqlParameter ("@gaAccountID", gaAccountID));

            return Db.Developer.ExecuteNonQuery (sql, parameters);
        }

        //--manage game functions---
        /// <summary>
        /// Delete Game
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="userId"></param>
        /// <returns>int</returns>
        public int DeleteGame (int gameId, int userId)
        {
            string sql = "CALL delete_game (@game_id, @user_id)";

            // clean up the cache of serverid/status
            List<int> serverIds = (List<int>) CentralCache.Get (CentralCache.keyGameServerIds (gameId));
            if (serverIds != null)
            {
                foreach (int serverId in serverIds)
                    CentralCache.Remove (CentralCache.keyGameServerAccess (serverId));
                CentralCache.Remove (CentralCache.keyGameServerIds (gameId));
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@game_id", gameId));
            parameters.Add (new MySqlParameter ("@user_id", userId));
            return Db.Developer.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// GetGameRatings
        /// </summary>
        /// <returns></returns>
        public DataTable GetGameRatings ()
        {
            string strQuery = "SELECT game_rating_id, game_rating, description, minimum_age, is_mature " +
                " FROM game_ratings ";

            return Db.Developer.GetDataTable (strQuery);
        }

        //pull down choice functions
        /// <summary>
        /// Get Game Status Options
        /// </summary>
        /// <returns></returns>
        public DataTable GetGameStatus ()
        {
            string strQuery = "SELECT game_status_id, game_status " +
                " FROM game_status ";

            return Db.Developer.GetDataTable (strQuery);
        }

        /// <summary>
        /// Get Game Access Options
        /// </summary>
        /// <returns></returns>
        public DataTable GetGameAccess ()
        {
            string strQuery = "SELECT game_access_id, game_access" +
                " FROM game_access ";

            return Db.Developer.GetDataTable (strQuery);
        }

        /// <summary>
        /// Get Game Passes
        /// </summary>
        /// <returns></returns>
        public DataTable GetGamePasses (int gameId)
        {
            string strQuery = "SELECT pg.name, gpg.pass_group_id, gpg.set_by_admin FROM game_pass_groups gpg, wok.pass_groups pg WHERE gpg.pass_group_id = pg.pass_group_id AND gpg.game_id = @gameId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@gameId", gameId));

            return Db.Developer.GetDataTable (strQuery, parameters);
        }

        /// <summary>
        /// Add Game Pass
        /// </summary>
        /// <returns></returns>
        public bool AddGamePass (int userId, bool isAdmin, int gameId, int passId)
        {
            CommunityMember.CommunityMemberAccountType priv = GetGamePrivileges (userId, gameId);
            if (isAdmin || priv == CommunityMember.CommunityMemberAccountType.OWNER)
            {
                MySQLSubscriptionDao subDao = new MySQLSubscriptionDao ();
                if (isAdmin || subDao.HasValidSubscriptionByPassType ((uint) userId, (uint) passId))
                {
                    // Send the message
                    string sql = "INSERT INTO game_pass_groups (game_id, pass_group_id, set_by_admin) VALUES (@gameId, @passId, @setByAdmin)";

                    List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
                    parameters.Add (new MySqlParameter ("@gameId", gameId));
                    parameters.Add (new MySqlParameter ("@passId", passId));
                    parameters.Add (new MySqlParameter ("@setByAdmin", (isAdmin ? "Y" : "N")));

                    return (Db.Developer.ExecuteNonQuery (sql, parameters) != 0);
                }
            }

            return false;
        }

        /// <summary>
        /// Delete Game Pass
        /// </summary>
        /// <returns></returns>
        public bool DeleteGamePass (int userId, bool isAdmin, int gameId, int passId)
        {
            CommunityMember.CommunityMemberAccountType priv = GetGamePrivileges (userId, gameId);
            if (isAdmin || priv == CommunityMember.CommunityMemberAccountType.OWNER)
            {
                // Send the message
                string sql = "DELETE FROM game_pass_groups WHERE game_id = @gameId AND pass_group_id = @passId";

                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
                parameters.Add (new MySqlParameter ("@gameId", gameId));
                parameters.Add (new MySqlParameter ("@passId", passId));

                return (Db.Developer.ExecuteNonQuery (sql, parameters) != 0);
            }

            return false;
        }

        /// <summary>
        /// Get Game Category options
        /// </summary>
        /// <returns></returns>
        [Obsolete("Kept temporarily to allow function while porting game categories over to communities. Needs to be moved to CommunityDao.", false)]
        public DataTable GetGameCategories ()
        {
            string strQuery = "SELECT category_id, category_name AS description " +
                " FROM kaneva.community_categories ";

            return Db.Developer.GetDataTable (strQuery);
        }

        /// <summary>
        /// Get Categories A Game Is In
        /// </summary>
        /// <returns></returns>
        [Obsolete("Kept temporarily to allow function while porting game categories over to communities. Needs to be moved to CommunityDao.", false)]
        public DataTable GetGameCategories(int gameId)
        {
            string strQuery = "SELECT cc.category_id, cc.category_name AS description" +
                " FROM kaneva.communities_game cg " +
                " INNER JOIN kaneva.communities_to_categories ctc ON cg.community_id = ctc.community_id " +
                " INNER JOIN kaneva.community_categories cc ON cc.category_id = ctc.category_id " +
                " WHERE cg.game_id = @game_id ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@gameId", gameId));

            return Db.Developer.GetDataTable(strQuery, parameters);
        }

        /// <summary>
        /// Gets the category id for a given game category name.
        /// </summary>
        /// <returns></returns>
        [Obsolete("Kept temporarily to allow function while porting game categories over to communities. Needs to be moved to CommunityDao.", false)]
        public int GetGameCategoryIdByName(string categoryName)
        {
            string strQuery = "SELECT gc.category_id" +
                " FROM kaneva.community_categories gc WHERE gc.category_name = @categoryName";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@categoryName", categoryName));

            DataRow row = Db.Developer.GetDataRow(strQuery, parameters);

            if (row != null && row["category_id"] != null)
            {
                return Convert.ToInt32(row["category_id"]);
            }

            return -1;
        }

        //--manage game functions---
        /// <summary>
        /// Delete a Games's categories
        /// </summary>
        /// <param name="gameId"></param>
        /// <returns>int</returns>
        [Obsolete("Kept temporarily to allow function while porting game categories over to communities. Needs to be moved to CommunityDao.", false)]
        public int DeleteGameCategories(int gameId)
        {
            // Send the message
            string sql = " DELETE FROM kaneva.communities_to_categories " +
                         " WHERE community_id IN (SELECT community_id FROM kaneva.communities_game WHERE game_id = @gameId) ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@gameId", gameId));

            return Db.Developer.ExecuteNonQuery(sql, parameters);
        }

        //--manage game functions---
        /// <summary>
        /// Add new category for a game
        /// </summary>
        /// <param name="game"></param>
        /// <returns>int</returns>
        [Obsolete("Kept temporarily to allow function while porting game categories over to communities. Needs to be moved to CommunityDao.", false)]
        public int InsertGameCategories(int gameId, int categoryId)
        {
            // Send the message
            string sql = " INSERT INTO kaneva.communities_to_categories (community_id, category_id) " +
                         " SELECT community_id, @categoryId " +
                         " FROM kaneva.communities_game " +
                         " WHERE game_id = @gameId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@gameId", gameId));
            parameters.Add(new MySqlParameter("@categoryId", categoryId));

            return Db.Developer.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Insert a rave
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="gameId"></param>
        /// <returns>int</returns>
        public int InsertGameRave (int userId, int gameId)
        {
            string sql = "INSERT INTO game_raves " +
                "(user_id, game_id, rave_date " +
                ") VALUES (" +
                "@userId, @gameId, @raveDate)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@gameId", gameId));
            parameters.Add (new MySqlParameter ("@raveDate", DateTime.Now));

            return Db.Developer.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// ClearZoneCustomizationsCache
        /// </summary>
        /// <param name="zoneIndex"></param>
        /// <param name="instanceId"></param>
        public void ClearZoneCustomizationsCache (int zoneIndex, int instanceId)
        {
            string zoneKey = CentralCache.keyZoneCustomizations (zoneIndex, instanceId);
            string zonesCached = (string) CentralCache.Get (zoneKey);
            string[] keys;
            char[] splitter = { ';' };

            if (zonesCached != null)
            {
                // Remove each spawn point
                keys = zonesCached.Split (splitter);

                foreach (string key in keys)
                {
                    CentralCache.Remove (key);
                }

                // Remove the stored keys
                CentralCache.Remove (zoneKey);
            }

            string zoneSizeKey = CentralCache.keyZoneDownloadSizes(zoneIndex, instanceId);
            CentralCache.Remove(zoneSizeKey);
        }


        /// <summary>
        /// get the zone customizations for an instance (Dynamic objects+world object textures)
        /// </summary>
        /// <param name="zoneIndex"></param>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        public DataSet GetZoneCustomizations (int zoneIndex, int instanceId, float x, float y, float z)
        {
            string key = CentralCache.keyZoneCustomizationsPoint (zoneIndex, instanceId, x, y, z);

            DataSet ret = (DataSet) CentralCache.Get (key);
            if (ret == null)
            {
                string sql = "CALL wok.get_zone_customizations(@zoneIndex, @instanceId, @x, @y, @z)";

                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
                parameters.Add (new MySqlParameter ("@zoneIndex", zoneIndex));
                parameters.Add (new MySqlParameter ("@instanceId", instanceId));
                parameters.Add (new MySqlParameter ("@x", x));
                parameters.Add (new MySqlParameter ("@y", y));
                parameters.Add (new MySqlParameter ("@z", z));

                ret = Db.WOK.GetDataSet (sql, parameters);

                // keep track of the keys we cache so they can be cleaned 
                // up by server just by zone+instance
                string zoneKey = CentralCache.keyZoneCustomizations (zoneIndex, instanceId);
                string zonesCached = (string) CentralCache.Get (zoneKey);
                if (zonesCached != null)
                {
                    if (!zonesCached.Contains (key))
                    {
                        zonesCached += ";" + key;
                        CentralCache.Store (zoneKey, zonesCached, TimeSpan.FromMinutes (ZONECUSTOMIZATION_CACHE_LIMIT_MIN));
                    }
                }
                else
                    CentralCache.Store (zoneKey, key, TimeSpan.FromMinutes (ZONECUSTOMIZATION_CACHE_LIMIT_MIN));

                CentralCache.Store (key, ret, TimeSpan.FromMinutes (ZONECUSTOMIZATION_CACHE_LIMIT_MIN));
            }
            return ret;
        }

        /// <summary>
        /// Get zone download size break-downs for an instance
        /// </summary>
        /// <param name="zoneIndex"></param>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        public IDictionary<uint, uint[]> GetZoneDownloadSizes(int zoneIndex, int instanceId, uint meshPathTypeId, uint texPathTypeId)
        {
            string key = CentralCache.keyZoneDownloadSizes(zoneIndex, instanceId);

            IDictionary<uint, uint[]> ret = (IDictionary<uint, uint[]>)CentralCache.Get(key);
            if (ret == null)
            {
                string sql =
                    "SELECT `type_id`, sum(`file_size`) `size`, sum(`file_size`*cnt) `inst_size` FROM (" +
                        "SELECT ip.`global_id`, ip.`type_id`, ip.`ordinal`, ip.`file_size`, count(*) cnt " +
                        "FROM `dynamic_objects` do " +
                        "INNER JOIN `item_paths` ip ON do.`global_id`=ip.`global_id` " +
                        "WHERE do.`zone_index` = @zoneIndex AND do.`zone_instance_id`=@instanceId " +
                        "AND (ip.`type_id`=@meshPathTypeId OR ip.`type_id`=@texPathTypeId) " +
                        "GROUP BY ip.`global_id`, ip.`type_id`, ip.`ordinal` " +
                    ") x GROUP BY `type_id`";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@zoneIndex", zoneIndex));
                parameters.Add(new MySqlParameter("@instanceId", instanceId));
                parameters.Add(new MySqlParameter("@meshPathTypeId", meshPathTypeId));
                parameters.Add(new MySqlParameter("@texPathTypeId", texPathTypeId));

                DataTable dt = Db.WOK.GetDataTable(sql, parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ret = new Dictionary<uint, uint[]>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        try
                        {
                            uint typeId = Convert.ToUInt32(dr["type_id"]);
                            uint size = Convert.ToUInt32(dr["size"]);
                            uint instSize = Convert.ToUInt32(dr["inst_size"]);
                            ret.Add(typeId, new uint[]{size, instSize});
                        }
                        catch (Exception) { }
                    }

                    CentralCache.Store(key, ret, TimeSpan.FromMinutes(ZONEDOWNLOADSIZES_CACHE_LIMIT_MIN));
                }
            }

            return ret;
        }

        // return true if framework is enabled for the world (i.e. a game world)
        public bool IsFrameworkEnabled(int instanceId, int zoneType)
        {
            string sql = "SELECT `framework_enabled` FROM `script_game_framework_settings` WHERE zone_instance_id=@instanceId AND zone_type=@zoneType";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@instanceId", instanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));

            object res = Db.WOK.GetScalar(sql, parameters);
            if (Convert.IsDBNull(res))
            {
                return false;   // Default is false ("F")
            }

            return Convert.ToString(res) == "T";
        }
        
        // if gameId is zero, looks for the user in any game
        public bool UserInGame (int userId, int gameId)
        {
            int lastGameId = 0;
            string key = CentralCache.keyUserInGame (userId);

            object o = CentralCache.Get (key);
            if (o != null)
            {
                lastGameId = (int) o;
            }
            else
            {
                string sql = "SELECT game_id FROM active_logins " +
                                        " WHERE user_id = @userId";

                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
                parameters.Add (new MySqlParameter ("@userId", userId));
                parameters.Add (new MySqlParameter ("@gameId", gameId));

                int myGameId = Convert.ToInt32 (Db.Developer.GetScalar (sql, parameters));
                CentralCache.Store (key, myGameId, TimeSpan.FromMinutes (LOGIN_CACHE_LIMIT_MIN));
            }


            if (gameId == 0)
                return lastGameId != 0;
            else
                return lastGameId == gameId;

        }

        public DataTable GetGameUserIn (int userId)
        {
            string strQuery = "SELECT game_id, server_id " +
                " FROM active_logins " +
                " WHERE user_id = @userId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            return Db.Developer.GetDataTable (strQuery, parameters);
        }

        /// <summary>
        /// Checks to see if a user is currently authorized to a game.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="gameId"></param>
        /// <returns></returns>
        public eLOGIN_RESULTS LoginUser (int userId, string userIpAddress, int gameId, int serverId, string actualIp, ref int roleId)
        {
            eLOGIN_RESULTS ret = eLOGIN_RESULTS.FAILED;
            roleId = 0; // nothing

            // 9/08 updated to call SP so behavior can be site-specific.
            string sqlSelectString = "CALL kepauth_login( @userId, @userIpAddress, @gameId, @serverId, @actualIp );";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@userIpAddress", userIpAddress));
            parameters.Add (new MySqlParameter ("@gameId", gameId));
            parameters.Add (new MySqlParameter ("@serverId", serverId));
            parameters.Add (new MySqlParameter ("@actualIp", actualIp));

            DataRow drAsset = Db.Developer.GetDataRow (sqlSelectString, parameters);
            // will return the following possible values that map to the C# return codes
            // SUCCESS 1
            // NO_GAME_ACCESS 4 (old rc, not member of community, used if no servers) 
            // GAME_FULL 9
            // NOT_AUTHORIZED 10 (mature, etc.)
            // WRONG SERVER 11 (the actual_ip_address doesn't match serverId)
            // ALREADY_LOGGED_ON 12 
            if (drAsset[0] != DBNull.Value)
            {
                ret = (eLOGIN_RESULTS) Convert.ToInt32 (drAsset[0]);
                if (ret == eLOGIN_RESULTS.SUCCESS && drAsset.Table.Columns.Count > 1) // older version of SP only returned one col
                    roleId = Convert.ToInt32 (drAsset[1]);
            }
            CentralCache.Store (CentralCache.keyUserInGame (userId), gameId);

            return ret;
        }

        /// <summary>
        /// Checks to see if a user is currently authorized to a game.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="gameId"></param>
        /// <returns></returns>
        public eLOGIN_RESULTS LogoutUser (int userId, int reasonCode, int serverId, string actualIp)
        {
            eLOGIN_RESULTS ret = eLOGIN_RESULTS.FAILED;

            // 9/08 updated to call SP so behavior can be site-specific.
            string sqlSelectString = "CALL kepauth_logout( @userId, @reasonCode, @serverId, @actualIp );";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@reasonCode", reasonCode));
            parameters.Add (new MySqlParameter ("@serverId", serverId));
            parameters.Add (new MySqlParameter ("@actualIp", actualIp));

            DataRow drAsset = Db.Developer.GetDataRow (sqlSelectString, parameters);
            // will return the following possible values that map to the C# return codes
            // SUCCESS 1
            // WRONG SERVER 11 (the actual_ip_address doesn't match serverId)
            // ALREADY_LOGGED_ON 12 
            if (drAsset[0] != DBNull.Value)
            {
                ret = (eLOGIN_RESULTS) Convert.ToInt32 (drAsset[0]);
            }
            CentralCache.Remove (CentralCache.keyUserInGame (userId));

            return ret;
        }

        public DataSet GetChildGameWebCallValues ()
        {
            string strQuery = "SELECT url_value FROM child_game_webcall_values";

            return Db.Developer.GetDataSet (strQuery);
        }

        public CommunityMember.CommunityMemberAccountType GetGamePrivileges (int userId, int gameId)
        {
            string strQuery;

            strQuery = "SELECT get_game_priv(@game_id, @user_id) AS game_privileges";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@game_id", gameId));
            parameters.Add (new MySqlParameter ("@user_id", userId));
            DataRow dr = Db.WOK.GetDataRow (strQuery, parameters);

            CommunityMember.CommunityMemberAccountType ret = 0;
            if (dr != null)
            {
                ret = (CommunityMember.CommunityMemberAccountType) dr["game_privileges"];
            }
            return ret;
        }

        public DataSet GetGameItems (int userId, int gameId, int globalId, int quantity)
        {
            string strQuery;

            CommunityMember.CommunityMemberAccountType priv = GetGamePrivileges (userId, gameId);

            // Show private Premium Items to moderators of the 3D App
            string availableStatuses = "(";
            if (priv == CommunityMember.CommunityMemberAccountType.OWNER || priv == CommunityMember.CommunityMemberAccountType.MODERATOR)
            {
                availableStatuses += Convert.ToInt32 (WOKItem.ItemActiveStates.Public) + ", " + Convert.ToInt32 (WOKItem.ItemActiveStates.Private) + ")";
            }
            else
            {
                availableStatuses += Convert.ToInt32 (WOKItem.ItemActiveStates.Public) + ")";
            }

            if (globalId == -1 && quantity == -1)
            {
                strQuery = "SELECT gi.global_id as global_id, iw.display_name as display_name, i.description as description, iw.designer_price as designer_price FROM wok.game_items gi, shopping.items_web iw, wok.items i WHERE gi.global_id = iw.global_id and gi.global_id = i.global_id and iw.item_active IN " + availableStatuses + " and game_id = @game_id";

                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
                parameters.Add (new MySqlParameter ("@game_id", gameId));

                return Db.WOK.GetDataSet (strQuery, parameters);
            }
            else
            {
                strQuery = "SELECT gi.global_id as global_id, iw.display_name as display_name, i.description as description, iw.designer_price * @quantity as total_cost FROM wok.game_items gi, shopping.items_web iw, wok.items i WHERE gi.global_id = iw.global_id and gi.global_id = i.global_id and iw.item_active IN " + availableStatuses + " and game_id = @game_id and gi.global_id = @global_id";

                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
                parameters.Add (new MySqlParameter ("@game_id", gameId));
                parameters.Add (new MySqlParameter ("@global_id", globalId));
                parameters.Add (new MySqlParameter ("@quantity", quantity));

                return Db.WOK.GetDataSet (strQuery, parameters);
            }
        }

        public WOKItem.ItemApprovalStatus GetGameItemApprovalStatus (int gameId, int globalId)
        {
            string strQuery;

            strQuery = "SELECT gi.approval_status FROM wok.game_items gi WHERE gi.game_id = @game_id and gi.global_id = @global_id";
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@game_id", gameId));
            parameters.Add (new MySqlParameter ("@global_id", globalId));
            DataRow dr = Db.WOK.GetDataRow (strQuery, parameters);

            WOKItem.ItemApprovalStatus ret = WOKItem.ItemApprovalStatus.Denied;
            if (dr != null)
            {
                ret = (WOKItem.ItemApprovalStatus) dr["approval_status"];
            }
            return ret;
        }

        public bool UserAllowedGame (int gameId, int userId)
        {
            string sql = "CALL developer.add_player_game_allowed( @kaneva_user_id, @game_id );";
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@game_id", gameId));
            parameters.Add (new MySqlParameter ("@kaneva_user_id", userId));

            int returnNum = Db.Developer.ExecuteNonQuery (sql, parameters);

            return (returnNum == 1);

        }

        public bool IsUserAllowedGame (int gameId, int userId)
        {
            string strQuery = "SELECT kaneva_user_id FROM player_game_allowed where kaneva_user_id = @userId AND game_id = @gameId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@gameId", gameId));
            parameters.Add (new MySqlParameter ("@userId", userId));

            DataRow drResult = Db.Developer.GetDataRow (strQuery, parameters);
            return (drResult != null);
        }

        /// <summary>
        /// get the current access of the game, separated out for caching
        /// </summary>
        /// <param name="serverId"></param>
        /// <returns></returns>
        public int GetGameAccessByServerId (int serverId)
        {
            int ret = (int) eGAME_ACCESS.NOT_FOUND;

            string key = CentralCache.keyGameServerAccess (serverId);

            object oRet = CentralCache.Get (key);
            if (oRet == null)
            {
                string sql = "SELECT game_access_id, g.game_id FROM games g INNER JOIN game_servers gs ON g.game_id = gs.game_id " +
                             " WHERE gs.server_id = @serverId";

                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
                parameters.Add (new MySqlParameter ("@serverId", serverId));

                DataRow dr = Db.Developer.GetDataRow (sql, parameters);

                if (dr != null)
                {
                    ret = Convert.ToInt32 (dr["game_access_id"]);

                    // see if server in list of server ids
                    string serverIdsKey = CentralCache.keyGameServerIds (Convert.ToInt32 (dr["game_id"]));
                    List<int> serverIds = (List<int>) CentralCache.Get (serverIdsKey);
                    if (serverIds != null)
                    {
                        if (serverIds.IndexOf (serverId) == -1)
                        {
                            serverIds.Add (serverId);
                            CentralCache.Store (serverIdsKey, serverIds);
                        }
                        // else already in the list
                    }
                    else
                    {
                        serverIds = new List<int> ();
                        serverIds.Add (serverId);
                        CentralCache.Store (serverIdsKey, serverIds);
                    }

                    CentralCache.Store (key, ret);
                }
            }
            else
            {
                ret = Convert.ToInt32 (oRet);
            }

            return ret;

        }

        /// <summary>
        /// Update the game's balance
        /// <param name="gameId"></param>
        /// <param name="keiPointId"></param>
        /// <param name="amount"></param>
        /// </summary>
        public int AdjustGameBalance (int gameId, string keiPointId, Double amount)
        {
            string sql = "CALL apply_transaction_to_game_balance( @gameId, @amount, @keiPointId, @rc, @newBalance ); SELECT CAST(@rc as UNSIGNED INT) as rc, CAST(@newBalance as DECIMAL) as newBalance;";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@gameId", gameId));
            parameters.Add (new MySqlParameter ("@keiPointId", keiPointId));
            parameters.Add (new MySqlParameter ("@amount", amount));

            DataRow row = Db.Developer.GetDataRow (sql.ToString (), parameters);

            if (Convert.ToInt32 (row["rc"]) != 0) // 0 = ok, 1 = insufficient funds, -1 = error
            {
                throw new Exception ("Not enough currency in account!");
            }

            return 0;
        }

        public bool IsWokGame (int gameId)
        {
            string sql = "SELECT wok.is_wok_game (@gameId);";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@gameId", gameId));

            return Convert.ToBoolean (Db.WOK.GetScalar (sql, parameters));
        }

        /// <summary>
        /// InsertGiftedItem
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="userId"></param>
        /// <param name="globalId"></param>
        /// <returns></returns>
        public int InsertGiftedItem (int gameId, int userId, int globalId)
        {
            string sqlString = "INSERT INTO game_gifted_items ( game_id, user_id, global_id, created_date ) " +
                               "VALUES ( @gameId, @userId, @globalId, NOW() ) ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@gameId", gameId));
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@globalId", globalId));
            return Db.Developer.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// GetGiftedItemsCount
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>        
        public int GetGiftedItemsCount (int gameId, int userId)
        {
            string sqlString = "SELECT COUNT(*) AS item_count " +
                               "FROM   game_gifted_items " +
                               "WHERE  game_id = @gameId " +
                               "AND    user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@gameId", gameId));
            parameters.Add (new MySqlParameter ("@userId", userId));
            DataRow row = Db.Developer.GetDataRow (sqlString.ToString (), parameters);

            if (row == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32 (row["item_count"]);
            }
        }

        /// <summary>
        /// GetLeaderBoard
        /// </summary>
        /// <param name="leaderboardId"></param>
        /// <returns></returns>
        public Leaderboard GetLeaderBoard (int communityId)
        {
            string sqlString = "SELECT community_id, name, sort_by, format_for_value, column_name" +
                " FROM leaderboards" +
                " WHERE community_id = @communityId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            DataRow row = Db.Developer.GetDataRow (sqlString.ToString (), parameters);

            if (row == null)
            {
                return new Leaderboard ();
            }

            return new Leaderboard (Convert.ToInt32 (row["community_id"]), row["name"].ToString (), row["sort_by"].ToString (), Convert.ToInt32 (row["format_for_value"]),
                row["column_name"].ToString ());

        }

        /// <summary>
        /// LeaderBoard
        /// </summary>
        public PagedList<LeaderboardValue> GetLeaderBoardValues (int communityId, int userId, bool onlyFriends, int pageNumber, int pageSize)
        {
            // Find out sort of this leaderboard
            Leaderboard leaderboard = GetLeaderBoard (communityId);


            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            string strQuery = "";

            if (onlyFriends)
            {
                strQuery = "SELECT l.community_id, l.name, l.sort_by, lv.user_id, current_value, changed_date, " +
                        " l.column_name, u.username, u.display_name, com.thumbnail_square_path, com.thumbnail_medium_path, " +
                        " u.mature_profile, u.ustate, u.gender " +
                    " FROM developer.leaderboards l, developer.leaderboards_values lv " +
                    " INNER JOIN kaneva.friends f ON f.friend_id = lv.user_id " +
                    " INNER JOIN kaneva.users u ON u.user_id = f.friend_id " +
                    " INNER JOIN kaneva.communities_personal com ON u.user_id = com.creator_id " +
                    " WHERE lv.community_id = l.community_id " +
                    " AND l.community_id = @communityId " +
                    " AND f.user_id = @userId " +
                " UNION " +
                    " SELECT l.community_id, l.name, l.sort_by, lv.user_id, current_value, changed_date, " +
                        " l.column_name, u.username, u.display_name, com.thumbnail_square_path, com.thumbnail_medium_path, " +
                        " u.mature_profile, u.ustate, u.gender " +
                    " FROM developer.leaderboards l, developer.leaderboards_values lv " +
                    " INNER JOIN kaneva.users u ON u.user_id = lv.user_id " +
                    " INNER JOIN kaneva.communities_personal com ON u.user_id = com.creator_id " +
                    " WHERE lv.community_id = l.community_id " +
                    " AND l.community_id = @communityId " +
                    " AND lv.user_id = @userId ";

                parameters.Add (new MySqlParameter ("@userId", userId));
            }
            else
            {
                // Global leaderboard not supported yet
                strQuery = " SELECT l.community_id, l.name, l.sort_by, lv.user_id, current_value, changed_date, " +
                       " l.column_name, u.username, u.display_name, com.thumbnail_square_path, com.thumbnail_medium_path, " +
                       " u.mature_profile, u.ustate, u.gender " +
                   " FROM developer.leaderboards l, developer.leaderboards_values lv " +
                   " INNER JOIN kaneva.users u ON u.user_id = lv.user_id " +
                   " INNER JOIN kaneva.communities_personal com ON u.user_id = com.creator_id " +
                   " WHERE lv.community_id = l.community_id " +
                   " AND l.community_id = @communityId ";
            }

            string orderby = "current_value DESC";
            if (leaderboard.SortBy.Equals ("ASC"))
            {
                orderby = "current_value ASC";
            }

            parameters.Add (new MySqlParameter ("@communityId", communityId));
            PagedDataTable dt = Db.Developer.GetPagedDataTable (strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<LeaderboardValue> list = new PagedList<LeaderboardValue> ();
            list.TotalCount = dt.TotalCount;

            int rank = ((pageNumber - 1) * pageSize) + 1;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new LeaderboardValue (Convert.ToInt32 (row["community_id"]), Convert.ToInt32 (row["user_id"]),
                 FormatLBValue (leaderboard.FormatForValue, Convert.ToInt32 (row["current_value"])), Convert.ToDateTime (row["changed_date"]), row["name"].ToString (),
                 row["column_name"].ToString (), row["username"].ToString (), row["display_name"].ToString (),
                 row["thumbnail_square_path"].ToString (), row["thumbnail_medium_path"].ToString (),
                 Convert.ToInt32 (row["mature_profile"]).Equals (1), row["ustate"].ToString (),
                 rank, row["gender"].ToString ()
                 ));

                rank++;
            }
            return list;
        }

        /// <summary>
        /// FormatLBValue
        /// </summary>
        /// <param name="formatType"></param>
        /// <param name="currentValue"></param>
        /// <returns></returns>
        private string FormatLBValue (int formatType, int currentValue)
        {
            string formattedValue = currentValue.ToString ();

            switch ((int) formatType)
            {
                case (int) Leaderboard.FormatType.Currency:
                    {
                        formattedValue = ((Double) currentValue).ToString ("$#,##0.00#"); ;
                        break;
                    }
                case (int) Leaderboard.FormatType.Time:
                    {
                        TimeSpan t = TimeSpan.FromSeconds (currentValue);
                        formattedValue = string.Format ("{0:D2}:{1:D2}:{2:D2}",
                                                t.Hours,
                                                t.Minutes,
                                                t.Seconds);

                        break;
                    }
            }


            return formattedValue;
        }

        /// <summary>
        /// EarnLeaderBoard
        /// </summary>
        public void EarnLeaderBoard (int communityId, int userId, int newValue)
        {
            string sqlString = "INSERT INTO leaderboards_values (" +
                " community_id, user_id, current_value, changed_date" +
                ") VALUES (" +
                " @communityId,  @user_id,  @current_value,  NOW() " +
                ") ON DUPLICATE KEY UPDATE current_value =  @current_value, changed_date = NOW() ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            parameters.Add (new MySqlParameter ("@user_id", userId));
            parameters.Add (new MySqlParameter ("@current_value", newValue));
            Db.Developer.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// EarnLevel
        /// </summary>
        public void EarnLevel (int communityId, int userId, uint levelNumber, uint percentComplete, string title)
        {
            string sqlString = "INSERT INTO levels_values (" +
                " community_id, user_id, level_number, percent_complete, changed_date, title " +
                ") VALUES (" +
                " @community_id,  @user_id,  @level_number,  @percent_complete,  NOW(),  @title " +
                ") ON DUPLICATE KEY UPDATE level_number = @level_number, percent_complete = @percent_complete, title = @title, changed_date = NOW() ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@community_id", communityId));
            parameters.Add (new MySqlParameter ("@user_id", userId));
            parameters.Add (new MySqlParameter ("@level_number", levelNumber));
            parameters.Add (new MySqlParameter ("@percent_complete", percentComplete));
            parameters.Add (new MySqlParameter ("@title", title));
            Db.Developer.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// EarnTitle
        /// </summary>
        public void EarnTitle (int communityId, int userId, string title)
        {
            string sqlString = "INSERT INTO levels_values (" +
                " community_id, user_id, level_number, percent_complete, changed_date, title " +
                ") VALUES (" +
                " @community_id,  @user_id,  @level_number,  @percent_complete,  NOW(),  @title " +
                ") ON DUPLICATE KEY UPDATE title = @title, changed_date = NOW() ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@community_id", communityId));
            parameters.Add (new MySqlParameter ("@user_id", userId));
            parameters.Add (new MySqlParameter ("@level_number", 0));
            parameters.Add (new MySqlParameter ("@percent_complete", 0));
            parameters.Add (new MySqlParameter ("@title", title));
            Db.Developer.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// GetTitle
        /// </summary>
        public string GetTitle (int communityId, int userId)
        {
            string sqlString = "SELECT community_id, user_id, title " +
               " FROM levels_values " +
               " WHERE user_id = @UserId " +
               " AND community_id = @communityId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            DataRow row = Db.Developer.GetDataRow (sqlString.ToString (), parameters);

            if (row == null)
            {
                return "";
            }

            return row["title"].ToString ();
        }

        /// <summary>
        /// GetLevelValue
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="userId"></param>
        public LevelValue GetLevelValue (int communityId, int userId)
        {
            string sqlString = "SELECT community_id, user_id, level_number, percent_complete, changed_date, title " +
               " FROM levels_values " +
               " WHERE user_id = @UserId " +
               " AND community_id = @communityId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            DataRow row = Db.Developer.GetDataRow (sqlString.ToString (), parameters);

            if (row == null)
            {
                return new LevelValue ();
            }

            return new LevelValue (Convert.ToInt32 (row["community_id"]), Convert.ToInt32 (row["user_id"]), Convert.ToUInt32 (row["level_number"]), Convert.ToUInt32 (row["percent_complete"]), Convert.ToDateTime (row["changed_date"]),
                row["title"].ToString ());
        }

        /// <summary>
        /// SaveLeaderBoard
        /// </summary>
        public int SaveLeaderBoard (Leaderboard lb)
        {
            string sqlString = "INSERT INTO leaderboards (" +
                " community_id, name, sort_by, format_for_value, column_name" +
                ") VALUES (" +
                " @communityId, @name, @sort_by, @format_for_value, @column_name " +
                ") ON DUPLICATE KEY UPDATE name = @name, sort_by = @sort_by, " +
                " format_for_value = @format_for_value, column_name = @column_name ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", lb.CommunityId));
            parameters.Add (new MySqlParameter ("@name", lb.Name));
            parameters.Add (new MySqlParameter ("@sort_by", lb.SortBy));
            parameters.Add (new MySqlParameter ("@format_for_value", lb.FormatForValue));
            parameters.Add (new MySqlParameter ("@column_name", lb.ColumnName));
            return Db.Developer.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// ResetLeaderBoard
        /// </summary>
        public int ResetLeaderBoard (int communityId)
        {
            string sqlString = "DELETE FROM leaderboards_values " +
                " WHERE community_id = @communityId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            return Db.Developer.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// RemovePlayerFromLeaderBoard
        /// </summary>
        public int RemovePlayerFromLeaderBoard (int communityId, int userId)
        {
            string sqlString = "DELETE FROM leaderboards_values " +
                " WHERE community_id = @communityId " +
                " AND user_id = @userId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            parameters.Add (new MySqlParameter ("@userId", userId));
            return Db.Developer.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// EarnAchievement
        /// </summary>
        public void EarnAchievement (int userId, uint achievementId)
        {
            string sqlString = "INSERT IGNORE INTO achievements_earned (" +
                " achievement_id, user_id, created_date " +
                ") VALUES (" +
                " @achievementId,  @userId, NOW()) ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@achievementId", achievementId));
            parameters.Add (new MySqlParameter ("@userId", userId));
            Db.Developer.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// AchievementForUser
        /// </summary>
        public PagedList<AchievementEarned> AchievementForUser (int userId, int communityId, string sortBy, bool showAll, bool onlyFriends, int pageNumber, int pageSize)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            string sqlString = "";

            if (onlyFriends)
            {
                sqlString = "SELECT ae.achievement_id, ae.user_id, ae.created_date, " +
                            " a.name, a.description, a.image_url, " +
                            " u.username, u.display_name, com.thumbnail_small_path, com.thumbnail_medium_path, " +
                            " u.mature_profile, u.ustate, u.gender " +
                        " FROM developer.achievements a, developer.achievements_earned ae " +
                        " INNER JOIN kaneva.friends f ON f.friend_id = ae.user_id " +
                        " INNER JOIN kaneva.users u ON u.user_id = f.friend_id " +
                        " INNER JOIN kaneva.communities_personal com ON u.user_id = com.creator_id " +
                        " WHERE ae.achievement_id = a.achievement_id " +
                        " AND a.community_id = @communityId " +
                        " AND f.user_id = @userId " +
                    " UNION " +
                        " SELECT ae.achievement_id, ae.user_id, ae.created_date, " +
                            " a.name, a.description, a.image_url, " +
                            " u.username, u.display_name, com.thumbnail_small_path, com.thumbnail_medium_path, " +
                            " u.mature_profile, u.ustate, u.gender " +
                        " FROM developer.achievements a, developer.achievements_earned ae " +
                        " INNER JOIN kaneva.users u ON u.user_id = ae.user_id  " +
                        " INNER JOIN kaneva.communities_personal com ON u.user_id = com.creator_id " +
                        " WHERE ae.achievement_id = a.achievement_id " +
                        " AND a.community_id = @communityId " +
                        " AND ae.user_id = @userId ";
                parameters.Add (new MySqlParameter ("@userId", userId));
            }
            else
            {
                sqlString = "SELECT ae.achievement_id, ae.user_id, ae.created_date, " +
                             " a.name, a.description, a.image_url, " +
                           " u.username, u.display_name, com.thumbnail_small_path, com.thumbnail_medium_path, " +
                           " u.mature_profile, u.ustate, u.gender " +
                       " FROM developer.achievements a, developer.achievements_earned ae " +
                       " INNER JOIN kaneva.users u ON u.user_id = ae.friend_id " +
                       " INNER JOIN kaneva.communities_personal com ON u.user_id = com.creator_id " +
                       " WHERE ae.achievement_id = a.achievement_id " +
                       " AND a.community_id = @communityId ";
            }


            parameters.Add (new MySqlParameter ("@communityId", communityId));
            PagedDataTable dt = Db.Developer.GetPagedDataTable (sqlString, "created_date DESC", parameters, pageNumber, pageSize);

            PagedList<AchievementEarned> list = new PagedList<AchievementEarned> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new AchievementEarned (Convert.ToUInt32 (row["achievement_id"]), Convert.ToUInt32 (row["user_id"]), Convert.ToDateTime (row["created_date"]),
                    row["name"].ToString (), row["description"].ToString (), row["image_url"].ToString (),
                    row["username"].ToString (), row["display_name"].ToString (),
                    row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (),
                    Convert.ToInt32 (row["mature_profile"]).Equals (1), row["ustate"].ToString (),
                    row["gender"].ToString ()
                 ));
            }

            return list;
        }

        /// <summary>
        /// AchievementForApp
        /// </summary>
        public PagedList<Achievement> AchievementForApp (int communityId, string sortBy, int pageNumber, int pageSize)
        {
            string strQuery = "SELECT achievement_id, community_id, name, description, points, image_url " +
             " FROM achievements " +
             " WHERE community_id = @communityId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            PagedDataTable dt = Db.Developer.GetPagedDataTable (strQuery, "name ASC", parameters, pageNumber, pageSize);

            PagedList<Achievement> list = new PagedList<Achievement> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new Achievement (Convert.ToUInt32 (row["achievement_id"]), Convert.ToInt32 (row["community_id"]), row["name"].ToString (), row["description"].ToString (), Convert.ToUInt32 (row["points"]),
                    row["image_url"].ToString ()
                 ));
            }

            return list;
        }

        /// <summary>
        /// AchievementForApp
        /// </summary>
        public PagedList<UserAchievementData> GetUserAchievementsForApp (int communityId, int userId, string orderBy, int pageNumber, int pageSize)
        {
            string strQuery = "SELECT a.achievement_id, a.community_id, name, description, points, image_url, " +
                " user_id, created_date, ISNULL(created_date) as x " +
                " FROM achievements a " +
                " LEFT JOIN achievements_earned ae ON ae.achievement_id = a.achievement_id " +
                " AND user_id = @userId " +
                " WHERE a.community_id = @communityId ";

            if (orderBy.Length == 0)
            {
                orderBy = "x, name";
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            parameters.Add (new MySqlParameter ("@userId", userId));
            PagedDataTable dt = Db.Developer.GetPagedDataTable (strQuery, orderBy, parameters, pageNumber, pageSize);

            PagedList<UserAchievementData> list = new PagedList<UserAchievementData> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new UserAchievementData (Convert.ToUInt32 (row["achievement_id"]), Convert.ToInt32 (row["community_id"]),
                    row["name"].ToString (), row["description"].ToString (), Convert.ToUInt32 (row["points"]),
                    row["image_url"].ToString (), row["user_id"] == DBNull.Value ? 0 : Convert.ToInt32 (row["user_id"]),
                    row["created_date"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime (row["created_date"])
                 ));
            }

            return list;
        }

        public Dictionary<string, int> GetAchievementsSummaryForApp (int communityId, int userId)
        {
            Dictionary<string, int> dSum = new Dictionary<string, int> ();

            // Get the totals for the 3D App
            string sqlString = "SELECT community_id, SUM(points) AS total_points, COUNT(achievement_id) AS total_achievements " +
                " FROM achievements " +
                " WHERE community_id=@communityId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            DataRow row = Db.Developer.GetDataRow (sqlString.ToString (), parameters);

            if (row != null && !row["community_id"].Equals (DBNull.Value))
            {
                dSum.Add ("CommunityId", Convert.ToInt32 (row["community_id"]));
                dSum.Add ("TotalPoints", Convert.ToInt32 (row["total_points"]));
                dSum.Add ("TotalAchievements", Convert.ToInt32 (row["total_achievements"]));
            }
            else
            {
                dSum.Add ("CommunityId", communityId);
                dSum.Add ("TotalPoints", 0);
                dSum.Add ("TotalAchievements", 0);
            }

            // Now get the totals for the user in this 3D App
            sqlString = "SELECT user_id, COUNT(ae.achievement_id) as user_total_achievements, SUM(points) AS user_total_points " +
                " FROM achievements_earned ae " +
                " INNER JOIN achievements a ON a.achievement_id = ae.achievement_id " +
                " WHERE user_id=@userId AND community_id=@communityId;";

            parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            parameters.Add (new MySqlParameter ("@userId", userId));
            row = Db.Developer.GetDataRow (sqlString.ToString (), parameters);

            if (row != null)
            {
                dSum.Add ("UserId", userId);
                dSum.Add ("UserTotalPoints", row["user_total_points"] != DBNull.Value ? Convert.ToInt32 (row["user_total_points"]) : 0);
                dSum.Add ("UserTotalAchievements", Convert.ToInt32 (row["user_total_achievements"]));
            }

            return dSum;
        }

        public Dictionary<string, int> GetAchievementsSummaryForAllApps (int userId)
        {
            Dictionary<string, int> dSum = new Dictionary<string, int> ();

            // Get the totals for the 3D App
            string sqlString = "SELECT SUM(points) AS total_points, COUNT(a.achievement_id) AS total_achievements " +
                " FROM achievements a " +
                " WHERE community_id IN ( " +
                "   SELECT community_id " +
                "   FROM achievements a " +
                "   INNER JOIN achievements_earned ae ON ae.achievement_id = a.achievement_id " +
                "   WHERE user_id=@userId) OR community_id = " + (int) FameTypes.World + ";";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            DataRow row = Db.Developer.GetDataRow (sqlString.ToString (), parameters);

            if (row != null)
            {
                dSum.Add ("TotalPoints", row["total_points"] == DBNull.Value ? 0 : Convert.ToInt32 (row["total_points"]));
                dSum.Add ("TotalAchievements", row["total_achievements"] == DBNull.Value ? 0 : Convert.ToInt32 (row["total_achievements"]));
            }

            // Now get the totals for the user across all 3D App in which they have fame
            sqlString = "SELECT SUM(points) AS user_total_points, COUNT(a.achievement_id) AS user_total_achievements " +
                " FROM achievements a " +
                " INNER JOIN achievements_earned ae ON ae.achievement_id = a.achievement_id " +
                " WHERE user_id=@userId;";

            parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            row = Db.Developer.GetDataRow (sqlString.ToString (), parameters);

            if (row != null)
            {
                dSum.Add ("UserTotalPoints", row["user_total_points"] == DBNull.Value ? 0 : Convert.ToInt32 (row["user_total_points"]));
                dSum.Add ("UserTotalAchievements", row["user_total_achievements"] == DBNull.Value ? 0 : Convert.ToInt32 (row["user_total_achievements"]));
            }

            return dSum;
        }

        /// <summary>
        /// GetAchievement
        /// </summary>
        public Achievement GetAchievement (uint achievementId)
        {
            string strQuery = "SELECT achievement_id, community_id, name, description, points, image_url " +
             " FROM achievements " +
             " WHERE achievement_id = @achievementId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@achievementId", achievementId));
            DataRow row = Db.Developer.GetDataRow (strQuery, parameters);

            if (row == null)
            {
                return new Achievement ();
            }

            return new Achievement (Convert.ToUInt32 (row["achievement_id"]), Convert.ToInt32 (row["community_id"]),
                row["name"].ToString (), row["description"].ToString (), Convert.ToUInt32 (row["points"]),
                row["image_url"].ToString ());
        }

        /// <summary>
        /// GetAchievementAwards
        /// </summary>
        public List<AchievementAward> GetAchievementAwards(uint achievementId, string gender)
        {
            string cacheKey = "developer.achievement_awards." + achievementId + gender;
            List<AchievementAward> list = (List<AchievementAward>)DbCache.Cache[cacheKey];

            if (list == null)
            {

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();

                string strQuery = "SELECT achievement_award_id, achievement_id, t1.name, " +
                    " global_id, animation_id, gender, IF(quantity IS NULL, 0, quantity) AS quantity, " +
                    " IF(t2.name IS NULL, '', t2.name) AS new_title_name, rewards " +
                    " FROM developer.achievement_awards AS t1 LEFT JOIN wok.titles AS t2 " +
                    " ON t1.new_title_id = t2.title_id " +
                    " WHERE t1.achievement_id = @achievementId AND (t1.gender = @gender OR t1.gender = 'U')";

                parameters.Add(new MySqlParameter("@gender", gender));
                parameters.Add(new MySqlParameter("@achievementId", achievementId));
                DataTable dt = Db.Fame.GetDataTable(strQuery, parameters);

                list = new List<AchievementAward>();

                foreach (DataRow row in dt.Rows)
                {
                    list.Add(new AchievementAward(Convert.ToUInt32(row["achievement_award_id"]), Convert.ToUInt32(row["achievement_id"]), row["name"].ToString(), +
                        Convert.ToInt32(row["global_id"]), Convert.ToInt32(row["animation_id"]), row["gender"].ToString(), Convert.ToInt32(row["quantity"]), row["new_title_name"].ToString(), Convert.ToInt32(row["rewards"])
                    ));
                }
                if (dt.Rows.Count == 0) //no awards for that level
                {
                    list.Add(new AchievementAward(0, achievementId, "", 0, 0, gender, 0, "", 0));
                }

                DbCache.Cache.Insert(cacheKey, list, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return list;
        }


        [Obsolete("Use version in MySQLCommunityDao instead.", false)]
        public void InsertAPIAuth (int communityId, string consumerKey, string consumerSecret)
        {
            // Send the message
            string sql = "INSERT INTO api_authentication " +
                "(community_id, consumer_key, consumer_secret) VALUES (@communityId, @consumerKey, @consumerSecret) " +
                " ON DUPLICATE KEY UPDATE consumer_key =  @consumerKey, consumer_secret = @consumerSecret ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            parameters.Add (new MySqlParameter ("@consumerKey", consumerKey));
            parameters.Add (new MySqlParameter ("@consumerSecret", consumerSecret));
            Db.Master.ExecuteNonQuery (sql, parameters);
        }

        /// <summary>
        /// APIAuthentication
        /// </summary>
        /// <param name="consumerKey"></param>
        /// <returns></returns>
        [Obsolete("Use version in MySQLCommunityDao instead.", false)]
        public APIAuthentication GetAPIAuth (int gameId)
        {
            string sql = "SELECT aa.community_id, consumer_key, consumer_secret, game_id, send_blast, admin_override_blast " +
               " FROM kaneva.api_authentication aa, kaneva.communities_game cg " +
               " WHERE aa.community_id = cg.community_id " +
               " AND game_id = @gameId ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@gameId", gameId));

            //perform query returning single data row from the developer database
            DataRow row = Db.Master.GetDataRow (sql.ToString (), parameters);

            if (row == null)
            {
                return new APIAuthentication ();
            }

            //populate new instance of Game Object
            return new APIAuthentication (Convert.ToInt32 (row["community_id"]), row["consumer_key"].ToString (), row["consumer_secret"].ToString ()
                , Convert.ToInt32 (row["game_id"]), row["admin_override_blast"].ToString ().Equals ("Y"), row["send_blast"].ToString ().Equals ("Y")
                );
        }

        /// <summary>
        /// APIAuthentication
        /// </summary>
        /// <param name="consumerKey"></param>
        /// <returns></returns>
        [Obsolete("Use version in MySQLCommunityDao instead.", false)]
        public APIAuthentication GetAPIAuth (string consumerKey, string consumerSecret)
        {
            APIAuthentication apiAuth = (APIAuthentication) CentralCache.Get (CentralCache.keyAPIAuthAuthentication + "wS" + consumerKey.ToString ());

            if (apiAuth != null)
            {
                // Found in cache
                return apiAuth;
            }

            string sql = "SELECT aa.community_id, consumer_key, consumer_secret, game_id, send_blast, admin_override_blast " +
                " FROM kaneva.api_authentication aa, kaneva.communities_game cg " +
                " WHERE aa.community_id = cg.community_id " +
                " AND consumer_key = @consumerKey  " +
                " AND consumer_secret = @consumerSecret";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@consumerKey", consumerKey));
            parameters.Add (new MySqlParameter ("@consumerSecret", consumerSecret));

            //perform query returning single data row from the developer database
            DataRow row = Db.Master.GetDataRow (sql.ToString (), parameters);

            if (row == null)
            {
                return new APIAuthentication ();
            }

            //populate new instance of Game Object
            apiAuth = new APIAuthentication (Convert.ToInt32 (row["community_id"]), row["consumer_key"].ToString (), row["consumer_secret"].ToString ()
                , Convert.ToInt32 (row["game_id"]), row["admin_override_blast"].ToString ().Equals ("Y"), row["send_blast"].ToString ().Equals ("Y")
                );

            CentralCache.Store (CentralCache.keyAPIAuthAuthentication + "wS" + consumerKey.ToString (), apiAuth, TimeSpan.FromMinutes (5));
            return apiAuth;
        }

        /// <summary>
        /// APIAuthentication
        /// </summary>
        /// <param name="consumerKey"></param>
        /// <returns></returns>
        [Obsolete("Use version in MySQLCommunityDao instead.", false)]
        public APIAuthentication GetAPIAuth (string consumerKey)
        {
            APIAuthentication apiAuth = (APIAuthentication) CentralCache.Get (CentralCache.keyAPIAuthAuthentication + consumerKey.ToString ());

            if (apiAuth != null)
            {
                // Found in cache
                return apiAuth;
            }

            string sql = "SELECT aa.community_id, consumer_key, consumer_secret, game_id, send_blast, admin_override_blast " +
                " FROM kaneva.api_authentication aa, kaneva.communities_game cg " +
                " WHERE aa.community_id = cg.community_id " +
                " AND consumer_key = @consumerKey  ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@consumerKey", consumerKey));

            //perform query returning single data row from the developer database
            DataRow row = Db.Master.GetDataRow (sql.ToString (), parameters);

            if (row == null)
            {
                return new APIAuthentication ();
            }


            //populate new instance of Game Object
            apiAuth = new APIAuthentication (Convert.ToInt32 (row["community_id"]), row["consumer_key"].ToString (), row["consumer_secret"].ToString ()
                , Convert.ToInt32 (row["game_id"]), row["admin_override_blast"].ToString ().Equals ("Y"), row["send_blast"].ToString ().Equals ("Y")
                );

            CentralCache.Store (CentralCache.keyAPIAuthAuthentication + consumerKey.ToString (), apiAuth, TimeSpan.FromMinutes (5));
            return apiAuth;
        }

        /// <summary>
        /// UpdateAPIAuth
        /// </summary>
        public void UpdateAPIAuth (int communityId, string consumerKey, string consumerSecret)
        {

        }

        /// <summary>
        /// SaveAchievement
        /// </summary>
        public uint SaveAchievement (Achievement achievement)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            string sqlString = "";

            // Add new achievement
            if (achievement.AchievementsId == 0)
            {
                sqlString = "INSERT INTO achievements (" +
                    " community_id, name, description, points, image_url" +
                    ") VALUES (" +
                    " @communityId, @name, @description, @points, @imageUrl)";
                parameters.Add (new MySqlParameter ("@communityId", achievement.CommunityId));
                parameters.Add (new MySqlParameter ("@name", achievement.Name));
                parameters.Add (new MySqlParameter ("@description", achievement.Description));
                parameters.Add (new MySqlParameter ("@points", achievement.Points));
                parameters.Add (new MySqlParameter ("@imageUrl", achievement.ImageUrl));
                achievement.AchievementsId = (uint) Db.Developer.ExecuteIdentityInsert (sqlString, parameters);
            }
            else // update existing achievement
            {
                sqlString = "UPDATE achievements SET " +
                    " community_id=@communityId, name=@name, description=@description, " +
                    " points=@points, image_url=@imageUrl " +
                    " WHERE achievement_id = @achievementId";

                parameters.Add (new MySqlParameter ("@achievementId", achievement.AchievementsId));
                parameters.Add (new MySqlParameter ("@communityId", achievement.CommunityId));
                parameters.Add (new MySqlParameter ("@name", achievement.Name));
                parameters.Add (new MySqlParameter ("@description", achievement.Description));
                parameters.Add (new MySqlParameter ("@points", achievement.Points));
                parameters.Add (new MySqlParameter ("@imageUrl", achievement.ImageUrl));
                Db.Developer.ExecuteNonQuery (sqlString, parameters);
            }



            return achievement.AchievementsId;
        }

        /// <summary>
        /// UpdateAchievementThumbnail
        /// </summary>
        public int UpdateAchievementThumbnail (uint achievementId, string imageUrl)
        {
            string sqlString = "UPDATE achievements SET " +
                    " image_url=@imageUrl " +
                    " WHERE achievement_id=@achievementId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@achievementId", achievementId));
            parameters.Add (new MySqlParameter ("@imageUrl", imageUrl));
            return Db.Developer.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// GetTotalAchievementPoints
        /// </summary>
        public int GetTotalAchievementPoints (int communityId, string filter)
        {
            string sqlString = "SELECT SUM(points) " +
                    " FROM achievements " +
                    " WHERE community_id=@communityId ";

            if (filter.Length > 0)
            {
                sqlString += " AND " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));

            Object result = Db.Developer.GetScalar (sqlString, parameters);

            return (result == DBNull.Value ? 0 : Convert.ToInt32 (result));
        }

        /// <summary>
        /// DeleteAchievement
        /// </summary>
        public int DeleteAchievement (uint achievementId)
        {
            // Delete achievements that belong to players
            string sqlString = "DELETE FROM achievements_earned " +
                " WHERE achievement_id = @achievementId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@achievementId", achievementId));
            Db.Developer.ExecuteNonQuery (sqlString, parameters);


            // Delete from achievements table
            sqlString = "DELETE FROM achievements " +
                " WHERE achievement_id = @achievementId ";

            parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@achievementId", achievementId));
            return Db.Developer.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// GetGamesInWhichUserHasFame
        /// </summary>
        public PagedList<Community> GetGamesInWhichUserHasFame (int userId, int pageNumber, int pageSize, string orderBy)
        {
            //string strQuery = "SELECT c.community_id, c.name, c.thumbnail_small_path, c.thumbnail_medium_path, is_adult, name_no_spaces " +
            //    " FROM kaneva.communities c " +
            //    " INNER JOIN achievements a ON a.community_id = c.community_id " +
            //    " INNER JOIN achievements_earned ae ON ae.achievement_id = a.achievement_id " +
            //    " WHERE ae.user_id = @userId OR c.community_id = " + (int)FameTypes.World +
            //    " GROUP BY c.community_id ";

            // Performance update, old is above
            string strQuery = "SELECT c.community_id, c.name, c.thumbnail_small_path, c.thumbnail_medium_path, " +
                " is_adult, name_no_spaces, 1 AS ForceOrder " +
                " FROM kaneva.communities c " +
                     " INNER JOIN achievements a ON a.community_id = c.community_id  " +
                     " INNER JOIN achievements_earned ae ON ae.achievement_id = a.achievement_id  " +
                " WHERE ae.user_id = @userId " +
                " AND c.community_id <> " + (int) FameTypes.World +
                " UNION DISTINCT " +
                " SELECT c.community_id, c.name, c.thumbnail_small_path, c.thumbnail_medium_path, " +
                " is_adult, name_no_spaces, 0 AS ForceOrder " +
                " FROM kaneva.communities c  " +
                " WHERE c.community_id = " + (int) FameTypes.World;


            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            PagedDataTable dt = Db.Developer.GetPagedDataTable (strQuery, orderBy, parameters, pageNumber, pageSize);

            PagedList<Community> list = new PagedList<Community> ();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                Community c = new Community ();
                c.CommunityId = Convert.ToInt32 (row["community_id"]);
                c.Name = row["name"].ToString ();
                c.ThumbnailSmallPath = row["thumbnail_small_path"].ToString ();
                c.ThumbnailMediumPath = row["thumbnail_medium_path"].ToString ();
                c.IsAdult = row["is_adult"].ToString ();
                c.NameNoSpaces = row["name_no_spaces"].ToString ();

                list.Add (c);
            }
            return list;
        }

        public int ResetAchievements (int communityId)
        {
            string sqlString = "DELETE ae.* FROM achievements_earned ae " +
                " INNER JOIN achievements a ON a.achievement_id = ae.achievement_id " +
                " WHERE a.community_id = @communityId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@communityId", communityId));
            return Db.Developer.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// Search Apartments
        /// </summary>
        public DataTable SearchApartments (bool bHasAPSubscription, bool bShowOnlyAP, Int32 pageNumber, Int32 pageSize, String searchString, String orderBy, ref int totalCount)
        {
            Sphinx sphinx = new Sphinx ();
            string communityIds = "";
            string newSearchString = "";

            searchString = sphinx.StripSphinxSearchString(searchString);

            using (ConnectionBase connection = new PersistentTcpConnection (sphinx.Host, sphinx.Port))
            {
                connection.ConnectionTimeout = 5000;

                if (searchString != null && searchString.Length > 0)
                {
                    // Escape some strings sphinx has issues with \()|-!@~/^$*
                    searchString = sphinx.CleanSphinxSearchString (searchString);

                    char[] splitter = { ' ' };
                    string[] arKeywords = null;

                    // Did they enter multiples?
                    arKeywords = searchString.Split (splitter);

                    if (arKeywords.Length > 1)
                    {
                        newSearchString = "";
                        for (int j = 0; j < arKeywords.Length; j++)
                        {
                            // Do a sphinx OR for now
                            if (j < arKeywords.Length - 1)
                            {
                                newSearchString += "@(name,description,display_name,creator_username,keywords) " + arKeywords[j].ToString () + " | ";
                            }
                            else
                            {
                                newSearchString += "@(name,description,display_name,creator_username,keywords) " + arKeywords[j].ToString ();
                            }
                        }
                    }
                    else
                    {
                        newSearchString = "@(name,description,display_name,creator_username,keywords) " + searchString;
                    }
                }

                SearchQuery searchQuery = new SearchQuery (newSearchString);

                // Set match mode to SPH_MATCH_EXTENDED2
                searchQuery.MatchMode = MatchMode.Extended2;

                // Add Sphinx index name to list
                searchQuery.Indexes.Add ("wok_apartment_cache_srch_idx");

                // Set up paging
                searchQuery.Limit = pageSize;
                searchQuery.Offset = (pageNumber - 1) * pageSize;

                // Create search command object
                SearchCommand searchCommand = new SearchCommand (connection);

                // Add newly created search query object to query list
                searchCommand.QueryList.Add (searchQuery);


                // Filtering
                if (!bHasAPSubscription)
                {
                    //  c.pass_group_id <> 1 
                    searchQuery.AttributeFilters.Add ("pass_group_id", (uint) 1, true);
                }
                else
                {
                    // Include mature
                    // Do they want only mature?
                    if (bShowOnlyAP)
                    {
                        //  c.pass_group_id = 1 
                        searchQuery.AttributeFilters.Add ("pass_group_id", (uint) 1, false);
                    }
                }


                // Sorting
                if (searchString != null)
                {
                    searchQuery.SortMode = ResultsSortMode.Relevance;
                }
                else
                {
                    if (orderBy != null && orderBy.Length > 0)
                    {
                        if (orderBy.Trim ().Length.Equals (0))
                        {
                            searchQuery.SortMode = ResultsSortMode.Relevance;
                        }
                        else
                        {
                            if (orderBy.ToUpper ().Contains ("ASC"))
                            {
                                searchQuery.SortMode = ResultsSortMode.AttributeAscending;
                            }
                            else
                            {
                                searchQuery.SortMode = ResultsSortMode.AttributeDescending;
                            }

                            // Strip ASC, DESC
                            searchQuery.SortBy = orderBy.Replace ("ASC", "").Replace ("DESC", "").Trim ();
                        }
                    }
                    else
                    {
                        // Default order by
                        searchQuery.SortMode = ResultsSortMode.Extended;
                        searchQuery.SortBy = "count DESC, wok_raves DESC";
                    }
                }

                // Execute command on server and obtain results
                try
                {
                    searchCommand.Execute ();
                }
                catch (Exception exc)
                {
                    m_logger.Error ("Error calling Sphinx " + sphinx.Host + ":" + sphinx.Port, exc);
                }

                int resultCount = 0;

                // Build result IN clause for MySQL
                foreach (SearchQueryResult result in searchCommand.Result.QueryResults)
                {
                    foreach (Match match in result.Matches)
                    {

                        if (communityIds.Length > 0)
                        {
                            communityIds = communityIds + "," + match.DocumentId.ToString ();
                        }
                        else
                        {
                            communityIds = match.DocumentId.ToString ();
                        }
                    }

                    // Note this will grab the last total count in collection, ok for this case, but not best place to do this
                    totalCount = (result.TotalFound > searchQuery.MaxMatches ? searchQuery.MaxMatches : result.TotalFound);
                    resultCount = result.Matches.Count;
                }

                // Nothing found in Sphinx
                if (communityIds.Length.Equals (0))
                {
                    return new DataTable ();
                }

                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

                string strSelect = " SELECT community_id, name, description, number_of_diggs, number_of_members, is_public, is_adult, " +
                      " pass_group_id, creator_id, display_name, zone_index, zone_instance_id, " +
                      " population AS `count`, wok_raves " +
                      " FROM wok.worlds_cache c " +
                      "  WHERE c.community_id in (" + communityIds + ")" +
                 " ORDER BY FIELD(community_id," + communityIds + ") ";

                DataTable pdtResult = Db.Master.GetDataTable (strSelect, parameters);
                return pdtResult;
            }


        }

        /// <summary>
        /// Search the worlds that start with
        /// </summary>
        public PagedList<Community> SearchWorldPrepopulation (string worldStartsWith, string orderby, int pageNumber, int pageSize)
        {
            // Friends I invited
            string strQuery = "SELECT name from communities " +
            " WHERE status_id = 1 AND name LIKE '" + worldStartsWith + "%'";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            PagedDataTable dt = Db.Search.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<Community> list = new PagedList<Community>();
            list.TotalCount = dt.TotalCount;
            Community community;

            foreach (DataRow row in dt.Rows)
            {
                community = new Community();
                community.Name = row ["name"].ToString ();
                list.Add(community);
            }
            return list;
        }

        /// <summary>
        /// Search Hangouts
        /// </summary>
        public DataTable SearchHangouts (bool bHasAPSubscription, bool bShowOnlyAP, bool bOnlyPopulated, bool bOnlyEmptyPopulation, Int32 pageNumber, Int32 pageSize, String searchString,
            int placeType, bool myCommunitiesOnly, int userId, String orderBy, int wokGameId, string country, int minAge, int maxAge, ref int totalCount)
        {
            Sphinx sphinx = new Sphinx ();
            string communityIds = "";
            string newSearchString = "";

            searchString = sphinx.StripSphinxSearchString(searchString);

            using (ConnectionBase connection = new PersistentTcpConnection (sphinx.Host, sphinx.Port))
            {
                connection.ConnectionTimeout = 5000;

                if (searchString != null && searchString.Length > 0)
                {
                    // Escape some strings sphinx has issues with \()|-!@~/^$*
                    searchString = sphinx.CleanSphinxSearchString (searchString);

                    char[] splitter = { ' ' };
                    string[] arKeywords = null;

                    // Did they enter multiples?
                    arKeywords = searchString.Split (splitter);

                    if (arKeywords.Length > 1)
                    {
                        newSearchString = "";
                        for (int j = 0; j < arKeywords.Length; j++)
                        {
                            // Do a sphinx OR for now
                            if (j < arKeywords.Length - 1)
                            {
                                newSearchString += "@(name,description,username,display_name,keywords) " + arKeywords[j].ToString () + " | ";
                            }
                            else
                            {
                                newSearchString += "@(name,description,username,display_name,keywords) " + arKeywords[j].ToString ();
                            }
                        }
                    }
                    else
                    {
                        newSearchString = "@(name,description,username,display_name,keywords) " + searchString;
                    }
                }

                // Only get pulbic
                if (!myCommunitiesOnly)
                {
                    if (newSearchString.Length > 0)
                    {
                        newSearchString = newSearchString + " & ";
                    }
                    newSearchString = newSearchString + " @is_public Y ";
                }

                // Country filter passed in?
                if (country.Trim ().Length > 0)
                {
                    if (newSearchString.Length > 0)
                    {
                        newSearchString = newSearchString + " & ";
                    }
                    newSearchString = newSearchString + " @country " + country;
                }


                SearchQuery searchQuery = new SearchQuery (newSearchString);

                // Set match mode to SPH_MATCH_EXTENDED2
                searchQuery.MatchMode = MatchMode.Extended2;

                // Add Sphinx index name to list
                searchQuery.Indexes.Add ("wok_hangout_cache_srch_idx");

                // Set up paging
                searchQuery.Limit = pageSize;
                searchQuery.Offset = (pageNumber - 1) * pageSize;

                // Create search command object
                SearchCommand searchCommand = new SearchCommand (connection);

                // Add newly created search query object to query list
                searchCommand.QueryList.Add (searchQuery);

                // Filtering

                // Get the ones with pop count not 0?
                if (bOnlyPopulated)
                {
                    searchQuery.AttributeFilters.Add ("pop_count", (uint) 0, true);
                }

                // Get the ones with pop count not 0?
                if (bOnlyEmptyPopulation)
                {
                    searchQuery.AttributeFilters.Add ("pop_count", (uint) 0, false);
                }

                if (!bHasAPSubscription) 
                {
                    //  c.pass_group_id <> 1 
                    searchQuery.AttributeFilters.Add ("pass_group_id", (uint) 1, true);
                }
                else
                {
                    // Include mature
                    // Do they want only mature?
                    if (bShowOnlyAP)
                    {
                        //  c.pass_group_id = 1 
                        searchQuery.AttributeFilters.Add ("pass_group_id", (uint) 1, false);
                    }
                }

                if (minAge > 0 && maxAge > 0)
                {
                    searchQuery.AttributeFilters.Add ("age", (uint) minAge, (uint) maxAge, false);
                }
                else if (minAge > 0)
                {
                    searchQuery.AttributeFilters.Add ("age", (uint) minAge, 150, false);
                }
                else if (maxAge > 0)
                {
                    searchQuery.AttributeFilters.Add ("age", 0, (uint) maxAge, false);
                }

                if (placeType > 0)
                {
                    searchQuery.AttributeFilters.Add ("place_type_id", (uint) placeType, false);
                }

                if (myCommunitiesOnly)
                {
                    List<int> arMyCommIds = new List<int> ();

                    string sqlMyCommunities = " SELECT community_id FROM kaneva.community_members cm " +
                        " WHERE cm.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE +
                        " AND cm.user_id = " + userId +
                         " ORDER BY cm.status_id ASC, cm.last_visited_at DESC ";
                    DataTable dtComms = Db.Slave.GetDataTable (sqlMyCommunities, new List<IDbDataParameter> ());

                    foreach (DataRow dr in dtComms.Rows)
                    {
                        arMyCommIds.Add ((int) Convert.ToInt32 (dr["community_id"]));
                    }

                    if (arMyCommIds.Count > 0)
                    {
                        searchQuery.AttributeFilters.Add ("community_id_filter", arMyCommIds, false);
                    }

                }
                else
                {
                    // Get all the privates I am a member of as well

                }


                // Sorting
                if (searchString.Length > 0)
                {
                    searchQuery.SortMode = ResultsSortMode.Relevance;
                }
                else
                {
                    if (orderBy != null && orderBy.Length > 0)
                    {
                        if (orderBy.Trim ().Length.Equals (0))
                        {
                            searchQuery.SortMode = ResultsSortMode.Relevance;
                        }
                        else
                        {
                            if (orderBy.ToUpper ().Contains ("ASC"))
                            {
                                searchQuery.SortMode = ResultsSortMode.AttributeAscending;
                            }
                            else
                            {
                                searchQuery.SortMode = ResultsSortMode.AttributeDescending;
                            }

                            // Strip ASC, DESC
                            searchQuery.SortBy = orderBy.Replace ("ASC", "").Replace ("DESC", "").Trim ();
                        }
                    }
                    else
                    {
                        // Default order by
                        searchQuery.SortMode = ResultsSortMode.Extended;
                        searchQuery.SortBy = "pop_count DESC, wok_raves DESC";
                    }
                }

                // Execute command on server and obtain results
                try
                {
                    searchCommand.Execute ();
                }
                catch (Exception exc)
                {
                    m_logger.Error ("Error calling Sphinx " + sphinx.Host + ":" + sphinx.Port, exc);
                }

                int resultCount = 0;

                // Build result IN clause for MySQL
                foreach (SearchQueryResult result in searchCommand.Result.QueryResults)
                {
                    foreach (Match match in result.Matches)
                    {

                        if (communityIds.Length > 0)
                        {
                            communityIds = communityIds + "," + match.DocumentId.ToString ();
                        }
                        else
                        {
                            communityIds = match.DocumentId.ToString ();
                        }
                    }

                    // Note this will grab the last total count in collection, ok for this case, but not best place to do this
                    totalCount = (result.TotalFound > searchQuery.MaxMatches ? searchQuery.MaxMatches : result.TotalFound);
                    resultCount = result.Matches.Count;
                }

                // Nothing found in Sphinx
                if (communityIds.Length.Equals (0))
                {
                    // This ensures we return a DataTable with the correct columns defined so we can add more records easier later.
                    communityIds = "-1";
                }

                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

                string strSelect = " SELECT STPURL, zone_type as zoneType, keywords, over_21_required, " +
                            " display_name, c.community_id, c.place_type_id, name, description, number_of_members, is_public, is_adult, pass_group_id, creator_id, " +
                            " c.name_no_spaces, c.thumbnail_small_path, c.creator_thumbnail_small_path, " +
                            " creator_username AS username, created_date, number_of_diggs, " +
                           " zone_index, zone_instance_id, wok_raves, population AS `count`, population " +
                      " FROM wok.worlds_cache c " +
                      " WHERE c.community_id in (" + communityIds + ")" +
                 " ORDER BY FIELD(community_id," + communityIds + ") ";

                DataTable pdtResult = Db.Master.GetDataTable (strSelect, parameters);
                return pdtResult;
            }


        }

        /// <summary>
        /// Search Communities
        /// </summary>
        public DataTable Search3DApps(bool onlyAccessPass, bool bGetMature, string searchString,
            bool bOnlyWithPhotos, string country, int[] communityTypeIds,
            string orderBy, int pageNumber, int pageSize, int iPlaceTypeId, bool myCommunitiesOnly, bool onlyOwned, int userId, int wokGameId, ref int totalNumRecords)
        {
            Sphinx sphinx = new Sphinx ();
            string newSearchString = "";
            string communityIds = "";
            string sortByIds = "";
            string myCommunitiesWhere = "";

            searchString = sphinx.StripSphinxSearchString(searchString);

            using (ConnectionBase connection = new PersistentTcpConnection (sphinx.Host, sphinx.Port))
            {
                connection.ConnectionTimeout = 5000;

                // Passed in search string
                if (searchString.Length > 0)
                {
                    // Escape some strings sphinx has issues with \()|-!@~/^$*
                    searchString = sphinx.CleanSphinxSearchString (searchString);

                    char[] splitter = { ' ' };
                    string[] arKeywords = null;

                    // Did they enter multiples?
                    arKeywords = searchString.Split (splitter);

                    if (arKeywords.Length > 1)
                    {
                        newSearchString = "";
                        for (int j = 0; j < arKeywords.Length; j++)
                        {
                            // Do a sphinx OR for now
                            if (j < arKeywords.Length - 1)
                            {
                                newSearchString += "@(name,description,creator_username,keywords) " + arKeywords[j].ToString () + " | ";
                            }
                            else
                            {
                                newSearchString += "@(name,description,creator_username,keywords) " + arKeywords[j].ToString ();
                            }
                        }
                    }
                    else
                    {
                        newSearchString = "@(name,description,creator_username,keywords) " + searchString;
                    }
                }

                SearchQuery searchQuery = new SearchQuery (newSearchString);

                // Set match mode to SPH_MATCH_EXTENDED2
                searchQuery.MatchMode = MatchMode.Extended2;

                // Add Sphinx index name to list
                searchQuery.Indexes.Add ("communities_srch_idx");

                // Set up paging
                searchQuery.Limit = pageSize;
                searchQuery.Offset = (pageNumber - 1) * pageSize;

                // Create search command object
                SearchCommand searchCommand = new SearchCommand (connection);

                // Add newly created search query object to query list
                searchCommand.QueryList.Add (searchQuery);

                // Place type filter
                if (iPlaceTypeId > 0)
                {
                    searchQuery.AttributeFilters.Add ("place_type_id", (uint) iPlaceTypeId, false);
                    myCommunitiesWhere += " AND c.place_type_id = " + iPlaceTypeId + " ";
                }

                // This excludes the communities to hold purchased zone. Used an array to pass 
                int zonePurchasePlaceTypeId = 99;
                searchQuery.AttributeFilters.Add ("place_type_id", zonePurchasePlaceTypeId, true);
                myCommunitiesWhere += " AND c.place_type_id <> " + zonePurchasePlaceTypeId + " ";

                // Mature
                if (!bGetMature)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }

                    searchQuery.Query = searchQuery.Query + " @is_adult N";
                    myCommunitiesWhere += " AND c.is_adult = 'N' ";
                }

                if (bGetMature && onlyAccessPass)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }

                    searchQuery.Query = searchQuery.Query + " @is_adult Y";
                    myCommunitiesWhere += " AND c.is_adult = 'Y' ";
                }

                // Filter by owner country
                if (country.Length > 0)
                {
                    // Country
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @country " + country;
                }

                // Filter by owner country
                if (bOnlyWithPhotos)
                {
                    // Country
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }
                    searchQuery.Query = searchQuery.Query + " @has_thumbnail Y";
                    myCommunitiesWhere += " AND c.has_thumbnail = 'Y' ";
                }

                // Community type
                if (communityTypeIds.Length > 0)
                {
                    searchQuery.AttributeFilters.Add("community_type_id", communityTypeIds, false);
                    myCommunitiesWhere += " AND c.community_type_id IN (" + string.Join(",", communityTypeIds) + ") ";
                }

                if (myCommunitiesOnly)
                {
                    List<int> arMyCommIds = new List<int> ();

                    // http://wiki.intranet.kaneva.com/mediawiki/index.php/2013_Roadmap_for_Consumers#Places_menu_phases
                    // 7.My Worlds tab: sort all Worlds in 'My Worlds' from last visited regardless of whether user is the owner, moderator or a member of the world. 
                    // OLD " ORDER BY cm.last_visited_at DESC ";
                    // NEW 

                    // Order by Owned, Moderated, Subscribed
                    string sqlMyCommunities = " SELECT c.community_id FROM kaneva.community_members cm " +
                        " INNER JOIN kaneva.communities c ON cm.community_id = c.community_id AND c.is_personal = 0 AND c.status_id = 1 " + myCommunitiesWhere + 
                        " WHERE cm.status_id = " + (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE +
                        (onlyOwned ? " AND cm.account_type_id = " + (UInt32)CommunityMember.CommunityMemberAccountType.OWNER : "") +
                        " AND cm.user_id = " + userId +
                         " ORDER BY cm.last_visited_at DESC ";
                    DataTable dtComms = Db.Slave.GetDataTable (sqlMyCommunities, new List<IDbDataParameter> ());

                    foreach (DataRow dr in dtComms.Rows)
                    {
                        arMyCommIds.Add ((int) Convert.ToInt32 (dr["community_id"]));
                        sortByIds += dr["community_id"].ToString () + ",";
                    }
                    sortByIds = sortByIds.TrimEnd (',');

                    if (arMyCommIds.Count > 0)
                    {
                        searchQuery.AttributeFilters.Add ("community_id_filter", arMyCommIds, false);
                    }

                }
                else
                {
                    // https://kaneva.atlassian.net/browse/ED-8072
                    // Exclude child worlds from search results.
                    searchQuery.AttributeFilters.Add("is_child_world", (uint)1, true);
                }

                // Community type 3DAPP
                // Now always mix them
                // searchQuery.AttributeFilters.Add ("community_type_id", (uint) 3, false);

                // Sorting
                if (orderBy.Trim ().Length.Equals (0))
                {
                    searchQuery.SortMode = ResultsSortMode.Relevance;

                    //name,description,creator_username,keywords   
                    //searchQuery.SortMode = ResultsSortMode.Expression;
                    searchQuery.FieldWeights.Add("name", 5);
                    searchQuery.FieldWeights.Add("description", 2);
                    searchQuery.FieldWeights.Add("creator_username", 1);
                    searchQuery.FieldWeights.Add("keywords", 1);
                }
                else
                {
                    if (orderBy.ToUpper ().Contains ("ASC"))
                    {
                        searchQuery.SortMode = ResultsSortMode.AttributeAscending;
                    }
                    else
                    {
                        searchQuery.SortMode = ResultsSortMode.AttributeDescending;
                    }

                    // Strip ASC, DESC
                    searchQuery.SortBy = orderBy.Replace ("ASC", "").Replace ("DESC", "").Trim ();
                }

                // If not searching for owned communities, we already have the correct ids and can skip sphinx
                if (myCommunitiesOnly && string.IsNullOrWhiteSpace(searchString))
                {
                    communityIds = sortByIds;
                }
                else
                {
                    // Execute command on server and obtain results
                    try
                    {
                        searchCommand.Execute();
                    }
                    catch (Exception exc)
                    {
                        m_logger.Error("Error calling Sphinx " + sphinx.Host + ":" + sphinx.Port, exc);
                    }

                    // Build result IN clause for MySQL
                    foreach (SearchQueryResult result in searchCommand.Result.QueryResults)
                    {
                        foreach (Match match in result.Matches)
                        {
                            if (communityIds.Length > 0)
                            {
                                communityIds = communityIds + "," + match.DocumentId.ToString();
                            }
                            else
                            {
                                communityIds = match.DocumentId.ToString();
                            }
                        }

                        // Note this will grab the last total count in collection, ok for this case, but not best place to do this
                        totalNumRecords = (result.TotalFound > searchQuery.MaxMatches ? searchQuery.MaxMatches : result.TotalFound);
                    }

                    // Sort by Sphinx results, unless we've already specified
                    sortByIds = (string.IsNullOrEmpty(sortByIds) ? communityIds : sortByIds);
                }

                if (string.IsNullOrEmpty (communityIds))
                {
                    DataSet ds = new DataSet ();
                    DataTable dt = new DataTable ();
                    ds.Tables.Add (dt);
                    return dt;
                }

                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
                parameters.Add(new MySqlParameter("@wokGameId", wokGameId));

                string strSelect = " SELECT COALESCE(g.game_id, 0) as game_id, c.community_id, 10 as zoneType, " +
                    " c.name, c.description, c.is_public, c.is_adult, c.keywords, c.over_21_required, " +
                    " creator_c.creator_id, u.username, c.created_date, " +
                    " c.name_no_spaces, c.thumbnail_small_path, " +
                    " creator_c.name_no_spaces as creator_name_no_spaces, creator_c.thumbnail_small_path as creator_thumbnail_small_path, " +
                    " cs.number_of_members, cs.number_of_views, cs.number_times_shared, cs.number_of_diggs, COALESCE(gs.server_status_id,0) as server_status_id, COALESCE(last_ping_datetime,NOW()) as last_ping_datetime,  " +
                    " CASE WHEN f3da.game_id IS NOT NULL THEN wok.concat_vw_url(@wokGameId,c.name,4) " +
                    "      WHEN g.game_id IS NOT NULL THEN wok.concat_vw_url(c.name,'',0)" +
                    "      ELSE wok.concat_vw_url(@wokGameId,c.name,6) " +
                    " END AS STPURL " +
                    " FROM communities c " +
                    " INNER JOIN kaneva.communities_personal creator_c ON c.creator_id = creator_c.creator_id AND creator_c.is_personal = 1 " +
                    " INNER JOIN channel_stats cs ON c.community_id = cs.channel_id " +
                    " INNER JOIN users u ON u.user_id = c.creator_id " +
                    " LEFT OUTER JOIN kaneva.communities_game cg ON cg.community_id = c.community_id " +
                    " LEFT OUTER JOIN developer.games g ON cg.game_id = g.game_id " +
                    " LEFT OUTER JOIN developer.game_servers gs ON gs.game_id = g.game_id " +
                    " LEFT OUTER JOIN developer.faux_3d_app f3da ON g.game_id = f3da.game_id " +
                    " WHERE c.community_id in " +
                    " (" + communityIds + ")" +
                    " AND (g.game_access_id IS NULL OR g.game_access_id IN (1,2)) " +
                    " GROUP BY c.community_id " +
                    " ORDER BY FIELD(c.community_id," + sortByIds + ")";

                if (myCommunitiesOnly && string.IsNullOrWhiteSpace(searchString))
                {
                    PagedDataTable result = Db.Slave.GetPagedDataTable(strSelect, "", parameters, pageNumber, pageSize);

                    if (result != null)
                    {
                        totalNumRecords = (int)result.TotalCount;

                        DataSet ds = new DataSet ();
                        ds.Tables.Add(result);

                        return ds.Tables[0];
                    }
                    
                    return null;
                }
                else
                {
                    return Db.Slave.GetDataTable(strSelect, parameters);
                }

            }
        }

        public DataTable GetUserHomeWorldForSearch(int userId, int wokGameId, ref int totalNumRecords)
        {
            string strSelect = " SELECT 0 as game_id, c.community_id, 10 as zoneType, " +
                " c.name, c.description, c.is_public, c.is_adult, c.keywords, c.over_21_required, " +
                " creator_c.creator_id, u.username, c.created_date, " +
                " c.name_no_spaces, c.thumbnail_small_path, " +
                " creator_c.name_no_spaces as creator_name_no_spaces, creator_c.thumbnail_small_path as creator_thumbnail_small_path, " +
                " cs.number_of_members, cs.number_of_views, cs.number_times_shared, cs.number_of_diggs, 0 as server_status_id, NOW() as last_ping_datetime,  " +
                " wok.concat_vw_url(@wokGameId,c.name,6) AS STPURL " +
                " FROM communities c " +
                " INNER JOIN kaneva.communities_personal creator_c ON c.creator_id = creator_c.creator_id AND creator_c.is_personal = 1 " +
                " INNER JOIN channel_stats cs ON c.community_id = cs.channel_id " +
                " INNER JOIN users u ON u.user_id = c.creator_id AND u.home_community_id = c.community_id " +
                " WHERE c.creator_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@wokGameId", wokGameId));

            PagedDataTable drResultPopulated = Db.Slave.GetPagedDataTable(strSelect, "", parameters, 1, 1);

            totalNumRecords = (int) drResultPopulated.TotalCount;

            DataSet dsResult = new DataSet ();
            DataTable dtTemp = drResultPopulated as DataTable;

            dsResult.Tables.Add (dtTemp);
            return dtTemp;

        }

        public DataTable GetWorldsByCategory(bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId, string category)
        {
            string gameIdSeperator = ", ";
            string gameIds = "3296" + gameIdSeperator + "3298" + gameIdSeperator + "5316" + gameIdSeperator + "5310" + gameIdSeperator;
            char[] charsToTrim = { ',', ' ' };

            string strSelect = "SELECT pc.STPURL,pc.game_id, pc.community_id, pc.zone_type as zoneType, " +
                " pc.name, pc.description, pc.is_public, pc.is_adult, pc.keywords, pc.over_21_required, " +
                " pc.creator_id, pc.creator_username AS username, pc.created_date, " +
                " pc.name_no_spaces, pc.thumbnail_small_path, pc.population, " +
                " pc.creator_name_no_spaces, pc.creator_thumbnail_small_path, " +
                " pc.number_of_members, pc.number_of_views, pc.number_times_shared, pc.number_of_diggs, 1 as server_status_id, NOW() as last_ping_datetime, " +
                " 0 as number_of_requests, 0 as number_of_friends_in_game, pc.number_of_diggs_past_7_days " +
                " FROM  wok.worlds_cache pc " +
                " INNER JOIN kaneva.communities_to_categories gtc ON gtc.community_id = pc.community_id " +
                " INNER JOIN kaneva.community_categories gc ON gc.category_id = gtc.category_id AND gc.description = @category " +
                " WHERE pc.game_id NOT IN (" + gameIds.TrimEnd (charsToTrim) + ") " +
                " AND pc.has_thumbnail = 'Y' " +
                " AND pc.is_public <> 'I' " +
                " AND pc.parent_community_id IS NULL ";

            if (ShowOnlyAP)
            {
                strSelect += " AND pc.is_adult = 'Y' ";
            }

            if (HideAP)
            {
                strSelect += " AND pc.is_adult <> 'Y' ";
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters = new List<IDbDataParameter> ();

            parameters.Add (new MySqlParameter ("@category", category));

            PagedDataTable drResultPopulated = Db.Master.GetPagedDataTable(strSelect, "population DESC, number_of_diggs_past_7_days DESC", parameters, pageNumber, pageSize);

            totalNumRecords = (int) drResultPopulated.TotalCount;

            DataSet dsResult = new DataSet ();
            DataTable dtTemp = drResultPopulated as DataTable;

            dsResult.Tables.Add (dtTemp);
            return dtTemp;
        }

        public DataTable TopWorldsTour(int userId, bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId)
        {
            string gameIdSeperator = ", ";
            string gameIds = "3296" + gameIdSeperator + "3298" + gameIdSeperator + "5316" + gameIdSeperator + "5310" + gameIdSeperator;
            char[] charsToTrim = { ',', ' ' };

            string strSelect = "SELECT pc.STPURL,pc.game_id, pc.community_id, pc.zone_instance_id as zone_instance_id, pc.zone_type as zoneType, " +
                " pc.name, pc.description, pc.is_public, pc.is_adult, pc.keywords, " +
                " pc.creator_id, pc.creator_username AS username, pc.created_date, " +
                " pc.name_no_spaces, pc.thumbnail_small_path, " +
                " pc.creator_name_no_spaces, pc.creator_thumbnail_small_path, " +
                " pc.number_of_diggs_past_7_days, loot_spawners, IF(wttr.user_id IS NULL, 'N', 'Y') as user_rewarded " +
                " FROM  wok.worlds_top_tour_cache pc " +
                " LEFT OUTER JOIN wok.worlds_top_tour_redeems wttr ON wttr.zone_instance_id = pc.zone_instance_id and wttr.zone_type = pc.zone_type and user_id = @userId " +
                " WHERE loot_spawners > 10 " + 
                " AND pc.number_of_diggs_past_7_days > 4 ";

            if (ShowOnlyAP)
            {
                strSelect += " AND pc.is_adult = 'Y' ";
            }

            if (HideAP)
            {
                strSelect += " AND pc.is_adult <> 'Y' ";
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            PagedDataTable drResultPopulated = Db.Slave.GetPagedDataTable(strSelect, "number_of_diggs_past_7_days DESC", parameters, pageNumber, pageSize);

            totalNumRecords = (int)drResultPopulated.TotalCount;

            DataSet dsResult = new DataSet();
            DataTable dtTemp = drResultPopulated as DataTable;

            dsResult.Tables.Add(dtTemp);
            return dtTemp;
        }

        public int ReedemTopWorldReward(int userId, int zoneInstanceId, int zoneType)
        {
            if (HasTopWorldRewardBeenRedeemed (userId, zoneInstanceId, zoneType))
            {
                return -2;
            }

            string sql = "INSERT INTO wok.worlds_top_tour_redeems " +
                "(user_id, zone_instance_id, zone_type, reward_date " +
                ") VALUES (" +
                "@userId, @zoneInstanceId, @zoneType, NOW())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));
            return Db.WOK.ExecuteNonQuery(sql, parameters);
        }

        public bool HasTopWorldRewardBeenRedeemed (int userId, int zoneInstanceId, int zoneType)
        {
            //create query string to get game by user id
            string sql = "SELECT user_id " +
                " FROM wok.worlds_top_tour_redeems " +
                " WHERE user_id = @userId " +
                " AND zone_instance_id = @zoneInstanceId " +
                " AND zone_type = @zoneType ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));


            //perform query returning single data row from the developer database
            DataRow row = Db.WOK.GetDataRow(sql.ToString(), parameters);
            return (row != null);
        }

        public DataTable GetMostVisited3DApps(bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId)
        {
            string gameIdSeperator = ", ";
            string gameIds = "3296" + gameIdSeperator + "3298" + gameIdSeperator + "5316" + gameIdSeperator;
            char[] charsToTrim = { ',', ' ' };

            // Most Populated Only
            string strSelect = " SELECT pc.STPURL,pc.game_id, pc.community_id, pc.zone_type as zoneType, " +
                " pc.name, pc.description, pc.is_public, pc.is_adult, pc.keywords, pc.over_21_required, " +
                " pc.creator_id, pc.creator_username AS username, pc.created_date, " +
                " pc.name_no_spaces, pc.thumbnail_small_path, " +
                " pc.creator_name_no_spaces, pc.creator_thumbnail_small_path, " +
                " pc.number_of_members, pc.number_of_views, pc.number_times_shared, pc.number_of_diggs, 1 as server_status_id, NOW() as last_ping_datetime, " +
                " 0 as number_of_requests, 0 as number_of_friends_in_game, pc.number_of_diggs_past_7_days " +
                " FROM  wok.worlds_cache pc " +
                " WHERE game_id NOT IN (" + gameIds.TrimEnd (charsToTrim) + ") " +
                " AND pc.has_thumbnail = 'Y' " +
                " AND pc.is_public <> 'I' " +
                " AND pc.parent_community_id IS NULL ";

            if (ShowOnlyAP)
            {
                strSelect += " AND pc.is_adult = 'Y' ";
            }

            if (HideAP)
            {
                strSelect += " AND pc.is_adult <> 'Y' ";
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters = new List<IDbDataParameter> ();
            PagedDataTable drResultPopulated = Db.Master.GetPagedDataTable (strSelect, "pc.number_of_views DESC", parameters, pageNumber, pageSize);

            totalNumRecords = (int) drResultPopulated.TotalCount;

            DataSet dsResult = new DataSet ();
            DataTable dtTemp = drResultPopulated as DataTable;

            dsResult.Tables.Add (dtTemp);
            return dtTemp;
        }

        public DataTable GetMostPopulated3DApps (bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId)
        {
            string gameIdSeperator = ", ";
            string gameIds = "3296" + gameIdSeperator + "3298" + gameIdSeperator + "5316" + gameIdSeperator;
            char[] charsToTrim = { ',', ' ' };

            // Most Populated Only
            string strSelect = " SELECT pc.STPURL,pc.game_id, pc.community_id, pc.zone_type as zoneType, " +
                " pc.name, pc.description, pc.is_public, pc.is_adult, pc.keywords, pc.over_21_required, " +
                " pc.creator_id, pc.creator_username AS username, pc.created_date, pc.population," +
                " pc.name_no_spaces, pc.thumbnail_small_path, " +
                " pc.creator_name_no_spaces, pc.creator_thumbnail_small_path, " +
                " pc.number_of_members, pc.number_of_views, pc.number_times_shared, pc.number_of_diggs, 1 as server_status_id, NOW() as last_ping_datetime, " +
                " 0 as number_of_requests, 0 as number_of_friends_in_game, pc.number_of_diggs_past_7_days " +
                " FROM  wok.worlds_cache pc " +
                " WHERE game_id NOT IN (" + gameIds.TrimEnd (charsToTrim) + ") " +
                " AND pc.has_thumbnail = 'Y' " +
                " AND pc.is_public <> 'I' " +
                " AND pc.population > 0 " +
                " AND pc.parent_community_id IS NULL ";

            if (ShowOnlyAP)
            {
                strSelect += " AND pc.is_adult = 'Y' ";
            }

            if (HideAP)
            {
                strSelect += " AND pc.is_adult <> 'Y' ";
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters = new List<IDbDataParameter> ();
            PagedDataTable drResultPopulated = Db.Master.GetPagedDataTable(strSelect, "pc.population DESC, pc.number_of_diggs_past_7_days DESC", parameters, pageNumber, pageSize);

            totalNumRecords = (int) drResultPopulated.TotalCount;

            DataSet dsResult = new DataSet ();
            DataTable dtTemp = drResultPopulated as DataTable;

            dsResult.Tables.Add (dtTemp);
            return dtTemp;

        }

        public DataTable GetMostRaved3DApps(bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId)
        {
            string gameIdSeperator = ", ";
            string gameIds = "3296" + gameIdSeperator + "3298" + gameIdSeperator + "5316" + gameIdSeperator;
            char[] charsToTrim = { ',', ' ' };

            // Most Populated Only
            string strSelect = " SELECT pc.STPURL,pc.game_id, pc.community_id, pc.zone_type as zoneType, " +
                " pc.name, pc.description, pc.is_public, pc.is_adult, pc.keywords, pc.over_21_required, " +
                " pc.creator_id, pc.creator_username AS username, pc.created_date, " +
                " pc.name_no_spaces, pc.thumbnail_small_path, " +
                " pc.creator_name_no_spaces, pc.creator_thumbnail_small_path, " +
                " pc.number_of_members, pc.number_of_views, pc.number_times_shared, pc.number_of_diggs, 1 as server_status_id, NOW() as last_ping_datetime, " +
                " 0 as number_of_requests, 0 as number_of_friends_in_game, pc.number_of_diggs_past_7_days " +
                " FROM  wok.worlds_cache pc " +
                " WHERE game_id NOT IN (" + gameIds.TrimEnd (charsToTrim) + ") " +
                " AND pc.has_thumbnail = 'Y' " +
                " AND pc.is_public <> 'I' " +
                " AND pc.parent_community_id IS NULL ";

            if (ShowOnlyAP)
            {
                strSelect += " AND pc.is_adult = 'Y' ";
            }

            if (HideAP)
            {
                strSelect += " AND pc.is_adult <> 'Y' ";
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters = new List<IDbDataParameter> ();
            PagedDataTable drResultPopulated = Db.Master.GetPagedDataTable(strSelect, "pc.number_of_diggs_past_7_days DESC", parameters, pageNumber, pageSize);

            totalNumRecords = (int) drResultPopulated.TotalCount;

            DataSet dsResult = new DataSet ();
            DataTable dtTemp = drResultPopulated as DataTable;

            dsResult.Tables.Add (dtTemp);
            return dtTemp;
        }

        // http://wiki.intranet.kaneva.com/mediawiki/index.php/2013_Roadmap_for_Consumers#Places_menu_-_AP_and_Requested
        public DataTable GetMostRequested(bool ShowOnlyAP, bool HideAP, int userId, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId)
        {
            string strSelect = " SELECT wok.concat_vw_url(c.name,'',0) as STPURL, COALESCE(g.game_id, 0) as game_id, c.community_id, 10 as zoneType, " +
                " c.name, c.description, c.is_public, c.is_adult, c.keywords, c.over_21_required, " +
                " creator_c.creator_id, u.username, c.created_date, 0 AS population," +
                " c.name_no_spaces, c.thumbnail_small_path, " +
                " creator_c.name_no_spaces as creator_name_no_spaces, creator_c.thumbnail_small_path as creator_thumbnail_small_path, " +
                " cs.number_of_members, cs.number_of_views, cs.number_times_shared, cs.number_of_diggs,  " +
                " nr.number_of_requests " +
                " FROM communities c " +
                " INNER JOIN kaneva.communities_personal creator_c ON c.creator_id = creator_c.creator_id AND creator_c.is_personal = 1 " +
                " INNER JOIN channel_stats cs ON c.community_id = cs.channel_id " +
                " INNER JOIN users u ON u.user_id = c.creator_id " +
                " INNER JOIN kaneva.communities_game cg ON cg.community_id = c.community_id " +
                " INNER JOIN developer.games g ON cg.game_id = g.game_id " +
                " INNER JOIN kaneva.notification_requests nr ON nr.game_id = g.game_id " +
                " LEFT OUTER JOIN developer.game_servers gs ON gs.game_id = g.game_id " +
                " WHERE nr.user_id = @userId "  + 
                " AND c.status_id = 1 ";

            if (ShowOnlyAP)
            {
                strSelect += " AND c.is_adult = 'Y' ";
            }

            if (HideAP)
            {
                strSelect += " AND c.is_adult <> 'Y' ";
            }


            strSelect += " GROUP BY c.community_id ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            PagedDataTable drResultPopulated = Db.Slave.GetPagedDataTable (strSelect, "nr.number_of_requests DESC", parameters, pageNumber, pageSize);

            totalNumRecords = (int)drResultPopulated.TotalCount;

            DataSet dsResult = new DataSet();
            DataTable dtTemp = drResultPopulated as DataTable;

            dsResult.Tables.Add(dtTemp);
            return dtTemp;
        }


        // http://wiki.intranet.kaneva.com/mediawiki/index.php/2013_Roadmap_for_Consumers#Places_menu_phases
        // http://wiki.intranet.kaneva.com/mediawiki/index.php/2013_Roadmap_for_Consumers#Places_menu_-_AP_and_Requested
        public DataTable MostPopulatedCombined(bool ShowOnlyAP, bool HideAP, int userId, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId, string countryFilterBy, string countrySortBy, int ageFilterBy, int ageSortBy, int offset)
        {
            totalNumRecords = 99;
            string gameIdSeperator = ",";
            string gameIds = "3296" + gameIdSeperator + "3298" + gameIdSeperator + "5316" + gameIdSeperator + "6931" + gameIdSeperator + "6932" + gameIdSeperator;

            char[] charsToTrim = { ',', ' ' };

            //DataTable dtBrowseResult = new DataTable();

            // Make sure page number is valid
            if (pageNumber < 1)
            {
                pageNumber = 1;
            }

            // Dev world is community_type 3 which means 3d app
            // Game World is community_type 2 with template 11 or 12 
            // Community is community_type 2 but template not 11 or 12

            // Billy requirements Email "Travel menu algorithm P1"
            // 10/8  Remove raves part " OR (pc.community_type_id = 5 AND number_of_diggs_past_7_days > 0) " +  per Billy "Show Homes, ONLY if they have 2 people or more"
            // 10/8  Add multiplier to game_worlds population x 2, so now order by population_rank
            // 10/8  Remove all public Kaneva Worlds

            // 11/7
            // Billy reqiurements "Travel Browse Menu " "Phase 2 - Dec 2nd launch" part 3   http://wiki.kaneva.com/mediawiki/index.php/2014_Roadmap_for_all#HUD_redesign_Phase_2
            // 3.Algorithm changes for this release (launch earlier separately? hot fix?): 
            //     1.Remove the 2x multiplyer on gameworlds and treat them the same as all other worlds
            // 2.Put back the 2 public zones on the 'Most Popular' filter: 
            //     1.Kaneva City 
            //     2.Employment Center 


            // 2/5/2016 Billy Email - RE: Travel algorithm shows nothing for most popular when no one in world
            // - Most Popular 'populated worlds' works as it does (Home > 1, etc)
            // - Raved World were DEV Worlds only, as we were trying to promote them back then
            // So, I think we should make the Raves worlds algorithm include all world types, including Communities, all game worlds, homes, etc)

            // 2/22/2016 https://kaneva.atlassian.net/browse/ED-4529
            // Billy and Jason talked on Feb 22, about the Homes showing in the Raved section, 
            // but since they have 1, they show "Low" and so looked populated. 
            // (Homes with 1 should never show and make for a bad world for anyone to go to) To eliminate confusion, 
            // we are going to remove Homes from the Raved section, even if they have a lot of Raves. 
            // Homes will only show in the Populated section if and only if they have 2 or more people in them.

            // 2/26/2016 https://kaneva.atlassian.net/wiki/pages/viewpage.action?pageId=26345481
            // Billy and Minaz expressed concern that the most popular algorithm wasn't showing faux_3d_apps in the correct order.
            // It has been decided to remove the country and age filters for now and go back to the way things were before.

            // Most Populated Mixed
            string strSelect = "select pc.STPURL, pc.game_id, pc.community_id, pc.zone_type as zoneType, " +
                "  pc.name, pc.description, pc.is_public, pc.is_adult, pc.keywords, pc.over_21_required, " +
                "   creator_id, creator_username AS username, created_date, population,  " +
                "   name_no_spaces, thumbnail_small_path, " +
                "   creator_name_no_spaces, creator_thumbnail_small_path, " +
                "   number_of_members, number_of_views, number_times_shared, number_of_diggs, pc.number_of_diggs_past_7_days, " +
                "   (CASE WHEN ((pc.community_type_id IN (2, 3) AND population > 0) OR (pc.community_type_id = 5 AND population >= 2)) THEN 1 else 0 END) as popRank  " +
                " from wok.worlds_cache pc " +
                " WHERE game_id NOT IN (" + gameIds.TrimEnd(charsToTrim) + ") " +
                " AND pc.is_public <> 'I' " +
                " AND (community_type_id IN (2,3) " +
                      " OR (pc.community_type_id = 5 AND population >= 2) " +
                      " ) " +
                " AND pc.parent_community_id IS NULL ";

            //if (countryFilterBy.Length > 0)
            //{
            //    strSelect += " AND pc.owner_country = '" + countryFilterBy + "'";
            //}

            //if (ageFilterBy > 0)
            //{
            //    if (ageFilterBy > 17)
            //    {
            //        strSelect += " AND pc.owner_under_18 = 0 ";
            //    }
            //    else
            //    {
            //        strSelect += " AND pc.owner_under_18 = 1  ";
            //    }
            //}

            string sqlSortBy = "";

            //if (countrySortBy.Length > 0)
            //{
            //    sqlSortBy += " FIELD(owner_country,'" + countrySortBy + "') desc, ";
            //}

            //if (ageSortBy > 0)
            //{
            //    // Per PRD, country sort takes precedence
            //    // Performance optimization for my age first
            //    // Per Billy 2/12/16, SWAP Age and Country precedence
            //    // Per Billy 2/16/16 SWAP it back Country over Age
            //    if (ageSortBy > 17)
            //    {
            //        sqlSortBy += " pc.owner_under_18 asc, ";
            //    }
            //    else
            //    {
            //        sqlSortBy += " pc.owner_under_18 desc, ";
            //    }
            //}

            if (ShowOnlyAP)
            {
                strSelect += " AND pc.is_adult = 'Y' ";
            }

            if (HideAP)
            {
                strSelect += " AND pc.is_adult <> 'Y' ";
            }

            // Allow connie to set offset
            // https://kaneva.atlassian.net/browse/ED-5160
            if (offset < 0)
            {
                offset = (pageNumber - 1) * pageSize;
            }


            string limit = " LIMIT " + offset + ", " + pageSize;

            strSelect += " order by " + sqlSortBy + " population DESC, number_of_diggs_past_7_days DESC, created_date DESC, name ASC " +
                limit.ToString();

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            DataTable drResultPopulated = Db.Master.GetDataTable(strSelect, parameters);
            return drResultPopulated;
        }

        public DataTable Browse3DAppsByRequest (bool ShowOnlyAP, bool HideAP, int userId, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId)
        {
            int recordCount = 0;
            totalNumRecords = 56;
            string gameIdSeperator = ", ";
            string gameIds = "3296" + gameIdSeperator + "3298" + gameIdSeperator + "5316" + gameIdSeperator;
            char[] charsToTrim = { ',', ' ' };
            DataTable dtBrowseResult = new DataTable ();

            // Make sure page number is valid
            if (pageNumber < 1)
            {
                pageNumber = 1;
            }

            ////// Most Populated Only
            ////string strSelect = " SELECT pc.STPURL,pc.game_id, pc.community_id, pc.zone_type as zoneType, " +
            ////    " pc.name, pc.description, pc.is_public, pc.is_adult, pc.keywords, pc.over_21_required, " +
            ////    " pc.creator_id, pc.username, pc.created_date, " +
            ////    " pc.name_no_spaces, pc.thumbnail_small_path, " +
            ////    " pc.creator_name_no_spaces, pc.creator_thumbnail_small_path, " +
            ////    " pc.number_of_members, pc.number_of_views, pc.number_times_shared, pc.number_of_diggs, 1 as server_status_id, NOW() as last_ping_datetime,  " +
            ////    " 0 as number_of_requests, 0 as number_of_friends_in_game " +
            ////    " FROM  wok.population_cache pc " +
            ////    " WHERE game_id NOT IN (" + gameIds.TrimEnd(charsToTrim) + ") " +
            ////    " AND pc.has_thumbnail = 'Y' " +
            ////    " AND pc.is_public = 'Y' " +
            ////    " AND pc.game_population > 0 ";

            ////if (ShowOnlyAP)
            ////{
            ////    strSelect += " AND pc.is_adult = 'Y' ";
            ////}

            ////List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            ////parameters = new List<IDbDataParameter>();
            ////PagedDataTable drResultPopulated = Db.Slave.GetPagedDataTable (strSelect, "pc.game_population DESC, pc.number_of_diggs DESC", parameters, pageNumber, pageSize);

            ////totalNumRecords = (int) drResultPopulated.TotalCount;

            ////DataSet dsResult = new DataSet();
            ////DataTable dtTemp = drResultPopulated as DataTable;

            ////dsResult.Tables.Add(dtTemp);
            ////return dtTemp;


            ////// First just get a list of my friends in 3D Apps
            ////string strSelect = " SELECT al.game_id, al.user_id " +
            ////    " FROM developer.active_logins al, kaneva.friends f " +
            ////    " WHERE al.user_id = f.friend_id " +
            ////    " and f.user_id = @userId " +
            ////    " and al.game_id NOT IN (3296, 3298) ";

            ////List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            ////parameters.Add(new MySqlParameter("@userId", userId));
            ////DataTable dtOnlineFriends = Db.Slave.GetDataTable(strSelect, parameters);

            ////// Find count of friends in each 3D App
            ////DataTable dtGroupedFriendsOnline = GroupBy("game_id", "user_id", dtOnlineFriends);


            int requestPageCount = pageSize;
            int populatedPage1Count = 0;
            int page1Size = 9;

            // 1. Max of 3 requests
            string strSelect = " SELECT wok.concat_vw_url(c.name,'',0) as STPURL, COALESCE(g.game_id, 0) as game_id, c.community_id, 10 as zoneType, " +
                " c.name, c.description, c.is_public, c.is_adult, c.keywords, c.over_21_required, " +
                " creator_c.creator_id, u.username, c.created_date, 0 AS population," +
                " c.name_no_spaces, c.thumbnail_small_path, " +
                " creator_c.name_no_spaces as creator_name_no_spaces, creator_c.thumbnail_small_path as creator_thumbnail_small_path, " +
                " cs.number_of_members, cs.number_of_views, cs.number_times_shared, cs.number_of_diggs, COALESCE(gs.server_status_id,0) as server_status_id, COALESCE(last_ping_datetime,NOW()) as last_ping_datetime,  " +
                " nr.number_of_requests " +
                " FROM communities c " +
                " INNER JOIN kaneva.communities_personal creator_c ON c.creator_id = creator_c.creator_id AND creator_c.is_personal = 1 " +
                " INNER JOIN channel_stats cs ON c.community_id = cs.channel_id " +
                " INNER JOIN users u ON u.user_id = c.creator_id " +
                " INNER JOIN kaneva.communities_game cg ON cg.community_id = c.community_id " +
                " INNER JOIN developer.games g ON cg.game_id = g.game_id " +
                " INNER JOIN kaneva.notification_requests nr ON nr.game_id = g.game_id " +
                " LEFT OUTER JOIN developer.game_servers gs ON gs.game_id = g.game_id " +
                " WHERE nr.user_id = @userId " +
                " AND c.has_thumbnail = 'Y' ";

            if (ShowOnlyAP)
            {
                strSelect += " AND c.is_adult = 'Y' ";
            }

            if (HideAP)
            {
                strSelect += " AND c.is_adult <> 'Y' ";
            }
            

            strSelect += " GROUP BY c.community_id " +
                " ORDER BY nr.number_of_requests DESC LIMIT 3 "; ;

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            dtBrowseResult = Db.Slave.GetDataTable (strSelect, parameters);

            recordCount = dtBrowseResult.Rows.Count;


            // 3 to 6 of Most Populated
            strSelect = " SELECT pc.STPURL,pc.game_id, pc.community_id, pc.zone_type as zoneType, " +
                " pc.name, pc.description, pc.is_public, pc.is_adult, pc.keywords, pc.over_21_required, " +
                " pc.creator_id, pc.creator_username AS username, pc.created_date, pc.population," +
                " pc.name_no_spaces, pc.thumbnail_small_path, " +
                " pc.creator_name_no_spaces, pc.creator_thumbnail_small_path, " +
                " pc.number_of_members, pc.number_of_views, pc.number_times_shared, pc.number_of_diggs, pc.number_of_diggs, 1 as server_status_id, NOW() as last_ping_datetime, " +
                " 0 as number_of_requests, 0 as number_of_friends_in_game, pc.number_of_diggs_past_7_days " +
                " FROM  wok.worlds_cache pc " +
                " WHERE game_id NOT IN (" + gameIds.TrimEnd (charsToTrim) + ") " +
                " AND pc.has_thumbnail = 'Y' " +
                " AND pc.is_public <> 'I' " +
                " AND pc.population > 0 " +
                " AND pc.parent_community_id IS NULL ";

            if (ShowOnlyAP)
            {
                strSelect += " AND pc.is_adult = 'Y' ";
            }

            if (HideAP)
            {
                strSelect += " AND pc.is_adult <> 'Y' ";
            }


            strSelect += " order by pc.population DESC, pc.number_of_diggs_past_7_days DESC " +
                " LIMIT " + (6 - recordCount).ToString ();

            parameters = new List<IDbDataParameter> ();

            DataTable drResultPopulated = Db.Slave.GetDataTable (strSelect, parameters);

            if (drResultPopulated != null)
            {
                // Add results together
                for (int i = 0; i < drResultPopulated.Rows.Count; i++)
                {
                    recordCount++;
                    populatedPage1Count++;
                    gameIds += drResultPopulated.Rows[i]["game_id"].ToString () + gameIdSeperator;

                    // Add it to the result
                    dtBrowseResult.ImportRow (drResultPopulated.Rows[i]);
                }
            }


            // 3 to 9 most raved
            // 3 to 6 of Most Populated
            strSelect = " SELECT pc.STPURL,pc.game_id, pc.community_id, pc.zone_type as zoneType, " +
                " pc.name, pc.description, pc.is_public, pc.is_adult, pc.keywords, pc.over_21_required, " +
                " pc.creator_id, pc.creator_username AS username, pc.created_date, pc.population," +
                " pc.name_no_spaces, pc.thumbnail_small_path," +
                " pc.creator_name_no_spaces, pc.creator_thumbnail_small_path, " +
                " pc.number_of_members, pc.number_of_views, pc.number_times_shared, pc.number_of_diggs, pc.number_of_diggs, 1 as server_status_id, NOW() as last_ping_datetime, " +
                " 0 as number_of_requests, 0 as number_of_friends_in_game, pc.number_of_diggs_past_7_days " +
                " FROM  wok.worlds_cache pc " +
                " WHERE game_id NOT IN (" + gameIds.TrimEnd (charsToTrim) + ") " +
                " AND pc.has_thumbnail = 'Y' " +
                " AND pc.is_public <> 'I' " +
                " AND pc.parent_community_id IS NULL ";

            if (ShowOnlyAP)
            {
                strSelect += " AND pc.is_adult = 'Y' ";
            }

            if (HideAP)
            {
                strSelect += " AND pc.is_adult <> 'Y' ";
            }


            strSelect += " order by pc.number_of_diggs_past_7_days DESC " +
                " LIMIT " + (page1Size - recordCount).ToString ();

            parameters = new List<IDbDataParameter> ();

            DataTable drResultRaves = Db.Master.GetDataTable (strSelect, parameters);

            if (drResultRaves != null)
            {
                // Add results together
                for (int i = 0; i < drResultRaves.Rows.Count; i++)
                {
                    recordCount++;
                    gameIds += drResultRaves.Rows[i]["game_id"].ToString () + gameIdSeperator;

                    // Add it to the result
                    dtBrowseResult.ImportRow (drResultRaves.Rows[i]);
                }
            }


            // Are we tryint to return first page?
            if (pageNumber.Equals (1))
            {
                return dtBrowseResult;
            }
            else
            {
                // http://wiki.kaneva.com/mediawiki/index.php/2012_Roadmap_for_Consumers#Places_menu_Population_Logic

                // Page 2 and up Most Populated and only populated
                strSelect = " SELECT pc.STPURL,pc.game_id, pc.community_id, pc.zone_type as zoneType, " +
                 " pc.name, pc.description, pc.is_public, pc.is_adult, pc.keywords, pc.over_21_required, " +
                 " pc.creator_id, pc.creator_username AS username, pc.created_date, pc.population," +
                 " pc.name_no_spaces, pc.thumbnail_small_path, " +
                 " pc.creator_name_no_spaces, pc.creator_thumbnail_small_path, " +
                 " pc.number_of_members, pc.number_of_views, pc.number_times_shared, pc.number_of_diggs, pc.number_of_diggs, 1 as server_status_id, NOW() as last_ping_datetime, " +
                 " 0 as number_of_requests, 0 as number_of_friends_in_game, pc.number_of_diggs_past_7_days " +
                 " FROM  wok.worlds_cache pc " +
                 " WHERE game_id NOT IN (3296, 3298, 5316, 5310)" +    // " WHERE game_id NOT IN (" + gameIds.TrimEnd(charsToTrim) + ") " + Don't add this here it is taken into account via populatedPage1Count
                 " AND pc.has_thumbnail = 'Y' " +
                 " AND pc.is_public <> 'I' " +
                 " AND pc.parent_community_id IS NULL ";


                if (ShowOnlyAP)
                {
                    strSelect += " AND pc.is_adult = 'Y' ";
                }

                if (HideAP)
                {
                    strSelect += " AND pc.is_adult <> 'Y' ";
                }
            

                int offset = (pageNumber - 1) * pageSize;
                string finalLimit = offset - page1Size + populatedPage1Count + ", " + pageSize;

                strSelect += " order by pc.population DESC, pc.number_of_diggs_past_7_days DESC " +
                    " LIMIT " + finalLimit;

                parameters = new List<IDbDataParameter> ();

                DataTable drResultPopulated2 = Db.Master.GetDataTable (strSelect, parameters);
                recordCount = drResultPopulated2.Rows.Count;
                return drResultPopulated2;
            }
        }

        /// <summary>
        /// Variant for A/B Test
        /// </summary>
        /// <returns></returns>
        public DataTable Browse3DAppsByRequestB (bool ShowOnlyAP, int userId, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId)
        {
            int recordCount = 0;
            totalNumRecords = 90;
            string gameIdSeperator = ", ";
            string gameIds = "3296" + gameIdSeperator + "3298" + gameIdSeperator + "5316" + gameIdSeperator;
            char[] charsToTrim = { ',', ' ' };
            DataTable dtBrowseResult = new DataTable ();

            // Make sure page number is valid
            if (pageNumber < 1)
            {
                pageNumber = 1;
            }

            ////// Most Populated Only
            ////string strSelect = " SELECT pc.STPURL,pc.game_id, pc.community_id, pc.zone_type as zoneType, " +
            ////    " pc.name, pc.description, pc.is_public, pc.is_adult, pc.keywords, pc.over_21_required, " +
            ////    " pc.creator_id, pc.username, pc.created_date, " +
            ////    " pc.name_no_spaces, pc.thumbnail_small_path, " +
            ////    " pc.creator_name_no_spaces, pc.creator_thumbnail_small_path, " +
            ////    " pc.number_of_members, pc.number_of_views, pc.number_times_shared, pc.number_of_diggs, 1 as server_status_id, NOW() as last_ping_datetime,  " +
            ////    " 0 as number_of_requests, 0 as number_of_friends_in_game " +
            ////    " FROM  wok.population_cache pc " +
            ////    " WHERE game_id NOT IN (" + gameIds.TrimEnd(charsToTrim) + ") " +
            ////    " AND pc.has_thumbnail = 'Y' " +
            ////    " AND pc.is_public = 'Y' " +
            ////    " AND pc.game_population > 0 ";

            ////if (ShowOnlyAP)
            ////{
            ////    strSelect += " AND pc.is_adult = 'Y' ";
            ////}

            ////List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            ////parameters = new List<IDbDataParameter>();
            ////PagedDataTable drResultPopulated = Db.Slave.GetPagedDataTable (strSelect, "pc.game_population DESC, pc.number_of_diggs DESC", parameters, pageNumber, pageSize);

            ////totalNumRecords = (int) drResultPopulated.TotalCount;

            ////DataSet dsResult = new DataSet();
            ////DataTable dtTemp = drResultPopulated as DataTable;

            ////dsResult.Tables.Add(dtTemp);
            ////return dtTemp;


            ////// First just get a list of my friends in 3D Apps
            ////string strSelect = " SELECT al.game_id, al.user_id " +
            ////    " FROM developer.active_logins al, kaneva.friends f " +
            ////    " WHERE al.user_id = f.friend_id " +
            ////    " and f.user_id = @userId " +
            ////    " and al.game_id NOT IN (3296, 3298) ";

            ////List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            ////parameters.Add(new MySqlParameter("@userId", userId));
            ////DataTable dtOnlineFriends = Db.Slave.GetDataTable(strSelect, parameters);

            ////// Find count of friends in each 3D App
            ////DataTable dtGroupedFriendsOnline = GroupBy("game_id", "user_id", dtOnlineFriends);


            int requestPageCount = pageSize;
            int populatedPage1Count = 0;
            int page1Size = 15;

            // 1. Max of 5 requests
            string strSelect = " SELECT wok.concat_vw_url(c.name,'',0) as STPURL, COALESCE(g.game_id, 0) as game_id, c.community_id, 10 as zoneType, " +
                " c.name, c.description, c.is_public, c.is_adult, c.keywords, c.over_21_required, " +
                " creator_c.creator_id, u.username, c.created_date, 0 AS population," +
                " c.name_no_spaces, c.thumbnail_small_path, " +
                " creator_c.name_no_spaces as creator_name_no_spaces, creator_c.thumbnail_small_path as creator_thumbnail_small_path, " +
                " cs.number_of_members, cs.number_of_views, cs.number_times_shared, cs.number_of_diggs, COALESCE(gs.server_status_id,0) as server_status_id, COALESCE(last_ping_datetime,NOW()) as last_ping_datetime,  " +
                " nr.number_of_requests " +
                " FROM communities c " +
                " INNER JOIN kaneva.communities_personal creator_c ON c.creator_id = creator_c.creator_id AND creator_c.is_personal = 1 " +
                " INNER JOIN channel_stats cs ON c.community_id = cs.channel_id " +
                " INNER JOIN users u ON u.user_id = c.creator_id " +
                " INNER JOIN kaneva.communities_game cg ON cg.community_id = c.community_id " +
                " INNER JOIN developer.games g ON cg.game_id = g.game_id " +
                " INNER JOIN kaneva.notification_requests nr ON nr.game_id = g.game_id " +
                " LEFT OUTER JOIN developer.game_servers gs ON gs.game_id = g.game_id " +
                " WHERE nr.user_id = @userId " +
                " AND c.has_thumbnail = 'Y' ";

            if (ShowOnlyAP)
            {
                strSelect += " AND c.is_adult = 'Y' ";
            }

            strSelect += " GROUP BY c.community_id " +
                " ORDER BY nr.number_of_requests DESC LIMIT 5 "; ;

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            dtBrowseResult = Db.Slave.GetDataTable (strSelect, parameters);

            recordCount = dtBrowseResult.Rows.Count;


            // 5 to 10 of Most Populated
            strSelect = " SELECT pc.STPURL,pc.game_id, pc.community_id, pc.zone_type as zoneType, " +
                " pc.name, pc.description, pc.is_public, pc.is_adult, pc.keywords, pc.over_21_required, " +
                " pc.creator_id, pc.creator_username AS username, pc.created_date, pc.population," +
                " pc.name_no_spaces, pc.thumbnail_small_path, " +
                " pc.creator_name_no_spaces, pc.creator_thumbnail_small_path, " +
                " pc.number_of_members, pc.number_of_views, pc.number_times_shared, pc.number_of_diggs, pc.number_of_diggs, 1 as server_status_id, NOW() as last_ping_datetime, " +
                " 0 as number_of_requests, 0 as number_of_friends_in_game, pc.number_of_diggs_past_7_days " +
                " FROM  wok.worlds_cache pc " +
                " WHERE game_id NOT IN (" + gameIds.TrimEnd (charsToTrim) + ") " +
                " AND pc.has_thumbnail = 'Y' " +
                " AND pc.is_public <> 'I' " +
                " AND pc.population > 0 " +
                " AND pc.parent_community_id IS NULL ";

            if (ShowOnlyAP)
            {
                strSelect += " AND pc.is_adult = 'Y' ";
            }

            strSelect += " order by pc.population DESC, pc.number_of_diggs_past_7_days DESC " +
                " LIMIT " + (10 - recordCount).ToString ();

            parameters = new List<IDbDataParameter> ();

            DataTable drResultPopulated = Db.Master.GetDataTable (strSelect, parameters);

            if (drResultPopulated != null)
            {
                // Add results together
                for (int i = 0; i < drResultPopulated.Rows.Count; i++)
                {
                    recordCount++;
                    populatedPage1Count++;
                    gameIds += drResultPopulated.Rows[i]["game_id"].ToString () + gameIdSeperator;

                    // Add it to the result
                    dtBrowseResult.ImportRow (drResultPopulated.Rows[i]);
                }
            }


            // 5 to 10 most raved
            // 5 to 10 of Most Populated
            strSelect = " SELECT pc.STPURL,pc.game_id, pc.community_id, pc.zone_type as zoneType, " +
                " pc.name, pc.description, pc.is_public, pc.is_adult, pc.keywords, pc.over_21_required, " +
                " pc.creator_id, pc.creator_username AS username, pc.created_date, pc.population," +
                " pc.name_no_spaces, pc.thumbnail_small_path," +
                " pc.creator_name_no_spaces, pc.creator_thumbnail_small_path, " +
                " pc.number_of_members, pc.number_of_views, pc.number_times_shared, pc.number_of_diggs, pc.number_of_diggs, 1 as server_status_id, NOW() as last_ping_datetime, " +
                " 0 as number_of_requests, 0 as number_of_friends_in_game, pc.number_of_diggs_past_7_days " +
                " FROM  wok.worlds_cache pc " +
                " WHERE game_id NOT IN (" + gameIds.TrimEnd (charsToTrim) + ") " +
                " AND pc.has_thumbnail = 'Y' " +
                " AND pc.is_public <> 'I' " +
                " AND pc.parent_community_id IS NULL ";

            if (ShowOnlyAP)
            {
                strSelect += " AND pc.is_adult = 'Y' ";
            }

            strSelect += " order by pc.number_of_diggs_past_7_days DESC " +
                " LIMIT " + (page1Size - recordCount).ToString ();

            parameters = new List<IDbDataParameter> ();

            DataTable drResultRaves = Db.Slave.GetDataTable (strSelect, parameters);

            if (drResultRaves != null)
            {
                // Add results together
                for (int i = 0; i < drResultRaves.Rows.Count; i++)
                {
                    recordCount++;
                    gameIds += drResultRaves.Rows[i]["game_id"].ToString () + gameIdSeperator;

                    // Add it to the result
                    dtBrowseResult.ImportRow (drResultRaves.Rows[i]);
                }
            }


            // Are we tryint to return first page?
            if (pageNumber.Equals (1))
            {
                return dtBrowseResult;
            }
            else
            {
                // http://wiki.kaneva.com/mediawiki/index.php/2012_Roadmap_for_Consumers#Places_menu_Population_Logic

                // Page 2 and up Most Populated and only populated
                strSelect = " SELECT pc.STPURL,pc.game_id, pc.community_id, pc.zone_type as zoneType, " +
                 " pc.name, pc.description, pc.is_public, pc.is_adult, pc.keywords, pc.over_21_required, " +
                 " pc.creator_id, pc.creator_username AS username, pc.created_date, pc.population," +
                 " pc.name_no_spaces, pc.thumbnail_small_path, " +
                 " pc.creator_name_no_spaces, pc.creator_thumbnail_small_path, " +
                 " pc.number_of_members, pc.number_of_views, pc.number_times_shared, pc.number_of_diggs, pc.number_of_diggs, 1 as server_status_id, NOW() as last_ping_datetime, " +
                 " 0 as number_of_requests, 0 as number_of_friends_in_game, pc.number_of_diggs_past_7_days " +
                 " FROM  wok.worlds_cache pc " +
                 " WHERE game_id NOT IN (3296, 3298, 5316)" +    // " WHERE game_id NOT IN (" + gameIds.TrimEnd(charsToTrim) + ") " + Don't add this here it is taken into account via populatedPage1Count
                 " AND pc.has_thumbnail = 'Y' " +
                 " AND pc.is_public <> 'I' " +
                 " AND pc.parent_community_id IS NULL ";


                if (ShowOnlyAP)
                {
                    strSelect += " AND pc.is_adult = 'Y' ";
                }

                int offset = (pageNumber - 1) * pageSize;
                string finalLimit = offset - page1Size + populatedPage1Count + ", " + pageSize;

                strSelect += " order by pc.population DESC, pc.number_of_diggs_past_7_days DESC " +
                    " LIMIT " + finalLimit;

                parameters = new List<IDbDataParameter> ();

                DataTable drResultPopulated2 = Db.Master.GetDataTable (strSelect, parameters);
                recordCount = drResultPopulated2.Rows.Count;
                return drResultPopulated2;
            }
        }

        private DataTable GroupBy (string i_sGroupByColumn, string i_sAggregateColumn, DataTable i_dSourceTable)
        {

            DataView dv = new DataView (i_dSourceTable);

            //getting distinct values for group column 
            DataTable dtGroup = dv.ToTable (true, new string[] { i_sGroupByColumn });

            //adding column for the row count 
            dtGroup.Columns.Add ("Count", typeof (int));

            //looping thru distinct values for the group, counting 
            foreach (DataRow dr in dtGroup.Rows)
            {
                dr["Count"] = i_dSourceTable.Compute ("Count(" + i_sAggregateColumn + ")", i_sGroupByColumn + " = '" + dr[i_sGroupByColumn] + "'");
            }

            //returning grouped/counted result 
            return dtGroup;
        }



        /// <summary>
        /// This finds last visit log id
        /// </summary>
        public int GetVisitLogId (int userId, int constrainCount)
        {
            int visitLogId = 0;

            Sphinx sphinx = new Sphinx ();

            using (ConnectionBase connection = new PersistentTcpConnection (sphinx.Host, sphinx.Port))
            {
                SearchQuery searchQuery = new SearchQuery ("");
                connection.ConnectionTimeout = 5000;

                // Set match mode to SPH_MATCH_EXTENDED2
                searchQuery.MatchMode = MatchMode.Extended2;

                // Add Sphinx index name to list
                searchQuery.Indexes.Add ("visit_log_srch_idx");

                // Set up paging
                searchQuery.Limit = constrainCount;
                searchQuery.Offset = 0;

                // Create search command object
                SearchCommand searchCommand = new SearchCommand (connection);

                // Add newly created search query object to query list
                searchCommand.QueryList.Add (searchQuery);

                // Sorting

                searchQuery.SortMode = ResultsSortMode.AttributeDescending;
                searchQuery.SortBy = "visited_at";

                if (userId > 0)
                {
                    searchQuery.AttributeFilters.Add ("kaneva_user_id", (uint) userId, false);
                }

                // Execute command on server and obtain results
                try
                {
                    searchCommand.Execute ();
                }
                catch (Exception exc)
                {
                    m_logger.Error ("Error calling Sphinx " + sphinx.Host + ":" + sphinx.Port, exc);
                }

                // Build result IN clause for MySQL
                foreach (SearchQueryResult result in searchCommand.Result.QueryResults)
                {
                    foreach (Match match in result.Matches)
                    {
                        visitLogId = Convert.ToInt32 (match.DocumentId);
                    }
                }
            }

            return visitLogId;
        }

        /// <summary>
        /// GetMostVisited
        /// </summary>
        public DataTable GetMostVisited (int userId, bool HideAP, int pageNumber, int pageSize, int constrainCount, int wokGameId, bool includePopulation)
        {
            Sphinx sphinx = new Sphinx ();
            string visitLogIds = "";
            int totalCount = 0;

            int visitLogId = GetVisitLogId (userId, constrainCount);

            // No visits found
            if (visitLogId.Equals (0))
            {
                DataSet ds = new DataSet ();
                DataTable dt = new DataTable ();
                ds.Tables.Add (dt);
                return dt;
            }

            using (ConnectionBase connection = new PersistentTcpConnection (sphinx.Host, sphinx.Port))
            {
                connection.ConnectionTimeout = 5000;

                SearchQuery searchQuery = new SearchQuery ("");

                // Set match mode to SPH_MATCH_EXTENDED2
                searchQuery.MatchMode = MatchMode.Extended2;

                // Add Sphinx index name to list
                searchQuery.Indexes.Add ("visit_log_srch_idx");

                // Set up paging
                searchQuery.Limit = constrainCount;
                searchQuery.Offset = (pageNumber - 1) * constrainCount;

                // Create search command object
                SearchCommand searchCommand = new SearchCommand (connection);

                // Add newly created search query object to query list
                searchCommand.QueryList.Add (searchQuery);

                searchQuery.AttributeFilters.Add ("visit_log_id2", (uint) visitLogId, (uint) UInt32.MaxValue, false);

                searchQuery.GroupBy = "compound_guid";
                searchQuery.GroupFunc = ResultsGroupFunction.Attribute;
                searchQuery.GroupSort = "@count desc";
                // searchQuery.GroupSort = "zone_type, zone_instance_id, game_id";

                if (userId > 0)
                {
                    searchQuery.AttributeFilters.Add ("kaneva_user_id", (uint) userId, false);
                }

                if (HideAP)
                {

                }

                // Sorting
                //searchQuery.SortMode = ResultsSortMode.AttributeDescending;
                //searchQuery.SortBy = "@group";

                // Execute command on server and obtain results
                try
                {
                    searchCommand.Execute ();
                }
                catch (Exception exc)
                {
                    m_logger.Error ("Error calling Sphinx " + sphinx.Host + ":" + sphinx.Port, exc);
                }

                // Build result IN clause for MySQL
                foreach (SearchQueryResult result in searchCommand.Result.QueryResults)
                {
                    foreach (Match match in result.Matches)
                    {
                        if (visitLogIds.Length > 0)
                        {
                            visitLogIds = visitLogIds + "," + match.DocumentId.ToString ();
                        }
                        else
                        {
                            visitLogIds = match.DocumentId.ToString ();
                        }
                    }

                    // Note this will grab the last total count in collection, ok for this case, but not best place to do this
                    totalCount = (result.TotalFound > searchQuery.MaxMatches ? searchQuery.MaxMatches : result.TotalFound);
                }

                // Nothing found in Sphinx
                if (visitLogIds.Length.Equals (0))
                {
                    DataSet ds = new DataSet ();
                    DataTable dt = new DataTable ();
                    ds.Tables.Add (dt);
                    return dt;
                }

                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

                string strSelect = "( SELECT visit_log_id, c.name as location, " +
                 " wok.concat_vw_url('" + wokGameId.ToString () + "',c.name,6) as STPURL, 0 as game_id, " +
                 " c.thumbnail_medium_path as image_path, c.community_id " + (includePopulation ? ", pc.population " : "") +
                 " FROM developer.visit_log v " +
                     " INNER JOIN wok.supported_worlds sw ON wok.zoneIndex(v.zone_index) = sw.zone_index " +
                     " INNER JOIN wok.channel_zones cz ON v.zone_index = cz.zone_index and v.zone_instance_id = cz.zone_instance_id " +
                     " INNER JOIN kaneva.communities c ON cz.zone_instance_id = c.community_id AND c.name NOT LIKE 'Preview Deed - %' " +
                     (includePopulation ? " INNER JOIN wok.worlds_cache pc ON pc.zone_index = v.zone_index AND pc.zone_instance_id = v.zone_instance_id " : "") +
                     " WHERE v.zone_type = 6 " +
                     " AND v.visit_log_id in (" + visitLogIds + ")" +
                  " ) UNION (" +
                      " SELECT visit_log_id, IF(length(c.name) = 0, 'unknown', c.name)  AS location,  " +
                      " wok.concat_vw_url(c.name,'',0) as STPURL,  g.game_id, " +
                      " c.thumbnail_medium_path as image_path, cg.community_id " + (includePopulation ? ", pc.population " : "") +
                      " FROM developer.visit_log v  " +
                      " INNER JOIN developer.games g ON v.game_id = g.game_id " +
                      " INNER JOIN kaneva.communities_game cg ON g.game_id = cg.game_id " +
                      " INNER JOIN kaneva.communities c ON cg.community_id = c.community_id AND c.name NOT LIKE 'Preview Deed - %' " +
                      " INNER JOIN developer.game_servers gs ON gs.game_id = g.game_id " +
                      (includePopulation ? " INNER JOIN wok.worlds_cache pc ON pc.game_id = g.game_id " : "") +
                      " WHERE v.game_id > 0 " +
                      " AND v.zone_type = 0 " +
                       " AND v.visit_log_id in (" + visitLogIds + ")" +
                       " AND g.game_status_id = 1  " +
                       " AND gs.server_status_id = 1   " +
                       " AND timestampdiff(minute, last_ping_datetime, NOW()) < wok.deadServerInterval() " +
                       " AND gs.visibility_id = 1 " +
                       " AND g.game_access_id = 1 " +
                       " AND gs.is_external = 1 " +
                       " ) UNION (" +
                    // These are faux 3dapps
                       " SELECT visit_log_id, IF(length(c.name) = 0, 'unknown', c.name)  AS location,  " +
                      " wok.concat_vw_url('" + wokGameId.ToString() + "',c.name,4) as STPURL,  0 as game_id, " +
                      " c.thumbnail_medium_path as image_path, cg.community_id " + (includePopulation ? ", pc.population " : "") +
                      " FROM developer.visit_log v " +
                      "  INNER JOIN developer.faux_3d_app f3d ON v.zone_index = f3d.zone_index " +
                       " INNER JOIN developer.games g ON f3d.game_id = g.game_id " +
                       " INNER JOIN kaneva.communities_game cg ON g.game_id = cg.game_id " +
                       " INNER JOIN kaneva.communities c ON cg.community_id = c.community_id " +
                       (includePopulation ? " INNER JOIN wok.worlds_cache pc ON pc.zone_index = v.zone_index AND pc.zone_instance_id = v.zone_instance_id " : "") +
                       " WHERE v.game_id = 0 " +
                       " AND v.zone_type = 4 " +
                       " AND v.visit_log_id in (" + visitLogIds + ")" +

                    ") ORDER BY FIELD(visit_log_id," + visitLogIds + ") limit " + pageSize;


                return Db.Master.GetDataTable (strSelect, parameters);

                //PagedList<Visit> list = new PagedList<Visit>();
                //list.TotalCount = Convert.ToUInt32(dt.Rows.Count);
                //Visit visit;

                //foreach (DataRow row in dt.Rows)
                //{
                //    visit = new Visit();


                //    list.Add(visit);
                //}

                //return list;

            }
        }

        /// <summary>
        /// Get Top 3Dapps
        /// </summary>
        public DataTable Top3Dapps(bool onlyAccessPass, bool HideAP, int pageNumber, int pageSize, ref int totalCount, int wokGameId, bool hideOwnerOnly = false)
        {
            Sphinx sphinx = new Sphinx ();
            string communityIds = "";
            totalCount = 0;

            using (ConnectionBase connection = new PersistentTcpConnection (sphinx.Host, sphinx.Port))
            {
                connection.ConnectionTimeout = 5000;

                SearchQuery searchQuery = new SearchQuery ("");

                // Set match mode to SPH_MATCH_EXTENDED2
                searchQuery.MatchMode = MatchMode.Extended2;

                // Add Sphinx index name to list
                searchQuery.Indexes.Add ("threed_app_visits_srch_idx");

                // Set up paging
                searchQuery.Limit = pageSize;
                searchQuery.Offset = (pageNumber - 1) * pageSize;

                // Create search command object
                SearchCommand searchCommand = new SearchCommand (connection);

                // Add newly created search query object to query list
                searchCommand.QueryList.Add (searchQuery);

                if (HideAP)
                {
                    // Only show 0
                    searchQuery.AttributeFilters.Add("pass_group_id", (uint)0, false);
                }

                if (hideOwnerOnly)
                {
                    if (searchQuery.Query.Length > 0)
                    {
                        searchQuery.Query = searchQuery.Query + " & ";
                    }

                    searchQuery.Query = searchQuery.Query + " @is_public (Y | N) ";
                }

                // Sorting
                searchQuery.GroupBy = "community_id";
                searchQuery.GroupFunc = ResultsGroupFunction.Attribute;
                searchQuery.GroupSort = "@count desc";

                // Execute command on server and obtain results
                try
                {
                    searchCommand.Execute ();
                }
                catch (Exception exc)
                {
                    m_logger.Error ("Error calling Sphinx " + sphinx.Host + ":" + sphinx.Port, exc);
                }

                int[] arViewCounts = new int[0];
                int i = 0;
                int resultCount = 0;

                // Build result IN clause for MySQL
                foreach (SearchQueryResult result in searchCommand.Result.QueryResults)
                {
                    arViewCounts = new int[result.Matches.Count];

                    foreach (Match match in result.Matches)
                    {
                        // Save view_counts
                        arViewCounts[i] = (int)match.AttributesValues["@count"].GetValue();
                        i++;

                        if (communityIds.Length > 0)
                        {
                            communityIds = communityIds + "," + match.AttributesValues["community_id"].GetValue().ToString();
                        }
                        else
                        {
                            communityIds = match.AttributesValues["community_id"].GetValue().ToString();
                        }
                    }

                    // Note this will grab the last total count in collection, ok for this case, but not best place to do this
                    totalCount = (result.TotalFound > searchQuery.MaxMatches ? searchQuery.MaxMatches : result.TotalFound);
                    resultCount = result.Matches.Count;
                }

                // Nothing found in Sphinx
                if (communityIds.Length.Equals(0))
                {
                    DataSet ds = new DataSet ();
                    DataTable dt = new DataTable ();
                    ds.Tables.Add (dt);
                    return dt;
                }

                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

                string strSelect = "SELECT STPURL, game_id, community_id, zone_type AS zoneType, " +
                    " name, description, is_public, is_adult, keywords, over_21_required, " +
                    " creator_id, creator_username AS username, created_date, " +
                    " name_no_spaces, thumbnail_small_path, " +
                    " creator_name_no_spaces, creator_thumbnail_small_path, " +
                    " number_of_members, number_of_views, number_times_shared, number_of_diggs, 1 as server_status_id, NOW() as last_ping_datetime, " +
                    " 0 AS number_of_requests, number_of_diggs_past_7_days " +
                    " FROM wok.worlds_cache " +
                    " WHERE community_id IN (" + communityIds + ") " +
                    " ORDER BY FIELD (community_id, " + communityIds + ") LIMIT " + pageSize;

                DataTable dtResult = Db.Search.GetDataTable (strSelect, parameters);

                try
                {
                    // Update monthly views
                    int j = 0;
                    if (dtResult.Rows.Count <= resultCount)
                    {
                        foreach (DataRow dr in dtResult.Rows)
                        {
                            dr["number_of_views"] = arViewCounts[j];
                            j++;
                        }
                    }
                }
                catch (Exception exc)
                {
                    m_logger.Error ("Error setting monthly views", exc);
                }

                return dtResult;
            }
        }

        /// <summary>
        /// Get Unique Visits
        /// </summary>
        public DataTable GetUnique3DAppVisits (int pageNumber, int pageSize)
        {
            Sphinx sphinx = new Sphinx ();

            using (ConnectionBase connection = new PersistentTcpConnection (sphinx.Host, sphinx.Port))
            {
                connection.ConnectionTimeout = 5000;

                SearchQuery searchQuery = new SearchQuery ("");

                // Set match mode to SPH_MATCH_EXTENDED2
                searchQuery.MatchMode = MatchMode.Extended2;

                // Add Sphinx index name to list
                searchQuery.Indexes.Add ("visit_log_srch_idx");

                // Set up paging
                searchQuery.Limit = pageSize;
                searchQuery.Offset = (pageNumber - 1) * pageSize;

                // Create search command object
                SearchCommand searchCommand = new SearchCommand (connection);

                // Add newly created search query object to query list
                searchCommand.QueryList.Add (searchQuery);

                // ONly 3dapp zones
                searchQuery.AttributeFilters.Add ("zone_type", (uint) 0, false);

                searchQuery.GroupBy = "compound_guid_UV";
                searchQuery.GroupFunc = ResultsGroupFunction.Attribute;
                searchQuery.GroupSort = "@count desc";

                // Execute command on server and obtain results
                try
                {
                    searchCommand.Execute ();
                }
                catch (Exception exc)
                {
                    m_logger.Error ("Error calling Sphinx " + sphinx.Host + ":" + sphinx.Port, exc);
                }

                DataSet ds = new DataSet ();
                DataTable dt = new DataTable ();
                DataRow drNew;

                dt.Columns.Add (new DataColumn ("game_id", System.Type.GetType ("System.Int32")));
                dt.Columns.Add (new DataColumn ("visit_count_unique", System.Type.GetType ("System.Int32")));

                ds.Tables.Add (dt);

                //int[] arViewCounts = new int[0];
                //int[] arGameIDs = new int[0];
                int i = 0;

                // Build result IN clause for MySQL
                foreach (SearchQueryResult result in searchCommand.Result.QueryResults)
                {
                    //arViewCounts = new int[result.Matches.Count];
                    //arGameIDs = new int[result.Matches.Count];

                    foreach (Match match in result.Matches)
                    {
                        // Save view_counts
                        //arViewCounts[i] = (int)match.AttributesValues["@count"].GetValue();
                        //arGameIDs[i] = (int)match.AttributesValues["game_id"].GetValue();
                        drNew = dt.NewRow ();
                        drNew["game_id"] = (int) match.AttributesValues["game_id"].GetValue ();
                        drNew["visit_count_unique"] = (int) match.AttributesValues["@count"].GetValue ();
                        dt.Rows.Add (drNew);

                        i++;
                    }
                }

                return dt;
            }
        }

        /// <summary>
        /// InsertNotifcation
        /// </summary>
        public int InsertNotifcation (int userId, int packetId, int levelId, int badgeId)
        {
            string sqlString = " INSERT INTO notifications " +
                "(user_id, created_date, packet_id, level_id, achievement_id)" +
                " VALUES " +
                " (@userId, NOW(), @packetId, @levelId, @badgeId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@packetId", packetId));
            parameters.Add (new MySqlParameter ("@levelId", levelId));
            parameters.Add (new MySqlParameter ("@badgeId", badgeId));
            return Db.Master.ExecuteIdentityInsert (sqlString, parameters);
        }

        public int InsertNotificationAppRequest (int userId, int gameId)
        {
            string sqlString = " INSERT INTO notification_requests " +
              "(user_id, game_id, number_of_requests)" +
              " VALUES " +
              " (@userId, @gameId, 1) " +
              " ON DUPLICATE KEY UPDATE number_of_requests = number_of_requests + 1 ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@gameId", gameId));
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int ClearNotificationAppRequest (int userId, int gameId)
        {
            string sqlString = " DELETE FROM notification_requests " +
                 " WHERE user_id = @userId AND game_id = @gameId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@gameId", gameId));
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int GetNumberOfNotificationAppRequest (int userId, int gameId)
        {
            string strQuery = "SELECT number_of_requests " +
            " FROM notification_requests " +
            " WHERE user_id = @userId AND game_id = @gameId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@gameId", gameId));
            DataRow row = Db.Master.GetDataRow (strQuery, parameters);

            if (row == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32 (row["number_of_requests"]);
            }
        }

        public int GetTotalNotificationAppRequests (int userId)
        {
            string strQuery = "SELECT COALESCE(SUM(number_of_requests),0) AS total_requests " +
            " FROM notification_requests nr " +
            " INNER JOIN kaneva.communities_game cg ON cg.game_id = nr.game_id " +
            " INNER JOIN communities c on c.community_id = cg.community_id " +
            " WHERE user_id = @userId " +
            " AND c.status_id = 1";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            DataRow row = Db.Master.GetDataRow (strQuery, parameters);

            if (row == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32 (row["total_requests"]);
            }
        }

        public int GetNumberOfVisitorsForGamesByOwner (int userId, DateTime startDate)
        {
            //return GetMostVisited(userId, false, 1, 1000, 1000, 3298).Rows.Count;

            Sphinx sphinx = new Sphinx();
            int totalCount = 0;
            int visitorCount = 0;
            int constrainCount = 0;
            int pageNumber = 1;

            using (ConnectionBase connection = new PersistentTcpConnection(sphinx.Host, sphinx.Port))
            {
                connection.ConnectionTimeout = 5000;

                SearchQuery searchQuery = new SearchQuery("");

                // Set match mode to SPH_MATCH_EXTENDED2
                searchQuery.MatchMode = MatchMode.Extended2;

                // Add Sphinx index name to list
                searchQuery.Indexes.Add("visit_log_srch_idx");

                // Set up paging
                constrainCount = searchQuery.MaxMatches;
                searchQuery.Limit = constrainCount;
                searchQuery.Offset = (pageNumber - 1) * constrainCount;

                // Create search command object
                SearchCommand searchCommand = new SearchCommand(connection);

                // Add newly created search query object to query list
                searchCommand.QueryList.Add(searchQuery);

                searchQuery.AttributeFilters.Add("game_id", (uint)3296, true);
                searchQuery.AttributeFilters.Add("game_id", (uint)3298, true);
                searchQuery.AttributeFilters.Add("game_id", (uint)5316, true);

                searchQuery.AttributeFilters.Add("creator_id", (uint)userId, false);
                searchQuery.AttributeFilters.Add("visited_at", startDate, DateTime.Now.AddYears(1), false);

                searchQuery.GroupBy = "compound_guid_UV_Hour";
                searchQuery.GroupFunc = ResultsGroupFunction.Attribute;
                searchQuery.GroupSort = "@count desc";

                // Execute command on server and obtain results
                try
                {
                    searchCommand.Execute();
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error calling Sphinx " + sphinx.Host + ":" + sphinx.Port, exc);
                }

                // Build result IN clause for MySQL
                foreach (SearchQueryResult result in searchCommand.Result.QueryResults)
                {
                    foreach (Match match in result.Matches)
                    {
                        visitorCount++;
                    }

                    // Note this will grab the last total count in collection, ok for this case, but not best place to do this
                    totalCount = (result.TotalFound > searchQuery.MaxMatches ? searchQuery.MaxMatches : result.TotalFound);
                }
            }

            return visitorCount;
        }

        public bool InsertAppMessageQueue (int gameId, string msgType, string msgSource)
        {
            string sql = "INSERT INTO wok.app_message_queue " +
               "(id, app_id, created_date, msg_type, msg_source) " +
               " VALUES " +
               " (UUID(), @gameId, NOW(), @msgType, @msgSource) ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@gameId", gameId));
            parameters.Add (new MySqlParameter ("@msgType", msgType));
            parameters.Add (new MySqlParameter ("@msgSource", msgSource));
            return (Db.Master.ExecuteNonQuery (sql, parameters) != 0);
        }

        public DataTable GetAppMessageQueue (int gameId)
        {
            string strQuery = "SELECT id, created_date, msg_type, msg_source " +
                " FROM wok.app_message_queue " +
                " WHERE app_id = @gameId " +
                " AND response_date IS NULL";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@gameId", gameId));
            return Db.Master.GetDataTable (strQuery, parameters);
        }

        public bool UpdateAppMessageQueue (string id, string clientResponseId, string clientResponseCode, DateTime clientProcessedDate)
        {
            string sql = "UPDATE wok.app_message_queue " +
                " SET client_response_id = @clientResponseId, " +
                " client_response_code = @clientResponseCode, " +
                " client_processed_date = @clientProcessedDate, " +
                " response_date = NOW() " +
                " WHERE id = @id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@clientResponseId", clientResponseId));
            parameters.Add (new MySqlParameter ("@clientResponseCode", clientResponseCode));
            parameters.Add (new MySqlParameter ("@clientProcessedDate", clientProcessedDate));
            return (Db.Master.ExecuteNonQuery (sql, parameters) != 0);
        }

        public DataRow GetGameFaux3DApp (int gameId)
        {
            string key = CentralCache.keyFaux3DApps;
            DataRow drGame = null;

            DataTable dt = (DataTable) CentralCache.Get (key);
            if (dt == null)
            {
                string sql = "SELECT f3.game_id, f3.zone_index, c.name AS game_name " +
                " FROM faux_3d_app f3 " +
                " INNER JOIN kaneva.communities_game cg ON cg.game_id = f3.game_id " +
                " INNER JOIN kaneva.communities c ON c.community_id = cg.community_id ";

                List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
                dt = Db.Developer.GetDataTable (sql, parameters);

                CentralCache.Store (key, dt, TimeSpan.FromMinutes (FAUX_3D_APP_CACHE_LIMIT_MIN));
            }

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow[] dr = dt.Select ("game_id=" + gameId);
                if (dr.Length == 1)
                {
                   drGame = dr[0];
                }
            }

            return drGame;
        }

        /// <summary>
        /// Get zone name
        /// </summary>
        public string GetZoneNameFromZoneIndex (int zoneIndex)
        {
            string sqlSelect = "SELECT name " +
                " FROM wok.channel_zones " +
                " WHERE zone_index = @zoneIndex " +
                " LIMIT 1";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@zoneIndex", zoneIndex));

            return Db.Master.GetScalar (sqlSelect, parameters).ToString ();
        }

        public PagedList<WOKItem> GetScriptAvailableItems(int pageNumber, int pageSize)
        {
            string strSelect = "SELECT DISTINCT(i.global_id) as global_id,name,i.description,market_cost, " +
                   " selling_price, use_type, " +
                   " iw.display_name, COALESCE(pass_group_id,0) as passTypeId, iw.date_added, " +
                   " item_creator_id, iw.item_active, " +
                   " inventory_type, i.base_global_id, iw.template_path, iw.texture_path, " +
                   " iw.thumbnail_path, iw.thumbnail_small_path, iw.thumbnail_medium_path, iw.thumbnail_large_path, iw.thumbnail_assetdetails_path, iw.designer_price, iw.keywords, " +
                   " (i.market_cost + iw.designer_price + COALESCE(iw2.designer_price,0)) as WebPrice, " +
                   " iw.number_sold_on_web, iw.number_of_raves, iw.number_of_views, username as owner_username, " +
                   " COALESCE(iw2.designer_price,0) as TemplateDesignerPrice, " +
                   " iw.category_id1, iw.category_id2, iw.category_id3, COALESCE(ia.item_id,0) as has_animation " +
                " FROM wok.script_available_items_plain wtdi " +
                " INNER JOIN wok.items i ON wtdi.global_id = i.global_id " +
                " INNER JOIN shopping.items_web iw ON i.global_id = iw.global_id " +
                " LEFT OUTER JOIN shopping.items_web iw2 ON iw.base_global_id = iw2.global_id " +
                " LEFT JOIN wok.item_parameters ip ON i.global_id = ip.global_id AND i.use_type = ip.param_type_id AND use_type <= 13 " +
                " LEFT OUTER JOIN kaneva.users u ON i.item_creator_id = u.user_id " +
                " LEFT OUTER JOIN wok.pass_group_items pgi ON pgi.global_id = i.global_id AND pgi.pass_group_id = 1 " +
                " LEFT OUTER JOIN wok.item_animations ia ON ia.item_id = i.global_id ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            PagedDataTable dt = Db.WOK.GetPagedDataTable(strSelect, "wtdi.global_id ASC", parameters, pageNumber, pageSize);

            PagedList<WOKItem> list = new PagedList<WOKItem>();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                WOKItem wokItem = new WOKItem(Convert.ToInt32(row["global_id"]), row["name"].ToString(), row["description"].ToString(), Convert.ToInt32(row["market_cost"]),
                Convert.ToInt32(row["selling_price"]), 0, 0, Convert.ToInt32(row["use_type"]),
                row["display_name"].ToString(), Convert.ToInt32(row["passTypeId"]),
                Convert.ToDateTime(row["date_added"]), Convert.ToUInt32(row["item_creator_id"]), Convert.ToInt32(row["item_active"]), 0,
                0, 0, 0,
                Convert.ToInt32(row["inventory_type"]), Convert.ToUInt32(row["base_global_id"]), row["template_path"].ToString(), row["texture_path"].ToString(),
                row["thumbnail_path"].ToString(), row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(), row["thumbnail_assetdetails_path"].ToString(), Convert.ToUInt32(row["designer_price"]),
                row["keywords"].ToString(), Convert.ToInt32(row["WebPrice"]), 0, Convert.ToUInt32(row["number_sold_on_web"]), Convert.ToUInt32(row["number_of_raves"]),
                Convert.ToUInt32(row["number_of_views"]), 0, 0, 0, WOKItem.DRL_UNKNOWN, Convert.ToUInt32(row["TemplateDesignerPrice"]),
                Convert.ToUInt32(row["category_id1"]), Convert.ToUInt32(row["category_id2"]), Convert.ToUInt32(row["category_id3"]),
                0,
                Convert.ToBoolean(row["has_animation"])
                );

                if (wokItem.UseType == WOKItem.USE_TYPE_BUNDLE)
                {
                    wokItem.BundleWebPrice = Convert.ToInt32(row["market_cost"]);
                }

                list.Add(wokItem);
            }
            return list;
        }

        /// <summary>
        /// Called to associate default global_ids with a template.
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="globalIds"></param>
        /// <returns></returns>
        public int InsertScriptAvailableItemsIds(int[] globalIds)
        {
            int rowsAffected = 0;

            if (globalIds.Length > 0)
            {
                string sql = " INSERT INTO script_available_items_plain (global_id) VALUES ";
                List<IDbDataParameter> parameters = new List<IDbDataParameter>();

                for (int x = 0; x < globalIds.Length; x++)
                {
                    sql += string.Format(" (@globalId{0}),", x);
                    parameters.Add(new MySqlParameter("@globalId" + x, globalIds[x]));
                }

                sql = sql.TrimEnd(',');

                rowsAffected = Db.WOK.ExecuteNonQuery(sql, parameters);
            }

            return rowsAffected;
        }

        /// <summary>
        /// Called to disassociate default global_ids with a template.
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="globalIds"></param>
        /// <returns></returns>
        public int DeleteScriptAvailableItemsIds(int[] globalIds)
        {
            int rowsAffected = 0;

            if (globalIds.Length > 0)
            {
                string sql = " DELETE FROM script_available_items_plain WHERE global_id IN (" + string.Join(", ", globalIds) + ")";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();

                rowsAffected = Db.WOK.ExecuteNonQuery(sql, parameters);
            }

            return rowsAffected;
        }

        public DataSet GetPremiumItems (int premiumItemTypeId)
        {
            string strQuery = "SELECT i.global_id, i.name, i.market_cost AS selling_price " +   
                " FROM wok.items i " +
                " INNER JOIN wok.item_parameters ip ON ip.global_id = i.global_id " + 
                " WHERE i.use_type = " + WOKItem.USE_TYPE_PREMIUM +	//205   
                " AND ip.param_type_id = 35 " +     
                " AND ip.value = @premiumItemTypeId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@premiumItemTypeId", premiumItemTypeId));

            return Db.WOK.GetDataSet (strQuery, parameters);
        }

        public DataTable GetLinkedZones (int communityId, int zoneInstanceId, int zoneType, int wokGameId)
        {
            string strSelect = "SELECT c.name, c.community_id, uwi.zone_instance_id, uwi.zone_type, " +
                " wok.concat_vw_url('" + wokGameId.ToString() + "', c.name, uwi.zone_type) AS STPURL, " +
                " c.thumbnail_medium_path AS image_thumbnail, c.created_date, 0 AS x " +
                " FROM wok.unified_world_ids uwi " +
                " INNER JOIN kaneva.communities c ON c.community_id = uwi.community_id " +
                " WHERE(zone_instance_id = @zoneInstanceId AND zone_type = @zoneType) " +
                " AND c.status_id = 1 " + 
                " UNION " +
                " SELECT c.name, c.community_id, uwi.zone_instance_id, uwi.zone_type, wok.concat_vw_url('" + wokGameId.ToString() + "', c.name, uwi.zone_type) AS STPURL, " +
                " c.thumbnail_medium_path AS image_thumbnail, c.created_date, 1 AS x " +
                " FROM wok.unified_world_ids uwi " +
                " INNER JOIN kaneva.communities c ON c.community_id = uwi.community_id " +
                " WHERE c.parent_community_id = @communityId " +
                " AND c.status_id = 1 " +
                " ORDER BY x, created_date ASC ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));

            return Db.Master.GetDataTable(strSelect, parameters);
        }

        public bool IsChildZone(int zoneInstanceId, int zoneType, out int parentCommunityId)
        {
            parentCommunityId = 0;

            string select = "SELECT parent_community_id " +
                "FROM wok.unified_world_ids ids " +
                "INNER JOIN kaneva.communities c ON ids.community_id = c.community_id " +
                "WHERE ids.zone_instance_id = @zoneInstanceId " +
                "AND ids.zone_type = @zoneType " +
                "AND c.status_id = @status ";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));
            parameters.Add(new MySqlParameter("@status", (int)CommunityStatus.ACTIVE));

            var result = Db.Master.GetScalar(select, parameters);
            if (result == null || result == DBNull.Value)
                return false;

            parentCommunityId = Convert.ToInt32(result);
            return true;
        }

        #region MetaGameItems

        public MetaGameItem GetMetaGameItem(string itemName)
        {
            if (string.IsNullOrWhiteSpace(itemName))
            {
                return new MetaGameItem();
            }

            string strQuery = 
                "SELECT mgi.item_name, mgi.item_type, mgi.conversion_value_rewards, mgi.conversion_value_credits, " +
                "mgit.redemption_rewards_limit, mgit.redemption_credits_limit " +
                "FROM meta_game_items mgi " +
                "INNER JOIN meta_game_item_types mgit ON mgi.item_type = mgit.item_type " +
                "WHERE mgi.item_name = @itemName ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@itemName", itemName));

            DataRow row = Db.Master.GetDataRow(strQuery, parameters);

            if (row == null)
            {
                return new MetaGameItem();
            }

            return new MetaGameItem(row["item_name"].ToString(), row["item_type"].ToString(),
                Convert.ToInt32(row["conversion_value_rewards"]), Convert.ToInt32(row["conversion_value_credits"]),
                Convert.ToInt32(row["redemption_rewards_limit"]), Convert.ToInt32(row["redemption_credits_limit"]));
        }

        public List<MetaGameItem> GetMetaGameItems(string itemType)
        {
            if (string.IsNullOrWhiteSpace(itemType))
            {
                return new List<MetaGameItem>();
            }

            string strQuery =
                "SELECT mgi.item_name, mgi.item_type, mgi.conversion_value_rewards, mgi.conversion_value_credits, " +
                "mgit.redemption_rewards_limit, mgit.redemption_credits_limit " +
                "FROM meta_game_items mgi " +
                "INNER JOIN meta_game_item_types mgit ON mgi.item_type = mgit.item_type " +
                "WHERE mgi.item_type = @itemType ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@itemType", itemType));

            DataTable dt = Db.Master.GetDataTable(strQuery, parameters);

            if (dt == null || dt.Rows.Count == 0)
            {
                return new List<MetaGameItem>();
            }

            List<MetaGameItem> items = new List<MetaGameItem>();
            foreach (DataRow row in dt.Rows)
            {
                items.Add(new MetaGameItem(row["item_name"].ToString(), row["item_type"].ToString(),
                    Convert.ToInt32(row["conversion_value_rewards"]), Convert.ToInt32(row["conversion_value_credits"]),
                    Convert.ToInt32(row["redemption_rewards_limit"]), Convert.ToInt32(row["redemption_credits_limit"])));
            }

            return items;
        }

        #endregion

        #region Game Leaderboard

        public int InsertObjectUserInteraction(int userId, int zoneInstanceId, int zoneType, int objPlacementId, string name, string interactionType)
        {
            string sql = "INSERT INTO wok.world_object_interaction " +
               "(kaneva_user_id, zone_instance_id, zone_type, obj_placement_id, name, interaction_type, created_date) " +
               " VALUES " +
               " (@userId, @zoneInstanceId, @zoneType, @objPlacementId, @name, @interactionType, NOW()) ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));
            parameters.Add(new MySqlParameter("@objPlacementId", objPlacementId));
            parameters.Add(new MySqlParameter("@name", name));
            parameters.Add(new MySqlParameter("@interactionType", interactionType));
            return Db.WOK.ExecuteNonQuery(sql, parameters);
        }

        public int UpdateLeaderboardObjectName(int objPlacementId, string name)
        {
            string sql = "UPDATE wok.world_object_interaction " +
               "SET name = @name " +
               "WHERE obj_placement_id = @objPlacementId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@objPlacementId", objPlacementId));
            parameters.Add(new MySqlParameter("@name", name));
            return Db.WOK.ExecuteNonQuery(sql, parameters);
        }

        public bool HasUserRavedGameObject(int userId, int objPlacementId)
        {
            string sql = "SELECT obj_placement_id " +
            " FROM wok.world_object_interaction " +
            " WHERE kaneva_user_id = @userId " +
            " AND obj_placement_id = @objPlacementId " +
            " AND interaction_type = 'RAVE'";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@objPlacementId", objPlacementId));
            
            DataRow row = Db.WOK.GetDataRow(sql, parameters);
            return (row != null);
        }

        public bool HasUserVisitedGameObject(int userId, int objPlacementId)
        {
            string sql = "SELECT obj_placement_id " +
            " FROM wok.world_object_interaction " +
            " WHERE kaneva_user_id = @userId " +
            " AND obj_placement_id = @objPlacementId " +
            " AND interaction_type = 'VISIT'" +
            " AND DATEDIFF(NOW(), created_date) = 0";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@objPlacementId", objPlacementId));

            DataRow row = Db.WOK.GetDataRow(sql, parameters);
            return (row == null);
        }

        public int UpdateTeamMembers(List<int> userIds, int objPlacementId)
        {
            int rowsAffected = 0;

            if (userIds != null && userIds.Count > 0)
            {
                // Remove all existing team members
                string sql = "DELETE FROM wok.world_leaderboard_team_members " +
                   " WHERE obj_placement_id = @objPlacementId";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@objPlacementId", objPlacementId));
                Db.WOK.ExecuteNonQuery(sql, parameters);


                // Insert the updated member list
                sql = "INSERT INTO wok.world_leaderboard_team_members " +
                    " (kaneva_user_id, obj_placement_id, date_added) " +
                    " VALUES ";

                parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@objPlacementId", objPlacementId));
                
                for (int x = 0; x < userIds.Count; x++)
                {
                    sql += string.Format(" (@userId{0},", x) + " @objPlacementId, NOW()),";
                    parameters.Add(new MySqlParameter("@userId" + x, userIds[x]));
                }

                sql = sql.TrimEnd(',');

                rowsAffected += Db.WOK.ExecuteNonQuery(sql, parameters);
            }

            return rowsAffected;
        }

        public DataTable GetTeamMembers(IEnumerable<int> objPlacementIds)
        {
            if (objPlacementIds == null || objPlacementIds.Count() == 0)
                return null;

            string sql = "SELECT tm.obj_placement_id, kaneva_user_id, u.username, c.thumbnail_small_path, " +
                " c.thumbnail_medium_path, c.thumbnail_large_path, c.thumbnail_xlarge_path, " +
                " c.thumbnail_square_path " +
                " FROM wok.world_leaderboard_team_members tm " +
                " INNER JOIN kaneva.users u ON u.user_id = tm.kaneva_user_id " +
                " INNER JOIN kaneva.communities c ON c.creator_id = tm.kaneva_user_id " +
                "   AND c.is_personal = 1 " +
                " WHERE tm.obj_placement_id IN (" + string.Join(",", objPlacementIds) + ")";

            return Db.WOK.GetDataTable(sql);
        }
        
        public DataTable GetTeamMembers (int objPlacementId)
        {
            return GetTeamMembers(new int[] {objPlacementId});
        }

        public DataTable GetLeaderboard(int zoneInstanceId, int zoneType, int ravePointValue, int visitPointValue)
        {
            string sql = "SELECT name, obj_placement_id, kaneva_user_id AS user_id, " +
                " SUM(IF(interaction_type='RAVE',@ravePointValue,0)) + SUM(IF(interaction_type='VISIT',@visitPointValue,0)) AS points " +
                " FROM wok.world_object_interaction " +
                " WHERE zone_instance_id = @zoneInstanceId " +
                " 	AND zone_type = @zoneType " +
                "   AND created_date > CONCAT(DATE(DATE_SUB(NOW(), INTERVAL (IF(DAYOFWEEK(NOW()) = 1, 8, DAYOFWEEK(NOW())) - 1) DAY)), ' 21:00:00') " +
                " GROUP BY obj_placement_id " +
                " ORDER BY points DESC";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));
            parameters.Add(new MySqlParameter("@ravePointValue", ravePointValue));
            parameters.Add(new MySqlParameter("@visitPointValue", visitPointValue));

            return Db.WOK.GetDataTable(sql, parameters);
        }

        public DataTable GetFinalLeaderboard(int zoneInstanceId, int zoneType)
        {
            string sql = "SELECT ranking, points, wl.obj_placement_id, name, created_date, world_leaderboard_id " +
                " FROM wok.world_leaderboard wl " +
                " WHERE zone_type = @zoneType " +
                " AND zone_instance_id = @zoneInstanceId " +
                " AND created_date >= " +
                "   CONCAT(DATE(DATE_SUB(NOW(), INTERVAL (IF(DAYOFWEEK(NOW()) = 1, IF (CURTIME() > '21:00:00',1,8), DAYOFWEEK(NOW())) - 1) DAY)), ' 21:00:00') " +
                " ORDER BY ranking";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));

            return Db.WOK.GetDataTable(sql, parameters);
        }

        public DataTable GetFinalLeaderboardTeamMembers(int zoneInstanceId, int zoneType)
        {
            string sql = "SELECT kaneva_user_id, username, wl.obj_placement_id, award_quantity, tmh.world_leaderboard_id, " +
                " c.thumbnail_small_path, c.thumbnail_medium_path, c.thumbnail_large_path, c.thumbnail_xlarge_path, " +
                " c.thumbnail_square_path " +
                " FROM wok.world_leaderboard_team_members_history tmh " +
                " INNER JOIN wok.world_leaderboard wl ON wl.world_leaderboard_id = tmh.world_leaderboard_id " +
                " INNER JOIN kaneva.users u ON u.user_id = tmh.kaneva_user_id " +
                " INNER JOIN kaneva.communities c ON c.creator_id = tmh.kaneva_user_id " +
                "   AND c.is_personal = 1 " +
                " WHERE zone_type = @zoneType " +
                " AND zone_instance_id = @zoneInstanceId " +
                "  AND wl.created_date >= " + 
                "    CONCAT(DATE(DATE_SUB(NOW(), INTERVAL (IF(DAYOFWEEK(NOW()) = 1, IF (CURTIME() > '21:00:00',1,8), DAYOFWEEK(NOW())) - 1) DAY)), ' 21:00:00') " + 
                " ORDER BY wl.ranking";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));

            return Db.WOK.GetDataTable(sql, parameters);
        }

        public DataTable GetUserLeaderboardAwards(int zoneInstanceId, int zoneType, int userId, string username)
        {
            string sql = "SELECT kaneva_user_id AS user_id, @username AS username, ranking, " +
                " award_quantity, tmh.obj_placement_id, name, date_awarded, wl.world_leaderboard_id " +
                " FROM wok.world_leaderboard_team_members_history tmh " +
                " INNER JOIN wok.world_leaderboard wl ON wl.world_leaderboard_id = tmh.world_leaderboard_id " +
                " WHERE kaneva_user_id = @userId " +
                " AND wl.zone_instance_id = @zoneInstanceId " +
                " AND wl.zone_type = @zoneType " +
                " AND award_quantity > 0 " +
                " AND date_redeemed IS NULL " +
                " ORDER BY date_awarded";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@username", username));

            return Db.WOK.GetDataTable(sql, parameters);
        }

        public bool RedeemUserLeaderboardAward(int userId, IEnumerable<int> worldLeaderboardIds)
        {
            if (worldLeaderboardIds == null || worldLeaderboardIds.Count() == 0)
            {
                return false;
            }

            string sql = "UPDATE wok.world_leaderboard_team_members_history " +
                " SET date_redeemed = NOW() " +
                " WHERE kaneva_user_id = @userId " +
                " AND world_leaderboard_id IN (" + string.Join(",", worldLeaderboardIds) +")";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));

            return (Db.Master.ExecuteNonQuery(sql, parameters) != 0);
        }

        public bool RedeemUserLeaderboardAward(int userId, int worldLeaderboardId)
        {
            return RedeemUserLeaderboardAward(userId, new int[] { worldLeaderboardId });
        }

        public DataTable CreateLeaderboardFinalResults(int ravePointValue, int visitPointValue)
        {
            string sql = "SELECT obj_placement_id, zone_type, zone_instance_id, " + 
                " max(created_date) AS max_date, " +
                " (select name from wok.world_object_interaction b where max(a.created_date) = b.created_date limit 1) AS flag_name, " +
                " SUM(IF(interaction_type='RAVE',@ravePointValue,0)) + SUM(IF(interaction_type='VISIT',@visitPointValue,0)) AS points " +
                " FROM wok.world_object_interaction a " +
                " WHERE created_date BETWEEN " +
                "    DATE_SUB( CONCAT(DATE(DATE_SUB(NOW(), INTERVAL (IF(DAYOFWEEK(NOW()) = 1, IF (CURTIME() > '21:00:00',1,8), DAYOFWEEK(NOW())) - 1) DAY)), ' 21:00:01'), INTERVAL 7 DAY ) AND " +
                "              CONCAT(DATE(DATE_SUB(NOW(), INTERVAL (IF(DAYOFWEEK(NOW()) = 1, IF (CURTIME() > '21:00:00',1,8), DAYOFWEEK(NOW())) - 1) DAY)), ' 21:00:00') " +
                " AND date_processed IS NULL " +
                " GROUP BY obj_placement_id " +
                " ORDER BY zone_instance_id, zone_type, points DESC";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@ravePointValue", ravePointValue));
            parameters.Add(new MySqlParameter("@visitPointValue", visitPointValue));

            return Db.WOK.GetDataTable(sql, parameters);
        }

        public int InsertLeaderboardFinalResults(int objPlacementId, int zoneInstanceId, int zoneType, string name, int ranking, int points)
        {
            string sql = "INSERT INTO wok.world_leaderboard " +
                " (obj_placement_id, zone_type, zone_instance_id, name, ranking, points, created_date) " +
                " VALUES " +
                " (@objPlacementId, @zoneType, @zoneInstanceId, @name, @ranking, @points, NOW()) ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@zoneInstanceId", zoneInstanceId));
            parameters.Add(new MySqlParameter("@zoneType", zoneType));
            parameters.Add(new MySqlParameter("@objPlacementId", objPlacementId));
            parameters.Add(new MySqlParameter("@name", name));
            parameters.Add(new MySqlParameter("@ranking", ranking));
            parameters.Add(new MySqlParameter("@points", points));
            return Db.WOK.ExecuteIdentityInsert(sql, parameters);
        }

        public int InsertLeaderboardFinalTeamMembers(DataTable dtMembers, int worldLeaderboardId, int objPlacementId, int awardQuantity)
        {
            int rowsAffected = 0;

            if (dtMembers != null && dtMembers.Rows.Count > 0)
            {
                string sql = "INSERT INTO wok.world_leaderboard_team_members_history " +
                    " (kaneva_user_id, world_leaderboard_id, obj_placement_id, award_quantity, date_awarded) " +
                    " VALUES ";

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@worldLeaderboardId", worldLeaderboardId));
                parameters.Add(new MySqlParameter("@objPlacementId", objPlacementId));
                parameters.Add(new MySqlParameter("@awardQuantity", awardQuantity));

                for (int x = 0; x < dtMembers.Rows.Count; x++)
                {
                    sql += string.Format(" (@userId{0},", x) + " @worldLeaderboardId, @objPlacementId, @awardQuantity, NOW()),";
                    parameters.Add(new MySqlParameter("@userId" + x, dtMembers.Rows[x]["kaneva_user_id"]));
                }

                sql = sql.TrimEnd(',');

                rowsAffected += Db.WOK.ExecuteNonQuery(sql, parameters);
            }

            return rowsAffected;
        }

        public int UpdateObjectInteractionsAsProcessed()
        {
            string sql = "UPDATE wok.world_object_interaction " +
                " SET date_processed = NOW() " +
                "  WHERE created_date BETWEEN " + 
                "    DATE_SUB( CONCAT(DATE(DATE_SUB(NOW(), INTERVAL (IF(DAYOFWEEK(NOW()) = 1, IF (CURTIME() > '21:00:00',1,8), DAYOFWEEK(NOW())) - 1) DAY)), ' 21:00:01'), INTERVAL 7 DAY ) AND " + 
                "              CONCAT(DATE(DATE_SUB(NOW(), INTERVAL (IF(DAYOFWEEK(NOW()) = 1, IF (CURTIME() > '21:00:00',1,8), DAYOFWEEK(NOW())) - 1) DAY)), ' 21:00:00')";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            return Db.WOK.ExecuteNonQuery(sql, parameters);
        }

        public int CleanupTeamMembersHistory()
        {
            string sql = "DELETE " +
                "FROM wok.world_leaderboard_team_members_history " +
                "WHERE (date_redeemed IS NOT NULL OR award_quantity <= 0) " +
                "AND date_awarded <= DATE_SUB(NOW(), INTERVAL 2 WEEK) ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            return Db.WOK.ExecuteNonQuery(sql, parameters);
        }

        public int CleanupWorldLeaderboards()
        {
            int recordsAffected = 0;

            // It's possible to do deletion directly as a part of this more complicated query
            // but better to avoid it to prevent accidental locking.
            string sql = "SELECT l.world_leaderboard_id " +
                "FROM wok.world_leaderboard l " +
                "WHERE l.created_date <= DATE_SUB(NOW(), INTERVAL 2 WEEK) " + 
                "AND NOT EXISTS ( " +
	            "    SELECT 1 " + 
                "    FROM wok.world_leaderboard_team_members_history h " +
                "    WHERE h.world_leaderboard_id = l.world_leaderboard_id " +
                ") ";

            DataTable dt = Db.WOK.GetDataTable(sql, new List<IDbDataParameter>());
            if (dt != null && dt.Rows.Count > 0)
            {
                List<int> ids = new List<int>();
                foreach(DataRow row in dt.Rows)
                {
                    ids.Add(Convert.ToInt32(row["world_leaderboard_id"]));
                }
                sql = "DELETE FROM wok.world_leaderboard " +
                    "WHERE world_leaderboard_id IN (" + string.Join(",", ids) + ") ";
                recordsAffected = Db.WOK.ExecuteNonQuery(sql, new List<IDbDataParameter>());
            }

            return recordsAffected;
        }

        public int CleanupLeaderboardVisits()
        {
            string sql = "DELETE " +
                "FROM wok.world_object_interaction " +
                "WHERE interaction_type = 'VISIT' " +
                "AND created_date <= DATE_SUB(NOW(), INTERVAL 2 WEEK) ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            return Db.WOK.ExecuteNonQuery(sql, parameters);
        }

        #endregion Game Leaderboard

        public DataTable GetTutorial(string testGroupId)
        {
            string strQuery = "SELECT file_name, ta.name AS attribute_name, attribute_value, page_order " +
                " FROM wok.tutorial_page_set tps " +
                " INNER JOIN wok.tutorials t ON t.tutorial_id = tps.tutorial_id " +
                " INNER JOIN wok.tutorial_pages tp ON tp.tutorial_page_id = tps.tutorial_page_id " +
                " INNER JOIN wok.tutorial_page_attributes tpa ON tpa.tutorial_page_id = tps.tutorial_page_id " +
                " INNER JOIN wok.tutorial_attributes ta ON ta.attribute_id = tpa.attribute_id " +
                " WHERE t.test_group_id = @testGroupId " +
                " ORDER BY page_order ASC, attribute_name";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@testGroupId", testGroupId));

            return Db.WOK.GetDataTable(strQuery, parameters);
        }

        public bool CanWorldDropGemChests(int userId, int communityId)
        {
            string sql = "SELECT c.community_id " +
                " FROM kaneva.user_subscriptions us " +
                " INNER JOIN kaneva.communities c ON c.creator_id = us.user_id " +
                    " AND us.user_id = @userId " +
                    " AND us.subscription_id = 2 " +
                    " AND us.status_id in (" + (int)Subscription.SubscriptionStatus.Active + "," + (int)Subscription.SubscriptionStatus.Cancelled + ") " +
                    " AND us.end_date > NOW() " +
                " INNER JOIN wok.unified_world_ids uwid ON uwid.community_id = c.community_id " +
                " INNER JOIN wok.script_game_framework_settings sgfs ON sgfs.zone_instance_id = uwid.zone_instance_id " +
                    " AND sgfs.zone_type = uwid.zone_type " +
                    " AND sgfs.framework_enabled = 'T' " +
                " WHERE c.community_id IN (@communityId) " +
                " UNION " +
                " SELECT c.community_id " +
                " FROM kaneva.communities c " +
                " INNER JOIN wok.worlds_to_templates wt ON wt.community_id = c.community_id " +
                    " AND wt.template_id >= 19 " +
                    " AND wt.template_id NOT IN(28, 30) " +
                " INNER JOIN wok.unified_world_ids uwid ON uwid.community_id = c.community_id " +
                " INNER JOIN wok.script_game_framework_settings sgfs ON sgfs.zone_instance_id = uwid.zone_instance_id " +
                    " AND sgfs.zone_type = uwid.zone_type " +
                    " AND sgfs.framework_enabled = 'T' " +
                " WHERE c.community_id IN(@communityId) " +
                " UNION " +
                " SELECT community_id " +
                " FROM( " +
                    " SELECT pc.community_id " +
                    " FROM  wok.worlds_top_tour_cache pc " +
                    " WHERE loot_spawners > 10 " +
                        " AND pc.number_of_diggs_past_7_days > 4 " +
                    " ORDER BY pc.number_of_diggs_past_7_days DESC " +
                    " LIMIT 6) AS top_worlds " +
                " WHERE top_worlds.community_id IN(@communityId)";

                //indicate any parameters for place holder variables in query string
            List <IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@communityId", communityId));

            //perform query returning single data row from the kaneva database
            DataRow row = Db.Master.GetDataRow(sql.ToString(), parameters);
            return (row != null);
        }

        public AsyncTaskStatus GetAsyncTaskStatusLog(string taskKey)
        {
            string sqlString = "SELECT task_key, status, message " +
                               "FROM wok.async_task_status_log " +
                               "WHERE task_key = @taskKey ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@taskKey", taskKey));

            DataRow row = Db.Master.GetDataRow(sqlString, parameters);
            if (row == null)
                return new AsyncTaskStatus();

            return new AsyncTaskStatus
            {
                TaskKey = row["task_key"].ToString(),
                Status = row["status"].ToString(),
                Message = row["message"].ToString()
            };
        }

        public int UpsertAsyncTaskStatusLog(AsyncTaskStatus status)
        {
            string sqlString = "INSERT INTO wok.async_task_status_log (task_key, updated_on, status, message) " +
                               "VALUES (@taskKey, NOW(), @status, @message) " +
                               "ON DUPLICATE KEY UPDATE updated_on = NOW(), status = @status, message = @message ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@taskKey", status.TaskKey));
            parameters.Add(new MySqlParameter("@status", status.Status));
            parameters.Add(new MySqlParameter("@message", status.Message));

            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        public int DeleteAsyncTaskStatusLog(string taskKey)
        {
            string sqlString = "DELETE FROM wok.async_task_status_log WHERE task_key = @taskKey";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@taskKey", taskKey));

            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        public bool TryGetZoneIdsFromCommunityId(int communityId, out int zoneIndex, out int zoneInstanceId, out int zoneType)
        {
            zoneIndex = zoneInstanceId = zoneType = 0;

            string sqlString = "SELECT zone_index, zone_instance_id, zone_type " +
                               "FROM wok.unified_world_ids " +
                               "WHERE community_id = @communityId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));

            DataRow row = Db.Master.GetDataRow(sqlString, parameters);
            if (row == null)
                return false;

            zoneIndex = Convert.ToInt32(row["zone_index"]);
            zoneInstanceId = Convert.ToInt32(row["zone_instance_id"]);
            zoneType = Convert.ToInt32(row["zone_type"]);

            return true;
        }

        public int ArchiveInactiveWorld(int communityId, string archivedVia)
        {
            // Clear cache.
            int zoneIndex, zoneInstanceId, zoneType;
            try
            {
                if (TryGetZoneIdsFromCommunityId(communityId, out zoneIndex, out zoneInstanceId, out zoneType))
                    CentralCache.Remove(CentralCache.keyZoneCustomizations(zoneIndex, zoneInstanceId));
            }
            catch (Exception e)
            {
                m_logger.Error("Error clearing zone customizations cache for communityId " + communityId.ToString());
            }

            // Send the message
            string sql = "CALL wok.archive_inactive_world(@communityId, @archivedVia, @ret);SELECT @ret";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            parameters.Add(new MySqlParameter("@archivedVia", archivedVia));

            return Convert.ToInt32(Db.Developer.GetScalar(sql, parameters));
        }

        public int UnarchiveInactiveWorld(int communityId)
        {
            // Clear cache.
            int zoneIndex, zoneInstanceId, zoneType;
            try
            {
                if (TryGetZoneIdsFromCommunityId(communityId, out zoneIndex, out zoneInstanceId, out zoneType))
                    CentralCache.Remove(CentralCache.keyZoneCustomizations(zoneIndex, zoneInstanceId));
            }
            catch (Exception e)
            {
                m_logger.Error("Error clearing zone customizations cache for communityId " + communityId.ToString());
            }

            // Send the message
            string sql = "CALL wok.unarchive_inactive_world(@communityId, @ret);SELECT @ret";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));

            return Convert.ToInt32(Db.Developer.GetScalar(sql, parameters));
        }

        public PagedList<ArchivedWorld> GetArchivedWorlds(string worldName, string creatorName, string orderBy,
            int pageNum, int itemsPerPage)
        {
            string sqlString = "SELECT c.community_id, c.name, c.creator_id, IFNULL(u.username, '') AS username, " +
                               "a.archived_via, a.archived_on " +
                               "FROM wok.archived_worlds a " +
                               "INNER JOIN kaneva.communities c ON a.community_id = c.community_id " +
                               "LEFT JOIN kaneva.users u ON c.creator_id = u.user_id " +
                               "WHERE 1=1 ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (!string.IsNullOrWhiteSpace(worldName))
            {
                sqlString += "AND c.name LIKE @worldName ";
                parameters.Add(new MySqlParameter("@worldName", worldName + "%"));
            }

            if (!string.IsNullOrWhiteSpace(creatorName))
            {
                sqlString += "AND u.username LIKE @creatorName ";
                parameters.Add(new MySqlParameter("@creatorName", creatorName + "%"));
            }

            PagedDataTable dt = Db.Master.GetPagedDataTable(sqlString, orderBy, parameters, pageNum, itemsPerPage);
            if (dt == null || dt.Rows.Count <= 0)
                return new PagedList<ArchivedWorld>();

            PagedList<ArchivedWorld> archivedWorlds = new PagedList<ArchivedWorld>();
            foreach (DataRow row in dt.Rows)
            {
                archivedWorlds.Add(new ArchivedWorld
                {
                    CommunityId = Convert.ToInt32(row["community_id"]),
                    Name = row["name"].ToString(),
                    CreatorId = Convert.ToInt32(row["creator_id"]),
                    CreatorUsername = (row["username"] ?? (object)string.Empty).ToString(),
                    ArchivedVia = row["archived_via"].ToString(),
                    ArchivedOn = Convert.ToDateTime(row["archived_on"])
                });
            }

            archivedWorlds.TotalCount = dt.TotalCount;

            return archivedWorlds;
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

    }
}
