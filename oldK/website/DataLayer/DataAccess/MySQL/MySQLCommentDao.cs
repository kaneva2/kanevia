///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLCommentDao : ICommentsDao
    {
        //reuses Comment object - asetId equals gameId
        public int InsertGameReview(Comment review)
        {
            //returning 0 means it failed somewhere
            int rowsAffected = 0; 

            string sqlString = "INSERT INTO game_comments ( " +
                " game_id, user_id, parent_comment_id, reply_to_comment_id, " +
                " comment, ip_address, created_date, last_updated_date" +
                " ) VALUES (" +
                " @game_id, @user_id, @parent_comment_id, @reply_to_comment_id, @bodyText, @ipAddress," +
                 " NOW(),NOW())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@game_id", review.AssetId));
            parameters.Add(new MySqlParameter("@user_id", review.UserId));
            parameters.Add(new MySqlParameter("@parent_comment_id", review.ParentCommentId));
            parameters.Add(new MySqlParameter("@reply_to_comment_id", review.ReplyToCommentId));
            parameters.Add(new MySqlParameter("@bodyText", review.CommentText));
            parameters.Add(new MySqlParameter("@ipAddress", review.IpAddress));

            rowsAffected = Db.Developer.ExecuteNonQuery(sqlString, parameters);

            if (rowsAffected > 0)
            {
                // Update the reply count
                sqlString = "UPDATE game_stats " +
                    " SET number_of_comments = (number_of_comments + 1) " +
                    " WHERE game_id = @game_id";

                // Reset params
                parameters.Clear();
                parameters.Add(new MySqlParameter("@game_id", review.AssetId));
                rowsAffected = Db.Developer.ExecuteNonQuery(sqlString, parameters);
            }

            return rowsAffected;
        }


        /// <summary>
        /// DeleteGameReview - Used for deleting game reviews
        /// </summary>
        public int DeleteGameReview(Comment review)
        {
            //returning 0 means it failed somewhere
            int rowsAffected = 0; 
            string sqlString = "CALL delete_game_comment ( @commentId )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@commentId", review.CommentId));
            rowsAffected = Db.Developer.ExecuteNonQuery(sqlString, parameters);

            // Update the stats
            if (rowsAffected > 0)
            {
                // Update the reply count
                sqlString = "UPDATE game_stats " +
                    " SET number_of_comments = (number_of_comments - 1) " +
                    " WHERE game_id = @game_id";

                // Reset params
                parameters.Clear();
                parameters.Add(new MySqlParameter("@game_id", review.AssetId));
                rowsAffected = Db.Developer.ExecuteNonQuery(sqlString, parameters);
            }

            return rowsAffected;
        }

        /// <summary>
        /// Update comment
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public int UpdateGameReview(Comment review)
        {
            string sqlString = "UPDATE game_comments " +
                " SET comment = @comment, " +
                " last_updated_date = NOW(), " +
                " last_updated_user_id = @lastUpdatedUserId " +
                " WHERE game_comment_id = @reviewId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@reviewId", review.CommentId));
            parameters.Add(new MySqlParameter("@comment", review.CommentText));
            parameters.Add(new MySqlParameter("@lastUpdatedUserId", review.LastUpdatedUserId));
            return Db.Developer.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// GetComment
        /// </summary>
        public Comment GetGameReview(int reviewId)
        {
            string sql = "SELECT game_comment_id, user_id, game_id, " +
                " parent_comment_id, reply_to_comment_id, comment, " +
                " ip_address, last_updated_date, created_date, " +
                " last_updated_user_id " +
                " FROM game_comments " +
                " WHERE game_comment_id = @reviewId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@reviewId", reviewId));

            DataRow row = Db.Developer.GetDataRow(sql.ToString(), parameters);

            return new Comment(Convert.ToInt32(row["game_comment_id"]), Convert.ToInt32(row["user_id"]), Convert.ToInt32(row["game_id"]),
                0, Convert.ToInt32(row["parent_comment_id"]), Convert.ToInt32(row["reply_to_comment_id"]), row["comment"].ToString(),
                0, row["ip_address"].ToString(), Convert.ToDateTime(row["last_updated_date"]), Convert.ToDateTime(row["created_date"]),
                Convert.ToInt32(row["last_updated_user_id"]), 0);
        }

        /// <summary>
        /// Get a list of comments for an asset
        /// </summary>
        /// <param name="assetId"></param>
        public IList<Comment> GetGameReviews(int gameId, int pageNumber, int pageSize)
        {
            string strQuery = "SELECT (IF(parent_comment_id=0,game_comment_id,parent_comment_id)) as _o, " +
                " game_comment_id, user_id, game_id, " +
                " parent_comment_id, reply_to_comment_id, comment, " +
                " ip_address, last_updated_date, created_date, " +
                " last_updated_user_id, " +
                " now() as ctime " +
                " FROM game_comments WHERE game_id = @gameId ";

            string orderby = "_o DESC, parent_comment_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@gameId", gameId));

            PagedDataTable dt = Db.Developer.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            IList<Comment> list = new List<Comment>();
            Comment comment;
            User user;
            foreach (DataRow row in dt.Rows)
            {
                comment = new Comment(Convert.ToInt32(row["game_comment_id"]), Convert.ToInt32(row["user_id"]), Convert.ToInt32(row["game_id"]),
                    0, Convert.ToInt32(row["parent_comment_id"]), Convert.ToInt32(row["reply_to_comment_id"]), row["comment"].ToString(),
                    0, row["ip_address"].ToString(), Convert.ToDateTime(row["last_updated_date"]), Convert.ToDateTime(row["created_date"]),
                    Convert.ToInt32(row["last_updated_user_id"]), 0);

                comment.CurrentTime = Convert.ToDateTime(row["ctime"]);  // Used to calculate how long ago posted

                //temporary and inefficient way to get the user information 
                //it is done this way to prevent a join across database in case the databases are ever
                //spilt across different servers

                //get user data
                string strQuery2 = "SELECT u.username as updatedUsername, " +
                " u.username, u.ustate, u.show_online, u.location, u.mature_profile, u.gender, com.name_no_spaces, com.thumbnail_small_path " +
                " FROM users u LEFT JOIN communities_personal com ON u.user_id = com.creator_id WHERE user_id = @userId";

                List<IDbDataParameter> parameters2 = new List<IDbDataParameter>();
                parameters2.Add(new MySqlParameter("@userId", Convert.ToInt32(row["user_id"])));

                DataRow row2 = Db.Master.GetDataRow(strQuery2.ToString(), parameters2);

                // Set the owner information
                user = new User();
                user.Username = row2["username"].ToString();
                user.UserId = Convert.ToInt32(row["user_id"]);
                user.Ustate = row2["ustate"].ToString();
                user.ShowOnline = Convert.ToInt32(row2["show_online"]).Equals(1);
                user.Location = row2["location"].ToString();
                user.MatureProfile = Convert.ToInt32(row2["mature_profile"]).Equals(1);
                user.Gender = row2["gender"].ToString();
                user.NameNoSpaces = row2["name_no_spaces"].ToString();
                user.ThumbnailSmallPath = row2["thumbnail_small_path"].ToString();

                comment.UpdatedUsername = row2["updatedUsername"].ToString();
                comment.Owner = user;

                list.Add(comment);
            }
            return list;
        }

        public int InsertReview(Comment review)
        {
            int reviewId = 0;

            // Get the next comment Id from shard db
            try
            {
                reviewId = int.Parse(Db.Shard.GetScalar("SELECT shard_info.get_wok_item_reviews_id()", 2).ToString());
            }
            catch (FormatException)
            {
            }

            if (reviewId.Equals(0))
            {
                return -1;
            }

            review.CommentId = reviewId;

            string sqlString = "INSERT INTO wok_catalog_items_comments ( " +
                " item_comment_id, item_id, user_id, parent_comment_id, reply_to_comment_id, " +
                " comment, ip_address, created_date, last_updated_date" +
                " ) VALUES (" +
                " @reviewId, @itemId, @user_id, @parent_comment_id, @reply_to_comment_id, @bodyText, @ipAddress," +
                 " NOW(),NOW())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@reviewId", review.CommentId));
            parameters.Add(new MySqlParameter("@itemId", review.AssetId));
            parameters.Add(new MySqlParameter("@user_id", review.UserId));
            parameters.Add(new MySqlParameter("@parent_comment_id", review.ParentCommentId));
            parameters.Add(new MySqlParameter("@reply_to_comment_id", review.ReplyToCommentId));
            parameters.Add(new MySqlParameter("@bodyText", review.CommentText));
            parameters.Add(new MySqlParameter("@ipAddress", review.IpAddress));

            Db.Master.ExecuteNonQuery(sqlString, parameters);

            if (review.AssetId > 0)
            {
                // Update the reply count
                sqlString = "UPDATE wok_catalog_items_stats " +
                    " SET comments_count = (comments_count + 1) " +
                    " WHERE item_id = @itemId";

                // Reset params
                parameters.Clear();
                parameters.Add(new MySqlParameter("@itemId", review.AssetId));
                Db.Master.ExecuteNonQuery(sqlString, parameters);
            }

            return 0;
        }


        /// <summary>
        /// DeleteReview - Used for deleting wok item reviews
        /// </summary>
        public void DeleteReview(Comment review)
        {
            string sqlString = "CALL delete_WOK_item_comment ( @commentId )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@commentId", review.CommentId));
            Db.Master.ExecuteNonQuery(sqlString, parameters);

            // Update the stats
            if (review.AssetId > 0)
            {
                // Update the reply count
                sqlString = "UPDATE wok_catalog_items_stats " +
                    " SET comments_count = (comments_count - 1) " +
                    " WHERE item_id = @itemId";

                // Reset params
                parameters.Clear();
                parameters.Add(new MySqlParameter("@itemId", review.AssetId));
                Db.Master.ExecuteNonQuery(sqlString, parameters);
            }
        }

        /// <summary>
        /// Update comment
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public int UpdateReview(Comment review)
        {
            string sqlString = "UPDATE wok_catalog_items_comments " +
                " SET comment = @comment, " +
                " last_updated_date = NOW(), " +
                " last_updated_user_id = @lastUpdatedUserId " +
                " WHERE item_comment_id = @reviewId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@reviewId", review.CommentId));
            parameters.Add(new MySqlParameter("@comment", review.CommentText));
            parameters.Add(new MySqlParameter("@lastUpdatedUserId", review.LastUpdatedUserId));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// GetComment
        /// </summary>
        public Comment GetReview(int reviewId)
        {
            string sql = "SELECT item_comment_id, user_id, item_id, " +
                " parent_comment_id, reply_to_comment_id, comment, " +
                " ip_address, last_updated_date, created_date, " +
                " last_updated_user_id " +
                " FROM wok_catalog_items_comments " +
                " WHERE item_comment_id = @reviewId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@reviewId", reviewId));

            DataRow row = Db.Slave2.GetDataRow(sql.ToString(), parameters);

            return new Comment(Convert.ToInt32(row["item_comment_id"]), Convert.ToInt32(row["user_id"]), Convert.ToInt32(row["item_id"]),
                0, Convert.ToInt32(row["parent_comment_id"]), Convert.ToInt32(row["reply_to_comment_id"]), row["comment"].ToString(),
                0, row["ip_address"].ToString(), Convert.ToDateTime(row["last_updated_date"]), Convert.ToDateTime(row["created_date"]),
                Convert.ToInt32(row["last_updated_user_id"]), 0);
        }

        /// <summary>
        /// Get a list of comments for an asset
        /// </summary>
        /// <param name="assetId"></param>
        public IList<Comment> GetWOKItemReviews(int itemId, int pageNumber, int pageSize)
        {
            string strQuery = "SELECT (IF(parent_comment_id=0,item_comment_id,parent_comment_id)) as _o, " +
                " wcic.item_comment_id, wcic.user_id, wcic.item_id, " +
                " wcic.parent_comment_id, wcic.reply_to_comment_id, wcic.comment, " +
                " wcic.ip_address, wcic.last_updated_date, wcic.created_date, " +
                " wcic.last_updated_user_id, " +
                " now() as ctime, u2.username as updatedUsername, " +
                " u.username, u.ustate, u.show_online, u.location, u.mature_profile, u.gender, com.name_no_spaces, com.thumbnail_small_path " +
                " FROM users u, " +
                " wok_catalog_items_comments wcic LEFT OUTER JOIN users u2 ON wcic.last_updated_user_id = u2.user_id, communities_personal com " +
                " WHERE wcic.item_id = @itemId " +
                " AND wcic.user_id = u.user_id" +
                " AND u.user_id = com.creator_id ";

            string orderby = "_o DESC, parent_comment_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@itemId", itemId));

            PagedDataTable dt = Db.Slave2.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            IList<Comment> list = new List<Comment>();
            Comment comment;
            User user;
            foreach (DataRow row in dt.Rows)
            {
                comment = new Comment(Convert.ToInt32(row["item_comment_id"]), Convert.ToInt32(row["user_id"]), Convert.ToInt32(row["item_id"]),
                    0, Convert.ToInt32(row["parent_comment_id"]), Convert.ToInt32(row["reply_to_comment_id"]), row["comment"].ToString(),
                    0, row["ip_address"].ToString(), Convert.ToDateTime(row["last_updated_date"]), Convert.ToDateTime(row["created_date"]),
                    Convert.ToInt32(row["last_updated_user_id"]), 0);

                comment.UpdatedUsername = row["updatedUsername"].ToString();
                comment.CurrentTime = Convert.ToDateTime(row["ctime"]);  // Used to calculate how long ago posted

                // Set the owner information
                user = new User();
                user.Username = row["username"].ToString();
                user.UserId = Convert.ToInt32(row["user_id"]);
                user.Ustate = row["ustate"].ToString();
                user.ShowOnline = Convert.ToInt32(row["show_online"]).Equals(1);
                user.Location = row["location"].ToString();
                user.MatureProfile = Convert.ToInt32(row["mature_profile"]).Equals(1);
                user.Gender = row["gender"].ToString();
                user.NameNoSpaces = row["name_no_spaces"].ToString();
                user.ThumbnailSmallPath = row["thumbnail_small_path"].ToString();

                comment.Owner = user;

                list.Add(comment);
            }
            return list;
        }

        /// <summary>
        /// InsertComment
        /// </summary>
        public int InsertComment(Comment comment)
        {
            string sqlString = "CALL add_comments (@assetId, @channel_id, @user_id, " +
                " @parent_comment_id, @reply_to_comment_id, @bodyText, " +
                " 0, @ipAddress, @statusId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetId", comment.AssetId));
            parameters.Add(new MySqlParameter("@channel_id", comment.ChannelId));
            parameters.Add(new MySqlParameter("@user_id", comment.UserId));
            parameters.Add(new MySqlParameter("@parent_comment_id", comment.ParentCommentId));
            parameters.Add(new MySqlParameter("@reply_to_comment_id", comment.ReplyToCommentId));
            parameters.Add(new MySqlParameter("@bodyText", comment.CommentText));
            parameters.Add(new MySqlParameter("@ipAddress", comment.IpAddress));
            parameters.Add(new MySqlParameter("@statusId", comment.StatusId));

            Db.Master.ExecuteNonQuery(sqlString, parameters);
            return 0;
        }

        /// <summary>
        /// DeleteComment - Used for deleting comments
        /// </summary>
        public void DeleteComment(Comment comment)
        {
            string sqlString = "CALL delete_comment ( @commentId )";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@commentId", comment.CommentId));
            Db.Master.ExecuteNonQuery(sqlString, parameters);

            // Update the stats
            if (comment.AssetId > 0)
            {
                // Update the reply count
                sqlString = "UPDATE assets_stats " +
                    " SET number_of_comments = (number_of_comments - 1) " +
                    " WHERE asset_id = @assetId";

                // Reset params
                parameters.Clear();
                parameters.Add(new MySqlParameter("@assetId", comment.AssetId));
                Db.Master.ExecuteNonQuery(sqlString, parameters);
            }
        }

        /// <summary>
        /// Update comment
        /// </summary>
        public int UpdateComment(Comment comment)
        {
            string sqlString = "CALL update_comments (@lastUpdatedUserId, @comment, @commentId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@commentId", comment.CommentId));
            parameters.Add(new MySqlParameter("@comment", comment.CommentText));
            parameters.Add(new MySqlParameter("@lastUpdatedUserId", comment.LastUpdatedUserId));
            return Db.Master.ExecuteNonQuery(sqlString, parameters);
        }


        /// <summary>
        /// GetComment
        /// </summary>
        public Comment GetComment(int commentId)
        {
            string sql = "SELECT comment_id, user_id, asset_id, " +
                " channel_id, parent_comment_id, reply_to_comment_id, comment, " +
                " comment_type, ip_address, last_updated_date, last_updated_date, created_date, " +
                " last_updated_user_id, status_id " +
                " FROM comments " +
                " WHERE comment_id = @commentId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@commentId", commentId));

            DataRow row = Db.Slave2.GetDataRow(sql.ToString(), parameters);

            return new Comment(Convert.ToInt32(row["comment_id"]), Convert.ToInt32(row["user_id"]), Convert.ToInt32(row["asset_id"]),
                Convert.ToInt32(row["channel_id"]), Convert.ToInt32(row["parent_comment_id"]), Convert.ToInt32(row["reply_to_comment_id"]), row["comment"].ToString(),
                Convert.ToInt32(row["comment_type"]), row["ip_address"].ToString(), Convert.ToDateTime(row["last_updated_date"]), Convert.ToDateTime(row["created_date"]),
                Convert.ToInt32(row["last_updated_user_id"]), Convert.ToInt32(row["status_id"]));
        }

        /// <summary>
        /// Get a list of comments
        /// </summary>
        public PagedList<Comment> GetChannelComments(int channelId, int pageNumber, int pageSize)
        {
            string strQuery = "SELECT c.comment_id, c.user_id, c.asset_id, " +
                " c.channel_id, c.parent_comment_id, c.reply_to_comment_id, c.comment, " +
                " c.comment_type, c.ip_address, c.last_updated_date, c.last_updated_date, c.created_date, " +
                " c.last_updated_user_id, c.status_id, " +
                " u.username, u2.username as updatedUsername, now() as ctime, " +
                " u.ustate, u.show_online, u.location, u.mature_profile, " +
                " u.gender, com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path " +
                " FROM users u " +
                " LEFT JOIN comments c ON c.user_id = u.user_id " +
                " LEFT OUTER JOIN users u2 ON c.last_updated_user_id = u2.user_id, " +
                " communities com"+ 
                " WHERE c.channel_id = @channelId " +
                " AND c.status_id = 1 " + //+ (int)Constants.eFORUM_STATUS.ACTIVE +
                " AND u.user_id = com.creator_id " +
                " AND com.is_personal = 1";

            string orderby = "c.comment_id DESC";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@channelId", channelId));

            PagedDataTable dt = Db.Slave.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<Comment> list = new PagedList<Comment>();
            Comment comment;
            User user;

            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                comment = new Comment(Convert.ToInt32(row["comment_id"]), Convert.ToInt32(row["user_id"]), Convert.ToInt32(row["asset_id"]),
                    Convert.ToInt32(row["channel_id"]), Convert.ToInt32(row["parent_comment_id"]), Convert.ToInt32(row["reply_to_comment_id"]), row["comment"].ToString(),
                    Convert.ToInt32(row["comment_type"]), row["ip_address"].ToString(), Convert.ToDateTime(row["last_updated_date"]), Convert.ToDateTime(row["created_date"]),
                    Convert.ToInt32(row["last_updated_user_id"]), Convert.ToInt32(row["status_id"]));

                comment.UpdatedUsername = row["updatedUsername"].ToString ();
                comment.CurrentTime = Convert.ToDateTime(row["ctime"]);  // Used to calculate how long ago posted

                // Set the owner information
                user = new User ();
                user.Username = row["username"].ToString();
                user.UserId = Convert.ToInt32(row["user_id"]);
                user.Ustate = row["ustate"].ToString();

                user.ShowOnline = Convert.ToInt32(row["show_online"]).Equals (1);
                user.Location = row["location"].ToString();
                user.MatureProfile = Convert.ToInt32(row["mature_profile"]).Equals (1);
                user.Gender = row["gender"].ToString();
                user.NameNoSpaces = row["name_no_spaces"].ToString();
                user.ThumbnailSmallPath = row["thumbnail_small_path"].ToString();
                user.ThumbnailMediumPath = row["thumbnail_medium_path"].ToString();

                comment.Owner = user;

                list.Add(comment);
            }
            return list;
        }

        /// <summary>
        /// Get a list of comments for an asset
        /// </summary>
        /// <param name="assetId"></param>
        public IList<Comment> GetAssetComments(int assetId, int pageNumber, int pageSize)
        {
            string strQuery = "SELECT (IF(parent_comment_id=0,comment_id,parent_comment_id)) as _o, " +
                " c.comment_id, c.user_id, c.asset_id, " +
                " c.channel_id, c.parent_comment_id, c.reply_to_comment_id, c.comment, " +
                " c.comment_type, c.ip_address, c.last_updated_date, c.last_updated_date, c.created_date, " +
                " c.last_updated_user_id, c.status_id, " +
                " now() as ctime, u2.username as updatedUsername, " +
                " u.username, u.ustate, u.show_online, u.location, u.mature_profile, u.gender, com.name_no_spaces, com.thumbnail_square_path, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture " +
                " FROM users u " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id, " + 
                " comments c " +
                " LEFT OUTER JOIN users u2 ON c.last_updated_user_id = u2.user_id, communities_personal com " +
                " WHERE c.asset_id = @assetId " +
                " AND c.user_id = u.user_id" +
                " AND c.status_id = 1 " + // + (int)Constants.eFORUM_STATUS.ACTIVE +
                " AND u.user_id = com.creator_id ";

            string orderby = "_o DESC, parent_comment_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetId", assetId));

            PagedDataTable dt = Db.Slave2.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            IList<Comment> list = new List<Comment>();
            Comment comment;
            User user;
            foreach (DataRow row in dt.Rows)
            {
                comment = new Comment(Convert.ToInt32(row["comment_id"]), Convert.ToInt32(row["user_id"]), Convert.ToInt32(row["asset_id"]),
                    Convert.ToInt32(row["channel_id"]), Convert.ToInt32(row["parent_comment_id"]), Convert.ToInt32(row["reply_to_comment_id"]), row["comment"].ToString(),
                    Convert.ToInt32(row["comment_type"]), row["ip_address"].ToString(), Convert.ToDateTime(row["last_updated_date"]), Convert.ToDateTime(row["created_date"]),
                    Convert.ToInt32(row["last_updated_user_id"]), Convert.ToInt32(row["status_id"]));

                comment.UpdatedUsername = row["updatedUsername"].ToString();
                comment.CurrentTime = Convert.ToDateTime(row["ctime"]);  // Used to calculate how long ago posted

                // Set the owner information
                user = new User();
                user.Username = row["username"].ToString();
                user.UserId = Convert.ToInt32(row["user_id"]);
                user.Ustate = row["ustate"].ToString();
                user.ShowOnline = Convert.ToInt32(row["show_online"]).Equals(1);
                user.Location = row["location"].ToString();
                user.MatureProfile = Convert.ToInt32(row["mature_profile"]).Equals(1);
                user.Gender = row["gender"].ToString();
                user.NameNoSpaces = row["name_no_spaces"].ToString();
                user.ThumbnailSquarePath = row["thumbnail_square_path"].ToString();
                user.FacebookSettings.FacebookUserId = Convert.ToUInt64 (row["fb_user_id"]);
                user.FacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (row["use_facebook_profile_picture"]);

                comment.Owner = user;                                                

                list.Add(comment);
            }
            return list;
        }

        /// <summary>
        /// Get num people who have commented to a channel 
        /// </summary>
        /// <param name="channelId"></param>
        public int GetNumPeopleForChannelComments(int channelId)
        {
            string sqlSelectString = " SELECT COUNT(DISTINCT(user_id)) AS num_people " +
            " FROM comments " +
            " WHERE channel_id = @channelId " +
            " AND status_id = 1"; // +(int)Constants.eFORUM_STATUS.ACTIVE;

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@channelId", channelId));

            DataRow drComment = Db.Slave2.GetDataRow(sqlSelectString, parameters);
            
            if (drComment != null)
            {
                return Convert.ToInt32(drComment["num_people"].ToString());
            }

            return 0;
        }

        /// <summary>
        /// Get num people who have commented on an asset 
        /// </summary>
        /// <param name="channelId"></param>
        public int GetNumPeopleForAssetComments(int assetId)
        {
            string sqlSelectString = " SELECT COUNT(DISTINCT(user_id)) AS num_people " +
                " FROM comments " +
                " WHERE asset_id = @assetId " +
                " AND status_id = 1"; // + (int)Constants.eFORUM_STATUS.ACTIVE;

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@assetId", assetId));

            DataRow drComment = Db.Slave2.GetDataRow(sqlSelectString, parameters);

            if (drComment != null)
            {
                return Convert.ToInt32(drComment["num_people"].ToString());
            }

            return 0;
        }

        /// <summary>
        /// Used to help thwart comment BOTS. calls function to check number of comments a day against 
        /// configurable criteria - throws error to indicate query failed - if 0 returned procedure failed
        /// </summary>
        /// <param name="userId"></param>
        public bool CommentsBOTCheck(int userId)
        {
            // call function to perform BOT check
            string sqlString = "SELECT commentBotCheck(" + @userId + ");";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userId", userId));
            int result = int.Parse (Db.Master.GetScalar(sqlString, parameters).ToString ()); //dbUtility.ExecuteScalar(sqlString, parameters);

            return (result > 0);	// potential bot detected if true
        }
    }
}
