///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLForumDao : IForumDao
    {
        /// <summary>
        /// Get the specified Forum
        /// </summary>
        /// <returns></returns>
        public Forum GetForum(int forumId)
        {
            Forum forum = new Forum();

            string sqlSelect = "SELECT forum_id, f.forum_category_id, f.community_id, forum_name, f.description, number_of_topics, last_post_date, " +
                " f.created_date, f.display_order, fc.display_order as fc_display_order, status_id, f.last_topic_id, f.last_user_id, " +
                " FROM forums f " +
                " INNER JOIN forum_categories fc on f.forum_category_id = fc.forum_category_id " +
                " WHERE f.forum_id = @forumId " +
                " ORDER BY fc.display_order ASC, f.display_order ASC";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@forumId", forumId));

            DataRow row = Db.Master.GetDataRow(sqlSelect.ToString(), parameters);

            if (row != null)
            {
                forum = new Forum(Convert.ToInt32(row["forum_id"]), Convert.ToInt32(row["forum_category_id"]), Convert.ToInt32(row["community_id"]),
                row["forum_name"].ToString(), row["description"].ToString(), Convert.ToInt32(row["number_of_topics"]), Convert.ToDateTime(row["created_date"]),
                Convert.ToDateTime(row["last_post_date"]), Convert.ToInt32(row["last_topic_id"]), Convert.ToInt32(row["last_user_id"]),
                Convert.ToInt32(row["display_order"]), Convert.ToInt32(row["status_id"]), Convert.ToInt32(row["fc_display_order"]));

            }

            return forum;
        }

        /// <summary>
        /// Get the list of Forums
        /// </summary>
        /// <returns></returns>
        public IList<Forum> GetForums(int communityId)
        {
            IList<Forum> list = new List<Forum>();

            string sqlSelect = "SELECT forum_id, f.forum_category_id, f.community_id, forum_name, f.description, number_of_topics, last_post_date, " +
                " f.created_date, f.display_order, fc.display_order as fc_display_order, status_id, f.last_topic_id, f.last_user_id " +
                " FROM forums f " +
                " INNER JOIN forum_categories fc on f.forum_category_id = fc.forum_category_id " +
                " WHERE f.community_id = @communityId " +
                " ORDER BY fc.display_order ASC, f.display_order ASC";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));

            DataTable dt = Db.Master.GetDataTable(sqlSelect, parameters);

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Forum(Convert.ToInt32(row["forum_id"]), Convert.ToInt32(row["forum_category_id"]), Convert.ToInt32(row["community_id"]),
                row["forum_name"].ToString(), row["description"].ToString(), Convert.ToInt32(row["number_of_topics"]), Convert.ToDateTime(row["created_date"]),
                Convert.ToDateTime(row["last_post_date"]), Convert.ToInt32(row["last_topic_id"]), Convert.ToInt32(row["last_user_id"]),
                Convert.ToInt32(row["display_order"]), Convert.ToInt32(row["status_id"]), Convert.ToInt32(row["fc_display_order"])));
            }

            return list;
        }

        /// <summary>
        /// Get the Forum Topics
        /// </summary>
        /// <returns></returns>
        public PagedList<ForumTopic> GetLatestForumTopics(int communityId, int pageNumber, int pageSize)
        {
            string strQuery = " SELECT t.subject, created_username, t.topic_id, t.forum_id, t.community_id, " + 
                " created_user_id, number_of_replies, t.created_date, t.last_reply_date, t.last_updated_date, " +
                " COALESCE( t.last_updated_user_id, created_user_id ) AS last_updated_user_id, " +
                " created_user_id, t.last_user_id, com.name_no_spaces as created_name_no_spaces, " +
                " com.thumbnail_small_path as created_thumbnail_small_path, " +
                " comLastPost.name_no_spaces AS last_post_name_no_spaces, " +
                " comLastPost.name AS last_username, " +
                " comLastPost.thumbnail_small_path as last_thumbnail_small_path " +
                " FROM topics t " +
                " INNER JOIN communities_personal comLastPost ON comLastPost.creator_id = t.last_user_id " +
                " INNER JOIN communities_personal com ON t.created_user_id = com.creator_id " +
                " WHERE t.community_id = @communityId " +
                " AND t.status_id <> " + (int)ForumStatus.DELETED +
                " GROUP BY t.topic_id " +
                " ORDER BY t.last_reply_date DESC";

            string orderBy = "";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));

            PagedDataTable dt = Db.Master.GetPagedDataTable(strQuery, orderBy, parameters, pageNumber, pageSize);

            PagedList<ForumTopic> list = new PagedList<ForumTopic>();
            //assigns total count
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new ForumTopic(row["subject"].ToString(), row["created_username"].ToString(), Convert.ToInt32(row["topic_id"]),
                    Convert.ToInt32(row["forum_id"]), Convert.ToInt32(row["community_id"]), Convert.ToInt32(row["created_user_id"]),
                    Convert.ToInt32(row["number_of_replies"]), Convert.ToDateTime(row["created_date"]), Convert.ToDateTime(row["last_reply_date"]),
                    Convert.ToDateTime(row["last_updated_date"]), Convert.ToInt32(row["last_updated_user_id"]),
                    Convert.ToInt32(row["last_user_id"]), row["created_name_no_spaces"].ToString(),
                    row["created_thumbnail_small_path"].ToString(), row["last_post_name_no_spaces"].ToString(),
                    row["last_username"].ToString(), row["last_thumbnail_small_path"].ToString()));
            }

            return list;
        }

        
        /// <summary>
        /// InsertForumCategory
        /// </summary>
        /// <param name="community"></param>
        /// <returns>int</returns>
        public int InsertForumCategory(int communityId, string name, string description)
        {
            string sql = "CALL add_forum_category(@communityId, @name, @description)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            parameters.Add(new MySqlParameter("@name", name));
            parameters.Add(new MySqlParameter("@description", description));

            return Convert.ToInt32(Db.Master.GetScalar(sql, parameters));
        }

        /// <summary>
        /// InsertForum
        /// </summary>
        /// <param name="community"></param>
        /// <returns>int</returns>
        public int InsertForum(int communityId, string forumName, string description, int forumCategoryId)
        {
            string sql = "CALL add_forum(@communityId, @forumName, @description, @forumCategoryId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));
            parameters.Add(new MySqlParameter("@forumName", forumName));
            parameters.Add(new MySqlParameter("@description", description));
            parameters.Add(new MySqlParameter("@forumCategoryId", forumCategoryId));

            return Convert.ToInt32(Db.Master.GetScalar(sql, parameters));
        }


        /// <summary>
        /// Get the number of forums for a given community
        /// </summary>
        /// <param name="community_id"></param>
        /// <returns></returns>
        public int GetForumCount(int communityId)
        {
            string sql = "SELECT COUNT(*) as count " +
                "FROM forums WHERE community_id = @communityId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@communityId", communityId));

            return Convert.ToInt32(Db.Master.GetScalar(sql, parameters));
        }

    }

}
