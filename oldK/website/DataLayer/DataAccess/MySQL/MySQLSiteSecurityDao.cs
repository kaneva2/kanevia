///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLSiteSecurityDao : ISiteSecurityDao
    {
        #region Security Roles

        /// <summary>
        /// Gets All Site Security Roles for a company in as a dataset
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable GetCompanyRoles(int companyId)
        {
            string strQuery = "SELECT site_role_id, role_id, role_description, company_id FROM site_roles WHERE company_id = @companyId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@companyId", companyId));

            return Db.SiteManagement.GetDataTable(strQuery, parameters);
        }

        /// <summary>
        /// Gets All Site Security Roles for a company in as a dataset
        /// </summary>
        /// <returns>DataTable</returns>
        public SiteRole GetCompanyRole(int companyId, int roleId)
        {
            string strQuery = "SELECT site_role_id, role_id, role_description, company_id FROM site_roles WHERE company_id = @companyId AND role_id = @roleId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@companyId", companyId));
            parameters.Add(new MySqlParameter("@roleId", roleId));

            DataRow row = Db.SiteManagement.GetDataRow(strQuery, parameters);

            return new SiteRole(Convert.ToInt32(row["site_role_id"]), Convert.ToInt32(row["role_id"]),
                     Convert.ToInt32(row["company_id"]), row["role_description"].ToString());
        }

        ///// <summary>
        ///// Gets All Site Security Roles for a company in a list format
        ///// </summary>
        ///// <returns>List<SiteRole></returns>        
        //public IList<SiteRole> GetCompanyRolesList(int companyId)
        //{
        //    // get all roles in pages datatable
        //    string strQuery = "SELECT site_role_id, role_id, role_description, company_id FROM site_roles WHERE company_id = @companyId";

        //    List<IDbDataParameter> parameters = new List<IDbDataParameter>();
        //    parameters.Add(new MySqlParameter("@companyId", companyId));

        //    DataTable dt = Db.SiteManagement.GetDataTable(strQuery, parameters);

        //    IList<SiteRole> list = new List<SiteRole>();
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        list.Add(new SiteRole(Convert.ToInt32(row["site_role_id"]), Convert.ToInt32(row["role_id"]),
        //             Convert.ToInt32(row["company_id"]), row["role_description"].ToString()));
        //    }
        //    return list;
        //}

        /// <summary>
        /// Gets All Site Security Roles for a company in a list format
        /// </summary>
        /// <returns>List<SiteRole></returns>        
        public IList<SiteRole> GetCompanyRolesList(int companyId)
        {
            string filter = "company_id = " + companyId.ToString();
            return (IList<SiteRole>)(GetAllRolesList(filter, "", 1, int.MaxValue).List);
        }

        /// <summary>
        /// Get All Site Security Roles as a paged list. Allows for various filtering
        /// </summary>
        /// <returns>PagedList<SiteRole></returns>        
        public PagedList<SiteRole> GetAllRolesList(string filter, string orderby, int pageNumber, int pageSize)
        {
            // get all roles in pages datatable
            string strQuery = "SELECT site_role_id, role_id, role_description, company_id FROM site_roles ";

            if (filter.Length > 0)
            {
                strQuery += " WHERE " + filter;
            }

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            PagedDataTable dt = Db.SiteManagement.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<SiteRole> list = new PagedList<SiteRole>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SiteRole(Convert.ToInt32(row["site_role_id"]), Convert.ToInt32(row["role_id"]),
                     Convert.ToInt32(row["company_id"]), row["role_description"].ToString()));
            }
            return list;
        }


        /// <summary>
        /// Add A New Role
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int AddNewRole(SiteRole role)
        {
            // Send the message
            string sql = "CALL add_new_role(@role_id, @role_description, @company_id)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@role_id", role.RoleId));
            parameters.Add(new MySqlParameter("@role_description", role.RolesName));
            parameters.Add(new MySqlParameter("@company_id", role.CompanyId));

            return Db.SiteManagement.ExecuteIdentityInsert(sql, parameters);
        }

        //--manage game functions---
        /// <summary>
        /// Update Role
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int UpdateRole(SiteRole role)
        {
            // Send the message
            string sql = "UPDATE site_roles SET " +
                "role_id = @role_id, role_description = @role_description, company_id = @company_id " +
                "WHERE site_role_id = @site_role_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@role_id", role.RoleId));
            parameters.Add(new MySqlParameter("@role_description", role.RolesName));
            parameters.Add(new MySqlParameter("@company_id", role.CompanyId));
            parameters.Add(new MySqlParameter("@site_role_id", role.SiteRoleId));

            return Db.SiteManagement.ExecuteNonQuery(sql, parameters);
        }
        
        //--manage game functions---
        /// <summary>
        /// Delete Game
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="userId"></param>
        /// <returns>int</returns>
        public int DeleteRole(int siteRoleId, int companyId)
        {
            string sql = "CALL delete_role(@siteRoleId, @companyId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@siteRoleId", siteRoleId));
            parameters.Add(new MySqlParameter("@companyId", companyId));

            Db.SiteManagement.ExecuteNonQuery(sql, parameters);

            return 1;//temporary until able to return value from stored procedure
        }

        #endregion

        #region Privileges

        /// <summary>
        /// Get All Site Security Privileges in a paged list
        /// </summary>
        /// <returns>PagedList<SitePrivilege></returns>        
        public PagedList<SitePrivilege> GetAllPrivilegesPgd(string filter, string orderby, int pageNumber, int pageSize)
        {
            // get all roles in pages datatable
            string strQuery = "SELECT privilege_id, privilege_name, privilege_description, site_administrators_only FROM site_privileges";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            if (filter.Length > 0)
            {
                strQuery += " WHERE " + filter;
            }

            PagedDataTable dt = Db.SiteManagement.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

            PagedList<SitePrivilege> list = new PagedList<SitePrivilege>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SitePrivilege(Convert.ToInt32(row["privilege_id"]), row["privilege_name"].ToString(),
                     row["privilege_description"].ToString(), Convert.ToBoolean(row["site_administrators_only"])));
            }
            return list;
        }

        /// <summary>
        /// Get All Site Security Privileges 
        /// </summary>
        /// <returns>PagedList<SitePrivilege></returns>        
        public PagedList<SitePrivilege> GetUserPrivilegesPgd()
        {
            return GetAllPrivilegesPgd("site_administrators_only = 0", "", 1, int.MaxValue);
        }

        /// <summary>
        /// Get All Site Security Privileges in a list
        /// </summary>
        /// <returns>IList<SitePrivilege></returns>
        public IList<SitePrivilege> GetAllPrivilegesList(string filter)
        {
            string strQuery = "SELECT privilege_id, privilege_name, privilege_description, site_administrators_only FROM site_privileges";

            if (filter.Length > 0)
            {
                strQuery += " WHERE " + filter;
            }

            DataTable dt = Db.SiteManagement.GetDataTable(strQuery);

            IList<SitePrivilege> list = new List<SitePrivilege>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SitePrivilege(Convert.ToInt32(row["privilege_id"]), row["privilege_name"].ToString(),
                     row["privilege_description"].ToString(), Convert.ToBoolean(row["site_administrators_only"])));
            }

            return list;
        }

        /// <summary>
        /// Get All Admin Site Security Privileges 
        /// </summary>
        /// <returns>IList<SitePrivilege></returns>
        public IList<SitePrivilege> GetAdminSitePrivileges()
        {
            return GetAllPrivilegesList("site_administrators_only = 1");
        }

        /// <summary>
        /// Get All User Site Security Privileges 
        /// </summary>
        /// <returns>IList<SitePrivilege></returns>
        public IList<SitePrivilege> GetAvailableUserPrivileges()
        {
            return GetAllPrivilegesList("site_administrators_only = 0");
        }

        /// <summary>
        /// Get All Site Security Privileges in a list
        /// </summary>
        /// <returns>DataTable<SitePrivilege></returns>
        public DataTable GetRolePrivileges(int siteRoleId, int companyId)
        {
            string strQuery = "SELECT privilege_id, access_level_id, company_id, site_role_id FROM roles_to_privileges where site_role_id = @siteRoleId" +
                " AND company_id = @companyId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@siteRoleId", siteRoleId));
            parameters.Add(new MySqlParameter("@companyId", companyId));

            return Db.SiteManagement.GetDataTable(strQuery, parameters);
        }

        /// <summary>
        /// Get All Site Security Privileges in a list based on users role id 
        /// </summary>
        /// <returns>DataTable<SitePrivilege></returns>
        public DataTable GetPrivilegesByRoleId(int userRoleId, int companyId)
        {
            string strQuery = "SELECT rtp.privilege_id, rtp.access_level_id, rtp.company_id, rtp.site_role_id " +
                "FROM roles_to_privileges rtp INNER JOIN site_roles sr ON rtp.site_role_id = sr.site_role_id " +
                "WHERE sr.role_id = @userRoleId AND rtp.company_id = @companyId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@userRoleId", userRoleId));
            parameters.Add(new MySqlParameter("@companyId", companyId));

            return Db.SiteManagement.GetDataTable(strQuery, parameters);
        }

        /// <summary>
        /// Add A New Privilege
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int AddNewPrivilege(SitePrivilege sitePriv)
        {
            // Send the message
            string sql = "CALL add_new_privilege(@privilegeName, @privilegeDescription, @site_administrators_only)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@privilegeName", sitePriv.PrivilegeName));
            parameters.Add(new MySqlParameter("@privilegeDescription", sitePriv.PrivilegeDescription));
            parameters.Add(new MySqlParameter("@site_administrators_only", (sitePriv.AdminOnly ? 0 : 1)));

            return Db.SiteManagement.ExecuteIdentityInsert(sql, parameters);
        }

        //--manage game functions---
        /// <summary>
        /// Update Role
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int UpdatePrivilege(SitePrivilege sitePriv)
        {
            // Send the message
            string sql = "CALL update_privilege(@privilegeName, @privilegeDescription, @adminOnly, @privilegeId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@privilegeName", sitePriv.PrivilegeName));
            parameters.Add(new MySqlParameter("@privilegeDescription", sitePriv.PrivilegeDescription));
            parameters.Add(new MySqlParameter("@adminOnly", (sitePriv.AdminOnly ? 0 : 1)));
            parameters.Add(new MySqlParameter("@privilegeId", sitePriv.PrivilegeId));

            return Db.SiteManagement.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Delete Privilege
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="userId"></param>
        /// <returns>int</returns>
        public int DeletePrivilege(int privilegeId)
        {
            string sql = "CALL delete_privilege (@privilegeId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@privilegeId", privilegeId));

            return Db.SiteManagement.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Delete All Privileges associated to a Role from the
        /// Privileges to Role table
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="userId"></param>
        /// <returns>int</returns>
        public int DeleteAllPrivilege2Role(int siteRoleId, int companyId)
        {
            string sql = "CALL delete_all_role_privileges(@siteRoleId, @companyId, @returnResults)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@siteRoleId", siteRoleId));
            parameters.Add(new MySqlParameter("@companyId", companyId));
            parameters.Add(new MySqlParameter("@returnResults", 1));

            return Db.SiteManagement.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Add A New Privilege to Role
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int AddNewPrivilege2Role(int siteRoleId, int companyId, int privilegeId, int accessLevelId)
        {
            // Send the message
            string sql = "CALL add_new_privilege_to_role(@siteRoleId, @companyId, @privilegeId, @accessLevelId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@siteRoleId", siteRoleId));
            parameters.Add(new MySqlParameter("@companyId", companyId));
            parameters.Add(new MySqlParameter("@privilegeId", privilegeId));
            parameters.Add(new MySqlParameter("@accessLevelId", accessLevelId));

            return Db.SiteManagement.ExecuteIdentityInsert(sql, parameters);
        }


        #endregion

        #region Access Levels

        /// <summary>
        /// Get All Site Security Privileges
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable GetAllAccessLevels()
        {
            string strQuery = "SELECT access_level_id, access_level FROM access_levels";

            return Db.SiteManagement.GetDataTable(strQuery);
        }

        #endregion
    }
}
