///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;
using Sphinx.Client.Commands.Search;
using Sphinx.Client.Connections;
using Kaneva.BusinessLayer.BusinessObjects;

using log4net;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MySQLContestDao : IContestDao
    {
        /// <summary>
        /// GetCommunities
        /// </summary>
        public PagedList<Contest> GetContests (int userId, string orderBy, string filter, int pageNumber, int pageSize)
        {
            string sqlSelect = " SELECT contest_id, c.user_id, u.username, u.gender, c.created_date, end_date, " +
                " title, c.description, prize_amount, kei_point_id, IF(end_date > NOW(), 1, 0) AS is_active, status, " +
                " num_entries, num_votes, num_comments, popular_vote_winner_entry_id, owner_vote_winner_entry_id, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path, NOW() as curr_date, " +
                        " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture ";

            string sqlTables = " FROM contests_ugc c " +
                " INNER JOIN users u ON u.user_id = c.user_id " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id ";

            string sqlWhere = "";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            //limits results to be just for a particular user and active memberships
            if (userId > 0)
            {
                sqlWhere = sqlWhere.Length > 0 ? " AND " : " WHERE ";
                sqlWhere += " c.user_id = @userId ";
                parameters.Add (new MySqlParameter ("@userId", userId));
            }

            if (filter.Length > 0)
            {
                sqlWhere += sqlWhere.Length > 0 ? " AND " : " WHERE ";
                sqlWhere += filter;
            }

            string sql = sqlSelect + sqlTables + sqlWhere;

            PagedDataTable pdt = Db.Master.GetPagedDataTable (sql, orderBy, parameters, pageNumber, pageSize);

            PagedList<Contest> list = new PagedList<Contest> ();
            //assigns total count
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                Contest c = new Contest (Convert.ToUInt64 (row["contest_id"]), Convert.ToDateTime (row["created_date"]), 
                    Convert.ToDateTime (row["end_date"]), row["title"].ToString (), row["description"].ToString (),
                    Convert.ToInt32 (row["prize_amount"]), Convert.ToBoolean (row["is_active"]), Convert.ToInt32 (row["status"]), 
                    Convert.ToInt32 (row["num_entries"]), Convert.ToInt32 (row["num_votes"]), Convert.ToInt32 (row["num_comments"]),
                    Convert.ToInt32 (row["user_id"]), row["username"].ToString (), row["gender"].ToString (),
                    row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (),
                    row["thumbnail_square_path"].ToString (), row["popular_vote_winner_entry_id"] == DBNull.Value ? 0 : Convert.ToUInt64 (row["popular_vote_winner_entry_id"]),
                    row["owner_vote_winner_entry_id"] == DBNull.Value ? 0 : Convert.ToUInt64 (row["owner_vote_winner_entry_id"]),
                    Convert.ToDateTime (row["curr_date"]));

                c.Owner.FacebookSettings.FacebookUserId = Convert.ToUInt64 (row["fb_user_id"]);
                c.Owner.FacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (row["use_facebook_profile_picture"]);

                list.Add (c);
            }

            return list;
        }

        /// <summary>
        /// SearchContests
        /// </summary>
        public PagedList<Contest> SearchContests(int userId, string searchString,  string orderBy, int pageNumber, int pageSize)
        {
            Sphinx sphinx = new Sphinx();
            string newSearchString = "";
            string contestIds = "";
            int totalCount = 0;

            searchString = sphinx.StripSphinxSearchString(searchString);

            using (ConnectionBase connection = new PersistentTcpConnection(sphinx.Host, sphinx.Port))
            {
                connection.ConnectionTimeout = 5000;

                // Search
                if (searchString.Length > 0)
                {
                    // Escape some strings sphinx has issues with \()|-!@~/^$*
                    searchString = sphinx.CleanSphinxSearchString(searchString);
                        
                    char[] splitter = { ' ' };
                    string[] arKeywords = null;

                    // Did they enter multiples?
                    arKeywords = searchString.Split(splitter);

                    if (arKeywords.Length > 1)
                    {
                        newSearchString = "";
                        for (int j = 0; j < arKeywords.Length; j++)
                        {
                            // Do a sphinx OR for now
                            if (j < arKeywords.Length - 1)
                            {
                                newSearchString += "@(title,description,username) " + arKeywords[j].ToString() + " | ";
                            }
                            else
                            {
                                newSearchString += "@(title,description,username) " + arKeywords[j].ToString();
                            }
                        }
                    }
                    else
                    {
                        newSearchString = "@(title,description,username) " + searchString;
                    }
                }

                SearchQuery searchQuery = new SearchQuery(newSearchString);

                // Set match mode to SPH_MATCH_EXTENDED2
                searchQuery.MatchMode = MatchMode.Extended2;

                // Add Sphinx index name to list
                //searchQuery.Indexes.Add("people_srch_idx");
                searchQuery.Indexes.Add("contests_srch_idx");

                // Set up paging
                searchQuery.Limit = pageSize;
                searchQuery.Offset = (pageNumber - 1) * pageSize;

                // Create search command object
                SearchCommand searchCommand = new SearchCommand(connection);

                // Add newly created search query object to query list
                searchCommand.QueryList.Add(searchQuery);

                // Only get one users data
                if (userId > 0)
                {
                    searchQuery.AttributeFilters.Add("user_id", userId, false);
                }
            
                // Sorting
                if (orderBy.Trim ().Length.Equals (0))
                {
                    searchQuery.SortMode = ResultsSortMode.Relevance;
                }
                else
                {
                    // Handle extended mode
                    if (orderBy.Contains (","))
                    {
                        searchQuery.SortMode = ResultsSortMode.Extended;
                        searchQuery.SortBy = orderBy;
                    }
                    else
                    {
                        if (orderBy.ToUpper ().Contains ("ASC"))
                        {
                            searchQuery.SortMode = ResultsSortMode.AttributeAscending;
                        }
                        else
                        {
                            searchQuery.SortMode = ResultsSortMode.AttributeDescending;
                        }

                        // Strip ASC, DESC
                        searchQuery.SortBy = orderBy.Replace ("ASC", "").Replace ("DESC", "").Trim ();
                    }
                }

                // Execute command on server and obtain results
                try
                {
                    searchCommand.Execute();
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error calling Sphinx " + sphinx.Host + ":" + sphinx.Port, exc);
                }

                // Build result IN clause for MySQL
                foreach (SearchQueryResult result in searchCommand.Result.QueryResults)
                {
                    foreach (Match match in result.Matches)
                    {
                        if (contestIds.Length > 0)
                        {
                            contestIds = contestIds + "," + match.DocumentId.ToString();
                        }
                        else
                        {
                            contestIds = match.DocumentId.ToString();
                        }
                    }

                    // Note this will grab the last total count in collection, ok for this case, but not best place to do this
                    totalCount = (result.TotalFound > searchQuery.MaxMatches ? searchQuery.MaxMatches : result.TotalFound);
                }

                // Nothing found in Sphinx
                if (contestIds.Length.Equals(0))
                {
                    PagedList<Contest> emptyList = new PagedList<Contest>();
                    emptyList.TotalCount = 0;
                    return emptyList;
                }

                List<IDbDataParameter> parameters = new List<IDbDataParameter>();

                string strSelect = " SELECT contest_id, c.user_id, u.username, u.gender, c.created_date, end_date, " +
                    " title, c.description, prize_amount, kei_point_id, IF(end_date > NOW(), 1, 0) AS is_active, status, " +
                    " num_entries, num_votes, num_comments, popular_vote_winner_entry_id, owner_vote_winner_entry_id, " +
                    " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path, NOW() as curr_date, " +
                    " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture " +
                    " FROM contests_ugc c " +
                    " INNER JOIN users u ON u.user_id = c.user_id " +
                    " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id " + 
                    " INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
                    " WHERE c.contest_id in " +
                    " (" + contestIds + ")" +
                    " ORDER BY FIELD(c.contest_id," + contestIds + ")";

                DataTable dt = Db.Slave.GetDataTable(strSelect, parameters);

                PagedList<Contest> list = new PagedList<Contest>();
                list.TotalCount = Convert.ToUInt32(totalCount);

                foreach (DataRow row in dt.Rows)
                {
                    Contest c = new Contest (Convert.ToUInt64 (row["contest_id"]), Convert.ToDateTime (row["created_date"]),
                       Convert.ToDateTime(row["end_date"]), row["title"].ToString(), row["description"].ToString(),
                       Convert.ToInt32 (row["prize_amount"]), Convert.ToBoolean (row["is_active"]), Convert.ToInt32 (row["status"]),
                       Convert.ToInt32(row["num_entries"]), Convert.ToInt32(row["num_votes"]), Convert.ToInt32(row["num_comments"]),
                       Convert.ToInt32(row["user_id"]), row["username"].ToString(), row["gender"].ToString(),
                       row["thumbnail_small_path"].ToString(), row["thumbnail_medium_path"].ToString(), row["thumbnail_large_path"].ToString(),
                       row["thumbnail_square_path"].ToString (), row["popular_vote_winner_entry_id"] == DBNull.Value ? 0 : Convert.ToUInt64 (row["popular_vote_winner_entry_id"]),
                       row["owner_vote_winner_entry_id"] == DBNull.Value ? 0 : Convert.ToUInt64 (row["owner_vote_winner_entry_id"]),
                       Convert.ToDateTime(row["curr_date"]));

                    c.Owner.FacebookSettings.FacebookUserId = Convert.ToUInt64 (row["fb_user_id"]);
                    c.Owner.FacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (row["use_facebook_profile_picture"]);

                    list.Add (c);
                }

                return list;
            }
        }


        public UInt64 CreateContest (Contest contest, int createFee, UInt16 transactionType, UInt16 contestTransactionType, string keiPointId,
            ref int wokTransactionLogId)
        {
            string sql = "CALL create_new_contest(@userId, @endDate, @title, @description, @prizeAmount, @transactionType, " +
                " @keiPointId, @contestTransactionType, @createFee, @rc, @user_balance, @tranId, @contestId); " +
                " SELECT CAST(@rc as UNSIGNED INT) as rc, CAST(@user_balance as decimal) as user_balance, CAST(@tranId as UNSIGNED INT) as tranId, CAST(@contestId as UNSIGNED INT) as contestId;";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", contest.Owner.UserId));
            parameters.Add (new MySqlParameter ("@endDate", contest.EndDate));
            parameters.Add (new MySqlParameter ("@title", contest.Title));
            parameters.Add (new MySqlParameter ("@description", contest.Description));
            parameters.Add (new MySqlParameter ("@prizeAmount", contest.PrizeAmount));
            parameters.Add (new MySqlParameter ("@transactionType", transactionType));
            parameters.Add (new MySqlParameter ("@keiPointId", keiPointId));
            parameters.Add (new MySqlParameter ("@contestTransactionType", contestTransactionType));
            parameters.Add (new MySqlParameter ("@createFee", createFee));
            
            DataRow row = Db.Master.GetDataRow (sql.ToString (), parameters);

            if (row["rc"] != null && Convert.ToInt32 (row["rc"]) != 0) // 0 = ok, 1 = insufficient funds, -1 = error
            {
                throw new Exception (Convert.ToString (row["rc"]));
            }

            //return the wok transaction id for use
            wokTransactionLogId = Convert.ToInt32 (row["tranId"]);
            
            // Invalidate cache
            CentralCache.Remove (CentralCache.keyKanevaUserBalances + contest.Owner.UserId.ToString ());

            if (row["contestId"] != null)
            {
                return Convert.ToUInt64 (row["contestId"]);
            }
            else
            {
                return 0;
            }
        }
        
        /// <summary>
        /// Insert a new contest
        /// </summary>
        /// <param name="contest"></param>
        /// <returns>int</returns>
        public int InsertContest (Contest contest)
        {
            string sqlString = " INSERT INTO contests_ugc " +
                " (user_id, created_date, end_date, title, description, prize_amount, contest_active) VALUES (" +
                " @userId, NOW(), @endDate, @title, @description, @prizeAmount, @status)" ;

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", contest.Owner.UserId));
            parameters.Add (new MySqlParameter ("@endDate", contest.EndDate));
            parameters.Add (new MySqlParameter ("@title", contest.Title));
            parameters.Add (new MySqlParameter ("@description", contest.Description));
            parameters.Add (new MySqlParameter ("@prizeAmount", contest.PrizeAmount));
            parameters.Add (new MySqlParameter ("@status", (int)eCONTEST_STATUS.Active));
            
            return Db.Master.ExecuteIdentityInsert (sqlString, parameters);
        }

        /// <summary>
        /// Get a contest
        /// </summary>
        public Contest GetContest (UInt64 contestId)
        {
            string sqlString = " SELECT contest_id, c.user_id, u.username, u.gender, c.created_date, end_date, " +
                " title, c.description, prize_amount, kei_point_id, IF(end_date > NOW(), 1, 0) AS is_active, status, " +
                " num_entries, num_votes, num_comments, popular_vote_winner_entry_id, owner_vote_winner_entry_id, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path, NOW() as curr_date, " +
                        " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture " +
                " FROM contests_ugc c " +
                " INNER JOIN users u ON u.user_id = c.user_id " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
                " WHERE c.contest_id = @contestId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestId", contestId));
            DataRow row = Db.Slave2.GetDataRow (sqlString, parameters);

            if (row == null)
            {
                return new Contest ();
            }

            Contest c = new Contest (Convert.ToUInt64 (row["contest_id"]), Convert.ToDateTime (row["created_date"]),
                Convert.ToDateTime (row["end_date"]), row["title"].ToString (), row["description"].ToString (),
                Convert.ToInt32 (row["prize_amount"]), Convert.ToBoolean (row["is_active"]), Convert.ToInt32 (row["status"]),
                Convert.ToInt32 (row["num_entries"]), Convert.ToInt32 (row["num_votes"]), Convert.ToInt32 (row["num_comments"]),
                Convert.ToInt32 (row["user_id"]), row["username"].ToString (), row["gender"].ToString (),
                row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (),
                row["thumbnail_square_path"].ToString (), row["popular_vote_winner_entry_id"] == DBNull.Value ? 0 : Convert.ToUInt64 (row["popular_vote_winner_entry_id"]),
                row["owner_vote_winner_entry_id"] == DBNull.Value ? 0 : Convert.ToUInt64 (row["owner_vote_winner_entry_id"]),
                Convert.ToDateTime (row["curr_date"])
                );

            c.Owner.FacebookSettings.FacebookUserId = Convert.ToUInt64 (row["fb_user_id"]);
            c.Owner.FacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (row["use_facebook_profile_picture"]);

            return c;
        }

        /// <summary>
        /// GetContestsEntered
        /// </summary>
        public PagedList<Contest> GetContestsEntered (int userId, string orderBy, string filter, int pageNumber, int pageSize)
        {
            string sqlSelect = " SELECT DISTINCT(c.contest_id), c.user_id, u.username, u.gender, c.created_date, end_date, " +
                " title, c.description, prize_amount, kei_point_id, IF(end_date > NOW(), 1, 0) AS is_active, status, " +
                " num_entries, c.num_votes, c.num_comments, popular_vote_winner_entry_id, owner_vote_winner_entry_id, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path,  com.thumbnail_square_path, NOW() as curr_date, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture ";

            string sqlTables = " FROM contests_ugc c " +
                " INNER JOIN contest_entries ce ON ce.contest_id = c.contest_id " +
                " INNER JOIN users u ON u.user_id = c.user_id " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id ";

            string sqlWhere = " WHERE ce.user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            if (filter.Length > 0)
            {
                sqlWhere += sqlWhere.Length > 0 ? " AND " : "";
                sqlWhere += filter;
            }

            string sql = sqlSelect + sqlTables + sqlWhere;

            PagedDataTable pdt = Db.Master.GetPagedDataTable (sql, orderBy, parameters, pageNumber, pageSize);

            PagedList<Contest> list = new PagedList<Contest> ();
            //assigns total count
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                Contest c = new Contest (Convert.ToUInt64 (row["contest_id"]), Convert.ToDateTime (row["created_date"]),
                    Convert.ToDateTime (row["end_date"]), row["title"].ToString (), row["description"].ToString (),
                    Convert.ToInt32 (row["prize_amount"]), Convert.ToBoolean (row["is_active"]), Convert.ToInt32 (row["status"]),
                    Convert.ToInt32 (row["num_entries"]), Convert.ToInt32 (row["num_votes"]), Convert.ToInt32 (row["num_comments"]),
                    Convert.ToInt32 (row["user_id"]), row["username"].ToString (), row["gender"].ToString (),
                    row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (),
                    row["thumbnail_square_path"].ToString (), row["popular_vote_winner_entry_id"] == DBNull.Value ? 0 : Convert.ToUInt64 (row["popular_vote_winner_entry_id"]),
                    row["owner_vote_winner_entry_id"] == DBNull.Value ? 0 : Convert.ToUInt64 (row["owner_vote_winner_entry_id"]),
                    Convert.ToDateTime (row["curr_date"]));

                c.Owner.FacebookSettings.FacebookUserId = Convert.ToUInt64 (row["fb_user_id"]);
                c.Owner.FacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (row["use_facebook_profile_picture"]);

                list.Add (c);
            }

            return list;
        }

        /// <summary>
        /// GetContestsFollowing
        /// </summary>
        public PagedList<Contest> GetContestsFollowing (int userId, string orderBy, string filter, int pageNumber, int pageSize)
        {
            string sqlSelect = " SELECT c.contest_id, c.user_id, u.username, u.gender, c.created_date, end_date, " +
                " title, c.description, prize_amount, kei_point_id, IF(end_date > NOW(), 1, 0) AS is_active, status, " +
                " num_entries, num_votes, num_comments, popular_vote_winner_entry_id, owner_vote_winner_entry_id, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path, NOW() as curr_date, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture ";

            string sqlTables = " FROM contests_ugc c " +
                " INNER JOIN contest_followers cf ON cf.contest_id = c.contest_id " +
                " INNER JOIN users u ON u.user_id = c.user_id " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id ";

            string sqlWhere = " WHERE cf.user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            if (filter.Length > 0)
            {
                sqlWhere += sqlWhere.Length > 0 ? " AND " : "";
                sqlWhere += filter;
            }

            string sql = sqlSelect + sqlTables + sqlWhere;

            PagedDataTable pdt = Db.Master.GetPagedDataTable (sql, orderBy, parameters, pageNumber, pageSize);

            PagedList<Contest> list = new PagedList<Contest> ();
            //assigns total count
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                Contest c = new Contest (Convert.ToUInt64 (row["contest_id"]), Convert.ToDateTime (row["created_date"]),
                    Convert.ToDateTime (row["end_date"]), row["title"].ToString (), row["description"].ToString (),
                    Convert.ToInt32 (row["prize_amount"]), Convert.ToBoolean (row["is_active"]), Convert.ToInt32 (row["status"]),
                    Convert.ToInt32 (row["num_entries"]), Convert.ToInt32 (row["num_votes"]), Convert.ToInt32 (row["num_comments"]),
                    Convert.ToInt32 (row["user_id"]), row["username"].ToString (), row["gender"].ToString (),
                    row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (),
                    row["thumbnail_square_path"].ToString (), row["popular_vote_winner_entry_id"] == DBNull.Value ? 0 : Convert.ToUInt64 (row["popular_vote_winner_entry_id"]),
                    row["owner_vote_winner_entry_id"] == DBNull.Value ? 0 : Convert.ToUInt64 (row["owner_vote_winner_entry_id"]),
                    Convert.ToDateTime (row["curr_date"]));

                c.Owner.FacebookSettings.FacebookUserId = Convert.ToUInt64 (row["fb_user_id"]);
                c.Owner.FacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (row["use_facebook_profile_picture"]);

                list.Add (c);
            }

            return list;
        }

        /// <summary>
        /// GetContestsVotedOn
        /// </summary>
        public PagedList<Contest> GetContestsVotedOn (int userId, string orderBy, string filter, int pageNumber, int pageSize)
        {
            string sqlSelect = " SELECT c.contest_id, c.user_id, u.username, u.gender, c.created_date, end_date, " +
                " title, c.description, prize_amount, kei_point_id, IF(end_date > NOW(), 1, 0) AS is_active, status, " +
                " c.num_entries, c.num_votes, c.num_comments, popular_vote_winner_entry_id, owner_vote_winner_entry_id, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path, NOW() as curr_date, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture ";

            string sqlTables = " FROM contests_ugc c " +
                " INNER JOIN contest_entries ce ON ce.contest_id = c.contest_id " +
                " INNER JOIN contest_entry_votes cev ON cev.contest_entry_id = ce.contest_entry_id " +
                " INNER JOIN users u ON u.user_id = c.user_id " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id ";

            string sqlWhere = " WHERE cev.user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            if (filter.Length > 0)
            {
                sqlWhere += sqlWhere.Length > 0 ? " AND " : "";
                sqlWhere += filter;
            }

            string sql = sqlSelect + sqlTables + sqlWhere;

            PagedDataTable pdt = Db.Master.GetPagedDataTable (sql, orderBy, parameters, pageNumber, pageSize);

            PagedList<Contest> list = new PagedList<Contest> ();
            //assigns total count
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                Contest c = new Contest (Convert.ToUInt64 (row["contest_id"]), Convert.ToDateTime (row["created_date"]),
                    Convert.ToDateTime (row["end_date"]), row["title"].ToString (), row["description"].ToString (),
                    Convert.ToInt32 (row["prize_amount"]), Convert.ToBoolean (row["is_active"]), Convert.ToInt32 (row["status"]),
                    Convert.ToInt32 (row["num_entries"]), Convert.ToInt32 (row["num_votes"]), Convert.ToInt32 (row["num_comments"]),
                    Convert.ToInt32 (row["user_id"]), row["username"].ToString (), row["gender"].ToString (),
                    row["thumbnail_small_path"].ToString (), row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (),
                    row["thumbnail_square_path"].ToString (), row["popular_vote_winner_entry_id"] == DBNull.Value ? 0 : Convert.ToUInt64 (row["popular_vote_winner_entry_id"]),
                    row["owner_vote_winner_entry_id"] == DBNull.Value ? 0 : Convert.ToUInt64 (row["owner_vote_winner_entry_id"]),
                    Convert.ToDateTime (row["curr_date"]));

                c.Owner.FacebookSettings.FacebookUserId = Convert.ToUInt64 (row["fb_user_id"]);
                c.Owner.FacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (row["use_facebook_profile_picture"]);

                list.Add (c);
            }

            return list;
        }

        /// <summary>
        /// GetContestsUserWon
        /// </summary>
        public PagedList<Contest> GetContestsUserWon (int userId, string orderBy, string filter, int pageNumber, int pageSize)
        {
            string sqlSelect = " SELECT c.contest_id, c.user_id, c.created_date, end_date, " +
                " title, c.description, prize_amount, kei_point_id, IF(end_date > NOW(), 1, 0) AS is_active, status, " +
                " c.num_entries, c.num_votes, c.num_comments, popular_vote_winner_entry_id, owner_vote_winner_entry_id, " +
                " NOW() as curr_date ";

            string sqlTables = " FROM contests_ugc c " +
                " INNER JOIN contest_entries ce ON ce.contest_entry_id = c.owner_vote_winner_entry_id ";

            string sqlWhere = " WHERE ce.user_id = @userId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            if (filter.Length > 0)
            {
                sqlWhere += sqlWhere.Length > 0 ? " AND " : "";
                sqlWhere += filter;
            }

            string sql = sqlSelect + sqlTables + sqlWhere;

            PagedDataTable pdt = Db.Master.GetPagedDataTable (sql, orderBy, parameters, pageNumber, pageSize);

            PagedList<Contest> list = new PagedList<Contest> ();
            //assigns total count
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                list.Add (new Contest (Convert.ToUInt64 (row["contest_id"]), Convert.ToDateTime (row["created_date"]),
                    Convert.ToDateTime (row["end_date"]), row["title"].ToString (), row["description"].ToString (),
                    Convert.ToInt32 (row["prize_amount"]), Convert.ToBoolean (row["is_active"]), Convert.ToInt32 (row["status"]),
                    Convert.ToInt32 (row["num_entries"]), Convert.ToInt32 (row["num_votes"]), Convert.ToInt32 (row["num_comments"]),
                    row["popular_vote_winner_entry_id"] == DBNull.Value ? 0 : Convert.ToUInt64 (row["popular_vote_winner_entry_id"]),
                    row["owner_vote_winner_entry_id"] == DBNull.Value ? 0 : Convert.ToUInt64 (row["owner_vote_winner_entry_id"]),
                    Convert.ToDateTime (row["curr_date"])
                    ));
            }

            return list;
        }

        /// <summary>
        /// GetContestsNeedWinner
        /// </summary>
        public PagedList<Contest> GetContestsNeedWinner (int userId, string orderBy, string filter, int pageNumber, int pageSize)
        {
            string sqlSelect = " SELECT c.contest_id, c.created_date, end_date, " +
                " title, c.description, prize_amount, kei_point_id, IF(end_date > NOW(), 1, 0) AS is_active, status, " +
                " c.num_entries, c.num_votes, c.num_comments, popular_vote_winner_entry_id, " +
                " owner_vote_winner_entry_id, NOW() as curr_date ";

            string sqlTables = " FROM contests_ugc c ";

            string sqlWhere = " WHERE user_id = @userId " +
                " AND owner_vote_winner_entry_id IS NULL " +
                " AND num_entries > 0 " +
                " AND end_date < NOW() ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));

            if (filter.Length > 0)
            {
                sqlWhere += sqlWhere.Length > 0 ? " AND " : "";
                sqlWhere += filter;
            }

            string sql = sqlSelect + sqlTables + sqlWhere;

            PagedDataTable pdt = Db.Master.GetPagedDataTable (sql, orderBy, parameters, pageNumber, pageSize);

            PagedList<Contest> list = new PagedList<Contest> ();
            //assigns total count
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                list.Add (new Contest (Convert.ToUInt64 (row["contest_id"]), Convert.ToDateTime (row["created_date"]),
                    Convert.ToDateTime (row["end_date"]), row["title"].ToString (), row["description"].ToString (),
                    Convert.ToInt32 (row["prize_amount"]), Convert.ToBoolean (row["is_active"]), Convert.ToInt32 (row["status"]),
                    Convert.ToInt32 (row["num_entries"]), Convert.ToInt32 (row["num_votes"]), Convert.ToInt32 (row["num_comments"]),
                    row["popular_vote_winner_entry_id"] == DBNull.Value ? 0 : Convert.ToUInt64 (row["popular_vote_winner_entry_id"]),
                    row["owner_vote_winner_entry_id"] == DBNull.Value ? 0 : Convert.ToUInt64 (row["owner_vote_winner_entry_id"]),
                    Convert.ToDateTime (row["curr_date"])
                    ));
            }

            return list;
        }
        
        /// <summary>
        /// Insert a new contest entry
        /// </summary>
        /// <param name="contestentry"></param>
        /// <returns>int</returns>
        public int InsertContestEntry (ContestEntry entry)
        {
            string sqlString = " INSERT INTO contest_entries " +
                " (contest_id, user_id, created_date, description) VALUES (" +
                " @contestId, @userId, NOW(), @description)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestId", entry.ContestId));
            parameters.Add (new MySqlParameter ("@userId", entry.Owner.UserId));
            parameters.Add (new MySqlParameter ("@description", entry.Description));

            int entryId = Db.Master.ExecuteIdentityInsert (sqlString, parameters);

            if (entryId > 0)
            {
                UpdateEntryCount (entry.ContestId, 1);
            }

            return entryId;
        }

        /// <summary>
        /// GetContestEntries
        /// </summary>
        public PagedList<ContestEntry> GetContestEntries (UInt64 contestId, string orderBy, string filter, int pageNumber, int pageSize)
        {
            string sqlSelect = " SELECT contest_entry_id, contest_id, ce.user_id, u.username, u.gender, " +
                " ce.created_date, ce.description, num_votes, num_comments, entry_status, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path, NOW() as curr_date, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture ";

            string sqlTables = " FROM contest_entries ce " +
                " INNER JOIN users u ON u.user_id = ce.user_id " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id ";

            string sqlWhere = " WHERE ce.contest_id = @contestId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestId", contestId));
        
            if (filter.Length > 0)
            {
                sqlWhere += sqlWhere.Length > 0 ? " AND " : "";
                sqlWhere += filter;
            }

            string sql = sqlSelect + sqlTables + sqlWhere;

            PagedDataTable pdt = Db.Master.GetPagedDataTable (sql, orderBy, parameters, pageNumber, pageSize);

            PagedList<ContestEntry> list = new PagedList<ContestEntry> ();
            //assigns total count
            list.TotalCount = pdt.TotalCount;

            foreach (DataRow row in pdt.Rows)
            {
                ContestEntry ce = new ContestEntry (Convert.ToUInt64 (row["contest_entry_id"]), Convert.ToUInt64 (row["contest_id"]), 
                    Convert.ToDateTime (row["created_date"]),row["description"].ToString (),
                    Convert.ToInt32 (row["num_votes"]), Convert.ToInt32 (row["num_comments"]), Convert.ToInt32 (row["user_id"]), 
                    row["username"].ToString (), row["gender"].ToString (), row["thumbnail_small_path"].ToString (),
                    row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (), row["thumbnail_square_path"].ToString (),
                    Convert.ToDateTime (row["curr_date"]), Convert.ToInt32 (row["entry_status"]));

                ce.Owner.FacebookSettings.FacebookUserId = Convert.ToUInt64 (row["fb_user_id"]);
                ce.Owner.FacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (row["use_facebook_profile_picture"]);

                list.Add (ce);
            }

            return list;
        }

        public IList<UInt64> GetEntriesUserHasVotedOn (UInt64 contestId, int userId)
        {
            string sql = " SELECT cev.contest_entry_id " +
                " FROM contest_entry_votes cev " +
                " INNER JOIN contest_entries ce ON ce.contest_entry_id = cev.contest_entry_id " +
                " WHERE cev.user_id = @userId " +
                " AND ce.contest_id = @contestId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@contestId", contestId));

            DataTable dt = Db.Slave.GetDataTable (sql, parameters);

            IList<UInt64> list = new List<UInt64> ();

            foreach (DataRow row in dt.Rows)
            {
                list.Add (Convert.ToUInt64(row["contest_entry_id"]));
            }

            return list;
        }

        /// <summary>
        /// GetContestEntryVoters
        /// </summary>
        public IList<ContestUser> GetContestEntryVoters (UInt64 contestEntryId, string orderBy, string filter)
        {
            string sqlSelect = " SELECT u.user_id, u.username, u.gender, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path ";

            string sqlTables = " FROM contest_entry_votes cev " +
                " INNER JOIN users u ON u.user_id = cev.user_id " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id ";

            string sqlWhere = " WHERE cev.contest_entry_id = @contestEntryId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestEntryId", contestEntryId));

            if (filter.Length > 0)
            {
                sqlWhere += sqlWhere.Length > 0 ? " AND " : "";
                sqlWhere += filter;
            }

            string sqlOrderBy = "";
            if (orderBy.Length > 0)
            {
                sqlOrderBy = " ORDER BY " + orderBy;
            }

            string sql = sqlSelect + sqlTables + sqlWhere + sqlOrderBy;

            DataTable dt = Db.Slave.GetDataTable (sql, parameters);

            IList<ContestUser> list = new List<ContestUser> ();
            
            foreach (DataRow row in dt.Rows)
            {
                list.Add (new ContestUser (Convert.ToInt32 (row["user_id"]), row["username"].ToString (), 
                    row["gender"].ToString (), row["thumbnail_small_path"].ToString (),
                    row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (),
                    row["thumbnail_square_path"].ToString ()
                    ));
            }

            return list;
        }

        /// <summary>
        /// GetContestFollowers
        /// </summary>
        public IList<ContestUser> GetContestFollowers (UInt64 contestId, string orderBy, string filter)
        {
            string sqlSelect = " SELECT u.user_id, u.username, u.gender, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path ";

            string sqlTables = " FROM contest_followers cf " +
                " INNER JOIN users u ON u.user_id = cf.user_id " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id ";

            string sqlWhere = " WHERE cf.contest_id = @contestId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestId", contestId));

            if (filter.Length > 0)
            {
                sqlWhere += sqlWhere.Length > 0 ? " AND " : "";
                sqlWhere += filter;
            }

            string sqlOrderBy = "";
            if (orderBy.Length > 0)
            {
                sqlOrderBy = " ORDER BY " + orderBy;
            }

            string sql = sqlSelect + sqlTables + sqlWhere + sqlOrderBy;

            DataTable dt = Db.Slave.GetDataTable (sql, parameters);

            IList<ContestUser> list = new List<ContestUser> ();

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new ContestUser (Convert.ToInt32 (row["user_id"]), row["username"].ToString (),
                    row["gender"].ToString (), row["thumbnail_small_path"].ToString (),
                    row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (),
                    row["thumbnail_square_path"].ToString ()
                    ));
            }

            return list;
        }

        /// <summary>
        /// GetContestEntry
        /// </summary>
        public ContestEntry GetContestEntry (UInt64 contestEntryId)
        {
            string sqlString = " SELECT contest_entry_id, contest_id, ce.user_id, u.username, u.gender, " +
                " ce.created_date, ce.description, num_votes, num_comments, entry_status, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path, NOW() as curr_date " +
                " FROM contest_entries ce " +
                " INNER JOIN users u ON u.user_id = ce.user_id " +
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
                " WHERE ce.contest_entry_id = @contestEntryId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestEntryId", contestEntryId));

            DataRow row = Db.Slave2.GetDataRow (sqlString, parameters);

            if (row == null)
            {
                return new ContestEntry ();
            }

            return new ContestEntry (Convert.ToUInt64 (row["contest_entry_id"]), Convert.ToUInt64 (row["contest_id"]),
                Convert.ToDateTime (row["created_date"]), row["description"].ToString (),
                Convert.ToInt32 (row["num_votes"]), Convert.ToInt32 (row["num_comments"]), Convert.ToInt32 (row["user_id"]),
                row["username"].ToString (), row["gender"].ToString (), row["thumbnail_small_path"].ToString (),
                row["thumbnail_medium_path"].ToString (), row["thumbnail_large_path"].ToString (), row["thumbnail_square_path"].ToString (),
                Convert.ToDateTime (row["curr_date"]), Convert.ToInt32 (row["entry_status"])
                );
        }

        public int UpdateContestWinner (UInt64 contestId, UInt64 popularWinnerEntryId, UInt64 ownerVoteWinnerEntryId)
        {
            string sqlString = "";
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestId", contestId));
 
            if (popularWinnerEntryId > 0)
            {
                sqlString += "popular_vote_winner_entry_id = @popularWinnerEntryId ";
                parameters.Add (new MySqlParameter ("@popularWinnerEntryId", popularWinnerEntryId));
            }

            if (ownerVoteWinnerEntryId > 0)
            {
                if (sqlString.Length > 0) sqlString += ",";
                sqlString += "owner_vote_winner_entry_id = @ownerVoteWinnerId ";
                parameters.Add (new MySqlParameter ("@ownerVoteWinnerId", ownerVoteWinnerEntryId));
            }
            
            sqlString = "UPDATE contests_ugc SET " + sqlString + " WHERE contest_id = @contestId";

            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int SetPopularWinner (UInt64 contestId)
        {
            string sqlString = "";
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestId", contestId));

            sqlString = "UPDATE contests_ugc SET popular_vote_winner_entry_id = " +
                " (SELECT contest_entry_id " +
                " FROM contest_entries WHERE contest_id = @contestId " +
                " ORDER BY num_votes DESC, created_date ASC LIMIT 1)" +
                " WHERE contest_id = @contestId";

            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int SetPopularWinnerAsContestWinner (UInt64 contestId)
        {
            string sqlString = "";
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestId", contestId));
            
            sqlString = "UPDATE contests_ugc SET owner_vote_winner_entry_id = " +
                " (SELECT contest_entry_id " +
                " FROM contest_entries WHERE contest_id = @contestId " +
                " ORDER BY num_votes DESC, created_date ASC LIMIT 1)" +
                " WHERE contest_id = @contestId";

            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int UpdateContestStatus (UInt64 contestId, int status, int userId)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string sqlString = "UPDATE contests_ugc SET status = @status, date_status_changed = NOW()";

            if (userId > 0)
            {
                sqlString += ", status_changed_by_user_id = @userId";
                parameters.Add (new MySqlParameter ("@userId", userId));
            }

            sqlString += " where contest_id = @contestId";

            parameters.Add (new MySqlParameter ("@status", status));
            parameters.Add (new MySqlParameter ("@contestId", contestId));
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int UpdateContestEntryStatus (UInt64 contestEntryId, int status, int userId)
        {
            string sqlString = "UPDATE contest_entries SET entry_status = @status, date_status_changed = NOW()," +
                " status_changed_by_user_id = @userId" +
                " WHERE contest_entry_id = @contestEntryId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@status", status));
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@contestEntryId", contestEntryId));
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int ArchiveContest (UInt64 contestId)
        {
            string sqlString = "UPDATE contests_ugc SET archived = 1" +
                " where contest_id = @contestId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestId", contestId));
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// InsertComment
        /// </summary>
        public int InsertComment (UInt64 contestId, UInt64 contestEntryId, int userId, string comment)
        {
            string sqlString = "INSERT INTO contest_comments ( " +
                " contest_id, contest_entry_id, comment, user_id, created_date " +
                " ) VALUES (" +
                " @contestId, @contestEntryId, @comment, @userId, NOW())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestId", contestId));
            parameters.Add (new MySqlParameter ("@comment", comment));
            parameters.Add (new MySqlParameter ("@userId", userId));

            if (contestEntryId == 0)
            {
                parameters.Add (new MySqlParameter ("@contestEntryId", DBNull.Value));
            }
            else
            {
                parameters.Add (new MySqlParameter ("@contestEntryId", contestEntryId));
            }

            int rc = Db.Master.ExecuteNonQuery (sqlString, parameters);

            if (rc > 0)
            {
                UpdateCommentCount (contestId, contestEntryId, 1);
            }

            return rc;
        }

        /// <summary>
        /// GetComments
        /// </summary> 
        public PagedList<ContestComment> GetContestComments (UInt64 contestId, UInt64 contestEntryId, int pageNumber, int pageSize, string sortBy)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            string strQuery = "SELECT comment_id, cc.contest_id, c.user_id AS contest_owner_user_id, contest_entry_id, comment, cc.user_id, cc.created_date, " +
                " u.username, u.gender, " +
                " com.thumbnail_small_path, com.thumbnail_medium_path, com.thumbnail_large_path, com.thumbnail_square_path, NOW() as curr_date, " +
                " COALESCE(ufs.fb_user_id,0) AS fb_user_id, COALESCE(ufs.use_facebook_profile_picture,0) AS use_facebook_profile_picture " +
                " FROM contest_comments cc " +
                " INNER JOIN contests_ugc c ON c.contest_id = cc.contest_id " +
                " INNER JOIN users u ON u.user_id = cc.user_id " +
                " LEFT OUTER JOIN user_facebook_settings ufs ON ufs.user_id = u.user_id " + 
                " INNER JOIN communities_personal com ON com.creator_id = u.user_id " +
                " WHERE cc.contest_id = @contestId "; 

            parameters.Add (new MySqlParameter ("@contestId", contestId));
            if (contestEntryId == 0)
            {
                strQuery += " AND contest_entry_id IS @contestEntryId";
                parameters.Add (new MySqlParameter ("@contestEntryId", DBNull.Value));
            }
            else
            {
                strQuery += " AND contest_entry_id = @contestEntryId"; 
                parameters.Add (new MySqlParameter ("@contestEntryId", contestEntryId));
            }

            PagedDataTable pdt = Db.Master.GetPagedDataTable (strQuery, sortBy, parameters, pageNumber, pageSize);

            PagedList<ContestComment> list = new PagedList<ContestComment> ();
            list.TotalCount = pdt.TotalCount;

            // reverse the order so the comments will display in correct order
            for (int i = pdt.Rows.Count - 1; i >= 0; i--)
            {
                ContestComment cc = new ContestComment (Convert.ToUInt64 (pdt.Rows[i]["comment_id"]), Convert.ToUInt64 (pdt.Rows[i]["contest_id"]), Convert.ToInt32 (pdt.Rows[i]["contest_owner_user_id"]),
                    pdt.Rows[i]["contest_entry_id"] == DBNull.Value ? 0 : Convert.ToUInt64 (pdt.Rows[i]["contest_entry_id"]), Convert.ToDateTime (pdt.Rows[i]["created_date"]),
                    pdt.Rows[i]["comment"].ToString (), 4, Convert.ToInt32 (pdt.Rows[i]["user_id"]), pdt.Rows[i]["username"].ToString (),
                    pdt.Rows[i]["gender"].ToString (), pdt.Rows[i]["thumbnail_small_path"].ToString (), pdt.Rows[i]["thumbnail_medium_path"].ToString (),
                    pdt.Rows[i]["thumbnail_large_path"].ToString (), pdt.Rows[i]["thumbnail_square_path"].ToString (), Convert.ToDateTime (pdt.Rows[i]["curr_date"]));

                cc.Creator.FacebookSettings.FacebookUserId = Convert.ToUInt64 (pdt.Rows[i]["fb_user_id"]);
                cc.Creator.FacebookSettings.UseFacebookProfilePicture = Convert.ToBoolean (pdt.Rows[i]["use_facebook_profile_picture"]);

                list.Add (cc);
            }
            return list;
        }

        /// <summary>
        /// DeleteComment
        /// </summary> 
        public int DeleteComment (UInt64 contestId, UInt64 contestEntryId, UInt64 commentId)
        {
            string sqlString = "";
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            if (contestEntryId > 0)
            {
                sqlString = "DELETE FROM contest_comments " +
                    " WHERE contest_entry_id = @contestEntryId AND comment_id = @commentId";
                parameters.Add (new MySqlParameter ("@contestEntryId", contestEntryId));
                parameters.Add (new MySqlParameter ("@commentId", commentId));
            }
            else
            {
                sqlString = "DELETE from contest_comments " +
                    " WHERE comment_id = @commentId AND contest_id = @contestId";
                parameters.Add (new MySqlParameter ("@commentId", commentId)); 
                parameters.Add (new MySqlParameter ("@contestId", contestId));
            }

            int rc = Db.Master.ExecuteNonQuery (sqlString, parameters);

            if (rc > 0)
            {
                UpdateCommentCount (contestId, contestEntryId, -1);
            }

            return rc;
        }

        private int UpdateCommentCount (UInt64 contestId, UInt64 contestEntryId, int amount)
        {
            string sqlString = "";
            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();

            if (contestEntryId > 0)
            {
                sqlString = "UPDATE contest_entries SET num_comments = num_comments + " + amount.ToString() +
                    " WHERE contest_entry_id = @contestEntryId";
                parameters.Add (new MySqlParameter ("@contestEntryId", contestEntryId));
            }
            else
            {
                sqlString = "UPDATE contests_ugc SET num_comments = num_comments + " + amount.ToString () +
                    " WHERE contest_id = @contestId";
                parameters.Add (new MySqlParameter ("@contestId", contestId));
            }

            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int UpdateEntryCount (UInt64 contestId, int amount)
        {
            string sqlString = "UPDATE contests_ugc SET num_entries = num_entries + " + amount.ToString () +
                " WHERE contest_id = @contestId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestId", contestId));

            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int UpdateVoteCount (UInt64 contestId, int amount)
        {
            string sqlString = "UPDATE contests_ugc SET num_votes = num_votes + " + amount.ToString () +
                " WHERE contest_id = @contestId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestId", contestId));

            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int DeleteEntryVoters (UInt64 contestEntryId)
        {
            string sqlString = "DELETE FROM contest_entry_votes " +
                " WHERE contest_entry_id = @contestEntryId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestEntryId", contestEntryId));
            
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int InsertVote (int userId, string ipAddress, UInt64 contestId, UInt64 contestEntryId)
        {
            string sqlString = "INSERT INTO contest_entry_votes ( " + 
                " contest_entry_id, user_id, created_date, ip_address " +
                ") VALUES (" +
                " @contestEntryId, @userId, NOW(), @ipAddress)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestEntryId", contestEntryId));
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@ipAddress", ipAddress));

            int rc = Db.Master.ExecuteNonQuery (sqlString, parameters);

            if (rc > 0)
            {
                sqlString = "UPDATE contest_entries SET num_votes = num_votes + 1 " +
                " WHERE contest_entry_id = @contestEntryId";

                parameters = new List<IDbDataParameter> ();
                parameters.Add (new MySqlParameter ("@contestEntryId", contestEntryId));

                rc = Db.Master.ExecuteNonQuery (sqlString, parameters);

                sqlString = "UPDATE contests_ugc SET num_votes = num_votes + 1 " +
                    " WHERE contest_id = @contestId";

                parameters = new List<IDbDataParameter> ();
                parameters.Add (new MySqlParameter ("@contestId", contestId));

                rc = Db.Master.ExecuteNonQuery (sqlString, parameters);
            }

            if (rc > 0)
            {
                sqlString = "SELECT num_votes FROM contest_entries " +
                    " WHERE contest_entry_id = @contestEntryId";

                parameters = new List<IDbDataParameter> ();
                parameters.Add (new MySqlParameter ("@contestEntryId", contestEntryId));

                DataRow drVote = Db.Master.GetDataRow (sqlString, parameters);

                if (drVote != null && drVote["num_votes"] != null)
                {
                    return Convert.ToInt32(drVote["num_votes"]);
                }

                return 0;   
            }
            return rc;
        }

        public int FollowContest (UInt64 contestId, int userId)
        {
            string sqlString = "INSERT IGNORE INTO contest_followers ( " +
                " contest_id, user_id " +
                ") VALUES (" +
                " @contestId, @userId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestId", contestId));
            parameters.Add (new MySqlParameter ("@userId", userId));
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public int UnFollowContest (UInt64 contestId, int userId)
        {
            string sqlString = "DELETE FROM contest_followers " +
                " WHERE contest_id = @contestId AND user_id = @userId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestId", contestId));
            parameters.Add (new MySqlParameter ("@userId", userId));
            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        public bool HasUserVotedOnContest (UInt64 contestId, int userId)
        {
            // Make sure the email does not already exist?
            string sqlString = "SELECT * FROM contests_ugc c " +
                " INNER JOIN contest_entries ce ON ce.contest_id = c.contest_id " +
                " INNER JOIN contest_entry_votes cev ON cev.contest_entry_id = ce.contest_entry_id " +
                " WHERE c.contest_id = @contestId " +
                " AND cev.user_id = @userId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@contestId", contestId));
            return (Convert.ToInt32 (Db.Master.GetScalar (sqlString, parameters)) > 0);
       }

        public bool HasUserVotedOnContestEntry (UInt64 contestEntryId, int userId)
        {
            // Make sure the email does not already exist?
            string sqlString = "SELECT * FROM contest_entry_votes " +
                " WHERE contest_entry_id = @contestEntryId " +
                " AND user_id = @userId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@contestEntryId", contestEntryId));
            return (Convert.ToInt32 (Db.Master.GetScalar (sqlString, parameters)) > 0);
        }

        public bool HasUserEnteredContest (UInt64 contestId, int userId)
        {
            // Make sure the email does not already exist?
            string sqlString = "SELECT * FROM contests_ugc c " +
                " INNER JOIN contest_entries ce ON ce.contest_id = c.contest_id " +
                " WHERE c.contest_id = @contestId " +
                " AND ce.user_id = @userId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@contestId", contestId));
            return (Convert.ToInt32 (Db.Master.GetScalar (sqlString, parameters)) > 0);
        }

        public bool IsUserFollowingContest (UInt64 contestId, int userId)
        {
            // Make sure the email does not already exist?
            string sqlString = "SELECT * FROM contest_followers " +
                " WHERE contest_id = @contestId " +
                " AND user_id = @userId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@userId", userId));
            parameters.Add (new MySqlParameter ("@contestId", contestId));
            return (Convert.ToInt32 (Db.Master.GetScalar (sqlString, parameters)) > 0);
        }

        public bool IsUserIPSameAsContestOwner (UInt64 contestId, string ipAddress)
        {
            string sqlString = "SELECT contest_id " +
                " FROM contests_ugc c " +
                " INNER JOIN users u ON u.user_id = c.user_id " +
                " WHERE c.contest_id = @contestId " +
                " AND (ip_address = INET_ATON(@ipAddress) OR last_ip_address = INET_ATON(@ipAddress))";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@contestId", contestId));
            parameters.Add (new MySqlParameter ("@ipAddress", ipAddress));

            return (Convert.ToInt32 (Db.Master.GetScalar (sqlString, parameters)) > 0);
        }

        public int InsertContestTransactionDetails (int wokTransLogId, UInt64 contestId, UInt16 contestTransType)
        {
            string sqlString = "INSERT INTO contest_transaction_details ( " +
                " trans_id, contest_id, transaction_type, transaction_date " +
                ") VALUES (" +
                " @wokTransLogId, @contestId, @contestTransType, NOW())";

            List<IDbDataParameter> parameters = new List<IDbDataParameter> ();
            parameters.Add (new MySqlParameter ("@wokTransLogId", wokTransLogId));
            parameters.Add (new MySqlParameter ("@contestId", contestId));
            parameters.Add (new MySqlParameter ("@contestTransType", contestTransType));

            return Db.Master.ExecuteNonQuery (sqlString, parameters);
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}
