///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MYSQLGameServerDao : IGameServerDao
    {
        /// <summary>
        /// Gets a Game Server.
        /// </summary>
        /// <param name="serverId">Unique user identifier.</param>
        /// <returns>GameServer.</returns>
        public GameServer GetServer(int serverId)
        {
            string key = CentralCache.keyGameServerById(serverId);

            GameServer gs = (GameServer)CentralCache.Get(key);
            if (gs == null)
            {
                //create query string to get game by user id
                string sql = "SELECT server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, " +
                    " number_of_players, max_players, last_ping_datetime, server_type_id, modifiers_id " +
                    " FROM game_servers gs WHERE server_id = @serverId ";

                //indicate any parameters for place holder variables in query string
                List<IDbDataParameter> parameters = new List<IDbDataParameter>();
                parameters.Add(new MySqlParameter("@serverId", serverId));

                //perform query returning single data row from the developer database
                DataRow row = Db.Developer.GetDataRow(sql.ToString(), parameters);
                if (row == null)
                {
                    return null;
                }

                //populate new instance of Game Object
                gs = new GameServer(Convert.ToInt32(row["server_id"]), Convert.ToInt32(row["game_id"]), Convert.ToInt32(row["visibility_id"]),
                    Convert.ToInt32(row["port"]), Convert.ToInt32(row["server_status_id"]), Convert.ToInt32(row["server_type_id"]),
                    Convert.ToInt32(row["modifiers_id"]), Convert.ToInt32(row["number_of_players"]), Convert.ToInt32(row["max_players"]),
                    row["server_name"].ToString(), row["ip_address"].ToString(), Convert.ToDateTime(row["server_started_date"]),
                    Convert.ToDateTime(row["last_ping_datetime"]));

                CentralCache.Store(key, gs);
            }
            return gs;
        }

        /// <summary>
        /// Gets all servers.
        /// </summary>
        /// <param name="serverName">Unique user identifier.</param>
        /// <returns>GameServer.</returns>
        public GameServer GetServer(string serverName)
        {
            //create query string to get game by user id
            string sql = "SELECT server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, " +
                " number_of_players, max_players, last_ping_datetime, server_type_id, modifiers_id " +
                " FROM game_servers gs WHERE server_name = @serverName ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@serverName", serverName));

            //perform query returning single data row from the developer database
            DataRow row = Db.Developer.GetDataRow(sql.ToString(), parameters);

            //populate new instance of Game Object
            return new GameServer(Convert.ToInt32(row["server_id"]), Convert.ToInt32(row["game_id"]), Convert.ToInt32(row["visibility_id"]),
                Convert.ToInt32(row["port"]), Convert.ToInt32(row["server_status_id"]), Convert.ToInt32(row["server_type_id"]),
                Convert.ToInt32(row["modifiers_id"]), Convert.ToInt32(row["number_of_players"]), Convert.ToInt32(row["max_players"]),
                row["server_name"].ToString(), row["ip_address"].ToString(), Convert.ToDateTime(row["server_started_date"]),
                Convert.ToDateTime(row["last_ping_datetime"]));
        }


        /// <summary>
        /// Gets all registered games.
        /// </summary>
        /// <param name="filter">filtering on query</param>
        /// <param name="orderby">ordering on query</param>
        /// <param name="pageNumber">page number for record set</param>
        /// <param name="pageSize">how many records in page</param>
        /// <returns>IList<GameServer> </returns>
        public IList<GameServer> GetAllServers(string filter, string orderby)
        {
            return GetServerListByGameId(0, filter, orderby);
        }

        /// <summary>
        /// Gets all server for a particular game games.
        /// </summary>
        /// <param name="gameId">limit results to specific user</param>
        /// <param name="filter">filtering on query</param>
        /// <param name="orderby">ordering on query</param>
        /// <returns>IList<GameServer>.</returns>
        public IList<GameServer> GetServerListByGameId(int gameId, string filter, string orderby)
        {
            string strQuery = "";
            string strWhere = "";
            string strOrderBy = "";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            // Friends I invited
            strQuery = "SELECT server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, " +
                " number_of_players, max_players, last_ping_datetime, server_type_id, modifiers_id ";

            strQuery += " FROM game_servers g ";

            if (gameId > 0)
            {
                strWhere += " WHERE game_id = @game_id ";
                parameters.Add(new MySqlParameter("@game_id", gameId));
            }

            if (filter.Length > 0)
            {
                if (strWhere.Length > 0)
                {
                    strWhere += " AND " + filter;
                }
                else
                {
                    strWhere += " WHERE " + filter;
                }
            }

            if (orderby != null && orderby.Length > 0)
            {
                strOrderBy = " ORDER BY " + orderby;
            }

            strQuery = strQuery + strWhere + strOrderBy;

            DataTable dt = Db.Developer.GetDataTable(strQuery, parameters);

            IList<GameServer> list = new List<GameServer>();
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new GameServer(Convert.ToInt32(row["server_id"]), Convert.ToInt32(row["game_id"]), Convert.ToInt32(row["visibility_id"]),
                Convert.ToInt32(row["port"]), Convert.ToInt32(row["server_status_id"]), Convert.ToInt32(row["server_type_id"]),
                Convert.ToInt32(row["modifiers_id"]), Convert.ToInt32(row["number_of_players"]), Convert.ToInt32(row["max_players"]),
                row["server_name"].ToString(), row["ip_address"].ToString(), Convert.ToDateTime(row["server_started_date"]),
                Convert.ToDateTime(row["last_ping_datetime"])));
            }
            return list;
        }

        /// <summary>
        /// Gets all server for a particular game games.
        /// </summary>
        /// <param name="gameId">limit results to specific user</param>
        /// <param name="filter">filtering on query</param>
        /// <param name="orderby">ordering on query</param>
        /// <returns>DataTable<GameServer>.</returns>
        public DataTable GetServerByGameId(int gameId, string filter, string orderby)
        {
            string strQuery = "";
            string strWhere = "";
            string strOrderBy = "";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            // Friends I invited
            strQuery = "SELECT server_id, server_name, game_id, visibility_id, server_started_date, ip_address, port, server_status_id, " +
                " number_of_players, max_players, last_ping_datetime, server_type_id, modifiers_id ";

            strQuery += " FROM game_servers g ";

            if (gameId > 0)
            {
                strWhere += " WHERE game_id = @game_id ";
                parameters.Add(new MySqlParameter("@game_id", gameId));
            }

            if (filter.Length > 0)
            {
                if (strWhere.Length > 0)
                {
                    strWhere += " AND " + filter;
                }
                else
                {
                    strWhere += " WHERE " + filter;
                }
            }

            if (orderby != null && orderby.Length > 0)
            {
                strOrderBy = " ORDER BY " + orderby;
            }

            strQuery = strQuery + strWhere + strOrderBy;

            return Db.Developer.GetDataTable(strQuery, parameters);
        }

        /// <summary>
        /// Gets all patch URLs for a particular game.
        /// </summary>
        /// <param name="gameId">limit results to specific user</param>
        /// <param name="filter">filtering on query</param>
        /// <param name="orderby">ordering on query</param>
        /// <returns>DataTable</returns>
        public DataTable GetPatchURLByGameId(int gameId, string filter, string orderby)
        {
            DataTable dt = null;
            string key = null;

            if (filter == "" && orderby == "")
            {
                key = CentralCache.keyDefaultGamePatch(gameId);
                dt = (DataTable)CentralCache.Get(key);
                if (dt != null)
                    return dt;
            }

            string strQuery = "";
            string strWhere = "";
            string strOrderBy = "";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            // Friends I invited
            strQuery = "SELECT patchURL_id, game_id, patch_id, patch_url, patch_type, client_path";

            strQuery += " FROM game_patch_urls gpu ";

            if (gameId > 0)
            {
                strWhere += " WHERE game_id = @game_id ";
                parameters.Add(new MySqlParameter("@game_id", gameId));
            }

            if (filter.Length > 0)
            {
                if (strWhere.Length > 0)
                {
                    strWhere += " AND " + filter;
                }
                else
                {
                    strWhere += " WHERE " + filter;
                }
            }

            if (orderby != null && orderby.Length > 0)
            {
                strOrderBy = " ORDER BY " + orderby;
            }

            strQuery = strQuery + strWhere + strOrderBy;

            dt = Db.Developer.GetDataTable(strQuery, parameters);
            if (key != null)
                CentralCache.Store(key, dt);

            return dt;
        }

        //--manage server functions---
        /// <summary>
        /// Add A New Game Server
        /// </summary>
        /// <param name="server"></param>
        /// <returns>int</returns>
       /* public int AddNewServer(GameServer server)
        {
            // Send the message
            string sql = "INSERT INTO game_servers " +
                "(server_name, game_id, visibility_id, server_started_date, ip_address, port, " +
                " number_of_players, max_players, last_ping_datetime, modifiers_id) " +
                " VALUES (@server_name, @game_id, @visibility_id, @server_started_date, @ip_address, @port, " +
                " @number_of_players, @max_players, @last_ping_datetime, @modifiers_id)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@server_name", server.ServerName));
            parameters.Add(new MySqlParameter("@game_id", server.GameId));
            parameters.Add(new MySqlParameter("@visibility_id", server.VisibiltyId));
            parameters.Add(new MySqlParameter("@server_started_date", server.ServerStartedDate));
            parameters.Add(new MySqlParameter("@ip_address", server.IPAddress));
            parameters.Add(new MySqlParameter("@port", server.Port));
            parameters.Add(new MySqlParameter("@number_of_players", server.NumberOfPlayers));
            parameters.Add(new MySqlParameter("@max_players", server.MaxPlayers));
            parameters.Add(new MySqlParameter("@last_ping_datetime", server.LastPingDate));
            parameters.Add(new MySqlParameter("@modifiers_id", server.GameServerModifiersId));

            return Db.Developer.ExecuteIdentityInsert(sql, parameters);
        }*/

        //--manage server functions---
        /// <summary>
        /// Add A New Game Server
        /// </summary>
        /// <param name="server"></param>
        /// <returns>int</returns>
        public int AddNewServer(GameServer server)
        {
            // Send the message
            string sql = "SET @serverId=0; CALL add_game_servers (@server_name, @game_id, @visibility_id, @server_started_date, @ip_address, " +
             "@port, @number_of_players, @max_players, @last_ping_datetime, @modifiers_id, @serverId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@server_name", server.ServerName));
            parameters.Add(new MySqlParameter("@game_id", server.GameId));
            parameters.Add(new MySqlParameter("@visibility_id", server.VisibiltyId));
            parameters.Add(new MySqlParameter("@server_started_date", server.ServerStartedDate));
            parameters.Add(new MySqlParameter("@ip_address", server.IPAddress));
            parameters.Add(new MySqlParameter("@port", server.Port));
            parameters.Add(new MySqlParameter("@number_of_players", server.NumberOfPlayers));
            parameters.Add(new MySqlParameter("@max_players", server.MaxPlayers));
            parameters.Add(new MySqlParameter("@last_ping_datetime", server.LastPingDate));
            parameters.Add(new MySqlParameter("@modifiers_id", server.GameServerModifiersId));

            return Db.Developer.ExecuteNonQuery(sql, parameters);
        }

        // called when adding a game server/patch from the Editor
        // will return rc values of 
        // 0 = ok
        // 2 = key or game_id not found
        // 3 = not authorized of game not found
        // 5 = access denied (or user id not found)
        public int AddNewServerAndPatch(int userId, string licenseKey, int gameId, string hostName, int port, string patchUrl, string alternatePatchUrl, string patchType, string clientPath, ref int webAssignedPort, out int serverId)
        {
            string sql = "call add_game_server( @userId, @licenseKey, @gameId, @hostName, @port, @patchUrl, @alternatePatchUrl, @patchType, @clientPath, @rc, @ipPort, @serverId ); " +
                         "SELECT CAST(@rc as UNSIGNED INT) as rc, CAST(@ipPort as UNSIGNED INT) as ipPort, CAST(@serverId as UNSIGNED INT) as serverId;";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(new MySqlParameter("@userId", userId));
            parameters.Add(new MySqlParameter("@licenseKey", licenseKey));
            parameters.Add(new MySqlParameter("@gameId", gameId));
            parameters.Add(new MySqlParameter("@hostName", hostName));
            parameters.Add(new MySqlParameter("@port", port));
            parameters.Add(new MySqlParameter("@patchUrl", patchUrl));
            parameters.Add(new MySqlParameter("@alternatePatchUrl", alternatePatchUrl));
            parameters.Add(new MySqlParameter("@patchType", patchType));
            parameters.Add(new MySqlParameter("@clientPath", clientPath));

            DataRow row = Db.Developer.GetDataRow(sql.ToString(), parameters);
            
            int ret = Convert.ToInt32(row["rc"]);
            if (ret == 0)
                CentralCache.Remove(CentralCache.keyDefaultGamePatch(gameId));

            webAssignedPort = Convert.ToInt32(row["ipPort"]);
            serverId = Convert.ToInt32(row["serverId"]);
            return ret;
        }

        /// <summary>
        /// get a list of registered games 
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public PagedDataTable GetGames(int pageNumber, int pageSize, string orderBy)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            
            // XML conversion barfs on empty strings, so add IFs
            // TODO -- enforce valid values in DB
            string selectList = " SELECT IF(length(g.game_name) = 0, 'unknown', g.game_name)  AS name,  " +
                                "          g.game_id, " +
                                "           IF(length(g.game_description) = 0, 'unknown', g.game_description)  AS description, " +
                                "           sum(gs.number_of_players) AS population,  " +
                                "           gl.max_game_users AS max_population,  " +
                                "           (SELECT count(*) FROM game_raves gr WHERE gr.game_id = g.game_id) AS raves " +
                                " FROM  games g " +
                                "       INNER JOIN game_servers gs on g.game_id = gs.game_id " +
                                "       INNER JOIN game_licenses gl on gl.game_id = g.game_id " +
                                " WHERE g.game_status_id = 1  " + //-- active
                                "      AND gs.server_status_id = 1  " + //-- 1 is running
                                "        AND timestampdiff (minute, last_ping_datetime, NOW()) < deadServerInterval() " + // -- minute limit  
                                "        AND gs.visibility_id = 1 " + //-- server is public  
                                "        AND g.game_access_id = 1 " + //-- game is public
                                "        AND gs.is_external = 1 " + //--exclude WOK games
                                " GROUP BY 1, 2, 3 ";

            string orderByList = orderBy;
            /*
                SELECT IF(length(g.game_name) = 0, 'unknown', g.game_name)  AS name, 
                          g.game_id,
                          IF(length(g.game_description) = 0, 'unknown', g.game_description)  AS description,
                          sum(gs.number_of_players) AS population, 
                          gl.max_game_users AS max_population, 
                          (SELECT count(*) FROM game_raves gr WHERE gr.game_id = g.game_id) AS raves
                FROM  games g
                      INNER JOIN game_servers gs on g.game_id = gs.game_id
                      INNER JOIN game_licenses gl on gl.game_id = g.game_id
                WHERE g.game_status_id = 1 -- active
                     AND gs.server_status_id = 1 -- 1 is running
	                   AND timestampdiff (minute, last_ping_datetime, NOW()) < deadServerInterval() -- minute limit 
	                   AND gs.visibility_id = 1 -- public 
                       AND g.game_access_id = 1 -- game is public
                GROUP BY 1, 2, 3;
            */

            return Db.Developer.GetPagedDataTable(selectList, orderByList, parameters, pageNumber, pageSize);
        }

        //--manage game functions---
        /// <summary>
        /// Update Game server
        /// </summary>
        /// <param name="server"></param>
        /// <returns>int</returns>
      /*  public int UpdateServer(GameServer server)
        {
            // Send the message
            string sql = "UPDATE game_servers SET " +
                " server_name = @server_name, game_id = @game_id, visibility_id = @visibility_id, server_started_date = @server_started_date, " +
                " ip_address = @ip_address, port = @port, number_of_players = @number_of_players, " + 
                " max_players = @max_players, last_ping_datetime = @last_ping_datetime, " +
                " modifiers_id = @modifiers_id WHERE server_id = @server_id";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@server_name", server.ServerName));
            parameters.Add(new MySqlParameter("@game_id", server.GameId));
            parameters.Add(new MySqlParameter("@visibility_id", server.VisibiltyId));
            parameters.Add(new MySqlParameter("@server_started_date", server.ServerStartedDate));
            parameters.Add(new MySqlParameter("@ip_address", server.IPAddress));
            parameters.Add(new MySqlParameter("@port", server.Port));
            parameters.Add(new MySqlParameter("@number_of_players", server.NumberOfPlayers));
            parameters.Add(new MySqlParameter("@max_players", server.MaxPlayers));
            parameters.Add(new MySqlParameter("@last_ping_datetime", server.LastPingDate));
            parameters.Add(new MySqlParameter("@modifiers_id", server.GameServerModifiersId));
            parameters.Add(new MySqlParameter("@server_id", server.ServerId));

            return Db.Developer.ExecuteNonQuery(sql, parameters);
        }*/

        //--manage game functions---
        /// <summary>
        /// Update Game server
        /// </summary>
        /// <param name="server"></param>
        /// <returns>int</returns>
        public int UpdateServer(GameServer server)
        {
            // Send the message
            string sql = "CALL update_game_servers (@server_id, @server_name, @game_id, @visibility_id, @server_started_date, @ip_address, @port, " +
                      "@number_of_players, @max_players, @last_ping_datetime, @modifiers_id)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@server_id", server.ServerId));
            parameters.Add(new MySqlParameter("@server_name", server.ServerName));
            parameters.Add(new MySqlParameter("@game_id", server.GameId));
            parameters.Add(new MySqlParameter("@visibility_id", server.VisibiltyId));
            parameters.Add(new MySqlParameter("@server_started_date", server.ServerStartedDate));
            parameters.Add(new MySqlParameter("@ip_address", server.IPAddress));
            parameters.Add(new MySqlParameter("@port", server.Port));
            parameters.Add(new MySqlParameter("@number_of_players", server.NumberOfPlayers));
            parameters.Add(new MySqlParameter("@max_players", server.MaxPlayers));
            parameters.Add(new MySqlParameter("@last_ping_datetime", server.LastPingDate));
            parameters.Add(new MySqlParameter("@modifiers_id", server.GameServerModifiersId));

            CentralCache.Remove( CentralCache.keyGameServerById( server.ServerId ) );

            return Db.Developer.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Update Game server at pinger initialization
        /// </summary>
        /// <param name="server"></param>
        /// <returns>int</returns>
        public DataRow UpdateGameServerStart(int serverId, int maxCount, int statusId, string ipAddress, int port, string actualIp, int type, uint protocolVersion, int schemaVersion, string serverVersion, uint assetVersion, int parentGameId)
        {
            string sql = "CALL update_game_servers_start( @serverId, @maxCount, @statusId, @ip, @port, @actualIp, @serverType, @protocolVer, @schemaVer, @engineVer, @assetVer, @parentGameId);";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@serverId", serverId));
            parameters.Add(new MySqlParameter("@maxCount", maxCount));
            parameters.Add(new MySqlParameter("@statusId", statusId));
            parameters.Add(new MySqlParameter("@ip", ipAddress));
            parameters.Add(new MySqlParameter("@port", port));
            parameters.Add(new MySqlParameter("@actualIp", actualIp));
            parameters.Add(new MySqlParameter("@serverType", type));
            parameters.Add(new MySqlParameter("@protocolVer", protocolVersion));
            parameters.Add(new MySqlParameter("@schemaVer", schemaVersion));
            parameters.Add(new MySqlParameter("@engineVer", serverVersion));
            parameters.Add(new MySqlParameter("@assetVer", assetVersion));
            parameters.Add(new MySqlParameter("@parentGameId", parentGameId));

            CentralCache.Remove(CentralCache.keyGameServerById(serverId));

            return Db.Developer.GetDataRow(sql, parameters);
        }

        /// <summary>
        /// Update Game server by pinging
        /// </summary>
        /// <param name="server"></param>
        /// <returns>int</returns>
        public DataRow UpdateGameServerPingCheck(int serverId, string actualIp, int numberOfPlayers, int maxNumberOfPlayers, int statusId, bool adminOnly)
        {
            string sql = "CALL update_game_servers_ping_check( @serverId, @actualIp, @numberOfPlayers, @maxNumberOfPlayers, @statusId, " +
                                                        (adminOnly ? "'T'" : "'F'") + ");";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@serverId", serverId));
            parameters.Add(new MySqlParameter("@actualIp", actualIp));
            parameters.Add(new MySqlParameter("@numberOfPlayers", numberOfPlayers));
            parameters.Add(new MySqlParameter("@maxNumberOfPlayers", maxNumberOfPlayers));
            parameters.Add(new MySqlParameter("@statusId", statusId));

            CentralCache.Remove(CentralCache.keyGameServerById(serverId));
            CentralCache.Remove(CentralCache.keyGameServerAccess(serverId));

            return Db.Developer.GetDataRow(sql, parameters);
        }

        //--manage game functions---
        /// <summary>
        /// Delete Game Server
        /// </summary>
        /// <param name="serverId"></param>
        /// <returns></returns>
        /*public int DeleteServer(int serverId)
        {
            // Send the message
            string sql = "DELETE FROM game_servers WHERE server_id =  @serverId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@serverId", serverId));

            return Db.Developer.ExecuteNonQuery(sql, parameters);
        }        
        */
        //--manage game functions---
        /// <summary>
        /// Delete Game Server
        /// </summary>
        /// <param name="serverId"></param>
        /// <returns></returns>
        public int DeleteServer(int serverId)
        {
            // Send the message
            string sql = "CALL remove_game_servers (@serverId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@serverId", serverId));

            CentralCache.Remove( CentralCache.keyGameServerById( serverId ) );

            return Db.Developer.ExecuteNonQuery(sql, parameters);
        }

        
        /// <summary>
        /// Add A New Patch URL
        /// </summary>
        /// <param name="server"></param>
        /// <returns>int</returns>
        public int AddNewPatchURL(int gameId, string patchUrl)
        {
            // Send the message
            string sql = "INSERT INTO game_patch_urls (game_id, patch_url) VALUES (@gameId, @patchUrl)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@gameId", gameId));
            parameters.Add(new MySqlParameter("@patchUrl", patchUrl));

            CentralCache.Remove(CentralCache.keyDefaultGamePatch(gameId));

            return Db.Developer.ExecuteIdentityInsert(sql, parameters);
        }

        //--manage game functions---
        /// <summary>
        /// Update Patch URL
        /// </summary>
        /// <param name="server"></param>
        /// <returns>int</returns>
        public int UpdatePatchURL(int gameId, int patchURLId, string patchUrl)
        {
            // Send the message
            string sql = "UPDATE game_patch_urls SET patch_url = @patchUrl WHERE patchURL_id = @patchURLId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@patchUrl", patchUrl));
            parameters.Add(new MySqlParameter("@patchURLId", patchURLId));

            CentralCache.Remove( CentralCache.keyDefaultGamePatch( gameId ) );

            return Db.Developer.ExecuteNonQuery(sql, parameters);
        }

        //--manage game functions---
        /// <summary>
        /// Delete Patch URL
        /// </summary>
        /// <param name="serverId"></param>
        /// <returns></returns>
        public int DeletePatchURL(int gameId, int patchURLId)
        {
            // Send the message
            string sql = "DELETE FROM game_patch_urls WHERE patchURL_id = @patchURLId";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@patchURLId", patchURLId));

            CentralCache.Remove(CentralCache.keyDefaultGamePatch(gameId));

            return Db.Developer.ExecuteNonQuery(sql, parameters);
        }


        //functions for pull down options
        /// <summary>
        /// Get Game Server Visibility Options
        /// </summary>
        /// <returns></returns>
        public DataTable GetGameServerVisibility()
        {
            string strQuery = "SELECT visibility_id, name, description FROM game_server_visibility ";

            return Db.Developer.GetDataTable(strQuery);
        }

        /// <summary>
        /// Get Game Server Status Options
        /// </summary>
        /// <returns></returns>
        public DataTable GetGameServerStatus()
        {
            string strQuery = "SELECT server_status_id, name, description FROM game_server_status ";

            return Db.Developer.GetDataTable(strQuery);
        }

        /// <summary>
        /// Gets Game server's config settings as a string 
        /// </summary>
        public string GetServerConfig(int serverType, string serverActualIp)
        {
            string strQuery = "CALL get_server_config( @type, @actualIp )";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@type", serverType));
            parameters.Add(new MySqlParameter("@actualIp", serverActualIp));

            DataRow dr = Db.Developer.GetDataRow(strQuery, parameters);
            string ret = "1\tDatabase error";
            if (dr != null)
            {
                ret = dr[0].ToString();
            }
            return ret;
        }

        /// <summary>
        /// Get Game Server Type Options
        /// </summary>
        /// <returns></returns>
        public DataTable GetGameServerType()
        {
            string strQuery = "SELECT server_type_id, name, description FROM game_server_types ";

            return Db.Developer.GetDataTable(strQuery);
        }

        /// <summary>
        /// Returns the version of a given server.
        /// </summary>
        /// <returns></returns>
        public string GetServerVersion(int serverId)
        {
            string sql = "SELECT coalesce(gs.server_engine_version, '') AS version " +
                " FROM game_servers gs " +
                " WHERE gs.server_id = @serverId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@serverId", serverId));

            return (Db.Developer.GetScalar(sql, parameters) ?? string.Empty).ToString();
        }

        /// <summary>
        /// Returns the version of a given server.
        /// </summary>
        /// <returns></returns>
        public string GetServerVersion(int gameId, string serverName, int port, int serverTypeId)
        {
            string sql = "SELECT coalesce(gs.server_engine_version, '') AS version " +
                " FROM game_servers gs " +
                " WHERE gs.game_id = @gameId " +
                " AND gs.server_name = @serverName " + 
                " AND gs.port = @port " +
                " AND gs.server_type_id = @serverTypeId " +
                " LIMIT 1 ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@gameId", gameId));
            parameters.Add(new MySqlParameter("@serverName", serverName));
            parameters.Add(new MySqlParameter("@port", port));
            parameters.Add(new MySqlParameter("@serverTypeId", serverTypeId));

            return (Db.Developer.GetScalar(sql, parameters) ?? string.Empty).ToString();
        }
    }
}
