///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.MySQL
{
    public class MYSQLGameLicenseDao : IGameLicenseDao
    {
        /// <summary>
        /// Gets a Game License By license id.
        /// </summary>
        /// <param name="gameLicenseId">Unique identifier.</param>
        /// <returns>GameLicense.</returns>
        public GameLicense GetGameLicense(int gameLicenseId)
        {
            //create query string to get game by user id
            string sql = "SELECT game_license_id, game_id, license_subscription_id, game_key, start_date, end_date, purchase_date, " + 
                " license_status_id, max_game_users, modifiers_id FROM game_licenses gl WHERE game_license_id = @gameLicenseId ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@gameLicenseId", gameLicenseId));

            //perform query returning single data row from the developer database
            DataRow row = Db.Developer.GetDataRow(sql.ToString(), parameters);

            //process null datetimes before conversion
            if (row["start_date"].GetType().Equals(System.DBNull.Value))
            {
                row["start_date"] = DateTime.MinValue.ToString();
            }
            if (row["end_date"].GetType().Equals(System.DBNull.Value))
            {
                row["end_date"] = DateTime.MinValue.ToString();
            }
            if (row["purchase_date"].GetType().Equals(System.DBNull.Value))
            {
                row["purchase_date"] = DateTime.MinValue.ToString();
            }

            //populate new instance of Game Object
            return new GameLicense(Convert.ToInt32(row["game_license_id"]), Convert.ToInt32(row["game_id"]), 
                Convert.ToInt32(row["license_subscription_id"]),Convert.ToInt32(row["license_status_id"]), 
                Convert.ToInt32(row["modifiers_id"]), row["game_key"].ToString(),
                Convert.ToDateTime(row["start_date"]), Convert.ToDateTime(row["end_date"]),
                Convert.ToDateTime(row["purchase_date"]), Convert.ToInt32(row["max_game_users"]));
        }

        /// <summary>
        /// Gets a Game License By game id.
        /// only effective for current business rule of only one license per game
        /// </summary>
        /// <param name="gameId">Unique identifier.</param>
        /// <returns>GameLicense</returns>
        public GameLicense GetGameLicenseByGameId(int gameId)
        {
            //create query string to get game by user id
            string sql = "SELECT game_license_id, game_id, license_subscription_id, game_key, start_date, end_date, purchase_date, " +
                " license_status_id, max_game_users, modifiers_id FROM game_licenses gl WHERE game_id = @gameId ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@gameId", gameId));

            //perform query returning single data row from the developer database
            DataRow row = Db.Developer.GetDataRow(sql.ToString(), parameters);

            if (row == null)
            {
                return new GameLicense();
            }

            //process null datetimes before conversion
            if (row["start_date"].Equals(System.DBNull.Value))
            {
                row["start_date"] = DateTime.MinValue.ToString();
            }
            if (row["end_date"].Equals(System.DBNull.Value))
            {
                row["end_date"] = DateTime.MinValue.ToString();
            }
            if (row["purchase_date"].Equals(System.DBNull.Value))
            {
                row["purchase_date"] = DateTime.MinValue.ToString();
            }

            //populate new instance of Game Object
            return new GameLicense(Convert.ToInt32(row["game_license_id"]), Convert.ToInt32(row["game_id"]),
                Convert.ToInt32(row["license_subscription_id"]), Convert.ToInt32(row["license_status_id"]),
                Convert.ToInt32(row["modifiers_id"]), row["game_key"].ToString(),
                Convert.ToDateTime(row["start_date"]), Convert.ToDateTime(row["end_date"]),
                Convert.ToDateTime(row["purchase_date"]), Convert.ToInt32(row["max_game_users"]));
        }

        /// <summary>
        /// Gets a Game License By game key.
        /// </summary>
        /// <param name="gameId">Unique identifier.</param>
        /// <returns>GameLicense</returns>
        public GameLicense GetGameLicenseByGameKey(string gameKey)
        {
            //create query string to get game by user id
            string sql = "SELECT game_license_id, game_id, license_subscription_id, game_key, start_date, end_date, purchase_date, " +
                " license_status_id, max_game_users, modifiers_id FROM game_licenses gl WHERE game_key = @gameKey ";

            //indicate any parameters for place holder variables in query string
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@gameKey", gameKey));

            //perform query returning single data row from the developer database
            DataRow row = Db.Developer.GetDataRow(sql.ToString(), parameters);

            //process null datetimes before conversion
            if (row["start_date"].Equals(System.DBNull.Value))
            {
                row["start_date"] = DateTime.MinValue.ToString();
            }
            if (row["end_date"].Equals(System.DBNull.Value))
            {
                row["end_date"] = DateTime.MinValue.ToString();
            }
            if (row["purchase_date"].Equals(System.DBNull.Value))
            {
                row["purchase_date"] = DateTime.MinValue.ToString();
            }

            //populate new instance of Game Object
            return new GameLicense(Convert.ToInt32(row["game_license_id"]), Convert.ToInt32(row["game_id"]),
                Convert.ToInt32(row["license_subscription_id"]), Convert.ToInt32(row["license_status_id"]),
                Convert.ToInt32(row["modifiers_id"]), row["game_key"].ToString(),
                Convert.ToDateTime(row["start_date"]), Convert.ToDateTime(row["end_date"]),
                Convert.ToDateTime(row["purchase_date"]), Convert.ToInt32(row["max_game_users"]));
        }

        /// <summary>
        /// Gets all game licenses.
        /// </summary>
        /// <param name="filter">filtering on query</param>
        /// <param name="orderby">ordering on query</param>
        /// <param name="pageNumber">page number for record set</param>
        /// <param name="pageSize">how many records in page</param>
        /// <returns>IIList<GameLicense></returns>
        public IList<GameLicense> GetAllLicenses(string filter, string orderby)
        {
            return GetLicenseByGameId(0, filter, orderby);
        }

        /// <summary>
        /// Gets all game licenses for a particular game.
        /// allows for multiple licenses per game or just one
        /// </summary>
        /// <param name="gameId">limit results to specific user</param>
        /// <param name="filter">filtering on query</param>
        /// <param name="orderby">ordering on query</param>
        /// <param name="pageNumber">page number for record set</param>
        /// <param name="pageSize">how many records in page</param>
        /// <returns>IList<GameServer>.</returns>
        public IList<GameLicense> GetLicenseByGameId(int gameId, string filter, string orderby)
        {
            // UNION WAY
            string strQuery = "";
            string strWhere = "";
            string strOrderBy = "";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            // Friends I invited
            strQuery = "SELECT game_license_id, game_id, license_subscription_id, game_key, start_date, end_date, purchase_date, " +
                " license_status_id, max_game_users, modifiers_id ";

            strQuery += " FROM game_licenses gl ";

            if (gameId > 0)
            {
                strWhere += " WHERE game_id = @game_id ";
                parameters.Add(new MySqlParameter("@game_id", gameId));
            }

            if (filter.Length > 0)
            {
                if (strWhere.Length > 0)
                {
                    strWhere += " AND " + filter;
                }
                else
                {
                    strWhere += " WHERE " + filter;
                }
            }

            if (orderby != null && orderby.Length > 0)
            {
                strOrderBy = " ORDER BY " + orderby;
            }

            strQuery = strQuery + strWhere + strOrderBy;

            DataTable dt = Db.Master.GetDataTable(strQuery, parameters);

            IList<GameLicense> list = new List<GameLicense>();
            foreach (DataRow row in dt.Rows)
            {
                //process null datetimes before conversion
                if (row["start_date"].Equals(System.DBNull.Value))
                {
                    row["start_date"] = DateTime.MinValue.ToString();
                }
                if (row["end_date"].Equals(System.DBNull.Value))
                {
                    row["end_date"] = DateTime.MinValue.ToString();
                }
                if (row["purchase_date"].Equals(System.DBNull.Value))
                {
                    row["purchase_date"] = DateTime.MinValue.ToString();
                }

                list.Add(new GameLicense(Convert.ToInt32(row["game_license_id"]), Convert.ToInt32(row["game_id"]),
                Convert.ToInt32(row["license_subscription_id"]), Convert.ToInt32(row["license_status_id"]),
                Convert.ToInt32(row["modifiers_id"]), row["game_key"].ToString(),
                Convert.ToDateTime(row["start_date"]), Convert.ToDateTime(row["end_date"]),
                Convert.ToDateTime(row["purchase_date"]), Convert.ToInt32(row["max_game_users"])));
            }
            return list;
        }


        //--manage server functions---
        /// <summary>
        /// Add A New Game License
        /// </summary>
        /// <param name="license"></param>
        /// <returns>int</returns>
        public int AddNewGameLicense(GameLicense license, int daysLeft)
        {
            // Send the message
            string sql = "CALL add_new_game_license (@game_id, @license_subscription_id, @game_key, NOW(), ADDDATE(NOW(), " +
                "INTERVAL @daysLeft DAY), NOW(), @license_status_id, @max_game_users, @modifiers_id, @gameLicenseId); SELECT CAST(@gameLicenseId as UNSIGNED INT) as gameLicenseId; ";
            
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@game_id", license.GameId));
            parameters.Add(new MySqlParameter("@license_subscription_id", license.LicenseSubscriptionId));
            parameters.Add(new MySqlParameter("@game_key", license.GameKey));
            parameters.Add(new MySqlParameter("@license_status_id", license.LicenseStatusId));
            parameters.Add(new MySqlParameter("@modifiers_id", license.ModifierId));
            parameters.Add(new MySqlParameter("@max_game_users", license.MaxGameUsers));
            parameters.Add(new MySqlParameter("@daysLeft", daysLeft));

            return Convert.ToInt32(Db.Developer.GetScalar(sql, parameters));
        }

        //--manage game functions---
        /// <summary>
        /// Update Game server license
        /// </summary>
        /// <param name="license"></param>
        /// <returns>int</returns>
        public int UpdateGameLicense(GameLicense license)
        {
            // Send the message
            string sql = "CALL update_game_license (@game_license_id, @game_id, @license_subscription_id, @game_key, @start_date, @end_date, " +
                "@purchase_date, @license_status_id, @max_game_users, @modifiers_id)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@game_license_id", license.GameLicenseId));
            parameters.Add(new MySqlParameter("@game_id", license.GameId));
            parameters.Add(new MySqlParameter("@license_subscription_id", license.LicenseSubscriptionId));
            parameters.Add(new MySqlParameter("@game_key", license.GameKey));
            parameters.Add(new MySqlParameter("@start_date", license.StartDate));
            parameters.Add(new MySqlParameter("@end_date", license.EndDate));
            parameters.Add(new MySqlParameter("@purchase_date", license.PurchaseDate));
            parameters.Add(new MySqlParameter("@license_status_id", license.LicenseStatusId));
            parameters.Add(new MySqlParameter("@max_game_users", license.MaxGameUsers));
            parameters.Add(new MySqlParameter("@modifiers_id", license.ModifierId));

            return Convert.ToInt32(Db.Developer.GetScalar(sql, parameters));
        }

        //--manage game functions---
        /// <summary>
        /// Delete Game Server License
        /// </summary>
        /// <param name="licenseId"></param>
        /// <returns></returns>
        public int DeleteGameLicense(int licenseId)
        {
            // Send the message
            string sql = "CALL delete_game_license(@licenseId)";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@licenseId", licenseId));

            return Db.Developer.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// Get All Game license type Options
        /// </summary>
        /// <returns></returns>
        public GameLicenseSubscription GetGameLicenseType(int license_subscription_id)
        {
            string strQuery = "SELECT license_subscription_id, name, length_of_subscription, amount, amount_kei_point_id, " +
                " max_server_users, disk_quota " +
                " FROM game_license_subscriptions " +
                " WHERE license_subscription_id = @license_subscription_id ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@license_subscription_id", license_subscription_id));
            DataRow row = Db.Developer.GetDataRow(strQuery, parameters);

            if (row == null)
            {
                return new GameLicenseSubscription();
            }

            return new GameLicenseSubscription(Convert.ToInt32 (row["license_subscription_id"]), row["name"].ToString(), Convert.ToInt32(row["length_of_subscription"]), Convert.ToDouble (row["amount"]), row["amount_kei_point_id"].ToString(),
                Convert.ToInt32(row["max_server_users"]), Convert.ToInt32(row["disk_quota"]));
        }

        /// <summary>
        /// Get All Game license type Options
        /// </summary>
        /// <returns></returns>
        public List<GameLicenseSubscription> GetGameLicenseTypes()
        {
            string strQuery = "SELECT license_subscription_id, name, length_of_subscription, amount, amount_kei_point_id, " +
                " max_server_users, disk_quota FROM game_license_subscriptions ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            DataTable dt = Db.Developer.GetDataTable(strQuery, parameters);

            List<GameLicenseSubscription> list = new List<GameLicenseSubscription>();

            foreach (DataRow row in dt.Rows)
            {
                list.Add(new GameLicenseSubscription(Convert.ToInt32 (row["license_subscription_id"]), row["name"].ToString(), Convert.ToInt32(row["length_of_subscription"]), Convert.ToDouble (row["amount"]), row["amount_kei_point_id"].ToString(),
                Convert.ToInt32(row["max_server_users"]), Convert.ToInt32(row["disk_quota"]))
                );
            }
            return list;
        }

        /// <summary>
        /// Get Game Server Status Options
        /// </summary>
        /// <returns></returns>
        public DataTable GetGameLicenseStatus()
        {
            string strQuery = "SELECT license_status_id, license_status FROM game_license_status ";

            return Db.Developer.GetDataTable(strQuery);
        }

        /// <summary>
        /// GetGameSubscription
        /// </summary>
        public GameSubscription GetGameSubscription(int SubscriptionId)
        {
            string strQuery = "SELECT subscription_id, game_license_id, game_id, gross_point_amount, renewable, " +
                "purchase_date, gs_status_id, billing_type, subscription_identifier, user_cancelled" +
                " FROM game_subscriptions " +
                " WHERE subscription_id = @SubscriptionId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@SubscriptionId", SubscriptionId));
            DataRow row = Db.Developer.GetDataRow(strQuery, parameters);

            if (row == null)
            {
                return new GameSubscription();
            }

            return new GameSubscription(Convert.ToUInt32(row["subscription_id"]), Convert.ToInt32(row["game_license_id"]), Convert.ToInt32(row["game_id"]), Convert.ToDouble(row["gross_point_amount"]),
                row["renewable"].ToString().Equals("Y"), Convert.ToDateTime(row["purchase_date"]), Convert.ToUInt32(row["gs_status_id"]), Convert.ToInt32(row["billing_type"]),
                row["subscription_identifier"].ToString(), row["user_cancelled"].ToString().Equals("Y"));
        }

        /// <summary>
        /// GetGameSubscriptionByOrderId
        /// </summary>
        public GameSubscription GetGameSubscriptionByOrderId(int orderId)
        {
            string strQuery = "SELECT gs.subscription_id, game_license_id, game_id, gross_point_amount, renewable, " +
                " purchase_date, gs_status_id, billing_type, subscription_identifier, user_cancelled " +
                " FROM game_subscriptions gs, game_subscription_orders gso " +
                " WHERE gso.order_id = @orderId " +
                " and gs.subscription_id = gso.subscription_id ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@orderId", orderId));
            DataRow row = Db.Developer.GetDataRow(strQuery, parameters);

            if (row == null)
            {
                return new GameSubscription();
            }

            return new GameSubscription(Convert.ToUInt32(row["subscription_id"]), Convert.ToInt32(row["game_license_id"]), Convert.ToInt32(row["game_id"]), Convert.ToDouble(row["gross_point_amount"]),
                row["renewable"].ToString().Equals("Y"), Convert.ToDateTime(row["purchase_date"]), Convert.ToUInt32(row["gs_status_id"]), Convert.ToInt32(row["billing_type"]),
                row["subscription_identifier"].ToString(), row["user_cancelled"].ToString().Equals("Y"));
        }

        /// <summary>
        /// GetCompanySubscriptions
        /// </summary>
        public PagedList<GameSubscription> GetGameSubscriptions(int GameId, string orderby, int pageNumber, int pageSize)
        {
            string strQuery = "SELECT subscription_id, game_license_id, game_id, gross_point_amount, renewable, " +
                "purchase_date, gs_status_id, billing_type, subscription_identifier, user_cancelled" +
                " FROM game_subscriptions " +
                " WHERE game_id = @GameId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@GameId", GameId));

            PagedDataTable dt = Db.Developer.GetPagedDataTable(strQuery, orderby, parameters, pageNumber, pageSize);

           PagedList<GameSubscription> list = new PagedList<GameSubscription>();
            list.TotalCount = dt.TotalCount;

            foreach (DataRow row in dt.Rows)
            {
                list.Add (new GameSubscription(Convert.ToUInt32(row["subscription_id"]), Convert.ToInt32(row["game_license_id"]), Convert.ToInt32(row["game_id"]), Convert.ToDouble(row["gross_point_amount"]),
                row["renewable"].ToString().Equals("Y"), Convert.ToDateTime(row["purchase_date"]), Convert.ToUInt32(row["gs_status_id"]), Convert.ToInt32(row["billing_type"]),
                row["subscription_identifier"].ToString(), row["user_cancelled"].ToString().Equals("Y"))
                );
            }
            return list;
        }

        /// <summary>
        /// InsertGameSubscription
        /// </summary>
        public int InsertGameSubscription(GameSubscription gameSubscription)
        {
            string sqlString = "INSERT INTO game_subscriptions (" +
              " game_license_id, game_id, gross_point_amount, renewable, " +
              " purchase_date, gs_status_id, billing_type, subscription_identifier, user_cancelled" +
              ") VALUES (" +
              " @game_license_id,  @game_id,  @gross_point_amount,  @renewable, " +
              "  NOW(),  @gs_status_id,  @billing_type,  @subscription_identifier,  @user_cancelled" +
              ")";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@game_license_id", gameSubscription.GameLicenseId));
            parameters.Add(new MySqlParameter("@game_id", gameSubscription.GameId));
            parameters.Add(new MySqlParameter("@gross_point_amount", gameSubscription.GrossPointAmount));
            parameters.Add(new MySqlParameter("@renewable", gameSubscription.Renewable ? "Y" : "N"));
            parameters.Add(new MySqlParameter("@gs_status_id", gameSubscription.GsStatusId));
            parameters.Add(new MySqlParameter("@billing_type", gameSubscription.BillingType));
            parameters.Add(new MySqlParameter("@subscription_identifier", gameSubscription.SubscriptionIdentifier));
            parameters.Add(new MySqlParameter("@user_cancelled", gameSubscription.UserCancelled ? "Y" : "N"));
            return Db.Developer.ExecuteIdentityInsert(sqlString, parameters);
        }


        /// <summary>
        /// UpdateGameSubscription
        /// </summary>
        public void UpdateGameSubscription(GameSubscription gameSubscription)
        {            
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            string sqlString = "UPDATE game_subscriptions SET " +
                "gross_point_amount=@gross_point_amount, " +
                "renewable=@renewable, " +
                "gs_status_id=@gs_status_id, " +
                "billing_type=@billing_type, " +
                "subscription_identifier=@subscription_identifier, " +
                "user_cancelled=@user_cancelled" +
                " WHERE subscription_id = @subscription_id";

                parameters.Add(new MySqlParameter("@gross_point_amount", gameSubscription.GrossPointAmount));
                parameters.Add(new MySqlParameter("@renewable", gameSubscription.Renewable ? "Y" : "N"));
                parameters.Add(new MySqlParameter("@gs_status_id", gameSubscription.GsStatusId));
                parameters.Add(new MySqlParameter("@billing_type", gameSubscription.BillingType));
                parameters.Add(new MySqlParameter("@subscription_identifier", gameSubscription.SubscriptionIdentifier));
                parameters.Add(new MySqlParameter("@user_cancelled", gameSubscription.UserCancelled ? "Y" : "N"));
                parameters.Add(new MySqlParameter("@subscription_id", gameSubscription.SubscriptionId));
                Db.Developer.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// InsertGameSubscriptionOrder
        /// </summary>
        public int InsertGameSubscriptionOrder(int subscriptionId, int orderId)
        {
            string sqlString = "INSERT INTO game_subscription_orders (" +
                " subscription_id, order_id" +
                ") VALUES (" +
                " @subscriptionId,  @orderId" +
                ")";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@subscriptionId", subscriptionId));
            parameters.Add(new MySqlParameter("@orderId", orderId));
            return Db.Developer.ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// GetGameLicenseContact
        /// </summary>
        public GameLicenseContact GetGameLicenseContact (int gameLicenseId)
        {
            string strQuery = "SELECT game_license_contact_id, name, org_name, phone_number, email, about_game, game_license_id" +
                " FROM game_license_contact, created_date "+
                " WHERE game_license_id = @gameLicenseId ";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@gameLicenseId", gameLicenseId));
            DataRow row = Db.Developer.GetDataRow(strQuery, parameters);

            if (row == null)
            {
                return new GameLicenseContact();
            }

            return new GameLicenseContact(Convert.ToUInt32(row["game_license_contact_id"]) , row["name"].ToString() , row["org_name"].ToString() , row["phone_number"].ToString() , row["email"].ToString() , 
                row["about_game"].ToString() , Convert.ToInt32(row["game_license_id"]) );
        }

        /// <summary>
        /// InsertGameLicenseContact
        /// </summary>
        public int InsertGameLicenseContact(GameLicenseContact gameLicenseContact)
        {
            string sqlString = "INSERT INTO game_license_contact (" +
                " name, org_name, phone_number, email, about_game, " +
                " game_license_id, created_date " +
                ") VALUES (" +
                " @name,  @org_name,  @phone_number,  @email,  @about_game, " +
                "  @game_license_id, NOW() " +
                ")";

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(new MySqlParameter("@name", gameLicenseContact.Name));
            parameters.Add(new MySqlParameter("@org_name", gameLicenseContact.OrgName));
            parameters.Add(new MySqlParameter("@phone_number", gameLicenseContact.PhoneNumber));
            parameters.Add(new MySqlParameter("@email", gameLicenseContact.Email));
            parameters.Add(new MySqlParameter("@about_game", gameLicenseContact.AboutGame));
            parameters.Add(new MySqlParameter("@game_license_id", gameLicenseContact.GameLicenseId));
            return Db.Developer.ExecuteIdentityInsert(sqlString, parameters);
        }

    }
}
