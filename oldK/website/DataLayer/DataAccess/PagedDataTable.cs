///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;

namespace Kaneva.DataLayer.DataObjects
{
	/// <summary>
	/// Summary description for PagedDataTable.
	/// </summary>
	public class PagedDataTable : DataTable
	{
		public PagedDataTable()
		{
		}

		/// <summary>
		/// The number of items in the datasource
		/// </summary>
		public UInt32 TotalCount
		{
			get 
			{
				return m_TotalCount;
			} 
			set
			{
				m_TotalCount = value;
			}
		}

        private UInt32 m_TotalCount = 0;

	}
}
