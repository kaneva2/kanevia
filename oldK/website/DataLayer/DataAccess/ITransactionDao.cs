///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface ITransactionDao
    {
        PagedList<FameTransaction> GetFameTransactionsByUser (int userId, string gender, string filter, string orderby, int pageNumber, int pageSize);
        PagedList<RewardTransaction> GetRewardTransactionsByUser (int userId, string filter, string orderby, int pageNumber, int pageSize);
        PagedList<PurchaseTransaction> GetPurchasesTransactionsByUser (int userId, string filter, string orderby, int pageNumber, int pageSize);
        PagedList<PurchaseTransaction> GetPurchasesRaveTransactionsByUser (int userId, string filter, string orderby, int pageNumber, int pageSize);
        PagedList<TradeTransaction> GetTradeTransactionsByUser (int userId, string orderby, int pageNumber, int pageSize);
        PagedList<GiftTransaction> GetGiftTransactionsByUser (int userId, string filter, string orderby, int pageNumber, int pageSize);
        PagedList<RaveTransaction> GetAssetRaveTransactionsByUser (int userId, bool isUserTheRaveGiver, string filter, string orderby, int pageNumber, int pageSize);
        PagedList<RaveTransaction> GetCommunityRaveTransactionsByUser(int userId, bool isUserTheRaveGiver, string filter1, string filter2, string orderby, int pageNumber, int pageSize);
        PagedList<RaveTransaction> Get3DHomeRaveTransactionsByUser (int userId, bool isUserTheRaveGiver, string filter, string orderby, int pageNumber, int pageSize);
        PagedList<RaveTransaction> GetUGCRaveTransactionsByUser (int userId, bool isUserTheRaveGiver, string filter, string orderby, int pageNumber, int pageSize);
        PagedList<RaveTransaction> GetCommunityRaveTransactions (int communityId, string filter1, string filter2, string orderby, int pageNumber, int pageSize);
        PagedList<PurchaseTransactionPremiumItem> GetPurchaseTransactionsByGame (int communityId, string filter, string orderby, int pageNumber, int pageSize);

        int GetTotalTransactionAmountByGame (int gameId);
        PremiumItemRedeemAmounts GetTotalTransactionCreditAmountsByGame (int communityId, int daysPendingInterval);
        PremiumItemRedeemAmounts GetTotalTransactionCreditAmountsByUser(int userId, int daysPendingInterval);
        int UpdateKanevaRewardEvent(int eventId, string eventName, int transactionTypeId);
        int AddKanevaRewardEvent(string eventName, int transactionTypeId);
        int RewardCredits(double amount, string keiPointId, int rewardEventId, int transTypeId, DateTime startDate, DateTime endDate);
        int RewardCreditsToMultipleUsers(string userIdList, string userEmailList, double amount, int transTypeId, string keiPointId, int rewardEventId, ref string failedUserIdList);
        //data tables can be converted to List later if so desired. Not done here for time constraints
        DataTable GetKanevaEvents();
        DataTable GetTransactionTypes(string filter, string orderBy);
        DataTable GetKanevaEventsByTransactionType(int transactionTypeId);
        PagedDataTable GetSalesOrderTransactions(DateTime dtStartDate, DateTime dtEndDate, string filter, string orderby, int pageNumber, int pageSize);

        //transaction functions for superrwards
        int LogSuperRewardsTransaction(SuperRewardsTransaction swTrans);
        int SuperRewardsCreditAlreadyGiven(uint srTransactionId);

        Currency GetConversionRateToDollar (string currencyType);
        int RedeemPremiumItemCredits (int communityId, int daysPendingBeforeRedeem);
    }
}
