///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IEventDao
    {
        int InsertEvent (Event evt, string trackingRequestGUID);
        int UpdateEvent (Event evt);
        int InsertEvent(int communityId, int userId, string title, string details, string location, int zoneIndex, int zoneInstanceId, DateTime startTime, DateTime endTime, int typeId, int timeZoneId, int premium, int privacy, bool isAPEvent, string trackingRequestGUID);
        int UpdateEvent (int eventId, int communityId, string title, string details, string location,
          DateTime startTime, DateTime endTime, int typeId, int privacy, int premium, int timeZoneId, bool isAPEvent);
        LocationDetail GetLocationDetail (int zone_index, int zone_instance_id, int creator_id, int community_id);
        PagedDataTable GetUserCreatedEvents(int userId, int startTime, string orderby, int pageNumber, int pageSize);
        Event GetEvent (int eventId);
        PagedList<EventReminder> GetEventReminders(string orderBy, int pageNumber, int pageSize);
        int LogEventReminderNotification(int eventId, int reminderId, DateTime sentDate);
        PagedList<Event> GetEventsByTimestampOffset(DateTime currentTime, int minuteOffset, string orderBy, int pageNumber, int pageSize);
        PagedList<Event> GetWorldEvents (int communityId, int startTimeFilter, DateTime startTime, string orderBy, int pageNumber, int pageSize, int recurringEventParentId);
        PagedList<Event> GetEvents (string filter, string orderBy, int pageNumber, int pageSize);
        int InsertEventInvitees (int communityId, int userId, int eventId, int inviteStatus, bool includeFriends);
        PagedList<EventInvitee> GetEventInviteesByStatus (int eventId, int inviteStatusId, bool includeEventOwner, string orderBy, int pageNumber, int pageSize);
        PagedList<EventInvitee> GetAutomatedEventInvitees(string orderBy, int pageNumber, int pageSize);
        int InsertEventAudit (int eventId, int userId, string desc);
        int UpdateEventStatus (int eventId, int statusId);
        EventInvitee GetEventInvitee (int userId, int eventId);
        int InsertEventInvitee (int eventId, int userId, int eventStatusId);
        int InsertComment (int eventId, string eventBlastId, int userId, string comment);
        int InsertEventBlast (int eventId, int userId, string comment);
        EventBlast GetEventBlast (string eventBlastId);
        PagedList<EventBlast> GetEventBlasts (int eventId, int pageNumber, int pageSize, string sortBy);
        PagedList<EventComment> GetEventComments (int eventId, string eventBlastId, int pageNumber, int pageSize, string sortBy, string groupBy);
        int DeleteEventComment (int eventId, string eventBlastId, string eventBlastCommentId);
        int UpdateEventInviteStatus (int eventId, int inviteeUserId, int inviteStatus);
        int UpdateEventInviteeAttendance(int eventId, int inviteeUserId, int inviteStatus);
        bool IsUserAlreadyRewardedToday(int userId);
        int InsertEventReward (int userId,int eventId,double rewardAmount);
        bool DoesCommunityHaveActiveEvent(int communityId);
        EventInviteCounts GetEventInviteCounts (int eventId);
        DataTable GetEventsUserAcceptedByStartTimeOffset(int userId, string startTimeOffset);
        List<Event> GetWorldEventsWithEventFlags(int communityId, string orderBy);
        Event GetEventByEventFlagPlacementId(int objPlacementId);
    }
}
