///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface ISGCustomDataDao
    {
        // Create
        bool ModifyDefaultSGCustomDataItem(SGCustomDataItem sgCustomDataItem, bool updateTemplates = false);
        bool AddSGCustomDataToCustomDeed(int zoneInstanceId, int zoneType, int templateGlobalId);

        // Read
        PagedList<SGCustomDataItem> GetDefaultSGCustomData(string orderBy, int pageNumber, int pageSize);
        PagedList<SGCustomDataItem> GetWorldSGCustomData(int zoneInstanceId, int zoneType, string filter, string orderBy, int pageNumber, int pageSize);
        SGCustomDataItem GetDefaultSGCustomDataItem(string attribute);

        // Update
        bool AddSGCustomDataToWorld(int zoneInstanceId, int zoneType, string attribute, string value);

        // Delete
        bool DeleteDefaultSGCustomDataItem(SGCustomDataItem sgCustomDataItem, bool removeFromTemplates = false);
        bool DeleteSGCustomDataFromCustomDeed(int deedTemplateId);

    }
}
