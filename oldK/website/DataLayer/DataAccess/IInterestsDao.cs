///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IInterestsDao
    {
        IList<InterestCategory> GetInterestCategories();
        DataTable GetCommonInterests(int userId, int userId2);
        DataTable GetUserInterests(int userId, string filter, string orderBy);
        DataTable GetInterestByCategory(int userId, int icId);
        DataTable GetCommonInterestsByCategory(int userId, int userId2, int icId);
        DataTable GetUserInterestsByCategory(int userId, int icId);
        DataTable GetCommonSchools(int userId, int userId2);
        DataTable GetUserSchools(int userId, string filter, string orderBy);
        int RemoveInterestFromAllUsers(int interestId);
        int AssignInterestToUser(int userId, int interestId, int icId);
        int UpdateUsersSearchableInterests(int userId);
        int AddInterestsToInterestCount(int interestId);
        int AddToSchoolCount(int schoolId);
        int AssociateSchoolToUser(int userId, int schoolId);
        DataTable GetInterests(int userId);
        int RemoveInterestFromUser(int userId, int interestId);
        int RemoveFromInterestCount(int interestId);
        int AddInterest(int icId, string interest);
        int InterestExists(int icId, string interest);
        int SchoolExists(string name);
        int AddSchool(string name, string city, string state);
        int RemoveFromSchoolCount(int schoolId);
        int RemoveSchoolFromUser(int userId, int schoolId);
    }
}
