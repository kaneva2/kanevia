///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Kaneva.DataLayer.DataObjects
{
	/// <summary>
	/// Summary description for DatabaseUtility.
	/// </summary>
	public interface IDatabaseUtility
	{
		PagedDataTable GetPagedDataTable (string query, string orderByList, Hashtable theParams, int pageNumber, int pageSize);
		PagedDataTable GetPagedDataTable (string query, string orderByList, Hashtable theParams, int pageNumber, int pageSize, int retryCount );

		PagedDataTable GetPagedDataTableNPC(string query, string orderByList, Hashtable theParams, int pageNumber, int pageSize);

		DataRow GetDataRow (string sql);
        DataRow GetDataRow(string sql, Hashtable parameters);
		DataRow GetDataRow( string sql, Hashtable parameters, string failureSql );
		
		DataTable GetDataTable (string sql);
		DataTable GetDataTable (string sql, Hashtable parameters);

		DataSet GetDataSet (string sql);

		object ExecuteScalar (string sql);
        object ExecuteScalar(string sql, Hashtable parameters);

		int ExecuteNonQuery (string sql);
		int ExecuteNonQuery (string sql, Hashtable parameters);
        int ExecuteNonQuery(string sql, Hashtable parameters, int retryCount);

		int ExecuteIdentityInsert (string sql, ref int identity);
		int ExecuteIdentityInsert (string sql, Hashtable parameters, ref int identity);

		DateTime GetCurrentDateTime ();
		string GetCurrentDateFunction ();

		string GetDatePlusDays (int days);
		string GetDatePlusMonths (int months);
		string GetDatePlusYears (int years);
		string GetDatePlusMinutes (int minutes);

		string GetDatePlusDays (int days, string dateColumn);
		string GetDatePlusMonths (int months, string dateColumn);
		string GetDatePlusYears (int years, string dateColumn);
		string GetDatePlusMinutes (int minutes, string dateColumn);

		string GetLengthFunction ();

		string FormatDate (DateTime dtDate);
		string FormatDateTime (DateTime dtDate);

		string CleanText (string strText);

	}

	public struct dbParms
	{
		public enum dataTypes { BigInt = 0, Blob, Binary, Bit, Char, Date, DateTime, Decimal,
			 Double, Float, Image, Int, Money, NChar, NText, NVarChar, Real,
			 SmallDateTime, SmallInt, SmallMoney, Text, Time, Timestamp, TinyInt,
			 UniqueIdentifier, VarBinary, VarChar, Variant, Year }

		public dbParms(string itemValue, int itemLength, dataTypes itemType)
		{
			m_value = itemValue;
			m_length = itemLength;
			m_type = itemType;
		}

		public string theValue
		{
			get
			{
				return m_value;
			}
			set
			{
				m_value = value;
			}
		}

		public int theLength
		{
			get
			{
				return m_length;
			}
			set
			{
				m_length = value;
			}
		}

		public dataTypes theType
		{
			get
			{
				return m_type;
			}
			set
			{
				m_type = value;
			}
		}

		public override string ToString()
		{
			return ( String.Format("{0}, {1}, {2}", m_value, m_length, m_type));
		}

		private string m_value;
		private int m_length;
		private dataTypes m_type;
	}
}
