///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IKachingDao
    {
        DataTable GetLeaderboardDay (string sortOrder, int numResults);
        DataTable GetLeaderboardWeek (string sortOrder, int numResults);
        DataTable GetLeaderboardAllTime(string sortOrder, int numResults);
        DataTable GetUserStats(DateTime dtStartDate, DateTime dtEndDate, int userId);
    }
}
