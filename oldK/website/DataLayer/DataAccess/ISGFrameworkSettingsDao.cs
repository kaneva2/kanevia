///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface ISGFrameworkSettingsDao
    {
        // Create
        bool AddSGFrameworkSettingsToCustomDeed(int zoneInstanceId, int zoneType, int templateGlobalId);

        // Read
        SGFrameworkSettings GetWorldSGFrameworkSettings(int zoneInstanceId, int zoneType);
        SGFrameworkSettings GetDeedSGFrameworkSettings(int templateGlid);

        // Delete
        bool DeleteSGFrameworkSettingsFromCustomDeed(int deedTemplateId);

    }
}
