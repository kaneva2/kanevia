///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IBlastDao
    {
        int SendAdminBlast (UInt64 entryId, int userId, DateTime dtExpires, string subject, string blastText, string cssHightLight, string thumbnailPath, bool sticky);
        int InsertUserEvent(UInt64 entryId, string subject, string diaryText, int userId, int communityId, DateTime dtEventDate, string highlight_css, string eventName, string location);

		int SendFriendActiveBlast(BlastTypes blastType, int userId, string userName, string subject, string diaryText, string cssHightLight, string thumbnailPath, int recipientId);
		int SendFriendActiveBlast(UInt64 entryId, BlastTypes blastType, int userId, string userName, string subject, string diaryText, string cssHightLight, string thumbnailPath, int recipientId);
		int SendChannelActiveBlast(BlastTypes blastType, int userId, string userName, int communityId, string communityName, bool isCommunityPersonal, string subject, string diaryText, string cssHightLight, string thumbnailPath, int recipientId);
        int SendChannelActiveBlast (UInt64 entryId, BlastTypes blastType, int userId, string userName, int communityId, string communityName, bool isCommunityPersonal, string subject, string diaryText, string cssHightLight, string thumbnailPath, int recipientId);
        int SendFriendPassiveBlast (BlastTypes blastType, int userId, string userName, string subject, string diaryText);
        int SendFriendPassiveBlast (UInt64 entryId, BlastTypes blastType, int userId, string userName, string subject, string diaryText);
        
        Blast GetBlast(UInt64 entryId);
        Blast GetAdminBlast(UInt64 entryId);

        IList<Blast> GetAdminBlasts();
        List<Blast> GetPassiveBlasts(int userId, int pageSize, string userVisibilityFilter, int pageNumber);
        List<Blast> GetMyPassiveBlasts (int userId, int pageSize, string userVisibilityFilter, int pageNumber);
        PagedList<Blast> GetActiveBlasts(int userId, int currentUserId, int pageSize, int pageNumber);
        PagedList<Blast> GetFriendActiveBlasts (int userId, int pageSize, int pageNumber);
        PagedList<Blast> GetMyActiveBlasts (int userId, int pageSize, int pageNumber);
        PagedList<Blast> GetCommunityActiveBlasts (int communityId, int senderId, bool isPersonalCommunity, int pageSize, int pageNumber);
        
        IList<Blast> GetUpcomingEvents(int userId, int pageSize);
        IList<Blast> GetAdminEvents ();
        int InsertBlastReply(UInt64 entryId, int userId, string message, string ipAddress);
        PagedList<BlastReply> GetBlastReplies(UInt64 entryId, int pageNumber, int pageSize, string sortBy);

        int DeleteAdminBlast(UInt64 blastId);
        int DeleteBlast(UInt64 blastId);
        void IncrementBlastsSent(int userId);

        UInt64 GetNewEntryId();
        int SendReblast (int blastType, int userId, string userName, string subject, string diaryEntry,
            string cssHightlight, string thumbnailPath, uint originatorId, string originatorName);
    }
}
