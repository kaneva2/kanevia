///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IFameDao
    {
        FameType GetFameType(int fameTypeId);
        UserFame GetUserFame(int userId, int fameTypeId);
        PagedList<UserFame> GetUserFame(int fameTypeId, string filter, string orderby, int pageNumber, int pageSize);        
        
        UserFame GetDanceFame(int userId, int fameTypeId, string username, int playerId);
        Level GetDanceLevel(int levelId, int fameTypeId);

        Packet GetPacket (int packetId);
        PagedList<Packet> GetPackets(int fameTypeId, string filter, string orderby, string groupby, int pageNumber, int pageSize);

        Level GetLevel(int levelId);
        int SetLevel(int userId, int fameTypeId, int level);
        PagedList<Level> GetLevels(int fameTypeId, string filter, string orderby, int pageNumber, int pageSize);
        List<LevelAwards> GetLevelAwards (int levelId, string gender);
        List<LevelAwards> GetAllLevelAwards(int fameTypeId);

        PagedList<PacketHistory> GetPacketHistory(int userId, int fameTypeId, string filter, string orderby, int pageNumber, int pageSize);
        PagedList<LevelHistory> GetLevelHistory(int userId, int fameTypeId, string filter, string orderby, int pageNumber, int pageSize);
        PagedList<DailyHistory> GetDailyHistoryForUser(int userId, int packetId, int fameTypeId, string filter, string orderby, int pageNumber, int pageSize);
        OnetimeHistory GetOneTimeHistory(int userId, int packetId);

        void AwardFame(int packetId, int userId, ref int returnCode, ref int levelId);

        int GetChecklistIdByPacketId(int packetId);
        PagedList<Nudge> GetChecklist (int userId, int checklistId, string filter, string orderby, int pageNumber, int pageSize);
        bool IsChecklistCompleteLongWay(int userId, int checklistId);
        PagedList<Nudge> GetNudges (int userId, string filter, string orderby, int pageNumber, int pageSize);
        int InsertChecklistComplete(int userId, int checklistId);

        bool IsChecklistComplete (int userId, int checklistId);

        int InsertDanceToLevel10(int userId);

        // Max per day
        void UpdateDailyHistory (int userId, int packetId, int fameTypeId);

        int GetRewardInterval(int userId, int packetId);
        void UpdateRewardInterval(int userId, int packetId);
        int GetRewardIntervalDaily(int userId, int packetId);
        void UpdateRewardIntervalDaily(int userId, int packetId);

        // Dailys
        List<User> GetYesterdayWebLogins();
        List<User> GetYesterdayWOKLogins();
        List<User> GetYesterday10MinsInWorld();
        List<User> GetYesterday3HoursInWorld();
        List<User> GetYesterdayDanceLevel10();
        List<User> GetYesterdayVisitForum();
        List<User> GetYesterdayRead3Posts();
        List<User> GetYesterdayView3Media();

        DataRow GetFamePointsFromInvitingFriends (int userId);
        DataRow GetUserLastDailyLoginPacketRedemption (int userId);

        Checklist GetChecklist (int checklistId, int userId);
        List<ChecklistPacket> GetChecklistPackets (int userId, int checklistId, string orderBy);
        PagedList<Nudge> GetChecklistNudges(int userId, int checklistId);
    }
}
