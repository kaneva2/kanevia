///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.DataLayer.DataObjects
{
    /// <summary>
    /// Factory of factories. This class is a factory class that creates
    /// data-base specific factories which in turn create data acces objects.
    /// 
    /// GoF Design Patterns: Factory.
    /// </summary>
    /// <remarks>
    /// This is the abstract factory design pattern applied in a hierarchy
    /// in which there is a factory of factories.
    /// </remarks>
    public class DaoFactories
    {
        /// <summary>
        /// Gets a provider specific (i.e. database specific) factory 
        /// 
        /// GoF Design Pattern: Factory
        /// </summary>
        /// <param name="dataProvider">Database provider.</param>
        /// <returns>Data access object factory.</returns>
        public static DaoFactory GetFactory(string dataProvider)
        {
            // Return the requested DaoFactory
            switch (dataProvider)
            {
                //case "System.Data.OleDb": return new Access.AccessDaoFactory();
                case "System.Data.SqlClient": return new SqlServer.SqlServerDaoFactory();
                //case "System.Data.OracleClient": return new Oracle.OracleDaoFactory();
                //default: return new Access.AccessDaoFactory();

                // OLD Version
                case "MySQLDirect.NET Data Provider": return new MySQL.MySQLDaoFactory(); 

                //MySQLDirect.NET Data Provider
                case "Devart.Data.MySql": return new MySQL.MySQLDaoFactory(); 

                // Mock DAOs for unit testing
                case "Kaneva.DataLayer.DataObjects.Mock": return new Mock.MockDaoFactory();

                default: throw new Exception("Kaneva Does not support this database "+dataProvider);
            }
        }
    }
}
