///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IScriptGameItemDao
    {
        // Create
        bool InsertSnapshotScriptGameItems(ref List<ScriptGameItem> sgItems, int userId, bool addToInventory = false, bool itemsAreDefault = false);
        bool AddSnapshotScriptGameItemsToCustomDeed(IEnumerable<ScriptGameItem> sgItems, int templateGlobalId);

        // Read
        string GetSGIGlobalConfigValue(string propertyName);
        Dictionary<string, string> GetSGIGlobalConfigValues();
        PagedList<ScriptGameItem> GetInventoryScriptGameItems(int playerId, string inventoryType, string searchString, IList<ScriptGameItemCategory> itemTypes,
            string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false);
        ScriptGameItem GetSnapshotScriptGameItem(int gIGlid, bool includeProperties = false, bool includeBundledGlids = false);
        ScriptGameItem GetSnapshotScriptGameItem(int zoneInstanceId, int zoneType, int gameItemId, bool includeProperties = false, bool includeBundledGlids = false);
        string GetSuggestedNameSuffixNum(int playerId, string inventoryType, string name, IList<ScriptGameItemCategory> itemTypes);
        PagedList<ScriptGameItem> GetTemplateScriptGameItems(int templateId, string searchString, string[] itemTypes,
            string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false);
        PagedList<ScriptGameItem> GetDefaultScriptGameItems(string searchString, string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false);
        PagedList<ScriptGameItem> GetWorldScriptGameItems(int zoneInstanceId, int zoneType, string filter, string orderBy, int pageNumber, int pageSize, bool includeProperties = false);
        PagedList<ScriptGameItem> GetDeedScriptGameItems(int templateGlid, string searchString, string[] itemTypes, 
            string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false);
        int GetSuggestedGIGLID();
        int GetSuggestedBundleGlid();
        int GetSuggestedGIID();
        bool WorldHasScriptGameItems(int zoneIndex, int zoneInstanceId);
        bool IsTypeInventoryCompatible(string itemType);
        
        // Update
        bool UpdateSGIGlobalConfig(Dictionary<string, string> config);
        bool UpdateSnapshotScriptGameItem(ScriptGameItem sgItem, int userId);
        bool UpdateWorldGIGLIDs(int zoneInstanceId, int zoneType, List<ScriptGameItem> sgItems);

        // Delete
        bool DeleteSGIGlobalConfig(string propName);
        bool DeleteStartingScriptGameItem(ScriptGameItem sgItem, int templateId = 0);
        bool DeleteAllSGItemsInCustomDeed(int templateGlobalId);
    }
}
