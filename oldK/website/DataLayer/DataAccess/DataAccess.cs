///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Kaneva.DataLayer.DataObjects
{
    /// <summary>
    /// This class shields the client from the details of database specific 
    /// data-access objects. It returns the appropriate data-access objects 
    /// according to the configuration in web.config.
    /// 
    /// GoF Design Patterns: dpFactory1, Singleton, Proxy.
    /// </summary>
    /// <remarks>
    /// This class makes extensive use of the dpFactory1 pattern in determining which 
    /// database specific DAOs (Data Access Objects) to return.
    /// 
    /// This class is like a Singleton -- it is a static class (Shared in VB) and 
    /// therefore only one 'instance' ever will exist.
    /// 
    /// This class is a Proxy in that it 'stands in' for the actual Data Access Object dpFactory1.
    /// </remarks>
    public static class DataAccess
    {
        // The static field initializers below are thread safe.
        // Furthermore, they are executed in the order in which they appear
        // in the class declaration. Note: if a static constructor
        // is present you want to initialize these in that constructor.
        private static readonly string dataProvider1 = ConfigurationManager.AppSettings.Get("DataProvider");
        private static readonly DaoFactory dpFactory1 = DaoFactories.GetFactory(dataProvider1);

        private static readonly string dataProvider2 = ConfigurationManager.AppSettings.Get("DataProvider2");
        private static readonly DaoFactory dpFactory2 = DaoFactories.GetFactory(dataProvider2);

        #region MYSQL Dao Providers

        /// <summary>
        /// Gets a provider specific Violations data access object.
        /// </summary>
        public static IEventDao EventDao
        {
            get { return dpFactory1.EventDao; }
        }
        

        /// <summary>
        /// Gets a provider specific Violations data access object.
        /// </summary>
        public static IViolationsDao ViolationsDao
        {
            get { return dpFactory1.ViolationsDao; }
        }

        /// <summary>
        /// Gets a provider specific Development company data access object.
        /// </summary>
        public static IPromotionsDao PromotionsDao
        {
            get { return dpFactory1.PromotionsDao; }
        }

        /// <summary>
        /// Gets a provider specific Development company data access object.
        /// </summary>
        public static IDevelopmentCompanyDao DevelopmentCompanyDao
        {
            get { return dpFactory1.DevelopmentCompanyDao; }
        }

        /// <summary>
        /// Gets a provider specific Game Developer data access object.
        /// </summary>
        public static IGameDeveloperDao GameDeveloperDao
        {
            get { return dpFactory1.GameDeveloperDao; }
        }

        /// <summary>
        /// Gets a provider specific User data access object.
        /// </summary>
        public static IUserDao UserDao
        {
            get { return dpFactory1.UserDao; }
        }

        /// <summary>
        /// Gets a provider specific site management data access object.
        /// </summary>
        public static ISiteManagementDao SiteManagementDao
        {
            get { return dpFactory1.SiteManagementDao; }
        }

        /// <summary>
        /// Gets a provider specific interests data access object.
        /// </summary>
        public static IInterestsDao InterestsDao
        {
            get { return dpFactory1.InterestsDao; }
        }

        /// <summary>
        /// Gets a provider specific site security data access object.
        /// </summary>
        public static ISiteSecurityDao SiteSecurityDao
        {
            get { return dpFactory1.SiteSecurityDao; }
        }

        /// <summary>
        /// Gets a provider specific Game data access object.
        /// </summary>
        public static IGameDao GameDao
        {
            get { return dpFactory1.GameDao; }
        }

        /// <summary>
        /// Gets a provider specific User data access object.
        /// </summary>
        public static IGameServerDao GameServerDao
        {
            get { return dpFactory1.GameServerDao; }
        }

        /// <summary>
        /// Gets a provider specific User data access object.
        /// </summary>
        public static IGameLicenseDao GameLicenseDao
        {
            get { return dpFactory1.GameLicenseDao; }
        }

        /// <summary>
        /// Gets a provider specific User data access object.
        /// </summary>
        public static IWorldTemplateDao WorldTemplateDao
        {
            get { return dpFactory1.WorldTemplateDao; }
        }

        /// <summary>
        /// Gets a provider specific UserStats data access object.
        /// </summary>
        public static ICommentsDao CommentsDao
        {
            get { return dpFactory1.CommentsDao; }
        }

        /// <summary>
        /// Gets a provider specific Blast data access object.
        /// </summary>
        public static IBlastDao BlastDao
        {
            get { return dpFactory1.BlastDao; }
        }

        /// <summary>
        /// Gets a provider specific Community data access object.
        /// </summary>
        public static ICommunityDao CommunityDao
        {
            get { return dpFactory1.CommunityDao; }
        }

        /// <summary>
        /// Gets a provider specific Contest data access object.
        /// </summary>
        public static IContestDao ContestDao
        {
            get { return dpFactory1.ContestDao; }
        }

        /// <summary>
        /// Gets a provider specific Blog data access object.
        /// </summary>
        public static IBlogDao BlogDao
        {
            get { return dpFactory1.BlogDao; }
        }

        /// <summary>
        /// Gets a provider specific Forum data access object.
        /// </summary>
        public static IForumDao ForumDao
        {
            get { return dpFactory1.ForumDao; }
        }

        /// <summary>
        /// Gets a provider specific Fame data access object.
        /// </summary>
        public static IFameDao FameDao
        {
            get { return dpFactory1.FameDao; }
        }

        /// <summary>
        /// Gets a provider specific ErrorLogging data access object.
        /// </summary>
        public static IErrorLoggingDao ErrorLoggingDao
        {
            get { return dpFactory1.ErrorLoggingDao; }
        }


        /// <summary>
        /// Gets a provider specific ErrorLogging data access object.
        /// </summary>
        public static IMetricsDao MetricsDao
        {
            get { return dpFactory1.MetricsDao; }
        }



        /// <summary>
        /// Gets a provider specific Media data access object.
        /// </summary>
        public static IMediaDao MediaDao
        {
            get { return dpFactory1.MediaDao; }
        }

        /// <summary>
        /// Gets a provider specific Media data access object.
        /// </summary>
        public static IShoppingDao ShoppingDao
        {
            get { return dpFactory1.ShoppingDao; }
        }

        /// <summary>
        /// Gets a provider specific ScriptGameItem data access object.
        /// </summary>
        public static IScriptGameItemDao ScriptGameItemDao
        {
            get { return dpFactory1.ScriptGameItemDao; }
        }

        /// <summary>
        /// Gets a provider specific ScriptGameItem data access object.
        /// </summary>
        public static ISGCustomDataDao SGCustomDataDao
        {
            get { return dpFactory1.SGCustomDataDao; }
        }

        /// <summary>
        /// Gets a provider specific ScriptGameItem data access object.
        /// </summary>
        public static ISGFrameworkSettingsDao SGFrameworkSettingsDao
        {
            get { return dpFactory1.SGFrameworkSettingsDao; }
        }

        /// <summary>
        /// Gets a provider specific Kaching data access object.
        /// </summary>
        public static IKachingDao KachingDao
        {
            get { return dpFactory1.KachingDao; }
        }

        /// <summary>
        /// Gets a provider specific Marketing data access object.
        /// </summary>
        public static IMarketingDao MarketingDao
        {
            get { return dpFactory1.MarketingDao; }
        } 
    

        /// <summary>
        /// Gets a provider specific Transaction data access object.
        /// </summary>
        public static ITransactionDao TransactionDao
        {
            get { return dpFactory1.TransactionDao; }
        }

        /// <summary>
        /// Gets a provider specific Subscription data access object.
        /// </summary>
        public static ISubscriptionDao SubscriptionDao
        {
            get { return dpFactory1.SubscriptionDao; }
        }

        /// <summary>
        /// Gets a provider specific Subscription data access object.
        /// </summary>
        public static IRaveDao RaveDao
        {
            get { return dpFactory1.RaveDao; }
        }

        /// <summary>
        /// Gets a provider specific Configuration data access object.
        /// </summary>
        public static IConfigurationDao ConfigurationDao
        {
            get { return dpFactory1.ConfigurationDao; }
        }

		/// <summary>
		/// Gets a provider specific Mail data access object.
		/// </summary>
		public static IMailDao MailDao
		{
			get { return dpFactory1.MailDao; }
		}

        /// <summary>
        /// Gets a provider specific Sitemap data access object.
        /// </summary>
        public static ISitemapDao SitemapDao
        {
            get { return dpFactory1.SitemapDao; }
        }

        /// <summary>
        /// Gets a provider specific Experiment data access object.
        /// </summary>
        public static IExperimentDao ExperimentDao
        {
            get { return dpFactory1.ExperimentDao; }
        }

        #endregion

        #region SQL Server Dao Providers

        /// <summary>
        /// Gets a provider specific Transaction data access object for SQL Server.
        /// </summary>
        public static ITransactionDao TransactionDao_DP2
        {
            get { return dpFactory2.TransactionDao; }
        }

        /// <summary>
        /// Gets a provider specific User data access object for SQL Server.
        /// </summary>
        public static IUserDao UserDao_DP2
        {
            get { return dpFactory2.UserDao; }
        }

        #endregion


    }
}
