///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Configuration;

namespace Kaneva.DataLayer.DataObjects
{
    /// <summary>
    /// Class that manages all lower level ADO.NET data base access.
    /// 
    /// GoF Design Patterns: Singleton
    /// </summary>
    /// <remarks>
    /// 
    /// This class is like a Singleton -- it is a static class and 
    /// therefore only one 'instance' ever will exist.
    /// </remarks>
    sealed class Db
    {
        // Note: constructor is 'private'
        private Db()
        {

        }

        //-- MYSQL Database servers 
        public static DbServer Master
        {
            get { return _Master; }
        }

        public static DbServer Metrics
        {
            get { return _Metrics; }
        }

        public static DbServer Slave
        {
            get { return _Slave; }
        }

        public static DbServer Slave2
        {
            get { return _Slave2; }
        }

        public static DbServer Search
        {
            get { return _Search; }
        }

        public static DbServer SearchMaster
        {
            get { return _SearchMaster; }
        }

        public static DbServer Diary
        {
            get { return _Diary; }
        }

        public static DbServer DiaryMaster
        {
            get { return _DiaryMaster; }
        }

        public static DbServer Stats
        {
            get { return _Stats; }
        }

        public static DbServer WOK
        {
            get { return _WOK; }
        }

        public static DbServer Shard
        {
            get { return _Shard; }
        }

        public static DbServer Fame
        {
            get { return _Fame; }
        }

        public static DbServer Developer
        {
            get { return _Developer; }
        }

        public static DbServer SiteManagement
        {
            get { return _SiteManagement; }
        }

        public static DbServer Shopping
        {
            get { return _Shopping; }
        }

        public static DbServer Kaching
        {
            get { return _Kaching; }
        }

        public static DbServer Audit
        {
            get { return _Audit; }
        }

		public static DbServer EmailViral
		{
			get { return _EmailViral; }
		}


        //-- SQL Server Database servers 
        public static DbServer MasterSQLSvr
        {
            get { return _MasterSQLsvr; }
        }

        /// <summary>
        /// Retrieves database connection string from Web.Config file.
        /// </summary>
        private static string ConnectionString (string dbName)
        {
            // Handle missing configs
            if (System.Configuration.ConfigurationManager.AppSettings[dbName] == null)
            {
                dbName = "ConnectionString";
            }

            if (System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"].ToUpper().Equals("TRUE"))
            {
                // Encrypt (System.Configuration.ConfigurationManager.AppSettings ["ConnectionString"]);
                return Decrypt(System.Configuration.ConfigurationManager.AppSettings[dbName]);
            }
            else
            {
                return System.Configuration.ConfigurationManager.AppSettings[dbName];
            }
        }

        /// <summary>
        /// Decrypt
        /// </summary>
        /// <param name="encryptedText"></param>
        /// <returns></returns>
        private static string Decrypt(string encryptedText)
        {
            TripleDESCryptoServiceProvider tds = new TripleDESCryptoServiceProvider();
            string result = "";

            string key = "jb7431%o$#=@tp+&";
            PasswordDeriveBytes pdbkey = new PasswordDeriveBytes(key, ASCIIEncoding.ASCII.GetBytes(String.Empty));
            byte[] keyByte = pdbkey.GetBytes(key.Length);

            // Decrypt it
            // Convert from base 64 string to bytes
            byte[] cryptoByte = Convert.FromBase64String(encryptedText);

            // Set private key
            tds.Key = keyByte;
            tds.IV = new byte[] { 0xf, 0x6f, 0x13, 0x2e, 0x35, 0xc2, 0xcd, 0xf9 };

            // Decryptor object
            ICryptoTransform cryptoTransform = tds.CreateDecryptor();
            MemoryStream msd = null;
            CryptoStream csd = null;
            StreamReader sr = null;

            try
            {
                // Memory stream object
                msd = new MemoryStream(cryptoByte, 0, cryptoByte.Length);

                // Crypto stream object
                csd = new CryptoStream(msd, cryptoTransform, CryptoStreamMode.Read);

                // Get the result from the Crypto stream
                sr = new StreamReader(csd);
                result = sr.ReadToEnd();
            }
            catch (Exception) {}
            finally
            {
                cryptoTransform.Dispose();

                if (msd != null)
                {
                    msd.Close();
                }
                if (csd != null)
                {
                    csd.Close();
                }
                if (sr != null)
                {
                    sr.Close();
                }
            }

            return result;
        }

        private static readonly string DataProvider1 = ConfigurationManager.AppSettings.Get("DataProvider");
        private static readonly string DataProvider2 = ConfigurationManager.AppSettings.Get("DataProvider2");

        //-- MySQL data providers 
        private static readonly DbServer _Master = new DbServer("Master", ConnectionString("ConnectionString"), DataProvider1);
        private static readonly DbServer _Metrics = new DbServer("Metrics", ConnectionString("ConnectionStringMetrics"), DataProvider1);
        private static readonly DbServer _Slave = new DbServer("Slave", ConnectionString("ConnectionStringReadOnly"), DataProvider1);
        private static readonly DbServer _Slave2 = new DbServer("Slave2", ConnectionString("ConnectionStringReadOnly2"), DataProvider1);
        private static readonly DbServer _Search = new DbServer("Search", ConnectionString("ConnectionStringSearch"), DataProvider1);
        private static readonly DbServer _SearchMaster = new DbServer("Search Master", ConnectionString("ConnectionStringMasterSearch"), DataProvider1);
        private static readonly DbServer _Diary = new DbServer("Diary", ConnectionString("ConnectionStringDiary"), DataProvider1);
        private static readonly DbServer _DiaryMaster = new DbServer("Diary Master", ConnectionString("ConnectionStringMasterDiary"), DataProvider1);
        private static readonly DbServer _Stats = new DbServer("Stats", ConnectionString("ConnectionStringStats"), DataProvider1);
        private static readonly DbServer _WOK = new DbServer("WOK", ConnectionString("ConnectionStringKGP"), DataProvider1);
        private static readonly DbServer _Shard = new DbServer("Shard", ConnectionString("ConnectionStringShard"), DataProvider1);
        private static readonly DbServer _Fame = new DbServer("Fame", ConnectionString("ConnectionStringFame"), DataProvider1);
        private static readonly DbServer _Developer = new DbServer("Developer", ConnectionString("ConnectionStringDeveloper"), DataProvider1);
        private static readonly DbServer _SiteManagement = new DbServer("SiteManagement", ConnectionString("ConnectionStringSiteManagement"), DataProvider1);
        private static readonly DbServer _Shopping = new DbServer("Shopping", ConnectionString("ConnectionStringShopping"), DataProvider1);
        private static readonly DbServer _Kaching = new DbServer("Kaching", ConnectionString("ConnectionStringKaching"), DataProvider1);
        private static readonly DbServer _Audit = new DbServer("Audit", ConnectionString("ConnectionStringAudit"), DataProvider1);
		private static readonly DbServer _EmailViral = new DbServer("EmailViral", ConnectionString("ConnectionStringEmailViral"), DataProvider1);

        //-- SQL Server data providers 
        private static readonly DbServer _MasterSQLsvr = new DbServer("Master_SQLSrv", ConnectionString("ConnectionString_SQLSrv"), DataProvider2);
        //private static readonly DbServer _SlaveSQLsvr = new DbServer("Slave", ConnectionString("ConnectionStringReadOnly"), sqlSvrDataProvider);
        //private static readonly DbServer _Slave2SQLsvr = new DbServer("Slave2", ConnectionString("ConnectionStringReadOnly2"), sqlSvrDataProvider);
        //private static readonly DbServer _SearchSQLsvr = new DbServer("Search", ConnectionString("ConnectionStringSearch"), sqlSvrDataProvider);
        //private static readonly DbServer _SearchMasterSQLsvr = new DbServer("Search Master", ConnectionString("ConnectionStringMasterSearch"), sqlSvrDataProvider);
        //private static readonly DbServer _DiarySQLsvr = new DbServer("Diary", ConnectionString("ConnectionStringDiary"), sqlSvrDataProvider);
        //private static readonly DbServer _DiaryMasterSQLsvr = new DbServer("Diary Master", ConnectionString("ConnectionStringMasterDiary"), sqlSvrDataProvider);
        //private static readonly DbServer _StatsSQLsvr = new DbServer("Stats", ConnectionString("ConnectionStringStats"), sqlSvrDataProvider);
        //private static readonly DbServer _WOKSQLsvr = new DbServer("WOK", ConnectionString("ConnectionStringKGP"), sqlSvrDataProvider);
        //private static readonly DbServer _ShardSQLsvr = new DbServer("Shard", ConnectionString("ConnectionStringShard"), sqlSvrDataProvider);
        //private static readonly DbServer _FameSQLsvr = new DbServer("Fame", ConnectionString("ConnectionStringFame"), sqlSvrDataProvider);
        //private static readonly DbServer _DeveloperSQLsvr = new DbServer("Developer", ConnectionString("ConnectionStringDeveloper"), sqlSvrDataProvider);
        //private static readonly DbServer _SiteManagementSQLsvr = new DbServer("SiteManagement", ConnectionString("ConnectionStringSiteManagement"), sqlSvrDataProvider);
        //private static readonly DbServer _ShoppingSQLsvr = new DbServer("Shopping", ConnectionString("ConnectionStringShopping"), sqlSvrDataProvider);
        //private static readonly DbServer _KachingSQLsvr = new DbServer("Kaching", ConnectionString("ConnectionStringKaching"), sqlSvrDataProvider);
        //private static readonly DbServer _AuditSQLsvr = new DbServer("Audit", ConnectionString("ConnectionStringAudit"), sqlSvrDataProvider);  

    }
}
