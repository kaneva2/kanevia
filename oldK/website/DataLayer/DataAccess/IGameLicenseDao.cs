///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IGameLicenseDao
    {
        //--select game functions---
        GameLicense GetGameLicense(int gameLicenseId);
        GameLicense GetGameLicenseByGameId(int gameId);
        GameLicense GetGameLicenseByGameKey(string gameKey);

        IList<GameLicense> GetAllLicenses(string filter, string orderby);
        IList<GameLicense> GetLicenseByGameId(int gameId, string filter, string orderby);

        int AddNewGameLicense(GameLicense license, int daysLeft);
        int UpdateGameLicense(GameLicense license);
        int DeleteGameLicense(int licenseId);

        GameLicenseSubscription GetGameLicenseType(int license_subscription_id);
        List<GameLicenseSubscription> GetGameLicenseTypes();
        DataTable GetGameLicenseStatus();

        GameSubscription GetGameSubscription(int SubscriptionId);
        GameSubscription GetGameSubscriptionByOrderId(int orderId);
        PagedList<GameSubscription> GetGameSubscriptions(int GameId, string orderby, int pageNumber, int pageSize);
        int InsertGameSubscription(GameSubscription gameSubscription);
        void UpdateGameSubscription(GameSubscription gameSubscription);
        int InsertGameSubscriptionOrder(int subscriptionId, int orderId);

        GameLicenseContact GetGameLicenseContact(int gameLicenseId);
        int InsertGameLicenseContact(GameLicenseContact gameLicenseContact);
    }
}
