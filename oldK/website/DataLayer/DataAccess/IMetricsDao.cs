///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Data;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IMetricsDao
    {
        #region Counter Data

        int InsertMetricsCounterData_v0(string counterName
                                            , double? a1
                                            , double? a2
                                            , double? b1
                                            , double? b2);

        int InsertMetricsCounterData_v1(string counterName
                                          , string k1
                                          , string k2
                                          , string k3
                                          , string k4
                                          , double? a1
                                          , double? a2
                                          , double? b1
                                          , double? b2);

        #endregion

        #region Website Performance

        int InsertMetricsPageTiming(string name, double timeInMS);

        #endregion

        # region Client Performance

        bool ClientRuntimeExists(string runtimeId);

        int InsertClientMtbfLog(int userId, double totalRuntime, uint totalRuns, uint totalCrashes, string appVersion, string runtimeId);

        string GetClientRuntimeSystemId(string runtimeId);

        string InsertClientRuntimePerformanceLog(string runtimeId, int userId, string systemConfigurationId, string appVersion, uint? testGroup, ulong? diskAvail, double? pingMs, uint? ripperDetected);

        int InsertClientRuntimeIP(string runtimeId, string ipAddress);

        int UpdateClientRuntimePerformanceLog(string runtimeId, int userId, double? appCpuTime, ulong? appMemNum, ulong? appMemAvg, ulong? appMemMin, ulong? appMemMax,
          ulong? texMemNum, ulong? texMemAvg, ulong? texMemMin, ulong? texMemMax, double? fps, string shutDownType, uint? ripperDetected, double? msgMoveEnabledTimeSec);

        int UpdateClientRuntimePerformanceLogUserId(string runtimeId, int userId);

        int InsertClientWebCallPerformanceLog(string runtimeId, string webCallType, uint webCalls, double webCallsMs, double webCallsResponseMs, uint webCallsBytes, uint webCallsErrored);

        int DeleteClientWebCallPerformanceLog(string runtimeId, string webCallType);

        int InsertClientZonePerformanceLog(string runtimeId, uint gameId, uint zoneIndex, uint zoneInstanceId, uint zoneType, double? pingMs, double? fps,
            double? zoneTimeMs, double? avgDo, double? minDo, double? maxDo, double? avgPoly, double? minPoly, double? maxPoly, uint mediaCount, uint texMem);

        #endregion

        # region Launcher Reports

        bool LauncherRuntimeExists(ulong runtimeId);

        int InsertLauncherMtbfLog(double totalRuntime, ushort totalRuns, ushort totalCrashes, ulong runtimeId);

        ulong SaveLauncherRuntimePerformanceLog(ulong runtimeId, string runtimeState, string systemConfigurationId, string appVersion, ulong? diskAvail, double? appCpuTime, uint? appMemNum,
            ulong? appMemAvg, ulong? appMemMin, ulong? appMemMax, ushort? patchRv);

        int InsertLauncherRuntimeIP(ulong runtimeId, string ipAddress);

        int DeleteLauncherRuntimeIP(ulong runtimeId);

        int InsertLauncherPatchPerformanceLog(ulong runtimeId, string patchUrl, double? patchMs, ushort dl_NumOk, ushort dl_NumErr, double dl_Ms, uint dl_Bytes,
          ushort dl_Waits, ushort dl_Retries, ushort dl_RetriesErr,
          ushort zip_NumOk, ushort zip_NumErr, double zip_Ms, uint zip_Bytes,
          ushort pak_NumOk, ushort pak_NumErr, double pak_Ms, uint pak_Bytes);

        int DeleteLauncherPatchPerformanceLog(ulong runtimeId);

        int InsertLauncherWebCallPerformanceLog(ulong runtimeId, string webCallType, uint webCalls, double webCallsMs, double webCallsResponseMs, uint webCallsBytes, uint webCallsErrored);

        int DeleteLauncherWebCallPerformanceLog(ulong runtimeId);

        #endregion

        #region Client Version History

        int InsertClientVersionHistory(string clientVersion, DateTime releaseDate, DateTime deprecatedDate, string comment);
        int UpdateClientVersionHistory(string clientVersion, DateTime releaseDate, DateTime deprecatedDate, string comment);
        int DeleteClientVersionHistory(string clientVersion);
        PagedDataTable GetClientVersionHistory(int page, int itemsPerPage, string clientVersion = "");

        #endregion

        #region Game Item Metrics

        int InsertScriptGameItemPlacementLog(int zoneInstanceId, int zoneType, string username, string gameplayMode, int gameItemId, int gameItemGlid, string itemType, string behavior);
        int InsertScriptGameItemLootingLog(int zoneInstanceId, int zoneType, string username, string lootSource, int gameItemId, int gameItemGlid, string itemType, string behavior);
        int InsertFrameworkActivationLog(int zoneInstanceId, int zoneType, string username, string activationAction);
        int RecordNewUserFunnelProgression(int userId, string funnelStep);
        int ResetNewUserFunnelProgression(int userId);

        #endregion
    }
}
