///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace Kaneva.DataLayer.DataObjects
{
    /// <summary>
    /// Class that manages all cached data base access.
    /// 
    /// GoF Design Patterns: Singleton
    /// </summary>
    /// <remarks>
    /// 
    /// This class is like a Singleton -- it is a static class and 
    /// therefore only one 'instance' ever will exist.
    /// </remarks>
    sealed class DbCache
    {
        // Note: constructor is 'private'
        private DbCache ()
        {
        }

        public static System.Web.Caching.Cache Cache
        {
            get { return HttpRuntime.Cache; }
        }

        private static readonly HttpRuntime httpRT = new HttpRuntime();
    }
}
