///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Devart.Data.MySql;

using log4net;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface ISitemapDao
    {
        PagedList<string> GetCommunities (int communityTypeId, string filter, string orderBy, int pageNumber, int pageSize);
        PagedList<string> GetUsers (string filter, string orderby, int pageNumber, int pageSize);
        PagedList<Int32> GetAssets (string filter, string orderby, int pageNumber, int pageSize);
        PagedList<Int32> GetShopItems (string filter, string orderby, int pageNumber, int pageSize);
    }
}
