///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IForumDao
    {
        Forum GetForum(int forumId);
        IList<Forum> GetForums(int communityId);
        PagedList<ForumTopic> GetLatestForumTopics(int communityId, int pageNumber, int pageSize);
        int InsertForumCategory(int communityId, string name, string description);
        int InsertForum(int communityId, string forumName, string description, int forumCategoryId);
        int GetForumCount(int communityId);
    }
}
