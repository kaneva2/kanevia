///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IExperimentDao
    {
        // Create / Update
        bool SaveExperiment(Experiment experiment, bool propagateTransactionExceptions = false);
        bool SaveExperimentCategory(ExperimentCategory category, bool propagateTransactionExceptions = false);
        bool SaveExperimentEffect(ExperimentEffect effect, bool propagateTransactionExceptions = false);
        bool SaveExperimentGroups(Experiment experiment, bool propagateTransactionExceptions = false);
        bool SaveExperimentGroupEffect(ExperimentGroup group, ExperimentEffect effect, bool propagateTransactionExceptions = false);
        bool SaveUserExperimentGroup(UserExperimentGroup participant, bool propagateTransactionExceptions = false);
        bool SaveWorldExperimentGroup(WorldExperimentGroup participant, bool propagateTransactionExceptions = false);
        bool SaveWorldTemplateExperimentGroup(WorldTemplateExperimentGroup participant, bool propagateTransactionExceptions = false);

        // Delete
        bool DeleteExperiment(Experiment experiment, bool propagateTransactionExceptions = false);
        bool DeleteExperimentCategory(ExperimentCategory category, bool propagateTransactionExceptions = false);
        bool DeleteExperimentEffect(ExperimentEffect effect, bool propagateTransactionExceptions = false);
        bool DeleteExperimentGroup(ExperimentGroup group, bool propagateTransactionExceptions = false);
        bool DeleteExperimentGroupEffect(ExperimentGroup group, ExperimentEffect effect, bool propagateTransactionExceptions = false);
        bool DeleteUserExperimentGroup(UserExperimentGroup participant, bool propagateTransactionExceptions = false);
        bool DeleteWorldExperimentGroup(WorldExperimentGroup participant, bool propagateTransactionExceptions = false);
        bool DeleteWorldTemplateExperimentGroup(WorldTemplateExperimentGroup participant, bool propagateTransactionExceptions = false);
        bool ClearWorldTemplateExperimentGroups(WorldTemplate template, bool propagateTransactionExceptions = false);

        // Read
        Experiment GetExperiment(string experimentId, bool includeExperimentGroups = true);
        PagedList<Experiment> SearchExperiments(string name, bool exactMatch, string orderBy, int pageNumber, int pageSize, bool includeExperimentGroups = true);
        PagedList<Experiment> SearchExperiments(string name, bool exactMatch, string category, string status, string orderBy, int pageNumber, int pageSize, bool includeExperimentGroups = true);
        IList<Experiment> GetAvailableExperimentsByUserContext(int userId, string context = "Request", bool includeExperimentGroups = true);
        ExperimentCategory GetExperimentCategory(string name);
        PagedList<ExperimentCategory> SearchExperimentCategories(string name, string sortBy, int pageNumber, int pageSize);
        PagedList<ExperimentEffect> GetExperimentEffects(string orderBy, int pageNumber, int pageSize);
        ExperimentGroup GetExperimentGroup(string experimentGroupId);
        PagedList<UserExperimentGroup> SearchUserExperimentGroups(string username, bool bExactMatch, string experimentId, string experimentStatus, string orderBy, int pageNum, int pageSize, bool includeExperimentGroups = true);
        IList<UserExperimentGroup> GetActiveUserExperimentGroups(int userId, bool includeExperimentGroups = true);
        UserExperimentGroup GetUserExperimentGroup(string userGroupId, bool includeExperimentGroups = true);
        UserExperimentGroup GetUserExperimentGroupByExperimentId(int userId, string experimentId, bool includeExperimentGroups = true);
        WorldExperimentGroup GetWorldExperimentGroup(int zoneInstanceId, int zoneType, string groupId, bool includeExperimentGroups = true);
        WorldExperimentGroup GetWorldExperimentGroupByExperimentId(int zoneInstanceId, int zoneType, string experimentId, bool includeExperimentGroups = true);
        IList<Experiment> GetAvailableExperimentsByWorldContext(int zoneInstanceId, int zoneType, string context = "Request", bool includeExperimentGroups = true);
        IList<WorldExperimentGroup> GetActiveWorldExperimentGroups(int zoneInstanceId, int zoneType, bool includeExperimentGroups = true);
        PagedList<WorldTemplateExperimentGroup> GetWorldTemplateExperimentGroups(WorldTemplate template, string orderBy, int pageNumber, int pageSize, bool includeExperimentGroups = true);
    }
}
