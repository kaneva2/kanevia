///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IBlogDao
    {
        Blog GetBlog(int blogId);
        PagedList<Blog> GetBlogs(int communityId, string filter, string orderby, int pageNumber, int pageSize);
        PagedList<Blog> GetAllUserBlogs(int userId, string filter, string orderby, int pageNumber, int pageSize);

        int InsertBlog(int communityId, string subject, string bodyText, int userId, string createdUsername, string ipAddress, string keywords);
        int UpdateBlog(int userId, int blogId, string subject, string bodyText, int statusId, string keywords);
        void DeleteBlog(int blogId, int userId, bool bPermanent);
        void DeleteBlog(int blogId, int userId);
        bool IsBlogOwner(int userId, int blogId);
        DataTable GetBlogKeywords(int channelId);

        int InsertBlogComment(int blogId, int userId, string bodyText, string ipAddress);
        int UpdateBlogComment(int blogCommentId, int userId, string bodyText, string ipAddress);
        BlogComment GetBlogComment(int blogCommentId);
        PagedList<BlogComment> GetBlogComments(int blogId, int pageNumber, int pageSize);
        PagedList<BlogComment> GetBlogCommentsByCommunity(int communityId, DateTime sinceDate, int pageNumber, int pageSize);
        void DeleteBlogComment(int blogCommentId, int userId);
    }
}
