///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IViolationsDao
    {
        PagedList<ViolationReport> GetAllViolationReportsJoined(string filter, string orderby, int pageNumber, int pageSize);
        PagedList<ViolationReport> GetAllViolationReports(string filter, string orderby, int pageNumber, int pageSize);
        PagedList<ViolationReport> GetAllViolationReportsGrouped(string filter, int pageNumber, int pageSize);
        IList<ViolationActionType> GetAllViolationActionTypes();
        IList<ViolationAction> GetAllViolationActions();
        IList<ViolationType> GetAllViolationTypes();
        DataTable GetViolationCounts(uint assetId, uint wokItemId);
        ViolationAction GetViolationAction(int violationTypeId);
        int AddNewViolationReport(ViolationReport report);
        int UpdateViolationReport(ViolationReport report);
        int DeleteViolationReport(ViolationReport report);
        ViolationReport GetViolationReportByReportId(ViolationReport report);
        ViolationReport GetViolationReportByTypeANDReporter(ViolationReport report);
        ViolationReport GetViolationReportByViolationType(ViolationReport report);
        int ProcessWokItemViolation(ViolationReport report);
        int ProcessAssetViolation(ViolationReport report);
    }
}
