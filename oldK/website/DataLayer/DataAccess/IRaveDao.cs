///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IRaveDao
    {
        // Communities
        int GetCommunityRaveCountByUser (int userId, int channelId);
        int InsertCommunityRave(int userId, int communityId, int numRaves);
        void AdjustCommunityRaveTotal (int communityId, int numRaves);
        DataRow GetCommunityRave (int userId, int channelId);
        int RaveCommunityAndAdjustStats(int ravingUserId, int communityId, int numRaves);
        
        // Assets
        int InsertAssetRave (int userId, int assetId);
        int GetAssetRaveCountByUser (int userId, int assetId);
        void AdjustAssetRaveTotal (int assetId, int numRaves);
        DataRow GetAssetRave (int userId, int assetId);

        // UGC
        void AdjustUGCRaveTotal (int globalId, int numRaves);
        int InsertUGCRave (int userId, int globalId);
        int GetUGCRaveCountByUser (int userId, int globalId);
        int GetUGCRaveCount (int globalId);

        int GetUserMegaRaveCount (int personalChannelId);
        int InsertMegaRaveHistory (UInt32 raveTypeId, int channelId, int zoneInstanceId, int zoneType, int assetId,
            int raveValue, int userIdRaver, int pricePaid, int commissionReceived);
        int UpdateMegaRaveHistoryCommission (int megaRaveHistoryId, int commissionReceived);
            
        RaveType GetRaveType (UInt32 raveTypeId);
        IList<RaveType> GetRaveTypes ();
    }
}
