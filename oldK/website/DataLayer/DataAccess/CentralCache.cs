///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Diagnostics;

using log4net;
using System.Configuration;
using Enyim.Caching;


namespace Kaneva.DataLayer.DataObjects
{
    /// <summary>
    /// Class that manages all cached data base access.
    /// 
    /// GoF Design Patterns: Singleton
    /// </summary>
    /// <remarks>
    /// 
    /// This class is like a Singleton -- it is a static class and 
    /// therefore only one 'instance' ever will exist.
    /// </remarks>
    public sealed class CentralCache
    {
        // Note: constructor is 'private'
        static CentralCache()
        {
            try
            {
                // If it enabled
                if (ConfigurationManager.AppSettings.Get("MemcachedEnabled") != null)
                {
                    bCacheEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("MemcachedEnabled"));
                }

                if (bCacheEnabled)
                {
                    m_logger.Info("MemcachedEnabled.  Starting up.");
                    _MemcachedClient = new MemcachedClient();
                }
                else
                {
                    m_logger.Info("Memcached is turned off. MemcachedEnabled is false.");
                }
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error starting cache", exc);
            }
        }

        public static bool Store(string strKey, Object oObject)
        {
            return storeInternal(Enyim.Caching.Memcached.StoreMode.Set, strKey, oObject);
        }

        public static bool Store(string strKey, Object oObject, TimeSpan tSpan)
        {
            return storeInternal(Enyim.Caching.Memcached.StoreMode.Set, strKey, oObject, tSpan );
        }

        /// <summary>
        /// Add an object to the cache.  If the item already exists, the add will fail.  This
        /// is an atomic, synchronized operation from the perspective of the cache.
        /// </summary>
        /// <param name="strKey">String key under which the object will be added to the cache</param>
        /// <param name="oObject">The object to be added to the cache</param>
        /// <returns>true if the object is successfully stored (or caching is disabled), else false</returns>
        public static bool Add(string strKey, Object oObject)
        {
            return storeInternal(Enyim.Caching.Memcached.StoreMode.Add, strKey, oObject );
        }

        /// <summary>
        /// Add an object to the cache.  If the item already exists, the add will fail.  This
        /// is an atomic, synchronized operation from the perspective of the cache.
        /// </summary>
        /// <param name="strKey">String key under which the object will be cached.</param>
        /// <param name="oObject">The object to be cached.</param>
        /// <param name="tSpan">TimeSpan indicating the time to live of the item</param>
        /// <returns>true if the object is successfully stored (or caching is disabled), else false</returns>
        public static bool Add(string strKey, Object oObject, TimeSpan tSpan)
        {
            return storeInternal(Enyim.Caching.Memcached.StoreMode.Add, strKey, oObject, tSpan);
        }

        /// <summary>
        /// Internal store method that allows for the specification of the store mode.
        /// </summary>
        /// <param name="mode">StoreMode
        /// set -- unconditionally sets a given key with a given value (update_foo() should use this) 
        /// add -- adds to the cache, only if it doesn't already exist (get_foo() should use this) 
        /// replace -- sets in the cache only if the key already exists (not as useful, only for completeness) 
        /// </param>
        /// <param name="strKey">String key under which the object will be cached.</param>
        /// <param name="oObject">The object to be cached.</param>
        /// <param name="tSpan">TimeSpan indicating the time to live of the item</param>
        /// <returns>true object is stored (or caching is disabled), else false</returns>
        private static bool storeInternal(  Enyim.Caching.Memcached.StoreMode   mode
                                            , string                            strKey
                                            , Object                            oObject
                                            , TimeSpan?                         tSpan = null)        {
            if (!(bCacheEnabled && _MemcachedClient != null))
                return true;

            if (!oObject.GetType().IsSerializable)                  {
                Debug.Assert(false);
                m_logger.Warn("Failed to store key " + strKey + " since object not serializable " + oObject.GetType().ToString());
                return false;
            }

            if (tSpan == null)              {
                if (_MemcachedClient.Store(mode, strKey, oObject))
                    return true;
            }
            else                            {
                if (_MemcachedClient.Store(mode, strKey, oObject, (TimeSpan)tSpan))
                    return true;
            }
            m_logger.Warn("Failed to store key " + strKey);
            return false;
        }

        public static bool Replace(string strKey, Object oObject)
        {
            if (bCacheEnabled && _MemcachedClient != null)
            {
                // StoreMode
                // set -- unconditionally sets a given key with a given value (update_foo() should use this) 
                // add -- adds to the cache, only if it doesn't already exist (get_foo() should use this) 
                // replace -- sets in the cache only if the key already exists (not as useful, only for completeness) 
                if (!_MemcachedClient.Store (Enyim.Caching.Memcached.StoreMode.Replace, strKey, oObject))
                {
                    m_logger.Warn("Failed to store key " + strKey);
                    return false;
                }
            }
            return true;
        }

        public static Object Get(string strKey)
        {
            if (bCacheEnabled && _MemcachedClient != null)
            {
                return _MemcachedClient.Get (strKey);
            }

            return null;
        }

        public static void Remove(string strKey)
        {
            if (bCacheEnabled && _MemcachedClient != null)
            {
                if (!_MemcachedClient.Remove(strKey))
                {
                    m_logger.Warn("Failed to remove key " + strKey);
                }
            }
        }

        /// <summary>
        /// FlushAll
        /// </summary>
        public static void FlushAll()
        {
            if (bCacheEnabled && _MemcachedClient != null)
            {
                _MemcachedClient.FlushAll ();
            }
        }

        /// <summary>
        /// Increment
        /// </summary>
        public static void Increment(string strKey, uint amount)
        {
            if (bCacheEnabled && _MemcachedClient != null)
            {
                long test = _MemcachedClient.Increment(strKey, 1);

            }
        }

        

        private static bool bCacheEnabled = false;
        private static MemcachedClient _MemcachedClient;

        // Logger
        private static readonly log4net.ILog m_logger = log4net.LogManager.GetLogger(System.Reflection.Assembly.GetCallingAssembly(), "Cache");

        // Cache Keys
        public static readonly string keyKanevaUser = "kaneva.user.";
        public static readonly string keyKanevaUserInfo = "kaneva.user.inf.";
        public static readonly string keyKanevaUserBalances = "kaneva.user_balances.";
        public static readonly string keyCommunitiesPersonalUserId = "kaneva.communities_personal.user_id.";
        public static readonly string keyRecentCommunityView = "app.recent_community_view";
        public static readonly string keyRecentAssetView = "app.recent_asset_view";
		public static readonly string keyMailerEnabledTemplates = "mailer.template";
        public static readonly string keyCommunityId = "community.id";
        public static readonly string keyKanevaFraudDetect = "kaneva.fraudDetect";
        public static readonly string keyFaux3DApps = "developer.faux_3d_apps";

        // Fame
        public static readonly string keyFameUserFame = "fame.user_fame.";
        public static readonly string keyFameDailyRewardTracking = "fame.daily_reward_tracking.";

        public static string keyUserByUserName(string userName )
        {
            return "kaneva.userxname." + userName;
        }
        public static string keyUserPref(int userId)
        {
            return "kaneva.upref." + userId.ToString();
        }
		public static string keyUserNotificationPref(int userId)
		{
			return "kaneva.unotpref." + userId.ToString();
		}
        public static string keyUserFBSettings (int userId)
        {
            return "kaneva.ufbset." + userId.ToString ();
        }
        public static string keySmokeTestUserLogin(string key)
        {
            return "kaneva.stul." + key;
        }

        // KIM
        public static string keyFriendTransactionsCached ()
        {
            return "kaneva.friend_transactions.cached";

        }
        public static string keyFriendTransactions (int start, int max)
        {
            return "kaneva.friend_transactions." + start.ToString () + "." + max.ToString ();
        }
		
		// Subscription stuff
        public static readonly string keyUnpurchasedSubscription = "kaneva.p_to_s.unpurchased";
        public static readonly string keyKanevaUserSubscriptionValid = "kaneva.user_subscriptions.valid";
        public static readonly string keyKanevaUserSubscriptionValidByPassGroup = "kaneva.user_subscriptions.pass_group";
        public static readonly string keyUserSubscriptionsActive = "kaneva.user_subscriptions.active";
         
        // wok web stuff
        public static string keyZoneCustomizationsPoint(int zoneIndex, int instanceId, float x, float y, float z)
        {
            return "wok.zp." + zoneIndex.ToString() + "," + instanceId.ToString() + "," + x.ToString() + "," + y.ToString() + "," + z.ToString();
        }
        public static string keyUserInGame(int userId)
        {
            return "wok.ig." + userId.ToString();
        }
        public static string keyZoneCustomizations(int zoneIndex, int instanceId)
        {
            return "wok.zz." + zoneIndex.ToString() + "," + instanceId.ToString();
        }
        public static string keyZoneDownloadSizes(int zoneIndex, int instanceId)
        {
            return "wok.zds." + zoneIndex.ToString() + "," + instanceId.ToString();
        }
        public static string keyPlayerAdmin(int userId)
        {
            return "wok.ua." + userId.ToString();
        }
        public static string keyGameServerAccess(int serverId)
        {
            return "game.access." + serverId.ToString();
        }
        public static string keyGameServerIds(int gameId)
        {
            return "game.server.ids." + gameId.ToString();
        }
        public static string keyGameServerById(int serverId)
        {
            return "game.svr." + serverId.ToString();
        }
        public static string keyDefaultGamePatch(int gameId)
        {
            return "game.patch." + gameId.ToString();
        }
        public static string keyEntityCfg(int userId)
        {
            return string.Format("wok.ecfg.{0}", userId);
        }
        public static string keyPlayNowGuid(string playNowGuid)
        {
            return string.Format("wok.png.{0}", playNowGuid);
        }
        public static string keyAsyncTaskCompletion(string guid)
        {
            return string.Format("act.{0}", guid);
        }

        // API Token data
        public static readonly string keyAPIAuthAuthentication = "kaneva.api_authentication";
        public static readonly string keyAPIToken = "token.requestAccess";

        public static readonly string keyThrottleConfig = "kaneva.throttle_config";
        public static readonly string keyThrottleStore = "kaneva.throttleStore";

        public static readonly string keyKanevaRaveTypes = "kaneva.raveTypes";

        // Single Sign-On
        public static string keyUserSingleSignOnToken(int userId)
        {
            return "kaneva.ussotoken." + userId.ToString();
        }

        public static readonly string keyPageMetrics = "metrics.page_timing";

        // Related keys
        public static IList<string> GetRelatedKeys(string key)
        {
            IList<string> res = null;
            if (key.StartsWith("wok.zz."))
            {
                res = new List<string>();
                res.Add(key.Replace("wok.zz.", "wok.zds."));
            }
            return res;
        }
    }
}
