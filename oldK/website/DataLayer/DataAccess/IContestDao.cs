///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IContestDao
    {
        PagedList<Contest> GetContests (int userId, string orderBy, string filter, int pageNumber, int pageSize);
        PagedList<Contest> SearchContests(int userId, string searchString, string orderBy, int pageNumber, int pageSize);
        int InsertContest (Contest contest);
        Contest GetContest (UInt64 contestId);
        PagedList<Contest> GetContestsEntered (int userId, string orderBy, string filter, int pageNumber, int pageSize);
        PagedList<Contest> GetContestsFollowing (int userId, string orderBy, string filter, int pageNumber, int pageSize);
        PagedList<Contest> GetContestsVotedOn (int userId, string orderBy, string filter, int pageNumber, int pageSize);
        int InsertContestEntry (ContestEntry entry);
        PagedList<ContestEntry> GetContestEntries (UInt64 contestId, string orderBy, string filter, int pageNumber, int pageSize);
        ContestEntry GetContestEntry (UInt64 contestEntryId);
        int InsertComment (UInt64 contestId, UInt64 contestEntryId, int userId, string comment);
        int DeleteComment (UInt64 contestId, UInt64 contestEntryId, UInt64 commentId);
        PagedList<ContestComment> GetContestComments (UInt64 contestId, UInt64 contestEntryId, int pageNumber, int pageSize, string sortBy);
        int InsertVote (int userId, string ipAddress, UInt64 contestId, UInt64 contestEntryId);
        int FollowContest (UInt64 contestId, int userId);
        int UnFollowContest (UInt64 contestId, int userId);
        bool HasUserVotedOnContest (UInt64 contestId, int userId);
        bool HasUserEnteredContest (UInt64 contestId, int userId);
        bool IsUserFollowingContest (UInt64 contestId, int userId);
        bool IsUserIPSameAsContestOwner (UInt64 contestId, string ipAddress);
        int UpdateContestWinner (UInt64 contestId, UInt64 popularWinnerEntryId, UInt64 ownerVoteWinnerEntryId);
        IList<ContestUser> GetContestEntryVoters (UInt64 contestEntryId, string orderBy, string filter);
        IList<ContestUser> GetContestFollowers (UInt64 contestId, string orderBy, string filter);
        PagedList<Contest> GetContestsUserWon (int userId, string orderBy, string filter, int pageNumber, int pageSize);
        PagedList<Contest> GetContestsNeedWinner (int userId, string orderBy, string filter, int pageNumber, int pageSize);
        int InsertContestTransactionDetails (int wokTransLogId, UInt64 contestId, UInt16 contestTransType);
        UInt64 CreateContest (Contest contest, int createFee, UInt16 transactionType, UInt16 contestTransactionType, string keiPointId, ref int wokTransactionLogId);
        int UpdateContestStatus (UInt64 contestId, int status, int userId);
        int UpdateContestEntryStatus (UInt64 contestEntryId, int status, int userId);
        int UpdateEntryCount (UInt64 contestId, int amount);
        int UpdateVoteCount (UInt64 contestId, int amount);
        int DeleteEntryVoters (UInt64 contestEntryId);
        int ArchiveContest (UInt64 contestId);
        int SetPopularWinner (UInt64 contestId);
        int SetPopularWinnerAsContestWinner (UInt64 contestId);
        IList<UInt64> GetEntriesUserHasVotedOn (UInt64 contestId, int userId);
        bool HasUserVotedOnContestEntry (UInt64 contestEntryId, int userId);
    }
}
