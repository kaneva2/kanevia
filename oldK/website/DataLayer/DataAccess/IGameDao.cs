///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IGameDao
    {
        //--select game functions---
        Game GetGame(int gameId);
        Game GetGame(string gameName);
        Game GetGameByServerId(int serverId);

        IList<Game> GetGamesByOwner(int ownerId, string filter, string orderby);
        PagedDataTable GetGamesByOwner(int ownerId, string filter, string orderby, int pageNumber, int pageSize);
        PagedDataTable SearchForGames(string searchString, string filter, string categories, bool bGetMature, bool bThumbnailRequired, int pastDays, int ownerId, string orderby, int pageNumber, int pageSize);
        IList<Game> GetAllGames(string filter, string orderby);
        DataTable GetGameCategories(int gameId);
        int GetGameIdByCommunityId(int communityId);
        int UpdateGameThumbnail(int gameId, string imagePath);
        int UpdateGameGAAccountID(int gameId, string gaAccountID);

        // Search
        DataTable SearchApartments(bool bHasAPSubscription, bool bShowOnlyAP, Int32 pageNumber, Int32 pageSize, String searchString, String orderBy, ref int totalCount);
        DataTable SearchHangouts(bool bHasAPSubscription, bool bShowOnlyAP, bool bOnlyPopulated, bool bOnlyEmptyPopulation, Int32 pageNumber, Int32 pageSize, String searchString, int placeType, bool myCommunitiesOnly, int userId, String orderBy, int wokGameId, string country, int minAge, int maxAge, ref int totalCount);
        DataTable Search3DApps(bool onlyAccessPass, bool bGetMature, string searchString,
            bool bOnlyWithPhotos, string country, int[] communityTypeIds,
            string orderBy, int pageNumber, int pageSize, int iPlaceTypeId, bool myCommunitiesOnly, bool onlyOwned, int userId, int wokGameId, ref int totalNumRecords);
        DataTable Browse3DAppsByRequest(bool ShowOnlyAP, bool HideAP, int userId, ref int totalNumRecords, int pageNumber, int PageSize, int wokGameId);
        DataTable Browse3DAppsByRequestB(bool ShowOnlyAP, int userId, ref int totalNumRecords, int pageNumber, int PageSize, int wokGameId);
        DataTable GetMostPopulated3DApps (bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId);
        DataTable GetMostRaved3DApps (bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId);
        DataTable GetMostVisited3DApps(bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId);
        DataTable MostPopulatedCombined(bool ShowOnlyAP, bool HideAP, int userId, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId,string countryFilterBy, string countrySortBy, int ageFilterBy, int ageSortBy, int offset);
        DataTable GetMostRequested(bool ShowOnlyAP, bool HideAP, int userId, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId);
        DataTable GetUserHomeWorldForSearch(int userId, int wokGameId, ref int totalNumRecords);
        PagedList<Community> SearchWorldPrepopulation(string worldStartsWith, string orderby, int pageNumber, int pageSize);

        //--manage game functions---
        int AddNewGame(Game game);
        int UpdateGame(Game game);
        int DeleteGame(int gameId, int userId);
        int TransactionalGameCreation(Game game, GameLicense license, Community community, int daysLeft);

        //--get pulldown items
        DataTable GetGameRatings();
        DataTable GetGameStatus();
        DataTable GetGameAccess();
        DataTable GetGameCategories();
        int GetGameCategoryIdByName(string categoryName);
        int DeleteGameCategories(int gameId);
        int InsertGameCategories(int gameId, int categoryId);
        int InsertGameRave(int userId, int gameId);
        void ClearZoneCustomizationsCache(int zoneIndex, int instanceId);
        DataSet GetZoneCustomizations(int zoneIndex, int instanceId, float x, float y, float z);
        IDictionary<uint, uint[]> GetZoneDownloadSizes(int zoneIndex, int instanceId, uint meshPathTypeId, uint texPathTypeId);
        bool IsFrameworkEnabled(int instanceId, int zoneType);
        bool UserInGame(int userId, int gameId); // gameId = 0 for any game
        DataTable GetGameUserIn (int userId);
        eLOGIN_RESULTS LoginUser(int userId, string userIpAddress, int gameId, int serverId, string actualIp, ref int roleId);
        eLOGIN_RESULTS LogoutUser(int userId, int reasonCode, int serverId, string actualIp);
        DataSet GetChildGameWebCallValues();
        DataSet GetGameItems(int userId, int gameId, int globalId, int quantity);
        WOKItem.ItemApprovalStatus GetGameItemApprovalStatus(int gameId, int globalId);
        CommunityMember.CommunityMemberAccountType GetGamePrivileges(int userId, int gameId);
        bool UserAllowedGame(int gameId, int userId);
        bool IsUserAllowedGame(int gameId, int userId);
        int GetGameAccessByServerId(int serverId);
        int AdjustGameBalance(int gameId, string keiPointId, Double amount);

        DataTable GetGamePasses(int gameId);
        bool AddGamePass(int userId, bool isAdmin, int gameId, int passId);
        bool DeleteGamePass(int userId, bool isAdmin, int gameId, int passId);

        // API Related
        int InsertGiftedItem(int gameId, int userId, int globalId);
        int GetGiftedItemsCount(int gameId, int userId);

        Leaderboard GetLeaderBoard(int communityId);
        PagedList<LeaderboardValue> GetLeaderBoardValues(int communityId, int userId, bool onlyFriends, int pageNumber, int pageSize);
        LevelValue GetLevelValue(int communityId, int userId);

        void EarnLeaderBoard(int communityId, int userId, int newValue);
        int SaveLeaderBoard (Leaderboard leaderboard);
        int ResetLeaderBoard (int communityId);

        void EarnLevel(int communityId, int userId, uint levelNumber, uint percentComplete, string title);

        void EarnTitle(int communityId, int userId, string title);
        string GetTitle(int communityId, int userId);
        int RemovePlayerFromLeaderBoard (int communityId, int userId);

        void EarnAchievement(int userId, uint achievementId);
        PagedList<AchievementEarned> AchievementForUser(int userId, int communityId, string sortBy, bool showAll, bool onlyFriends, int pageNumber, int pageSize);
        PagedList<Achievement> AchievementForApp(int communityId, string sortBy, int pageNumber, int pageSize);
        PagedList<UserAchievementData> GetUserAchievementsForApp (int communityId, int userId, string orderBy, int pageNumber, int pageSize);
        Achievement GetAchievement (uint achievementId);
        uint SaveAchievement (Achievement achievement);
        int UpdateAchievementThumbnail (uint achievementId, string imageUrl);
        int GetTotalAchievementPoints (int communityId, string filter);
        int DeleteAchievement (uint achievementId);
        int ResetAchievements (int communityId);
        List<AchievementAward> GetAchievementAwards(uint achievementId, string gender);


        PagedList<Community> GetGamesInWhichUserHasFame (int userId, int pageNumber, int pageSize, string orderBy);
        Dictionary<string, int> GetAchievementsSummaryForApp (int communityId, int userId);
        Dictionary<string, int> GetAchievementsSummaryForAllApps (int userId);

        void InsertAPIAuth(int communityId, string consumerKey, string consumerSecret);
        APIAuthentication GetAPIAuth(int gameId);
        APIAuthentication GetAPIAuth(string consumerKey, string consumerSecret);
        APIAuthentication GetAPIAuth(string consumerKey);

        DataTable GetMostVisited(int userId, bool HideAP, int pageNumber, int pageSize, int constrainCount, int wokGameId, bool includePopulation);
        DataTable Top3Dapps(bool onlyAccessPass, bool HideAP, int pageNumber, int pageSize, ref int totalCount, int wokGameId, bool hideOwnerOnly = false);
        DataTable GetUnique3DAppVisits(int pageNumber, int pageSize);
        DataTable GetWorldsByCategory(bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId, string category);
        DataTable TopWorldsTour(int userId, bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId);
        int ReedemTopWorldReward(int userId, int zoneInstanceId, int zoneType);
        bool HasTopWorldRewardBeenRedeemed(int userId, int zoneInstanceId, int zoneType);

        int InsertNotifcation(int userId, int packetId, int levelId, int badgeId);

        int InsertNotificationAppRequest (int userId, int gameId);
        int ClearNotificationAppRequest(int userId, int gameId);
        int GetNumberOfNotificationAppRequest (int userId, int gameId);
        int GetTotalNotificationAppRequests (int userId);
        int GetNumberOfVisitorsForGamesByOwner (int userId, DateTime startDate);

        bool InsertAppMessageQueue(int gameId, string msgType, string msgSource);
        DataTable GetAppMessageQueue(int gameId);
        bool UpdateAppMessageQueue (string id, string clientResponseId, string clientResponseCode, DateTime clientProcessedDate);
        DataRow GetGameFaux3DApp (int gameId);
        string GetZoneNameFromZoneIndex (int zoneIndex);

        PagedList<WOKItem> GetScriptAvailableItems(int pageNumber, int pageSize);
        int InsertScriptAvailableItemsIds(int[] globalIds);
        int DeleteScriptAvailableItemsIds(int[] globalIds);

        DataSet GetPremiumItems (int premiumItemTypeId);
        MetaGameItem GetMetaGameItem(string itemName);
        List<MetaGameItem> GetMetaGameItems(string itemType);

        int InsertObjectUserInteraction(int userId, int zoneInstanceId, int zoneType, int objPlacementId, string name, string interactionType);
        int UpdateLeaderboardObjectName(int objPlacementId, string name);
        bool HasUserRavedGameObject(int userId, int objPlacementId);
        bool HasUserVisitedGameObject(int userId, int objPlacementId);
        int UpdateTeamMembers(List<int> userIds, int objPlacementId);
        DataTable GetTeamMembers(IEnumerable<int> objPlacementIds);
        DataTable GetTeamMembers(int objPlacementId);
        DataTable GetLeaderboard(int zoneInstanceId, int zoneType, int ravePointValue, int visitPointValue);
        DataTable GetFinalLeaderboard(int zoneInstanceId, int zoneType);
        DataTable GetFinalLeaderboardTeamMembers(int zoneInstanceId, int zoneType);
        DataTable GetUserLeaderboardAwards(int zoneInstanceId, int zoneType, int userId, string username);
        bool RedeemUserLeaderboardAward(int userId, IEnumerable<int> worldLeaderboardIds);
        bool RedeemUserLeaderboardAward(int userId, int worldLeaderboardId);
        DataTable CreateLeaderboardFinalResults(int ravePointValue, int visitPointValue);
        int InsertLeaderboardFinalResults(int objPlacementId, int zoneInstanceId, int zoneType, string name, int ranking, int points);
        int InsertLeaderboardFinalTeamMembers(DataTable dtMembers, int worldLeaderboardId, int objPlacementId, int awardQuantity);
        int UpdateObjectInteractionsAsProcessed();
        int CleanupTeamMembersHistory();
        int CleanupWorldLeaderboards();
        int CleanupLeaderboardVisits();

        DataTable GetLinkedZones(int communityId, int zoneInstanceId, int zoneType, int wokGameId);
        bool IsChildZone(int zoneInstanceId, int zoneType, out int parentCommunityId);

        DataTable GetTutorial(string testGroupId);
        bool CanWorldDropGemChests(int userId, int communityId);

        AsyncTaskStatus GetAsyncTaskStatusLog(string taskKey);
        int UpsertAsyncTaskStatusLog(AsyncTaskStatus status);
        int DeleteAsyncTaskStatusLog(string taskKey);

        int ArchiveInactiveWorld(int communityId, string archivedVia);
        int UnarchiveInactiveWorld(int communityId);
        PagedList<ArchivedWorld> GetArchivedWorlds(string worldName, string creatorName, string orderBy,
            int pageNum, int itemsPerPage);
    }
}
