///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IShoppingDao
    {
        // Shopping
        int AddCustomItem(WOKItem item, UInt32 animTargetActorGLID);
        int AddWebItem(WOKItem item, bool propagateTransExceptions = false);
        int UpdateItemTextureActiveState(int globalId, int itemActiveState);
        int DeleteItemFromPendingAdds(int globalId);
        int DeleteItemFromDynamicObjects(int globalId);
        int DeleteItemFromAllInventory(int globalId);

        int UpdateCustomItemTexture(WOKItem item);
        int UpdateCustomItemTextureEncryption(WOKItem item, string path);
        int UpdateCustomItem(WOKItem item);
        int UpdateShopItem(WOKItem modifiedItem, WOKItem origItem, int modifyingUserId);
        int InsertIntoStoreInventories(int storeId, int itemId);
        int DeleteFromStoreInventories(int storeId, int itemId);
        int DeleteFromStoreInventories(int itemId);
        int DeleteCustomItemThumbnail(int globalId);
        int DeletePremiumItem(int globalId);
        int UpdatePremiumItemApprovalStatus(WOKItem item);
        int UpdateItemDeniedReason(int globalId, string desc);
        string GetItemDeniedReason(int globalId);
        WOKItem GetItem(int globalId, bool bAllowUGCTemplate);
        List<WOKItem> GetItems(IEnumerable<int> globalIds, bool bAllowUGCTemplate, string orderBy);
        WOKItem GetAnimationActor(int globalId);
        List<WOKItem> GetItemAnimations(int globalId, string filter, bool showPrivate);
        DataTable GetItemAnimations(int globalId, bool showPrivate);
        DataTable GetAnimations(string globalIds);
        PagedList<WOKItem> GetOwnerItems(string searchString, int ownerId, UInt32 itemCategoryId, int pageSize, int pageNumber, string orderBy);
        PagedList<WOKItem> SearchItems(string searchString, int ownerId, UInt32[] itemCategoryIds, int pageSize, int pageNumber, string orderBy, bool showRestricted, bool onlyAccessPass, int showPrivate, bool onlyAnimated);
        PagedList<WOKItem> GetOwnerItems(string searchString, int ownerId, UInt32 itemCategoryId, int pageSize, int pageNumber, string orderBy, bool bIncludeUGCTemplate);
        PagedList<WOKItem> GetPremiumItems(int gameId, int pageSize, int pageNumber, string orderBy, bool showPrivate, bool showNonApproved);
        List<DeedItem> GetWorldDODeedItems(int zoneIndex, int zoneInstanceId, int worldOwnerId);
        List<DeedItem> GetWorldDOPDeedItems(int zoneIndex, int zoneInstanceId, int worldOwnerId);
        List<DeedItem> GetDODeedItems(int deedTemplateId, int deedOwnerId);
        List<DeedItem> GetDOPDeedItems(int deedTemplateId, int deedOwnerId);
        int GetCommunityIdFromDeedTemplateId(int deedTemplateId);
        List<WOKItem> GetBundlesItemBelongsTo(int itemId, string filter, bool onlyPublic = true);
        List<WOKItem> GetDeedsItemBelongsTo(int itemId, string filter, bool onlyPublic = true);
        List<WOKItem> GetBundleItems(int bundleId, string filter, bool onlyPublic = true);
        List<WOKItem> GetDerivedItemsIncludedInBundles(int globalId, string filter);
        List<WOKItem> GetDerivedItemsIncludedInDeeds(int globalId);
        int AddItemToBundle(int bundleGlobalId, int itemGlobalId, int quantity, bool propagateQueryExceptions = false);
        int RemoveItemsFromBundle(int bundleGlobalId, List<int> itemGlobalIds);
        int RemoveAllFromBundle(int bundleGlobalId, bool propagateQueryExceptions = false);
        int GetBundleStoredWebPrice(int globalId);

        bool IsItemAccessPass(int globalId);

        DataTable GetPasses(string globalIds);
        DataTable GetExclusionGroupIds(string globalIds);

        ItemCategory GetItemCategoryByName(string name);
        ItemCategory GetItemCategoryByTree(string parentCategory, string subCategory);
        ItemCategory GetItemCategoryParentByUploadType(int uploadType);
        int GetUploadTypeByItemCategoryId(int categoryId, int retry);
        ItemCategory GetItemCategory(UInt32 categoryId);

        List<ItemCategory> GetItemCategoriesByParentCategoryId (UInt32[] parentCategoryIds);
        DataTable GetItemCatsByUploadType(uint uploadType);
        List<ItemCategory> GetItemCatByUploadTypeActorGroup(uint uploadType, int actorGroup);
        List<ItemCategory> GetItemCategoriesByFilter(string filter);
        PagedList<ItemCategory> GetItemCategories(UInt32 itemCategoryId, string filter, int pageSize, int pageNumber, string orderBy);

        List<ItemCategory> GetItemCategoriesByItemId(int globalId);
        PagedList<WOKItem> GetItemPromotions(UInt32 itemCategoryId, int pageSize);

        PagedList<ItemReview> GetItemReviews(int globalId, int pageSize, int pageNumber);
        int AddItemReview(ItemReview itemReview);
        int DeleteItemReview(uint reviewId);
        PagedList<WOKItem> GetRelatedItems(WOKItem item, UInt32 itemCategoryId, int pageSize, int pageNumber, bool showRestricted);

        int AddItemPurchase(int globalId, int txnType, int userId, UInt32 itemOwnerId, int purchasePrice, UInt32 ownerPrice, string ipAddress, string keiPointId, int quantity, int parentPurchaseId);
        PagedList<ItemPurchase> GetItemPurchases(int userId, int pageSize, int pageNumber);
        int GetWeeklySales(int userId, ref int credits, ref int numberOfSales);
        int InsertPremiumItemCreditRedemption (int purchaseId, int communityId);

        int InsertItemDigg(ItemDigg itemDigg);
        ItemDigg GetItemDigg(int userId, int globalId);

        int AddCategoryToItem(int globalId, uint ItemCategoryId, int categoryIndex);
        int DeleteCategoryFromItem(int globalId, int categoryIndex);

        int DeleteWOKCategoryItems(int categoryId);
        int UpdateCategorysParent(int categoryId, int parentCategoryId);
        DataTable GetWOKCategoryItemsByCategoryId(int categoryId);
        DataTable GetWOKCategoryItems(int itemID, int categoryId);
        int MoveWOKCategoryItem(int oldCategoryId, int newCategoryId, int itemId);
        int DeleteCategory(int categoryId);
        List<WOKItem> GetWOKItemsList();
        int UpdateWOKCategory(int categoryId, string category, int parentCategoryId, string description, int userId, string marketingPath);
        int InsertWOKCategory(string category, int parentCategoryId, string description, int userId, string marketingPath);

        List<ItemTemplate> GetItemTemplates();
        int CreateItemTemplate(WOKItem item);
        List<ItemCategory> GetCategoriesThatHaveItems(string filter);

        void AddItemToGame(int globalId, int gameId, int approvalStatus);

        int GetBaseItemGlobalId(int globalId);  // Get original item which a derived item is based on

        DataTable GetItems(int globalId, int ugcId, int templateId, int count);
        DataTable GetSpaceDynamicObjects(int playerId, int zoneIndex, int instanceId);
        DataTable GetSpaceDynamicObjectParams(string ids);
        DataTable GetSpaceWorldObjectSettings(int playerId, int zoneIndex, int instanceId);
        DataTable GetDynamicObjectParams(int objPlacementId);
        DataTable GetDynamicObjectPlaylists(int objPlacementId, int zoneIndex, int zoneInstanceId, int globalId);
        DataRow GetDynamicObjectSoundCustomizations(int objPlacementId);

        bool AddWorldDOsToCustomDeed(int zoneIndex, int zoneInstanceId, int templateGlobalId);
        bool AddWOPSettingsToCustomDeed(int zoneIndex, int zoneInstanceId, int templateGlobalId);

        int DeleteAllDOsFromCustomDeed(int deedTemplateId);
        int DeleteAllWOPSettingsFromCustomDeed(int deedTemplateId);

        void AddItemPath(ItemPath itemPath);
        List<ItemPath> GetItemPaths(int globalId);
        void AddItemPathTexture(ItemPathTexture itemPathTex);

        void AddItemPathFileStore(int globalId, ItemPathType pathTypeInfo, string filestore);
        string GetItemPathFileStore(int globalId, ItemPathType pathTypeInfo);
        ItemPathType GetItemPathType(string pathType);
        uint GetExistingUniqueAssetIdBySHA256(ItemPathType pathTypeInfo, byte[] hashSHA256);
        uint AllocUniqueAssetIdBySHA256(ItemPathType pathTypeInfo, byte[] hashSHA256, int size, int compressedSize, out bool isNew);

        ItemParameter GetItemParameter(int globalId, int paramTypeId);
        void UpdateItemParameter(int globalId, int paramTypeId, string value, bool propagateQueryExceptions = false);

        int CreateCustomZoneTemplate(int zoneIndex, int instanceId, int templateId, bool isAdmin);

        int ChangeZoneMap(int oldZoneIndex, int zoneInstanceId, int newZoneIndex, string playerName, int templateId,
            string gameNameNoSpaces, ref int result, ref string url);
        int ImportZone (int playerId, int oldZoneIndex, int oldZoneInstanceId, int baseZoneIndexPlain,
            int srcZoneIndex, int srcZoneInstanceId, bool maintainScripts, ref int result, ref int newZoneIndex);
        int InsertChannelZone(int userId, int communityId, int zoneIndex, int instanceId, int zoneType, string name,
            int serverId, string tiedToServer, string country, int age, int scriptServerId);
        DataRow GetCustomDeedUsage(int deedId);
        bool IsOriginalUGCZone(int zoneIndex, int zoneInstanceId);
        WOK3DPlace GetDeedTryOnZone(int deedTemplateId);
        int RemoveItemsFromDeed(int deedTemplateId, List<int> itemGlobalIds);

        int CopyGlid(int originalGlid);
        DataRow GetItemByPlacementId(int objPlacementId, int zoneInstanceId, int playerId);

        int CreateItemPreloadList(string name);
        bool UpdateItemPreloadList(int listId, string newName);
        int PublishItemPreloadList(int listId);
        DataTable GetAllItemPreloadLists();
        int GetLatestItemPreloadListVersion();
        DataRow GetItemPreloadList(int listId);       

        void AddItemPreloadListItem(int listId, int globalId);
        void RemoveItemPreloadListItem(int listId, int globalId);
        int MergeItemPreloadListItems(int dstListId, int srcListId);
        DataTable GetItemPreloadListItems(int listId);
    }
}
