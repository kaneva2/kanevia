///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IGameDeveloperDao
    {
        GameDeveloper GetGameDeveloperByUserId(int userId);
        GameDeveloper GetGameDeveloperNoUserInfo(int userId, int companyId);
        GameDeveloper GetGameDeveloperNoUserInfo(int userId);
        PagedList<GameDeveloper> GetGameDevelopersList(int company_id, string filter, string orderby, int pageNumber, int pageSize);
        //PagedDataTable GetGameDevelopers(int company_id, string filter, string orderby, int pageNumber, int pageSize);
        int InsertGameDeveloper(GameDeveloper gameDev);
        int UpdateGameDeveloper(GameDeveloper gameDev);
        int DeleteGameDeveloper(int gameDevId);
        int InsertGameDeveloperEmail (string email);
        DeveloperInvitation GetCompanyInvitation (int inviteId, int companyId);
        DeveloperInvitation GetCompanyInvitation(string validationKey);
        PagedList<DeveloperInvitation> GetCompanyInvitationsList(int companyId, string filter, string orderby, int pageNumber, int pageSize);
        int InsertInvitation(DeveloperInvitation invite);
        int DeleteInvitation(DeveloperInvitation invite);
        int TrackDownLoads(User user, string itemDownLoaded);

    }
}
