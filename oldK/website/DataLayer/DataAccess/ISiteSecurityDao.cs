///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface ISiteSecurityDao
    {
        DataTable GetCompanyRoles(int companyId);
        IList<SiteRole> GetCompanyRolesList(int companyId);
        PagedList<SiteRole> GetAllRolesList(string filter, string orderby, int pageNumber, int pageSize);
        int AddNewRole(SiteRole role);
        int UpdateRole(SiteRole role);
        int DeleteRole(int siteRoleId, int company);
        SiteRole GetCompanyRole(int companyId, int roleId);
        PagedList<SitePrivilege> GetAllPrivilegesPgd(string filter, string orderby, int pageNumber, int pageSize);
        PagedList<SitePrivilege> GetUserPrivilegesPgd();
        IList<SitePrivilege> GetAllPrivilegesList(string filter);
        IList<SitePrivilege> GetAvailableUserPrivileges();
        IList<SitePrivilege> GetAdminSitePrivileges();
        int AddNewPrivilege(SitePrivilege sitePriv);
        int UpdatePrivilege(SitePrivilege sitePriv);
        int DeletePrivilege(int privilegeId);
        DataTable GetAllAccessLevels();
        DataTable GetRolePrivileges(int siteRoleId, int companyId);
        DataTable GetPrivilegesByRoleId(int userRoleId, int companyId);
        int DeleteAllPrivilege2Role(int siteRoleId, int companyId);
        int AddNewPrivilege2Role(int siteRoleId, int companyId, int privilegeId, int accessLevelId);
    }
}
