///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface ISiteManagementDao
    {
        IList<WebSite> GetAvailableWebsites();
        int AddAvailableWebsite(WebSite website);
        int UpdateAvailableWebsite(WebSite website);
        int DeleteAvailableWebsite(int websiteId);
        int AddMainMenuItem(SM_MenuItem menuItem);
        int UpdateMainMenuItem(SM_MenuItem menuItem);
        int DeleteMainMenuItem(int menuItemId);
        int AddSubMenuItem(SM_MenuItem subMenuItem);
        int UpdateSubMenuItem(SM_MenuItem subMenuItem);
        int DeleteSubMenuItem(int subMenuItemId);
        int AddUserControl(KanevaUserControl userControl);
        int UpdateUserControl(KanevaUserControl userControl);
        int DeleteUserControl(int userControlId);
        IList<SM_MenuItem> GetMainMenu(int websiteId);
        IList<SM_MenuItem> GetMainMenuFiltered(int websiteId, string privilegeList);
        IList<SM_MenuItem> GetSubMenu(int mainNavId);
        IList<SM_MenuItem> GetSubMenuFiltered(int mainNavId, string privilegeList);
        KanevaUserControl GetUserControlByID(int userControlId);
        KanevaUserControl GetUserControlByName(string name);
        IList<KanevaUserControl> GetUserControls(int userControlId, string name);
        string GetUserControlPath(int userControlId);
        DataTable GetUserControlTypes();

        int InsertCSRLog(int userId, string activity, int assetId, int updatedUserId);

        int GetNumberChannelShares(DateTime dtStartDate, DateTime dtEndDate);
        int GetNumberPublicChannelShares(DateTime dtStartDate, DateTime dtEndDate);
        int GetNumberPersonalChannelShares(DateTime dtStartDate, DateTime dtEndDate);
        int GetNumberAssetShares(DateTime dtStartDate, DateTime dtEndDate);
    }
}
