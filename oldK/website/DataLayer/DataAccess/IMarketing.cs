///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IMarketingDao
    {
        DataTable GetShareLeaderboard(string sortOrder, int numResults);
        int UpdateShareView(int userId);
        int UpdateShareClick(int userId);
        int AddReferral(int referrerUserId, int referredUserId);
        int CompleteReferral(int referredUserId);
        int countIps(int referredUserId);
        int GetReferrer(int referredUserId);
    }
}
