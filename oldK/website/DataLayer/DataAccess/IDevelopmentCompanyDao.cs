///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IDevelopmentCompanyDao
    {
        DevelopmentCompany GetDevelopmentCompany(int companyId);
        DevelopmentCompany GetCompanyByDeveloperId(int gameDeveloperId);
        PagedList<DevelopmentCompany> GetDevelopmentCompanies(int companyId, string filter, string orderby, int pageNumber, int pageSize);

        int InsertDevelopmentCompany(DevelopmentCompany company);

        DataTable GetVWorldInvolvment();
        DataTable GetProjectStartDates();
        DataTable GetClientPlatforms();
        DataTable GetProjectStatus();
        IList<SiteRole> GetCompaniesRoles(int companyId);
        int CreateDefaultCompanyRoles(int companyId);

    }
}
