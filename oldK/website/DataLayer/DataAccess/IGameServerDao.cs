///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IGameServerDao
    {
        //--select game functions---
        GameServer GetServer(int serverId);
        GameServer GetServer(string serverName);

        IList<GameServer> GetAllServers(string filter, string orderby);
        IList<GameServer> GetServerListByGameId(int serverId, string filter, string orderby);
        DataTable GetServerByGameId(int serverId, string filter, string orderby);
        DataTable GetPatchURLByGameId(int gameId, string filter, string orderby);


        //--manage game functions---
        int AddNewServer(GameServer server);
        int UpdateServer(GameServer server);
        DataRow UpdateGameServerStart(int serverId, int maxCount, int statusId, string ipAddress, int port, string actualIp, int type, uint protocolVersion, int schemaVersion, string serverVersion, uint assetVersion, int parentGameId);
        DataRow UpdateGameServerPingCheck(int serverId, string actualIp, int numberOfPlayers, int maxNumberOfPlayers, int statusId, bool adminOnly);
        int DeleteServer(int serverId);

        int AddNewPatchURL(int gameId, string patchUrl);
        int UpdatePatchURL(int gameId, int patchURLId, string patchUrl);
        int DeletePatchURL(int gameId, int patchURLId);

        DataTable GetGameServerVisibility();
        DataTable GetGameServerStatus();
        string GetServerConfig(int serverType, string serverActualIp);
        DataTable GetGameServerType();
        string GetServerVersion(int serverId);
        string GetServerVersion(int gameId, string serverName, int port, int serverTypeId);

        int AddNewServerAndPatch(int userId, string licenseKey, int gameId, string hostName, int port, string patchUrl, string alternatePatchUrl, string patchType, string clientPath, ref int webAssignedPort, out int serverId);

        PagedDataTable GetGames(int pageNumber, int pageSize, string orderBy);
    }
}
