///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Data.Common;
using log4net;

using Devart.Data.MySql;

namespace Kaneva.DataLayer.DataObjects
{
    /// <summary>
    /// Class that manages all lower level ADO.NET data base access.
    /// 
    /// GoF Design Patterns: Factory, Proxy.
    /// </summary>
    /// <remarks>
    /// This class is a 'swiss army knife' of data access. It handles all the 
    /// database access details and shields its complexity from its clients.
    /// 
    /// The Factory Design pattern is used to create database specific instances
    /// of Connection objects, Command objects, etc.
    /// 
    /// This class is a Proxy in that it 'stands in' for the actual DbProviderFactory.
    /// </remarks>
    public class DbServer
    {
        private string name;
        private string connectionString;

        public DbServer(string name, string connectionString, string dataProvider)
        {
            factory = DbProviderFactories.GetFactory(dataProvider);

            this.name = name;
            this.connectionString = connectionString;

            // Hack in this fix. Bad bad we have to access specific provider property here but oh well.
            // Stop calls to SHOW TABLE STATUS on INNODB selects with auto incs.  
            // Would populate DataColumn.AutoIncrementSeed property
            DbDataAdapter adapter = factory.CreateDataAdapter();
            if (adapter is MySqlDataAdapter)
            {
                MySqlDataAdapter.RetrieveAutoIncrementSeed = false;
            }
        }

        public string Name
        {
            get { return name; }
        }

        public string ConnectionString
        {
            get { return connectionString; }
        }

        #region Data Update handlers

        /// <summary>
        /// Executes Update statements in the database.
        /// </summary>
        /// <param name="sql">Sql statement.</param>
        /// <returns>Number of rows affected.</returns>
        public int Update(string sql)
        {
            return Update(sql, new List<IDbDataParameter>());
        }

        /// <summary>
        /// Executes Update statements in the database.
        /// </summary>
        /// <param name="sql">Sql statement.</param>
        /// <returns>Number of rows affected.</returns>
        public int Update(string sql, List<IDbDataParameter> parameters)
        {
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;

                using (DbCommand command = factory.CreateCommand())
                {
                    command.Connection = connection;
                    command.CommandText = sql;

                    connection.Open();
                    return command.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region Data Retrieve Handlers

        /// <summary>
        /// Populates a DataSet according to a Sql statement.
        /// </summary>
        /// <param name="sql">Sql statement.</param>
        /// <returns>Populated DataSet.</returns>
        public DataSet GetDataSet(string sql)
        {
            return GetDataSet(sql, new List<IDbDataParameter>());
        }

        /// <summary>
        /// Populates a DataSet according to a Sql statement.
        /// </summary>
        /// <param name="sql">Sql statement.</param>
        /// <returns>Populated DataSet.</returns>
        public DataSet GetDataSet(string sql, List<IDbDataParameter> parameters)
        {
            DataSet ds = new DataSet();

            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;

                using (DbCommand command = factory.CreateCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sql;

                    try
                    {
                        using (DbDataAdapter adapter = factory.CreateDataAdapter())
                        {
                            adapter.SelectCommand = command;
                            AddParams(adapter.SelectCommand, parameters);

                            adapter.Fill(ds);
                        }
                    }
                    catch (Exception e)
                    {
                        m_logger.Error(connection.DataSource + "-" + connection.Database + "-Error in GetDataSet: " + e.ToString() + "; QUERY = " + sql);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return ds;
        }

        /// <summary>
        /// GetPagedDataTable
        /// </summary>
        public PagedDataTable GetPagedDataTable(string query, string orderBy, List<IDbDataParameter> parameters, int pageNumber, int pageSize)
        {
            return GetPagedDataTable(query, orderBy, parameters, pageNumber, pageSize, 1);
        }

        /// <summary>
        /// GetPagedDataTable
        /// </summary>
        public PagedDataTable GetPagedDataTable(string query, string orderBy, List<IDbDataParameter> parameters, int pageNumber, int pageSize, int retryCount)
        {
            PagedDataTable pdtResults = new PagedDataTable();

            if (retryCount < 1)
                retryCount = 1;

            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;

                for (int i = 0; i < retryCount; i++)
                {
                    try
                    {
                        // Make sure page number is valid
                        if (pageNumber < 1)
                        {
                            pageNumber = 1;
                        }

                        // Build limit query
                        int offset = (pageNumber - 1) * pageSize;
                        string limit = " LIMIT " + offset + ", " + pageSize;

                        // Build the query, insert SQL_CALC_FOUND_ROWS after the first select
                        string select = "SELECT";
                        int index = query.IndexOf(select);
                        query = query.Insert(index + select.Length, " SQL_CALC_FOUND_ROWS ");

                        string mysqlQuery = query;
                        if (orderBy.Length > 0)
                        {
                            mysqlQuery += " ORDER BY " + orderBy;
                        }

                        using (DbCommand command = factory.CreateCommand())
                        {
                            connection.Open();
                            command.Connection = connection;
                            command.CommandType = CommandType.Text;
                            command.CommandText = mysqlQuery + limit;

                            using (DbDataAdapter adapter = factory.CreateDataAdapter())
                            {
                                adapter.SelectCommand = command;
                                AddParams(adapter.SelectCommand, parameters);

                                adapter.Fill(pdtResults);

                                // Get the Counts
                                command.Parameters.Clear();
                                command.CommandText = "SELECT found_rows()";
                                pdtResults.TotalCount = uint.Parse(command.ExecuteScalar().ToString());
                            }
                        }

                        break;
                    }
                    catch (Exception e)
                    {
                        if (i + 1 >= retryCount) // only log last time
                            m_logger.Error(connection.DataSource + "-" + connection.Database + "-Error in GetPagedDataTable: " + e.ToString() + "; QUERY = " + query);
                    }
                    finally
                    {
                        connection.Close();
                    }

                }
            }
            return pdtResults;
        }

        /// <summary>
        /// GetPagedDataTable
        /// </summary>
        public PagedDataTable GetIndexedPagedDataTable (string query, string orderBy, List<IDbDataParameter> parameters, int startIndex, int pageSize, int retryCount)
        {
            PagedDataTable pdtResults = new PagedDataTable ();

            if (retryCount < 1)
                retryCount = 1;

            using (DbConnection connection = factory.CreateConnection ())
            {
                connection.ConnectionString = connectionString;

                for (int i = 0; i < retryCount; i++)
                {
                    try
                    {
                        // Make sure page number is valid
                        if (startIndex < 1)
                        {
                            startIndex = 1;
                        }

                        // Build limit query
                        int offset = startIndex;  // (pageNumber - 1) * pageSize;
                        string limit = " LIMIT " + offset + ", " + pageSize;

                        // Build the query, insert SQL_CALC_FOUND_ROWS after the first select
                        string select = "SELECT";
                        int index = query.IndexOf (select);
                        query = query.Insert (index + select.Length, " SQL_CALC_FOUND_ROWS ");

                        string mysqlQuery = query;
                        if (orderBy.Length > 0)
                        {
                            mysqlQuery += " ORDER BY " + orderBy;
                        }

                        using (DbCommand command = factory.CreateCommand ())
                        {
                            connection.Open ();
                            command.Connection = connection;
                            command.CommandType = CommandType.Text;
                            command.CommandText = mysqlQuery + limit;

                            using (DbDataAdapter adapter = factory.CreateDataAdapter ())
                            {
                                adapter.SelectCommand = command;
                                AddParams (adapter.SelectCommand, parameters);

                                adapter.Fill (pdtResults);

                                // Get the Counts
                                command.Parameters.Clear ();
                                command.CommandText = "SELECT found_rows()";
                                pdtResults.TotalCount = uint.Parse (command.ExecuteScalar ().ToString ());
                            }
                        }

                        break;
                    }
                    catch (Exception e)
                    {
                        if (i + 1 >= retryCount) // only log last time
                            m_logger.Error (connection.DataSource + "-" + connection.Database + "-Error in GetPagedDataTable: " + e.ToString () + "; QUERY = " + query);
                    }
                    finally
                    {
                        connection.Close ();
                    }

                }
            }
            return pdtResults;
        }

        /// <summary>
        /// Populates a DataTable according to a Sql statement.
        /// </summary>
        /// <param name="sql">Sql statement.</param>
        /// <returns>Populated DataTable.</returns>
        public DataTable GetDataTable(string sql)
        {
            DataSet ds = GetDataSet(sql);
            if (ds.Tables.Count > 0)
                return ds.Tables[0];
            else
                return null;
        }

        /// <summary>
        /// Populates a DataTable according to a Sql statement.
        /// </summary>
        /// <param name="sql">Sql statement.</param>
        /// <returns>Populated DataTable.</returns>
        public DataTable GetDataTable(string sql, List<IDbDataParameter> parameters)
        {
            DataSet ds = GetDataSet(sql, parameters);
            if (ds.Tables.Count > 0)
                return ds.Tables[0];
            else
                return null;
        }

        /// <summary>
        /// Populates a DataRow according to a Sql statement.
        /// </summary>
        /// <param name="sql">Sql statement.</param>
        /// <returns>Populated DataRow.</returns>
        public DataRow GetDataRow(string sql)
        {
            return GetDataRow(sql, new List<IDbDataParameter>());
        }

        /// <summary>
        /// Populates a DataRow according to a Sql statement.
        /// </summary>
        /// <param name="sql">Sql statement.</param>
        /// <returns>Populated DataRow.</returns>
        public DataRow GetDataRow(string sql, List<IDbDataParameter> parameters)
        {
            DataRow row = null;

            DataTable dt = GetDataTable(sql, parameters);
            if (dt.Rows.Count > 0)
            {
                row = dt.Rows[0];
            }

            return row;
        }

        /// <summary>
        /// Executes a Sql statement and returns a scalar value.
        /// </summary>
        /// <param name="sql">Sql statement.</param>
        /// <returns>Scalar value.</returns>
        public object GetScalar(string sql)
        {
            return GetScalar(sql, new List<IDbDataParameter>(), 1);
        }

        /// <summary>
        /// Executes a Sql statement and returns a scalar value.
        /// </summary>
        /// <param name="sql">Sql statement.</param>
        /// <returns>Scalar value.</returns>
        public object GetScalar(string sql, int retryCount)
        {
            return GetScalar(sql, new List<IDbDataParameter>(), retryCount);
        }

        /// <summary>
        /// Executes a Sql statement and returns a scalar value.
        /// </summary>
        /// <returns>Scalar value.</returns>
        public object GetScalar(string sql, List<IDbDataParameter> parameters)
        {
            return GetScalar (sql, parameters, 1);
        }

        /// <summary>
        /// Executes a Sql statement and returns a scalar value.
        /// </summary>
        /// <returns>Scalar value.</returns>
        public object GetScalar(string sql, List<IDbDataParameter> parameters, int retryCount)
        {
            Object obj = new object();

            if (retryCount < 1)
                retryCount = 1;

            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;

                for (int i = 0; i < retryCount; i++)
                {
                    try
                    {
                        using (DbCommand command = factory.CreateCommand())
                        {
                            command.Connection = connection;
                            command.CommandText = sql;
                            AddParams(command, parameters);

                            connection.Open();
                            obj = command.ExecuteScalar();
                        }
                        break;
                    }
                    catch (Exception e)
                    {
                        if (i + 1 >= retryCount) // only log last time
                            m_logger.Error(connection.DataSource + "-" + connection.Database + "-Error in GetScalar: " + e.ToString() + "; QUERY = " + sql);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return obj;
        }

        /// <summary>
        /// ExecuteIdentityInsert
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="identity"></param>
        /// <returns></returns>
        public int ExecuteIdentityInsert(string sqlString, List<IDbDataParameter> parameters, bool propagateExceptions = false)
        {
            return ExecuteIdentityInsert(sqlString, parameters, 1, propagateExceptions);
        }

        /// <summary>
        /// ExecuteIdentityInsert
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="identity"></param>
        /// <returns></returns>
        public int ExecuteIdentityInsert(string sqlString, List<IDbDataParameter> parameters, int retryCount, bool propagateExceptions = false)
        {
             int ret = 0;

             if (retryCount < 1)
                retryCount = 1;
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;

                for (int i = 0; i < retryCount; i++)
                {
                    try
                    {
                        using (DbCommand command = factory.CreateCommand())
                        {
                            connection.Open();
                            command.Connection = connection;
                            command.CommandType = CommandType.Text;
                            command.CommandText = sqlString;

                            using (DbDataAdapter adapter = factory.CreateDataAdapter())
                            {
                                adapter.SelectCommand = command;
                                AddParams(adapter.SelectCommand, parameters);

                                ret = command.ExecuteNonQuery();

                                command.CommandText = "SELECT @@identity";
                                ret = Convert.ToInt32(command.ExecuteScalar());
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        if (i + 1 >= retryCount) // only log last time
                        {
                            m_logger.Error(connection.DataSource + "-" + connection.Database + "-Error in ExecuteIdentityInsert: " + e.ToString() + "; QUERY = " + sqlString);

                            if (propagateExceptions)
                            {
                                throw e;
                            }
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }

                }
            }
            return ret;
        }

        /// <summary>
        /// Execute a non query with parameters
        /// </summary>
        public int ExecuteNonQuery(string sqlString, List<IDbDataParameter> parameters, bool propagateExceptions = false)
        {
            return ExecuteNonQuery(sqlString, parameters, 1, propagateExceptions);
        }

        /// <summary>
        /// Execute a non query with parameters
        /// </summary>
        public int ExecuteNonQuery(string sqlString, List<IDbDataParameter> parameters, int retryCount, bool propagateExceptions = false)
        {
            int ret = 0;

            if (retryCount < 1)
                retryCount = 1;

            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;

                for (int i = 0; i < retryCount; i++)
                {
                    try
                    {
                        using (DbCommand command = factory.CreateCommand())
                        {
                            connection.Open();
                            command.Connection = connection;
                            command.CommandType = CommandType.Text;
                            command.CommandText = sqlString;

                            using (DbDataAdapter adapter = factory.CreateDataAdapter())
                            {
                                adapter.SelectCommand = command;
                                AddParams(adapter.SelectCommand, parameters);

                                ret = command.ExecuteNonQuery();
                            }
                        }

                        break;
                    }
                    catch (Exception e)
                    {
                        if (i + 1 >= retryCount) // only log last time
                        {
                            m_logger.Error(connection.DataSource + "-" + connection.Database + "-Error in ExecuteNonQuery: " + e.ToString() + "; QUERY = " + sqlString);

                            if (propagateExceptions)
                                throw e;
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        /// <summary>
        /// Execute an Insert and return the identity
        /// </summary>
        public int Insert (string sqlString, List<IDbDataParameter> parameters, int retryCount)
        {
            int id = -1;

            if (retryCount < 1)
                retryCount = 1;

            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;

                for (int i = 0; i < retryCount; i++)
                {
                    try
                    {
                        using (DbCommand command = factory.CreateCommand())
                        {
                            connection.Open();
                            command.Connection = connection;
                            command.CommandType = CommandType.Text;
                            command.CommandText = sqlString;

                            using (DbDataAdapter adapter = factory.CreateDataAdapter())
                            {
                                adapter.SelectCommand = command;
                                AddParams(adapter.SelectCommand, parameters);

                                command.ExecuteNonQuery();

                                // Execute db specific autonumber or identity retrieval code
                                // SELECT SCOPE_IDENTITY() -- for SQL Server
                                // SELECT @@IDENTITY -- for MS Access
                                // SELECT MySequence.NEXTVAL FROM DUAL -- for Oracle
                                // SELECT last_insert_id() as lastId -- for MySQL
                                string identitySelect;
                                switch (dataProvider)
                                {
                                    // Access
                                    case "System.Data.OleDb":
                                        identitySelect = "SELECT @@IDENTITY";
                                        break;
                                    // Sql Server
                                    case "System.Data.SqlClient":
                                        identitySelect = "SELECT SCOPE_IDENTITY()";
                                        break;
                                    // Oracle
                                    case "System.Data.OracleClient":
                                        identitySelect = "SELECT MySequence.NEXTVAL FROM DUAL";
                                        break;
                                    case "MySQLDirect.NET Data Provider":
                                        identitySelect = "SELECT last_insert_id() AS lastId";
                                        break;
                                    default:
                                        identitySelect = "SELECT @@IDENTITY";
                                        break;
                                }

                                command.CommandText = identitySelect;
                                id = int.Parse(command.ExecuteScalar().ToString());
                            }
                        }

                        break;
                    }
                    catch (Exception e)
                    {
                        if (i + 1 >= retryCount) // only log last time
                            m_logger.Error(connection.DataSource + "-" + connection.Database + "-Error in ExecuteNonQuery: " + e.ToString() + "; QUERY = " + sqlString);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return id;
        }

        /// <summary>
        /// AddParams
        /// </summary>
        private void AddParams(DbCommand command, List<IDbDataParameter> parameters)
        {
            IEnumerator<IDbDataParameter> myEnumerator = parameters.GetEnumerator();
            while (myEnumerator.MoveNext())
            {
                command.Parameters.Add(myEnumerator.Current);
            }
        }

        #endregion

        private readonly string dataProvider = null;
        private readonly DbProviderFactory factory;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.Assembly.GetCallingAssembly(), "Database");
    }
}
