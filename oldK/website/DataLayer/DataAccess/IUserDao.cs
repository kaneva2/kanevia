///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IUserDao
    {
        User GetUser(int userId);
        DataRow GetUserObsoleteForWok(int userId);
        User GetUser(string userName);
        User GetUserByEmail(string userName);
        PagedList<User> GetUsersByEmail(List<string> emails);
        User GetCommunityOwnerByCommunityId(int communityId);
        UserStats GetUserStats(int userId);
        PagedList<User> GetUsersList(string filter, string orderby, int pageNumber, int pageSize);
        PagedList<User> GetUsersList(string username, string email, string firstName, string lastName, int userStatus, string filter, string orderby, int pageNumber, int pageSize);

        PagedList<User> SearchUsers(bool onlyAccessPass, bool showMature, string searchString, bool male, bool female, bool bOnlyPhoto,
            int ageFrom, int ageTo,
            string country, string zipCode, int zipMiles, int newWithinDays, int updatedWithinDays,
            int heightFromFeet, int heightFromInches, int heightToFeet, int heightToInches,
            string ethnicity, string religion,
            string relationship, string orientation, string education,
            string drink, string smoke,
            string interest,
            string filter, string orderBy, int pageNumber, int pageSize);

        //int UpdateUser(User user);
        int UpdateUserSecurity(User user);

        NotificationPreferences GetUserNotificationPreferences(int userId);
        int UpdateUserNotificationPreferences(NotificationPreferences notificationPreferences);

        Preferences GetUserPreferences(int userId);
        int UpdateUserPreferences(Preferences preferences);

        UserBalances GetUserBalances(int userId);
        UserBalances GetUserBalancesCached(int userId);


        Friend GetFriend(int userId, int friendId);
        int AcceptFriend(int userId, int friendId);
        int UpdateFriendRequestSentDate(int userId, int friendId);
        int DeleteFriend(int userId, int friendId);
        PagedList<Friend> GetFriends(int userId, int friendId, string filter, string orderby, int pageNumber, int pageSize, bool bShowMature, string userNameFilter);
        List<Friend> GetFriendsFast(int userId, int friendId, string filter, string orderby, int pageNumber, int pageSize);

        FriendGroup GetFriendGroup(int friendGroupId, int ownerId);
        int InsertFriendGroup(FriendGroup group);
        PagedList<Friend> GetFriendsInGroup(int friendGroupId, int ownerId, string filter, string orderby, int pageNumber, int pageSize);
        PagedList<User> SearchUsers(bool onlyAccessPass, bool showMature, string searchString, bool male, bool female, bool bOnlyPhoto,
            int ageFrom, int ageTo,
            string country, string zipCode, int zipMiles, int newWithinDays, int updatedWithinDays,
            int heightFromFeet, int heightFromInches, int heightToFeet, int heightToInches,
            string ethnicity, string religion,
            string relationship, string orientation, string education,
            string drink, string smoke,
            string interest, bool bOnlineNow, string hometown,
            string filter, string orderBy, int pageNumber, int pageSize);
        PagedList<Friend> SearchFriends(int userId, string filter, string orderby, int pageNumber, int pageSize);
        PagedList<Friend> GetFriendsRecommended(string country, string zipCode, int age, string orderBy, int pageNumber, int pageSize);

        int InsertFriendRequest(int userId, int friendId);
        DataRow GetFriendRequest(int userId, int friendId);
        int DeleteFriendRequest(int userId, int friendId);
        int DenyFriend(int userId, int friendId);
        PagedList<FriendDates> GetFriendBirthdays(int userId, string filter, string orderBy, int pageSize, int pageNumber);
        PagedList<FriendDates> GetFriendAnniversaries(int userId, string filter, string orderBy, int pageSize, int pageNumber);
        PagedList<FriendDates> GetUsersBirthdaysWithFriendsList(string timeSpanOccuringInNextNumDays, string timeSpanOffsetLastLoggedIn, string orderBy, int pageSize, int pageNumber);

        int GetUserIdFromUsername(string username);
        int GetUserHomeCommunityIdByUserId(int userId);
        int GetUserHomeCommunityIdByPlayerId(int playerId);
        string GetUserName(int userId);
        int AdjustUserBalance(int userId, string keiPointId, Double amount, UInt16 transType, ref int wok_transaction_log_id);
        int AdjustUserBalanceWithDetails(int userId, string keiPointId, Double amount, UInt16 transType, int globalId, int quantity, ref int wok_transaction_log_id);

        int InsertUser(string username, string password, string salt, int role, int accountType,
            string first_name, string last_name, string display_name, string gender, string homepage, string email, DateTime birthdate,
            string key_value, string country, string zipCode, string registrationKey, string ipAddress, int validationStatus, int joinSourceId);

        void InvalidateKanevaUserCache(int userId);
        void UpdateUserAccount(int userId, string firstName, string lastName, string email);
        void UpdateUserAccount(int userId, string displayName, string email, string country, string zipCode);
        int UpdateUserName(int userId, string oldName, string newName);

        int InsertMessage(Message message);
        bool IsUserBlocked(int userId, int blockedUserId);
        bool IsUserBlocked(int userId, int gameId, int communityId);
        int ChangePlayerNameColor(int userId, int color);

        bool UsernameExists(string username);
        bool EmailExists(string email);

        Crew GetCrew(int userId);
        int AddCrew(Crew crew);
        int AddCrewPeep(CrewPeep crewPeep);

        bool IsUserOnlineInKIM(int userId);

        void InsertRewardLog(int userId, double amount, string keiPointId, int transTypeId, int rewardEventId, int wokTransLogId);
        void AddItemToPendingInventory(int userId, string inventory_type, int global_id, int quantity, int inventorySubType, bool suppressInventoryFetch = false, bool propagateQueryExceptions = false);
        void AddItemToGameInventoriesSync(int gameId, int userId, string inventory_type, int global_id, int quantity, int inventorySubType);


        DataRow GetGameInventoriesSync(int gameId, int userId, string inventory_type, int global_id, int inventorySubType);
        int CleanUpRegistrationIssue(string username);

        IList<State> GetStates();
        IList<Country> GetCountries();

        int InsertPlayerTitle(int playerId, string title, int sortOrder);
        void UpdatePlayerTitle(int playerId, string title);
        int InsertUserSourceAcquisitionUrl(int userId, string fullUrl, string hostName, int sourceTypeId);
        string GetPlayerTitleByFameType(int userId, int fameTypeId);
        int GetPlayerLoginCount(int userId);
        DataRow GetUserLastLoginByGame(int userId, int gameId);
        DateTime GetUserFirstWokLogin(int userId);
        Dictionary<int, bool> AreUsersOnlineInWoK(int[] userIds);

        int LogSearch(int userId, string searchString, int searchType);
        int SearchConvert(int logId);
        PagedList<UserLoginIssue> GetLoginAttempts(DateTime dtStartDate, DateTime dtEndDate, string orderBy, int pageNumber, int pageSize);
        int InsertUserLoginIssue(int userId, string ipAddress, string description, string errorType, string serverType);

        int InsertKIMLoginLog(int userId, string servername, string ipAddress);

        DataSet GetPlayerData(int kanevaUserId, int flags, int gameId);
        DataSet GetPlayListData(int zoneIndex, int zoneInstanceId, int playListId, int playerId);
        void GetPlayerAdmin(int kanevaUserId, out bool isAdmin, out bool isGm, out DateTime secondTolastLogin);
        PagedDataTable GetUserLoginHistory(int userId, string orderby, int pageNumber, int pageSize);

        PagedList<UserMessage> GetUserMessages(int userId, string filter, string orderby, int pageNumber, int pageSize);
        PagedList<UserMessage> GetSentMessages(int userId, string filter, string orderby, int pageNumber, int pageSize);
        UserMessage GetUserMessage(int userId, int messageId);

        FraudDetect GetFraudDetect(int userId);
        void SaveFraudDetect(int userId, FraudDetect fraudDetect);

        // **********************************************************************************************
        // Update User Functions
        // **********************************************************************************************
        #region UpdateUser Functions

        IList<KanevaColor> GetAvailableNameColors();
        int GetUsersNameColor(int wokPlayerId);
        DataRow GetUsersNameColorRGB(int userId);
        int UpdateUsersNameColor(int userId, int nameColorId);

        void UpdateUserEmailSecurity(int userId, string showEmail);
        void UpdateUser(int userId, int showMature, int browseAnon, int showOnline, string newsletter,
            int notifyBlogComments, int notifyProfileComments, int notifyFriendMessages, int notifyAnyoneMessages,
            int notifyFriendRequests, int notifyNewFriends, int friendsCanComment, int everyoneCanComment, int matureProfile, int receiveUpdates);
        void UpdateUser(int userId, int showMature);
        void UpdateUserNoCommunication(int userId, string keyvalue);
        void UpdateBlastPreferences(int userId, int blastPreferences, int blastShowPreferences);
        int AddBlastBlock(int userId, int blockedUserId, int blockedCommunityId);
        int DeleteBlastBlock(int userId, int blockedUserId, int blockedCommunityId);
        PagedList<BlastBlock> GetBlastBlocks(int userId, string filter, string orderby, int pageNumber, int pageSize);
        bool IsBlastBlocked(int userId, int blockedUserId, int blockedCommunityId);
        void UpdateUserIdentityCSR(int userId, string firstName, string lastName, string description, string gender,
            string homepage, string email, bool bShowEmail, bool bShowMature, string country, string zipCode, DateTime birthDate, string displayName);
        void UpdateUserStatus(int userId, int userStatus, bool propagateTransExceptions = false);
        int UpdatePassword(int userId, string password, string salt);
        int UpdateLastLogin(int userId, string ipAddress, string serverName);
        void UpdateOnlineStatus(int userId, int online, string uState, int loginId);
        void UpdateLastIP(int userId, string ipAddress);
        void UpdateFailedLogins(int userId);
        void UpdateUserRestriction(int userId, int matureProfile);
        void UpdateUserEmailStatus(int userId, int emailStatus);
        void UpdateUser(int userId, string description);
        int UpdateUserHomeCommunityId(int userId, int communityId);
        int DeleteBlockedUser(int userId, int profileId);
        int InsertUserToBlock(int userId, int profileId, int blockSourceId);
        PagedDataTable GetBlockedUsers(int userId, string orderBy, int pageNumber, int pageSize);

        #endregion


        #region User Profile/Personal Info

        DataRow GetUserPersonalInfo(int userId);
        DataRow GetUserProfile(int userId);
        bool DoesUserProfileExist(int userId);
        int AddNewUserProfile(int userId);
        int UpdateUserProfile(int userId, string relationship, string orientation, string religion, string ethnicity, string children, string education,
            string income, int heightFeet, int heightInches, string smoke, string drink, string hometown);

        #endregion

        DataRow GetUserIdPlayerIdFromUsername(string username);
        int GetPersonalChannelId(int userId);

        FacebookSettings GetUserFacebookSettings(int userId);
        User GetUserByFacebookUserId(UInt64 fbUserId);
        PagedList<User> GetUsersByFacebookUserId(int userId, string idList, string orderby, int pageNumber, int pageSize);
        int SetFacebookAccessToken(UInt64 fbUserId, string token);
        int SetUseFacebookProfilePic(int userId, bool useProfilePic);
        int ConnectUserToFacebook(int userId, UInt64 fbUserId, bool useFBProfilePic, string accessToken);
        int DisconnectUserFromFacebook(int userId);
        bool IsFacebookUserAlreadyConnected(UInt64 fbUserId);
        IList<UInt64> GetFacebookUserIdForConnectedUsers(string fbUserIdList);
        void InvalidateKanevaUserFacebookCache(int userId);

        // Tracking
        int InsertTrackingRequest(string requestId, int requestTypeId, int userIdSent, int gameId, int communityId);
        int InsertTrackingTypeSent(string requestTypeSentId, string requestId, int messageTypeId, int userId);
        int AcceptTrackingRequest(string requestTypeSentId, int userId, int acceptTypeId = 0, char isIE = 'N', string userAgent = "");
        int GetTrackingRequestType(string requestTypeSentId);

        // System Configuration
        List<User> GetSystemUsers(string systemId);
        Graph<int, User> GetUserAltsGraph(int passedUserId);
        UserSystem GetUserSystem(string systemId);
        List<UserSystem> GetUserSystems(int userId);
        bool InsertUserSystem(string systemId, bool propagateExceptions = false);
        bool InsertUsersSystemIds(int userId, string systemId);
        IList<UserSystemConfiguration> GetUserSystemConfiguationsBySystemName(string systemName);
        UserSystemConfiguration GetUserSystemConfiguation(string systemConfigurationId);
        UserSystemConfiguration GetUserSystemConfiguation(string systemId, uint? cpus, uint? cpuMhz, ulong? sysMem, ulong? diskSize, uint? gpuX, uint? gpuY,
            uint? gpuHz, uint? gpuFmt, string dxVer, string cpuVer, string sysVer, string sysName, string net, string netMAC, string netType, string netHash, string gpu);
        UserSystemConfiguration GetMostRecentConfigForSystem(string systemId);
        UserSystemConfiguration InsertUserSystemConfiguration(string systemId, uint? cpus, uint? cpuMhz, ulong? sysMem, ulong? diskSize, uint? gpuX, uint? gpuY,
            uint? gpuHz, uint? gpuFmt, string dxVer, string cpuVer, string sysVer, string sysName, string net, string netMAC, string netType, string netHash, string gpu,
            uint? cpuPhysCore, string cpuVendor, string cpuBrand, uint? cpuFamily, uint? cpuModel, uint? cpuStepping,
            uint? cpuSSE3, uint? cpuSSSE3, uint? cpuSSE4_1, uint? cpuSSE4_2, uint? cpuSSE4_A, uint? cpuSSE5, uint? cpuAVX, uint? cpuAVX2, uint? cpuFMA3, uint? cpuFMA4, string cpuCodeName);
        int UpdateSystemConfigurationLastUserId(string runtimeId);
        int InsertUserAgentData(int userId, string os, string browser, string useragent);

        // Suspensions
        List<UserSuspension> GetUserSuspensions(int userId, bool activeOnly = false);
        Dictionary<int, List<UserSuspension>> GetUserSuspensions(IEnumerable<int> userIds, bool activeOnly = false);
        Dictionary<int, List<UserSuspension>> GetUserSuspensionsBySystemBanId(int systemBanId, bool activeOnly = false);
        int SaveUserSuspension(int userCurrentStatus, UserSuspension suspension, int banningUserId, string banNote, bool propagateTransExceptions = false);
        int DeleteUserSuspension(int banId, int userId, bool propagateTransExceptions = false);

        // System suspensions
        List<UserSystemSuspension> GetUserSystemSuspensions(string systemId, bool activeOnly = false);
        int SaveUserSystemSuspension(UserSystemSuspension systemSuspension, int banningUserId);
        int DeleteUserSystemSuspension(int banId);

        // Location in WoK
        DataRow GetUserLoggedInWorld(int userId);

        DataRow GetInvitePointAwards(int invitePointId);

        // Meta game item conversions
        UserMetaGameItemBalances GetUserMetaGameItemBalances(int userId);
        bool SaveUserMetaGameItemBalances(UserMetaGameItemBalances balances, bool propagateTransExceptions = false);
        int GetUserTotalMGIConversions(int userId, string itemType, DateTime startDate, DateTime endDate, string currencyType = null);
        int GetUserTotalMGIConversionAmount(int userId, string itemType, DateTime startDate, DateTime endDate, string currencyType = null);
        bool ConvertMetaGameItemToCurrency(int userId, string itemType, string currencyType, int numItemsConverted, int currencyAmountReceived,
            UserMetaGameItemBalances metaGameItemBalances, bool propagateTransExceptions = false);
        UserMetaGameItemRoyalties GetUserMetaGameItemRoyalties(int userId, string itemType);
        bool SpendMetaGameItems(int userId, string itemType, uint numItemsSpent, uint totalRewardsValue,
            int zoneInstanceId, int zoneType, string gameItemName, string gameItemType,
            int worldOwnerId, UserMetaGameItemBalances metaGameItemBalances, bool propagateTransExceptions = false);

        bool HasUserCompletedAvatarSelect(int wokPlayerId);

        int InsertUserNotificationProfile(NotificationsProfile notifcationsProfile);
        List<NotificationsProfile> GetNotificationsProfile(int userId);
        int DismissNotficationProfile(int userId, uint notificationId);

        void InsertUserNote(int userId, int createdUserId, string note);
        void InsertUserNote(int userId, int createdUserId, string note, int wokTransLogId);

        string InsertDeveloperAuthorizationDropBox(int userId, string systemName, byte[] content, uint secondsToLive);
        byte[] GetDeveloperAuthorizationFromDropBox(int userId, string systemName, string uuid);
        void CleanupDeveloperAuthorizationDropBox();
    }
}
