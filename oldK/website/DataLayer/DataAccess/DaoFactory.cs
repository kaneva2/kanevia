///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.DataLayer.DataObjects
{
    /// <summary>
    /// Abstract factory class that creates data access objects.
    /// 
    /// GoF Design Pattern: Factory.
    /// </summary>
    public abstract class DaoFactory
    {
        /// <summary>
        /// Gets a Event data access object.
        /// </summary>
        public abstract IEventDao EventDao { get; }
        
        /// <summary>
        /// Gets a game development company data access object.
        /// </summary>
        public abstract IPromotionsDao PromotionsDao { get; }

        /// <summary>
        /// Gets a violation data access object.
        /// </summary>
        public abstract IViolationsDao ViolationsDao { get; }

        /// <summary>
        /// Gets a game development company data access object.
        /// </summary>
        public abstract IDevelopmentCompanyDao DevelopmentCompanyDao { get; }

        /// <summary>
        /// Gets a game developer data access object.
        /// </summary>
        public abstract IGameDeveloperDao GameDeveloperDao { get; }

        /// <summary>
        /// Gets a user data access object.
        /// </summary>
        public abstract IUserDao UserDao { get; }

        /// <summary>
        /// Gets a site management data access object.
        /// </summary>
        public abstract ISiteManagementDao SiteManagementDao { get; }

        /// <summary>
        /// Gets a site security data access object.
        /// </summary>
        public abstract ISiteSecurityDao SiteSecurityDao { get; }

        /// <summary>
        /// Gets an interest data access object.
        /// </summary>
        public abstract IInterestsDao InterestsDao { get; }

        /// <summary>
        /// Gets a Comments data access object.
        /// </summary>
        public abstract ICommentsDao CommentsDao { get; }

        /// <summary>
        /// Gets a Blast data access object.
        /// </summary>
        public abstract IBlastDao BlastDao { get; }

        /// <summary>
        /// Gets a Community data access object.
        /// </summary>
        public abstract ICommunityDao CommunityDao { get; }

        /// <summary>
        /// Gets a Contest data access object.
        /// </summary>
        public abstract IContestDao ContestDao { get; }

        /// <summary>
        /// Gets a Blog data access object.
        /// </summary>
        public abstract IBlogDao BlogDao { get; }

        /// <summary>
        /// Gets a Forum data access object.
        /// </summary>
        public abstract IForumDao ForumDao { get; }

        /// <summary>
        /// Gets a Fame data access object.
        /// </summary>
        public abstract IFameDao FameDao { get; }

        /// <summary>
        /// Gets a Game data access object.
        /// </summary>
        public abstract IGameDao GameDao { get; }

        /// <summary>
        /// Gets a Game Server data access object.
        /// </summary>
        public abstract IGameServerDao GameServerDao { get; }

        /// Gets a Game Server data access object.
        /// </summary>
        public abstract IGameLicenseDao GameLicenseDao { get; }

        /// Gets a World Template data access object.
        /// </summary>
        public abstract IWorldTemplateDao WorldTemplateDao { get; }

        /// <summary>
        /// Gets a Error Logging data access object.
        /// </summary>
        public abstract IErrorLoggingDao ErrorLoggingDao { get; }

        /// <summary>
        /// Gets a Metrics data access object.
        /// </summary>
        public abstract IMetricsDao MetricsDao { get; }

        /// <summary>
        /// Gets a Media data access object.
        /// </summary>
        public abstract IMediaDao MediaDao { get; }

        /// <summary>
        /// Gets a Shopping data access object.
        /// </summary>
        public abstract IShoppingDao ShoppingDao { get; }

        /// Gets a Script Game Item data access object.
        /// </summary>
        public abstract IScriptGameItemDao ScriptGameItemDao { get; }

        /// Gets a Script Game Cusom Data data access object.
        /// </summary>
        public abstract ISGCustomDataDao SGCustomDataDao { get; }

        /// Gets a Script Game Framework Settings data access object.
        /// </summary>
        public abstract ISGFrameworkSettingsDao SGFrameworkSettingsDao { get; }

        /// <summary>
        /// Gets a Kaching data access object.
        /// </summary>
        public abstract IKachingDao KachingDao { get; }
        
        /// <summary>
        /// Gets a Marketing data access object.
        /// </summary>
        public abstract IMarketingDao MarketingDao { get; }

        /// <summary>
        /// Gets a Transaction data access object.
        /// </summary>
        public abstract ITransactionDao TransactionDao { get; }

        /// <summary>
        /// Gets a Subscription data access object.
        /// </summary>
        public abstract ISubscriptionDao SubscriptionDao { get; }

        /// <summary>
        /// Gets a Configuration data access object.
        /// </summary>
        public abstract IConfigurationDao ConfigurationDao { get; }

        /// <summary>
        /// Gets a Rave data access object.
        /// </summary>
        public abstract IRaveDao RaveDao { get; }

		/// <summary>
		/// Gets a Mail data access object.
		/// </summary>
		public abstract IMailDao MailDao { get; }

        /// <summary>
        /// Gets a Mail data access object.
        /// </summary>
        public abstract ISitemapDao SitemapDao { get; }

        /// <summary>
        /// Gets a Mail data access object.
        /// </summary>
        public abstract IExperimentDao ExperimentDao { get; }
    }
}
