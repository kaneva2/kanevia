///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IMediaDao
    {
        Asset GetAsset(int assetId, int userId);
        Asset GetNewAssetInformation(int assetId);
        int UpdateAssetRestriction(int assetId, int assetRatingId, bool isMature);
        int UpdateAssetInSearch(int assetId);
        int DeleteAsset(Asset asset);
        int UpdateAssetStatus(Asset asset, int statusId);
        int DeleteAssetFromChannel(Asset asset);
        int DeleteAssetFromSearch(Asset asset);

        int UpdateMediaViews(int assetId, string userIpAddress, int userId);
        PagedList<AssetView> GetUserViewHistory(int userId, string orderBy, int pageNumber, int pageSize);
        void ShareAsset(int assetId, int fromUserId, string fromIp, string toEmail, int toUserId);
        void SharedAssetClicked(string keyValue);

        // Searching
        PagedList<Asset> SearchMedia(bool bGetMature, int assetTypeId, bool bGetPrivate, bool bThumbnailRequired, string searchString,
            string categories, int pastDays, string orderBy, int pageNumber, int pageSize, bool onlyAccessPass, List<int> iAssetList);
        PagedList<Asset> SearchMedia(bool bGetMature, List<int> assetTypeId, bool bGetPrivate, bool bThumbnailRequired, string searchString,
            string categories, int pastDays, string orderBy, int pageNumber, int pageSize, bool onlyAccessPass, List<int> iAssetList);

        PagedList<Asset> BrowseMedia(string sphinxIndex, bool bGetMature, int assetTypeId, bool bGetPrivate, bool bThumbnailRequired,
            string categories, int pastDays, string orderBy, int pageNumber, int pageSize, bool onlyAccessPass);

        PagedList<Asset> GetAccessibleAssetsInCommunity (int communityId, int assetGroupId, bool bGetMature,
            int assetTypeId, int userId, string filter, string orderBy, int pageNumber, int pageSize);

        // Category info
        int GetAssetCategoryIdByName(int assetTypeId, string catName);

        // Asset groups/playlist
        PagedList<AssetGroup> GetAssetGroups(int channelId, string filter, string orderby, int pageNumber, int pageSize);
        PagedList<AssetGroup> GetAssetGroups(int channelId, string assetGroupIds);
        bool IsAssetInGroup(int assetGroupId, int assetId);
        int InsertAssetGroup(int channelId, string groupName, string groupDescription, int shuffle);
        int DeleteAssetGroup(int channelId, int assetGroupId);
        int DeleteAllAssetsFromGroup(int assetGroupId);
        AssetGroup GetAssetGroup(int assetGroupId, int channelId);
        List<int> GetGroupIdsWithAsset(int assetId);
        List<int> GetGroupIdsWithAssetByChannel(int assetId, int channelId);
        PagedList<AssetGroup> GetGroupCategoryAssetGroups(int groupCategoryId, int page, int pageSize);
        int InsertAssetInGroup(int channelId, int assetGroupId, int assetId);
        int UpdateAssetGroupAssetSortOrder(int assetId, int sortOrder, int assetGroupId);
        int MoveAssetToTopOfGroup(int assetGroupId, int assetId);
        int MoveAssetToBottomOfGroup(int assetGroupId, int assetId);
        int RemoveAssetFromGroup(int assetGroupId, int assetId);
        int RemoveAssetFromAllGroups(int assetId);
        int RemoveAssetFromGroupsByChannel(int channelId, int assetId);
        PagedList<AssetGroupAsset> GetWokPlaylist(int assetGroupId);

        List<int> GetUserUploadedAssetIds(int userId);
        PagedList<Asset> GetUserUploadedAssets(int userId, List<int> assetTypeIds, string orderBy, int pageNum, int itemsPerPage);
    }
}

