///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Data;
using System.Linq;

namespace Kaneva.DataLayer.DataObjects.Mock
{
    public class MockUserDao : IUserDao
    {
        private MockDatabase _mockDatabase;

        public MockUserDao()
        {
            _mockDatabase= new MockDatabase();
        }

        // InitTable
        private MockDatabaseTable InitTable(string tableName)
        {
            switch (tableName)
            {
                case "user_preferences":
                    MockDatabaseTable userPreferences = new MockDatabaseTable("user_preferences",
                        new DataColumn[] 
                        {
                            new DataColumn("user_id", typeof(int)),
                            new DataColumn("kei_point_id_purchase_type", typeof(string)),
                            new DataColumn("filter_by_country", typeof(bool))
                        },
                        new string[] { "user_id" });
                    _mockDatabase.CreateTableIfNotExists(userPreferences);
                    break;
                case "user_balances":
                    MockDatabaseTable userBalances = new MockDatabaseTable("user_balances",
                        new DataColumn[] 
                        {
                            new DataColumn("user_id", typeof(int)),
                            new DataColumn("kei_point_id", typeof(double)),
                            new DataColumn("balance", typeof(decimal))
                        },
                        new string[] { "user_id" });
                    _mockDatabase.CreateTableIfNotExists(userBalances);
                    break;
                case "user_facebook_settings":
                    MockDatabaseTable userFacebookSettings = new MockDatabaseTable("user_facebook_settings",
                        new DataColumn[] 
                        {
                            new DataColumn("user_id", typeof(int)),
                            new DataColumn("fb_user_id", typeof(int)),
                            new DataColumn("access_token", typeof(string)),
                            new DataColumn("use_facebook_profile_picture", typeof(bool)),
                            new DataColumn("created_date", typeof(DateTime))
                        },
                        new string[] { "user_id" });
                    _mockDatabase.CreateTableIfNotExists(userFacebookSettings);
                    break;
                case "user_notification_preferences":
                    MockDatabaseTable userNotificationPreferences = new MockDatabaseTable("user_notification_preferences",
                        new DataColumn[] 
                        {
                            new DataColumn("user_id", typeof(int)),
                            new DataColumn("notify_blast_comments", typeof(bool)),
                            new DataColumn("notify_media_comments", typeof(bool)),
                            new DataColumn("notify_shop_comments_purchases", typeof(bool)),
                            new DataColumn("notify_event_invites", typeof(bool)),
                            new DataColumn("notify_friend_birthdays", typeof(bool)),
                            new DataColumn("notify_world_blasts", typeof(bool)),
                            new DataColumn("notify_world_requests", typeof(bool))
                        },
                        new string[] { "user_id" });
                    _mockDatabase.CreateTableIfNotExists(userNotificationPreferences);
                    break;
                case "users":
                    MockDatabaseTable users = new MockDatabaseTable("users",
                        new DataColumn[] 
                        {
                            new DataColumn("user_id", typeof(int)),
                            new DataColumn("username", typeof(string)),
                            new DataColumn("first_name", typeof(string)),
                            new DataColumn("last_name", typeof(string)),
                            new DataColumn("email", typeof(string)),
                            new DataColumn("home_community_id", typeof(int))
                        },
                        new string[] { "user_id" });
                    users.SetColumnAutoIncrement("user_id", true);
                    _mockDatabase.CreateTableIfNotExists(users);
                    break;
                case "friend_groups":
                    MockDatabaseTable friendGroups = new MockDatabaseTable("friend_groups",
                        new DataColumn[] 
                        {
                            new DataColumn("friend_group_id", typeof(int)),
                            new DataColumn("owner_id", typeof(int)),
                            new DataColumn("name", typeof(string)),
                            new DataColumn("created_date", typeof(DateTime)),
                            new DataColumn("friend_count", typeof(int))
                        },
                        new string[] { "friend_group_id" });
                    friendGroups.SetColumnAutoIncrement("friend_group_id", true);
                    _mockDatabase.CreateTableIfNotExists(friendGroups);
                    break;
                default:
                    throw new ArgumentException("Unable to initialize table with name " + tableName, "tableName");
            }

            return _mockDatabase.GetTable(tableName);
        }
        
        
        public User GetUser(int userId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataRow GetUserObsoleteForWok(int userId)
        {
            throw new NotImplementedException();
        }

        public User GetUser(string userName)
        {
            throw new NotImplementedException();
        }

        public User GetUserByEmail(string userName)
        {
            throw new NotImplementedException();
        }

        public PagedList<User> GetUsersByEmail(List<string> emails)
        {
            throw new NotImplementedException();
        }

        public User GetCommunityOwnerByCommunityId(int communityId)
        {
            throw new NotImplementedException();
        }

        public UserStats GetUserStats(int userId)
        {
            throw new NotImplementedException();
        }

        public PagedList<User> GetUsersList(string filter, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public PagedList<User> GetUsersList(string username, string email, string firstName, string lastName, int userStatus, string filter, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public PagedList<User> SearchUsers(bool onlyAccessPass, bool showMature, string searchString, bool male, bool female, bool bOnlyPhoto, int ageFrom, int ageTo, string country, string zipCode, int zipMiles, int newWithinDays, int updatedWithinDays, int heightFromFeet, int heightFromInches, int heightToFeet, int heightToInches, string ethnicity, string religion, string relationship, string orientation, string education, string drink, string smoke, string interest, string filter, string orderBy, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public int UpdateUserSecurity(User user)
        {
            throw new NotImplementedException();
        }

        // GetUserNotificationPreferences 
        public NotificationPreferences GetUserNotificationPreferences(int userId)
        {
            MockDatabaseTable table = InitTable("user_notification_preferences");

            try
            {
                DataRow[] results = table.SelectRecords("user_id = " + userId.ToString());
                NotificationPreferences unp = new NotificationPreferences();
                if (results.Length == 1)
                {
                    unp.UserId = (int)results[0]["user_id"];
                    unp.NotifyBlastComments = Convert.ToBoolean(results[0]["notify_blast_comments"]);
                    unp.NotifyEventInvites = Convert.ToBoolean(results[0]["notify_event_invites"]);
                    unp.NotifyFriendBirthdays = Convert.ToBoolean(results[0]["notify_friend_birthdays"]);
                    unp.NotifyMediaComments = Convert.ToBoolean(results[0]["notify_media_comments"]);
                    unp.NotifyShopCommentsPurchases = Convert.ToBoolean(results[0]["notify_shop_comments_purchases"]);
                    unp.NotifyWorldBlasts = Convert.ToBoolean(results[0]["notify_world_blasts"]);
                    unp.NotifyWorldRequests = Convert.ToBoolean(results[0]["notify_world_requests"]);
                }
                return unp;
            }
            catch (Exception) { return new NotificationPreferences(); }
        }

        // UpdateUserNotificationPreferences
        public int UpdateUserNotificationPreferences(NotificationPreferences notificationPreferences)
        {
            MockDatabaseTable table = InitTable("user_notification_preferences");

            try
            {
                DataRow row = table.GenerateRow();
                row["user_id"] = notificationPreferences.UserId;
                row["notify_blast_comments"] = notificationPreferences.NotifyBlastComments;
                row["notify_event_invites"] = notificationPreferences.NotifyEventInvites;
                row["notify_friend_birthdays"] = notificationPreferences.NotifyFriendBirthdays;
                row["notify_media_comments"] = notificationPreferences.NotifyMediaComments;
                row["notify_shop_comments_purchases"] = notificationPreferences.NotifyShopCommentsPurchases;
                row["notify_world_blasts"] = notificationPreferences.NotifyWorldBlasts;
                row["notify_world_requests"] = notificationPreferences.NotifyWorldRequests;
                table.InsertRecord(row);
            }
            catch (Exception) { return 0; }

            return 1;
        }

        // GetUserPreferences
        public Preferences GetUserPreferences(int userId)
        {
            MockDatabaseTable table = InitTable("user_preferences");

            try
            {
                DataRow[] results = table.SelectRecords("user_id = " + userId.ToString());
                Preferences prefs = new Preferences();
                if (results.Length == 1)
                {
                    prefs.UserId =  (int)results[0]["user_id"];
                    prefs.AlwaysPurchaseWithCurrencyType = results[0]["kei_point_id_purchase_type"].ToString();
                    prefs.FilterByCountry = results[0]["filter_by_country"].ToString().Equals("Y");
                }
                return prefs;
            }
            catch (Exception) { return new Preferences(); }
        }

        // UpdateUserPreferences
        public int UpdateUserPreferences(Preferences preferences)
        {
            MockDatabaseTable table = InitTable("user_preferences");

            try
            {
                DataRow[] records = table.SelectRecords("user_id=" + preferences.UserId);
                if (records.Length == 0)
                {
                    DataRow row = table.GenerateRow();
                    row["user_id"] = preferences.UserId;
                    row["kei_point_id_purchase_type"] = preferences.AlwaysPurchaseWithCurrencyType;
                    row["filter_by_country"] = preferences.FilterByCountry;
                    table.InsertRecord(row);
                }
                else
                {
                    Dictionary<string, object> dict = new Dictionary<string, object>();
                    dict.Add("user_id",preferences.UserId);
                    dict.Add("kei_point_id_purchase_type",preferences.AlwaysPurchaseWithCurrencyType);
                    dict.Add("filter_by_country", preferences.FilterByCountry);
                    table.UpdateRecord("user_id=" + preferences.UserId, dict);
                }
            }
            catch (Exception) { return 0; }

            return 1;
        }

        // GetUserBalances
        public UserBalances GetUserBalances(int userId)
        {
            MockDatabaseTable table = InitTable("user_balances");

            try
            {
                DataRow[] results = table.SelectRecords("user_id = " + userId.ToString());
                UserBalances ub = new UserBalances();
                foreach (DataRow row in results)
                {
                    switch (row["kei_point_id"].ToString())
                    {
                        case Currency.CurrencyType.CREDITS:
                            ub.KPoint = Convert.ToDouble(row["balance"]);
                            break;

                        case Currency.CurrencyType.REWARDS:
                            ub.GPoint = Convert.ToDouble(row["balance"]);
                            break;

                        case Currency.CurrencyType.MPOINT:
                            ub.MPoint = Convert.ToDouble(row["balance"]);
                            break;

                        case Currency.CurrencyType.DOLLAR:
                            ub.Dollar = Convert.ToDouble(row["balance"]);
                            break;
                    }
                }
                return ub;
            }
            catch (Exception) { return new UserBalances(); }
        }

        public UserBalances GetUserBalancesCached(int userId)
        {
            throw new NotImplementedException();
        }

        public Friend GetFriend(int userId, int friendId)
        {
            throw new NotImplementedException();
        }

        public int AcceptFriend(int userId, int friendId)
        {
            throw new NotImplementedException();
        }

        public int UpdateFriendRequestSentDate(int userId, int friendId)
        {
            throw new NotImplementedException();
        }

        public int DeleteFriend(int userId, int friendId)
        {
            throw new NotImplementedException();
        }

        public PagedList<Friend> GetFriends(int userId, int friendId, string filter, string orderby, int pageNumber, int pageSize, bool bShowMature, string userNameFilter)
        {
            throw new NotImplementedException();
        }

        public List<Friend> GetFriendsFast(int userId, int friendId, string filter, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public FriendGroup GetFriendGroup(int friendGroupId, int ownerId)
        {
            throw new NotImplementedException();
        }

        // InsertFriendGroup
        public int InsertFriendGroup(FriendGroup group)
        {
            MockDatabaseTable table = InitTable("friend_groups");

            try
            {
                DataRow row = table.GenerateRow();
                row["owner_id"] = group.OwnerId;
                row["name"] = group.Name;
                row["created_date"] = DateTime.Now;
                table.InsertRecord(row);
            }
            catch (Exception) { return 0; }

            return 1;
        }

        public PagedList<Friend> GetFriendsInGroup(int friendGroupId, int ownerId, string filter, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public PagedList<User> SearchUsers(bool onlyAccessPass, bool showMature, string searchString, bool male, bool female, bool bOnlyPhoto, int ageFrom, int ageTo, string country, string zipCode, int zipMiles, int newWithinDays, int updatedWithinDays, int heightFromFeet, int heightFromInches, int heightToFeet, int heightToInches, string ethnicity, string religion, string relationship, string orientation, string education, string drink, string smoke, string interest, bool bOnlineNow, string hometown, string filter, string orderBy, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public PagedList<Friend> SearchFriends(int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public PagedList<Friend> GetFriendsRecommended(string country, string zipCode, int age, string orderBy, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public int InsertFriendRequest(int userId, int friendId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataRow GetFriendRequest(int userId, int friendId)
        {
            throw new NotImplementedException();
        }

        public int DeleteFriendRequest(int userId, int friendId)
        {
            throw new NotImplementedException();
        }

        public int DenyFriend(int userId, int friendId)
        {
            throw new NotImplementedException();
        }

        public PagedList<FriendDates> GetFriendBirthdays(int userId, string filter, string orderBy, int pageSize, int pageNumber)
        {
            throw new NotImplementedException();
        }

        public PagedList<FriendDates> GetFriendAnniversaries(int userId, string filter, string orderBy, int pageSize, int pageNumber)
        {
            throw new NotImplementedException();
        }

        public PagedList<FriendDates> GetUsersBirthdaysWithFriendsList(string timeSpanOccuringInNextNumDays, string timeSpanOffsetLastLoggedIn, string orderBy, int pageSize, int pageNumber)
        {
            throw new NotImplementedException();
        }

        public int GetUserIdFromUsername(string username)
        {
            throw new NotImplementedException();
        }

        public int GetUserHomeCommunityIdByUserId(int userId)
        {
            throw new NotImplementedException();
        }

        public int GetUserHomeCommunityIdByPlayerId(int playerId)
        {
            throw new NotImplementedException();
        }

        public string GetUserName(int userId)
        {
            throw new NotImplementedException();
        }

        public int AdjustUserBalance(int userId, string keiPointId, double amount, ushort transType, ref int wok_transaction_log_id)
        {
            throw new NotImplementedException();
        }

        public int AdjustUserBalanceWithDetails(int userId, string keiPointId, double amount, ushort transType, int globalId, int quantity, ref int wok_transaction_log_id)
        {
            throw new NotImplementedException();
        }

        public int InsertUser(string username, string password, string salt, int role, int accountType, string first_name, string last_name, string display_name, string gender, string homepage, string email, DateTime birthdate, string key_value, string country, string zipCode, string registrationKey, string ipAddress, int validationStatus, int joinSourceId)
        {
            throw new NotImplementedException();
        }

        public void InvalidateKanevaUserCache(int userId)
        {
            throw new NotImplementedException();
        }

        public void UpdateUserAccount(int userId, string firstName, string lastName, string email)
        {
            throw new NotImplementedException();
        }

        public void UpdateUserAccount(int userId, string displayName, string email, string country, string zipCode)
        {
            throw new NotImplementedException();
        }

        public int UpdateUserName(int userId, string oldName, string newName)
        {
            throw new NotImplementedException();
        }

        public int InsertMessage(Message message)
        {
            throw new NotImplementedException();
        }

        public bool IsUserBlocked(int userId, int blockedUserId)
        {
            throw new NotImplementedException();
        }

        public bool IsUserBlocked(int userId, int gameId, int communityId)
        {
            throw new NotImplementedException();
        }

        public int ChangePlayerNameColor(int userId, int color)
        {
            throw new NotImplementedException();
        }

        public bool UsernameExists(string username)
        {
            throw new NotImplementedException();
        }

        public bool EmailExists(string email)
        {
            throw new NotImplementedException();
        }

        public Crew GetCrew(int userId)
        {
            throw new NotImplementedException();
        }

        public int AddCrew(Crew crew)
        {
            throw new NotImplementedException();
        }

        public int AddCrewPeep(CrewPeep crewPeep)
        {
            throw new NotImplementedException();
        }

        public bool IsUserOnlineInKIM(int userId)
        {
            throw new NotImplementedException();
        }

        public void InsertRewardLog(int userId, double amount, string keiPointId, int transTypeId, int rewardEventId, int wokTransLogId)
        {
            throw new NotImplementedException();
        }

        public void AddItemToPendingInventory(int userId, string inventory_type, int global_id, int quantity, int inventorySubType, bool suppressInventoryFetch = false, bool propagateQueryExceptions = false)
        {
            throw new NotImplementedException();
        }

        public void AddItemToGameInventoriesSync(int gameId, int userId, string inventory_type, int global_id, int quantity, int inventorySubType)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataRow GetGameInventoriesSync(int gameId, int userId, string inventory_type, int global_id, int inventorySubType)
        {
            throw new NotImplementedException();
        }

        // CleanUpRegistrationIssue
        public int CleanUpRegistrationIssue(string username)
        {
            MockDatabaseTable table = InitTable("users");
            int result = 0;

            try
            {
                DataRow[] records = table.SelectRecords("username='" + username + "'");
                if (records.Length > 0)
                {
                    table.DeleteRecord(records[0]);
                    result = 1;
                }
            }
            catch (Exception) { }

            return result;
        }

        public IList<State> GetStates()
        {
            throw new NotImplementedException();
        }

        public IList<Country> GetCountries()
        {
            throw new NotImplementedException();
        }

        public int InsertPlayerTitle(int playerId, string title, int sortOrder)
        {
            throw new NotImplementedException();
        }

        public void UpdatePlayerTitle(int playerId, string title)
        {
            throw new NotImplementedException();
        }

        public int InsertUserSourceAcquisitionUrl(int userId, string fullUrl, string hostName, int sourceTypeId)
        {
            throw new NotImplementedException();
        }

        public string GetPlayerTitleByFameType(int userId, int fameTypeId)
        {
            throw new NotImplementedException();
        }

        public int GetPlayerLoginCount(int userId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataRow GetUserLastLoginByGame(int userId, int gameId)
        {
            throw new NotImplementedException();
        }

        public DateTime GetUserFirstWokLogin(int userId)
        {
            throw new NotImplementedException();
        }

        public Dictionary<int, bool> AreUsersOnlineInWoK(int[] userIds)
        {
            throw new NotImplementedException();
        }

        public int LogSearch(int userId, string searchString, int searchType)
        {
            throw new NotImplementedException();
        }

        public int SearchConvert(int logId)
        {
            throw new NotImplementedException();
        }

        public PagedList<UserLoginIssue> GetLoginAttempts(DateTime dtStartDate, DateTime dtEndDate, string orderBy, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public int InsertUserLoginIssue(int userId, string ipAddress, string description, string errorType, string serverType)
        {
            throw new NotImplementedException();
        }

        public int InsertKIMLoginLog(int userId, string servername, string ipAddress)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataSet GetPlayerData(int kanevaUserId, int flags, int gameId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataSet GetPlayListData(int zoneIndex, int zoneInstanceId, int playListId, int playerId)
        {
            throw new NotImplementedException();
        }

        public void GetPlayerAdmin(int kanevaUserId, out bool isAdmin, out bool isGm, out DateTime secondTolastLogin)
        {
            throw new NotImplementedException();
        }

        public PagedDataTable GetUserLoginHistory(int userId, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public PagedList<UserMessage> GetUserMessages(int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public PagedList<UserMessage> GetSentMessages(int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public UserMessage GetUserMessage(int userId, int messageId)
        {
            throw new NotImplementedException();
        }

        public FraudDetect GetFraudDetect(int userId)
        {
            throw new NotImplementedException();
        }

        public void SaveFraudDetect(int userId, FraudDetect fraudDetect)
        {
            throw new NotImplementedException();
        }

        public IList<KanevaColor> GetAvailableNameColors()
        {
            throw new NotImplementedException();
        }

        public int GetUsersNameColor(int wokPlayerId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataRow GetUsersNameColorRGB(int userId)
        {
            throw new NotImplementedException();
        }

        public int UpdateUsersNameColor(int userId, int nameColorId)
        {
            throw new NotImplementedException();
        }

        public void UpdateUserEmailSecurity(int userId, string showEmail)
        {
            throw new NotImplementedException();
        }

        public void UpdateUser(int userId, int showMature, int browseAnon, int showOnline, string newsletter, int notifyBlogComments, int notifyProfileComments, int notifyFriendMessages, int notifyAnyoneMessages, int notifyFriendRequests, int notifyNewFriends, int friendsCanComment, int everyoneCanComment, int matureProfile, int receiveUpdates)
        {
            throw new NotImplementedException();
        }

        public void UpdateUser(int userId, int showMature)
        {
            throw new NotImplementedException();
        }

        public void UpdateUserNoCommunication(int userId, string keyvalue)
        {
            throw new NotImplementedException();
        }

        public void UpdateBlastPreferences(int userId, int blastPreferences, int blastShowPreferences)
        {
            throw new NotImplementedException();
        }

        public int AddBlastBlock(int userId, int blockedUserId, int blockedCommunityId)
        {
            throw new NotImplementedException();
        }

        public int DeleteBlastBlock(int userId, int blockedUserId, int blockedCommunityId)
        {
            throw new NotImplementedException();
        }

        public PagedList<BlastBlock> GetBlastBlocks(int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public bool IsBlastBlocked(int userId, int blockedUserId, int blockedCommunityId)
        {
            throw new NotImplementedException();
        }

        public void UpdateUserIdentityCSR(int userId, string firstName, string lastName, string description, string gender, string homepage, string email, bool bShowEmail, bool bShowMature, string country, string zipCode, DateTime birthDate, string displayName)
        {
            throw new NotImplementedException();
        }

        public void UpdateUserStatus(int userId, int userStatus, bool propagateTransExceptions = false)
        {
            throw new NotImplementedException();
        }

        public int UpdatePassword(int userId, string password, string salt)
        {
            throw new NotImplementedException();
        }

        public int UpdateLastLogin(int userId, string ipAddress, string serverName)
        {
            throw new NotImplementedException();
        }

        public void UpdateOnlineStatus(int userId, int online, string uState, int loginId)
        {
            throw new NotImplementedException();
        }

        public void UpdateLastIP(int userId, string ipAddress)
        {
            throw new NotImplementedException();
        }

        public void UpdateFailedLogins(int userId)
        {
            throw new NotImplementedException();
        }

        public void UpdateUserRestriction(int userId, int matureProfile)
        {
            throw new NotImplementedException();
        }

        public void UpdateUserEmailStatus(int userId, int emailStatus)
        {
            throw new NotImplementedException();
        }

        public void UpdateUser(int userId, string description)
        {
            throw new NotImplementedException();
        }

        // UpdateUserHomeCommunityId
        public int UpdateUserHomeCommunityId(int userId, int communityId)
        {
            MockDatabaseTable table = InitTable("users");

            try
            {
                DataRow[] records = table.SelectRecords("user_id=" + userId);
                if (records.Length == 0)
                {
                    DataRow row = table.GenerateRow();
                    row["user_id"] = userId;
                    row["home_community_id"] = communityId;
                    table.InsertRecord(row);
                }
                else
                {
                    Dictionary<string, object> dict = new Dictionary<string, object>();
                    dict.Add("user_id", userId);
                    dict.Add("home_community_id", communityId);
                    table.UpdateRecord("user_id=" + userId, dict);
                }
            }
            catch (Exception) { return 0; }

            return 1;
        }

        public int DeleteBlockedUser(int userId, int profileId)
        {
            throw new NotImplementedException();
        }

        public int InsertUserToBlock(int userId, int profileId, int blockSourceId)
        {
            throw new NotImplementedException();
        }

        public PagedDataTable GetBlockedUsers(int userId, string orderBy, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataRow GetUserPersonalInfo(int userId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataRow GetUserProfile(int userId)
        {
            throw new NotImplementedException();
        }

        public bool DoesUserProfileExist(int userId)
        {
            throw new NotImplementedException();
        }

        public int AddNewUserProfile(int userId)
        {
            throw new NotImplementedException();
        }

        public int UpdateUserProfile(int userId, string relationship, string orientation, string religion, string ethnicity, string children, string education, string income, int heightFeet, int heightInches, string smoke, string drink, string hometown)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataRow GetUserIdPlayerIdFromUsername(string username)
        {
            throw new NotImplementedException();
        }

        public int GetPersonalChannelId(int userId)
        {
            throw new NotImplementedException();
        }

        public FacebookSettings GetUserFacebookSettings(int userId)
        {
            throw new NotImplementedException();
        }

        public User GetUserByFacebookUserId(ulong fbUserId)
        {
            throw new NotImplementedException();
        }

        public PagedList<User> GetUsersByFacebookUserId(int userId, string idList, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public int SetFacebookAccessToken(ulong fbUserId, string token)
        {
            throw new NotImplementedException();
        }

        public int SetUseFacebookProfilePic(int userId, bool useProfilePic)
        {
            throw new NotImplementedException();
        }

        // ConnectUserToFacebook
        public int ConnectUserToFacebook(int userId, ulong fbUserId, bool useFBProfilePic, string accessToken)
        {
            MockDatabaseTable table = InitTable("user_facebook_settings");

            try
            {
                DataRow row = table.GenerateRow();
                row["user_id"] = userId;
                row["fb_user_id"] = fbUserId;
                row["access_token"] = accessToken;
                row["use_facebook_profile_picture"] = useFBProfilePic;
                row["created_date"] = DateTime.Now;
                table.InsertRecord(row);
            }
            catch (Exception) { return 0; }

            return 1;
        }

        public int DisconnectUserFromFacebook(int userId)
        {
            throw new NotImplementedException();
        }

        public bool IsFacebookUserAlreadyConnected(ulong fbUserId)
        {
            throw new NotImplementedException();
        }

        public IList<ulong> GetFacebookUserIdForConnectedUsers(string fbUserIdList)
        {
            throw new NotImplementedException();
        }

        public void InvalidateKanevaUserFacebookCache(int userId)
        {
            throw new NotImplementedException();
        }

        public int InsertTrackingRequest(string requestId, int requestTypeId, int userIdSent, int gameId, int communityId)
        {
            throw new NotImplementedException();
        }

        public int InsertTrackingTypeSent(string requestTypeSentId, string requestId, int messageTypeId, int userId)
        {
            throw new NotImplementedException();
        }

        public int AcceptTrackingRequest(string requestTypeSentId, int userId, int acceptTypeId = 0, char isIE = 'N', string userAgent = "")
        {
            throw new NotImplementedException();
        }

        public int GetTrackingRequestType(string requestTypeSentId)
        {
            throw new NotImplementedException();
        }

        public List<User> GetSystemUsers(string systemId)
        {
            throw new NotImplementedException();
        }

        public Graph<int, User> GetUserAltsGraph(int passedUserId)
        {
            throw new NotImplementedException();
        }

        public UserSystem GetUserSystem(string systemId)
        {
            throw new NotImplementedException();
        }

        public List<UserSystem> GetUserSystems(int userId)
        {
            throw new NotImplementedException();
        }

        public bool InsertUserSystem(string systemId, bool propagateExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool InsertUsersSystemIds(int userId, string systemId)
        {
            throw new NotImplementedException();
        }

        public IList<UserSystemConfiguration> GetUserSystemConfiguationsBySystemName(string systemName)
        {
            throw new NotImplementedException();
        }

        public UserSystemConfiguration GetUserSystemConfiguation(string systemConfigurationId)
        {
            throw new NotImplementedException();
        }

        public UserSystemConfiguration GetUserSystemConfiguation(string systemId, uint? cpus, uint? cpuMhz, ulong? sysMem, ulong? diskSize, uint? gpuX, uint? gpuY, uint? gpuHz, uint? gpuFmt, string dxVer, string cpuVer, string sysVer, string sysName, string net, string netMAC, string netType, string netHash, string gpu)
        {
            throw new NotImplementedException();
        }

        public UserSystemConfiguration GetMostRecentConfigForSystem(string systemId)
        {
            throw new NotImplementedException();
        }

        public UserSystemConfiguration InsertUserSystemConfiguration(string systemId, uint? cpus, uint? cpuMhz, ulong? sysMem, ulong? diskSize, uint? gpuX, uint? gpuY, uint? gpuHz, uint? gpuFmt, string dxVer, string cpuVer, string sysVer, string sysName, string net, string netMAC, string netType, string netHash, string gpu,
            uint? cpuPhysCore, string cpuVendor, string cpuBrand, uint? cpuFamily, uint? cpuModel, uint? cpuStepping,
            uint? cpuSSE3, uint? cpuSSSE3, uint? cpuSSE4_1, uint? cpuSSE4_2, uint? cpuSSE4_A, uint? cpuSSE5, uint? cpuAVX, uint? cpuAVX2, uint? cpuFMA3, uint? cpuFMA4, string cpuCodeName)
        {
            throw new NotImplementedException();
        }

        public int UpdateSystemConfigurationLastUserId(string runtimeId)
        {
            throw new NotImplementedException();
        }

        public int InsertUserAgentData(int userId, string os, string browser, string useragent)
        {
            throw new NotImplementedException();
        }

        public List<UserSuspension> GetUserSuspensions(int userId, bool activeOnly = false)
        {
            throw new NotImplementedException();
        }

        public Dictionary<int, List<UserSuspension>> GetUserSuspensions(IEnumerable<int> userIds, bool activeOnly = false)
        {
            throw new NotImplementedException();
        }

        public Dictionary<int, List<UserSuspension>> GetUserSuspensionsBySystemBanId(int systemBanId, bool activeOnly = false)
        {
            throw new NotImplementedException();
        }

        public int SaveUserSuspension(int userCurrentStatus, UserSuspension suspension, int banningUserId, string banNote, bool propagateTransExceptions = false)
        {
            throw new NotImplementedException();
        }

        public int DeleteUserSuspension(int banId, int userId, bool propagateTransExceptions = false)
        {
            throw new NotImplementedException();
        }

        public List<UserSystemSuspension> GetUserSystemSuspensions(string systemId, bool activeOnly = false)
        {
            throw new NotImplementedException();
        }

        public int SaveUserSystemSuspension(UserSystemSuspension systemSuspension, int banningUserId)
        {
            throw new NotImplementedException();
        }

        public int DeleteUserSystemSuspension(int banId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataRow GetUserLoggedInWorld(int userId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataRow GetInvitePointAwards(int invitePointId)
        {
            throw new NotImplementedException();
        }

        public UserMetaGameItemBalances GetUserMetaGameItemBalances(int userId)
        {
            throw new NotImplementedException();
        }

        public bool SaveUserMetaGameItemBalances(UserMetaGameItemBalances balances, bool propagateTransExceptions = false)
        {
            throw new NotImplementedException();
        }

        public UserMetaGameItemRoyalties GetUserMetaGameItemRoyalties(int userId, string itemType)
        {
            throw new NotImplementedException();
        }

        public bool SpendMetaGameItems(int userId, string itemType, uint numItemsSpent, uint totalRewardsValue,
            int zoneInstanceId, int zoneType, string gameItemName, string gameItemType,
            int worldOwnerId, UserMetaGameItemBalances metaGameItemBalances, bool propagateTransExceptions = false)
        {
            throw new NotImplementedException();
        }

        public int GetUserTotalMGIConversions(int userId, string itemType, DateTime startDate, DateTime endDate, string currencyType = null)
        {
            throw new NotImplementedException();
        }

        public int GetUserTotalMGIConversionAmount(int userId, string itemType, DateTime startDate, DateTime endDate, string currencyType = null)
        {
            throw new NotImplementedException();
        }

        public bool ConvertMetaGameItemToCurrency(int userId, string itemType, string currencyType, int numItemsConverted, int currencyAmountReceived, UserMetaGameItemBalances metaGameItemBalances, bool propagateTransExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool HasUserCompletedAvatarSelect(int wokPlayerId)
        {
            throw new NotImplementedException();
        }

        public int InsertUserNotificationProfile(NotificationsProfile notifcationsProfile)
        {
            throw new NotImplementedException();
        }

        public List<NotificationsProfile> GetNotificationsProfile(int userId)
        {
            throw new NotImplementedException();
        }

        public int DismissNotficationProfile(int userId, uint notificationId)
        {
            throw new NotImplementedException();
        }

        public void InsertUserNote(int userId, int createdUserId, string note)
        {
            throw new NotImplementedException();
        }

        public void InsertUserNote(int userId, int createdUserId, string note, int wokTransLogId)
        {
            throw new NotImplementedException();
        }

        public string InsertDeveloperAuthorizationDropBox(int userId, string systemName, byte[] content, uint secondsToLive)
        {
            throw new NotImplementedException();
        }

        public byte[] GetDeveloperAuthorizationFromDropBox(int userId, string systemName, string uuid)
        {
            throw new NotImplementedException();
        }

        public void CleanupDeveloperAuthorizationDropBox()
        {
            throw new NotImplementedException();
        }
    }
}
