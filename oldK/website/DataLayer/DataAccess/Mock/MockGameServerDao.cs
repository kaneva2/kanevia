///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaneva.DataLayer.DataObjects.Mock
{
    public class MockGameServerDao: IGameServerDao
    {
        public BusinessLayer.BusinessObjects.GameServer GetServer(int serverId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.GameServer GetServer(string serverName)
        {
            throw new NotImplementedException();
        }

        public IList<BusinessLayer.BusinessObjects.GameServer> GetAllServers(string filter, string orderby)
        {
            throw new NotImplementedException();
        }

        public IList<BusinessLayer.BusinessObjects.GameServer> GetServerListByGameId(int serverId, string filter, string orderby)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetServerByGameId(int serverId, string filter, string orderby)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetPatchURLByGameId(int gameId, string filter, string orderby)
        {
            throw new NotImplementedException();
        }

        public int AddNewServer(BusinessLayer.BusinessObjects.GameServer server)
        {
            throw new NotImplementedException();
        }

        public int UpdateServer(BusinessLayer.BusinessObjects.GameServer server)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataRow UpdateGameServerStart(int serverId, int maxCount, int statusId, string ipAddress, int port, string actualIp, int type, uint protocolVersion, int schemaVersion, string serverVersion, uint assetVersion, int parentGameId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataRow UpdateGameServerPingCheck(int serverId, string actualIp, int numberOfPlayers, int maxNumberOfPlayers, int statusId, bool adminOnly)
        {
            throw new NotImplementedException();
        }

        public int DeleteServer(int serverId)
        {
            throw new NotImplementedException();
        }

        public int AddNewPatchURL(int gameId, string patchUrl)
        {
            throw new NotImplementedException();
        }

        public int UpdatePatchURL(int gameId, int patchURLId, string patchUrl)
        {
            throw new NotImplementedException();
        }

        public int DeletePatchURL(int gameId, int patchURLId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetGameServerVisibility()
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetGameServerStatus()
        {
            throw new NotImplementedException();
        }

        public string GetServerConfig(int serverType, string serverActualIp)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetGameServerType()
        {
            throw new NotImplementedException();
        }

        public string GetServerVersion(int serverId)
        {
            throw new NotImplementedException();
        }

        public string GetServerVersion(int gameId, string serverName, int port, int serverTypeId)
        {
            throw new NotImplementedException();
        }

        public int AddNewServerAndPatch(int userId, string licenseKey, int gameId, string hostName, int port, string patchUrl, string alternatePatchUrl, string patchType, string clientPath, ref int webAssignedPort, out int serverId)
        {
            throw new NotImplementedException();
        }

        public PagedDataTable GetGames(int pageNumber, int pageSize, string orderBy)
        {
            throw new NotImplementedException();
        }
    }
}
