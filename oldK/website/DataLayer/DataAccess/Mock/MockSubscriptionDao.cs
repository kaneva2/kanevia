///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using Kaneva.BusinessLayer.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Kaneva.DataLayer.DataObjects.Mock
{
    public class MockSubscriptionDao : ISubscriptionDao
    {
        public List<Subscription> GetUnpurchasedSubscriptionsList(int userID)
        {
            throw new NotImplementedException();
        }

        public List<Subscription> GetUnpurchasedSubscriptionsList(int userID, string filter)
        {
            throw new NotImplementedException();
        }

        public Subscription GetSubscription(uint SubscriptionId)
        {
            return new Subscription(SubscriptionId, "Test", 25.99, 25.99, DateTime.Now, false,
                Subscription.SubscriptionTerm.Monthly, 0, "", "", "", 0, "", 25);
        }

        public Subscription GetSubscriptionByPromotionId(uint promotionId)
        {
            throw new NotImplementedException();
        }

        public int InsertSubscription(Subscription subscription)
        {
            throw new NotImplementedException();
        }

        public int InsertSubscription2Promotion(int promotion_id, int subscription_id)
        {
            throw new NotImplementedException();
        }

        public int DeleteAllSubscription2PromotionByPromoID(int promotion_id)
        {
            throw new NotImplementedException();
        }

        public int DeleteAllSubscription2PromotionBySubscptID(int subscription_id)
        {
            throw new NotImplementedException();
        }

        public int DeleteSubscription(int subscriptionId)
        {
            throw new NotImplementedException();
        }

        public void UpdateSubscription(Subscription subscription)
        {
            throw new NotImplementedException();
        }

        public DataTable GetSubscriptionTerms()
        {
            throw new NotImplementedException();
        }

        public DataTable GetSubscriptionStatus()
        {
            throw new NotImplementedException();
        }

        public DataTable GetAvailableSubscriptions()
        {
            throw new NotImplementedException();
        }


        public uint InsertUserSubscription(UserSubscription userSubscription)
        {
            throw new NotImplementedException();
        }

        public UserSubscription GetUserSubscription(uint userSubscriptionId)
        {
            return new UserSubscription(userSubscriptionId, 1, 1, 4, 0.0,
                false, DateTime.Now, Subscription.SubscriptionStatus.Active, 0,
                "", false, DateTime.Now, Subscription.SubscriptionTerm.Monthly, Subscription.SubscriptionTerm.Monthly,
                DateTime.Now, DateTime.Now, "", 0, 0, false);
        }

        public UserSubscription GetUserSubscription(uint SubscriptionId, uint userID)
        {
            return new UserSubscription(SubscriptionId, 1, 1, (int) userID, 0.0,
                false, DateTime.Now, Subscription.SubscriptionStatus.Active, 0,
                "", false, DateTime.Now, Subscription.SubscriptionTerm.Monthly, Subscription.SubscriptionTerm.Monthly,
                DateTime.Now, DateTime.Now, "", 0, 0, false);
        }

        public List<UserSubscription> GetUserSubscriptions(int userId)
        {
            List<UserSubscription> list = new List<UserSubscription>();

            list.Add(new UserSubscription(1, 1, 1, (int)userId, 0.0,
                false, DateTime.Now, Subscription.SubscriptionStatus.Active, 0,
                "", false, DateTime.Now, Subscription.SubscriptionTerm.Monthly, Subscription.SubscriptionTerm.Monthly,
                DateTime.Now, DateTime.Now, "", 0, 0, false));

            return list;
        }

        public void UpdateUserSubscription(UserSubscription userSubscription)
        {
            // Do nothing here
        }

        public List<Subscription> GetSubscriptionsByPromotionId(uint promotionId)
        {
            throw new NotImplementedException();
        }

        public List<SubscriptionEntitlement> GetSubscriptionEntitlements(int subscriptionId, string gender)
        {
            List<SubscriptionEntitlement> list = new List<SubscriptionEntitlement>();

            list.Add(new SubscriptionEntitlement(1, 1, 0.0, "", 0, 1, false));
           
            return list;
        }

        public List<UserSubscription> GetActiveUserSubscriptions(int userId)
        {
            throw new NotImplementedException();
        }

        public bool HasValidSubscription(int userId, int subscriptionId)
        {
            throw new NotImplementedException();
        }

        public bool HasValidSubscriptionByPassType(uint userId, uint passGroupId)
        {
            throw new NotImplementedException();
        }

        public Subscription GetSubscriptionByUserSubscriptionId(uint userSubscriptionId)
        {
            throw new NotImplementedException();
        }

        public List<Subscription> GetSubscriptionsByPassGroupIds(string passGroupIds)
        {
            throw new NotImplementedException();
        }

        public UserSubscription GetUserSubscriptionByPromotionId(uint promotionID, uint userID)
        {
            throw new NotImplementedException();
        }

        public List<UserSubscription> GetUserSubscriptionsByPromotionIds(string promotionIDList, uint userID)
        {
            throw new NotImplementedException();
        }

        public List<Subscription> GetUserActiveSubscriptions(int userId)
        {
            throw new NotImplementedException();
        }


        public UserSubscription GetUserSubscriptionByPassGroupId(uint passGroupId, uint userID)
        {
            throw new NotImplementedException();
        }

        public DataTable GetUnpurchasedSubscriptions(int userID, string passGroupFilter)
        {
            throw new NotImplementedException();
        }

        public DataTable GetPassGroupsToSubscription()
        {
            throw new NotImplementedException();
        }

        public DataTable GetPassGroupsAssociatedWSubscription(uint subscriptionId)
        {
            throw new NotImplementedException();
        }

        public List<Subscription> GetAllSubscriptionsList()
        {
            throw new NotImplementedException();
        }

        public DataTable GetAllSubscriptions()
        {
            throw new NotImplementedException();
        }
        public int InsertSubscription2PassGroup(uint pass_group_id, uint subscription_id)
        {
            throw new NotImplementedException();
        }

        public int DeleteAllSubscription2PassGroupByPassGroupID(uint pass_group_id)
        {
            throw new NotImplementedException();
        }

        public int DeleteAllSubscription2PassGroupBySubscptID(uint subscription_id)
        {
            throw new NotImplementedException();
        }


        public int InsertUserSubscriptionOrder(uint usersubscriptionId, int orderId)
        {
            throw new NotImplementedException();
        }

        public DateTime GetNextEndDate(DateTime purchaseDate, DateTime currentEndDate, Subscription.SubscriptionTerm subscriptionTerm)
        {
            throw new NotImplementedException();
        }
    }
}
