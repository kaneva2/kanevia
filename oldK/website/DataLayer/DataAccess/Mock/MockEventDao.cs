///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaneva.DataLayer.DataObjects.Mock
{
    public class MockEventDao: IEventDao
    {
        public int InsertEvent(BusinessLayer.BusinessObjects.Event evt, string trackingRequestGUID)
        {
            throw new NotImplementedException();
        }

        public int UpdateEvent(BusinessLayer.BusinessObjects.Event evt)
        {
            throw new NotImplementedException();
        }

        public int InsertEvent(int communityId, int userId, string title, string details, string location, int zoneIndex, int zoneInstanceId, DateTime startTime, DateTime endTime, int typeId, int timeZoneId, int premium, int privacy, bool isAPEvent, string trackingRequestGUID)
        {
            throw new NotImplementedException();
        }

        public int UpdateEvent(int eventId, int communityId, string title, string details, string location, DateTime startTime, DateTime endTime, int typeId, int privacy, int premium, int timeZoneId, bool isAPEvent)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.LocationDetail GetLocationDetail(int zone_index, int zone_instance_id, int creator_id, int community_id)
        {
            throw new NotImplementedException();
        }

        public PagedDataTable GetUserCreatedEvents(int userId, int startTime, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.Event GetEvent(int eventId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.EventReminder> GetEventReminders(string orderBy, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public int LogEventReminderNotification(int eventId, int reminderId, DateTime sentDate)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.Event> GetEventsByTimestampOffset(DateTime currentTime, int minuteOffset, string orderBy, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.Event> GetWorldEvents(int communityId, int startTimeFilter, DateTime startTime, string orderBy, int pageNumber, int pageSize, int recurringEventParentId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.Event> GetEvents(string filter, string orderBy, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public int InsertEventInvitees(int communityId, int userId, int eventId, int inviteStatus, bool includeFriends)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.EventInvitee> GetEventInviteesByStatus(int eventId, int inviteStatusId, bool includeEventOwner, string orderBy, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.EventInvitee> GetAutomatedEventInvitees(string orderBy, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public int InsertEventAudit(int eventId, int userId, string desc)
        {
            throw new NotImplementedException();
        }

        public int UpdateEventStatus(int eventId, int statusId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.EventInvitee GetEventInvitee(int userId, int eventId)
        {
            throw new NotImplementedException();
        }

        public int InsertEventInvitee(int eventId, int userId, int eventStatusId)
        {
            throw new NotImplementedException();
        }

        public int InsertComment(int eventId, string eventBlastId, int userId, string comment)
        {
            throw new NotImplementedException();
        }

        public int InsertEventBlast(int eventId, int userId, string comment)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.EventBlast GetEventBlast(string eventBlastId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.EventBlast> GetEventBlasts(int eventId, int pageNumber, int pageSize, string sortBy)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.EventComment> GetEventComments(int eventId, string eventBlastId, int pageNumber, int pageSize, string sortBy, string groupBy)
        {
            throw new NotImplementedException();
        }

        public int DeleteEventComment(int eventId, string eventBlastId, string eventBlastCommentId)
        {
            throw new NotImplementedException();
        }

        public int UpdateEventInviteStatus(int eventId, int inviteeUserId, int inviteStatus)
        {
            throw new NotImplementedException();
        }

        public int UpdateEventInviteeAttendance(int eventId, int inviteeUserId, int inviteStatus)
        {
            throw new NotImplementedException();
        }

        public bool IsUserAlreadyRewardedToday(int userId)
        {
            throw new NotImplementedException();
        }

        public int InsertEventReward(int userId, int eventId, double rewardAmount)
        {
            throw new NotImplementedException();
        }

        public bool DoesCommunityHaveActiveEvent(int communityId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.EventInviteCounts GetEventInviteCounts(int eventId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetEventsUserAcceptedByStartTimeOffset(int userId, string startTimeOffset)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.Event> GetWorldEventsWithEventFlags(int communityId, string orderBy)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.Event GetEventByEventFlagPlacementId(int objPlacementId)
        {
            throw new NotImplementedException();
        }
    }
}
