///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaneva.DataLayer.DataObjects.Mock
{
    public class MockSGCustomDataDao: ISGCustomDataDao
    {
        public bool ModifyDefaultSGCustomDataItem(BusinessLayer.BusinessObjects.SGCustomDataItem sgCustomDataItem, bool updateTemplates = false)
        {
            throw new NotImplementedException();
        }

        public bool AddSGCustomDataToCustomDeed(int zoneInstanceId, int zoneType, int templateGlobalId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.SGCustomDataItem> GetDefaultSGCustomData(string orderBy, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.SGCustomDataItem> GetWorldSGCustomData(int zoneInstanceId, int zoneType, string filter, string orderBy, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.SGCustomDataItem GetDefaultSGCustomDataItem(string attribute)
        {
            throw new NotImplementedException();
        }

        public bool AddSGCustomDataToWorld(int zoneInstanceId, int zoneType, string attribute, string value)
        {
            throw new NotImplementedException();
        }

        public bool DeleteDefaultSGCustomDataItem(BusinessLayer.BusinessObjects.SGCustomDataItem sgCustomDataItem, bool removeFromTemplates = false)
        {
            throw new NotImplementedException();
        }

        public bool DeleteSGCustomDataFromCustomDeed(int deedTemplateId)
        {
            throw new NotImplementedException();
        }
    }
}
