///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Data;
using System.Linq;

namespace Kaneva.DataLayer.DataObjects.Mock
{
    public class MockScriptGameItemDao : IScriptGameItemDao
    {
        private MockDatabase _mockDatabase;

        private const int TEST_TEMPLATE_GLID = 1;
        private const int TEST_ZONE_INSTANCE_ID = 1;
        private const int TEST_ZONE_TYPE = 6;
        private static string[] _itemTypes = new string[]{"ammo", "armor", "blueprint", "character", "consumable", "flag", 
            "gembox", "generic", "harvestable", "placeable", "quest", "recipe", "shortcut", "spawner", "tool", "waypoint", "weapon"};

        public MockScriptGameItemDao()
        {
            _mockDatabase= new MockDatabase();
        }

        private MockDatabaseTable InitTable(string tableName)
        {
            switch (tableName)
            {
                case "script_game_item_global_configs":
                    MockDatabaseTable counterData = new MockDatabaseTable("script_game_item_global_configs",
                        new DataColumn[] 
                        {
                            new DataColumn("property_name", typeof(string)),
                            new DataColumn("property_value", typeof(string))
                        },
                        new string[] { "property_name" });
                    _mockDatabase.CreateTableIfNotExists(counterData);
                    break;
                case "world_game_items":
                    MockDatabaseTable worldGameItems = new MockDatabaseTable("world_game_items",
                        new DataColumn[]
                        {
                            new DataColumn("zoneInstanceId", typeof(int)),
                            new DataColumn("zoneType", typeof(int)),
                            new DataColumn("gameItemId", typeof(int)),
                            new DataColumn("gameItemGlid", typeof(int)),
                            new DataColumn("gameItemName", typeof(string)),
                            new DataColumn("itemType", typeof(string))
                        },
                        new string[] { "zoneInstanceId", "zoneType", "gameItemId" });
                    worldGameItems.SetColumnAutoIncrement("gameItemId", true);
                    _mockDatabase.CreateTableIfNotExists(worldGameItems);
                        
                    // Fill default data.
                    foreach(string itemType in _itemTypes)
                    {
                        for(int i = 1; i < 11; i++)
                        {
                            string itemName = "Test" + itemType + i;
                            DataRow row = worldGameItems.GenerateRow();
                            row["zoneInstanceId"] = TEST_ZONE_INSTANCE_ID;
                            row["zoneType"] = TEST_ZONE_TYPE;
                            row["gameItemGlid"] = 0;
                            row["gameItemName"] = itemName;
                            row["itemType"] = itemType;
                            worldGameItems.InsertRecord(row);
                        }
                    }
                    break;
                case "game_item_snapshots":
                    MockDatabaseTable gameItemSnapshots = new MockDatabaseTable("game_item_snapshots",
                        new DataColumn[]
                        {
                            new DataColumn("gameItemGlid", typeof(int)),
                            new DataColumn("gameItemName", typeof(string)),
                            new DataColumn("itemType", typeof(string))
                        },
                        new string[] { "gameItemGlid" });
                    gameItemSnapshots.SetColumnAutoIncrement("gameItemGlid", true);
                    _mockDatabase.CreateTableIfNotExists(gameItemSnapshots);
                    break;
                case "template_game_items":
                    MockDatabaseTable templateGameItems = new MockDatabaseTable("template_game_items",
                        new DataColumn[]
                        {
                            new DataColumn("templateGlid", typeof(int)),
                            new DataColumn("gameItemId", typeof(int)),
                            new DataColumn("gameItemGlid", typeof(int)),
                            new DataColumn("gameItemName", typeof(string)),
                            new DataColumn("itemType", typeof(string))
                        },
                        new string[] { "templateGlid", "gameItemId" });
                    templateGameItems.SetColumnAutoIncrement("gameItemId", true);
                    _mockDatabase.CreateTableIfNotExists(templateGameItems);
                    break;
                default:
                    throw new ArgumentException("Unable to initialize table with name " + tableName, "tableName");
            }

            return _mockDatabase.GetTable(tableName);
        }

        public bool InsertSnapshotScriptGameItems(ref List<ScriptGameItem> sgItems, int userId, bool addToInventory = false, bool itemsAreDefault = false)
        {
            // Fill table with a default set of game items.
            MockDatabaseTable table = InitTable("game_item_snapshots");

            try
            {
                foreach (ScriptGameItem item in sgItems)
                {
                    DataRow row = table.GenerateRow();
                    row["gameItemName"] = item.Name;
                    row["itemType"] = item.ItemType;
                    table.InsertRecord(row);
                    item.GIGlid = (int)table.SelectRecords(string.Empty, "gameItemGlid desc").FirstOrDefault()["gameItemGlid"];
                }
            }
            catch (Exception) { return false; }

            return true;
        }

        public bool AddSnapshotScriptGameItemsToCustomDeed(IEnumerable<ScriptGameItem> sgItems, int templateGlobalId)
        {
            // Fill table with a default set of game items.
            MockDatabaseTable table = InitTable("template_game_items");

            try
            {
                foreach (ScriptGameItem item in sgItems)
                {
                    if (item.GIId <= 0)
                        throw new ArgumentException("sgItems contains an item where GIId <= 0.");
                    if (item.GIGlid <= 0)
                        throw new ArgumentException("sgItems contains an item where GIGLID <= 0.");
                    DataRow row = table.GenerateRow();
                    row["templateGlid"] = templateGlobalId;
                    row["gameItemId"] = item.GIId;
                    row["gameItemGlid"] = item.GIGlid;
                    row["gameItemName"] = item.Name;
                    row["itemType"] = item.ItemType;
                    table.InsertRecord(row);
                }
            }
            catch (Exception) { return false; }

            return true;
        }

        public string GetSGIGlobalConfigValue(string propertyName)
        {
            MockDatabaseTable table = InitTable("script_game_item_global_configs");

            try
            {
                DataRow[] results = table.SelectRecords("property_name='" + propertyName.ToLower() + "'");
                return results.FirstOrDefault()["property_value"].ToString();
            }
            catch (Exception) { return string.Empty; }
        }

        public Dictionary<string, string> GetSGIGlobalConfigValues()
        {
            MockDatabaseTable table = InitTable("script_game_item_global_configs");

            try
            {
                DataRow[] results = table.SelectRecords("");

                Dictionary<string, string> configs = new Dictionary<string, string>();
                foreach (DataRow row in results)
                {
                    configs[row["property_name"].ToString()] = row["property_value"].ToString();
                }

                return configs;
            }
            catch (Exception) { return new Dictionary<string, string>(); }
        }

        public bool UpdateSGIGlobalConfig(Dictionary<string, string> config)
        {
            MockDatabaseTable table = InitTable("script_game_item_global_configs");
            
            try
            {
                table.Truncate();
                foreach(KeyValuePair<string, string> kvp in config)
                {
                    DataRow row = table.GenerateRow();
                    row["property_name"] = kvp.Key.ToLower();
                    row["property_value"] = kvp.Value;
                    table.InsertRecord(row);
                }
            }
            catch (Exception) { return false; }

            return true;
        }

        public bool DeleteSGIGlobalConfig(string propName)
        {
            MockDatabaseTable table = InitTable("script_game_item_global_configs");

            try
            {
                DataRow[] results = table.SelectRecords("property_name='" + propName.ToLower() + "'");
                foreach (DataRow row in results)
                {
                    table.DeleteRecord(row);
                }
            }
            catch (Exception) { return false; }

            return true;
        }

        public PagedList<ScriptGameItem> GetInventoryScriptGameItems(int playerId, string inventoryType, string searchString, IList<ScriptGameItemCategory> itemTypes, string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false)
        {
            throw new NotImplementedException();
        }

        public ScriptGameItem GetSnapshotScriptGameItem(int gIGlid, bool includeProperties = false, bool includeBundledGlids = false)
        {
            throw new NotImplementedException();
        }

        public ScriptGameItem GetSnapshotScriptGameItem(int zoneInstanceId, int zoneType, int gameItemId, bool includeProperties = false, bool includeBundledGlids = false)
        {
            throw new NotImplementedException();
        }

        public string GetSuggestedNameSuffixNum(int playerId, string inventoryType, string name, IList<ScriptGameItemCategory> itemTypes)
        {
            throw new NotImplementedException();
        }

        public PagedList<ScriptGameItem> GetTemplateScriptGameItems(int templateId, string searchString, string[] itemTypes, string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false)
        {
            throw new NotImplementedException();
        }

        public PagedList<ScriptGameItem> GetDefaultScriptGameItems(string searchString, string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false)
        {
            throw new NotImplementedException();
        }

        public PagedList<ScriptGameItem> GetWorldScriptGameItems(int zoneInstanceId, int zoneType, string filter, string orderBy, int pageNumber, int pageSize, bool includeProperties = false)
        {
            // Fill table with a default set of game items.
            MockDatabaseTable table = InitTable("world_game_items");

            try
            {
                DataRow[] results = table.SelectRecords(filter, orderBy);
                DataRow[] pagedResults = table.PaginateResults(results, pageNumber, pageSize);
                PagedList<ScriptGameItem> items = new PagedList<ScriptGameItem>();
                foreach(DataRow row in results)
                {
                    ScriptGameItem item = new ScriptGameItem
                    {
                        ZoneInstanceId = (int)row["zoneInstanceId"],
                        ZoneType = (int)row["zoneType"],
                        GIId = (int)row["gameItemId"],
                        GIGlid = (int)row["gameItemGlid"],
                        Name = row["gameItemName"].ToString(),
                        ItemType = row["itemType"].ToString()
                    };
                    items.Add(item);
                }
                items.TotalCount = (uint)results.Length;
                return items;
            }
            catch (Exception) { return new PagedList<ScriptGameItem>(); }
        }

        public PagedList<ScriptGameItem> GetDeedScriptGameItems(int templateGlid, string searchString, string[] itemTypes, string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false)
        {
            // Fill table with a default set of game items.
            MockDatabaseTable table = InitTable("template_game_items");

            try
            {
                DataRow[] results = table.SelectRecords(searchString, orderBy);
                DataRow[] pagedResults = table.PaginateResults(results, pageNumber, pageSize);
                PagedList<ScriptGameItem> items = new PagedList<ScriptGameItem>();
                foreach (DataRow row in results)
                {
                    ScriptGameItem item = new ScriptGameItem
                    {
                        GIId = (int)row["gameItemId"],
                        GIGlid = (int)row["gameItemGlid"],
                        Name = row["gameItemName"].ToString(),
                        ItemType = row["itemType"].ToString()
                    };
                    items.Add(item);
                }
                items.TotalCount = (uint)results.Length;
                return items;
            }
            catch (Exception) { return new PagedList<ScriptGameItem>(); }
        }

        public int GetSuggestedGIGLID()
        {
            throw new NotImplementedException();
        }

        public int GetSuggestedBundleGlid()
        {
            throw new NotImplementedException();
        }

        public int GetSuggestedGIID()
        {
            throw new NotImplementedException();
        }

        public bool WorldHasScriptGameItems(int zoneIndex, int zoneInstanceId)
        {
            throw new NotImplementedException();
        }

        public bool IsTypeInventoryCompatible(string itemType)
        {
            throw new NotImplementedException();
        }

        public bool UpdateSnapshotScriptGameItem(ScriptGameItem sgItem, int userId)
        {
            throw new NotImplementedException();
        }

        public bool UpdateWorldGIGLIDs(int zoneInstanceId, int zoneType, List<ScriptGameItem> sgItems)
        {
            // Fill table with a default set of game items.
            MockDatabaseTable table = InitTable("world_game_items");

            try
            {
                foreach (ScriptGameItem item in sgItems)
                {
                    table.UpdateRecords("gameItemId=" + item.GIId, new KeyValuePair<string, object>("gameItemGlid", item.GIGlid));
                }
            }
            catch (Exception) { return false; }

            return true;
        }

        public bool DeleteStartingScriptGameItem(ScriptGameItem sgItem, int templateId = 0)
        {
            throw new NotImplementedException();
        }

        public bool DeleteAllSGItemsInCustomDeed(int templateGlobalId)
        {
            throw new NotImplementedException();
        }
    }
}
