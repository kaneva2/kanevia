///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaneva.DataLayer.DataObjects.Mock
{
    public class MockGameLicenseDao: IGameLicenseDao
    {
        public BusinessLayer.BusinessObjects.GameLicense GetGameLicense(int gameLicenseId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.GameLicense GetGameLicenseByGameId(int gameId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.GameLicense GetGameLicenseByGameKey(string gameKey)
        {
            throw new NotImplementedException();
        }

        public IList<BusinessLayer.BusinessObjects.GameLicense> GetAllLicenses(string filter, string orderby)
        {
            throw new NotImplementedException();
        }

        public IList<BusinessLayer.BusinessObjects.GameLicense> GetLicenseByGameId(int gameId, string filter, string orderby)
        {
            throw new NotImplementedException();
        }

        public int AddNewGameLicense(BusinessLayer.BusinessObjects.GameLicense license, int daysLeft)
        {
            throw new NotImplementedException();
        }

        public int UpdateGameLicense(BusinessLayer.BusinessObjects.GameLicense license)
        {
            throw new NotImplementedException();
        }

        public int DeleteGameLicense(int licenseId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.GameLicenseSubscription GetGameLicenseType(int license_subscription_id)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.GameLicenseSubscription> GetGameLicenseTypes()
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetGameLicenseStatus()
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.GameSubscription GetGameSubscription(int SubscriptionId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.GameSubscription GetGameSubscriptionByOrderId(int orderId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.GameSubscription> GetGameSubscriptions(int GameId, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public int InsertGameSubscription(BusinessLayer.BusinessObjects.GameSubscription gameSubscription)
        {
            throw new NotImplementedException();
        }

        public void UpdateGameSubscription(BusinessLayer.BusinessObjects.GameSubscription gameSubscription)
        {
            throw new NotImplementedException();
        }

        public int InsertGameSubscriptionOrder(int subscriptionId, int orderId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.GameLicenseContact GetGameLicenseContact(int gameLicenseId)
        {
            throw new NotImplementedException();
        }

        public int InsertGameLicenseContact(BusinessLayer.BusinessObjects.GameLicenseContact gameLicenseContact)
        {
            throw new NotImplementedException();
        }
    }
}
