///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaneva.DataLayer.DataObjects.Mock
{
    public class MockSGFrameworkSettingsDao: ISGFrameworkSettingsDao
    {
        private MockDatabase _mockDatabase;

        private const int TEST_ZONE_INSTANCE_ID = 1;
        private const int TEST_ZONE_TYPE = 6;

        public MockSGFrameworkSettingsDao()
        {
            _mockDatabase= new MockDatabase();
        }

        private MockDatabaseTable InitTable(string tableName)
        {
            switch (tableName)
            {
                case "script_game_framework_settings":
                    MockDatabaseTable sgFrameworkSettings = new MockDatabaseTable("script_game_framework_settings",
                        new DataColumn[]
                        {
                            new DataColumn("zone_instance_id", typeof(int)),
                            new DataColumn("zone_type", typeof(int)),
                            new DataColumn("framework_enabled", typeof(string))
                        },
                        new string[] { "zone_instance_id", "zone_type", "framework_enabled" });
                    _mockDatabase.CreateTableIfNotExists(sgFrameworkSettings);
                        
                    // Fill default data.
                    DataRow row = sgFrameworkSettings.GenerateRow();
                    row["zone_instance_id"] = TEST_ZONE_INSTANCE_ID;
                    row["zone_type"] = TEST_ZONE_TYPE;
                    row["framework_enabled"] = "T";
                    sgFrameworkSettings.InsertRecord(row);
                    break;
                default:
                    throw new ArgumentException("Unable to initialize table with name " + tableName, "tableName");
            }

            return _mockDatabase.GetTable(tableName);
        }

        public bool AddSGFrameworkSettingsToCustomDeed(int zoneInstanceId, int zoneType, int templateGlobalId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.SGFrameworkSettings GetWorldSGFrameworkSettings(int zoneInstanceId, int zoneType)
        {
            MockDatabaseTable table = InitTable("script_game_framework_settings");

            try
            {
                DataRow[] results = table.SelectRecords(string.Empty);
                string frameworkEnabled = results.FirstOrDefault()["framework_enabled"].ToString();
                return new BusinessLayer.BusinessObjects.SGFrameworkSettings(frameworkEnabled.Equals("T"));
            }
            catch (Exception) { return new BusinessLayer.BusinessObjects.SGFrameworkSettings(); }
        }

        public BusinessLayer.BusinessObjects.SGFrameworkSettings GetDeedSGFrameworkSettings(int templateGlid)
        {
            throw new NotImplementedException();
        }

        public bool DeleteSGFrameworkSettingsFromCustomDeed(int deedTemplateId)
        {
            throw new NotImplementedException();
        }
    }
}
