///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Kaneva.DataLayer.DataObjects.Mock
{
    /// <summary>
    /// Class representing a mock database table for use in unit testing.
    /// </summary>
    public class MockDatabaseTable
    {
        private DataTable _table;

        public MockDatabaseTable(string name, DataColumn[] columns, string[] primaryKeyNames)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Argument must be a non-empty string.", "name");
            if (columns == null)
                throw new ArgumentNullException("columns");
            if (primaryKeyNames == null)
                throw new ArgumentNullException("primaryKeyNames");
            if (columns.Length < primaryKeyNames.Length)
                throw new ArgumentException("Argument primaryKeyNames longer than argument columns.");

            _table = new DataTable(name);
            _table.Columns.AddRange(columns);

            DataColumn[] primaryKeyColumn = new DataColumn[primaryKeyNames.Length];
            for (int i = 0; i < primaryKeyColumn.Length; i++)
            {
                primaryKeyColumn[i] = _table.Columns[primaryKeyNames[i]];
            }
            _table.PrimaryKey = primaryKeyColumn;
        }

        public DataRow GenerateRow()
        {
            return _table.NewRow();
        }

        public void InsertRecord(DataRow record)
        {
            if (record == null)
                throw new ArgumentNullException("record");

            _table.Rows.Add(record);
            _table.AcceptChanges();
        }

        // Update one value in multiple rows
        public void UpdateRecords(string filterExpression, KeyValuePair<string, object> change)
        {
            if (string.IsNullOrWhiteSpace(filterExpression))
                throw new ArgumentNullException("filterExpression");
            if (_table.Columns[change.Key] == null)
                throw new ArgumentException("Invalid column name.");

            DataRow[] records = SelectRecords(filterExpression);
            foreach (DataRow record in records)
            {
                record[change.Key] = change.Value;
            }
            _table.AcceptChanges();
        }

        // Update multiple values in multiple rows
        public void UpdateRecord(string filterExpression, IEnumerable<KeyValuePair<string, object>> change)
        {
            if (string.IsNullOrWhiteSpace(filterExpression))
                throw new ArgumentNullException("filterExpression");
            
            DataRow[] records = SelectRecords(filterExpression);

            if (records.Length == 0)
                throw new DataException("UpdateRecord");

            foreach (DataRow record in records)
            {
                foreach (KeyValuePair<string, object> kvp in change)
                {
                    if (kvp.Key == null)
                    { 
                       throw new ArgumentException("Invalid column name.");
                    }

                    record[kvp.Key] = kvp.Value;
                }
            }
            _table.AcceptChanges();
        }

        public DataRow[] SelectRecords(string filterExpression, string sort = "")
        {
            DataRow[] results;
            if (string.IsNullOrWhiteSpace(filterExpression))
            {
                if (string.IsNullOrWhiteSpace(sort))
                    results = _table.Select(filterExpression);
                else
                    results = _table.Select(filterExpression, sort);
            }
            else
            {
                results = _table.Select();
            }

            return results;
        }

        public DataRow[] PaginateResults(DataRow[] results, int pageNum, int pageSize)
        {
            // Skip to the requested page.
            int recordsToSkip = (pageNum - 1) * pageSize;
            if (results.Length <= recordsToSkip)
                throw new ArgumentException("pageNum greater than available pages.", "pageNum");
            results = results.Skip(recordsToSkip).ToArray();

            // Take the page of data.
            int recordsToTake = (results.Length - recordsToSkip >= pageSize ? pageSize : results.Length - recordsToSkip);
            if (recordsToTake > 0)
                results = results.Take(recordsToTake).ToArray();

            return results;
        }

        public void DeleteRecord(DataRow record)
        {
            if (record == null)
                throw new ArgumentNullException("record");

            _table.Rows.Remove(record);
            _table.AcceptChanges();
        }

        public void Truncate()
        {
            _table.Rows.Clear();
        }

        public void SetColumnAutoIncrement(string columnName, bool isAutoIncrement)
        {
            if (_table.Columns.IndexOf(columnName) == -1)
                throw new ArgumentException("Unable to find column with name " + columnName, "columnName");

            _table.Columns[columnName].AutoIncrement = isAutoIncrement;
            _table.Columns[columnName].AutoIncrementSeed = 1;
        }

        public string TableName
        {
            get { return _table.TableName; }
        }

        public int RowCount
        {
            get { return _table.Rows.Count; }
        }
    }
}

