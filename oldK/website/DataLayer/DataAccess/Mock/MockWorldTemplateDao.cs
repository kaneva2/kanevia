///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaneva.DataLayer.DataObjects.Mock
{
    public class MockWorldTemplateDao: IWorldTemplateDao
    {
        public int InsertWorldTemplate(BusinessLayer.BusinessObjects.WorldTemplate template)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.WorldTemplate GetWorldTemplate(int templateId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.WorldTemplate GetDefaultHomeWorldTemplate()
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.WorldTemplate> GetWorldTemplates(int[] status, int[] passGroupId, DateTime? userSignupDate, bool excludeIncubatorHosted, int pageNumber, int pageSize, string orderBy)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.WorldTemplate> GetWorldTemplates(List<int> templateIds, int pageNumber, int pageSize, string orderBy)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.WorldTemplate> GetGamingWorldTemplates(int pageNumber, int pageSize, string orderBy)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.WorldTemplate> SearchWorldTemplates(string searchString, int pageNumber, int pageSize, string orderBy)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetWorldTemplateDefaultItemIds(int templateId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.WOKItem> GetWorldTemplateDefaultItems(int templateId, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.WorldTemplate> GetWorldTemplatesByExperimentGroups(List<string> experimentGroupIds, string action)
        {
            throw new NotImplementedException();
        }

        public int UpdateWorldTemplate(BusinessLayer.BusinessObjects.WorldTemplate template)
        {
            throw new NotImplementedException();
        }

        public int DeleteWorldTemplate(BusinessLayer.BusinessObjects.WorldTemplate template)
        {
            throw new NotImplementedException();
        }

        public int InsertWorldsToTemplate(int communityId, int templateId)
        {
            throw new NotImplementedException();
        }

        public int DeleteWorldsToTemplate(int communityId, int templateId)
        {
            throw new NotImplementedException();
        }

        public int InsertWorldTemplateDefaultItemIds(int templateId, int[] globalIds)
        {
            throw new NotImplementedException();
        }

        public int DeleteWorldTemplateDefaultItemIds(int templateId, int[] globalIds)
        {
            throw new NotImplementedException();
        }

        public int GetWorldTemplateId(int communityId)
        {
            throw new NotImplementedException();
        }
    }
}
