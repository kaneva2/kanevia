///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects.Mock
{
    public class MockCommunityDao : ICommunityDao
    {
        public Community GetCommunity(int communityId)
        {
            throw new NotImplementedException();
        }

        public CommunityStats GetCommunityStats(int communityId)
        {
            throw new NotImplementedException();
        }

        public CommunityPreferences GetCommunityPreferences(int communityId)
        {
            throw new NotImplementedException();
        }

        public CommunityMember GetCommunityMember(int communityId, int userId)
        {
            throw new NotImplementedException();
        }

        public PagedList<CommunityMember> GetCommunityMembers(int communityId, uint communityMemberStatusId, bool bIncludeSuspended, bool bIncludePending, bool bIncludeDeleted, bool bIncludeRejected, string filter, string orderby, int pageNumber, int pageSize, bool bShowMature, string userNameFilter, bool includeNotificationPref)
        {
            throw new NotImplementedException();
        }

        public List<CommunityCategory> GetCommunityCategories()
        {
            throw new NotImplementedException();
        }

        public List<CommunityCategory> GetCommunityCategories(int communityId)
        {
            throw new NotImplementedException();
        }

        public int InsertCommunityCategory(int communityId, int categoryId)
        {
            throw new NotImplementedException();
        }

        public int DeleteCommunityCategories(int communityId)
        {
            throw new NotImplementedException();
        }

        public List<CommunityPlaceType> GetCommunityPlaceTypes()
        {
            throw new NotImplementedException();
        }

        public List<CommunityTab> GetCommunityTabs(int CommunityId, int CommunityTypeId)
        {
            throw new NotImplementedException();
        }

        public List<CommunityTab> GetCommunityTabsByCommunityType(int CommunityTypeId)
        {
            throw new NotImplementedException();
        }

        public int UpdateCommunityTabs(int tabId, int communityId, int communityTypeId, bool isSelected, bool isDefault)
        {
            throw new NotImplementedException();
        }

        public string GetCommunityAboutText(int communityId)
        {
            throw new NotImplementedException();
        }

        public int UpdateCommunityAboutText(int communityId, string aboutText)
        {
            throw new NotImplementedException();
        }

        public PagedList<Community> GetCommunities(int userId, int[] communityTypeIds, string orderBy, string filter, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public PagedList<Community> GetCommunities(int userId, int[] communityTypeIds, string orderBy, string filter, int pageNumber, int pageSize, bool with3dAppInfo, string nameFilter)
        {
            throw new NotImplementedException();
        }

        public Community GetApartmentCommunity(int zoneInstanceId)
        {
            throw new NotImplementedException();
        }

        public PagedList<Community> SearchCommunities(bool onlyAccessPass, bool bGetMature, string searchString, bool bOnlyWithPhotos, string country, int pastDays, int[] communityTypeIds, string orderBy, int pageNumber, int pageSize, int iPlaceTypeId)
        {
            throw new NotImplementedException();
        }

        public int UpdateCommunityURL(int communityId, string url)
        {
            throw new NotImplementedException();
        }

        public WOK3DPlace Get3DPlace(int zoneInstanceId, int zoneType)
        {
            throw new NotImplementedException();
        }

        public WOK3DPlace Get3DPlace(int channelZoneId)
        {
            throw new NotImplementedException();
        }

        public WOK3DPlace Get3DPlace(string worldName)
        {
            throw new NotImplementedException();
        }

        public PagedList<WOK3DPlace> Search3DPlaceByName(string name, int page, int itemsPerPage)
        {
            throw new NotImplementedException();
        }

        public bool IsUserWOK3DPlaceOwner(int userId, int zoneIndex, int zoneInstanceId)
        {
            throw new NotImplementedException();
        }

        public int Set3DPlaceTiedStatus(int channelZoneId, int serverId, string tiedToServer)
        {
            throw new NotImplementedException();
        }

        public int Set3DPlaceSpinDownDelayMinutes(int zoneInstanceId, int zoneType, int spinDownDelayMinutes)
        {
            throw new NotImplementedException();
        }

        public WOK3DApp GetWOK3DApp(int communityId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetWorldPasses(int wokGameId, int gameId, int zoneInstanceId, int zoneType)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetCommunityPasses(int communityId, int communityType)
        {
            throw new NotImplementedException();
        }

        public bool AddCommunityPass(int userId, bool isAdmin, int communityId, int communityType, int passId)
        {
            throw new NotImplementedException();
        }

        public bool DeleteCommunityPass(int userId, bool isAdmin, int communityId, int communityType, int passId)
        {
            throw new NotImplementedException();
        }

        public Community GetPersonalCommunityFromPlayerId(int playerId)
        {
            throw new NotImplementedException();
        }

        public int GetNumberOfCommunities(int creatorId, int statusId, int[] communityTypes)
        {
            throw new NotImplementedException();
        }

        public int GetNumberOf3DApps(int creatorId, int statusId)
        {
            throw new NotImplementedException();
        }

        public int InsertCommunityPreferences(Community community)
        {
            throw new NotImplementedException();
        }

        public int UpdateCommunityPreferences(Community community)
        {
            throw new NotImplementedException();
        }

        public void ShareCommunity(int communityId, int userId, int sentUserId, string email)
        {
            throw new NotImplementedException();
        }

        public int InsertCommunity(Community community)
        {
            throw new NotImplementedException();
        }

        public int IsCommunityNameTaken(bool isPersonal, string communityName, int communityId)
        {
            throw new NotImplementedException();
        }

        public int IsCommunityURLTaken(string url, int communityId)
        {
            throw new NotImplementedException();
        }

        public int InsertCommunityMember(CommunityMember communityMember)
        {
            throw new NotImplementedException();
        }

        public bool IsCommunityValid(int communityId)
        {
            throw new NotImplementedException();
        }

        public void UpdateChannelViews(int userId, int communityId, bool browseAnon, string ipAddress)
        {
            throw new NotImplementedException();
        }

        public int UpdateCommunity(int communityId, int ownerId, bool IsPersonal, string thumbnailPath, string thumbnailType, bool hasThumbnail)
        {
            throw new NotImplementedException();
        }

        public int UpdateCommunity(Community community)
        {
            throw new NotImplementedException();
        }

        public int UpdateCommunityThumb(int communityId, string thumbnailPath, string dbColumn)
        {
            throw new NotImplementedException();
        }

        public int DeleteAllCommunityAssets(int communityId)
        {
            throw new NotImplementedException();
        }

        public int DeleteAllCommunityMembers(int communityId)
        {
            throw new NotImplementedException();
        }

        public int DeleteCommunityRecoverable(int communityId)
        {
            throw new NotImplementedException();
        }

        public int DeleteCommunityThumbs(int communityId)
        {
            throw new NotImplementedException();
        }

        public int RestoreDeletedCommunity(int communityId)
        {
            throw new NotImplementedException();
        }

        public bool IsActiveCommunityMember(int communityId, int userId)
        {
            throw new NotImplementedException();
        }

        public int UpdateCommunityMember(int communityId, int userId, uint statusId)
        {
            throw new NotImplementedException();
        }

        public int UpdateCommunityMember(int communityId, int userId, int accountTypeId, string newsletter, string allowAssetUploads, string allowForumUse)
        {
            throw new NotImplementedException();
        }

        public int UpdateCommunityMember(int communityId, int userId, CommunityMember.CommunityMemberAccountType accountType)
        {
            throw new NotImplementedException();
        }

        public int RemoveMemberFromAllOwnersWorlds(int ownerId, int memberId)
        {
            throw new NotImplementedException();
        }

        public int GetCommunityGameId(int communityId)
        {
            throw new NotImplementedException();
        }

        public int GetCommunityIdFromGameId(int gameId)
        {
            throw new NotImplementedException();
        }

        public int GetCommunityIdFromName(string communityName, bool isPersonal)
        {
            throw new NotImplementedException();
        }

        public uint GetCommunityIdFromFaux3DAppZoneIndex(int zoneIndex)
        {
            throw new NotImplementedException();
        }

        public int GetWorldOwnerFromWorldIds(int wokGameId, int gameId, int zoneIndex, int zoneInstanceId, int zoneType)
        {
            throw new NotImplementedException();
        }

        public int GetCommunityIdFromWorldIds(int wokGameId, int gameId, int zoneIndex, int zoneInstanceId, int zoneType)
        {
            throw new NotImplementedException();
        }

        public PagedList<Community> GetUserTopWorlds(int userId, int[] communityTypeIds, string orderBy, string filter, int pageNumber, int pageSize, bool includeCreditBalance, int premiumItemDaysPendingBeforeRedeem)
        {
            throw new NotImplementedException();
        }

        public void InsertAPIAuth(int communityId, string consumerKey, string consumerSecret)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.API.APIAuthentication GetAPIAuth(int gameId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.API.APIAuthentication GetAPIAuth(string consumerKey, string consumerSecret)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.API.APIAuthentication GetAPIAuth(string consumerKey)
        {
            throw new NotImplementedException();
        }

        public bool IsPhysicsEnabledForCommunity(int communityId)
        {
            throw new NotImplementedException();
        }

        public int UpdatePhysicsEnabled(int communityId, bool isEnabled)
        {
            throw new NotImplementedException();
        }

        public int UpdateCommunityParentId(int communityId, int parentId)
        {
            throw new NotImplementedException();
        }

        public int InsertHomeCommunityIdAsChildWorld(int communityId)
        {
            throw new NotImplementedException();
        }

        public int RemoveHomeCommunityIdAsChildWorld(int communityId)
        {
            throw new NotImplementedException();
        }
    }
}

