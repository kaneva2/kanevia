///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Data;
using System.Linq;

namespace Kaneva.DataLayer.DataObjects.Mock
{
    public class MockFameDao : IFameDao
    {
        public UserFame GetUserFame(int userId, int fameTypeId)
        {
            throw new NotImplementedException();
        }

        public FameType GetFameType(int fameTypeId)
        {
            throw new NotImplementedException();
        }

        public UserFame GetDanceFame(int userId, int fameTypeId, string username, int playerId)
        {
            throw new NotImplementedException();
        }

        public Level GetDanceLevel(int levelId, int fameTypeId)
        {
            throw new NotImplementedException();
        }

        public PagedList<UserFame> GetUserFame(int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Level GetLevel(int levelId)
        {
            throw new NotImplementedException();
        }

        public int SetLevel(int userId, int fameTypeId, int level)
        {
            throw new NotImplementedException();
        }

        public PagedList<Level> GetLevels(int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public List<LevelAwards> GetLevelAwards(int levelId, string gender)
        {
            throw new NotImplementedException();
        }

        public List<LevelAwards> GetAllLevelAwards(int fameTypeId)
        {
            throw new NotImplementedException();
        }

        public Packet GetPacket(int packetId)
        {
            throw new NotImplementedException();
        }

        public PagedList<Packet> GetPackets(int fameTypeId, string filter, string orderBy, string groupBy, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public PagedList<PacketHistory> GetPacketHistory(int userId, int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public PagedList<DailyHistory> GetDailyHistoryForUser(int userId, int packetId, int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public PagedList<LevelHistory> GetLevelHistory(int userId, int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public OnetimeHistory GetOneTimeHistory(int userId, int packetId)
        {
            throw new NotImplementedException();
        }

        public void AwardFame(int packetId, int userId, ref int returnCode, ref int levelId)
        {
            throw new NotImplementedException();
        }

        public PagedList<Nudge> GetChecklist(int userId, int checklistId, string filter, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public int GetChecklistIdByPacketId(int packetId)
        {
            throw new NotImplementedException();
        }

        public PagedList<Nudge> GetNudges(int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public bool IsChecklistComplete(int userId, int checklistId)
        {
            throw new NotImplementedException();
        }

        public bool IsChecklistCompleteLongWay(int userId, int checklistId)
        {
            throw new NotImplementedException();
        }

        public int InsertChecklistComplete(int userId, int checklistId)
        {
            throw new NotImplementedException();
        }

        public void UpdateDailyHistory(int userId, int packetId, int fameTypeId)
        {
            throw new NotImplementedException();
        }

        public int GetRewardInterval(int userId, int packetId)
        {
            throw new NotImplementedException();
        }

        public void UpdateRewardInterval(int userId, int packetId)
        {
            throw new NotImplementedException();
        }

        public int GetRewardIntervalDaily(int userId, int packetId)
        {
            throw new NotImplementedException();
        }

        public void UpdateRewardIntervalDaily(int userId, int packetId)
        {
            throw new NotImplementedException();
        }

        public int InsertDanceToLevel10(int userId)
        {
            throw new NotImplementedException();
        }

        public List<User> GetYesterdayWebLogins()
        {
            throw new NotImplementedException();
        }

        public List<User> GetYesterdayWOKLogins()
        {
            throw new NotImplementedException();
        }

        public List<User> GetYesterday10MinsInWorld()
        {
            throw new NotImplementedException();
        }

        public List<User> GetYesterday3HoursInWorld()
        {
            throw new NotImplementedException();
        }

        public List<User> GetYesterdayDanceLevel10()
        {
            throw new NotImplementedException();
        }

        public List<User> GetYesterdayVisitForum()
        {
            throw new NotImplementedException();
        }

        public List<User> GetYesterdayRead3Posts()
        {
            throw new NotImplementedException();
        }

        public List<User> GetYesterdayView3Media()
        {
            throw new NotImplementedException();
        }

        public DataRow GetFamePointsFromInvitingFriends(int userId)
        {
            throw new NotImplementedException();
        }

        public DataRow GetUserLastDailyLoginPacketRedemption(int userId)
        {
            throw new NotImplementedException();
        }

        public Checklist GetChecklist(int checklistId, int userId)
        {
            throw new NotImplementedException();
        }

        public List<ChecklistPacket> GetChecklistPackets(int userId, int checklistId, string orderBy)
        {
            throw new NotImplementedException();
        }

        public PagedList<Nudge> GetChecklistNudges(int userId, int checklistId)
        {
            throw new NotImplementedException();
        }
    }
}
