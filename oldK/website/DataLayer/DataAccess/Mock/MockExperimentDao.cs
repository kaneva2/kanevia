///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaneva.DataLayer.DataObjects.Mock
{
    public class MockExperimentDao: IExperimentDao
    {
        public bool SaveExperiment(BusinessLayer.BusinessObjects.Experiment experiment, bool propagateTransactionExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool SaveExperimentCategory(BusinessLayer.BusinessObjects.ExperimentCategory category, bool propagateTransactionExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool SaveExperimentEffect(BusinessLayer.BusinessObjects.ExperimentEffect effect, bool propagateTransactionExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool SaveExperimentGroups(BusinessLayer.BusinessObjects.Experiment experiment, bool propagateTransactionExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool SaveExperimentGroupEffect(BusinessLayer.BusinessObjects.ExperimentGroup group, BusinessLayer.BusinessObjects.ExperimentEffect effect, bool propagateTransactionExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool SaveUserExperimentGroup(BusinessLayer.BusinessObjects.UserExperimentGroup participant, bool propagateTransactionExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool SaveWorldExperimentGroup(BusinessLayer.BusinessObjects.WorldExperimentGroup participant, bool propagateTransactionExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool SaveWorldTemplateExperimentGroup(BusinessLayer.BusinessObjects.WorldTemplateExperimentGroup participant, bool propagateTransactionExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool DeleteExperiment(BusinessLayer.BusinessObjects.Experiment experiment, bool propagateTransactionExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool DeleteExperimentCategory(BusinessLayer.BusinessObjects.ExperimentCategory category, bool propagateTransactionExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool DeleteExperimentEffect(BusinessLayer.BusinessObjects.ExperimentEffect effect, bool propagateTransactionExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool DeleteExperimentGroup(BusinessLayer.BusinessObjects.ExperimentGroup group, bool propagateTransactionExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool DeleteExperimentGroupEffect(BusinessLayer.BusinessObjects.ExperimentGroup group, BusinessLayer.BusinessObjects.ExperimentEffect effect, bool propagateTransactionExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool DeleteUserExperimentGroup(BusinessLayer.BusinessObjects.UserExperimentGroup participant, bool propagateTransactionExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool DeleteWorldExperimentGroup(BusinessLayer.BusinessObjects.WorldExperimentGroup participant, bool propagateTransactionExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool DeleteWorldTemplateExperimentGroup(BusinessLayer.BusinessObjects.WorldTemplateExperimentGroup participant, bool propagateTransactionExceptions = false)
        {
            throw new NotImplementedException();
        }

        public bool ClearWorldTemplateExperimentGroups(BusinessLayer.BusinessObjects.WorldTemplate template, bool propagateTransactionExceptions = false)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.Experiment GetExperiment(string experimentId, bool includeExperimentGroups = true)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.Experiment> SearchExperiments(string name, bool exactMatch, string orderBy, int pageNumber, int pageSize, bool includeExperimentGroups = true)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.Experiment> SearchExperiments(string name, bool exactMatch, string category, string status, string orderBy, int pageNumber, int pageSize, bool includeExperimentGroups = true)
        {
            throw new NotImplementedException();
        }

        public IList<BusinessLayer.BusinessObjects.Experiment> GetAvailableExperimentsByUserContext(int userId, string context = "Request", bool includeExperimentGroups = true)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.ExperimentCategory GetExperimentCategory(string name)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.ExperimentCategory> SearchExperimentCategories(string name, string sortBy, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.ExperimentEffect> GetExperimentEffects(string orderBy, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.ExperimentGroup GetExperimentGroup(string experimentGroupId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.UserExperimentGroup> SearchUserExperimentGroups(string username, bool bExactMatch, string experimentId, string experimentStatus, string orderBy, int pageNum, int pageSize, bool includeExperimentGroups = true)
        {
            throw new NotImplementedException();
        }

        public IList<BusinessLayer.BusinessObjects.UserExperimentGroup> GetActiveUserExperimentGroups(int userId, bool includeExperimentGroups = true)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.UserExperimentGroup GetUserExperimentGroup(string userGroupId, bool includeExperimentGroups = true)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.UserExperimentGroup GetUserExperimentGroupByExperimentId(int userId, string experimentId, bool includeExperimentGroups = true)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.WorldExperimentGroup GetWorldExperimentGroup(int zoneInstanceId, int zoneType, string groupId, bool includeExperimentGroups = true)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.WorldExperimentGroup GetWorldExperimentGroupByExperimentId(int zoneInstanceId, int zoneType, string experimentId, bool includeExperimentGroups = true)
        {
            throw new NotImplementedException();
        }

        public IList<BusinessLayer.BusinessObjects.Experiment> GetAvailableExperimentsByWorldContext(int zoneInstanceId, int zoneType, string context = "Request", bool includeExperimentGroups = true)
        {
            throw new NotImplementedException();
        }

        public IList<BusinessLayer.BusinessObjects.WorldExperimentGroup> GetActiveWorldExperimentGroups(int zoneInstanceId, int zoneType, bool includeExperimentGroups = true)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.WorldTemplateExperimentGroup> GetWorldTemplateExperimentGroups(BusinessLayer.BusinessObjects.WorldTemplate template, string orderBy, int pageNumber, int pageSize, bool includeExperimentGroups = true)
        {
            throw new NotImplementedException();
        }
    }
}
