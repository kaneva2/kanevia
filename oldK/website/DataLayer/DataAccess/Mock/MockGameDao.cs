///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using Kaneva.BusinessLayer.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaneva.DataLayer.DataObjects.Mock
{
    public class MockGameDao: IGameDao
    {
        public BusinessLayer.BusinessObjects.Game GetGame(int gameId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.Game GetGame(string gameName)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.Game GetGameByServerId(int serverId)
        {
            throw new NotImplementedException();
        }

        public IList<BusinessLayer.BusinessObjects.Game> GetGamesByOwner(int ownerId, string filter, string orderby)
        {
            throw new NotImplementedException();
        }

        public PagedDataTable GetGamesByOwner(int ownerId, string filter, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public PagedDataTable SearchForGames(string searchString, string filter, string categories, bool bGetMature, bool bThumbnailRequired, int pastDays, int ownerId, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public IList<BusinessLayer.BusinessObjects.Game> GetAllGames(string filter, string orderby)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetGameCategories(int gameId)
        {
            throw new NotImplementedException();
        }

        public int GetGameIdByCommunityId(int communityId)
        {
            throw new NotImplementedException();
        }

        public int UpdateGameThumbnail(int gameId, string imagePath)
        {
            throw new NotImplementedException();
        }

        public int UpdateGameGAAccountID(int gameId, string gaAccountID)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable SearchApartments(bool bHasAPSubscription, bool bShowOnlyAP, int pageNumber, int pageSize, string searchString, string orderBy, ref int totalCount)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable SearchHangouts(bool bHasAPSubscription, bool bShowOnlyAP, bool bOnlyPopulated, bool bOnlyEmptyPopulation, int pageNumber, int pageSize, string searchString, int placeType, bool myCommunitiesOnly, int userId, string orderBy, int wokGameId, string country, int minAge, int maxAge, ref int totalCount)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable Search3DApps(bool onlyAccessPass, bool bGetMature, string searchString, bool bOnlyWithPhotos, string country, int[] communityTypeIds, string orderBy, int pageNumber, int pageSize, int iPlaceTypeId, bool myCommunitiesOnly, bool onlyOwned, int userId, int wokGameId, ref int totalNumRecords)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable Browse3DAppsByRequest(bool ShowOnlyAP, bool HideAP, int userId, ref int totalNumRecords, int pageNumber, int PageSize, int wokGameId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable Browse3DAppsByRequestB(bool ShowOnlyAP, int userId, ref int totalNumRecords, int pageNumber, int PageSize, int wokGameId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetMostPopulated3DApps(bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetMostRaved3DApps(bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetMostVisited3DApps(bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable MostPopulatedCombined(bool ShowOnlyAP, bool HideAP, int userId, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId, string countryFilterBy, string countrySortBy, int ageFilterBy, int ageSortBy, int offset)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetMostRequested(bool ShowOnlyAP, bool HideAP, int userId, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetUserHomeWorldForSearch(int userId, int wokGameId, ref int totalNumRecords)
        {
            throw new NotImplementedException();
        }

        public int AddNewGame(BusinessLayer.BusinessObjects.Game game)
        {
            throw new NotImplementedException();
        }

        public int UpdateGame(BusinessLayer.BusinessObjects.Game game)
        {
            throw new NotImplementedException();
        }

        public int DeleteGame(int gameId, int userId)
        {
            throw new NotImplementedException();
        }

        public int TransactionalGameCreation(BusinessLayer.BusinessObjects.Game game, BusinessLayer.BusinessObjects.GameLicense license, BusinessLayer.BusinessObjects.Community community, int daysLeft)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetGameRatings()
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetGameStatus()
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetGameAccess()
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetGameCategories()
        {
            throw new NotImplementedException();
        }

        public int GetGameCategoryIdByName(string categoryName)
        {
            throw new NotImplementedException();
        }

        public int DeleteGameCategories(int gameId)
        {
            throw new NotImplementedException();
        }

        public int InsertGameCategories(int gameId, int categoryId)
        {
            throw new NotImplementedException();
        }

        public int InsertGameRave(int userId, int gameId)
        {
            throw new NotImplementedException();
        }

        public void ClearZoneCustomizationsCache(int zoneIndex, int instanceId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataSet GetZoneCustomizations(int zoneIndex, int instanceId, float x, float y, float z)
        {
            throw new NotImplementedException();
        }

        public IDictionary<uint, uint[]> GetZoneDownloadSizes(int zoneIndex, int instanceId, uint meshPathTypeId, uint texPathTypeId)
        {
            throw new NotImplementedException();
        }

        public bool IsFrameworkEnabled(int instanceId, int zoneType)
        {
            throw new NotImplementedException();
        }

        public bool UserInGame(int userId, int gameId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetGameUserIn(int userId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.eLOGIN_RESULTS LoginUser(int userId, string userIpAddress, int gameId, int serverId, string actualIp, ref int roleId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.eLOGIN_RESULTS LogoutUser(int userId, int reasonCode, int serverId, string actualIp)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataSet GetChildGameWebCallValues()
        {
            throw new NotImplementedException();
        }

        public System.Data.DataSet GetGameItems(int userId, int gameId, int globalId, int quantity)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.WOKItem.ItemApprovalStatus GetGameItemApprovalStatus(int gameId, int globalId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.CommunityMember.CommunityMemberAccountType GetGamePrivileges(int userId, int gameId)
        {
            throw new NotImplementedException();
        }

        public bool UserAllowedGame(int gameId, int userId)
        {
            throw new NotImplementedException();
        }

        public bool IsUserAllowedGame(int gameId, int userId)
        {
            throw new NotImplementedException();
        }

        public int GetGameAccessByServerId(int serverId)
        {
            throw new NotImplementedException();
        }

        public int AdjustGameBalance(int gameId, string keiPointId, double amount)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetGamePasses(int gameId)
        {
            throw new NotImplementedException();
        }

        public bool AddGamePass(int userId, bool isAdmin, int gameId, int passId)
        {
            throw new NotImplementedException();
        }

        public bool DeleteGamePass(int userId, bool isAdmin, int gameId, int passId)
        {
            throw new NotImplementedException();
        }

        public int InsertGiftedItem(int gameId, int userId, int globalId)
        {
            throw new NotImplementedException();
        }

        public int GetGiftedItemsCount(int gameId, int userId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.API.Leaderboard GetLeaderBoard(int communityId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.API.LeaderboardValue> GetLeaderBoardValues(int communityId, int userId, bool onlyFriends, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.API.LevelValue GetLevelValue(int communityId, int userId)
        {
            throw new NotImplementedException();
        }

        public void EarnLeaderBoard(int communityId, int userId, int newValue)
        {
            throw new NotImplementedException();
        }

        public int SaveLeaderBoard(BusinessLayer.BusinessObjects.API.Leaderboard leaderboard)
        {
            throw new NotImplementedException();
        }

        public int ResetLeaderBoard(int communityId)
        {
            throw new NotImplementedException();
        }

        public void EarnLevel(int communityId, int userId, uint levelNumber, uint percentComplete, string title)
        {
            throw new NotImplementedException();
        }

        public void EarnTitle(int communityId, int userId, string title)
        {
            throw new NotImplementedException();
        }

        public string GetTitle(int communityId, int userId)
        {
            throw new NotImplementedException();
        }

        public int RemovePlayerFromLeaderBoard(int communityId, int userId)
        {
            throw new NotImplementedException();
        }

        public void EarnAchievement(int userId, uint achievementId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.API.AchievementEarned> AchievementForUser(int userId, int communityId, string sortBy, bool showAll, bool onlyFriends, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.API.Achievement> AchievementForApp(int communityId, string sortBy, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.API.UserAchievementData> GetUserAchievementsForApp(int communityId, int userId, string orderBy, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.API.Achievement GetAchievement(uint achievementId)
        {
            throw new NotImplementedException();
        }

        public uint SaveAchievement(BusinessLayer.BusinessObjects.API.Achievement achievement)
        {
            throw new NotImplementedException();
        }

        public int UpdateAchievementThumbnail(uint achievementId, string imageUrl)
        {
            throw new NotImplementedException();
        }

        public int GetTotalAchievementPoints(int communityId, string filter)
        {
            throw new NotImplementedException();
        }

        public int DeleteAchievement(uint achievementId)
        {
            throw new NotImplementedException();
        }

        public int ResetAchievements(int communityId)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.API.AchievementAward> GetAchievementAwards(uint achievementId, string gender)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.Community> GetGamesInWhichUserHasFame(int userId, int pageNumber, int pageSize, string orderBy)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, int> GetAchievementsSummaryForApp(int communityId, int userId)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, int> GetAchievementsSummaryForAllApps(int userId)
        {
            throw new NotImplementedException();
        }

        public void InsertAPIAuth(int communityId, string consumerKey, string consumerSecret)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.API.APIAuthentication GetAPIAuth(int gameId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.API.APIAuthentication GetAPIAuth(string consumerKey, string consumerSecret)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.API.APIAuthentication GetAPIAuth(string consumerKey)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetMostVisited(int userId, bool HideAP, int pageNumber, int pageSize, int constrainCount, int wokGameId, bool includePopulation)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable Top3Dapps(bool onlyAccessPass, bool HideAP, int pageNumber, int pageSize, ref int totalCount, int wokGameId, bool hideOwnerOnly = false)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetUnique3DAppVisits(int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetWorldsByCategory(bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId, string category)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable TopWorldsTour(int userId, bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId)
        {
            throw new NotImplementedException();
        }

        public int ReedemTopWorldReward(int userId, int zoneInstanceId, int zoneType)
        {
            throw new NotImplementedException();
        }

        public bool HasTopWorldRewardBeenRedeemed(int userId, int zoneInstanceId, int zoneType)
        {
            throw new NotImplementedException();
        }

        public int InsertNotifcation(int userId, int packetId, int levelId, int badgeId)
        {
            throw new NotImplementedException();
        }

        public int InsertNotificationAppRequest(int userId, int gameId)
        {
            throw new NotImplementedException();
        }

        public int ClearNotificationAppRequest(int userId, int gameId)
        {
            throw new NotImplementedException();
        }

        public int GetNumberOfNotificationAppRequest(int userId, int gameId)
        {
            throw new NotImplementedException();
        }

        public int GetTotalNotificationAppRequests(int userId)
        {
            throw new NotImplementedException();
        }

        public int GetNumberOfVisitorsForGamesByOwner(int userId, DateTime startDate)
        {
            throw new NotImplementedException();
        }

        public bool InsertAppMessageQueue(int gameId, string msgType, string msgSource)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetAppMessageQueue(int gameId)
        {
            throw new NotImplementedException();
        }

        public bool UpdateAppMessageQueue(string id, string clientResponseId, string clientResponseCode, DateTime clientProcessedDate)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataRow GetGameFaux3DApp(int gameId)
        {
            throw new NotImplementedException();
        }

        public string GetZoneNameFromZoneIndex(int zoneIndex)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.WOKItem> GetScriptAvailableItems(int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public int InsertScriptAvailableItemsIds(int[] globalIds)
        {
            throw new NotImplementedException();
        }

        public int DeleteScriptAvailableItemsIds(int[] globalIds)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataSet GetPremiumItems(int premiumItemTypeId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.MetaGameItem GetMetaGameItem(string itemName)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.MetaGameItem> GetMetaGameItems(string itemType)
        {
            throw new NotImplementedException();
        }

        public int InsertObjectUserInteraction(int userId, int zoneInstanceId, int zoneType, int objPlacementId, string name, string interactionType)
        {
            throw new NotImplementedException();
        }

        public int UpdateLeaderboardObjectName(int objPlacementId, string name)
        {
            throw new NotImplementedException();
        }

        public bool HasUserRavedGameObject(int userId, int objPlacementId)
        {
            throw new NotImplementedException();
        }

        public bool HasUserVisitedGameObject(int userId, int objPlacementId)
        {
            throw new NotImplementedException();
        }

        public int UpdateTeamMembers(List<int> userIds, int objPlacementId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetTeamMembers(IEnumerable<int> objPlacementIds)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetTeamMembers(int objPlacementId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetLeaderboard(int zoneInstanceId, int zoneType, int ravePointValue, int visitPointValue)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetFinalLeaderboard(int zoneInstanceId, int zoneType)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetFinalLeaderboardTeamMembers(int zoneInstanceId, int zoneType)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetUserLeaderboardAwards(int zoneInstanceId, int zoneType, int userId, string username)
        {
            throw new NotImplementedException();
        }

        public bool RedeemUserLeaderboardAward(int userId, IEnumerable<int> worldLeaderboardIds)
        {
            throw new NotImplementedException();
        }

        public bool RedeemUserLeaderboardAward(int userId, int worldLeaderboardId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable CreateLeaderboardFinalResults(int ravePointValue, int visitPointValue)
        {
            throw new NotImplementedException();
        }

        public int InsertLeaderboardFinalResults(int objPlacementId, int zoneInstanceId, int zoneType, string name, int ranking, int points)
        {
            throw new NotImplementedException();
        }

        public int InsertLeaderboardFinalTeamMembers(System.Data.DataTable dtMembers, int worldLeaderboardId, int objPlacementId, int awardQuantity)
        {
            throw new NotImplementedException();
        }

        public int UpdateObjectInteractionsAsProcessed()
        {
            throw new NotImplementedException();
        }

        public int CleanupTeamMembersHistory()
        {
            throw new NotImplementedException();
        }

        public int CleanupWorldLeaderboards()
        {
            throw new NotImplementedException();
        }

        public int CleanupLeaderboardVisits()
        {
            throw new NotImplementedException();
        }

        public Kaneva.BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.Community> SearchWorldPrepopulation(string worldStartsWith, string orderby, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public DataTable GetLinkedZones(int communityId, int zoneInstanceId, int zoneType, int wokGameId)
        {
            throw new NotImplementedException();
        }

        public bool IsChildZone(int zoneInstanceId, int zoneType, out int parentCommunityId)
        {
            throw new NotImplementedException();
        }

        public DataTable GetTutorial(string testGroupId)
        {
            throw new NotImplementedException();
        }

        public bool CanWorldDropGemChests(int userId, int communityId)
        {
            throw new NotImplementedException();
        }

        public AsyncTaskStatus GetAsyncTaskStatusLog(string taskKey)
        {
            throw new NotImplementedException();
        }

        public int UpsertAsyncTaskStatusLog(AsyncTaskStatus status)
        {
            throw new NotImplementedException();
        }

        public int DeleteAsyncTaskStatusLog(string taskKey)
        {
            throw new NotImplementedException();
        }

        public int ArchiveInactiveWorld(int communityId, string archivedVia)
        {
            throw new NotImplementedException();
        }

        public int UnarchiveInactiveWorld(int communityId)
        {
            throw new NotImplementedException();
        }

        public PagedList<ArchivedWorld> GetArchivedWorlds(string worldName, string creatorName, string orderBy,
            int pageNum, int itemsPerPage)
        {
            throw new NotImplementedException();
        }
    }
}
