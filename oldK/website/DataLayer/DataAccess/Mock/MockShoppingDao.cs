///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using Kaneva.BusinessLayer.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaneva.DataLayer.DataObjects.Mock
{
    public class MockShoppingDao: IShoppingDao
    {
        public int AddCustomItem(BusinessLayer.BusinessObjects.WOKItem item, uint animTargetActorGLID)
        {
            throw new NotImplementedException();
        }

        public int AddWebItem(BusinessLayer.BusinessObjects.WOKItem item, bool propagateTransExceptions = false)
        {
            throw new NotImplementedException();
        }

        public int UpdateItemTextureActiveState(int globalId, int itemActiveState)
        {
            throw new NotImplementedException();
        }

        public int DeleteItemFromPendingAdds(int globalId)
        {
            throw new NotImplementedException();
        }

        public int DeleteItemFromDynamicObjects(int globalId)
        {
            throw new NotImplementedException();
        }

        public int DeleteItemFromAllInventory(int globalId)
        {
            throw new NotImplementedException();
        }

        public int UpdateCustomItemTexture(BusinessLayer.BusinessObjects.WOKItem item)
        {
            throw new NotImplementedException();
        }

        public int UpdateCustomItemTextureEncryption(BusinessLayer.BusinessObjects.WOKItem item, string path)
        {
            throw new NotImplementedException();
        }

        public int UpdateCustomItem(BusinessLayer.BusinessObjects.WOKItem item)
        {
            throw new NotImplementedException();
        }

        public int UpdateShopItem(BusinessLayer.BusinessObjects.WOKItem modifiedItem, BusinessLayer.BusinessObjects.WOKItem origItem, int modifyingUserId)
        {
            throw new NotImplementedException();
        }

        public int InsertIntoStoreInventories(int storeId, int itemId)
        {
            throw new NotImplementedException();
        }

        public int DeleteFromStoreInventories(int storeId, int itemId)
        {
            throw new NotImplementedException();
        }

        public int DeleteFromStoreInventories(int itemId)
        {
            throw new NotImplementedException();
        }

        public int DeleteCustomItemThumbnail(int globalId)
        {
            throw new NotImplementedException();
        }

        public int DeletePremiumItem(int globalId)
        {
            throw new NotImplementedException();
        }

        public int UpdatePremiumItemApprovalStatus(BusinessLayer.BusinessObjects.WOKItem item)
        {
            throw new NotImplementedException();
        }

        public int UpdateItemDeniedReason(int globalId, string desc)
        {
            throw new NotImplementedException();
        }

        public string GetItemDeniedReason(int globalId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.WOKItem GetItem(int globalId, bool bAllowUGCTemplate)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.WOKItem> GetItems(IEnumerable<int> globalIds, bool bAllowUGCTemplate, string orderBy)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.WOKItem GetAnimationActor(int globalId)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.WOKItem> GetItemAnimations(int globalId, string filter, bool showPrivate)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetItemAnimations(int globalId, bool showPrivate)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetAnimations(string globalIds)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.WOKItem> GetOwnerItems(string searchString, int ownerId, uint itemCategoryId, int pageSize, int pageNumber, string orderBy)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.WOKItem> SearchItems(string searchString, int ownerId, uint[] itemCategoryIds, int pageSize, int pageNumber, string orderBy, bool showRestricted, bool onlyAccessPass, int showPrivate, bool onlyAnimated)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.WOKItem> GetOwnerItems(string searchString, int ownerId, uint itemCategoryId, int pageSize, int pageNumber, string orderBy, bool bIncludeUGCTemplate)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.WOKItem> GetPremiumItems(int gameId, int pageSize, int pageNumber, string orderBy, bool showPrivate, bool showNonApproved)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.DeedItem> GetWorldDODeedItems(int zoneIndex, int zoneInstanceId, int worldOwnerId)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.DeedItem> GetWorldDOPDeedItems(int zoneIndex, int zoneInstanceId, int worldOwnerId)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.DeedItem> GetDODeedItems(int deedTemplateId, int deedOwnerId)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.DeedItem> GetDOPDeedItems(int deedTemplateId, int deedOwnerId)
        {
            throw new NotImplementedException();
        }

        public int GetCommunityIdFromDeedTemplateId(int deedTemplateId)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.WOKItem> GetBundlesItemBelongsTo(int itemId, string filter, bool onlyPublic = true)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.WOKItem> GetDeedsItemBelongsTo(int itemId, string filter, bool onlyPublic = true)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.WOKItem> GetBundleItems(int bundleId, string filter, bool onlyPublic = true)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.WOKItem> GetDerivedItemsIncludedInBundles(int globalId, string filter)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.WOKItem> GetDerivedItemsIncludedInDeeds(int globalId)
        {
            throw new NotImplementedException();
        }

        public int AddItemToBundle(int bundleGlobalId, int itemGlobalId, int quantity, bool propagateQueryExceptions = false)
        {
            throw new NotImplementedException();
        }

        public int RemoveItemsFromBundle(int bundleGlobalId, List<int> itemGlobalIds)
        {
            throw new NotImplementedException();
        }

        public int RemoveAllFromBundle(int bundleGlobalId, bool propagateQueryExceptions = false)
        {
            throw new NotImplementedException();
        }

        public int GetBundleStoredWebPrice(int globalId)
        {
            throw new NotImplementedException();
        }

        public bool IsItemAccessPass(int globalId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetPasses(string globalIds)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetExclusionGroupIds(string globalIds)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.ItemCategory GetItemCategoryByName(string name)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.ItemCategory GetItemCategoryParentByUploadType(int uploadType)
        {
            throw new NotImplementedException();
        }

        public int GetUploadTypeByItemCategoryId(int categoryId, int retry)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.ItemCategory GetItemCategory(uint categoryId)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.ItemCategory> GetItemCategoriesByParentCategoryId(uint[] parentCategoryIds)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetItemCatsByUploadType(uint uploadType)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.ItemCategory> GetItemCatByUploadTypeActorGroup(uint uploadType, int actorGroup)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.ItemCategory> GetItemCategoriesByFilter(string filter)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.ItemCategory> GetItemCategories(uint itemCategoryId, string filter, int pageSize, int pageNumber, string orderBy)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.ItemCategory> GetItemCategoriesByItemId(int globalId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.WOKItem> GetItemPromotions(uint itemCategoryId, int pageSize)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.ItemReview> GetItemReviews(int globalId, int pageSize, int pageNumber)
        {
            throw new NotImplementedException();
        }

        public int AddItemReview(BusinessLayer.BusinessObjects.ItemReview itemReview)
        {
            throw new NotImplementedException();
        }

        public int DeleteItemReview(uint reviewId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.WOKItem> GetRelatedItems(BusinessLayer.BusinessObjects.WOKItem item, uint itemCategoryId, int pageSize, int pageNumber, bool showRestricted)
        {
            throw new NotImplementedException();
        }

        public int AddItemPurchase(int globalId, int txnType, int userId, uint itemOwnerId, int purchasePrice, uint ownerPrice, string ipAddress, string keiPointId, int quantity, int parentPurchaseId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.PagedList<BusinessLayer.BusinessObjects.ItemPurchase> GetItemPurchases(int userId, int pageSize, int pageNumber)
        {
            throw new NotImplementedException();
        }

        public int GetWeeklySales(int userId, ref int credits, ref int numberOfSales)
        {
            throw new NotImplementedException();
        }

        public int InsertPremiumItemCreditRedemption(int purchaseId, int communityId)
        {
            throw new NotImplementedException();
        }

        public int InsertItemDigg(BusinessLayer.BusinessObjects.ItemDigg itemDigg)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.ItemDigg GetItemDigg(int userId, int globalId)
        {
            throw new NotImplementedException();
        }

        public int AddCategoryToItem(int globalId, uint ItemCategoryId, int categoryIndex)
        {
            throw new NotImplementedException();
        }

        public int DeleteCategoryFromItem(int globalId, int categoryIndex)
        {
            throw new NotImplementedException();
        }

        public int DeleteWOKCategoryItems(int categoryId)
        {
            throw new NotImplementedException();
        }

        public int UpdateCategorysParent(int categoryId, int parentCategoryId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetWOKCategoryItemsByCategoryId(int categoryId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetWOKCategoryItems(int itemID, int categoryId)
        {
            throw new NotImplementedException();
        }

        public int MoveWOKCategoryItem(int oldCategoryId, int newCategoryId, int itemId)
        {
            throw new NotImplementedException();
        }

        public int DeleteCategory(int categoryId)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.WOKItem> GetWOKItemsList()
        {
            throw new NotImplementedException();
        }

        public int UpdateWOKCategory(int categoryId, string category, int parentCategoryId, string description, int userId, string marketingPath)
        {
            throw new NotImplementedException();
        }

        public int InsertWOKCategory(string category, int parentCategoryId, string description, int userId, string marketingPath)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.ItemTemplate> GetItemTemplates()
        {
            throw new NotImplementedException();
        }

        public int CreateItemTemplate(BusinessLayer.BusinessObjects.WOKItem item)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.ItemCategory> GetCategoriesThatHaveItems(string filter)
        {
            throw new NotImplementedException();
        }

        public void AddItemToGame(int globalId, int gameId, int approvalStatus)
        {
            throw new NotImplementedException();
        }

        public int GetBaseItemGlobalId(int globalId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetItems(int globalId, int ugcId, int templateId, int count)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetSpaceDynamicObjects(int playerId, int zoneIndex, int instanceId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetSpaceDynamicObjectParams(string ids)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetSpaceWorldObjectSettings(int playerId, int zoneIndex, int instanceId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetDynamicObjectParams(int objPlacementId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetDynamicObjectPlaylists(int objPlacementId, int zoneIndex, int zoneInstanceId, int globalId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataRow GetDynamicObjectSoundCustomizations(int objPlacementId)
        {
            throw new NotImplementedException();
        }

        public bool AddWorldDOsToCustomDeed(int zoneIndex, int zoneInstanceId, int templateGlobalId)
        {
            throw new NotImplementedException();
        }

        public bool AddWOPSettingsToCustomDeed(int zoneIndex, int zoneInstanceId, int templateGlobalId)
        {
            throw new NotImplementedException();
        }

        public int DeleteAllDOsFromCustomDeed(int deedTemplateId)
        {
            throw new NotImplementedException();
        }

        public int DeleteAllWOPSettingsFromCustomDeed(int deedTemplateId)
        {
            throw new NotImplementedException();
        }

        public void AddItemPath(BusinessLayer.BusinessObjects.ItemPath itemPath)
        {
            throw new NotImplementedException();
        }

        public List<BusinessLayer.BusinessObjects.ItemPath> GetItemPaths(int globalId)
        {
            throw new NotImplementedException();
        }

        public void AddItemPathTexture(BusinessLayer.BusinessObjects.ItemPathTexture itemPathTex)
        {
            throw new NotImplementedException();
        }

        public void AddItemPathFileStore(int globalId, BusinessLayer.BusinessObjects.ItemPathType pathTypeInfo, string filestore)
        {
            throw new NotImplementedException();
        }

        public string GetItemPathFileStore(int globalId, BusinessLayer.BusinessObjects.ItemPathType pathTypeInfo)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.ItemPathType GetItemPathType(string pathType)
        {
            throw new NotImplementedException();
        }

        public uint GetExistingUniqueAssetIdBySHA256(BusinessLayer.BusinessObjects.ItemPathType pathTypeInfo, byte[] hashSHA256)
        {
            throw new NotImplementedException();
        }

        public uint AllocUniqueAssetIdBySHA256(BusinessLayer.BusinessObjects.ItemPathType pathTypeInfo, byte[] hashSHA256, int size, int compressedSize, out bool isNew)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.ItemParameter GetItemParameter(int globalId, int paramTypeId)
        {
            throw new NotImplementedException();
        }

        public void UpdateItemParameter(int globalId, int paramTypeId, string value, bool propagateQueryExceptions = false)
        {
            throw new NotImplementedException();
        }

        public int CreateCustomZoneTemplate(int zoneIndex, int instanceId, int templateId, bool isAdmin)
        {
            throw new NotImplementedException();
        }

        public int ChangeZoneMap(int oldZoneIndex, int zoneInstanceId, int newZoneIndex, string playerName, int templateId, string gameNameNoSpaces, ref int result, ref string url)
        {
            throw new NotImplementedException();
        }

        public int ImportZone(int playerId, int oldZoneIndex, int oldZoneInstanceId, int baseZoneIndexPlain, int srcZoneIndex, int srcZoneInstanceId, bool maintainScripts, ref int result, ref int newZoneIndex)
        {
            throw new NotImplementedException();
        }

        public int InsertChannelZone(int userId, int communityId, int zoneIndex, int instanceId, int zoneType, string name, int serverId, string tiedToServer, string country, int age, int scriptServerId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataRow GetCustomDeedUsage(int deedId)
        {
            throw new NotImplementedException();
        }

        public bool IsOriginalUGCZone(int zoneIndex, int zoneInstanceId)
        {
            throw new NotImplementedException();
        }

        public BusinessLayer.BusinessObjects.WOK3DPlace GetDeedTryOnZone(int deedTemplateId)
        {
            throw new NotImplementedException();
        }

        public int RemoveItemsFromDeed(int deedTemplateId, List<int> itemGlobalIds)
        {
            throw new NotImplementedException();
        }

        public int CopyGlid(int originalGlid)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataRow GetItemByPlacementId(int objPlacementId, int zoneInstanceId, int playerId)
        {
            throw new NotImplementedException();
        }

        public ItemCategory GetItemCategoryByTree(string parentCategory, string subCategory)
        {
            throw new NotImplementedException();
        }

        public int CreateItemPreloadList(string name)
        {
            throw new NotImplementedException();
        }

        public bool UpdateItemPreloadList(int listId, string newName)
        {
            throw new NotImplementedException();
        }

        public int PublishItemPreloadList(int listId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetAllItemPreloadLists()
        {
            throw new NotImplementedException();
        }

        public System.Data.DataRow GetItemPreloadList(int listId)
        {
            throw new NotImplementedException();
        }

        public int GetLatestItemPreloadListVersion()
        {
            throw new NotImplementedException();
        }

        public void AddItemPreloadListItem(int listId, int globalId)
        {
            throw new NotImplementedException();
        }

        public void RemoveItemPreloadListItem(int listId, int globalId)
        {
            throw new NotImplementedException();
        }
        
        public int MergeItemPreloadListItems(int dstListId, int srcListId)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetItemPreloadListItems(int listId)
        {
            throw new NotImplementedException();
        }
    }
}
