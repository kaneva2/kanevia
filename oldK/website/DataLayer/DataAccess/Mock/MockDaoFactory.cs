///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace Kaneva.DataLayer.DataObjects.Mock
{
    /// <summary>
    /// Factory that creates mock data access objects.
    /// 
    /// GoF Design Pattern: Factory.
    /// </summary>
    public class MockDaoFactory : DaoFactory
    {
        public override IBlastDao BlastDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IBlogDao BlogDao
        {
            get { throw new NotImplementedException(); }
        }

        public override ICommentsDao CommentsDao
        {
            get { throw new NotImplementedException(); }
        }

        public override ICommunityDao CommunityDao
        {
            get { return new MockCommunityDao(); }
        }

        public override IConfigurationDao ConfigurationDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IContestDao ContestDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IDevelopmentCompanyDao DevelopmentCompanyDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IErrorLoggingDao ErrorLoggingDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IEventDao EventDao
        {
            get { return new MockEventDao(); }
        }

        public override IExperimentDao ExperimentDao
        {
            get { return new MockExperimentDao(); }
        }

        public override IFameDao FameDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IForumDao ForumDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IGameDao GameDao
        {
            get { return new MockGameDao(); }
        }

        public override IGameDeveloperDao GameDeveloperDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IGameLicenseDao GameLicenseDao
        {
            get { return new MockGameLicenseDao(); }
        }

        public override IGameServerDao GameServerDao
        {
            get { return new MockGameServerDao(); }
        }

        public override IInterestsDao InterestsDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IKachingDao KachingDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IMailDao MailDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IMarketingDao MarketingDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IMediaDao MediaDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IMetricsDao MetricsDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IPromotionsDao PromotionsDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IRaveDao RaveDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IScriptGameItemDao ScriptGameItemDao
        {
            get { return new MockScriptGameItemDao(); }
        }

        public override ISGCustomDataDao SGCustomDataDao
        {
            get { return new MockSGCustomDataDao(); }
        }

        public override ISGFrameworkSettingsDao SGFrameworkSettingsDao
        {
            get { return new MockSGFrameworkSettingsDao(); }
        }

        public override IShoppingDao ShoppingDao
        {
            get { return new MockShoppingDao(); }
        }

        public override ISiteManagementDao SiteManagementDao
        {
            get { throw new NotImplementedException(); }
        }

        public override ISitemapDao SitemapDao
        {
            get { throw new NotImplementedException(); }
        }

        public override ISiteSecurityDao SiteSecurityDao
        {
            get { throw new NotImplementedException(); }
        }

        public override ISubscriptionDao SubscriptionDao
        {
            get { return new MockSubscriptionDao(); }
        }

        public override ITransactionDao TransactionDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IUserDao UserDao
        {
            get { return new MockUserDao(); }
        }

        public override IViolationsDao ViolationsDao
        {
            get { throw new NotImplementedException(); }
        }

        public override IWorldTemplateDao WorldTemplateDao
        {
            get { return new MockWorldTemplateDao(); }
        }
    }
}
