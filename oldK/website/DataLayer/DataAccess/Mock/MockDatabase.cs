///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;

namespace Kaneva.DataLayer.DataObjects.Mock
{
    /// <summary>
    /// Class representing an in-memory mock database for use in unit testing.
    /// </summary>
    public class MockDatabase
    {
        private Dictionary<string, MockDatabaseTable> _tables;

        public MockDatabase()
        {
            _tables = new Dictionary<string, MockDatabaseTable>();
        }

        public void CreateTableIfNotExists(MockDatabaseTable table)
        {
            MockDatabaseTable tOut;
            if (!_tables.TryGetValue(table.TableName, out tOut))
                _tables.Add(table.TableName, table);
        }

        public void DropTableIfExists(string tableName)
        {
            MockDatabaseTable tOut;
            if (!_tables.TryGetValue(tableName, out tOut))
                _tables.Remove(tableName);
        }

        public MockDatabaseTable GetTable(string tableName)
        {
            MockDatabaseTable tOut;
            if (!_tables.TryGetValue(tableName, out tOut))
                throw new ArgumentException("Unable to find table with name " + tableName);
            return tOut;
        }
    }
}

