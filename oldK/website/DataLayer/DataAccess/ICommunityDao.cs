///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;
using System.Data;

namespace Kaneva.DataLayer.DataObjects
{
    public interface ICommunityDao
    {
        Community GetCommunity(int communityId);
        CommunityStats GetCommunityStats(int communityId);
        CommunityPreferences GetCommunityPreferences(int communityId);
        CommunityMember GetCommunityMember(int communityId, int userId);
        PagedList<CommunityMember> GetCommunityMembers (int communityId, UInt32 communityMemberStatusId,
            bool bIncludeSuspended, bool bIncludePending, bool bIncludeDeleted, bool bIncludeRejected,
            string filter, string orderby, int pageNumber, int pageSize, bool bShowMature, string userNameFilter, bool includeNotificationPref);
        List<CommunityCategory> GetCommunityCategories ();
        List<CommunityCategory> GetCommunityCategories(int communityId);
        int InsertCommunityCategory(int communityId, int categoryId);
        int DeleteCommunityCategories(int communityId);
        List<CommunityPlaceType> GetCommunityPlaceTypes();
        List<CommunityTab> GetCommunityTabs (int CommunityId, int CommunityTypeId);
        List<CommunityTab> GetCommunityTabsByCommunityType (int CommunityTypeId);
        int UpdateCommunityTabs (int tabId, int communityId, int communityTypeId, bool isSelected, bool isDefault);
        string GetCommunityAboutText (int communityId);
        int UpdateCommunityAboutText (int communityId, string aboutText);
        PagedList<Community> GetCommunities(int userId, int[] communityTypeIds, string orderBy, string filter, int pageNumber, int pageSize);
        PagedList<Community> GetCommunities(int userId, int[] communityTypeIds, string orderBy, string filter, int pageNumber, int pageSize, bool with3dAppInfo, string nameFilter);
        Community GetApartmentCommunity(int zoneInstanceId);
        PagedList<Community> SearchCommunities(bool onlyAccessPass, bool bGetMature, string searchString,
            bool bOnlyWithPhotos, string country, int pastDays, int[] communityTypeIds, 
            string orderBy, int pageNumber, int pageSize, int iPlaceTypeId);
        int UpdateCommunityURL(int communityId, string url);

        WOK3DPlace Get3DPlace (int zoneInstanceId, int zoneType);
        WOK3DPlace Get3DPlace(int channelZoneId);
        WOK3DPlace Get3DPlace(string worldName);
        PagedList<WOK3DPlace> Search3DPlaceByName(string name, int page, int itemsPerPage);
        bool IsUserWOK3DPlaceOwner(int userId, int zoneIndex, int zoneInstanceId);
        int Set3DPlaceTiedStatus(int channelZoneId, int serverId, string tiedToServer);
        int Set3DPlaceSpinDownDelayMinutes(int zoneInstanceId, int zoneType, int spinDownDelayMinutes);
        WOK3DApp GetWOK3DApp(int communityId);

        DataTable GetWorldPasses(int wokGameId, int gameId, int zoneInstanceId, int zoneType);
        DataTable GetCommunityPasses(int communityId, int communityType);
        bool AddCommunityPass(int userId, bool isAdmin, int communityId, int communityType, int passId);
        bool DeleteCommunityPass(int userId, bool isAdmin, int communityId, int communityType, int passId);

        Community GetPersonalCommunityFromPlayerId (int playerId);
        int GetNumberOfCommunities(int creatorId, int statusId, int[] communityTypes);
        int GetNumberOf3DApps(int creatorId, int statusId);

        int InsertCommunityPreferences(Community community);
        int UpdateCommunityPreferences(Community community);
        void ShareCommunity(int communityId, int userId, int sentUserId, string email);
        int InsertCommunity(Community community);
        int IsCommunityNameTaken(bool isPersonal, string communityName, int communityId);
        int IsCommunityURLTaken(string url, int communityId);
        int InsertCommunityMember(CommunityMember communityMember);
        bool IsCommunityValid(int communityId);

        void UpdateChannelViews(int userId, int communityId, bool browseAnon, string ipAddress);
        int UpdateCommunity(int communityId, int ownerId, bool IsPersonal, string thumbnailPath, string thumbnailType, bool hasThumbnail);
        int UpdateCommunity(Community community);
        int UpdateCommunityThumb(int communityId, string thumbnailPath, string dbColumn);

        int DeleteAllCommunityAssets(int communityId);
        int DeleteAllCommunityMembers(int communityId);
        int DeleteCommunityRecoverable(int communityId);
        int DeleteCommunityThumbs(int communityId);

        int RestoreDeletedCommunity(int communityId);

        bool IsActiveCommunityMember (int communityId, int userId);

        int UpdateCommunityMember(int communityId, int userId, UInt32 statusId);
        int UpdateCommunityMember(int communityId, int userId, int accountTypeId, string newsletter, string allowAssetUploads, string allowForumUse);
        int UpdateCommunityMember(int communityId, int userId, CommunityMember.CommunityMemberAccountType accountType);
        int RemoveMemberFromAllOwnersWorlds (int ownerId, int memberId);

        int GetCommunityGameId(int communityId);
        int GetCommunityIdFromGameId (int gameId);
        int GetCommunityIdFromName(string communityName, bool isPersonal);
        uint GetCommunityIdFromFaux3DAppZoneIndex (int zoneIndex);
        int GetWorldOwnerFromWorldIds(int wokGameId, int gameId, int zoneIndex, int zoneInstanceId, int zoneType);
        int GetCommunityIdFromWorldIds(int wokGameId, int gameId, int zoneIndex, int zoneInstanceId, int zoneType);
        PagedList<Community> GetUserTopWorlds (int userId, int[] communityTypeIds, string orderBy, string filter, int pageNumber, int pageSize, bool includeCreditBalance, int premiumItemDaysPendingBeforeRedeem);

        void InsertAPIAuth(int communityId, string consumerKey, string consumerSecret);
        APIAuthentication GetAPIAuth(int gameId);
        APIAuthentication GetAPIAuth(string consumerKey, string consumerSecret);
        APIAuthentication GetAPIAuth(string consumerKey);

        int UpdateCommunityParentId(int communityId, int parentId);
        int InsertHomeCommunityIdAsChildWorld(int communityId);
        int RemoveHomeCommunityIdAsChildWorld(int communityId);
    }
}
