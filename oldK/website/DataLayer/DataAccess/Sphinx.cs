///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

using System.Configuration;
using log4net;

namespace Kaneva.DataLayer.DataObjects
{
    sealed class Sphinx
    {
        private static bool[] _lookup;

        // Note: constructor is 'private'
        static Sphinx()
        {
            _lookup = new bool[65536];
            for (char c = '0'; c <= '9'; c++) _lookup[c] = true;
            for (char c = 'A'; c <= 'Z'; c++) _lookup[c] = true;
            for (char c = 'a'; c <= 'z'; c++) _lookup[c] = true;
            _lookup[' '] = true;
            _lookup['.'] = true;
            _lookup['_'] = true;
            _lookup['@'] = true;
            _lookup['-'] = true;
        }

        public string Host
        {
            get
            {
                if (ConfigurationManager.AppSettings.Get("Sphinx.server") != null)
                {
                    return ConfigurationManager.AppSettings.Get("Sphinx.server");
                }
                else
                {
                    return "localhost";
                }
            }
        }

        public int Port
        {
            get
            {
                if (ConfigurationManager.AppSettings.Get("Sphinx.port") != null)
                {
                    return Convert.ToInt32(ConfigurationManager.AppSettings.Get("Sphinx.port"));
                }
                else
                {
                    return 9312;
                }
            }
        }

        public string CleanSphinxSearchString(string strSearch)
        {
            //\()|-!@~/^$*
            strSearch = strSearch.Replace("$", "\\$");
            strSearch = strSearch.Replace("@", "\\@");
            strSearch = strSearch.Replace("*", "\\*");
            strSearch = strSearch.Replace("!", "\\!");
            strSearch = strSearch.Replace("(", "\\(");
            strSearch = strSearch.Replace(")", "\\)");
            strSearch = strSearch.Replace("^", "\\^");
            strSearch = strSearch.Replace("~", "\\~");
            return strSearch;
        }

        // Strip a string that is passed to Sphinx for only certain values
        public string StripSphinxSearchString(string str)
        {
            char[] buffer = new char[str.Length];
            int index = 0;
            foreach (char c in str)
            {
                if (_lookup[c])
                {
                    buffer[index] = c;
                    index++;
                }
            }
            return new string(buffer, 0, index);
        }

    }
}