///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.DataLayer.DataObjects
{
    public interface IPromotionsDao
    {
        List<Promotion> GetPromotionsWithSubscriptionByPromoType(int promotionTypeID);
        List<Promotion> GetPromotionsByPromotionAndPassType(int promotionTypeID, int passTypeID);
        List<Promotion> GetActivePromotions(string promoOfferTypeIDList);
        List<Promotion> GetActivePromotions(string promoOfferTypeIDList, string keiPointType);
        List<Promotion> GetActivePromotions(string promoOfferTypeIDList, string keiPointType, Double minimumPointAmount);
        List<Promotion> GetActivePromotions(string promoOfferTypeIDList, string keiPointType, Double minimumPointAmount, DateTime dateNOW);
        Promotion GetPromotionsByPromoId(int promotionalID);
        int GetUserTransactionCount(int userId, string logicalOperator, string promotionTypeFilter);
        DataTable GetUserPurchasesByType(int userId, int promotionType);
        DataTable GetPromotionalOfferItemsByGender(int promoId, string genderList);
        int GetBestSellingPromotion(string promoOfferTypeIDList);
        DataTable GetPassGroupsAssociatedWPromotion(uint promotionId);
        List<Promotion> GetAllActiveSubscriptionBasedPromotions(string excludedPasses, string includedPasses);
        List<Promotion> GetAllActiveUnpurchasedSubscriptionBasedPromotions(int userID, string passGroupFilter);
    }
}
