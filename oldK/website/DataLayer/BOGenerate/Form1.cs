///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CoreLab.MySql;

using System.Globalization;
using System.Threading;

namespace BOGenerate
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            mySqlConnection1.ConnectionString = txtDBString.Text;

            DataTable dtResults = new DataTable ();
            string strResult = "";
            string strConstructor = "";
            string strCommonSQL = "";
            string strPrimaryKey = "unknown";

            string strTable = CamelCase (txtTableName.Text.Replace ("_"," ")).Replace (" ","");

            try
            {
                // Build the query
                string mysqlQuery = "describe " + txtTableName.Text;
                
                mySqlConnection1.Open();

                // Get results
                MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(mysqlQuery, mySqlConnection1);
                mySqlDataAdapter.Fill(dtResults);
            }
            catch (Exception exc)
            {
                textBox2.Text = exc.Message;
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                mySqlConnection1.Close();
            }

            // BO CODE
            string strColumn = "";
            string strDataType = "";

            strResult += "namespace Kaneva.BusinessLayer.BusinessObjects\r\n";
            strResult += "{\r\n";
            strResult += "    [Serializable]\r\n";
            strResult += "    public class " + strTable + "\r\n";
            strResult += "    {\r\n";

            if (dtResults != null)
            {
                // Declare vars
                foreach (DataRow row in dtResults.Rows)
                {
                    strColumn = GetColumnName(row["field"].ToString());
                    strDataType = GetDataType (row["type"].ToString());

                    strResult += "        private " + strDataType + " _" + strColumn + ";\r\n";

                    if (row["key"].ToString ().ToUpper().Equals ("PRI"))
                    {
                        strPrimaryKey = row["field"].ToString();
                    }
                }

                strResult += "\r\n";

                // Declare Constructor
                strResult += "    public " + strTable + " (";
                for (int i = 0; i < dtResults.Rows.Count; i++)
                {
                    strColumn = GetColumnName(dtResults.Rows[i]["field"].ToString());
                    strDataType = GetDataType(dtResults.Rows[i]["type"].ToString());

                    strResult += strDataType + " " + strColumn.Substring(0, 1).ToLower() + strColumn.Substring(1);

                    if (!i.Equals(dtResults.Rows.Count - 1))
                    {
                        strResult += ", ";
                    }
                }
                strResult += "    )\r\n    {\r\n";
                foreach (DataRow row in dtResults.Rows)
                {
                    strColumn = GetColumnName(row["field"].ToString());
                    strDataType = GetDataType(row["type"].ToString());

                    strResult += "        this._" + strColumn + " = " + strColumn.Substring(0, 1).ToLower() + strColumn.Substring(1) + ";\r\n";
                }
                strResult += "    }";

                strResult += "\r\n";
                strResult += "\r\n";

                // Declare Props
                foreach (DataRow row in dtResults.Rows)
                {
                    strColumn = GetColumnName(row["field"].ToString());
                    strDataType = GetDataType(row["type"].ToString());

                    strResult += "    public " + strDataType + " " + strColumn + "\r\n";
                    strResult += "    {\r\n";
                    strResult += "        get {return _" + strColumn + ";}\r\n";
                    strResult += "        set {_" + strColumn + " = value;}\r\n";
                    strResult += "    }\r\n\r\n";
                }
            }

            strResult += "\r\n}\r\n";





            // Constructor
            strConstructor += "return new " + strTable + " (";

            for (int i = 0; i < dtResults.Rows.Count; i++)
            {
                strConstructor += GetConstructor(dtResults.Rows[i]["field"].ToString(), dtResults.Rows[i]["type"].ToString()) + " ";

                if (!i.Equals(dtResults.Rows.Count -1))
                {
                    strConstructor += ", ";
                }

                if (i > 0 && (i % 4) == 0)
                {
                    strConstructor += "\r\n";
                }
            }

            strConstructor += ")";




            // Common SQL
            strCommonSQL = "string sqlString = \"INSERT INTO " + txtTableName.Text + " (\" +\r\n";
            strCommonSQL += "    \" ";

            for (int i = 0; i < dtResults.Rows.Count; i++)
            {
                strCommonSQL += dtResults.Rows[i]["field"].ToString();
                if (!i.Equals(dtResults.Rows.Count - 1))
                {
                    strCommonSQL += ", ";
                }
                if (i > 0 && (i % 5) == 0)
                {
                    strCommonSQL += "\" +\r\n";
                    strCommonSQL += "    \" ";
                }
            }

            strCommonSQL += "\" +\r\n    \") VALUES (\" +\r\n    \"";

            for (int i = 0; i < dtResults.Rows.Count; i++)
            {
                strCommonSQL += " @" + dtResults.Rows[i]["field"].ToString();
                if (!i.Equals(dtResults.Rows.Count - 1))
                {
                    strCommonSQL += ", ";
                }
                if (i > 0 && (i % 5) == 0)
                {
                    strCommonSQL += "\" +\r\n";
                    strCommonSQL += "    \" ";
                }
            }
            strCommonSQL += "\" +\r\n    \")\";\r\n\r\n";

            strCommonSQL += "    List < IDbDataParameter > parameters = new List<IDbDataParameter>();\r\n";
            for (int i = 0; i < dtResults.Rows.Count; i++)
            {
                strCommonSQL += "    parameters.Add(new MySqlParameter(\"@" + dtResults.Rows[i]["field"] + "\", " + strTable + "." + dtResults.Rows[i]["field"] + "));\r\n";
            }
            strCommonSQL += "    return Db.DB_NAME_HERE.ExecuteNonQuery(sqlString, parameters);\r\n\r\n";

            
            // Select
            strCommonSQL += "string sqlString = \"SELECT ";
            for (int i = 0; i < dtResults.Rows.Count; i++)
            {
                strCommonSQL += dtResults.Rows[i]["field"].ToString();
                if (!i.Equals(dtResults.Rows.Count - 1))
                {
                    strCommonSQL += ", ";
                }
                else
                {
                    strCommonSQL += "\" +\r\n";
                }
                if (i > 0 && (i % 5) == 0)
                {
                    strCommonSQL += "\" +\r\n";
                    strCommonSQL += "    \"";
                }
            }

            strCommonSQL += "    \" FROM " + txtTableName.Text + "\"+\r\n";
            strCommonSQL += "    \" WHERE " + strPrimaryKey + " = @" + CamelCase(strPrimaryKey.Replace("_", " ")).Replace(" ", "") + "\r\n\r\n";


            // Update
            strCommonSQL += "string sqlString = \"UPDATE " + txtTableName.Text + " SET\"+\r\n";

            for (int i = 0; i < dtResults.Rows.Count; i++)
            {
                strCommonSQL += "    \"" + dtResults.Rows[i]["field"].ToString() + "=@" + dtResults.Rows[i]["field"].ToString() + "";
                if (!i.Equals(dtResults.Rows.Count - 1))
                {
                    strCommonSQL += ", ";
                }

                strCommonSQL += "\"+\r\n";
            }
            strCommonSQL += "    \" WHERE " + strPrimaryKey + " = @" + CamelCase(strPrimaryKey.Replace("_", " ")).Replace(" ", "") + "\"\r\n\r\n";



            txtCommonSQL.Text = strCommonSQL;
            textBox2.Text = strResult;
            txtConstructor.Text = strConstructor;           
        }

        private string GetConstructor(string strField, string strDataType)
        {
            if (strDataType.StartsWith("int"))
            {
                if (strDataType.EndsWith("unsigned"))
                {
                    return "Convert.ToUInt32(row[\"" + strField + "\"])";
                }
                else
                {
                    return "Convert.ToInt32(row[\"" + strField + "\"])";
                }
            }
            if (strDataType.StartsWith("enum('Y','N')"))
            {
                return "row[\"" + strField + "\"].ToString().Equals(\"Y\")";
            }
            if (strDataType.StartsWith("tinyint"))
            {
                return "Convert.ToInt32(row[\"" + strField + "\"])";
            }
            else if (strDataType.StartsWith("varchar"))
            {
                return "row[\"" + strField + "\"].ToString()";
            }
            else if (strDataType.StartsWith("datetime"))
            {
                return "Convert.ToDateTime(row[\"" + strField + "\"])";
            }
            else if (strDataType.StartsWith("text"))
            {
                return "row[\"" + strField + "\"].ToString()";
            }
            else if (strDataType.StartsWith("decimal"))
            {
                return "Convert.ToDouble(row[\"" + strField + "\"])";
            }
            else if (strDataType.StartsWith("double"))
            {
                return "Convert.ToDouble(row[\"" + strField + "\"])";
            }
            else if (strDataType.StartsWith("bigint"))
            {
                if (strDataType.EndsWith("unsigned"))
                {
                    return "Convert.ToUInt64(row[\"" + strField + "\"])";
                }
                else
                {
                    return "Convert.ToInt64(row[\"" + strField + "\"])";
                }
            }

            return "row[\"" + strField + "\"].ToString()";
        }

        private string GetColumnName(string strField)
        {
            // Handle reverse fields
            if (strField.StartsWith ("_"))
            {
                strField = "rev" + strField;
            }

            string strColumn = "";
            string[] columnsName = (strField).Split('_');

            for (int i = 0; i < columnsName.Length; i++)
            {
                strColumn = strColumn + columnsName[i].Substring(0, 1).ToUpper() + columnsName[i].Substring(1);
            }

            return strColumn;
        }

        private string GetDataType(string strDataType)
        {
            if (strDataType.StartsWith ("int"))
            {
                if (strDataType.EndsWith ("unsigned"))
                {
                    return "UInt32";
                }
                else
                {
                    return "int";
                }
            }
            if (strDataType.StartsWith("enum('Y','N')"))
            {
                return "bool";
            }
            if (strDataType.StartsWith("tinyint"))
            {
                return "int";
            }
            else if (strDataType.StartsWith("varchar"))
            {
                return "string";
            }
            else if (strDataType.StartsWith("datetime"))
            {
                return "DateTime";
            }
            else if (strDataType.StartsWith("text"))
            {
                return "string";
            }
            else if (strDataType.StartsWith("decimal"))
            {
                return "Double";
            }
            else if (strDataType.StartsWith("bigint"))
            {
                if (strDataType.EndsWith("unsigned"))
                {
                    return "UInt64";
                }
                else
                {
                    return "Int64";
                }
            }

            return "string";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtDBString.Text = "User Id=root;Password=glue2004!;Host=localhost;Database=kaneva;";
        }

        private string CamelCase(string text)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            return textInfo.ToTitleCase(text);
        }

    }
}