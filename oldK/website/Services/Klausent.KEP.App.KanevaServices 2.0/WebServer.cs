///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace KlausEnt.KEP.Kaneva.KanevaService
{
    class WebServer
    {
        public WebServer(string servername, string nic1, string nic2)
		{
            m_server = servername;
            m_nic1 = nic1;
            m_nic2 = nic2;
		}

		public string servername
		{
			get 
			{
                return m_server;
			}
		}

        public string nic1
		{
			get 
			{
				return m_nic1;
			}
		}

        public string nic2
        {
            get
            {
                return m_nic2;
            }
        }

		private string m_server;
        private string m_nic1;
        private string m_nic2;
	}
}
