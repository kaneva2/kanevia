///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;

namespace KlausEnt.KEP.Kaneva.KanevaService
{
	/// <summary>
	/// Summary description for BillingInstaller.
	/// </summary>
	[RunInstaller(true)]
	public class ServiceInstaller : System.Configuration.Install.Installer
	{
		private System.ServiceProcess.ServiceInstaller serviceInstaller1;
		private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller1;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ServiceInstaller ()
		{
			// This call is required by the Designer.
			InitializeComponent();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Install
		/// </summary>
		/// <param name="stateServer"></param>
		public override void Install (IDictionary stateServer)
		{
			Microsoft.Win32.RegistryKey system,currentControlSet,services,service; 

			try
			{
				//Let the project installer do its job
				base.Install(stateServer);

				//Open the HKEY_LOCAL_MACHINE\SYSTEM key
				system = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("System");
				currentControlSet = system.OpenSubKey("CurrentControlSet");
				services = currentControlSet.OpenSubKey("Services");
				service = services.OpenSubKey (this.serviceInstaller1.ServiceName, true);
				service.SetValue ("Description", "Runs Web Stats Logging and reporting");
				//(Optional) Add some custom information your service will use...
				//config = service.CreateSubKey("Parameters");
			}
			catch(Exception e)
			{
				Console.WriteLine ("An exception was thrown during service installation:\n" + e.ToString());
			}
		}

		public override void Uninstall(IDictionary stateServer)
		{
			Microsoft.Win32.RegistryKey system,currentControlSet,services,service;

			try
			{
				//Drill down to the service key and open it with write permission
				system = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("System");
				currentControlSet = system.OpenSubKey("CurrentControlSet");
				services = currentControlSet.OpenSubKey("Services");
				service = services.OpenSubKey(this.serviceInstaller1.ServiceName, true);
				//Delete any keys you created during installation (or that your service created)
				service.DeleteSubKeyTree("Parameters");
			}
			catch(Exception e)
			{
				Console.WriteLine("Exception encountered while uninstalling service:\n" + e.ToString());
			}
			finally
			{
				//Let the project installer do its job
				base.Uninstall(stateServer);
			}
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.serviceInstaller1 = new System.ServiceProcess.ServiceInstaller();
			this.serviceProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
			// 
			// serviceInstaller1
			// 
			this.serviceInstaller1.ServiceName = "Kaneva Services 2.0";
			this.serviceInstaller1.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
			// 
			// serviceProcessInstaller1
			// 
			this.serviceProcessInstaller1.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
			this.serviceProcessInstaller1.Password = null;
			this.serviceProcessInstaller1.Username = null;
			// 
			// ServiceInstaller 
			// 
			this.Installers.AddRange(new System.Configuration.Install.Installer[] {
																					  this.serviceInstaller1,
																					  this.serviceProcessInstaller1});
		}
		#endregion
	}
}
