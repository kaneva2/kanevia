///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.Xml;
using log4net;

namespace KlausEnt.KEP.Kaneva.KanevaService
{
    /// <summary>
    /// Summary description for ContentServerSectionHandler.
    /// </summary>
    public class WebStatsSectionHandler : IConfigurationSectionHandler
    {
        public WebStatsSectionHandler()
        {
        }

        public object Create (object parent, object configContext, System.Xml.XmlNode section)
        {
            XmlNodeList contentServerSettings;
            System.Collections.Hashtable htWebServers = new System.Collections.Hashtable();

            contentServerSettings = section.SelectNodes("WebServers//webserver");

            foreach (XmlNode nodeTracker in contentServerSettings)
            {
                try
                {
                    //Add them to the card types collection
                    WebServer contentServer = new WebServer (nodeTracker.Attributes.GetNamedItem("servername").Value, nodeTracker.Attributes.GetNamedItem("nic1").Value, nodeTracker.Attributes.GetNamedItem("nic2").Value);
                    htWebServers.Add(nodeTracker.Attributes.GetNamedItem("servername").Value, contentServer);
                }
                catch (Exception exc)
                {
                   m_logger.Error("Error reading content server configuration", exc);
                }

            }

            return htWebServers;
        }

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    }

}
