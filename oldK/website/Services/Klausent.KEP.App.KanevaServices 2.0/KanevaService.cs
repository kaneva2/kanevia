///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using System.Text;
using System.Net;
using System.Security.Cryptography;
using System.Web.Security;
using System.Timers;
using System.IO;
using log4net;
using KlausEnt.KEP.Kaneva.framework.widgets;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.KanevaService
{
	public class KanevaService : System.ServiceProcess.ServiceBase
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		static KanevaService ()
		{
			// Set up log for net
			string s = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
			FileInfo l_fi = new FileInfo (s);
			log4net.Config.DOMConfigurator.Configure (l_fi);
		}

		public KanevaService ()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();

			// Create service timers
            CreateTimers ();

			// Run on startup
            if (WebStatCheckEnabled)
            {
                RunWebStatCheck();
            }
		}

		// The main entry point for the process
		static void Main ()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new KanevaService() };
			System.ServiceProcess.ServiceBase.Run (ServicesToRun);
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
            this.ServiceName = "Kaneva Services 2.0";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		protected override void OnStart (string[] args)
		{
            if (UGCConversionEnabled) 
			{
                tmrUGCConversion.AutoReset = true;
                tmrUGCConversion.Enabled = true;
                tmrUGCConversion.Start();
			}

			if (EmailNotificationsEnabled) 
			{	
				tmrEmailNotification.AutoReset = true;
				tmrEmailNotification.Enabled = true;
				tmrEmailNotification.Start ();
			}

			if (SummaryTablesUpdateEnabled) 
			{
				tmrSummaryTables.AutoReset = true;
				tmrSummaryTables.Enabled = true;
				tmrSummaryTables.Start ();
			}

			if (DatabaseCleanupEnabled)
			{
				tmrDatabaseCleanup.AutoReset = true;
				tmrDatabaseCleanup.Enabled = true;
				tmrDatabaseCleanup.Start ();
			}

			if (VideoImageGenerationEnabled) 
			{
				tmrVideoImageGeneration.AutoReset = true;
				tmrVideoImageGeneration.Enabled = true;
				tmrVideoImageGeneration.Start ();
			}

			if (FunctionalityCheckEnabled)
			{
				tmrFunctionalityCheck.AutoReset = true;
				tmrFunctionalityCheck.Enabled = true;
				tmrFunctionalityCheck.Start ();
			}

            if (WebStatCheckEnabled)
			{
                tmrWebStatCheck.AutoReset = true;
                tmrWebStatCheck.Enabled = true;
                tmrWebStatCheck.Start();
			}

            if (WebStatEmailEnabled)
			{
                tmrWebStatEmail.AutoReset = true;
                tmrWebStatEmail.Enabled = true;
                tmrWebStatEmail.Start();
			}

            if (BirthdayGiftEnabled)
            {
                tmrBirthdayGift.AutoReset = true;
                tmrBirthdayGift.Enabled = true;
                tmrBirthdayGift.Start();
            }

            if (AnniversaryGiftEnabled)
            {
                tmrAnniversaryGift.AutoReset = true;
                tmrAnniversaryGift.Enabled = true;
                tmrAnniversaryGift.Start();
            }
            
		}
 
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop ()
		{
            if (UGCConversionEnabled)
            {
                tmrUGCConversion.AutoReset = false;
                tmrUGCConversion.Enabled = false;
            }

			if (EmailNotificationsEnabled) 
			{	
				tmrEmailNotification.AutoReset = false;
				tmrEmailNotification.Enabled = false;
			}

			if (SummaryTablesUpdateEnabled) 
			{
				tmrSummaryTables.AutoReset = false;
				tmrSummaryTables.Enabled = false;
			}

			if (DatabaseCleanupEnabled)
			{
				tmrDatabaseCleanup.AutoReset = false;
				tmrDatabaseCleanup.Enabled = false;
			}

			if (VideoImageGenerationEnabled) 
			{
				tmrVideoImageGeneration.AutoReset = false;
				tmrVideoImageGeneration.Enabled = false;
			}

			if (FunctionalityCheckEnabled)
			{
				tmrFunctionalityCheck.AutoReset = false;
				tmrFunctionalityCheck.Enabled = false;
			}

            if (WebStatCheckEnabled)
            {
                tmrWebStatCheck.AutoReset = false;
                tmrWebStatCheck.Enabled = false;
            }

            if (WebStatEmailEnabled)
            {
                tmrWebStatEmail.AutoReset = false;
                tmrWebStatEmail.Enabled = false;
            }
            
            if (BirthdayGiftEnabled)
            {
                tmrBirthdayGift.AutoReset = false;
                tmrBirthdayGift.Enabled = false;
            }
            
            if (BirthdayGiftEnabled)
            {
                tmrAnniversaryGift.AutoReset = false;
                tmrAnniversaryGift.Enabled = false;
            }

		}

		/// <summary>
		/// Pause this service
		/// </summary>
		protected override void OnPause () 
		{
            if (UGCConversionEnabled)
            {
                tmrUGCConversion.Stop();
            }

			if (EmailNotificationsEnabled) 
			{	
				tmrEmailNotification.Stop ();
			}

			if (SummaryTablesUpdateEnabled) 
			{
				tmrSummaryTables.Stop ();
			}

			if (DatabaseCleanupEnabled)
			{
				tmrDatabaseCleanup.Stop ();
			}

			if (VideoImageGenerationEnabled) 
			{
				tmrVideoImageGeneration.Stop ();
			}

			if (FunctionalityCheckEnabled)
			{
				tmrVideoImageGeneration.Stop ();
			}
		}

		/// <summary>
		/// Continue this service
		/// </summary>
		protected override void OnContinue () 
		{
            if (UGCConversionEnabled)
            {
                tmrUGCConversion.Start();
            }

			if (EmailNotificationsEnabled) 
			{	
				tmrEmailNotification.Start ();
			}

			if (SummaryTablesUpdateEnabled) 
			{
				tmrSummaryTables.Start ();
			}

			if (DatabaseCleanupEnabled)
			{
				tmrDatabaseCleanup.Start ();
			}

			if (VideoImageGenerationEnabled) 
			{
				tmrVideoImageGeneration.Start ();
			}

			if (FunctionalityCheckEnabled)
			{
				tmrVideoImageGeneration.Start ();
			}
		}

		/// <summary>
		/// tmrSummaryTables_Elapsed
		/// 
		/// Call the update_report_stats stored proc
		protected void tmrSummaryTables_Elapsed (object sender, System.Timers.ElapsedEventArgs e) 
		{
			try
			{
				tmrSummaryTables.Stop ();
				m_logger.Info ("Summary Tables Updater - update_report_stats - START");
				Services.CallUpdateReportStats ();
				m_logger.Info ("Summary Tables Updater - update_report_stats - END");
			}
			catch (Exception exc)
			{
				m_logger.Error ("Summary Tables Updater - update_report_stats - exception", exc);
			}
			finally
			{
				tmrSummaryTables.Start ();
			}
		}

		/// <summary>
		/// tmrDatabaseCleanup_Elapsed
		/// 
		/// Call the _database_cleanup stored proc
		protected void tmrDatabaseCleanup_Elapsed (object sender, System.Timers.ElapsedEventArgs e) 
		{
			try
			{
				tmrDatabaseCleanup.Stop ();
				m_logger.Info ("Database Cleanup START");
				Services.CallDatabaseCleanup ();
				m_logger.Info ("Database Cleanup END");
			}
			catch (Exception exc)
			{
				m_logger.Error ("Database Cleanup exception", exc);
			}
			finally
			{
				tmrDatabaseCleanup.Start ();
			}
		}

		/// <summary>
		/// tmrVideoImageGeneration_Elapsed
		/// 
		/// Call the Video Image Generation
		protected void tmrVideoImageGeneration_Elapsed (object sender, System.Timers.ElapsedEventArgs e) 
		{
			try
			{
				tmrVideoImageGeneration.Stop ();

				int AssetIdStart = 0;

                if (System.Configuration.ConfigurationManager.AppSettings["AssetIdStart"] != null)
				{
                    AssetIdStart = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["AssetIdStart"]);
				}

				m_logger.Info ("Video image generation - START");
				GenerateVideoImages (AssetIdStart);
				m_logger.Info ("Video image generation - END");
			}
			catch (Exception exc)
			{
				m_logger.Error ("Video image generation exception", exc);
			}
			finally
			{
				tmrVideoImageGeneration.Start ();
			}
		}

        /// <summary>
        /// UGCConversionTimer_Elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UGCConversionTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                tmrUGCConversion.Stop();
                m_logger.Info("UGC Conversion START");

                ConvertUGC();

                m_logger.Info("UGC Conversion END");
            }
            catch (Exception exc)
            {
                m_logger.Error("UGC Conversion exception", exc);
            }
            finally
            {
                tmrUGCConversion.Start();
            }
        }

        /// <summary>
        /// ConvertUGC
        /// </summary>
        protected void ConvertUGC()
        {
            DatabaseUtility dbUtility = Global.GetDatabaseUtility();
            int GIdStart = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["GIdStart"]);
            int convertedCacheDuration = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["convertedCacheDuration"]);
            string nxtExePath = System.Configuration.ConfigurationManager.AppSettings["nxtExePath"].ToString();

            string sqlSelect = "SELECT global_id, template_path_encrypted " +
                " FROM shopping.items_web iw " +
                " WHERE iw.converted = 0 " +
                " AND template_path_encrypted IS NOT NULL " + 
                " AND iw.global_id > " + GIdStart.ToString ();

            DataTable dtConvertFiles = dbUtility.GetDataTable(sqlSelect);

            int globalId = 0;
            string templatePathEncrypted = "";
            //string relativeFile = ""; //"\\4\\3006826\\wqbYyLT7eK.dat";

            for (int i = 0; i < dtConvertFiles.Rows.Count; i++)
            {
                globalId = Convert.ToInt32 (dtConvertFiles.Rows[i]["global_id"]);
                templatePathEncrypted = "\\" + dtConvertFiles.Rows [i]["template_path_encrypted"].ToString ().Replace ("/","\\");
                //templatePathEncrypted = "\\4\\3006826\\wqbYyLT7eK.dat";

                if (templatePathEncrypted.Length > 0 && System.IO.File.Exists (KanevaGlobals.TexturePath + templatePathEncrypted))
                {

                    try
                    {
                        string fileName = System.IO.Path.GetFileName(templatePathEncrypted);
                        string filePath = System.IO.Path.GetDirectoryName(templatePathEncrypted) + "\\";
                        string decryptedFileName = System.IO.Path.GetFileNameWithoutExtension(fileName) + "_DECPT.jpg";
                        string convertedFileName = System.IO.Path.GetFileNameWithoutExtension(decryptedFileName) + ".dds";
                        string newFileName = System.IO.Path.GetFileNameWithoutExtension(fileName) + "_2.dat";

                        // First decrypt the file
                        ImageHelper.DencryptTextureToFile(KanevaGlobals.TexturePath + templatePathEncrypted, KanevaGlobals.TexturePath + filePath + decryptedFileName);


                        //Create process
                        //System.Diagnostics.ProcessStartInfo processInfo =
                        //    new System.Diagnostics.ProcessStartInfo(@nxtExe,
                        //    "-quality_production -dxt1c -file M_Shirts_000_UVs.jpg");

                        System.Diagnostics.ProcessStartInfo processInfo =
                            new System.Diagnostics.ProcessStartInfo(nxtExePath + "nvdxt.exe",
                            "-quality_production -dxt1c -file " + KanevaGlobals.TexturePath + filePath + decryptedFileName + " -outdir " + KanevaGlobals.TexturePath + filePath);

                        //Some other useful options if you want to capture the output to process in your app, good for command line apps
                        processInfo.UseShellExecute = false;
                        processInfo.CreateNoWindow = false;
                        processInfo.WorkingDirectory = @nxtExePath;

                        System.Diagnostics.Process dxtProcess = System.Diagnostics.Process.Start(processInfo);
                        dxtProcess.WaitForExit();
                        int exitCode = dxtProcess.ExitCode;
                        dxtProcess.Close();

                        // Encrypt the file and rename to .dat
                        ImageHelper.EncryptTextureToFile(KanevaGlobals.TexturePath + filePath + convertedFileName, KanevaGlobals.TexturePath + filePath + newFileName);

                        // Delete temp decrypted file
                        System.IO.File.Delete(KanevaGlobals.TexturePath + filePath + decryptedFileName);
                        System.IO.File.Delete(KanevaGlobals.TexturePath + filePath + convertedFileName);

                        // Save this name to the database and mark it as converted
                        string sqlUpdate = "UPDATE shopping.items_web " +
                            " SET template_path_encrypted = '" + filePath + newFileName + "'," +
                            " converted = 1, " +
                            " cache_duration = " + convertedCacheDuration.ToString () +
                            " WHERE global_id = " + globalId.ToString ();

                        dbUtility.ExecuteNonQuery (sqlUpdate);
                    }
                    catch (Exception exc)
                    {
                        m_logger.Error("Error converting UGC " + globalId, exc);
                    }
                }
            }
        }

		/// <summary>
		/// NotificationsTimer_Elapsed
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void NotificationsTimer_Elapsed (object sender, System.Timers.ElapsedEventArgs e) 
		{
			try
			{
				tmrEmailNotification.Stop ();
				m_logger.Info ("Send Notifications START");
				Services.SendNotifications ();
				m_logger.Info ("Send Notifications END");
			}
			catch (Exception exc)
			{
				m_logger.Error ("Send Notifications exception", exc);
			}
			finally
			{
				tmrEmailNotification.Start ();
			}
		}

		/// <summary>
		/// FunctionalityCheckTimer_Elapsed
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void FunctionalityCheckTimer_Elapsed (object sender, System.Timers.ElapsedEventArgs e) 
		{
			try
			{
				tmrFunctionalityCheck.Stop ();

				m_logger.Info ("Functionality Check START");
				RunFunctionalityTest ();
				m_logger.Info ("Functionality Check END");
			}
			finally
			{
				tmrFunctionalityCheck.Start ();
			}
		}

        /// <summary>
		/// WebStatCheckTimer_Elapsed
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void WebStatCheckTimer_Elapsed (object sender, System.Timers.ElapsedEventArgs e) 
		{
			try
			{
				tmrWebStatCheck.Stop ();
				RunWebStatCheck ();
			}
			finally
			{
				tmrWebStatCheck.Start ();
			}
       }

          /// <summary>
       /// WebStatEmailTimer_Elapsed
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void WebStatEmailTimer_Elapsed (object sender, System.Timers.ElapsedEventArgs e) 
		{
            DatabaseUtility dbUtility = Global.GetDatabaseUtility();

			try
			{
                tmrWebStatEmail.Stop();

                // Get the datetime of the last time notifications where sent
                DataRow drConfig = GetConfiguration();

                if (drConfig == null)
                {
                    m_logger.Error("No configuration data found - Web Stat Email Functionality END");
                    return;
                }

                DateTime dtLastDailySent = Convert.ToDateTime(drConfig["last_daily_webstat_email_sent"]);
                DateTime dtNow = Convert.ToDateTime(drConfig["current_dateTime"]);

                // Is it time to run?
                if (dtNow.AddDays(-1).CompareTo(dtLastDailySent) > 0)
                {
                    m_logger.Info("Web Stat Email Functionality START");

                    string sqlSelect = "SELECT server_name, MAX(uptime), " +
                        " AVG(processor_time), MAX(processor_time), AVG(processor_priv_time), MAX(processor_priv_time), " +
                        " AVG(processor_interrupt_time), MAX(processor_interrupt_time), " +
                        " AVG(system_processor_queue_length), MAX(system_processor_queue_length), AVG(system_context_switches_sec), MAX(system_context_switches_sec), " +
                        " AVG(memory_avail_mbytes), MIN(memory_avail_mbytes), AVG(memory_page_reads_sec), MAX(memory_page_reads_sec), " +
                        " AVG(memory_pages_sec), MAX(memory_pages_sec), AVG(server_pool_nonpaged_bytes), MAX(server_pool_nonpaged_bytes), " +
                        " AVG(server_pool_nonpaged_failures), MAX(server_pool_nonpaged_failures), AVG(server_pool_paged_failures), MAX(server_pool_paged_failures), " +
                        " AVG(server_pool_nonpaged_peak), MAX(server_pool_nonpaged_peak), AVG(memory_cache_bytes), MAX(memory_cache_bytes), " +
                        " AVG(memory_cache_faults_sec), MAX(memory_cache_faults_sec), AVG(cache_MDL_read_hits_percent), MAX(cache_MDL_read_hits_percent), " +
                        " AVG(disk_queue_length), MAX(disk_queue_length), AVG(disk_avg_read_queue_length), MAX(disk_avg_read_queue_length), " +
                        " AVG(disk_avg_write_queue_length), MAX(disk_avg_write_queue_length), AVG(disk_avg_disk_sec_read), MAX(disk_avg_disk_sec_read), " +
                        " AVG(nic1_bytes_total_sec), MAX(nic1_bytes_total_sec), AVG(nic2_byts_total_sec), MAX(nic2_byts_total_sec), " +
                        " AVG(tcp_segments_sent_sec), MAX(tcp_segments_sent_sec), AVG(udp_datagrams_sent_sec), MAX(udp_datagrams_sent_sec), " +
                        " AVG(net_exceptions), MAX(net_exceptions), AVG(net_exceptions_sec), MAX(net_exceptions_sec), " +
                        " AVG(net_contention_rate_sec), MAX(net_contention_rate_sec), AVG(net_queue_length), MAX(net_queue_length), " +
                        " AVG(net_GC_percent), MAX(net_GC_percent), AVG(net_requests_sec), MAX(net_requests_sec), " +
                        " AVG(isapi_requests_sec), MAX(isapi_requests_sec), AVG(net_requests_timed_out), MAX(net_requests_timed_out), " +
                        " AVG(net_request_execution_time), MAX(net_request_execution_time), AVG(net_cache_total_entries), MAX(net_cache_total_entries), " +
                        " AVG(net_cache_hit_ratio), MIN(net_cache_hit_ratio), AVG(net_output_cache_entries), MAX(net_output_cache_entries), " +
                        " AVG(net_output_cache_hit_ratio), MIN(net_output_cache_hit_ratio), AVG(net_output_cache_turnover_rate), MAX(net_output_cache_turnover_rate) " +
                        " FROM web_health " +
                        " WHERE dt_taken > " + dbUtility.GetDatePlusDays(-1) +
                        " GROUP BY server_name " +
                        " ORDER BY server_name ";

                    DataTable dtWebStats = dbUtility.GetDataTable(sqlSelect);

                    // Build the email
                    int numberOfServers = dtWebStats.Rows.Count;

                    // Top Row
                    StringBuilder strEmail = new StringBuilder("<table cellspacing=\"1\" align=\"center\" border=\"1\">");

                    strEmail.Append(BuildRow("Processor", "Threshold", dtWebStats, "server_name", true, 0, ""));

                    strEmail.Append(BuildRow("Uptime", "7 days<br>14 days max", dtWebStats, "MAX(uptime)", false, 7, ""));
                    strEmail.Append(BuildRow("Avg Proc %", "85", dtWebStats, "AVG(processor_time)", false, 85, "#,##0.00\\%"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Max Proc %", "&nbsp;", dtWebStats, "MAX(processor_time)", false, 0, "#,##0.00\\%"));
                    strEmail.Append(BuildRow("Avg Proc Priv", "75", dtWebStats, "AVG(processor_priv_time)", false, 75, "#,##0.00\\%"));
                    strEmail.Append(BuildRow("Max Proc Priv", "&nbsp;", dtWebStats, "MAX(processor_priv_time)", false, 0, "#,##0.00\\%"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Avg Proc Interrupt", "&nbsp;", dtWebStats, "AVG(processor_interrupt_time)", false, 0, "#,##0.00\\%"));
                    strEmail.Append(BuildRow("Max Proc Interrupt", "&nbsp;", dtWebStats, "MAX(processor_interrupt_time)", false, 0, "#,##0.00\\%"));
                    strEmail.Append(BuildRow("Avg Proc Queue", "2", dtWebStats, "AVG(system_processor_queue_length)", false, 2, "#,##0"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Max Proc Queue", "&nbsp;", dtWebStats, "MAX(system_processor_queue_length)", false, 0, "#,##0"));
                    strEmail.Append(BuildRow("Avg Context Switch/Sec", "15,000", dtWebStats, "AVG(system_context_switches_sec)", false, 15000, "#,##0"));
                    strEmail.Append(BuildRow("Max Context Switch/Sec", "&nbsp;", dtWebStats, "MAX(system_context_switches_sec)", false, 0, "#,##0"));

                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Memory", "Threshold", dtWebStats, "server_name", true, 0, ""));
                    strEmail.Append(BuildRow("Avg Avail MB", "25% RAM (512MB)", dtWebStats, "AVG(memory_avail_mbytes)", false, 512, "#,##0.0", "red", false));
                    strEmail.Append(BuildRow("Min Avail MB", "512MB", dtWebStats, "MIN(memory_avail_mbytes)", false, 512, "#,##0", "yellow", false));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Avg Mem Page Reads/Sec", "5", dtWebStats, "AVG(memory_page_reads_sec)", false, 5, "#,##0.0"));
                    strEmail.Append(BuildRow("Max Mem Page Reads/Sec", "&nbsp;", dtWebStats, "MAX(memory_page_reads_sec)", false, 0, "#,##0"));
                    strEmail.Append(BuildRow("Avg Mem Pages/Sec", "5", dtWebStats, "AVG(memory_pages_sec)", false, 5, "#,##0"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Max Mem Pages/Sec", "&nbsp;", dtWebStats, "MAX(memory_pages_sec)", false, 0, "#,##0"));
                    strEmail.Append(BuildRow("Avg Pool Nonpaged Bytes", "&nbsp;", dtWebStats, "AVG(server_pool_nonpaged_bytes)", false, 0, "#,##0.0"));
                    strEmail.Append(BuildRow("Max Pool Nonpaged Bytes", "&nbsp;", dtWebStats, "MAX(server_pool_nonpaged_bytes)", false, 0, "#,##0.0"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Avg Pool Nonpaged Fails", "&nbsp;", dtWebStats, "AVG(server_pool_nonpaged_failures)", false, 0, "#,##0.0"));
                    strEmail.Append(BuildRow("Max Pool Nonpaged Fails", "&nbsp;", dtWebStats, "MAX(server_pool_nonpaged_failures)", false, 0, "#,##0.0"));
                    strEmail.Append(BuildRow("Avg Pool Paged Fails", "&nbsp;", dtWebStats, "AVG(server_pool_paged_failures)", false, 0, "#,##0.0"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Max Pool Paged Fails", "&nbsp;", dtWebStats, "MAX(server_pool_paged_failures)", false, 0, "#,##0.0"));
                    strEmail.Append(BuildRow("Avg Pool Nonpaged Peak", "&nbsp;", dtWebStats, "AVG(server_pool_nonpaged_peak)", false, 0, "#,##0.0"));
                    strEmail.Append(BuildRow("Max Pool Nonpaged Peak", "&nbsp;", dtWebStats, "MAX(server_pool_nonpaged_peak)", false, 0, "#,##0.0"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Avg Mem Cache Bytes", "&nbsp;", dtWebStats, "AVG(memory_cache_bytes)", false, 0, "#,##0.0"));
                    strEmail.Append(BuildRow("Max Mem Cache Bytes", "&nbsp;", dtWebStats, "MAX(memory_cache_bytes)", false, 0, "#,##0.0"));
                    strEmail.Append(BuildRow("Avg Mem Caches Faults/Sec", "&nbsp;", dtWebStats, "AVG(memory_cache_faults_sec)", false, 0, "#,##0.0"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Max Mem Caches Faults/Sec", "&nbsp;", dtWebStats, "MAX(memory_cache_faults_sec)", false, 0, "#,##0.0"));
                    strEmail.Append(BuildRow("Avg MDL Read Hits %", "&nbsp;", dtWebStats, "AVG(cache_MDL_read_hits_percent)", false, 0, "#,##0.0\\%"));
                    strEmail.Append(BuildRow("Max MDL Read Hits %", "&nbsp;", dtWebStats, "MAX(cache_MDL_read_hits_percent)", false, 0, "#,##0.0\\%"));

                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Disk I/O", "Threshold", dtWebStats, "server_name", true, 0, ""));
                    strEmail.Append(BuildRow("Avg Queue Length", "Spin +2", dtWebStats, "AVG(disk_queue_length)", false, 0, "#,##0.000"));
                    strEmail.Append(BuildRow("Max Queue Length", "&nbsp;", dtWebStats, "MAX(disk_queue_length)", false, 0, "#,##0.000"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Avg Read QL", "2", dtWebStats, "AVG(disk_avg_read_queue_length)", false, 2, "#,##0.000"));
                    strEmail.Append(BuildRow("Max Read QL", "&nbsp;", dtWebStats, "MAX(disk_avg_read_queue_length)", false, 0, "#,##0.000"));
                    strEmail.Append(BuildRow("Avg Write QL", "2", dtWebStats, "AVG(disk_avg_write_queue_length)", false, 2, "#,##0.000"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Max Write QL", "&nbsp;", dtWebStats, "MAX(disk_avg_write_queue_length)", false, 0, "#,##0.000"));
                    strEmail.Append(BuildRow("Avg Read/Sec", "&nbsp;", dtWebStats, "AVG(disk_avg_disk_sec_read)", false, 0, "#,##0.000"));
                    strEmail.Append(BuildRow("Max Read/Sec", "&nbsp;", dtWebStats, "MAX(disk_avg_disk_sec_read)", false, 0, "#,##0.000"));

                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Network I/O", "Threshold", dtWebStats, "server_name", true, 0, ""));
                    strEmail.Append(BuildRow("Avg NIC1 Avg Bytes/Sec", "80% NB", dtWebStats, "AVG(nic1_bytes_total_sec)", false, 0, "#,##0"));
                    strEmail.Append(BuildRow("Max NIC1 Max Bytes/Sec", "&nbsp;", dtWebStats, "MAX(nic1_bytes_total_sec)", false, 0, "#,##0"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Avg NIC2 Avg Bytes/Sec", "80% NB", dtWebStats, "AVG(nic2_byts_total_sec)", false, 0, "#,##0"));
                    strEmail.Append(BuildRow("Max NIC2 Max Bytes/Sec", "&nbsp;", dtWebStats, "MAX(nic2_byts_total_sec)", false, 0, "#,##0"));
                    strEmail.Append(BuildRow("Avg TCP Avg Seg sent/sec", "&nbsp;", dtWebStats, "AVG(tcp_segments_sent_sec)", false, 0, "#,##0"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Max TCP Avg Seg sent/sec", "&nbsp;", dtWebStats, "MAX(tcp_segments_sent_sec)", false, 0, "#,##0"));
                    strEmail.Append(BuildRow("Avg UDP dg sent/sec", "&nbsp;", dtWebStats, "AVG(udp_datagrams_sent_sec)", false, 0, "#,##0"));
                    strEmail.Append(BuildRow("Max UDP dg sent/sec", "&nbsp;", dtWebStats, "MAX(udp_datagrams_sent_sec)", false, 0, "#,##0"));

                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow(".NET", "Threshold", dtWebStats, "server_name", true, 0, ""));
                    strEmail.Append(BuildRow("Avg Requests/Sec", "&nbsp;", dtWebStats, "AVG(net_requests_sec)", false, 0, "#,##0.00"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Max Requests/Sec", "&nbsp;", dtWebStats, "MAX(net_requests_sec)", false, 0, "#,##0.00"));
                    strEmail.Append(BuildRow("Avg ISAPI Req/Sec", "&nbsp;", dtWebStats, "AVG(isapi_requests_sec)", false, 0, "#,##0.00"));
                    strEmail.Append(BuildRow("Max ISAPI Req/Sec", "&nbsp;", dtWebStats, "MAX(isapi_requests_sec)", false, 0, "#,##0.00"));
                    strEmail.Append("\r\n");
                    //strEmail.Append(BuildRow("Avg Exceptions", "&nbsp;", dtWebStats, "AVG(net_exceptions)", false, 0, "#,##0.0"));
                    strEmail.Append(BuildRow("Total Exceptions", "&nbsp;", dtWebStats, "MAX(net_exceptions)", false, 0, "#,##0"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Avg Exceptions/Sec", "5% RPS", dtWebStats, "AVG(net_exceptions_sec)", false, 0, "#,##0.00"));
                    strEmail.Append(BuildRow("Max Exceptions/Sec", "&nbsp;", dtWebStats, "MAX(net_exceptions_sec)", false, 0, "#,##0.00"));
                    strEmail.Append(BuildRow("Avg Contention Rate/sec", "&nbsp;", dtWebStats, "AVG(net_contention_rate_sec)", false, 0, "#,##0.0"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Max Contention Rate/sec", "&nbsp;", dtWebStats, "MAX(net_contention_rate_sec)", false, 0, "#,##0.0"));
                    strEmail.Append(BuildRow("Avg Queue Length", "&nbsp;", dtWebStats, "AVG(net_queue_length)", false, 0, "#,##0.0"));
                    strEmail.Append(BuildRow("Max Queue Length", "&nbsp;", dtWebStats, "MAX(net_queue_length)", false, 0, "#,##0.0"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Avg GC %", "5%", dtWebStats, "AVG(net_GC_percent)", false, 0, "#,##0.0\\%"));
                    strEmail.Append(BuildRow("Max GC %", "&nbsp;", dtWebStats, "MAX(net_GC_percent)", false, 0, "#,##0.0\\%"));
                  
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Avg Req Timeout", "&nbsp;", dtWebStats, "AVG(net_requests_timed_out)", false, 0, "#,##0.0"));
                    strEmail.Append(BuildRow("Max Req Timeout", "&nbsp;", dtWebStats, "MAX(net_requests_timed_out)", false, 0, "#,##0"));
                    strEmail.Append(BuildRow("Avg Execution Time", "1000", dtWebStats, "AVG(net_request_execution_time)", false, 1000, "#,##0.00 ms"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Max Execution Time", "5000", dtWebStats, "MAX(net_request_execution_time)", false, 5000, "#,##0 ms"));
                    strEmail.Append(BuildRow("Avg Cache Entries", "&nbsp;", dtWebStats, "AVG(net_cache_total_entries)", false, 0, "#,##0.00"));
                    strEmail.Append(BuildRow("Max Cache Entries", "&nbsp;", dtWebStats, "MAX(net_cache_total_entries)", false, 0, "#,##0"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Avg Cache Hit Ratio", "~80%", dtWebStats, "AVG(net_cache_hit_ratio)", false, 80, "#,##0.00\\%", "red", false));
                    strEmail.Append(BuildRow("Min Cache Hit Ratio", "&nbsp;", dtWebStats, "MIN(net_cache_hit_ratio)", false, 0, "#,##0.00\\%"));
                    strEmail.Append(BuildRow("Avg HTML Cache Entries", "&nbsp;", dtWebStats, "AVG(net_output_cache_entries)", false, 0, "#,##0.00"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Max HTML Cache Entries", "&nbsp;", dtWebStats, "MAX(net_output_cache_entries)", false, 0, "#,##0"));
                    strEmail.Append(BuildRow("Avg HTML Cache Hit Ratio", "~80%", dtWebStats, "AVG(net_output_cache_hit_ratio)", false, 80, "#,##0.00\\%", "red", false));
                    strEmail.Append(BuildRow("Min HTML Cache Hit Ratio", "&nbsp;", dtWebStats, "MIN(net_output_cache_hit_ratio)", false, 0, "#,##0.00\\%"));
                    strEmail.Append("\r\n");
                    strEmail.Append(BuildRow("Avg Cache Turnover Rate", "&nbsp;", dtWebStats, "AVG(net_output_cache_turnover_rate)", false, 0, "#,##0.00"));
                    strEmail.Append(BuildRow("Max Cache Turnover Rate", "&nbsp;", dtWebStats, "MAX(net_output_cache_turnover_rate)", false, 0, "#,##0.00"));

                    strEmail.Append("</table>");

                    MailUtility.SendEmail(System.Configuration.ConfigurationManager.AppSettings["FromEmail"], System.Configuration.ConfigurationManager.AppSettings["WebStatToEmail"], "Daily Web Site Status", strEmail.ToString(), true, false, 2);

                    // Update the last notifications sent date
                    string sqlUpdate = "UPDATE configuration SET " +
                        " last_daily_webstat_email_sent = " + dbUtility.GetCurrentDateFunction();
                    dbUtility.ExecuteNonQuery(sqlUpdate);

                    m_logger.Info("Web Stat Email Functionality END");
                }
			}
			finally
			{
                tmrWebStatEmail.Start();
			}
       }

       /// <summary>
       /// tmrBirthdayGiftTimer_Elapsed
       /// 
       /// Call the birthday gift stored proc
       protected void BirthdayGiftTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
       {
           DatabaseUtility dbUtility = Global.GetDatabaseUtility();

           try
           {
               m_logger.Info("Birthday Gift Timer START " + DateTime.Now.ToString());

               tmrBirthdayGift.Stop();
               string sqlUpdate;

               try
               {
                   // refresh the database connection
                   // seems it has a stale connection for some reason
                   sqlUpdate = "SELECT count(event_type_id) FROM kaneva.event_types";
                   dbUtility.ExecuteNonQuery(sqlUpdate);
               }
               catch (Exception exc)
               {
                   m_logger.Error("Birthday Gift select count failed", exc);
               }

               // Call the procedure
               sqlUpdate = "call kaneva.send_birthday_gifts()";
               dbUtility.ExecuteNonQuery(sqlUpdate);

               //calc the time till the next 1am
               DateTime d = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day ,BirthdayHour, 0, 0);
               if (DateTime.Now.Hour >= BirthdayHour && DateTime.Now.Millisecond > 0) //shift forward if we have passed todays run
                    d = d.AddDays(1.00f);
               TimeSpan ts = d.Subtract(DateTime.Now);

               tmrBirthdayGift.Interval = System.Convert.ToDouble(ts.TotalMilliseconds); 

               m_logger.Info("Birthday Gift Timer END");
           }
           catch (Exception exc)
           {
               m_logger.Error("Birthday Gift Timer exception", exc);
           }
           finally
           {
               tmrBirthdayGift.Start();
           }
       }

       /// <summary>
       /// tmrAnniversaryGiftTimer_Elapsed
       /// 
       /// Call the birthday gift stored proc
        protected void AnniversaryGiftTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
       {
           DatabaseUtility dbUtility = Global.GetDatabaseUtility();
           
           try
           {
               tmrAnniversaryGift.Stop();
               m_logger.Info("Anniversary Gift Timer START " + DateTime.Now.ToString() );
               
               // Call the procedure
               string sqlUpdate = "call kaneva.send_anniversary_gifts()";
               dbUtility.ExecuteNonQuery(sqlUpdate);

               //calc the time till the next 2am
               DateTime d = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, AnniversaryHour, 0, 0);
               if (DateTime.Now.Hour >= AnniversaryHour && DateTime.Now.Millisecond > 0) //shift forward if we have passed todays run
                   d = d.AddDays(1.00f);
               TimeSpan ts = d.Subtract(DateTime.Now);

               tmrAnniversaryGift.Interval = System.Convert.ToDouble(ts.TotalMilliseconds); 

               m_logger.Info("Anniversary Gift Timer END " + DateTime.Now.ToString());
           }
           catch (Exception exc)
           {
               m_logger.Error("Birthday Gift Timer exception", exc);
           }
           finally
           {
               tmrAnniversaryGift.Start();
           }
       }
       /// <summary>
       /// BuildRow
        /// </summary>
        private string BuildRow(string strStat, string strThreshold, DataTable dtWebStats, string strColumn, bool IsHeader, int MaxThreshold, string strFormat)
        {
            return BuildRow(strStat, strThreshold, dtWebStats, strColumn, IsHeader, MaxThreshold, strFormat, "red", true);
        }

        /// <summary>
       /// BuildRow
        /// </summary>
        private string BuildRow(string strStat, string strThreshold, DataTable dtWebStats, string strColumn, bool IsHeader, int MaxThreshold, string strFormat, string thresholdColor, bool bThreasHoldCompareGreaterThan)
        {
            StringBuilder strResult = new StringBuilder();
            
            if (IsHeader)
            {
                strResult.Append ("<tr style=\"background-color:#DDDDDD;font-weight:bold;\">");
            }
            else
            {
                strResult.Append ("<tr>");
            }
            
            strResult.Append ("<td><B>" + strStat + "</B></td><td>" + strThreshold + "</td>");

            for (int i = 0; i < dtWebStats.Rows.Count; i++)
            {
                if (strStat.ToLower ().Equals ("uptime"))
                {
                    int minutesUp = (int)(Convert.ToInt32(dtWebStats.Rows[i][strColumn]) / 60);
                    int hoursUp = minutesUp / 60;
                    int daysUp = hoursUp / 24;

                    // Handle Thresholds
                    if (daysUp >= MaxThreshold + 7)
                    {
                        strResult.Append("<td" + ((daysUp >= MaxThreshold) ? " style=\"background-color:" + thresholdColor + ";font-weight:bold;\">" : ">"));
                    }
                    else if (daysUp >= MaxThreshold)
                    {
                        strResult.Append("<td" + ((daysUp >= MaxThreshold) ? " style=\"background-color:yellow;font-weight:bold;\">" : ">"));
                    }
                    else
                    {
                        strResult.Append("<td>");
                    }

                    // Show the number
                    strResult.Append(daysUp.ToString() + " days " + (hoursUp % 24).ToString() + "h " + (minutesUp % 60).ToString() + "m" + "</td>");
                }
                else
                {
                    if (dtWebStats.Rows[i][strColumn].GetType().Equals(System.Type.GetType("System.Single")))
                    {
                        float fValue = (float)dtWebStats.Rows[i][strColumn];

                        // Handle Thresholds
                        if (bThreasHoldCompareGreaterThan)
                        {
                            strResult.Append("<td" + ((MaxThreshold > 0 && fValue > (float)MaxThreshold) ? " style=\"background-color:" + thresholdColor + ";font-weight:bold;\">" : ">"));
                        }
                        else
                        {
                            strResult.Append("<td" + ((MaxThreshold > 0 && fValue < (float)MaxThreshold) ? " style=\"background-color:" + thresholdColor + ";font-weight:bold;\">" : ">"));
                        }

                        // Show the number
                        strResult.Append(fValue.ToString(strFormat) + "</td>");
                    }
                    else if (dtWebStats.Rows[i][strColumn].GetType().Equals(System.Type.GetType("System.Double")))
                    {
                        Double dValue = (Double)dtWebStats.Rows[i][strColumn];

                        // Handle Thresholds
                        if (bThreasHoldCompareGreaterThan)
                        {
                            strResult.Append("<td" + ((MaxThreshold > 0 && dValue > (double)MaxThreshold) ? " style=\"background-color:" + thresholdColor + ";font-weight:bold;\">" : ">"));
                        }
                        else
                        {
                            strResult.Append("<td" + ((MaxThreshold > 0 && dValue < (double)MaxThreshold) ? " style=\"background-color:" + thresholdColor + ";font-weight:bold;\">" : ">"));
                        }

                        // Show the number
                        strResult.Append(dValue.ToString(strFormat) + "</td>");
                    }
                    else
                    {
                        strResult.Append("<td>" + dtWebStats.Rows[i][strColumn].ToString() + "</td>");
                    }
                }
            }

            strResult.Append("</tr>");
            return strResult.ToString ();
        }

		/// <summary>
		/// GetConfiguration
		/// </summary>
		/// <param name="messageId"></param>
		/// <returns></returns>
		private static DataRow GetConfiguration ()
		{
            string sqlSelect = "SELECT NOW() as current_dateTime, last_daily_webstat_email_sent " +
				" FROM configuration ";

			return Global.GetDatabaseUtility ().GetDataRow (sqlSelect, false);
		}


		private void RunFunctionalityTest ()
		{
            //bool bUserSuccess = false;
            //bool bUploadSuccess = false;

            //try
            //{
            //    m_logger.Info ("Functionality START");

            //    string strUserName = "kTest" + KanevaGlobals.GenerateUniqueString (10);
            //    string strPassword = KanevaGlobals.GenerateUniqueString (10);
					
            //    // ****************************
            //    // Insert a new user
            //    // ****************************
            //    byte[] salt = new byte[9];
            //    new RNGCryptoServiceProvider().GetBytes (salt);
            //    string hashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile (UsersUtility.MakeHash(strPassword+strUserName.ToLower()) + Convert.ToBase64String (salt), "MD5");	    

            //    // Generate a unique key_code for registration purposes
            //    string keyCode = KanevaGlobals.GenerateUniqueString (20);

            //    // Create a registration Key
            //    string regKey = KanevaGlobals.GenerateUniqueString (50);

            //    // Get the local IP address
            //    string strHostName = Dns.GetHostName ();
            //    string strIP = "127.0.0.1";
            //    //IPHostEntry ipEntry = Dns.GetHostByName (strHostName);
            //    IPHostEntry ipEntry = Dns.GetHostEntry (strHostName);
            //    IPAddress [] addr = ipEntry.AddressList;
            //    if (addr.Length > 0)
            //    {
            //        strIP = addr [0].ToString ();
            //    }

            //    int result = UsersUtility.addu(strUserName, hashPassword, Convert.ToBase64String(salt), 2, 1,
            //        "kaneva", "kaneva", strUserName, "M", "", strUserName + "@kaneva.com", new DateTime(1973, 7, 21), keyCode,
            //        "US", "30328", regKey, strIP);

            //    if (result.Equals(0))
            //    {
            //        if (!CreateDefaultUserHomePage(strUserName, UsersUtility.GetUserIdFromUsername(strUserName)))
            //        {
            //            result = 99;
            //        }
            //    }

            //    // Did it pass?
            //    if (result.Equals (0))
            //    {
            //        bUserSuccess = true;

            //        m_logger.Info ("New User Success - Name = " + strUserName);

            //        // Use the new user to test upload
            //        _userId = UsersUtility.GetUserIdFromUsername (strUserName);
            //        _username = strUserName;
            //    }
            //    else
            //    {
            //        m_logger.Info ("New User Failure");
            //        // Failure
            //        MailUtility.SendEmail (KanevaGlobals.FromEmail, "monitors@kaneva.com", "Error Inserting User", "result = " + result, false);
            //    }
					
            //    // ****************************
            //    // Upload a photo
            //    // ****************************
            //    bool bSuccess = true;
            //    string strTestPhoto = System.Configuration.ConfigurationManager.AppSettings["TestPhoto"].ToString();
            //    string errorMessage = AddFile (strTestPhoto, strIP, "Kaneva Test", "", "Kaneva Auto Test file upload", 0, (int) Constants.eASSET_PERMISSION.PRIVATE, true, ref bSuccess);					

            //    if (bSuccess)
            //    {
            //        bUploadSuccess = true;
            //        m_logger.Info ("Upload Success");
            //    }
            //    else
            //    {
            //        // Failure
            //        m_logger.Info ("Upload Failure");
            //        MailUtility.SendEmail (KanevaGlobals.FromEmail, "monitors@kaneva.com", "Error Inserting Photo", "result = " + errorMessage, false);
            //    }
					

            //    // ****************************
            //    // Upload a video
            //    // ****************************


            //    m_logger.Info ("Functionality END");
            //}
            //catch (Exception exc)
            //{
            //    m_logger.Error ("Functionality exception", exc);

            //    // Failure
            //    MailUtility.SendEmail (KanevaGlobals.FromEmail, "monitors@kaneva.com", "Error in Web Site Check", exc.ToString (), false);
            //}

            //// Create a text file for Nagios to read
            //try
            //{
            //    // Create the string
            //    string results = DateTime.Now.ToShortDateString () + " " + DateTime.Now.ToShortTimeString () + 
            //        ";User=" + ((bUserSuccess) ? "success" : "failure") + 
            //        ";Upload=" + ((bUploadSuccess) ? "success" : "failure");

            //    string NagiosOutputFile = System.Configuration.ConfigurationManager.AppSettings["NagiosOutputFile"].ToString();
				
            //    System.IO.StreamWriter fsOriginal = new System.IO.StreamWriter (NagiosOutputFile);
            //    fsOriginal.Write (results);
            //    fsOriginal.Close ();
            //}
            //catch (Exception exc)
            //{
            //    m_logger.Error ("Error in Web Site Check generating Nagios File", exc);
            //    MailUtility.SendEmail (KanevaGlobals.FromEmail, "monitors@kaneva.com", "Error in Web Site Check generating Nagios File", exc.ToString (), false);
            //}

            //// Clean up after
            //bool bCleanup = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["Cleanup"]);

            //if (bCleanup)
            //{
            //    if (bUserSuccess && (_userId != 4))
            //    {
            //        UsersUtility.UpdateUserStatus (_userId, (int) Constants.eUSER_STATUS.LOCKED);
            //    }
            //}

		}

        /// <summary>
        /// Creates the default home page for a newly registered user. Places a set of default
        /// widgets on the page.
        /// </summary>
        /// <returns>bool - true on success</returns>
        private bool CreateDefaultUserHomePage(string username, int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            if (userId.Equals(0))
            {
                return false;
            }

            try
            {
                string module_title = "";

                //create a pesonal channel
                //pass true as first param to override number of channels limit
                int communityId = CommunityUtility.InsertCommunity(true, 0, 0,
                    username, "NEW", userId, username, "Y", "N", "Y", "Y", true,
                    (int)Constants.eCOMMUNITY_STATUS.ACTIVE);

                // Give each user a default home page
                int page_id = PageUtility.AddLayoutPage(true, communityId, "Home", 0, 0, 1);

                if (page_id == 0)
                {
                    return false;
                }


                // Create default set of widgets for new user

                // -- WIDGETS IN HEADER -- //


                // -- WIDGETS IN LEFT COLUMN --//

                // CONTROL PANEL MODULE
                //module_title = GetModuleTitle( (int)Constants.eMODULE_TYPE.CONTROL_PANEL);

                int control_panel_id = WidgetUtility.InsertLayoutModuleControlPanel(username);

                if (control_panel_id == 0)
                {
                    return false;
                }

                PageUtility.AddLayoutPageModule(control_panel_id, page_id, (int)Constants.eMODULE_TYPE.CONTROL_PANEL, (int)Constants.eMODULE_ZONE.BODY, 1);


                // FRIENDS MODULE
                module_title = GetModuleTitle((int)Constants.eMODULE_TYPE.FRIENDS);

                int friend_module_id = WidgetUtility.InsertLayoutModuleFriends(module_title, Constants.DEFAULT_FRIEND_ENTRIES_PER_PAGE);

                if (friend_module_id == 0)
                {
                    return false;
                }

                PageUtility.AddLayoutPageModule(friend_module_id, page_id, (int)Constants.eMODULE_TYPE.FRIENDS, (int)Constants.eMODULE_ZONE.BODY, 2);


                // BLOG MODULE
                module_title = GetModuleTitle((int)Constants.eMODULE_TYPE.BLOGS);

                int blog_module_id = WidgetUtility.InsertLayoutModuleBlogs(module_title);

                if (blog_module_id == 0)
                {
                    return false;
                }

                PageUtility.AddLayoutPageModule(blog_module_id, page_id, (int)Constants.eMODULE_TYPE.BLOGS, (int)Constants.eMODULE_ZONE.BODY, 3);


                // SLIDE SHOW
                module_title = GetModuleTitle((int)Constants.eMODULE_TYPE.SLIDE_SHOW);

                int slide_show_module_id = WidgetUtility.InsertLayoutModuleSlideShow(module_title);

                if (slide_show_module_id == 0)
                {
                    return false;
                }

                PageUtility.AddLayoutPageModule(slide_show_module_id, page_id, (int)Constants.eMODULE_TYPE.SLIDE_SHOW, (int)Constants.eMODULE_ZONE.BODY, 4);


                // VIDEO Catalog
                module_title = GetModuleTitle((int)Constants.eMODULE_TYPE.VIDEO_PLAYER);

                int video_module_id = WidgetUtility.InsertLayoutModuleStores((int)Constants.eMODULE_TYPE.VIDEO_PLAYER, module_title);

                if (video_module_id == 0)
                {
                    return false;
                }

                PageUtility.AddLayoutPageModule(video_module_id, page_id, (int)Constants.eMODULE_TYPE.VIDEO_PLAYER, (int)Constants.eMODULE_ZONE.BODY, 5);


                // CHANNEL LIST
                module_title = GetModuleTitle((int)Constants.eMODULE_TYPE.CHANNELS);

                int channel_module_id = WidgetUtility.InsertLayoutModuleChannels(module_title);

                if (channel_module_id == 0)
                {
                    return false;
                }

                PageUtility.AddLayoutPageModule(channel_module_id, page_id, (int)Constants.eMODULE_TYPE.CHANNELS, (int)Constants.eMODULE_ZONE.BODY, 6);


                // -- WIDGETS IN RIGHT COLUMN -- //


                // VIDEO PLAYER
                module_title = GetModuleTitle((int)Constants.eMODULE_TYPE.OMM_VIDEO_PLAYER);

                int video_player_module_id = WidgetUtility.InsertLayoutModuleOmmMedia((int)Constants.eMODULE_TYPE.OMM_VIDEO_PLAYER, module_title);

                if (video_player_module_id == 0)
                {
                    return false;
                }

                PageUtility.AddLayoutPageModule(video_player_module_id, page_id, (int)Constants.eMODULE_TYPE.OMM_VIDEO_PLAYER, (int)Constants.eMODULE_ZONE.COLUMN, 1);


                // MULTIPLE PICTURES MODULE
                module_title = GetModuleTitle((int)Constants.eMODULE_TYPE.MULTIPLE_PICTURES);

                int multiple_pictures_module_id = WidgetUtility.InsertLayoutModuleMultiplePictures(module_title, Constants.DEFAULT_MULTIPLE_PICTURES_PICTURES_PER_PAGE, Constants.DEFAULT_MULTIPLE_PICTURES_SHOW_IMAGE_TITLE, -1, (int)Constants.ePICTURE_SIZE.MEDIUM);

                if (multiple_pictures_module_id == 0)
                {
                    return false;
                }

                PageUtility.AddLayoutPageModule(multiple_pictures_module_id, page_id, (int)Constants.eMODULE_TYPE.MULTIPLE_PICTURES, (int)Constants.eMODULE_ZONE.COLUMN, 2);


                // PROFILE (INTERESTS) MODULE
                module_title = GetModuleTitle((int)Constants.eMODULE_TYPE.PROFILE);

                int profile_module_id = WidgetUtility.InsertLayoutModuleProfile(module_title);

                if (profile_module_id == 0)
                {
                    return false;
                }

                PageUtility.AddLayoutPageModule(profile_module_id, page_id, (int)Constants.eMODULE_TYPE.PROFILE, (int)Constants.eMODULE_ZONE.COLUMN, 3);


                // COMMENTS MODULE
                module_title = GetModuleTitle((int)Constants.eMODULE_TYPE.COMMENTS);

                int guestbook_module_id = WidgetUtility.InsertLayoutModuleComments(module_title, Constants.DEFAULT_COMMENT_MAX_PER_PAGE, Constants.DEFAULT_COMMENT_SHOW_DATE_TIME, Constants.DEFAULT_COMMENT_SHOW_PROFILE_PIC, Constants.DEFAULT_COMMENT_AUTHOR_ALIGNMENT, 1, 1);

                if (guestbook_module_id == 0)
                {
                    return false;
                }

                PageUtility.AddLayoutPageModule(guestbook_module_id, page_id, (int)Constants.eMODULE_TYPE.COMMENTS, (int)Constants.eMODULE_ZONE.COLUMN, 4);

                // Give each user two frieds groups. Family and Coworkers
                UsersUtility.InsertFriendGroup(userId, "Family");
                UsersUtility.InsertFriendGroup(userId, "Coworkers");
                UsersUtility.InsertFriendGroup(userId, "Inner Circle");

                return true;
            }
            catch (Exception e)
            {
                m_logger.Error(e.ToString());
            }

            return false;
        }

        private string GetModuleTitle(int iAny)
        {
            return "Func Test";
        }

		/// <summary>
		/// AddFile
		/// </summary>
		private string AddFile (string filepath, string fromIPaddress, string title, string tags, string description, 
			int categoryId, int permission, bool isRestricted, ref bool bAllSuccess)
		{
			// Get the file
			//System.Drawing.Image imgOriginal = System.Drawing.Image.FromFile (filepath);
			System.IO.FileStream fsOriginal = new System.IO.FileStream (filepath, System.IO.FileMode.Open, System.IO.FileAccess.Read);


			string filename = "KanevaTestImage.jpg";
			int assetTypeId = (int) Constants.eASSET_TYPE.PICTURE;

			// Get the upload repository
			string uploadRepository = "";
			try 
			{
				uploadRepository = Configuration.UploadRepository;
			}
			catch (Exception e)
			{
				bAllSuccess = false;
				m_logger.Error ("Error reading config file", e);
				return "UploadRepository is not defined in web.config";
			}

			// Add the file
			try
			{
				int assetId = 0;		
				string newPath = CreateNewRecord (fromIPaddress, ref assetId, _userId, title, filename, assetTypeId, tags, fsOriginal.Length, 
					uploadRepository, categoryId, description, permission, isRestricted);

				// Make sure the directory exists
				FileInfo fileInfo = new FileInfo (newPath + Path.DirectorySeparatorChar);
				if (!fileInfo.Directory.Exists)
				{
					fileInfo.Directory.Create ();
				}

				//imgOriginal.Save (newPath + Path.DirectorySeparatorChar + filename);
				

				int maximumBufferSize = 4096;
				byte [] transferBuffer;

				if (fsOriginal.Length > maximumBufferSize)
				{
					transferBuffer = new byte [maximumBufferSize];
				}
				else
				{
					transferBuffer = new byte [fsOriginal.Length];
				}

				System.IO.FileStream fsNewFile = new System.IO.FileStream (newPath + Path.DirectorySeparatorChar + filename, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);

				int bytesRead;
				do 
				{
					bytesRead = fsOriginal.Read (transferBuffer, 0, transferBuffer.Length);
					fsNewFile.Write (transferBuffer, 0, bytesRead);
				}
				while (bytesRead > 0);

				fsNewFile.Close ();

				// For pictures, generate the thumbs
				if (assetTypeId.Equals ((int) Constants.eASSET_TYPE.PICTURE))
				{
					CreatePhotoThumbs (assetId, _userId, CommunityUtility.GetPersonalChannelId (_userId), newPath + Path.DirectorySeparatorChar + filename, filename);
				}
			}
			catch (UnauthorizedAccessException exc)
			{
				bAllSuccess = false;
				m_logger.Error ("Possible error with LibTorrentProxy.exe DCOM settings, allow user to launch", exc);
				return ("There was a fatal error posting the File \\'" + filename + "\\'. Please try again later.\\n");
			}
			catch (Exception exc)
			{
				bAllSuccess = false;
				m_logger.Error ("Error reading file", exc);
				return ("There was an error posting the File \\'" + filename + "\\'. Please try again.\\n");
			}
			finally 
			{
				fsOriginal.Close ();
			}

			return "";
		}

		/// <summary>
		/// Create new asset_upload and asset record
		/// </summary>
		/// <param name="user_id"></param>
		private string CreateNewRecord (string fromIPaddress, ref int assetId, int user_id, string itemName, string fileName, int assetTypeId, 
			string tags, long size, string uploadRepository, int categoryId, string description, int permission, bool isRestricted)
		{
			//create a new asset
			string newItemName = itemName;
			string newPath = "";

			/* Code taken from assetEdit page  */			 
			int isMature = (int) Constants.eASSET_RATING.GENERAL;
			if (isRestricted)
			{
				isMature = (int) Constants.eASSET_RATING.MATURE;
			}

			// Categories
			int category1Id = categoryId;

			// Create a new asset record
			assetId = StoreUtility.InsertAsset (assetTypeId, newItemName, user_id, _username, 
				(int) Constants.ePUBLISH_STATUS.UPLOADED, permission, isMature, category1Id, description);
			
			if (assetId > 0)
			{
				// Non pictures must be processed, pictures don't
				if (assetTypeId.Equals ((int) Constants.eASSET_TYPE.PICTURE))
				{
					newPath = Path.Combine (Path.Combine (KanevaGlobals.ContentServerPath, user_id.ToString ()), assetId.ToString ());
					StoreUtility.UpdateAssetFilePath (assetId, fileName, newPath, size);
					StoreUtility.UpdateAssetStatus (assetId, (int) Constants.eASSET_STATUS.ACTIVE);
					StoreUtility.UpdateAssetPublishStatus (assetId, (int) Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE, 0);
				}
				else
				{
					// Add non-pictures to the asset  upload table to be processed
					int assetUploadId = StoreUtility.InsertAssetUpload(user_id, fileName, uploadRepository, size, "", (int) Constants.eDS_INVENTORYTYPE.ASSETS, assetId);
					newPath = Path.Combine (Path.Combine (uploadRepository, user_id.ToString()), assetId.ToString());
					StoreUtility.UpdateAssetUploadPath (assetUploadId, newPath);
					StoreUtility.UpdateAssetUploadStatus (assetId, (int) Constants.ePUBLISH_STATUS.UPLOADED, fromIPaddress);
				}

				// Insert into user's personal channel
				int personalChannelId =  CommunityUtility.GetPersonalChannelId(_userId);
				StoreUtility.InsertAssetChannel (assetId,personalChannelId);
			}
			else
			{
				m_logger.Error ("Failed to insert asset record user " + user_id + " filename = " + fileName);
			}

			return newPath;
		}

		/// <summary>
		/// CreatePhotoThumbs
		/// </summary>
		private void CreatePhotoThumbs (int assetId, int userId, int communityId, string imagePath, string filename)
		{
			System.Drawing.Image imgOriginal = null;

			string contentRepository = "";
			if (KanevaGlobals.ContentServerPath != null)
			{
				contentRepository = KanevaGlobals.ContentServerPath;
			}
			else
			{
				m_logger.Error ("ContentServerPath not found generating Photo Thumbs");
				return;
			}

			string fileStoreHack = "";
			if (KanevaGlobals.FileStoreHack != "")
			{
				fileStoreHack = KanevaGlobals.FileStoreHack;
			}

			try
			{
				//Create an image object from a file on disk
				imgOriginal = System.Drawing.Image.FromFile (imagePath);
				
				// Save the image at the specified sizes
				// Save the original path
				StoreUtility.UpdateAssetThumb (assetId, fileStoreHack + userId.ToString () + "/" + assetId.ToString() + "/" + filename, "image_full_path");
								
				// Small
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_SMALL_HEIGHT, (int) Constants.PHOTO_THUMB_SMALL_WIDTH, "_sm", "thumbnail_small_path",
					filename, imagePath, contentRepository, userId, assetId);

				// Medium
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_MEDIUM_HEIGHT, (int) Constants.PHOTO_THUMB_MEDIUM_WIDTH, "_me", "thumbnail_medium_path",
					filename, imagePath, contentRepository, userId, assetId);

				// Large
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_LARGE_HEIGHT, (int) Constants.PHOTO_THUMB_LARGE_WIDTH, "_la", "thumbnail_large_path",
					filename, imagePath, contentRepository, userId, assetId);

				// XLarge
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_XLARGE_HEIGHT, (int) Constants.PHOTO_THUMB_XLARGE_WIDTH, "_xl", "thumbnail_xlarge_path",
					filename, imagePath, contentRepository, userId, assetId);

				// Asset Details
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_ASSETDETAILS_HEIGHT, (int) Constants.PHOTO_THUMB_ASSETDETAILS_WIDTH, "_ad", "thumbnail_assetdetails_path",
					filename, imagePath, contentRepository, userId, assetId);
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error reading image path is " + imagePath, exc);
				
			}
			finally 
			{
				if (imgOriginal != null)
				{
					imgOriginal.Dispose();
				}
			}
		}

		/// <summary>
		/// CreateTimers Method
		/// 
		/// The timer fires every 15 minutes (60000 * 15).
		/// </summary>
		private void CreateTimers () 
		{
            if (UGCConversionEnabled)
            {
                int conversionInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["UGCConversionInterval"]);

                m_logger.Info("Starting UGC Conversion every " + conversionInterval + " minutes");
                tmrUGCConversion.Interval = (60000 * conversionInterval);	// Minutes to run
                tmrUGCConversion.Elapsed += new System.Timers.ElapsedEventHandler(UGCConversionTimer_Elapsed);
                tmrUGCConversion.Enabled = true;
            }

			// ****************************************
			// Email Notifications
			// ****************************************
			if (EmailNotificationsEnabled) 
			{
                int notificationMinutes = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NotificationInterval"]);

				// Start it up
				m_logger.Info ("Starting Email Notifications every " + notificationMinutes + " minutes");
				tmrEmailNotification.Interval = (60000 * notificationMinutes);	// Minutes to run
				tmrEmailNotification.Elapsed += new System.Timers.ElapsedEventHandler (NotificationsTimer_Elapsed);
				tmrEmailNotification.Enabled = true;
			}

			// ****************************************
			// Summary Tables Updater
			// ****************************************			
			int sp_mintues_update_report_stats = 15;

			if (SummaryTablesUpdateEnabled) 
			{
                if (System.Configuration.ConfigurationManager.AppSettings["sp_mintues_update_report_stats"] != null)
				{
                    sp_mintues_update_report_stats = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["sp_mintues_update_report_stats"]);
				}

				// Run it once at startup
				try
				{
					m_logger.Info ("Summary Tables Updater - update_report_stats - START");
					Services.CallUpdateReportStats ();
					m_logger.Info ("Summary Tables Updater - update_report_stats - END");
				}
				catch (Exception exc)
				{
					m_logger.Error ("Summary Tables Updater exception", exc);
				}
				
				// Start it up
				m_logger.Info ("Starting Summary Tables Updater - update_report_stats every " + sp_mintues_update_report_stats + " minutes");
				tmrSummaryTables.Interval = (60000 * sp_mintues_update_report_stats);	// Minutes to run
				tmrSummaryTables.Elapsed += new System.Timers.ElapsedEventHandler (tmrSummaryTables_Elapsed);
				tmrSummaryTables.Enabled = true;
			}

			// ****************************************
			// Database Cleanup
			// ****************************************
			int sp_mintues_database_cleanup = 10;

			if (DatabaseCleanupEnabled)
			{
                if (System.Configuration.ConfigurationManager.AppSettings["sp_mintues_database_cleanup"] != null)
				{
                    sp_mintues_database_cleanup = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["sp_mintues_database_cleanup"]);
				}	

				// Run it once at startup
				try
				{
					m_logger.Info ("Database Cleanup - START");
					Services.CallDatabaseCleanup ();
					m_logger.Info ("Database Cleanup - END");
				}
				catch (Exception exc)
				{
					m_logger.Error ("Database Cleanup exception", exc);
				}

				// Start it up
				m_logger.Info ("Starting Database Cleanup to run on every " + sp_mintues_database_cleanup + " minutes");
				tmrDatabaseCleanup.Interval = (60000 * sp_mintues_database_cleanup);	// Minutes to run
				tmrDatabaseCleanup.Elapsed += new System.Timers.ElapsedEventHandler (tmrDatabaseCleanup_Elapsed);
				tmrDatabaseCleanup.Enabled = true;
			}


			// ****************************************
			// Video Image Generation
			// ****************************************
			int VideoImageGenerationInterval = 20;
			int AssetIdStart = 0;

			// Do we want to run it on this machine?
			if (VideoImageGenerationEnabled) 
			{
                if (System.Configuration.ConfigurationManager.AppSettings["VideoImageGenerationInterval"] != null)
				{
                    VideoImageGenerationInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["VideoImageGenerationInterval"]);
				}

                if (System.Configuration.ConfigurationManager.AppSettings["AssetIdStart"] != null)
				{
                    AssetIdStart = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["AssetIdStart"]);
				}

				// Run it once at startup
				try
				{
					m_logger.Info ("Video image generation - START");
					GenerateVideoImages (AssetIdStart);
					m_logger.Info ("Video image generation - END");
				}
				catch (Exception exc)
				{
					m_logger.Error ("Video image generation exception", exc);
				}
				
				// Start up video image generation
				m_logger.Info ("Video ImageGeneration to run every " + VideoImageGenerationInterval + " minutes");
				tmrVideoImageGeneration.Interval = (60000 * VideoImageGenerationInterval);	// Minutes to run
				tmrVideoImageGeneration.Elapsed += new System.Timers.ElapsedEventHandler (tmrVideoImageGeneration_Elapsed);
				tmrVideoImageGeneration.Enabled = true;
			}

			if (FunctionalityCheckEnabled)
			{
				int FunctionalityCheckInterval = 60;

                if (System.Configuration.ConfigurationManager.AppSettings["FunctionalityCheckInterval"] != null)
				{
                    FunctionalityCheckInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["FunctionalityCheckInterval"]);
				}

				m_logger.Info ("Functionality Check START");
				RunFunctionalityTest ();
				m_logger.Info ("Functionality Check END");

				// Start up video image generation
				m_logger.Info ("Functionality Check to run every " + FunctionalityCheckInterval + " minutes");
				tmrFunctionalityCheck.Interval = (60000 * FunctionalityCheckInterval);	// Minutes to run
				tmrFunctionalityCheck.Elapsed += new System.Timers.ElapsedEventHandler (FunctionalityCheckTimer_Elapsed);
				tmrFunctionalityCheck.Enabled = true;
			}

            if (WebStatCheckEnabled)
            {
                int WebStatCheckInterval = 60;

                if (System.Configuration.ConfigurationManager.AppSettings["WebStatCheckInterval"] != null)
                {
                    WebStatCheckInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["WebStatCheckInterval"]);
                }

                // Run on start up
                //RunWebStatCheck ();

                // Start up video image generation
                m_logger.Info("WebStat Check to run every " + WebStatCheckInterval + " minutes");
                tmrWebStatCheck.Interval = (60000 * WebStatCheckInterval);	// Minutes to run
                tmrWebStatCheck.Elapsed += new System.Timers.ElapsedEventHandler(WebStatCheckTimer_Elapsed);
                tmrWebStatCheck.Enabled = true;
            }

            if (WebStatEmailEnabled)
            {
                int WebStatEmailInterval = 10;

                // Start up video image generation
                m_logger.Info("Webstat Email to run every " + WebStatEmailInterval + " minutes");
                tmrWebStatEmail.Interval = (60000 * WebStatEmailInterval);	// Minutes to run
                tmrWebStatEmail.Elapsed += new System.Timers.ElapsedEventHandler(WebStatEmailTimer_Elapsed);
                tmrWebStatEmail.Enabled = true;
            }

            if (BirthdayGiftEnabled)
            {
                //calc the time till the next run
                DateTime d = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, BirthdayHour, 0, 0);
                if (DateTime.Now.Hour >= BirthdayHour && DateTime.Now.Millisecond > 0) //shift forward if we have passed todays run
                    d = d.AddDays(1.00f);
                TimeSpan ts = d.Subtract(DateTime.Now);

                tmrBirthdayGift.Interval = System.Convert.ToDouble(ts.TotalMilliseconds);

                // Start up video image generation
                m_logger.Info("BirthdayGift process to run in " + tmrBirthdayGift.Interval + " miliseconds from " + DateTime.Now.ToString());
                tmrBirthdayGift.Elapsed += new System.Timers.ElapsedEventHandler(BirthdayGiftTimer_Elapsed);
                tmrBirthdayGift.Enabled  = true;
            }
            
            if (AnniversaryGiftEnabled)
            {
                //calc the time till the next run
                DateTime d = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, AnniversaryHour, 0, 0);
                if (DateTime.Now.Hour >= AnniversaryHour && DateTime.Now.Millisecond > 0) //shift forward if we have passed todays run
                    d = d.AddDays(1.00f);
                TimeSpan ts = d.Subtract(DateTime.Now);

                tmrAnniversaryGift.Interval = System.Convert.ToDouble(ts.TotalMilliseconds);

                m_logger.Info("Anniversary Gift process to run in " + tmrAnniversaryGift.Interval + " miliseconds from " + DateTime.Now.ToString());
                tmrAnniversaryGift.Elapsed += new System.Timers.ElapsedEventHandler(AnniversaryGiftTimer_Elapsed);
                tmrAnniversaryGift.Enabled = true;
            }
	}

		/// <summary>
		/// GenerateVideoImages
		/// </summary>
		private void GenerateVideoImages (int assetIdStart)
		{
			DataTable dt = StoreUtility.GetVideoGenThumbs (assetIdStart, Int32.MaxValue, (int) Constants.eASSET_TYPE.VIDEO);

			if (ImageHelper.GenerateMediaThumbs (dt))
			{
				//m_Service_logger.Info ("Video Thumbs Generation SUCCEEDED");
			}
			else
			{
				m_logger.Error ("Video Thumbs Generation Failed");
			}
		}

        /// <summary>
        /// RunWebStatCheck
        /// </summary>
        private void RunWebStatCheck()
        {
            string lastServer = "Unknown";
            m_logger.Info("Web Stat Check START");

            Hashtable htWebServers = WebServers;

            if (htWebServers == null || htWebServers.Count == 0)
			{
				m_logger.Warn ("No web servers defined in config");
			}
			else
			{
                System.Collections.IDictionaryEnumerator myEnumerator = htWebServers.GetEnumerator();

				while ( myEnumerator.MoveNext() )
				{
					try
					{
                        WebServer webServer = (WebServer)myEnumerator.Value;
                        lastServer = webServer.servername;
                        RunWebStatCheck(webServer.servername, webServer.nic1, webServer.nic2);
					}
					catch (Exception exc)
					{
                        m_logger.Error("Error running webstat check " + lastServer, exc);
					}
				}
			}

            m_logger.Info("Web Stat Check End");
        }

        /// <summary>
        /// RunWebStatCheck
        /// </summary>
        private void RunWebStatCheck (string servername, string nic1, string nic2)
        {
            // Web Server Statistics

            // *****************************************************
            // Set up all the perf Counters and get initial value
            // *****************************************************

            // *****************************************************
            // Processor
            // *****************************************************

            m_logger.Debug("Starting to read system performance data for " + servername);

            // ASP.NET Version
            string strASPpNETVersion = ASPNETVersion;

            PerformanceCounter perfSysUp = new PerformanceCounter("System", "System Up Time", "", servername);
            PerformanceCounter perfProcessorTime = new PerformanceCounter("Processor", "% Processor Time", "_Total", servername);
            PerformanceCounter perfProcessorPrivTime = new PerformanceCounter("Processor", "% Privileged Time", "_Total", servername);
            PerformanceCounter perfProcessorInterruptTime = new PerformanceCounter("Processor", "% Interrupt Time", "_Total", servername);
            PerformanceCounter perfProcessorQueue = new PerformanceCounter("System", "Processor Queue Length", "", servername);
            PerformanceCounter perfProcessorContextSwitch = new PerformanceCounter("System", "Context Switches/sec", "", servername);


            try
            {
                perfSysUp.NextValue();
                perfProcessorTime.NextValue();
                perfProcessorPrivTime.NextValue();
                perfProcessorInterruptTime.NextValue();
                perfProcessorQueue.NextValue();
                perfProcessorContextSwitch.NextValue();
            }
            catch(Exception exc)
            {
                m_logger.Error("Error with General " + servername, exc);
            }

            // *****************************************************
            // Memory
            // *****************************************************
            PerformanceCounter perfMem = new PerformanceCounter("Memory", "Available MBytes", "", servername);
            PerformanceCounter perfMemPR = new PerformanceCounter("Memory", "Page Reads/sec", "", servername);
            PerformanceCounter perfMemPS = new PerformanceCounter("Memory", "Pages/sec", "", servername);
            PerformanceCounter perfMemPNB = new PerformanceCounter("Server", "Pool Nonpaged Bytes", "", servername);
            PerformanceCounter perfMemPNF = new PerformanceCounter("Server", "Pool Nonpaged Failures", "", servername);
            PerformanceCounter perfMemPPF = new PerformanceCounter("Server", "Pool Paged Failures", "", servername);
            PerformanceCounter perfMemPNP = new PerformanceCounter("Server", "Pool Nonpaged Peak", "", servername);
            PerformanceCounter perfMemCB = new PerformanceCounter("Memory", "Cache Bytes", "", servername);
            PerformanceCounter perfMemCFS = new PerformanceCounter("Memory", "Cache Faults/sec", "", servername);
            PerformanceCounter perfMemMDL = new PerformanceCounter("Cache", "MDL Read Hits %", "", servername);

            try
            {
                perfMem.NextValue();
                perfMemPR.NextValue();
                perfMemPS.NextValue();
                perfMemPNB.NextValue();
                perfMemPNF.NextValue();
                perfMemPPF.NextValue();
                perfMemPNP.NextValue();
                perfMemCB.NextValue();
                perfMemCFS.NextValue();
                perfMemMDL.NextValue();
            }
            catch(Exception exc)
            {
                m_logger.Error("Error with Memory " + servername, exc);
            }

            // *****************************************************
            // Disk I/O
            // *****************************************************
            PerformanceCounter perfDisk = new PerformanceCounter("PhysicalDisk", "Avg. Disk Queue Length", "_Total", servername);
            PerformanceCounter perfDiskQL = new PerformanceCounter("PhysicalDisk", "Avg. Disk Read Queue Length", "_Total", servername);
            PerformanceCounter perfDiskWQL = new PerformanceCounter("PhysicalDisk", "Avg. Disk Write Queue Length", "_Total", servername);
            PerformanceCounter perfDiskDSR = new PerformanceCounter("PhysicalDisk", "Avg. Disk sec/Read", "_Total", servername);

            try
            {
                perfDisk.NextValue();
                perfDiskQL.NextValue();
                perfDiskWQL.NextValue();
                perfDiskDSR.NextValue();
            }
            catch(Exception exc)
            {
                m_logger.Error("Error with Disk I/O" + servername, exc);
            }

            // *****************************************************
            // Network
            // *****************************************************
            PerformanceCounter perfNetBTS1 = new PerformanceCounter();
            PerformanceCounter perfNetBTS2 = new PerformanceCounter();
            PerformanceCounter perfNetBRS1 = new PerformanceCounter();
            PerformanceCounter perfNetBRS2 = new PerformanceCounter();
            PerformanceCounter perfNetBSS1 = new PerformanceCounter();
            PerformanceCounter perfNetBSS2 = new PerformanceCounter();
            PerformanceCounter perfNetIBTS1 = new PerformanceCounter();
            PerformanceCounter perfNetIBTS2 = new PerformanceCounter();
            PerformanceCounter perfNetTCP = new PerformanceCounter();
            PerformanceCounter perfNetUDP = new PerformanceCounter();

            PerformanceCounterCategory pccNicExists = new PerformanceCounterCategory("Network Interface", servername);

            if (pccNicExists.InstanceExists(nic1))
            {
                perfNetBTS1 = new PerformanceCounter("Network Interface", "Bytes Total/sec", nic1, servername);
                perfNetBTS1.NextValue();

                perfNetBRS1 = new PerformanceCounter("Network Interface", "Bytes Received/sec", nic1, servername);
                perfNetBRS1.NextValue();

                perfNetBSS1 = new PerformanceCounter("Network Interface", "Bytes Sent/sec", nic1, servername);
                perfNetBSS1.NextValue();

                perfNetIBTS1 = new PerformanceCounter("Network Interface", "Bytes Total/sec", nic1, servername);
                perfNetIBTS1.NextValue();
            }

            if (pccNicExists.InstanceExists(nic2))
            {
                perfNetBTS2 = new PerformanceCounter("Network Interface", "Bytes Total/sec", nic2, servername);
                perfNetBTS2.NextValue();

                perfNetBRS2 = new PerformanceCounter("Network Interface", "Bytes Received/sec", nic2, servername);
                perfNetBRS2.NextValue();

                perfNetBSS2 = new PerformanceCounter("Network Interface", "Bytes Sent/sec", nic2, servername);
                perfNetBSS2.NextValue();

                perfNetIBTS2 = new PerformanceCounter("Network Interface", "Bytes Total/sec", nic2, servername);
                perfNetIBTS2.NextValue();
            }

            PerformanceCounterCategory pccTCCExists = new PerformanceCounterCategory("TCP", servername);

            if (PerformanceCounterCategory.Exists("TCP", servername))
            {
                perfNetTCP = new PerformanceCounter("TCP", "Segments Sent/sec", "", servername);
                perfNetTCP.NextValue();
            }

            PerformanceCounterCategory pccUPDExists = new PerformanceCounterCategory("UDP", servername);
            if (PerformanceCounterCategory.Exists("UDP", servername))
            {
                perfNetUDP = new PerformanceCounter("UDP", "Datagrams Sent/sec", "", servername);
                perfNetUDP.NextValue();
            }

            PerformanceCounter perfNetIT = new PerformanceCounter("Processor", "% Interrupt Time", "_Total", servername);
            perfNetIT.NextValue();

            // *****************************************************
            // ASP
            // *****************************************************
            PerformanceCounter perfCLRExcThrown = new PerformanceCounter(".NET CLR Exceptions", "# of Exceps Thrown", "_Global_", servername);
            perfCLRExcThrown.NextValue();

            PerformanceCounter perfCLRExcThrownSec = new PerformanceCounter(".NET CLR Exceptions", "# of Exceps Thrown / sec", "_Global_", servername);
            perfCLRExcThrownSec.NextValue();

            PerformanceCounter perfCLRExcFinally = new PerformanceCounter(".NET CLR Exceptions", "# of Finallys / sec", "_Global_", servername);
            perfCLRExcFinally.NextValue();

            PerformanceCounter perfCLRExcDepth = new PerformanceCounter(".NET CLR Exceptions", "Throw to Catch Depth / sec", "_Global_", servername);
            perfCLRExcDepth.NextValue();

            PerformanceCounter perfLocksAndThreads = new PerformanceCounter(".NET CLR LocksAndThreads", "Contention Rate / sec", "_Global_", servername);
            perfLocksAndThreads.NextValue();

            PerformanceCounter perfQL = new PerformanceCounter(".NET CLR LocksAndThreads", "Current Queue Length", "_Global_", servername);
            perfQL.NextValue();

            PerformanceCounter perfCLRMEM = new PerformanceCounter(".NET CLR Memory", "% Time in GC", "_Global_", servername);
            perfCLRMEM.NextValue();

            PerformanceCounter perfASPRS = new PerformanceCounter(strASPpNETVersion, "Requests/Sec", "__Total__", servername);
            perfASPRS.NextValue();

            PerformanceCounter perfASPERS = new PerformanceCounter("Web Service", "ISAPI Extension Requests/sec", "_Total", servername);
            perfASPERS.NextValue();

            PerformanceCounter perfASPRTO = new PerformanceCounter(strASPpNETVersion, "Requests Timed Out", "__Total__", servername);
            perfASPRTO.NextValue();

            PerformanceCounter perfASPRET = new PerformanceCounter("ASP.NET v2.0.50727", "Request Execution Time", "", servername);
            perfASPRET.NextValue(); 

            PerformanceCounter perfASPCTE = new PerformanceCounter(strASPpNETVersion, "Cache Total Entries", "__Total__", servername);
            perfASPCTE.NextValue();

            PerformanceCounter perfASPCTHR = new PerformanceCounter(strASPpNETVersion, "Cache Total Hit Ratio", "__Total__", servername);
            perfASPCTHR.NextValue();

            PerformanceCounter perfASPOCE = new PerformanceCounter(strASPpNETVersion, "Output Cache Entries", "__Total__", servername);
            perfASPOCE.NextValue();

            PerformanceCounter perfASPOCHR = new PerformanceCounter(strASPpNETVersion, "Output Cache Hit Ratio", "__Total__", servername);
            perfASPOCHR.NextValue();

            PerformanceCounter perfASPOCTR = new PerformanceCounter(strASPpNETVersion, "Output Cache Turnover Rate", "__Total__", servername);
            perfASPOCTR.NextValue();

            // *****************************************************
            // Timers take about 1 second to poll
            // *****************************************************
            System.Threading.Thread.Sleep(1000);

            // *****************************************************
            // Now it is safe to use all Perf Counters
            // *****************************************************
            Hashtable parameters = new Hashtable();
            DatabaseUtility dbUtility = Global.GetDatabaseUtility();

            StringBuilder sb = new StringBuilder();
            sb.Append(" INSERT INTO web_health ");
            sb.Append(" (dt_taken, server_name, uptime, ");
            sb.Append(" processor_time, processor_priv_time, processor_interrupt_time, ");
            sb.Append(" system_processor_queue_length, system_context_switches_sec, ");
            sb.Append(" memory_avail_mbytes, memory_page_reads_sec, memory_pages_sec, ");
            sb.Append(" server_pool_nonpaged_bytes, server_pool_nonpaged_failures, server_pool_paged_failures, ");
            sb.Append(" server_pool_nonpaged_peak, memory_cache_bytes, memory_cache_faults_sec, ");
            sb.Append(" cache_MDL_read_hits_percent,disk_queue_length,disk_avg_read_queue_length,");
            sb.Append(" disk_avg_write_queue_length,disk_avg_disk_sec_read,nic1_bytes_total_sec,");
            sb.Append(" nic2_byts_total_sec,tcp_segments_sent_sec,udp_datagrams_sent_sec,");
            sb.Append(" net_exceptions,net_exceptions_sec,net_contention_rate_sec,");
            sb.Append(" net_queue_length,net_GC_percent,net_requests_sec,");
            sb.Append(" isapi_requests_sec,net_requests_timed_out,net_request_execution_time,");
            sb.Append(" net_cache_total_entries,net_cache_hit_ratio,net_output_cache_entries,");
            sb.Append(" net_output_cache_hit_ratio,net_output_cache_turnover_rate ");
            sb.Append(" ) VALUES (");
            sb.Append(" " + dbUtility.GetCurrentDateFunction() + ", @server_name, @uptime, ");
            sb.Append(" @processor_time, @processor_priv_time, @processor_interrupt_time, ");
            sb.Append(" @system_processor_queue_length, @system_context_switches_sec, ");
            sb.Append(" @memory_avail_mbytes, @memory_page_reads_sec, @memory_pages_sec, ");
            sb.Append(" @server_pool_nonpaged_bytes, @server_pool_nonpaged_failures, @server_pool_paged_failures, ");
            sb.Append(" @server_pool_nonpaged_peak, @memory_cache_bytes, @memory_cache_faults_sec, ");
            sb.Append(" @cache_MDL_read_hits_percent,@disk_queue_length,@disk_avg_read_queue_length,");
            sb.Append(" @disk_avg_write_queue_length,@disk_avg_disk_sec_read,@nic1_bytes_total_sec,");
            sb.Append(" @nic2_byts_total_sec,@tcp_segments_sent_sec,@udp_datagrams_sent_sec,");
            sb.Append(" @net_exceptions,@net_exceptions_sec,@net_contention_rate_sec,");
            sb.Append(" @net_queue_length,@net_GC_percent,@net_requests_sec,");
            sb.Append(" @isapi_requests_sec,@net_requests_timed_out,@net_request_execution_time,");
            sb.Append(" @net_cache_total_entries,@net_cache_hit_ratio,@net_output_cache_entries,");
            sb.Append(" @net_output_cache_hit_ratio,@net_output_cache_turnover_rate )");
                 
            try
            {             
                parameters.Add("@server_name", servername);
                parameters.Add("@uptime", perfSysUp.NextValue());

                parameters.Add("@processor_time", perfProcessorTime.NextValue());
                parameters.Add("@processor_priv_time", perfProcessorPrivTime.NextValue());
                parameters.Add("@processor_interrupt_time", perfProcessorInterruptTime.NextValue());
                parameters.Add("@system_processor_queue_length", perfProcessorQueue.NextValue());
                parameters.Add("@system_context_switches_sec", perfProcessorContextSwitch.NextValue());
            }
            catch (Exception exc)
            {
                m_logger.Error("Error with General params " + servername, exc);
            }

            // Mem
            try
            {
                parameters.Add("@memory_avail_mbytes", perfMem.NextValue());
                parameters.Add("@memory_page_reads_sec", perfMemPR.NextValue());
                parameters.Add("@memory_pages_sec", perfMemPS.NextValue());
                parameters.Add("@server_pool_nonpaged_bytes", perfMemPNB.NextValue());
                parameters.Add("@server_pool_nonpaged_failures", perfMemPNF.NextValue());
                parameters.Add("@server_pool_paged_failures", perfMemPPF.NextValue());
                parameters.Add("@server_pool_nonpaged_peak", perfMemPNP.NextValue());
                parameters.Add("@memory_cache_bytes", perfMemCB.NextValue());
                parameters.Add("@memory_cache_faults_sec", perfMemCFS.NextValue());
                parameters.Add("@cache_MDL_read_hits_percent", perfMemMDL.NextValue());
            }
            catch (Exception exc)
            {
                m_logger.Error("Error with Memory params " + servername, exc);
            }

            // Disk
            try
            {
                parameters.Add("@disk_queue_length", perfDisk.NextValue());
                parameters.Add("@disk_avg_read_queue_length", perfDiskQL.NextValue());
                parameters.Add("@disk_avg_write_queue_length", perfDiskWQL.NextValue());
                parameters.Add("@disk_avg_disk_sec_read", perfDiskDSR.NextValue());
            }
            catch(Exception exc)
            {
                m_logger.Error("Error with Disk I/O parmams" + servername, exc);
            }

            // Network
            if (pccNicExists.InstanceExists(nic1))
            {
                parameters.Add("@nic1_bytes_total_sec", perfNetBTS1.NextValue());
            }
            else
            {
                parameters.Add("@nic1_bytes_total_sec", -1);
            }

            if (pccNicExists.InstanceExists(nic2))
            {
                parameters.Add("@nic2_byts_total_sec", perfNetBTS2.NextValue());

            }
            else
            {
                parameters.Add("@nic2_byts_total_sec", -1);
            }

            if (PerformanceCounterCategory.Exists("TCP", servername))
            {
                parameters.Add("@tcp_segments_sent_sec", perfNetTCP.NextValue());
            }
            else
            {
                parameters.Add("@tcp_segments_sent_sec", -1);
            }

            if (PerformanceCounterCategory.Exists("UDP", servername))
            {
                parameters.Add("@udp_datagrams_sent_sec", perfNetUDP.NextValue());
            }
            else
            {
                parameters.Add("@udp_datagrams_sent_sec", -1);
            }

            // .net
            try
            {
                parameters.Add("@net_exceptions", perfCLRExcThrown.NextValue());
                parameters.Add("@net_exceptions_sec", perfCLRExcThrownSec.NextValue());
                parameters.Add("@net_contention_rate_sec", perfLocksAndThreads.NextValue());
                parameters.Add("@net_queue_length", perfQL.NextValue());

                parameters.Add("@net_GC_percent", perfCLRMEM.NextValue());
                parameters.Add("@net_requests_sec", perfASPRS.NextValue());
                parameters.Add("@isapi_requests_sec", perfASPERS.NextValue());
                parameters.Add("@net_requests_timed_out", perfASPRTO.NextValue());
                parameters.Add("@net_request_execution_time", perfASPRET.NextValue());
                parameters.Add("@net_cache_total_entries", perfASPCTE.NextValue());
                parameters.Add("@net_cache_hit_ratio", perfASPCTHR.NextValue());
                parameters.Add("@net_output_cache_entries", perfASPOCE.NextValue());
                parameters.Add("@net_output_cache_hit_ratio", perfASPOCHR.NextValue());
                parameters.Add("@net_output_cache_turnover_rate", perfASPOCTR.NextValue());
            }
            catch (Exception exc)
            {
                m_logger.Error("Error with NET params " + servername, exc);
            }

            m_logger.Debug("Saving performance data to db " + servername);

            dbUtility.ExecuteNonQuery(sb.ToString(), parameters);
        }

        /// <summary>
        /// Get list of web servers
        /// </summary>
        public static System.Collections.Hashtable WebServers
        {
            get
            {
                m_logger.Info("Getting WebStatsSettings");
                return (System.Collections.Hashtable)System.Configuration.ConfigurationSettings.GetConfig("WebStatsSettings");
            }
        }

        /// <summary>
        /// ASPNETVersion
        /// </summary>
        /// <returns></returns>
        private string ASPNETVersion
        {
            get {
                if (System.Configuration.ConfigurationManager.AppSettings["ASPNETVersion"] == null)
                {
                    return "ASP.NET Apps v2.0.50727";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["ASPNETVersion"].ToString (); 
                }
            
            }
        }

        /// <summary>
        /// UGCConversionEnabled
		/// </summary>
		/// <returns></returns>
        private bool UGCConversionEnabled
		{
            get {
                if (System.Configuration.ConfigurationManager.AppSettings["enableUGCConversion"] != null)
                {
                    return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["enableUGCConversion"]); 
                }

                return false;
            }
		}

		/// <summary>
		/// EmailNotificationsEnabled
		/// </summary>
		/// <returns></returns>
		private bool EmailNotificationsEnabled
		{
            get { return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["enableEmailNotifications"]); }
		}

		/// <summary>
		/// VideoImageGenerationEnabled
		/// </summary>
		/// <returns></returns>
		private bool VideoImageGenerationEnabled
		{
            get { return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["enableVideoImageGeneration"]); }
		}

		/// <summary>
		/// SummaryTablesUpdateEnabled
		/// </summary>
		/// <returns></returns>
		private bool SummaryTablesUpdateEnabled
		{
            get { return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["enableSummaryTablesUpdate"]); }
		}

		/// <summary>
		/// DatabaseCleanupEnabled
		/// </summary>
		/// <returns></returns>
		private bool DatabaseCleanupEnabled
		{
            get { return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["enableDatabaseCleanup"]); }
		}

		/// <summary>
		/// FunctionalityCheckEnabled
		/// </summary>
		/// <returns></returns>
		private bool FunctionalityCheckEnabled
		{
            get { return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["enableFunctionalityCheck"]); }
		}

        /// <summary>
		/// WebStatCheckEnabled
		/// </summary>
		/// <returns></returns>
		private bool WebStatCheckEnabled
		{
            get {
                return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["enableWebStatCheck"]);
            }   
		}

        /// <summary>
		/// WebStatEmailEnabled
		/// </summary>
		/// <returns></returns>
		private bool WebStatEmailEnabled
		{
            get {
                return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["enableWebStatEmail"]);
            }   
		}

        /// <summary>
        /// BirthdayGiftEnabled
        /// </summary>
        /// <returns></returns>
        private bool BirthdayGiftEnabled
        {
            get
            {
                return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["enableBirthdayGift"]);
            }
        }

        /// <summary>
        /// AnniversaryGiftEnabled
        /// </summary>
        /// <returns></returns>
        private bool AnniversaryGiftEnabled
        {
            get
            {
                return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["enableAnniversaryGift"]);
            }
        }

        private int AnniversaryHour
        {
            get
            {
                return Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["anniversaryJobHour"]);
            }
        }

        private int BirthdayHour
        {
            get
            {
                return Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["birthdayJobHour"]);
            }
        }
        
        private int _userId = 4;
		private string _username = "jason";

		/// <summary>
		/// Timers
		/// </summary>
		System.Timers.Timer tmrEmailNotification = new System.Timers.Timer ();
		System.Timers.Timer tmrSummaryTables = new System.Timers.Timer ();
		System.Timers.Timer tmrDatabaseCleanup = new System.Timers.Timer ();
		System.Timers.Timer tmrVideoImageGeneration = new System.Timers.Timer ();
		System.Timers.Timer tmrFunctionalityCheck = new System.Timers.Timer ();
        System.Timers.Timer tmrWebStatCheck = new System.Timers.Timer();
        System.Timers.Timer tmrWebStatEmail = new System.Timers.Timer();
        System.Timers.Timer tmrUGCConversion = new System.Timers.Timer();
        System.Timers.Timer tmrBirthdayGift = new System.Timers.Timer();
        System.Timers.Timer tmrAnniversaryGift = new System.Timers.Timer();


		/// <summary>
		/// Logger
		/// </summary>
		private static readonly ILog m_logger = LogManager.GetLogger (typeof (KanevaService));
	}
}
