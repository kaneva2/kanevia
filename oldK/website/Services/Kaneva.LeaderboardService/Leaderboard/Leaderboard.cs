///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;
using System.IO;

namespace Leaderboard
{
    public partial class Leaderboard : Form
    {
        #region Declarations

        private GameFacade gameFacade;
        private UserFacade userFacade;
        private ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private int _zoneInstanceId = 0;
        private int _zoneType = 0;
        private int _points = 0;
        private int _rank = 1;
        private int _count = 0;
        private int _awardAmount = 0;
        private int _prizeAmount = 0;
        private int _objPlacementId = 0;
        private string _flagName = "";

        const int PRIZE_FIRST_PLACE = 10000;
        const int PRIZE_SECOND_PLACE = 2000;
        const int PRIZE_THIRD_PLACE = 1000;

        #endregion

        #region Initialization
        
        public Leaderboard()
        {
            InitializeComponent();
        }

        private void Leaderboard_Load(object sender, EventArgs e)
        {
            RunLeaderboardService();

            this.Dispose();
            this.Close();
        }

        #endregion Initialization

        #region Main Execution

        private void RunLeaderboardService()
        {
            // Set up log for net
            string s = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
            FileInfo l_fi = new FileInfo(s);
            log4net.Config.XmlConfigurator.Configure(l_fi);
            
            m_logger.Info("-- Starting Leaderboard Service --");

            // Get the objects per game with point totals
            DataTable dtResults = GetGameFacade.CreateLeaderboardFinalResults(Configuration.WorldLeaderboardRavePointValue, Configuration.WorldLeaderboardVisitPointValue);

            if (dtResults != null && dtResults.Rows.Count > 0)
            {
                foreach (DataRow result in dtResults.Rows)
                {
                    try
                    {
                        // Check to see if this object is in the same game as the prior object so we can determine ranking
                        if (Convert.ToInt32(result["zone_instance_id"]) == _zoneInstanceId && Convert.ToInt32(result["zone_type"]) == _zoneType)
                        {
                            _count++;
                            if (Convert.ToInt32(result["points"]) != _points)
                            {
                                _rank = _count;
                            }
                        }
                        else
                        {
                            _count = _rank = 1;
                        }

                        _flagName = result["flag_name"].ToString();
                        _points = Convert.ToInt32(result["points"]);
                        _zoneInstanceId = Convert.ToInt32(result["zone_instance_id"]);
                        _zoneType = Convert.ToInt32(result["zone_type"]);
                        _objPlacementId = Convert.ToInt32(result["obj_placement_id"]);

                        // Insert results of leaderboard into world_leaderboard table
                        int _worldLeaderboardId = GetGameFacade.InsertLeaderboardFinalResults(_objPlacementId, _zoneInstanceId,
                            _zoneType, _flagName, _rank, _points);

                        if (_worldLeaderboardId <= 0)
                        {
                            throw new IndexOutOfRangeException("Failed adding world leaderboard.");
                        }

                        // Get the team members for all flags on leaderboard
                        DataTable dtTeamMembers = GetGameFacade.GetTeamMembers(_objPlacementId);

                        if (dtTeamMembers != null && dtTeamMembers.Rows.Count > 0)
                        {
                            // Calculate winners and award amounts
                            _prizeAmount = 0;
                            _awardAmount = 0;

                            if (_rank <= 3)
                            {
                                switch (_rank)
                                {
                                    case 1:
                                        _prizeAmount = PRIZE_FIRST_PLACE;
                                        break;
                                    case 2:
                                        _prizeAmount = PRIZE_SECOND_PLACE;
                                        break;
                                    case 3:
                                        _prizeAmount = PRIZE_THIRD_PLACE;
                                        break;
                                    default:
                                        break;
                                }

                                // Caluculate the award amounts
                                _awardAmount = _prizeAmount / dtTeamMembers.Rows.Count;
                            }

                            // Insert members list into world_leaderboard_team_members_history
                            int ret = GetGameFacade.InsertLeaderboardFinalTeamMembers(dtTeamMembers, _worldLeaderboardId, _objPlacementId, _awardAmount);

                            m_logger.Info(ret + " team members added to history table for worldLeaderboardId=" + _worldLeaderboardId + " & objPlacementId=" + _objPlacementId);
                        }
                    }
                    catch (Exception exc)
                    {
                        m_logger.Error("Error in processessing leaderboard", exc);
                    }
                }

                // Mark interaction records as processed for leaderboard final results
                GetGameFacade.UpdateObjectInteractionsAsProcessed();
            }

            // Remove users from world_leaderboard_team_members_history at least 2 weeks old that do not have any unredeemed awards
            // Remove entries in world_leaderboards that do not have any members with unredeemed awards
            // Remove entries from world_object_interaction for VISITS that are at least 2 weeks old
            GetGameFacade.CleanupExpiredLeaderboards();
            
            m_logger.Info("-- Leaderboard Service Completed --");
        }

        #endregion Main Execution

        #region Properties

        /// <summary>
        /// Gets the UserFacadeInstance.
        /// </summary>
        private UserFacade GetUserFacade
        {
            get
            {
                if (userFacade == null)
                {
                    userFacade = new UserFacade();
                }
                return userFacade;
            }
        }

        /// <summary>
        /// Gets the GameFacade Instance.
        /// </summary>
        private GameFacade GetGameFacade
        {
            get
            {
                if (gameFacade == null)
                {
                    gameFacade = new GameFacade();
                }
                return gameFacade;
            }
        }

        #endregion Properties
    }
}
