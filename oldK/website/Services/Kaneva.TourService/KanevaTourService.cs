///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.IO;
using log4net;


namespace KanevaTourService
{
    public partial class KanevaTourService : ServiceBase
    {
      static KanevaTourService()
      {
        // Set up log for net
        string s = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
        FileInfo l_fi = new FileInfo(s);
        log4net.Config.XmlConfigurator.Configure(l_fi);
      }

      // The main entry point for the process
      static void Main()
      {
          System.ServiceProcess.ServiceBase[] ServicesToRun;
          ServicesToRun = new System.ServiceProcess.ServiceBase[] { new KanevaTourService() };
          System.ServiceProcess.ServiceBase.Run(ServicesToRun);
      }

      public KanevaTourService()
      {
          CreateTimers();   
          InitializeComponent();
      }

      protected override void OnStart(string[] args)
      {                 

        //Start timers
        tmrTimer.AutoReset = true;
        tmrTimer.Enabled = true;
        tmrTimer.Start();
                
      }

      protected override void OnStop()
      {
        tmrTimer.AutoReset = false;
        tmrTimer.Enabled = false;         
      }

      /// <summary>
      /// CreateTimers Method
      /// 
      /// </summary>
      private void CreateTimers()
      {
        int ServiceInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ServiceInterval"]);         
          
        // Start                
        tmrTimer.Interval = (60000 * ServiceInterval);	// Minutes to run
        tmrTimer.Elapsed += new System.Timers.ElapsedEventHandler(tmrTimer_Elapsed);
        tmrTimer.Enabled = true;



      }

      /// <summary>
      /// tmrTimer_Elapsed
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      protected void tmrTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
      {
        try
        {
          tmrTimer.Stop();     
   
          // do stuff
          int ret = KlausEnt.KEP.Kaneva.Tour.TourUtility.UpdateTour();
        }
        catch (Exception exc)
        {
            // catch
            exc.GetType();
        }
        finally
        {
          tmrTimer.Start();                              
        }
      }

      System.Timers.Timer tmrTimer = new System.Timers.Timer();

      /// <summary>
      /// Logger
      /// </summary>
      private static readonly ILog m_logger = LogManager.GetLogger(typeof(KanevaTourService));
    }
}
