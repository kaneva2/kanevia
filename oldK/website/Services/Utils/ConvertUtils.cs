///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace Com.Kei.Utils
{
	/// <summary>
	/// Summary description for ConvertUtils.
	/// </summary>
	public class ConvertUtils
	{
		public const string STR_DEFAULT = "";
		public const long LONG_DEFAULT = -1;
		public const int INT_DEFAULT = -1;
		public static readonly DateTime DATETIME_DEFAULT = DateTime.MinValue;
		
		/// <summary>
		/// returns STR_DEFAULT if o is null or DB.NUll
		/// </summary>
		/// <param name="o"></param>
		/// <returns></returns>
		public static string toStr(object o)
		{
			string retVal = STR_DEFAULT;
			if(o != null && o != DBNull.Value)
			{
				retVal = o.ToString();
			}
			return retVal;
		}

		/// <summary>
		/// returns LONG_DEFAULT if o doesn't represent a long
		/// </summary>
		/// <param name="o"></param>
		/// <returns></returns>
		public static long toLong(object o)
		{
			long retVal = LONG_DEFAULT;
			try
			{
				retVal = long.Parse(o.ToString());
			}
			catch{}
			return retVal;
		}

		/// <summary>
		/// returns INT_DEFAULT if o doesn't represent an int
		/// </summary>
		/// <param name="o"></param>
		/// <returns></returns>
		public static int toInt(object o)
		{
			int retVal = INT_DEFAULT;
			try
			{
				retVal = int.Parse(o.ToString());
			}
			catch{}
			return retVal;
		}

		/// <summary>
		/// returns DATETIME_DEFAULT if o doesn't represent a datetime
		/// </summary>
		/// <param name="o"></param>
		/// <returns></returns>
		public static DateTime toDateTime(object o)
		{
			DateTime retVal = DATETIME_DEFAULT;
			try
			{
				retVal = DateTime.Parse((o.ToString()));
			}
			catch{}
			return retVal;
		}
	}
}
