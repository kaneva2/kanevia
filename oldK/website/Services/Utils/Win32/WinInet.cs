///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System ;
using System.Runtime ;
using System.Runtime.InteropServices ;

namespace Com.Kei.Utils.Win32
{
	/// <summary>
	/// Summary description for WinInet.
	/// </summary>
	public class WinInet
	{
		public WinInet()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		[Flags]
			enum InternetConnectionState: int
		{
			INTERNET_CONNECTION_MODEM = 0x1,
			INTERNET_CONNECTION_LAN = 0x2,
			INTERNET_CONNECTION_PROXY = 0x4,
			INTERNET_RAS_INSTALLED = 0x10,
			INTERNET_CONNECTION_OFFLINE = 0x20,
			INTERNET_CONNECTION_CONFIGURED = 0x40
		}
		
		/// <summary>
		/// determine whether or not there is a connection to the Internet present on the local machine.
		/// </summary>
		/// <param name="lpdwFlags"></param>
		/// <param name="dwReserved"></param>
		/// <returns></returns>
		[DllImport("WININET", CharSet=CharSet.Auto)]
		static extern bool InternetGetConnectedState(
			ref InternetConnectionState lpdwFlags, 
			int dwReserved);

		//Creating a function that uses the API function...
		/// <summary>
		/// this call doesn't seem to be expensive. 100000 calls took less than 1 second
		/// </summary>
		/// <returns></returns>
		public static bool IsConnectedToInternet( )
		{
			InternetConnectionState flags = 0;
			return InternetGetConnectedState( ref flags, 0 ) ;
		}
	}
}
