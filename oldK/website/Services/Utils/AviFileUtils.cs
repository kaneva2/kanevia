///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Runtime.InteropServices;

namespace Com.Kei.Utils
{
	/// <summary>
	/// Summary description for AviFileUtils.
	/// </summary>
	public class AviFileUtils
	{
		/// <summary>
		/// avi file extension
		/// </summary>
		public const string AVI_EXT = ".avi";

		public AviFileUtils()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		[StructLayout(LayoutKind.Sequential, CharSet=CharSet.Auto)]
		public struct AVIFILEINFO 
		{
			public uint dwMaxBytesPerSec;
			public uint dwFlags;
			public uint dwCaps;
			public uint dwStreams;
			public uint dwSuggestedBufferSize;
			public uint dwWidth;
			public uint dwHeight;
			public uint dwScale;
			public uint dwRate;
			public uint dwLength;
			public uint dwEditCount;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst=64)]
			public string szFileType;
		}
		
		[DllImport("avifil32.dll")]
		public static extern void AVIFileInit();

		[DllImport("avifil32.dll")]
		public static extern void AVIFileExit();

		[DllImport("avifil32.dll", CharSet=CharSet.Auto)]
		public static extern uint AVIFileOpen(out IntPtr ppfile, string
			szFile, uint mode, IntPtr pclsidHandler);

		[DllImport("avifil32.dll")]
		public static extern uint AVIFileRelease(IntPtr pfile);

		[DllImport("avifil32.dll", CharSet=CharSet.Auto)]
		public static extern uint AVIFileInfo(IntPtr pfile, out AVIFILEINFO
			pfi, int lSize);

		public static AVIFILEINFO GetInfo(string path)
		{
			AVIFILEINFO retVal = new AVIFILEINFO() ;

			AVIFileInit();
			IntPtr hFile;
			AVIFileOpen(out hFile, path, 0, new IntPtr() );
			AVIFileInfo(hFile, out retVal,
				System.Runtime.InteropServices.Marshal.SizeOf(retVal));
			AVIFileRelease(hFile);
			return retVal;
		}
	}
}
