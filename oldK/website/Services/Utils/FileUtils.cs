///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.IO;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using Com.Kei.Utils.Win32;

namespace Com.Kei.Utils
{
	/// <summary>
	/// Summary description for FileUtils.
	/// </summary>
	public class FileUtils
	{
		/// <summary>
		/// all lower case
		/// </summary>
		private static readonly IList WMP_EXTENSION;
		
		/// <summary>
		/// all lower case
		/// </summary>
		private static readonly IList PICTURE_EXTENSION;

		public FileUtils()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		static FileUtils()
		{
			WMP_EXTENSION = new ArrayList();
			PICTURE_EXTENSION = new ArrayList();

			//Windows Media formats
			WMP_EXTENSION.Add(".asf"); 
			WMP_EXTENSION.Add(".asx"); 
			WMP_EXTENSION.Add(".avi");
			WMP_EXTENSION.Add(".wav"); 
			WMP_EXTENSION.Add(".wax"); 
			WMP_EXTENSION.Add(".wma"); 
			WMP_EXTENSION.Add(".wm");
			WMP_EXTENSION.Add(".wmv");

			//Audio Visual Interleave
			WMP_EXTENSION.Add(".avi");

			//Moving Pictures Experts Group (MPEG)
			WMP_EXTENSION.Add(".m3u"); 
			WMP_EXTENSION.Add(".mp2v"); 
			WMP_EXTENSION.Add(".mpg"); 
			WMP_EXTENSION.Add(".mpeg"); 
			WMP_EXTENSION.Add(".m1v"); 
			WMP_EXTENSION.Add(".mp2"); 
			WMP_EXTENSION.Add(".mp3"); 
			WMP_EXTENSION.Add(".m3u"); 
			WMP_EXTENSION.Add(".mpa"); 
			WMP_EXTENSION.Add(".mpe"); 
			WMP_EXTENSION.Add(".mpv2"); 

			//Note MPEG 2 requires third-party software or hardware decoders.
			//DVD Video
			WMP_EXTENSION.Add(".vob"); 

			//Note DVD playback requires third-party software or hardware decoders and is supported only in Windows XP.
			//Musical Instrument Digital Interface (MIDI)
			WMP_EXTENSION.Add(".mid"); 
			WMP_EXTENSION.Add(".midi"); 
			WMP_EXTENSION.Add(".rmi");

			//Apple Macintosh AIFF Resource
			WMP_EXTENSION.Add(".aif"); 
			WMP_EXTENSION.Add(".aifc"); 
			WMP_EXTENSION.Add(".aiff");

			//Sun Microsystems and NeXT
			WMP_EXTENSION.Add(".au"); 
			WMP_EXTENSION.Add(".snd");

			//Intel Video File
			WMP_EXTENSION.Add(".ivf");

			//Note Intel Video File support requires the Intel Video 5.x codec.
			//Windows Media Player Skins
			WMP_EXTENSION.Add(".wmz"); 
			WMP_EXTENSION.Add(".wms");

			//QuickTime Content
			WMP_EXTENSION.Add(".avi"); 
//			WMP_EXTENSION.Add(".mov"); 
//			WMP_EXTENSION.Add(".qt");

			//Note Windows Media Player can play back older forms of the QuickTime format only. For additional information, click the following article number to view the article in the Microsoft Knowledge Base:
			//316992 Windows Media Player Multimedia File Formats
			//Algebraic-Code-Excited Linear Prediction
			//UNIX
			WMP_EXTENSION.Add(".au"); 
			WMP_EXTENSION.Add(".snd");

			//Macromedia Flash
			WMP_EXTENSION.Add(".swf");

			//image files
			PICTURE_EXTENSION.Add(".bmp");
			PICTURE_EXTENSION.Add(".jpg");
			PICTURE_EXTENSION.Add(".jpeg");
			PICTURE_EXTENSION.Add(".jpe");
			PICTURE_EXTENSION.Add(".jfif");
			PICTURE_EXTENSION.Add(".gif");
			PICTURE_EXTENSION.Add(".tif");
			PICTURE_EXTENSION.Add(".tiff");
//			PICTURE_EXTENSION.Add(".png");
			PICTURE_EXTENSION.Add(".ico");
		}

		/// <summary>
		/// true if file can be played by windows media player
		/// </summary>
		/// <param name="file"></param>
		/// <returns></returns>
		public static bool IsWmpFile(FileInfo file)
		{
			String ext = file.Extension;
			return WMP_EXTENSION.Contains(ext);
		}

		/// <summary>
		/// true if file is an supported image
		/// </summary>
		/// <param name="file"></param>
		/// <returns></returns>
		public static bool IsPictureFile(FileInfo file)
		{
			String ext = file.Extension;
			return PICTURE_EXTENSION.Contains(ext);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="d"></param>
		/// <returns>size of the directory</returns>
		public static long DirSize(DirectoryInfo d) 
		{    
			long Size = 0;    
			// Add file sizes.
			FileInfo[] fis = d.GetFiles();
			foreach (FileInfo fi in fis) 
			{      
				Size += fi.Length;    
			}
			// Add subdirectory sizes.
			DirectoryInfo[] dis = d.GetDirectories();
			foreach (DirectoryInfo di in dis) 
			{
				Size += DirSize(di);   
			}
			return(Size);  
		}

		/// <summary>
		/// true if the path represents a file 
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public static bool isFile(string path)
		{
			bool retVal = false;
			try
			{
				FileInfo fileInfo = new FileInfo(path);
				retVal =  fileInfo.Exists;
			}
			catch(Exception){}

			return retVal;
		}

		/// <summary>
		/// /// 
		/// </summary>
		/// <param name="path"></param>
		/// <returns>true if the path represents a directory</returns>
		public static bool isDirectory(string path)
		{
			bool retVal = false;
			try
			{
				DirectoryInfo directoryInfo = new DirectoryInfo(path);
				retVal = directoryInfo.Exists;
			}
			catch(Exception){}

			return retVal;
		}

		/// <summary>
		/// /// 
		/// </summary>
		/// <param name="path"></param>
		/// <returns>true if the path represents a disk</returns>
		public static bool IsDrive(string path)
		{
			//this is Windows specific
			return path.EndsWith(":") | path.EndsWith(@":\");
		}

		/// <summary>
		/// return the drive name i.e c:
		/// </summary>
		/// <param name="fileSystemInfo"></param>
		/// <returns></returns>
		public static string GetDriveName(FileSystemInfo fileSystemInfo)
		{
			string fullName = fileSystemInfo.FullName;
			int index = fullName.IndexOf(":");
			return fullName.Substring(0, index + 1);
		}

		/// <summary>
		/// return the drive name i.e c:
		/// </summary>
		/// <param name="fullName"></param>
		/// <returns></returns>
		public static string GetDriveName(string fullName)
		{
			int index = fullName.IndexOf(":");
			return fullName.Substring(0, index + 1);
		}

		/// <summary>
		/// windows media player supported files
		/// </summary>
		public static IList WmpExtensions
		{
			get { return WMP_EXTENSION; }
		}

		/// <summary>
		/// picture files
		/// </summary>
		public static IList PictureExtensions
		{
			get { return PICTURE_EXTENSION; }
		}

		public static string GetDirInfo(string path)
		{
			StringBuilder sb = new StringBuilder() ;
			int numFiles = 0;
			long length = 0;
			Calculate(new DirectoryInfo(path), ref numFiles, ref length);

			DirectoryInfo info = new DirectoryInfo(path);
//			sb.Append(info.Name);
//			sb.Append(" ");
			sb.Append(numFiles);
			sb.Append(" files ");
			sb.Append(" ");
			sb.Append(StrFormatter.FormatFileSize(length));
			sb.Append(" Date Modified: ");
			sb.Append(info.LastWriteTime.ToShortDateString());
			
			return sb.ToString();
		}

		private static void Calculate(DirectoryInfo dir, ref int numFiles, ref long length)
		{
			FileInfo[] fis = dir.GetFiles();
			numFiles += fis.Length;
			foreach (FileInfo fi in fis) 
			{      
				length += fi.Length;
			}
			// Add subdirectory sizes.
			DirectoryInfo[] dis = dir.GetDirectories();
			foreach (DirectoryInfo di in dis) 
			{
				Calculate(di, ref numFiles, ref length);   
			}
		}

		public static string GetFileInfo(string path)
		{
			SHFILEINFO fi;
			Shell32.SHGetFileInfo(path, 0, out fi, Marshal.SizeOf(typeof(SHFILEINFO)), 
				(int)
				(FileInfoFlags.SHGFI_TYPENAME | 
				FileInfoFlags.SHGFI_DISPLAYNAME |
					FileInfoFlags.SHGFI_ATTRIBUTES));

			StringBuilder sb = new StringBuilder() ;
			try
			{
				FileInfo info = new FileInfo(path);
				sb.Append("Type: ");
				sb.Append(fi.szTypeName);
				if(info.Extension.ToLower().Equals(AviFileUtils.AVI_EXT.ToLower()))
				{
					AviFileUtils.AVIFILEINFO aviInfo = AviFileUtils.GetInfo(path);
					sb.Append(" Duration: ");
					TimeSpan length = new TimeSpan(0,0,(int) aviInfo.dwLength);
					sb.Append(length.ToString());
					sb.Append(" Dimensions: ");
					sb.Append(aviInfo.dwWidth);
					sb.Append("x");
					sb.Append(aviInfo.dwHeight);
				}
			}
			catch(Exception)
			{
				//invalid avi file
			}

			try
			{
				FileInfo info = new FileInfo(path);
				sb.Append(" Size: ");
				sb.Append(StrFormatter.FormatFileSize(info.Length));
				sb.Append(" Date Modified: ");
				sb.Append(info.LastWriteTime.ToShortDateString());
			}catch(Exception){}
			return sb.ToString();
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="directoryInfo"></param>
		/// <param name="recursive"></param>
		/// <param name="timeout">retry in milliseconds, 100 milliseconds increment</param>
		public static void DeleteDirectory(DirectoryInfo directoryInfo,bool recursive, int timeout)
		{
			int retries = timeout / 100;
			for(int i = 0; i < retries; i++)
			{
				try
				{
					directoryInfo.Delete(recursive);
					return;
				}catch(Exception)
				{
					Thread.Sleep(100);
				}
			}
			directoryInfo.Delete(recursive);
		}
	}
}
