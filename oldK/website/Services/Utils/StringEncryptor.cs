///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Security.Cryptography;
using System.Text;

namespace Com.Kei.Utils
{
	/// <summary>
	/// Simple password based string encryption
	/// </summary>
	public class StringEncryptor
	{
		private const string PASSWORD = "K31Mm0!";
		TripleDESCryptoServiceProvider des;
		byte[] pwdhash;

		public StringEncryptor()
		{
			MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
			pwdhash = hashmd5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(PASSWORD));

			//implement DES3 encryption
			des = new TripleDESCryptoServiceProvider();

			//the key is the secret password hash.
			des.Key = pwdhash;

			des.Mode = CipherMode.ECB; 
		}

		public string Encrypt(string s)
		{
			byte[] buff = ASCIIEncoding.ASCII.GetBytes(s);
			return Convert.ToBase64String(
				des.CreateEncryptor().TransformFinalBlock(buff, 0, buff.Length)
				);
		}

		public string Decrypt(string s)
		{
			byte[]buff = Convert.FromBase64String(s);
			
			return ASCIIEncoding.ASCII.GetString(
				des.CreateDecryptor().TransformFinalBlock(buff, 0, buff.Length)
				);
		}
	}
}
