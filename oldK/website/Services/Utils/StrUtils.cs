///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace Com.Kei.Utils
{
	/// <summary>
	/// Summary description for StrUtils.
	/// </summary>
	public class StrUtils
	{
		public StrUtils()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static bool IsEmpty(string s)
		{
			return s == null || s.Length == 0;
		}
	}
}
