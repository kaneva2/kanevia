///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;

namespace Com.Kei.Utils
{
	/// <summary>
	/// Summary description for IOUtils.
	/// </summary>
	public class IOUtils
	{
		public IOUtils()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static void SaveFile(Stream stream, string saveTo)
		{
			BinaryReader reader = new BinaryReader(stream);
			SaveFile(reader, saveTo);
		}

		public static void SaveFile(BinaryReader reader, string saveTo)
		{
			FileInfo fileInfo = new FileInfo(saveTo);
			if(!fileInfo.Directory.Exists)
			{
				fileInfo.Directory.Create();
			}

			FileStream fileStream = new FileStream(saveTo, FileMode.Create, FileAccess.Write);

			// Write bytes to the output stream.
			int size = 4096;
			byte[] bytes = new byte[4096];
			int numBytes;
			while((numBytes = reader.Read(bytes, 0, size)) > 0)
			{
				fileStream.Write(bytes, 0, numBytes);
			}
  
			// cleanup
			reader.Close();
			fileStream.Close();
		}
	}
}
