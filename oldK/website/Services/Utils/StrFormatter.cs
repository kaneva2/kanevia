///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Globalization;
using System.Text;

namespace Com.Kei.Utils
{
	/// <summary>
	/// Summary description for StrFormatter.
	/// </summary>
	public class StrFormatter
	{
		public static readonly string KB = "KB";
		public static readonly string MB = "MB";
		public static readonly long KILO_BYTE = 1024;

		public StrFormatter()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		/// kb/second
		/// </summary>
		/// <param name="speed">byte/second</param>
		/// <returns></returns>
		public static string FormatSpeed(float speed)
		{
			string unit = "KB/S";
			int scale = 1000; //show kb/second
			StringBuilder sb = new StringBuilder();

			speed = speed/scale;
			string s = String.Format("{0:0.0}", speed);

			sb.Append(s);
			sb.Append(" ");
			sb.Append(unit);

			return sb.ToString();
		}

		/// <summary>
		/// Format number to KB
		/// </summary>
		/// <param name="size"></param>
		/// <returns></returns>
		public static string FormatFileSize(long size) 
		{
			return FormatFileSize(size, true);
			
		} 

		/// <summary>
		/// 
		/// </summary>
		/// <param name="size">file size in byte</param>
		/// <param name="roundToMB">true if show MB, otherwise show KB</param>
		/// <returns></returns>
		public static string FormatFileSize(long size, bool roundToMB)
		{
			StringBuilder sb = new StringBuilder() ;
			NumberFormatInfo numberFormatInfo = new NumberFormatInfo(); 
			long kbSize = 0; 
    
			if (size > 0 && size < 1024 ) 
			{ 
				size = KILO_BYTE;
			} 
			//convert to KB 
			kbSize = size / KILO_BYTE;
 
			if(kbSize < 1024 && roundToMB) // less than 1 mb
			{
				//format number with default format 
				sb.Append(kbSize.ToString("n",numberFormatInfo));
				//remove decimal 
				sb.Replace(".00", "");
				sb.Append(" ");
				sb.Append(KB);
			}
			else
			{
				long mbSize = kbSize / KILO_BYTE;
				//format number with default format 
				sb.Append(mbSize.ToString("n",numberFormatInfo));
				//remove decimal 
				//				sb.Replace(".00", "");
				sb.Append(" ");
				sb.Append(MB);
			}

			return sb.ToString();
		}
	}
}
