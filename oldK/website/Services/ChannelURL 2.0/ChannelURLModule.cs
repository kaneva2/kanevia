///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;

using System.IO;
using System.Text;
using System.Security.Cryptography;

using System.Data;
using System.Collections;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for ChannelURLHandler.
	/// </summary>
	public class ChannelURLModule : System.Web.IHttpModule
	{
		/// <summary>
		/// Init is required from the IHttpModule interface
		/// </summary>
		/// <param name="Appl"></param>
		public void Init (System.Web.HttpApplication Appl)
		{
			//make sure to wire up to BeginRequest
			Appl.BeginRequest+=new System.EventHandler (Rewrite_BeginRequest);
		}
 
		/// <summary>
		/// Dispose is required from the IHttpModule interface
		/// </summary>
		public void Dispose () 
		{
			//make sure you clean up after yourself
		}

		/// <summary>
		/// To handle the starting of the incoming request
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		public void Rewrite_BeginRequest (object sender, System.EventArgs args) 
		{
			HttpApplication app = (HttpApplication) sender; 
			HttpRequest request = app.Request; 
			HttpResponse response = app.Response;

			String strTestDNS;
			strTestDNS = request.Url.Host + request.Path;
			strTestDNS = strTestDNS.ToLower ();

			string commURL = "";

			// Assume Channel URL is the part at the end
			commURL = System.IO.Path.GetFileName (strTestDNS);

            // This check was added of issue where ajax call to /services/validation.asmx/Username was failing because
            // a user had a profile url of kaneva.com/username and the call was getting redirected to the user profile page
            // instead of passing through to the web service.
            if (!strTestDNS.Contains("/services/"))
            {
                if (commURL.Length.Equals(0))
                {
                    // There is an issue for these URL's and using RewritePath
                    // http://www.kaneva.com/help/
                    if (strTestDNS.EndsWith("kaneva.com/") || strTestDNS.EndsWith("kaneva.com"))
                    {
                        app.Context.RewritePath("~/free-virtual-world.kaneva");
                    }
                    else
                    {
                        //response.Redirect ("http://" + SiteName + "/home.aspx");
                        response.Status = "301 Moved Permanently";
                        response.AddHeader("Location", "http://" + SiteName + "/free-virtual-world.kaneva");
                        response.End();
                    }
                }

                // Test for channel URL. It is a URL without a .
                if (commURL.IndexOf(".").Equals(-1))
                {
                    // regular channel
                    DataRow drChannel = GetChannelFromURL(commURL);

                    if (drChannel != null)
                    {
                        if (Convert.ToInt32(drChannel["is_personal"]).Equals(1))
                        {
                            response.Redirect(GetPersonalChannelUrl(drChannel["name_no_spaces"].ToString()));
                        }
                        else
                        {
                            response.Redirect(GetBroadcastChannelUrl(drChannel["name_no_spaces"].ToString()));
                        }
                        return;
                    }
                }
            }
		}

		/// <summary>
		/// Return the personal channel link
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		private string GetPersonalChannelUrl (string nameNoSpaces)
		{
			return "http://" + SiteName + "/channel/" + nameNoSpaces + ".people";
		}

		/// <summary>
		/// Return the broadcast channel link
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		private string GetBroadcastChannelUrl (string nameNoSpaces)
		{
			return "http://" + SiteName + "/channel/" + nameNoSpaces + ".channel";
		}

		/// <summary>
		/// Get a community
		/// </summary>
		/// <param name="urlName"></param>
		/// <returns></returns>
		private DataRow GetChannelFromURL (string urlName)
		{
			DatabaseUtility dbUtility = GetDatabaseUtility ();

			string sqlSelect = "SELECT c.community_id, c.name_no_spaces, is_personal " +
				" FROM communities c " +
				" WHERE c.url = @urlName";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@urlName", urlName);
			return dbUtility.GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// Get the database utility
		/// </summary>
		/// <returns></returns>
		private DatabaseUtility GetDatabaseUtility ()
		{
			if (m_DatabaseUtilty == null)
			{
				m_DatabaseUtilty = new MySQLUtility (ConnectionString);
			}
			return m_DatabaseUtilty;
		}

		/// <summary>
		/// Retrieves SiteName string from Web.Config file.
		/// </summary>
		private string SiteName 
		{
			get 
			{
				return System.Configuration.ConfigurationManager.AppSettings ["SiteName"];
			} 
		}

		/// <summary>
		/// Retrieves database connection string from Web.Config file.
		/// </summary>
		private string ConnectionString 
		{
			get 
			{
                if (System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings["dbEncrypt"].ToUpper().Equals("TRUE"))
				{
					// Encrypt (System.Configuration.ConfigurationSettings.AppSettings ["ConnectionString"]);
                    return Decrypt(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
				}
				else
				{
                    return System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
				}
			} 
		}

		/// <summary>
		/// Decrypt
		/// </summary>
		/// <param name="encryptedText"></param>
		/// <returns></returns>
		private string Decrypt (string encryptedText)
		{
			TripleDESCryptoServiceProvider tds = new TripleDESCryptoServiceProvider ();

			string key = "jb7431%o$#=@tp+&";
			PasswordDeriveBytes pdbkey = new PasswordDeriveBytes (key, ASCIIEncoding.ASCII.GetBytes (String.Empty));
			byte[] keyByte = pdbkey.GetBytes (key.Length);

			// Decrypt it
			// Convert from base 64 string to bytes
			byte[] cryptoByte = Convert.FromBase64String (encryptedText);

			// Set private key
			tds.Key = keyByte;
			tds.IV = new byte[] {0xf, 0x6f, 0x13, 0x2e, 0x35, 0xc2, 0xcd, 0xf9};			

			// Decryptor object
			ICryptoTransform cryptoTransform = tds.CreateDecryptor ();
			try
			{
				// Memory stream object
				MemoryStream msd = new MemoryStream (cryptoByte, 0, cryptoByte.Length);

				// Crypto stream object
				CryptoStream csd = new CryptoStream (msd, cryptoTransform, CryptoStreamMode.Read);

				// Get the result from the Crypto stream
				StreamReader sr = new StreamReader (csd);
				return sr.ReadToEnd ();
			}
			catch (Exception)
			{
				//m_logger.Error ("Decryption Error", exc);
				return null;
			}
		}

		private static DatabaseUtility m_DatabaseUtilty;

	}

}
