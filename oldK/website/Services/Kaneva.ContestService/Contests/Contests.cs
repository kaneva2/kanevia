///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using KlausEnt.KEP.Kaneva.Mailer;
using System.IO;                     
using log4net;

namespace Contests
{
    public partial class Contests : Form
    {
        private ContestFacade contestFacade;
        private UserFacade userFacade;
        private BlastFacade blastFacade;
        ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        public Contests ()
        {
            InitializeComponent ();
        }

        private void RunContestService ()
        {
            // Set up log for net
            string s = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
            FileInfo l_fi = new FileInfo (s);
            log4net.Config.XmlConfigurator.Configure (l_fi);

            contestFacade = new ContestFacade();

            m_logger.Info("-- Starting Contest Service --");
            PagedList<Contest> contests;

            // Get contests with no winner and that have ended within within the past 3 days and
            // that have not been processed by this service yet
            // PROCESS CONTEST ENDED WAITING FOR WINNER
            try
            {
                contests = contestFacade.GetContests (0, "", "owner_vote_winner_entry_id IS NULL AND status = " + (int) eCONTEST_STATUS.Active + " AND NOW() > end_date", 1, 100);
                m_logger.Info (contests.Count.ToString() + " contests found that are waiting for a winner to be selected.");
                
                foreach (Contest c in contests)
                {
                    try
                    {
                        // If no entries, contest is over, refund credits to owner
                        if (c.NumberOfEntries == 0)
                        {
                            EndContestWithNoEntries (c, true);
                        }
                        // If num entries < minimum number of entries
                        else if (c.NumberOfEntries < Configuration.ContestMinumumEntries)
                        {
                            // Treat this case same as contest that has min num entries
                            ProcessContestEndedWaitingForWinner (c, eCONTEST_MSG_TYPES.Notify_Owner_Contest_Ended_Less_Than_Min_Entries);
                        }
                        // Equal to or more than the min. number entries needed
                        else
                        {
                            ProcessContestEndedWaitingForWinner (c, eCONTEST_MSG_TYPES.Notify_Owner_Contest_Ended);
                        }
                    }
                    catch (Exception exc)
                    {
                        m_logger.Error ("Error in contests ended waiting for winner for contestId " + c.ContestId, exc);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error in process contest ended waiting for winner", exc);
            }
            
            // Get contests that have no winner and ended more than 3 days ago
            // PROCESS CONTEST USING POPULAR WINNER
            try
            {               
                contests = contestFacade.GetContests (0, "", "owner_vote_winner_entry_id IS NULL AND status = " + (int)eCONTEST_STATUS.Ended_Waiting_For_Winner + " AND NOW() > DATE_ADD(end_date, INTERVAL 3 DAY)", 1, 100);
                m_logger.Info (contests.Count.ToString() + " contests found that have no winner and ended more than 3 days ago.");

                foreach (Contest c in contests)
                {
                    try
                    {
                        // If no entries, there is no winner, refund owner
                        if (c.NumberOfEntries == 0)
                        {
                            EndContestWithNoEntries (c, true);
                        }
                        else if (c.NumberOfEntries < Configuration.ContestMinumumEntries)
                        {
                            EndContestWithNoEntries (c, false);
                        }
                        // Equal to or more than the min. number entries needed
                        else
                        {
                            ProcessContestUsingPopularWinner (c);
                        }
                    }
                    catch (Exception exc)
                    {
                        m_logger.Error ("Error in contests service processing contests that have no winner and ended more than 3 days ago for contestId " + c.ContestId, exc);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error in process contest using popular winner", exc);
            }

            // Get contests older than 30 days and archive them
            // ARCHIVE CONTESTS
            try
            {
                contests = contestFacade.GetContests (0, "", "NOW() > DATE_ADD(end_date, INTERVAL 30 DAY) AND archived = 0", 1, 100);
                m_logger.Info (contests.Count.ToString () + " contests that need to be archived.");

                foreach (Contest c in contests)
                {
                    try
                    {
                        contestFacade.ArchiveContest (c.ContestId);
                        m_logger.Info ("ContestId " + c.ContestId.ToString () + " has been archived.");
                    }
                    catch (Exception exc)
                    {
                        m_logger.Error ("Error in contests service archiving contests for contestId " + c.ContestId, exc);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error in process archive contest", exc);
            }

            m_logger.Info ("-- Contest Service Completed --");
        }

        private void EndContestWithNoEntries (Contest c, bool sendEmail)
        { 
            // Refund Owner Credits          
            int result = GetContestFacade.MakeContestPayment (c.Owner.UserId, c.ContestId, "KPOINT", Convert.ToDouble (c.PrizeAmount), (int) TransactionType.eTRANSACTION_TYPES.CASH_TT_CONTEST, (int) TransactionType.eCONTEST_TRANSACTION_TYPES.CONTEST_TT_REFUND);

            if (result > 0)
            {
                // Update contest to not active
                GetContestFacade.UpdateContestStatus (c.ContestId, (int) eCONTEST_STATUS.Ended_Owner_Refunded);
                m_logger.Info ("ContestId " + c.ContestId.ToString () + " has ended with no entries.  Contest owner refunded.");

                // Send Emails
                if (sendEmail)
                {
                    SendMessage (eCONTEST_MSG_TYPES.Ended_No_Winner, c, c.Owner.UserId);
                }
            }
            else
            {
                m_logger.Error ("Payment failed trying to refund owner for contest with no entries.  ContestId " + c.ContestId.ToString ());
            }
        }

        private void ProcessContestEndedWaitingForWinner (Contest c, eCONTEST_MSG_TYPES msgType)
        {
            // Update contest to not waiting for winner to be selected
            GetContestFacade.UpdateContestStatus (c.ContestId, (int) eCONTEST_STATUS.Ended_Waiting_For_Winner);

            // Set popular winner
            GetContestFacade.SetPopularWinner (c.ContestId);

            // Send Emails
            SendMessage (msgType, c, c.Owner.UserId);

            m_logger.Info ("ContestId " + c.ContestId.ToString () + " status set to " + ((int) eCONTEST_STATUS.Ended_Waiting_For_Winner).ToString () + " (Ended_Waiting_For_Winner).");
        }

        private void ProcessContestUsingPopularWinner (Contest contest)
        {
            m_logger.Info ("Processing ContestId " + contest.ContestId.ToString () + " setting winner using popular vote winner.");
        
            // Set contest winner
            GetContestFacade.UpdateContestOwnerVoteWinner (contest.ContestId, contest.PopularVoteWinnerEntryId);

            // Pay the Winner
            ContestEntry winner = GetContestFacade.GetContestEntry (contest.PopularVoteWinnerEntryId);

            int successfulPayment = 0;
            try                         
            {
                successfulPayment = GetContestFacade.MakeContestPayment (winner.Owner.UserId, winner.ContestId, "KPOINT", Convert.ToDouble (contest.PrizeAmount), (int) TransactionType.eTRANSACTION_TYPES.CASH_TT_CONTEST, (int) TransactionType.eCONTEST_TRANSACTION_TYPES.CONTEST_TT_WINNER);
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error adjusting user balance for winning ContestId " + contest.ContestId.ToString () + ". ", ex);
                return;
            }

            if (successfulPayment > 0)
            {
                m_logger.Info ("ContestId " + contest.ContestId.ToString () + " : successful payment to winner.  Entry Id " + winner.ContestEntryId.ToString () + ".  UserId " + winner.Owner.UserId.ToString ());
        
                // Update the contest status
                GetContestFacade.UpdateContestStatus (contest.ContestId, (int) eCONTEST_STATUS.Ended_Winner_Paid);
                m_logger.Info ("ContestId " + contest.ContestId.ToString () + " status set to " + ((int) eCONTEST_STATUS.Ended_Winner_Paid).ToString () + " (Ended_Winner_Paid).");
        
                // Send Emails
                // Used to hold user ids that have have been sent emails so we don't duplicate
                HashSet<int> ids = new HashSet<int> ();
                
                // Have to call get contest to load the contes entries via proxy
                contest = GetContestFacade.GetContest (contest.ContestId);
                // Send email to all entrants including winner
                if (contest.ContestEntries.Count > 0)
                {
                    foreach (ContestEntry entry in contest.ContestEntries)
                    {
                        try
                        {
                            if (entry.ContestEntryId == winner.ContestEntryId)
                            {
                                // Send email to winner
                                SendMessage (eCONTEST_MSG_TYPES.You_Won, contest, entry.Owner.UserId);
                            }
                            else
                            {
                                SendMessage (eCONTEST_MSG_TYPES.Entered_Did_Not_Win, contest, entry.Owner.UserId);
                            }

                            ids.Add (entry.Owner.UserId);
                        }
                        catch { }
                    }
                }

                // Send email to user who voted for winner
                IList<ContestUser> voters = GetContestFacade.GetContestEntryVoters (winner.ContestEntryId, "", "");
                if (voters.Count > 0)
                {
                    foreach (ContestUser voter in voters)
                    {
                        try
                        {
                            if (!ids.Contains (voter.UserId))
                            {
                                SendMessage (eCONTEST_MSG_TYPES.Voted_On_Winner, contest, voter.UserId);
                                ids.Add (voter.UserId);
                            }
                        }
                        catch { }
                    }
                }

                // Send email to followers
                IList<ContestUser> followers = GetContestFacade.GetContestFollowers (winner.ContestId, "", "");
                if (followers.Count > 0)
                {
                    foreach (ContestUser follower in followers)
                    {
                        try
                        {
                            if (!ids.Contains (follower.UserId))
                            {
                                SendMessage (eCONTEST_MSG_TYPES.Voted_On_Winner, contest, follower.UserId);
                            }
                        }
                        catch { }
                    }
                }

                // Send blast of winner
                GetBlastFacade.SendContestWinnerBlast (winner.Owner.UserId,
                    "/mykaneva/contests.aspx?contestId=" + contest.ContestId.ToString (),
                    contest.Title, contest.PrizeAmount, winner.Owner.Username,
                    winner.Owner.Username, winner.Owner.ThumbnailSmallPath);
            }
        }

        private void SendMessage (eCONTEST_MSG_TYPES type, Contest contest, int toUserId)
        {
            string subj = "";
            string msg = "";
            int fromUserId = 1;

            if (GetContestFacade.GetContestMessageDetails (type, contest, Configuration.SiteName, ref subj, ref msg))
            {
                // Insert the message
                Kaneva.BusinessLayer.BusinessObjects.Message message = new Kaneva.BusinessLayer.BusinessObjects.Message (0, fromUserId, toUserId, subj,
                    msg, new DateTime (), 0, (int) eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                GetUserFacade.InsertMessage (message);

                User userTo = GetUserFacade.GetUser (toUserId);
                if (userTo.UserId > 0)
                {
                    if (Convert.ToInt32 (userTo.NotifyAnyoneMessages).Equals (1))
                    {
                        // They want emails for all messages
                        SendPrivateMessageNotificationEmail (userTo.Email, message.Subject, message.MessageId, message.MessageText, userTo.UserId, fromUserId);
                    }
                }
            }
        }

        /// <summary>
        /// SendPrivateMessageNotificationEmail
        /// </summary>
        public static void SendPrivateMessageNotificationEmail (string toEmail, string strSubject, int messageId, string messageText, int toUserId, int fromUserId)
        {
            // store subject and message as overload fields		
            int overloadId = 0;
            overloadId = Queuer.AddOverloadField (overloadId, "#PMSubject#", strSubject);
            overloadId = Queuer.AddOverloadField (overloadId, "#PMMessageText#", messageText);

            //if overloadid did not work don't send the email.
            if (overloadId != 0)
            {
                int typeId = EmailTypeUtility.GetTypeIdByName ("PrivateMessage");
                string strMessageId = messageId.ToString ();
                TemplateSherpa.DressAndSendTemplate (typeId, 0, toEmail, 0, fromUserId, toUserId, "", 0, strMessageId, "", 0, overloadId);
            }
        }

        private void Contest_Load (object sender, EventArgs e)
        {
            RunContestService ();

            this.Dispose ();
            this.Close ();
        }

        private ContestFacade GetContestFacade
        {
            get
            {
                if (contestFacade == null)
                {
                    contestFacade = new ContestFacade ();
                }
                return contestFacade;
            }
        }

        private UserFacade GetUserFacade
        {
            get
            {
                if (userFacade == null)
                {
                    userFacade = new UserFacade ();
                }
                return userFacade;
            }
        }

        private BlastFacade GetBlastFacade
        {
            get
            {
                if (blastFacade == null)
                {
                    blastFacade = new BlastFacade ();
                }
                return blastFacade;
            }
        }
    }
}
