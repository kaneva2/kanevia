///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿namespace Contests
{
    partial class Contests
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose ();
            }
            base.Dispose (disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            //this.components = new System.ComponentModel.Container ();
            //this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            //this.Text = "Form1";
            this.label1 = new System.Windows.Forms.Label ();
            this.SuspendLayout ();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point (97, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size (111, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Processing Contests ...";
            // 
            // FameDaily
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF (6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size (337, 106);
            this.Controls.Add (this.label1);
            this.Name = "Contests";
            this.Text = "Contests Service";
            this.Load += new System.EventHandler (this.Contest_Load);
            this.ResumeLayout (false);
            this.PerformLayout ();
        }

        private System.Windows.Forms.Label label1;

        #endregion
    }
}

