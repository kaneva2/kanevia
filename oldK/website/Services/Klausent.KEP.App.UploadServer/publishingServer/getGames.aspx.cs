///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Klausent.KEP.App.PublishingServer
{
	/// <summary>
	/// Summary description for uploadInit.
	/// </summary>
	public partial class getGames : System.Web.UI.Page
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(getGames));

		private static readonly SessionManager sessionManager = SessionManager.GetInstance();

		String	_ticket = null;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			ReturnGameList();
		}

		private void ReturnGameList()
		{
			bool validRequest = ValidateRequest();
			if(!validRequest)
			{
				try
				{
					log.Warn("invalid request received");
					WriteInvalidRequest();
				}
				catch(ThreadAbortException)
				{
					//Response.End() thorws ThreadAbortException
				}
				return;
			}

			try
			{
				int user_id = sessionManager.AuthorizeTicket(ref _ticket);
				if ( user_id != 0)
				{
					//return a list of gameids, encryption keys, and dev licenses.
                    string sort = null;
                    if (Request["sort"] != null)
                    {
                        if (Request["sort"] == "date")
                            sort = "game_creation_date DESC";
                        else if (Request["sort"] == "name")
                            sort = "game_name";
                    }

					DataTable games = StoreUtility.GetAvailableGames(user_id,sort);
					ReturnSuccessfulCode(games);
				}
				else
				{
					WriteErrorTicketNotAuthorized();
					return;
				}
			}
			catch(ThreadAbortException)
			{
				//Response.End() thorws ThreadAbortException
			}
			catch(Exception e)
			{
				log.Error("failed to get game list", e);

				WriteException();
				return;
			}
		}

		/// <summary>
		/// validate the request params
		/// </summary>
		/// <returns></returns>
		private bool ValidateRequest()
		{
			bool validRequest = true;
			try
			{
				_ticket = Request.Params[Constants.PARAM_UPLOAD_INIT_TICKET];
				if(_ticket == null || _ticket.Length == 0) 
				{
					validRequest = false;
				}
			}
			catch(Exception)
			{
				validRequest = false;
			}

			return validRequest;
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_INVALID_REQUEST to the response
		/// </summary>
		private void WriteInvalidRequest()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_INVALID_REQUEST);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_GET_GAMES to the response
		/// </summary>
		private void WriteException()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_GET_GAMES);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_TICKET_NOT_AUTHORIZED to the response
		/// </summary>
		private void WriteErrorTicketNotAuthorized()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_TICKET_NOT_AUTHORIZED);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		private void ReturnSuccessfulCode(DataTable games)
		{	
            GameFacade gameFacade = new GameFacade();

			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.SUCCESS);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("<games>");
			foreach(DataRow row in games.Rows)
			{
                Response.Write("<game name=\"" + row["game_name"] + "\" id=\"" + row["game_id"] + "\" encryptionKey=\"" +
                    row["game_encryption_key"] + "\" parentId=\"" + row["parent_game_id"] + "\"");

                // find the highest level key for them (as of 2/08, instead of returning all)
                String serverKey = "";
                int licType = 0;

                try
                {
                    GameLicense gameLicense = gameFacade.GetGameLicenseByGameId(Convert.ToInt32(row["game_id"]));
                    serverKey = gameLicense.GameKey;
                    licType = gameLicense.LicenseSubscriptionId;
                }
                catch (Exception exc)
                {
                    log.Error("Failed to get license",exc);
                }

                //Constants.eLICENSE_SUBSCRIPTIONS licType = Constants.eLICENSE_SUBSCRIPTIONS.ANONYMOUS;
              
                //DataTable commLicenses = StoreUtility.GetCommercialLicenses(int.Parse(row["game_id"].ToString()));
                //if (commLicenses != null && commLicenses.Rows.Count > 0)
                //{
                //    serverKey = (String)commLicenses.Rows[0]["game_key"];
                //    licType = Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL;
                //}


                //if (serverKey.Length == 0)
                //{
                //    DataTable pilotLicenses = StoreUtility.GetPilotLicenses(int.Parse(row["game_id"].ToString()));
                //    if (pilotLicenses != null && pilotLicenses.Rows.Count > 0)
                //    {
                //        serverKey = (String)pilotLicenses.Rows[0]["game_key"];
                //        licType = Constants.eLICENSE_SUBSCRIPTIONS.PILOT;
                //    }
                //}

                //if (serverKey.Length == 0)
                //{
                //    DataTable devLicenses = StoreUtility.GetDevLicenses(int.Parse(row["game_id"].ToString()));
                //    if (devLicenses != null && devLicenses.Rows.Count > 0)
                //    {
                //        serverKey = (String)devLicenses.Rows[0]["game_key"];
                //        licType = Constants.eLICENSE_SUBSCRIPTIONS.DEVELOPER;
                //    }
                //}
                Response.Write( " serverKey=\"" + serverKey + "\" licType=\"" + licType.ToString() + "\">");

				Response.Write("</game>");
			}
			Response.Write("</games>\r\n");
			Response.Write("</kaneva>");
			Response.End();
			return;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
