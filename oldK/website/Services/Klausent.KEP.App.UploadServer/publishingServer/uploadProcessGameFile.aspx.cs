///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.UI;
using Klausent.KEP.App.PublishingServer.Domain.Game;
using Klausent.KEP.App.PublishingServer.Service.Game;
using Klausent.KEP.App.PublishingServer.Utils;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace Klausent.KEP.App.PublishingServer
{
	/// <summary>
	/// Summary description for uploadProcessGameFile.
	/// </summary>
	public partial class uploadProcessGameFile : Page
	{
		private static readonly ILog log = LogManager.GetLogger(typeof (uploadProcessGameFile));

		private static readonly SessionManager sessionManager = SessionManager.GetInstance();
		private static readonly GameUploadManager gameUploadManager = GameUploadManager.GetInstance();

		String _ticket = null;
		int _gameUploadId = -1;
		string _relativePath = null;
		string _filename = null;
		long _startByte = -1;
		long _endByte = -1;
		String _requestCrc = null;

		protected void Page_Load(object sender, EventArgs e)
		{
			// Put user code to initialize the page here
			InitiateUpload();
		}

		private void InitiateUpload()
		{
			//validate request
			bool validRequest = ValidateRequest();
			if (!validRequest)
			{
				try
				{
					log.Warn("invalid request received");
					WriteErrorInvalidRequest();
				}
				catch (ThreadAbortException)
				{
				}
				return;
			}

			try
			{
				int user_id = sessionManager.AuthorizeTicket(ref _ticket);
				if (user_id != 0)
				{
					GameUploadFileList gameUploadFileList = gameUploadManager.Get(_gameUploadId);

					if (gameUploadFileList == null)
					{
						//this should not happen if client functions correctly
						WriteErrorInvalidRequest();
						return;
					}
					else if(gameUploadFileList.PublishStatusId == 
						(int) Constants.eGAME_UPLOAD_PUBLISH_STATUS.UPLOADED)
					{
						WriteResponse((int) Constants.ePS_RETURNCODES.GAME_UPLOADED, _ticket);
						return;
					}
					else
					{
						//check if user owns the game
						if (user_id != gameUploadFileList.OwnerId)
						{
							WriteNotOwner();
							return;
						}
						else
						{
							string fullName = _relativePath == null ? "" : _relativePath;
							if(!fullName.EndsWith(Constants.GAME_UPLOAD_DIR_SEPARATOR))
							{
								fullName += Constants.GAME_UPLOAD_DIR_SEPARATOR;
							}
							fullName += _filename;

							GameUploadFile gameUploadFile = gameUploadFileList.Get(fullName);

							if (gameUploadFile == null)
							{
								//file not in the list of files to be uploaded
								WriteFileNotInList();
								return;
							}
							else
							{
								WriteFile(gameUploadFileList, gameUploadFile);
								return;
							}
						}
					}
				}
				else
				{
					WriteResponse((int) Constants.ePS_RETURNCODES.ERROR_TICKET_NOT_AUTHORIZED, null);
					return;
				}
			}
			catch (ThreadAbortException)
			{
				//Response.End() thorws ThreadAbortException
			}
			catch (Exception e)
			{
				log.Error("failed to save chunk", e);
				WriteResponse((int) Constants.ePS_RETURNCODES.EXCEPTION_UPLOAD, _ticket);
				return;
			}
		}

		/// <summary>
		/// validate request params
		/// </summary>
		/// <returns></returns>
		private bool ValidateRequest()
		{
			bool validRequest = true;
			try
			{
				_ticket = Request.Params[Constants.PARAM_UPLOAD_PROCESS_TICKET];
				_gameUploadId = int.Parse(Request.Params[Constants.PARAM_UPLOAD_PROCESS_GAME_UPLOAD_ID]);

				_relativePath = Request.Params[Constants.PARAM_UPLOAD_PROCESS_GAME_RELATIVE_PATH];
				_filename = Request.Params[Constants.PARAM_UPLOAD_PROCESS_GAME_FILE_NAME];

				_startByte = long.Parse(Request.Params[Constants.PARAM_UPLOAD_PROCESS_START_BYTE]);
				_endByte = long.Parse(Request.Params[Constants.PARAM_UPLOAD_PROCESS_END_BYTE]);
				_requestCrc = Request.Params[Constants.PARAM_UPLOAD_PROCESS_CRC];

				if (_ticket == null || _ticket.Length == 0 ||
					_requestCrc == null || _requestCrc.Length == 0)
				{
					validRequest = false;
				}
			}
			catch (Exception)
			{
				validRequest = false;
			}

			return validRequest;
		}

		private void WriteFile(GameUploadFileList gameUploadFileList, GameUploadFile gameUploadFile)
		{
			HttpFileCollection uploadedFiles;
			uploadedFiles = Request.Files; // Load File collection into HttpFileCollection variable.
			HttpPostedFile file = uploadedFiles.Get(Constants.PARAM_UPLOAD_PROCESS_DATA);
			Stream uploadedChunkStream = file.InputStream;

			bool crcMatched = CheckCRC(uploadedChunkStream, _requestCrc);
			if (crcMatched)
			{
				//crc verification passed
				FileInfo fileInfo = new FileInfo(gameUploadFile.GetSavedPath());
				if (!fileInfo.Directory.Exists)
				{
					if (_startByte == 0)
					{
						//first chunk
						fileInfo.Directory.Create();
						//fileInfo.Create(); don't create here as it will lock the file
					}
					else
					{
						//invalid start byte
						log.Warn("invalid start byte, filename = " + gameUploadFile.GetSavedPath() +
							"\ntarget file doesn't exist on the server and requested start byte is a non-zero value: " + _startByte);
						WriteResponse((int) Constants.ePS_RETURNCODES.ERROR_INVALID_START_BYTE, _ticket);
						return;
					}
				}

				if (_startByte != 0 && fileInfo.Length != _startByte)
				{
					log.Warn("invalid start byte, filename = " + gameUploadFile.GetSavedPath() +
						"\nfile size on server = " + fileInfo.Length + " and requested start byte = " + _startByte);
					WriteResponse((int) Constants.ePS_RETURNCODES.ERROR_INVALID_START_BYTE, _ticket);
					return;
				}
				else
				{
					//start saving
					gameUploadFile.WriteChunk(uploadedChunkStream);
					if (_endByte + 1 == gameUploadFile.Size)
					{
						//received the full file, not check md5
						gameUploadFile.CloseFileHandle();
						if (gameUploadFile.CheckMD5())
						{
							//file is successfully uploaded
							if (log.IsInfoEnabled)
							{
								log.Info("file uploaded, path = " + gameUploadFile.GetSavedPath());
							}
							gameUploadFile.ResultId = (int) Constants.eGAME_FILE_UPLOAD_RESULT.UPLOADED;
							gameUploadManager.Save((gameUploadFileList));
							if(gameUploadFileList.IsUploaded())
							{
								//this whole game is uploaded
								//update asset upload record
								if(log.IsInfoEnabled)
								{
									log.Info("game uploaded, gameId = " + gameUploadFileList.GameId + 
										" gameUploadId = " + gameUploadFileList.GameUploadId);
								}
								gameUploadFileList.PublishStatusId = 
									(int) Constants.eGAME_UPLOAD_PUBLISH_STATUS.UPLOADED;
								StoreUtility.UpdateGameUploadStatus(gameUploadFileList.GameUploadId,
									(int) Constants.eGAME_UPLOAD_PUBLISH_STATUS.UPLOADED,
									Request.UserHostAddress);
								WriteResponse((int) Constants.ePS_RETURNCODES.GAME_UPLOADED, _ticket);
							}
							else
							{
								WriteResponse((int) Constants.ePS_RETURNCODES.SUCCESS, _ticket);
							}
						}
						else
						{
							gameUploadFile.ClearUploaded();
							log.Info("MD5 checking failed, filename = " + gameUploadFile.GetSavedPath());
							WriteResponse((int) Constants.ePS_RETURNCODES.ERROR_MD5_ERROR, _ticket);
						}
					}
					else
					{
						//more data to send
						WriteResponse((int) Constants.ePS_RETURNCODES.SUCCESS, _ticket);
						return;
					}
				}
			}
			else
			{
				log.Info("CRC checking failed, filename = " + gameUploadFile.GetSavedPath());
				WriteResponse((int) Constants.ePS_RETURNCODES.ERROR_CRC_ERROR, _ticket);
				return;
			}
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_USER_NOT_OWNER to the response
		/// </summary>
		private void WriteNotOwner()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition", "inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int) Constants.ePS_RETURNCODES.ERROR_USER_NOT_OWNER);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_FILE_NOT_IN_LIST to the response
		/// </summary>
		private void WriteFileNotInList()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition", "inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int) Constants.ePS_RETURNCODES.ERROR_FILE_NOT_IN_LIST);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		private void WriteErrorInvalidRequest()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition", "inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int) Constants.ePS_RETURNCODES.ERROR_INVALID_REQUEST);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		/// <summary>
		/// check see if crc32 of data received matches the crc sent from the client
		/// </summary>
		/// <param name="uploadedChunkStream"></param>
		/// <param name="requestCrc"></param>
		/// <returns></returns>
		private bool CheckCRC(Stream uploadedChunkStream, string requestCrc)
		{
			byte[] data = new byte[uploadedChunkStream.Length];

			int offset = 0;
			int remaining = data.Length;
			while (remaining > 0)
			{
				int read = uploadedChunkStream.Read(data, offset, remaining);
				remaining -= read;
				offset += read;
			}

			string crcCal = HashCalculator.GetCRC32(data);
			return crcCal.Equals(requestCrc);
		}


		private void WriteResponse(int result, string ticket)
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition", "inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write(result);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(ticket == null ? "" : ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new EventHandler(this.Page_Load);
		}

		#endregion
	}
}
