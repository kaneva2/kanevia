///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Klausent.KEP.App.PublishingServer.Domain.Asset;
using Klausent.KEP.App.PublishingServer.Service.Asset;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace Klausent.KEP.App.PublishingServer
{
	/// <summary>
	/// Summary description for uploadInit.
	/// </summary>
	public partial class uploadInit : System.Web.UI.Page
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(uploadInit));

		private static readonly SessionManager sessionManager = SessionManager.GetInstance();
		private static readonly UploadItemManager uploadItemManager = UploadItemManager.GetInstance();
		private static readonly Settings settings = Settings.GetInstance();

		String	_ticket = null;
		String	_filename = null;
		String	_hash = null;
		long	_size = 0;
		int		_typeId = 0;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			InitiateUpload();
		}

		private void InitiateUpload()
		{
			bool validRequest = ValidateRequest();
			if(!validRequest)
			{
				try
				{
					log.Warn("invalid request received");
					WriteInvalidRequest();
				}
				catch(ThreadAbortException)
				{
					//Response.End() thorws ThreadAbortException
				}
				return;
			}

			try
			{
				int user_id = sessionManager.AuthorizeTicket(ref _ticket);
				if ( user_id != 0)
				{
					int assetId = 0;
					//check if asset is marked to be deleted
					if(StoreUtility.CheckAssetMarkedForDeletion(_hash, user_id))
					{
						//asset with same info hash is marked for deletion, user can not republish until
						//TCC cleans it up
						WriteErrorFileDeleted();
						return;
					}else if(StoreUtility.CheckAssetUploadFileExist(_hash))
					{
						//see if existing file has same hash and file name
						assetId = StoreUtility.GetUploadingAssetUploadId(user_id, _filename, _hash);
						
						if(assetId <= 0)
						{
							//existing file doesn't quality for resuming
							log.Warn("User requested to upload a file that already exists on the server, user_id = " + 
								user_id +" _hash = " + _hash + " filename = " + _filename);
							//same name file already exists
							WriteErrorFileExists();
							return;
						}
						else
						{
							UploadItem uploadItem = uploadItemManager.Get(assetId);
							if (uploadItem.AssetStatusId == (int) Constants.eASSET_STATUS.DELETED || 
								uploadItem.AssetStatusId == (int) Constants.eASSET_STATUS.MARKED_FOR_DELETION)
							{
								//file deleted
								uploadItem.CloseFileHandle();
								WriteErrorFileDeleted();
								return;
							}else if(uploadItem.TimeToFinish <= DateTime.Now)
							{
								//upload expired
								uploadItem.CloseFileHandle();
								WriteErrorUploadExpired();
								return;
							}
							else
							{
								if(log.IsInfoEnabled)
								{
									log.Info("Resuming uploadItem, userId = " + user_id +" filename = " + _filename);
								}
								ReturnSuccessfulCode(uploadItem);
								return;
							}
						}
					}
					else
					{
						//check file size
						int sizeLimit = StoreUtility.GetMaxUploadFileSize();
						if( ((long) sizeLimit) * 1024 * 1024 < _size)
						{
							WriteErrorMaxFileSizeExceeded(sizeLimit);
							return;
						}
						else
						{
							//create asset upload record
							CreateNewRecord(user_id);
							return;
						}
					}
				}
				else
				{
					WriteErrorTicketNotAuthorized();
					return;
				}
			}catch(ThreadAbortException)
			{
				//Response.End() thorws ThreadAbortException
			}catch(Exception e)
			{
				log.Error("failed to init upload", e);

				WriteException();
				return;
			}
		}

		/// <summary>
		/// validate the request params
		/// </summary>
		/// <returns></returns>
		private bool ValidateRequest()
		{
			bool validRequest = true;
			try
			{
				_ticket = Request.Params[Constants.PARAM_UPLOAD_INIT_TICKET];
				_filename = Request.Params[Constants.PARAM_UPLOAD_INIT_FILE_NAME];
				_hash = Request.Params[Constants.PARAM_UPLOAD_INIT_HASH];
				_size = long.Parse(Request.Params[Constants.PARAM_UPLOAD_INIT_SIZE]);
				_typeId = int.Parse(Request.Params[Constants.PARAM_UPLOAD_INIT_TYPE]);
				if(_ticket == null || _ticket.Length == 0 || 
					_filename == null || _filename.Length == 0 || 
					_hash == null || _hash.Length == 0 || 
					(_typeId != (int) Constants.eDS_INVENTORYTYPE.GAMES && _typeId != (int) Constants.eDS_INVENTORYTYPE.ASSETS))
				{
					validRequest = false;
				}
			}
			catch(Exception)
			{
				validRequest = false;
			}

			return validRequest;
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_UPLOAD_EXPIRED to the response
		/// </summary>
		private void WriteErrorUploadExpired()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_UPLOAD_EXPIRED);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
			
		}

		/// <summary>
		/// create new asset_upload and asset record
		/// </summary>
		/// <param name="user_id"></param>
		private void CreateNewRecord(int user_id)
		{
			//create a new asset
			string newDisplayName = _filename;
			int index = 2;

			// Make the item name unique
			while (StoreUtility.AssetNameAlreadyExists(newDisplayName, 0))
			{
				newDisplayName = _filename + " [" + index + "]";
				index ++;
			}

			int assetId = StoreUtility.InsertAsset ((int) Constants.eASSET_TYPE.VIDEO, (int)Constants.eASSET_SUBTYPE.ALL, newDisplayName, user_id, UsersUtility.GetUserNameFromId (user_id),
                (int)Constants.ePUBLISH_STATUS.UPLOADING, 0, (int)Constants.eASSET_RATING.GENERAL, 
                0, // category id
                "", // teaser
                "",  // instructions
                "", // company name  
                "" ); // offsite it
			
			if(assetId > 0)
			{
				int assetUploadId = StoreUtility.InsertAssetUpload(user_id, _filename, settings.UploadRepository, _size, _hash, _typeId, assetId);

				string newPath = Path.Combine(Path.Combine(settings.UploadRepository, user_id.ToString()), assetId.ToString());
				StoreUtility.UpdateAssetUploadPath(assetUploadId, newPath);

				UploadItem uploadItem = uploadItemManager.Get(assetId);
				if(log.IsInfoEnabled)
				{
					log.Info("Starting new uploadItem, userId = " + user_id +" filename = " + uploadItem.Path);
				}

				ReturnSuccessfulCode(uploadItem);
			}
			else
			{
				log.Error("Failed to insert db record user " + user_id + " filename = " + _filename);
				WriteServerError();
			}
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_MAX_FILE_SIZE_EXCEEDED to the response
		/// </summary>
		private void WriteErrorMaxFileSizeExceeded(int limit)
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_MAX_FILE_SIZE_EXCEEDED);
			Response.Write("</result>\r\n");
			Response.Write("<limit>");
			Response.Write(limit);
			Response.Write("</limit>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.EXCEPTION_INIT_UPLOAD to the response
		/// </summary>
		private void WriteException()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.EXCEPTION_INIT_UPLOAD);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.SERVER_ERROR to the response
		/// </summary>
		private void WriteServerError()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.SERVER_ERROR);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}


		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_INVALID_REQUEST to the response
		/// </summary>
		private void WriteInvalidRequest()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_INVALID_REQUEST);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}
		
		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_FILE_DELETED to the response
		/// </summary>
		private void WriteErrorFileDeleted()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_FILE_DELETED);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_FILE_EXISTS to the response
		/// </summary>
		private void WriteErrorFileExists()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_FILE_EXISTS);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_TICKET_NOT_AUTHORIZED to the response
		/// </summary>
		private void WriteErrorTicketNotAuthorized()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_TICKET_NOT_AUTHORIZED);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		private void ReturnSuccessfulCode(UploadItem uploadItem)
		{	
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.SUCCESS);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("<assetId>");
			Response.Write(uploadItem.AssetId);
			Response.Write("</assetId>\r\n");
			Response.Write("<startByte>");
			Response.Write(uploadItem.GetNextStartByte());
			Response.Write("</startByte>\r\n");
			Response.Write("</kaneva>");
			Response.End();
			return;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
