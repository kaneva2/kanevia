///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using Klausent.KEP.App.PublishingServer.Dao;
using Klausent.KEP.App.PublishingServer.Domain.Game;
using Klausent.KEP.App.PublishingServer.Service.Game;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace Klausent.KEP.App.PublishingServer
{
    /// <summary>
    /// Summary description for uploadInitGame.
    /// </summary>
    public partial class uploadInitGame : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger (typeof(uploadInitGame));

        private static readonly SessionManager sessionManager = SessionManager.GetInstance();
		private static readonly GameUploadManager gameUploadManager = GameUploadManager.GetInstance();
		private static readonly Settings settings = Settings.GetInstance();


        String  _ticket = null;
		int		_gameId = 0;
    	GameUploadFileList _fileList = null;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            InitiateUpload();
        }

		/// <summary>
		/// validate the request params
		/// </summary>
		/// <returns></returns>
		private bool ValidateRequest()
		{
			bool validRequest = true;
			try
			{
				_ticket = Request.Params[Constants.PARAM_UPLOAD_INIT_TICKET];
				_gameId = int.Parse(Request.Params[Constants.PARAM_UPLOAD_INIT_GAME_ID]);
				HttpPostedFile file = Request.Files[0];
				if(!(ParseFileList(file)) || _ticket == null || _ticket.Length == 0 || file == null) 
				{
					validRequest = false;
				}
			}
			catch(Exception)
			{
				validRequest = false;
			}

			return validRequest;
		}

		/// <summary>
		/// parse the posted xml file and generate upload file list
		/// </summary>
		/// <param name="postedFile"></param>
		/// <returns></returns>
		private bool ParseFileList(HttpPostedFile postedFile)
		{
			bool retVal = true;
			try
			{
				//parse input file into a xml file
				Stream stream = postedFile.InputStream;
				XmlTextReader txtReader = new XmlTextReader(stream);
				XmlValidatingReader reader = new XmlValidatingReader(txtReader);
				reader.ValidationType = ValidationType.DTD;
				XmlDocument doc = new XmlDocument();
				doc.Load(reader);

				XmlNodeList list = doc.ChildNodes;
				XmlNode node = list[0];
				_fileList = new GameUploadFileList() ;
				_fileList.GameId = _gameId;
				IList fileList = new ArrayList();
				foreach(XmlNode n in node.ChildNodes)
				{
					string path = n.InnerText.Trim();
					string dir = "";
					string filename = null;
					int index = path.LastIndexOf(Constants.GAME_UPLOAD_DIR_SEPARATOR);
					if(index >= 0)
					{
						dir = path.Substring(0, index);
						filename = path.Substring(index + 1);
						if(filename == null || filename.Length == 0)
						{
							log.Error(path + " does not contain a valid file name");
							return false;
						}
					}
					else
					{
						filename = path;
					}

					GameUploadFile gameUploadFile = new GameUploadFile() ;
					gameUploadFile.RelativePath = dir;
					gameUploadFile.Name = filename;
					fileList.Add(gameUploadFile);
				}
				_fileList.SetFiles(fileList);
			}catch(Exception e)
			{
				retVal = false;
				log.Error("Failed to parse initial manifest file", e);
			}
			return retVal;
		}

        private void InitiateUpload()
        {
            bool validRequest = ValidateRequest();
            if(!validRequest)
            {
                try
                {
                    log.Warn("invalid request received");
                    WriteInvalidRequest();
                }
                catch(ThreadAbortException)
                {
                    //Response.End() thorws ThreadAbortException
                }
                return;
            }

            try
            {
                int user_id = sessionManager.AuthorizeTicket(ref _ticket);
                if ( user_id != 0)
                {
					//check if user owns the game
					if (!StoreUtility.IsAssetOwner(user_id, _gameId))
					{
						WriteNotOwner();
						return;
					}else
					{
						//check if a previous upload is not yet processed
						if(StoreUtility.IsGameUploadBeingProcessed(_gameId))
						{
							WriteGameUploadBeingProcessed();
							return;
						}
						else
						{
							string savedPath = Path.Combine(Path.Combine(settings.UploadRepository, 
								user_id.ToString()), _gameId.ToString());
							//release file locked by previous upload
							IList currentUploads = gameUploadManager.GetByGameId(_gameId);
							foreach(GameUploadFileList gameUpload in currentUploads)
							{
								gameUpload.CloseFileHandles();
							}

							int assetUploadId = StoreUtility.InsertGameUpload(user_id, savedPath, _gameId);
							if(assetUploadId > 0)
							{
								this._fileList.GameUploadId = assetUploadId;
								this._fileList.SavedPath = savedPath;
								_fileList.OwnerId = user_id;
								gameUploadManager.Add(_fileList);
								ReturnSuccessfulCode();
								return;
							}
							else
							{
								this.WriteServerError();
								return;
							}
						}
					}
                }
                else
                {
                    WriteErrorTicketNotAuthorized();
                    return;
                }
            }catch(ThreadAbortException)
            {
                //Response.End() thorws ThreadAbortException
            }catch(Exception e)
            {
                log.Error("failed to init upload", e);

                WriteException();
                return;
            }
        }

        /// <summary>
        /// write Constants.ePS_RETURNCODES.EXCEPTION_INIT_UPLOAD to the response
        /// </summary>
        private void WriteException()
        {
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "text/xml";
            Response.AddHeader("Content-Disposition","inline;filename=result.xml");
            Response.Write("<kaneva>\r\n");
            Response.Write("<result>");
            Response.Write((int)Constants.ePS_RETURNCODES.EXCEPTION_INIT_UPLOAD);
            Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
            Response.Write("</kaneva>");
            Response.End();
        }
		
		/// <summary>
		/// write Constants.ePS_RETURNCODES.SERVER_ERROR to the response
		/// </summary>
		private void WriteGameUploadBeingProcessed()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_GAME_UPLOAD_BEING_PROCESSED);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

        /// <summary>
        /// write Constants.ePS_RETURNCODES.SERVER_ERROR to the response
        /// </summary>
        private void WriteServerError()
        {
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "text/xml";
            Response.AddHeader("Content-Disposition","inline;filename=result.xml");
            Response.Write("<kaneva>\r\n");
            Response.Write("<result>");
            Response.Write((int)Constants.ePS_RETURNCODES.SERVER_ERROR);
            Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
            Response.Write("</kaneva>");
            Response.End();
        }


        /// <summary>
        /// write Constants.ePS_RETURNCODES.ERROR_INVALID_REQUEST to the response
        /// </summary>
        private void WriteInvalidRequest()
        {
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "text/xml";
            Response.AddHeader("Content-Disposition","inline;filename=result.xml");
            Response.Write("<kaneva>\r\n");
            Response.Write("<result>");
            Response.Write((int)Constants.ePS_RETURNCODES.ERROR_INVALID_REQUEST);
            Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
            Response.Write("</kaneva>");
            Response.End();
        }

		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_USER_NOT_OWNER to the response
		/// </summary>
		private void WriteNotOwner()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_USER_NOT_OWNER);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

        /// <summary>
        /// write Constants.ePS_RETURNCODES.ERROR_TICKET_NOT_AUTHORIZED to the response
        /// </summary>
        private void WriteErrorTicketNotAuthorized()
        {
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "text/xml";
            Response.AddHeader("Content-Disposition","inline;filename=result.xml");
            Response.Write("<kaneva>\r\n");
            Response.Write("<result>");
            Response.Write((int)Constants.ePS_RETURNCODES.ERROR_TICKET_NOT_AUTHORIZED);
            Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
            Response.Write("</kaneva>");
            Response.End();
        }

        private void ReturnSuccessfulCode()
        {   
            Response.Clear();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "text/xml";
            Response.AddHeader("Content-Disposition","inline;filename=result.xml");
            Response.Write("<kaneva>\r\n");
            Response.Write("<result>");
            Response.Write((int)Constants.ePS_RETURNCODES.SUCCESS);
            Response.Write("</result>\r\n");
			Response.Write("<gameUploadId>");
			Response.Write(_fileList.GameUploadId);
			Response.Write("</gameUploadId>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
            Response.Write("</kaneva>");
            Response.End();
            return;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
        }
        #endregion
    }
}
