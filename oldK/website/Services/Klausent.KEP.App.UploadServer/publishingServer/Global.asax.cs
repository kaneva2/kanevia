///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Security.Principal;
using System.Web;
using System.Web.SessionState;
using Klausent.KEP.App.PublishingServer.Service.Asset;
using Klausent.KEP.App.PublishingServer.Service.Game;
using log4net;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Klausent.KEP.App.PublishingServer 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(Global));
		private static readonly GameUploadManager gameUploadManager = GameUploadManager.GetInstance();
		private static readonly UploadItemManager uploadItemManager = UploadItemManager.GetInstance();

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		public Global()
		{
			string s = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
			FileInfo l_fi = new FileInfo(s);
			log4net.Config.DOMConfigurator.Configure(l_fi);
			if(log.IsInfoEnabled)
			{
				log.Info("Setup log4net");
			}
			InitializeComponent();
		}	
		
		protected void Application_Start(Object sender, EventArgs e)
		{
			if(log.IsInfoEnabled)
			{
				log.Info("SERVICE STARTED");
			}
		}
 
		protected void Session_Start(Object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_Error(Object sender, EventArgs e)
		{

		}

		protected void Session_End(Object sender, EventArgs e)
		{

		}

		protected void Application_End(Object sender, EventArgs e)
		{
			CloseFileHandles();
			log.Fatal("SERVICE STOPPED");
		}

		/// <summary>
		/// close all file locks
		/// </summary>
		private void CloseFileHandles()
		{
			log.Info("Closing all asset upload file handles...");
			uploadItemManager.CloseAll();

			log.Info("Closing all game upload file handles...");
			gameUploadManager.CloseAll();
		}

			
		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

namespace KlausEnt.KEP.Kaneva 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		private static DatabaseUtility m_DatabaseUtilty;

		/// <summary>
		/// Get the database utility
		/// </summary>
		/// <returns></returns>
		public static DatabaseUtility GetDatabaseUtility ()
		{
			if (m_DatabaseUtilty == null)
			{
				switch (KanevaGlobals.DatabaseType)
				{
					case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
					{
						m_DatabaseUtilty = new SQLServerUtility (KanevaGlobals.ConnectionString);
						break;
					}
					case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
					{
						m_DatabaseUtilty = new MySQLUtility (KanevaGlobals.ConnectionString);
						break;
					}
					default:
					{
						throw new Exception ("This database type is not supported");
					}
				}
			}
			return m_DatabaseUtilty;
		}

		public Global()
		{
			InitializeComponent();
		}	
		
		protected void Application_Start(Object sender, EventArgs e)
		{
			
		}
 
		protected void Session_Start(Object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{
            if (Request.IsAuthenticated)
            {
                // Load user data
                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(User.Identity.Name);
                HttpContext.Current.Items["User"] = user;

                ArrayList ar = UsersUtility.GetUserRoleArray(user.Role);
                string[] roles = (string[])ar.ToArray(Type.GetType("System.String"));

                HttpContext.Current.User = new GenericPrincipal(User.Identity, roles);
            }
            else
            {
                User user = new User();
                HttpContext.Current.Items["User"] = user;
            }
		}

		protected void Application_Error(Object sender, EventArgs e)
		{
			
		}

		protected void Session_End(Object sender, EventArgs e)
		{

		}

		protected void Application_End(Object sender, EventArgs e)
		{

		}
			
		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

