///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Web.Security;
using log4net;

namespace Klausent.KEP.App.PublishingServer
{
	/// <summary>
	/// Summary description for SessionManager.
	/// </summary>
	public class SessionManager
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(SessionManager));
		private static SessionManager inst;

		protected IDictionary ticketTable;

		private SessionManager()
		{
			//
			// TODO: Add constructor logic here
			//
			ticketTable = new Hashtable();
		}

		public static SessionManager GetInstance()
		{
			if (inst == null)
			{
				inst = new SessionManager() ;
			}
			return inst;
		}

		/// <summary>
		/// Login to Kaneva and obtain a ticket for future requests.
		/// </summary>
		/// <param name="username">User name.</param>
		/// <param name="password">Password.</param>
		/// <param name="ticket">Issued ticket to use on subsequent service calls.</param>
		/// <returns>True if successful, false otherwise.</returns>
		public int Login( string username, string password, ref string ticket)
		{
			int roleMembership = 0;
			int validLogin = 0;
			int ret = 0;

			try
			{
				System.Data.DataRow dr = KlausEnt.KEP.Kaneva.UsersUtility.GetUserByEmail(username);
                int user_id = 0;
                if (dr != null )
                    user_id = Convert.ToInt32(dr["user_id"]);
				validLogin = KlausEnt.KEP.Kaneva.UsersUtility.Authorize( username, password, 0, ref roleMembership, "", false);

				if (validLogin == (int) KlausEnt.KEP.Kaneva.Constants.eLOGIN_RESULTS.SUCCESS)
				{
					ticket = genTicket(user_id);
					if(log.IsInfoEnabled)
					{
						log.Info("Log in successful, username = " + username);
					}
					ret = (int) KlausEnt.KEP.Kaneva.Constants.ePS_RETURNCODES.SUCCESS;
				} 
				else
				{
					if(log.IsInfoEnabled)
					{
						log.Info("Log in failed, username = " + username);
					}
					ret = (int) KlausEnt.KEP.Kaneva.Constants.ePS_RETURNCODES.LOGIN_NOT_VALIDATED;
				}
			}
			catch (Exception e)
			{
				log.Error("Error logging in, username = " + username, e);
				ret = (int) KlausEnt.KEP.Kaneva.Constants.ePS_RETURNCODES.ERROR_LOGIN;
			}

			return ret;
		}

		/// <summary>
		/// Generates a new ticket and adds it to the in memory ticket table.
		/// </summary>
		/// <param name="user_id">The user_id associated with the user. Used for utility.</param>
		/// <returns>Returns the new ticket string.</returns>
		private string genTicket(int user_id)
		{
			string encTicket = null;

			try
			{
				lock ( ticketTable.SyncRoot)
				{
					Guid guid = System.Guid.NewGuid();
					FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
						1,
						guid.ToString(),
						System.DateTime.Now,
						System.DateTime.Now.AddMinutes(20),
						false,
						user_id.ToString(),
						"");

					// Encrypt the ticket.
					encTicket = FormsAuthentication.Encrypt(ticket);

					ticketTable.Add( guid.ToString(), encTicket);
				}
			}
			catch (Exception e)
			{
				log.Error("Exception generating ticket", e);
			}

			return encTicket;
		}

		/// <summary>
		/// Determines if the given ticket is valid. It has not expired and it exists in
		/// the memory ticket table. If valid, issues a new ticket.
		/// </summary>
		/// <param name="ticket">The ticket to validate.</param>
		/// <returns>Returns the user_id associated with the ticket if valid. Zero otherwise.</returns>
		public int AuthorizeTicket( ref string ticket)
		{
			int user_id = 0;
			try
			{
				lock ( ticketTable.SyncRoot)
				{

					FormsAuthenticationTicket decTicket = FormsAuthentication.Decrypt(ticket);

					if ( ( ! decTicket.Expired) &&
						ticketTable.Contains(decTicket.Name))
					{
						user_id = Int32.Parse(decTicket.UserData);
//						ticket = genTicket(user_id);
					}

//					ticketTable.Remove(decTicket.Name);
				}
			}
			catch (Exception e)
			{
				log.Error("Error authorizing ticket",e);
			}

			if ( user_id != 0)
			{
				if(log.IsDebugEnabled)
				{
					log.Debug("ticket authorized, userID = " + user_id);
				}
			} 
			else
			{
				log.Warn("ticket not authorized");
			}

			return user_id;
		}
	}
}
