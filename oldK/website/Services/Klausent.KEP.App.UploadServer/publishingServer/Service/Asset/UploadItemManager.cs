///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Timers;
using Klausent.KEP.App.PublishingServer.Domain.Asset;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace Klausent.KEP.App.PublishingServer.Service.Asset
{
	/// <summary>
	/// Summary description for UploadItemManager.
	/// </summary>
	public class UploadItemManager
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(UploadItemManager));
		private static readonly Settings settings = Settings.GetInstance();

		private IDictionary uploadItems;
		private static UploadItemManager inst;

		System.Timers.Timer _idleSessionsTimer = new Timer();

		private UploadItemManager()
		{
			//
			// TODO: Add constructor logic here
			//
			uploadItems = new Hashtable();

			_idleSessionsTimer.Enabled = true;
			_idleSessionsTimer.Interval = (1000 * settings.IdleTimeout);

			_idleSessionsTimer.Elapsed += new System.Timers.ElapsedEventHandler (IdleSessionsTimer_Elapsed);
		}

		public static UploadItemManager GetInstance()
		{
			if(inst == null)
			{
				inst = new UploadItemManager() ;
			}
			return inst;
		}

		public void Add(UploadItem uploadItem)
		{
			lock(uploadItems.SyncRoot)
			{
				uploadItems.Add(uploadItem.AssetId, uploadItem);
			}
		}

		/// <summary>
		/// get upload item by asset id
		/// </summary>
		/// <param name="assetId"></param>
		/// <param name="loadIfNotPresent">true to laod from db is not found in cache</param>
		/// <returns></returns>
		public UploadItem Get(int assetId, bool loadIfNotPresent)
		{
			UploadItem retVal = (UploadItem) uploadItems[assetId];
			if(retVal == null && loadIfNotPresent)
			{
				retVal = GetUploadItemFromDb(assetId);
				Add(retVal);
			}
			return retVal;
		}

		public UploadItem Get(int assetId)
		{
			return Get(assetId, true);
		}

		/// <summary>
		/// refresh asset
		/// </summary>
		/// <param name="uploadItem"></param>
		public void Update(UploadItem uploadItem)
		{
			DataRow row = StoreUtility.GetAssetUploadByAssetId(uploadItem.AssetId);
			try
			{
				uploadItem.MD5Hash = row["MD5_hash"].ToString();
				uploadItem.Path = Path.Combine(row["path"].ToString(), row["filename"].ToString());
				uploadItem.Size = long.Parse(row["size"].ToString());
				uploadItem.UserId = int.Parse(row["owner_id"].ToString());
				uploadItem.TimeStarted = DateTime.Parse(row["created_datetime"].ToString());
				uploadItem.TimeToFinish = uploadItem.TimeStarted.AddSeconds(settings.UploadTimeLimit);
				uploadItem.AssetStatusId = int.Parse(row["status_id"].ToString());
			}
			catch(Exception e)
			{
				log.Error("Failed to update asset upload record form db", e);
			}
		}

		private UploadItem GetUploadItemFromDb(int assetId)
		{
			UploadItem uploadItem = null;
			DataRow row = StoreUtility.GetAssetUploadByAssetId(assetId);
			try{
				uploadItem = new UploadItem();
				uploadItem.AssetId = assetId;
				uploadItem.MD5Hash = row["MD5_hash"].ToString();
				uploadItem.Path = Path.Combine(row["path"].ToString(), row["filename"].ToString());
				uploadItem.Size = long.Parse(row["size"].ToString());
				uploadItem.UserId = int.Parse(row["owner_id"].ToString());
				uploadItem.TimeStarted = DateTime.Parse(row["created_datetime"].ToString());
				uploadItem.TimeToFinish = uploadItem.TimeStarted.AddSeconds(settings.UploadTimeLimit);
				uploadItem.AssetStatusId = int.Parse(row["status_id"].ToString());
			}catch(Exception e)
			{
				log.Error("Failed to retrieve asset upload record form db", e);
			}

			return uploadItem;
		}

		/// <summary>
		/// if a file has stopped uploading for certain amount of time, close the file handle
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void IdleSessionsTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) 
		{
			try
			{
				_idleSessionsTimer.Stop ();
				CleanIdleSessions();
			}
			catch (Exception exc)
			{
				log.Error ("Exception cleaning idle sessions", exc);
			}
			finally
			{
				_idleSessionsTimer.Start ();
			}
		}

		private void CleanIdleSessions()
		{
			DateTime now = DateTime.Now;
			IList itemsToRemove = new ArrayList();
			foreach(UploadItem item in this.uploadItems.Values)
			{
				if(now.Subtract(item.LastAccessed).Seconds >= settings.IdleTimeout)
				{
					itemsToRemove.Add(item);
				}
			}

			lock(uploadItems.SyncRoot)
			{
				foreach(UploadItem item in itemsToRemove)
				{
					if(log.IsInfoEnabled)
					{
						log.Info("removing " + item.Path + " last accessed " + item.LastAccessed);
					}
					item.CloseFileHandle();
					uploadItems.Remove(item.AssetId);
				}
			}
		}

		/// <summary>
		/// close all file handles, release the locking
		/// </summary>
		public void CloseAll()
		{
			foreach(UploadItem item in this.uploadItems.Values)
			{
				item.CloseFileHandle();
			}
		}
	}
}
