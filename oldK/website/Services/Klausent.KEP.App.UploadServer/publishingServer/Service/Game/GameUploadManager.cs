///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Timers;
using Klausent.KEP.App.PublishingServer.Dao;
using Klausent.KEP.App.PublishingServer.Domain.Game;
using Klausent.KEP.App.PublishingServer.Utils;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace Klausent.KEP.App.PublishingServer.Service.Game
{
	/// <summary>
	/// Summary description for UploadItemManager.
	/// </summary>
	public class GameUploadManager
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(GameUploadManager));
		private static readonly Settings settings = Settings.GetInstance();
		private static readonly GameUploadFileListDao gameUploadFileListDao = 
			GameUploadFileListDao.GetInstance();

		private IDictionary gameUploads;
		private static GameUploadManager inst;
		
		System.Timers.Timer _idleSessionsTimer = new Timer();

		private GameUploadManager()
		{
			//
			// TODO: Add constructor logic here
			//
			gameUploads = new Hashtable();

			_idleSessionsTimer.Enabled = true;
			_idleSessionsTimer.Interval = (1000 * settings.IdleTimeout);

			_idleSessionsTimer.Elapsed += new System.Timers.ElapsedEventHandler (IdleSessionsTimer_Elapsed);
		}

		public static GameUploadManager GetInstance()
		{
			if(inst == null)
			{
				inst = new GameUploadManager() ;
			}
			return inst;
		}

		public void Add(GameUploadFileList gameUploadFileList)
		{
			lock(gameUploads.SyncRoot)
			{
				gameUploads.Add(gameUploadFileList.GameUploadId, gameUploadFileList);
				gameUploadFileListDao.Save(gameUploadFileList,GetManifestFile(gameUploadFileList));
			}
		}

		/// <summary>
		/// get active game upload sessions by game id
		/// </summary>
		/// <param name="gameId"></param>
		/// <returns></returns>
		public IList GetByGameId(int gameId)
		{
			IList retVal = new ArrayList();
			foreach(GameUploadFileList gameUploadFileList in gameUploads.Values)
			{
				if(gameUploadFileList.GameId == gameId)
				{
					retVal.Add(gameUploadFileList);
				}
			}
			return retVal;
		}

		/// <summary>
		/// get upload item by asset id
		/// </summary>
		/// <param name="gameUploadId"></param>
		/// <returns></returns>
		public GameUploadFileList Get(int gameUploadId)
		{
			GameUploadFileList retVal = (GameUploadFileList) gameUploads[gameUploadId];
			if(retVal == null )
			{
				retVal = GetGameUploadFileListFromDb(gameUploadId);
				Add(retVal);
			}
			
			return retVal;
		}

		private GameUploadFileList GetGameUploadFileListFromDb(int gameUploadId)
		{
			GameUploadFileList fileList = null;
			DataRow row = StoreUtility.GetGameUpload(gameUploadId);
			try
			{
				fileList = new GameUploadFileList();
				fileList.GameUploadId = int.Parse(row["game_upload_id"].ToString()); 
				fileList.GameId = int.Parse(row["asset_id"].ToString());
				fileList.OwnerId = int.Parse(row["owner_id"].ToString());
				fileList.SavedPath = row["path"].ToString();
				fileList.PublishStatusId = int.Parse(row["publish_status_id"].ToString());
				
				//load file list
				GameUploadFileList loaded = gameUploadFileListDao.Load(GetManifestFile(fileList));
				//use gameid, gameuploadid from db, just get file list from manifest file
				fileList.GameUploadFiles = loaded.GameUploadFiles;
			}
			catch(Exception e)
			{
				log.Error("Failed to load game upload manifest", e);
			}

			return fileList;
		}

		private string GetManifestFile(GameUploadFileList fileList)
		{
			return Path.Combine(fileList.SavedPath, Constants.GAME_FILE_MANIFEST);
		}

		/// <summary>
		/// save gameUploadFileList object to xml file
		/// </summary>
		/// <param name="gameUploadFileList"></param>
		public void Save(GameUploadFileList gameUploadFileList)
		{
			gameUploadFileListDao.Save(gameUploadFileList,GetManifestFile(gameUploadFileList));
		}

		/// <summary>
		/// if a file has stopped uploading for certain amount of time, close the file handle
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void IdleSessionsTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) 
		{
			try
			{
				_idleSessionsTimer.Stop ();
				CleanIdleSessions();
			}
			catch (Exception exc)
			{
				log.Error ("Exception cleaning idle sessions", exc);
			}
			finally
			{
				_idleSessionsTimer.Start ();
			}
		}

		private void CleanIdleSessions()
		{
			DateTime now = DateTime.Now;
			foreach(GameUploadFileList gameFileList in this.gameUploads.Values)
			{
				foreach(GameUploadFile file in gameFileList.Files.Values)
				if(now.Subtract(file.LastAccessed).Seconds >= settings.IdleTimeout)
				{
					log.Info("File " + file.GetFullName() + 
						" timed out, closing file handle. gameId = " + gameFileList.GameId +
						" last accessed " + file.LastAccessed);
					file.CloseFileHandle();
				}
			}
		}

		/// <summary>
		/// close all file handles, release the locking
		/// </summary>
		public void CloseAll()
		{
			foreach(GameUploadFileList gameFileList in this.gameUploads.Values)
			{
				foreach(GameUploadFile file in gameFileList.Files.Values)
				{
					file.CloseFileHandle();
				}
			}
		}
	}
}
