///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace Klausent.KEP.App.PublishingServer
{
	/// <summary>
	/// Summary description for login.
	/// </summary>
	public partial class login : System.Web.UI.Page
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(login));

		private static readonly SessionManager sessionManager = SessionManager.GetInstance();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			Login();
		}

		private void Login()
		{
			String username = Request.Params[Constants.PARAM_LOGIN_USERNAME];
			String password = Request.Params[Constants.PARAM_LOGIN_PASSWORD];

			String ticket = null;
			int result = sessionManager.Login(username, password, ref ticket);
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write(result);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
