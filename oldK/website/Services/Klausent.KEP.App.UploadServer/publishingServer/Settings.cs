///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace Klausent.KEP.App.PublishingServer
{
	/// <summary>
	/// Summary description for AppSettings.
	/// </summary>
	public class Settings
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(Settings));

		private string	_uploadRepository;
		private int		_idleTimeout;
		private DateTime	_latestBuiltDate;
		private int		_uploadTimeLimit;

		private static Settings inst;
		private Settings()
		{
			//
			// TODO: Add constructor logic here
			//
			_uploadRepository = System.Configuration.ConfigurationSettings.AppSettings["upload_repository"];
			_idleTimeout = int.Parse(System.Configuration.ConfigurationSettings.AppSettings["idleSession"]);
			_latestBuiltDate = DateTime.Parse(System.Configuration.ConfigurationSettings.AppSettings["latestBuiltDate"].ToString());
			_uploadTimeLimit = StoreUtility.GetFileUploadTimeout() * 60 * 60;
		}

		public static Settings GetInstance()
		{
			if (inst == null)
			{
				inst = new Settings() ;
			}
			return inst;
		}

		public string UploadRepository
		{
			get { return _uploadRepository; }
			set { _uploadRepository = value; }
		}

		/// <summary>
		/// in seconds, before inactive upload items are removed
		/// </summary>
		public int IdleTimeout
		{
			get { return _idleTimeout; }
			set { _idleTimeout = value; }
		}

		/// <summary>
		/// build date of latest publishing client
		/// </summary>
		public DateTime LatestBuiltDate
		{
			get { return _latestBuiltDate; }
			set { _latestBuiltDate = value; }
		}

		/// <summary>
		/// time users have to complete the uploads, in seconds
		/// </summary>
		public int UploadTimeLimit
		{
			get { return _uploadTimeLimit; }
			set { _uploadTimeLimit = value; }
		}
	}
}
