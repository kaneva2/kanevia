///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Klausent.KEP.App.PublishingServer
{
	/// <summary>
	/// Summary description for versionChecker.
	/// </summary>
	public partial class versionChecker : System.Web.UI.Page
	{
		private static readonly Settings settings = Settings.GetInstance();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			ReturnVersion();
		}

		private void ReturnVersion()
		{
			try
			{
				Response.Clear();
				Response.Cache.SetCacheability(HttpCacheability.NoCache);
				Response.ContentType = "text/xml";
				Response.AddHeader("Content-Disposition","inline;filename=result.xml");
				Response.Write("<kaneva>\r\n");
				Response.Write("<month>");
				Response.Write(settings.LatestBuiltDate.Month);
				Response.Write("</month>\r\n");
				Response.Write("<day>");
				Response.Write(settings.LatestBuiltDate.Day);
				Response.Write("</day>\r\n");
				Response.Write("<year>");
				Response.Write(settings.LatestBuiltDate.Year);
				Response.Write("</year>\r\n");
				Response.Write("</kaneva>");
				Response.End();
			}
			catch(ThreadAbortException)
			{
				//Response.End() thorws ThreadAbortException
			}
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
