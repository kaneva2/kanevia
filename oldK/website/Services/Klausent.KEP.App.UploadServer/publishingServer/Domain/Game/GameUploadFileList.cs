///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Xml.Serialization;
using KlausEnt.KEP.Kaneva;

namespace Klausent.KEP.App.PublishingServer.Domain.Game
{
	/// <summary>
	/// Summary description for GameUploadFileList.
	/// </summary>
	[XmlRoot("gameUploadFileList")]
	public class GameUploadFileList
	{
		private int _gameUploadId;
		private int _gameId;
		private int _ownerId;
		private IDictionary _files;
		private string	_savedPath;
		private int _publishStatusId;
		public GameUploadFileList()
		{
			_files = new Hashtable();
		}

		public void SetFiles(IList files)
		{
			
			foreach(GameUploadFile file in files)
			{
				AddFile(file);
			}
		}

		public void AddFile(GameUploadFile file)
		{
			lock(_files.SyncRoot)
			{
				file.GameUploadFileList = this;
				_files.Add(file.GetFullName(), file);
			}
		}

		/// <summary>
		/// get file to upload by full path (RELATIVE_PATH\FILE_NAME)
		/// </summary>
		/// <param name="fullPath"></param>
		/// <returns></returns>
		public GameUploadFile Get(string fullPath)
		{
			return (GameUploadFile) _files[fullPath];
		}

		/// <summary>
		/// returns true if every file that needs to be uploaded is uploaded
		/// </summary>
		/// <returns></returns>
		public bool IsUploaded()
		{
			bool retVal = true;
			foreach(GameUploadFile file in _files.Values)
			{
				if (file.ResultId == (int) Constants.eGAME_FILE_UPLOAD_RESULT.NOT_UPLOADED ||
					file.ResultId == (int) Constants.eGAME_FILE_UPLOAD_RESULT.ERROR)
				{
					retVal = false;
					break;
				}

			}
			return retVal;
		}

		/// <summary>
		/// close all file handles
		/// </summary>
		public void CloseFileHandles()
		{
			foreach(GameUploadFile file in _files.Values)
			{
				file.CloseFileHandle();
			}
		}

		/// <summary>
		/// a unique id of this batch upload
		/// </summary>
		public int GameUploadId
		{
			get { return _gameUploadId; }
			set { _gameUploadId = value; }
		}

		/// <summary>
		/// asset id of the game
		/// </summary>
		public int GameId
		{
			get { return _gameId; }
			set { _gameId = value; }
		}

		/// <summary>
		/// owner of the game
		/// </summary>
		public int OwnerId
		{
			get { return _ownerId; }
			set { _ownerId = value; }
		}

		/// <summary>
		/// where the uploaded files are saved
		/// </summary>
		public string SavedPath
		{
			get { return _savedPath; }
			set { _savedPath = value; }
		}

		[XmlIgnore]
		public IDictionary Files
		{
			get { return _files; }
			set { _files = value; }
		}

		[XmlIgnore]
		public int PublishStatusId
		{
			get { return _publishStatusId; }
			set { _publishStatusId = value; }
		}

		/// <summary>
		/// this is used to make XmlSerializer works
		/// </summary>
		public GameUploadFile[] GameUploadFiles 
		{
			get 
			{
				GameUploadFile[] items = new GameUploadFile[ _files.Count ];
				_files.Values.CopyTo(items,0);
				return items;
			}
			set 
			{
				if( value == null ) return;
				GameUploadFile[] items = value;
				_files.Clear();
				foreach( GameUploadFile item in items )
				{
					AddFile(item);
				}
			}
		}
	}
}
