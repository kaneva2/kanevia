///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Xml.Serialization;
using Klausent.KEP.App.PublishingServer.Utils;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace Klausent.KEP.App.PublishingServer.Domain.Game
{
	/// <summary>
	/// Summary description for GameUploadFile.
	/// </summary>
	[XmlRoot("gameUploadFile")]
	public class GameUploadFile
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(GameUploadFile));

		private string		_relativePath;
		private string		_name;
		private int			_resultId;
		private string		_hash;
		private long		_size;
		private FileStream	_fileStream;
		private DateTime	_lastAccessed = DateTime.Now;
		private GameUploadFileList _gameUploadFileList;

		public GameUploadFile()
		{
			//
			// TODO: Add constructor logic here
			//
			_resultId = (int) Constants.eGAME_FILE_UPLOAD_RESULT.NOT_UPLOADED;
			_hash = null;
		}

		/// <summary>
		/// returns RELATIVE_PATH\FILE_NAME
		/// </summary>
		/// <returns></returns>
		public string GetFullName()
		{
			string retVal = _relativePath == null ? "" : _relativePath;
			if(!retVal.EndsWith(Constants.GAME_UPLOAD_DIR_SEPARATOR))
			{
				retVal += Constants.GAME_UPLOAD_DIR_SEPARATOR;
			}
			retVal += _name;

			return retVal;
		}

		/// <summary>
		/// return next byte that should be uploaded by client
		/// </summary>
		/// <returns></returns>
		public long GetNextStartByte()
		{
			return GetUploadedSize();
		}

		public long GetUploadedSize()
		{
			long retVal = 0;
			FileInfo fi = new FileInfo(GetSavedPath()) ;
			if(fi.Exists)
			{
				retVal = fi.Length;
			}
			return retVal;
		}

		/// <summary>
		/// return where the file is to be saved
		/// </summary>
		/// <returns></returns>
		public string GetSavedPath()
		{
			return Path.Combine(Path.Combine(_gameUploadFileList.SavedPath, _relativePath),_name);
		}

		/// <summary>
		/// write the chunk of data to target location
		/// acess to this method is synchronized
		/// </summary>
		/// <param name="uploadedChunkStream"></param>
		public void WriteChunk(Stream uploadedChunkStream)
		{
			lock(this)
			{
				uploadedChunkStream.Seek(0, SeekOrigin.Begin);
				
				if(_fileStream == null)
				{
					_fileStream = new FileStream(GetSavedPath(), FileMode.Append, FileAccess.Write, FileShare.None);
				}

				// Write bytes to the output stream.
				int size = 4096;
				byte[] bytes = new byte[4096];
				int numBytes;
				while((numBytes = uploadedChunkStream.Read(bytes, 0, size)) > 0)
				{
					_fileStream.Write(bytes, 0, numBytes);
				}
			
				_fileStream.Flush();
				// cleanup
				uploadedChunkStream.Close();
				_lastAccessed = DateTime.Now;
			}
		}

		/// <summary>
		/// get md5 hash of uploaded portion
		/// </summary>
		/// <returns></returns>
		public string GetUploadedMD5()
		{
			string retVal = null;
			string savedPath = GetSavedPath();
			if(File.Exists(savedPath))
			{
				lock(this)
				{
					CloseFileHandle();

					FileStream fileStream = new FileStream(GetSavedPath(), FileMode.Open, FileAccess.Read);
					string md5Cal = HashCalculator.GetMD5(fileStream);
					fileStream.Close();
					retVal = md5Cal;
				}
			}
			return retVal;
		}

		public bool CheckMD5()
		{
			string md5Cal = GetUploadedMD5();
			return md5Cal != null && md5Cal.Equals(_hash);
		}

		public void CloseFileHandle()
		{
			lock(this)
			{
				if(_fileStream != null)
				{
					log.Info("Unlocking file " + GetSavedPath());
					_fileStream.Close();
					_fileStream = null;
				}
			}
		}

		/// <summary>
		/// delete what has been uploaded
		/// </summary>
		public void ClearUploaded()
		{
			CloseFileHandle();
			lock(this)
			{
				FileInfo uploaded = new FileInfo(this.GetSavedPath());
				if(uploaded.Exists)
				{
					uploaded.Delete();
				}
			}
			this.ResultId = (int) Constants.eGAME_FILE_UPLOAD_RESULT.NOT_UPLOADED;
		}

		/// <summary>
		/// relative path to which the file is to saved
		/// </summary>
		public string RelativePath
		{
			get { return _relativePath; }
			set { _relativePath = value; }
		}

		/// <summary>
		/// name of the file
		/// </summary>
		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}
		
		public int ResultId
		{
			get { return _resultId; }
			set { _resultId = value; }
		}

		/// <summary>
		/// md5 hash of the file
		/// </summary>
		public string Hash
		{
			get { return _hash; }
			set { _hash = value; }
		}

		public long Size
		{
			get { return _size; }
			set { _size = value; }
		}

		[XmlIgnore]
		public GameUploadFileList GameUploadFileList
		{
			get { return _gameUploadFileList; }
			set { _gameUploadFileList = value; }
		}

		/// <summary>
		/// last time this file is uploaded, not saved in xml file
		/// </summary>
		[XmlIgnore]
		public DateTime LastAccessed
		{
			get { return _lastAccessed; }
			set { _lastAccessed = value; }
		}
	}
}
