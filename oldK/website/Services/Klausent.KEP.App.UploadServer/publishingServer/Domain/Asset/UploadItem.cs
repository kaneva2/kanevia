///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Threading;
using Klausent.KEP.App.PublishingServer.Utils;
using log4net;

namespace Klausent.KEP.App.PublishingServer.Domain.Asset
{
	/// <summary>
	/// Summary description for UploadItem.
	/// </summary>
	public class UploadItem
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(UploadItem));
		
		private int			_assetId;
		private int			_userId;
		private string		_path;
		private string		_hash;
		private long		_size;
		private BufferedStream	_fileStream;
		private DateTime	_lastAccessed;
		private DateTime	_timeStarted;
		private DateTime	_timeToFinish;
		private int			_assetStatusId;
		
		public UploadItem()
		{
			//
			// TODO: Add constructor logic here
			//
			_lastAccessed = DateTime.Now;
		}

		public int AssetId
		{
			get { return _assetId; }
			set { _assetId = value; }
		}

		public int UserId
		{
			get { return _userId; }
			set { _userId = value; }
		}

		public string Path
		{
			get { return _path; }
			set { _path = value; }
		}

		public string MD5Hash
		{
			get { return _hash; }
			set { _hash = value; }
		}

		public long Size
		{
			get { return _size; }
			set { _size = value; }
		}

		public DateTime TimeStarted
		{
			get { return _timeStarted; }
			set { _timeStarted = value; }
		}

		public DateTime TimeToFinish
		{
			get { return _timeToFinish; }
			set { _timeToFinish = value; }
		}

		public long GetUploadedSize()
		{
			long retVal = 0;
			FileInfo fi = new FileInfo(Path) ;
			if(fi.Exists)
			{
				retVal = fi.Length;
			}
			return retVal;
		}

		public int AssetStatusId
		{
			get { return _assetStatusId; }
			set { _assetStatusId = value; }
		}

		/// <summary>
		/// return next byte that should be uploaded by client
		/// </summary>
		/// <returns></returns>
		public long GetNextStartByte()
		{
			return GetUploadedSize();
		}

		/// <summary>
		/// write the chunk of data to target location
		/// acess to this method is synchronized
		/// </summary>
		/// <param name="uploadedChunkStream"></param>
		public void WriteChunk(Stream uploadedChunkStream)
		{
			lock(this)
			{
				uploadedChunkStream.Seek(0, SeekOrigin.Begin);
				if(_fileStream == null)
				{
					_fileStream = new BufferedStream( new 
						FileStream(_path, FileMode.Append, FileAccess.Write, FileShare.None));
				}

				// Write bytes to the output stream.
				int size = 4 * 10 * 1024;
				byte[] bytes = new byte[4 * 10 * 1024];
				int numBytes;
				while((numBytes = uploadedChunkStream.Read(bytes, 0, size)) > 0)
				{
					_fileStream.Write(bytes, 0, numBytes);
				}
			
				//_fileStream.Flush();
				// cleanup
				uploadedChunkStream.Close();
				_lastAccessed = DateTime.Now;
			}
		}

		public bool CheckMD5()
		{
			lock(this)
			{
				CloseFileHandle();

				FileStream fileStream = new FileStream(_path, FileMode.Open, FileAccess.Read);
				string md5Cal = HashCalculator.GetMD5(fileStream);
				fileStream.Close();
				return md5Cal.Equals(_hash);
			}
		}

		public void CloseFileHandle()
		{
			lock(this)
			{
				if(_fileStream != null)
				{
					log.Info("Unlocking file " + _path);
					_fileStream.Close();
					_fileStream = null;
				}
			}
		}

		/// <summary>
		/// last time this item is accessed
		/// </summary>
		public DateTime LastAccessed
		{
			get { return _lastAccessed; }
		}
	}
}
