///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using ICSharpCode.SharpZipLib.Checksums;

namespace Klausent.KEP.App.PublishingServer.Utils
{
	/// <summary>
	/// Summary description for HashCalculator.
	/// </summary>
	public class HashCalculator
	{
		public HashCalculator()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static string GetCRC32(byte[] data)
		{
			Crc32 crc32 = new Crc32();
			crc32.Update(data);
			return crc32.Value.ToString();
		}

		public static string GetMD5(byte[] data)
		{
			MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider(); 
			byte[] hash = md5.ComputeHash(data);

			StringBuilder sb = new StringBuilder();
			foreach(byte b in hash) 
			{
				sb.Append(String.Format("{0:x2}", b));
			}

			return sb.ToString();
		}

		public static string GetMD5(Stream stream)
		{
			MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider(); 
			byte[] hash = md5.ComputeHash(stream);

			StringBuilder sb = new StringBuilder();
			foreach(byte b in hash) 
			{
				sb.Append(String.Format("{0:x2}", b));
			}

			return sb.ToString();
		}
	}
}
