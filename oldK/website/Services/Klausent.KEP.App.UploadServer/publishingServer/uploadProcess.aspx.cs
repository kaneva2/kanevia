///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Klausent.KEP.App.PublishingServer.Domain.Asset;
using Klausent.KEP.App.PublishingServer.Service.Asset;
using Klausent.KEP.App.PublishingServer.Utils;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace Klausent.KEP.App.PublishingServer
{
	/// <summary>
	/// Summary description for uploadProcess.
	/// </summary>
	public partial class uploadProcess : System.Web.UI.Page
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(uploadProcess));

		private static readonly SessionManager sessionManager = SessionManager.GetInstance();
		private static readonly UploadItemManager uploadItemManager = UploadItemManager.GetInstance();

		String _ticket = null;
		int _assetId = -1;
		long _startByte = -1;
		long _endByte = -1;
		String _requestCrc = null;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			InitiateUpload();
		}
		private void InitiateUpload()
		{
			//validate request
			bool validRequest = ValidateRequest();

			if(!validRequest)
			{
				try
				{
					log.Warn("invalid request received");
					WriteErrorInvalidRequest();
				}
				catch(ThreadAbortException){}
				return;
			}
				
			try
			{
				int user_id = sessionManager.AuthorizeTicket(ref _ticket);
				if ( user_id != 0)
				{
					UploadItem uploadItem = uploadItemManager.Get(_assetId);
					if(uploadItem == null)
					{
						//this should not happen if client functions correctly
						WriteErrorInvalidRequest();
						return;
					}
					else
					{
						if (uploadItem.AssetStatusId == (int) Constants.eASSET_STATUS.DELETED || 
							uploadItem.AssetStatusId == (int) Constants.eASSET_STATUS.MARKED_FOR_DELETION)
						{
							//file deleted
							uploadItem.CloseFileHandle();
							WriteErrorFileDeleted();
							return;
						}else if(uploadItem.TimeToFinish <= DateTime.Now)
						{
							//upload expired
							uploadItem.CloseFileHandle();
							WriteErrorUploadExpired();
							return;
						}
						else
						{
							WriteFile(uploadItem);
							return;
						}
					}
				}
				else
				{	
					WriteResponse((int) Constants.ePS_RETURNCODES.ERROR_TICKET_NOT_AUTHORIZED,null);
					return;
				}
			}
			catch(ThreadAbortException)
			{
				//Response.End() thorws ThreadAbortException
			}catch(Exception e)
			{
				log.Error("failed to save chunk", e);
				WriteResponse((int) Constants.ePS_RETURNCODES.EXCEPTION_UPLOAD,_ticket);
				return;
			}
		}

		/// <summary>
		/// validate request params
		/// </summary>
		/// <returns></returns>
		private bool ValidateRequest()
		{
			bool validRequest = true;
			try
			{
				_ticket = Request.Params[Constants.PARAM_UPLOAD_PROCESS_TICKET];
				_assetId = int.Parse(Request.Params[Constants.PARAM_UPLOAD_PROCESS_ASSET_ID]);
				_startByte = long.Parse(Request.Params[Constants.PARAM_UPLOAD_PROCESS_START_BYTE]);
				_endByte = long.Parse(Request.Params[Constants.PARAM_UPLOAD_PROCESS_END_BYTE]);
				_requestCrc = Request.Params[Constants.PARAM_UPLOAD_PROCESS_CRC];
				
				if(_ticket == null || _ticket.Length == 0 || 
					_requestCrc == null || _requestCrc.Length == 0)
				{
					validRequest = false;
				}
			}
			catch(Exception)
			{
				validRequest = false;
			}

			return validRequest;
		}

		private void WriteFile(UploadItem uploadItem)
		{
			HttpFileCollection uploadedFiles;
			uploadedFiles = Request.Files; // Load File collection into HttpFileCollection variable.
			HttpPostedFile file = uploadedFiles.Get(Constants.PARAM_UPLOAD_PROCESS_DATA);
			Stream uploadedChunkStream = file.InputStream;

			bool crcMatched = CheckCRC(uploadedChunkStream, _requestCrc);
			if(crcMatched)
			{
				//crc verification passed
				FileInfo fileInfo = new FileInfo(uploadItem.Path);
				if(!fileInfo.Directory.Exists)
				{
					if(_startByte == 0)
					{
						//first chunk
						fileInfo.Directory.Create();
						//fileInfo.Create(); don't create here as it will lock the file
					}
					else
					{
						//invalid start byte
						log.Warn("invalid start byte, filename = " + uploadItem.Path + 
							"\ntarget file doesn't exist on the server and requested start byte is a non-zero value: " + _startByte);
						WriteResponse((int)Constants.ePS_RETURNCODES.ERROR_INVALID_START_BYTE, _ticket);
						return;
					}
				}

				if(_startByte!= 0 && fileInfo.Length != _startByte)
				{
					log.Warn("invalid start byte, filename = " + uploadItem.Path + 
						"\nfile size on server = " + fileInfo.Length + " and requested start byte = " + _startByte);
					WriteResponse((int)Constants.ePS_RETURNCODES.ERROR_INVALID_START_BYTE, _ticket);
					return;
				}
				else
				{
					//start saving
					uploadItem.WriteChunk(uploadedChunkStream);
					if(_endByte + 1 == uploadItem.Size)
					{
						//received the full file, not check md5
						uploadItem.CloseFileHandle();
						if(uploadItem.CheckMD5())
						{
							//file is successfully uploaded
							if(log.IsInfoEnabled)
							{
								log.Info("file uploaded, path = " + uploadItem.Path);
							}
							StoreUtility.UpdateAssetUploadStatus(uploadItem.AssetId, 
								(int) Constants.ePUBLISH_STATUS.UPLOADED,
								Request.UserHostAddress);
							WriteResponse((int)Constants.ePS_RETURNCODES.SUCCESS, _ticket);
						}
						else
						{
							log.Info("MD5 checking failed, filename = " + uploadItem.Path);
							WriteResponse((int)Constants.ePS_RETURNCODES.ERROR_MD5_ERROR, _ticket);
						}
					}
					else
					{
						//more data to send
						WriteResponse((int)Constants.ePS_RETURNCODES.SUCCESS, _ticket);
						return;
					}
				}
			}
			else
			{
				log.Info("CRC checking failed, filename = " + uploadItem.Path);
				WriteResponse((int)Constants.ePS_RETURNCODES.ERROR_CRC_ERROR, _ticket);
				return;
			}
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_UPLOAD_EXPIRED to the response
		/// </summary>
		private void WriteErrorUploadExpired()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_UPLOAD_EXPIRED);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
			
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_FILE_DELETED to the response
		/// </summary>
		private void WriteErrorFileDeleted()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_FILE_DELETED);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		private void WriteErrorInvalidRequest()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_INVALID_REQUEST);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		/// <summary>
		/// check see if crc32 of data received matches the crc sent from the client
		/// </summary>
		/// <param name="uploadedChunkStream"></param>
		/// <param name="requestCrc"></param>
		/// <returns></returns>
		private bool CheckCRC(Stream uploadedChunkStream, string requestCrc)
		{
			byte[] data = new byte[uploadedChunkStream.Length];
					
			int offset=0;
			int remaining = data.Length;
			while (remaining > 0)
			{
				int read = uploadedChunkStream.Read(data, offset, remaining);
				remaining -= read;
				offset += read;
			}

			string crcCal = HashCalculator.GetCRC32(data);
			return crcCal.Equals(requestCrc);
		}
		
		

		private void WriteResponse(int result, string ticket)
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write(result);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(ticket == null ? "" : ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
