///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Klausent.KEP.App.PublishingServer.Dao;
using Klausent.KEP.App.PublishingServer.Domain.Game;
using Klausent.KEP.App.PublishingServer.Service.Game;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace Klausent.KEP.App.PublishingServer
{
	/// <summary>
	/// Summary description for uploadInit.
	/// </summary>
	public partial class uploadInitGameFile : System.Web.UI.Page
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(uploadInitGameFile));

		private static readonly SessionManager sessionManager = SessionManager.GetInstance();
		private static readonly GameUploadManager gameUploadManager = GameUploadManager.GetInstance();
		private static readonly GameFileManifestDao gameFileManifestDao = GameFileManifestDao.GetInstance();

		String	_ticket = null;
		String	_filename = null;
		String  _filePath = null;
		String	_hash = null;
		long	_size = 0;
		int		_gameUploadId = 0;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			InitiateUpload();
		}

		private void InitiateUpload()
		{
			bool validRequest = ValidateRequest();
			if(!validRequest)
			{
				try
				{
					log.Warn("invalid request received");
					WriteInvalidRequest();
				}
				catch(ThreadAbortException)
				{
					//Response.End() thorws ThreadAbortException
				}
				return;
			}

			try
			{
				int user_id = sessionManager.AuthorizeTicket(ref _ticket);
				if ( user_id != 0)
				{
					GameUploadFileList gameUploadFileList = gameUploadManager.Get(_gameUploadId);

					if(gameUploadFileList == null)
					{
						//this should not happen if client functions correctly
						WriteErrorInvalidRequest();
						return;
					}else if(gameUploadFileList.PublishStatusId == 
						(int) Constants.eGAME_UPLOAD_PUBLISH_STATUS.UPLOADED)
					{
						WriteResponse((int) Constants.ePS_RETURNCODES.GAME_UPLOADED, _ticket);
						return;
					}
					else
					{
						//check if user owns the game
						if (!StoreUtility.IsAssetOwner(user_id, gameUploadFileList.GameId))
						{
							WriteNotOwner();
							return;
						}
						else
						{
							string fullName = _filePath == null ? "" : _filePath;
							if(!fullName.EndsWith(Constants.GAME_UPLOAD_DIR_SEPARATOR))
							{
								fullName += Constants.GAME_UPLOAD_DIR_SEPARATOR;
							}
							fullName += _filename;
							GameUploadFile gameUploadFile = gameUploadFileList.Get(fullName);
							if (gameUploadFile == null)
							{
								//file not in the list of files to be uploaded
								WriteFileNotInList();
								return;
							}else
							{
								CheckFileStatus(gameUploadFileList, gameUploadFile);
								return;
							}
						}
					}
				}
				else
				{
					WriteErrorTicketNotAuthorized();
					return;
				}
			}
			catch(ThreadAbortException)
			{
				//Response.End() thorws ThreadAbortException
			}
			catch(Exception e)
			{
				log.Error("failed to init game file upload", e);

				WriteException();
				return;
			}
		}

		/// <summary>
		/// get hash of uploading file and
		/// 1. skip if file in staging directory is same
		/// 2. skip if file in uploading direcotry is same
		/// 3. resume if file hash in local manifest is same and partially uploaded
		/// 4. create new if file hash in local manifest is same but not uploaded
		/// 5. create new if file hash in local manifest is different, also update 
		/// local manifest
		/// </summary>
		private void CheckFileStatus(GameUploadFileList gameUploadFileList,
			GameUploadFile gameUploadFile)
		{
			if(IsFileOnStaging(gameUploadFileList, gameUploadFile))
			{
				gameUploadFile.Hash = _hash;
				gameUploadFile.Size = _size;
				gameUploadFile.ResultId = (int) Constants.eGAME_FILE_UPLOAD_RESULT.NO_CHANGE;
				gameUploadManager.Save((gameUploadFileList));
				SkipFileUpload(gameUploadFileList);
			}else
			{
				CheckUploadedFileStatus(gameUploadFileList , gameUploadFile);
			}
		}

		private void CheckUploadedFileStatus(GameUploadFileList gameUploadFileList,
			GameUploadFile gameUploadFile)
		{
			if(gameUploadFile.Hash == null)
			{
				//when manifest file is overwriten, so is file hash.
				//so if it's null, check the hash of uploaded file, if it exists
				string currHash = gameUploadFile.GetUploadedMD5();
				if(currHash != null && currHash.Equals(_hash))
				{
					gameUploadFile.Hash = _hash;
					gameUploadFile.Size = _size;
					gameUploadFile.ResultId = (int) Constants.eGAME_FILE_UPLOAD_RESULT.UPLOADED;
					gameUploadManager.Save((gameUploadFileList));
					SkipFileUpload(gameUploadFileList);
				}else
				{
					//file has been changed
					//kill the handle, kill the uploaded portion
					gameUploadFile.ClearUploaded();
					gameUploadFile.Hash = _hash;
					gameUploadFile.Size = _size;
					gameUploadFile.ResultId = (int) Constants.eGAME_FILE_UPLOAD_RESULT.NOT_UPLOADED;
					gameUploadManager.Save((gameUploadFileList));
					WriteProceedUpload(gameUploadFile);
				}
			}else
			{
				// compare hash recorded in manifest file with hash sent from client
				if(!gameUploadFile.Hash.Equals(_hash))
				{
					//file has been changed
					//kill the handle, kill the uploaded portion
					gameUploadFile.ClearUploaded();
					gameUploadFile.Hash = _hash;
					gameUploadFile.Size = _size;
					gameUploadFile.ResultId = (int) Constants.eGAME_FILE_UPLOAD_RESULT.NOT_UPLOADED;
					gameUploadManager.Save((gameUploadFileList));
					WriteProceedUpload(gameUploadFile);
				}
				else
				{
					//hash sent from client is same as the hash recorded on the server. it means that
					//client has attempted to upload the same file before
					if(gameUploadFile.GetUploadedSize() == _size)
					{
						//file uploaded
						gameUploadFile.ResultId = (int) Constants.eGAME_FILE_UPLOAD_RESULT.UPLOADED;
						gameUploadManager.Save((gameUploadFileList));
						SkipFileUpload(gameUploadFileList);
					}else
					{
						//file either doesn't exist or not complete
						gameUploadFile.ResultId = (int) Constants.eGAME_FILE_UPLOAD_RESULT.NOT_UPLOADED;
						gameUploadFile.Size = _size;
						gameUploadManager.Save((gameUploadFileList));
						WriteProceedUpload(gameUploadFile);
					}
				}
			}
		}

		/// <summary>
		/// return true if file trying to upload is already on staging
		/// </summary>
		/// <param name="gameUploadFileList"></param>
		/// <param name="gameUploadFile"></param>
		/// <returns></returns>
		private bool IsFileOnStaging(GameUploadFileList gameUploadFileList, GameUploadFile gameUploadFile)
		{
			bool retVal = false;
			//string stagingDir = StoreUtility.GetGameStagingDirectory(
			//	gameUploadFileList.GameId);
            string stagingDir = "Not implemented in developer DB yet";

			if(stagingDir == null || stagingDir.Trim().Length == 0 || !new DirectoryInfo(stagingDir).Exists)
			{
				//staging is not yet setup
				retVal = false;
			}else
			{
				//see if file on staging has same hash
				try
				{
					GameFileManifest gameFileManifest = gameFileManifestDao.Load(
						Path.Combine(stagingDir, Constants.GAME_FILE_MANIFEST));
					if(gameFileManifest != null)
					{
						//staging contains manifest file
						GameFile gameFile = gameFileManifest.GetGameFile(gameUploadFile.GetFullName());
						if(gameFile == null || !gameFile.Hash.Equals(_hash))
						{
							retVal = false;
						}
						else
						{
							retVal = true;
						}
					}
					else
					{
						//staging does not contain manifest file. It's either newly setup or damaged, so
						//assume the file is not on staging
						retVal = false;
					}
				}catch(Exception e)
				{
					log.Error("Invalid manifest file at " + stagingDir +" gameId = " + gameUploadFileList.GameId, e);
				}
			}
			return retVal;
		}

		/// <summary>
		/// validate the request params
		/// </summary>
		/// <returns></returns>
		private bool ValidateRequest()
		{
			bool validRequest = true;
			try
			{
				_ticket = Request.Params[Constants.PARAM_UPLOAD_INIT_TICKET];
				_filename = Request.Params[Constants.PARAM_UPLOAD_INIT_FILE_NAME];
				_filePath = Request.Params[Constants.PARAM_UPLOAD_INIT_DEST_PATH];
				_hash = Request.Params[Constants.PARAM_UPLOAD_INIT_HASH];
				_size = long.Parse(Request.Params[Constants.PARAM_UPLOAD_INIT_SIZE]);
				_gameUploadId = int.Parse(Request.Params[Constants.PARAM_UPLOAD_INIT_GAME_UPLOAD_ID]);
				if(_ticket == null || _ticket.Length == 0 || 
					_filename == null || _filename.Length == 0 || 
					_hash == null || _hash.Length == 0)
				{
					validRequest = false;
				}
			}
			catch(Exception)
			{
				validRequest = false;
			}

			return validRequest;
		}

		
		/// <summary>
		/// write GAME_UPLOADED if whole game is uploaded
		/// FILE_NOT_CHANGED otherwise
		/// </summary>
		/// <param name="gameUploadFileList"></param>
		private void SkipFileUpload(GameUploadFileList gameUploadFileList)
		{
			//check if the whole game has been uploaded
			if(gameUploadFileList.IsUploaded())
			{
				//this whole game is uploaded
				//update asset upload record
				if(log.IsInfoEnabled)
				{
					log.Info("game uploaded, gameId = " + gameUploadFileList.GameId + 
						" gameUploadId = " + gameUploadFileList.GameUploadId);
				}
				gameUploadFileList.PublishStatusId = 
					(int) Constants.eGAME_UPLOAD_PUBLISH_STATUS.UPLOADED;
				StoreUtility.UpdateGameUploadStatus(gameUploadFileList.GameUploadId,
					(int) Constants.eGAME_UPLOAD_PUBLISH_STATUS.UPLOADED,
					Request.UserHostAddress);
				WriteResponse((int) Constants.ePS_RETURNCODES.GAME_UPLOADED, _ticket);
			}
			else
			{
				//no need to upload this file, but there are more to process
				WriteFileExists();
			}
		}

		private void WriteFileExists()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.FILE_NOT_CHANGED);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}
		private void WriteErrorInvalidRequest()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_INVALID_REQUEST);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_USER_NOT_OWNER to the response
		/// </summary>
		private void WriteNotOwner()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_USER_NOT_OWNER);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_FILE_NOT_IN_LIST to the response
		/// </summary>
		private void WriteFileNotInList()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_FILE_NOT_IN_LIST);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.EXCEPTION_INIT_UPLOAD to the response
		/// </summary>
		private void WriteException()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.EXCEPTION_INIT_UPLOAD);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_INVALID_REQUEST to the response
		/// </summary>
		private void WriteInvalidRequest()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_INVALID_REQUEST);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_TICKET_NOT_AUTHORIZED to the response
		/// </summary>
		private void WriteErrorTicketNotAuthorized()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_TICKET_NOT_AUTHORIZED);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		/// <summary>
		/// instruct user to start uploaded the file
		/// </summary>
		/// <param name="gameUploadFile"></param>
		private void WriteProceedUpload(GameUploadFile gameUploadFile)
		{	
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.SUCCESS);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(_ticket == null ? "" : _ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("<startByte>");
			Response.Write(gameUploadFile.GetNextStartByte());
			Response.Write("</startByte>\r\n");
			Response.Write("</kaneva>");
			Response.End();
			return;
		}

		private void WriteResponse(int result, string ticket)
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition", "inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write(result);
			Response.Write("</result>\r\n");
			Response.Write("<ticket>");
			Response.Write(ticket == null ? "" : ticket);
			Response.Write("</ticket>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
