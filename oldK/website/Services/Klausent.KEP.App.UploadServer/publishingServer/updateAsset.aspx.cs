///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Klausent.KEP.App.PublishingServer.Domain.Asset;
using Klausent.KEP.App.PublishingServer.Service.Asset;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace Klausent.KEP.App.PublishingServer
{
	/// <summary>
	/// Kaneva calls this page to notify that an asset has been marked for deletion
	/// </summary>
	public partial class updateAsset : System.Web.UI.Page
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(uploadInit));
		
		private static readonly UploadItemManager uploadItemManager = UploadItemManager.GetInstance();
		
		int		_assetId = 0;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			try
			{
				UpdateAsset();
			}
			catch(ThreadAbortException)
			{
				//Response.End() thorws ThreadAbortException
			}
		}

		private void UpdateAsset()
		{
			bool validRequest = ValidateRequest();
			if(!validRequest)
			{
				log.Warn("invalid request received");
				WriteInvalidRequest();
				return;
			}
			else
			{
				log.Info("updating asset: " + _assetId);
				UploadItem item = uploadItemManager.Get(_assetId, false);
				if(item == null)
				{
					//not found in memory, means not uploading
					ReturnSuccessfulCode();
				}
				else
				{
					uploadItemManager.Update(item);
					ReturnSuccessfulCode();
				}
			}
		}

		/// <summary>
		/// validate the request params
		/// </summary>
		/// <returns></returns>
		private bool ValidateRequest()
		{
			bool validRequest = true;
			try
			{
				_assetId = int.Parse(Request.Params[Constants.PARAM_NOTIFY_DELETION_ASSET_ID]);
			}
			catch(Exception)
			{
				validRequest = false;
			}

			return validRequest;
		}

		/// <summary>
		/// write Constants.ePS_RETURNCODES.ERROR_INVALID_REQUEST to the response
		/// </summary>
		private void WriteInvalidRequest()
		{
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.ERROR_INVALID_REQUEST);
			Response.Write("</result>\r\n");
			Response.Write("</kaneva>");
			Response.End();
		}

		private void ReturnSuccessfulCode()
		{	
			Response.Clear();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.ContentType = "text/xml";
			Response.AddHeader("Content-Disposition","inline;filename=result.xml");
			Response.Write("<kaneva>\r\n");
			Response.Write("<result>");
			Response.Write((int)Constants.ePS_RETURNCODES.SUCCESS);
			Response.Write("</result>\r\n");
			Response.Write("</kaneva>");
			Response.End();
			return;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
