///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace CloudSponge
{
    public static class MailAddressEx
    {
        private static Dictionary<string, ContactSource> hostToSourceMapping = new Dictionary<string, ContactSource>(StringComparer.OrdinalIgnoreCase)
            {
                {"gmail.com", ContactSource.Gmail},
                {"live.com", ContactSource.WindowsLive},
                {"yahoo.com", ContactSource.Yahoo},
                {"plaxo.com", ContactSource.Plaxo},
                {"aol.com", ContactSource.AOL}
            };

        public static ContactSource GetCloudSpongeSource(this MailAddress address)
        {
            ContactSource source = ContactSource.Uknown;
            hostToSourceMapping.TryGetValue(address.Host, out source);
            return source;
        }
    }
}
