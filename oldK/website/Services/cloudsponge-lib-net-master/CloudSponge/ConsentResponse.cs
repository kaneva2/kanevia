///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Xml.Linq;

namespace CloudSponge
{
    public class ConsentResponse : BeginImportResponse
    {
        
        public string Url { get; private set; }

        public override void Populate(XContainer root)
        {
            root = root.Element("result");

            base.Populate(root);
            
            Url = root.Element("url").Value;
        }
    }
}
