///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace CloudSponge
{
    [Serializable]
    public enum PhoneNumberType
    {
        Home,
        Work,
        Other
    }
    [Serializable]
    public class PhoneNumber
    {
        public PhoneNumberType Type { get; internal set; }
        public string Number { get; internal set; }
    }
    [Serializable]
    public class Contact
    {
        public string FirstName { get; internal set; }
        public string LastName { get; internal set; }
        public List<PhoneNumber> PhoneNumbers { get; internal set; }
        public List<string> EmailAddresses { get; internal set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Gender { get; set; }
        public string ThumbnailPath { get; set; }
        public bool UseFacebookProfilePicture { get; set; }
        public UInt64 FacebookUserId { get; set; }
    }
    [Serializable]
    public class ContactsResponse : CloudSpongeResponse
    {
        public List<Contact> Contacts { get; private set; }
        public override void Populate(XContainer root)
        {
            root = root.Element("contactsResponse");
            base.Populate(root);

            this.Contacts = (from c in root.Element("contacts").Elements("contact")
                             where c.Element ("email").Elements ("email").Count() > 0
                             select new Contact
                            {
                                FirstName = _accessStringProperty(c, "first-name"),
                                LastName = _accessStringProperty(c, "last-name"),
                                EmailAddresses = (from e in c.Element("email").Elements("email")
                                                  select _accessStringProperty(e, "address")).ToList(),
                                PhoneNumbers = (from p in c.Element("phone").Elements("phone")
                                                select new PhoneNumber
                                               {
                                                   Number = _accessStringProperty(p, "number"),
                                                   Type = _accessPhoneNumberTypeProperty(p, "type")
                                               }).ToList()
                            }).ToList();
        }

        private String _accessStringProperty(XElement el, String property)
        {
            String ret = "";
            XElement xValue;
            if (el != null)
            {
                xValue = el.Element(property);
                if (xValue != null)
                {
                    ret = xValue.Value;
                }
            }
            return ret;
        }

        private PhoneNumberType _accessPhoneNumberTypeProperty(XElement el, String property)
        {
            PhoneNumberType ret = PhoneNumberType.Home;
            String phoneTypeString = _accessStringProperty(el, property);
            if (phoneTypeString != "")
            {
                if (Enum.TryParse(phoneTypeString, true, out ret))
                {
                    if (Enum.IsDefined(typeof(PhoneNumberType), ret))
                    {
                        ret = (PhoneNumberType)Enum.Parse(typeof(PhoneNumberType), phoneTypeString, true);   
                    }
                    else
                    {
                        ret = PhoneNumberType.Home;
                    }
                }
      //          ret = (PhoneNumberType)Enum.Parse(typeof(PhoneNumberType), phoneTypeString, true);
            }
            return ret;
        }
    }
}
