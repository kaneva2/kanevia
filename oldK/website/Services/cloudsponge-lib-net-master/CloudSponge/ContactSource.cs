///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CloudSponge
{
    public enum ContactSource
    {
        Uknown,
        Yahoo,
        WindowsLive,
        Gmail,
        AOL,
        Plaxo,
        AddressBook,
        Outlook,
        Mailru
    }

    public static class ContactSourceEx
    {
        public static AuthenticationMethod GetSupportedMethods(this ContactSource source)
        {
            switch (source)
            {
                case ContactSource.Yahoo:
                case ContactSource.WindowsLive:
                    return AuthenticationMethod.Consent;
                case ContactSource.Gmail:
                    return AuthenticationMethod.Consent;// | AuthenticationMethod.Import;
                case ContactSource.Mailru:
                    return AuthenticationMethod.Consent;
                case ContactSource.AOL:
                    return AuthenticationMethod.Consent;
                case ContactSource.Plaxo:
                    return AuthenticationMethod.Import;
                case ContactSource.AddressBook:
                case ContactSource.Outlook:
                    return AuthenticationMethod.DesktopApplet;
                case ContactSource.Uknown:
                    return AuthenticationMethod.Consent | AuthenticationMethod.Import | AuthenticationMethod.DesktopApplet;
                default:
                    throw new NotImplementedException();
            }

        }
        public static bool IsSupported(this ContactSource source, AuthenticationMethod authentication)
        {
            return (source.GetSupportedMethods() & authentication) == authentication;
        }
    }
}
