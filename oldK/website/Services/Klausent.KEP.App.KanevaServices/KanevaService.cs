///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using System.Net;
using System.Security.Cryptography;
using System.Web.Security;
using System.Timers;
using System.IO;
using log4net;
using KlausEnt.KEP.Kaneva.framework.widgets;


using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.KanevaService
{
	public class KanevaService : System.ServiceProcess.ServiceBase
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		static KanevaService ()
		{
			// Set up log for net
			string s = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
			FileInfo l_fi = new FileInfo (s);
			log4net.Config.DOMConfigurator.Configure (l_fi);
		}

		public KanevaService ()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();

			// Create service timers
			CreateTimers ();

			// Run on startup
			//RunFunctionalityTest ();
		}

		// The main entry point for the process
		static void Main ()
		{
			// IF RUN AS SERVICE
            System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new KanevaService() };
			System.ServiceProcess.ServiceBase.Run (ServicesToRun);

            // ELSE TEST AS NON SERVICE
            //KanevaService service = new KanevaService();
            //string [] args = new string[] {"Tom", "Dick", "Harry"};
 			//service.OnStart(args);
			//System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = "Kaneva Website Services";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		protected override void OnStart (string[] args)
		{
			if (EmailNotificationsEnabled) 
			{	
				tmrEmailNotification.AutoReset = true;
				tmrEmailNotification.Enabled = true;
				tmrEmailNotification.Start ();
			}

			if (SummaryTablesUpdateEnabled) 
			{
				tmrSummaryTables.AutoReset = true;
				tmrSummaryTables.Enabled = true;
				tmrSummaryTables.Start ();
			}

			if (DatabaseCleanupEnabled)
			{
				tmrDatabaseCleanup.AutoReset = true;
				tmrDatabaseCleanup.Enabled = true;
				tmrDatabaseCleanup.Start ();
			}

			if (VideoImageGenerationEnabled) 
			{
				tmrVideoImageGeneration.AutoReset = true;
				tmrVideoImageGeneration.Enabled = true;
				tmrVideoImageGeneration.Start ();
			}

			if (FunctionalityCheckEnabled)
			{
				tmrFunctionalityCheck.AutoReset = true;
				tmrFunctionalityCheck.Enabled = true;
				tmrFunctionalityCheck.Start ();
			}

			if (KDPRewardsEnabled)
			{
				tmrKDPRewards.Enabled = true;
				tmrKDPRewards.Start ();
			}
		}
 
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop ()
		{
			if (EmailNotificationsEnabled) 
			{	
				tmrEmailNotification.AutoReset = false;
				tmrEmailNotification.Enabled = false;
			}

			if (SummaryTablesUpdateEnabled) 
			{
				tmrSummaryTables.AutoReset = false;
				tmrSummaryTables.Enabled = false;
			}

			if (DatabaseCleanupEnabled)
			{
				tmrDatabaseCleanup.AutoReset = false;
				tmrDatabaseCleanup.Enabled = false;
			}

			if (VideoImageGenerationEnabled) 
			{
				tmrVideoImageGeneration.AutoReset = false;
				tmrVideoImageGeneration.Enabled = false;
			}

			if (FunctionalityCheckEnabled)
			{
				tmrFunctionalityCheck.AutoReset = false;
				tmrFunctionalityCheck.Enabled = false;
			}

			if (KDPRewardsEnabled)
			{
				tmrKDPRewards.AutoReset = false;
				tmrKDPRewards.Enabled = false;
			}

		}

		/// <summary>
		/// Pause this service
		/// </summary>
		protected override void OnPause () 
		{
			if (EmailNotificationsEnabled) 
			{	
				tmrEmailNotification.Stop ();
			}

			if (SummaryTablesUpdateEnabled) 
			{
				tmrSummaryTables.Stop ();
			}

			if (DatabaseCleanupEnabled)
			{
				tmrDatabaseCleanup.Stop ();
			}

			if (VideoImageGenerationEnabled) 
			{
				tmrVideoImageGeneration.Stop ();
			}

			if (FunctionalityCheckEnabled)
			{
				tmrVideoImageGeneration.Stop ();
			}

			if (KDPRewardsEnabled)
			{
				tmrKDPRewards.Stop ();
			}
		}

		/// <summary>
		/// Continue this service
		/// </summary>
		protected override void OnContinue () 
		{
			if (EmailNotificationsEnabled) 
			{	
				tmrEmailNotification.Start ();
			}

			if (SummaryTablesUpdateEnabled) 
			{
				tmrSummaryTables.Start ();
			}

			if (DatabaseCleanupEnabled)
			{
				tmrDatabaseCleanup.Start ();
			}

			if (VideoImageGenerationEnabled) 
			{
				tmrVideoImageGeneration.Start ();
			}

			if (FunctionalityCheckEnabled)
			{
				tmrVideoImageGeneration.Start ();
			}

			if (KDPRewardsEnabled)
			{
				tmrKDPRewards.Start ();
			}
		}

		/// <summary>
		/// tmrSummaryTables_Elapsed
		/// 
		/// Call the update_report_stats stored proc
		protected void tmrSummaryTables_Elapsed (object sender, System.Timers.ElapsedEventArgs e) 
		{
			try
			{
				tmrSummaryTables.Stop ();
				m_logger.Info ("Summary Tables Updater - update_report_stats - START");
				Services.CallUpdateReportStats ();
				m_logger.Info ("Summary Tables Updater - update_report_stats - END");
			}
			catch (Exception exc)
			{
				m_logger.Error ("Summary Tables Updater - update_report_stats - exception", exc);
			}
			finally
			{
				tmrSummaryTables.Start ();
			}
		}

		/// <summary>
		/// tmrDatabaseCleanup_Elapsed
		/// 
		/// Call the _database_cleanup stored proc
		protected void tmrDatabaseCleanup_Elapsed (object sender, System.Timers.ElapsedEventArgs e) 
		{
			try
			{
				tmrDatabaseCleanup.Stop ();
				m_logger.Info ("Database Cleanup START");
				Services.CallDatabaseCleanup ();
				m_logger.Info ("Database Cleanup END");
			}
			catch (Exception exc)
			{
				m_logger.Error ("Database Cleanup exception", exc);
			}
			finally
			{
				tmrDatabaseCleanup.Start ();
			}
		}

		/// <summary>
		/// tmrVideoImageGeneration_Elapsed
		/// 
		/// Call the Video Image Generation
		protected void tmrVideoImageGeneration_Elapsed (object sender, System.Timers.ElapsedEventArgs e) 
		{
			try
			{
				tmrVideoImageGeneration.Stop ();

				int AssetIdStart = 0;

				if (System.Configuration.ConfigurationSettings.AppSettings ["AssetIdStart"] != null)
				{
					AssetIdStart = Convert.ToInt32 (System.Configuration.ConfigurationSettings.AppSettings ["AssetIdStart"]);
				}

				m_logger.Info ("Video image generation - START");
				GenerateVideoImages (AssetIdStart);
				m_logger.Info ("Video image generation - END");
			}
			catch (Exception exc)
			{
				m_logger.Error ("Video image generation exception", exc);
			}
			finally
			{
				tmrVideoImageGeneration.Start ();
			}
		}

		/// <summary>
		/// NotificationsTimer_Elapsed
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void NotificationsTimer_Elapsed (object sender, System.Timers.ElapsedEventArgs e) 
		{
			try
			{
				tmrEmailNotification.Stop ();
				m_logger.Info ("Send Notifications START");
				Services.SendNotifications ();
				m_logger.Info ("Send Notifications END");
			}
			catch (Exception exc)
			{
				m_logger.Error ("Send Notifications exception", exc);
			}
			finally
			{
				tmrEmailNotification.Start ();
			}
		}

		/// <summary>
		/// FunctionalityCheckTimer_Elapsed
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void FunctionalityCheckTimer_Elapsed (object sender, System.Timers.ElapsedEventArgs e) 
		{
			try
			{
				tmrFunctionalityCheck.Stop ();

				m_logger.Info ("Functionality Check START");
				RunFunctionalityTest ();
				m_logger.Info ("Functionality Check END");
			}
			finally
			{
				tmrFunctionalityCheck.Start ();
			}
		}

		/// <summary>
		/// tmrKDPRewards_Elapsed
		/// 
		protected void KDPRewardsTimer_Elapsed (object sender, System.Timers.ElapsedEventArgs e) 
		{
			try
			{
				tmrKDPRewards.Stop ();
				m_logger.Info ("KDP Rewards - START");
				RunKDPRewards();
				m_logger.Info ("KDP Rewards - END");
			}
			catch (Exception exc)
			{
				m_logger.Error ("KDP Rewards timer elapsed - exception", exc);
			}
			finally
			{
                tmrKDPRewards.Interval = GetMilliSecondsToMidnight() + 300000; // Add 5 minute fudge factor
				tmrKDPRewards.Start ();
			}
		}

		private void RunKDPRewards()
		{
			int return_code_daily = 0;
			int return_code_monthly = 0;

			try
			{
				Services.KDPRewards(ref return_code_daily, ref return_code_monthly);

				if ( return_code_daily == 0 && return_code_monthly == 0)
				{
					// All went well.
					// 1) Send a private message to the winner from the admin
					// 2) Blast the friends of the winner
					// 3) Blast kaneva with monthly winners

					DateTime blastExpires = DateTime.Now;

					double hoursToExpire = KDPRewardsMonthlyBlastExpires;

					blastExpires = blastExpires.AddHours(hoursToExpire);

					Services.KDPRewardRecognition( KDPRewardsMsgFromId, KDPRewardsPrivateMsgSubject, KDPRewardsPrivateMsgBody,
												   KDPRewardsFriendBlastSubject, KDPRewardsFriendBlastBody,
												   KDPRewardsMonthlyBlastSubject,
												   KDPRewardsMonthlyBlastBody, blastExpires);

				}
			}
			catch (Exception exc)
			{
				m_logger.Error ("RunKDPRewards - exception", exc);
			}
		}

		private void RunFunctionalityTest ()
		{
			bool bUserSuccess = false;
			bool bUploadSuccess = false;

			try
			{
				m_logger.Info ("Functionality START");

				string strUserName = "kTest" + KanevaGlobals.GenerateUniqueString (10);
				string strPassword = KanevaGlobals.GenerateUniqueString (10);
					
				// ****************************
				// Insert a new user
				// ****************************
				byte[] salt = new byte[9];
				new RNGCryptoServiceProvider().GetBytes (salt);
				string hashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile (UsersUtility.MakeHash(strPassword+strUserName.ToLower()) + Convert.ToBase64String (salt), "MD5");	    

				// Generate a unique key_code for registration purposes
				string keyCode = KanevaGlobals.GenerateUniqueString (20);

				// Create a registration Key
				string regKey = KanevaGlobals.GenerateUniqueString (50);

				// Get the local IP address
				string strHostName = Dns.GetHostName ();
				string strIP = "127.0.0.1";
				IPHostEntry ipEntry = Dns.GetHostByName (strHostName);
				IPAddress [] addr = ipEntry.AddressList;
				if (addr.Length > 0)
				{
					strIP = addr [0].ToString ();
				}

				int result = UsersUtility.InsertUser (strUserName, hashPassword, Convert.ToBase64String (salt), 2, 1,
					"kaneva", "kaneva",strUserName, "M", "", strUserName + "@kaneva.com", new DateTime (1973, 7, 21), keyCode,
					"US", "30328", regKey, strIP);

				if (result.Equals (0))
				{
					if (!CreateDefaultUserHomePage (strUserName, UsersUtility.GetUserIdFromUsername (strUserName)))
					{
						result = 99;
					}
				}

				// Did it pass?
				if (result.Equals (0))
				{
					bUserSuccess = true;

					m_logger.Info ("New User Success - Name = " + strUserName);

					// Use the new user to test upload
					_userId = UsersUtility.GetUserIdFromUsername (strUserName);
					_username = strUserName;
				}
				else
				{
					m_logger.Info ("New User Failure");
					// Failure
					MailUtility.SendEmail (KanevaGlobals.FromEmail, "monitors@kaneva.com", "Error Inserting User", "result = " + result, false);
				}
					
				// ****************************
				// Upload a photo
				// ****************************
				bool bSuccess = true;
				string strTestPhoto = System.Configuration.ConfigurationSettings.AppSettings ["TestPhoto"].ToString ();
				string errorMessage = AddFile (strTestPhoto, strIP, "Kaneva Test", "", "Kaneva Auto Test file upload", 0, (int) Constants.eASSET_PERMISSION.PRIVATE, true, ref bSuccess);					

				if (bSuccess)
				{
					bUploadSuccess = true;
					m_logger.Info ("Upload Success");
				}
				else
				{
					// Failure
					m_logger.Info ("Upload Failure");
					MailUtility.SendEmail (KanevaGlobals.FromEmail, "monitors@kaneva.com", "Error Inserting Photo", "result = " + errorMessage, false);
				}
					

				// ****************************
				// Upload a video
				// ****************************


				m_logger.Info ("Functionality END");
			}
			catch (Exception exc)
			{
				m_logger.Error ("Functionality exception", exc);

				// Failure
				MailUtility.SendEmail (KanevaGlobals.FromEmail, "monitors@kaneva.com", "Error in Web Site Check", exc.ToString (), false);
			}

			// Create a text file for Nagios to read
			try
			{
				// Create the string
				string results = DateTime.Now.ToShortDateString () + " " + DateTime.Now.ToShortTimeString () + 
					";User=" + ((bUserSuccess) ? "success" : "failure") + 
					";Upload=" + ((bUploadSuccess) ? "success" : "failure");

				string NagiosOutputFile = System.Configuration.ConfigurationSettings.AppSettings ["NagiosOutputFile"].ToString ();
				
				System.IO.StreamWriter fsOriginal = new System.IO.StreamWriter (NagiosOutputFile);
				fsOriginal.Write (results);
				fsOriginal.Close ();
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error in Web Site Check generating Nagios File", exc);
				MailUtility.SendEmail (KanevaGlobals.FromEmail, "monitors@kaneva.com", "Error in Web Site Check generating Nagios File", exc.ToString (), false);
			}

			// Clean up after
			bool bCleanup = Convert.ToBoolean (System.Configuration.ConfigurationSettings.AppSettings ["Cleanup"]);

			if (bCleanup)
			{
				if (bUserSuccess && (_userId != 4))
				{
					UsersUtility.UpdateUserStatus (_userId, (int) Constants.eUSER_STATUS.LOCKED);
				}
			}

		}

		/// <summary>
		/// Creates the default home page for a newly registered user. Places a set of default
		/// widgets on the page.
		/// </summary>
		/// <returns>bool - true on success</returns>
		private bool CreateDefaultUserHomePage (string username, int userId)
		{
			DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

			if (userId.Equals (0))
			{
				return false;
			}

			try
			{
				string module_title = "";

                //create a pesonal channel
                //pass true as first param to override number of channels limit
                int communityId = CommunityUtility.InsertCommunity(true, 0, 0,
                    username, "NEW", userId, username, "Y", "N", "Y", "Y", true,
                    (int)Constants.eCOMMUNITY_STATUS.ACTIVE);

                // Give each user a default home page
                int page_id = PageUtility.AddLayoutPage(true, communityId, "Home", 0, 0, 1);

				if ( page_id == 0)
				{
					return false;
				}


				// Create default set of widgets for new user

				// -- WIDGETS IN HEADER -- //


				// -- WIDGETS IN LEFT COLUMN --//

				// CONTROL PANEL MODULE
				//module_title = GetModuleTitle( (int)Constants.eMODULE_TYPE.CONTROL_PANEL);

				int control_panel_id = WidgetUtility.InsertLayoutModuleControlPanel(username);

				if ( control_panel_id == 0)
				{
					return false;
				}

				PageUtility.AddLayoutPageModule(control_panel_id, page_id, (int) Constants.eMODULE_TYPE.CONTROL_PANEL, (int) Constants.eMODULE_ZONE.BODY, 1);


				// FRIENDS MODULE
				module_title = GetModuleTitle( (int)Constants.eMODULE_TYPE.FRIENDS );

				int friend_module_id = WidgetUtility.InsertLayoutModuleFriends( module_title, Constants.DEFAULT_FRIEND_ENTRIES_PER_PAGE);

				if ( friend_module_id == 0)
				{
					return false;
				}

				PageUtility.AddLayoutPageModule(friend_module_id, page_id, (int) Constants.eMODULE_TYPE.FRIENDS, (int) Constants.eMODULE_ZONE.BODY, 2);


				// BLOG MODULE
				module_title = GetModuleTitle( (int)Constants.eMODULE_TYPE.BLOGS );

				int blog_module_id = WidgetUtility.InsertLayoutModuleBlogs( module_title);

				if ( blog_module_id == 0)
				{
					return false;
				}

				PageUtility.AddLayoutPageModule(blog_module_id, page_id, (int) Constants.eMODULE_TYPE.BLOGS, (int) Constants.eMODULE_ZONE.BODY, 3);


				// SLIDE SHOW
				module_title = GetModuleTitle( (int)Constants.eMODULE_TYPE.SLIDE_SHOW);

				int slide_show_module_id = WidgetUtility.InsertLayoutModuleSlideShow ( module_title );

				if ( slide_show_module_id == 0)
				{
					return false;
				}

				PageUtility.AddLayoutPageModule(slide_show_module_id, page_id, (int) Constants.eMODULE_TYPE.SLIDE_SHOW, (int) Constants.eMODULE_ZONE.BODY, 4);


				// VIDEO Catalog
				module_title = GetModuleTitle( (int)Constants.eMODULE_TYPE.VIDEO_PLAYER);

				int video_module_id = WidgetUtility.InsertLayoutModuleStores ( (int) Constants.eMODULE_TYPE.VIDEO_PLAYER, module_title);

				if ( video_module_id == 0)
				{
					return false;
				}

				PageUtility.AddLayoutPageModule(video_module_id, page_id, (int) Constants.eMODULE_TYPE.VIDEO_PLAYER, (int) Constants.eMODULE_ZONE.BODY, 5);


				// CHANNEL LIST
				module_title = GetModuleTitle( (int)Constants.eMODULE_TYPE.CHANNELS );

				int channel_module_id = WidgetUtility.InsertLayoutModuleChannels ( module_title );

				if ( channel_module_id == 0)
				{
					return false;
				}

				PageUtility.AddLayoutPageModule(channel_module_id, page_id, (int) Constants.eMODULE_TYPE.CHANNELS, (int) Constants.eMODULE_ZONE.BODY, 6);


				// -- WIDGETS IN RIGHT COLUMN -- //


				// VIDEO PLAYER
				module_title = GetModuleTitle( (int)Constants.eMODULE_TYPE.OMM_VIDEO_PLAYER );

				int video_player_module_id = WidgetUtility.InsertLayoutModuleOmmMedia( (int) Constants.eMODULE_TYPE.OMM_VIDEO_PLAYER, module_title );

				if ( video_player_module_id == 0)
				{
					return false;
				}

				PageUtility.AddLayoutPageModule(video_player_module_id, page_id, (int) Constants.eMODULE_TYPE.OMM_VIDEO_PLAYER, (int) Constants.eMODULE_ZONE.COLUMN, 1);


				// MULTIPLE PICTURES MODULE
				module_title = GetModuleTitle( (int)Constants.eMODULE_TYPE.MULTIPLE_PICTURES );

				int multiple_pictures_module_id = WidgetUtility.InsertLayoutModuleMultiplePictures( module_title, Constants.DEFAULT_MULTIPLE_PICTURES_PICTURES_PER_PAGE, Constants.DEFAULT_MULTIPLE_PICTURES_SHOW_IMAGE_TITLE, -1, (int)Constants.ePICTURE_SIZE.MEDIUM);

				if ( multiple_pictures_module_id == 0)
				{
					return false;
				}

				PageUtility.AddLayoutPageModule(multiple_pictures_module_id, page_id, (int) Constants.eMODULE_TYPE.MULTIPLE_PICTURES, (int) Constants.eMODULE_ZONE.COLUMN, 2);


				// PROFILE (INTERESTS) MODULE
				module_title = GetModuleTitle( (int)Constants.eMODULE_TYPE.PROFILE );

				int profile_module_id = WidgetUtility.InsertLayoutModuleProfile(module_title);

				if ( profile_module_id == 0)
				{
					return false;
				}

				PageUtility.AddLayoutPageModule (profile_module_id, page_id, (int) Constants.eMODULE_TYPE.PROFILE, (int) Constants.eMODULE_ZONE.COLUMN, 3);


				// COMMENTS MODULE
				module_title = GetModuleTitle( (int)Constants.eMODULE_TYPE.COMMENTS );

				int guestbook_module_id = WidgetUtility.InsertLayoutModuleComments( module_title, Constants.DEFAULT_COMMENT_MAX_PER_PAGE, Constants.DEFAULT_COMMENT_SHOW_DATE_TIME, Constants.DEFAULT_COMMENT_SHOW_PROFILE_PIC, Constants.DEFAULT_COMMENT_AUTHOR_ALIGNMENT, 1, 1);

				if ( guestbook_module_id == 0)
				{
					return false;
				}

				PageUtility.AddLayoutPageModule(guestbook_module_id, page_id, (int) Constants.eMODULE_TYPE.COMMENTS, (int) Constants.eMODULE_ZONE.COLUMN, 4);

				// Give each user two frieds groups. Family and Coworkers
				UsersUtility.InsertFriendGroup (userId, "Family");
				UsersUtility.InsertFriendGroup (userId, "Coworkers");
				UsersUtility.InsertFriendGroup (userId, "Inner Circle");

				return true;
			}
			catch(Exception e)
			{
				m_logger.Error (e.ToString ());
			}

			return false;
		}

		private string GetModuleTitle (int iAny)
		{
			return "Func Test";
		}

		/// <summary>
		/// AddFile
		/// </summary>
		private string AddFile (string filepath, string fromIPaddress, string title, string tags, string description, 
			int categoryId, int permission, bool isRestricted, ref bool bAllSuccess)
		{
			// Get the file
			//System.Drawing.Image imgOriginal = System.Drawing.Image.FromFile (filepath);
			System.IO.FileStream fsOriginal = new System.IO.FileStream (filepath, System.IO.FileMode.Open, System.IO.FileAccess.Read);


			string filename = "KanevaTestImage.jpg";
			int assetTypeId = (int) Constants.eASSET_TYPE.PICTURE;

			// Get the upload repository
			string uploadRepository = "";
			try 
			{
                uploadRepository = Configuration.UploadRepository;
			}
			catch (Exception e)
			{
				bAllSuccess = false;
				m_logger.Error ("Error reading config file", e);
				return "UploadRepository is not defined in web.config";
			}

			// Add the file
			try
			{
				int assetId = 0;		
				string newPath = CreateNewRecord (fromIPaddress, ref assetId, _userId, title, filename, assetTypeId, tags, fsOriginal.Length, 
					uploadRepository, categoryId, description, permission, isRestricted);

				// Make sure the directory exists
				FileInfo fileInfo = new FileInfo (newPath + Path.DirectorySeparatorChar);
				if (!fileInfo.Directory.Exists)
				{
					fileInfo.Directory.Create ();
				}

				//imgOriginal.Save (newPath + Path.DirectorySeparatorChar + filename);
				

				int maximumBufferSize = 4096;
				byte [] transferBuffer;

				if (fsOriginal.Length > maximumBufferSize)
				{
					transferBuffer = new byte [maximumBufferSize];
				}
				else
				{
					transferBuffer = new byte [fsOriginal.Length];
				}

				System.IO.FileStream fsNewFile = new System.IO.FileStream (newPath + Path.DirectorySeparatorChar + filename, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);

				int bytesRead;
				do 
				{
					bytesRead = fsOriginal.Read (transferBuffer, 0, transferBuffer.Length);
					fsNewFile.Write (transferBuffer, 0, bytesRead);
				}
				while (bytesRead > 0);

				fsNewFile.Close ();

				// For pictures, generate the thumbs
				if (assetTypeId.Equals ((int) Constants.eASSET_TYPE.PICTURE))
				{
					CreatePhotoThumbs (assetId, _userId, CommunityUtility.GetPersonalChannelId (_userId), newPath + Path.DirectorySeparatorChar + filename, filename);
				}
			}
			catch (UnauthorizedAccessException exc)
			{
				bAllSuccess = false;
				m_logger.Error ("Possible error with LibTorrentProxy.exe DCOM settings, allow user to launch", exc);
				return ("There was a fatal error posting the File \\'" + filename + "\\'. Please try again later.\\n");
			}
			catch (Exception exc)
			{
				bAllSuccess = false;
				m_logger.Error ("Error reading file", exc);
				return ("There was an error posting the File \\'" + filename + "\\'. Please try again.\\n");
			}
			finally 
			{
				fsOriginal.Close ();
			}

			return "";
		}

		/// <summary>
		/// Create new asset_upload and asset record
		/// </summary>
		/// <param name="user_id"></param>
		private string CreateNewRecord (string fromIPaddress, ref int assetId, int user_id, string itemName, string fileName, int assetTypeId, 
			string tags, long size, string uploadRepository, int categoryId, string description, int permission, bool isRestricted)
		{
			//create a new asset
			string newItemName = itemName;
			string newPath = "";

			/* Code taken from assetEdit page  */			 
			int isMature = (int) Constants.eASSET_RATING.GENERAL;
			if (isRestricted)
			{
				isMature = (int) Constants.eASSET_RATING.MATURE;
			}

			// Categories
			int category1Id = categoryId;

			// Create a new asset record
			assetId = StoreUtility.InsertAsset (assetTypeId, newItemName, user_id, _username, 
				(int) Constants.ePUBLISH_STATUS.UPLOADED, permission, isMature, category1Id, description);
			
			if (assetId > 0)
			{
				// Non pictures must be processed, pictures don't
				if (assetTypeId.Equals ((int) Constants.eASSET_TYPE.PICTURE))
				{
					newPath = Path.Combine (Path.Combine (KanevaGlobals.ContentServerPath, user_id.ToString ()), assetId.ToString ());
					StoreUtility.UpdateAssetFilePath (assetId, fileName, newPath, size);
					StoreUtility.UpdateAssetStatus (assetId, (int) Constants.eASSET_STATUS.ACTIVE);
					StoreUtility.UpdateAssetPublishStatus (assetId, (int) Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE, 0);
				}
				else
				{
					// Add non-pictures to the asset  upload table to be processed
					int assetUploadId = StoreUtility.InsertAssetUpload(user_id, fileName, uploadRepository, size, "", (int) Constants.eDS_INVENTORYTYPE.ASSETS, assetId);
					newPath = Path.Combine (Path.Combine (uploadRepository, user_id.ToString()), assetId.ToString());
					StoreUtility.UpdateAssetUploadPath (assetUploadId, newPath);
					StoreUtility.UpdateAssetUploadStatus (assetId, (int) Constants.ePUBLISH_STATUS.UPLOADED, fromIPaddress);
				}

				// Insert into user's personal channel
				int personalChannelId =  CommunityUtility.GetPersonalChannelId(_userId);
				StoreUtility.InsertAssetChannel (assetId,personalChannelId);
			}
			else
			{
				m_logger.Error ("Failed to insert asset record user " + user_id + " filename = " + fileName);
			}

			return newPath;
		}

		/// <summary>
		/// CreatePhotoThumbs
		/// </summary>
		private void CreatePhotoThumbs (int assetId, int userId, int communityId, string imagePath, string filename)
		{
			System.Drawing.Image imgOriginal = null;

			string contentRepository = "";
			if (KanevaGlobals.ContentServerPath != null)
			{
				contentRepository = KanevaGlobals.ContentServerPath;
			}
			else
			{
				m_logger.Error ("ContentServerPath not found generating Photo Thumbs");
				return;
			}

			string fileStoreHack = "";
			if (KanevaGlobals.FileStoreHack != "")
			{
				fileStoreHack = KanevaGlobals.FileStoreHack;
			}

			try
			{
				//Create an image object from a file on disk
				imgOriginal = System.Drawing.Image.FromFile (imagePath);
				
				// Save the image at the specified sizes
				// Save the original path
				StoreUtility.UpdateAssetThumb (assetId, fileStoreHack + userId.ToString () + "/" + assetId.ToString() + "/" + filename, "image_full_path");
								
				// Small
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_SMALL_HEIGHT, (int) Constants.PHOTO_THUMB_SMALL_WIDTH, "_sm", "thumbnail_small_path",
					filename, imagePath, contentRepository, userId, assetId);

				// Medium
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_MEDIUM_HEIGHT, (int) Constants.PHOTO_THUMB_MEDIUM_WIDTH, "_me", "thumbnail_medium_path",
					filename, imagePath, contentRepository, userId, assetId);

				// Large
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_LARGE_HEIGHT, (int) Constants.PHOTO_THUMB_LARGE_WIDTH, "_la", "thumbnail_large_path",
					filename, imagePath, contentRepository, userId, assetId);

				// XLarge
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_XLARGE_HEIGHT, (int) Constants.PHOTO_THUMB_XLARGE_WIDTH, "_xl", "thumbnail_xlarge_path",
					filename, imagePath, contentRepository, userId, assetId);

				// Asset Details
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_ASSETDETAILS_HEIGHT, (int) Constants.PHOTO_THUMB_ASSETDETAILS_WIDTH, "_ad", "thumbnail_assetdetails_path",
					filename, imagePath, contentRepository, userId, assetId);
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error reading image path is " + imagePath, exc);
				
			}
			finally 
			{
				if (imgOriginal != null)
				{
					imgOriginal.Dispose();
				}
			}
		}

		/// <summary>
		/// CreateTimers Method
		/// 
		/// The timer fires every 15 minutes (60000 * 15).
		/// </summary>
		private void CreateTimers () 
		{   
			// ****************************************
			// Email Notifications
			// ****************************************
			if (EmailNotificationsEnabled) 
			{			
				int notificationMinutes = Convert.ToInt32 (System.Configuration.ConfigurationSettings.AppSettings ["NotificationInterval"]);

				// Start it up
				m_logger.Info ("Starting Email Notifications every " + notificationMinutes + " minutes");
				tmrEmailNotification.Interval = (60000 * notificationMinutes);	// Minutes to run
				tmrEmailNotification.Elapsed += new System.Timers.ElapsedEventHandler (NotificationsTimer_Elapsed);
				tmrEmailNotification.Enabled = true;
			}

			// ****************************************
			// Summary Tables Updater
			// ****************************************			
			int sp_mintues_update_report_stats = 15;

			if (SummaryTablesUpdateEnabled) 
			{
				if (System.Configuration.ConfigurationSettings.AppSettings ["sp_mintues_update_report_stats"] != null)
				{
					sp_mintues_update_report_stats = Convert.ToInt32 (System.Configuration.ConfigurationSettings.AppSettings ["sp_mintues_update_report_stats"]);
				}

				// Run it once at startup
				try
				{
					m_logger.Info ("Summary Tables Updater - update_report_stats - START");
					Services.CallUpdateReportStats ();
					m_logger.Info ("Summary Tables Updater - update_report_stats - END");
				}
				catch (Exception exc)
				{
					m_logger.Error ("Summary Tables Updater exception", exc);
				}
				
				// Start it up
				m_logger.Info ("Starting Summary Tables Updater - update_report_stats every " + sp_mintues_update_report_stats + " minutes");
				tmrSummaryTables.Interval = (60000 * sp_mintues_update_report_stats);	// Minutes to run
				tmrSummaryTables.Elapsed += new System.Timers.ElapsedEventHandler (tmrSummaryTables_Elapsed);
				tmrSummaryTables.Enabled = true;
			}

			// ****************************************
			// Database Cleanup
			// ****************************************
			int sp_mintues_database_cleanup = 10;

			if (DatabaseCleanupEnabled)
			{
				if (System.Configuration.ConfigurationSettings.AppSettings ["sp_mintues_database_cleanup"] != null)
				{
					sp_mintues_database_cleanup = Convert.ToInt32 (System.Configuration.ConfigurationSettings.AppSettings ["sp_mintues_database_cleanup"]);
				}	

				// Run it once at startup
				try
				{
					m_logger.Info ("Database Cleanup - START");
					Services.CallDatabaseCleanup ();
					m_logger.Info ("Database Cleanup - END");
				}
				catch (Exception exc)
				{
					m_logger.Error ("Database Cleanup exception", exc);
				}

				// Start it up
				m_logger.Info ("Starting Database Cleanup to run on every " + sp_mintues_database_cleanup + " minutes");
				tmrDatabaseCleanup.Interval = (60000 * sp_mintues_database_cleanup);	// Minutes to run
				tmrDatabaseCleanup.Elapsed += new System.Timers.ElapsedEventHandler (tmrDatabaseCleanup_Elapsed);
				tmrDatabaseCleanup.Enabled = true;
			}


			// ****************************************
			// Video Image Generation
			// ****************************************
			int VideoImageGenerationInterval = 20;
			int AssetIdStart = 0;

			// Do we want to run it on this machine?
			if (VideoImageGenerationEnabled) 
			{
				if (System.Configuration.ConfigurationSettings.AppSettings ["VideoImageGenerationInterval"] != null)
				{
					VideoImageGenerationInterval = Convert.ToInt32 (System.Configuration.ConfigurationSettings.AppSettings ["VideoImageGenerationInterval"]);
				}

				if (System.Configuration.ConfigurationSettings.AppSettings ["AssetIdStart"] != null)
				{
					AssetIdStart = Convert.ToInt32 (System.Configuration.ConfigurationSettings.AppSettings ["AssetIdStart"]);
				}

				// Run it once at startup
				try
				{
					m_logger.Info ("Video image generation - START");
					GenerateVideoImages (AssetIdStart);
					m_logger.Info ("Video image generation - END");
				}
				catch (Exception exc)
				{
					m_logger.Error ("Video image generation exception", exc);
				}
				
				// Start up video image generation
				m_logger.Info ("Video ImageGeneration to run every " + VideoImageGenerationInterval + " minutes");
				tmrVideoImageGeneration.Interval = (60000 * VideoImageGenerationInterval);	// Minutes to run
				tmrVideoImageGeneration.Elapsed += new System.Timers.ElapsedEventHandler (tmrVideoImageGeneration_Elapsed);
				tmrVideoImageGeneration.Enabled = true;
			}

			if (FunctionalityCheckEnabled)
			{
				int FunctionalityCheckInterval = 60;

				if (System.Configuration.ConfigurationSettings.AppSettings ["FunctionalityCheckInterval"] != null)
				{
					FunctionalityCheckInterval = Convert.ToInt32 (System.Configuration.ConfigurationSettings.AppSettings ["FunctionalityCheckInterval"]);
				}

				m_logger.Info ("Functionality Check START");
				RunFunctionalityTest ();
				m_logger.Info ("Functionality Check END");

				// Start up video image generation
				m_logger.Info ("Functionality Check to run every " + FunctionalityCheckInterval + " minutes");
				tmrFunctionalityCheck.Interval = (60000 * FunctionalityCheckInterval);	// Minutes to run
				tmrFunctionalityCheck.Elapsed += new System.Timers.ElapsedEventHandler (FunctionalityCheckTimer_Elapsed);
				tmrFunctionalityCheck.Enabled = true;
			}

			if (KDPRewardsEnabled)
			{
				bool runAtStartup = false;

				if (System.Configuration.ConfigurationSettings.AppSettings ["KDPRewardsRunAtStartup"] != null)
				{
					runAtStartup = Convert.ToBoolean (System.Configuration.ConfigurationSettings.AppSettings ["KDPRewardsRunAtStartup"]);
				}

				if ( runAtStartup)
				{
					m_logger.Info ("KDP Rewards - START");
					RunKDPRewards();
					m_logger.Info ("KDP Rewards - END");
				}

				m_logger.Info ("KDP Rewards to run every day at midnight");
				tmrKDPRewards.Interval = GetMilliSecondsToMidnight() + 300000; // Add 5 minute fudge factor
				tmrKDPRewards.Elapsed += new System.Timers.ElapsedEventHandler (KDPRewardsTimer_Elapsed);
				tmrKDPRewards.Enabled = true;
			}

		}

		private double GetMilliSecondsToMidnight()
		{
			DateTime dtMidnight = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + 1, 0, 0, 0);
			TimeSpan ts = dtMidnight.Subtract(DateTime.Now);

			TimeSpan tsMidnight = new TimeSpan(ts.Hours, ts.Minutes, ts.Seconds);

			return tsMidnight.TotalMilliseconds;
		}

		/// <summary>
		/// GenerateVideoImages
		/// </summary>
		private void GenerateVideoImages (int assetIdStart)
		{
			DataTable dt = StoreUtility.GetVideoGenThumbs (assetIdStart, Int32.MaxValue, (int) Constants.eASSET_TYPE.VIDEO);

			if (ImageHelper.GenerateMediaThumbs (dt))
			{
				//m_Service_logger.Info ("Video Thumbs Generation SUCCEEDED");
			}
			else
			{
				m_logger.Error ("Video Thumbs Generation Failed");
			}

		}

		/// <summary>
		/// EmailNotificationsEnabled
		/// </summary>
		/// <returns></returns>
		private bool EmailNotificationsEnabled
		{
			get {return Convert.ToBoolean (System.Configuration.ConfigurationSettings.AppSettings ["enableEmailNotifications"]);}
		}

		/// <summary>
		/// VideoImageGenerationEnabled
		/// </summary>
		/// <returns></returns>
		private bool VideoImageGenerationEnabled
		{
			get {return Convert.ToBoolean (System.Configuration.ConfigurationSettings.AppSettings ["enableVideoImageGeneration"]);}
		}

		/// <summary>
		/// SummaryTablesUpdateEnabled
		/// </summary>
		/// <returns></returns>
		private bool SummaryTablesUpdateEnabled
		{
			get {return Convert.ToBoolean (System.Configuration.ConfigurationSettings.AppSettings ["enableSummaryTablesUpdate"]);}
		}

		/// <summary>
		/// DatabaseCleanupEnabled
		/// </summary>
		/// <returns></returns>
		private bool DatabaseCleanupEnabled
		{
			get {return Convert.ToBoolean (System.Configuration.ConfigurationSettings.AppSettings ["enableDatabaseCleanup"]);}
		}

		/// <summary>
		/// FunctionalityCheckEnabled
		/// </summary>
		/// <returns></returns>
		private bool FunctionalityCheckEnabled
		{
			get {return Convert.ToBoolean (System.Configuration.ConfigurationSettings.AppSettings ["enableFunctionalityCheck"]);}
		}

		/// <summary>
		/// KDPRewardsEnabled
		/// </summary>
		/// <returns></returns>
		private bool KDPRewardsEnabled
		{
			get {return Convert.ToBoolean (System.Configuration.ConfigurationSettings.AppSettings ["enableKDPRewards"]);}
		}

		/// <summary>
		/// KDPRewardsPrivateMsgSubject - subject of e-mail sent to winner
		/// </summary>
		/// <returns></returns>
		private string KDPRewardsPrivateMsgSubject
		{
			get
			{
				  if (System.Configuration.ConfigurationSettings.AppSettings ["KDPRewardsPrivateMsgSubject"] == null)
				  {
					  throw new Exception ("KDPRewardsPrivateMsgSubject not found in config file");
				  }
				  else
				  {
					  return System.Configuration.ConfigurationSettings.AppSettings ["KDPRewardsPrivateMsgSubject"];
				  }
		    } 
		}

		/// <summary>
		/// KDPRewardsPrivateMsg - body of e-mail sent to winner
		/// </summary>
		/// <returns></returns>
		private string KDPRewardsPrivateMsgBody
		{
			get
			{
				  if (System.Configuration.ConfigurationSettings.AppSettings ["KDPRewardsPrivateMsgBody"] == null)
				  {
					  throw new Exception ("KDPRewardsPrivateMsgBody not found in config file");
				  }
				  else
				  {
					  return System.Configuration.ConfigurationSettings.AppSettings ["KDPRewardsPrivateMsgBody"];
				  }
		    } 
		}

		/// <summary>
		/// KDPRewardsMsgFromId - who the reward recognition e-mails are sent from
		/// </summary>
		/// <returns></returns>
		private int KDPRewardsMsgFromId
		{
			get
			{
				  if (System.Configuration.ConfigurationSettings.AppSettings ["KDPRewardsMsgFromId"] == null)
				  {
					  throw new Exception ("KDPRewardsMsgFromId not found in config file");
				  }
				  else
				  {
					  return Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings ["KDPRewardsMsgFromId"]);
				  }
		    } 
		}

		/// <summary>
		/// KDPRewardsFriendBlastSubject - subject for the friend blast
		/// </summary>
		/// <returns></returns>
		private string KDPRewardsFriendBlastSubject
		{
			get
			{
				  if (System.Configuration.ConfigurationSettings.AppSettings ["KDPRewardsFriendBlastSubject"] == null)
				  {
					  throw new Exception ("KDPRewardsFriendBlastSubject not found in config file");
				  }
				  else
				  {
					  return System.Configuration.ConfigurationSettings.AppSettings ["KDPRewardsFriendBlastSubject"];
				  }
		    } 
		}

		/// <summary>
		/// KDPRewardsFriendBlastBody - body for the friend blast
		/// </summary>
		/// <returns></returns>
		private string KDPRewardsFriendBlastBody
		{
			get {return System.Configuration.ConfigurationSettings.AppSettings ["KDPRewardsFriendBlastBody"];}
		}

	    /// <summary>
		/// KDPRewardsMonthlyBlastSubject - subject for the monthly blast
		/// </summary>
		/// <returns></returns>
		private string KDPRewardsMonthlyBlastSubject
		{
			get
			{
				  if (System.Configuration.ConfigurationSettings.AppSettings ["KDPRewardsMonthlyBlastSubject"] == null)
				  {
					  throw new Exception ("KDPRewardsMonthlyBlastSubject not found in config file");
				  }
				  else
				  {
					  return System.Configuration.ConfigurationSettings.AppSettings ["KDPRewardsMonthlyBlastSubject"];
				  }
		    } 
		}

		/// <summary>
		/// KDPRewardsMonthlyBlastBody - body for the monthly blast
		/// </summary>
		/// <returns></returns>
		private string KDPRewardsMonthlyBlastBody
		{
			get {return System.Configuration.ConfigurationSettings.AppSettings ["KDPRewardsMonthlyBlastBody"];}
		}

	    /// <summary>
		/// KDPRewardsMonthlyBlastExpires - number of hours from now that the monthly KDP winner blast will expire
		/// </summary>
		/// <returns></returns>
		private double KDPRewardsMonthlyBlastExpires
		{
			get
			{
				  if (System.Configuration.ConfigurationSettings.AppSettings ["KDPRewardsMonthlyBlastExpires"] == null)
				  {
					  throw new Exception ("KDPRewardsMonthlyBlastExpires not found in config file");
				  }
				  else
				  {
					  return Convert.ToDouble(System.Configuration.ConfigurationSettings.AppSettings ["KDPRewardsMonthlyBlastExpires"]);

				  }
		    } 
		}

		private int _userId = 4;
		private string _username = "jason";

		/// <summary>
		/// Timers
		/// </summary>
		System.Timers.Timer tmrEmailNotification = new System.Timers.Timer ();
		System.Timers.Timer tmrSummaryTables = new System.Timers.Timer ();
		System.Timers.Timer tmrDatabaseCleanup = new System.Timers.Timer ();
		System.Timers.Timer tmrVideoImageGeneration = new System.Timers.Timer ();
		System.Timers.Timer tmrFunctionalityCheck = new System.Timers.Timer ();
		System.Timers.Timer tmrKDPRewards = new System.Timers.Timer ();


		/// <summary>
		/// Logger
		/// </summary>
		private static readonly ILog m_logger = LogManager.GetLogger (typeof (KanevaService));
	}
}
