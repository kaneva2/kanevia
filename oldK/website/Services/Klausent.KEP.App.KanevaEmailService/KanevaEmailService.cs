///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.IO;
using KlausEnt.KEP.Kaneva.Mailer;
using log4net;


namespace KlausEnt.KEP.Kaneva.KanevaEmailService
{
    public partial class KanevaEmailService : ServiceBase
    {
      static KanevaEmailService()
      {
        // Set up log for net
        string s = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
        FileInfo l_fi = new FileInfo(s);
        log4net.Config.XmlConfigurator.Configure(l_fi);
      }

      // The main entry point for the process
      static void Main()
      {
          System.ServiceProcess.ServiceBase[] ServicesToRun;
          ServicesToRun = new System.ServiceProcess.ServiceBase[] { new KanevaEmailService() };
          System.ServiceProcess.ServiceBase.Run(ServicesToRun);
      }
      
      public KanevaEmailService()
      {
          CreateTimers();   
          InitializeComponent();
      }


      protected override void OnStart(string[] args)
      {
          Logger.LogEmailServiceEvent("info","Start");                    

          //Start timers
          tmrQueuer.AutoReset = true;
          tmrQueuer.Enabled = true;
          tmrQueuer.Start();
          
          tmrChewer.AutoReset = true;
          tmrChewer.Enabled = true;
          //tmrChewer.Start();          
      }

      protected override void OnStop()
      {
        tmrQueuer.AutoReset = false;
        tmrQueuer.Enabled = false;
        Logger.LogEmailServiceEvent("info", "Queuer Stop");           

        tmrChewer.AutoReset = false;
        tmrChewer.Enabled = false;
        Logger.LogEmailServiceEvent("info", "Chewer Stop");           
      }

      /// <summary>
      /// CreateTimers Method
      /// 
      /// </summary>
      private void CreateTimers()
      {
        int ServiceInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ServiceInterval"]);

        Logger.LogEmailServiceEvent("info", "Run tasks every " + ServiceInterval + " minute(s).");          
          
        // Start Queuer                
        tmrQueuer.Interval = (60000 * ServiceInterval);	// Minutes to run
        tmrQueuer.Elapsed += new System.Timers.ElapsedEventHandler(QueuerTimer_Elapsed);
        tmrQueuer.Enabled = true;

        // Start Chewer                
        tmrChewer.Interval = (60000 * ServiceInterval);	// Minutes to run
        tmrChewer.Elapsed += new System.Timers.ElapsedEventHandler(ChewerTimer_Elapsed);
        tmrChewer.Enabled = true;


      }

      /// <summary>
      /// QueuerTimer_Elapsed
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      protected void QueuerTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
      {
        try
        {
          tmrQueuer.Stop();

          int queueCount = 0;

          if (QueuerEnabled && (DateTime.Now > NextQue))
          {
            Logger.LogEmailServiceEvent("info", "START Fill Queue");       
            queueCount = Mailer.Queuer.Queue();
            Logger.LogEmailServiceEvent("info", "END Fill Queue. Queued:" + queueCount);            
          }
          else
          {
            Logger.LogEmailServiceEvent("info", "SKIP Que. QueuerEnabled: " + QueuerEnabled.ToString() + " Next Que at: " + NextQue.ToString());            
          }
        
          int chewCount = 0;
          Logger.LogEmailServiceEvent("info", "START Empty Queue");
          chewCount = Mailer.Chewer.Chew();
          Logger.LogEmailServiceEvent("info", "END Empty Queue. Chewed: " + chewCount);          
          
          // if we are out of things to send then bump the timer to an hour.
          if (chewCount == 0 && queueCount == 0)
          {            
            tmrQueuer.Interval = (60000 * 2);
            Logger.LogEmailServiceEvent("info", "Going to sleep for 2 min.");            
          }
          else
          {
            int ServiceInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ServiceInterval"]);
            if (tmrQueuer.Interval > (60000 * ServiceInterval))
            {
              // set it back to normal if there were some results.            
              tmrQueuer.Interval = (60000 * ServiceInterval);	// Minutes to run
              Logger.LogEmailServiceEvent("info", "Going back to regular schedule.");
            }
          }


          // if nothing was queued set the next queue time for an hour
          if (queueCount == 0)
          {
            if (NextQue <= DateTime.Now)
            {
              NextQue = DateTime.Now.AddHours(1);
              Logger.LogEmailServiceEvent("info", "Setting next que to run in an hour.");
            }
          }
 
         


        }
        catch (Exception exc)
        {
          Logger.LogEmailServiceEvent("Error", "Queuer timer exception", exc);
        }
        finally
        {
          tmrQueuer.Start();                              
        }
      }


      /// <summary>
      /// ChewerTimer_Elapsed
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      protected void ChewerTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
      {
        try
        {
          tmrChewer.Stop();

          //m_logger.Info("START Empty Queue ");
          //Mailer.Chewer.Chew();
          //m_logger.Info("END Empty Queue");
        }
        catch (Exception exc)
        {
          Logger.LogEmailServiceEvent("Error", "Service timer exception", exc);         
        }
        finally
        {
          tmrChewer.Start();          
        }
      }


      System.Timers.Timer tmrQueuer = new System.Timers.Timer();
      System.Timers.Timer tmrChewer = new System.Timers.Timer();


      public bool QueuerEnabled = true;
      public DateTime NextQue = DateTime.Now;

      /// <summary>
      /// Logger
      /// </summary>
      private static readonly ILog m_logger = LogManager.GetLogger(typeof(KanevaEmailService));
    }
}
