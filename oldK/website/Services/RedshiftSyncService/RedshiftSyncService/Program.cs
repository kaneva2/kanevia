///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedshiftSyncService
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Out.WriteLine("Fetching users from Redshift...");
            var userIds = GetUserIdsToAward();
            Console.Out.WriteLine(userIds.Count + " ids found in Redshift!");

            if (userIds.Count > 0)
            {
                // Award the users. 
            }

            // Pause to close
            Console.Out.Write("Press \"ENTER\" to close...");
            Console.ReadLine();
        }

        static List<int> GetUserIdsToAward()
        {
            var userIds = new List<int>();
            using (var conn = new NpgsqlConnection(System.Configuration.ConfigurationManager.AppSettings["RedshiftConnectionString"]))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;

                    // Retrieve all rows
                    cmd.CommandText = "SELECT DISTINCT fwv_user_id " +
                                      "FROM fact_world_visits " +
                                      "WHERE fwv_visit_start >= DATEADD(day, -90, DATE(GETDATE()));";
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            userIds.Add(reader.GetInt32(0));
                        }
                    }
                }
            }
            return userIds;
        }
    }
}
