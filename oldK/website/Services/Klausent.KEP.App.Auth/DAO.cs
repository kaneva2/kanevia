///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.Collections;

using System.Data;
using KlausEnt.KEP.Kaneva;
using Kaneva.DataLayer.DataObjects;

namespace KEPAuth
{
	/// <summary>
	/// Retrieves user information from the data store for authorization.
	/// </summary>
	public class DAO
	{
		public DAO()
		{
		}

		/// <summary>
		/// Checks to see if a user is currently authorized to a game.
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="gameId"></param>
		/// <returns></returns>
        public Constants.eLOGIN_RESULTS getGameAuthorization(int userId, int gameId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityDeveloper();

            Constants.eLOGIN_RESULTS ret = Constants.eLOGIN_RESULTS.FAILED;

			// 9/08 updated to call SP so behavior can be site-specific.
			string sqlSelectString = " SELECT check_game_auth( " + userId + "," + gameId + ");";

			try
			{

				DataRow drAsset = dbUtility.GetDataRow (sqlSelectString, false);
                // will return the following possible values that map to the C# return codes
                // SUCCESS 1
                // NO_GAME_ACCESS 4 (old rc, not member of community, used if no servers) 
                // GAME_FULL 9
                // NOT_AUTHORIZED 10 (mature, etc.)
				if (drAsset [0] != DBNull.Value)
				{
					ret = (Constants.eLOGIN_RESULTS)Convert.ToInt32 (drAsset[0]);
				}
			}
			catch(Exception e)
			{
				Log.getInstance().writeLog (Log.Error,"Error", e.Message);
			}
			finally
			{
			}

			return ret;
		}
				
	}
}
