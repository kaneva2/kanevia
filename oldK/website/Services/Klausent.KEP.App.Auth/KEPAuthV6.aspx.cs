///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Data;
using System.Web.Security;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.Metrics;
using KlausEnt.KEP.Kaneva;
using Kaneva.DataLayer.DataObjects;
using System.Diagnostics;

namespace KEPAuth
{
    public partial class KEPAuthV6 : System.Web.UI.Page
    {

        /// <summary>
		/// OnInit
		/// </summary>
		/// <param name="e"></param>
        protected override void OnInit(System.EventArgs e)
        {
            if (Configuration.MetricPageTimingEnabled)
            {
                // Metric Timing
                Stopwatch stopwatch = new Stopwatch();
                this.Context.Items["MetricPageTiming"] = stopwatch;
                stopwatch.Start();
            }
        }

        /// <summary>
        /// OnPreRender
        /// </summary>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (Configuration.MetricPageTimingEnabled)
            {
                // Metric Timing
                Stopwatch stopwatch = (Stopwatch)this.Context.Items["MetricPageTiming"];
                stopwatch.Stop();

                TimeSpan ts = stopwatch.Elapsed;

                string pageName = Path.GetFileName(Page.Request.Url.AbsolutePath).ToUpper();

                // Is this a page to record metrics on
                if (Configuration.MetricsPageTiming.ContainsKey(pageName))
                {
                    PageTiming pt = (PageTiming)Configuration.MetricsPageTiming[pageName];
                    MetricsFacade metricsFacade = new MetricsFacade();

                    if (metricsFacade.CanLogPageMetrics(pt.URL.ToUpper ()))
                    {
                        string action = "";
                        if (Request.Params["action"] != null)
                        {
                            action = Request.Params["action"].ToUpper ();

                            if (pt.Action.ToUpper().IndexOf(action, 0) > -1)
                            {
                                metricsFacade.InsertMetricsPageTiming(pt.Name + " " + action, ts.TotalMilliseconds);
                            }
                        }

                        
                    }
                }
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["action"] == null)
            {
                string errorStr = "<Result code=\"-1\">\r\n<Description>action param not specified</Description>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            string actionreq = Request.Params["action"];


            if (actionreq.Equals("login"))
            {
                if ((Request.Params["userName"] == null) || (Request.Params["userIpAddress"] == null) || (Request.Params["password"] == null) || (Request.Params["gameId"] == null) || (Request.Params["serverId"] == null))
                {
                    string errorStr = "<Result code=\"-1\">\r\n<Description>userName, userIpAddress, password, gameId or serverId param not specified</Description>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                string username = "";
                string userIpAddress = "";
                string password = "";
                int gameId = 0;
                int serverId = 0;

                try
                {
                    username = Request.Params["userName"];
                    userIpAddress = Request.Params["userIpAddress"];
                    password = Request.Params["password"];
                    gameId = Convert.ToInt32(Request.Params["gameId"]);
                    serverId = Convert.ToInt32(Request.Params["serverId"]);
                }
                catch (Exception)
                {
                    string errorStr = "<Result code=\"-1\">\r\n<Description>Invalid parameter value passed in</Description>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                login(username, userIpAddress, password, gameId, serverId);


            }
            else if (actionreq.Equals("logout"))
            {

                if ((Request.Params["userId"] == null) || (Request.Params["reasonCode"] == null) || (Request.Params["serverId"] == null))
                {
                    string errorStr = "<Result code=\"-1\">\r\n<Description>userId, reasonCode  or serverId param not specified</Description>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                int userId = 0;
                int reasonCode = 0;
                int serverId = 0;

                try
                {
                    userId = Convert.ToInt32(Request.Params["userId"]);
                    reasonCode = Convert.ToInt32(Request.Params["reasonCode"]);
                    serverId = Convert.ToInt32(Request.Params["serverId"]);
                }
                catch (Exception)
                {
                    string errorStr = "<Result code=\"-1\">\r\n<Description>Invalid parameter value passed in</Description>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                logout(userId, reasonCode, serverId);
            }


        }

        DataTable GetCountries()
        {
            // Build a unique cache key
            string cacheKey = "KEPAuthSummaryCountries";
            DataTable dtCountries = (DataTable)Context.Cache[cacheKey];

            if (dtCountries == null)
            {
                // Add to the cache
                dtCountries = KanevaGlobals.GetDatabaseUtilityReadOnly().GetDataTable("SELECT * FROM kaneva.countries_to_summarize c");
                if (dtCountries != null && dtCountries.Columns["country"] != null)
                    Context.Cache.Insert(cacheKey, dtCountries, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtCountries;
        }





        public void login(string userName, string userIpAddress, string password, int gameId, int serverId)
        {
            string userInfo = "<user/>";

            UserFacade uf = new UserFacade();
            User u = uf.GetUser(userName);

            // Get the user from the databse
            if (u.UserId == 0)
            {
                Log.getInstance().writeLog(Log.Info, "auth_notfound", userName);

                string errorStr = "<Result code=\"2\">\r\n<Description>Not found</Description>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            GameFacade gf = new GameFacade();
            int accessId = gf.GetGameAccessByServerId(serverId);
            if (accessId == (int)eGAME_ACCESS.BANNED)
            {
                string errorStr = "<Result>\r\n  code=\"14\">\r\n<Description>Banned</Description>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }
            else if (accessId == (int)eGAME_ACCESS.PRIVATE)
            {
                // must be admin to get into private game
                // gamelogin.aspx enforces that earlier for 
                // better UX.  It also checks banned, but easy to enforce
                // here too, so do it.
            }

            string theirCountry = u.Country;
            u.Country = "US"; // default to US, filtering off

            // if their country not in the countries table, leave filtering off
            if (GetCountries() != null)
            {
                DataRow[] drCountry = GetCountries().Select("country = '" + theirCountry + "'");
                if (drCountry.Length > 0) // check their filter settings
                {
                    Preferences p = uf.GetUserPreferences(u.UserId);

                    if (p != null && p.FilterByCountry)
                    {
                        u.Country = theirCountry;
                    }
                }
            }

            // Get the hashed password
            string calcHashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(password + u.Salt, "MD5");

            Constants.eLOGIN_RESULTS ret = (Constants.eLOGIN_RESULTS)UsersUtility.Authorize(u.Username, calcHashPassword, gameId, u.Password, u.StatusId, false);

            // Was the correct password supplied?
            if (ret == Constants.eLOGIN_RESULTS.SUCCESS)
            {
                int roleId = 0;
                // if they are trying to authenticate against a game, check they have a subscription or bought it
                eLOGIN_RESULTS authCode = gf.LoginUser(u.UserId, userIpAddress, gameId, serverId, Context.Request.UserHostAddress, ref roleId);
                if (gameId > 0 && authCode != eLOGIN_RESULTS.SUCCESS)
                {
                    Log.getInstance().writeLog(Log.Info, "loginUser", "Bad auth code of " + authCode.ToString() + " for " + userName + " and gameId of " + gameId);
                    string errorStr = "<Result code=\"15\">\r\n<Description>Bad auth code of " + authCode.ToString() + " for " + userName + " and gameId of " + gameId + "</Description>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                Log.getInstance().writeLog(Log.Success, "auth_success", userName);

                // build the userInfo's XML 
                userInfo = "<user>";
                userInfo += "<u>" + u.UserId.ToString() + "</u>";
                userInfo += "<r>" + u.Role.ToString() + "</r>";  // this is Kaneva role
                userInfo += "<g>" + u.Gender + "</g>";

                DateTime bd = Convert.ToDateTime(u.BirthDate);
                string birthDateStr = bd.ToString("yyyy-MM-dd", System.Globalization.DateTimeFormatInfo.InvariantInfo); // get back to MySQL format

                userInfo += "<b>" + birthDateStr + "</b>";
                userInfo += "<m>" + (u.ShowMature ? "1" : "0") + "</m>";
                userInfo += "<c>" + u.Country + "</c>";
                userInfo += "<p>" + roleId + "</p>"; // game prive id
                userInfo += "</user>";

                string result = "<Result code=\"0\">\r\n<Description>OK</Description>\r\n<Data>\r\n" + userInfo + "</Data>\r\n</Result>";
                Response.Write(result);
                return;
            }
            // Has this account been deleted?
            else if (ret == Constants.eLOGIN_RESULTS.ACCOUNT_DELETED)
            {
                Log.getInstance().writeLog(Log.Info, "auth_deleted", userName);
                u.Role = 0;

                string errorStr = "<Result code=\"6\">\r\n  <Description>Account Deleted</Description>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }
            // Has this account been locked?
            else if (ret == Constants.eLOGIN_RESULTS.ACCOUNT_LOCKED)
            {
                Log.getInstance().writeLog(Log.Info, "auth_locked", userName);
                u.Role = 0;

                string errorStr = "<Result code=\"7\">\r\n<Description>Account Locked</Description>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }
            else
            {
                Log.getInstance().writeLog(Log.Failure, "auth_failure", userName);
                u.Role = 0;
                string errorStr = "<Result code=\"3\">\r\n<Description>Invalid Password</Description>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }
        }


        public void logout(int userId, int reasonCode, int serverId)
        {
            GameFacade gf = new GameFacade();
            eLOGIN_RESULTS authCode = gf.LogoutUser(userId, reasonCode, serverId, Context.Request.UserHostAddress);

            string errorStr = "<Result>\r\n  <ReturnCode>" + (int)authCode + "</ReturnCode>\r\n  <Description>Logout</Description>\r\n</Result>";
            Response.Write(errorStr);
            return;
        }
    }
}
