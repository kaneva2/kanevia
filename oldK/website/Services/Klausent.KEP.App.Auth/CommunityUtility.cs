///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using System.Web;
using System.Collections;
using System.Threading;

using KlausEnt.KEP.Kaneva.framework.widgets;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for CommunityUtility.
	/// </summary>
	public class CommunityUtility
	{
		static CommunityUtility()
		{
			m_DbNameKanevaNonCluster = KanevaGlobals.DbNameKanevaNonCluster;
		}


		/// <summary>
		/// GetCommunityTypes
		/// </summary>
		public static DataTable GetCommunityTypes ()
		{
			string sqlSelect = "SELECT * FROM community_categories WHERE category_id > 2";
			Hashtable parameters = new Hashtable ();			
			return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
		}


		/// <summary>
		/// GetBroadcastChannelImageURL
		/// </summary>
		/// <param name="imagePath">The relative path to the image on the NAS. Example - 4/howdown.gif</param>
		/// <param name="defaultSize">The default size of the image if the user has not uploaded a photo. Example - 'sm', 'me', 'la'</param>
		/// <returns></returns>
		public static string GetBroadcastChannelImageURL (string imagePath, string defaultSize)
		{
			if (imagePath.Length.Equals (0))
			{
				return KanevaGlobals.ImageServer + "/KanevaIconBroadband_" + defaultSize + ".gif";
			}
			else
			{
				return KanevaGlobals.ImageServer + "/" + imagePath;
			}
		}

		/// <summary>
		/// Get a communities
		/// </summary>
		/// <param name="orderby"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public static DataRow GetCommunity (int communityId)
		{
			return GetCommunity (communityId, Constants.eCOMMUNITY_STATUS.ACTIVE);
		}

		/// <summary>
		/// Get a communities
		/// </summary>
		/// <param name="orderby"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public static DataRow GetCommunity (int communityId, Constants.eCOMMUNITY_STATUS statusId)
		{
			string sqlSelect = "SELECT c.community_id, c.name, c.name_no_spaces, c.description, c.is_public, c.is_adult, c.over_21_required, c.created_date, " +
				" c.status_id, c.email, c.percent_royalty_paid, c.show_forums, c.show_store, c.show_servers, c.creator_id, " +
				" c.allow_free_items, c.allow_pay_items, c.show_server_uptime, c.show_server_ip, c.category_id, " + 
				" c.thumbnail_path, c.is_personal, c.template_id, " +
				" c.creator_username as username, c.keywords, c.allow_publishing, c.allow_member_events, c.url, " +
				" c.thumbnail_small_path, c.thumbnail_medium_path, c.thumbnail_large_path," +
				" cs.number_of_views, cs.number_of_members, cs.number_of_diggs, cs.number_times_shared, cs.number_of_pending_members, " +
				" u.zip_code, u.age, u.gender, u.country, u.location, u.mature_profile, us.number_of_friends as friends" +
				" FROM communities c " +
				" INNER JOIN channel_stats cs ON c.community_id = cs.channel_id " +
				" INNER JOIN users u ON u.user_id = c.creator_id " +
				" INNER JOIN users_stats us ON us.user_id = c.creator_id " +
				" WHERE c.status_id = @statusId " +
				" AND c.community_id = @communityId ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			parameters.Add ("@statusId", (int) statusId);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, true);
		}

		/// <summary>
		/// GetPublishChannels
		/// </summary>
		/// <returns></returns>
		public static PagedDataTable GetPublishChannels (int userId, string orderby)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string selectList = " c.community_id, c.name, c.is_personal, c.creator_id, c.creator_username, " +
				" c.creator_username as username, c.is_adult, c.is_public, c.keywords, c.description, " +
				" c.name_no_spaces, c.thumbnail_small_path, " +
				" cs.number_of_members ";

			string tableList = " communities c " +
				" INNER JOIN community_members cm ON c.community_id = cm.community_id " +
				" INNER JOIN channel_stats cs ON c.community_id = cs.channel_id ";

			string sqlSelect = "SELECT " + selectList +
				" FROM " + tableList +
				" WHERE c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE +
				" AND cm.user_id = @userId " +
				//" AND c.category_id NOT IN (" + (int) Constants.eCOMMUNITY_CATEGORIES.MAIN_TAB + ")" + 
				" AND cm.active_member = 'Y' " +
				" AND cm.status_id = " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE;

			sqlSelect += " UNION " +
				" SELECT " + selectList +
				" FROM " + tableList +
				" WHERE c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE +
				" AND cm.user_id = @userId " +
				//" AND c.category_id NOT IN (" + (int) Constants.eCOMMUNITY_CATEGORIES.MAIN_TAB + ")" + 
				" AND c.allow_publishing = 1 " +
				" AND cm.status_id = " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE;

			if (orderby.Trim ().Length > 0)
			{
				sqlSelect += " ORDER BY " + orderby;
			}

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@userId", userId);
			return dbUtility.GetPagedDataTableUnion (sqlSelect, "", parameters, 1, 100);
		}

        /// <summary>
        /// GetPublishChannels
        /// </summary>
        /// <returns></returns>
        public static DataTable GetPublishChannels(int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string selectList = " c.community_id, c.name, c.is_personal, c.creator_id, c.creator_username, " +
                " c.creator_username as username, c.is_adult, c.is_public, c.keywords, c.description, " +
                " c.name_no_spaces, c.thumbnail_small_path, " +
                " cs.number_of_members ";

            string tableList = " communities c " +
                " INNER JOIN community_members cm ON c.community_id = cm.community_id " +
                " INNER JOIN channel_stats cs ON c.community_id = cs.channel_id ";

            string sqlSelect = "SELECT " + selectList +
                " FROM " + tableList +
                " WHERE c.status_id = " + (int)Constants.eCOMMUNITY_STATUS.ACTIVE +
                " AND cm.user_id = @userId " +
                //" AND c.category_id NOT IN (" + (int) Constants.eCOMMUNITY_CATEGORIES.MAIN_TAB + ")" + 
                " AND cm.active_member = 'Y' " +
                " AND cm.status_id = " + (int)Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE;

            sqlSelect += " UNION " +
                " SELECT " + selectList +
                " FROM " + tableList +
                " WHERE c.status_id = " + (int)Constants.eCOMMUNITY_STATUS.ACTIVE +
                " AND cm.user_id = @userId " +
                //" AND c.category_id NOT IN (" + (int) Constants.eCOMMUNITY_CATEGORIES.MAIN_TAB + ")" + 
                " AND c.allow_publishing = 1 " +
                " AND cm.status_id = " + (int)Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE;

                sqlSelect += " ORDER BY c.name ";


            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);
            return KanevaGlobals.GetDatabaseUtility().GetDataTable(sqlSelect, parameters);
        }


		/// <summary>
		/// Get a community Id
		/// </summary>
		/// <param name="communityName"></param>
		/// <returns></returns>
		public static int GetCommunityIdFromName (string communityName, bool isPersonal)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			StringBuilder sqlSelect = new StringBuilder() ;
			sqlSelect.Append (" SELECT c.community_id ");

			if (isPersonal)
			{
				sqlSelect.Append (" FROM communities_personal c ");
			}
			else
			{
				sqlSelect.Append (" FROM communities_public c ");
			}
			
			sqlSelect.Append (" WHERE c.name_no_spaces = @communityName ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityName", communityName.Replace (" ", ""));
			DataRow drCommunity = dbUtility.GetDataRow (sqlSelect.ToString (), parameters, false);

			if (drCommunity != null && drCommunity ["community_id"] != DBNull.Value)
			{
				return Convert.ToInt32 (drCommunity ["community_id"]);
			}

			return 0;
		}

		/// <summary>
		/// Get a community
		/// </summary>
		/// <param name="urlName"></param>
		/// <returns></returns>
		public static DataRow GetChannelFromURL (string urlName)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlSelect = "SELECT c.community_id, c.name_no_spaces, is_personal " +
				" FROM communities c " +
				" WHERE c.url = @urlName";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@urlName", urlName);
			return dbUtility.GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// UpdateChannelURL
		/// </summary>
		/// <param name="communityId"></param>
		/// <param name="url"></param>
		public static int UpdateChannelURL (int communityId, string url)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			parameters.Add ("@communityId", communityId);
			parameters.Add ("@url", url);

			// Make sure this community name does not already exist
			// check is_personal flag too, only checking uniqueness within personal channels or topic channels
			string sqlString = "SELECT COUNT(*) " +
				" FROM communities where url = @url " +
				" AND community_id <> @communityId "; 
			int count = dbUtility.ExecuteScalar (sqlString, parameters);
			if (count > 0)
			{
				return 1;
			}

			string sqlSelect = " UPDATE communities " +
				" SET url = @url " +
				" WHERE community_id = @community_id";

			parameters = new Hashtable ();
			parameters.Add("@community_id", communityId);
			parameters.Add("@url", url);
			dbUtility.ExecuteNonQuery (sqlSelect, parameters);

			return 0;
		}

		public static void UpdateCommunityNotifications( int communityId, int userId, int notifications)
		{
			string sqlSelect = " UPDATE community_members " +
				" SET notifications = @notifications " +
				" WHERE community_id = @community_id AND user_id = @user_id ";

			Hashtable parameters = new Hashtable ();
			parameters.Add("@community_id", communityId);
			parameters.Add("@user_id", userId);
			parameters.Add("@notifications", notifications);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sqlSelect, parameters);
		}

		/// <summary>
		/// GetCommunityUsers
		/// </summary>
		public static PagedDataTable GetCommunityUsers (int communityId, int communityMemberStatusId, bool bIncludePending, bool bIncludeDeleted, bool bIncludeRejected, string filter, string orderby, int pageNumber, int pageSize)
		{
			Hashtable parameters = new Hashtable ();		

			string selectList = "cm.community_id, cm.account_type_id, cm.added_date, cm.status_id, " +
				" cm.notifications, cm.item_notify, cm.item_review_notify, cm.blog_notify, cm.blog_comment_notify, " +
				" cm.post_notify, cm.reply_notify, u.gender, u.birth_date, " +
				" u.username, u.email, u.show_email, u.user_id, u.email, u.show_online, " +
				" (u.online & u.show_online) as online, u.location, " +
				" com.name_no_spaces, com.thumbnail_small_path, com.thumbnail_medium_path, " +
				" cs.number_of_members, cs.number_of_views, cs.number_times_shared, cs.number_of_diggs ";

			string tableList = "users u, community_members cm,  communities_personal com " +
				" INNER JOIN channel_stats cs ON com.community_id = cs.channel_id ";
			
			string whereClause = "u.user_id = cm.user_id " +
				" AND cm.community_id = @communityId " +
				" AND com.creator_id = u.user_id ";

				// Did they want a specific status id?
				if (communityMemberStatusId > 0)
				{
					whereClause += " AND cm.status_id = @statusId ";
					parameters.Add ("@statusId", communityMemberStatusId);
				}
				else
				{
					// All Status's, but maybe not deleted or pending
					// Deleted?
					if (!bIncludeDeleted)
					{
						whereClause += " AND cm.status_id <> " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.DELETED;
					}
					// Pending?
					if (!bIncludePending)
					{
						whereClause += " AND cm.status_id <> " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.PENDING;
					}
					// Rejected?
					if (!bIncludeRejected)
					{
						whereClause += " AND cm.status_id <> " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.REJECTED;
					}
				}

			// Filter it?
			if (filter.Length > 0)
			{
				whereClause += " AND " + filter;
			}
			
			parameters.Add ("@communityId", communityId);
			return  KanevaGlobals.GetDatabaseUtility ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, 
					pageNumber, pageSize);
		}

		/// <summary>
		/// Update a community
		/// </summary>
        public static int UpdateCommunity (int communityId, string thumbnailPath, string thumbnailType)
        {
            return UpdateCommunity (communityId, thumbnailPath, thumbnailType, true);
        }
        
        /// <summary>
		/// Update a community
		/// </summary>
		public static int UpdateCommunity (int communityId, string thumbnailPath, string thumbnailType, bool hasThumbnail)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			
			string sqlString = "UPDATE communities " +
				" SET " +
				" thumbnail_path = @thumbnailPath," +
				" thumbnail_type = @thumbnailType, " +
				" has_thumbnail = @hasThumbnail " +
				" WHERE community_id = @communityId";

			Hashtable parameters = new Hashtable ();

			if (thumbnailPath.Trim ().Length.Equals (0))
			{
				parameters.Add ("@thumbnailPath", DBNull.Value);
				parameters.Add ("@hasThumbnail", 'N');
			}
            else if (!hasThumbnail) // this case added so Dance Party 3D default avatars don't show up in search as having thumbnail
            {
                parameters.Add ("@thumbnailPath", thumbnailPath);
                parameters.Add ("@hasThumbnail", 'N');
            }
            else
            {
                parameters.Add ("@thumbnailPath", thumbnailPath);
                parameters.Add ("@hasThumbnail", 'Y');
            }

			parameters.Add ("@thumbnailType", thumbnailType);
			parameters.Add ("@communityId", communityId);

			dbUtility.ExecuteNonQuery (sqlString, parameters);
			return 0;
		}

		/// <summary>
		/// Update a community
		/// </summary>
		public static int UpdateCommunity (int communityId, bool over21)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			
			string sqlString = "UPDATE communities " +
				" SET " +
				" over_21_required = @over21required " +
				" WHERE community_id = @communityId";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@communityId", communityId);
			parameters.Add ("@over21required", over21 ? "Y" : "N");
			dbUtility.ExecuteNonQuery (sqlString, parameters);
			return 0;
		}

		/// <summary>
		/// Update a community
		/// </summary>
		public static int DeleteCommunityThumbs (int communityId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			
			string sqlString = "UPDATE communities " +
				" SET " +
				" thumbnail_path = @thumbnailPath," +
				" thumbnail_type = @thumbnailType, " +
				" thumbnail_small_path = @thumbnailSmallPath," +
				" thumbnail_medium_path = @thumbnailMediumPath," +
				" thumbnail_large_path = @thumbnailLargePath," +
				" thumbnail_xlarge_path = @thumbnailXLargePath, " +
				" has_thumbnail = 'N' " +
				" WHERE community_id = @communityId";

			Hashtable parameters = new Hashtable ();

			parameters.Add ("@thumbnailPath", DBNull.Value);
			parameters.Add ("@thumbnailSmallPath", "");
			parameters.Add ("@thumbnailMediumPath", "");
			parameters.Add ("@thumbnailLargePath", "");
			parameters.Add ("@thumbnailXLargePath", "");
			parameters.Add ("@thumbnailType", "");
			parameters.Add ("@communityId", communityId);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
			return 0;
		}

		/// <summary>
		/// Update a community
		/// </summary>
		public static int UpdateCommunityThumb (int communityId, string thumbnailPath, string dbColumn)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			
			string sqlString = "UPDATE communities " +
				" SET " +
				dbColumn + " = @thumbnailPath " +
				" WHERE community_id = @communityId";

			Hashtable parameters = new Hashtable ();

			if (thumbnailPath.Trim ().Length.Equals (0))
			{
				parameters.Add ("@thumbnailPath", DBNull.Value);
			}
			else
			{
				parameters.Add ("@thumbnailPath", thumbnailPath);
			}

			parameters.Add ("@communityId", communityId);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
			return 0;
		}

		/// <summary>
		/// Update a community
		/// </summary>
		public static int UpdateCommunity (int communityId, int categoryId, string name, string description, 
			string isPublic, string isAdult, string showForums, string showStore, string showServers, string allowFreeItems,
			string allowPayItems, string showServerUptime, string showServerIp, int allowPublishing, bool isPersonal)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();
			string nameNoSpaces = name.Replace (" ", "");

			parameters.Add ("@communityId", communityId);
			parameters.Add ("@nameNoSpaces", nameNoSpaces);

			// Make sure this community name does not already exist
			// check is_personal flag too, only checking uniqueness within personal channels or topic channels

			string sqlString = "";

			if (isPersonal)
			{
				sqlString = "SELECT COUNT(*) " +
					" FROM communities_personal " +
					" WHERE name_no_spaces = @nameNoSpaces " +
					" AND community_id <> @communityId "; 
			}
			else
			{
				sqlString = "SELECT COUNT(*) " +
					" FROM communities_public " +
					" WHERE name_no_spaces = @nameNoSpaces " +
					" AND community_id <> @communityId "; 
			}

			int count = dbUtility.ExecuteScalar (sqlString, parameters);
			if (count > 0)
			{
				return 1;
			}

			sqlString = "UPDATE communities " +
				" SET " +
				" category_id = @categoryId," +
				" name_no_spaces = @nameNoSpaces," +
				" is_public = @isPublic," +
				" is_adult = @isAdult," +
				" show_forums = @showForums," +
				" show_store = @showStore," +
				" show_servers = @showServers," +
				" allow_free_items = @allowFreeItems," +
				" allow_pay_items = @allowPayItems," +
				" show_server_uptime = @showServerUptime," +
				" show_server_ip = @showServerIp," +
				" allow_publishing = @allowPublishing," +
				" status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE + "," +
				" last_edit = " + dbUtility.GetCurrentDateFunction () +
				" WHERE community_id = @communityId";

			parameters.Add ("@categoryId", categoryId);
			parameters.Add ("@isPublic", isPublic);
			parameters.Add ("@isAdult", isAdult);
			parameters.Add ("@showForums", showForums);
			parameters.Add ("@showStore", showStore);
			parameters.Add ("@showServers", showServers);
			parameters.Add ("@allowFreeItems", allowFreeItems);
			parameters.Add ("@allowPayItems", allowPayItems);
			parameters.Add ("@showServerUptime", showServerUptime);
			parameters.Add ("@showServerIp", showServerIp);
			parameters.Add ("@allowPublishing", allowPublishing);

			dbUtility.ExecuteNonQuery (sqlString, parameters);

			// Split out into two updates due to INNO db split and keeping Full Text Indexes
			UpdateCommunityFT (communityId, name, description);

			return 0;
		}

		/// <summary>
		/// UpdateCommunity
		/// </summary>
		public static int UpdateCommunityFT (int communityId, string name, string description)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			string sqlString = "UPDATE communities " +
				" SET " +
				" name = @name," +
				" description = @description " +
				" WHERE community_id = @communityId";

			parameters.Add ("@communityId", communityId);
			parameters.Add ("@name", name);
			parameters.Add ("@description", description);


			return dbUtility.ExecuteNonQuery (sqlString, parameters);
		}

		///<summary>
		///UpdateNewControlPanelTitle
		///</summary>
		public static int UpdateNewControlPanelTitle (string module_page_id, string title)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			string sqlString = "UPDATE layout_module_channel_control_panel " +
				" SET " +
				" title = @title" +
				" WHERE module_page_id = @module_page_id" +
				" AND title = 'new'";

			parameters.Add ("@module_page_id", module_page_id);
			parameters.Add ("@title", title);			

			return dbUtility.ExecuteNonQuery (sqlString, parameters);
		
		}


		/// <summary>
		/// InsertCommunity
		/// </summary>
		public static int InsertCommunity (bool bIsSiteAdmin, int categoryId, string name, string description, int creatorId, string creatorUsername,
			string isPublic, string isAdult, string showForums, string showStore, string showServers, 
			string allowFreeItems, string allowPayItems, string showServerUptime, string showServerIP, bool isPersonal)
		{
			return InsertCommunity (bIsSiteAdmin, categoryId, name, description, creatorId, creatorUsername,
				isPublic, isAdult, showForums, showStore, showServers, 
				allowFreeItems, allowPayItems, showServerUptime, showServerIP, isPersonal, 
				(int) Constants.eCOMMUNITY_STATUS.NEW);
		}


		/// <summary>
		/// Insert a new community
		/// </summary>
		public static int InsertCommunity (bool bIsSiteAdmin, int categoryId, string name, string description, int creatorId, string creatorUsername,
			string isPublic, string isAdult, string showForums, string showStore, string showServers, 
			string allowFreeItems, string allowPayItems, string showServerUptime, string showServerIP, bool isPersonal, int statusId)
		{
			if(isPersonal)
			{
				//if this is a personal channel, set status to be active instead of new
				statusId = (int) Constants.eCOMMUNITY_STATUS.ACTIVE;
			}

			string nameNoSpace = name.Replace(" ","");
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			parameters.Add ("@creatorId", creatorId);

			// Make sure they did not exceed thier community count.
			string sqlString = "SELECT count(*) FROM communities " +
				" WHERE creator_id = @creatorId " + 
				" AND status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE;

			int count = dbUtility.ExecuteScalar (sqlString, parameters);
			if ((count > KanevaGlobals.MaxNumberOfCummunitiesPerUser) && !bIsSiteAdmin)
			{
				return 2;
			}

			sqlString = "INSERT INTO communities ( " +
				" category_id, creator_id, " +
				" created_date, last_edit, status_id, " +
				" percent_royalty_paid, is_public, is_adult, show_forums, show_store, show_servers, " +
				" allow_free_items, allow_pay_items, show_server_uptime, show_server_ip, " +
				" db_connection_string, allow_publishing, is_personal, name_no_spaces, " +
                " name, description, creator_username ";

			sqlString += " ) VALUES (" +
				"@categoryId, @creatorId, " +
				dbUtility.GetCurrentDateFunction () + "," + dbUtility.GetCurrentDateFunction () + ", @statusId, " +
				KanevaGlobals.DefaultPercentRoyaltyPaid + ",@isPublic, @isAdult, @showForums, @showStore, @showServers," +
				"@allowFreeItems, @allowPayItems, @showServerUptime, @showServerIp," + 
				"NULL, 1, @isPersonal, @nameNoSpace, " +
                " @name, @description, @creatorUsername ";

			sqlString += ")";

			int communityId = 0; 

			parameters.Add ("@categoryId", categoryId);
			parameters.Add ("@isPublic", isPublic);
			parameters.Add ("@isAdult", isAdult);
			parameters.Add ("@showForums", showForums);
			parameters.Add ("@showStore", showStore);
			parameters.Add ("@showServers", showServers);
			parameters.Add ("@allowFreeItems", allowFreeItems);
			parameters.Add ("@allowPayItems", allowPayItems);
			parameters.Add ("@showServerUptime", showServerUptime);
			parameters.Add ("@showServerIp", showServerIP);
			parameters.Add ("@isPersonal", isPersonal ? 1 : 0);
			parameters.Add ("@statusId", statusId);
			parameters.Add ("@nameNoSpace", nameNoSpace);

            parameters.Add("@name", name);
            parameters.Add("@description", description);
            parameters.Add("@creatorUsername", creatorUsername);

			dbUtility.ExecuteIdentityInsert (sqlString, parameters, ref communityId);

			if (communityId > 0)
			{
				// Add the creator as a member
				InsertCommunityMember (communityId, creatorId, (int) Constants.eCOMMUNITY_ACCOUNT_TYPE.OWNER, "Y", 0, "Y", "Y", (int) Constants.eCOMMUNITY_ACCOUNT_TYPE.OWNER);

				// Update max values here due to sql limitations
				// int MaxRank = dbUtility.ExecuteScalar ("SELECT MAX(rank) + 1 FROM communities");
				// dbUtility.ExecuteNonQuery ("UPDATE communities SET rank = " + MaxRank + " WHERE community_id = " + communityId);
			}

			return communityId;
		}

		/// <summary>
		/// Insert a new community member
		/// </summary>
		/// <param name="communityId"></param>
		/// <param name="userId"></param>
		/// <param name="account_type_id"></param>
		/// <param name="newsletter"></param>
		/// <param name="invited_by_user_id"></param>
		/// <param name="allow_asset_uploads"></param>
		/// <param name="allow_forum_use"></param>
		public static int InsertCommunityMember (int communityId, int userId, int accountTypeId, string newsletter,
			int invitedByUserId, string allowAssetUploads, string allowForumUse, int statusId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();	
			string sInvitedByUser = (invitedByUserId == 0)? "NULL": invitedByUserId.ToString ();

			// Make sure this community name does not already exist
			string sqlString = "SELECT count(*) FROM community_members WHERE " +
				" user_id = @userId " +
				" AND community_id = @communityId ";

			parameters.Add ("@communityId", communityId);
			parameters.Add ("@userId", userId);
			int count = dbUtility.ExecuteScalar (sqlString, parameters);
			if (count > 0)
			{
				return 1;
			}

			// Add this user
			sqlString = "INSERT INTO community_members ( " +
				" community_id, user_id, account_type_id, added_date, " +
				" status_id, newsletter, invited_by_user_id, allow_asset_uploads, allow_forum_use" +
				" ) VALUES (" +
				" @communityId, @userId, @accountTypeId," + dbUtility.GetCurrentDateFunction () + "," +
				" @statusId, @newsletter," + sInvitedByUser + ",@allowAssetUploads, @allowForumUse" +
				")";

			parameters.Add ("@accountTypeId", accountTypeId);
			parameters.Add ("@newsletter", newsletter);
			parameters.Add ("@allowAssetUploads", allowAssetUploads);
			parameters.Add ("@allowForumUse", allowForumUse);
			parameters.Add ("@statusId", statusId);
			dbUtility.ExecuteNonQuery (sqlString, parameters);

			// Blast it
			if (statusId.Equals ((int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE))
			{
				DataRow drChannel = GetCommunity (communityId);

                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(userId);
				
				if(user.BlastPrivacyJoinCommunity)
				{
                    BlastFacade blastFacade = new BlastFacade();
                    blastFacade.SendCommunityJoinBlast(userId, user.Username, user.NameNoSpaces, drChannel["name"].ToString(), drChannel["name_no_spaces"].ToString());
				}
			}

			return 0;
		}

		/// <summary>
		/// Get a list of the available broadband channels
		/// that are public and broad band (non-personal) intended for pulldown
		/// </summary>
		/// <returns></returns>
		public static DataTable GetAllPublicChannels ()
		{
			return GetAllPublicChannels("");
		}

        /// <summary>
        /// Get a list of the available broadband channels
        /// that are public and broad band (non-personal) intended for pulldown
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAllPublicChannels(string filter)
        {
            string sqlSelect = "SELECT community_id, name_no_spaces, name, last_edit " +
                " FROM communities_public " +
                " WHERE is_public = @isPublic " +
                " AND status_id = " + (int)Constants.eCOMMUNITY_STATUS.ACTIVE;

            if ((filter != null) && (filter != ""))
            {
                sqlSelect += " AND " + filter;
            }

            Hashtable parameters = new Hashtable();
            parameters.Add("@isPublic", "Y");

            return KanevaGlobals.GetDatabaseUtility().GetDataTable(sqlSelect, parameters);
        }

		/// <summary>
		/// GetCommunitySubscripsions
		/// </summary>
		/// <returns></returns>
		public static DataTable GetCommunitySubscriptions (int communityId, bool onlyActive)
		{
			string sqlSelect = "SELECT community_subscription_id, community_id, name, description, subscription_amount, subscription_kei_point_id, " +
				" length_of_subscription, status_id, updated_date, created_date " +
				" FROM community_subscriptions " +
				" WHERE community_id = @communityId ";

			if (onlyActive)
			{
				sqlSelect += " AND status_id = " + (int) Constants.eCOMMUNITY_SUBSCRIPTION_STATUS.ACTIVE;
			}
			
			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
		}

		/// <summary>
		/// GetUserCommunitySubscriptionCount
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static int GetUserCommunitySubscriptionCount (int userId, bool onlyExpiringSoon)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlSelect = "SELECT COALESCE(COUNT(*),0) " +
				" FROM community_members cm, community_member_subscriptions cms " +
				" WHERE cm.user_id = @userId " +
				" AND cm.status_id = " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE +
				" AND cm.user_id = cms.user_id " +
				" AND cm.community_id = cms.community_id " +
				" AND cms.start_datetime <= " + dbUtility.GetCurrentDateFunction () + 
				" AND (cms.end_datetime >= " + dbUtility.GetCurrentDateFunction () + " OR cms.end_datetime IS NULL)" +
				" AND cms.status_id = " + (int) Constants.eCOMMUNITY_MEMBER_SUBSCRIPTION_STATUS.COMPLETED;

			if (onlyExpiringSoon)
			{
				sqlSelect += " AND cms.end_datetime > " + dbUtility.GetDatePlusDays (7);
			}

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@userId", userId);
			return KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sqlSelect, parameters);
		}

		/// <summary>
		/// GetCommunitySubscripsion
		/// </summary>
		/// <returns></returns>
		public static DataRow GetCommunitySubscription (int communitySubscriptionId)
		{
			string sqlSelect = "SELECT community_id, name, description, subscription_amount, subscription_kei_point_id, " +
				" length_of_subscription, status_id, updated_date, created_date " +
				" FROM community_subscriptions " +
				" WHERE community_subscription_id = @subId ";
				
			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@subId", communitySubscriptionId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// GetActiveCommunityMemberSubscripsion
		/// </summary>
		/// <returns></returns>
		public static DataRow GetActiveCommunityMemberSubscription (int communityId, int userId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlSelect = "SELECT cms.community_member_subscription_id, cms.community_id, cms.user_id, cms.community_subscription_id, cms.status_id " +
				" FROM community_members cm, community_member_subscriptions cms " +
				" WHERE cm.user_id = @userId " +
				" AND cm.community_id = @communityId " + 
				" AND cm.status_id = " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE +
				" AND cm.user_id = cms.user_id " +
				" AND cm.community_id = cms.community_id " +
				" AND cms.start_datetime <= " + dbUtility.GetCurrentDateFunction () + 
				" AND (cms.end_datetime >= " + dbUtility.GetCurrentDateFunction () + " OR cms.end_datetime IS NULL)" +
				" AND cms.status_id = " + (int) Constants.eCOMMUNITY_MEMBER_SUBSCRIPTION_STATUS.COMPLETED;
			
			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			parameters.Add ("@userId", userId);
			return dbUtility.GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// Insert a community subscription
		/// </summary>
		public static int InsertCommunitySubscription (int communityId, string name, string description, double subscriptionAmount, 
			int subscriptionLength, int statusId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "INSERT INTO community_subscriptions " +
				"(community_id, name, description, subscription_amount, subscription_kei_point_id, " +
				" length_of_subscription, status_id, updated_date, created_date " +
				") VALUES (" +
				"@communityId, @name, @description, @subscriptionAmount, 'KPoint'," +
				"@subscriptionLength, @statusId," + dbUtility.GetCurrentDateFunction () + "," + dbUtility.GetCurrentDateFunction () +
				")";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			parameters.Add ("@name", name);
			parameters.Add ("@description", description);
			parameters.Add ("@subscriptionAmount", subscriptionAmount);
			parameters.Add ("@subscriptionLength", subscriptionLength);
			parameters.Add ("@statusId", statusId);
			return dbUtility.ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// Update a community subscription
		/// </summary>
		public static int UpdateCommunitySubscription (int communityId, string name, int subscriptionLength, double subscriptionAmount, int statusId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// If it does not exist, just insert it 
			string sqlString = "SELECT COUNT(*) " +
				" FROM community_subscriptions " +
				" WHERE community_id = @communityId " +
				" AND length_of_subscription = @subscriptionLength ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			parameters.Add ("@subscriptionLength", subscriptionLength);
			int result = KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sqlString, parameters);

			if (result == 0)
			{
				return InsertCommunitySubscription (communityId, name, name, subscriptionAmount, subscriptionLength, statusId);
			}
		
			// Update it
			sqlString = "UPDATE community_subscriptions " +
				" SET subscription_amount = @subscriptionAmount " +
				" ,status_id = @statusId " +
				" ,updated_date = " + dbUtility.GetCurrentDateFunction () +
				" WHERE community_id = @communityId " +
				" AND length_of_subscription = @subscriptionLength";

			parameters.Add ("@subscriptionAmount", subscriptionAmount);
			parameters.Add ("@statusId", statusId);
			return dbUtility.ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// UpdateCommunityMember status
		/// </summary>
		public static int UpdateCommunityMember (int communityId, int userId, int statusId)
		{
			int result = 0;
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "UPDATE community_members SET " +
				" status_id = @statusId " +
				" WHERE community_id = @communityId " + 
				" AND user_id = @userId ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			parameters.Add ("@userId", userId);
			parameters.Add ("@statusId", statusId);
			result = dbUtility.ExecuteNonQuery (sqlString, parameters);

			// Blast it
			if (statusId.Equals ((int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE))
			{
				DataRow drChannel = GetCommunity (communityId);

                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(userId);
				if(user.BlastPrivacyJoinCommunity)
				{
                    BlastFacade blastFacade = new BlastFacade();
                    blastFacade.SendCommunityJoinBlast(userId, user.Username, user.NameNoSpaces, drChannel["name"].ToString(), drChannel["name_no_spaces"].ToString());
				}
			}

			return result;
		}

        /// <summary>
        /// AcceptAllPendingMembers
        /// </summary>
        public static int AcceptAllPendingMembers (int communityId)
        {
            int result = 0;
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlString = "UPDATE community_members SET " +
                " status_id = @statusId " +
                " WHERE community_id = @communityId " +
                " AND status_id = @oldStatusId ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@communityId", communityId);
            parameters.Add("@statusId", (int)Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE);
            parameters.Add("@oldStatusId", (int)Constants.eCOMMUNITY_MEMBER_STATUS.PENDING);
            result = dbUtility.ExecuteNonQuery(sqlString, parameters);
            return result;
        }

		/// <summary>
		/// UpdateCommunityMember status
		/// </summary>
		public static int UpdateCommunityMember (int communityId, int userId, int accountTypeId, string newsletter,
			string allowAssetUploads, string allowForumUse)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "UPDATE community_members SET " +
				" account_type_id = @accountTypeId," +
				" newsletter = @newsletter," +
				" allow_asset_uploads = @allowAssetUploads," +
				" allow_forum_use = @allowForumUse" +
				" WHERE community_id = @communityId " + 
				" AND user_id = @userId ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			parameters.Add ("@userId", userId);
			parameters.Add ("@accountTypeId", accountTypeId);
			parameters.Add ("@newsletter", newsletter);
			parameters.Add ("@allowAssetUploads", allowAssetUploads);
			parameters.Add ("@allowForumUse", allowForumUse);
			return dbUtility.ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// UpdateCommunityMember status
		/// </summary>
		public static void UpdateCommunityMember (int communityId, int userId, Constants.eCOMMUNITY_ACCOUNT_TYPE accountType)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "UPDATE community_members SET " +
				" account_type_id = @accountTypeId " +
				" WHERE community_id = @communityId " + 
				" AND user_id = @userId ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			parameters.Add ("@userId", userId);
			parameters.Add ("@accountTypeId", (int) accountType);
			int result = dbUtility.ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// DeleteAllCommunityMembers status
		/// </summary>
		public static int DeleteAllCommunityMembers (int communityId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "UPDATE community_members SET " +
				" status_id = " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.DELETED +
				" WHERE community_id = @communityId";
			
			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			dbUtility.ExecuteNonQuery (sqlString, parameters);

			return 0;
		}

		/// <summary>
		/// Get a community member
		/// </summary>
		/// <param name="communityId"></param>
		/// <param name="userId"></param>
		/// <returns></returns>
        [Obsolete("Use new Business Layer")]
		public static DataRow GetCommunityMember (int communityId, int userId)
		{
			string sqlString = "SELECT community_id, user_id, account_type_id, added_date, status_id, " +
				" newsletter, invited_by_user_id, allow_asset_uploads, allow_forum_use " +
				" FROM community_members WHERE " +
				" user_id = @userId " +
				" AND community_id = @communityId";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			parameters.Add ("@userId", userId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlString, parameters, false);
		}

		/// <summary>
		/// Delete a community
		/// </summary>
		/// <param name="assetId"></param>
		public static int DeleteCommunity (int communityId, int userId)
		{
			// Make sure they are an admin or owner of the community
			if (!IsCommunityOwner (communityId, userId))
			{
				return 1;
			}

			// Delete the members
			DeleteAllCommunityMembers (communityId);

			// Delete the assets
			DataTable dtAssets = StoreUtility.GetAssets (communityId);
			for (int i = 0; i < dtAssets.Rows.Count; i ++)
			{
				//StoreUtility.DeleteAsset (Convert.ToInt32 (dtAssets.Rows [i]["asset_id"]), userId);
				StoreUtility.RemoveAssetFromChannel (userId, Convert.ToInt32 (dtAssets.Rows [i]["asset_id"]), communityId);
			}

			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// Delete the community
			string sqlString = "UPDATE communities " +
				" SET status_id = " + (int) Constants.eCOMMUNITY_STATUS.DELETED + 
				" , last_edit = " + dbUtility.GetCurrentDateFunction () +
				" WHERE community_id = @communityId";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			return 0;
		}

		/// <summary>
		/// get channels to migrate thumbnails
		/// </summary>
		/// <returns></returns>
		public static DataTable GetChannelImagesGenThumbs (int isPersonal)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sb = "SELECT community_id, thumbnail_path, thumbnail_type, creator_id " +
				" FROM communities " +
				" WHERE thumbnail_path IS NOT NULL " +
				" AND is_personal = @isPersonal ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@isPersonal", isPersonal);
			return dbUtility.GetDataTable (sb, parameters);
		}

		/// <summary>
		/// get ContestProfileHeaders widgets to migrate Banners
		/// </summary>
		/// <returns></returns>
		public static DataTable GetContestProfileHeaderBanners ()
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sb = "SELECT lm.module_page_id, lm.banner, com.community_id " +
				" FROM layout_module_contest_profile_header lm " +
				" INNER JOIN layout_page_modules m ON m.module_page_id = lm.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" INNER JOIN communities com ON lp.channel_id = com.community_id " +
				" WHERE lm.banner IS NOT NULL " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.CONTEST_PROFILE_HEADER);
			return dbUtility.GetDataTable (sb, parameters);
		}

		/// <summary>
		/// get title widgets to migrate Banners
		/// </summary>
		/// <returns></returns>
		public static DataTable GetTitleBanners ()
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sb = "SELECT lm.module_page_id, lm.banner, com.community_id " +
				" FROM layout_module_title_text lm " +
				" INNER JOIN layout_page_modules m ON m.module_page_id = lm.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" INNER JOIN communities com ON lp.channel_id = com.community_id " +
				" WHERE lm.banner IS NOT NULL " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.TITLE_TEXT);
			return dbUtility.GetDataTable (sb, parameters);
		}

		/// <summary>
		/// Verify this user has access to the community
		/// </summary>
		/// <param name="communityId"></param>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static bool HasCommunityAccess (int communityId, int userId, bool isAuthenticated, bool IsAdult)
		{
			// Administrator always has rights to access
			if (UsersUtility.IsUserAdministrator ())
			{
				return true;
			}

			// Get the community in question 
			DataRow drCommunity = GetCommunity (communityId);


			// If it adult content, and logged in, and not an adult.
            if (drCommunity["is_adult"].ToString().Equals(Constants.CONTENT_TYPE_ADULT) && isAuthenticated && !IsAdult)
			{
				return false;
			}

			// If it is a pay community, see if they have a subscription
			// If it is a pay community, make sure they have an active subscription
			if (IsPayCommunity (communityId))
			{
				return HasActiveCommunitySubscription (communityId, userId);
			}

			// If private, make sure they have access
			if (!CommunityUtility.IsCommunityPublic (drCommunity))
			{
				// Make sure they are a member, owner, or moderator
				return (CommunityUtility.IsActiveCommunityMember (communityId, userId));
			}

			return true;
		}

		/// <summary>
		/// Is the specified community public?
		/// </summary>
		/// <param name="communityId"></param>
		/// <returns></returns>
		public static bool IsCommunityPublic (int communityId)
		{
			DataRow drCommunity = GetCommunity (communityId);
			return (IsCommunityPublic (drCommunity));
		}

		/// <summary>
		/// Is the specified community public?
		/// </summary>
		/// <param name="drCommunity"></param>
		/// <returns></returns>
		public static bool IsCommunityPublic (DataRow drCommunity)
		{
			return (IsCommunityPublic (drCommunity ["is_public"].ToString ()));
		}

		/// <summary>
		/// Is the specified community public?
		/// </summary>
		public static bool IsCommunityPublic (string IsPublic)
		{
			return (IsPublic.Equals ("Y"));
		}

		/// <summary>
		/// Is the community a pay community?
		/// </summary>
		/// <param name="communityId"></param>
		/// <returns></returns>
		public static bool IsPayCommunity (int communityId)
		{
			DataTable dtCommSubscriptions = GetCommunitySubscriptions (communityId, true);
			for (int i = 0; i < dtCommSubscriptions.Rows.Count; i ++)
			{
				if (Convert.ToInt32 (dtCommSubscriptions.Rows [i]["status_id"]).Equals ((int) Constants.eCOMMUNITY_SUBSCRIPTION_STATUS.ACTIVE) && Convert.ToDouble (dtCommSubscriptions.Rows [i]["subscription_amount"]) > 0.0)
				{
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Does the user have an active subscription to the community?
		/// </summary>
		/// <param name="communityId"></param>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static bool HasActiveCommunitySubscription (int communityId, int userId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

//			// It is a pay community
//			string sqlSelect = "SELECT COUNT(*) " +
//				" FROM community_members cm, community_member_subscriptions cms " +
//				" WHERE cm.user_id = " + userId +
//				" AND cm.community_id = " + communityId + 
//				" AND cm.status_id = " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE +
//				" AND cm.user_id = cms.user_id " +
//				" AND cm.community_id = cms.community_id " +
//				" AND cms.start_datetime <= " + dbUtility.GetCurrentDateFunction () + 
//				" AND (cms.end_datetime >= " + dbUtility.GetCurrentDateFunction () + " OR cms.end_datetime IS NULL)" +
//				" AND cms.status_id = " + (int) Constants.eCOMMUNITY_MEMBER_SUBSCRIPTION_STATUS.COMPLETED;

			DataRow drCommSub = GetActiveCommunityMemberSubscription (communityId, userId);
//
//			int result = dbUtility.ExecuteScalar (sqlSelect);
//			return (result > 0);
			return (drCommSub != null);
		}

		/// <summary>
		/// Is the user a communty member?
		/// </summary>
		/// <param name="communityId"></param>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static bool IsActiveCommunityMember (int communityId, int userId)
		{
			return IsActiveCommunityMember (communityId, userId, true);
		}

		/// <summary>
		/// Is the user a communty member?
		/// </summary>
		/// <param name="communityId"></param>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static bool IsActiveCommunityMember (int communityId, int userId, bool bAlwaysTrueIfAdmin)
		{
			// Administrator always has rights to access
			if (UsersUtility.IsUserAdministrator () && bAlwaysTrueIfAdmin)
			{
				return true;
			}

			// Everyone is active for watch/play/create/support
			if (IsKanevaMainCommunity (communityId))
			{
				return true;
			}

			// If it is a pay community, make sure they have an active subscription
			if (IsPayCommunity (communityId))
			{
				return HasActiveCommunitySubscription (communityId, userId);
			}

			string sqlSelect = "SELECT COUNT(*) " +
				" FROM community_members " +
				" WHERE user_id = @userId " +
				" AND community_id = @communityId " + 
				" AND status_id = " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE;

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@communityId", communityId);
			parameters.Add ("@userId", userId);
			int result = KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sqlSelect, parameters);
			return (result > 0);
		}

		/// <summary>
		/// Is the user a communtiy moderator
		/// </summary>
		/// <param name="role"></param>
		/// <returns></returns>
        //[Obsolete("Use new Business Layer")]
		public static bool IsCommunityModerator (int communityId, int userId)
		{
			return IsCommunityModerator (communityId, userId, true);
		}

		/// <summary>
		/// Is the user a communtiy moderator
		/// </summary>
		/// <param name="role"></param>
		/// <returns></returns>
        //[Obsolete("Use new Business Layer")]
		public static bool IsCommunityModerator (int communityId, int userId, bool bAlwaysTrueIfAdmin)
		{
			// Administrator always has rights to access
			if (bAlwaysTrueIfAdmin && UsersUtility.IsUserAdministrator ())
			{
				return true;
			}

			DataRow drCommuntiyUser = CommunityUtility.GetCommunityMember (communityId, userId);

			if (drCommuntiyUser != null)
			{
				int accountTypeID = Convert.ToInt32 (drCommuntiyUser ["account_type_id"]);
				int statusId = Convert.ToInt32 (drCommuntiyUser ["status_id"]);

				// Owner is also always a moderator
				if (statusId.Equals	((int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE) && (accountTypeID.Equals ((int) Constants.eCOMMUNITY_ACCOUNT_TYPE.MODERATOR) ||  accountTypeID.Equals ((int) Constants.eCOMMUNITY_ACCOUNT_TYPE.OWNER)))
				{
					return true;
				}
			}

			return (false);
		}

		/// <summary>
		/// Is the user a communtiy owner
		/// </summary>
		/// <param name="role"></param>
		/// <returns></returns>
		public static bool IsCommunityOwner (int communityId, int userId)
		{
			return IsCommunityOwner (communityId, userId, true);
		}

		/// <summary>
		/// Is the user a communtiy owner
		/// </summary>
		/// <param name="role"></param>
		/// <returns></returns>
		public static bool IsCommunityOwner (int communityId, int userId, bool bAlwaysTrueIfAdmin)
		{
			// Administrator always has rights to access
			if (bAlwaysTrueIfAdmin && UsersUtility.IsUserAdministrator ())
			{
				return true;
			}

			DataRow drCommuntiyUser = CommunityUtility.GetCommunityMember (communityId, userId);

			if (drCommuntiyUser != null)
			{
				int accountTypeID = Convert.ToInt32 (drCommuntiyUser ["account_type_id"]);
				int statusId = Convert.ToInt32 (drCommuntiyUser ["status_id"]);

				if (statusId.Equals	((int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE) && accountTypeID.Equals ((int) Constants.eCOMMUNITY_ACCOUNT_TYPE.OWNER))
				{
					return true;
				}
			}

			return (false);
		}

		/// <summary>
		/// TransferChannel
		/// </summary>
		public static bool TransferChannel (int channelId, int userId, int newOwnerId, int ownerId)
		{
			if (IsCommunityOwner (channelId, userId))
			{
				Hashtable parameters = new Hashtable ();
				DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

				// Make the old owner just a member
				UpdateCommunityMember (channelId, ownerId, Constants.eCOMMUNITY_ACCOUNT_TYPE.SUBSCRIBER);

                // Make the new owner owner
                UpdateCommunityMember(channelId, newOwnerId, Constants.eCOMMUNITY_ACCOUNT_TYPE.OWNER);

                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(newOwnerId);

				string sqlString = "UPDATE communities " +
					" SET creator_id = @ownerId, " +
                    " creator_username = @username " +
					" WHERE community_id = @communityId";
			
				parameters.Add ("@communityId", channelId);
				parameters.Add ("@ownerId", newOwnerId);
                parameters.Add("@username", user.Username);
				dbUtility.ExecuteNonQuery (sqlString, parameters);

				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Get the type of member from a given accountTypeId and status
		/// </summary>
		/// <param name="accountTypeId"></param>
		/// <returns></returns>
		public static string GetMemberTypeDescription (object accountTypeId, object cmStatusId)
		{
			if (accountTypeId != DBNull.Value)
			{
				// Make sure thier status is active
				if (cmStatusId != DBNull.Value)
				{
					switch ((int) Convert.ToInt32 (cmStatusId))
					{
						case ((int) Constants.eCOMMUNITY_MEMBER_STATUS.DELETED):
						{
							return "";
						}
						case ((int) Constants.eCOMMUNITY_MEMBER_STATUS.LOCKED):
						{
							return "*locked";
						}
						case ((int) Constants.eCOMMUNITY_MEMBER_STATUS.PENDING):
						{
							return "";
						}
					}
				}

				return GetMemberAccountTypeDescription (accountTypeId);
			}

			return "";
		}

		/// <summary>
		/// GetMemberAccountTypeDescription
		/// </summary>
		/// <param name="accountTypeId"></param>
		/// <returns></returns>
		public static string GetMemberAccountTypeDescription (object accountTypeId)
		{
			if (accountTypeId == DBNull.Value)
				return "";

			switch ((int) Convert.ToInt32 (accountTypeId))
			{
				case ((int) Constants.eCOMMUNITY_ACCOUNT_TYPE.OWNER):
				{
					return "*owner";
				}
				case ((int) Constants.eCOMMUNITY_ACCOUNT_TYPE.MODERATOR):
				{
					return "*moderator";
				}
				case ((int) Constants.eCOMMUNITY_ACCOUNT_TYPE.SUBSCRIBER):
				{
					return "*member";
				}
				default:
				{
					return "Unknown Account Type";
				}
			}
		}

		public static string GetMemberStatusIdDescription (object statusId)
		{
			// Make sure thier status is active
			if (statusId != DBNull.Value)
			{
				switch ((int) Convert.ToInt32 (statusId))
				{
					case ((int) Constants.eCOMMUNITY_MEMBER_STATUS.DELETED):
					{
						return "*deleted";
					}
					case ((int) Constants.eCOMMUNITY_MEMBER_STATUS.LOCKED):
					{
						return "*locked";
					}
					case ((int) Constants.eCOMMUNITY_MEMBER_STATUS.PENDING):
					{
						return "*pending";
					}
					case ((int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE):
					{
						return "*active";
					}
				}
			}
			return "Unknown";
		}

		/// <summary>
		/// Get the community status
		/// </summary>
		/// <param name="isPublic"></param>
		/// <returns></returns>
		public static string GetStatus (string isPublic)
		{
			if (Convert.ToString (isPublic).Equals ("Y"))
			{
				return "Public";
			}
			else
			{
				return "Private";
			}
		}

		/// <summary>
		/// GetPendingCommunityMembersForUser
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static int GetPendingCommunityMembersForUser (int userOwnderId)
		{
			string sqlSelect = "SELECT COALESCE(COUNT(*),0) " +
				" FROM community_members cm, communities c " +
				" WHERE c.creator_id = @userId " + 
				" AND c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE + 
				" AND cm.community_id = c.community_id " +
				" AND cm.status_id = " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.PENDING;

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@userId", userOwnderId);
			return KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sqlSelect, parameters);
		}

		/// <summary>
		/// GetPendingCommunitiesForUser
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static int GetPendingCommunitiesForUser (int userId)
		{
			string sqlSelect = "SELECT COALESCE(COUNT(*),0) " +
				" FROM community_members cm, communities c " +
				" WHERE cm.user_id = @userId " + 
				" AND c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE + 
				" AND cm.community_id = c.community_id " +
				" AND cm.status_id = " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.PENDING;

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@userId", userId);
			return KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sqlSelect, parameters);
		}

		/// <summary>
		/// UpdateChannelTags that are stored in the community table
		/// </summary>
		/// <param name="channelId"></param>
		public static void UpdateChannelTags (int channelId, string strKeywords)
		{
			string sqlUpdate = "UPDATE communities SET keywords = @keywords WHERE community_id = @channelId";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channelId", channelId);
			parameters.Add ("@keywords", strKeywords.Trim ());
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlUpdate, parameters);
		}

		/// <summary>
		/// GetChannelTags
		/// </summary>
		/// <returns></returns>
		private static DataTable GetChannelTags (int channelId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlSelect = "SELECT c.keywords " +
				" FROM communities c " +
				" WHERE c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE +
				" AND c.community_id = @channelId";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channelId", channelId);
			return dbUtility.GetDataTable (sqlSelect, parameters);
		}

		/// <summary>
		/// GetCommunityStats
		/// </summary>
		/// <param name="communityId"></param>
		/// <returns></returns>
		public static DataRow GetCommunityStats (int communityId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string strSQL = " SELECT u.signup_date, u.last_login, u.second_to_last_login, " +
				" cs.number_of_views, cs.number_of_members, cs.number_times_shared, 0 as number_of_topics, " +
				" SUM(ac.count) as num_assets, " +
				" c.creator_id, c.creator_username, c.created_date, c.last_edit, c.last_update " +
				" FROM summary_assets_by_type_by_channel ac " +
				" INNER JOIN communities c ON c.community_id = ac.channel_id " +
				" INNER JOIN channel_stats cs ON cs.channel_id = c.community_id " +
				" INNER JOIN users u ON u.user_id = c.creator_id " +
				" WHERE c.community_id = @communityId " +
				" GROUP BY ac.channel_id " ;

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@communityId", communityId);
			return dbUtility.GetDataRow (strSQL, parameters, false); 
		}

		/// <summary>
		/// IsKanevaMainCommunity
		/// </summary>
		public static bool IsKanevaMainCommunity (int communityId)
		{
			// Everyone is active for watch/play/create/support
			if (communityId == (int) Constants.eCOMMUNITIES.PLAY || communityId == (int) Constants.eCOMMUNITIES.WATCH || communityId == (int) Constants.eCOMMUNITIES.CREATE || communityId == (int) Constants.eCOMMUNITIES.SUPPORT || communityId == (int) Constants.eCOMMUNITIES.FILMMAKERS)
			{
				return true;
			}

			return false;
		}

		/// <summary>
		/// returns true if community exists
		/// </summary>
		/// <param name="communityId"></param>
		/// <returns></returns>
		public static bool IsCommunityValid(int communityId)
		{
			string sqlSelect = " SELECT COUNT(*) " +
				" FROM communities " +
				" WHERE community_id = @community_id";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@community_id", communityId);

			return KanevaGlobals.GetDatabaseUtility().ExecuteScalar(sqlSelect, parameters) > 0;
		}

//		/// <summary>
//		/// GetChannelTypeFilterSQL
//		/// </summary>
//		public static string GetChannelTypeFilterSQL (Constants.eCHANNEL_FILTER_TYPE type, bool bIncludeAlias)
//		{
//			switch (type)
//			{
//				case Constants.eCHANNEL_FILTER_TYPE.OWNER:
//					if (bIncludeAlias)
//					{
//						return " (cm.account_type_id = " +  (int) Constants.eCOMMUNITY_ACCOUNT_TYPE.OWNER + " AND cm.status_id = " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE + ")";
//					}
//					else
//					{
//						return " (account_type_id = " +  (int) Constants.eCOMMUNITY_ACCOUNT_TYPE.OWNER + " AND cm_status_id = " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE + ")";
//					}
//
//				case Constants.eCHANNEL_FILTER_TYPE.SUBSCRIBER:
//					if (bIncludeAlias)
//					{
//						return " (cm.account_type_id = " +  (int) Constants.eCOMMUNITY_ACCOUNT_TYPE.SUBSCRIBER + " OR cm.account_type_id = " +  (int) Constants.eCOMMUNITY_ACCOUNT_TYPE.MODERATOR + " OR cm.account_type_id = " + (int) Constants.eCOMMUNITY_ACCOUNT_TYPE.OWNER + ") AND cm.status_id = " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE;
//					}
//					else
//					{
//						return " (account_type_id = " +  (int) Constants.eCOMMUNITY_ACCOUNT_TYPE.SUBSCRIBER + " OR account_type_id = " +  (int) Constants.eCOMMUNITY_ACCOUNT_TYPE.MODERATOR + " OR account_type_id = " + (int) Constants.eCOMMUNITY_ACCOUNT_TYPE.OWNER + ") AND cm_status_id = " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE;
//					}
//
//				case Constants.eCHANNEL_FILTER_TYPE.PUBLIC:
//					if (KanevaWebGlobals.ShowMature ())
//					{
//						return " is_public = 'Y'";
//					}
//					else
//					{
//						return " is_public = 'Y' and is_adult <> 'Y'";
//					}
//
//				case Constants.eCHANNEL_FILTER_TYPE.PRIVATE:
//					if (KanevaWebGlobals.ShowMature ())
//					{
//						return " is_public = 'N'";
//					}
//					else
//					{
//						return " is_public = 'N' and is_adult <> 'Y'";
//					}
//
//				case Constants.eCHANNEL_FILTER_TYPE.ADULT:
//
//					return "is_adult = 'Y'";
//
//				case Constants.eCHANNEL_FILTER_TYPE.TEEN:
//					return " is_adult = 'N'";
//
//				case Constants.eCHANNEL_FILTER_TYPE.GENERAL:
//					return " is_adult = 'C'";
//
//				case Constants.eCHANNEL_FILTER_TYPE.MODERATOR:
//					if (bIncludeAlias)
//					{
//						return " (cm.account_type_id = " +  (int) Constants.eCOMMUNITY_ACCOUNT_TYPE.MODERATOR + " AND cm.status_id = " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE + ")";;
//					}
//					else
//					{
//						return " (account_type_id = " +  (int) Constants.eCOMMUNITY_ACCOUNT_TYPE.MODERATOR + " AND cm_status_id = " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE + ")";;
//					}
//			
//				default:
//				{
//					if (KanevaWebGlobals.ShowMature ())
//					{
//						return "";
//					}
//					else
//					{
//						return " is_adult <> 'Y'";
//					}
//				}
//			}
//		}

		/// <summary>
		/// GetChannelFilterType
		/// </summary>
		public static Constants.eCHANNEL_FILTER_TYPE GetChannelFilterType (int type)
		{
			switch (type)
			{
				case (int) Constants.eCHANNEL_FILTER_TYPE.OWNER:
					return Constants.eCHANNEL_FILTER_TYPE.OWNER;

				case (int) Constants.eCHANNEL_FILTER_TYPE.SUBSCRIBER:
					return Constants.eCHANNEL_FILTER_TYPE.SUBSCRIBER;

				case (int) Constants.eCHANNEL_FILTER_TYPE.PUBLIC:
					return Constants.eCHANNEL_FILTER_TYPE.PUBLIC;

				case (int) Constants.eCHANNEL_FILTER_TYPE.PRIVATE:
					return Constants.eCHANNEL_FILTER_TYPE.PRIVATE;

				case (int) Constants.eCHANNEL_FILTER_TYPE.ADULT:
					return Constants.eCHANNEL_FILTER_TYPE.ADULT;

				case (int) Constants.eCHANNEL_FILTER_TYPE.TEEN:
					return Constants.eCHANNEL_FILTER_TYPE.TEEN;

				case (int) Constants.eCHANNEL_FILTER_TYPE.GENERAL:
					return Constants.eCHANNEL_FILTER_TYPE.GENERAL;

				case (int) Constants.eCHANNEL_FILTER_TYPE.MODERATOR:
					return Constants.eCHANNEL_FILTER_TYPE.MODERATOR;
			
				default:
				{
					return Constants.eCHANNEL_FILTER_TYPE.DEFAULT;
				}
			}
		}

		/// <summary>
		/// GetUserCommunityMembershipSql
		/// </summary>
		public static string GetUserCommunityMembershipSql (int userId)
		{
			string sqlString = "SELECT c.community_id " +
				" FROM communities c LEFT OUTER JOIN community_members cm ON c.community_id = cm.community_id " +
			    " WHERE cm.user_id = " + userId;
    
			return sqlString;
		}

		// **********************************************************************************************
		// Searching
		// **********************************************************************************************
		#region Search Functions

		public static double GetChannelCount ()
		{
			string sqlSelect = "SELECT COUNT(*)" +
				" FROM communities_public c " +
				" WHERE c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE +
				" AND c.community_id NOT IN (" + (int) Constants.eCOMMUNITIES.WATCH + ", " + (int) Constants.eCOMMUNITIES.PLAY + ", " + (int) Constants.eCOMMUNITIES.CREATE + ", " + (int) Constants.eCOMMUNITIES.SUPPORT + ", " + (int) Constants.eCOMMUNITIES.FILMMAKERS + ")";

			return KanevaGlobals.GetDatabaseUtility().ExecuteScalarDouble (sqlSelect, new Hashtable ());
		}

		/// <summary>
		/// GetWOKHangoutCount
		/// </summary>
		/// <returns></returns>
		public static double GetWOKHangoutCount ()
		{
			string sqlSelect = "SELECT COUNT(community_id) " +
				" FROM wok.hangout_cache ";

			return KanevaGlobals.GetDatabaseUtility().ExecuteScalarDouble (sqlSelect, new Hashtable ());
		}

		/// <summary>
		/// Get Communities for notifications
		/// </summary>
		public static PagedDataTable GetCommunitiesForNotifications ( 
			string filter, string orderBy, int pageNumber, int pageSize)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly ();
			Hashtable parameters = new Hashtable ();

			string sqlSelectList = "DISTINCT c.community_id, " +
				" c.name, c.description, c.is_public, c.is_adult, c.keywords, c.over_21_required, " +
				" c.creator_id, c.creator_username as username, c.created_date, " +
				" c.name_no_spaces, c.thumbnail_small_path, " +
				" creator_c.name_no_spaces as creator_name_no_spaces, creator_c.thumbnail_small_path as creator_thumbnail_small_path, " +
				" cs.number_of_members, cs.number_of_views, cs.number_times_shared, cs.number_of_diggs ";

			string sqlTableList = "communities_personal creator_c, communities_public c " +
				" INNER JOIN channel_stats cs ON cs.channel_id = c.community_id ";

			string sqlWhereClause = " c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE +
				" AND c.community_id NOT IN (" + (int) Constants.eCOMMUNITIES.WATCH + ", " + (int) Constants.eCOMMUNITIES.PLAY + ", " + (int) Constants.eCOMMUNITIES.CREATE + ", " + (int) Constants.eCOMMUNITIES.SUPPORT + ", " + (int) Constants.eCOMMUNITIES.FILMMAKERS + ")" +
				" AND creator_c.creator_id = c.creator_id ";

			if (filter.Length > 0)
			{
				sqlWhereClause += " AND " + filter;
			}

			return dbUtility.GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, orderBy, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// Search Communities
		/// </summary>
		public static PagedDataTable SearchCommunitiesNew (bool bGetMature, bool bGetOver21, string searchString, 
			bool bOnlyWithVideos, bool bOnlyWithMusic, bool bOnlyWithPhotos, 
			string country, string zipCode, int zipMiles, 
			int numberOfMembersMin, int numberOfMembersMax, int pastDays, int pastDaysUpdated,
			string filter, string orderBy, int pageNumber, int pageSize)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilitySearch ();
			Hashtable parameters = new Hashtable ();

			string sqlSelectList = " scr.community_id, " +
				" scr.name, scr.description, scr.is_public, scr.is_adult, scr.keywords, scr.over_21_required, " +
				" scr.creator_id, scr.username, scr.created_date, " +
				" scr.name_no_spaces, scr.thumbnail_small_path, " +
				" scr.creator_name_no_spaces, scr.creator_thumbnail_small_path, " +
				" scr.number_of_members, scr.number_of_views, scr.number_times_shared, scr.number_of_diggs ";

			string sqlTableList = " searchable_communities sc INNER JOIN searchable_communities_results scr ON sc.community_id = scr.community_id ";

			string sqlWhereClause = " 1=1 ";

			if (searchString.Length > 0)
			{
				// If no order by, do relevancy
				if (orderBy.Length.Equals (0))
				{
					parameters.Add ("@searchStringOrig", searchString);
					sqlSelectList += ", MATCH (sc.name,sc.description,sc.creator_username,sc.keywords) AGAINST (@searchStringOrig) as rel";
					orderBy = " rel DESC ";
				}

				char [] splitter  = {' '};
				string [] arKeywords = null;

				// Did they enter multiples?
				arKeywords = searchString.Split (splitter);

				if (arKeywords.Length > 1)
				{
					searchString = "";
					for (int j = 0; j < arKeywords.Length; j ++)
					{
						searchString += arKeywords [j].ToString () + "* ";
					}
				}
				else
				{
					searchString = searchString + "*";
				}

				sqlWhereClause += " AND MATCH (sc.name,sc.description,sc.creator_username,sc.keywords) AGAINST (@searchString IN BOOLEAN MODE)";
				parameters.Add ("@searchString", searchString);
			}

			// Filter
			if (!bGetMature)
			{
				sqlWhereClause += " AND sc.contain_mature <> 'Y'";
			}

			// Filter
			if (!bGetOver21)
			{
				sqlWhereClause += " AND sc.over_21_required = 'N'";
			}

			// Distance or country
			if (country.Length > 0)
			{
				// Country
				sqlTableList += " ,users u ";
				sqlWhereClause += " AND c.creator_id = u.user_id " +
					" AND u.country = @country ";

				parameters.Add ("@country", country);

				// Zip
				if (zipMiles > 0 && zipCode.Length > 0)
				{
					string selectZip = " SELECT latitude, longitude " +
						" FROM zip_codes " +
						" WHERE zip_code = @zipCode ";
					Hashtable zipParams = new Hashtable ();
					zipParams.Add ("@zipCode", zipCode);
					DataRow drZip = dbUtility.GetDataRow (selectZip, zipParams, false);

					if (drZip == null)
					{
						sqlWhereClause += " AND sc.zip_code = @zipCode ";
						parameters.Add ("@zipCode", zipCode);	
					}
					else
					{
						double upperLat = 0.0, upperLong = 0.0, lowerLat = 0.0, lowerLong = 0.0;
						StoreUtility.GetCoordinates (zipMiles, Convert.ToDouble (drZip ["latitude"]), Convert.ToDouble (drZip ["longitude"]), ref upperLat, ref upperLong, ref lowerLat, ref lowerLong);
						
						sqlWhereClause += " AND sc.zip_code IN " +
							" (SELECT zip_code from zip_codes WHERE " +
							" latitude > @lowerLat " +
							" AND latitude < @upperLat " +
							" AND longitude > @lowerLong " +
							" AND longitude < @upperLong )";
						parameters.Add ("@lowerLat", lowerLat);	
						parameters.Add ("@upperLat", upperLat);
						parameters.Add ("@lowerLong", lowerLong);
						parameters.Add ("@upperLong", upperLong);
					}
				}
			}

			if (bOnlyWithPhotos)
			{
				sqlWhereClause += " AND sc.has_thumbnail = 'Y' ";
			}

			if (bOnlyWithVideos)
			{
				if (bOnlyWithMusic)
				{
					sqlWhereClause += " AND (sc.has_video = 'Y' OR sc.has_music = 'Y')";
				}
				else
				{
					// Only video
					sqlWhereClause += " AND sc.has_video = 'Y' ";
				}
			}
			else
			{
				if (bOnlyWithMusic)
				{
					sqlWhereClause += " AND sc.has_music = 'Y' ";
				}
			}

			// Only get last x number of days?
			if (pastDays > 0)
			{
				sqlWhereClause += " AND sc.created_date > " + dbUtility.GetDatePlusDays (-pastDays);
			}

			if (numberOfMembersMin > 1)
			{
				sqlWhereClause += " AND sc.member_number > " + numberOfMembersMin;
			}
			
			if (numberOfMembersMax > 0)
			{
				sqlWhereClause += " AND sc.member_number < " + numberOfMembersMax;
			}

			if (pastDaysUpdated > 0)
			{
				sqlWhereClause += " AND sc.last_update > " + dbUtility.GetDatePlusDays (-pastDaysUpdated);
			}

			if (filter.Length > 0)
			{
				sqlWhereClause += " AND " + filter;
			}

			
			return dbUtility.GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, orderBy, parameters, pageNumber, pageSize);
		}

		#endregion

		#region member groups

		/// <summary>
		/// return number of members, pending applications and groups
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static DataRow GetMemberCounts(int channelId)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" SELECT COUNT(DISTINCT cm.user_id) AS num_members, ");
			sb.Append(" COUNT(DISTINCT cm_pending.user_id) AS num_pending, ");
			sb.Append(" COUNT(DISTINCT cmg.id) AS num_groups ");
			sb.Append(" FROM communities c ");
			sb.Append(" LEFT JOIN community_members cm ON cm.community_id = c.community_id AND ");
			sb.Append(" cm.status_id = ").Append((int)Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE);
			sb.Append(" LEFT JOIN community_members cm_pending ON cm_pending.community_id = c.community_id ");
			sb.Append(" AND cm_pending.status_id = ").Append((int)Constants.eCOMMUNITY_MEMBER_STATUS.PENDING);
			sb.Append(" LEFT JOIN community_member_groups cmg ON cmg.channel_id = c.community_id ");
			sb.Append(" WHERE c.community_id = @channelId");

			Hashtable param = new Hashtable();
			param.Add("@channelId",channelId);

			return KanevaGlobals.GetDatabaseUtility().GetDataRow(sb.ToString(),param,false);
		}

		
		public static DataTable GetMemberGroups(int channelId)
		{
			return GetMemberGroups(channelId, "", "", 1, Int32.MaxValue);
		}


		/// <summary>
		/// Get the member groups for a channel
		/// </summary>
		/// <param name="channelId"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public static PagedDataTable GetMemberGroups (int channelId, string filter, string orderby, 
			int pageNumber, int pageSize)
		{
			string selectList = " mg.id, mg.name, mg.created_datetime, " +
				" (SELECT COUNT(*) " +
				" FROM community_member_group_members mgs, users u " +
				" WHERE mgs.member_group_id = mg.id " +
				" AND mgs.member_id = u.user_id " +
				" AND u.active = 'Y' ) as member_count ";

			string tableList = " community_member_groups mg ";

			string whereClause = " mg.channel_id = @channelId ";		// Members invited by my I confirmed

			// Filter it?
			if (filter.Length > 0)
			{
				whereClause += " AND " + filter;
			}

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channelId", channelId);

			return KanevaGlobals.GetDatabaseUtility ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// Insert a member group
		/// </summary>
		/// <returns></returns>
		public static int InsertMemberGroup (int channelId, string groupName)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sql = "INSERT INTO community_member_groups " +
				"(channel_id, name, created_datetime " +
				") VALUES (" +
				"@channelId, @groupName, " + dbUtility.GetCurrentDateFunction () + ")";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channelId", channelId);
			parameters.Add ("@groupName", groupName);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		/// <summary>
		/// Insert a member into a group
		/// </summary>
		/// <returns></returns>
		public static int InsertMemberInGroup (int channelId, int memberGroupId, int memberId)
		{
			// Make sure they are not already in the group
			if (IsMemberInGroup (memberGroupId, memberId))
			{
				return 0;
			}

			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sql = "INSERT INTO community_member_group_members " +
				"(member_group_id, member_id" +
				") VALUES (" +
				"@memberGroupId, @memberId)";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@memberId", memberId);
			parameters.Add ("@memberGroupId", memberGroupId);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		/// <summary>
		/// IsMemberInGroup
		/// </summary>
		/// <param name="memberGroupId"></param>
		/// <param name="memberId"></param>
		/// <returns></returns>
		public static bool IsMemberInGroup (int memberGroupId, int memberId)
		{
			string sqlSelect = "SELECT member_group_id " +
				" FROM community_member_group_members " +
				" WHERE member_group_id = @memberGroupId" +
				" AND member_id = @memberId ";

			Hashtable parameters = new Hashtable ();		
			parameters.Add ("@memberGroupId", memberGroupId);
			parameters.Add ("@memberId", memberId);
			DataRow drMemberGroup = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);

			return (drMemberGroup != null);
		}

		/// <summary>
		/// GetMemberGroup
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static DataRow GetMemberGroup (int id)
		{
			string sqlSelect = "SELECT c.channel_id, c.name, c.created_datetime " +
				" FROM community_member_groups c " +
				" WHERE c.id = @id";

			Hashtable parameters = new Hashtable ();		
			parameters.Add ("@id", id);
			return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// Remove a member from a group
		/// </summary>
		/// <returns></returns>
		public static int RemoveMemberFromGroup (int memberGroupId, int memberId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sql = "DELETE FROM community_member_group_members " +
				" WHERE member_id = @memberId " +
				" AND member_group_id = @member_group_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@memberId", memberId);
			parameters.Add ("@member_group_id", memberGroupId);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		/// <summary>
		/// GetMemberGroup
		/// </summary>
		/// <param name="memberGroupId"></param>
		/// <returns></returns>
		public static PagedDataTable GetMembersInGroup (int memberGroupId, string orderby, int pageNumber, int pageSize)
		{	
			return GetMembersInGroup (memberGroupId, "", orderby, pageNumber, pageSize);
		}
		/// <summary>
		/// GetMemberGroup
		/// </summary>
		/// <param name="memberGroupId"></param>
		/// <returns></returns>
		public static PagedDataTable GetMembersInGroup (int memberGroupId, string filter, string orderby, int pageNumber, int pageSize)
		{
			string selectList = "fgf.member_id, u.username, u.user_id, u.gender, " +
				" (u.online & u.show_online) as online, u.location, cm.added_date, " +
				" cm.account_type_id, cm.community_id, uc.name_no_spaces, uc.thumbnail_small_path, uc.thumbnail_medium_path ";

			string tableList = "community_member_groups fg " +
				" INNER JOIN community_member_group_members fgf ON fg.id = fgf.member_group_id " +
				" INNER JOIN users u ON fgf.member_id = u.user_id " +
				" INNER JOIN communities_personal uc ON uc.creator_id = u.user_id " +
				" INNER JOIN community_members cm ON cm.user_id = u.user_id AND cm.community_id = fg.channel_id";

			string whereClause = "fgf.member_group_id = @memberGroupId" +
				" AND u.active = 'Y'";

			// Filter it?
			if (filter.Length > 0)
			{
				whereClause += " AND " + filter;
			}

			Hashtable parameters = new Hashtable ();		
			parameters.Add ("@memberGroupId", memberGroupId);

			return KanevaGlobals.GetDatabaseUtility ().GetPagedDataTable (selectList, tableList, whereClause, 
				orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// Delete a member group
		/// </summary>
		/// <returns></returns>
		public static int DeleteMemberGroup (int id)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// Delete all members from group
			DeleteAllMembersFromGroup (id);

			// Delete the member group
			string sql = "DELETE FROM community_member_groups " +
				" WHERE id = @id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		/// <summary>
		/// DeleteAllMembersFromGroup
		/// </summary>
		/// <returns></returns>
		public static int DeleteAllMembersFromGroup (int id)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// Delete any members in the group
			string sql = "DELETE FROM community_member_group_members " +
				" WHERE member_group_id = @id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		#endregion

		/// <summary>
		/// returns the personal channel id for a given user
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static int GetPersonalChannelId(int userId)
		{
			string sqlSelect = "SELECT community_id " +
				" FROM communities_personal " +
				" WHERE creator_id = @userId ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@userId", userId);

			return KanevaGlobals.GetDatabaseUtility ().ExecuteScalar(sqlSelect, parameters);
		
		}

		/// <summary>
		/// returns true if the channel is personal
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static bool IsChannelPersonal (int channelId)
		{
			string sqlSelect = "SELECT is_personal " +
				" FROM communities " +
				" WHERE community_id = @channelId";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channelId", channelId);

			DataRow rowChannel = KanevaGlobals.GetDatabaseUtility().GetDataRow(sqlSelect,parameters, false);
			return IsChannelPersonal (rowChannel);
		}

		/// <summary>
		/// returns true if the channel is personal
		/// </summary>
		public static bool IsChannelPersonal (DataRow drChannel)
		{
			return drChannel != null && drChannel ["is_personal"].ToString().Equals("1");
		}

		/// <summary>
		/// returns true if the channel is personal
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static bool IsChannelPublishable (int channelId)
		{
			string sqlSelect = "SELECT allow_publishing " +
				" FROM communities " +
				" WHERE community_id = @channelId";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channelId", channelId);

			DataRow rowChannel = KanevaGlobals.GetDatabaseUtility().GetDataRow (sqlSelect,parameters, false);
			return IsChannelPublishable (rowChannel);
		}

		/// <summary>
		/// returns true if the channel is personal
		/// </summary>
		public static bool IsChannelPublishable (DataRow drChannel)
		{
			return drChannel != null && drChannel ["allow_publishing"].ToString().Equals("1");
		}

		// <summary>
		/// return number of times a channel is viewed
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static int GetChannelViewed (int channelId)
		{
			string sqlSelect = "SELECT number_of_views " +
				" FROM channel_stats " +
				" WHERE channel_id = @channelId ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channelId", channelId);

			return KanevaGlobals.GetDatabaseUtility ().ExecuteScalar(sqlSelect, parameters);
		}

		/// <summary>
		/// ShareCommunity
		/// </summary>
		public static void ShareCommunity (int communityId, int userId, int sentUserId, string email)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "INSERT INTO community_shares " +
				" (user_id, sent_user_id, community_id, email, created_date) VALUES (" +
				"@userId, @sentUserId, @communityId, @email, " + dbUtility.GetCurrentDateFunction () + ")";

			parameters.Add ("@userId", userId);          
			parameters.Add ("@sentUserId", sentUserId); 
			parameters.Add ("@communityId", communityId);

			if (email.Trim ().Length.Equals (0))
			{
				parameters.Add ("@email", DBNull.Value);
			}
			else
			{
				parameters.Add ("@email", email);
			}

			dbUtility.ExecuteNonQuery (sqlString, parameters);

			// This is now done in a trigger
			// Increase Share Count
			//IncreaseNumberTimesShared (communityId);
		}

		/// <summary>
		/// GetUserCommentCount
		/// </summary>
		public static int GetUniqueVistors (int channelId)
		{
			string sqlSelect = "SELECT DISTINCT(user_id) FROM community_views upv " +
				" WHERE community_id = @channelId " +
				" AND user_id > 0 ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@channelId", channelId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters).Rows.Count;
		}

		/// <summary>
		/// Update number of channel views
		/// </summary>
		/// <param name="user_id"></param>
		public static void UpdateChannelViews (int userId, int communityId, bool browseAnon, string ipAddress)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			// Has this IP viewed within the last minute?
			string sqlSelect = "SELECT cv.view_id, (UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(created_date)) as last_logged " + 
				" FROM recent_community_views cv " +
				" WHERE community_id = @communityId ";

			if (userId > 0)
			{
				sqlSelect += " AND user_id = @userId";
				parameters.Add ("@userId", userId);
			}
			else
			{
				sqlSelect += " AND ip_address = INET_ATON(@ipAddress)";
				parameters.Add ("@ipAddress", ipAddress);
			}

			sqlSelect += " ORDER BY cv.view_id DESC LIMIT 1 ";
		
			parameters.Add ("@communityId", communityId);
			DataRow drCommView = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);

			if (drCommView == null || (!drCommView ["last_logged"].Equals (DBNull.Value) && Convert.ToInt32 (drCommView ["last_logged"]) > 60))
			{
				// Record as a browse
				string sqlInsertView = "INSERT INTO community_views " +
					" (user_id, community_id, created_date, anonymous, ip_address)" +
					" VALUES (@userId, @communityId, " + dbUtility.GetCurrentDateFunction () + ", @anonymous, INET_ATON(@ipAddress))";
					
				parameters = new Hashtable ();	
				parameters.Add ("@userId", userId);
				parameters.Add ("@communityId", communityId);
				parameters.Add ("@anonymous", browseAnon ? 1 : 0);
				parameters.Add ("@ipAddress", ipAddress);
				dbUtility.ExecuteNonQuery (sqlInsertView, parameters);	
			}
		}

		/// <summary>
		/// GetTotalChannelStats
		/// </summary>
		public static DataRow GetTotalChannelStats (int user_id, int community_account_type_id )
		{
			string sqlSelect = "SELECT SUM(cs.number_of_members) as number_of_members, SUM(cs.number_of_views) as number_of_views, " +
				" SUM(cs.number_of_diggs) as number_of_diggs, SUM(cs.number_times_shared) as number_times_shared, " +
				" cm.account_type_id, COUNT(c.community_id) as number_of_channels " +
				" FROM users u " +
				" LEFT JOIN community_members cm ON cm.user_id = u.user_id " +
				" LEFT JOIN communities_personal c ON c.community_id = cm.community_id " +
				" INNER JOIN channel_stats cs ON cs.channel_id = c.community_id " +
				" WHERE c.status_id = " + (int) Constants.eCOMMUNITY_STATUS.ACTIVE + 
				" AND cm.account_type_id = @community_account_type_id " +
				" AND cm.status_id = " + (int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE + 
				" AND u.user_id = @user_id " +
				" GROUP BY cm.account_type_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@user_id", user_id);
			parameters.Add ("@community_account_type_id", community_account_type_id);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}


		#region events
		public enum eEVENT_START_TIME_FILTER
		{
			PAST = 1,
			UPCOMING = 2,
			ALL = 3
		}
		

		/// <summary>
		/// 
		/// </summary>
		/// <param name="channelId"></param>
		/// <param name="startTime"></param>
		/// <param name="orderby"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public static PagedDataTable GetChannelEvents(int channelId, int startTime, string orderby, 
			int pageNumber, int pageSize)
		{
			DatabaseUtility utility = KanevaGlobals.GetDatabaseUtility ();
			string selectList = "e.event_id, e.community_id, e.user_id, e.title, e.details, " +
				" e.location, e.start_time, e.end_time, e.type_id, e.time_zone_id ";

			string tableList = " events e ";

			string whereClause = " e.community_id = @channelId";
			Hashtable parameters = new Hashtable ();

			switch (startTime)
			{
				case (int) eEVENT_START_TIME_FILTER.PAST:
					whereClause += " AND e.start_time < @start_time";
					parameters.Add("@start_time" ,utility.GetCurrentDateTime());
					break;
				case (int) eEVENT_START_TIME_FILTER.UPCOMING:
					whereClause += " AND e.start_time >= @start_time";
					parameters.Add("@start_time" ,utility.GetCurrentDateTime());
					break;
				case (int) eEVENT_START_TIME_FILTER.ALL:
					break;
				default:
					break;
			}


			
			parameters.Add ("@channelId", channelId);
			
			return  utility.GetPagedDataTable (selectList, tableList, whereClause, 
				orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// Get an event
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static DataRow GetEvent(int id)
		{
			string sqlSelect = "SELECT e.event_id, e.community_id, e.user_id, e.title, e.details, " +
				" e.location, e.start_time, e.end_time, e.type_id, e.time_zone_id FROM events e " +
				" WHERE event_id = @id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityId"></param>
		/// <param name="userId"></param>
		/// <param name="title"></param>
		/// <param name="details"></param>
		/// <param name="location"></param>
		/// <param name="startTime"></param>
		/// <param name="endTime"></param>
		/// <param name="typeId"></param>
		/// <param name="timeZoneId"></param>
		/// <returns></returns>
		public static int InsertEvent(
			int communityId, 
			int userId, 
			string title, 
			string details,
			string location, 
			DateTime startTime, 
			DateTime endTime, 
			int typeId, 
			int timeZoneId
			)
		{
			string query = " INSERT INTO events " + 
				" ( community_id, user_id, title, details, " +
				" location, start_time, end_time, type_id, time_zone_id) " +
				" VALUES( @community_id, @user_id, @title, @details, " +
				" @location, @start_time, @end_time, @type_id, @time_zone_id) ";

			Hashtable parameters = new Hashtable ();
			parameters.Add("@community_id", communityId);
			parameters.Add("@user_id", userId);
			parameters.Add("@title", title);
			parameters.Add("@details", details);
			parameters.Add("@location", location);
			parameters.Add("@start_time", startTime);
			parameters.Add("@end_time", endTime);
			parameters.Add("@type_id", typeId);
			parameters.Add("@time_zone_id", timeZoneId);

			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);
			return retVal;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="eventId"></param>
		/// <param name="communityId"></param>
		/// <param name="userId"></param>
		/// <param name="title"></param>
		/// <param name="details"></param>
		/// <param name="location"></param>
		/// <param name="startTime"></param>
		/// <param name="endTime"></param>
		/// <param name="typeId"></param>
		/// <param name="timeZoneId"></param>
		public static void SaveEvent(
			int eventId, 
			int communityId, 
			int userId, 
			string title, 
			string details,
			string location, 
			DateTime startTime, 
			DateTime endTime, 
			int typeId, 
			int timeZoneId
			)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE events ");
			sb.Append(" SET community_id = @community_id, ");
			sb.Append(" user_id= @user_id, ");
			sb.Append(" title= @title, ");
			sb.Append(" details = @details, ");
			sb.Append(" location = @location, ");
			sb.Append(" start_time = @start_time, ");
			sb.Append(" end_time = @end_time, ");
			sb.Append(" type_id = @type_id, ");
			sb.Append(" time_zone_id = @time_zone_id ");
			sb.Append(" WHERE event_id = @id ");

			Hashtable parameters = new Hashtable ();
			parameters.Add("@id", eventId);
			parameters.Add("@community_id", communityId);
			parameters.Add("@user_id", userId);
			parameters.Add("@title", title);
			parameters.Add("@details", details);
			parameters.Add("@location", location);
			parameters.Add("@start_time", startTime);
			parameters.Add("@end_time", endTime);
			parameters.Add("@type_id", typeId);
			parameters.Add("@time_zone_id", timeZoneId);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		public static void DeleteEvent(int id)
		{
			//delete from layout_module_title_text table
			string sqlString = "DELETE FROM events " +
				" WHERE event_id = @id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}
		#endregion

		#region diggs
		/// <summary>
		/// Insert a digg
		/// </summary>
		public static int InsertDigg (int userId, int channelId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// Must be logged in to digg someone
			if (userId < 1)
			{
				return 2;
			}

			//added "hammer to let admins rave as mnay times as they like
			
			if(!UsersUtility.IsUserAdministrator())
			{
				// Make sure they have not already digged
				DataRow drDigg = GetDigg (userId, channelId);

				// If they are already a friend, don't do anything
				if (drDigg != null)
				{
					// Already dugg
					return 1;
				}
			} 

			string sql = "INSERT INTO channel_diggs " +
				"(user_id, channel_id, created_date " +
				") VALUES (" +
				"@userId, @channelId," + dbUtility.GetCurrentDateFunction () + ")";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@userId", userId);
			parameters.Add ("@channelId", channelId);
			dbUtility.ExecuteNonQuery (sql, parameters);

//	Replaced with a trigger
//			// Update the profile digg count
//			sql = "UPDATE communities " +
//				" SET number_of_diggs = number_of_diggs + 1 " +
//				" WHERE community_id = @channelId ";
//			parameters = new Hashtable ();			
//			parameters.Add ("@channelId", channelId);
//			dbUtility.ExecuteNonQuery (sql, parameters);

			return 0;
		}

		/// <summary>
		/// Get a digg for a user
		/// </summary>
		public static DataRow GetDigg (int userId, int channelId)
		{
			string sqlSelect = "select d.digg_id " +
				" FROM channel_diggs d " +
				" WHERE user_id = @userId " +
				" AND channel_id = @channelId ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@userId", userId);
			parameters.Add ("@channelId", channelId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}
		#endregion

		#region templates
		/// <summary>
		/// UpdateUserStandardTemplate
		/// </summary>
		public static void UpdateUserStandardTemplate (int channelId, int standardTemplateId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// Does the user have a current template?
			DataRow drTemplate = WidgetUtility.GetTemplateForChannel(channelId);

			// Did they already have a template?
			if (drTemplate == null)
			{
				int templateId = WidgetUtility.InsertTemplate (standardTemplateId);
				if (templateId != -1)
				{
					UpdateTemplate (channelId, templateId);
				}
				return;
			}
			else
			{
				WidgetUtility.UpdateTemplate (Convert.ToInt32 (drTemplate ["template_id"]), standardTemplateId);
			}
		}

		/// <summary>
		/// UpdateUserTemplate
		/// </summary>
		public static void UpdateTemplate(int channelId, int templateId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlUpdate = "UPDATE communities " +
				" SET " +
				" template_id = @templateId" +
				" WHERE community_id = @channelId";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@templateId", templateId);
			parameters.Add ("@channelId", channelId);
			dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
		}
		#endregion

		#region messages

		/// <summary>
		/// return number of pending members of channels the user owns or moderates
		/// </summary>
		public static double GetPendingMembersCount (int userId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string strSQL = " SELECT SUM(cs.number_of_pending_members) " +
				" FROM channel_stats cs, community_members cm " +
				" WHERE cs.channel_id = cm.community_id " +
				" AND cm.has_edit_rights = 'Y' " +
				" AND cm.user_id = @userId " +
				" AND cm.status_id = " + (int)Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE;
			
			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@userId", userId);
			return dbUtility.ExecuteScalarDouble (strSQL, parameters);
		}

		/// <summary>
		/// return number of pending members of channels the user owns or moderates
		/// </summary>
		public static PagedDataTable GetPendingMembers (int userId, string filter, string orderby, int pageNumber, int pageSize)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string select = " com.thumbnail_small_path, com.name_no_spaces, c.community_id, c.name AS channel_name, cm2.user_id, u.username, " + 
				" u.gender, u.birth_date, u.age, u.location ";
			
			StringBuilder tableList = new StringBuilder() ;
			tableList.Append(" communities c ");
			tableList.Append(" INNER JOIN community_members cm1 ON c.community_id = cm1.community_id ");
			tableList.Append(" INNER JOIN community_members cm2 ON c.community_id = cm2.community_id ");
			tableList.Append(" INNER JOIN users u ON u.user_id = cm2.user_id ");
			tableList.Append(" INNER JOIN communities_personal com ON com.creator_id = u.user_id ");

			StringBuilder whereClause = new StringBuilder() ;
			whereClause.Append(" cm1.user_id = @userId AND cm1.status_id = ").Append(
				(int)Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE);
			whereClause.Append(" AND cm1.has_edit_rights = 'Y' ");
			whereClause.Append(" AND cm2.status_id = ").Append((int) Constants.eCOMMUNITY_MEMBER_STATUS.PENDING);
			if(filter!= null && filter.Trim().Length > 0)
			{
				whereClause.Append(" AND ").Append(filter);
			}
			
			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@userId", userId);

			return dbUtility.GetPagedDataTable(select,tableList.ToString(),whereClause.ToString(), orderby,
				parameters,pageNumber, pageSize);
		}
		#endregion

		#region KGP related
		/// <summary>
		///  return the number of users in a channel in the wok game
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static int GetPlayersInChannel( int channelId )
		{
			Int32 BROADBAND		= 6;	// CI_CHANNEL

			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			string dbName = KanevaGlobals.DbNameKGP;

			string select = "select `count` from " + dbName + ".summary_active_population pp where pp.zone_type = " + BROADBAND + 
                            " and pp.zone_instance_id = " + channelId.ToString();

			return dbUtility.ExecuteScalar( select );
			
		}

		public static DataTable GetZonesPassIds(int zoneIndex, int instanceId )
		{
			string dbNameForGame = KanevaGlobals.DbNameKGP;
			string dbNameForKaneva = KanevaGlobals.DbNameKaneva;
			string sql;
			/*
			if could easily call SP w/result set, call this
			sql = "CALL " + dbNameForGame + ".getZonePassesDetails( @zoneIndex, @instanceId )";
			*/
			
			Hashtable parameters = new Hashtable ();
			parameters.Clear();
			// ack! ugly parsing of zone type 
			if ( (zoneIndex & 0xf0000000) == 0x40000000 )
			{
				sql = 
					"SELECT g.name, g.description, g.pass_group_id "+
					"FROM " + dbNameForGame + ".pass_group_perm_channels cz "+
					"	INNER JOIN " + dbNameForGame + ".pass_groups g "+
					"	ON cz.pass_group_id = g.pass_group_id "+
					"WHERE cz.zone_index_plain = " + dbNameForGame + ".zoneIndex(@zoneIndex)  "+
					"ORDER BY name ";

				parameters.Add ("@zoneIndex", zoneIndex );

			}
			else
			{
				sql = 
					"SELECT g.name, g.description, g.pass_group_id "+
					"FROM " + dbNameForGame + ".pass_group_channel_zones cz "+
					"	INNER JOIN " + dbNameForGame + ".pass_groups g "+
					"	ON cz.pass_group_id = g.pass_group_id "+
					"WHERE cz.zone_instance_id = @instanceId AND cz.zone_type = " + dbNameForGame + ".zoneType(@zoneIndex)"+
					"ORDER BY name ";

				parameters.Add ("@instanceId", instanceId);
				parameters.Add ("@zoneIndex", zoneIndex );

			}


			return KanevaGlobals.GetDatabaseUtility ().GetDataTable(sql, parameters );
		}

		#endregion

        #region Join Template Functions
        /// <summary>
        /// Get the community settings for the join pages
        /// </summary>
        /// <param name="communityId"></param>
        /// <returns></returns>
        public static DataRow GetJoinConfig(int communityId)
        {
            string sqlString = "SELECT community_id, join_id, css, page_join_copy, page_verify_copy, " +
                " page_complete_copy, user_profile_theme_male_id, user_profile_theme_female_id, " +
                " user_profile_video_id, user_profile_male_avatar_path, user_profile_female_avatar_path, " +
                " email_body_top, email_body_bottom, email_subject " +
                " FROM join_themes WHERE " +
                " community_id = @communityId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@communityId", communityId);
            return KanevaGlobals.GetDatabaseUtility().GetDataRow(sqlString, parameters, false);
        }

        /// <summary>
        /// Get the community settings for the join pages
        /// </summary>
        /// <param name="communityId"></param>
        /// <returns></returns>
        public static DataTable GetJoinCommunities(int sourceCommunityId)
        {
            string sqlSelect = "SELECT source_community_id, join_community_id " +
                " FROM join_communities WHERE " +
                " source_community_id = @sourceCommunityId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@sourceCommunityId", sourceCommunityId);
            return KanevaGlobals.GetDatabaseUtility().GetDataTable(sqlSelect, parameters);
        }

        #endregion

		/// <summary>
		/// Database name for tables that are no in the cluster
		/// </summary>
		private static string m_DbNameKanevaNonCluster = "my_kaneva";

	}
}
