///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using System.Web.Security;

using KlausEnt.KEP.Kaneva;

namespace KEPAuth
{
	/// <summary>
	/// Authorizes a given user/pass and returns associated role information. Password is stored as a one way hash, thus
	/// it is computed before comparing.
	/// </summary>
	[WebService(Namespace="http://www.kaneva.com/webservices/",Description="Kaneva Authorization Web Serivce.")]
	public class KEPAuth : System.Web.Services.WebService
	{
		private DAO m_dao;

		protected DAO getDAO()
		{
			return m_dao;
		}

		public KEPAuth()
		{
			//CODEGEN: This call is required by the ASP.NET Web Services Designer
			InitializeComponent();

			m_dao = new DAO();
		}

		#region Component Designer generated code
		
		//Required by the Web Services Designer 
		private IContainer components = null;
				
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);		
		}
		
		#endregion

		/// <summary>
		/// Authorize a user.
		/// </summary>
		/// <param name="userName"></param>
		/// <param name="password"></param>
		/// <param name="gameId"></param>
		/// <param name="roleMembership"></param>
		/// <returns>Status of Authentication</returns>
		[WebMethod (Description="Authenticate a user.")]
		public int authorize ( string userName, string password, int gameId, ref int userId, ref int roleMembership)
		{
			string salt = null;
			string hashPassword = null;
			userId = 0;
			roleMembership = 0;
			int userStatus = 0;

            // new values for V2, not used for V1 KEPAuth
            string gender = Constants.GENDER_BOTH;
            string birthDate = "";
            bool showMature = false;

			// Get the user from the databse
            if (!getDAO().getUser(userName, ref hashPassword, ref salt, ref roleMembership, ref userId, ref userStatus, ref gender, ref birthDate, ref showMature))
            {
				Log.getInstance().writeLog(Log.Info, "auth_notfound", userName );
				return (int) Constants.eLOGIN_RESULTS.USER_NOT_FOUND;
			}

			// Get the hashed password
			string calcHashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(password + salt, "MD5");	    

			// Was the correct password supplied?
			if (calcHashPassword == hashPassword)
			{
				// if they are trying to authenticate against a game, check they have a subscription or bought it
                Constants.eLOGIN_RESULTS authCode = getDAO().getGameAuthorization(userId, gameId);
                if (gameId > 0 && authCode != Constants.eLOGIN_RESULTS.SUCCESS )
                {
                    Log.getInstance().writeLog(Log.Info, "auth_gameaccess", userName + ", " + gameId);
                    return (int)authCode;
                }

                // 5/19/08 Non Validated can now log into games
                //// Have they validated this account?
                //if (userStatus == (int) Constants.eUSER_STATUS.REGNOTVALIDATED)
                //{
                //    Log.getInstance().writeLog(Log.Info, "auth_invalid", userName );
                //    return (int) Constants.eLOGIN_RESULTS.NOT_VALIDATED;
                //}

				// Has this account been deleted?
				if (userStatus == (int) Constants.eUSER_STATUS.DELETED || userStatus == (int) Constants.eUSER_STATUS.DELETEDBYUS)
				{
					Log.getInstance().writeLog(Log.Info, "auth_deleted", userName  );
					return (int) Constants.eLOGIN_RESULTS.ACCOUNT_DELETED;
				}

				// Has this account been locked?
				if (userStatus == (int) Constants.eUSER_STATUS.LOCKED)
				{
					Log.getInstance().writeLog(Log.Info, "auth_locked", userName );
					return (int) Constants.eLOGIN_RESULTS.ACCOUNT_LOCKED;
				}

				Log.getInstance().writeLog(Log.Success, "auth_success", userName);

				return (int) Constants.eLOGIN_RESULTS.SUCCESS;
			}
			else
			{
				Log.getInstance().writeLog(Log.Failure, "auth_failure", userName);
				roleMembership = 0;
				return (int) Constants.eLOGIN_RESULTS.INVALID_PASSWORD;
			}
		}

	}
}
