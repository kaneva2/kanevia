///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using KlausEnt.KEP.Kaneva;

namespace KlausEnt.KEP.KepPublishing
{
	/// <summary>
	/// Summary description for DatabaseUtilityFactory.
	/// </summary>
	public class DatabaseUtilityFactory
	{
		private static DatabaseUtility m_DatabaseUtilty;

		public DatabaseUtilityFactory()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		/// Get the database utility
		/// </summary>
		/// <returns></returns>
		public static DatabaseUtility GetDatabaseUtility ()
		{
			if (m_DatabaseUtilty == null)
			{
				switch (KanevaGlobals.DatabaseType)
				{
					case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
					{
						m_DatabaseUtilty = new SQLServerUtility (KanevaGlobals.ConnectionString);
						break;
					}
					case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
					{
						m_DatabaseUtilty = new MySQLUtility (KanevaGlobals.ConnectionString);
						break;
					}
					default:
					{
						throw new Exception ("This database type is not supported");
					}
				}
			}
			return m_DatabaseUtilty;
		}
	}
}
