///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace KlausEnt.KEP.KepPublishing
{
	/// <summary>
	/// Summary description for TorrentDao.
	/// </summary>
	public class TorrentDao
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(TorrentDao));
		private static TorrentDao instance;
		private static readonly string SQL_SELECT_TORRENT;

		static TorrentDao()
		{
			SQL_SELECT_TORRENT = " SELECT torrent_id, user_id, info_hash, torrent_name, " + 
				" content_extension, target_dir, torrent_status " + 
				" FROM torrents " + 
				" WHERE torrent_status in (" +
				((int) Constants.eTORRENT_STATUS.UPLOADED) +
				" , " +
				((int) Constants.eTORRENT_STATUS.TORRENT_REPLACED) +
				" ) " + 
				" AND status_id =" +
				((int) Constants.eTORRENT_ENTRY_STATUS.ACTIVE);
		}
		private TorrentDao()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static TorrentDao GetInstance()
		{
			if(instance == null)
			{
				instance = new TorrentDao() ;
			}
			return instance;
		}

		/// <summary>
		/// get all torrents to be processed
		/// </summary>
		/// <returns></returns>
		public IList GetAll()
		{
			IList retVal = new ArrayList();
			try
			{
				DatabaseUtility dbUtility = DatabaseUtilityFactory.GetDatabaseUtility ();
				if(log.IsDebugEnabled)
				{
					log.Debug("executing " + SQL_SELECT_TORRENT);
				}
				DataTable dt = dbUtility.GetDataTable(SQL_SELECT_TORRENT);
				DataRowCollection rows = null;
				if(dt != null)
				{
					rows = dt.Rows;
				}
				if(rows != null && rows.Count > 0)
				{
					foreach(DataRow row in rows)
					{
						int torrentId = int.Parse(row["torrent_id"].ToString());
						int userId = int.Parse(row["user_id"].ToString());
						string infohash = row["info_hash"].ToString();
						string torrentName = row["torrent_name"].ToString();
						string contentExtension = row["content_extension"].ToString();
						string targetDir = row["target_dir"].ToString();
//						bool contentHosted = int.Parse(row["content_hosted"].ToString()) == 1;
						int torrentStatus = int.Parse(row["torrent_status"].ToString());

						Torrent torrent = new Torrent();
						torrent.TorrentId = torrentId;
						torrent.UserId = userId;
						torrent.Infohash = infohash;
						torrent.TorrentName = torrentName;
						torrent.FileName = contentExtension;
						torrent.TargetDir = targetDir;
//						torrent.ContentHosted = contentHosted;
						torrent.TorrentStatusId = torrentStatus;

						retVal.Add(torrent);
					}
					dt.Dispose();
				}
			}
			catch(Exception e)
			{
				log.Error("exception processing uploaded content", e);
			}finally
			{
				//				
			}

			return retVal;
		}

		/// <summary>
		/// Update torrent
		/// </summary>
		/// <param name="torrentId">id</param>
		/// <param name="infoHash">display purposes for Kaneva</param>
		/// <param name="torrentStatusId"></param>
		/// <returns>torrent id if successful</returns>
		public void UpdateTorrent(int torrentId, string torrentName, string infoHash, int torrentStatusId)
		{
			DatabaseUtility dbUtility = DatabaseUtilityFactory.GetDatabaseUtility ();

			string sqlUpdate =	" UPDATE torrents " +
				" SET torrent_name = @torrent_name, " +
				" torrent_status = @torrent_status, " + 
				" info_hash = @info_hash " +
				" where torrent_id = @torrent_id ";
			
			Hashtable parameters = new Hashtable ();
			parameters.Add ("@torrent_name", torrentName);
			parameters.Add ("@torrent_status", torrentStatusId);
			parameters.Add ("@info_hash", infoHash);
			parameters.Add ("@torrent_id", torrentId);
			dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
		}
	}
}
