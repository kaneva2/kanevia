///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace KlausEnt.KEP.KepPublishing
{
	/// <summary>
	/// Summary description for Torrent.
	/// </summary>
	public class Torrent
	{
		private int		_torrentId;
		private int		_userId;
		private string	_infohash;
		private string	_torrentName;
		private string	_fileName;
		private string	_targetDir;
//		private bool	_contentHosted;
		private int		_torrentStatusId;
		public Torrent()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public int TorrentId
		{
			get { return _torrentId; }
			set { _torrentId = value; }
		}

		public int UserId
		{
			get { return _userId; }
			set { _userId = value; }
		}

		public string Infohash
		{
			get { return _infohash; }
			set { _infohash = value; }
		}

		public string TorrentName
		{
			get { return _torrentName; }
			set { _torrentName = value; }
		}

		public string FileName
		{
			get { return _fileName; }
			set { _fileName = value; }
		}

		public string TargetDir
		{
			get { return _targetDir; }
			set { _targetDir = value; }
		}

//		public bool ContentHosted
//		{
//			get { return _contentHosted; }
//			set { _contentHosted = value; }
//		}

		public int TorrentStatusId
		{
			get { return _torrentStatusId; }
			set { _torrentStatusId = value; }
		}
	}
}
