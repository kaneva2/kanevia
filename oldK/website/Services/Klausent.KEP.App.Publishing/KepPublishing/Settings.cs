///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace KlausEnt.KEP.KepPublishing
{
	/// <summary>
	/// Summary description for Settings.
	/// </summary>
	public class Settings
	{
		private string _publishAnnounceUrl;
		private string _torrentCreator;
		private static Settings instance;
		private string _replaceTorrentExt;
		private string _replaceTorrentPrefix;
		private Settings()
		{
			//
			// TODO: Add constructor logic here
			//
			_publishAnnounceUrl = 
				System.Configuration.ConfigurationSettings.AppSettings ["PublishAnnounceUrl"];
			_torrentCreator = 
				System.Configuration.ConfigurationSettings.AppSettings ["KanevaTorrentCreator"];
			_replaceTorrentExt = 
				System.Configuration.ConfigurationSettings.AppSettings ["ReplaceTorrentExt"];
			_replaceTorrentPrefix = 
				System.Configuration.ConfigurationSettings.AppSettings ["ReplaceTorrentPrefix"];
		}

		public static Settings GetInstance()
		{
			if(instance == null)
			{
				instance = new Settings() ;
			}

			return instance;
		}

		public string TorrentCreator
		{
			get { return _torrentCreator; }
		}

		/// <summary>
		/// tracker used for torrent publishing
		/// </summary>
		public string PublishAnnounceUrl
		{
			get { return _publishAnnounceUrl; }
		}

		/// <summary>
		/// extension used by a recreated torrrent file
		/// </summary>
		public string ReplaceTorrentExt
		{
			get { return _replaceTorrentExt; }
		}

		/// <summary>
		/// filename prefix used by a recreated torrrent file
		/// </summary>
		public string ReplaceTorrentPrefix
		{
			get { return _replaceTorrentPrefix; }
		}
	}
}
