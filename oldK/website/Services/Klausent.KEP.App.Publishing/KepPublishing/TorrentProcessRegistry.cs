///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Text;
using log4net;

namespace KlausEnt.KEP.KepPublishing
{
	/// <summary>
	/// Summary description for TorrentProcessRegistry.
	/// </summary>
	public class TorrentProcessRegistry
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(TorrentProcessRegistry));

		private IDictionary _processes;
		private static TorrentProcessRegistry instance;
		
		private TorrentProcessRegistry()
		{
			//
			// TODO: Add constructor logic here
			//
			_processes = new Hashtable();
		}

		public static TorrentProcessRegistry GetInstance()
		{
			if(instance == null)
			{
				instance = new TorrentProcessRegistry() ;
			}
			return instance;
		}

		/// <summary>
		/// register a torrent processing task, return false if the torrent 
		/// is already being processed
		/// </summary>
		/// <param name="torrent"></param>
		/// <returns></returns>
		public bool Register(Torrent torrent, ITorrentProcessor processor)
		{
			bool retVal = false;
			lock(this)
			{
				if(!_processes.Contains(torrent.Infohash.ToUpper()))
				{
					if(log.IsDebugEnabled)
					{
						log.Debug(processor.GetType().ToString() + " is processing torrent " + torrent.Infohash.ToUpper());
					}
					_processes.Add(torrent.Infohash.ToUpper(),processor);
					retVal = true;
				}
				else
				{
					if(log.IsInfoEnabled)
					{
						StringBuilder sb = new StringBuilder() ;
						sb.Append(processor.GetType().ToString());
						sb.Append(" CAN NOT process torrent ");
						sb.Append(torrent.Infohash.ToUpper());
						sb.Append("\r\n");
						sb.Append("Torrent is being processed by");
						sb.Append(_processes[torrent.Infohash.ToUpper()].GetType().ToString());
						log.Info(sb.ToString());
					}
				}
			}
			return retVal;
		}

		/// <summary>
		/// this task is completed, sucessfully or not
		/// </summary>
		/// <param name="torrent"></param>
		/// <param name="processor"></param>
		/// <returns></returns>
		public bool UnRegister(Torrent torrent, ITorrentProcessor processor)
		{
			bool retVal = false;
			lock(this)
			{
				if(!_processes.Contains(torrent.Infohash.ToUpper()))
				{
					log.Error("Torrent not found in the registry " + torrent.Infohash.ToUpper());
				}
				else
				{
					_processes.Remove(torrent.Infohash.ToUpper());
					if(log.IsDebugEnabled)
					{
						log.Debug("Torrent removed from the registry " + torrent.Infohash.ToUpper());
					}
					retVal = true;
				}
			}
			return retVal;
		}
	}
}
