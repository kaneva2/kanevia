///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;

namespace KlausEnt.KEP.KEPDS
{
	/// <summary>
	/// </summary>
	public class BNBTUtility
	{
		public BNBTUtility()
		{
		}
		/// <summary>
		/// Post the given torrent file to the tracker. Currently,
		/// support for BNBT only.
		/// </summary>
		/// <param name="fileName">The file name being posted.</param>
		/// <param name="user_id">The user_id associated with the user.</param>
		/// <param name="fileToPost">A stream of the file to post to the tracker</param>
		/// <returns>Zero if successful, error code otherwise.</returns>
		public int PostToTracker( string fileName, int user_id, Stream fileToPost)
		{
			int ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS;

			TcpClient tcpClient = null;

			try
			{
				string ip = null;
				int port = 0;
				string torrentPath = null;
				string trackerUser = null;
				string trackerPass = null;

				GetTrackerInfo( ref ip, ref port, ref torrentPath, ref trackerUser, ref trackerPass);

				string boundary = "----------" + DateTime.Now.Ticks.ToString("x");

				StringBuilder sb = new StringBuilder();

				sb.Append("--");
				sb.Append(boundary);
				sb.Append("\r\n");
				sb.Append("Content-Disposition: form-data; name=\"");
				sb.Append("torrent");
				sb.Append("\"; filename=\"");
				sb.Append(torrentPath);
				sb.Append(fileName);
				sb.Append("\"");
				sb.Append("\r\n");
				sb.Append("Content-Type: ");
				sb.Append("application/x-bittorrent");
				sb.Append("\r\n");
				sb.Append("\r\n");

				string sb1Header = sb.ToString();
				byte[] sb1Bytes = Encoding.UTF8.GetBytes(sb1Header);

				StringBuilder sb2 = new StringBuilder();

				string contentName = fileName.Replace(".torrent","");

				sb2.Append("\r\n--");
				sb2.Append(boundary);
				sb2.Append("\r\n");
				sb2.Append("Content-Disposition: form-data; name=\"name\"\r\n\r\n");
				sb2.Append(contentName);
				sb2.Append("\r\n");
				sb2.Append("--");
				sb2.Append(boundary);
				sb2.Append("\r\nContent-Disposition: form-data; name=\"infolink\"\r\n\r\n");
				sb2.Append("\r\n");
				sb2.Append("--");
				sb2.Append(boundary);
				sb2.Append("\r\nContent-Disposition: form-data; name=\"tag\"\r\n\r\n");
				sb2.Append("Other");

				string sb2Header = sb2.ToString();
				byte[] sb2Bytes = Encoding.UTF8.GetBytes(sb2Header);

				byte[] boundaryBytes = Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");

				long length = sb2Bytes.Length + sb1Bytes.Length + fileToPost.Length + boundaryBytes.Length;

				tcpClient = new TcpClient();

				// Establishing the connection under IIS on a multi-homed box
				// is delayed 5-6 seconds. Non multi-homed works ok.
				tcpClient.Connect( IPAddress.Parse(ip), port );

				NetworkStream request = tcpClient.GetStream();

				StringBuilder sb3 = new StringBuilder();

				sb3.Append("POST /upload.html HTTP/1.1\r\n");
				sb3.Append("Content-Type: multipart/form-data; boundary=" + boundary + "\r\n");
				sb3.Append("Cookie: login=\"" + trackerUser + "\"; md5=\"" + trackerPass + "\"\r\n");
				sb3.Append("Content-Length: " + length + "\r\n\r\n");

				string sb3Header = sb3.ToString();
				byte[] sb3Bytes = Encoding.UTF8.GetBytes(sb3Header);

				request.Write(sb3Bytes, 0, sb3Bytes.Length);
				request.Write(sb1Bytes, 0, sb1Bytes.Length);

				byte[] buffer = new byte[checked((uint)Math.Min(4096, (int)fileToPost.Length))];
				int bytesRead = 0;

				while ( (bytesRead = fileToPost.Read(buffer, 0, buffer.Length)) != 0 )
				{
					request.Write(buffer, 0, bytesRead);
				}

				request.Write(sb2Bytes, 0, sb2Bytes.Length);

				request.Write(boundaryBytes, 0, boundaryBytes.Length);

				request.Flush();

				// Receive the response.
				Byte[] bytesReceived = new Byte[256];
				int bytes = 0;
				string page = "Return\r\n";

				do 
				{
					bytes = request.Read(bytesReceived, 0, bytesReceived.Length);
					page = page + Encoding.ASCII.GetString(bytesReceived, 0, bytes);
				}
				while (bytes > 0);

				// Parse the page for error
				ret = ParseTrackerPostResult(fileName, user_id, page);
			}
			catch (Exception e)
			{
				Log.getInstance().writeLog (Log.Error, "IDS_EXCEPTION_POSTING_TO_TRACKER", e.Message);
				ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.EXCEPTION_POSTING_TO_TRACKER;
			} 
			finally
			{
				if ( tcpClient != null )
				{
					tcpClient.Close();
				}
			}

			// Tested this call with 64 concurrent threads. Could not
			// get the BNBT tracker to accept greater than 64.

			return ret;
		}

		/// <summary>
		/// Parse the returned page from the tracker when posting a file for any error messages.
		/// For BNBT, these are embedded with javascript.
		/// </summary>
		/// <param name="fileName">The file name being posted.</param>
		/// <param name="user_id">The user_id associated with the user.</param>
		/// <param name="page">The HTML page to parse, currently only BNBT support.</param>
		/// <returns>Zero if successful, error code otherwise.</returns>
		private int ParseTrackerPostResult(string fileName, int user_id, string page)
		{
			int ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS;

			// Returned page contains javascript to inform the user.
			// Grab the failure code if it exists.
			string failedFunction = "confirm ( failed_";

			int failedMsgIndex = page.IndexOf(failedFunction);

			if ( failedMsgIndex != -1 )
			{
				string failedMsgNum = page.Substring( failedMsgIndex + failedFunction.Length, 1);

				// Determine the actual error message and log it

				switch (Convert.ToInt32(failedMsgNum))
				{
					case 1:
						Log.getInstance().writeLog (Log.Error, "IDS_ERROR_POST_FILE_CORRUPT_OR_INVALID", " UserId = " + user_id + " File name = " + fileName);
						ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_FILE_CORRUPT_OR_INVALID;
						break;
					case 2:
						Log.getInstance().writeLog (Log.Error, "IDS_ERROR_POST_FILE_NOT_TORRENT", " UserId = " + user_id + " File name = " + fileName);
						ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_FILE_NOT_TORRENT;
						break;
					case 3:
						Log.getInstance().writeLog (Log.Error, "IDS_ERROR_POST_FILE_CATEGORY_TAG_INVALID", " UserId = " + user_id + " File name = " + fileName);
						ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_FILE_CATEGORY_TAG_INVALID;
						break;
					case 4:
						Log.getInstance().writeLog (Log.Error, "IDS_ERROR_POST_FILE_ALREADY_EXISTS", " UserId = " + user_id + " File name = " + fileName);
						ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_FILE_ALREADY_EXISTS;
						break;
					case 5:
						Log.getInstance().writeLog (Log.Error, "IDS_ERROR_POST_FILE_HASH_CONFLICT", " UserId = " + user_id + " File name = " + fileName);
						ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_FILE_HASH_CONFLICT;
						break;
					default:
						Debug.Assert(false);
						break;
				}
			}

			return ret;
		}
		/// <summary>
		/// Delete a file from the tracker given the info hash of the file.
		/// </summary>
		/// <param name="fileName">The file name being posted.</param>
		/// <param name="user_id">The user_id associated with the user.</param>
		/// <param name="infoHash">The info hash of the file to delete. Deletion is done by hash.</param>
		/// <returns>Zero if successful, error code otherwise.</returns>
		public int DeleteFromTracker( string fileName, int user_id, string infoHash)
		{
			int ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS;

			string ip = null;
			int port = 0;
			string torrentPath = null;
			string trackerUser = null;
			string trackerPass = null;

			TcpClient tcpClient = null;

			try
			{
				GetTrackerInfo( ref ip, ref port, ref torrentPath, ref trackerUser, ref trackerPass);

				tcpClient = new TcpClient();

				// Establishing the connection under IIS on a multi-homed box
				// is delayed 5-6 seconds. Non multi-homed works ok.
				tcpClient.Connect( IPAddress.Parse(ip), port );

				NetworkStream request = tcpClient.GetStream();

				StringBuilder sb3 = new StringBuilder();

				sb3.Append("GET /index.html?del=" + infoHash + "&ok=1 HTTP/1.1\r\n");
				sb3.Append("Cookie: login=\"" + trackerUser + "\"; md5=\"" + trackerPass + "\"\r\n");
				sb3.Append("Content-Length: 0\r\n\r\n");

				string sb3Header = sb3.ToString();
				byte[] sb3Bytes = Encoding.UTF8.GetBytes(sb3Header);

				request.Write(sb3Bytes, 0, sb3Bytes.Length);

				// Receive the response.
				Byte[] bytesReceived = new Byte[256];
				int bytes = 0;
				string page = "Return\r\n";

				do 
				{
					bytes = request.Read(bytesReceived, 0, bytesReceived.Length);
					page = page + Encoding.ASCII.GetString(bytesReceived, 0, bytes);
				}
				while (bytes > 0);

				ret = ParseTrackerDeleteResult(fileName, user_id, infoHash, page);
			}
			catch (Exception e)
			{
				Log.getInstance().writeLog (Log.Error, "IDS_EXCEPTION_DELETING_FROM_TRACKER", "Info Hash = " + infoHash + "  " + e.Message);
				ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.EXCEPTION_DELETING_FROM_TRACKER;
			}
			finally
			{
				if ( tcpClient != null )
				{
					tcpClient.Close();
				}
			}

			return ret;
		}

		/// <summary>
		/// Parse the returned page from the tracker when deleting a file for any error messages.
		/// For BNBT, these are embedded with javascript.
		/// </summary>
		/// <param name="fileName">The file name being deleted.</param>
		/// <param name="user_id">The user_id associated with the user.</param>
		/// <param name="page">The HTML page to parse, currently only BNBT support.</param>
		/// <returns>Zero if successful, error code otherwise.</returns>
		private int ParseTrackerDeleteResult(string fileName, int user_id, string infoHash, string page)
		{
			int ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS;

			// Returned page contains lightweight results from the BNBT tracker.
			string successText = "Torrent Deleted: " + infoHash;

			int successTextIndex = page.IndexOf(successText);

			if ( successTextIndex == -1 )
			{
				Log.getInstance().writeLog (Log.Error, "IDS_ERROR_DELETE_FILE_FAILURE", " UserId = " + user_id + " File name = " + fileName + " Info Hash = " + infoHash);
				ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_DELETE_FILE_FAILURE;
			}

			return ret;
		}

		/// <summary>
		/// Get configuration info about the tracker from the configuration file.
		/// </summary>
		/// <param name="ip">Out IP of tracker.</param>
		/// <param name="port">Out port of tracker.</param>
		/// <param name="torrentPath">Out IP of tracker.</param>
		/// <param name="trackerUser">The user used for login.</param>
		/// <param name="trackerMd5Pass">Password for given tracker user.</param>
		private void GetTrackerInfo( ref string ip, ref int port, ref string torrentPath, ref string trackerUser, ref string trackerMd5Pass)
		{
			ip = System.Configuration.ConfigurationSettings.AppSettings ["tracker_ip"];
			port = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings ["tracker_port"]);
			torrentPath = System.Configuration.ConfigurationSettings.AppSettings ["tracker_torrent_path"];
			trackerUser = System.Configuration.ConfigurationSettings.AppSettings ["tracker_user"];
			trackerMd5Pass = System.Configuration.ConfigurationSettings.AppSettings ["tracker_md5_password"];
		}

		/// <summary>
		/// Determines from web.config file whether posting to the tracker is enabled.
		/// </summary>
		/// <returns>true if posting to tracker is enabled. Otherwise, false.</returns>
		public bool EnabledTrackerPost()
		{
			BooleanSwitch postToBNBTTracker;

			postToBNBTTracker = new BooleanSwitch("KEPDS-BNBTTrackerPost","Controls KEPDS posting to BNBT Tracker");

			if ( postToBNBTTracker.Enabled)
			{
				return true;
			}

			return false;
		}
	}
}
