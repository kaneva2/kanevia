///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;

namespace KlausEnt.KEP.KEPDS
{
	/// <summary>
	/// </summary>
	public class AzureusUtility
	{
		public const String AZUREUS_IMPORT_CLIENT = "Azureus Import Client ";
		public const String AZUREUS_HOME_CLIENT = "Azureus Home Client ";
		public const String AZUREUS_HOME_CLIENT_CONTENT_REPOSITORY = "KEIcontent";

		static long m_clientDirNum = 0;
		static object classWideLock = new object();

		public AzureusUtility()
		{
		}

		public int PostToAzureusTracker( string fileName, int user_id, Stream fileToPost)
		{
			int ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS;

			TcpClient tcpClient = null;

			try
			{
				string ip = null;
				int port = 0;
				string torrentPath = null;

				GetTrackerInfo( ref ip, ref port, ref torrentPath);

				string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");

				StringBuilder sb = new StringBuilder();

				sb.Append("--");
				sb.Append(boundary);
				sb.Append("\r\n");
				sb.Append("Content-Disposition: form-data; name=\"");
				sb.Append("upfile");
				sb.Append("\"; filename=\"");
				sb.Append(torrentPath);
				sb.Append(fileName);
				sb.Append("\"");
				sb.Append("\r\n");
				sb.Append("Content-Type: ");
				sb.Append("application/x-bittorrent");
				sb.Append("\r\n");
				sb.Append("\r\n");

				string sb1Header = sb.ToString();
				byte[] sb1Bytes = Encoding.UTF8.GetBytes(sb1Header);

				StringBuilder sb2 = new StringBuilder();

				string contentName = fileName.Replace(".torrent","");

				sb2.Append("\r\n--");
				sb2.Append(boundary);
				sb2.Append("\r\n");
				sb2.Append("Content-Disposition: form-data; name=\"torrent_comment\"\r\n\r\n");
				sb2.Append(contentName);
				sb2.Append("\r\n");
				sb2.Append("--");
				sb2.Append(boundary);
				sb2.Append("\r\nContent-Disposition: form-data; name=\"torrent_hash\"\r\n\r\n");
				sb2.Append("\r\n");
				sb2.Append("--");
				sb2.Append(boundary);
				sb2.Append("\r\nContent-Disposition: form-data; name=\"torrent_announce_protocol\"\r\n\r\n");
				sb2.Append("HTTP");
				sb2.Append("\r\n");
				sb2.Append("--");
				sb2.Append(boundary);
				sb2.Append("\r\nContent-Disposition: form-data; name=\"torrent_include_hashes\"\r\n\r\n");
				sb2.Append("IncludeHashes");


				string sb2Header = sb2.ToString();
				byte[] sb2Bytes = Encoding.UTF8.GetBytes(sb2Header);

				byte[] boundaryBytes = Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");

				long length = sb2Bytes.Length + sb1Bytes.Length + fileToPost.Length + boundaryBytes.Length;

				tcpClient = new TcpClient();

				// Establishing the connection under IIS on a multi-homed box
				// is delayed 5-6 seconds. Non multi-homed works ok.
				tcpClient.Connect( IPAddress.Parse(ip), port );

				NetworkStream request = tcpClient.GetStream();

				StringBuilder sb3 = new StringBuilder();

				sb3.Append("POST /upload.cgi HTTP/1.1\r\n");
				sb3.Append("Content-Type: multipart/form-data; boundary=" + boundary + "\r\n");
				sb3.Append("Content-Length: " + length + "\r\n\r\n");

				string sb3Header = sb3.ToString();
				byte[] sb3Bytes = Encoding.UTF8.GetBytes(sb3Header);

				request.Write(sb3Bytes, 0, sb3Bytes.Length);
				request.Write(sb1Bytes, 0, sb1Bytes.Length);

				byte[] buffer = new byte[checked((uint)Math.Min(4096, (int)fileToPost.Length))];
				int bytesRead = 0;

				while ( (bytesRead = fileToPost.Read(buffer, 0, buffer.Length)) != 0 )
				{
					request.Write(buffer, 0, bytesRead);
				}

				request.Write(sb2Bytes, 0, sb2Bytes.Length);

				request.Write(boundaryBytes, 0, boundaryBytes.Length);

				request.Flush();

				// Receive the response.
				Byte[] bytesReceived = new Byte[256];
				int bytes = 0;
				string page = "Return\r\n";

				do 
				{
					bytes = request.Read(bytesReceived, 0, bytesReceived.Length);
					page = page + Encoding.ASCII.GetString(bytesReceived, 0, bytes);
				}
				while (bytes > 0);

				// Parse the page for error
				ret = ParseTrackerPostResult(fileName, user_id, page);
			}
			catch (Exception e)
			{
				Log.getInstance().writeLog (Log.Error, "IDS_EXCEPTION_POSTING_TO_TRACKER", e.Message);
				ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.EXCEPTION_POSTING_TO_TRACKER;
			} 
			finally
			{
				if ( tcpClient != null )
				{
					tcpClient.Close();
				}
			}

			return ret;
		}

		/// <summary>
		/// Parse the returned page from the tracker when posting a file for any error messages.
		/// For BNBT, these are embedded with javascript.
		/// </summary>
		/// <param name="fileName">The file name being posted.</param>
		/// <param name="user_id">The user_id associated with the user.</param>
		/// <param name="page">The HTML page to parse, currently only BNBT support.</param>
		/// <returns>Zero if successful, error code otherwise.</returns>
		private int ParseTrackerPostResult(string fileName, int user_id, string page)
		{
			int ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS;

			// Returned page contains lightweight results from the BNBT tracker.
			string successText = "Upload successful";

			int successTextIndex = page.IndexOf(successText);

			if ( successTextIndex == -1 )
			{
				Log.getInstance().writeLog (Log.Error, "IDS_ERROR_POST_TRACKER", " Filename = " + fileName + " Userid=" + user_id + " Page=" + page);
				ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_TRACKER;
			}

			return ret;
		}

		/// <summary>
		/// Post an asset file to a single common area of the Azureus tracker. Currently,
		/// the tracker does not import this torrent but it allows external torrents to be tracked.
		/// The torrent is placed in this directory for later retrieval when requested by another
		/// client. The content servers might not have the associated torrent due to the content
		/// being free and it alleviates the processing of retrieving the torrent from having to
		/// recursively walk multiple "Azureus Home Client N\torrents" directories.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="user_id">The user_id for the associated torrent file to post.</param>
		/// <param name="closeStream">Go ahead and close the stream.</param>
		/// <param name="fileToPost">The torrent file to post.</param>
		/// <returns>Zero if successful, error code otherwise.</returns>
		public int PostToAzureusTracker( string fileName, int user_id, bool closeStream, Stream fileToPost)
		{
			BinaryReader binReader = null;
			FileStream attachmentFile = null;

			int ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS;

			try
			{
				string azureusTrackerTorrentPath = System.Configuration.ConfigurationSettings.AppSettings ["azureus_tracker_torrent_path"];

				fileToPost.Seek(0, SeekOrigin.Begin); // reset in case we've already read it for some othe purpose

				binReader = new BinaryReader(fileToPost);

				attachmentFile = new FileStream( azureusTrackerTorrentPath + "\\" + fileName, FileMode.Create, FileAccess.Write);

				// Write bytes to the output stream.

				int size = 4096;
				byte[] bytes = new byte[size];
				int numBytes;
				while((numBytes = binReader.Read(bytes, 0, size)) > 0)
				{
					attachmentFile.Write(bytes, 0, numBytes);
				}
			}
			catch (Exception e)
			{
				Log.getInstance().writeLog (Log.Error, "IDS_EXCEPTION_POSTING_TO_TRACKER", " Filename = " + fileName + " Userid=" + user_id + " Exception:" + e.Message);
				ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.EXCEPTION_POSTING_TO_TRACKER;
			}
			finally
			{
				if ( (binReader != null) && (closeStream) )
				{
					binReader.Close();
				}
				if ( attachmentFile != null)
				{
					attachmentFile.Close();
				}
			}

			return ret;
		}

		/// <summary>
		/// get the path of next azureus content repository to post
		/// </summary>
		/// <returns></returns>
		public int GetNextAzureusContentPath(ref string contentPath, ref long clientNumber)
		{
			int ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS;
			try
			{
				//synchronize the call to GetNextAzureusPostPath at class level
				//in other words, only one instance of Azureus can access GetNextAzureusPostPath() at the same time
				lock(classWideLock)
				{
					string azureusTorrentPath = null;
					long startDirNum = 0;
					ret = GetNextAzureusPostPath( ref startDirNum, ref azureusTorrentPath, ref clientNumber);
					if ( ret != (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS)
					{
						return (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_TO_AZUREUS;
					}
					contentPath = System.Configuration.ConfigurationSettings.AppSettings ["azureus_torrent_path"];
					contentPath +=  AZUREUS_HOME_CLIENT + m_clientDirNum + "\\KEIcontent\\"; 
				}
			}
			catch (Exception e)
			{
				Log.getInstance().writeLog (Log.Error, "IDS_ERROR_IMPORT_AZUREUS_CONTENT_EXCEPTION", e.StackTrace);
				ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_TO_AZUREUS;
			}
			return ret;
		}

		/// <summary>
		/// Post an asset file to an Azureus client application's import directory. This client
		/// application is functioning as a content server. Multiple clients may exist
		/// in the short term to provide sufficient bandwidth as these clients are utilized
		/// in a server capacity.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="fileToPost">The torrent file to post.</param>
		/// <param name="contentLocation">target directory of content</param>
		/// <returns>Zero if successful, error code otherwise.</returns>
		public int PostToAzureus( string fileName, Stream fileToPost, ref string contentLocation)
		{
			int ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS;
			string azureusTorrentPath = null;
			BinaryReader binReader = null;
			FileStream attachmentFile = null;

			contentLocation = null;

			try
			{
				lock(this)
				{
					long startDirNum = 0;
					long clientNumber = 0;
					ret = GetNextAzureusPostPath( ref startDirNum, ref azureusTorrentPath, ref clientNumber);
					if ( ret != (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS)
					{
						return (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_TO_AZUREUS;
					}
				}

				contentLocation = System.Configuration.ConfigurationSettings.AppSettings ["azureus_torrent_path"];
				contentLocation +=  AZUREUS_HOME_CLIENT + m_clientDirNum + "\\KEIcontent\\";

				fileToPost.Seek(0, SeekOrigin.Begin); // reset in case we've already read it for some othe purpose

				binReader = new BinaryReader(fileToPost);

				attachmentFile = new FileStream( azureusTorrentPath + "\\" + fileName, FileMode.Create, FileAccess.Write);

				// Write bytes to the output stream.

				int size = 4096;
				byte[] bytes = new byte[size];
				int numBytes;
				while((numBytes = binReader.Read(bytes, 0, size)) > 0)
				{
					attachmentFile.Write(bytes, 0, numBytes);
				}
			}
			catch (Exception e)
			{
				Log.getInstance().writeLog (Log.Error, "IDS_ERROR_POST_AZUREUS_EXCEPTION", " Filename = " + fileName + " Userid=" + " Exception:" + e.Message);
				ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_TO_AZUREUS;
			}
			finally
			{
				if ( binReader != null)
				{
					binReader.Close();
				}
				if ( attachmentFile != null)
				{
					attachmentFile.Close();
				}
			}

			return ret;
		}

		/// <summary>
		/// Post an asset file to a specific content server
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="fileToPost">The torrent file to post.</param>
		/// <param name="contentServerNum">The sequence number of the content server to post to.</param>/// 
		/// <returns>Zero if successful, error code otherwise.</returns>
		public int PostToSpecifiedAzureus( string fileName, Stream fileToPost, long contentServerNum)
		{
			int ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS;
			string azureusTorrentPath = null;
			BinaryReader binReader = null;
			FileStream attachmentFile = null;

			try
			{
				lock(this)
				{
					ret = GetAzureusPostPathBySquenceId( contentServerNum, ref azureusTorrentPath);
					if ( ret != (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS)
					{
						return (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_TO_AZUREUS;
					}
				}

				fileToPost.Seek(0, SeekOrigin.Begin); // reset in case we've already read it for some othe purpose

				binReader = new BinaryReader(fileToPost);

				attachmentFile = new FileStream( azureusTorrentPath + "\\" + fileName, FileMode.Create, FileAccess.Write);
				// Write bytes to the output stream.

				int size = 4096;
				byte[] bytes = new byte[size];
				int numBytes;
				while((numBytes = binReader.Read(bytes, 0, size)) > 0)
				{
					attachmentFile.Write(bytes, 0, numBytes);
				}
			}
			catch (Exception e)
			{
				Log.getInstance().writeLog (Log.Error, "IDS_ERROR_POST_AZUREUS_EXCEPTION", " Filename = " + fileName + " Userid=" + " Exception:" + e.Message);
				ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_TO_AZUREUS;
			}
			finally
			{
				if ( binReader != null)
				{
					binReader.Close();
				}
				if ( attachmentFile != null)
				{
					attachmentFile.Close();
				}
			}

			return ret;
		}

		/// <summary>
		/// return content server torrent import dir by sequence id
		/// </summary>
		/// <param name="clientDirNum"></param>
		/// <param name="targetDir"></param>
		/// <returns></returns>
		private int GetAzureusPostPathBySquenceId( long clientDirNum, ref string targetDir)
		{
			int ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS;
			try
			{
				string azureusTorrentPath = System.Configuration.ConfigurationSettings.AppSettings ["azureus_torrent_path"];

				DirectoryInfo clientDir = new DirectoryInfo(azureusTorrentPath + AZUREUS_IMPORT_CLIENT + clientDirNum);

				if ( ! clientDir.Exists )
				{
					Log.getInstance().writeLog (Log.Error, "IDS_ERROR_POST_AZUREUS_NO_TARGET_INITIAL_DIR", "");
					ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_TO_AZUREUS;
				}else
				{
					targetDir = clientDir.FullName;
				}
			}
			catch (Exception e)
			{
				Log.getInstance().writeLog (Log.Error, "IDS_ERROR_POST_AZUREUS_EXCEPTION", e.Message);
				ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_TO_AZUREUS;
			}
			return ret;
		}



		/// <summary>
		/// Obtain the next Azureus client's import directory. The design is to drop the
		/// torrent files into import directories in a round robin fashion. The directories
		/// will be named "Azureus Import Client 1", "Azureus Import Client 2", etc. Within each directory is
		/// a config file named "HOT.CFG" that contains the maximum number of entries the
		/// client can support. The key/value pair is max=N where N represents the number
		/// of entries. The directories must be in ascending order, although, the HOT.CFG file can be
		/// used to control deposits of torrents, e.g. rename it. Once an entry is imported, it should
		/// show up in "Azureus Home Client 1\torrents" (where N is the same as the import directory number).
		/// This torrents directory is where max is checked against.
		/// 
		/// Notes: protect the invocation of this method with a lock 
		/// 
		/// </summary>
		/// <param name="startDirNum">This method recursively calls itself as it walks the directories.
		///	This parameter will be Zero when called initially (outer methods). Recursion calls will use this
		///	parameter to detect when all directories have been cycled over when looking for a target.
		///	<param name="targetDir">Output target directory for torrent.</param>
		/// <returns>Zero if successful, error code otherwise.</returns>
		private int GetNextAzureusPostPath( ref long startDirNum, ref string targetDir, ref long clientNumber)
		{
			int ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS;

			bool tryNextDir = false;

			m_clientDirNum++;

			if ( startDirNum == 0)
			{
				startDirNum = m_clientDirNum;
			}
			else
			{
				if (startDirNum == m_clientDirNum)
				{
					// we've recursively called ourselves enough times that we've wrapped
					// around all the directories
					Log.getInstance().writeLog (Log.Error, "IDS_ERROR_POST_AZUREUS_NO_TARGET_DIR_CYCLE", "");
					return (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_TO_AZUREUS;
				}
			}

			try
			{
				string azureusTorrentPath = System.Configuration.ConfigurationSettings.AppSettings ["azureus_torrent_path"];

				DirectoryInfo clientDir = new DirectoryInfo(azureusTorrentPath + AZUREUS_IMPORT_CLIENT + m_clientDirNum);
				if ( ! clientDir.Exists )
				{
					if ( startDirNum == 1)
					{
						Log.getInstance().writeLog (Log.Error, "IDS_ERROR_POST_AZUREUS_NO_TARGET_INITIAL_DIR", "");
						ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_TO_AZUREUS;
					}
					else
					{
						// Reset back to first directory
						m_clientDirNum = 1;
						clientDir = new DirectoryInfo(azureusTorrentPath + AZUREUS_IMPORT_CLIENT + m_clientDirNum);
						if ( ! clientDir.Exists )
						{
							Log.getInstance().writeLog (Log.Error, "IDS_ERROR_POST_AZUREUS_NO_TARGET_INITIAL_DIR", "");
							ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_TO_AZUREUS;
						}
					}
				}

				if ( ret == (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS)
				{
					BinaryReader binReader = null;

					try
					{
						binReader = new BinaryReader(File.Open(azureusTorrentPath + AZUREUS_IMPORT_CLIENT + m_clientDirNum + "\\HOT.CFG", FileMode.Open));

						byte[] bytes = new byte[32];
						int numBytes= binReader.Read(bytes, 0, 32);
						if (numBytes < 6) // Line should be "Max=N" where N is some number of entries
						{
							Log.getInstance().writeLog (Log.Error, "IDS_ERROR_POST_AZUREUS_CFG_ERROR", " File " + azureusTorrentPath + "Azureus Client " + m_clientDirNum + "\\HOT.CFG");
							ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_TO_AZUREUS;
						}
						else
						{
							string maxFilesStr = System.Text.Encoding.ASCII.GetString(bytes, 4, (numBytes - 4));
							long maxFiles = Convert.ToInt32(maxFilesStr);

							DirectoryInfo clientHomeDir = new DirectoryInfo(azureusTorrentPath + AZUREUS_HOME_CLIENT + m_clientDirNum + @"\torrents");
							long currentNumFiles = clientHomeDir.GetFileSystemInfos().Length;

							if ( currentNumFiles >= maxFiles)
							{
								tryNextDir = true;
							}
							else
							{
								targetDir = clientDir.FullName;
								clientNumber = m_clientDirNum;
							}
						}
					}
					catch (FileNotFoundException)
					{
						tryNextDir = true;
					}
					catch (Exception e)
					{
						Log.getInstance().writeLog (Log.Error, "IDS_ERROR_POST_AZUREUS_EXCEPTION", e.Message);
						ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_TO_AZUREUS;
					}
					finally
					{
						if ( binReader != null)
						{
							binReader.Close();
						}
					}
				}
			}
			catch (Exception e)
			{
				Log.getInstance().writeLog (Log.Error, "IDS_ERROR_POST_AZUREUS_EXCEPTION", e.Message);
				ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_TO_AZUREUS;
			}

			if ( (ret == (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS) && (tryNextDir) )
			{	
				ret = GetNextAzureusPostPath( ref startDirNum, ref targetDir, ref clientNumber);
			}

			return ret;
		}

		/// <summary>
		/// Get configuration info about the tracker from the configuration file.
		/// </summary>
		/// <param name="ip">Out IP of tracker.</param>
		/// <param name="port">Out port of tracker.</param>
		/// <param name="torrentPath">Out IP of tracker.</param>
		private void GetTrackerInfo( ref string ip, ref int port, ref string torrentPath)
		{
			ip = System.Configuration.ConfigurationSettings.AppSettings ["tracker_ip"];
			port = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings ["tracker_port"]);
			torrentPath = System.Configuration.ConfigurationSettings.AppSettings ["tracker_torrent_path"];
		}

		/// <summary>
		/// Determines from web.config file whether posting to the tracker is enabled.
		/// </summary>
		/// <returns>true if posting to tracker is enabled. Otherwise, false.</returns>
		public bool EnabledTrackerPost()
		{
			BooleanSwitch postToAzureusTracker;

			postToAzureusTracker = new BooleanSwitch("KEPDS-AzureusTrackerPost","Controls KEPDS posting to Azureus Tracker");

			if ( postToAzureusTracker.Enabled)
			{
				return true;
			}

			return false;
		}

		/// <summary>
		/// Determines from web.config file whether posting to the content server is enabled.
		/// </summary>
		/// <returns>true if posting to tracker is enabled. Otherwise, false.</returns>
		public bool EnabledContentPost()
		{
			BooleanSwitch postToAzureus;

			postToAzureus = new BooleanSwitch("KEPDS-AzureusPost","Controls KEPDS posting to Azureus client");

			if ( postToAzureus.Enabled)
			{
				return true;
			}

			return false;
		}

		/// <summary>
		/// Determines from web.config file whether posting to the content server is enabled.
		/// </summary>
		/// <returns>true if posting to tracker is enabled. Otherwise, false.</returns>
		public bool EnabledFreeContentPost()
		{
			BooleanSwitch postToAzureusFree;

			postToAzureusFree = new BooleanSwitch("KEPDS-AzureusPostFree","Controls KEPDS posting to Azureus client for free content");

			if ( postToAzureusFree.Enabled)
			{
				return true;
			}

			return false;
		}
	}
}