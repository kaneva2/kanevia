///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;

namespace KlausEnt.KEP.KEPDS
{
	/// <summary>
	/// </summary>
	public class ABCUtility
	{
		public ABCUtility()
		{
		}

		/// <summary>
		/// Post the torrent file to an ABC client via its remote interface.
		/// </summary>
		/// <param name="fileName">The name of the torrent file.</param>
		/// <returns>Zero if successful, error code otherwise.</returns>
		public int PostToABC( string fileName)
		{
			int ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS;

			TcpClient tcpClient = null;

			try
			{
				string ipABC = null;
				int portABC = 0;
				string keyABC = null;
				string ipTorrent= null;
				string torrentFolder = null;

				GetABCInfo( ref ipABC, ref portABC, ref keyABC, ref ipTorrent, ref torrentFolder);

				tcpClient = new TcpClient();

				tcpClient.Connect( ipABC, portABC);

				NetworkStream request = tcpClient.GetStream();

				byte[] sendBytes = Encoding.ASCII.GetBytes("ID|" + keyABC + "\nADD|http://" + ipTorrent + "/" + torrentFolder + "/" + fileName);
				string y = "ID|" + keyABC + "\nADD|http://" + ipTorrent + "/" + torrentFolder + "/" + fileName;

				request.Write(sendBytes, 0, sendBytes.Length);

				request.Flush();

				// Receive the response.
				Byte[] bytesReceived = new Byte[256];
				int bytes = 0;
				string page = null;

				do 
				{
					bytes = request.Read(bytesReceived, 0, bytesReceived.Length);
					page = page + Encoding.ASCII.GetString(bytesReceived, 0, bytes);
				}
				while (bytes > 0);

				ret = ParseABCPostResult(fileName, page);
			}
			catch (Exception e)
			{
				Log.getInstance().writeLog (Log.Error, "IDS_EXCEPTION_POSTING_TO_ABC", e.Message);
				ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.EXCEPTION_POSTING_TO_ABC;
			} 
			finally
			{
				if ( tcpClient != null )
				{
					tcpClient.Close();
				}
			}

			return ret;
		}


		/// <summary>
		/// Get configuration info about the optional ABC client from the configuration file.
		/// </summary>
		/// <param name="ipABC">Out IP of ABC client.</param>
		/// <param name="portABC">Out port ABC client.</param>
		/// <param name="keyABC">ABC key to use in remote interface.</param>
		/// <param name="ipTorrent">Out IP of web site that points to torrent files</param>
		/// <param name="torrentFolder">Folder of web site that points to torrent files.</param>
		private void GetABCInfo( ref string ipABC, ref int portABC, ref string keyABC, ref string ipTorrent, ref string torrentFolder)
		{
			ipABC = System.Configuration.ConfigurationSettings.AppSettings ["ABC_ip"];
			portABC = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings ["ABC_port"]);
			keyABC = System.Configuration.ConfigurationSettings.AppSettings ["ABC_key"];
			ipTorrent = System.Configuration.ConfigurationSettings.AppSettings ["torrent_path_ip"];
			torrentFolder = System.Configuration.ConfigurationSettings.AppSettings ["torrent_path_folder"];
		}


		/// <summary>
		/// Parse the returned page from the ABC client when posting a file for any error messages.
		/// An exhaustive list is not known.
		/// </summary>
		/// <param name="fileName">The file name being posted.</param>
		/// <returns>Zero if successful, error code otherwise.</returns>
		private int ParseABCPostResult(string fileName, string page)
		{
			int ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS;

			// The list we've been able to accumulate via working with ABC
			// If the return isn't ok, but doesn't match an error, return a global error.

			string okReturn = "Feedback\nOK";
			string duplicateReturn = "Feedback\nError=This torrent is duplicate";
			string urlNotFoundReturn = "Feedback\nError=Can't get torrent from URL";
			
			if ( page != okReturn)
			{
				if ( page == duplicateReturn)
				{
					Log.getInstance().writeLog (Log.Error, "IDS_EXCEPTION_POSTING_TO_ABC", " " + fileName + " is duplicate");
					ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_FILE_ABC_DUPLICATE;
				}
				else if ( page == urlNotFoundReturn)
				{
					Log.getInstance().writeLog (Log.Error, "IDS_EXCEPTION_POSTING_TO_ABC", " bad url given to ABC");
					ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_FILE_ABC_URL_BAD;
				}
				else
				{
					Log.getInstance().writeLog (Log.Error, "IDS_EXCEPTION_POSTING_TO_ABC", " " + page);
					ret = (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.ERROR_POST_FILE_ABC_UNKNOWN_ERROR;
				}
			}

			return ret;
		}
		/// <summary>
		/// Determines from web.config file whether posting to the content server is enabled.
		/// </summary>
		/// <returns>true if posting to tracker is enabled. Otherwise, false.</returns>
		public bool EnabledContentPost()
		{
			BooleanSwitch postToABC;

			postToABC = new BooleanSwitch("KEPDS-ABCPost","Controls KEPDS posting to ABC client");

			if ( postToABC.Enabled)
			{
				return true;
			}

			return false;
		}
}
}
