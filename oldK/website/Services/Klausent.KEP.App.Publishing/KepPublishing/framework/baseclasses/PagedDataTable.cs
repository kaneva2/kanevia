///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for PagedDataTable.
	/// </summary>
	public class PagedDataTable : DataTable
	{
		public PagedDataTable()
		{
		}

		/// <summary>
		/// The number of items in the datasource
		/// </summary>
		public int TotalCount
		{
			get 
			{
				return m_TotalCount;
			} 
			set
			{
				m_TotalCount = value;
			}
		}

		private int m_TotalCount = 0;

	}
}
