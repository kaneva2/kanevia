///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;

using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Data;

using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for KanevaGlobals.
	/// </summary>
	public class KanevaGlobals
	{
		public KanevaGlobals()
		{
		}

		/// <summary>
		/// IsPottyMouth
		/// </summary>
		public static bool IsPottyMouth (string strText)
		{
			try
			{
				strText = strText.ToUpper ();

				if 
					(
					(strText.IndexOf ("SHIT") > -1) || (strText.IndexOf ("FUCK") > -1)
					|| (strText.IndexOf ("PUSSY") > -1) || strText.Equals ("STFU") 
					|| strText.Equals ("COCK") || strText.Equals ("COCKSUCKER") 
					|| strText.Equals ("TITS") || strText.Equals ("WHORE") || strText.Equals ("PENIS")
					|| strText.Equals ("FAG") || strText.Equals ("HOMO") || strText.Equals ("GAY") || strText.Equals ("LESBIAN") || strText.Equals ("LESBO") 
					|| strText.Equals ("QUEER") || strText.Equals ("NIGGER") || strText.Equals ("NIGGA") 
					|| strText.Equals ("WTF") || strText.Equals ("SEX") || strText.Equals ("ANAL") 
					|| strText.Equals ("BOOB") || strText.Equals ("BOOBS") 
					|| strText.Equals ("ASS") || strText.Equals ("ASSHOLE")
					|| strText.Equals ("CUNT") || strText.Equals ("VAGINA")
					|| strText.Equals ("NOOB") || strText.Equals ("PRICK")
					)
				{
					return true;
				}

//				#if KSERVICE
//				{
//					return false;
//				}
//				#else
//				{
//					// Else look up the database table
//					DataTable dtPottyMouth = WebCache.GetPottyWords ();
//					return (dtPottyMouth.Select ("bad_word = '" + CleanText (strText) + "'").Length > 0);
//				}
//				#endif
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error in IsPottyMouth ", exc);
			}
			return false;
		}

		/// <summary>
		/// Clean text for db insertion
		/// </summary>
		/// <param name="strText"></param>
		/// <returns></returns>
		public static string CleanText (string strText)
		{
			strText = strText.Replace ("'", "''");
			strText = strText.Replace ("\\", "\\\\");
			return strText.Replace ("\"", "\"\"");
		}

		/// <summary>
		/// Clean text for db insertion
		/// </summary>
		/// <param name="strText"></param>
		/// <returns></returns>
		public static string CleanHtmlEditorText (string strText)
		{
			// This is the only cleaning we need done with the HTML editor
			// It already does ".
			return strText.Replace ("'", "''");
		}

		/// <summary>
		/// Clean text for javascript
		/// </summary>
		/// <param name="strText"></param>
		/// <returns></returns>
		public static string CleanJavascript (string strText)
		{
			return strText.Replace ("'", "\\'");
		}

		/// <summary>
		/// Clean text for javascript
		/// </summary>
		/// <param name="strText"></param>
		/// <returns></returns>
		public static string CleanJavascriptFull (string strText)
		{
			return (strText.Replace ("'", "\\'")).Replace("\r\n","\\r\\n");
		}

		/// <summary>
		/// Get the time difference
		/// </summary>
		/// <returns></returns>
		public static string GetDateTimeDifference (Object dtStartDate)
		{
			DateTime dtCurrentTime = KanevaGlobals.GetCurrentDateTime ();
			TimeSpan tsDifference;

			if (dtStartDate.Equals (DBNull.Value))
			{
				return "";
			}
			
			tsDifference = dtCurrentTime - (DateTime) dtStartDate;
			return tsDifference.Days.ToString ("#,##0") + "d " + tsDifference.Hours.ToString ("#0") + "h " + tsDifference.Minutes.ToString ("#0") + "m";
		}

//		/// <summary>
//		/// Get the formated age for display
//		/// </summary>
//		/// <param name="dtBirthDate"></param>
//		/// <returns></returns>
//		public static string GetFormatedAge (Object dtBirthDate)
//		{
//			return (GetAgeInYears (dtBirthDate).ToString ("#,##0"));
//		}

		/// <summary>
		/// Get someones age in years - doesn't take objects or nulls
		/// </summary>
		/// <param name="birthDay"></param>
		/// <returns></returns>
		/// 
		public static int GetAgeInYears (DateTime birthDay)
		{
			//retreive current time from database
			DateTime dtCurrentTime = KanevaGlobals.GetCurrentDateTime ();

			//.25 an attempt to correct for leap years
			double age = (((TimeSpan)dtCurrentTime.Subtract(birthDay.Date)).Days) / 365.25;
			//special case check for day of birthday scenarios
			if((birthDay.Month == dtCurrentTime.Month) && (birthDay.Day == dtCurrentTime.Day))
			{
				return Convert.ToInt32(Math.Ceiling(age));
			}
			return Convert.ToInt32(Math.Floor(age));
		}


		/// <summary>
		/// Get the current date time
		/// </summary>
		/// <returns></returns>
		public static DateTime GetCurrentDateTime ()
		{
			return GetDatabaseUtility ().GetCurrentDateTime ();
		}

		/// <summary>
		/// This method shortens a string to a certain length and
		/// adds ... when the string is longer than a specified
		/// maximum length.
		/// </summary>
		/// <param name="text"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static string TruncateWithEllipsis(string text, int length) 
		{
			if (text.Length > length)
				text = text.Substring(0,length) + HttpContext.Current.Server.HtmlDecode("&#8230;");
			return text;
		}

		/// <summary>
		/// This method chops off the end of a string when the
		/// string is longer than a maximum length.
		/// </summary>
		/// <param name="text"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static string Truncate(string text, int length) 
		{
			if (text.Length > length)
				text = text.Substring(0,length);
			return text;
		}

		/// <summary>
		/// Represents the path of the current application.
		/// </summary>
		public static string AppPath 
		{
			get 
			{
				if (HttpContext.Current.Request.ApplicationPath == "/")
					return String.Empty;
				return HttpContext.Current.Request.ApplicationPath;
			}
		}

		public static string ResolveAbsoluteUrl (string Url) 
		{
			if (HttpContext.Current.Request.IsSecureConnection)
				return "https://" + Url;
			else
				return "http://" + Url;        
		}


		/// <summary>
		/// Removes everything from a request except for the page name.
		/// </summary>
		/// <param name="requestPath"></param>
		/// <returns></returns>
		public static string GetPageName(string requestPath)
		{
			// Remove query string
			if ( requestPath.IndexOf( '?' ) != -1 )
				requestPath = requestPath.Substring( 0, requestPath.IndexOf( '?' ) );       

			// Remove base path
			return requestPath.Remove( 0, requestPath.LastIndexOf( "/" ) );
		}

		/// <summary>
		/// Retrieves Blogs per page from Web.Config file.
		/// </summary>
		public static int DatabaseType
		{
			get 
			{
				switch (System.Configuration.ConfigurationManager.AppSettings ["databaseType"])
				{
					case "SQLServer":
					{
						return (int) Constants.eSUPPORTED_DATABASES.SQLSERVER;
					}
					case "MySQL":
					{
						return (int) Constants.eSUPPORTED_DATABASES.MYSQL;
					}
				}

				return 0;
			} 
		}

		public static string WokPatcherUrl 
		{
			get
			{
				string ret = System.Configuration.ConfigurationManager.AppSettings ["wokPatcherUrl"];
				if ( ret == null )
					ret = "http://www.kaneva.com/patch/wok";
				return ret;
			}
		}
		/// <summary>
		/// Retrieves the Announce URL
		/// </summary>
		public static string AnnounceURL
		{
			get 
			{
				return "http://" + System.Configuration.ConfigurationManager.AppSettings ["tracker_ip"] + ":" + System.Configuration.ConfigurationManager.AppSettings ["tracker_port"] + "/announce";;
			} 
		}

		/// <summary>
		/// Retrieves Blogs per page from Web.Config file.
		/// </summary>
		public static int BlogsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["BlogsPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves Assets per page from Web.Config file.
		/// </summary>
		public static int AssetsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["AssetsPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves Servers per page from Web.Config file.
		/// </summary>
		public static int ServersPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["ServersPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves ServersShowDaysOld per page from Web.Config file.
		/// </summary>
		public static int ServersShowDaysOld
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["ServersShowDaysOld"] == null)
				{
					return 30;
				}
				else
				{
					return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["ServersShowDaysOld"]);
				}
			} 
		}

		/// <summary>
		/// Retrieves cutoff age of minor
		/// </summary>
		public static int MinorCutOffAge
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["AgeCutOff"] == null)
				{
					return 14;
				}
				else
				{
					return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["AgeCutOff"]);
				}
			} 
		}


		/// <summary>
		/// Retrieves Communities per page from Web.Config file.
		/// </summary>
		public static int CommunitiesPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["CommunitiesPerPage"]);
			} 
		}
		
		/// <summary>
		/// Retrieves World Objects per page from Web.Config file.
		/// </summary>
		public static int WorldObjectsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["WorldObjectsPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves Members per page from Web.Config file.
		/// </summary>
		public static int MembersPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MembersPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves Topics per page from Web.Config file.
		/// </summary>
		public static int TopicsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["TopicsPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves Threads per page from Web.Config file.
		/// </summary>
		public static int ThreadsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["ThreadsPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves TransactionsPerPage per page from Web.Config file.
		/// </summary>
		public static int TransactionsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["TransactionsPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves CreditTransactionsPerPage per page from Web.Config file.
		/// </summary>
		public static int CreditTransactionsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["CreditTransactionsPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves Friends per page from Web.Config file.
		/// </summary>
		public static int FriendsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["FriendsPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves SearchResultsPerPage per page from Web.Config file.
		/// </summary>
		public static int SearchResultsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["SearchResultsPerPage"]);
			} 
		}

		/// <summary>
		/// Use Search Farm?
		/// </summary>
		public static bool UseSearchFarm 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["UseSearchFarm"] == null)
				{
					return false;
				}
				else
				{
					return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings ["UseSearchFarm"]);
				}
			} 
		}

		/// <summary>
		/// Max uploadable image size
		/// </summary>
		public static int MaxUploadedImageSize
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MaxUploadedImageSize"]);
			} 
		}

		/// <summary>
		/// Max uploadable Avatar size
		/// </summary>
		public static int MaxUploadedAvatarSize
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MaxUploadedAvatarSize"]);;
			} 
		}

		/// <summary>
		/// Max uploadable Avatar size
		/// </summary>
		public static int MaxUploadedBannerSize
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MaxUploadedBannerSize"]);;
			} 
		}

		/// <summary>
		/// Max uploadable Screenshot size
		/// </summary>
		public static int MaxUploadedScreenShotSize
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MaxUploadedScreenShotSize"]);
			} 
		}

		/// <summary>
		/// MaxNumberOfCummunitiesPerUser
		/// </summary>
		public static int MaxNumberOfCummunitiesPerUser
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MaxNumberOfCummunitiesPerUser"]);
			} 
		}

		/// <summary>
		/// MaxNumberOfLogicalAccounts
		/// </summary>
		public static int MaxNumberOfLogicalAccounts
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MaxNumberOfLogicalAccounts"]);
			} 
		}

		/// <summary>
		/// MaxCommentLength
		/// </summary>
		public static int MaxCommentLength
		{
			get 
			{
				try
				{
					int len = Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MaxCommentLength"]);

					if (len > 0)
					{
						return len;
					}
					else
					{
						return 256;
					}
				}
				catch 
				{
					return 256;
				}
			} 
		}

		/// <summary>
		/// MaxBlastLength
		/// </summary>
		public static int MaxBlastLength
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["MaxBlastLength"] == null)
				{
					return 140;
				}
				else
				{
					return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MaxBlastLength"]);
				}
			} 
		}

		/// <summary>
		/// NumberOfDaysDownloadIsAvailable
		/// </summary>
		public static int NumberOfDaysDownloadIsAvailable
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["NumberOfDaysDownloadIsAvailable"]);
			} 
		}

		// NumberOfDaysDownloadStopDate
		public static int NumberOfDaysDownloadStopDate
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["NumberOfDaysDownloadStopDate"]);
			} 
		}

		/// <summary>
		/// DefaultPercentRoyaltyPaid
		/// </summary>
		public static int DefaultPercentRoyaltyPaid
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["DefaultPercentRoyaltyPaid"]);
			} 
		}

		/// <summary>
		/// Get list of trackers
		/// </summary>
		public static System.Collections.Hashtable Trackers
		{
			get
			{
				return (System.Collections.Hashtable) System.Configuration.ConfigurationManager.GetSection ("BitTorrentTrackers");
			}
		}

		/// <summary>
		/// Get list of possible Media Converter Servers
		/// </summary>
		public static System.Collections.Hashtable MediaConverters
		{
			get
			{
                return (System.Collections.Hashtable)System.Configuration.ConfigurationManager.GetSection("MediaConverters");
			}
		}

		/// <summary>
		/// Get list of content servers
		/// </summary>
		public static System.Collections.Hashtable ContentServers
		{
			get
			{
                return (System.Collections.Hashtable)System.Configuration.ConfigurationManager.GetSection("ContentServersSettings");
			}
		}

		/// <summary>
		/// Get list of upload servers
		/// </summary>
		public static System.Collections.Hashtable UploadServers
		{
			get
			{
                return (System.Collections.Hashtable)System.Configuration.ConfigurationManager.GetSection("UploadServersSettings");
			}
		}

		/// <summary>
		/// ShareCatchpaCount
		/// </summary>
		public static int ShareCatchpaCount
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["ShareCatchpaCount"] == null)
				{
					return 50;
				}
				else
				{
					return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["ShareCatchpaCount"]);
				}
			} 
		}

		/// <summary>
		/// Retrieves the tracker username from the Web.Config file.
		/// </summary>
		public static string TrackerUser 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings ["dbEncrypt"].ToUpper ().Equals ("TRUE"))
				{
					// Encrypt (System.Configuration.ConfigurationManager.AppSettings ["tracker_user"]);
					return Decrypt (System.Configuration.ConfigurationManager.AppSettings ["tracker_user"]);
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["tracker_user"];
				}
			} 
		}

		/// <summary>
		/// Retrieves the tracker username from the Web.Config file.
		/// </summary>
		public static string TrackerPass 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings ["dbEncrypt"].ToUpper ().Equals ("TRUE"))
				{
					// Encrypt (System.Configuration.ConfigurationManager.AppSettings ["tracker_pass"]);
					return Decrypt (System.Configuration.ConfigurationManager.AppSettings ["tracker_pass"]);
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["tracker_pass"];
				}
			} 
		}

		/// <summary>
		/// Retrieves database connection string from Web.Config file.
		/// </summary>
        //[Obsolete("Use new Data Access Layer")]
		public static string ConnectionString 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings ["dbEncrypt"].ToUpper ().Equals ("TRUE"))
				{
					// Encrypt (System.Configuration.ConfigurationManager.AppSettings ["ConnectionString"]);
					return Decrypt (System.Configuration.ConfigurationManager.AppSettings ["ConnectionString"]);
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["ConnectionString"];
				}
			} 
		}

		/// <summary>
		/// Retrieves database connection string from Web.Config file.
		/// </summary>
        //[Obsolete("Use new Data Access Layer")]
		public static string ConnectionStringReadOnly 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings ["dbEncrypt"].ToUpper ().Equals ("TRUE"))
				{
					if (System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringReadOnly"] != null)
					{
						// Encrypt (System.Configuration.ConfigurationManager.AppSettings ["ConnectionString"]);
						return Decrypt (System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringReadOnly"]);
					}
					else
					{
						return ConnectionString;
					}
				}
				else
				{
					if (System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringReadOnly"] != null)
					{
						return System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringReadOnly"];
					}
					else
					{
						return ConnectionString;
					}
				}
			} 
		}

		/// <summary>
		/// Retrieves KGP database connection string from Web.Config file.
		/// </summary>
        //[Obsolete("Use new Data Access Layer")]
		public static string ConnectionStringKGP 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings ["dbEncrypt"].ToUpper ().Equals ("TRUE"))
				{
					return Decrypt (System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringKGP"]);
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringKGP"];
				}
			} 
		}

		/// <summary>
		/// Retrieves database connection string for Search from Web.Config file.
		/// </summary>
        //[Obsolete("Use new Data Access Layer")]
		public static string ConnectionStringSearch 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings ["dbEncrypt"].ToUpper ().Equals ("TRUE"))
				{
					if (System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringSearch"] != null)
					{
						return Decrypt (System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringSearch"]);
					}
					else
					{
						return ConnectionString;
					}
				}
				else
				{
					if (System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringSearch"] != null)
					{
						return System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringSearch"];
					}
					else
					{
						return ConnectionString;
					}
				}
			} 
		}

		/// <summary>
		/// Retrieves database connection string for Master Search from Web.Config file.
		/// </summary>
        //[Obsolete("Use new Data Access Layer")]
		public static string ConnectionStringMasterSearch 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings ["dbEncrypt"].ToUpper ().Equals ("TRUE"))
				{
					if (System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringMasterSearch"] != null)
					{
						return Decrypt (System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringMasterSearch"]);
					}
					else
					{
						return ConnectionString;
					}
				}
				else
				{
					if (System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringMasterSearch"] != null)
					{
						return System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringMasterSearch"];
					}
					else
					{
						return ConnectionString;
					}
				}
			} 
		}

		/// <summary>
		/// Retrieves database connection string for Master Stats from Web.Config file.
		/// </summary>
        //[Obsolete("Use new Data Access Layer")]
		public static string ConnectionStringStats 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["dbEncrypt"] != null && System.Configuration.ConfigurationManager.AppSettings ["dbEncrypt"].ToUpper ().Equals ("TRUE"))
				{
					if (System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringStats"] != null)
					{
						return Decrypt (System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringStats"]);
					}
					else
					{
						return ConnectionString;
					}
				}
				else
				{
					if (System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringStats"] != null)
					{
						return System.Configuration.ConfigurationManager.AppSettings ["ConnectionStringStats"];
					}
					else
					{
						return ConnectionString;
					}
				}
			} 
		}

		/// <summary>
		/// Retrieves Kaneva database name
		/// </summary>
		public static string DbNameKaneva 
		{
			get 
			{
				return System.Configuration.ConfigurationManager.AppSettings ["DbNameKaneva"];
			} 
		}

		/// <summary>
		/// Retrieves Kaneva database name that is not in the cluster
		/// </summary>
		public static string DbNameKanevaNonCluster 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["DbNameKanevaNonCluster"] == null)
				{
					return "my_kaneva";
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["DbNameKanevaNonCluster"];
				}
			} 
		}

		/// <summary>
		/// Retrieves name of the World of Kanva game, different for dev, staging, prod
		/// </summary>
		public static string WokGameName 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["WokGameName"] == null)
				{
					return "World of Kaneva";
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["WokGameName"];
				}
			} 
		}

		/// <summary>
		/// Retrieves KGP (WOK) database name
		/// </summary>
		public static string DbNameKGP 
		{
			get 
			{
				return System.Configuration.ConfigurationManager.AppSettings ["DbNameKGP"];
			} 
		}

		/// <summary>
		/// Retrieves ImageServer string from Web.Config file.
		/// </summary>
		public static string ImageServer 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["ImageServer"] == null)
				{
					return "http://images.kaneva.com";
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["ImageServer"];
				}
			} 
		}

        /// <summary>
        /// Retrieves WOKImageServer string from Web.Config file.
        /// </summary>
        public static string WOKImageServer
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["WOKImageServer"] == null)
                {
                    return "http://images.kaneva.com";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["WOKImageServer"];
                }
            }
        }


		/// <summary>
		/// Retrieves SMTPServer string from Web.Config file.
		/// </summary>
		public static string SMTPServer 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["SMTPServer"] == null)
				{
					return "";
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["SMTPServer"];
				}
			} 
		}

		/// <summary>
		/// Retrieves From string from Web.Config file.
		/// </summary>
		public static string FromEmail
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["FromEmail"] == null)
				{
					return "kaneva@kaneva.com";
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["FromEmail"];
				}
			} 
		}

		/// <summary>
		/// Retrieves From string from Web.Config file.
		/// </summary>
		public static string WebSideStoryAccount
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["WebSideStoryAccount"] == null)
				{
					// Default to dev
					return "DM5701157NDM";
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["WebSideStoryAccount"].ToString ();
				}
			} 
		}

		/// <summary>
		/// Retrieves MinimumCreditCardTransactionAmount string from Web.Config file.
		/// </summary>
		public static double MinimumCreditCardTransactionAmount 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["MinimumCreditCardTransactionAmount"] == null)
				{
					return 5.00;
				}
				else
				{
					return Convert.ToDouble (System.Configuration.ConfigurationManager.AppSettings ["MinimumCreditCardTransactionAmount"]);
				}
			} 
		}

		/// <summary>
		/// Retrieves EnableCheckout string from Web.Config file.
		/// </summary>
		public static bool EnableCheckout 
		{
			get 
			{
				return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings ["EnableCheckout"]);
			} 
		}

        /// <summary>
        /// Retrieves EnablePaypal string from Web.Config file.
        /// </summary>
        public static bool EnablePaypal
        {
            get
            {
                try
                {
                    return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["EnablePaypal"]);
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
		/// Retrieves PayPalURL string from Web.Config file.
		/// </summary>
		public static string PayPalURL 
		{
			get 
			{
				return System.Configuration.ConfigurationManager.AppSettings ["PayPalURL"];
			} 
		}

		/// <summary>
		/// Retrieves PayPalBusiness string from Web.Config file.
		/// </summary>
		public static string PayPalBusiness 
		{
			get 
			{
				return System.Configuration.ConfigurationManager.AppSettings ["PayPalBusiness"];
			} 
		}

		/// <summary>
		/// Retrieves PayPalPDTToken string from Web.Config file.
		/// </summary>
		public static string PayPalPDTToken 
		{
			get 
			{
				return System.Configuration.ConfigurationManager.AppSettings ["PayPalPDTToken"];
			} 
		}

		/// <summary>
		/// Retrieves InCommURL string from Web.Config file.
		/// </summary>
		public static string InCommURL 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["InCommURL"] != null)
				{
					return System.Configuration.ConfigurationManager.AppSettings ["InCommURL"];
				}
				else
				{
					return "http://66.147.172.198:8080/transferedvalue/kaneva";
				}
			} 
		}

		/// <summary>
		/// Retrieves InCommAccountNumber string from Web.Config file.
		/// </summary>
		public static string InCommAccountNumber 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["InCommAccountNumber"] != null)
				{
					return System.Configuration.ConfigurationManager.AppSettings ["InCommAccountNumber"];
				}
				else
				{
					return "Kaneva";
				}
			} 
		}


		/// <summary>
		/// Retrieves SSLLogin string from Web.Config file.
		/// </summary>
		public static bool SSLLogin 
		{
			get 
			{
				return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings ["SSLLogin"]);
			} 
		}

		/// <summary>
		/// Retrieves LogFile string from Web.Config file.
		/// </summary>
		public static string LogFile 
		{
			get 
			{
				return System.Configuration.ConfigurationManager.AppSettings ["LogFile"];
			} 
		}

		/// <summary>
		/// Retrieves SiteName string from Web.Config file.
		/// </summary>
		public static string SiteName 
		{
			get 
			{
				return System.Configuration.ConfigurationManager.AppSettings ["SiteName"];
			} 
		}

        /// <summary>
        /// Retrieves Join Location string from Web.Config file.
        /// </summary>
        public static string JoinLocation
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["JoinLocation"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["JoinLocation"];
                }
                else
                {
                    return "~/register/kaneva/registerInfo.aspx";
                }


            }
        }


        /// <summary>
        /// Retrieves ContentServerPath string from Web.Config file.
        /// </summary>
        public static string ContentServerPath
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["ContentServerPath"];
            }
        }

        /// <summary>
        /// Retrieves ContentServerPath string from Web.Config file.
        /// </summary>
        public static string WOKImagesServerPath
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["WOKImagesPath"];
            }
        }

        /// <summary>
		/// Retrieves FileStoreHack string from Web.Config file.
		/// </summary>
		public static string FileStoreHack 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["FileStoreHack"] != null)
				{
					return System.Configuration.ConfigurationManager.AppSettings ["FileStoreHack"];
				}
				else
				{
					return "";
				}
			} 
		}

        /// <summary>
        /// Retrieves WOKFileStoreHack string from Web.Config file.
		/// </summary>
		public static string WOKFileStoreHack 
		{
			get 
			{
                if (System.Configuration.ConfigurationManager.AppSettings["WOKFileStoreHack"] != null)
				{
                    return System.Configuration.ConfigurationManager.AppSettings["WOKFileStoreHack"];
				}
				else
				{
					return "";
				}
			} 
		}

        
		/// <summary>
		/// Retrieves UploadRepository string from Web.Config file.
		/// </summary>
		public static string UploadRepository 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["uploadRepository"] == null)
				{
					throw new Exception ("UploadRepository is not found in config file");
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["uploadRepository"];
				}
			} 
		}

		/// <summary>
		/// Retrieves StreamingServer string from Web.Config file.
		/// </summary>
		public static string StreamingServer 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["StreamingServer"] == null)
				{
					return "http://KanevaStaging";
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["StreamingServer"];
				}
			} 
		}

		/// <summary>
		/// Retrieves StreamingFile string from Web.Config file.
		/// </summary>
		public static string StreamingFile 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["StreamingFile"] == null)
				{
					return "stream.php";
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["StreamingFile"];
				}
			} 
		}

		/// <summary>
		/// Retrieves MaxFileSizeToStream string from Web.Config file.
		/// </summary>
		public static Int64 MaxFileSizeToStream 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["MaxFileSizeToStream"] == null)
				{
					return 20972000;
				}
				else
				{
					return Convert.ToInt64 (System.Configuration.ConfigurationManager.AppSettings ["MaxFileSizeToStream"]);
				}
			} 
		}

		/// <summary>
		/// FormatBlogRating
		/// </summary>
		/// <param name="amount"></param>
		/// <returns></returns>
		public static string FormatBlogRating (Object rating)
		{
			if (rating.Equals (DBNull.Value))
			{
				return Constants.BLOG_NO_RATING;
			}
			else
			{
				try 
				{
					return Convert.ToDouble (rating).ToString ("#0.##") + "/10";
				}
				catch (Exception exc)
				{
					m_logger.Error ("FormatBlogRating Error", exc);
					return Constants.BLOG_NO_RATING;
				}
			}
		}

		/// <summary>
		/// Format the date time into a readable formate
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public static string FormatDateTime (DateTime dtDate)
		{
			System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo ("en-US", true);
			return dtDate.ToString ("MMM-d-yy hh:mm tt", culture);
		}


		/// <summary>
		/// Format the date into a readable formate
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public static string FormatDate (DateTime dtDate)
		{
			System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo ("en-US", true);
			return dtDate.ToString ("MMM-d-yy", culture);
		}

		/// <summary>
		/// Format the date into a readable formate
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public static string FormatDateNumbersOnly (DateTime dtDate)
		{
			System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo ("en-US", true);
			return dtDate.ToString ("MM-dd-yy", culture);
		}

		/// <summary>
		/// Format the date into a time span
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public static string FormatDateTimeSpan(Object dtDate)
		{
			return FormatDateTimeSpan(dtDate, DateTime.Now);	
		}
		public static string FormatDateTimeSpan(Object dtDate, Object dtDateNow)
		{
			string timespan = string.Empty;

			if (dtDate.Equals (DBNull.Value))
				return "";

			TimeSpan span = Convert.ToDateTime(dtDateNow) - Convert.ToDateTime(dtDate);
			
			if (span.Days > 29)
				timespan = (span.Days/30).ToString() + " month" + ((span.Days/30) > 1 ? "s" : "") + " ago";
			else if (span.Days > 0)
				timespan = span.Days.ToString() + " day" + (span.Days > 1 ? "s" : "") + " ago";
			else if (span.Hours > 0)
				timespan = span.Hours.ToString() + " hour" + (span.Hours > 1 ? "s" : "") + " ago";
			else if (span.Minutes > 0)
				timespan = span.Minutes.ToString() + " minute" + (span.Minutes > 1 ? "s" : "") + " ago";
			else if (span.Seconds > 0)
				timespan = span.Seconds.ToString() + " second" + (span.Seconds > 1 ? "s" : "") + " ago";
			else
				timespan = "1 second ago";
			
			return timespan;
		}
		/// <summary>
		/// Format the date into a readable formate
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public static DateTime UnFormatDate (string strDate)
		{
			try
			{
				System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo ("en-US", true);
				return DateTime.Parse (strDate, culture, System.Globalization.DateTimeStyles.None);
			}
			catch (Exception exc)
			{
				m_logger.Error ("Problem unformatting date", exc);
				return GetCurrentDateTime ();
			}
		}

		/// <summary>
		/// Format the date into a readable formate
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public static string FormatImageSize (long imageSize)
		{
			Double newImageSize;

			// Less than 1Kb show b
			if (imageSize < 1024)	// 1K = 1024 b
			{
				return imageSize.ToString ("#,##0.## b");
			}
			// Less than a MB show Kb
			else if (imageSize < 1048600)		// 1048600 b = 1MB = 1024 KB
			{
				newImageSize = (Double) imageSize / 1024;
				return newImageSize.ToString ("#,##0.## Kb");
			}
			// Show in MB
			else // if (imageSize < 1073700000) // 1073700000 b = 1 Gb, 1099500000000 b = 1 TB 
			{
				newImageSize = (Double) imageSize / 1048600;
				return newImageSize.ToString ("#,##0.## Mb");
			}
		}

		/// <summary>
		/// Format the date into a readable formate
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public static string FormatImageSize (double imageSize)
		{
			Double newImageSize;

			if (imageSize < 1000)
			{
				return imageSize.ToString ("#,##0.## b");
			}
			else if (imageSize < 1000000)
			{
				newImageSize = (Double) imageSize / 1000;
				return newImageSize.ToString ("#,##0.## Kb");
			}
			else
			{
				newImageSize = (Double) imageSize / 1000000;
				return newImageSize.ToString ("#,##0.## Mb");
			}
		}

		/// <summary>
		/// Format currency
		/// </summary>
		/// <param name="dblCurrency"></param>
		/// <returns></returns>
		public static string FormatCurrency (Object dblCurrency)
		{
			if (dblCurrency == null)
				return ("$0.00");

			return ((Double) dblCurrency).ToString ("$#,##0.00#");
		}

		/// <summary>
		/// Format currency
		/// </summary>
		/// <param name="dblCurrency"></param>
		/// <returns></returns>
		public static string FormatCurrencyForTextBox (Object dblCurrency)
		{
			if (dblCurrency == null)
				return ("0.00");

			// No partial pennies here.
			return ((Double) dblCurrency).ToString ("#0.00");
		}


		/// <summary>
		/// Format KPoints with other options
		/// </summary>
		/// <param name="dblKPoints"></param>
		/// <param name="bIsHTML"></param>
		/// <param name="bShowFreeText"></param>
		/// <returns></returns>
		public static string FormatKPoints (Double dblKPoints, bool bIsHTML, bool bShowFreeText)
		{
			string formatString = "#,##0 credits";
//			string currencySymbol;
//			
//			if (bIsHTML)
//			{
//				currencySymbol = Constants.CURR_KPOINT_SYMBOL_HTML;
//			}
//			else
//			{
//				currencySymbol = Constants.CURR_KPOINT_SYMBOL_NON_HTML;
//			}

			if (dblKPoints.Equals (null))
				return ("0");

			if (bShowFreeText && dblKPoints.Equals (0.0))
			{
				return "FREE - 0 kp";
			}

			if (dblKPoints < 0)
			{
				return (Math.Abs (dblKPoints)).ToString ("(" + formatString + ")");
			}
			else
			{
				return (dblKPoints).ToString (formatString);
			}
		}

		/// <summary>
		/// Format KPoints
		/// </summary>
		/// <param name="dblKPoints"></param>
		/// <returns></returns>
		public static string FormatKPoints (Double dblKPoints)
		{
			return FormatKPoints (dblKPoints, true, true);
		}

		/// <summary>
		/// Is the number numeric?
		/// </summary>
		/// <param name="candidate"></param>
		/// <returns></returns>
		public static bool IsNumeric (string candidate) 
		{ 

			char [] ca = candidate.ToCharArray(); 
			for (int i = 0; i < ca.Length;i++) 
			{ 
				if (ca[i] > 57 || ca[i] < 48) 
					return false; 
			} 
			return true; 
		} 

		/// <summary>
		/// Format seconds as time hh:mm:ss
		/// </summary>
		/// <param name="runTime"></param>
		/// <returns></returns>
		public static string FormatSecondsAsTime(int runTime)
		{
			return String.Format( "{0:D}:{1:D2}:{2:D2}", runTime / 3600, runTime / 60 % 60, runTime % 60 );
		}
		/// <summary>
		/// Get the results text
		/// </summary>
		/// <returns></returns>
		public static string GetResultsText (int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
		{
			return GetResultsText (TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
		}

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public static string GetResultsText(UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }


		/// <summary>
		/// Get the results text
		/// </summary>
		/// <returns></returns>
		public static string GetResultsText (int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
		{
			
			return GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts,
			"Results %START%-%END% of %TOTAL%");
		}

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public static string GetResultsText(UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
        {

            return GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts,
            "Results %START%-%END% of %TOTAL%");
        }

		/// <summary>
		/// Get the results text
		/// </summary>
		/// <returns></returns>
		/// <param name="template">%START%, %END%, %TOTAL% will be replaced by starting number, ending number and total
		/// number of items showing</param>
		/// <returns></returns>
		public static string GetResultsText (UInt32 TotalCount, int pageNumber, int resultsPerPage, 
			int currentPageCount, bool bGetCounts, string template)
		{
			if (TotalCount > 0)
			{
				if (bGetCounts)
				{
					int start = ((pageNumber - 1) * resultsPerPage)+1;
					int end = (pageNumber - 1) * resultsPerPage+currentPageCount;
					return template.Replace("%START%", start.ToString()).Replace(
						"%END%", end.ToString()).Replace("%TOTAL%", TotalCount.ToString());
				}
				else
				{
					return "";
				}
			}
			else
			{
				return "<B>No Results Found</B>";
			}
		}

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        /// <param name="template">%START%, %END%, %TOTAL% will be replaced by starting number, ending number and total
        /// number of items showing</param>
        /// <returns></returns>
        public static string GetResultsText(int TotalCount, int pageNumber, int resultsPerPage,
            int currentPageCount, bool bGetCounts, string template)
        {
            if (TotalCount > 0)
            {
                if (bGetCounts)
                {
                    int start = ((pageNumber - 1) * resultsPerPage) + 1;
                    int end = (pageNumber - 1) * resultsPerPage + currentPageCount;
                    return template.Replace("%START%", start.ToString()).Replace(
                        "%END%", end.ToString()).Replace("%TOTAL%", TotalCount.ToString());
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "<B>No Results Found</B>";
            }
        }

		/// <summary>
		/// Encrypt
		/// </summary>
		/// <param name="plainText"></param>
		/// <returns></returns>
		public static string Encrypt (string plainText)
		{
			TripleDESCryptoServiceProvider tds = new TripleDESCryptoServiceProvider ();
			byte[] plainByte = ASCIIEncoding.ASCII.GetBytes (plainText);

			string key = "jb7431%o$#=@tp+&";
			PasswordDeriveBytes pdbkey = new PasswordDeriveBytes (key, ASCIIEncoding.ASCII.GetBytes (String.Empty));
			byte[] keyByte = pdbkey.GetBytes (key.Length);

			// Set private key
			tds.Key = keyByte;
			tds.IV = new byte[] {0xf, 0x6f, 0x13, 0x2e, 0x35, 0xc2, 0xcd, 0xf9};
				
			// Encryptor object
			ICryptoTransform cryptoTransform = tds.CreateEncryptor ();
   
			// Memory stream object
			MemoryStream ms = new MemoryStream ();

			// Crpto stream object
			CryptoStream cs = new CryptoStream (ms, cryptoTransform, CryptoStreamMode.Write);

			// Write encrypted byte to memory stream
			cs.Write (plainByte, 0, plainByte.Length);
			cs.FlushFinalBlock ();

			// Get the encrypted byte length
			byte[] cryptoByte = ms.ToArray ();

			ms.Close ();
			cs.Close ();

			// Convert into base 64 to enable result to be used in Config
			return Convert.ToBase64String (cryptoByte, 0, cryptoByte.GetLength(0));
		}

		/// <summary>
		/// Decrypt
		/// </summary>
		/// <param name="encryptedText"></param>
		/// <returns></returns>
		public static string Decrypt (string encryptedText)
		{
			TripleDESCryptoServiceProvider tds = new TripleDESCryptoServiceProvider ();
			string result = "";

			string key = "jb7431%o$#=@tp+&";
			PasswordDeriveBytes pdbkey = new PasswordDeriveBytes (key, ASCIIEncoding.ASCII.GetBytes (String.Empty));
			byte[] keyByte = pdbkey.GetBytes (key.Length);

			// Decrypt it
			// Convert from base 64 string to bytes
			byte[] cryptoByte = Convert.FromBase64String (encryptedText);

			// Set private key
			tds.Key = keyByte;
			tds.IV = new byte[] {0xf, 0x6f, 0x13, 0x2e, 0x35, 0xc2, 0xcd, 0xf9};			

			// Decryptor object
			ICryptoTransform cryptoTransform = tds.CreateDecryptor ();
			MemoryStream msd = null;
			CryptoStream csd = null;
			StreamReader sr = null;

			try
			{
				// Memory stream object
				msd = new MemoryStream (cryptoByte, 0, cryptoByte.Length);

				// Crypto stream object
				csd = new CryptoStream (msd, cryptoTransform, CryptoStreamMode.Read);

				// Get the result from the Crypto stream
				sr = new StreamReader (csd);
				result = sr.ReadToEnd ();
			}
			catch (Exception exc)
			{
				m_logger.Error ("Decryption Error", exc);
			}
			finally
			{
				cryptoTransform.Dispose ();

				if (msd != null)
				{
					msd.Close ();
				}
				if (csd != null)
				{
					csd.Close ();
				}
				if (sr != null)
				{
					sr.Close ();
				}
			}

			return result;
		}

		/// <summary>
		/// Generate a token
		/// </summary>
		/// <returns></returns>
		public static string GenerateUniqueString (int maxLength)
		{
			try
			{
				// Generate 4 random bytes for the seed
				byte [] randomBytes = new byte[4];
				new RNGCryptoServiceProvider().GetBytes (randomBytes);

				// Convert 4 bytes into a 32-bit integer value.
				int seed = (randomBytes[0] & 0x7f) << 24 |
					randomBytes[1]         << 16 |
					randomBytes[2]         <<  8 |
					randomBytes[3];

				// Now, this is real randomization.
				Random random  = new Random (seed);

				char [] token = new char [maxLength];

				for (int i = 0; i < token.Length; i++)
				{
					token [i] = TOKEN_CHARACTERS [random.Next (0, TOKEN_CHARACTERS.Length - 1)];
				}

				return (new string (token)).Substring (0, maxLength);
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error generating token", exc);
			}

			return ("Kaneva");
		}

		/// <summary>
		/// Return the personal channel link
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static string GetPersonalChannelUrl (string nameNoSpaces)
		{
			return "http://" + KanevaGlobals.SiteName + "/channel/" + nameNoSpaces + ".people";
		}

		/// <summary>
		/// Return the broadcast channel link
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static string GetBroadcastChannelUrl (string nameNoSpaces)
		{
			return "http://" + KanevaGlobals.SiteName + "/channel/" + nameNoSpaces + ".channel";
		}

		/// <summary>
		/// Return the blog link
		/// </summary>
		/// <param name="blogId"></param>
		/// <returns></returns>
		public static string GeBlogUrl (int blogId)
		{
			return "http://" + SiteName + "/blog/" + blogId.ToString () + ".blog";
		}

		/// <summary>
		/// Get Asset Details Link
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public static string GetAssetDetailsLink (int assetId)
		{
			return "http://" + SiteName + "/asset/" + assetId.ToString () + ".media";
		}

		/// <summary>
		/// replace separators(commas, spaces, semicolons) with spaces
		/// </summary>
		/// <param name="tags"></param>
		/// <returns></returns>
		public static string GetNormalizedTags(string tags)
		{
			return tags.Replace(","," ").Replace(";"," ").Replace("  "," ");
		}

		/// <summary>
		/// Get the version
		/// </summary>
		/// <returns></returns>
		public static string Version ()
		{
			if (version == null)
			{
				// Get the current version
				System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly ();
				System.Reflection.AssemblyName an = a.GetName();
				version = "version " + an.Version.Major + "." + an.Version.Minor + "." + an.Version.Build + "." + an.Version.Revision;
			}

			return version;
		}

		/// <summary>
		/// Get the database utility
		/// </summary>
		/// <returns></returns>
		public static DatabaseUtility GetDatabaseUtilityKGP()
		{
			if (m_DatabaseUtiltyKGP == null)
			{
				switch (KanevaGlobals.DatabaseType)
				{
					case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
					{
						m_DatabaseUtiltyKGP = new SQLServerUtility (KanevaGlobals.ConnectionStringKGP);
						break;
					}
					case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
					{
						m_DatabaseUtiltyKGP = new MySQLUtility (KanevaGlobals.ConnectionStringKGP);
						break;
					}
					default:
					{
						throw new Exception ("This database type is not supported");
					}
				}
			}
			return m_DatabaseUtiltyKGP;
		}

		/// <summary>
		/// Get the database utility
		/// </summary>
		/// <returns></returns>
		public static DatabaseUtility GetDatabaseUtility ()
		{
			if (m_DatabaseUtilty == null)
			{
				switch (KanevaGlobals.DatabaseType)
				{
					case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
					{
						m_DatabaseUtilty = new SQLServerUtility (KanevaGlobals.ConnectionString);
						break;
					}
					case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
					{
						m_DatabaseUtilty = new MySQLUtility (KanevaGlobals.ConnectionString);
						break;
					}
					default:
					{
						throw new Exception ("This database type is not supported");
					}
				}
			}
			return m_DatabaseUtilty;
		}

		/// <summary>
		/// Get the read only database utility read only
		/// </summary>
		/// <returns></returns>
		public static DatabaseUtility GetDatabaseUtilityReadOnly ()
		{
			if (m_DatabaseUtilityReadOnly == null)
			{
				switch (KanevaGlobals.DatabaseType)
				{
					case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
					{
						m_DatabaseUtilityReadOnly = new SQLServerUtility (KanevaGlobals.ConnectionStringReadOnly);
						break;
					}
					case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
					{
						m_DatabaseUtilityReadOnly = new MySQLUtility (KanevaGlobals.ConnectionStringReadOnly);
						break;
					}
					default:
					{
						throw new Exception ("This database type is not supported");
					}
				}
			}
			return m_DatabaseUtilityReadOnly;
		}

		/// <summary>
		/// Get the search database utility
		/// </summary>
		/// <returns></returns>
		public static DatabaseUtility GetDatabaseUtilitySearch ()
		{
			if (m_DatabaseUtilitySearch == null)
			{
				switch (KanevaGlobals.DatabaseType)
				{
					case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
					{
						m_DatabaseUtilitySearch = new SQLServerUtility (KanevaGlobals.ConnectionStringSearch);
						break;
					}
					case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
					{
						m_DatabaseUtilitySearch = new MySQLUtility (KanevaGlobals.ConnectionStringSearch);
						break;
					}
					default:
					{
						throw new Exception ("This database type is not supported");
					}
				}
			}
			return m_DatabaseUtilitySearch;
		}

		/// <summary>
		/// Get the search master database utility
		/// </summary>
		/// <returns></returns>
		public static DatabaseUtility GetDatabaseUtilityMasterSearch ()
		{
			if (m_DatabaseUtilityMasterSearch == null)
			{
				switch (KanevaGlobals.DatabaseType)
				{
					case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
					{
						m_DatabaseUtilityMasterSearch = new SQLServerUtility (KanevaGlobals.ConnectionStringMasterSearch);
						break;
					}
					case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
					{
						m_DatabaseUtilityMasterSearch = new MySQLUtility (KanevaGlobals.ConnectionStringMasterSearch);
						break;
					}
					default:
					{
						throw new Exception ("This database type is not supported");
					}
				}
			}
			return m_DatabaseUtilityMasterSearch;
		}

		/// <summary>
		/// Get the stats database utility
		/// </summary>
		/// <returns></returns>
		public static DatabaseUtility GetDatabaseUtilityStats ()
		{
			if (m_DatabaseUtilityStats == null)
			{
				switch (KanevaGlobals.DatabaseType)
				{
					case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
					{
						m_DatabaseUtilityStats = new SQLServerUtility (KanevaGlobals.ConnectionStringStats);
						break;
					}
					case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
					{
						m_DatabaseUtilityStats = new MySQLUtility (KanevaGlobals.ConnectionStringStats);
						break;
					}
					default:
					{
						throw new Exception ("This database type is not supported");
					}
				}
			}
			return m_DatabaseUtilityStats;
		}

		/// <summary>
		/// Version number
		/// </summary>
		private static string version = null;

		/// <summary>
		/// DataBase Utility Read only for searching
		/// </summary>
		private static DatabaseUtility m_DatabaseUtilityReadOnly;

		/// <summary>
		/// DataBase Utility for Master Database
		/// </summary>
		private static DatabaseUtility m_DatabaseUtilty;

		/// <summary>
		/// DataBase Utility for KGP Database
		/// </summary>
		private static DatabaseUtility m_DatabaseUtiltyKGP;

		/// <summary>
		/// DataBase Utility for Search
		/// </summary>
		private static DatabaseUtility m_DatabaseUtilitySearch;

		/// <summary>
		/// DataBase Utility for Master Search
		/// </summary>
		private static DatabaseUtility m_DatabaseUtilityMasterSearch;

		/// <summary>
		/// DataBase Utility for Stats
		/// </summary>
		private static DatabaseUtility m_DatabaseUtilityStats;

		// Possible token strings
		private static char [] TOKEN_CHARACTERS = "abcdefgijkmnopqrstwxyzABCDEFGHJKLMNPQRSTWXYZ23456789".ToCharArray ();

		/// <summary>
		/// Logger
		/// </summary>
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

	}
}
