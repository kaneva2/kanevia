///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using KlausEnt.KEP.Kaneva;

namespace KlausEnt.KEP.KepPublishing
{
	/// <summary>
	/// Summary description for TorrentProcessorFactory.
	/// </summary>
	public class TorrentProcessorFactory
	{
		public TorrentProcessorFactory()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static ITorrentProcessor GetProcessor(Torrent torrent)
		{
			ITorrentProcessor retVal = null;
			switch(torrent.TorrentStatusId)
			{
				case (int) Constants.eTORRENT_STATUS.UPLOADED:
					retVal = new PostTorrentUploadedProcessor();
					break;
				case (int) Constants.eTORRENT_STATUS.TORRENT_REPLACED:
					retVal = new PostTorrentReplacedProcessor();
					break;
				default:
					retVal = null;
					break;
			}

			return retVal;
		}
	}
}
