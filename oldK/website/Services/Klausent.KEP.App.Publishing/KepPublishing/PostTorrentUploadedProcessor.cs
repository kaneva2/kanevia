///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Threading;
using Com.Kei.BitTorrentAdaptor;
using KlausEnt.KEP.Kaneva;
using KlausEnt.KEP.KEPDS;
using log4net;

namespace KlausEnt.KEP.KepPublishing
{
	/// <summary>
	/// Summary description for PostUploadProcessor.
	/// </summary>
	public class PostTorrentUploadedProcessor : ITorrentProcessor
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(PostTorrentUploadedProcessor));
		public PostTorrentUploadedProcessor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public void Process(Torrent torrent)
		{
			PostUploadProcessorWorker worker = new PostUploadProcessorWorker() ;
			worker.Torrent = torrent;
			ThreadPool.QueueUserWorkItem(new WaitCallback(worker.Run), this);
		}
	}

	class PostUploadProcessorWorker
	{
		private static readonly SessionAdaptor sessionAdaptor = SessionAdaptor.GetInstance();
		private static readonly TorrentInfoAdaptor torrentInfoAdaptor = TorrentInfoAdaptor.GetInstance();
		private static readonly TorrentProcessRegistry torrentProcessRegistry = TorrentProcessRegistry.GetInstance();

		private static readonly Settings settings = Settings.GetInstance();
		private static readonly ILog log = LogManager.GetLogger(typeof(PostUploadProcessorWorker));

		protected AzureusUtility AZUREUS;
		protected ABCUtility ABC;
		protected BNBTUtility BNBT;

		private	Torrent _torrent;

		public PostUploadProcessorWorker()
		{
			AZUREUS = new AzureusUtility();
			BNBT = new BNBTUtility();
			ABC = new ABCUtility();
		}

		public void Run(object state)
		{
			ITorrentProcessor processor = (ITorrentProcessor)state;
			if(torrentProcessRegistry.Register(_torrent, processor))
			{
				try
				{
					MakeNewTorrent();
				}
				catch(Exception e)
				{
					log.Error("error making new torrent", e);
				}
				finally
				{
					torrentProcessRegistry.UnRegister(_torrent, processor);
				}
			}
		}

		private int MakeNewTorrent()
		{
			int retVal = (int) Constants.eDS_RETURNCODES.EXCEPTION_PROCESSING_UPLOADED_CONTENT;
			if(log.IsInfoEnabled)
			{
				log.Info("Post-upload process: Making new torrent, infohash = " + _torrent.Infohash);
			}
			try
			{
				NewTorrent newTorrent = new NewTorrent();
				//newTorrent.UniqueId = Guid.NewGuid().ToString();
				newTorrent.Comment = "";
				string contentPath = Path.Combine(_torrent.TargetDir,_torrent.FileName);
				//libtorrent doesn't like \, use / instead
				newTorrent.ContentPath = contentPath.Replace(Path.DirectorySeparatorChar.ToString(),"/");
				newTorrent.Creator = settings.TorrentCreator;
				//TODO we may need to change the tracker for load distribution
				newTorrent.Tracker = settings.PublishAnnounceUrl;

				//append a tag in front of the original torrent file name
				string fileNameWithoutExt = Settings.GetInstance().ReplaceTorrentPrefix + 
					_torrent.TorrentName;
				string filePath = Path.Combine(Path.GetTempPath(), fileNameWithoutExt);

				if(log.IsInfoEnabled)
				{
					log.Info("Post-upload process: Making new torrent, name = " + fileNameWithoutExt);
				}

				newTorrent.TorrentPath = filePath.Replace(Path.DirectorySeparatorChar.ToString(), "/");
				sessionAdaptor.MakeTorrent(newTorrent);
				FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
				retVal = PostTorrent(fileNameWithoutExt, _torrent.UserId, fileStream, true, _torrent.TargetDir);
				fileStream.Close();

				TorrentInfoView torrentInfoView = torrentInfoAdaptor.GetInfo(filePath);

				if(log.IsDebugEnabled)
				{
					log.Debug("Post-upload process: torrent recreated, infohash = " + torrentInfoView.InfoHash);
				}

				if ( retVal == (int) Constants.eDS_RETURNCODES.SUCCESS)
				{
					// Update status, on failure just orphan the tracker torrent
					TorrentDao.GetInstance().UpdateTorrent(_torrent.TorrentId, 
						fileNameWithoutExt,
						torrentInfoView.InfoHash, 
						(int) KlausEnt.KEP.Kaneva.Constants.eTORRENT_STATUS.TORRENT_RECREATED);
					if(log.IsInfoEnabled)
					{
						log.Info("new torrent is created, torrent status updated to TORRENT_RECREATED, infohash = " + torrentInfoView.InfoHash);
					}
				}

				File.Delete(filePath);
			}
			catch(Exception e)
			{
				retVal = (int) Constants.eDS_RETURNCODES.EXCEPTION_PROCESSING_UPLOADED_CONTENT;
				log.Error("exception processing uploaded content", e);
			}

			return retVal;
		}

		private int PostTorrent(string fileName, int user_id, Stream fileStream, bool isSecure, string targetDir)
		{
			int ret = (int) Constants.eDS_RETURNCODES.SUCCESS;
			if ( BNBT.EnabledTrackerPost())
			{
				ret = BNBT.PostToTracker( fileName, user_id, fileStream);
			}

			if ( AZUREUS.EnabledTrackerPost())
			{
				//ret = AZUREUS.PostToAzureusTracker( fileName, user_id, RequestSoapContext.Current.Attachments[0].Stream);
				ret = AZUREUS.PostToAzureusTracker( fileName, user_id, 
					! ( (AZUREUS.EnabledContentPost()) && (isSecure || AZUREUS.EnabledFreeContentPost())), fileStream);
			}

			if ( ret == (int) Constants.eDS_RETURNCODES.SUCCESS)
			{
				string fileNameWithExt = fileName + Settings.GetInstance().ReplaceTorrentExt;

				if ( ABC.EnabledContentPost())
				{
					ret = ABC.PostToABC( fileNameWithExt);
				}

				if ( AZUREUS.EnabledContentPost() && (isSecure || AZUREUS.EnabledFreeContentPost()) )
				{
					ret = AZUREUS.PostToSpecifiedAzureus( fileNameWithExt, fileStream, GetAzureusServerId(targetDir));
				}
			}

			return ret;
		}

		protected int GetAzureusServerId(String targetDir)
		{
			string seperator = "\\";
			int begin = targetDir.IndexOf(AzureusUtility.AZUREUS_HOME_CLIENT) + AzureusUtility.AZUREUS_HOME_CLIENT.Length;
			int end = targetDir.IndexOf(seperator, begin);
			string idStr = targetDir.Substring(begin, end-begin);
			try
			{
				return int.Parse(idStr);
			}catch(Exception e)
			{
				throw new FormatException("Failed to parse azureus server id, id string = " + idStr, e);
			}
		}

		public Torrent Torrent
		{
			get { return _torrent; }
			set { _torrent = value; }
		}
	}
}
