///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Threading;
using Com.Kei.BitTorrentAdaptor;
using KlausEnt.KEP.Kaneva;
using KlausEnt.KEP.KEPDS;
using log4net;

namespace KlausEnt.KEP.KepPublishing
{
	/// <summary>
	/// Summary description for PostTorrentReplacedProcessor.
	/// </summary>
	public class PostTorrentReplacedProcessor : ITorrentProcessor
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(PostTorrentReplacedProcessor));
		public PostTorrentReplacedProcessor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public void Process(Torrent torrent)
		{
			PostTorrentReplacedProcessorWorker worker = new PostTorrentReplacedProcessorWorker() ;
			worker.Torrent = torrent;
			ThreadPool.QueueUserWorkItem(new WaitCallback(worker.Run), this);
		}
	}

	class PostTorrentReplacedProcessorWorker
	{
		private static readonly TorrentProcessRegistry torrentProcessRegistry = TorrentProcessRegistry.GetInstance();

		private static readonly Settings settings = Settings.GetInstance();
		private static readonly ILog log = LogManager.GetLogger(typeof(PostUploadProcessorWorker));

		private	Torrent _torrent;

		public PostTorrentReplacedProcessorWorker()
		{
		}

		public void Run(object state)
		{
			ITorrentProcessor processor = (ITorrentProcessor)state;
			if(torrentProcessRegistry.Register(_torrent, processor))
			{
				try
				{
					UpdateTorrentStatus();
				}
				catch(Exception e)
				{
					log.Error("error making new torrent", e);
				}
				finally
				{
					torrentProcessRegistry.UnRegister(_torrent, processor);
				}
			}
		}

		/// <summary>
		/// last step is completed so update torrent_status to PUBLISH_COMPLETE
		/// </summary>
		private void UpdateTorrentStatus()
		{
			TorrentDao.GetInstance().UpdateTorrent(_torrent.TorrentId, 
				_torrent.TorrentName,
				_torrent.Infohash, 
				(int) KlausEnt.KEP.Kaneva.Constants.eTORRENT_STATUS.PUBLISH_COMPLETE);
			if(log.IsInfoEnabled)
			{
				log.Info("torrent is replaced, status set to PUBLISH_COMPLETE, infohash = " 
					+ _torrent.Infohash);
			}
		}

		public Torrent Torrent
		{
			get { return _torrent; }
			set { _torrent = value; }
		}
	}
}
