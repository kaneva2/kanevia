///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Timers;
using log4net;

namespace KlausEnt.KEP.KepPublishing
{
	public class KepPublishing : System.ServiceProcess.ServiceBase
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(KepPublishing));

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Timers
		/// </summary>
		System.Timers.Timer _contentProcessTimer = new Timer();
		TorrentDao torrentDao = TorrentDao.GetInstance();

		static KepPublishing()
		{
			string s = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
			FileInfo l_fi = new FileInfo(s);
			log4net.Config.DOMConfigurator.Configure(l_fi);
			if(log.IsInfoEnabled)
			{
				log.Info("Setup log4net");
			}
		}

		public KepPublishing()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitComponent call
			int seconds = Convert.ToInt32 (System.Configuration.ConfigurationSettings.AppSettings ["torrentStatusUpdaterInterval"]);
			if(log.IsInfoEnabled)
			{
				log.Info("Service is scheduled to run every " + seconds + " seconds");
			}

			_contentProcessTimer.Enabled = true;
			_contentProcessTimer.Interval = (1000 * seconds);

			_contentProcessTimer.Elapsed += new System.Timers.ElapsedEventHandler (ContentProcessTimer_Elapsed);
		}

		// The main entry point for the process
		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
	
			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = new System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
			//
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new KepPublishing() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = "Service1";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		protected override void OnStart(string[] args)
		{
			// TODO: Add code here to start your service.
			_contentProcessTimer.Start();
			if(log.IsInfoEnabled)
			{
				log.Info("SERVICE STARTED");
			}
		}
 
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
			// TODO: Add code here to perform any tear-down necessary to stop your service.
			_contentProcessTimer.Stop();
			log.Fatal("SERVICE STOPPED");
		}

		/// <summary>
		/// TorrentUpdateTimer_Elapsed
		/// 
		/// Update torrent seeds
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void ContentProcessTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) 
		{
			try
			{
				_contentProcessTimer.Stop ();
				ProcessTorrents();
			}
			catch (Exception exc)
			{
				log.Error ("Exception processing content", exc);
			}
			finally
			{
				_contentProcessTimer.Start ();
			}
		}

		protected void ProcessTorrents()
		{
			if(log.IsDebugEnabled)
			{
				log.Debug("Checking torrent status");
			}
			IList torrents = torrentDao.GetAll();
			foreach(Torrent torrent in torrents)
			{
				ITorrentProcessor processor = TorrentProcessorFactory.GetProcessor(torrent);
				if(processor != null)
				{
					processor.Process(torrent);
				}else
				{
					log.Error("No processor found for torrent " + torrent.Infohash + " torrent status id = " + torrent.TorrentStatusId);
				}
			}
			if(log.IsDebugEnabled)
			{
				log.Debug("Done checking status, number of torrents need to process = " + torrents.Count);
			}
		}
	}
}
