///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Timers;
using KlausEnt.KEP.GameUploadPublishing;
using KlausEnt.KEP.Kaneva;
using System.Web.Mail;
using log4net;

namespace KlausEnt.KEP.AssetPublishing
{
	// small utility class to pass the result of the asyncronous conversions
	// back to this class.

	public class SignalResult
	{
		public int result = 0;
	}

	public class AssetPublishing : System.ServiceProcess.ServiceBase
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(AssetPublishing));
		private static readonly Settings settings = Settings.GetInstance();

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Timers
		/// </summary>
		System.Timers.Timer _contentProcessTimer = new Timer();
		AssetDao assetDao = AssetDao.GetInstance();
		GameUploadDao gameUploadDao = GameUploadDao.GetInstance();

		static AssetPublishing()
		{
			string s = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
			FileInfo l_fi = new FileInfo(s);
			log4net.Config.DOMConfigurator.Configure(l_fi);
			if(log.IsInfoEnabled)
			{
				log.Info("Setup log4net");
			}
		}

		public AssetPublishing()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitComponent call
			int seconds = Convert.ToInt32 (System.Configuration.ConfigurationSettings.AppSettings ["publishingProcessorInterval"]);
			if(log.IsInfoEnabled)
			{
				log.Info("Service is scheduled to run every " + seconds + " seconds");
			}

			_contentProcessTimer.Enabled = true;
			_contentProcessTimer.Interval = (1000 * seconds);

			_contentProcessTimer.Elapsed += new System.Timers.ElapsedEventHandler (ContentProcessTimer_Elapsed);
		}

		// The main entry point for the process
		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;

			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = new System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
			//
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new AssetPublishing() };

			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = "Service1";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		protected override void OnStart(string[] args)
		{
			// TODO: Add code here to start your service.
			_contentProcessTimer.Start();
			if(log.IsInfoEnabled)
			{
				log.Info("SERVICE STARTED");
			}
		}

		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
			// TODO: Add code here to perform any tear-down necessary to stop your service.
			_contentProcessTimer.Stop();
			log.Fatal("SERVICE STOPPED");
		}

		/// <summary>
		/// ContentProcessTimer_Elapsed
		///
		/// Update assets
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void ContentProcessTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			try
			{
				_contentProcessTimer.Stop ();
				ProcessGames();
				ProcessAssets();
			}
			catch (Exception exc)
			{
				log.Error ("Exception processing content", exc);
			}
			finally
			{
				_contentProcessTimer.Start ();
			}
		}

		protected void ProcessAssets()
		{
			if(log.IsDebugEnabled)
			{
				log.Debug("Checking asset status");
			}

			IList assetsAll = assetDao.GetAll();
			IList assetsToConvert = new ArrayList();
			IList assetsNoConvert = new ArrayList();

			log.Info("Number of assets found to process:" + assetsAll.Count);

			foreach(Asset asset in assetsAll)
			{
				string path = null;

				try
				{
					path = asset.GetFilePath();
					FileInfo fi = new FileInfo(path);
					string flvExtensions = settings.FlvConversionExtensions;

					bool performFLVConversion = false;

					foreach (string ss in flvExtensions.Split(','))
					{
						if ( fi.Extension != null && (string.Compare(ss, fi.Extension, true) == 0) )
						{
							// If the asset has an extension in our list of extensions
							// to convert to FLV format.

							performFLVConversion = true;
							break;
						}
					}

					if (performFLVConversion)
					{
						assetsToConvert.Add(asset);
					}
					else
					{
						assetsNoConvert.Add(asset);
					}
				}
				catch(Exception e)
				{
					log.Error("Exception caught processing asset " + asset.AssetId, e);
					log.Error("	Path is:" + path);
					StoreUtility.UpdateAssetPublishStatus(asset.AssetId, (int) Constants.ePUBLISH_STATUS.ERROR, 0);
				}
			}

			// Act on all assets not requiring conversion first.

			foreach(Asset asset in assetsNoConvert)
			{
				IAssetProcessor processor = AssetProcessorFactory.GetProcessor(asset);
				if(processor != null)
				{
					processor.Process(asset);
				}
				else
				{
					log.Error("No processor found for asset " + asset.AssetId + " asset publish status id = " + asset.PublishStatusId);
				}
			}

			SignalResult sigResult = new SignalResult();

			int num_converted = 0;

			foreach(Asset asset in assetsToConvert)
			{
				IAssetProcessor processor = AssetProcessorFactory.GetProcessor(asset);
				if(processor != null)
				{
					processor.ProcessAsync(asset, ref sigResult);

					if ( sigResult.result == 1)
					{
						num_converted++;
					}
				}
				else
				{
					log.Error("No processor found for asset " + asset.AssetId + " asset publish status id = " + asset.PublishStatusId);
				}
			}

			if (assetsToConvert.Count > 0)
			{
				StoreUtility.UpdatePublishingStats(assetsToConvert.Count, num_converted);
				SendPublishConversionBehindAlertEmail((assetsToConvert.Count - num_converted));
			}

			if(log.IsDebugEnabled)
			{
				log.Debug("Done checking status, number of assets need to process = " + assetsAll.Count);

				log.Debug("Number of assets requiring conversion = " + assetsToConvert.Count);
				log.Debug("Number of assets outstanding for conversion = " + (assetsToConvert.Count - num_converted));
			}
		}

		private void SendPublishConversionBehindAlertEmail(int numOutStanding)
		{
			if ( numOutStanding > settings.PublishConversionBehindSpread)
			{
				string itBody = "A Publishing Processor run identified " + numOutStanding + " outstanding items to convert. This is greater than the e-mail triggering max of " + settings.PublishConversionBehindSpread + ". Additional converter boxes may need to be brought online if this problem persists.";

                //try
                //{
                //    MailUtility.SendEmail(settings.PublishConversionBehindEmailFrom,
                //        settings.PublishConversionBehindEmailTo,
                //        "Publishing Processor falling behind on conversions",
                //        itBody,
                //        MailFormat.Text);
                //}
                //catch(Exception e)
                //{
                //    log.Error("Error alerting via e-mail of publishing processor falling behind", e);
                //}
			}
		}

		protected void ProcessGames()
		{
			if(log.IsDebugEnabled)
			{
				log.Debug("Checking game upload status");
			}
			IList gameUploads = gameUploadDao.GetAll();
			foreach(GameUpload gameUpload in gameUploads)
			{
				IGameUploadProcessor processor =
					GameUploadProcessorFactory.GetProcessor(gameUpload);
				if(processor != null)
				{
					processor.Process(gameUpload);
				}
				else
				{
					log.Error("No processor found for game upload " + gameUpload.GameUploadId + " game upload publish status id = " + gameUpload.PublishStatusId);
				}
			}
			if(log.IsDebugEnabled)
			{
				log.Debug("Done checking status, number of games need to process = " + gameUploads.Count);
			}

		}
	}
}
