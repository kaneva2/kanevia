///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Timers;
using System.Runtime.InteropServices;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace KlausEnt.KEP.AssetPublishing
{
	using System;
	using System.Timers;

	public class FlixEngine
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(PostAssetUploadedProcessor));
		private static readonly Settings settings = Settings.GetInstance();

		private WF_FlixCOM.WF_FlixEncoderClass flix;

		private String InputFile;
		private String OutputFile;

		private int inputFileErrorCode;
		private int _assetId;
		private int	_percent_complete;
		private bool _first_pass_encoding_complete;

		[DllImport("kernel32.dll", SetLastError=true)] 
		static extern void Sleep ( int dwMilliseconds);

		public FlixEngine(int assetId)
		{
			_assetId = assetId;
		}

		public bool ConvertMedia( string assetPath, ref long outFileLength, ref string thumbNailPath, ref ulong videoDuration)
		{
			bool ret = true;
			_percent_complete = 0;
			_first_pass_encoding_complete = false;

			try
			{
				flix = new WF_FlixCOM.WF_FlixEncoderClass();

				if ( settings.FlixLogEnabled == 1)
				{
					flix.SetUseLogging(1);
					flix.SetLogPath(settings.FlixLogFile);
				}

				flix.LoadSettings(settings.FlvConfigFile);

				InputFile = assetPath;

				OutputFile = assetPath;

				int extensionIndex = OutputFile.LastIndexOf(".");

				OutputFile = OutputFile.Substring(0, extensionIndex);
				thumbNailPath = OutputFile;

				OutputFile += ".flv";
				thumbNailPath += ".jpg";

				WF_FlixCOM.WF_Video videoOptions = flix.GetVideoOptions();

				switch (settings.FlvCompressMode)
				{
					case 0:
						videoOptions.SetCompressMode(WF_FlixCOM.WF_CompressMode.COMPRESSMODE_GOOD);
						break;
					case 1:
						videoOptions.SetCompressMode(WF_FlixCOM.WF_CompressMode.COMPRESSMODE_BEST);
						break;
					default:
						break;
				}

				inputFileErrorCode = flix.SetInputFileEx(InputFile);

				flix.SetOutputFile(OutputFile);
			
				if(inputFileErrorCode == 0)
				{
					WF_FlixCOM.WF_VideoInputProperties videoProperties = flix.GetInputVideoProperties();

					log.Info("Video Properties for " + InputFile);
					log.Info("Framerate " + videoProperties.GetFramerate());
					log.Info("Duration " + videoProperties.GetDuration());
					log.Info("Height " + videoProperties.GetImageHeight());
					log.Info("Width " + videoProperties.GetImageWidth());
					log.Info("Codec " + videoProperties.GetCodec());
					log.Info("Color depth " + videoProperties.GetColorDepth());

					WF_FlixCOM.WF_AudioInputProperties audioProperties = flix.GetInputAudioProperties();

					log.Info("Audio Properties for " + InputFile);
					log.Info("Duration " + audioProperties.GetDuration());
					log.Info("Sample Size " + audioProperties.GetSampleSize());
					log.Info("Sampling Rate " + audioProperties.GetSamplingrate());
					log.Info("Stereo " + audioProperties.GetStereo());
					log.Info("Codec " + audioProperties.GetCodec());

					// Generate a thumb nail preview of the first frame, collect other
					// frames post conversion.

					WF_FlixCOM.WF_Exports exports = flix.GetOtherExports();
					exports.SetNewPrefix(thumbNailPath);
					exports.AddExport (WF_FlixCOM.WF_ExportsIdentifiers.ExportsJPEG);
					exports.SetOutputJpegPath(thumbNailPath);

					// Get the duration of the input video

					WF_FlixCOM.WF_VideoInputProperties inputProperties = flix.GetInputVideoProperties();
					videoDuration = System.Convert.ToUInt64(inputProperties.GetDuration());

					// Encode it

					flix.Encode();

					System.Timers.Timer tm = new System.Timers.Timer();
					tm.Elapsed += new ElapsedEventHandler(EncStatus);
					tm.Interval = 5000;
					tm.Enabled = true;
				
					flix.OnEncodingMessage += new WF_FlixCOM._IWF_FlixCOMEncoderEvents_OnEncodingMessageEventHandler(EncMsg);
					while(flix.IsEncoderRunning()==1)
					{
						Sleep(1000);
					};

					tm.Enabled = false;

					WF_FlixCOM.WF_EncodingStatus stat = flix.GetEncodingStatus();
					log.Info("Total Frames =" +  stat.GetTotalFrames());			

					FileInfo fi = new FileInfo(OutputFile);
					outFileLength = fi.Length;
				}
				else
				{
					log.Error("Conversion SetInputFileEx error for " + InputFile + " Error:" + inputFileErrorCode);
					ret = false;
				}
			}
			catch(Exception e)
			{
				log.Error("Exception converting asset: ", e);
				ret = false;
			}

			return ret;
		}

		private void EncStatus(object source, ElapsedEventArgs e)
		{
			int percent_complete = 0;
			WF_FlixCOM.WF_EncodingStatus stat = flix.GetEncodingStatus();
			percent_complete = stat.GetPercentCompleted();

			if ( _first_pass_encoding_complete)
			{
				percent_complete = _percent_complete + (percent_complete / 2);
			}
			else
			{
				percent_complete = (percent_complete / 2);
			}

			log.Info("Asset Conversion: " + stat.GetElapsedTime() + " seconds. Percent Complete: " + percent_complete + "%");
			if (percent_complete <= 95) // Leave a fudge for generating the .jpg thumbnail images
			{
				StoreUtility.UpdateAssetConversionPercentComplete(_assetId, percent_complete);
			}
		}

		private void EncMsg(string msg)
		{
			log.Info("Asset Conversion Msg: " + msg);

			if ( msg.IndexOf("Converting to FLV") != -1 )
			{
				_percent_complete = 50;
				_first_pass_encoding_complete = true;
			}
		}

		private void EncGenJpgStatus(object source, ElapsedEventArgs e)
		{
			WF_FlixCOM.WF_EncodingStatus stat = flix.GetEncodingStatus();
			log.Info("Asset Thumbnail generation: " + stat.GetElapsedTime() + " seconds");
		}

		private void EncGenJpgMsg(string msg)
		{
			log.Info("Asset Conversion Jpg Msg: " + msg);
			msg = null;
		}

		public bool GenThumbnails( string assetPath, ulong videoDuration)
		{
			ulong inTime = 2000;  // Captured first frame during conversion, advance 2 seconds for the next frame in time
			ulong outTime = 2500; // Cut the clip at 2.5 seconds for the end time of the first .jpg. This will speed conversion.

			for ( int ii = 1; ii <= settings.FLVThumbNailNum; ii++)
			{
				if ( ((inTime/1000) >= videoDuration) || ((outTime/1000) >= videoDuration) )
				{
					break;
				}

				log.Info("Generating Thumbnail " +  ii.ToString());			

				_GenThumbnail( assetPath, inTime, outTime, ii);

				inTime += 2000;
				outTime += 2000;
			}

			StoreUtility.UpdateAssetConversionPercentComplete(_assetId, 100);

			return true;
		}

		private bool _GenThumbnail( string assetPath, ulong inTime, ulong outTime, int jpgNum)
		{
			bool ret = true;

			try
			{
				flix = new WF_FlixCOM.WF_FlixEncoderClass();

				string thumbNailPath = null;

				if ( settings.FlixLogEnabled == 1)
				{
					flix.SetUseLogging(1);
					flix.SetLogPath(settings.FlixLogFile);
				}

				flix.LoadSettings(settings.FlvConfigFile);

				// Override the bitrate control to be VBR_1PASSControl

				WF_FlixCOM.WF_Video flixVideo = flix.GetVideoOptions();
				flixVideo.SetRateControlType(WF_FlixCOM.WF_VideoBitrateControls.VBR_1PASSControl);

				InputFile = assetPath;

				OutputFile = assetPath;

				int extensionIndex = OutputFile.LastIndexOf(".");

				OutputFile = OutputFile.Substring(0, extensionIndex);
				thumbNailPath = OutputFile;

				OutputFile += "_tmp_.flv";

				thumbNailPath += "_";
				thumbNailPath += jpgNum.ToString();
				thumbNailPath += ".jpg";

				WF_FlixCOM.WF_Video videoOptions = flix.GetVideoOptions();

				switch (settings.FlvCompressMode)
				{
					case 0:
						videoOptions.SetCompressMode(WF_FlixCOM.WF_CompressMode.COMPRESSMODE_GOOD);
						break;
					case 1:
						videoOptions.SetCompressMode(WF_FlixCOM.WF_CompressMode.COMPRESSMODE_BEST);
						break;
					default:
						break;
				}

				WF_FlixCOM.WF_MediaEditor mediaEditor = flix.GetMediaEditorOptions();

				mediaEditor.SetCutMovie(1);
				mediaEditor.SetInTime(System.Convert.ToInt32(inTime));
				mediaEditor.SetOutTime(System.Convert.ToInt32(outTime));

				inputFileErrorCode = flix.SetInputFileEx(InputFile);

				flix.SetOutputFile(OutputFile);
			
				if(inputFileErrorCode == 0)
				{
					// Generate a thumb nail

					WF_FlixCOM.WF_Exports exports = flix.GetOtherExports();
					exports.SetNewPrefix(thumbNailPath);
					exports.AddExport (WF_FlixCOM.WF_ExportsIdentifiers.ExportsJPEG);
					exports.SetOutputJpegPath(thumbNailPath);

					// Encode it

					flix.Encode();

					System.Timers.Timer tm = new System.Timers.Timer();
					tm.Elapsed += new ElapsedEventHandler(EncGenJpgStatus);
					tm.Interval = 2000;
					tm.Enabled = true;
				
					flix.OnEncodingMessage += new WF_FlixCOM._IWF_FlixCOMEncoderEvents_OnEncodingMessageEventHandler(EncGenJpgMsg);
					while(flix.IsEncoderRunning()==1)
					{
						Sleep(1000);
					};

					tm.Enabled = false;

					WF_FlixCOM.WF_EncodingStatus stat = flix.GetEncodingStatus();
					log.Info("Total Frames =" +  stat.GetTotalFrames());			

					// Remove the encoded file, it was encoded just to get the .jpg
					FileInfo fi = new FileInfo(OutputFile);
					fi.Delete(); 
				}
				else
				{
					log.Error("Thumbnail generation SetInputFileEx error for " + InputFile + " Error:" + inputFileErrorCode);
					ret = false;
				}
			}
			catch(Exception e)
			{
				log.Error("Exception generating thumbnails: ", e);
				ret = false;
			}

			return ret;
		}
	}
}
