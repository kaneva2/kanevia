///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Xml.Serialization;

namespace Klausent.KEP.App.PublishingServer.Domain.Game
{
	/// <summary>
	/// Summary description for GameFileManifest.
	/// </summary>
	public class GameFileManifest
	{
		private int			_gameId;
		private DateTime	_lastUpdated;
		private IDictionary _files;
		public GameFileManifest()
		{
			_files = new Hashtable();
			_lastUpdated = DateTime.Now;
		}

		public GameFile GetGameFile(string fullname)
		{
			return (GameFile) _files[fullname];
		}

		[XmlIgnore]
		public IDictionary Files
		{
			get { return _files; }
			set { _files = value; }
		}

		public int GameId
		{
			get { return _gameId; }
			set { _gameId = value; }
		}

		public DateTime LastUpdated
		{
			get { return _lastUpdated; }
			set { _lastUpdated = value; }
		}

		public GameFile[] GameFiles 
		{
			get 
			{
				GameFile[] items = new GameFile[ _files.Count ];
				_files.Values.CopyTo(items,0);
				return items;
			}
			set 
			{
				if( value == null ) return;
				GameFile[] items = value;
				_files.Clear();
				foreach( GameFile item in items )
				{
					_files.Add(item.GetFullName(), item);
				}
			}
		}
	}
}
