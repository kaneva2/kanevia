///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using KlausEnt.KEP.Kaneva;

namespace Klausent.KEP.App.PublishingServer.Domain.Game
{
	/// <summary>
	/// Summary description for GameFile.
	/// </summary>
	public class GameFile
	{
		private string _path;
		private string _name;
		private string _hash;

		public GameFile()
		{
		}

		/// <summary>
		/// returns RELATIVE_PATH\FILE_NAME
		/// </summary>
		/// <returns></returns>
		public string GetFullName()
		{
			string retVal = _path == null ? "" : _path;
			if(!retVal.EndsWith(Constants.GAME_UPLOAD_DIR_SEPARATOR))
			{
				retVal += Constants.GAME_UPLOAD_DIR_SEPARATOR;
			}
			retVal += _name;

			return retVal;
		}

		public string Path
		{
			get { return _path; }
			set { _path = value; }
		}

		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}

		public string Hash
		{
			get { return _hash; }
			set { _hash = value; }
		}
	}
}
