///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Xml.Serialization;

namespace Klausent.KEP.App.PublishingServer.Utils
{
	/// <summary>
	/// Summary description for XmlSerializerUtils.
	/// </summary>
	public class XmlSerializerUtils
	{
		private XmlSerializer _xmlSerializer;

		public XmlSerializerUtils(Type type)
		{
			//
			// TODO: Add constructor logic here
			//
			_xmlSerializer = new XmlSerializer(type);
		}

		public object Load(string path)
		{
			object retVal = null;
			lock(_xmlSerializer)
			{
				TextReader r = new StreamReader( path );
				retVal = _xmlSerializer.Deserialize( r );
				r.Close();
			}

			return retVal;
		}

		public void Save(object obj, string path)
		{
			lock(_xmlSerializer)
			{
				FileInfo f = new FileInfo(path);
				if(!f.Directory.Exists)
				{
					Directory.CreateDirectory(f.DirectoryName);
				}

				TextWriter w = new StreamWriter( path );
				_xmlSerializer.Serialize( w, obj );
				w.Close();
			}
		}
	}
}
