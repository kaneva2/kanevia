///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Web.Mail;
using Klausent.KEP.App.PublishingServer.Dao;
using Klausent.KEP.App.PublishingServer.Domain.Game;
using Klausent.KEP.App.PublishingServer.Utils;
using KlausEnt.KEP.AssetPublishing;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace KlausEnt.KEP.GameUploadPublishing
{
	/// <summary>
	/// Summary description for PostGameUploadedProcessor.
	/// </summary>
	public class PostGameUploadedProcessor : IGameUploadProcessor
	{
		public PostGameUploadedProcessor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public void Process(GameUpload gameUpload)
		{
			PostGameUploadedProcessorWorker worker = new PostGameUploadedProcessorWorker() ;
			worker.GameUpload = gameUpload;
			ThreadPool.QueueUserWorkItem(new WaitCallback(worker.Run), this);
		}
	}

	class PostGameUploadedProcessorWorker
	{
		private static readonly GameUploadProcessRegistry gameUploadProcessRegistry = GameUploadProcessRegistry.GetInstance();
		private static GameUploadFileListDao gameUploadFileListDao = GameUploadFileListDao.GetInstance();
		private static GameFileManifestDao gameFileManifestDao = GameFileManifestDao.GetInstance();
		private static Settings settings = Settings.GetInstance();
		
		private static readonly ILog log = LogManager.GetLogger(typeof(PostGameUploadedProcessorWorker));

		private	GameUpload _gameUpload;

		public PostGameUploadedProcessorWorker()
		{
		}

		public void Run(object state)
		{
			IGameUploadProcessor processor = (IGameUploadProcessor)state;
			if(gameUploadProcessRegistry.Register(_gameUpload, processor))
			{
				try
				{
					log.Info("Merging uploaded game to staging, gameId = " + _gameUpload.GameId);
					MergeFiles();
					log.Info("Finished merging uploaded game to staging, gameId = " + _gameUpload.GameId);
				}
				catch(Exception e)
				{
					log.Error("error processing uploaded gameUpload", e);
				}
				finally
				{
					gameUploadProcessRegistry.UnRegister(_gameUpload, processor);
				}
			}
		}

		/// <summary>
		/// 1. move files in upload directory to staging directory
		/// 2. delete files that are in staging maniefest but not in manifest in upload directory
		/// 3. record change history
		/// 4. move manifest from upload to staging
		/// 5. send email notifications to game master and owner
		/// 6. delete content from upload dir
		/// </summary>
		private int MergeFiles()
		{
			//TODO missing function to check whether stagin server is running

			int retVal = (int) Constants.eGAME_UPLOAD_PROCESSOR_RETURNCODES.SUCCESS;
			try
			{
				StringBuilder changeLog = new StringBuilder();
				changeLog.Append("Update - " + DateTime.Now).Append("\r\n");
				CopyFiles(changeLog);
				DeleteExtraFiles(changeLog);
				UpdateManifest();
				changeLog.Append("\r\n");
				WriteChangeLog(changeLog);
				UpdateGameUploadStatus();
				SendEmailNotifications();
				DeleteUploadedFiles();
			}catch(Exception e)
			{
				log.Error("Failed to update game on staging, game id = " + 
					_gameUpload.GameId + " game upload id = " + _gameUpload.GameUploadId, e);
			    UpdateGameUploadStatusError();
				retVal = (int) Constants.eGAME_UPLOAD_PROCESSOR_RETURNCODES.ERROR_MOVING_FILES;
			}

			return retVal;
		}

		/// <summary>
		/// move files in upload directory to staging directory, except manifest.xml
		/// </summary>
		/// <returns></returns>
		private void CopyFiles(StringBuilder changeLog)
		{
			//move files
			DirectoryInfo uploadedDir = new DirectoryInfo(_gameUpload.UploadedDir);
			DirectoryInfo stagingDir = new DirectoryInfo(_gameUpload.StagingDir);

			try
			{
				XCopy(uploadedDir, stagingDir, true, changeLog);
			}
			catch(Exception e)
			{
				log.Error("Failed to copy uploaded game files");
				throw e;
			}
			log.Info(" Updated files copied, gameId = " + _gameUpload.GameId);
		}

		/// <summary>
		/// delete files that are in staging maniefest but not in manifest in upload directory
		/// </summary>
		/// <returns></returns>
		private void DeleteExtraFiles(StringBuilder changeLog)
		{
			FileInfo manifestStaging = new FileInfo(Path.Combine(_gameUpload.StagingDir, 
				Constants.GAME_FILE_MANIFEST));
			FileInfo manifestUpload = new FileInfo(Path.Combine(_gameUpload.UploadedDir, 
				Constants.GAME_FILE_MANIFEST));

			GameFileManifest stagingManifest = null;
			GameUploadFileList gameUploadFileList = null;

			//load staging manifest
			if(!manifestStaging.Exists)
			{
				log.Warn("No " + Constants.GAME_FILE_MANIFEST + 
					" found at " + _gameUpload.StagingDir +" creating new...");
				stagingManifest = new GameFileManifest() ;
			}else
			{
				stagingManifest = gameFileManifestDao.Load(manifestStaging.FullName);
			}

			//load uploadedManifest
			gameUploadFileList = gameUploadFileListDao.Load(manifestUpload.FullName);

			foreach(GameFile gameFile in stagingManifest.Files.Values)
			{
				if(gameUploadFileList.Get(gameFile.GetFullName()) == null)
				{
					//this file needs to be deleted
					FileInfo fileToDel = new FileInfo(Path.Combine(_gameUpload.StagingDir, gameFile.GetFullName()));
					log.Info("Deleting " + fileToDel.FullName +" from staging server");
					fileToDel.Delete();
					changeLog.Append("Deleted\t" + gameFile.GetFullName()).Append("\r\n");
				}
			}

			log.Info("Extra files on staging deleted, gameId = " + _gameUpload.GameId);
		}

		/// <summary>
		/// move manifest from upload to staging
		/// </summary>
		/// <returns></returns>
		private void UpdateManifest()
		{
			//load manifests
			GameFileManifest stagingManifest = new GameFileManifest();
			GameUploadFileList gameUploadFileList = gameUploadFileListDao.Load(
				Path.Combine(_gameUpload.UploadedDir, Constants.GAME_FILE_MANIFEST));

			GameUploadFile[] gameUploadFiles = gameUploadFileList.GameUploadFiles;
			GameFile[] gameFiles = new GameFile[gameUploadFiles.Length];
			int i = 0;
			foreach(GameUploadFile gameUploadFile in gameUploadFiles)
			{
				GameFile gameFile = new GameFile() ;
				gameFile.Path = gameUploadFile.RelativePath;
				gameFile.Name = gameUploadFile.Name;
				gameFile.Hash = gameUploadFile.Hash;
				if(gameFile.Hash == null || gameFile.Hash.Trim().Length == 0)
				{
					//this is not right
					log.Warn("No hash value found for file " + gameFile.GetFullName() +" recalculating...");
					gameFile.Hash = HashCalculator.GetMD5(new FileStream(Path.Combine(_gameUpload.StagingDir, 
						gameFile.GetFullName()),FileMode.Open));
				}
				gameFiles[i++] = gameFile;
			}
			stagingManifest.GameFiles = gameFiles;
			stagingManifest.GameId = _gameUpload.GameId;
			stagingManifest.LastUpdated = DateTime.Now;

			gameFileManifestDao.Save(stagingManifest, 
				Path.Combine(_gameUpload.StagingDir, Constants.GAME_FILE_MANIFEST));

			log.Info("Manifest file updated on staging, gameId = " + _gameUpload.GameId);
		}

		/// <summary>
		/// create or update change log
		/// </summary>
		/// <param name="changeLog"></param>
		private void WriteChangeLog(StringBuilder changeLog)
		{
			string changeLogPath = Path.Combine(_gameUpload.StagingDir, Constants.GAME_FILE_CHANGE_LOG);
			FileInfo changeLogFile = new FileInfo(changeLogPath) ;
			if(!changeLogFile.Exists)
			{
				log.Info("Created change log for game " + _gameUpload.GameId);
				changeLogFile.Create().Close();
			}

			//read in old logs
			TextReader reader = new StreamReader(changeLogPath);
			String line = null;
			while ((line=reader.ReadLine())!=null)
			{
				changeLog.Append(line);
			}
			reader.Close();

			//rewrite the whole log
			changeLogFile.Delete();
			TextWriter writer = new StreamWriter(changeLogPath);
			writer.Write(changeLog.ToString());
			writer.Close();

			log.Info("change log updated, gameId = " + _gameUpload.GameId);
		}

		private void UpdateGameUploadStatus()
		{
			StoreUtility.UpdateGameUploadStatus(_gameUpload.GameUploadId, 
				(int) Constants.eGAME_UPLOAD_PUBLISH_STATUS.STAGING_UPDATED, null);

			log.Info("Game upload status changed to STAGING_UPDATED, gameUploadId = " + _gameUpload.GameUploadId);
		}

		private void UpdateGameUploadStatusError()
		{
			StoreUtility.UpdateGameUploadStatus(_gameUpload.GameUploadId, 
				(int) Constants.eGAME_UPLOAD_PUBLISH_STATUS.ERROR, null);

			log.Info("Game upload status changed to ERROR, gameUploadId = " + _gameUpload.GameUploadId);
		}

		/// <summary>
		/// notify IT and game owner that the game is published
		/// </summary>
		private void SendEmailNotifications()
		{
			//notify IT
			log.Info("Sending game uploaded notification to IT");
			string itBody = "Game " + _gameUpload.Name + " is uploaded on " + _gameUpload.StagingDir;
            //try
            //{
            //    MailUtility.SendEmail(settings.GameUploadedEmailFrom,settings.GameUploadedEmailItTo,
            //        "Game uploaded on staging", itBody, MailFormat.Text);
            //}catch(Exception e)
            //{
            //    log.Error("Error sending game uploaded notification to IT", e);
            //}

			//notify owner
			if(_gameUpload.UserEmail != null && _gameUpload.UserEmail.Trim().Length > 0 )
			{
				log.Info("Sending game uploaded notification to user");
				try
				{
					string itUser = "Game " + _gameUpload.Name + " is uploaded on Kaneva";
                    //MailUtility.SendEmail(settings.GameUploadedEmailFrom,_gameUpload.UserEmail,
                    //    "Game uploaded", itUser, MailFormat.Text);
				}catch(Exception e)
				{
					log.Error("Error sending game uploaded notification to user ", e);
				}
			}else
			{
				log.Error("Unable to send game uploaded notification to user because " + 
					"user doesn't have a valid email, gameId = " + _gameUpload.GameId);
			}
		}

		/// <summary>
		/// delete content from upload dir
		/// </summary>
		/// <returns></returns>
		private void DeleteUploadedFiles()
		{
			try
			{
				DirectoryInfo di = new DirectoryInfo(_gameUpload.UploadedDir);
				DirectoryInfo userDir = di.Parent;
				di.Delete(true);
				//delete user dir if it's empty
				if(userDir.GetFileSystemInfos().Length  == 0)
				{
					userDir.Delete();
				}
				if(log.IsInfoEnabled)
				{
					log.Info("deleted uploaded game files at " + _gameUpload.UploadedDir);
				}
			}
			catch(Exception e)
			{
				log.Error("Failed to delete uploaded game files");
				throw e;
			}

			log.Info("Deleted uploaded files from upload server, gameId = " + _gameUpload.GameId);
		}

		/// <summary>
		/// copy file and folder from one dir to another
		/// </summary>
		/// <param name="sourceDir"></param>
		/// <param name="destDir"></param>
		/// <param name="overwrite"></param>
		private void XCopy(DirectoryInfo sourceDir,
			DirectoryInfo destDir, bool overwrite, StringBuilder changeLog)
		{
			XCopy(sourceDir, destDir, overwrite, 0, changeLog);
		}


		/// <summary>
		/// copy file and folder from one dir to another
		/// </summary>
		/// <param name="sourceDir"></param>
		/// <param name="destDir"></param>
		/// <param name="overwrite"></param>
		/// <param name="level">curr folder level</param>
		private void XCopy(DirectoryInfo sourceDir,
			DirectoryInfo destDir, bool overwrite, int level, StringBuilder changeLog)
		{
			DirectoryInfo[] sourceSubDirs = sourceDir.GetDirectories();
			FileInfo[] sourceFiles = sourceDir.GetFiles();

			//Create dest dir
			if (!destDir.Exists) destDir.Create();
            
			//copy files and sub dirs recursively
			foreach (DirectoryInfo di in sourceSubDirs)
			{
				XCopy(di, new DirectoryInfo(
					destDir.FullName + @"\" + di.Name),overwrite, level + 1, changeLog);
			}
			
			foreach (FileInfo fi in sourceFiles)
			{
				if(level == 0 && fi.Name.Equals(Constants.GAME_FILE_MANIFEST))
				{
					//ignore manifest file
					continue;
				}
				string fullName = Path.Combine(destDir.FullName, fi.Name);
				if(File.Exists(fullName))
				{
					//a update
					changeLog.Append("Updated\t" + GetRelativeName(fullName)).Append("\r\n");
				}else
				{
					//new file
					changeLog.Append("Added\t" + GetRelativeName(fullName)).Append("\r\n");
				}
				fi.CopyTo(fullName, overwrite);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="fullName">full file path on staging</param>
		/// <returns>file name relative to the staging dir</returns>
		private string GetRelativeName(string fullName)
		{
			return fullName.Substring(_gameUpload.StagingDir.Length + 1);
		}

		public GameUpload GameUpload
		{
			get { return _gameUpload; }
			set { _gameUpload = value; }
		}
	}
}
