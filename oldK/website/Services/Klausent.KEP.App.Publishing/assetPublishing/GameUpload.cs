///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;

namespace KlausEnt.KEP.GameUploadPublishing
{
	/// <summary>
	/// Summary description for GameUpload.
	/// </summary>
	public class GameUpload
	{
		private int		_gameUploadId;
		private int		_gameId;
		private int		_userId;
		private string	_uploadedDir;
		private string  _stagingDir;
		private int		_publishStatusId;
		private string	_name;
		private string	_userEmail;

		public GameUpload()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public int GameUploadId
		{
			get { return _gameUploadId; }
			set { _gameUploadId = value; }
		}

		public int UserId
		{
			get { return _userId; }
			set { _userId = value; }
		}

		public int GameId
		{
			get { return _gameId; }
			set { _gameId = value; }
		}

		public string UploadedDir
		{
			get { return _uploadedDir; }
			set { _uploadedDir = value; }
		}

		public int PublishStatusId
		{
			get { return _publishStatusId; }
			set { _publishStatusId = value; }
		}

		public string StagingDir
		{
			get { return _stagingDir; }
			set { _stagingDir = value; }
		}

		/// <summary>
		/// name of the game
		/// </summary>
		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}

		/// <summary>
		/// owner's email
		/// </summary>
		public string UserEmail
		{
			get { return _userEmail; }
			set { _userEmail = value; }
		}
	}
}
