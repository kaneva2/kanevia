///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Threading;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace KlausEnt.KEP.AssetPublishing
{
	/// <summary>
	/// Summary description for PostTorrentImportedProcessor.
	/// </summary>
	public class PostTorrentImportedProcessor : IAssetProcessor
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(PostTorrentImportedProcessor));
		public PostTorrentImportedProcessor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public void ProcessAsync(Asset asset, ref SignalResult accepted)
		{
			PostTorrentImportedProcessorWorker worker = new PostTorrentImportedProcessorWorker() ;
			worker.Asset = asset;
			worker.Run(this);
		}

		public void Process(Asset asset)
		{
			PostTorrentImportedProcessorWorker worker = new PostTorrentImportedProcessorWorker() ;
			worker.Asset = asset;
			ThreadPool.QueueUserWorkItem(new WaitCallback(worker.Run), this);
		}
	}

	class PostTorrentImportedProcessorWorker
	{
		private static readonly AssetProcessRegistry assetProcessRegistry = AssetProcessRegistry.GetInstance();
		private static readonly ILog log = LogManager.GetLogger(typeof(PostTorrentImportedProcessorWorker));

		private	Asset _asset;

		public PostTorrentImportedProcessorWorker()
		{
		}

		public void Run(object state)
		{
			IAssetProcessor processor = (IAssetProcessor)state;
			if(assetProcessRegistry.Register(_asset, processor))
			{
				try
				{
					UpdatePublishStatus();
				}
				catch(Exception e)
				{
					log.Error("error making new asset", e);
				}
				finally
				{
					assetProcessRegistry.UnRegister(_asset, processor);
				}
			}
		}

		/// <summary>
		/// last step is completed so update publish_status to PUBLISH_COMPLETE
		/// </summary>
		private void UpdatePublishStatus()
		{
			StoreUtility.UpdateAssetPublishStatus(_asset.AssetId, (int) Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE, 0);
			//also update torrent_status for backward compatiblities
			//StoreUtility.UpdateAssetTorrentStatus(_asset.AssetId, (int) Constants.eTORRENT_STATUS.PUBLISH_COMPLETE);

			if(log.IsInfoEnabled)
			{
				log.Info("torrent is imported, status set to PUBLISH_COMPLETE, asset_id = " 
					+ _asset.AssetId);
			}
		}

		public Asset Asset
		{
			get { return _asset; }
			set { _asset = value; }
		}
	}
}
