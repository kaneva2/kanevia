///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using KlausEnt.KEP.Kaneva;

namespace KlausEnt.KEP.AssetPublishing
{
	/// <summary>
	/// Summary description for AssetProcessorFactory.
	/// </summary>
	public class AssetProcessorFactory
	{
		public AssetProcessorFactory()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static IAssetProcessor GetProcessor(Asset torrent)
		{
			IAssetProcessor retVal = null;
			switch(torrent.PublishStatusId)
			{
				case (int) Constants.ePUBLISH_STATUS.UPLOADED:
					retVal = new PostAssetUploadedProcessor();
					break;
				case (int) Constants.ePUBLISH_STATUS.TORRENT_IMPORTED:
					retVal = new PostTorrentImportedProcessor();
					break;
				default:
					retVal = null;
					break;
			}

			return retVal;
		}
	}
}
