///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;

namespace KlausEnt.KEP.AssetPublishing
{
	/// <summary>
	/// Summary description for Asset.
	/// </summary>
	public class Asset
	{
		private int		_assetId;
		private int		_userId;
		private string	_fileName;
		private string	_uploadedDir;
		private int		_publishStatusId;

		public Asset()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public int AssetId
		{
			get { return _assetId; }
			set { _assetId = value; }
		}

		public int UserId
		{
			get { return _userId; }
			set { _userId = value; }
		}

		public string FileName
		{
			get { return _fileName; }
			set { _fileName = value; }
		}

		public string UploadedDir
		{
			get { return _uploadedDir; }
			set { _uploadedDir = value; }
		}

		public int PublishStatusId
		{
			get { return _publishStatusId; }
			set { _publishStatusId = value; }
		}

		/// <summary>
		/// full path where the file is saved
		/// </summary>
		/// <returns></returns>
		public String GetFilePath()
		{
			return Path.Combine(_uploadedDir, _fileName);
		}
	}
}
