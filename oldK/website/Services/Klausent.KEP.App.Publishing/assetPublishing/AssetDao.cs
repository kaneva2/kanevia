///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace KlausEnt.KEP.AssetPublishing
{
	/// <summary>
	/// This class uses StoreUtility to populate/save asset records
	/// </summary>
	public class AssetDao
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(AssetDao));
		private static AssetDao instance;

		private AssetDao()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static AssetDao GetInstance()
		{
			if(instance == null)
			{
				instance = new AssetDao() ;
			}
			return instance;
		}

		/// <summary>
		/// get all assets to be processed
		/// </summary>
		/// <returns></returns>
		public IList GetAll()
		{
			IList retVal = new ArrayList();
			DataTable table = StoreUtility.GetAssetsToProcess();
			if(table != null && table.Rows.Count > 0)
			{
				foreach(DataRow row in table.Rows)
				{
					Asset asset = new Asset();
					asset.AssetId = int.Parse(row["asset_id"].ToString());
					asset.FileName = row["filename"].ToString();
					asset.PublishStatusId = int.Parse(row["publish_status_id"].ToString());
					asset.UploadedDir = row["path"].ToString();
				    asset.UserId = int.Parse(row["owner_id"].ToString());
					retVal.Add(asset);
				}
			}
			return retVal;
		}
	}
}
