///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using Klausent.KEP.App.PublishingServer.Domain.Game;
using Klausent.KEP.App.PublishingServer.Utils;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace Klausent.KEP.App.PublishingServer.Dao
{
	/// <summary>
	/// Summary description for GameUploadFileListDao.
	/// </summary>
	public class GameUploadFileListDao
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(GameUploadFileListDao));

		private static GameUploadFileListDao inst;
		private XmlSerializerUtils _xmlSerializerUtils;

		private GameUploadFileListDao()
		{
			_xmlSerializerUtils = new XmlSerializerUtils( typeof( GameUploadFileList ) );
		}

		public static GameUploadFileListDao GetInstance()
		{
			if(inst == null)
			{
				inst = new GameUploadFileListDao() ;
			}
			return inst;
		}

		
		/// <summary>
		/// load game manifest file 
		/// </summary>
		/// <param name="path">full path where the xml file is </param>
		/// <returns></returns>
		public GameUploadFileList Load(string path)
		{
			try
			{
				return (GameUploadFileList) _xmlSerializerUtils.Load(path);
			}
			catch(Exception e)
			{
				log.Error("Failed to load game upload file manifest", e);
				throw e;
			}
		}
		
		/// <summary>
		/// save GameUploadFileList to xml file
		/// </summary>
		/// <param name="fileList"></param>
		/// <param name="path">full path where the xml file is going to be save</param>
		public void Save(GameUploadFileList fileList, string path)
		{
			try
			{
				_xmlSerializerUtils.Save(fileList, path);
			}
			catch(Exception e)
			{
				log.Error("Failed to save game upload file manifest", e);
				throw e;
			}
		}
	}
}
