///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using Klausent.KEP.App.PublishingServer.Domain.Game;
using Klausent.KEP.App.PublishingServer.Utils;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace Klausent.KEP.App.PublishingServer.Dao
{
	/// <summary>
	/// Summary description for GameFileManifestDao.
	/// </summary>
	public class GameFileManifestDao
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(GameFileManifestDao));

		private static GameFileManifestDao inst;
		private XmlSerializerUtils _xmlSerializerUtils;

		private GameFileManifestDao()
		{
			_xmlSerializerUtils = new XmlSerializerUtils( typeof( GameFileManifest ) );
		}

		public static GameFileManifestDao GetInstance()
		{
			if(inst == null)
			{
				inst = new GameFileManifestDao() ;
			}
			return inst;
		}

		
		/// <summary>
		/// load game manifest file 
		/// </summary>
		/// <param name="path">full path where the xml file is </param>
		/// <returns></returns>
		public GameFileManifest Load(string path)
		{
			GameFileManifest retVal = null;
			try
			{
				retVal = (GameFileManifest) _xmlSerializerUtils.Load(path);
			}catch(FileNotFoundException)
			{
				//game not on staging yet
			}
			catch(Exception e)
			{
				log.Error("Failed to load game file manifest", e);
				throw e;
			}
			return retVal;
		}
		
		/// <summary>
		/// save GameFileManifest to xml file
		/// </summary>
		/// <param name="gameFileManifest"></param>
		/// <param name="path">full path where the xml file is going to be save</param>
		public void Save(GameFileManifest gameFileManifest, string path)
		{
			try
			{
				_xmlSerializerUtils.Save(gameFileManifest, path);
			}
			catch(Exception e)
			{
				log.Error("Failed to save game file manifest", e);
				throw e;
			}
		}
	}
}
