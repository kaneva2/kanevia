///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Text;
using KlausEnt.KEP.AssetPublishing;
using log4net;

namespace KlausEnt.KEP.GameUploadPublishing
{
	/// <summary>
	/// Summary description for GameUploadProcessRegistry.
	/// </summary>
	public class GameUploadProcessRegistry
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(GameUploadProcessRegistry));

		private IDictionary _processes;
		private static GameUploadProcessRegistry instance;
		
		private GameUploadProcessRegistry()
		{
			//
			// TODO: Add constructor logic here
			//
			_processes = new Hashtable();
		}

		public static GameUploadProcessRegistry GetInstance()
		{
			if(instance == null)
			{
				instance = new GameUploadProcessRegistry() ;
			}
			return instance;
		}

		/// <summary>
		/// register a gameUpload processing task, return false if the gameUpload 
		/// is already being processed
		/// </summary>
		/// <param name="gameUpload"></param>
		/// <returns></returns>
		public bool Register(GameUpload gameUpload, IGameUploadProcessor processor)
		{
			bool retVal = false;
			lock(this)
			{
				if(!_processes.Contains(gameUpload.GameId))
				{
					if(log.IsDebugEnabled)
					{
						log.Debug(processor.GetType().ToString() + " is processing game " + gameUpload.GameId);
					}
					_processes.Add(gameUpload.GameId,processor);
					retVal = true;
				}
				else
				{
					if(log.IsInfoEnabled)
					{
						StringBuilder sb = new StringBuilder() ;
						sb.Append(processor.GetType().ToString());
						sb.Append(" CAN NOT process game ");
						sb.Append(gameUpload.GameId);
						sb.Append("\r\n");
						sb.Append("GameUpload is being processed by");
						sb.Append(_processes[gameUpload.GameId].GetType().ToString());
						log.Info(sb.ToString());
					}
				}
			}
			return retVal;
		}

		/// <summary>
		/// this task is completed, sucessfully or not
		/// </summary>
		/// <param name="gameUpload"></param>
		/// <param name="processor"></param>
		/// <returns></returns>
		public bool UnRegister(GameUpload gameUpload, IGameUploadProcessor processor)
		{
			bool retVal = false;
			lock(this)
			{
				if(!_processes.Contains(gameUpload.GameId))
				{
					log.Error("GameUpload not found in the registry, gameId = " + gameUpload.GameId);
				}
				else
				{
					_processes.Remove(gameUpload.GameId);
					if(log.IsDebugEnabled)
					{
						log.Debug("GameUpload removed from the registry, gameId = " + gameUpload.GameId);
					}
					retVal = true;
				}
			}
			return retVal;
		}
	}
}
