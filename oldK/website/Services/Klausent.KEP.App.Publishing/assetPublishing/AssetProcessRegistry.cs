///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Text;
using log4net;

namespace KlausEnt.KEP.AssetPublishing
{
	/// <summary>
	/// Summary description for AssetProcessRegistry.
	/// </summary>
	public class AssetProcessRegistry
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(AssetProcessRegistry));

		private IDictionary _processes;
		private static AssetProcessRegistry instance;
		
		private AssetProcessRegistry()
		{
			//
			// TODO: Add constructor logic here
			//
			_processes = new Hashtable();
		}

		public static AssetProcessRegistry GetInstance()
		{
			if(instance == null)
			{
				instance = new AssetProcessRegistry() ;
			}
			return instance;
		}

		/// <summary>
		/// register a asset processing task, return false if the asset 
		/// is already being processed
		/// </summary>
		/// <param name="asset"></param>
		/// <returns></returns>
		public bool Register(Asset asset, IAssetProcessor processor)
		{
			bool retVal = false;
			lock(this)
			{
				if(!_processes.Contains(asset.AssetId))
				{
					if(log.IsDebugEnabled)
					{
						log.Debug(processor.GetType().ToString() + " is processing asset " + asset.AssetId);
					}
					_processes.Add(asset.AssetId,processor);
					retVal = true;
				}
				else
				{
					if(log.IsInfoEnabled)
					{
						StringBuilder sb = new StringBuilder() ;
						sb.Append(processor.GetType().ToString());
						sb.Append(" CAN NOT process asset ");
						sb.Append(asset.AssetId);
						sb.Append("\r\n");
						sb.Append("Asset is being processed by");
						sb.Append(_processes[asset.AssetId].GetType().ToString());
						log.Info(sb.ToString());
					}
				}
			}
			return retVal;
		}

		/// <summary>
		/// this task is completed, sucessfully or not
		/// </summary>
		/// <param name="asset"></param>
		/// <param name="processor"></param>
		/// <returns></returns>
		public bool UnRegister(Asset asset, IAssetProcessor processor)
		{
			bool retVal = false;
			lock(this)
			{
				if(!_processes.Contains(asset.AssetId))
				{
					log.Error("Asset not found in the registry " + asset.AssetId);
				}
				else
				{
					_processes.Remove(asset.AssetId);
					if(log.IsDebugEnabled)
					{
						log.Debug("Asset removed from the registry " + asset.AssetId);
					}
					retVal = true;
				}
			}
			return retVal;
		}
	}
}
