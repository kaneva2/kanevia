///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using Com.Kei.Utils;
using System.Runtime.InteropServices;
using KlausEnt.KEP.Kaneva;
using KlausEnt.KEP.KEPDS;
using log4net;

namespace KlausEnt.KEP.AssetPublishing
{
	/// <summary>
	/// Summary description for PostAssetUploadedProcessor.
	/// </summary>
	public class PostAssetUploadedProcessor : IAssetProcessor
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(PostAssetUploadedProcessor));
		public PostAssetUploadedProcessor()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		public void ProcessAsync(Asset asset, ref SignalResult result)
		{
			PostAssetUploadedProcessorWorker worker = new PostAssetUploadedProcessorWorker() ;
			worker.Asset = asset;
			worker._sigResult = result;
			worker.Run(this);
		}

		public void Process(Asset asset)
		{
			PostAssetUploadedProcessorWorker worker = new PostAssetUploadedProcessorWorker() ;
			worker.Asset = asset;
			ThreadPool.QueueUserWorkItem(new WaitCallback(worker.Run), this);
		}

	}

	class PostAssetUploadedProcessorWorker
	{
		private static readonly AssetProcessRegistry assetProcessRegistry = AssetProcessRegistry.GetInstance();

		private static readonly Settings settings = Settings.GetInstance();
		private static readonly ILog log = LogManager.GetLogger(typeof(PostAssetUploadedProcessorWorker));

		protected AzureusUtility AZUREUS;
		//protected ABCUtility ABC;
		protected BNBTUtility BNBT;

		private	Asset _asset;
		private long _clientNumber;
		private string _contentPath;

		public SignalResult _sigResult;

		public PostAssetUploadedProcessorWorker()
		{
			AZUREUS = new AzureusUtility();
			BNBT = new BNBTUtility();
			//ABC = new ABCUtility();
		}

		public void Run(object state)
		{
			IAssetProcessor processor = (IAssetProcessor)state;
			if(assetProcessRegistry.Register(_asset, processor))
			{
				try
				{
					log.Info("processing asset " + _asset.AssetId);
					string path = _asset.GetFilePath();
					FileInfo fi = new FileInfo(path);

					string flvExtensions = settings.FlvConversionExtensions;

					bool performFLVConversion = false;

					foreach (string ss in flvExtensions.Split(','))
					{
						if ( fi.Extension != null && (string.Compare(ss, fi.Extension, true) == 0) )
						{
							// If the asset has an extension in our list of extensions
							// to convert to FLV format.

							performFLVConversion = true;
							break;
						}
					}

					SkipCreateTorrent(performFLVConversion);
				}
				catch(Exception e)
				{
					log.Error("error processing uploaded asset", e);
				}
				finally
				{
					assetProcessRegistry.UnRegister(_asset, processor);
				}
			}
		}

		/// <summary>
		/// for larger files, torrent creation is skipped. just move the files to the content
		/// folder and mark asset publish status as complete
		/// </summary>
		private void SkipCreateTorrent( bool flvConversion)
		{
			//move the file
			string assetPath = null;
			bool alreadyMoved = false;

			int retVal = MoveNonTorrentContentFile(ref assetPath, ref alreadyMoved);

			if( (retVal == (int) Constants.eASSET_PROCESSOR_RETURNCODES.SUCCESS) || (alreadyMoved) )
			{
				log.Info("Asset " + _asset.AssetId +" has been moved, no torrent made");

				if  ( flvConversion)
				{
					_sigResult.result = 0;

					System.Collections.Hashtable htMediaConverters = KanevaGlobals.MediaConverters;

					if (htMediaConverters == null)
					{
						log.Error("No media converters defined in .config file");
					}
					else
					{
						System.Collections.IDictionaryEnumerator myEnumerator = htMediaConverters.GetEnumerator();

						while ( myEnumerator.MoveNext() )
						{
							try
							{
								MediaConverter converter = (MediaConverter) myEnumerator.Value;

								if ( SignalMediaConverter(converter.IP, converter.Port, _asset.AssetId, assetPath) )
								{
									// A machine accepted to perform the conversion.

									_sigResult.result = 1;

									string filename = _asset.FileName;
									int position = filename.LastIndexOf('.');

									if (position != -1)
									{
										filename = filename.Remove(position);
										filename += ".FLV";
									}

									string mediaURL = _contentPath.Replace ("\\","/").Replace (KanevaGlobals.ContentServerPath,"") + "/" + filename;

									log.Info("Updating media_path to: " + mediaURL);

									StoreUtility.UpdateAssetMediaPath(_asset.AssetId, mediaURL);

									break;
								}
							}
							catch (Exception exc)
							{
								log.Error ("Error signaling media converters:", exc);
							}
						}
					}
				}

				if ( ! flvConversion)
				{
					// update publishing status to complete, if flv conversion was performed, the media
					// converter will update the status

					ulong duration = CheckMP3Duration(assetPath); 

					StoreUtility.UpdateAssetPublishStatus(_asset.AssetId, (int) Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE, duration);
				}

				//delete uploaded file
				if ( ! alreadyMoved)
				{
					DeleteUploadedFile();
				}
			}
			else if ( ! alreadyMoved)
			{
				StoreUtility.UpdateAssetPublishStatus(_asset.AssetId, (int) Constants.ePUBLISH_STATUS.ERROR, 0);
			}
		}

		private bool SignalMediaConverter(string ip, int port, int assetId, string assetPath)
		{
			bool successCode = false;

			TcpClient tc = null;

			try
			{
				tc = new TcpClient(ip, port);
				string flvExtensions = settings.FlvConversionExtensions;

				tc.ReceiveTimeout = settings.MediaConverterReceiveTimeout;
				tc.SendTimeout = settings.MediaConverterSendTimeout;

				NetworkStream request = tc.GetStream ();
				StringBuilder csvRequest = new StringBuilder();

				csvRequest.Append("1"); // Place holder for Command Id. Currently ignored by media converter. Only one command.
				csvRequest.Append(":" + assetPath);
				csvRequest.Append(":" + assetId);
				csvRequest.Append("\r\n");

				string csvString = csvRequest.ToString();
				byte[] csvBytes = Encoding.UTF8.GetBytes(csvString);

				log.Info("Signaling media converter: " + ip + ":" + port + " for assetId=" + assetId + " media location=" + assetPath);

				request.Write(csvBytes, 0, csvBytes.Length);
				request.Flush();

				Byte[] response = new Byte[32];
				int bytes = 0;

				bytes = request.Read(response, 0, response.Length);

				log.Info("Media converter response byte count=" + bytes + " response[0]=" + response[0] + " response[1]=" + response[1]);

				if ( response[0] == 1) // 1 = mediaConverter accepted request, 0 reject
				{
					successCode = true;
				}

				tc.Close();
			}
			catch (Exception e)
			{
				log.Error("Error signaling media converter: " + ip + ":" + port + " for assetId=" + assetId, e);
			}
			finally
			{
				if ( tc != null )
				{
					tc.Close();
				}
			}

			if ( successCode)
			{
				log.Info("Media converter: " + ip + " accepted request");
			}
			else
			{
				log.Info("Media converter: " + ip + " rejected request");
			}

			return successCode;
		}
	

		/// <summary>
		/// move file to content server
		/// </summary>
		private int MoveNonTorrentContentFile(ref string assetPath, ref bool alreadyMoved)
		{
			int retVal = (int) Constants.eASSET_PROCESSOR_RETURNCODES.SUCCESS;
			alreadyMoved = false;

			//saving file to content server
			try
			{
				_contentPath = Path.Combine(Path.Combine(settings.HttpContentPath, _asset.UserId.ToString()),
					_asset.AssetId.ToString());
				string targetPath = Path.Combine(_contentPath, _asset.FileName);
				log.Info("asset " + _asset.AssetId + " targetPath: " + targetPath);
				if ( ! File.Exists(targetPath) )
				{
					FileStream fileStream = new FileStream(_asset.GetFilePath(), FileMode.Open, FileAccess.Read);

					IOUtils.SaveFile(fileStream, targetPath);
					if(log.IsInfoEnabled)
					{
						log.Info("asset " + _asset.AssetId + " moved to " + targetPath);
					}

					//update content_extension and target_dir in assets table
					StoreUtility.UpdateAssetFilePath(_asset.AssetId, _asset.FileName,_contentPath, 
						(new FileInfo(targetPath)).Length);
				}
				else
				{
					// Already exists at target location. Moved but no media processors available to handle
					// the conversion at the time. Picked back up for later processing.

					alreadyMoved = true;

					if(log.IsInfoEnabled)
					{
						log.Info("asset " + _asset.AssetId + " already exists at " + targetPath);
					}
				}

				assetPath = targetPath;
			}
			catch(Exception e)
			{
				log.Error("Failed to move uploaded file to content server, asset_id = " + _asset.AssetId, e);
				retVal = (int) Constants.eASSET_PROCESSOR_RETURNCODES.ERROR_MOVING_FILES;
			}

			return retVal;
		}
		
        public Shell32.Folder GetShell32NameSpaceFolder(Object folder)
        {
            Type shellAppType = Type.GetTypeFromProgID("Shell.Application");

            Object shell = Activator.CreateInstance(shellAppType);
            return (Shell32.Folder)shellAppType.InvokeMember("NameSpace",
            System.Reflection.BindingFlags.InvokeMethod, null, shell, new object[] { folder });
        }

		private ulong CheckMP3Duration(string assetPath)
		{
			ulong duration_seconds = 0;

			try
			{
				FileInfo fi = new FileInfo(assetPath);

				if ( fi.Extension != null && (string.Compare(".mp3", fi.Extension, true) == 0) )
				{
					log.Info("determining mp3 duration for " + assetPath);

					// OLD way to determine MP3 duration. Commented out and re-implemented
					// 08/21/12 Brett. Call COM object at run time. There were changes to the
					// Shell32 interface making XP/Vista/7 early binding incompatible.
					// 
					// Shell32.Shell mp3info = new Shell32.ShellClass();
					// Shell32.Folder dir = mp3info.NameSpace(fi.Directory.FullName);
					// Shell32.FolderItem mediafile = dir.ParseName(fi.Name);

                    Shell32.Folder dir = GetShell32NameSpaceFolder(fi.Directory.FullName);
                    Shell32.FolderItem mediafile = dir.ParseName(fi.Name);


					// "FILENAME" :0x0,  //0
					// "SIZE"     :0x1,  //1
					// "TITLE"    :0xA,  //10
					// "COMMENT"  :0xE,  //14
					// "INTERPRET":0x10, //16
					// "ALBUM"    :0x11, //17
					// "YEAR"     :0x12, //18
					// "TRACK"    :0x13, //19
					// "GENRE"    :0x14, //20
					// "LENGTH"   :0x15, //21
					// "BITRATE"  :0x16  //22

					string length = dir.GetDetailsOf(mediafile, 21);

					// length returned as a string in format of hour(s):minute(s):second(s)

					string [] length_details;

					length_details = length.Split(':');

					if ( length_details.Length == 3)
					{ 
						duration_seconds = 60 * 60 * UInt32.Parse(length_details[0]);
						duration_seconds += 60 * UInt32.Parse(length_details[1]);
						duration_seconds += UInt32.Parse(length_details[2]);
					}
					else if ( length_details.Length == 2)
					{
						duration_seconds = 60 * UInt32.Parse(length_details[0]);
						duration_seconds += UInt32.Parse(length_details[1]);
					}
					else if ( length_details.Length == 1)
					{
						duration_seconds = UInt32.Parse(length_details[0]);
					}

					log.Info("mp3 duration for " + assetPath + " is " + duration_seconds + " seconds");
				}
			}
			catch (Exception exc)
			{
				log.Error("Exception checking mp3 duration", exc);
			}

			return duration_seconds;
		} 

		/// <summary>
		/// move file to content server
		/// </summary>
		private int MoveTorrentContentFile()
		{
			int retVal = (int) Constants.eASSET_PROCESSOR_RETURNCODES.SUCCESS;
			//saving file to content server
			string contentPath = null;
			int result = AZUREUS.GetNextAzureusContentPath(ref contentPath, ref _clientNumber);
			if(result == (int) KlausEnt.KEP.Kaneva.Constants.eDS_RETURNCODES.SUCCESS)
			{
				try
				{
					FileStream fileStream = new FileStream(_asset.GetFilePath(), FileMode.Open, FileAccess.Read);
					_contentPath = Path.Combine(Path.Combine(contentPath, _asset.UserId.ToString()),
						_asset.AssetId.ToString());
					string targetPath = Path.Combine(_contentPath, _asset.FileName);
					IOUtils.SaveFile(fileStream, targetPath);
					if(log.IsInfoEnabled)
					{
						log.Info("asset " + _asset.AssetId + " saved to " + targetPath);
					}
				}
				catch(Exception e)
				{
					log.Error("Failed to move uploaded file to content server, asset_id = " + _asset.AssetId, e);
					retVal = (int) Constants.eASSET_PROCESSOR_RETURNCODES.ERROR_MOVING_FILES;
				}
			}else
			{
				log.Error("Failed to move uploaded file to content server, unable to get azureus content path");
				retVal = (int) Constants.eASSET_PROCESSOR_RETURNCODES.ERROR_MOVING_FILES;
			}

			return retVal;
		}

		/// <summary>
		/// delete uploaded file
		/// </summary>
		private int DeleteUploadedFile()
		{
			int ret = (int) Constants.eASSET_PROCESSOR_RETURNCODES.SUCCESS;
			try
			{
				string path = _asset.GetFilePath();
				FileInfo fi = new FileInfo(path);
				DirectoryInfo assetDir = fi.Directory;
				DirectoryInfo userDir = assetDir.Parent;

				fi.Delete(); 
				if(log.IsInfoEnabled)
				{
					log.Info("deleted uploaded file " + path);
				}
				if(assetDir.GetFileSystemInfos().Length  == 0)
				{
					assetDir.Delete(); //not to do recursively deletion just in case the folder contains other data, although it shouldn't
					//delete user dir if it's empty
					if(userDir.GetFileSystemInfos().Length  == 0)
					{
						userDir.Delete();
					}
				}
				else
				{
					//this is not supposed to happen with current setup
					log.Warn("Path " + path + " contains other files/folder after deletion uploaded file, asset " + _asset.AssetId);
				}

			}catch(Exception e)
			{
				log.Error("Failed to delete uploaded file", e);
				ret = (int) Constants.eASSET_PROCESSOR_RETURNCODES.ERROR_DELETEING_UPLOADED_FILE;
			}
			return ret;
		}

		public Asset Asset
		{
			get { return _asset; }
			set { _asset = value; }
		}
	}
}
