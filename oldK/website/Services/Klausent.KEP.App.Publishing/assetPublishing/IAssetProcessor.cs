///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace KlausEnt.KEP.AssetPublishing
{
	/// <summary>
	/// Summary description for IAssetProcessor.
	/// </summary>
	public interface IAssetProcessor
	{
		void Process(Asset asset);
		void ProcessAsync(Asset asset, ref SignalResult accepted);
		//other methods like Cancel() etc
	}
}
