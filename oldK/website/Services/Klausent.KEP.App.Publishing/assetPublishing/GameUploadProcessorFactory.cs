///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using KlausEnt.KEP.AssetPublishing;
using KlausEnt.KEP.Kaneva;

namespace KlausEnt.KEP.GameUploadPublishing
{
	/// <summary>
	/// Summary description for GameUploadProcessorFactory.
	/// </summary>
	public class GameUploadProcessorFactory
	{
		public GameUploadProcessorFactory()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static IGameUploadProcessor GetProcessor(GameUpload torrent)
		{
			IGameUploadProcessor retVal = null;
			switch(torrent.PublishStatusId)
			{
				case (int) Constants.eGAME_UPLOAD_PUBLISH_STATUS.UPLOADED:
					retVal = new PostGameUploadedProcessor();
					break;
				default:
					retVal = null;
					break;
			}

			return retVal;
		}
	}
}
