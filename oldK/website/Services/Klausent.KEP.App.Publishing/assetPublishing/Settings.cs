///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace KlausEnt.KEP.AssetPublishing
{
	/// <summary>
	/// Summary description for Settings.
	/// </summary>
	public class Settings
	{
		private string _publishAnnounceUrl;
		private string _torrentCreator;
		private static Settings instance;
		private string _importTorrentExt;
		private string _importTorrentPrefix;
		private string _gameUploadedEmailFrom;
		private string _gameUploadedEmailITTo;
		private string _httpContentPath;
		private int _minTorrentFileSize;
		private string _flvConversionExtensions;
		private string _flvConfigFile;
		private int _flvCompressMode;
		private int _flixLogEnabled;
		private string _flixLogFile;
		private int _flixThumbnailNum;

		private int _mediaConverterReceiveTimeout;
		private int	_mediaConverterSendTimeout;

		private string _publishConversionBehindEmailFrom;
		private string _publishConversionBehindEmailTo;
		private int _publishConversionBehindSpread;

		
		private Settings()
		{
			//
			// TODO: Add constructor logic here
			//
			_publishAnnounceUrl = 
				System.Configuration.ConfigurationSettings.AppSettings ["PublishAnnounceUrl"];
			_torrentCreator = 
				System.Configuration.ConfigurationSettings.AppSettings ["KanevaTorrentCreator"];
			_importTorrentExt = 
				System.Configuration.ConfigurationSettings.AppSettings ["ImportTorrentExt"];
			_importTorrentPrefix = 
				System.Configuration.ConfigurationSettings.AppSettings ["ImportTorrentPrefix"];
			_gameUploadedEmailFrom = 
				System.Configuration.ConfigurationSettings.AppSettings ["GameUploadedEmailFrom"];
			_gameUploadedEmailITTo = 
				System.Configuration.ConfigurationSettings.AppSettings ["GameUploadedEmailITTo"];
			
			_httpContentPath = 
				System.Configuration.ConfigurationSettings.AppSettings ["http_content_path"];
			
			_minTorrentFileSize = 
				Int32.Parse(System.Configuration.ConfigurationSettings.AppSettings [
					"minTorrentFileSize"]);

			_flvConversionExtensions =
				System.Configuration.ConfigurationSettings.AppSettings ["FLVExtensionsToConvert"];

			_flvConfigFile =
				System.Configuration.ConfigurationSettings.AppSettings ["FLVConfigFile"];

			_flvCompressMode = 
				Int32.Parse(System.Configuration.ConfigurationSettings.AppSettings [
				"FLVCompressMode"]);

			_flixLogEnabled = 
				Int32.Parse(System.Configuration.ConfigurationSettings.AppSettings [
				"FLIXLogEnabled"]);

			_flixLogFile =
				System.Configuration.ConfigurationSettings.AppSettings ["FLIXLogFile"];

			_flixThumbnailNum = 
				Int32.Parse(System.Configuration.ConfigurationSettings.AppSettings [
				"FLVThumbnailNum"]);

			_mediaConverterReceiveTimeout = Int32.Parse(System.Configuration.ConfigurationSettings.AppSettings [
				"MediaConverterReceiveTimeout"]);

			_mediaConverterSendTimeout = Int32.Parse(System.Configuration.ConfigurationSettings.AppSettings [
				"MediaConverterSendTimeout"]);

			_publishConversionBehindEmailFrom = 
				System.Configuration.ConfigurationSettings.AppSettings ["PublishConversionBehindEmailFrom"];
			_publishConversionBehindEmailTo = 
				System.Configuration.ConfigurationSettings.AppSettings ["PublishConversionBehindEmailTo"];
			_publishConversionBehindSpread = 
				Int32.Parse(System.Configuration.ConfigurationSettings.AppSettings ["PublishConversionBehindSpread"]);
		}

		public static Settings GetInstance()
		{
			if(instance == null)
			{
				instance = new Settings() ;
			}

			return instance;
		}

		public string TorrentCreator
		{
			get { return _torrentCreator; }
		}

		/// <summary>
		/// tracker used for torrent publishing
		/// </summary>
		public string PublishAnnounceUrl
		{
			get { return _publishAnnounceUrl; }
		}

		/// <summary>
		/// extension used by a import torrrent file
		/// </summary>
		public string ImportTorrentExt
		{
			get { return _importTorrentExt; }
		}

		/// <summary>
		/// filename prefix used by a import torrrent file
		/// </summary>
		public string ImportTorrentPrefix
		{
			get { return _importTorrentPrefix; }
		}

		public string GameUploadedEmailFrom
		{
			get { return _gameUploadedEmailFrom; }
			set { _gameUploadedEmailFrom = value; }
		}

		public string GameUploadedEmailItTo
		{
			get { return _gameUploadedEmailITTo; }
			set { _gameUploadedEmailITTo = value; }
		}

		public string HttpContentPath
		{
			get { return _httpContentPath; }
			set { _httpContentPath = value; }
		}

		/// <summary>
		/// a file has to be larger then this value, in MB, for a torrent to be created
		/// </summary>
		public int MinTorrentFileSize
		{
			get { return _minTorrentFileSize; }
			set { _minTorrentFileSize = value; }
		}

		public string FlvConversionExtensions
		{
			get { return _flvConversionExtensions; }
			set { _flvConversionExtensions = value; }
		}

		public string FlvConfigFile
		{
			get { return _flvConfigFile; }
			set { _flvConfigFile = value; }
		}

		public int FlvCompressMode
		{
			get { return _flvCompressMode; }
			set { _flvCompressMode = value; }
		}

		public int FlixLogEnabled
		{
			get { return _flixLogEnabled; }
			set { _flixLogEnabled = value; }
		}

		public string FlixLogFile
		{
			get { return _flixLogFile; }
			set { _flixLogFile = value; }
		}

		public int FLVThumbNailNum
		{
			get { return _flixThumbnailNum; }
			set { _flixThumbnailNum = value; }
		}

		public int MediaConverterReceiveTimeout
		{
			get { return _mediaConverterReceiveTimeout; }
			set { _mediaConverterReceiveTimeout = value; }
		}

		public int MediaConverterSendTimeout
		{
			get { return _mediaConverterSendTimeout; }
			set { _mediaConverterSendTimeout = value; }
		}

		public string PublishConversionBehindEmailFrom
		{
			get { return _publishConversionBehindEmailFrom; }
			set { _publishConversionBehindEmailFrom = value; }
		}

		public string PublishConversionBehindEmailTo
		{
			get { return _publishConversionBehindEmailTo; }
			set { _publishConversionBehindEmailTo = value; }
		}

		public int PublishConversionBehindSpread
		{
			get { return _publishConversionBehindSpread; }
			set { _publishConversionBehindSpread = value; }
		}

	}
}
