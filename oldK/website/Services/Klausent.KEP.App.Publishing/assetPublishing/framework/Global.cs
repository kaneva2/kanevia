///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Security.Principal;
using System.Web;

namespace KlausEnt.KEP.Kaneva 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		private static DatabaseUtility m_DatabaseUtilty;

		public static string Version()
		{
			return "";
		}

		/// <summary>
		/// Get the database utility
		/// </summary>
		/// <returns></returns>
		public static DatabaseUtility GetDatabaseUtility ()
		{
			if (m_DatabaseUtilty == null)
			{
				switch (KanevaGlobals.DatabaseType)
				{
					case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
					{
						m_DatabaseUtilty = new SQLServerUtility (KanevaGlobals.ConnectionString);
						break;
					}
					case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
					{
						m_DatabaseUtilty = new MySQLUtility (KanevaGlobals.ConnectionString);
						break;
					}
					default:
					{
						throw new Exception ("This database type is not supported");
					}
				}
			}
			return m_DatabaseUtilty;
		}

		public Global()
		{
			InitializeComponent();
		}	

		public static System.Web.Caching.Cache Cache()
		{
			//A HACK TO GET IT TO COMPILE
			return null;
		}
		
		protected void Application_Start(Object sender, EventArgs e)
		{
			
		}
 
		protected void Session_Start(Object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{
			if (Request.IsAuthenticated)
			{
				HttpContext.Current.User = new GenericPrincipal (User.Identity, UsersUtility.GetUserRoles (User.Identity.Name));
			}
		}

		protected void Application_Error(Object sender, EventArgs e)
		{
			
		}

		protected void Session_End(Object sender, EventArgs e)
		{

		}

		protected void Application_End(Object sender, EventArgs e)
		{

		}
			
		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
		}
		#endregion
	}
}


