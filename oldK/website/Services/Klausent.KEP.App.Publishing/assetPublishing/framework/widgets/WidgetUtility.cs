///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Text;

namespace KlausEnt.KEP.Kaneva.framework.widgets
{
	/// <summary>
	/// Summary description for WidgetUtility.
	/// </summary>
	public class WidgetUtility
	{
		/// <summary>
		/// set the last_update column in communities table to be current date,
		/// note that <c>modulePageId</c> is the module_page_id in layout_page_modules 
		/// table
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <param name="moduleId"></param>
		public static void LayoutPageModuleUpdated(int modulePageId, int moduleId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			StringBuilder sb = new StringBuilder();
			sb.Append(" SELECT lp.channel_id ");
			sb.Append(" FROM layout_pages lp ");
			sb.Append(" INNER JOIN layout_page_modules lpm ON lpm.page_id = lp.page_id ");
			sb.Append(" WHERE lpm.module_id = @module_id AND module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@module_id", moduleId);
			parameters.Add ("@module_page_id", modulePageId);

			int channelId = dbUtility.ExecuteScalar(sb.ToString(), parameters);

			sb = new StringBuilder();
			sb.Append(" UPDATE communities ");
			sb.Append(" SET last_update = ").Append(dbUtility.GetCurrentDateFunction());
			sb.Append(" WHERE community_id = @channelId ");

			parameters = new Hashtable ();
			parameters.Add ("@channelId", channelId);

			dbUtility.ExecuteNonQuery (sb.ToString(), parameters);	
		}

		/// <summary>
		/// set the last_update column in communities table to be current date,
		/// note that <c>id</c> is the id column in layout_page_modules 
		/// table
		/// </summary>
		/// <param name="id"></param>
		public static void ModuleDeleted(int id)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			StringBuilder sb = new StringBuilder();
			sb.Append(" SELECT lp.channel_id ");
			sb.Append(" FROM layout_pages lp ");
			sb.Append(" INNER JOIN layout_page_modules lpm ON lpm.page_id = lp.page_id ");
			sb.Append(" WHERE lpm.id = @id ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@id", id);

			int channelId = dbUtility.ExecuteScalar(sb.ToString(), parameters);

			sb = new StringBuilder();
			sb.Append(" UPDATE communities ");
			sb.Append(" SET last_update = ").Append(dbUtility.GetCurrentDateFunction());
			sb.Append(" WHERE community_id = @channelId ");

			parameters = new Hashtable ();
			parameters.Add ("@channelId", channelId);

			dbUtility.ExecuteNonQuery (sb.ToString(), parameters);
		}
	
		/// <summary>
		/// get pages for a user
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static DataTable GetPages(int userId)
		{
			string sqlSelect = " SELECT page_id, name " +
				" FROM layout_pages " +
				" WHERE user_id = @user_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@user_id", userId);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataTable(sqlSelect, parameters);
		}

		/// <summary>
		/// get pages for a channel
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static DataTable GetChannelPages(int channelId)
		{
			string sqlSelect = " SELECT page_id, name " +
				" FROM layout_pages " +
				" WHERE channel_id = @channel_id " + 
				" ORDER BY sequence ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channel_id", channelId);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataTable(sqlSelect, parameters);
		}
		

		/// <summary>
		/// get the a layout_page_module row
		/// </summary>
		/// <param name="id">id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetLayoutPageModule(int id)
		{
			string sqlSelect = " SELECT module_page_id, module_id " +
				" FROM layout_page_modules " +
				" WHERE id = @id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// return true if this is a personal page, false if its channel page
		/// </summary>
		/// <param name="pageId"></param>
		/// <returns></returns>
		public static bool IsPersonalPage(int pageId)
		{
			string sqlSelect = " SELECT user_id FROM layout_pages WHERE page_id = @page_id";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@page_id", pageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row["user_id"] != DBNull.Value && Convert.ToInt32(row["user_id"]) > 0;
		}

		#region title
		/// <summary>
		/// returns the first title widget in the homepage, ifthere is one
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static DataRow GetDefaultModuleTitle(int channelId)
		{
			StringBuilder sb = new StringBuilder() ; 
			sb.Append(" SELECT distinct t.text, t.show_menu, t.module_page_id, t.banner_path, ");
			sb.Append(" m.page_id, lp.page_id, lp.channel_id, m.sequence ");
			sb.Append(" FROM layout_module_title_text t ");
			sb.Append(" INNER JOIN layout_page_modules m ON m.module_page_id = t.module_page_id ");
			sb.Append(" INNER JOIN layout_pages lp ON lp.page_id = m.page_id ");
			sb.Append(" WHERE lp.channel_id = @channelId AND lp.home_page = 1 ");
			sb.Append(" ORDER BY m.sequence LIMIT 1 ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channelId", channelId);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sb.ToString(), parameters, false);
		}
		/// <summary>
		/// Get a title widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleTitle(int id)
		{
			StringBuilder sb = new StringBuilder() ; 
			sb.Append(" SELECT t.text, t.show_menu, t.module_page_id, banner_path, ");
			sb.Append(" m.page_id, lp.page_id, lp.channel_id ");
			sb.Append(" FROM layout_module_title_text t");
			sb.Append(" INNER JOIN layout_page_modules m ON m.module_page_id = t.module_page_id ");
			sb.Append(" INNER JOIN layout_pages lp ON lp.page_id = m.page_id ");
			sb.Append(" WHERE m.id = @id ");
			sb.Append(" AND m.module_id = @moduleType ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.TITLE_TEXT);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sb.ToString(), parameters, false);
		}

		/// <summary>
		/// load a title row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleTitle(int modulePageId)
		{
			string sqlSelect = " SELECT text, show_menu, banner_path " +
							" FROM layout_module_title_text " +
							" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// insert a record to layout_module_title_text table and return the module_page_id
		/// </summary>
		/// <param name="text"></param>
		/// <param name="show_menu"></param>
		/// <returns></returns>
		public static int InsertLayoutModuleTitle(string text, bool show_menu, string bannerPath)
		{
			string query = " INSERT INTO layout_module_title_text " + 
						" (text, show_menu, banner_path) " +
						" VALUES(@text, @show_menu, @bannerPath) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@text", text);
			parameters.Add ("@show_menu", show_menu ? 1 : 0);
			parameters.Add ("@bannerPath", bannerPath);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.TITLE_TEXT);
			return retVal;
		}

		/// <summary>
		/// SaveLayoutModuleTitle
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <param name="text"></param>
		/// <param name="show_menu"></param>
		/// <param name="bannerPath"></param>
		public static void SaveLayoutModuleTitle (int modulePageId, string text, bool show_menu, bool removeBanner)
		{
			Hashtable parameters = new Hashtable ();			

			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_title_text ");
			sb.Append(" SET text = @text, ");
			sb.Append(" show_menu = @show_menu ");
			sb.Append(" WHERE module_page_id = @module_page_id ");
			
			parameters.Add ("@text", text);
			parameters.Add ("@show_menu", show_menu ? 1 : 0);
			parameters.Add ("@module_page_id", modulePageId);
			
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.TITLE_TEXT);
		}

		/// <summary>
		/// delete a title widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleTitle (int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_title_text table
			string sqlString = "DELETE FROM layout_module_title_text " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		/// <summary>
		/// return's the default title text for a page
		/// </summary>
		/// <param name="pageId"></param>
		/// <returns></returns>
		public static string GetDefaultTitle(int pageId)
		{
			DataRow drUser = UsersUtility.GetUserByPageId(pageId);
			string retVal = null;
			if(drUser != null)
			{
				retVal = drUser["username"].ToString();
			}

			DataRow drChannel = PageUtility.GetChannelByPageId(pageId);
			if(drChannel != null)
			{
				retVal = drChannel["name"].ToString();
			}
			return retVal;
		}

		/// <summary>
		/// Update a widget banner
		/// </summary>
		public static int UpdateChannelBanner (int modulePageId, string bannerPath, string dbTable)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			
			string sqlString = "UPDATE " + dbTable +
				" SET " +
				" banner_path = @bannerPath " +
				" WHERE module_page_id = @modulePageId";

			Hashtable parameters = new Hashtable ();

			if (bannerPath.Trim ().Length.Equals (0))
			{
				parameters.Add ("@bannerPath", "");
			}
			else
			{
				parameters.Add ("@bannerPath", bannerPath);
			}

			parameters.Add ("@modulePageId", modulePageId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
			return 0;
		}

		#endregion

		#region Profile

		/// <summary>
		/// insert a record to layout_module_profile table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleProfile(string title)
		{
			string query = " INSERT INTO layout_module_profile " + 
				" (title) " +
				" VALUES(@title) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			int retVal = -1;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.PROFILE);
			return retVal;
		}

		/// <summary>
		/// delete a profile widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleProfile(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_profile table
			string sqlString = "DELETE FROM layout_module_profile " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		/// <summary>
		/// Get a Profile widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleProfile(int id)
		{
			string sqlSelect = " SELECT p.title, m.page_id " +
				" FROM layout_module_profile p" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = p.module_page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.PROFILE);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a Profile row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleProfile(int modulePageId)
		{
			string sqlSelect = " SELECT title " +
				" FROM layout_module_profile " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// save a Profile widget
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static void SaveLayoutModuleProfile(int modulePageId, string title)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_profile ");
			sb.Append(" SET title = @title ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@module_page_id", modulePageId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.PROFILE);
		}

		#endregion

        #region Interests

        /// <summary>
        /// insert a record to layout_module_interests table and return the module_page_id
        /// </summary>
        /// <returns></returns>
        public static int InsertLayoutModuleInterests(string title)
        {
            string query = " INSERT INTO layout_module_interests " +
                " (title) " +
                " VALUES(@title) ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@title", title);
            int retVal = -1;
            KanevaGlobals.GetDatabaseUtility().ExecuteIdentityInsert(query, parameters, ref retVal);

            LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.INTERESTS);
            return retVal;
        }

        /// <summary>
        /// delete an interest widget
        /// </summary>
        /// <param name="id">the id column in layout_page_modules table</param>
        /// <returns></returns>
        public static void DeleteModuleInterests(int id)
        {
            ModuleDeleted(id);

            //delete from layout_module_profile table
            string sqlString = "DELETE FROM layout_module_interests " +
                " WHERE module_page_id IN " +
                " ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@id", id);
            KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sqlString, parameters);

            //delete from layout_page_modules table
            sqlString = "DELETE FROM layout_page_modules where id = @id";

            parameters = new Hashtable();
            parameters.Add("@id", id);
            KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sqlString, parameters);


        }

        /// <summary>
        /// Get a Interests widget data
        /// </summary>
        /// <param name="id">the id column in layout_page_modules table</param>
        /// <returns></returns>
        public static DataRow GetModuleInterests(int id)
        {
            string sqlSelect = " SELECT i.title, m.page_id " +
                " FROM layout_module_interests i" +
                " INNER JOIN layout_page_modules m ON m.module_page_id = i.module_page_id " +
                " WHERE m.id = @id " +
                " AND m.module_id = @moduleType ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@id", id);
            parameters.Add("@moduleType", (int)Constants.eMODULE_TYPE.INTERESTS);
            return KanevaGlobals.GetDatabaseUtility().GetDataRow(sqlSelect, parameters, false);
        }

        /// <summary>
        /// load a Profile row
        /// </summary>
        /// <param name="modulePageId"></param>
        /// <returns></returns>
        public static DataRow GetLayoutModuleInterests(int modulePageId)
        {
            string sqlSelect = " SELECT title " +
                " FROM layout_module_interests " +
                " WHERE module_page_id = @module_page_id ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@module_page_id", modulePageId);
            DataRow row = KanevaGlobals.GetDatabaseUtility().GetDataRow(sqlSelect, parameters, false);
            return row;
        }

        /// <summary>
        /// save a Interests widget
        /// </summary>
        /// <param name="modulePageId"></param>
        /// <returns></returns>
        public static void SaveLayoutModuleInterests(int modulePageId, string title)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" UPDATE layout_module_interests ");
            sb.Append(" SET title = @title ");
            sb.Append(" WHERE module_page_id = @module_page_id ");

            Hashtable parameters = new Hashtable();
            parameters.Add("@title", title);
            parameters.Add("@module_page_id", modulePageId);
            KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sb.ToString(), parameters);

            LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.INTERESTS);
        }

        #endregion

        #region Personal

        /// <summary>
        /// insert a record to layout_module_interests table and return the module_page_id
        /// </summary>
        /// <returns></returns>
        public static int InsertLayoutModulePersonal (string title)
        {
            string query = " INSERT INTO layout_module_personal " +
                " (title) " +
                " VALUES(@title) ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@title", title);
            int retVal = -1;
            KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert (query, parameters, ref retVal);

            LayoutPageModuleUpdated (retVal, (int) Constants.eMODULE_TYPE.PERSONAL);
            return retVal;
        }

        /// <summary>
        /// delete a personal widget
        /// </summary>
        /// <param name="id">the id column in layout_page_modules table</param>
        /// <returns></returns>
        public static void DeleteModulePersonal (int id)
        {
            ModuleDeleted (id);

            //delete from layout_module_profile table
            string sqlString = "DELETE FROM layout_module_personal " +
                " WHERE module_page_id IN " +
                " ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@id", id);
            KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

            //delete from layout_page_modules table
            sqlString = "DELETE FROM layout_page_modules where id = @id";

            parameters = new Hashtable ();
            parameters.Add ("@id", id);
            KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// Get a Interests widget data
        /// </summary>
        /// <param name="id">the id column in layout_page_modules table</param>
        /// <returns></returns>
        public static DataRow GetModulePersonal (int id)
        {
            string sqlSelect = " SELECT i.title, m.page_id " +
                " FROM layout_module_personal i" +
                " INNER JOIN layout_page_modules m ON m.module_page_id = i.module_page_id " +
                " WHERE m.id = @id " +
                " AND m.module_id = @moduleType ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@id", id);
            parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.PERSONAL);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// load a Personal row
        /// </summary>
        /// <param name="modulePageId"></param>
        /// <returns></returns>
        public static DataRow GetLayoutModulePersonal (int modulePageId)
        {
            string sqlSelect = " SELECT title " +
                " FROM layout_module_personal " +
                " WHERE module_page_id = @module_page_id ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@module_page_id", modulePageId);
            DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
            return row;
        }

        /// <summary>
        /// save a Personal widget
        /// </summary>
        /// <param name="modulePageId"></param>
        /// <returns></returns>
        public static void SaveLayoutModulePersonal (int modulePageId, string title)
        {
            StringBuilder sb = new StringBuilder ();
            sb.Append (" UPDATE layout_module_personal ");
            sb.Append (" SET title = @title ");
            sb.Append (" WHERE module_page_id = @module_page_id ");

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@title", title);
            parameters.Add ("@module_page_id", modulePageId);
            KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sb.ToString (), parameters);

            LayoutPageModuleUpdated (modulePageId, (int) Constants.eMODULE_TYPE.PERSONAL);
        }

        #endregion

        #region blogs

		/// <summary>
		/// Get a blogs widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleBlogs(int id)
		{
			string sqlSelect = " SELECT b.title, b.entries_per_page, b.show_dates, b.tags, m.page_id, lp.channel_id " +
				" FROM layout_module_blogs b" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = b.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.BLOGS);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a blogs row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleBlogs(int modulePageId)
		{
			string sqlSelect = " SELECT title, entries_per_page, show_dates, tags  " +
				" FROM layout_module_blogs " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// insert a record with default values to layout_module_blogs table and 
		/// return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleBlogs(string title)
		{
			return InsertLayoutModuleBlogs(title, Constants.DEFAULT_BLOG_ENTRIES_PER_PAGE,
				Constants.DEFAULT_BLOG_ENTRIES_SHOW_DATE, "");
		}
		
		/// <summary>
		/// insert a record to layout_module_blogs table and return the module_page_id
		/// </summary>
		/// <param name="title"></param>
		/// <param name="entriesPerPage"></param>
		/// <param name="showDates"></param>
		/// <returns></returns>
		public static int InsertLayoutModuleBlogs(string title, int entriesPerPage, bool showDates, string tags)
		{
			string query = " INSERT INTO layout_module_blogs " + 
				" (title, entries_per_page, show_dates, tags) " +
				" VALUES(@title, @entries_per_page, @show_dates, @tags) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@entries_per_page", entriesPerPage);
			parameters.Add ("@show_dates", showDates ? 1 : 0);
			parameters.Add ("@tags", tags);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.BLOGS);
			return retVal;
		}

		public static void SaveLayoutModuleBlogs(int modulePageId, string title, 
			int entriesPerPage, bool showDates, string tags)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_blogs ");
			sb.Append(" SET title = @title, ");
			sb.Append(" entries_per_page = @entries_per_page, ");
			sb.Append(" tags = @tags, ");
			sb.Append(" show_dates = @show_dates ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@entries_per_page", entriesPerPage);
			parameters.Add ("@show_dates", showDates ? 1 : 0);
			parameters.Add ("@tags", tags);
			parameters.Add ("@module_page_id", modulePageId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.BLOGS);
		}

		/// <summary>
		/// delete a title widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleBlogs(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_blogs table
			string sqlString = "DELETE FROM layout_module_blogs " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}
		#endregion 

		#region Friends

		/// <summary>
		/// insert a default record to layout_module_friends table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleFriends(string title)
		{
			return InsertLayoutModuleFriends(title, 10 );
		}

		/// <summary>
		/// insert a record to layout_module_friends table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleFriends( string title, int people_per_page )
		{
			string query = " INSERT INTO layout_module_friends " + 
				" (title, people_per_page) " +
				" VALUES(@title, @people_per_page) ";

			Hashtable parameters = new Hashtable ();	
		
			if (title != "")
				parameters.Add ("@title", title );
			else
				parameters.Add ("@title", DBNull.Value );

			parameters.Add ("@people_per_page", people_per_page );
			int retVal = -1;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.FRIENDS);
			return retVal;
		}

		/// <summary>
		/// delete a friends widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleFriends(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_friends table
			string sqlString = "DELETE FROM layout_module_friends " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		/// <summary>
		/// Get a friends widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleFriends(int id)
		{
			string sqlSelect = " SELECT f.title, f.people_per_page, f.friend_group_id, m.page_id " +
				" FROM layout_module_friends f" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = f.module_page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.FRIENDS);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a Friends row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleFriends(int modulePageId)
		{
			string sqlSelect = " SELECT title, people_per_page, friend_group_id " +
				" FROM layout_module_friends " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// save a Friends widget
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static void SaveLayoutModuleFriends(int modulePageId, string title, int people_per_page, int friend_group_id)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_friends ");
			sb.Append(" SET title = @title, ");
			sb.Append(" people_per_page = @people_per_page, ");
			sb.Append(" friend_group_id = @friend_group_id ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@people_per_page", people_per_page);
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@friend_group_id", friend_group_id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.FRIENDS);
		}

		#endregion

		#region Store

		/// <summary>
		/// insert a default record to layout_module_stores table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleStores (int moduleId, string title)
		{
			return InsertLayoutModuleStores (title, 10, -1, moduleId);
		}

		/// <summary>
		/// insert a record to layout_module_stores table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleStores ( string title, int itemsPerPage, int groupId, int moduleId)
		{
			string query = " INSERT INTO layout_module_stores " + 
				" (title,items_per_page,group_id) " +
				" VALUES (@title, @itemsPerPage, @group_id) ";

			Hashtable parameters = new Hashtable ();	
		
			if (title != "")
				parameters.Add ("@title", title );
			else
				parameters.Add ("@title", DBNull.Value );

			if ( groupId != -1 )
				parameters.Add ("@group_id", groupId);
			else
				parameters.Add ("@group_id", DBNull.Value);

			parameters.Add ("@itemsPerPage", itemsPerPage  );
			int retVal = -1;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated (retVal, moduleId);
			return retVal;
		}

		/// <summary>
		/// delete a stores widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleStores (int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_friends table
			string sqlString = "DELETE FROM layout_module_stores " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		/// <summary>
		/// Get a stores widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleStores (int id)
		{
			string sqlSelect = " SELECT s.title, s.items_per_page, m.page_id, s.group_id, lp.channel_id " +
				" FROM layout_module_stores s" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = s.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" WHERE m.id = @id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a stores row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleStores (int modulePageId)
		{
			string sqlSelect = " SELECT title, items_per_page, group_id " +
				" FROM layout_module_stores " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// save a store widget
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static void SaveLayoutModuleStore (int modulePageId, string title, int items_per_page, int group_id, int moduleId)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_stores ");
			sb.Append(" SET title = @title, ");
			sb.Append(" group_id = @group_id, ");
			sb.Append(" items_per_page = @items_per_page ");
			sb.Append(" WHERE module_page_id = @module_page_id ");


			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@items_per_page", items_per_page);
			parameters.Add ("@module_page_id", modulePageId);

			if ( group_id != -1 )
				parameters.Add ("@group_id", group_id);
			else
				parameters.Add ("@group_id", DBNull.Value);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated (modulePageId, moduleId);
		}

		#endregion

		#region Channels
		/// <summary>
		/// Get a title widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleChannels(int id)
		{
			string sqlSelect = " SELECT c.title, c.entries_per_page, c.filter, m.page_id " +
				" FROM layout_module_channels c" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = c.module_page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.CHANNELS);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a title row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleChannels(int modulePageId)
		{
			string sqlSelect = " SELECT title, entries_per_page, filter " +
				" FROM layout_module_channels " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}
		
		/// <summary>
		/// insert a record with default values to layout_module_channels table and 
		/// return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleChannels(string title)
		{
			return InsertLayoutModuleChannels(title, Constants.DEFAULT_CHANNELS_ENTRIES_PER_PAGE, (int) Constants.DEFAULT_CHANNELS_FILTER);
		}

		/// <summary>
		/// insert a record to layout_module_channels table and return the module_page_id
		/// </summary>
		/// <param name="title"></param>
		/// <param name="entries_per_page"></param>
		/// <returns></returns>
		public static int InsertLayoutModuleChannels(string title, int entries_per_page, int filter)
		{
			string query = " INSERT INTO layout_module_channels " + 
				" (title, entries_per_page, filter) " +
				" VALUES(@title, @entries_per_page, @filter) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@entries_per_page", entries_per_page);
			parameters.Add ("@filter", filter);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.CHANNELS);
			return retVal;
		}

		public static void SaveLayoutModuleChannels(int modulePageId, string title, int entries_per_page, int filter)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_channels ");
			sb.Append(" SET title = @title, ");
			sb.Append(" entries_per_page = @entries_per_page, ");
			sb.Append(" filter = @filter ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();	
			parameters.Add ("@title", title);
			parameters.Add ("@entries_per_page", entries_per_page);
			parameters.Add ("@filter", filter);
			parameters.Add ("@module_page_id", modulePageId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.CHANNELS);
		}

		/// <summary>
		/// delete a title widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleChannels(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_channels table
			string sqlString = "DELETE FROM layout_module_channels " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		#endregion

		#region Menu
		/// <summary>
		/// Get a menu widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleMenu(int id)
		{
			string sqlSelect = " SELECT t.module_page_id, m.page_id, lp.page_id, lp.channel_id " +
				" FROM layout_module_menu t" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = t.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.MENU);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// insert a record to layout_module_menu table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleMenu()
		{
			string query = " INSERT INTO layout_module_menu " + 
				" () " +
				" VALUES() ";

			Hashtable parameters = new Hashtable ();
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.MENU);
			return retVal;
		}

		/// <summary>
		/// delete a title widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleMenu(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_menu table
			string sqlString = "DELETE FROM layout_module_menu " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		#endregion

		#region Comments

		/// <summary>
		/// insert a default record to layout_module_friends table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleComments(string title)
		{
			return InsertLayoutModuleComments( title, Constants.DEFAULT_COMMENT_MAX_PER_PAGE, Constants.DEFAULT_COMMENT_SHOW_DATE_TIME, Constants.DEFAULT_COMMENT_SHOW_PROFILE_PIC, Constants.DEFAULT_COMMENT_AUTHOR_ALIGNMENT, 1, 1);
		}

		/// <summary>
		/// insert a record to layout_module_comments table and return the module_page_id
		/// <param name="title"></param>
		/// <param name="max_comments"></param>
		/// <param name="show_datetime"></param>
		/// <param name="show_profile_pic"></param>
		/// <param name="author_alignment"></param>
		/// <param name="permission_friends"></param>
		/// <param name="permission_everyone"></param>
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleComments( string title, int max_comments, bool show_datetime,
			bool show_profile_pic, char author_alignment, int permission_friends, int permission_everyone)
		{
			string query = " INSERT INTO layout_module_comments " + 
				" (title,  max_comments, show_datetime, show_profile_pic, author_alignment, permission_friends, permission_everyone) " +
				" VALUES(@title, @max_comments, @show_datetime, @show_profile_pic, @author_alignment, @permission_friends, @permission_everyone) ";

			Hashtable parameters = new Hashtable ();	
		
			
			parameters.Add ("@title", title);
			parameters.Add ("@max_comments", max_comments);
			parameters.Add ("@show_datetime", show_datetime ? 1 : 0);
			parameters.Add ("@show_profile_pic", show_profile_pic ? 1 : 0);
			parameters.Add ("@author_alignment", author_alignment);
			parameters.Add ("@permission_friends", permission_friends);
			parameters.Add ("@permission_everyone", permission_everyone);

			int retVal = -1;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.COMMENTS);
			return retVal;
		}

		/// <summary>
		/// delete a comments widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleComments(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_friends table
			string sqlString = "DELETE FROM layout_module_comments " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		/// <summary>
		/// Get a comments widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleComments(int id)
		{
			string sqlSelect = " SELECT c.title, c.max_comments, c.show_datetime, c.show_profile_pic, c.author_alignment, c.permission_friends, c.permission_everyone, m.page_id, lp.channel_id " +
				" FROM layout_module_comments c" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = c.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.COMMENTS);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a Comments row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleComments(int modulePageId)
		{
			string sqlSelect = " SELECT title, max_comments, show_datetime, show_profile_pic, author_alignment, permission_friends, permission_everyone " +
				" FROM layout_module_comments " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// save a Friends widget
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static void SaveLayoutModuleComments(int modulePageId, string title, int max_comments, bool show_datetime,
			bool show_profile_pic, char author_alignment, int permission_friends, int permission_everyone)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_comments ");
			sb.Append(" SET title = @title, ");
			sb.Append(" max_comments = @max_comments, ");
			sb.Append(" show_datetime = @show_datetime, ");
			sb.Append(" show_profile_pic = @show_profile_pic, ");
			sb.Append(" author_alignment = @author_alignment, ");
			sb.Append(" permission_friends = @permission_friends, ");
			sb.Append(" permission_everyone = @permission_everyone ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@max_comments", max_comments);
			parameters.Add ("@show_datetime", show_datetime ? 1 : 0);
			parameters.Add ("@show_profile_pic", show_profile_pic ? 1 : 0);
			parameters.Add ("@author_alignment", author_alignment);
			parameters.Add ("@permission_friends", permission_friends);
			parameters.Add ("@permission_everyone", permission_everyone);
			parameters.Add ("@module_page_id", modulePageId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.COMMENTS);
		}

		#endregion

		#region Html Content
		/// <summary>
		/// Get a title widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleHtml(int id)
		{
			string sqlSelect = " SELECT t.body_text, t.title_text, t.module_page_id, m.page_id " +
				" FROM layout_module_html t" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = t.module_page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.HTML_CONTENT);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a title row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleHtml(int modulePageId)
		{
			string sqlSelect = " SELECT body_text, title_text " +
				" FROM layout_module_html " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}
		
		/// <summary>
		/// insert a record with default values to layout_module_html table and 
		/// return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleHtml(string title)
		{
			return InsertLayoutModuleHtml(title, "");
		}

		/// <summary>
		/// insert a record to layout_module_html table and return the module_page_id
		/// </summary>
		/// <param name="title_txt"></param>
		/// <param name="body_text"></param>
		/// <returns></returns>
		public static int InsertLayoutModuleHtml(string title_txt, string body_text)
		{
			string query = " INSERT INTO layout_module_html " + 
				" (title_text, body_text) " +
				" VALUES(@title_text, @body_text) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title_text", title_txt);
			parameters.Add ("@body_text", body_text);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.HTML_CONTENT);
			return retVal;
		}

		public static void SaveLayoutModuleHtml(int modulePageId, string title_txt, string body_text)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_html ");
			sb.Append(" SET title_text = @title_text, ");
			sb.Append(" body_text = @body_text ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title_text", title_txt);
			parameters.Add ("@body_text", body_text);
			parameters.Add ("@module_page_id", modulePageId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.HTML_CONTENT);
		}

		/// <summary>
		/// delete a title widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleHtml(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_html table
			string sqlString = "DELETE FROM layout_module_html " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		#endregion

		#region System Stats
		/// <summary>
		/// insert a record to layout_module_system_stats table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleSystemStats(string title)
		{
			string query = " INSERT INTO layout_module_system_stats " + 
				" (title) " +
				" VALUES(@title) ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@title", title);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.SYSTEM_STATS);
			return retVal;
		}

		/// <summary>
		/// delete a title widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleSystemStats(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_system_stats table
			string sqlString = "DELETE FROM layout_module_system_stats " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// Get a System Stats widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleSystemStats(int id)
		{
			string sqlSelect = " SELECT ss.title, m.page_id, lp.channel_id, c.is_personal " +
				" FROM layout_module_system_stats ss" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = ss.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" INNER JOIN communities c ON c.community_id = lp.channel_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.SYSTEM_STATS);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a System Stats row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleSystemStats(int modulePageId)
		{
			string sqlSelect = " SELECT title " +
				" FROM layout_module_system_stats " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// save a System Stats widget
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static void SaveLayoutModuleSystemStats(int modulePageId, string title)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_system_stats ");
			sb.Append(" SET title = @title ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@title", title);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.SYSTEM_STATS);
		}

		#endregion

		#region Counter

		/// <summary>
		/// Get a Counter widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleCounter (int id)
		{
			string sqlSelect = " SELECT c.title, c.font_color, c.background_color, c.description, m.page_id, " +
				" c.font_size, c.font_style, lp.channel_id" +
				" FROM layout_module_counter c" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = c.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.MY_COUNTER);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a title row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleCounter(int modulePageId)
		{
			string sqlSelect = " SELECT title, font_color, background_color, font_size, font_style, description " +
				" FROM layout_module_counter " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}
		
		/// <summary>
		/// insert a record with default values to layout_module_counter table and 
		/// return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleCounter(string title)
		{
			return InsertLayoutModuleCounter(title, Constants.DEFAULT_COUNTER_FONT_COLOR,
				Constants.DEFAULT_COUNTER_BG_COLOR,
				Constants.DEFAULT_COUNTER_FONT_SIZE,
				Constants.DEFAULT_COUNTER_FONT_STYLE,
				""
				);
		}
		
		/// <summary>
		/// insert a record to layout_module_counter table and return the module_page_id
		/// </summary>
		/// <param name="title"></param>
		/// <param name="fontColor"></param>
		/// <param name="backgroundColor"></param>
		/// <param name="fontSize"></param>
		/// <param name="fontStyle"></param>
		/// <returns></returns>
		public static int InsertLayoutModuleCounter(string title,
			string fontColor,
			string backgroundColor,
			int fontSize,
			string fontStyle,
			string description)
		{
			string query = " INSERT INTO layout_module_counter " + 
				" (title, font_color, background_color, font_size, font_style, description) " +
				" VALUES(@title, @font_color, @background_color, @font_size, @font_style, @description) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@font_color", fontColor);
			parameters.Add ("@background_color", backgroundColor);
			parameters.Add ("@font_size", fontSize);
			parameters.Add ("@font_style", fontStyle);
			parameters.Add ("@description", description);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.MY_COUNTER);
			return retVal;
		}

		public static void SaveLayoutModuleCounter(int modulePageId, string title,
			string fontColor,
			string backgroundColor,
			int fontSize,
			string fontStyle,
			string description)
		{
			

			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_counter ");
			sb.Append(" SET title = @title, ");
			sb.Append(" font_color = @font_color, ");
			sb.Append(" background_color = @background_color, ");
			sb.Append(" font_size = @font_size, ");
			sb.Append(" font_style = @font_style, ");
			sb.Append(" description = @description ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@font_color", fontColor);
			parameters.Add ("@background_color", backgroundColor);
			parameters.Add ("@font_size", fontSize);
			parameters.Add ("@font_style", fontStyle);
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@description", description);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.MY_COUNTER);
		}

		/// <summary>
		/// delete a counter widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleCounter(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_counter table
			string sqlString = "DELETE FROM layout_module_counter " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		#endregion

		#region My Picture

		//GetModuleMyPicture

		/// <summary>
		/// Get a My Picture widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleMyPicture(int id)
		{
			string sqlSelect = " SELECT mp.title, mp.show_gender, mp.show_location, mp.show_age, m.page_id " +
				" FROM layout_module_my_picture mp" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = mp.module_page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.MY_PICTURE);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a My Picture row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleMyPicture(int modulePageId)
		{
			string sqlSelect = " SELECT title, show_gender, show_location, show_age " +
				" FROM layout_module_my_picture " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}
		
		/// <summary>
		/// insert a record with default values to layout_module_my_picture table and 
		/// return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleMyPicture(string title)
		{
			return InsertLayoutModuleMyPicture(title, Constants.DEFAULT_MY_PICTURE_SHOW_GENDER,
				Constants.DEFAULT_MY_PICTURE_SHOW_LOCATION,
				Constants.DEFAULT_MY_PICTURE_SHOW_AGE
				);
		}
		
		/// <summary>
		/// insert a record to layout_module_my_picture table and return the module_page_id
		/// </summary>
		/// <param name="title"></param>
		/// <param name="show_gender"></param>
		/// <param name="show_location"></param>
		/// <param name="show_age"></param>
		/// <returns></returns>
		public static int InsertLayoutModuleMyPicture(string title,
			bool show_gender,
			bool show_location,
			bool show_age)
		{
			string query = " INSERT INTO layout_module_my_picture " + 
				" (title, show_gender, show_location, show_age) " +
				" VALUES(@title, @show_gender, @show_location, @show_age) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@show_gender", show_gender ? 1 : 0 );
			parameters.Add ("@show_location", show_location ? 1 : 0 );
			parameters.Add ("@show_age", show_age ? 1 : 0 );

			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.MY_PICTURE);
			return retVal;
		}

		public static void SaveLayoutModuleMyPicture(int modulePageId, string title,
			bool show_gender,
			bool show_location,
			bool show_age)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_my_picture ");
			sb.Append(" SET title = @title, ");
			sb.Append(" show_gender = @show_gender, ");
			sb.Append(" show_location = @show_location, ");
			sb.Append(" show_age = @show_age ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@show_gender", show_gender ? 1 : 0 );
			parameters.Add ("@show_location", show_location ? 1 : 0 );
			parameters.Add ("@show_age", show_age ? 1 : 0 );
			parameters.Add ("@module_page_id", modulePageId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.MY_PICTURE);
		}

		/// <summary>
		/// delete a My Picture widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleMyPicture(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_my_picture table
			string sqlString = "DELETE FROM layout_module_my_picture " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		#endregion

		#region New People

		/// <summary>
		/// insert a default record to layout_module_new_people table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleNewPeople(string title)
		{
			return InsertLayoutModuleNewPeople( title, Constants.DEFAULT_NEW_PEOPLE_NUM_SHOWING, Constants.DEFAULT_NEW_PEOPLE_WITH_PIC_ONLY);
		}
		
		/// <summary>
		/// insert a record to layout_module_new_people table and return the module_page_id
		/// </summary>
		/// <param name="title"></param>
		/// <param name="numberShowing">number of people to show</param>
		/// <param name="withPictureOnly">whether only to show people with picture or not</param>
		/// <returns></returns>
		public static int InsertLayoutModuleNewPeople( string title, int numberShowing, bool withPictureOnly)
		{
			string query = " INSERT INTO layout_module_new_people " + 
				" (number_showing, with_picture_only, title) " +
				" VALUES(@number_showing, @with_picture_only, @title) ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@title", title );
			parameters.Add ("@number_showing", numberShowing );
			parameters.Add ("@with_picture_only", withPictureOnly ? 1 : 0 );

			int retVal = -1;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.NEW_PEOPLE);
			return retVal;
		}

		/// <summary>
		/// delete a new_people widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleNewPeople(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_new_people table
			string sqlString = "DELETE FROM layout_module_new_people " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		/// <summary>
		/// Get a new_people widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleNewPeople(int id)
		{
			string sqlSelect = " SELECT f.title, f.number_showing, f.with_picture_only, m.page_id " +
				" FROM layout_module_new_people f" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = f.module_page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.NEW_PEOPLE);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a NewPeople row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleNewPeople(int modulePageId)
		{
			string sqlSelect = " SELECT title, number_showing, with_picture_only " +
				" FROM layout_module_new_people " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// save a NewPeople widget
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <param name="numberShowing">number of people to show</param>
		/// <param name="withPictureOnly">whether only to show people with picture or not</param>
		/// <returns></returns>
		public static void SaveLayoutModuleNewPeople(int modulePageId, string title, int numberShowing, bool withPictureOnly)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_new_people ");
			sb.Append(" SET number_showing = @number_showing, ");
			sb.Append(" with_picture_only = @with_picture_only, ");
			sb.Append(" title = @title ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@number_showing", numberShowing );
			parameters.Add ("@with_picture_only", withPictureOnly ? 1 : 0 );
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@title", title);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.NEW_PEOPLE);
		}

		#endregion

		#region Multiple Pictures

		//GetModuleMultiplePictures

		/// <summary>
		/// Get a Multiple Pictures widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleMultiplePictures(int id)
		{
			string sqlSelect = " SELECT mp.title, mp.pictures_per_page, mp.show_image_title, mp.group_id, mp.size, m.page_id " +
				" FROM layout_module_multiple_pictures mp" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = mp.module_page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.MULTIPLE_PICTURES);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a Multiple Pictures row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleMultiplePictures(int modulePageId)
		{
			string sqlSelect = " SELECT title, pictures_per_page, show_image_title, group_id, size " +
				" FROM layout_module_multiple_pictures " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}
		
		/// <summary>
		/// insert a record with default values to layout_module_multiple_pictures table and 
		/// return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleMultiplePictures(string title)
		{
			return InsertLayoutModuleMultiplePictures(title, Constants.DEFAULT_MULTIPLE_PICTURES_PICTURES_PER_PAGE,
				Constants.DEFAULT_MULTIPLE_PICTURES_SHOW_IMAGE_TITLE, -1, (int)Constants.ePICTURE_SIZE.LARGE
				);
		}
		
		/// <summary>
		/// insert a record to layout_module_multiple_pictures table and return the module_page_id
		/// </summary>
		/// <param name="title"></param>
		/// <param name="pictures_per_page"></param>
		/// <param name="show_image_title"></param>
		/// <param name="group_id"></param>
		/// <param name="size"></param>
		/// <returns></returns>
		public static int InsertLayoutModuleMultiplePictures(string title,
			int pictures_per_page,
			bool show_image_title,
			int group_id,
			int size)
		{
			string query = " INSERT INTO layout_module_multiple_pictures " + 
				" (title, pictures_per_page, show_image_title, group_id, size) " +
				" VALUES(@title, @pictures_per_page, @show_image_title, @group_id, @size) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@pictures_per_page", pictures_per_page );
			parameters.Add ("@show_image_title", show_image_title ? 1 : 0 );

			if ( group_id != -1 )
				parameters.Add ("@group_id", group_id);
			else
				parameters.Add ("@group_id", DBNull.Value);

			if ( size != -1 )
				parameters.Add ("@size", size);
			else
				parameters.Add ("@size", DBNull.Value);

			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.MULTIPLE_PICTURES);
			return retVal;
		}

		public static void SaveLayoutModuleMultiplePictures(int modulePageId, string title,
			int pictures_per_page,
			bool show_image_title,
			int group_id,
			int size)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_multiple_pictures ");
			sb.Append(" SET title = @title, ");
			sb.Append(" pictures_per_page = @pictures_per_page, ");
			sb.Append(" show_image_title = @show_image_title, ");
			sb.Append(" group_id = @group_id, ");
			sb.Append(" size = @size ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@pictures_per_page", pictures_per_page );
			parameters.Add ("@show_image_title", show_image_title ? 1 : 0 );
			parameters.Add ("@module_page_id", modulePageId);

			if ( group_id != -1 )
				parameters.Add ("@group_id", group_id);
			else
				parameters.Add ("@group_id", DBNull.Value);

			if ( size != -1 )
				parameters.Add ("@size", size);
			else
				parameters.Add ("@size", DBNull.Value);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.MULTIPLE_PICTURES);
		}

		/// <summary>
		/// delete a Multiple Pictures widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleMultiplePictures(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_multiple_pictures table
			string sqlString = "DELETE FROM layout_module_multiple_pictures " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		#endregion

		#region Slide Show

		/// <summary>
		/// Get a Slide Show widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleSlideShow(int id)
		{
			string sqlSelect = " SELECT ss.title, ss.hide_caption, ss.automated, ss.size, ss.group_id, m.page_id " +
				" FROM layout_module_slide_show ss" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = ss.module_page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.SLIDE_SHOW);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a Slide Show row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleSlideShow(int modulePageId)
		{
			string sqlSelect = " SELECT title, hide_caption, automated, size, group_id " +
				" FROM layout_module_slide_show " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}
		
		/// <summary>
		/// insert a record with default values to layout_module_slide_show table and 
		/// return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleSlideShow(string title)
		{
			return InsertLayoutModuleSlideShow(title, Constants.DEFAULT_SLIDE_SHOW_HIDE_CAPTION,
				Constants.DEFAULT_SLIDE_SHOW_AUTOMATED, (int)Constants.ePICTURE_SIZE.LARGE, -1
				);
		}
		
		/// <summary>
		/// insert a record to layout_module_slide_show table and return the module_page_id
		/// </summary>
		/// <param name="title"></param>
		/// <param name="hide_caption"></param>
		/// <param name="automated"></param>
		/// <param name="size"></param>
		/// <param name="group_id"></param>
		/// <returns></returns>
		public static int InsertLayoutModuleSlideShow(string title,
			bool hide_caption, 
			bool automated, 
			int size,
			int group_id)
		{
			string query = " INSERT INTO layout_module_slide_show " + 
				" (title, hide_caption, automated, size, group_id) " +
				" VALUES(@title, @hide_caption, @automated, @size, @group_id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@hide_caption", hide_caption ? 1 : 0 );
			parameters.Add ("@automated", automated ? 1 : 0 );

			if ( size != -1 )
				parameters.Add ("@size", size);
			else
				parameters.Add ("@size", DBNull.Value);

			if ( group_id != -1 )
				parameters.Add ("@group_id", group_id);
			else
				parameters.Add ("@group_id", DBNull.Value);

			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.SLIDE_SHOW);
			return retVal;
		}

		public static void SaveLayoutModuleSlideShow(int modulePageId, string title,
			bool hide_caption, 
			bool automated, 
			int size,
			int group_id)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_slide_show ");
			sb.Append(" SET title = @title, ");
			sb.Append(" hide_caption = @hide_caption, ");
			sb.Append(" automated = @automated, ");
			sb.Append(" size = @size, ");
			sb.Append(" group_id = @group_id ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@hide_caption", hide_caption ? 1 : 0 );
			parameters.Add ("@automated", automated ? 1 : 0 );

			if ( size != -1 )
				parameters.Add ("@size", size);
			else
				parameters.Add ("@size", DBNull.Value);

			if ( group_id != -1 )
				parameters.Add ("@group_id", group_id);
			else
				parameters.Add ("@group_id", DBNull.Value);

			parameters.Add ("@module_page_id", modulePageId);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.SLIDE_SHOW);
		}

		/// <summary>
		/// delete a Slide Show widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleSlideShow(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_slide_show table
			string sqlString = "DELETE FROM layout_module_slide_show " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		#endregion

		#region Single Picture

		/// <summary>
		/// Get a Single Picture widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleSinglePicture(int id)
		{
			string sqlSelect = " SELECT sp.title, sp.asset_id, sp.alignment, sp.size, m.page_id " +
				" FROM layout_module_single_picture sp" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = sp.module_page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.SINGLE_PICTURE);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a Single Picture row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleSinglePicture(int modulePageId)
		{
			string sqlSelect = " SELECT title, asset_id, alignment, size " +
				" FROM layout_module_single_picture " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}
		
		/// <summary>
		/// insert a record with default values to layout_module_single_picture table and 
		/// return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleSinglePicture(string title)
		{
			return InsertLayoutModuleSinglePicture(title, 
				Constants.DEFAULT_SINGLE_PICTURE_ASSET_ID, 
				Constants.DEFAULT_SINGLE_PICTURE_ALIGNMENT,
				(int)Constants.ePICTURE_SIZE.LARGE);
		}
		
		/// <summary>
		/// insert a record to layout_module_single_picture table and return the module_page_id
		/// </summary>
		/// <param name="title"></param>
		/// <param name="asset_id"></param>
		/// <param name="alignment"></param>
		/// <param name="size"></param>
		/// <returns></returns>
		public static int InsertLayoutModuleSinglePicture(string title,
			int asset_id, 
			int alignment, 
			int size)
		{
			string query = " INSERT INTO layout_module_single_picture " + 
				" (title, asset_id, alignment, size) " +
				" VALUES(@title, @asset_id, @alignment, @size) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@asset_id", asset_id );
			parameters.Add ("@alignment", alignment );

			if ( size != -1 )
				parameters.Add ("@size", size);
			else
				parameters.Add ("@size", DBNull.Value);

			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.SINGLE_PICTURE);
			return retVal;
		}

		public static void SaveLayoutModuleSinglePicture(int modulePageId, string title,
			int asset_id, 
			int alignment, 
			int size)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_single_picture ");
			sb.Append(" SET title = @title, ");
			sb.Append(" asset_id = @asset_id, ");
			sb.Append(" alignment = @alignment, ");
			sb.Append(" size = @size ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@asset_id", asset_id );
			parameters.Add ("@alignment", alignment );

			if ( size != -1 )
				parameters.Add ("@size", size);
			else
				parameters.Add ("@size", DBNull.Value);

			parameters.Add ("@module_page_id", modulePageId);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.SINGLE_PICTURE);
		}

		/// <summary>
		/// delete a Single Picture widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleSinglePicture(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_slide_show table
			string sqlString = "DELETE FROM layout_module_single_picture " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		#endregion

		#region Control Panel

		public static int GetLayoutPage_NumPersonalControlPanels(int page_id )
		{
			return GetLayoutPage_NumControlPanels( page_id, (int) Constants.eMODULE_TYPE.CONTROL_PANEL );
		}

		/// <summary>
		/// Get a Control Panel widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleControlPanel(int id)
		{
			string sqlSelect = " SELECT cp.title, cp.show_gender, cp.show_location, cp.show_age, m.page_id " +
				" FROM layout_module_control_panel cp" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = cp.module_page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.CONTROL_PANEL);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a Control Panel row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleControlPanel(int modulePageId)
		{
			string sqlSelect = " SELECT title, show_gender, show_location, show_age " +
				" FROM layout_module_control_panel " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// insert a record to layout_module_control_panel table and return the module_page_id
		/// </summary>
		public static int InsertLayoutModuleControlPanel (string title )
		{
			return InsertLayoutModuleControlPanel( title, 
				Constants.DEFAULT_CONTROL_PANEL_SHOW_GENDER,
				Constants.DEFAULT_CONTROL_PANEL_SHOW_LOCATION,
				Constants.DEFAULT_CONTROL_PANEL_SHOW_AGE );
		}

		/// <summary>
		/// insert a record to layout_module_control_panel table and return the module_page_id
		/// </summary>
		public static int InsertLayoutModuleControlPanel (string title,
			bool show_gender,
			bool show_location,
			bool show_age)
		{
			string query = " INSERT INTO layout_module_control_panel " + 
				" (title, show_gender, show_location, show_age) " +
				" VALUES(@title, @show_gender, @show_location, @show_age) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@show_gender", show_gender ? 1 : 0 );
			parameters.Add ("@show_location", show_location ? 1 : 0 );
			parameters.Add ("@show_age", show_age ? 1 : 0 );

			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.CONTROL_PANEL);
			return retVal;
		}

		public static void SaveLayoutModuleControlPanel(int modulePageId, string title,
			bool show_gender,
			bool show_location,
			bool show_age)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_control_panel ");
			sb.Append(" SET title = @title, ");
			sb.Append(" show_gender = @show_gender, ");
			sb.Append(" show_location = @show_location, ");
			sb.Append(" show_age = @show_age ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@show_gender", show_gender ? 1 : 0 );
			parameters.Add ("@show_location", show_location ? 1 : 0 );
			parameters.Add ("@show_age", show_age ? 1 : 0 );
			parameters.Add ("@module_page_id", modulePageId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.CONTROL_PANEL);
		}

		/// <summary>
		/// delete a Control Panel widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleControlPanel(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_slide_show table
			string sqlString = "DELETE FROM layout_module_control_panel " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		#endregion

		#region ChannelOwner
		/// <summary>
		/// Get a channel_owner widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleChannelOwner(int id)
		{
			string sqlSelect = " SELECT t.module_page_id, m.page_id, lp.page_id, lp.channel_id " +
				" FROM layout_module_channel_owner t" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = t.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.CHANNEL_OWNER);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// insert a record to layout_module_channel_owner table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleChannelOwner()
		{
			string query = " INSERT INTO layout_module_channel_owner " + 
				" () " +
				" VALUES() ";

			Hashtable parameters = new Hashtable ();
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.CHANNEL_OWNER);
			return retVal;
		}

		/// <summary>
		/// delete a title widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleChannelOwner(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_channel_owner table
			string sqlString = "DELETE FROM layout_module_channel_owner " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		#endregion

		#region Channel Members
		/// <summary>
		/// Get a title widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleChannelMembers(int id)
		{
			string sqlSelect = " SELECT c.title, c.entries_per_page, c.show_join_date, " +
				" c.show_role, c.show_digs, m.page_id, lp.channel_id " +
				" FROM layout_module_channel_members c" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = c.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.CHANNEL_MEMBERS);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a title row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleChannelMembers(int modulePageId)
		{
			string sqlSelect = " SELECT title, entries_per_page, show_join_date, show_role, show_digs " +
				" FROM layout_module_channel_members " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}
		
		/// <summary>
		/// insert a record with default values to layout_module_channel_members table and 
		/// return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleChannelMembers(string title)
		{
			return InsertLayoutModuleChannelMembers(title, 
				Constants.DEFAULT_CHANNELS_MEMBERS_ENTRIES_PER_PAGE,
				Constants.DEFAULT_CHANNEL_MEMBER_SHOW_JOIN_DATE,
				Constants.DEFAULT_CHANNEL_MEMBER_SHOW_ROLE,
				Constants.DEFAULT_CHANNEL_MEMBER_SHOW_DIGS
				);
		}

		/// <summary>
		/// insert a record to layout_module_channel_members table and return the module_page_id
		/// </summary>
		/// <param name="title"></param>
		/// <param name="entries_per_page"></param>
		/// <returns></returns>
		public static int InsertLayoutModuleChannelMembers(string title, int entries_per_page,
			bool showJoinDate,
			bool showRole,
			bool showDigs
			)
		{
			string query = " INSERT INTO layout_module_channel_members " + 
				" (title, entries_per_page,show_join_date, show_role, show_digs) " +
				" VALUES(@title, @entries_per_page, @show_join_date, @show_role, @show_digs) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@entries_per_page", entries_per_page);
			parameters.Add ("@show_join_date", showJoinDate ? 1 : 0);
			parameters.Add ("@show_role", showRole ? 1 : 0);
			parameters.Add ("@show_digs", showDigs ? 1 : 0);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.CHANNEL_MEMBERS);
			return retVal;
		}

		public static void SaveLayoutModuleChannelMembers(int modulePageId, string title, int entries_per_page,
			bool showJoinDate,
			bool showRole,
			bool showDigs
			)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_channel_members ");
			sb.Append(" SET title = @title, ");
			sb.Append(" entries_per_page = @entries_per_page, ");
			sb.Append(" show_join_date = @show_join_date, ");
			sb.Append(" show_role = @show_role, ");
			sb.Append(" show_digs = @show_digs ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();	
			parameters.Add ("@title", title);
			parameters.Add ("@entries_per_page", entries_per_page);
			parameters.Add ("@show_join_date", showJoinDate ? 1 : 0);
			parameters.Add ("@show_role", showRole ? 1 : 0);
			parameters.Add ("@show_digs", showDigs ? 1 : 0);
			parameters.Add ("@module_page_id", modulePageId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.CHANNEL_MEMBERS);
		}

		/// <summary>
		/// delete a title widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleChannelMembers(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_channel_members table
			string sqlString = "DELETE FROM layout_module_channel_members " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		#endregion


		#region events
		/// <summary>
		/// Get a events widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleEvents(int id)
		{
			string sqlSelect = " SELECT t.title, t.show_add_event, t.module_page_id, t.entries_per_page, " +
				" m.page_id, lp.page_id, lp.channel_id, c.allow_member_events " +
				" FROM layout_module_events t" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = t.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" INNER JOIN communities c ON c.community_id = lp.channel_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.CHANNEL_EVENTS);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a events row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleEvents(int modulePageId)
		{
			string sqlSelect = " SELECT title, show_add_event, entries_per_page" +
				" FROM layout_module_events " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}
		
		/// <summary>
		/// insert a record with default values to layout_module_events table and 
		/// return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleEvents(string title)
		{
			return InsertLayoutModuleEvents(title, true, Constants.DEFAULT_EVENTS_ENTRIES_PER_PAGE);
		}

		/// <summary>
		/// insert a record to layout_module_events table and return the module_page_id
		/// </summary>
		/// <param name="title"></param>
		/// <param name="show_add_event"></param>
		/// <returns></returns>
		public static int InsertLayoutModuleEvents(string title, bool show_add_event, int entries_per_page)
		{
			string query = " INSERT INTO layout_module_events " + 
				" (title, show_add_event, entries_per_page) " +
				" VALUES(@title, @show_add_event, @entries_per_page) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@show_add_event", show_add_event ? 1 : 0);
			parameters.Add ("@entries_per_page", entries_per_page);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.CHANNEL_EVENTS);
			return retVal;
		}

		public static void SaveLayoutModuleEvents(int modulePageId, string title, bool show_add_event, int entries_per_page)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_events ");
			sb.Append(" SET title = @title, ");
			sb.Append(" show_add_event = @show_add_event, ");
			sb.Append(" entries_per_page = @entries_per_page ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@show_add_event", show_add_event ? 1 : 0);
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@entries_per_page", entries_per_page);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.CHANNEL_EVENTS);
		}

		/// <summary>
		/// delete a events widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleEvents(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_events table
			string sqlString = "DELETE FROM layout_module_events " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		/// <summary>
		/// return's the default events title for a page
		/// </summary>
		/// <param name="pageId"></param>
		/// <returns></returns>
		public static string GetDefaultEvents(int pageId)
		{
			DataRow drUser = UsersUtility.GetUserByPageId(pageId);
			string retVal = null;
			if(drUser != null)
			{
				retVal = drUser["username"].ToString() + "'s Home Page";
			}

			DataRow drChannel = PageUtility.GetChannelByPageId(pageId);
			if(drChannel != null)
			{
				retVal = drChannel["name"].ToString() + "'s Home Page";
			}
			return retVal;
		}

		#endregion

		#region forum

		/// <summary>
		/// Get a forum widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleChannelForum(int id)
		{
			string sqlSelect = " SELECT b.title, b.entries_per_page, m.page_id, lp.channel_id, c.name, c.name_no_spaces " +
				" FROM layout_module_forum b" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = b.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" INNER JOIN communities c ON c.community_id = lp.channel_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.CHANNEL_FORUM);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a forum row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleChannelForum(int modulePageId)
		{
			string sqlSelect = " SELECT title, entries_per_page" +
				" FROM layout_module_forum " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// insert a record with default values to layout_module_forum table and 
		/// return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleChannelForum(string title)
		{
			return InsertLayoutModuleChannelForum(title, Constants.DEFAULT_CHANNEL_FORUM_ENTRIES_PER_PAGE);
		}
		
		/// <summary>
		/// insert a record to layout_module_forum table and return the module_page_id
		/// </summary>
		/// <param name="title"></param>
		/// <param name="entriesPerPage"></param>
		/// <returns></returns>
		public static int InsertLayoutModuleChannelForum(string title, int entriesPerPage)
		{
			string query = " INSERT INTO layout_module_forum " + 
				" (title, entries_per_page) " +
				" VALUES(@title, @entries_per_page) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@entries_per_page", entriesPerPage);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.CHANNEL_FORUM);
			return retVal;
		}

		public static void SaveLayoutModuleChannelForum(int modulePageId, string title, 
			int entriesPerPage)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_forum ");
			sb.Append(" SET title = @title, ");
			sb.Append(" entries_per_page = @entries_per_page ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@entries_per_page", entriesPerPage);
			parameters.Add ("@module_page_id", modulePageId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.CHANNEL_FORUM);
		}

		/// <summary>
		/// delete a title widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleChannelForum(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_forum table
			string sqlString = "DELETE FROM layout_module_forum " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}
		#endregion

		#region Gifts
		/// <summary>
		/// get the parameters
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleUserGifts(int id)
		{
			string sqlSelect = " SELECT p.title, m.page_id, p.entries_per_page " +
				" FROM layout_module_user_gifts p" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = p.module_page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.USER_GIFTS);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// delete a gift widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleUserGifts(int id)
		{
			ModuleDeleted(id);

			string sqlString;
			Hashtable parameters;

			//delete from layout_module_forum table
			sqlString = "DELETE FROM layout_module_user_gifts " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// add a gift widget
		/// </summary>
		/// <param name="module_title"></param>
		/// <returns></returns>
		public static int InsertLayoutModuleUserGifts ( string module_title, int  pageSize )
		{
			string query = " INSERT INTO layout_module_user_gifts " + 
				" (title, entries_per_page) " +
				" VALUES(@title,@entries) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", module_title);
			parameters.Add( "@entries", pageSize );
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.USER_GIFTS);
			return retVal;
			
		}
		/// <summary>
		/// save it
		/// </summary>
		/// <param name="module_title"></param>
		/// <returns></returns>
		public static void SaveLayoutModuleUserGifts(int modulePageId, string title, int pageSize )
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_user_gifts ");
			sb.Append(" SET title = @title, ");
			sb.Append(" entries_per_page = @entries ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@title", title);
			parameters.Add( "@entries", pageSize );
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.USER_GIFTS);
		}
		#endregion 

		#region ChannelPortal
		/// <summary>
		/// get the parameters
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleChannelPortal(int id)
		{
			string sqlSelect = " SELECT p.title " +
				" FROM layout_module_channel_portal p" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = p.module_page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.CHANNEL_PORTAL);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// delete a gift widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleChannelPortal(int id)
		{
			ModuleDeleted(id);

			string sqlString;
			Hashtable parameters;

			//delete from layout_module_forum table
			sqlString = "DELETE FROM layout_module_channel_portal " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// add a gift widget
		/// </summary>
		/// <param name="module_title"></param>
		/// <returns></returns>
		public static int InsertLayoutModuleChannelPortal ( string module_title )
		{
			string query = " INSERT INTO layout_module_channel_portal " + 
				" (title) " +
				" VALUES(@title) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", module_title);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.CHANNEL_PORTAL);
			return retVal;
			
		}
		/// <summary>
		/// save it
		/// </summary>
		/// <param name="module_title"></param>
		/// <returns></returns>
		public static void SaveLayoutModuleChannelPortal(int modulePageId, string title )
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_channel_portal ");
			sb.Append(" SET title = @title ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@title", title);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.CHANNEL_PORTAL);
		}
		#endregion 

		#region ProfilePortal
		/// <summary>
		/// get the parameters
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleProfilePortal(int id)
		{
			/*string sqlSelect = " SELECT p.title, m.page_id, p.show_stats " +
				" FROM layout_module_profile_portal p" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = p.module_page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.PROFILE_PORTAL);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);*/

			string sqlSelect = " SELECT cp.title, cp.show_gender, cp.show_location, cp.show_age, m.page_id " +
				" FROM layout_module_control_panel cp" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = cp.module_page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.PROFILE_PORTAL);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);

		}

		/// <summary>
		/// delete a gift widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleProfilePortal(int id)
		{
			ModuleDeleted(id);

			string sqlString;
			Hashtable parameters;

			//delete from layout_module_forum table
			sqlString = "DELETE FROM layout_module_profile_portal " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// add a gift widget
		/// </summary>
		/// <param name="module_title"></param>
		/// <returns></returns>
		public static int InsertLayoutModuleProfilePortal ( string module_title, bool showStats )
		{
			string query = " INSERT INTO layout_module_profile_portal " + 
				" (title, show_stats) " +
				" VALUES(@title,@showStats) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", module_title);
			parameters.Add( "@showStats", showStats ? 'T' : 'F' );
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.PROFILE_PORTAL);
			return retVal;
			
		}
		/// <summary>
		/// save it
		/// </summary>
		/// <param name="module_title"></param>
		/// <returns></returns>
		public static void SaveLayoutModuleProfilePortal(int modulePageId, string title, bool showStats  )
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_profile_portal ");
			sb.Append(" SET title = @title, ");
			sb.Append(" show_stats = @showStats ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@title", title);
			parameters.Add( "@showStats", showStats ? 'T' : 'F' );
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.PROFILE_PORTAL);
		}
		#endregion 


		#region Channel Control Panel

		public static int GetLayoutPage_NumChannelControlPanels(int page_id )
		{
			return GetLayoutPage_NumControlPanels( page_id, (int) Constants.eMODULE_TYPE.CHANNEL_CONTROL_PANEL );
		}

		public static int GetLayoutPage_NumControlPanels(int page_id, int cp_module_id )
		{
			string sqlSelect = " SELECT count(module_page_id) as num_control_panels FROM layout_page_modules " +
				" WHERE page_id = @page_id and module_id = @cp_module_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@page_id", page_id);
			parameters.Add ("@cp_module_id", cp_module_id);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return Convert.ToInt32(row["num_control_panels"]);
		}

		/// <summary>
		/// insert a record to layout_module_channel_control_panel table and return the module_page_id
		/// wrapper for default insert 
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleChannelControlPanel( string title)
		{
			return InsertLayoutModuleChannelControlPanel(title, true, "", true, false, true, true, true, true, true);
		}

		/// <summary>
		/// insert a record to layout_module_channel_control_panel table and return the module_page_id
		/// wrapper for additional page creation
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleChannelControlPanel( string title, bool show_menu, string bannerPath)
		{
			return InsertLayoutModuleChannelControlPanel(title, show_menu, bannerPath, true, false, true, true, true, true, true);
		}

		/// <summary>
		/// insert a record to layout_module_channel_control_panel table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleChannelControlPanel( string title, bool show_menu, string bannerPath,  
			bool JoinChannel, bool QuitChannel, bool ReportAbuse, bool SendMessage, 
			bool SubmitMedia, bool TellIt, bool Vote)
		{
			string query = " INSERT INTO layout_module_channel_control_panel " + 
				" (title, show_menu, banner_path, show_join_channel, show_quit_channel, show_send_message, show_rave_it, " +
				" show_report_abuse, show_tell_it, show_submit_media ) " +
				" VALUES(@title, @show_menu, @bannerPath, @JoinChannel, @QuitChannel, @SendMessage, @Vote, " +
				" @ReportAbuse, @TellIt, @SubmitMedia ) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@show_menu", show_menu ? 1 : 0);
			parameters.Add ("@bannerPath", bannerPath);
			parameters.Add ("@JoinChannel", JoinChannel ? 1 : 0);
			parameters.Add ("@QuitChannel", QuitChannel ? 1 : 0);
			parameters.Add ("@SendMessage", SendMessage ? 1 : 0);
			parameters.Add ("@Vote", Vote ? 1 : 0);
			parameters.Add ("@ReportAbuse", ReportAbuse ? 1 : 0);
			parameters.Add ("@TellIt", TellIt ? 1 : 0);
			parameters.Add ("@SubmitMedia", SubmitMedia ? 1 : 0);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.CHANNEL_CONTROL_PANEL);
			return retVal;

		}

		/// <summary>
		/// gets the control panel widget for use of creation of extrapages
		/// default settings.
		/// </summary>
		/// <param name="channel_is">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetDefaultModuleChannelControlPanel(int channelId)
		{
			StringBuilder sb = new StringBuilder() ; 
			sb.Append(" SELECT distinct t.title, t.show_menu, t.module_page_id, t.banner_path, ");
			sb.Append(" m.page_id, lp.page_id, lp.channel_id, m.sequence ");
			sb.Append(" FROM layout_module_channel_control_panel t ");
			sb.Append(" INNER JOIN layout_page_modules m ON m.module_page_id = t.module_page_id ");
			sb.Append(" INNER JOIN layout_pages lp ON lp.page_id = m.page_id ");
			sb.Append(" WHERE lp.channel_id = @channelId AND lp.home_page = 1 ");
			sb.Append(" ORDER BY m.sequence LIMIT 1 ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@channelId", channelId);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sb.ToString(), parameters, false);
		}

		/// <summary>
		/// delete a title widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleChannelControlPanel(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_contest_profile_header table
			string sqlString = "DELETE FROM layout_module_channel_control_panel " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

		}

		/// <summary>
		/// Get a System Stats widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleChannelControlPanel(int id)
		{
			string sqlSelect = " " + 
				" SELECT mt.module_page_id, lp.channel_id, c.name, c.allow_publishing, c.name_no_spaces, c.url, " +
				" mt.title, mt.show_menu, mt.banner_path, " +
				" mt.show_join_channel, mt.show_quit_channel, mt.show_send_message, mt.show_rave_it, " +
				" mt.show_report_abuse, mt.show_tell_it, mt.show_submit_media, " +
				" cs.number_of_diggs " +
				" FROM layout_module_channel_control_panel mt" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = mt.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" INNER JOIN communities c ON c.community_id = lp.channel_id " +
				" INNER JOIN channel_stats cs ON cs.channel_id = lp.channel_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.CHANNEL_CONTROL_PANEL);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);

		}

		/// <summary>
		/// load a System Stats row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleChannelControlPanel(int modulePageId)
		{
			string sqlSelect = " SELECT title, show_menu, banner_path, show_join_channel, show_quit_channel, " +
				" show_send_message, show_rave_it, show_report_abuse, show_tell_it, show_submit_media " + 
				" FROM layout_module_channel_control_panel " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// save a System Stats widget
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static void SaveLayoutModuleChannelControlPanel( int modulePageId, string title, bool show_menu,
			bool removeBanner, bool JoinChannel, bool QuitChannel, bool ReportAbuse, bool SendMessage, 
			bool SubmitMedia, bool TellIt, bool Vote)
		{
			Hashtable parameters = new Hashtable ();			

			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_channel_control_panel ");
			sb.Append(" SET title = @title, ");
			sb.Append(" show_menu = @show_menu, ");
			sb.Append(" show_join_channel = @JoinChannel, ");
			sb.Append(" show_quit_channel = @QuitChannel, ");
			sb.Append(" show_send_message = @SendMessage, ");
			sb.Append(" show_rave_it = @Vote, ");
			sb.Append(" show_report_abuse = @ReportAbuse, ");
			sb.Append(" show_tell_it = @TellIt, ");
			sb.Append(" show_submit_media = @SubmitMedia ");
			sb.Append(" WHERE module_page_id = @module_page_id ");
	
			parameters.Add ("@title", title);
			parameters.Add ("@show_menu", show_menu ? 1 : 0);
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@JoinChannel", JoinChannel ? 1 : 0);
			parameters.Add ("@QuitChannel", QuitChannel ? 1 : 0);
			parameters.Add ("@SendMessage", SendMessage ? 1 : 0);
			parameters.Add ("@Vote", Vote ? 1 : 0);
			parameters.Add ("@ReportAbuse", ReportAbuse ? 1 : 0);
			parameters.Add ("@TellIt", TellIt ? 1 : 0);
			parameters.Add ("@SubmitMedia", SubmitMedia ? 1 : 0);
			
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.TITLE_TEXT);

		}

		#endregion

		#region channel_description

		/// <summary>
		/// Get a channel_description widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleChannelDescription(int id)
		{
			StringBuilder sqlSelect = new StringBuilder() ;
			sqlSelect.Append(" SELECT b.title, b.num_moderators, b.show_moderators, m.page_id, lp.channel_id, ");
			sqlSelect.Append(" c.name, c.created_date, c.creator_username, c.thumbnail_small_path, c.thumbnail_medium_path, c.thumbnail_large_path, cs.number_of_members ");
			sqlSelect.Append(" FROM layout_module_channel_description b");
			sqlSelect.Append(" INNER JOIN layout_page_modules m ON m.module_page_id = b.module_page_id ");
			sqlSelect.Append(" INNER JOIN layout_pages lp ON lp.page_id = m.page_id ");
			sqlSelect.Append(" INNER JOIN communities c ON c.community_id = lp.channel_id ");
			sqlSelect.Append(" INNER JOIN channel_stats cs ON cs.channel_id = lp.channel_id ");
			sqlSelect.Append(" WHERE m.id = @id ");
			sqlSelect.Append(" AND m.module_id = @moduleType ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.CHANNEL_DESCRIPTION);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect.ToString(), parameters, false);
		}

		/// <summary>
		/// load a channel_description row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleChannelDescription(int modulePageId)
		{
			string sqlSelect = " SELECT title, num_moderators, show_moderators " +
				" FROM layout_module_channel_description " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// insert a record with default values to layout_module_channel_description table and 
		/// return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleChannelDescription(string title)
		{
			return InsertLayoutModuleChannelDescription(title, Constants.DEFAULT_CHANNEL_NUM_MODERATORS_TO_SHOW,
				Constants.DEFAULT_CHANNEL_SHOW_MODERATORS);
		}
		
		/// <summary>
		/// insert a record to layout_module_channel_description table and return the module_page_id
		/// </summary>
		/// <param name="title"></param>
		/// <param name="numModerators"></param>
		/// <param name="showModerators"></param>
		/// <returns></returns>
		public static int InsertLayoutModuleChannelDescription(string title, int numModerators, bool showModerators)
		{
			string query = " INSERT INTO layout_module_channel_description " + 
				" (title, num_moderators, show_moderators) " +
				" VALUES(@title, @num_moderators, @show_moderators) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@num_moderators", numModerators);
			parameters.Add ("@show_moderators", showModerators ? 1 : 0);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.BLOGS);
			return retVal;
		}

		public static void SaveLayoutModuleChannelDescription(int modulePageId, string title, 
			int numModerators, bool showModerators)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_channel_description ");
			sb.Append(" SET title = @title, ");
			sb.Append(" num_moderators = @num_moderators, ");
			sb.Append(" show_moderators = @show_moderators ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@num_moderators", numModerators);
			parameters.Add ("@show_moderators", showModerators ? 1 : 0);
			parameters.Add ("@module_page_id", modulePageId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.BLOGS);
		}

		/// <summary>
		/// delete a title widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleChannelDescription(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_channel_description table
			string sqlString = "DELETE FROM layout_module_channel_description " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}
		#endregion 


		#region mixed_media
		/// <summary>
		/// Get a mixed_media widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleMixedMedia(int id)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" SELECT mmm.module_page_id, mmm.text, mmm.view_type_id, mmm.thumbnail_size, ");
			sb.Append(" mmm.num_showing, mmm.num_rows, mmm.num_columns, mmm.show_name, ");
			sb.Append(" mmm.show_description, mmm.show_diggs, mmm.show_num_views, mmm.show_num_shares, ");
			sb.Append(" mmm.show_price, mmm.show_type, mmm.asset_group_id, m.page_id, ");
			sb.Append(" lp.page_id, lp.channel_id ");
			sb.Append(" FROM layout_module_mixed_media mmm ");
			sb.Append(" INNER JOIN layout_page_modules m ON m.module_page_id = mmm.module_page_id ");
			sb.Append(" INNER JOIN layout_pages lp ON lp.page_id = m.page_id ");
			sb.Append(" WHERE m.id = @id ");
			sb.Append(" AND m.module_id = @moduleType ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.MIXED_MEDIA);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sb.ToString(), parameters, false);
		}

		/// <summary>
		/// load a mixed_media row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleMixedMedia(int modulePageId)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" SELECT mmm.module_page_id, mmm.text, mmm.view_type_id, mmm.thumbnail_size, ");
			sb.Append(" mmm.num_showing, mmm.num_rows, mmm.num_columns, mmm.show_name, ");
			sb.Append(" mmm.show_description, mmm.show_diggs, mmm.show_num_views, mmm.show_num_shares, ");
			sb.Append(" mmm.show_price, mmm.show_type, mmm.asset_group_id");
			sb.Append(" FROM layout_module_mixed_media mmm ");
			sb.Append(" WHERE mmm.module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sb.ToString(), parameters, false);
			return row;
		}
		
		/// <summary>
		/// insert a record with default values to layout_module_mixed_media table and 
		/// return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleMixedMedia(string title)
		{
			return InsertLayoutModuleMixedMedia(
				title,
				Constants.DEFAULT_MIXED_MEDIA_VIEW_TYPE_ID,
				Constants.DEFAULT_MIXED_MEDIA_THUMBNAIL_SIZE,
				Constants.DEFAULT_MIXED_MEDIA_NUM_SHOWING,
				Constants.DEFAULT_MIXED_MEDIA_NUM_ROWS,
				Constants.DEFAULT_MIXED_MEDIA_NUM_COLUMNS,
				Constants.DEFAULT_MIXED_MEDIA_SHOW_NAME,
				Constants.DEFAULT_MIXED_MEDIA_SHOW_TEASER,
				Constants.DEFAULT_MIXED_MEDIA_SHOW_DIGGS,
				Constants.DEFAULT_MIXED_MEDIA_SHOW_NUM_VIEWS,
				Constants.DEFAULT_MIXED_MEDIA_SHOW_NUM_SHARES,
				Constants.DEFAULT_MIXED_MEDIA_SHOW_PRICE,
				Constants.DEFAULT_MIXED_MEDIA_SHOW_TYPE,
				0
				);
		}
		
		/// <summary>
		/// insert a record to layout_module_mixed_media table and return the module_page_id
		/// </summary>
		/// <param name="text"></param>
		/// <param name="view_type_id"></param>
		/// <param name="thumbnail_size"></param>
		/// <param name="num_showing"></param>
		/// <param name="num_rows"></param>
		/// <param name="num_columns"></param>
		/// <param name="show_name"></param>
		/// <param name="show_description"></param>
		/// <param name="show_diggs"></param>
		/// <param name="show_num_views"></param>
		/// <param name="show_num_shares"></param>
		/// <param name="show_price"></param>
		/// <param name="show_type"></param>
		/// <returns></returns>
		public static int InsertLayoutModuleMixedMedia(
			string text, int view_type_id, int thumbnail_size,
			int num_showing, int num_rows, int num_columns, bool show_name,
			bool show_description, bool show_diggs, bool show_num_views, bool show_num_shares,
			bool show_price, bool show_type, int asset_group_id)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" INSERT INTO layout_module_mixed_media ");
			sb.Append(" (text, view_type_id, thumbnail_size, ");
			sb.Append(" num_showing, num_rows, num_columns, show_name, ");
			sb.Append(" show_description, show_diggs, show_num_views, show_num_shares, ");
			sb.Append(" show_price, show_type, asset_group_id ) ");
			sb.Append(" VALUES(@text, @view_type_id, @thumbnail_size, ");
			sb.Append(" @num_showing, @num_rows, @num_columns, @show_name, ");
			sb.Append(" @show_description, @show_diggs, @show_num_views, @show_num_shares, ");
			sb.Append(" @show_price, @show_type, @asset_group_id) ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@text", text);
			parameters.Add ("@view_type_id", view_type_id); 
			parameters.Add ("@thumbnail_size", thumbnail_size); 
			parameters.Add ("@num_showing", num_showing > 0 ? num_showing : 0); 
			parameters.Add ("@num_rows", num_rows > 0 ? num_rows : 0); 
			parameters.Add ("@num_columns", num_columns > 0 ? num_columns : 0); 
			parameters.Add ("@show_name", show_name ? 1 : 0); 
			parameters.Add ("@show_description", show_description ? 1 : 0); 
			parameters.Add ("@show_diggs", show_diggs ? 1 : 0); 
			parameters.Add ("@show_num_views", show_num_views ? 1 : 0); 
			parameters.Add ("@show_num_shares", show_num_shares ? 1 : 0); 
			parameters.Add ("@show_price", show_price ? 1 : 0); 
			parameters.Add ("@show_type", show_type ? 1 : 0); 
			parameters.Add ("@asset_group_id", asset_group_id > 0 ? 
				asset_group_id : 0);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(sb.ToString(), parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.MIXED_MEDIA);
			return retVal;
		}

		public static void SaveLayoutModuleMixedMedia(int modulePageId, 
			string text, int view_type_id, int thumbnail_size,
			int num_showing, int num_rows, int num_columns, bool show_name,
			bool show_description, bool show_diggs, bool show_num_views, bool show_num_shares,
			bool show_price, bool show_type, int asset_group_id)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_mixed_media ");
			sb.Append(" SET text = @text, "); 
			sb.Append(" view_type_id = @view_type_id, "); 
			sb.Append(" thumbnail_size = @thumbnail_size, "); 
			sb.Append(" num_showing = @num_showing, "); 
			sb.Append(" num_rows = @num_rows, "); 
			sb.Append(" num_columns = @num_columns, "); 
			sb.Append(" show_name = @show_name, "); 
			sb.Append(" show_description = @show_description, "); 
			sb.Append(" show_diggs = @show_diggs, "); 
			sb.Append(" show_num_views = @show_num_views, "); 
			sb.Append(" show_num_shares = @show_num_shares, "); 
			sb.Append(" show_price = @show_price, "); 
			sb.Append(" show_type = @show_type, ");
			sb.Append(" asset_group_id = @asset_group_id ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@text", text);
			parameters.Add ("@view_type_id", view_type_id); 
			parameters.Add ("@thumbnail_size", thumbnail_size); 
			parameters.Add ("@num_showing", num_showing > 0 ? num_showing.ToString() : "NULL"); 
			parameters.Add ("@num_rows", num_rows > 0 ? num_rows.ToString() : "NULL"); 
			parameters.Add ("@num_columns", num_columns > 0 ? num_columns.ToString() : "NULL"); 
			parameters.Add ("@show_name", show_name ? 1 : 0); 
			parameters.Add ("@show_description", show_description ? 1 : 0); 
			parameters.Add ("@show_diggs", show_diggs ? 1 : 0); 
			parameters.Add ("@show_num_views", show_num_views ? 1 : 0); 
			parameters.Add ("@show_num_shares", show_num_shares ? 1 : 0); 
			parameters.Add ("@show_price", show_price ? 1 : 0); 
			parameters.Add ("@show_type", show_type ? 1 : 0); 
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@asset_group_id", asset_group_id > 0 ? 
				asset_group_id.ToString() : "NULL");
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.MIXED_MEDIA);
		}

		/// <summary>
		/// delete a mixed_media widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleMixedMedia(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_mixed_media table
			string sqlString = "DELETE FROM layout_module_mixed_media " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		#endregion


		

		#region OMM Media

		/// <summary>
		/// insert a default record to layout_module_omm_media table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleOmmMedia (int moduleId, string title )
		{
			return InsertLayoutModuleOmmMedia (title, 0, false, moduleId);
		}

		/// <summary>
		/// insert a record to layout_module_omm_media table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleOmmMedia ( string title, int assetId, bool autoStart, int moduleId)
		{
			string query = " INSERT INTO layout_module_omm_media " + 
				" (title,asset_id,auto_start) " +
				" VALUES (@title, @assetId, @autoStart) ";

			Hashtable parameters = new Hashtable ();	
		
			if (title != "")
				parameters.Add ("@title", title );
			else
				parameters.Add ("@title", DBNull.Value );

			parameters.Add ("@assetId", assetId  );
			parameters.Add ("@autoStart", autoStart ? 1 : 0 );
			int retVal = -1;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated (retVal, moduleId);
			return retVal;
		}

		/// <summary>
		/// delete a OMM widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleOmmMedia (int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_friends table
			string sqlString = "DELETE FROM layout_module_omm_media " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		/// <summary>
		/// Get a OmmMedia widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleOmmMedia (int id)
		{
			string sqlSelect = " SELECT s.title, s.asset_id, m.page_id, lp.channel_id, auto_start " +
				" FROM layout_module_omm_media s" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = s.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" WHERE m.id = @id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a OmmMedia row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleOmmMedia (int modulePageId)
		{
			string sqlSelect = " SELECT title, asset_id, auto_start " +
				" FROM layout_module_omm_media " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// save a OmmMedia widget
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static void SaveLayoutModuleOmmMedia (int modulePageId, string title, int assetId, bool autoStart, int moduleId)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_omm_media ");
			sb.Append(" SET title = @title, ");
			sb.Append(" asset_id = @assetId, ");
			sb.Append(" auto_start = @autoStart ");
			sb.Append(" WHERE module_page_id = @module_page_id ");


			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@autoStart", autoStart ? 1 : 0 );
			parameters.Add ("@module_page_id", modulePageId);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated (modulePageId, moduleId);
		}

		#endregion


		#region OMM Playlist

		/// <summary>
		/// insert a default record to layout_module_omm_playlist table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleOmmPlaylist (int moduleId, string title)
		{
			return InsertLayoutModuleOmmPlaylist (title, 10, -1, moduleId);
		}

		/// <summary>
		/// insert a record to layout_module_omm_playlist table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleOmmPlaylist ( string title, int itemsPerPage, int groupId, int moduleId)
		{
			string query = " INSERT INTO layout_module_omm_playlist " + 
				" (title,items_per_page,group_id) " +
				" VALUES (@title, @itemsPerPage, @group_id) ";

			Hashtable parameters = new Hashtable ();	
		
			if (title != "")
				parameters.Add ("@title", title );
			else
				parameters.Add ("@title", DBNull.Value );

			if ( groupId != -1 )
				parameters.Add ("@group_id", groupId);
			else
				parameters.Add ("@group_id", DBNull.Value);

			parameters.Add ("@itemsPerPage", itemsPerPage  );
			int retVal = -1;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated (retVal, moduleId);
			return retVal;
		}

		/// <summary>
		/// delete a OMM playlist widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleOmmPlaylist (int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_friends table
			string sqlString = "DELETE FROM layout_module_omm_playlist " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		/// <summary>
		/// Get a stores OMM Playlist data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleOmmPlaylist (int id)
		{
			string sqlSelect = " SELECT s.title, s.items_per_page, m.page_id, s.group_id, lp.channel_id " +
				" FROM layout_module_omm_playlist s" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = s.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" WHERE m.id = @id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a OMM Playlist row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleOmmPlaylist (int modulePageId)
		{
			string sqlSelect = " SELECT title, items_per_page, group_id " +
				" FROM layout_module_omm_playlist " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// save a OMM Playlist widget
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static void SaveLayoutModuleOmmPlaylist (int modulePageId, string title, int items_per_page, int group_id, int moduleId)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_omm_playlist ");
			sb.Append(" SET title = @title, ");
			sb.Append(" group_id = @group_id, ");
			sb.Append(" items_per_page = @items_per_page ");
			sb.Append(" WHERE module_page_id = @module_page_id ");


			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@items_per_page", items_per_page);
			parameters.Add ("@module_page_id", modulePageId);

			if ( group_id != -1 )
				parameters.Add ("@group_id", group_id);
			else
				parameters.Add ("@group_id", DBNull.Value);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated (modulePageId, moduleId);
		}

		#endregion

		#region Channel Member Map

		/// <summary>
		/// insert a record to layout_module_channel_top_contributors table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleChannelTopContributors(string title, int numTopMembers)
		{
			string query = " INSERT INTO layout_module_channel_top_contributors " + 
				" (title, number_of_top_contributors) " +
				" VALUES(@title, @number_of_top_contributors) ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@title", title);
			parameters.Add ("@number_of_top_contributors", numTopMembers);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.TOP_CONTRIBUTORS);
			return retVal;
		}

		/// <summary>
		/// delete a title widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleChannelTopContributors(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_channel_top_contributors table
			string sqlString = "DELETE FROM layout_module_channel_top_contributors " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		/// <summary>
		/// Get a System Stats widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleChannelTopContributors(int id)
		{
			string sqlSelect = " SELECT ss.title, ss.number_of_top_contributors, m.page_id, " +
				" lp.channel_id, c.name, " +
				" cs.number_of_diggs " +
				" FROM layout_module_channel_top_contributors ss" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = ss.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" INNER JOIN communities c ON c.community_id = lp.channel_id " +
				" INNER JOIN channel_stats cs ON cs.channel_id = lp.channel_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.TOP_CONTRIBUTORS);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a System Stats row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleChannelTopContributors(int modulePageId)
		{
			string sqlSelect = " SELECT title, number_of_top_contributors " +
				" FROM layout_module_channel_top_contributors " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// save a System Stats widget
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static void SaveLayoutModuleChannelTopContributors(int modulePageId, string title, 
			int numTopMembers)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_channel_top_contributors ");
			sb.Append(" SET title = @title, ");
			sb.Append(" number_of_top_contributors = @number_of_top_contributors ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@title", title);
			parameters.Add ("@number_of_top_contributors", numTopMembers);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.TOP_CONTRIBUTORS);
		}

		#endregion

		#region Contest Top Vote Getters

		/// <summary>
		/// Get a ContestSubmissions widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleContestTopVotes (int id)
		{
			string sqlSelect = " SELECT lp.channel_id, s.show_Mature, s.title, s.contest_id, s.num_results_to_return, s.info_placement, s.intro_text, s.footer, s.rankby_id " +
				" FROM layout_module_contest_topvotes s" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = s.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.CONTEST_TOP_RESULTS);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a ContestSubmissions row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleContestTopVotes (int modulePageId)
		{
			string sqlSelect = " SELECT title, show_Mature, contest_id, num_results_to_return, info_placement, intro_text, footer, rankby_id " +
				" FROM layout_module_contest_topvotes " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}
		
		/// <summary>
		/// insert a record to layout_module_contest_submissions table and return the module_page_id
		/// </summary>
		public static int InsertLayoutModuleContestTopVotes (string title, bool showMature, int contestId, string introText, int resultsToReturn, bool infoPlacement, string footer, int rankby_id)
		{
			string query = " INSERT INTO layout_module_contest_topvotes (title, show_Mature, contest_id, num_results_to_return, info_placement, intro_text, footer, rankby_id) " +
				" VALUES (@title, @showMature, @contestId, @resultsToReturn, @infoPlacement, @introText, @footer, @rankby_id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@showMature", showMature ? 1 : 0);
			parameters.Add ("@introText", introText);
			parameters.Add ("@contestId", contestId);
			parameters.Add ("@resultsToReturn", resultsToReturn);
			parameters.Add ("@infoPlacement", infoPlacement ? 1 : 0);
			parameters.Add ("@footer", footer);
			parameters.Add ("@rankby_id", rankby_id);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.CONTEST_TOP_RESULTS);
			return retVal;
		}

		public static void SaveLayoutModuleContestTopVotes (int modulePageId, bool showMature, int contestId, string title, string introText, int resultsToReturn, bool infoPlacement, string footer, int rankby_id)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_contest_topvotes ");
			sb.Append(" SET title = @title, show_Mature = @showMature, contest_id = @contestId, intro_text = @introText, num_results_to_return = @resultsToReturn, info_placement = @infoPlacement, footer = @footer, rankby_id = @rankby_id ");
			sb.Append(" WHERE module_page_id = @modulePageId ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@showMature", showMature ? 1:0);
			parameters.Add ("@contestId", contestId);
			parameters.Add ("@introText", introText);
			parameters.Add ("@modulePageId", modulePageId);
			parameters.Add ("@resultsToReturn", resultsToReturn);
			parameters.Add ("@infoPlacement", infoPlacement ? 1 : 0);
			parameters.Add ("@footer", footer);
			parameters.Add ("@rankby_id", rankby_id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.CONTEST_TOP_RESULTS);
		}

		/// <summary>
		/// delete a ContestSubmissions widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleContestTopVotes (int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_blogs table
			string sqlString = "DELETE FROM layout_module_contest_topvotes " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		#endregion

		#region Contest Submissions

		/// <summary>
		/// Get a ContestSubmissions widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleContestSubmissions (int id)
		{
			string sqlSelect = " SELECT lp.channel_id, s.title, s.show_Mature, s.contest_id, assets_per_page, assets_per_row, info_placement, intro_text_title, intro_text, cloud_title, hide_cloud, max_cloud_length " +
				" FROM layout_module_contest_submissions s" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = s.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.CONTEST_SUBMISSIONS);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a ContestSubmissions row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleContestSubmissions (int modulePageId)
		{
			string sqlSelect = " SELECT title, contest_id, show_Mature, assets_per_page, assets_per_row, info_placement, intro_text_title, intro_text, cloud_title, hide_cloud, max_cloud_length " +
				" FROM layout_module_contest_submissions " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}
		
		/// <summary>
		/// insert a record to layout_module_contest_submissions table and return the module_page_id
		/// </summary>
		public static int InsertLayoutModuleContestSubmissions (string title, bool showMature, int contestId, int assetsPerPage, string introText, string cloudTitle, bool hideCloud, int assetsPerRow, bool infoPlacement, int maxCloudLength)
		{
			string query = " INSERT INTO layout_module_contest_submissions " + 
				" (title, show_Mature, contest_id, assets_per_page, assets_per_row, info_placement, intro_text, cloud_title, hide_cloud, max_cloud_length) " +
				" VALUES (@title, @showMature, @contestId, @assetsPerPage, @assetsPerRow, @infoPlacement, @introText, @cloudTitle, @hideCloud, @maxCloudLength) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@showMature", showMature ? 1:0);
			parameters.Add ("@assetsPerPage", assetsPerPage);
			parameters.Add ("@introText", introText);
			parameters.Add ("@cloudTitle", cloudTitle);
			parameters.Add ("@contestId", contestId);
			parameters.Add ("@hideCloud", hideCloud? 1: 0);
			parameters.Add ("@assetsPerRow", assetsPerRow);
			parameters.Add ("@infoPlacement", infoPlacement ? 1:0);
			parameters.Add ("@maxCloudLength", maxCloudLength);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.CONTEST_SUBMISSIONS);
			return retVal;
		}

		public static void SaveLayoutModuleContestSubmissions (int modulePageId, bool showMature, int contestId, string title, 
			int assetsPerPage, string introText, string cloudTitle, bool hideCloud, int assetsPerRow, bool infoPlacement, int maxCloudLength)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_contest_submissions ");
			sb.Append(" SET title = @title, show_Mature = @showMature, contest_id = @contestId, assets_per_page = @assetsPerPage, intro_text = @introText, cloud_title = @cloudTitle, hide_cloud = @hideCloud, assets_per_row = @assetsPerRow, info_placement = @infoPlacement, max_cloud_length = @maxCloudLength ");
			sb.Append(" WHERE module_page_id = @modulePageId ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@showMature", showMature ? 1:0);
			parameters.Add ("@contestId", contestId);
			parameters.Add ("@assetsPerPage", assetsPerPage);
			parameters.Add ("@introText", introText);
			parameters.Add ("@cloudTitle", cloudTitle);
			parameters.Add ("@modulePageId", modulePageId);
			parameters.Add ("@hideCloud", hideCloud? 1: 0);
			parameters.Add ("@assetsPerRow", assetsPerRow);
			parameters.Add ("@infoPlacement", infoPlacement ? 1:0);
			parameters.Add ("@maxCloudLength", maxCloudLength);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.CONTEST_SUBMISSIONS);
		}

		/// <summary>
		/// delete a ContestSubmissions widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleContestSubmissions (int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_blogs table
			string sqlString = "DELETE FROM layout_module_contest_submissions " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		#endregion
		
		#region Contest Upload

		/// <summary>
		/// Get a contest upload widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleContestUpload(int id)
		{
			string sqlSelect = " SELECT cu.title, cu.body_text, cu.description_text, cu.module_page_id, cu.terms_text, cu.results_url, m.page_id, " +
				" cu.media_upload_label, cu.artist_name_label, cu.media_title_label, cu.keyword_label, cu.photo_upload_label, cu.contest_id, " +
				" cu.allow_video, cu.allow_photos, cu.allow_music, cu.allow_games, cu.not_logged_in_HTML, cu.submissions_end_HTML, cu.voting_ended_HTML, " +
				" cu.show_media_description, cu.media_description_label " + 
				" FROM layout_module_contest_upload cu" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = cu.module_page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.CONTEST_UPLOAD);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a contest upload row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleContestUpload(int modulePageId)
		{
			string sqlSelect = " SELECT title, contest_id, description_text, terms_text, results_url, body_text, " +
				" media_upload_label, artist_name_label, media_title_label, keyword_label, photo_upload_label, " +
				" allow_video, allow_photos, allow_music, allow_games, not_logged_in_HTML, submissions_end_HTML, voting_ended_HTML, " +
				" show_media_description, media_description_label " + 
				" FROM layout_module_contest_upload " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}
		
		/// <summary>
		/// insert a record to layout_module_html table and return the module_page_id
		/// </summary>
		/// <param name="title_txt"></param>
		/// <param name="body_text"></param>
		/// <returns></returns>						  
		public static int InsertLayoutModuleContestUpload (string title, int contest_id, string desc, string terms, string body_right, string url, 
			string not_logged_in_HTML, string submissions_end_HTML, string voting_ended_HTML, bool allow_games, bool allow_music, bool allow_photos,
			bool allow_video, string photo_upload_label, string keyword_label, string media_title_label, string artist_name_label,
			string media_upload_label, bool show_media_description, string media_description_label)
		{
			string query = " INSERT INTO layout_module_contest_upload " + 
				" (title, contest_id, description_text, terms_text, body_text, results_url, not_logged_in_HTML, submissions_end_HTML, " +
				" voting_ended_HTML, allow_games, allow_music, allow_photos, allow_video, photo_upload_label, keyword_label, media_title_label, " +
				" artist_name_label, media_upload_label, show_media_description, media_description_label) " +
				" VALUES(@title, @contest_id, @desc, @terms, @body_right, @url, @not_logged_in_HTML, @submissions_end_HTML, " + 
				" @voting_ended_HTML, @allow_games, @allow_music, @allow_photos, @allow_video, @photo_upload_label, @keyword_label, @media_title_label, " + 
				" @artist_name_label, @media_upload_label, @show_media_description, @media_description_label) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@contest_id", contest_id);
			parameters.Add ("@desc", desc);
			parameters.Add ("@terms", terms);
			parameters.Add ("@body_right", body_right);
			parameters.Add ("@url", url);
			parameters.Add ("@not_logged_in_HTML", not_logged_in_HTML);
			parameters.Add ("@submissions_end_HTML", submissions_end_HTML);
			parameters.Add ("@voting_ended_HTML", voting_ended_HTML);
			parameters.Add ("@allow_games", allow_games ? 1 : 0);
			parameters.Add ("@allow_music", allow_music ? 1 : 0);
			parameters.Add ("@allow_photos", allow_photos ? 1 : 0);
			parameters.Add ("@allow_video", allow_video ? 1 : 0);	   
			parameters.Add ("@photo_upload_label", photo_upload_label);
			parameters.Add ("@keyword_label", keyword_label);
			parameters.Add ("@media_title_label", media_title_label);
			parameters.Add ("@artist_name_label", artist_name_label);
			parameters.Add ("@media_upload_label", media_upload_label);
			parameters.Add ("@show_media_description", show_media_description ? 1 : 0);
			parameters.Add ("@media_description_label", media_description_label);

			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.CONTEST_UPLOAD);
			return retVal;
		}
																			
		public static void SaveLayoutModuleContestUpload(int modulePageId, int contest_id, string title, string desc_text, string terms_text, 
			string body_right_text, string results_url, string media_upload_label, string artist_name_label, string media_title_label,
			string keyword_label, string photo_upload_label, bool allow_video, bool allow_photos, bool allow_music, bool allow_games,
			string not_logged_in_HTML, string submissions_end_HTML, string voting_ended_HTML, bool show_media_description, 
			string media_description_label)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_contest_upload ");
			sb.Append(" SET title = @title, ");
			sb.Append(" contest_id = @contest_id, ");
			sb.Append(" body_text = @body_right_text, ");
			sb.Append(" description_text = @desc_text, ");
			sb.Append(" terms_text = @terms_text, ");
			sb.Append(" results_url = @results_url, ");

			sb.Append(" media_upload_label = @media_upload_label, ");
			sb.Append(" artist_name_label = @artist_name_label, ");
			sb.Append(" media_title_label = @media_title_label, ");
			sb.Append(" keyword_label = @keyword_label, ");
			sb.Append(" photo_upload_label = @photo_upload_label, ");

			sb.Append(" allow_video = @allow_video, ");
			sb.Append(" allow_photos = @allow_photos, ");
			sb.Append(" allow_music = @allow_music, ");
			sb.Append(" allow_games = @allow_games, ");

			sb.Append(" not_logged_in_HTML = @not_logged_in_HTML, ");
			sb.Append(" submissions_end_HTML = @submissions_end_HTML, ");
			sb.Append(" voting_ended_HTML = @voting_ended_HTML, ");
			sb.Append(" show_media_description = @show_media_description, ");
			sb.Append(" media_description_label = @media_description_label ");

			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@contest_id", contest_id);
			parameters.Add ("@body_right_text", body_right_text);
			parameters.Add ("@desc_text", desc_text);
			parameters.Add ("@terms_text", terms_text);
			parameters.Add ("@results_url", results_url);
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@media_upload_label", media_upload_label);
			parameters.Add ("@artist_name_label", artist_name_label);
			parameters.Add ("@media_title_label", media_title_label);
			parameters.Add ("@keyword_label", keyword_label);
			parameters.Add ("@photo_upload_label", photo_upload_label);
			parameters.Add ("@allow_video", allow_video ? 1 : 0);	   
			parameters.Add ("@allow_photos", allow_photos ? 1 : 0);
			parameters.Add ("@allow_music", allow_music ? 1 : 0);
			parameters.Add ("@allow_games", allow_games ? 1 : 0);
			parameters.Add ("@not_logged_in_HTML", not_logged_in_HTML);
			parameters.Add ("@submissions_end_HTML", submissions_end_HTML);
			parameters.Add ("@voting_ended_HTML", voting_ended_HTML);
			parameters.Add ("@show_media_description", show_media_description ? 1 : 0);
			parameters.Add ("@media_description_label", media_description_label);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.CONTEST_UPLOAD);
		}

		/// <summary>
		/// delete a contest upload widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleContestUpload(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_html table
			string sqlString = "DELETE FROM layout_module_contest_upload " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		#endregion

		#region Contest Get Raved

		/// <summary>
		/// Get a GetModuleContestGetRaved widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleContestGetRaved (int id)
		{
			string sqlSelect = " SELECT s.title, s.contest_id, intro_text, banner_image, banner_text, contest_url, login_message, no_assets_message, " +
				" no_assets_html, have_assets_html, not_loggedin_html, banner_image_not_logged_in " +
				" FROM layout_module_contest_get_raved s" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = s.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.CONTEST_GET_RAVED);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a ContestGetRaved row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleContestGetRaved (int modulePageId)
		{
			string sqlSelect = " SELECT title, contest_id, intro_text, banner_image, banner_text, contest_url, login_message, no_assets_message, no_assets_html, have_assets_html, not_loggedin_html, banner_image_not_logged_in " +
				" FROM layout_module_contest_get_raved " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}
		
		/// <summary>
		/// insert a record to layout_module_contest_get_raved table and return the module_page_id
		/// </summary>
		public static int InsertLayoutModuleContestGetRaved (string title, int contestId, string introText, string bannerImage, string bannerText, string contestURL, string loginMessage, string noAssetsMessage, 
			string no_assets_html, string have_assets_html, string not_loggedin_html, string banner_image_not_logged_in)
		{
			string query = " INSERT INTO layout_module_contest_get_raved " + 
				" (title, contest_id, intro_text, banner_image, banner_text, contest_url, login_message, no_assets_message, no_assets_html, have_assets_html, not_loggedin_html, banner_image_not_logged_in) " +
				" VALUES (@title, @contestId, @introText, @bannerImage, @bannerText, @contestURL, @loginMessage, @noAssetsMessage, @no_assets_html, @have_assets_html, @not_loggedin_html, @banner_image_not_logged_in) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@introText", introText);
			parameters.Add ("@contestId", contestId);
			parameters.Add ("@bannerImage", bannerImage);
			parameters.Add ("@bannerText", bannerText);
			parameters.Add ("@loginMessage", loginMessage);
			parameters.Add ("@noAssetsMessage", noAssetsMessage);
			parameters.Add ("@no_assets_html", no_assets_html);
			parameters.Add ("@have_assets_html", have_assets_html);
			parameters.Add ("@not_loggedin_html", not_loggedin_html);
			parameters.Add ("@banner_image_not_logged_in", banner_image_not_logged_in);
			parameters.Add ("@contestURL", contestURL);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.CONTEST_SUBMISSIONS);
			return retVal;
		}

		public static void SaveLayoutModuleContestGetRaved (int modulePageId, int contestId, string title, 
			string introText, string bannerImage, string bannerText, string contestURL, string loginMessage, string noAssetsMessage,
			string no_assets_html, string have_assets_html, string not_loggedin_html, string banner_image_not_logged_in)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_contest_get_raved ");
			sb.Append(" SET title = @title, contest_id = @contestId, intro_text = @introText,");
			sb.Append("banner_image = @bannerImage, banner_text = @bannerText, contest_url = @contestURL, login_message = @loginMessage, no_assets_message = @noAssetsMessage, " +
				" no_assets_html = @no_assets_html, have_assets_html = @have_assets_html, not_loggedin_html = @not_loggedin_html, banner_image_not_logged_in = @banner_image_not_logged_in");
			sb.Append(" WHERE module_page_id = @modulePageId ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@contestId", contestId);
			parameters.Add ("@introText", introText);
			parameters.Add ("@modulePageId", modulePageId);
			parameters.Add ("@bannerImage", bannerImage);
			parameters.Add ("@bannerText", bannerText);
			parameters.Add ("@contestURL", contestURL);
			parameters.Add ("@loginMessage", loginMessage);
			parameters.Add ("@noAssetsMessage", noAssetsMessage);
			parameters.Add ("@no_assets_html", no_assets_html);
			parameters.Add ("@have_assets_html", have_assets_html);
			parameters.Add ("@not_loggedin_html", not_loggedin_html);
			parameters.Add ("@banner_image_not_logged_in", banner_image_not_logged_in);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.CONTEST_SUBMISSIONS);
		}

		/// <summary>
		/// delete a GetRaved widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleContestGetRaved (int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_blogs table
			string sqlString = "DELETE FROM layout_module_contest_get_raved " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		#endregion

		#region Contest Profile Header

		/// <summary>
		/// load a profile header row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutProfileHeader(int modulePageId)
		{
			string sqlSelect = " SELECT text, show_menu, banner_path, show_join_channel, show_quit_channel, " +
				" show_send_message, show_rave_it, show_report_abuse, show_tell_it, show_submit_media " + 
				" FROM layout_module_contest_profile_header " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// return's the default profile header for a page
		/// </summary>
		/// <param name="pageId"></param>
		/// <returns></returns>
		public static string GetDefaultProfileHeader(int pageId)
		{
			DataRow drUser = UsersUtility.GetUserByPageId(pageId);
			string retVal = null;
			if(drUser != null)
			{
				retVal = drUser["username"].ToString();
			}

			DataRow drChannel = PageUtility.GetChannelByPageId(pageId);
			if(drChannel != null)
			{
				retVal = drChannel["name"].ToString();
			}
			return retVal;
		}

		/// <summary>
		/// Get a System Profile Header widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleProfileHeader (int id)
		{
			string sqlSelect = " " + 
				" SELECT mt.module_page_id, lp.channel_id, c.name, c.allow_publishing, c.name_no_spaces, c.url, " +
				" mt.text, mt.show_menu, mt.banner_path, " +
				" mt.show_join_channel, mt.show_quit_channel, mt.show_send_message, mt.show_rave_it, " +
				" mt.show_report_abuse, mt.show_tell_it, mt.show_submit_media, " +
				" cs.number_of_diggs " +
				" FROM layout_module_contest_profile_header mt" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = mt.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" INNER JOIN communities c ON c.community_id = lp.channel_id " +
				" INNER JOIN channel_stats cs ON cs.channel_id = lp.channel_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.CONTEST_PROFILE_HEADER);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// Get a System Profile Header widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleProfileHeader(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_contest_profile_header table
			string sqlString = "DELETE FROM layout_module_contest_profile_header " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// insert or a record to layout_module_contest_profile_header table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleProfileHeader( string text, bool show_menu, string bannerPath,  
			bool JoinChannel, bool QuitChannel, bool ReportAbuse, bool SendMessage, 
			bool SubmitMedia, bool TellIt, bool Vote)
		{
			string query = " INSERT INTO layout_module_contest_profile_header " + 
				" (text, show_menu, banner_path, show_join_channel, show_quit_channel, show_send_message, show_rave_it, " +
				" show_report_abuse, show_tell_it, show_submit_media ) " +
				" VALUES(@text, @show_menu, @bannerPath, @JoinChannel, @QuitChannel, @SendMessage, @Vote, " +
				" @ReportAbuse, @TellIt, @SubmitMedia ) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@text", text);
			parameters.Add ("@show_menu", show_menu ? 1 : 0);
			parameters.Add ("@bannerPath", bannerPath);
			parameters.Add ("@JoinChannel", JoinChannel ? 1 : 0);
			parameters.Add ("@QuitChannel", QuitChannel ? 1 : 0);
			parameters.Add ("@SendMessage", SendMessage ? 1 : 0);
			parameters.Add ("@Vote", Vote ? 1 : 0);
			parameters.Add ("@ReportAbuse", ReportAbuse ? 1 : 0);
			parameters.Add ("@TellIt", TellIt ? 1 : 0);
			parameters.Add ("@SubmitMedia", SubmitMedia ? 1 : 0);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.CONTEST_PROFILE_HEADER);
			return retVal;
		}

		/// <summary>
		/// save a System Stats widget
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static void SaveLayoutModuleProfileHeader( int modulePageId, string text, bool show_menu,
			bool removeBanner, bool JoinChannel, bool QuitChannel, bool ReportAbuse, bool SendMessage, 
			bool SubmitMedia, bool TellIt, bool Vote)
		{
			Hashtable parameters = new Hashtable ();			

			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_contest_profile_header ");
			sb.Append(" SET text = @text, ");
			sb.Append(" show_menu = @show_menu, ");
			sb.Append(" show_join_channel = @JoinChannel, ");
			sb.Append(" show_quit_channel = @QuitChannel, ");
			sb.Append(" show_send_message = @SendMessage, ");
			sb.Append(" show_rave_it = @Vote, ");
			sb.Append(" show_report_abuse = @ReportAbuse, ");
			sb.Append(" show_tell_it = @TellIt, ");
			sb.Append(" show_submit_media = @SubmitMedia ");
			sb.Append(" WHERE module_page_id = @module_page_id ");
	
			parameters.Add ("@text", text);
			parameters.Add ("@show_menu", show_menu ? 1 : 0);
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@JoinChannel", JoinChannel ? 1 : 0);
			parameters.Add ("@QuitChannel", QuitChannel ? 1 : 0);
			parameters.Add ("@SendMessage", SendMessage ? 1 : 0);
			parameters.Add ("@Vote", Vote ? 1 : 0);
			parameters.Add ("@ReportAbuse", ReportAbuse ? 1 : 0);
			parameters.Add ("@TellIt", TellIt ? 1 : 0);
			parameters.Add ("@SubmitMedia", SubmitMedia ? 1 : 0);
			
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.TITLE_TEXT);

		}

		#endregion

		#region Community Top Banner

		/// <summary>
		/// load a profile header row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutTopBanner(int modulePageId)
		{
			string sqlSelect = " SELECT text, show_menu, banner_path, show_join_channel, show_quit_channel, " +
				" show_send_message, show_rave_it, show_report_abuse, show_tell_it, show_submit_media " + 
				" FROM layout_module_contest_profile_header " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// return's the default profile header for a page
		/// </summary>
		/// <param name="pageId"></param>
		/// <returns></returns>
		public static string GetDefaultTopBanner(int pageId)
		{
			DataRow drUser = UsersUtility.GetUserByPageId(pageId);
			string retVal = null;
			if(drUser != null)
			{
				retVal = drUser["username"].ToString();
			}

			DataRow drChannel = PageUtility.GetChannelByPageId(pageId);
			if(drChannel != null)
			{
				retVal = drChannel["name"].ToString();
			}
			return retVal;
		}

		/// <summary>
		/// Get a System Profile Header widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleTopBanner (int id)
		{
			string sqlSelect = " " + 
				" SELECT mt.module_page_id, lp.channel_id, c.name, c.allow_publishing, c.name_no_spaces, c.url, " +
				" mt.text, mt.show_menu, mt.banner_path, " +
				" mt.show_join_channel, mt.show_quit_channel, mt.show_send_message, mt.show_rave_it, " +
				" mt.show_report_abuse, mt.show_tell_it, mt.show_submit_media, " +
				" cs.number_of_diggs " +
				" FROM layout_module_contest_profile_header mt" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = mt.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" INNER JOIN communities c ON c.community_id = lp.channel_id " +
				" INNER JOIN channel_stats cs ON cs.channel_id = lp.channel_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.COMMUNITY_TOP_BANNER);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// Get a System Profile Header widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleTopBanner(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_contest_profile_header table
			string sqlString = "DELETE FROM layout_module_contest_profile_header " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// insert or a record to layout_module_contest_profile_header table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleTopBanner( string text, bool show_menu, string bannerPath,  
			bool JoinChannel, bool QuitChannel, bool ReportAbuse, bool SendMessage, 
			bool SubmitMedia, bool TellIt, bool Vote)
		{
			string query = " INSERT INTO layout_module_contest_profile_header " + 
				" (text, show_menu, banner_path, show_join_channel, show_quit_channel, show_send_message, show_rave_it, " +
				" show_report_abuse, show_tell_it, show_submit_media ) " +
				" VALUES(@text, @show_menu, @bannerPath, @JoinChannel, @QuitChannel, @SendMessage, @Vote, " +
				" @ReportAbuse, @TellIt, @SubmitMedia ) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@text", text);
			parameters.Add ("@show_menu", show_menu ? 1 : 0);
			parameters.Add ("@bannerPath", bannerPath);
			parameters.Add ("@JoinChannel", JoinChannel ? 1 : 0);
			parameters.Add ("@QuitChannel", QuitChannel ? 1 : 0);
			parameters.Add ("@SendMessage", SendMessage ? 1 : 0);
			parameters.Add ("@Vote", Vote ? 1 : 0);
			parameters.Add ("@ReportAbuse", ReportAbuse ? 1 : 0);
			parameters.Add ("@TellIt", TellIt ? 1 : 0);
			parameters.Add ("@SubmitMedia", SubmitMedia ? 1 : 0);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.COMMUNITY_TOP_BANNER);
			return retVal;
		}

		/// <summary>
		/// save a System Stats widget
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static void SaveLayoutModuleTopBanner( int modulePageId, string text, bool show_menu,
			bool removeBanner, bool JoinChannel, bool QuitChannel, bool ReportAbuse, bool SendMessage, 
			bool SubmitMedia, bool TellIt, bool Vote)
		{
			Hashtable parameters = new Hashtable ();			

			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_contest_profile_header ");
			sb.Append(" SET text = @text, ");
			sb.Append(" show_menu = @show_menu, ");
			sb.Append(" show_join_channel = @JoinChannel, ");
			sb.Append(" show_quit_channel = @QuitChannel, ");
			sb.Append(" show_send_message = @SendMessage, ");
			sb.Append(" show_rave_it = @Vote, ");
			sb.Append(" show_report_abuse = @ReportAbuse, ");
			sb.Append(" show_tell_it = @TellIt, ");
			sb.Append(" show_submit_media = @SubmitMedia ");
			sb.Append(" WHERE module_page_id = @module_page_id ");
	
			parameters.Add ("@text", text);
			parameters.Add ("@show_menu", show_menu ? 1 : 0);
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@JoinChannel", JoinChannel ? 1 : 0);
			parameters.Add ("@QuitChannel", QuitChannel ? 1 : 0);
			parameters.Add ("@SendMessage", SendMessage ? 1 : 0);
			parameters.Add ("@Vote", Vote ? 1 : 0);
			parameters.Add ("@ReportAbuse", ReportAbuse ? 1 : 0);
			parameters.Add ("@TellIt", TellIt ? 1 : 0);
			parameters.Add ("@SubmitMedia", SubmitMedia ? 1 : 0);
			
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.TITLE_TEXT);

		}

		#endregion

		#region Leader Board

		/// <summary>
		/// load a System Stats row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleLeaderBoard(int modulePageId)
		{
			string sqlSelect = " SELECT title, header, description, leader_config_id, number_of_results, footer, show_pimp_page, show_pimp_profile, show_channel_members," + 
				" show_friend_accepts, show_raves_your_profile, show_raves_channel, show_join_wok, show_channels_created," +
				" show_raves_media, show_photos_uploaded, show_video_uploaded, show_music_uploaded," + 
				" show_people_invited, show_user_image, time_period_id, show_user_name, show_user_points " +
				" FROM layout_module_leader_board lmlb " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// insert a record to layout_module_leader_board table and return the module_page_id
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleLeaderBoard(string title, string header, string description, 
			int leader_config_id, int number_of_results, string footer, bool show_pimp_page, bool show_pimp_profile, bool show_channel_members, bool show_friend_accepts,
			bool show_raves_your_profile, bool show_raves_channel, bool show_join_wok, bool show_channels_created,
			bool show_raves_media, bool show_photos_uploaded, bool show_video_uploaded, bool show_music_uploaded,
			bool show_people_invited, bool show_user_image, int time_period_id, bool show_user_name, bool show_user_points,int leaderBoard_Type )
		{

			string query = " INSERT INTO layout_module_leader_board " + 
				" (title, header, description, leader_config_id, number_of_results, footer, show_pimp_page, show_pimp_profile, show_channel_members," + 
				" show_friend_accepts, show_raves_your_profile, show_raves_channel, show_join_wok, show_channels_created," +
				" show_raves_media, show_photos_uploaded, show_video_uploaded, show_music_uploaded," + 
				" show_people_invited, show_user_image, time_period_id, show_user_name, show_user_points ) " +
				" VALUES(@title, @header, @description, @leader_config_id, @number_of_results, @footer, @show_pimp_page, @show_pimp_profile, @show_channel_members," + 
				" @show_friend_accepts, @show_raves_your_profile, @show_raves_channel, @show_join_wok, @show_channels_created," +
				" @show_raves_media, @show_photos_uploaded, @show_video_uploaded, @show_music_uploaded," + 
				" @show_people_invited, @show_user_image, @time_period_id, @show_user_name, @show_user_points ) ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@title", title);
			parameters.Add ("@header", header);
			parameters.Add ("@description", description);
			parameters.Add ("@leader_config_id", leader_config_id);
			parameters.Add ("@number_of_results", number_of_results);
			parameters.Add ("@footer", footer);
			parameters.Add ("@show_pimp_page", show_pimp_page ? 1 : 0);
			parameters.Add ("@show_pimp_profile", show_pimp_profile ? 1 : 0);
			parameters.Add ("@show_channel_members", show_channel_members ? 1 : 0);
			parameters.Add ("@show_friend_accepts", show_friend_accepts ? 1 : 0);
			parameters.Add ("@show_raves_your_profile", show_raves_your_profile ? 1 : 0);
			parameters.Add ("@show_raves_channel", show_raves_channel ? 1 : 0);
			parameters.Add ("@show_join_wok", show_join_wok ? 1 : 0);
			parameters.Add ("@show_channels_created", show_channels_created ? 1 : 0);
			parameters.Add ("@show_raves_media", show_raves_media ? 1 : 0);
			parameters.Add ("@show_photos_uploaded", show_photos_uploaded ? 1 : 0);
			parameters.Add ("@show_video_uploaded", show_video_uploaded ? 1 : 0);
			parameters.Add ("@show_music_uploaded", show_music_uploaded ? 1 : 0);
			parameters.Add ("@show_people_invited", show_people_invited ? 1 : 0);
			parameters.Add ("@show_user_image", show_user_image ? 1 : 0);
			parameters.Add ("@time_period_id", time_period_id);
			parameters.Add ("@show_user_name", show_user_name ? 1 : 0);
			parameters.Add ("@show_user_points", show_user_points ? 1 : 0);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);
			
			switch(leaderBoard_Type)
			{
				case (int)Constants.eMODULE_TYPE.LEADER_BOARD:
					LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.LEADER_BOARD);
					break;
				case (int)Constants.eMODULE_TYPE.KANEVA_LEADER_BOARD:
					LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.KANEVA_LEADER_BOARD);
					break;
			}
			return retVal;
		}

		/// <summary>
		/// save changes to the layout_module_leader_board
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		/// 
		public static void SaveLayoutModuleLeaderBoard(int modulePageId, string title, string header, string description, 
			int leader_config_id, int number_of_results, string footer, bool show_pimp_page, bool show_pimp_profile, bool show_channel_members, bool show_friend_accepts,
			bool show_raves_your_profile, bool show_raves_channel, bool show_join_wok, bool show_channels_created,
			bool show_raves_media, bool show_photos_uploaded, bool show_video_uploaded, bool show_music_uploaded,
			bool show_people_invited, bool show_user_image, int time_period_id, bool show_user_name, bool show_user_points, int leaderBoard_Type )
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_leader_board ");
			sb.Append(" SET title = @title, ");
			sb.Append(" header = @header, ");
			sb.Append(" description = @description, ");
			sb.Append(" leader_config_id = @leader_config_id, ");
			sb.Append(" number_of_results = @number_of_results, ");
			sb.Append(" footer = @footer, ");
			sb.Append(" show_pimp_page = @show_pimp_page, ");
			sb.Append(" show_pimp_profile = @show_pimp_profile, ");
			sb.Append(" show_channel_members = @show_channel_members, ");
			sb.Append(" show_friend_accepts = @show_friend_accepts, ");
			sb.Append(" show_raves_your_profile = @show_raves_your_profile, ");
			sb.Append(" show_raves_channel = @show_raves_channel, ");
			sb.Append(" show_join_wok = @show_join_wok, ");
			sb.Append(" show_channels_created = @show_channels_created, ");
			sb.Append(" show_raves_media = @show_raves_media, ");
			sb.Append(" show_photos_uploaded = @show_photos_uploaded, ");
			sb.Append(" show_video_uploaded = @show_video_uploaded, ");
			sb.Append(" show_music_uploaded = @show_music_uploaded, ");
			sb.Append(" show_people_invited = @show_people_invited, ");
			sb.Append(" show_user_image = @show_user_image, ");
			sb.Append(" time_period_id = @time_period_id, ");
			sb.Append(" show_user_name = @show_user_name, ");
			sb.Append(" show_user_points = @show_user_points ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@title", title);
			parameters.Add ("@header", header);
			parameters.Add ("@description", description);
			parameters.Add ("@leader_config_id", leader_config_id);
			parameters.Add ("@number_of_results", number_of_results);
			parameters.Add ("@footer", footer);
			parameters.Add ("@show_pimp_page", show_pimp_page ? 1 : 0);
			parameters.Add ("@show_pimp_profile", show_pimp_profile ? 1 : 0);
			parameters.Add ("@show_channel_members", show_channel_members ? 1 : 0);
			parameters.Add ("@show_friend_accepts", show_friend_accepts ? 1 : 0);
			parameters.Add ("@show_raves_your_profile", show_raves_your_profile ? 1 : 0);
			parameters.Add ("@show_raves_channel", show_raves_channel ? 1 : 0);
			parameters.Add ("@show_join_wok", show_join_wok ? 1 : 0);
			parameters.Add ("@show_channels_created", show_channels_created ? 1 : 0);
			parameters.Add ("@show_raves_media", show_raves_media ? 1 : 0);
			parameters.Add ("@show_photos_uploaded", show_photos_uploaded ? 1 : 0);
			parameters.Add ("@show_video_uploaded", show_video_uploaded ? 1 : 0);
			parameters.Add ("@show_music_uploaded", show_music_uploaded ? 1 : 0);
			parameters.Add ("@show_people_invited", show_people_invited ? 1 : 0);
			parameters.Add ("@show_user_image", show_user_image ? 1 : 0);
			parameters.Add ("@time_period_id", time_period_id);
			parameters.Add ("@show_user_name", show_user_name ? 1 : 0);
			parameters.Add ("@show_user_points", show_user_points ? 1 : 0);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			switch(leaderBoard_Type)
			{
				case (int)Constants.eMODULE_TYPE.LEADER_BOARD:
					LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.LEADER_BOARD);
					break;
				case (int)Constants.eMODULE_TYPE.KANEVA_LEADER_BOARD:
					LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.KANEVA_LEADER_BOARD);
					break;
			}

		}

		/// <summary>
		/// delete a title widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleLeaderBoard(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_channel_control_panel table
			string sqlString = "DELETE FROM layout_module_leader_board " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// Gets the modules configuration for the view page
		/// </summary>
		/// <param name="id">the modele id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleLeaderBoard(int id, int leaderBoard_Type)
		{
			string sqlSelect = " SELECT lb.title, m.page_id, header, lb.description, leader_config_id, number_of_results, footer, show_pimp_page, show_pimp_profile, show_channel_members," + 
				" show_friend_accepts, show_raves_your_profile, show_raves_channel, show_join_wok, show_channels_created," +
				" show_raves_media, show_photos_uploaded, show_video_uploaded, show_music_uploaded," + 
				" show_people_invited, show_user_image, period_in_days, show_user_name, show_user_points " + 
				" FROM layout_module_leader_board lb" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = lb.module_page_id " +
				" LEFT JOIN leader_board_time_periods lbtp ON lb.time_period_id = lbtp.time_period_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);

			switch(leaderBoard_Type)
			{
				case (int)Constants.eMODULE_TYPE.LEADER_BOARD:
					parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.LEADER_BOARD);
					break;
				case (int)Constants.eMODULE_TYPE.KANEVA_LEADER_BOARD:
					parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.KANEVA_LEADER_BOARD);
					break;
			}

			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// Gets the modules configuration options for the leader board comparisons
		/// </summary>
		/// <returns></returns>
		public static DataTable GetLeaderBoardConfigurationOptions()
		{
			string sqlSelect = " SELECT leader_config_id, config_option_description " +
				" FROM leader_board_configuration_options lbco"; 

			return  KanevaGlobals.GetDatabaseUtility ().GetDataTable(sqlSelect);
		}

		/// <summary>
		/// Gets the time period options for the leader board comparisons
		/// </summary>
		/// <returns></returns>
		public static DataTable GetLeaderBoardTimePeriods()
		{
			string sqlSelect = " SELECT time_period_id, period_in_days, description " +
				" FROM leader_board_time_periods lbtp"; 

			return  KanevaGlobals.GetDatabaseUtility ().GetDataTable(sqlSelect);
		}

		#endregion 

		#region Hot New Stuff

		/// <summary>
		/// load the current hot stuff setup
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleHotNewStuff(int modulePageId)
		{
			string sqlSelect = " SELECT title, header, footer, show_mature, time_period_id," + 
				" media_per_page, media_per_row, media_location, image_size " +
				" FROM layout_module_hotnew_stuff lmhs " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// insert a record into layout module hot new stuff table
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleHotNewStuff(string title, string header, string footer, bool show_mature,
			int time_period_id, int media_per_page, int media_per_row, bool media_location, int imagesize)
		{

			string query = " INSERT INTO layout_module_hotnew_stuff " + 
				" (title, header, footer, show_mature, time_period_id, media_per_page, media_per_row, media_location, image_size) " +
				" VALUES(@title, @header, @footer, @show_mature, @time_period_id, @media_per_page," + 
				" @media_per_row, @media_location, @imagesize) ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@title", title);
			parameters.Add ("@header", header);
			parameters.Add ("@footer", footer);
			parameters.Add ("@show_mature", show_mature ? 1 : 0);
			parameters.Add ("@time_period_id", time_period_id);
			parameters.Add ("@media_per_page", media_per_page);
			parameters.Add ("@media_per_row", media_per_row);
			parameters.Add ("@media_location", media_location ? 1 : 0);
			parameters.Add ("@imagesize", imagesize);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.HOT_NEW_STUFF);
			return retVal;
		}

		/// <summary>
		/// save changes to the layout_module_leader_board
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		/// 
		public static void SaveLayoutModuleHotNewStuff(int modulePageId, string title, string header, string footer, bool show_mature,
			int time_period_id, int media_per_page, int media_per_row, bool media_location, int imagesize)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_hotnew_stuff ");
			sb.Append(" SET title = @title, ");
			sb.Append(" header = @header, ");
			sb.Append(" footer = @footer, ");
			sb.Append(" show_mature = @show_mature, ");
			sb.Append(" time_period_id = @time_period_id, ");
			sb.Append(" media_per_page = @media_per_page, ");
			sb.Append(" media_per_row = @media_per_row, ");
			sb.Append(" media_location = @media_location, ");
			sb.Append(" image_size = @imagesize ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@title", title);
			parameters.Add ("@header", header);
			parameters.Add ("@footer", footer);
			parameters.Add ("@show_mature", show_mature ? 1 : 0);
			parameters.Add ("@time_period_id", time_period_id);
			parameters.Add ("@media_per_page", media_per_page);
			parameters.Add ("@media_per_row", media_per_row);
			parameters.Add ("@media_location", media_location ? 1 : 0);
			parameters.Add ("@imagesize", imagesize);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.HOT_NEW_STUFF);
		}

		/// <summary>
		/// delete a title widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleHotNewStuff(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_channel_control_panel table
			string sqlString = "DELETE FROM layout_module_hotnew_stuff " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// Gets the modules configuration for the view page
		/// </summary>
		/// <param name="id">the modele id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleHotNewStuff(int id)
		{
			string sqlSelect = " SELECT m.page_id, title, header, footer, show_mature, period_in_days," + 
				" media_per_page, media_per_row, media_location, hstp.description, lmhs.image_size " +
				" FROM layout_module_hotnew_stuff lmhs " +
				" INNER JOIN layout_page_modules m ON m.module_page_id = lmhs.module_page_id " +
				" LEFT JOIN hotnew_stuff_time_periods hstp ON lmhs.time_period_id = hstp.time_period_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.HOT_NEW_STUFF);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// Gets the time period options for the hot new stuff widget
		/// </summary>
		/// <returns></returns>
		public static DataTable GetHotNewStuffTimePeriods()
		{
			string sqlSelect = " SELECT time_period_id, period_in_days, description " +
				" FROM hotnew_stuff_time_periods hstp"; 

			return  KanevaGlobals.GetDatabaseUtility ().GetDataTable(sqlSelect);
		}


		#endregion 

		#region Newest Media

		/// <summary>
		/// load the current hot stuff setup
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleNewestMedia(int modulePageId)
		{
			string sqlSelect = " SELECT title, header, footer, show_mature, time_period_id," + 
				" media_per_page, media_per_row, media_location, image_size " +
				" FROM layout_module_newest_media lmnm " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// insert a record into layout module hot new stuff table
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleNewestMedia(string title, string header, string footer, bool show_mature,
			int time_period_id, int media_per_page, int media_per_row, bool media_location, int imagesize)
		{

			string query = " INSERT INTO layout_module_newest_media " + 
				" (title, header, footer, show_mature, time_period_id, media_per_page, media_per_row, media_location, image_size) " +
				" VALUES(@title, @header, @footer, @show_mature, @time_period_id, @media_per_page," + 
				" @media_per_row, @media_location, @imagesize) ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@title", title);
			parameters.Add ("@header", header);
			parameters.Add ("@footer", footer);
			parameters.Add ("@show_mature", show_mature ? 1 : 0);
			parameters.Add ("@time_period_id", time_period_id);
			parameters.Add ("@media_per_page", media_per_page);
			parameters.Add ("@media_per_row", media_per_row);
			parameters.Add ("@media_location", media_location ? 1 : 0);
			parameters.Add ("@imagesize", imagesize);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.NEWEST_MEDIA);
			return retVal;
		}

		/// <summary>
		/// save changes to the layout_module_leader_board
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		/// 
		public static void SaveLayoutModuleNewestMedia(int modulePageId, string title, string header, string footer, bool show_mature,
			int time_period_id, int media_per_page, int media_per_row, bool media_location, int imagesize)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_newest_media ");
			sb.Append(" SET title = @title, ");
			sb.Append(" header = @header, ");
			sb.Append(" footer = @footer, ");
			sb.Append(" show_mature = @show_mature, ");
			sb.Append(" time_period_id = @time_period_id, ");
			sb.Append(" media_per_page = @media_per_page, ");
			sb.Append(" media_per_row = @media_per_row, ");
			sb.Append(" media_location = @media_location, ");
			sb.Append(" image_size = @imagesize ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@title", title);
			parameters.Add ("@header", header);
			parameters.Add ("@footer", footer);
			parameters.Add ("@show_mature", show_mature ? 1 : 0);
			parameters.Add ("@time_period_id", time_period_id);
			parameters.Add ("@media_per_page", media_per_page);
			parameters.Add ("@media_per_row", media_per_row);
			parameters.Add ("@media_location", media_location ? 1 : 0);
			parameters.Add ("@imagesize", imagesize);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.NEWEST_MEDIA);
		}

		/// <summary>
		/// delete a title widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleNewestMedia(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_channel_control_panel table
			string sqlString = "DELETE FROM layout_module_newest_media " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// Gets the modules configuration for the view page
		/// </summary>
		/// <param name="id">the modele id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleNewestMedia(int id)
		{
			string sqlSelect = " SELECT m.page_id, title, header, footer, show_mature, period_in_days," + 
				" media_per_page, media_per_row, media_location, hstp.description, lmhs.image_size " +
				" FROM layout_module_newest_media lmhs " +
				" INNER JOIN layout_page_modules m ON m.module_page_id = lmhs.module_page_id " +
				" LEFT JOIN hotnew_stuff_time_periods hstp ON lmhs.time_period_id = hstp.time_period_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.NEWEST_MEDIA);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// Gets the time period options for the hot new stuff widget
		/// </summary>
		/// <returns></returns>
		public static DataTable GetNewestMediaTimePeriods()
		{
			string sqlSelect = " SELECT time_period_id, period_in_days, description " +
				" FROM hotnew_stuff_time_periods hstp"; 

			return  KanevaGlobals.GetDatabaseUtility ().GetDataTable(sqlSelect);
		}


		#endregion 

		#region Most Viewed Media

		/// <summary>
		/// load the current hot stuff setup
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleMostViewedMedia(int modulePageId)
		{
			string sqlSelect = " SELECT title, header, footer, show_mature, time_period_id," + 
				" media_per_page, media_per_row, media_location, image_size " +
				" FROM layout_module_most_viewed lmmv " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// insert a record into layout module hot new stuff table
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleMostViewedMedia(string title, string header, string footer, bool show_mature,
			int time_period_id, int media_per_page, int media_per_row, bool media_location, int imagesize)
		{

			string query = " INSERT INTO layout_module_most_viewed " + 
				" (title, header, footer, show_mature, time_period_id, media_per_page, media_per_row, media_location, image_size) " +
				" VALUES(@title, @header, @footer, @show_mature, @time_period_id, @media_per_page," + 
				" @media_per_row, @media_location, @imagesize) ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@title", title);
			parameters.Add ("@header", header);
			parameters.Add ("@footer", footer);
			parameters.Add ("@show_mature", show_mature ? 1 : 0);
			parameters.Add ("@time_period_id", time_period_id);
			parameters.Add ("@media_per_page", media_per_page);
			parameters.Add ("@media_per_row", media_per_row);
			parameters.Add ("@media_location", media_location ? 1 : 0);
			parameters.Add ("@imagesize", imagesize);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.MOST_VIEWED);
			return retVal;
		}

		/// <summary>
		/// save changes to the layout_module_leader_board
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		/// 
		public static void SaveLayoutModuleMostViewedMedia(int modulePageId, string title, string header, string footer, bool show_mature,
			int time_period_id, int media_per_page, int media_per_row, bool media_location, int imagesize)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_most_viewed ");
			sb.Append(" SET title = @title, ");
			sb.Append(" header = @header, ");
			sb.Append(" footer = @footer, ");
			sb.Append(" show_mature = @show_mature, ");
			sb.Append(" time_period_id = @time_period_id, ");
			sb.Append(" media_per_page = @media_per_page, ");
			sb.Append(" media_per_row = @media_per_row, ");
			sb.Append(" media_location = @media_location, ");
			sb.Append(" image_size = @imagesize ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@title", title);
			parameters.Add ("@header", header);
			parameters.Add ("@footer", footer);
			parameters.Add ("@show_mature", show_mature ? 1 : 0);
			parameters.Add ("@time_period_id", time_period_id);
			parameters.Add ("@media_per_page", media_per_page);
			parameters.Add ("@media_per_row", media_per_row);
			parameters.Add ("@media_location", media_location ? 1 : 0);
			parameters.Add ("@imagesize", imagesize);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.MOST_VIEWED);
		}

		/// <summary>
		/// delete a title widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleMostViewedMedia(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_channel_control_panel table
			string sqlString = "DELETE FROM layout_module_most_viewed " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// Gets the modules configuration for the view page
		/// </summary>
		/// <param name="id">the modele id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleMostViewedMedia(int id)
		{
			string sqlSelect = " SELECT m.page_id, title, header, footer, show_mature, period_in_days," + 
				" media_per_page, media_per_row, media_location, lmmv.image_size, hstp.description, lmmv.image_size " +
				" FROM layout_module_most_viewed lmmv " +
				" INNER JOIN layout_page_modules m ON m.module_page_id = lmmv.module_page_id " +
				" LEFT JOIN hotnew_stuff_time_periods hstp ON lmmv.time_period_id = hstp.time_period_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.MOST_VIEWED);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// Gets the time period options for the hot new stuff widget
		/// </summary>
		/// <returns></returns>
		public static DataTable GetMostViewedMediaTimePeriods()
		{
			string sqlSelect = " SELECT time_period_id, period_in_days, description " +
				" FROM hotnew_stuff_time_periods hstp"; 

			return  KanevaGlobals.GetDatabaseUtility ().GetDataTable(sqlSelect);
		}


		#endregion 

		#region Billboard

		/// <summary>
		/// Get a ContestSubmissions widget data
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleBillboard (int id)
		{
			string sqlSelect = " SELECT lp.channel_id, lmb.show_mature, lmb.title, lmb.results_to_return, lmb.results_per_page, lmb.info_placement, lmb.header, lmb.footer, lmb.image_size " +
				" FROM layout_module_billboard lmb" +
				" INNER JOIN layout_page_modules m ON m.module_page_id = lmb.module_page_id " +
				" INNER JOIN layout_pages lp ON lp.page_id = m.page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.BILLBOARD);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a ContestSubmissions row
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutBillboard (int modulePageId)
		{
			string sqlSelect = " SELECT lmb.show_mature, lmb.title, lmb.results_to_return, lmb.results_per_page, lmb.info_placement, lmb.header, lmb.footer, lmb.image_size  " +
				" FROM layout_module_billboard lmb" +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}
		
		/// <summary>
		/// insert a record to layout_module_contest_submissions table and return the module_page_id
		/// </summary>
		public static int InsertLayoutModuleBillboard (string title, bool showMature, string header, int resultsToReturn, bool infoPlacement, string footer, int resultsPerPage, int @imagesize)
		{
			string query = " INSERT INTO layout_module_billboard (title, show_mature, results_to_return, info_placement, header, footer, results_per_page, image_size ) " +
				" VALUES (@title, @showMature, @resultsToReturn, @infoPlacement, @header, @footer, @resultsPerPage , @imagesize ) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@showMature", showMature ? 1 : 0);
			parameters.Add ("@header", header);
			parameters.Add ("@resultsToReturn", resultsToReturn);
			parameters.Add ("@infoPlacement", infoPlacement ? 1 : 0);
			parameters.Add ("@footer", footer);
			parameters.Add ("@resultsPerPage", resultsPerPage);
			parameters.Add ("@imagesize", imagesize);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.BILLBOARD);
			return retVal;
		}

		public static void SaveLayoutModuleBillboard (int modulePageId, bool showMature, string title, string header, int resultsToReturn, bool infoPlacement, string footer, int resultsPerPage, int imagesize)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_billboard ");
			sb.Append(" SET title = @title, show_mature = @showMature, header = @header, results_to_return = @resultsToReturn, info_placement = @infoPlacement, footer = @footer, results_per_page = @resultsPerPage, image_size = @imagesize ");
			sb.Append(" WHERE module_page_id = @modulePageId ");

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@title", title);
			parameters.Add ("@showMature", showMature ? 1:0);
			parameters.Add ("@header", header);
			parameters.Add ("@modulePageId", modulePageId);
			parameters.Add ("@resultsToReturn", resultsToReturn);
			parameters.Add ("@infoPlacement", infoPlacement ? 1 : 0);
			parameters.Add ("@footer", footer);
			parameters.Add ("@resultsPerPage", resultsPerPage);
			parameters.Add ("@imagesize", imagesize);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.BILLBOARD);
		}

		/// <summary>
		/// delete a ContestSubmissions widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleBillboard (int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_blogs table
			string sqlString = "DELETE FROM layout_module_billboard " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			
		}

		#endregion

		#region User Upload

		/// <summary>
		/// load the current user upload set up
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataRow GetLayoutModuleUserUpload(int modulePageId)
		{
			string sqlSelect = " SELECT title, header, asset_group_id, channel_owner_tags, tag_instructions " + 
				" FROM layout_module_user_upload lmuu " +
				" WHERE module_page_id = @module_page_id ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataRow row = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
			return row;
		}

		/// <summary>
		/// load the current user upload descriptions for that set up
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		public static DataTable GetUserUploadDescriptions(int modulePageId)
		{
			string sqlSelect = " SELECT description_id, field_description, required " +
				" FROM user_upload_descriptions uud " +
				" WHERE module_page_id = @module_page_id " + 
				" ORDER BY description_id asc ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@module_page_id", modulePageId);
			DataTable results = KanevaGlobals.GetDatabaseUtility ().GetDataTable(sqlSelect, parameters);
			return results;
		}


		/// <summary>
		/// insert a record into layout module hot new stuff table
		/// </summary>
		/// <returns></returns>
		public static int InsertLayoutModuleUserUpload(string title, string header, int asset_group_id, string chnlOwnerTags, string instructions, ArrayList descriptions)
		{
			string query = " INSERT INTO layout_module_user_upload " + 
				" (title, header, asset_group_id, channel_owner_tags, tag_instructions) " +
				" VALUES(@title, @header, @asset_group_id, @channel_owner_tags, @tag_instructions) ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@title", title);
			parameters.Add ("@header", header);
			parameters.Add ("@asset_group_id", asset_group_id);
			parameters.Add ("@channel_owner_tags", chnlOwnerTags);
			parameters.Add ("@tag_instructions", instructions);
			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			LayoutPageModuleUpdated(retVal, (int)Constants.eMODULE_TYPE.USER_UPLOAD);

			//insert descriptions into secondary table
			SaveUploadDescription(descriptions,retVal);

			return retVal;
		}

		/// <summary>
		/// insert a record into uploads descriptions table
		/// </summary>
		/// <returns></returns>
		public static void SaveUploadDescription(ArrayList values, int id)
		{
			foreach( object internalObj in values)
			{
				ArrayList temp = (ArrayList) internalObj;
				string description = temp[1].ToString();
				bool required = Convert.ToBoolean(temp[2].ToString());
				int description_id = Convert.ToInt32(temp[0].ToString());

				if(description_id == -1)
				{
					string query = " INSERT INTO user_upload_descriptions " + 
						" (module_page_id, field_description, required) " +
						" VALUES(@id, @description, @required) ";

					Hashtable parameters = new Hashtable ();
					parameters.Add ("@id", id);
					parameters.Add ("@description", description);
					parameters.Add ("@required", required ? 1 : 0);
					int retVal = 0;
					KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);
				}
				else if(description_id > 0)
				{
					StringBuilder sb = new StringBuilder() ;
					sb.Append(" UPDATE user_upload_descriptions ");
					sb.Append(" SET field_description = @field_description, ");
					sb.Append(" required = @required ");
					sb.Append(" WHERE description_id = @description_id ");

					Hashtable parameters = new Hashtable ();
					parameters.Add ("@field_description", description);
					parameters.Add ("@required", required ? 1 : 0);
					parameters.Add ("@description_id", description_id);

					KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sb.ToString(), parameters);
				}

			}
		}

		/// <summary>
		/// save changes to the layout_module_leader_board
		/// </summary>
		/// <param name="modulePageId"></param>
		/// <returns></returns>
		/// 
		public static void SaveLayoutModuleUserUpload(int modulePageId, string title, string header, int asset_group_id, string chnlOwnerTags, string instructions, ArrayList descriptions)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE layout_module_user_upload ");
			sb.Append(" SET title = @title, ");
			sb.Append(" header = @header, ");
			sb.Append(" asset_group_id = @asset_group_id, ");
			sb.Append(" channel_owner_tags = @channel_owner_tags, ");
			sb.Append(" tag_instructions = @tag_instructions ");
			sb.Append(" WHERE module_page_id = @module_page_id ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@module_page_id", modulePageId);
			parameters.Add ("@title", title);
			parameters.Add ("@header", header);
			parameters.Add ("@asset_group_id", asset_group_id);
			parameters.Add ("@channel_owner_tags", chnlOwnerTags);
			parameters.Add ("@tag_instructions", instructions);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);

			LayoutPageModuleUpdated(modulePageId, (int)Constants.eMODULE_TYPE.USER_UPLOAD);

			//update descriptions into secondary table
			SaveUploadDescription(descriptions,modulePageId);

		}

		/// <summary>
		/// delete a title widget
		/// </summary>
		/// <param name="id">the id column in layout_page_modules table</param>
		/// <returns></returns>
		public static void DeleteModuleUserUpload(int id)
		{
			ModuleDeleted(id);

			//delete from layout_module_channel_control_panel table
			string sqlString = "DELETE FROM layout_module_user_upload " +
				" WHERE module_page_id IN " +
				" ( SELECT module_page_id FROM layout_page_modules WHERE id = @id) ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//delete from layout_page_modules table
			sqlString = "DELETE FROM layout_page_modules where id = @id";

			parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// Gets the modules configuration for the view page
		/// </summary>
		/// <param name="id">the modele id column in layout_page_modules table</param>
		/// <returns></returns>
		public static DataRow GetModuleUserUpload (int id)
		{
			string sqlSelect = " SELECT m.page_id, lmuu.module_page_id, title, header, asset_group_id, channel_owner_tags, tag_instructions " + 
				" FROM layout_module_user_upload lmuu " +
				" INNER JOIN layout_page_modules m ON m.module_page_id = lmuu.module_page_id " +
				" WHERE m.id = @id " +
				" AND m.module_id = @moduleType ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@id", id);
			parameters.Add ("@moduleType", (int) Constants.eMODULE_TYPE.USER_UPLOAD);
			return  KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}


		#endregion 

		#region Page Customizing and themes/templates

		/// <summary>
		/// GetThemes
		/// </summary>
		/// <returns></returns>
		public static PagedDataTable GetStandardTemplates (string orderby, int pageNumber, int pageSize)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			string sqlSelectList = "standard_template_id, name, description, style_sheet, preview_url ";

			string sqlTableList = "layout_standard_templates";

			// Need a where clause so the query works
			string sqlWhereClause = "standard_template_id > 0";

			return dbUtility.GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// load a theme row
		/// </summary>
		public static DataRow GetStandardTemplate (int standardTemplateId)
		{
			string sqlSelect = " SELECT standard_template_id, name, description, style_sheet, preview_url " +
				" FROM layout_standard_templates " +
				" WHERE standard_template_id = @standardTemplateId ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@standardTemplateId", standardTemplateId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
		}

		/// <summary>
		/// load a template row
		/// </summary>
		public static DataRow GetTemplateForChannel(int channelId)
		{
			DataRow drChannel = CommunityUtility.GetCommunity(channelId);
			return GetTemplate (Convert.ToInt32 (drChannel ["template_id"]));
		}

		/// <summary>
		/// load a template row
		/// </summary>
		public static DataRow GetTemplate (int templateId)
		{
			string sqlSelect = "SELECT lt.template_id, lt.standard_template_id, lt.title, lt.base_font, lt.base_hover, lt.base_link, lt.base_visited, " +
				" lt.background_picture, lt.background_orientation, lt.background_fixed, " + 
				" lt.module_border_rgb, lt.module_picture, lt.module_border_width, lt.module_border_style, lt.module_header_rgb, lt.module_background_rgb, " +
				" lt.module_outer_border_rgb, lt.module_outer_border_width, " +
				" lt.background_rgb, lt.background_repeat, lt.opacity, lt.custom_css, " + 
				" lbrm.html as repeat_html " +
				" FROM layout_templates lt LEFT OUTER JOIN layout_background_repeat_modes lbrm ON lt.background_repeat = lbrm.mode_id " +
				" WHERE template_id = @templateId ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@templateId", templateId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
		}

		/// <summary>
		/// InsertTemplate
		/// </summary>
		public static int InsertTemplate (int standardTemplateId)
		{
			string query = " INSERT INTO layout_templates " + 
				" (standard_template_id) " +
				" VALUES (@standardTemplateId) ";

			Hashtable parameters = new Hashtable ();	
			parameters.Add ("@standardTemplateId", standardTemplateId  );
			int retVal = -1;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert (query, parameters, ref retVal);
			return retVal;
		}

		/// <summary>
		/// UpdateUserTemplate
		/// </summary>
		public static void UpdateTemplate (int templateId, int standardTemplateId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlUpdate = "UPDATE layout_templates " +
				" SET " +
				" standard_template_id = @standardTemplateId" +
				" WHERE template_id = @templateId";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@standardTemplateId", standardTemplateId);
			parameters.Add ("@templateId", templateId);
			dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
		}

		/// <summary>
		/// UpdateUserTemplate
		/// </summary>
		public static void UpdateTemplate (int templateId, int baseFontId, int hyperlinkFontId, int hyperlinkHoverFontId, int hyperlinkVisitedFontId,
			string moduleBackgroundRgb, string moduleBorderRgb, string moduleBorderWidth, string moduleBorderStyle, string moduleHeaderRgb, 
			string backgroundRgb, string backgroundPicture, int backgroundRepeatMode, string modulePicture, string moduleOuterBorderRgb, string moduleOuterBorderWidth,
			int opacity, string customCSS)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlUpdate = "UPDATE layout_templates " +
				" SET " +
				" base_font = @baseFontId, " +
				" base_link = @hyperlinkFontId," +
				" base_hover = @hyperlinkHoverFontId," +
				" base_visited = @hyperlinkVisitedFontId," +
				" background_rgb = @backgroundRgb, " + 
				" background_picture = @backgroundPicture, " +
				" background_repeat = @backgroundRepeatMode, " +
				" module_background_rgb = @moduleBackgroundRgb," +
				" module_picture = @modulePicture, " +
				" module_border_rgb = @moduleBorderRgb," +
				" module_border_width = @moduleBorderWidth," +
				" module_border_style = @moduleBorderStyle," +
				" module_header_rgb = @moduleHeaderRgb," +
				" module_outer_border_rgb = @moduleOuterBorderRgb," +
				" module_outer_border_width = @moduleOuterBorderWidth," +
				" opacity = @opacity," +
				" custom_css = @customCSS" +
				" WHERE template_id = @templateId";

			Hashtable parameters = new Hashtable ();			

			if (baseFontId.Equals (0))
				parameters.Add ("@baseFontId", DBNull.Value );
			else
				parameters.Add ("@baseFontId", baseFontId);

			if (hyperlinkFontId.Equals (0))
				parameters.Add ("@hyperlinkFontId", DBNull.Value );
			else
				parameters.Add ("@hyperlinkFontId", hyperlinkFontId);
	
			if (hyperlinkHoverFontId.Equals (0))
				parameters.Add ("@hyperlinkHoverFontId", DBNull.Value );
			else
				parameters.Add ("@hyperlinkHoverFontId", hyperlinkHoverFontId);

			if (hyperlinkVisitedFontId.Equals (0))
				parameters.Add ("@hyperlinkVisitedFontId", DBNull.Value );
			else
				parameters.Add ("@hyperlinkVisitedFontId", hyperlinkVisitedFontId);

			if (backgroundRgb.Equals (""))
				parameters.Add ("@backgroundRgb", DBNull.Value );
			else
				parameters.Add ("@backgroundRgb", backgroundRgb);

			if (backgroundPicture.Equals (""))
				parameters.Add ("@backgroundPicture", DBNull.Value );
			else
				parameters.Add ("@backgroundPicture", backgroundPicture);

			if (backgroundRepeatMode.Equals ("0"))
				parameters.Add ("@backgroundRepeatMode", DBNull.Value );
			else
				parameters.Add ("@backgroundRepeatMode", backgroundRepeatMode);

			if (moduleBackgroundRgb.Equals (""))
				parameters.Add ("@moduleBackgroundRgb", DBNull.Value );
			else
				parameters.Add ("@moduleBackgroundRgb", moduleBackgroundRgb);

			if (moduleBorderRgb.Equals (""))
				parameters.Add ("@moduleBorderRgb", DBNull.Value );
			else
				parameters.Add ("@moduleBorderRgb", moduleBorderRgb);

			if (modulePicture.Equals (""))
				parameters.Add ("@modulePicture", DBNull.Value );
			else
				parameters.Add ("@modulePicture", modulePicture);

			if (moduleBorderWidth.Equals (""))
				parameters.Add ("@moduleBorderWidth", DBNull.Value );
			else
				parameters.Add ("@moduleBorderWidth", moduleBorderWidth);

			if (moduleBorderStyle.Equals (""))
				parameters.Add ("@moduleBorderStyle", DBNull.Value );
			else
				parameters.Add ("@moduleBorderStyle", moduleBorderStyle);

			if (moduleHeaderRgb.Equals (""))
				parameters.Add ("@moduleHeaderRgb", DBNull.Value );
			else
				parameters.Add ("@moduleHeaderRgb", moduleHeaderRgb);

			if (moduleOuterBorderRgb.Equals (""))
				parameters.Add ("@moduleOuterBorderRgb", DBNull.Value );
			else
				parameters.Add ("@moduleOuterBorderRgb", moduleOuterBorderRgb);

			if (moduleOuterBorderWidth.Equals (""))
				parameters.Add ("@moduleOuterBorderWidth", DBNull.Value );
			else
				parameters.Add ("@moduleOuterBorderWidth", moduleOuterBorderWidth);

			if (opacity.Equals (-1))
				parameters.Add ("@opacity", DBNull.Value );
			else
				parameters.Add ("@opacity", opacity);

			if (customCSS.Equals (""))
				parameters.Add ("@customCSS", DBNull.Value );
			else
				parameters.Add ("@customCSS", customCSS);


			parameters.Add ("@templateId", templateId);
			dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
		}

		/// <summary>
		/// GetFonts
		/// </summary>
		public static DataTable GetFonts ()
		{
			string sqlSelect = " SELECT font_id, name, pixel_sizes_csv  " +
				" FROM layout_fonts " +
				" ORDER BY name ";

			Hashtable parameters = new Hashtable ();			
			return  KanevaGlobals.GetDatabaseUtility ().GetDataTable(sqlSelect, parameters);
		}

		/// <summary>
		/// load a font
		/// </summary>
		public static DataRow GetFont (int fontId)
		{
			string sqlSelect = "SELECT ltf.font_id, lf.name, ltf.font_name_id, ltf.pixel_size, ltf.rgb " +
				" FROM layout_template_font ltf LEFT OUTER JOIN layout_fonts lf ON ltf.font_name_id = lf.font_id " +
				" WHERE ltf.font_id = @fontId ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@fontId", fontId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);
		}

		/// <summary>
		/// InsertFont
		/// </summary>
		public static int InsertFont (int fontNameId, int pixelSize, string rgb)
		{
			string query = " INSERT INTO layout_template_font " + 
				" (font_name_id, pixel_size, rgb) " +
				" VALUES (@fontNameId, @pixelSize, @rgb) ";

			Hashtable parameters = new Hashtable ();
	
			if (fontNameId.Equals (0))
				parameters.Add ("@fontNameId", DBNull.Value );
			else
				parameters.Add ("@fontNameId", fontNameId);

			if (rgb.Equals (""))
				parameters.Add ("@rgb", DBNull.Value );
			else
				parameters.Add ("@rgb", rgb);

			if (pixelSize.Equals (0))
				parameters.Add ("@pixelSize", DBNull.Value );
			else
				parameters.Add ("@pixelSize", pixelSize);

			int retVal = -1;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert (query, parameters, ref retVal);
			return retVal;
		}

		/// <summary>
		/// UpdateFont
		/// </summary>
		public static void UpdateFont (int fontId, int fontNameId, int pixelSize, string rgb)
		{
			string query = " UPDATE layout_template_font " + 
				" SET font_name_id = @fontNameId," +
				" pixel_size = @pixelSize," +
				" rgb = @rgb " +
				" WHERE font_id = @fontId ";

			Hashtable parameters = new Hashtable ();	

			if (fontNameId.Equals (0))
				parameters.Add ("@fontNameId", DBNull.Value );
			else
				parameters.Add ("@fontNameId", fontNameId);

			if (rgb.Equals (""))
				parameters.Add ("@rgb", DBNull.Value );
			else
				parameters.Add ("@rgb", rgb);

			if (pixelSize.Equals (0))
				parameters.Add ("@pixelSize", DBNull.Value );
			else
				parameters.Add ("@pixelSize", pixelSize);

			parameters.Add ("@fontId", fontId  );
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (query, parameters);
		}

		/// <summary>
		/// DeleteFont
		/// </summary>
		/// <param name="fontId"></param>
		public static void DeleteFont (int fontId)
		{
			//delete from layout_module_title_text table
			string sqlString = "DELETE FROM layout_template_font " +
				" WHERE font_id = @fontId ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@fontId", fontId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// GetBgRepeatModes
		/// </summary>
		public static DataTable GetBgRepeatModes ()
		{
			string sqlSelect = " SELECT mode_id, mode, html " +
				" FROM layout_background_repeat_modes ";

			Hashtable parameters = new Hashtable ();			
			return  KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
		}

		/// <summary>
		/// GetBgRepeatMode
		/// </summary>
		public static DataRow GetBgRepeatMode (int modeId)
		{
			string sqlSelect = "SELECT mode_id, mode, html " +
				" FROM layout_background_repeat_modes " +
				" WHERE mode_id = @modeId ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@modeId", modeId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		

		#endregion

	}
}
