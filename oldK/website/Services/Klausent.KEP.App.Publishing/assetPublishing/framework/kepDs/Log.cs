///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Resources;
using System.Diagnostics;

// Import log4net classes.
using log4net;
using log4net.Config;


namespace KlausEnt.KEP.KEPDS
{
	/// <summary>
	/// Log to the event log for this web service. The event log must be created prior
	/// to logging by utilizing the createeventlog application due to security constraints.
	/// </summary>
	public class Log
	{
		private static readonly ILog m_auditLog = LogManager.GetLogger("DS");

		// Created by EventLogCreator tool since this application does not have
		// sufficient privledges to create the new event log entry.

		public const string serviceLog = "KEI DS Service";

		// Convenience to callers
		public const int Failure = 0;
		public const int Success = 1;
		public const int Error = 2;
		public const int Info = 3;
		public const int Warn = 4;

		
		// Single instance for application
		private static Log m_theLog;

		private ResourceManager m_resourceManager;

		private Log()
		{
			// BasicConfigurator replaced with DOMConfigurator.
			DOMConfigurator.Configure(new System.IO.FileInfo(System.Configuration.ConfigurationSettings.AppSettings ["LogConfigFile"]));
		}

		/// <summary>
		/// Retrieve the single instance of the logger for this web service assembly.
		/// </summary>
		static public Log getInstance()
		{
			lock (typeof(Log))
			{
				if ( m_theLog == null)
				{
					m_theLog = new Log();
				}
			}

			return m_theLog;
		}

		/// <summary>
		/// Retrieve the resource string file for this assembly.
		/// </summary>
		protected ResourceManager getRM()
		{
			if ( m_resourceManager == null)
			{
				m_resourceManager = new ResourceManager("KEPDS.rstrings", this.GetType().Assembly);
			}

			return m_resourceManager;
		}

		/// <summary>
		/// Write a message to the event log. Append additional information for context if specified.
		/// </summary>
		public void writeLog( int type, string resourceString, string additionalInfo)
		{
			lock (this)
			{
				if ( (getRM() != null) && (resourceString != null) )
				{
					// Make sure the resourse string is found
					string resString = "";
					try
					{
						resString = getRM().GetString(resourceString);	
					}
					catch (Exception) {resString = resourceString;}

					switch ( type )
					{
						case Info:
						case Success:
							m_auditLog.Info( resString+" "+additionalInfo );
							break;

						case Warn:
						case Failure:
							m_auditLog.Warn( resString+" "+additionalInfo );
							break;

						case Error:
							m_auditLog.Error( resString+" "+additionalInfo );
							break;

						default:
							m_auditLog.Error( resString+" "+additionalInfo );
							break;
					}
				}
			}
		}
	}
}
