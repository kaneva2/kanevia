///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Collections;
using System.Diagnostics;

using CoreLab.MySql;

// Import log4net classes.
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for MySQLUtility.
	/// </summary>
	public class MySQLUtility : DatabaseUtility 
	{
		/// <summary>
		/// Constructor
		/// </summary>
		public MySQLUtility (string connectionString)
		{
			m_connectionString = connectionString;
		}

		/// <summary>
		/// GetPagedDataTable
		/// </summary>
		public PagedDataTable GetPagedDataTable (string selectList, string tableList, string whereClause, string orderBy, Hashtable parameters, int pageNumber, int pageSize)
		{
			return GetPagedDataTable (selectList, tableList, whereClause, orderBy, parameters, pageNumber, pageSize, 1 );
		}

		/// <summary>
		/// GetPagedDataTable with a retry count
		/// </summary>
		public PagedDataTable GetPagedDataTable (string selectList, string tableList, string whereClause, string orderBy, Hashtable parameters, int pageNumber, int pageSize, int retryCount )
		{
			MySqlConnection mySqlConnection = new MySqlConnection();
			mySqlConnection.ConnectionString = m_connectionString;
			PagedDataTable pdtResults = new PagedDataTable ();
			if ( retryCount < 1 )
				retryCount = 1;

			for ( int i = 0; i < retryCount; i++ )
			{
				try 
				{
					// Make sure page number is valid
					if (pageNumber < 1)
					{
						pageNumber = 1;
					}

					// Build limit query
					int offset = (pageNumber - 1) * pageSize;
					string limit = " LIMIT " + offset + ", " + pageSize;

					// Build the query
					string mysqlQuery = "SELECT SQL_CALC_FOUND_ROWS " + selectList + " FROM " + tableList;
					if(whereClause.Length > 0)
					{
						mysqlQuery += " WHERE " + whereClause;
					}
					if (orderBy.Length > 0)
					{
						mysqlQuery  += " ORDER BY " + orderBy;
					}

					mySqlConnection.Open ();

					// Get results
					MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter (mysqlQuery + limit, mySqlConnection);

					AddParams (mySqlDataAdapter.SelectCommand, parameters);
					mySqlDataAdapter.Fill (pdtResults);

					// Get the Counts
					pdtResults.TotalCount = ExecuteScalar ("SELECT found_rows()", new Hashtable (), mySqlConnection);

					break;
				} 
				catch (Exception e) 
				{
					if ( i+1 >= retryCount ) // only log last time
						m_logger.Error( "Error in GetPagedDataTable: " + e.ToString() + "; QUERY = SELECT " + selectList + " FROM " + tableList + " WHERE " + whereClause);
				} 
				finally 
				{
					mySqlConnection.Close ();
				}
			}

			return pdtResults;
		}


		/// <summary>
		/// GetPagedDataTableNPC - returns a paged data result without returning the total results found
		/// used for cases where performance is suffering
		/// </summary>
		public PagedDataTable GetPagedDataTableNPC (string query, string orderBy, Hashtable parameters, int pageNumber, int pageSize)
		{
			MySqlConnection mySqlConnection = new MySqlConnection();
			mySqlConnection.ConnectionString = m_connectionString;
			PagedDataTable pdtResults = new PagedDataTable ();

			try 
			{
				// Make sure page number is valid
				if (pageNumber < 1)
				{
					pageNumber = 1;
				}

				// Build limit query
				int offset = (pageNumber - 1) * pageSize;
				string limit = " LIMIT " + offset + ", " + pageSize;

				string mysqlQuery = query;
				if (orderBy.Length > 0)
				{
					mysqlQuery  += " ORDER BY " + orderBy;
				}

				mySqlConnection.Open ();

				// Get results
				MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter (mysqlQuery + limit, mySqlConnection);

				AddParams (mySqlDataAdapter.SelectCommand, parameters);
				mySqlDataAdapter.Fill (pdtResults);

				// Get the Counts
				//pdtResults.TotalCount = mySqlDataAdapter.;
			} 
			catch (Exception e) 
			{
				m_logger.Error( "Error in GetPagedDataTableNPC: " + e.ToString() + "; QUERY = " + query);
			} 
			finally 
			{
				mySqlConnection.Close ();
			}

			return pdtResults;
		}

		/// <summary>
		/// this is sort of a hack, atm its meant to be used for a couple methods in storeUtility that needs paging
		/// but the query doesn't fit the format required by GetPagedDataTable()
		/// </summary>
		/// <param name="query"></param>
		/// <param name="orderBy"></param>
		/// <param name="parameters"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public PagedDataTable GetPagedDataTableUnion(string query, string orderBy, Hashtable parameters, int pageNumber, int pageSize)
		{
			return GetPagedDataTableUnion( query, orderBy, parameters, pageNumber, pageSize, 1);
		}
		/// <summary>
		/// this is sort of a hack, atm its meant to be used for a couple methods in storeUtility that needs paging
		/// but the query doesn't fit the format required by GetPagedDataTable()
		/// </summary>
		/// <param name="query"></param>
		/// <param name="orderBy"></param>
		/// <param name="parameters"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public PagedDataTable GetPagedDataTableUnion(string query, string orderBy, Hashtable parameters, int pageNumber, int pageSize, int retryCount )
		{
			MySqlConnection mySqlConnection = new MySqlConnection();
			mySqlConnection.ConnectionString = m_connectionString;
			PagedDataTable pdtResults = new PagedDataTable ();

			if ( retryCount < 1 )
				retryCount = 1;

			for ( int i = 0; i < retryCount; i++ )
			{
				try 
				{
					// Make sure page number is valid
					if (pageNumber < 1)
					{
						pageNumber = 1;
					}

					// Build limit query
					int offset = (pageNumber - 1) * pageSize;
					string limit = " LIMIT " + offset + ", " + pageSize;

					// Build the query, insert SQL_CALC_FOUND_ROWS after the first select
					string select = "SELECT";
					int index = query.IndexOf(select);
					query = query.Insert(index + select.Length, " SQL_CALC_FOUND_ROWS ");
					
					string mysqlQuery = query;
					if (orderBy.Length > 0)
					{
						mysqlQuery  += " ORDER BY " + orderBy;
					}

					mySqlConnection.Open ();

					// Get results
					MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter (mysqlQuery + limit, mySqlConnection);

					AddParams (mySqlDataAdapter.SelectCommand, parameters);
					mySqlDataAdapter.Fill (pdtResults);

					// Get the Counts
					pdtResults.TotalCount = ExecuteScalar ("SELECT found_rows()", new Hashtable (), mySqlConnection);

					break;
				} 
				catch (Exception e) 
				{
					if ( i+1 >= retryCount ) // only log last time
						m_logger.Error( "Error in GetPagedDataTableUnion: " + e.ToString() + "; QUERY = " + query);
				} 
				finally 
				{
					mySqlConnection.Close ();
				}

			}
			return pdtResults;
		}

		/// <summary>
		/// Get a data table
		/// </summary>
		/// <param name="sqlSelect"></param>
		/// <returns></returns>
		public DataTable GetDataTable (string sqlSelect)
		{
			return GetDataTable (sqlSelect, new Hashtable ());
		}

		/// <summary>
		/// Get a data table
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="theParams"></param>
		/// <returns></returns>
		public DataTable GetDataTable (string sqlSelect, Hashtable theParams)
		{
			MySqlConnection mySqlConnection  = new MySqlConnection();
			mySqlConnection.ConnectionString = m_connectionString;

			// Get results
			DataTable dtResults = new DataTable ();

			try 
			{
				MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter (sqlSelect, mySqlConnection);
				AddParams (mySqlDataAdapter.SelectCommand, theParams);
				mySqlDataAdapter.Fill (dtResults);
			} 
			catch (Exception e) 
			{
				m_logger.Error( "Error in GetDataTable: " + sqlSelect, e);
			} 
			finally 
			{
				mySqlConnection.Close ();
			}

			return dtResults;
		}

		/// <summary>
		/// Get a data set
		/// </summary>
		/// <param name="sqlSelect"></param>
		/// <returns>DataSet</returns>
		public DataSet GetDataSet (string sqlSelect)
		{
			MySqlConnection mySqlConnection  = new MySqlConnection();
			mySqlConnection.ConnectionString = m_connectionString;

			// Get results
			DataSet dsResults = new DataSet ();

			try 
			{
				MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter (sqlSelect, mySqlConnection);
				mySqlDataAdapter.Fill (dsResults);
			} 
			catch (Exception e) 
			{
				m_logger.Error( "Error in GetDataSet: " + e.ToString() );
			} 
			finally 
			{
				mySqlConnection.Close ();
			}

			return dsResults;
		}

		/// <summary>
		/// Execute a scalar
		/// </summary>
		/// <param name="sql"></param>
		/// <returns></returns>
		public int ExecuteScalar (string sql)
		{
			return ExecuteScalar (sql, new Hashtable ());
		}

		/// <summary>
		/// ExecuteScalar
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		public int ExecuteScalar (string sql, Hashtable parameters)
		{
			MySqlConnection mySqlConnection = new MySqlConnection();
			mySqlConnection.ConnectionString = m_connectionString;

			int result = 0;

			try 
			{
				result = ExecuteScalar (sql, parameters, mySqlConnection);
			} 
			catch (Exception e) 
			{
				m_logger.Error( "Error in ExecuteScalar: QUERY = " + sql, e);
			} 
			finally 
			{
				mySqlConnection.Close ();
			}

			return result;
		}

		/// <summary>
		/// ExecuteScalar
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		private int ExecuteScalar (string sql, Hashtable parameters, MySqlConnection mySqlConnection)
		{
			MySqlCommand command = new MySqlCommand (sql, mySqlConnection);
			int result = 0;

			AddParams (command, parameters);

			mySqlConnection.Open();
			Object o = command.ExecuteScalar();
			if ( o is Int32 )
				result = (int)(Int32)o;
			else if ( o is Int64 )
				result = (int)(Int64)o;
			else 
				result = Convert.ToInt32 (o);

			return result;
		}

		/// <summary>
		/// ExecuteScalarDouble
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		public double ExecuteScalarDouble (string sql, Hashtable parameters)
		{
			MySqlConnection mySqlConnection = new MySqlConnection();
			mySqlConnection.ConnectionString = m_connectionString;

			MySqlCommand command = new MySqlCommand (sql, mySqlConnection);
			double result = 0;

			try 
			{
				AddParams (command, parameters);

				mySqlConnection.Open();
				Object o = command.ExecuteScalar();

				result = Convert.ToDouble (o);
			} 
			catch (Exception e) 
			{
				m_logger.Error( "Error in ExecuteScalarDouble: : QUERY = " + sql, e);
			} 
			finally 
			{
				mySqlConnection.Close ();
			}

			return result;
		}

		/// <summary>
		/// Get a datarow
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="bAddNewRowIfNotExist"></param>
		/// <returns></returns>
		public DataRow GetDataRow (string sql,  bool bAddNewRowIfNotExist)
		{
			return GetDataRow (sql, new Hashtable (), bAddNewRowIfNotExist, "" );
		}

		/// <summary>
		/// Get a datarow
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="theParams"></param>
		/// <param name="bAddNewRowIfNotExist"></param>
		/// <returns></returns>
		public DataRow GetDataRow (string sql, Hashtable theParams, bool bAddNewRowIfNotExist)
		{
			return GetDataRow( sql, theParams, bAddNewRowIfNotExist, "" );
		}

		/// <summary>
		/// Get a datarow
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <param name="bAddNewRowIfNotExist"></param>
		/// <param name="failureSql">if not empty, will execute this as non query.  Mostly used to restore current db
		/// since the driver, or someone will switch it sometimes</param>
		/// <returns></returns>
		public DataRow GetDataRow( string sql, Hashtable parameters, bool bAddNewRowIfNotExist, string failureSql )
		{
			MySqlConnection mySqlConnection = new MySqlConnection();
			mySqlConnection.ConnectionString = m_connectionString;

			// Get blog
			DataTable dtResults = new DataTable ();

			try 
			{
				MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter (sql, mySqlConnection);
				AddParams (mySqlDataAdapter.SelectCommand, parameters);
				mySqlDataAdapter.Fill (dtResults);

				if (dtResults.Rows.Count > 0) 
				{
					return dtResults.Rows [0];
				}
			} 
			catch (Exception e) 
			{
				m_logger.Error( "Error in GetDataRow: QUERY = " + sql, e);
				if ( failureSql.Length > 0 )
				{
					try
					{
						ExecuteNonQuery( failureSql );
					}
					catch (Exception ee )
					{
						m_logger.Error( "Error in GetDataRow, executing failureSql: " + ee.ToString() );
					}
				}
			} 
			finally 
			{
				mySqlConnection.Close ();
			}

			// Existing row was not found, get a new one
			if (bAddNewRowIfNotExist) 
			{
				return dtResults.NewRow ();
			} 
			else 
			{
				return null;
			}
		}

		/// <summary>
		/// Get the data row specifiying any parameters as sql parameters to lessen an injection attack.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="theParams">An ArrayList of dbParms to construct the sql statement.</param>
		/// <param name="bAddNewRowIfNotExist"></param>
		/// <returns>The record if found.</returns>
		public DataRow GetDataRowSafe (string sql, ArrayList theParams, bool bAddNewRowIfNotExist)
		{
			MySqlConnection mySqlConnection = new MySqlConnection();
			mySqlConnection.ConnectionString = m_connectionString;

			DataTable dtResults = new DataTable ();

			try 
			{
				MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter (sql, mySqlConnection);

				int paramNum = 1;
				foreach ( dbParms theParm in theParams)
				{
					MySqlParameter parm = new MySqlParameter();
					parm = mySqlDataAdapter.SelectCommand.Parameters.Add("@param" + paramNum,
																		 convertToMySqlType(theParm.theType), theParm.theLength);

					parm.Value = theParm.theValue;
					paramNum++;
				}

				mySqlDataAdapter.Fill (dtResults);

				if (dtResults.Rows.Count > 0) {
					return dtResults.Rows [0];
				}
			} 
			catch (Exception e) 
			{
				m_logger.Error( "Error in GetDataRowSafe: " + e.ToString() );
			} 
			finally 
			{
				mySqlConnection.Close ();
			}

			// Existing row was not found, get a new one
			if (bAddNewRowIfNotExist) {
				return dtResults.NewRow ();
			} else {
				return null;
			}
		}

		/// <summary>
		/// Execute a non query
		/// </summary>
		/// <param name="sqlString"></param>
		/// <returns></returns>
		public int ExecuteNonQuery (string sqlString)
		{   
			int ret = 0;
			MySqlConnection mySqlConnection = new MySqlConnection();
			mySqlConnection.ConnectionString = m_connectionString;

			MySqlCommand command = new MySqlCommand (sqlString, mySqlConnection);

			try 
			{
				mySqlConnection.Open ();
				ret = command.ExecuteNonQuery ();
			} 
			catch (Exception e) 
			{
				m_logger.Error( "Error in ExecuteNonQuery: QUERY = " + sqlString, e);
			} 
			finally 
			{
				mySqlConnection.Close ();
			}

			return ret;
		}

		/// <summary>
		/// ExecuteIdentityInsert
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="identity"></param>
		/// <returns></returns>
		public int ExecuteIdentityInsert (string sql, ref int identity)
		{
			return ExecuteIdentityInsert (sql, new Hashtable (), ref identity);
		}

		/// <summary>
		/// ExecuteIdentityInsert
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <param name="identity"></param>
		/// <returns></returns>
		public int ExecuteIdentityInsert (string sql, Hashtable parameters ,ref int identity)
		{
			int ret = 0;

			MySqlConnection mySqlConnection = new MySqlConnection();
			mySqlConnection.ConnectionString = m_connectionString;

			MySqlCommand command = new MySqlCommand (sql, mySqlConnection);

			try 
			{
				AddParams (command, parameters);

				mySqlConnection.Open ();
				ret = command.ExecuteNonQuery ();

				command = new MySqlCommand ("SELECT last_insert_id() as lastId", mySqlConnection);
				Object o = command.ExecuteScalar();
				if ( o is Int32 )
					identity = (int)(Int32)o;
				else if ( o is Int64 )
					identity = (int)(Int64)o;
				else
					return Convert.ToInt32 (o);
			} 
			catch (Exception e) 
			{
				m_logger.Error( "Error in ExecuteIdentityInsert: QUERY = " + sql, e);
			} 
			finally 
			{
				mySqlConnection.Close ();
			}

			return ret;
		}

		/// <summary>
		/// Execute a non query with parameters
		/// </summary>
		public int ExecuteNonQuery (string sqlString, Hashtable parameters)
		{
            return ExecuteNonQuery(sqlString, parameters, 1);
		}

        /// <summary>
        /// Execute a non query with parameters
        /// </summary>
        public int ExecuteNonQuery(string sqlString, Hashtable parameters, int retryCount)
        {
            int ret = 0;

            MySqlConnection mySqlConnection = new MySqlConnection();
            mySqlConnection.ConnectionString = m_connectionString;

            if ( retryCount < 1 )
				retryCount = 1;

            for (int i = 0; i < retryCount; i++)
            {
                try
                {
                    MySqlCommand command = new MySqlCommand (sqlString, mySqlConnection);

                    AddParams(command, parameters);

                    mySqlConnection.Open();
                    ret = command.ExecuteNonQuery();

                    break;
                }
                catch (Exception e)
                {
                    if (i + 1 >= retryCount) // only log last time
                        m_logger.Error("Error in ExecuteNonQuery: " + e.ToString() + "; QUERY = " + sqlString);
                }
                finally
                {
                    mySqlConnection.Close();
                }
            }

            return ret;
        }

		/// <summary>
		/// Get the current date time
		/// </summary>
		/// <returns></returns>
		public DateTime GetCurrentDateTime ()
		{
			DataRow drDate = GetDataRow ("SELECT " + GetCurrentDateFunction (), false);
			if (drDate != null)
			{
				return Convert.ToDateTime (drDate [0]);
			}
			return DateTime.Now;
		}

		/// <summary>
		/// Get the current date format
		/// </summary>
		/// <returns></returns>
		public string GetCurrentDateFunction ()
		{
			return "NOW()";
		}

		public string GetLengthFunction ()
		{
			return "LENGTH";
		}

		public string FormatDate (DateTime dtDate)
		{
			return dtDate.ToString ("yyyy-MM-dd");
		}

		public string FormatDateTime (DateTime dtDate)
		{
			return dtDate.ToString ("yyyy-MM-dd HH:mm:ss");
		}

		public string CleanText (string strText)
		{
			// XXX what else do we need here?
			strText = strText.Replace ("'", "''");
			return strText.Replace ("\\", "\\\\");
			//return strText.Replace ("\"", "\"\"");	// Don't seem to need this
		}

		public string GetDatePlusDays (int days)
		{
			return "ADDDATE(NOW(),INTERVAL " + days + " DAY)";
		}

		public string GetDatePlusMonths (int months)
		{
			return "ADDDATE(NOW(),INTERVAL " + months + " MONTH)";
		}

		public string GetDatePlusYears (int years)
		{
			return "ADDDATE(NOW(),INTERVAL " + years + " YEAR)";
		}

		public string GetDatePlusMinutes (int minutes)
		{
			return "ADDDATE(NOW(),INTERVAL " + minutes + " MINUTE)";
		}

		public string GetDatePlusDays (int days, string dateColumn)
		{
			return "ADDDATE(" + dateColumn + ",INTERVAL " + days + " DAY)";
		}

		public string GetDatePlusMonths (int months, string dateColumn)
		{
			return "ADDDATE(" + dateColumn + ",INTERVAL " + months + " MONTH)";
		}

		public string GetDatePlusYears (int years, string dateColumn)
		{
			return "ADDDATE(" + dateColumn + ",INTERVAL " + years + " YEAR)";
		}

		public string GetDatePlusMinutes (int minutes, string dateColumn)
		{
			return "ADDDATE(" + dateColumn + ",INTERVAL " + minutes + " MINUTE)";
		}

		/// <summary>
		/// AddParams
		/// </summary>
		/// <param name="command"></param>
		/// <param name="parameters"></param>
		private void AddParams (MySqlCommand command, Hashtable parameters)
		{
			IDictionaryEnumerator myEnumerator = parameters.GetEnumerator ();
			while ( myEnumerator.MoveNext() ) 
			{
				command.Parameters.Add ((string) myEnumerator.Key, myEnumerator.Value);
			}
		}

		private MySqlType convertToMySqlType(dbParms.dataTypes type)
		{
			MySqlType retType;

			switch (type) 
			{
				case dbParms.dataTypes.BigInt:
					retType = MySqlType.BigInt;
					break;
				case dbParms.dataTypes.Blob:
					retType = MySqlType.Blob;
					break;
				case dbParms.dataTypes.Char:
					retType = MySqlType.Char;
					break;
				case dbParms.dataTypes.Date:
					retType = MySqlType.Date;
					break;
				case dbParms.dataTypes.DateTime:
					retType = MySqlType.DateTime;
					break;
				case dbParms.dataTypes.Decimal:
					retType = MySqlType.Decimal;
					break;
				case dbParms.dataTypes.Double:
					retType = MySqlType.Double;
					break;
				case dbParms.dataTypes.Float:
					retType = MySqlType.Float;
					break;
				case dbParms.dataTypes.Int:
					retType = MySqlType.Int;
					break;
				case dbParms.dataTypes.SmallInt:
					retType = MySqlType.SmallInt;
					break;
				case dbParms.dataTypes.Text:
					retType = MySqlType.Text;
					break;
				case dbParms.dataTypes.Time:
					retType = MySqlType.Time;
					break;
				case dbParms.dataTypes.Timestamp:
					retType = MySqlType.TimeStamp;
					break;
				case dbParms.dataTypes.TinyInt:
					retType = MySqlType.TinyInt;
					break;
				case dbParms.dataTypes.VarChar:
					retType = MySqlType.VarChar;
					break;
				case dbParms.dataTypes.Year:
					retType = MySqlType.Year;
					break;
				default:
					Debug.Assert(false);
					retType = MySqlType.Char;
					break;
			}

			return retType;
		}

		/// <summary>
		/// The database connection string
		/// </summary>
		private string m_connectionString;

		// Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.Assembly.GetCallingAssembly(), "Database");
	}
}
