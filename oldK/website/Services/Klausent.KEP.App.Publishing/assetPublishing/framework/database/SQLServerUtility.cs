///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Collections;
using System.Diagnostics;

using System.Data.SqlClient;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for SQLServerUtility.
	/// </summary>
	public class SQLServerUtility : DatabaseUtility {

		/// <summary>
		/// Constructor
		/// </summary>
		public SQLServerUtility (string connectionString)
		{
			m_connectionString = connectionString;
		}

		/// <summary>
		/// GetPagedDataTable
		/// </summary>
		public PagedDataTable GetPagedDataTable (string selectList, string tableList, string whereClause, string orderby, Hashtable theParams, int pageNumber, int pageSize)
		{
			return GetPagedDataTable (selectList, tableList, whereClause, orderby, theParams, pageNumber, pageSize, 1 );
		}

		public PagedDataTable GetPagedDataTable (string selectList, string tableList, string whereClause, string orderby, Hashtable theParams, int pageNumber, int pageSize, int retryCount )
		{
			// TODO retryCount impl
			SqlConnection sqlConnection = new SqlConnection();
			sqlConnection.ConnectionString = m_connectionString;

			// Get results
			PagedDataTable dtResults = new PagedDataTable ();
			DataSet dsResults = new DataSet ();

			try 
			{
				string sqlQuery = "SELECT " + selectList + " FROM " + tableList + " WHERE " + whereClause;
				if (orderby.Length > 0)
				{
					sqlQuery  += " ORDER BY " + orderby;
				}

				pageNumber = pageNumber - 1;

				SqlDataAdapter sqlDataAdapter = new SqlDataAdapter (sqlQuery, sqlConnection);
				AddParams (sqlDataAdapter.SelectCommand, theParams);
				sqlDataAdapter.Fill (dsResults, pageNumber * pageSize, pageSize, "temp");

				DataTable dtTemp = dsResults.Tables [0];

				// Copy it to a PagedDataTable
				for (int i = 0; i < dtTemp.Columns.Count; i++)
				{
					dtResults.Columns.Add (dtTemp.Columns [i].ColumnName, dtTemp.Columns [i].DataType);
				}

				for (int i = 0; i < dtTemp.Rows.Count; i++)
				{

					dtResults.ImportRow (dtTemp.Rows [i]);
				}

				// Get the Counts
				dtResults.TotalCount = ExecuteScalar ("SELECT COUNT(*) " + " FROM " + tableList + " WHERE " + whereClause, theParams);
			} 
			catch (Exception e) 
			{
				m_logger.Error (e.ToString ());
			} 
			finally 
			{
				sqlConnection.Close ();
			}

			return dtResults;
		}

		/// <summary>
		/// GetPagedDataTableNPC - willl need to be implemented when/if we move to SQLServer
		/// </summary>
		/// <param name="query"></param>
		/// <param name="orderBy"></param>
		/// <param name="parameters"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public PagedDataTable GetPagedDataTableNPC (string query, string orderBy, Hashtable parameters, int pageNumber, int pageSize)
		{
			
			PagedDataTable pdtResults = new PagedDataTable ();
			return pdtResults;
		}


		/// <summary>
		/// WARNING! This method is not implemented on SQL Server
		/// </summary>
		/// <param name="query"></param>
		/// <param name="orderByList"></param>
		/// <param name="theParams"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public PagedDataTable GetPagedDataTableUnion(string query, string orderByList, Hashtable theParams, int pageNumber, int pageSize)
		{
			PagedDataTable pdtResults = new PagedDataTable ();
			return pdtResults;
		}
		public PagedDataTable GetPagedDataTableUnion(string query, string orderByList, Hashtable theParams, int pageNumber, int pageSize, int retryCount )
		{
			PagedDataTable pdtResults = new PagedDataTable ();
			return pdtResults;
		}


		/// <summary>
		/// Get a data table
		/// </summary>
		/// <param name="sql"></param>
		/// <returns></returns>
		public DataTable GetDataTable (string sqlSelect)
		{
			return GetDataTable (sqlSelect, new Hashtable ());
		}

		/// <summary>
		/// Get a data table
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="theParams"></param>
		/// <returns></returns>
		public DataTable GetDataTable (string sqlSelect, Hashtable theParams)
		{
			SqlConnection sqlConnection = new SqlConnection();
			sqlConnection.ConnectionString = m_connectionString;

			// Get results
			DataTable dtResults = new DataTable ();

			try 
			{
				SqlDataAdapter sqlDataAdapter = new SqlDataAdapter (sqlSelect, sqlConnection);
				AddParams (sqlDataAdapter.SelectCommand, theParams);
				sqlDataAdapter.Fill (dtResults);
			} 
			catch (Exception e) 
			{
				m_logger.Error (e.ToString ());
			} 
			finally 
			{
				sqlConnection.Close ();
			}

			return dtResults;
		}

		/// <summary>
		/// Get a data table
		/// </summary>
		/// <param name="sql"></param>
		/// <returns></returns>
		public DataSet GetDataSet (string sqlSelect)
		{
			SqlConnection sqlConnection = new SqlConnection();
			sqlConnection.ConnectionString = m_connectionString;

			// Get results
			DataSet dsResults = new DataSet ();

			try 
			{
				SqlDataAdapter sqlDataAdapter = new SqlDataAdapter (sqlSelect, sqlConnection);
				sqlDataAdapter.Fill (dsResults);
			} 
			catch (Exception e) {
				m_logger.Error (e.ToString ());
			} 
			finally 
			{
				sqlConnection.Close ();
			}

			return dsResults;
		}

		/// <summary>
		/// Execute a scalar
		/// </summary>
		/// <param name="sql"></param>
		/// <returns></returns>
		public int ExecuteScalar (string sql)
		{
			return ExecuteScalar (sql, new Hashtable ());
		}

		/// <summary>
		/// ExecuteScalar
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		public int ExecuteScalar (string sql, Hashtable parameters)
		{
			SqlConnection sqlConnection = new SqlConnection();
			sqlConnection.ConnectionString = m_connectionString;

			SqlCommand command = new SqlCommand (sql, sqlConnection);
			int result = 0;

			try 
			{
				AddParams (command, parameters);

				sqlConnection.Open();
				result = (int) command.ExecuteScalar();
			} 
			catch (Exception e) 
			{
				m_logger.Error (e.ToString ());
			} 
			finally 
			{
				sqlConnection.Close ();
			}

			return result;
		}

		/// <summary>
		/// ExecuteScalarDouble
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		public double ExecuteScalarDouble (string sql, Hashtable parameters)
		{
			SqlConnection sqlConnection = new SqlConnection();
			sqlConnection.ConnectionString = m_connectionString;

			SqlCommand command = new SqlCommand (sql, sqlConnection);
			double result = 0;

			try 
			{
				AddParams (command, parameters);

				sqlConnection.Open();
				result = Convert.ToDouble (command.ExecuteScalar());
			} 
			catch (Exception e) 
			{
				m_logger.Error (e.ToString ());
			} 
			finally 
			{
				sqlConnection.Close ();
			}

			return result;
		}

		/// <summary>
		/// Get a data table
		/// </summary>
		/// <param name="sql"></param>
		/// <returns></returns>
		public DataRow GetDataRow (string sql, bool bAddNewRowIfNotExist)
		{
			return GetDataRow (sql, new Hashtable (), bAddNewRowIfNotExist);
		}

		// <summary>
		/// Get a datarow
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="theParams"></param>
		/// <param name="bAddNewRowIfNotExist"></param>
		/// <returns></returns>
		public DataRow GetDataRow (string sql, Hashtable theParams, bool bAddNewRowIfNotExist)
		{
			return GetDataRow (sql, theParams, bAddNewRowIfNotExist, "" );
		}

		public DataRow GetDataRow( string sql, Hashtable parameters, bool bAddNewRowIfNotExist, string failureSql )
		{

			SqlConnection sqlConnection = new SqlConnection();
			sqlConnection.ConnectionString = m_connectionString;

			// Get blog
			DataTable dtResults = new DataTable ();

			try 
			{
				SqlDataAdapter sqlDataAdapter = new SqlDataAdapter (sql, sqlConnection);
				AddParams (sqlDataAdapter.SelectCommand, parameters);
				sqlDataAdapter.Fill (dtResults);

				if (dtResults.Rows.Count > 0) 
				{
					return dtResults.Rows [0];
				}
			} 
			catch (Exception e) 
			{
				m_logger.Error (e.ToString ());
				if ( failureSql.Length > 0 )
				{
					try
					{
						ExecuteNonQuery( failureSql );
					}
					catch (Exception ee )
					{
						m_logger.Error( "Error in GetDataRow, executing failureSql: " + ee.ToString() );
					}
				}
			} 
			finally 
			{
				sqlConnection.Close ();
			}

			// Existing row was not found, get a new one
			if (bAddNewRowIfNotExist) 
			{
				return dtResults.NewRow ();
			} 
			else 
			{
				return null;
			}
		}

		/// <summary>
		/// Get the data row specifiying any parameters as sql parameters to lessen an injection attack.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="theParams">An ArrayList of dbParms to construct the sql statement.</param>
		/// <param name="bAddNewRowIfNotExist"></param>
		/// <returns>The record if found.</returns>
		public DataRow GetDataRowSafe (string sql, ArrayList theParams, bool bAddNewRowIfNotExist)
		{
			SqlConnection sqlConnection = new SqlConnection();
			sqlConnection.ConnectionString = m_connectionString;

			// Get blog
			DataTable dtResults = new DataTable ();

			try {
				SqlDataAdapter sqlDataAdapter = new SqlDataAdapter (sql, sqlConnection);

				int paramNum = 1;
				foreach ( dbParms theParm in theParams)
				{
					SqlParameter parm = new SqlParameter();
					parm = sqlDataAdapter.SelectCommand.Parameters.Add("@param" + paramNum,
																	   convertToSqlDbType(theParm.theType), theParm.theLength);

					parm.Value = theParm.theValue;
					paramNum++;
				}

				sqlDataAdapter.Fill (dtResults);

				if (dtResults.Rows.Count > 0) {
					return dtResults.Rows [0];
				}
			} catch (Exception e) {
				m_logger.Error (e.ToString ());
			} finally {
				sqlConnection.Close ();
			}

			// Existing row was not found, get a new one
			if (bAddNewRowIfNotExist) {
				return dtResults.NewRow ();
			} else {
				return null;
			}
		}

		/// <summary>
		/// Execute a non query
		/// </summary>
		/// <param name="sqlString"></param>
		/// <returns></returns>
		public int ExecuteNonQuery (string sqlString)
		{
			SqlConnection sqlConnection = new SqlConnection();
			sqlConnection.ConnectionString = m_connectionString;

			SqlCommand command = new SqlCommand (sqlString, sqlConnection);

			try {
				sqlConnection.Open ();
				command.ExecuteNonQuery ();
			} catch (Exception e) {
				m_logger.Error (e.ToString ());
			} finally {
				sqlConnection.Close ();
			}

			return 0;
		}

		/// <summary>
		/// ExecuteIdentityInsert
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="identity"></param>
		/// <returns></returns>
		public int ExecuteIdentityInsert (string sql, ref int identity)
		{
			return ExecuteIdentityInsert (sql, new Hashtable (), ref identity);
		}

		/// <summary>
		/// ExecuteIdentityInsert
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <param name="identity"></param>
		/// <returns></returns>
		public int ExecuteIdentityInsert (string sql, Hashtable parameters, ref int identity)
		{
			SqlConnection sqlConnection = new SqlConnection();
			sqlConnection.ConnectionString = m_connectionString;

			SqlCommand command = new SqlCommand (sql, sqlConnection);

			try {
				IDictionaryEnumerator myEnumerator = parameters.GetEnumerator ();
				while ( myEnumerator.MoveNext() ) {
					command.Parameters.AddWithValue ((string) myEnumerator.Key, myEnumerator.Value);
				}

				sqlConnection.Open ();
				command.ExecuteNonQuery ();

				command = new SqlCommand ("SELECT @@identity", sqlConnection);
				identity = Convert.ToInt32 (command.ExecuteScalar ());
			} catch (Exception e) {
				m_logger.Error (e.ToString ());
			} finally {
				sqlConnection.Close ();
			}

			return 0;
		}

		/// <summary>
		/// Execute a non query with parameters
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		public int ExecuteNonQuery (string sqlString, Hashtable parameters)
		{
            return ExecuteNonQuery(sqlString, parameters, 1);
		}

        /// <summary>
        /// Execute a non query with parameters
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string sqlString, Hashtable parameters, int retryCount)
        {
            SqlConnection sqlConnection = new SqlConnection();
            sqlConnection.ConnectionString = m_connectionString;

            SqlCommand command = new SqlCommand(sqlString, sqlConnection);

            IDictionaryEnumerator myEnumerator = parameters.GetEnumerator();
            while (myEnumerator.MoveNext())
            {
                command.Parameters.AddWithValue((string)myEnumerator.Key, myEnumerator.Value);
            }

            try
            {
                sqlConnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                m_logger.Error(e.ToString());
            }
            finally
            {
                sqlConnection.Close();
            }

            return 0;
        }

		/// <summary>
		/// Get the current date time
		/// </summary>
		/// <returns></returns>
		public DateTime GetCurrentDateTime ()
		{
			DataRow drDate = GetDataRow ("SELECT " + GetCurrentDateFunction (), false);
			if (drDate != null)
			{
				return Convert.ToDateTime (drDate [0]);
			}
			return DateTime.Now;
		}

		/// <summary>
		/// Get the current date format
		/// </summary>
		/// <returns></returns>
		public string GetCurrentDateFunction ()
		{
			return "GETDATE ()";
		}

		public string GetDatePlusDays (int days)
		{
			return "DATEADD(DAY," + days + ",GETDATE ())";
		}

		public string GetDatePlusMonths (int months)
		{
			return "DATEADD(MONTH," + months + ",GETDATE ())";
		}

		public string GetDatePlusYears (int years)
		{
			return "DATEADD(YEAR," + years + ",GETDATE ())";
		}

		public string GetDatePlusMinutes (int minutes)
		{
			return "DATEADD(MINUTE," + minutes + ",GETDATE ())";
		}

		public string GetDatePlusDays (int days, string dateColumn)
		{
			return "DATEADD(DAY," + days + "," + dateColumn + ")";
		}

		public string GetDatePlusMonths (int months, string dateColumn)
		{
			return "DATEADD(MONTH," + months + "," + dateColumn + ")";
		}

		public string GetDatePlusYears (int years, string dateColumn)
		{
			return "DATEADD(YEAR," + years + "," + dateColumn + ")";
		}

		public string GetDatePlusMinutes (int minutes, string dateColumn)
		{
			return "DATEADD(MINUTE," + minutes + "," + dateColumn + ")";
		}

		public string GetLengthFunction ()
		{
			return "DATALENGTH";
		}

		public string FormatDate (DateTime dtDate)
		{
			return dtDate.ToString ("yyyy-MM-dd");
		}

		public string FormatDateTime (DateTime dtDate)
		{
			return dtDate.ToString ("yyyy-MM-dd HH:mm:ss");
		}

		public string CleanText (string strText)
		{
			// SQL server does not need to reaplace \\ with \\\\, only do this in mysql

			// XXX what else do we need here?
			return strText.Replace ("'", "''");
			//return strText.Replace ("\"", "\"\"");  // Don't seem to need this
		}

		/// <summary>
		/// AddParams
		/// </summary>
		/// <param name="command"></param>
		/// <param name="parameters"></param>
		private void AddParams (SqlCommand command, Hashtable parameters)
		{
			IDictionaryEnumerator myEnumerator = parameters.GetEnumerator ();
			while ( myEnumerator.MoveNext() ) 
			{
                command.Parameters.AddWithValue((string)myEnumerator.Key, myEnumerator.Value);
			}
		}

		private SqlDbType convertToSqlDbType(dbParms.dataTypes type)
		{
			SqlDbType retType;

			switch (type) {
			case dbParms.dataTypes.BigInt:
				retType = SqlDbType.BigInt;
				break;
			case dbParms.dataTypes.Binary:
				retType = SqlDbType.Binary;
				break;
			case dbParms.dataTypes.Bit:
				retType = SqlDbType.Bit;
				break;
			case dbParms.dataTypes.Char:
				retType = SqlDbType.Char;
				break;
			case dbParms.dataTypes.DateTime:
				retType = SqlDbType.DateTime;
				break;
			case dbParms.dataTypes.Decimal:
				retType = SqlDbType.Decimal;
				break;
			case dbParms.dataTypes.Float:
				retType = SqlDbType.Float;
				break;
			case dbParms.dataTypes.Image:
				retType = SqlDbType.Image;
				break;
			case dbParms.dataTypes.Int:
				retType = SqlDbType.Int;
				break;
			case dbParms.dataTypes.Money:
				retType = SqlDbType.Money;
				break;
			case dbParms.dataTypes.NChar:
				retType = SqlDbType.NChar;
				break;
			case dbParms.dataTypes.NText:
				retType = SqlDbType.NText;
				break;
			case dbParms.dataTypes.NVarChar:
				retType = SqlDbType.NVarChar;
				break;
			case dbParms.dataTypes.Real:
				retType = SqlDbType.Real;
				break;
			case dbParms.dataTypes.SmallDateTime:
				retType = SqlDbType.SmallDateTime;
				break;
			case dbParms.dataTypes.SmallInt:
				retType = SqlDbType.SmallInt;
				break;
			case dbParms.dataTypes.SmallMoney:
				retType = SqlDbType.SmallMoney;
				break;
			case dbParms.dataTypes.Text:
				retType = SqlDbType.Text;
				break;
			case dbParms.dataTypes.Timestamp:
				retType = SqlDbType.Timestamp;
				break;
			case dbParms.dataTypes.TinyInt:
				retType = SqlDbType.TinyInt;
				break;
			case dbParms.dataTypes.UniqueIdentifier:
				retType = SqlDbType.UniqueIdentifier;
				break;
			case dbParms.dataTypes.VarBinary:
				retType = SqlDbType.VarBinary;
				break;
			case dbParms.dataTypes.VarChar:
				retType = SqlDbType.VarChar;
				break;
			case dbParms.dataTypes.Variant:
				retType = SqlDbType.Variant;
				break;
			default:
				Debug.Assert(false);
				retType = SqlDbType.Char;
				break;
			}

			return retType;
		}

		/// <summary>
		/// The database connection string
		/// </summary>
		private string m_connectionString;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger ("Database");
	}
}
