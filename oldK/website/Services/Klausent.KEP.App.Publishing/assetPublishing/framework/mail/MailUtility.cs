///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Data;
using log4net;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for MailUtility.
	/// </summary>
	public class MailUtility
	{
		public MailUtility()
		{
		}

		/// <summary>
		/// ShareAssetWithFriend
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void ShareAssetWithFriend (int userId, string recipient, int assetId, string sender, string message, DataRow drAsset, string assetType, System.Web.UI.Page page)
		{
			string senderURL = sender;
			string assetLink = "http://" + KanevaGlobals.SiteName + "/asset/" + assetId + ".storeItem";

			if (userId > 0)
			{
                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(userId);

                sender = user.Username;
                senderURL = "member <a target='_resource' href='" + KanevaGlobals.GetPersonalChannelUrl(user.NameNoSpaces) + "' >" + sender + "</a>";
			}

			string subject = sender + " has sent you a Kaneva " + assetType;

			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>" + GetEmailHeader ();
			strMessage += "<br> Kaneva " + senderURL + " has shared the " + assetType + " <a target='_resource' href='" + assetLink + "'>" + drAsset ["name"].ToString () + "</a> with you.";

			strMessage += "<br><br>" + assetType + " link: ";
			strMessage += "<br> <a target='_resource' href='" + assetLink + "'>" + assetLink + "</a>";
		
			// Attach optional personal message?
			if (message.Trim ().Length > 0)
			{
				strMessage += "<br>\r\nPersonal message from " + sender + ":";
				strMessage += "<br><br>" + message;
			}

			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><span style='font-size:11px;'>This email was sent by Kaneva, 5901C Peachtree-Dunwoody Road, Atlanta, Georgia, 30328 USA.</span>";

			strMessage += "</body></html>";
			SendEmail (KanevaGlobals.FromEmail, recipient, subject, strMessage, true, true);
		}

        /// <summary>
		/// Send an email out for buying Kaneva Credits
		/// </summary>
		public static void SendKpointPurchaseEmail (int userId, int orderId)
		{
            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);

			if (orderId > 0)
			{
				DataRow drOrder = StoreUtility.GetOrder (orderId);

				if (drOrder ["point_transaction_id"] != DBNull.Value)
				{
					DataRow drPurchasePoints = StoreUtility.GetPurchasePointTransaction (Convert.ToInt32 (drOrder ["point_transaction_id"]));

					string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'><table border='0' width='100%'><tr><td width='20%'></td><td " + C_TD_STYLE + ">" +
						GetEmailHeader ();
					strMessage += "<br>Kaneva Credit Order Confirmation";
					strMessage += "<br>";
					strMessage += "<br>Your Kaneva Credit order was successfully completed. Below is your information. Please keep a copy for your records.";
					strMessage += "<br>";
					strMessage += "<br>You can always review your order by signing into the Kaneva web site and visiting: My Kaneva &GT; Preferences &GT; Credits.";
					strMessage += "<br>";
					strMessage += "<br><B>Order Number:</B> " + drPurchasePoints ["order_id"].ToString ();
//						adf070510 was: Constants.KPOINT_ORDER_ID_PREFIX + drPurchasePoints ["point_transaction_id"].ToString ();
					strMessage += "<br><B>Description:</B> " + drPurchasePoints ["description"].ToString ();
					strMessage += "<br><B>Amount Paid:</B> " + KanevaGlobals.FormatCurrency (Convert.ToDouble (drPurchasePoints ["amount_debited"]));
					strMessage += "<br><B>Credits Received:</B> " + KanevaGlobals.FormatKPoints (Convert.ToDouble (drPurchasePoints ["totalPoints"]));
					strMessage += "<br><B>Purchase Date:</B> " + KanevaGlobals.FormatDateTime (Convert.ToDateTime (drPurchasePoints ["transaction_date"]));
					strMessage += "<br>\r\n";
					strMessage += "<br>Enjoy The Virtual World of Kaneva!";
					strMessage += "<br>";
					strMessage += "<br>This email was sent to " + user.Email + " by Kaneva, 5901C Peachtree-Dunwoody Road, Atlanta, Georgia, 30328 USA.";
//					strMessage += "<br><br>" + GetEmailFooter ();
					strMessage += "<br><br><br><br></td><td width='20%'></td></tr></table></body></html>";

					SendEmail (KanevaGlobals.FromEmail, user.Email, "Kaneva Credit Order Confirmation", strMessage, true);
				}
			}
		}

		/// <summary>
		/// SendPurchaseEmail
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void SendPurchaseEmail (int userId, int orderId, int assetId, bool bFeatureItem)
		{
			DataRow drOrder = StoreUtility.GetOrder (orderId);

			// Don't send emails for free items
			Double amount = Convert.ToDouble (drOrder ["gross_point_amount"]);
			if (amount.Equals (0.0) && !bFeatureItem)
			{
				return;
			}

            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);

			DataRow drAsset = StoreUtility.GetAsset (assetId);

			string strFeatured = "";

			// Is it a featured item purchase?
			if (bFeatureItem)
			{
				strFeatured = "Featured ";
			}

			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'><table border='0' width='100%'><tr><td width='20%'></td><td " + C_TD_STYLE + ">" +
				GetEmailHeader ();
			strMessage += "<br>Kaneva " + strFeatured + "Item Purchase Confirmation";
			strMessage += "<br>";
			strMessage += "<br>Please keep this email for your records. Your item information is as follows:";
			strMessage += "<br>";
			strMessage += "<br><B>Order Number:</B> " + Constants.ITEM_ORDER_ID_PREFIX + drOrder ["order_id"].ToString ();
			strMessage += "<br><B>Item:</B> " + drAsset ["name"].ToString ();
			strMessage += "<br><B>Purchase Amount:</B> " + KanevaGlobals.FormatKPoints (amount);
			strMessage += "<br><B>Purchase Date:</B> " + KanevaGlobals.FormatDateTime (Convert.ToDateTime (drOrder ["purchase_date"]));
			strMessage += "<br>";
			if (!bFeatureItem)
			{
				//Show stats
				try
				{
					DataRow drOrderStats = StoreUtility.GetActiveAssetOrderRecord (userId, assetId);
					strMessage += "<br>You must begin downloading this item by:</B> " + KanevaGlobals.FormatDateTime (Convert.ToDateTime (drOrderStats ["download_end_date"]));
					strMessage += "<br>You may download this item:</B> " + drOrderStats ["number_of_downloads_allowed"].ToString () + " times";
					strMessage += "<br>";
				}
				catch (Exception){}

				strMessage += "<br>To download your item: <a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/checkout/kpointselection.aspx?orderId=" + orderId + "&assetId=" + assetId + "\">http://" + KanevaGlobals.SiteName + "/checkout/kpointselection.aspx?orderId=" + orderId + "&assetId=" + assetId + "</a>";
				strMessage += "<br>";
			}
			strMessage += "<br>To review your order, you may go to your MyKaneva K-Point Purchases screen for this item.";
			strMessage += "<br>";
			strMessage += "<br>Thank you for purchasing this item.";
			strMessage += "<br>";
			strMessage += "<br>This email was sent to " + user.Email + " by Kaneva, 5901C Peachtree-Dunwoody Road, Atlanta, Georgia, 30328 USA.";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><br><br><br></td><td width='20%'></td></tr></table></body></html>";

            SendEmail(KanevaGlobals.FromEmail, user.Email, "Kaneva " + strFeatured + "Item Purchase Confirmation", strMessage, true);
		}

		/// <summary>
		/// SendGameSubPurchaseEmail
		/// </summary>
		public static void SendGameSubPurchaseEmail (int userId, int orderId, int assetId)
		{
			DataRow drOrder = StoreUtility.GetOrder (orderId);

			// Don't send emails for free items
			Double amount = Convert.ToDouble (drOrder ["gross_point_amount"]);
			if (amount.Equals (0.0))
			{
				return;
			}

            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);

			DataRow drAsset = StoreUtility.GetAsset (assetId);

			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'><table border='0' width='100%'><tr><td width='20%'></td><td " + C_TD_STYLE + ">" +
				GetEmailHeader ();
			strMessage += "<br>Kaneva Game Subscription Purchase Confirmation. A payment has been made for a game which you have an <br>" +
				"active monthly subscription.";
			strMessage += "<br>";
			strMessage += "<br>Please keep this email for your records. Your order information is as follows:";
			strMessage += "<br>";
			strMessage += "<br><B>Item:</B> " + drAsset ["name"].ToString ();
			strMessage += "<br><B>Order Number:</B> " + Constants.ITEM_ORDER_ID_PREFIX + drOrder ["order_id"].ToString ();
			strMessage += "<br><B>Subscription Price:</B> " + KanevaGlobals.FormatKPoints (amount);
			strMessage += "<br><B>Puchase Date:</B> " + KanevaGlobals.FormatDateTime (Convert.ToDateTime (drOrder ["purchase_date"]));
			strMessage += "<br>";

			// Did they purchase additional K-Points?
			if (drOrder ["point_transaction_id"] != DBNull.Value)
			{
				DataRow drPurchasePoints = StoreUtility.GetPurchasePointTransaction (Convert.ToInt32 (drOrder ["point_transaction_id"]));

				strMessage += "<br>";
				strMessage += "Your k-point balance was insufficient to cover the subscription amount, K-Points were purchased on your behalf (" + KanevaGlobals.FormatCurrency (KanevaGlobals.MinimumCreditCardTransactionAmount) + " minimum).";
				strMessage += "<br>";
				strMessage += "<br><B>Order Number:</B> " + Constants.KPOINT_ORDER_ID_PREFIX + drPurchasePoints ["point_transaction_id"].ToString ();
				strMessage += "<br><B>Description:</B> " + drPurchasePoints ["description"].ToString ();
				strMessage += "<br><B>Amount Paid:</B> " + KanevaGlobals.FormatCurrency (Convert.ToDouble (drPurchasePoints ["amount_debited"]));
				strMessage += "<br><B>K-Points Received:</B> " + KanevaGlobals.FormatKPoints (Convert.ToDouble (drPurchasePoints ["totalPoints"]));
				strMessage += "<br><B>Puchase Date:</B> " + KanevaGlobals.FormatDateTime (Convert.ToDateTime (drPurchasePoints ["transaction_date"]));
				strMessage += "<br>";
			}
			
			strMessage += "<br>To review your order, you may go to your MyKaneva K-Point Purchases screen for this item.";
			strMessage += "<br>Help on game subscriptions can be found here <a style='color: green;' href=\"http://docs.kaneva.com/bin/view/Public/KanevaComPurchaseGame\">http://docs.kaneva.com/bin/view/Public/KanevaComPurchaseGame</a>.";
			strMessage += "<br>";
			strMessage += "<br>Thank you for your payment for this game subscription.";
			strMessage += "<br>";
            strMessage += "<br>This email was sent to " + user.Email + " by Kaneva, 5901C Peachtree-Dunwoody Road, Atlanta, Georgia, 30328 USA.";
			strMessage += "<br>To cancel this subscription, go to your My Kaneva - My Profile - My Games screen <a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/mykaneva/subscriptz.aspx\">http://" + KanevaGlobals.SiteName + "/mykaneva/subscriptz.aspx</a>";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><br><br><br></td><td width='20%'></td></tr></table></body></html>";

			SendEmail (KanevaGlobals.FromEmail, user.Email, "Kaneva Game Subscription Payment Confirmation", strMessage, true);
		}

		/// <summary>
		/// SendAuthorizeGameServer
		/// </summary>
		public static void SendAuthorizeGameServer (int userId, int orderId, double amount, int assetId)
		{
            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);

			DataRow drOrder = StoreUtility.GetOrder (orderId);
			DataRow drAsset = StoreUtility.GetAsset (assetId);

			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'><table border='0' width='100%'><tr><td width='20%'></td><td " + C_TD_STYLE + ">" +
				GetEmailHeader ();
			strMessage += "<br>Kaneva Commercial Server License Authorization Confirmation";
			strMessage += "<br>";
			strMessage += "<br>Please keep this email for your records. Your item information is as follows:";
			strMessage += "<br>";
			strMessage += "<br><B>Item:</B> Authorize a Commercial Server License for " + drAsset ["name"].ToString ();
			strMessage += "<br><B>Order Number:</B> " + Constants.ITEM_ORDER_ID_PREFIX + drOrder ["order_id"].ToString ();
			strMessage += "<br><B>Authorization Amount:</B> " + KanevaGlobals.FormatCurrency (amount);
			strMessage += "<br><B>Authorization Date:</B> " + KanevaGlobals.FormatDateTime (Convert.ToDateTime (drOrder ["purchase_date"]));
			strMessage += "<br>";

			strMessage += "<br>This transaction has been posted your credit card as a temporary 'hold' (also called an 'authorization')";
			strMessage += "<br>This hold will be removed from your account over the next few business days, and the transaction will no";
			strMessage += "<br>longer be listed as a pending transaction.  At the end of the month this account will be billed for the amount";
			strMessage += "<br>owed based on end-user participation for your commercial game server license.";
			strMessage += "<br>";


			strMessage += "<br>To review and manage your game server licenses for this game, you may go to your Publish - My Published Games screen.";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName +  "asset/publishedGames.aspx\">http://" + KanevaGlobals.SiteName +  "asset/publishedGames.aspx</a>";

			strMessage += "<br>";
			strMessage += "<br>Thank you for authorizing this server.";
            strMessage += "<br>This email was sent to " + user.Email + " by Kaneva, 5901C Peachtree-Dunwoody Road, Atlanta, Georgia, 30328 USA.";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><br><br><br></td><td width='20%'></td></tr></table></body></html>";

            SendEmail(KanevaGlobals.FromEmail, user.Email, "Kaneva Commercial Server Authorization Confirmation", strMessage, true);
		}

		/// <summary>
		/// SendOwnerEmail
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void SendOwnerEmail (int orderId, int assetId)
		{
			DataRow drOrder = StoreUtility.GetOrder (orderId);

			// Don't send emails for free items
			Double amount = Convert.ToDouble (drOrder ["gross_point_amount"]);
			if (amount.Equals (0.0))
			{
				return;
			}

			DataRow drAsset = StoreUtility.GetAsset (assetId);

            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(Convert.ToInt32(drAsset["owner_id"]));

			// Asset Owner
			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'><table border='0' width='100%'><tr><td width='20%'></td><td " + C_TD_STYLE + ">" +
				GetEmailHeader ();
			strMessage += "<br>An Item you are the owner of has been purchased on Kaneva";
			strMessage += "<br>";
			strMessage += "<br>Please keep this email for your records. Your item information is as follows:";
			strMessage += "<br>";
			strMessage += "<br><B>Order Number:</B> " + Constants.ITEM_ORDER_ID_PREFIX + drOrder ["order_id"].ToString ();
			strMessage += "<br><B>Item:</B> " + drAsset ["name"].ToString ();
			strMessage += "<br><B>Puchase Amount:</B> " + KanevaGlobals.FormatKPoints (amount);
			strMessage += "<br><B>Puchase Date:</B> " + KanevaGlobals.FormatDateTime (Convert.ToDateTime (drOrder ["purchase_date"]));
			strMessage += "<br>";
			strMessage += "<br>To review the order, you may go to your MyKaneva Monthly Sales screens.";
			strMessage += "<br>";
			strMessage += "<br>You are receiving this email because you are the item owner.";
            strMessage += "<br>This email was sent to " + user.Email + " by Kaneva, 5901C Peachtree-Dunwoody Road, Atlanta, Georgia, 30328 USA.";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><br><br><br></td><td width='20%'></td></tr></table></body></html>";

            SendEmail(KanevaGlobals.FromEmail, user.Email, "An Item you are the owner of has been purchased on Kaneva", strMessage, true);
		}

		/// <summary>
		/// SendPrivateMessageNotificationEmail
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void SendPrivateMessageNotificationEmail (string email, string subject, int messageId, DataRow drUserTo, int userIdFrom)
		{
            UserFacade userFacade = new UserFacade();
            User userFrom = userFacade.GetUser(userIdFrom);

			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>" + GetEmailHeader ();
			strMessage += "<br>Hi " + drUserTo ["username"].ToString () + ",";
			strMessage += "<br>";
            strMessage += "<br><a target='_resource' href='" + KanevaGlobals.GetPersonalChannelUrl(userFrom.NameNoSpaces) + "' >" +
                userFrom.Username + "</a>" +
				" has sent you a private message on Kaneva.";
			strMessage += "\r\n";
			strMessage += "<br>Click to read:";
			strMessage += "<a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/mykaneva/messageDetails.aspx?messageId=" + messageId + "\">http://" + KanevaGlobals.SiteName + "/mykaneva/messageDetails.aspx?messageId=" + messageId + "</a>";
			strMessage += "<br>";
			strMessage += "<br>Have fun,";
			strMessage += "<br>Team Kaneva";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><span style='font-size:11px;'>This email was sent to " + email + " by Kaneva, 5901C Peachtree-Dunwoody Road, Atlanta, Georgia, 30328 USA.</span>";
			strMessage += "</body></html>";

			SendEmail (KanevaGlobals.FromEmail, email, "Kaneva: New Private Message [" + subject + "]", strMessage, true);
		}

		/// <summary>
		/// SendNowPlayingNotificationEmail
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void SendNowPlayingNotification (string email, string itemName, int assetId, string channelName)
		{
			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>" + GetEmailHeader ();
			strMessage += "<br>New <B>Now Playing Item</B> in the " + channelName + " community";
			strMessage += "<br>";
			strMessage += "<br>To view the new Now Playing item, please visit the following URL:";
			strMessage += "<br>";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/asset/" + assetId + ".StoreItem\">http://" + KanevaGlobals.SiteName + "/asset/" + assetId + ".StoreItem</a>";
			strMessage += "<hr>";
			strMessage += "\r\n";
			strMessage += "<br>If you'd like to change when you receive new Now Playing Item notifications:";
			strMessage += "<br>";
			strMessage += "<br>Sign in and adjust the 'I want updates' setting for this Community. The option can be found on your My Kaneva - My Communities page.<br>To change update settings for this community, click Change Setting, and then select 'As it happens', 'Daily Digest', 'Weekly Digest', or 'None'.";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx\">http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx</a>";
			strMessage += "<br>";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><span style='font-size:11px;'>This email was sent to " + email + " by Kaneva, 5901C Peachtree-Dunwoody Road, Atlanta, Georgia, 30328 USA.</span>";
			strMessage += "</body></html>";

			SendEmail (KanevaGlobals.FromEmail, email, "New Now Playing Item [" + itemName + "]", strMessage, true);
		}

		/// <summary>
		/// SendReviewNotificationEmail
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void SendReviewNotification (string email, string itemName, int assetId, string channelName)
		{
			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>" + GetEmailHeader ();
			strMessage += "<br>New <B>Now Playing Item Review</B> in the " + channelName + " community";
			strMessage += "<br>";
			strMessage += "<br>To view the review, please visit the following URL:";
			strMessage += "<br>";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/asset/" + assetId + ".storeItem\">http://" + KanevaGlobals.SiteName + "/asset/assetComments?assetId=" + assetId + "</a>";
			strMessage += "<hr>";
			strMessage += "\r\n";
			strMessage += "<br>If you'd like to change when you receive new <B>Now Playing Item Review</B> notifications:";
			strMessage += "<br>";
			strMessage += "<br>Sign in and adjust the 'I want updates' setting for this Community. The option can be found on your My Kaneva - My Communities page.<br>To change update settings for this community, click Change Setting, and then select 'As it happens', 'Daily Digest', 'Weekly Digest', or 'None'.";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx\">http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx</a>";
			strMessage += "<br>";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><span style='font-size:11px;'>This email was sent to " + email + " by Kaneva, 5901C Peachtree-Dunwoody Road, Atlanta, Georgia, 30328 USA.</span>";
			strMessage += "</body></html>";

			SendEmail (KanevaGlobals.FromEmail, email, "New Review [" + itemName + "]", strMessage, true);
		}

		/// <summary>
		/// SendForumTopicNotification
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void SendForumTopicNotification (string email, string itemName, int topicId, string channelName, int channelId)
		{
			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>" + GetEmailHeader ();
			strMessage += "<br>New <B>Forum Topic</B> in the " + channelName + " community";
			strMessage += "<br>";
			strMessage += "<br>To view the topic, please visit the following URL:";
			strMessage += "<br>";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + topicId + "&communityId=" + channelId + "\">http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + topicId + "&communityId=" + channelId + "</a>";
			strMessage += "<hr>";
			strMessage += "\r\n";
			strMessage += "<br>If you'd like to change when you receive new Forum Topic notifications:";
			strMessage += "<br>";
			strMessage += "<br>Sign in and adjust the 'I want updates' setting for this Community. The option can be found on your My Kaneva - My Communities page.<br>To change update settings for this community, click Change Setting, and then select 'As it happens', 'Daily Digest', 'Weekly Digest', or 'None'.";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx\">http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx</a>";
			strMessage += "<br>";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><span style='font-size:11px;'>This email was sent to " + email + " by Kaneva, 5901C Peachtree-Dunwoody Road, Atlanta, Georgia, 30328 USA.</span>";
			strMessage += "</body></html>";

			SendEmail (KanevaGlobals.FromEmail, email, "New Forum Topic [" + itemName + "]", strMessage, true);
		}

		/// <summary>
		/// SendForumReplyNotification
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void SendForumReplyNotification (string email, string itemName, int topicId, string channelName, int channelId)
		{
			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>" + GetEmailHeader ();
			strMessage += "<br>New <B>Forum Reply</B> in the " + channelName + " community";
			strMessage += "<br>";
			strMessage += "<br>To view the reply, please visit the following URL:";
			strMessage += "<br>";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + topicId + "&communityId=" + channelId + "\">http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + topicId + "&communityId=" + channelId + "</a>";
			strMessage += "<hr>";
			strMessage += "\r\n";
			strMessage += "<br>If you'd like to change when you receive new Forum Reply notifications:";
			strMessage += "<br>";
			strMessage += "<br>Sign in and adjust the 'I want updates' setting for this Community. The option can be found on your My Kaneva - My Communities page.<br>To change update settings for this community, click Change Setting, and then select 'As it happens', 'Daily Digest', 'Weekly Digest', or 'None'.";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx\">http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx</a>";
			strMessage += "<br>";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><span style='font-size:11px;'>This email was sent to " + email + " by Kaneva, 5901C Peachtree-Dunwoody Road, Atlanta, Georgia, 30328 USA.</span>";
			strMessage += "</body></html>";

			SendEmail (KanevaGlobals.FromEmail, email, "New Forum Reply [" + itemName + "]", strMessage, true);
		}

		/// <summary>
		/// SendBlogNotificationEmail
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void SendBlogNotification (string email, string itemName, int blogId, string channelName, int channelId)
		{
			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>" + GetEmailHeader ();
			strMessage += "<br>New <B>Blog Entry</B> in the " + channelName + " community";
			strMessage += "<br>";
			strMessage += "<br>To view the blog, please visit the following URL:";
			strMessage += "<br>";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/blog/blogComments.aspx?topicId=" + blogId + "&communityId=" + channelId + "\">http://" + KanevaGlobals.SiteName + "/blogComments.aspx?topicId=" + blogId + "&communityId=" + channelId + "</a>";
			strMessage += "<hr>";
			strMessage += "\r\n";
			strMessage += "<br>If you'd like to change when you receive new Blog Entry notifications:";
			strMessage += "<br>";
			strMessage += "<br>Sign in and adjust the 'I want updates' setting for this Community. The option can be found on your My Kaneva - My Communities page.<br>To change update settings for this community, click Change Setting, and then select 'As it happens', 'Daily Digest', 'Weekly Digest', or 'None'.";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx\">http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx</a>";
			strMessage += "<br>";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><span style='font-size:11px;'>This email was sent to " + email + " by Kaneva, 5901C Peachtree-Dunwoody Road, Atlanta, Georgia, 30328 USA.</span>";
			strMessage += "</body></html>";

			SendEmail (KanevaGlobals.FromEmail, email, "New Blog Entry [" + itemName + "]", strMessage, true);
		}

		/// <summary>
		/// SendBlogCommentNotificationEmail
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
		public static void SendBlogCommentNotification (string email, string itemName, int blogId, string channelName, int channelId)
		{
			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>" + GetEmailHeader ();
			strMessage += "<br>New <B>Blog Comment</B> in the " + channelName + " community";
			strMessage += "<br>";
			strMessage += "<br>To view the blog comment, please visit the following URL:";
			strMessage += "<br>";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/blog/blogComments.aspx?topicId=" + blogId + "&communityId=" + channelId + "\">http://" + KanevaGlobals.SiteName + "/blogComments.aspx?topicId=" + blogId + "&communityId=" + channelId + "</a>";
			strMessage += "<hr>";
			strMessage += "<br>";
			strMessage += "\r\n";
			strMessage += "<br>If you'd like to change when you receive new <B>Blog Comment</B> notifications for this community:";
			strMessage += "<br>";
			strMessage += "<br>Sign in and adjust the 'I want updates' setting for this Community. The option can be found on your My Kaneva - My Communities page.<br>To change update settings for this community, click Change Setting, and then select 'As it happens', 'Daily Digest', 'Weekly Digest', or 'None'.";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx\">http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx</a>";
			strMessage += "<br>";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><span style='font-size:11px;'>This email was sent to " + email + " by Kaneva, 5901C Peachtree-Dunwoody Road, Atlanta, Georgia, 30328 USA.</span>";
			strMessage += "</body></html>";

			SendEmail (KanevaGlobals.FromEmail, email, "New Blog Comment [" + itemName + "]", strMessage, true);
		}

		/// <summary>
		/// SendBlogCommentNotificationDaily
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userId"></param>
		/// <param name="activationKey"></param>
        public static void SendNotificationDailyDigest(string email, DataRow drCommunityUser, PagedDataTable pdtReviews, PagedDataTable pdtTopics, PagedDataTable pdtThreads, PagedList<Blog> plBlogs, PagedList<BlogComment> plBlogComments, string channelName, int channelId)
		{
			string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>" + GetEmailHeader ();
			strMessage += "<br>Kaneva Digest for the <B>" + channelName + "</B> community";
			strMessage += "<br>";
			strMessage += "<br>";

            //// Now Playing Items
            //if ((pdtAssets.Rows.Count > 0) && Convert.ToInt32 (drCommunityUser ["item_notify"]).Equals (1))
            //{
            //    strMessage += "\r\nNew <B>Now Playing Items</B>";
            //    strMessage += "<br>";
            //    strMessage += "<br>To view the new Now Playing items, please visit the following URL(s):";
            //    strMessage += "<br>";
            //    for (int k=0; k < pdtAssets.Rows.Count; k++)
            //    {
            //        strMessage += "<br>" + (k + 1) + ". " + System.Web.HttpUtility.HtmlDecode (pdtAssets.Rows [k]["name"].ToString ()) + " <a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/asset/" + Convert.ToInt32 (pdtAssets.Rows [k]["asset_id"]) + ".StoreItem\">http://" + KanevaGlobals.SiteName + "/asset/" + Convert.ToInt32 (pdtAssets.Rows [k]["asset_id"]) + ".StoreItem</a>";
            //    }
            //    strMessage += "<hr>";
            //}

			// New Reviews
			if ((pdtReviews.Rows.Count > 0) && Convert.ToInt32 (drCommunityUser ["item_review_notify"]).Equals (1))
			{
				strMessage += "\r\nNew <B>Now Playing Item Reviews</B>";
				strMessage += "<br>";
				strMessage += "<br>To view the reviews, please visit the following URL(s):";
				strMessage += "<br>";
				for (int k=0; k < pdtReviews.Rows.Count; k++)
				{
					strMessage += "<br>" + (k + 1) + ". " + System.Web.HttpUtility.HtmlDecode (pdtReviews.Rows [k]["name"].ToString ()) + " <a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/asset/" + Convert.ToInt32 (pdtReviews.Rows [k]["asset_id"]) + ".storeItem\">http://" + KanevaGlobals.SiteName + "/asset/assetComments?assetId=" + Convert.ToInt32 (pdtReviews.Rows [k]["asset_id"]) + "</a>";
				}
				strMessage += "<hr>";
			}


			// Forum Topics
			if ((pdtTopics.Rows.Count > 0) && Convert.ToInt32 (drCommunityUser ["post_notify"]).Equals (1))
			{
				strMessage += "\r\nNew <B>Forum Topics</B>";
				strMessage += "<br>";
				strMessage += "<br>To view new forum topics, please visit the following URL(s):";
				strMessage += "<br>";
				for (int k=0; k < pdtTopics.Rows.Count; k++)
				{
					strMessage += "<br>" + (k + 1) + ". " + System.Web.HttpUtility.HtmlDecode (pdtTopics.Rows [k]["forum_name"].ToString ()) + " - " + System.Web.HttpUtility.HtmlDecode (pdtTopics.Rows [k]["subject"].ToString ()) + " <a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + Convert.ToInt32 (pdtTopics.Rows [k]["topic_id"]) + "&communityId=" + channelId + "\">http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + Convert.ToInt32 (pdtTopics.Rows [k]["topic_id"]) + "&communityId=" + channelId + "</a>";
				}
				strMessage += "<hr>";
			}


			// Forum replies
			if ((pdtThreads.Rows.Count > 0) && Convert.ToInt32 (drCommunityUser ["reply_notify"]).Equals (1))
			{
				strMessage += "\r\nNew <B>Forum Replies</B>";
				strMessage += "<br>";
				strMessage += "<br>To view forum replies, please visit the following URL(s):";
				strMessage += "<br>";
				for (int k=0; k < pdtThreads.Rows.Count; k++)
				{
					strMessage += "<br>" + (k + 1) + ". " + System.Web.HttpUtility.HtmlDecode (pdtThreads.Rows [k]["forum_name"].ToString ()) + " - " + System.Web.HttpUtility.HtmlDecode (pdtThreads.Rows [k]["subject"].ToString ()) + " <a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + Convert.ToInt32 (pdtThreads.Rows [k]["topic_id"]) + "&communityId=" + channelId + "\">http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + Convert.ToInt32 (pdtThreads.Rows [k]["topic_id"]) + "&communityId=" + channelId + "</a>";
				}
				strMessage += "<hr>";
			}


			// Blogs
			if ((plBlogs.Count > 0) && Convert.ToInt32 (drCommunityUser ["blog_notify"]).Equals (1))
			{
				strMessage += "\r\nNew <B>Blog Entries</B>";
				strMessage += "<br>";
				strMessage += "<br>To view the blog, please visit the following URL(s):";
				strMessage += "<br>";

                int k = 1;
                foreach (Blog blog in plBlogs)
                {
                    strMessage += "<br>" + k + ". " + System.Web.HttpUtility.HtmlDecode(blog.Subject) + " <a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/blog/blogComments.aspx?topicId=" + blog.BlogId + "&communityId=" + channelId + "\">http://" + KanevaGlobals.SiteName + "/blogComments.aspx?topicId=" + blog.BlogId + "&communityId=" + channelId + "</a>";
                    k++;
                }
				strMessage += "<hr>";
			}


			// Blog comments
            if ((plBlogComments.Count > 0) && Convert.ToInt32(drCommunityUser["blog_comment_notify"]).Equals(1))
			{
				strMessage += "\r\nNew <B>Blog Comments</B>";
				strMessage += "<br>";
				strMessage += "<br>To view the blog comments, please visit the following URL(s):";
				strMessage += "<br>";

                int k = 1;
                foreach (BlogComment blogComment in plBlogComments)
				{
                    strMessage += "<br>" + k + ". " + System.Web.HttpUtility.HtmlDecode(blogComment.Subject) + " <a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/blog/blogComments.aspx?topicId=" + blogComment.BlogId + "&communityId=" + channelId + "\">http://" + KanevaGlobals.SiteName + "/blogComments.aspx?topicId=" + blogComment.BlogId + "&communityId=" + channelId + "</a>";
                    k++;
				}
				strMessage += "<hr>";
			}

			strMessage += "<br><br>";
			strMessage += "\r\nIf you'd like to change when you receive new Digest notifications:";
			strMessage += "<br>";
			strMessage += "<br>Sign in and adjust the 'I want updates' setting for this Community. The option can be found on your My Kaneva - My Communities page.<br>To change update settings for this community, click Change Setting, and then select 'As it happens', 'Daily Digest', 'Weekly Digest', or 'None'.";
			strMessage += "<br><a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx\">http://" + KanevaGlobals.SiteName + "/bookmarks/channelTags.aspx</a>";
			strMessage += "<br>";
			strMessage += "<br><br>" + GetEmailFooter ();
			strMessage += "<br><span style='font-size:11px;'>This email was sent to " + email + " by Kaneva, 5901C Peachtree-Dunwoody Road, Atlanta, Georgia, 30328 USA.</span>";
			strMessage += "</body></html>";

			SendEmail (KanevaGlobals.FromEmail, email, "Digest for the " + channelName + " community", strMessage, true);
		}

        /// <summary>
        /// SendMarketingUserNoWok
        /// </summary>
        public static void SendMarketingUserNoWok(int userId, string subject, string message, string header, string footer, string kanevaUsers, string kanevaCommunities)
        {
            try
            {
                UserFacade userFacade = new UserFacade();
                User userTo = userFacade.GetUser(userId);

                message = message.Replace("#header#", header);
                message = message.Replace("#footer#", footer);
                message = message.Replace("#username#", userTo.Username);
                message = message.Replace("#firstname#", userTo.FirstName);
                message = message.Replace("#sitename#", KanevaGlobals.SiteName);
                message = message.Replace("#toemail#", userTo.Email);
                //message = message.Replace("#rewards#", UsersUtility.getUserBalance(userTo.UserId, "gpoint").ToString());
                message = message.Replace("#rewards#", "500");

                string stats = "";

                if (userTo.Stats.NumberOfNewMessages.Equals(0) && userTo.Stats.NumberOfRequests.Equals(0) && userTo.Stats.NumberOfPending.Equals(0))
                {
                    stats += "<p>There are <span style=\"font-size: 14px;\"><strong>" + kanevaUsers  + "</strong></span> people in Kaneva.</p>";
                    stats += "<p>There are <span style=\"font-size: 14px;\"><strong>" + kanevaCommunities + "</strong></span> communities.</p>";
                    message = message.Replace("#stats#", stats);
                }
                else
                {
                    stats += "<p>You have <span style=\"font-size: 14px;\"><strong>" + userTo.Stats.NumberOfNewMessages.ToString() + "</strong></span> unread messages.<br>";
                    stats += "<a href=\"http://www.kaneva.com/mykaneva/mailbox.aspx\" style=\"color: #2e6a9a\">Find out who they're from</a></p>";
                    stats += "<p>You have <span style=\"font-size: 14px;\"><strong>" + userTo.Stats.NumberOfRequests.ToString() + "</strong></span> friend requests.<br>";
                    stats += "<a href=\"http://www.kaneva.com/mykaneva/mailboxFriendRequests.aspx\" style=\"color: #2e6a9a\">See who's waiting to meet you</a></p>";
                    stats += "<p>You have invited <span style=\"font-size: 14px;\"><strong>" + userTo.Stats.NumberOfPending.ToString() + "</strong></span> friends to hangout with you in 3D.<br>";
                    stats += "<a href=\"http://www.kaneva.com/myKaneva/inviteFriend.aspx\" style=\"color: #2e6a9a\">Invite your friends now</a></p>";
                    message = message.Replace("#stats#", stats);
                }

                string toEmail ="";
                if (MarketingEmailsDebugMode)
                {
                    toEmail = System.Configuration.ConfigurationManager.AppSettings["debugModeEmail"].ToString();
                }
                else
                {
                    // commented out to ensure we don't send emails yet.
                     toEmail = userTo.Email;
                }                

                SendEmail(KanevaGlobals.FromEmail, toEmail, subject, message, true, true);

            }
            catch (Exception exc)
            {
                m_logger.Error("Error sending system email, SendMarketingUserNoWok, userid =" + userId, exc);
            }
        }

        /// <summary>
        /// SendMarketingUserNoWok
        /// </summary>
        public static void SendMarketingPeopleNearby(int userId, string subject, string message, string header, string footer, string kanevaUsers, string kanevaCommunities)
        {
            PagedDataTable pdt = null;
            string peopleNearby = "<!-- no info -->";
            UserFacade userFacade = new UserFacade();
            User userTo = userFacade.GetUser(userId);
            int sendFlag = 1;  // this is a flag that we set to 0 if there aren't enough people nearby to warrant sending.

            try
            {
                pdt = UsersUtility.SearchUsersNew(false, string.Empty,
                    true, true, true, 18, 0,
                    userTo.Country, userTo.ZipCode, 100,
                    2, 0, 0, -1, 0, -1, string.Empty, string.Empty, string.Empty, string.Empty,
                    string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "_signup_date", 0, 12);

                if (pdt.Rows.Count <= 12)
                {
                    pdt = UsersUtility.SearchUsersNew(false, string.Empty,
                        true, true, true, 18, 0,
                        userTo.Country, userTo.ZipCode, 200,
                        2, 0, 0, -1, 0, -1, string.Empty, string.Empty, string.Empty, string.Empty,
                        string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "_signup_date", 0, 12);

                }

                if (pdt.Rows.Count < 3)
                {
                    sendFlag = 0;
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error retrieving, SendMarketingPeopleNearby, on detail user =" + userId, exc);
            }



            try
            {
                int i = 0; 
                for (i = 0; i < pdt.Rows.Count; i++)
                {
                    peopleNearby += "<td align=\"center\"> " +
                        "<a href=\"" + KanevaGlobals.GetPersonalChannelUrl(pdt.Rows[i]["name_no_spaces"].ToString()) + "\">" +
                        "<img src=\"" + UsersUtility.GetProfileImageURL (pdt.Rows[i]["thumbnail_medium_path"].ToString (), "me", pdt.Rows[i]["gender"].ToString ())   + "\" height=\"75\" border=\"0\"></a><br /> " +
                        "<a href=\"" + KanevaGlobals.GetPersonalChannelUrl(pdt.Rows[i]["name_no_spaces"].ToString()) + "\" style=\"color: #226388\"><span style=\"font-size: 10px;\">" +
                        pdt.Rows[i]["username"].ToString() + "</span></a></td>";

                    if (i == 3) { peopleNearby += "</tr><tr><td><br/></td></tr><tr>"; }
                    if (i == 7) { peopleNearby += "</tr><tr><td> </td></tr><tr>"; }
                }

            }
            catch (Exception exc)
            {
                m_logger.Error("Error building search results, SendMarketingPeopleNearby, on detail user =" + userId , exc);
            }

                
                message = message.Replace("#header#", header);
                message = message.Replace("#footer#", footer);
                message = message.Replace("#username#", userTo.Username);
                message = message.Replace("#firstname#", userTo.FirstName);
                message = message.Replace("#sitename#", KanevaGlobals.SiteName);
                message = message.Replace("#toemail#", userTo.Email);
                message = message.Replace("#rewards#", UsersUtility.getUserBalance(userTo.UserId, "gpoint").ToString());
                message = message.Replace("#peoplenearby#", peopleNearby);


                if (UsersUtility.getUserBalance(userTo.UserId, "gpoint") == 0)
                {
                    message = message.Replace("#creditimg#", "buy-credits.jpg");
                }
                else
                {
                    message = message.Replace("#creditimg#", "shop-inworld.jpg");
                }


                string stats = "";

                if (userTo.Stats.NumberOfNewMessages.Equals(0) && userTo.Stats.NumberOfRequests.Equals(0) && userTo.Stats.NumberOfPending.Equals(0))
                {
                    stats += "<p>There are <span style=\"font-size: 14px;\"><strong>" + kanevaUsers + "</strong></span> people in Kaneva.</p>";
                    stats += "<p>There are <span style=\"font-size: 14px;\"><strong>" + kanevaCommunities + "</strong></span> communities.</p>";
                    message = message.Replace("#stats#", stats);
                }
                else
                {
                    stats += "<p>You have <span style=\"font-size: 14px;\"><strong>" + userTo.Stats.NumberOfNewMessages.ToString() + "</strong></span> unread messages.<br>";
                    stats += "<a href=\"http://www.kaneva.com/mykaneva/mailbox.aspx\" style=\"color: #b6e5f6\">Find out who they're from</a></p>";
                    stats += "<p>You have <span style=\"font-size: 14px;\"><strong>" + userTo.Stats.NumberOfRequests.ToString() + "</strong></span> friend requests.<br>";
                    stats += "<a href=\"http://www.kaneva.com/mykaneva/mailboxFriendRequests.aspx\" style=\"color: #b6e5f6\">See who's waiting to meet you</a></p>";
                    stats += "<p>You have invited <span style=\"font-size: 14px;\"><strong>" + userTo.Stats.NumberOfPending.ToString() + "</strong></span> friends to hangout with you in 3D.<br>";
                    stats += "<a href=\"http://www.kaneva.com/myKaneva/inviteFriend.aspx\" style=\"color: #b6e5f6\">Invite your friends now</a></p>";
                    message = message.Replace("#stats#", stats);
                }

                if (peopleNearby.Contains(".jpg"))
                {
                    if (sendFlag.Equals(1))
                    {

                        string toEmail = "";
                        if (MarketingEmailsDebugMode)
                        {
                            toEmail = System.Configuration.ConfigurationManager.AppSettings["debugModeEmail"].ToString();
                        }
                        else
                        {
                            // commented out to ensure we don't send emails yet.
                            toEmail = userTo.Email;
                        }
                        
                        SendEmail(KanevaGlobals.FromEmail, toEmail, subject, message, true, true);
                    }
                }


        }

        /// <summary>
        /// SendMarketingUserNoWok
        /// </summary>
        public static void SendMarketingUsersNotSpent(int userId, string subject, string message, string header, string footer, string kanevaUsers, string kanevaCommunities)
        {
            try
            {
                UserFacade userFacade = new UserFacade();
                User userTo = userFacade.GetUser(userId);

                message = message.Replace("#header#", header);
                message = message.Replace("#footer#", footer);
                message = message.Replace("#username#", userTo.Username);
                message = message.Replace("#firstname#", userTo.FirstName);
                message = message.Replace("#sitename#", KanevaGlobals.SiteName);
                message = message.Replace("#toemail#", userTo.Email);

                if (UsersUtility.getUserBalance(userTo.UserId, "gpoint") > 0 && UsersUtility.getUserBalance(userTo.UserId, "kpoint") > 0)
                {
                    message = message.Replace("#rewards2#", UsersUtility.getUserBalance(userTo.UserId, "gpoint").ToString() + " rewards and ");
                }
                else if(UsersUtility.getUserBalance(userTo.UserId, "gpoint") > 0)
                {
                    message = message.Replace("#rewards2#", UsersUtility.getUserBalance(userTo.UserId, "gpoint").ToString() + " rewards ");
                }
                else
                {
                    message = message.Replace("#rewards2#", "");
                }


                if (UsersUtility.getUserBalance(userTo.UserId, "kpoint") > 0)
                {
                    message = message.Replace("#credits2#", UsersUtility.getUserBalance(userTo.UserId, "kpoint").ToString() + " credits ");
                }
                else
                {
                    message = message.Replace("#credits2#", "");
                }

                message = message.Replace("#credits#", UsersUtility.getUserBalance(userTo.UserId, "kpoint").ToString());
                message = message.Replace("#rewards#", UsersUtility.getUserBalance(userTo.UserId, "gpoint").ToString());


                string stats = "";

                if (userTo.Stats.NumberOfNewMessages.Equals(0) && userTo.Stats.NumberOfRequests.Equals(0) && userTo.Stats.NumberOfPending.Equals(0))
                {
                    stats += "<p>There are <span style=\"font-size: 14px;\"><strong>" + kanevaUsers + "</strong></span> people in Kaneva.</p>";
                    stats += "<a href=\"http://www.kaneva.com/people/people.kaneva\" style=\"color: #b6e5f6\">Search for Friends</a></p>";
                    stats += "<p>There are <span style=\"font-size: 14px;\"><strong>" + kanevaCommunities + "</strong></span> communities.</p>";
                    stats += "<a href=\"http://www.kaneva.com/community/channel.kaneva\" style=\"color: #b6e5f6\">Join a community</a></p>";
                    message = message.Replace("#stats#", stats);
                }
                else
                {
                    stats += "<p>You have <span style=\"font-size: 14px;\"><strong>" + userTo.Stats.NumberOfNewMessages.ToString() + "</strong></span> unread messages.<br>";
                    stats += "<a href=\"http://www.kaneva.com/mykaneva/mailbox.aspx\" style=\"color: #b6e5f6\">Find out who they're from</a></p>";
                    stats += "<p>You have <span style=\"font-size: 14px;\"><strong>" + userTo.Stats.NumberOfRequests.ToString() + "</strong></span> friend requests.<br>";
                    stats += "<a href=\"http://www.kaneva.com/mykaneva/mailboxFriendRequests.aspx\" style=\"color: #b6e5f6\">See who's waiting to meet you</a></p>";
                    stats += "<p>You have invited <span style=\"font-size: 14px;\"><strong>" + userTo.Stats.NumberOfPending.ToString() + "</strong></span> friends to hangout with you in 3D.<br>";
                    stats += "<a href=\"http://www.kaneva.com/myKaneva/inviteFriend.aspx\" style=\"color: #b6e5f6\">Invite your friends now</a></p>";
                    message = message.Replace("#stats#", stats);
                }


                if (UsersUtility.getUserBalance(userTo.UserId, "gpoint") > 0 || UsersUtility.getUserBalance(userTo.UserId, "kpoint") > 0)
                {

                    string toEmail = "";
                    if (MarketingEmailsDebugMode)
                    {
                        toEmail = System.Configuration.ConfigurationManager.AppSettings["debugModeEmail"].ToString();
                    }
                    else
                    {
                        // commented out to ensure we don't send emails yet.
                        toEmail = userTo.Email;
                    }
                    
                    SendEmail(KanevaGlobals.FromEmail, toEmail, subject, message, true, true);
                }

            }
            catch (Exception exc)
            {
                m_logger.Error("Error sending system email, SendMarketingUsersNotSpent, userid =" + userId , exc);
            }
        }

        private static bool MarketingEmailsDebugMode
        {
            get { return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["debugMode"]); }
        }

		public static string GetEmailHeader ()
		{
			return "<img src='http://" + KanevaGlobals.SiteName + "/images/head/logok.gif' border='0'><br><br><br>"; 
		}

		public static string GetEmailFooter ()
		{
			return "<hr>" +
				"<span style='font-size:11px;'>\r\nEveryone at Kaneva cares about your privacy. If you don't want to receive emails like this " +
				"in the future, you can <a href=\'http://" + KanevaGlobals.SiteName + "/mykaneva/settings.aspx' target='_resource'>update your account settings here</a>.</span>" +
				"<br><br><span style='font-size:11px;'>" +
				"(c) 2006-2007 Kaneva. All rights reserved worldwide.</span>";
		}

		/// <summary>
		/// Sends a single email. 
		/// </summary>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="subject"></param>
		/// <param name="body"></param>
		/// <param name="bodyFormat"></param>
		/// <param name="smtpServer"></param>
		public static void SendEmail
			(
			string from,
			string to,
			string subject,
			string body,
			bool isHtml
			) 
		{
            SendEmail(from, to, subject, body, isHtml, false);
		}

		/// <summary>
		/// Sends a single email. 
		/// </summary>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="subject"></param>
		/// <param name="body"></param>
		/// <param name="bodyFormat"></param>
		/// <param name="smtpServer"></param>
		public static void SendEmail
			(
			string from,
			string to,
			string subject,
			string body,
            bool isHtml,
			bool bSendBCCToKaneva
			) 
		{
			MailMessage message = new MailMessage ();
            message.From = new MailAddress (from);
			message.Bcc.Add (new MailAddress (to));

			if (bSendBCCToKaneva)
			{
				message.Bcc.Add (new MailAddress ("maillogger@kaneva.com"));
			}

			message.Subject = subject;
			message.Body = body;
            message.IsBodyHtml = isHtml;

            SmtpClient smtpClient = new SmtpClient ();
            smtpClient.Host = KanevaGlobals.SMTPServer;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;

			try
			{
                smtpClient.Send (message);
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error sending email", exc);
			}
        
		}

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		private static string C_TD_STYLE = "style='font-family:arial; color:#666666; font-size:12px;' width='60%' align='left' bgcolor='#ffffff'";
   
	}
}
