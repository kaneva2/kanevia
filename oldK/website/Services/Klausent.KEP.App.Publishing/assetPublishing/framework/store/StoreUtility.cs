///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Futureware.MantisConnect;

//using System.Web.Services;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Collections.Specialized;

using log4net;

namespace KlausEnt.KEP.Kaneva
{
    /// <summary>
    /// Summary description for StoreUtility.
    /// </summary>
    public class StoreUtility
    {
        static StoreUtility()
        {
			m_DbNameKanevaNonCluster = KanevaGlobals.DbNameKanevaNonCluster;
        }

        // **********************************************************************************************
        // Asset Functions
        // **********************************************************************************************
        #region Asset Functions

		/// <summary>
		///
		/// </summary>
		/// <param name="assetId"></param>
		/// <param name="fromUserId"></param>
		/// <param name="fromIp"></param>
		/// <param name="toEmail"></param>
		/// <param name="toUserId"></param>
		public static void ShareAsset (int assetId, int fromUserId,
			string fromIp,
			string toEmail,
			int toUserId)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			StringBuilder sb = new StringBuilder() ;
			sb.Append(" INSERT INTO asset_shares ");
			sb.Append(" (from_user_id, asset_id, to_email, created_date, from_ip, ");
			sb.Append(" to_user_id) VALUES (");
			sb.Append(" @from_user_id, @asset_id, @to_email, @created_date, @from_ip, ");
			sb.Append(" @to_user_id) ");


			parameters.Add ("@from_user_id", fromUserId > 0 ? fromUserId : 0);
			parameters.Add ("@asset_id", assetId);
			parameters.Add ("@to_email", toEmail != null ? toEmail : "NULL");
			parameters.Add ("@created_date",  dbUtility.GetCurrentDateTime());
			parameters.Add ("@from_ip", fromIp != null ? fromIp : "NULL") ;
			parameters.Add ("@to_user_id", toUserId > 0 ? toUserId : 0);

			dbUtility.ExecuteNonQuery (sb.ToString(), parameters);
		}

		/// <summary>
		/// when a link sent from "share an asset" email is clicked
		/// </summary>
		/// <param name="keyValue"></param>
		public static void SharedAssetClicked(string keyValue)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string query = " UPDATE asset_shares " +
			" SET clicked = 1 " +
			" WHERE key_value =@keyValue ";

			parameters.Add ("@key_value", keyValue);

			dbUtility.ExecuteNonQuery (query, parameters);
		}

        /// <summary>
        /// Get Asset Details Link
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static string GetAssetDetailsLink(int assetId, Page page)
        {
            return page.ResolveUrl("~/asset/" + assetId.ToString() + ".media");
        }

        /// <summary>
		/// Get Asset Details Link
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public static string GetAssetEmbedLink (int itemId, int height, int width, Control page, bool playList, bool fullUrlRequired )
		{
			return GetAssetEmbedLink (itemId, height, width, page, playList, fullUrlRequired, false, 0 );
		}

		/// <summary>
		/// Get Asset Details Link
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public static string GetAssetEmbedLink (int itemId, int height, int width, Control page, bool playList, bool fullUrlRequired, bool autoStart )
		{
			return GetAssetEmbedLink (itemId, height, width, page, playList, fullUrlRequired, autoStart, 0 );
		}

		/// <summary>
		/// Get Asset Details Link
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public static string GetAssetEmbedLink (int itemId, int height, int width, Control page, bool playList, bool fullUrlRequired, bool autoStart, int userId )
		{
			if ( !playList )
			{
				// get to 4:3
				const int FIXED_HEIGHT = 35; // fixed part of OMM as of today!!
				height = (int)((width * .75) + FIXED_HEIGHT);
			}
			else if ( itemId != Constants.COOL_ITEM_PLAYLIST_ID && itemId != Constants.COOL_ITEM_PLAYLIST_ID_HOME &&
				itemId != Constants.COOL_ITEM_PLAYLIST_ID_MEMBER_CHANNEL && itemId != Constants.COOL_ITEM_PLAYLIST_ID_BROADBAND_CHANNEL &&
				itemId != Constants.COOL_ITEM_PLAYLIST_ID_MEDIA)
			{
				// resize to something around 9:4
				height = (int)(9/4.0 * width);
			}

			string dimensions = "height=\"" + height + "\" width=\"" + width + "\"";
			string urlPrefix = "";

			if ( fullUrlRequired ) // if used for embedding on another page
			{
				itemId = -itemId; // HACKHACKHACK to indicate to request.aspx.cs that it is embedded
				urlPrefix = "http://" + KanevaGlobals.SiteName;
			}
			else
			{
				urlPrefix = "http://" + KanevaGlobals.SiteName;
			}


			string equalSign = "%3d";
			string questionMark = "%3f";
			string amp = "%26";

			string mediaURL = urlPrefix + "/flash/omm/OMM.swf" +
					"?v=3&startMe=" + urlPrefix + "/services/omm/request.aspx" + questionMark + Constants.QS_TYPE + equalSign;

			if ( playList )
			{
				mediaURL += Constants.GET_PLAYLIST + amp + Constants.QS_PLAYLISTID + equalSign + itemId;
				if ( userId != 0 )
				{
					mediaURL += amp + Constants.QS_USERID + equalSign + userId.ToString();
				}
			}
			else
				// for single item, autostart, and turn off detail mode
				mediaURL += Constants.GET_FULL_METADATA + amp + Constants.QS_ASSETID + equalSign + itemId + "&autoStart=" + (autoStart ? "true" : "false") + "&detailMode=" +
					(autoStart ? "false" : "true");

			if ( fullUrlRequired )
				return "<embed src=\"" + mediaURL + "\" quality=\"high\" " + dimensions + " name=\"MediaPlayer\" align=\"middle\" allowScriptAccess=\"sameDomain\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" />";
			else
			{
				m_embedCounter ++;

				string alternateOMMText = "<div class=\"bodytext\"><p/><p/><b style=\"font-size: 14px;\">This computer needs Flash 8 or later to view this video.</b></p><p>To get Flash, <a href=\"http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash\">click here.</a></p></div>";

				return String.Format( "<div id=\"o{0:X}\">" + alternateOMMText + "</div>" +
					"<script type=\"text/javascript\">" +
					"var so = new SWFObject(\"" + mediaURL + "\", \"MediaPlayer\", \"" + width + " \", \"" + height + "\", \"8\", \"#ffffff\"); " +
					"so.addParam(\"quality\", \"high\"); " +
					"so.addParam(\"wmode\", \"transparent\"); " +
					"so.addParam(\"bgcolor\", \"#ffffff\"); " +
					"so.write(\"o{0:X}\"); " +
					"</script> ", m_embedCounter, m_embedCounter );
			}

		}

		/// <summary>
		/// GetYouTubeEmbed
		/// </summary>
		public static string GetYouTubeEmbedLink (string videoId, int height, int width, bool fullUrlRequired, bool autoStart)
		{
			string mediaURL = GetYouTubeSWF (videoId, autoStart);

			if ( fullUrlRequired )
			{
				return "<object width=\"" + width + "\" height=\"" + height + "\">" +
					"<param name=\"movie\" value=\"" + mediaURL + "\"></param>" +
					"<param name=\"wmode\" value=\"transparent\"></param>" +
					"<embed src=\"" + mediaURL + "\" type=\"application/x-shockwave-flash\" wmode=\"transparent\" width=\"" + width + "\" height=\"" + height + "\"></embed>" +
					"</object>";
			}
			else
			{
				string alternateOMMText = "<div class=\"bodytext\"><p/><p/><b style=\"font-size: 14px;\">This computer needs Flash 8 or later to view this video.</b></p><p>To get Flash, <a href=\"http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash\">click here.</a></p></div>";

				return String.Format( "<div id=\"o{0:X}\">" + alternateOMMText + "</div>" +
					"<script type=\"text/javascript\">" +
					"var so = new SWFObject(\"" + mediaURL + "\", \"MediaPlayer\", \"" + width + " \", \"" + height + "\", \"8\", \"#ffffff\"); " +
					"so.addParam(\"quality\", \"high\"); " +
					"so.addParam(\"wmode\", \"transparent\"); " +
					"so.addParam(\"bgcolor\", \"#ffffff\"); " +
					"so.write(\"o{0:X}\"); " +
					"</script> ", m_embedCounter, m_embedCounter );
			}

		}

		/// <summary>
		/// GetYouTubeSWF
		/// </summary>
		public static string GetYouTubeSWF (string videoId, bool autoStart)
		{
			if (autoStart)
			{
				return "http://www.youtube.com/v/" + videoId + "&autoplay=1";
			}
			else
			{
				return "http://www.youtube.com/v/" + videoId;
			}
		}

		/// <summary>
		/// GetFeaturedAssets
		/// </summary>
		/// <returns></returns>
		public static DataTable GetFeaturedAssets()
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlSelect = "SELECT fa.featured_asset_id, asset_id, user_id, start_datetime, end_datetime, shown_count, status_id, playlist_type" +
				" FROM featured_assets fa ";
			
			return dbUtility.GetDataTable(sqlSelect);
		}

		/// <summary>
		/// InsertFeaturedAsset - requires all table values
		/// </summary>
		/// <returns></returns>
		public static int InsertFeaturedAsset (int assetId, int userId, DateTime startDate, DateTime endDate, int shownCount, int status, int playListType)
		{
			DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

			// Record User Asset Subscriptions
			string sqlInsert = "INSERT INTO featured_assets " +
				" (asset_id, user_id, start_datetime, end_datetime, shown_count, status_id, playlist_type) " +
				" VALUES " +
				" (@assetId, @userId, @startDateTime, @endDateTime, @shownCount, @statusId, @playListType)";

			int featuredAssetId = 0;
			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@userId", userId);
			parameters.Add ("@startDateTime", startDate);
			parameters.Add ("@endDateTime", endDate);
			parameters.Add ("@shownCount", shownCount);
			parameters.Add ("@statusId", status);
			parameters.Add ("@playListType", playListType);
			dbUtility.ExecuteIdentityInsert (sqlInsert, parameters, ref featuredAssetId);
			return featuredAssetId;
		}

		/// <summary>
		/// UpdateFeaturedAsset - requires all table column values
		/// </summary>
		/// <param name="featuredAssetId"></param>
		/// <param name="status"></param>
		/// <returns></returns>
		public static int UpdateFeaturedAsset (int featuredAssetId, int assetId, int userId, DateTime startDate, DateTime endDate, int shownCount, int status, int playListType)
		{
			DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

			string sqlUpdate = "UPDATE featured_assets SET " + 
				"asset_id = @assetId, " +
				"user_id = @userId, " +
				"start_datetime = @startDateTime, " +
				"end_datetime = @endDateTime, " +
				"shown_count = @shownCount, " +
				"status_id = @statusId, " +
				"playlist_type = @playListType " +
				" WHERE featured_asset_id = @faId";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@userId", userId);
			parameters.Add ("@startDateTime", startDate);
			parameters.Add ("@endDateTime", endDate);
			parameters.Add ("@shownCount", shownCount);
			parameters.Add ("@statusId", status);
			parameters.Add ("@playListType", playListType);
			parameters.Add ("@faId",  featuredAssetId);
			return dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
		}

		/// <summary>
		/// DeleteFeaturedAsset
		/// </summary>
		/// <param name="id">the id of the asset</param>
		/// <returns></returns>
		public static void DeleteFeaturedAsset(int featuredAssetId)
		{
			//delete from contest table
			string sqlString = "DELETE FROM featured_assets WHERE featured_asset_id = @featuredAssetId ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@featuredAssetId", featuredAssetId);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}


		/// <summary>
		/// Get the correct Media Type
		/// </summary>
		public static string GetMediaImageURL (string imagePath, string size, int assetId, int assetTypeId, int thumbGenerated, Page page)
		{
			if (assetTypeId.Equals ((int) Constants.eASSET_TYPE.PICTURE) || assetTypeId.Equals ((int) Constants.eASSET_TYPE.PATTERN))
			{
				return GetPhotoImageURL (imagePath, size);
			}
			else if (assetTypeId.Equals ((int) Constants.eASSET_TYPE.MUSIC))
			{
				return GetMusicImageURL (imagePath, size);
			}
			else if (assetTypeId.Equals ((int) Constants.eASSET_TYPE.GAME))
			{
				return GetGameImageURL (imagePath, size);
			}
            else if (assetTypeId.Equals((int)Constants.eASSET_TYPE.TV))
            {
                return GetTVImageURL(imagePath, size);
            }
            else if (assetTypeId.Equals((int)Constants.eASSET_TYPE.WIDGET))
            {
                return GetWidgetImageURL(imagePath, size);
            }
            else
			{
				if (thumbGenerated.Equals (1))
				{
					return GetVideoImageURL (imagePath, size);
				}
				else
				{
					return page.ResolveUrl ("~/displayImage.aspx?assetId=" + assetId + "&sd=Y&Resize=Y&MaxWidth=76&MaxHeight=56");
				}
			}
		}

        /// <summary>
        /// GetWidgetImageURL
        /// </summary>
        /// <param name="imagePath">The relative path to the image on the NAS. Example - 4/22/sports.gif</param>
        /// <param name="defaultSize">The default size of the image if the user has not uploaded a photo. Example - 'sm', 'me', 'la', 'xl'</param>
        /// <returns></returns>
        public static string GetWidgetImageURL(string imagePath, string defaultSize)
        {
            if (imagePath.Length.Equals(0))
            {
                return KanevaGlobals.ImageServer + "/KanevaIconWidget_" + defaultSize + ".gif";
            }
            else
            {
                return KanevaGlobals.ImageServer + "/" + imagePath;
            }
        }

        /// <summary>
        /// GetTVImageURL
        /// </summary>
        /// <param name="imagePath">The relative path to the image on the NAS. Example - 4/22/sports.gif</param>
        /// <param name="defaultSize">The default size of the image if the user has not uploaded a photo. Example - 'sm', 'me', 'la', 'xl'</param>
        /// <returns></returns>
        public static string GetTVImageURL(string imagePath, string defaultSize)
        {
            if (imagePath.Length.Equals(0))
            {
                return KanevaGlobals.ImageServer + "/KanevaIconTV_" + defaultSize + ".gif";
            }
            else
            {
                return KanevaGlobals.ImageServer + "/" + imagePath;
            }
        }

        /// <summary>
        /// GetPhotoImageURL
        /// </summary>
        /// <param name="imagePath">The relative path to the image on the NAS. Example - 4/22/sports.gif</param>
        /// <param name="defaultSize">The default size of the image if the user has not uploaded a photo. Example - 'sm', 'me', 'la', 'xl'</param>
        /// <returns></returns>
        public static string GetPhotoImageURL(string imagePath, string defaultSize)
        {
            if (imagePath.Length.Equals(0))
            {
                return KanevaGlobals.ImageServer + "/KanevaIconMale_" + defaultSize + ".gif";
            }
            else
            {
                return KanevaGlobals.ImageServer + "/" + imagePath;
            }
        }

        /// <summary>
		/// GetMusicImageURL
		/// </summary>
		/// <param name="imagePath">The relative path to the image on the NAS. Example - 4/22/sports.gif</param>
		/// <param name="defaultSize">The default size of the image if the user has not uploaded a photo. Example - 'sm', 'me', 'la', 'xl'</param>
		/// <returns></returns>
		public static string GetMusicImageURL (string imagePath, string defaultSize)
		{
			if (imagePath.Length.Equals (0))
			{
				return KanevaGlobals.ImageServer + "/KanevaIconAudio_" + defaultSize + ".gif";
			}
			else
			{
				return KanevaGlobals.ImageServer + "/" + imagePath;
			}
		}

		/// <summary>
		/// GetGameImageURL
		/// </summary>
		/// <param name="imagePath">The relative path to the image on the NAS. Example - 4/22/sports.gif</param>
		/// <param name="defaultSize">The default size of the image if the user has not uploaded a photo. Example - 'sm', 'me', 'la', 'xl'</param>
		/// <returns></returns>
		public static string GetGameImageURL (string imagePath, string defaultSize)
		{
			if (imagePath.Length.Equals (0))
			{
				return KanevaGlobals.ImageServer + "/KanevaIcon01_" + defaultSize + ".gif";
			}
			else
			{
				return KanevaGlobals.ImageServer + "/" + imagePath;
			}
		}

		/// <summary>
		/// GetVideoImageURL
		/// </summary>
		/// <param name="imagePath">The relative path to the image on the NAS. Example - 4/22/sports.gif</param>
		/// <param name="defaultSize">The default size of the image if the user has not uploaded a photo. Example - 'sm', 'me', 'la', 'xl'</param>
		/// <returns></returns>
		public static string GetVideoImageURL (string imagePath, string defaultSize)
		{
			if (imagePath.Length.Equals (0))
			{
				return KanevaGlobals.ImageServer + "/KanevaIcon01_" + defaultSize + ".gif";
			}
			else
			{
				return KanevaGlobals.ImageServer + "/" + imagePath;
			}
		}

		/// <summary>
		/// Return the banner image URL for a widget
		/// </summary>
		public static string GetWidgetBannerImageURL ( string imagePath )
		{
			return KanevaGlobals.ImageServer + "/" + imagePath;
		}

		/// <summary>
		/// Return the gift image URL, since used in game, must be jpg
		/// </summary>
		public static string GetGiftImageURL ( string giftId )
		{
			if (giftId.Length.Equals (0))
			{
				return KanevaGlobals.ImageServer + "/gifts/generic.jpg";
			}
			else
			{
				return KanevaGlobals.ImageServer + "/gifts/" + giftId + ".jpg";
			}
		}

		/// <summary>
		/// Return the gift image URL, since used in game, must be jpg
		/// </summary>
		public static string GetGiftIcon()
		{
			return KanevaGlobals.ImageServer + "/gifts/icon.jpg";
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="orderby"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pgSize"></param>
		public static PagedDataTable Get2DGiftCatalog( string orderby, int pageNumber, int pageSize)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			string selectList = "`gift_id`, `name`, `description`, `gifted_count`, `price` ";

			string tableList = "`gift_catalog_items` ";

			return dbUtility.GetPagedDataTable (selectList, tableList, "", orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="orderby"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pgSize"></param>
		public static PagedDataTable Get3DGiftCatalog( int catalog_id, string orderby, int pageNumber, int pageSize)
		{
			string dbNameForGame = KanevaGlobals.DbNameKGP;

			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			string selectList = "`gift_id`, `name`, `description`, `gifted_count`, `price` ";

			string tableList = dbNameForGame + ".`gift_catalog_3d_items` ";

			string whereClause = "catalog_id = @catId";
			parameters.Add("@catId", catalog_id );

			return dbUtility.GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// Get the rave icon for asset
		/// 		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public static string GetRaveIcon(int diggId, Page page)
		{
			if (diggId > 0)
			{
				// Already dugg
				return (page.ResolveUrl("~/images/icons/bb_raves.gif"));
			}

			return (page.ResolveUrl("~/images/icons/raveit_bt.gif"));
		}

		/// <summary>
        /// IsStreamable
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static bool IsAssetStreamable (int assetId)
        {
            // If it is not a game, less then 20MB, is correct file type, and subscribed to streaming
            DataRow drAsset = StoreUtility.GetAsset (assetId);
            return IsAssetStreamable (drAsset);
        }

        /// <summary>
        /// IsStreamable
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static bool IsAssetStreamable (DataRow drAsset)
        {
            // If it is not a game, less then 20MB, is correct file type, and subscribed to streaming
            if (drAsset == null)
            {
                return false;
            }

            // Can't stream game type
            if (IsKanevaGame (drAsset))
            {
                return false;
            }

            // Make sure it is one of the supported file types
            if (!drAsset ["content_extension"].Equals (DBNull.Value))
            {
                if (IsFileStreamable (drAsset ["content_extension"].ToString ()))
                {
//                    // Make sure it is under the max limit  (20972000 bytes is 20MB)
//                    if (!drAsset ["file_size"].Equals (DBNull.Value) && (Convert.ToInt64 (drAsset ["file_size"]) < (KanevaGlobals.MaxFileSizeToStream))
//                    {
//                        return true;
//                    }
					// Take out the size check completely per Animesh/Chris K
					return true;
                }
            }

            return false;
        }

        /// <summary>
        /// IsQuickTimeContent
        /// </summary>
        /// <param name="drAsset"></param>
        public static bool IsQuickTimeContent (DataRow drAsset)
        {
            if (drAsset == null)
            {
                return false;
            }

            // Can't stream game type
            if (IsKanevaGame (drAsset))
            {
                return false;
            }

            // Make sure it is one of the supported file types
            if (!drAsset ["content_extension"].Equals (DBNull.Value))
            {
                string contentExtension = System.IO.Path.GetExtension (drAsset ["content_extension"].ToString ()).ToUpper ();
                return (contentExtension.Equals (".MOV") || contentExtension.Equals (".MP4"));
            }

            return false;
        }

		/// <summary>
		/// IsOMMContent
		/// </summary>
		/// <param name="drAsset"></param>
		public static bool IsOMMContent (DataRow drAsset)
		{
			if (drAsset == null)
			{
				return false; 
			}

			// Make sure it is one of the supported file types
			if (!drAsset ["content_extension"].Equals (DBNull.Value))
			{
				string contentExtension = System.IO.Path.GetExtension (drAsset ["content_extension"].ToString ()).ToUpper ();
				return ((contentExtension.Equals (".FLV") || contentExtension.Equals (".MP3")) && Convert.ToInt32 (drAsset ["asset_sub_type_id"]).Equals ((int) Constants.eASSET_SUBTYPE.ALL));
			}

			return false;
		}

		/// <summary>
		/// IsYouTubeContent
		/// </summary>
		/// <param name="drAsset"></param>
		public static bool IsYouTubeContent (DataRow drAsset)
		{
			if (drAsset == null)
			{
				return false; 
			}

			return (Convert.ToInt32 (drAsset ["asset_type_id"]).Equals ((int) Constants.eASSET_TYPE.VIDEO) && Convert.ToInt32 (drAsset ["asset_sub_type_id"]).Equals ((int) Constants.eASSET_SUBTYPE.YOUTUBE));
		}

		/// <summary>
		/// IsStreamingTVContent
		/// </summary>
		/// <param name="drAsset"></param>
		public static bool IsStreamingTVContent (DataRow drAsset)
		{
			if (drAsset == null)
			{
				return false; 
			}

			return (Convert.ToInt32 (drAsset ["asset_type_id"]).Equals ((int) Constants.eASSET_TYPE.TV));
		}

		/// <summary>
		/// IsStreamingTVContent
		/// </summary>
		/// <param name="drAsset"></param>
		public static bool IsFlashWidget (DataRow drAsset)
		{
			if (drAsset == null)
			{
				return false; 
			}

			return (Convert.ToInt32 (drAsset ["asset_type_id"]).Equals ((int) Constants.eASSET_TYPE.WIDGET));
		}

		/// <summary>
		/// IsFlashContent
		/// </summary>
		/// <param name="drAsset"></param>
		public static bool IsFlashContent (DataRow drAsset)
		{
			if (drAsset == null)
			{
				return false;
			}

			// Can't stream game type
			if (IsKanevaGame (drAsset))
			{
				return false;
			}

			// Make sure it is one of the supported file types
			if (!drAsset ["content_extension"].Equals (DBNull.Value))
			{
				string contentExtension = System.IO.Path.GetExtension (drAsset ["content_extension"].ToString ()).ToUpper ();
				return (contentExtension.Equals (".SWF"));
			}
			else if(drAsset ["asset_type_id"].Equals ((int) Constants.eASSET_TYPE.WIDGET))
			{
				string contentExtension = (System.IO.Path.GetExtension (drAsset ["asset_offsite_id"].ToString ()).ToUpper ()).Substring(0,4);
				return (contentExtension.Equals (".SWF"));
			}

			return false;
		}

        /// <summary>
        /// IsAudioContent
        /// </summary>
        /// <param name="drAsset"></param>
        public static bool IsAudioContent (DataRow drAsset)
        {
            if (drAsset == null)
            {
                return false;
            }

            // Make sure it is one of the supported file types
            if (!drAsset ["content_extension"].Equals (DBNull.Value))
            {
                // NOTE: MP4 may contain video, MP4a is audio only
                string contentExtension = System.IO.Path.GetExtension (drAsset ["content_extension"].ToString ()).ToUpper ();
                return IsFileMusic (contentExtension);
            }

            return false;
        }

		/// <summary>
		/// IsImageContent
		/// </summary>
		/// <param name="drAsset"></param>
		public static bool IsImageContent (DataRow drAsset)
		{
			if (drAsset == null)
			{
				return false;
			}

			// Make sure it is one of the supported file types
			if (!drAsset ["content_extension"].Equals (DBNull.Value))
			{
				// NOTE: MP4 may contain video, MP4a is audio only
				return IsImageContent (drAsset ["content_extension"].ToString ());
			}

			return false;
		}

		/// <summary>
		/// IsImageContent
		/// </summary>
		/// <param name="drAsset"></param>
		public static bool IsImageContent (string contentExtension)
		{
			contentExtension = System.IO.Path.GetExtension (contentExtension).ToUpper ();
			return (contentExtension.Equals (".JPG") || contentExtension.Equals (".GIF"));
		}

        /// <summary>
        /// IsFileStreamable
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static bool IsFileStreamable (string fileName)
        {
            string contentExtension = System.IO.Path.GetExtension (fileName).ToUpper ();

            return  IsFilePicture (contentExtension) || IsFileVideo (contentExtension) ||
                IsFileGame (contentExtension) || IsFileMusic (contentExtension);
        }

		/// <summary>
		/// IsFilePicture
		/// </summary>
		public static bool IsFilePicture (string contentExtension)
		{
			return Constants.FILE_TYPE_PICTURE.Contains(contentExtension.ToUpper());
		}

		/// <summary>
		/// IsFileVideo
		/// </summary>
		public static bool IsFileVideo (string contentExtension)
		{
			return Constants.FILE_TYPE_VIDEO.Contains(contentExtension.ToUpper());
		}

		/// <summary>
		/// IsFileGame
		/// </summary>
		public static bool IsFileGame (string contentExtension)
		{
			return Constants.FILE_TYPE_GAME.Contains(contentExtension.ToUpper());
		}

		/// <summary>
		/// IsFileMusic
		/// </summary>
		public static bool IsFileMusic (string contentExtension)
		{
			return Constants.FILE_TYPE_MUSIC.Contains(contentExtension.ToUpper());
		}

        /// <summary>
        /// GetPopularTags
        /// </summary>
        /// <returns></returns>
        public static DataTable GetPopularTags (int assetTypeId, int minTagCount, int limit)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string sqlSelect = "";

            if (assetTypeId != 0)
            {
                parameters.Add ("@assetTypeId", assetTypeId);

                // We want a specific asset type
                sqlSelect += "SELECT keyword, total_count, asset_type_id " +
                    " FROM popular_keywords " +
                    " WHERE asset_type_id = @assetTypeId " +
                    " AND total_count >= @minTagCount " +
                    " ORDER BY total_count DESC, keyword ";
            }
            else
            {
				// All asset types
				sqlSelect += "SELECT * from ( " +
					" SELECT keyword, SUM(total_count) AS total_count, asset_type_id " +
					" FROM popular_keywords " +
					" WHERE total_count >= @minTagCount " +
					" AND asset_type_id <> " + cCOMMUNITY_POPULAR_KEYWORD_TYPE +
					" GROUP BY keyword, asset_type_id " +
					" ORDER BY total_count DESC " +
					" ) X GROUP BY X.keyword " +
					" ORDER BY X.total_count DESC ";
			}

            sqlSelect += " limit 0," + limit;

            parameters.Add ("@minTagCount", minTagCount);
            return dbUtility.GetDataTable (sqlSelect, parameters);
        }

        /// <summary>
        /// AddToHash
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="keywordTags"></param>
        public static void AddToHash (char delimiter, Hashtable keywords, string keywordTags)
        {
			char [] splitter = {delimiter}; //= {' '};
            string [] values = keywordTags.Split (splitter);
            string theValue;

            for (int i = 0; i < values.Length; i ++)
            {
                theValue = values [i].ToString ().Trim ();

                // You can use a tag once per item, and Tags must be at least 2 characters.
                if (!keywords.ContainsKey (theValue) && theValue.Length > 1)
                {
					// Make sure it is not a potty word
					if (!KanevaGlobals.IsPottyMouth (theValue))
					{
						keywords.Add (theValue, values [i].ToString ());
					}
                }
            }
        }

		public static void AddToHash (Hashtable keywords, string keywordTags)
		{
			AddToHash (' ', keywords, keywordTags);
		}

        /// <summary>
        /// IsAssetFeatured
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static bool IsAssetFeatured (int assetId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "SELECT a.asset_id" +
                " FROM assets a, featured_assets fa " +
                " WHERE a.asset_id = fa.asset_id " +
                " AND fa.status_id = " + (int) Constants.eUSER_LICENSE_SUBSCRIPTION_STATUS.ACTIVE +
                " AND fa.start_datetime <= " + dbUtility.GetCurrentDateFunction () +
                " AND (fa.end_datetime >= " + dbUtility.GetCurrentDateFunction () + " OR fa.end_datetime IS NULL)" +
                " AND a.asset_id = @assetId" +
				SQLCommon_GetPublishedAssetsSQL ();

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            DataRow drResult = dbUtility.GetDataRow (sqlSelect, parameters, false);
            return (drResult != null);
        }

		/// <summary>
		/// GetCoolAssets
		/// </summary>
		/// <returns></returns>
		public static PagedDataTable GetCoolAssets (int assetTypeId, string filter, int pageNumber, int pageSize)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			string selectList = "a.asset_id, a.asset_type_id, a.asset_sub_type_id, a.name, a.teaser, rand() as RandomOrder1, a.file_size, a.run_time_seconds, " +
				" a.owner_username, a.asset_rating_id, a.mature, a.amount, a.keywords, a.thumbnail_small_path, " +
				" ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs, ass.number_of_channels, " +
				" com.thumbnail_small_path as owner_thumbnail_small_path ";

			string tableList = " featured_assets fa, vw_published_public_mature_assets a " +
				" INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id " +
				" INNER JOIN communities_personal com ON a.owner_id = com.creator_id ";

			string whereClause = "a.asset_id = fa.asset_id " +
				" AND fa.status_id = " + (int) Constants.eUSER_LICENSE_SUBSCRIPTION_STATUS.ACTIVE +
				" AND fa.start_datetime <= " + dbUtility.GetCurrentDateFunction () +
				" AND (fa.end_datetime >= " + dbUtility.GetCurrentDateFunction () + " OR fa.end_datetime IS NULL)";

			// Specific asset type?
			if (assetTypeId > 0)
			{
				parameters.Add ("@assetTypeId", assetTypeId);
				whereClause += " AND a.asset_type_id = @assetTypeId ";
			}

			if (filter.Length > 0)
			{
				whereClause += " AND " + filter;
			}

			string orderby = "RandomOrder1";

			return dbUtility.GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}


		/// <summary>
        /// Get the Asset for the given assetID
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static DataRow GetAsset (int assetId)
        {
            return GetAsset (assetId, false, 0);
        }

		public static DataRow GetAsset (int assetId, bool includeDeleted)
		{
			return GetAsset (assetId, includeDeleted, 0);
		}

		/// <summary>
        /// Get the Asset for the given assetID
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static DataRow GetAsset (int assetId, bool includeDeleted, int userId)
        {
			Hashtable parameters = new Hashtable ();
			string strSelect;
			string strJoin = string.Empty;

			if (userId > 0)
			{
				strSelect = " COALESCE(d.digg_id, 0) as digg_id,";
				strJoin = " LEFT OUTER JOIN asset_diggs d ON d.asset_id = a.asset_id AND d.user_id = @userId";
				parameters.Add ("@userId", userId);
			}
			else
			{
				strSelect = " 0 as digg_id,";
			}

            string sqlSelect = "SELECT file_size, a.content_extension, a.target_dir, " +
                " a.asset_id, a.asset_type_id, a.asset_sub_type_id, a.name, a.body_text, a.short_description, a.is_kaneva_game, " +
                " a.asset_rating_id, a.mature, a.owner_id, a.require_login, a.run_time_seconds, " +
                " a.category1_id, a.category2_id, a.category3_id, a.amount, a.kei_point_id, a.asset_offsite_id, " +
                " a.image_caption, " +
                " a.allow_ratings, a.allow_comments, a.created_date, a.status_id, a.teaser, a.patch_server_url, " +
                " a.owner_username as username, a.owner_id as user_id, " +
                " a.thumbnail_small_path, a.thumbnail_medium_path, a.thumbnail_large_path, a.thumbnail_xlarge_path, a.thumbnail_assetdetails_path, a.image_full_path, a.thumbnail_gen, " +
                strSelect +
                " CONCAT_WS(',', ac1.name, ac2.name, ac3.name) as categories, " +
                " a.publish_status_id, " +
                " a.license_name, a.license_additional, a.license_cc, a.license_URL, a.license_type, a.keywords, " +
                " a.permission, a.permission_group, u.zip_code, " +
                " ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs, ass.number_of_channels " +
                " FROM assets a " +
                " INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id " +
                " INNER JOIN users u ON u.user_id = a.owner_id " +
                strJoin +
                " LEFT OUTER JOIN asset_categories ac1 ON ac1.category_id = a.category1_id " +
                " LEFT OUTER JOIN asset_categories ac2 ON ac2.category_id = a.category2_id " +
                " LEFT OUTER JOIN asset_categories ac3 ON ac3.category_id = a.category3_id " +
                " WHERE a.asset_id = @assetId";

            // Don't include deleted?
            if (!includeDeleted)
            {
                sqlSelect += SQLCommon_GetOnlyActiveStatusAssetsSQL ();
            }

            parameters.Add ("@assetId", assetId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, true);
        }

        /// <summary>
        /// GetAssets
        /// </summary>
        public static DataTable GetAssets (int channelId)
        {
            string sqlSelect = "SELECT a.asset_id " +
                " FROM assets a, asset_channels ac " +
                " WHERE ac.channel_id = @channelId" +
				" AND a.asset_id = ac.asset_id ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@channelId", channelId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
        }

		/// <summary>
		/// Get the assets to add to a channel
		/// </summary>
		public static DataTable GetAssets (int channelId, string assetIds)
		{
            string sqlString = " SELECT a.asset_id, a.name " +
                " FROM assets a, asset_channels ac " +
                " WHERE a.asset_id = ac.asset_id " +
                " AND a.asset_id IN (" + assetIds + ")" +
                " AND ac.channel_id = @channelId " +
                " ORDER BY a.name ASC "; /* +
				" LIMIT 20";  // Limit to to prevent users from abusing system adding to many assets to channels
            */
			Hashtable parameters = new Hashtable ();
			parameters.Add ("@channelId", channelId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlString, parameters);
		}

		/// <summary>
		/// GetAssetsInChannelForGame
		/// </summary>
		public static PagedDataTable GetAssetsInChannelForGame (int channelId, bool bGetMature, bool wantPictures, bool wantPatterns, string filter, string orderby, int pageNumber, int pageSize)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string selectList = "DISTINCT file_size, " +
				" a.content_extension, a.asset_id, a.asset_type_id, a.name, a.short_description, a.created_date, " +
				" a.thumbnail_medium_path " ;

			string tableList = "asset_channels ac, " +
				" assets a ";


			string whereClause =  " ac.asset_id = a.asset_id " +
				" AND ac.channel_id = @channelId ";

			// Only show assets that have been processed
			whereClause += SQLCommon_GetPublishedAssetsSQL ();

			if (!bGetMature)
			{
				whereClause += SQLCommon_GetNonMatureAssets ();
			}

			// Specific asset type?
			if ( wantPictures || wantPatterns )
			{
				whereClause += " AND a.asset_type_id IN ( ";
				if ( wantPictures )
					whereClause += (int)Constants.eASSET_TYPE.PICTURE;
				if ( wantPictures && wantPatterns )
					whereClause += ",";
				if ( wantPatterns )
					whereClause += (int)Constants.eASSET_TYPE.PATTERN;
				whereClause += ") ";
			}

			if (filter.Length > 0)
			{
				whereClause += " AND " + filter;
			}

			parameters.Add ("@channelId", channelId);
			return dbUtility.GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// GetTotalMediaCounts
		/// </summary>
		public static DataRow GetTotalMediaCounts ()
		{
			Hashtable parameters = new Hashtable ();

			string sqlString = "SELECT " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.VIDEO + ",count,0)) AS sum_video, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.MUSIC + ",count,0)) AS sum_music, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.PICTURE + ",count,0)) AS sum_picture, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.GAME + ",count,0)) AS sum_game, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.PATTERN + ",count,0)) AS sum_pattern, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.TV + ",count,0)) AS sum_tv, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.WIDGET + ",count,0)) AS sum_widget, " +
				" sum(count) as sum_total " +
				" FROM summary_assets_by_type ";

			return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlString, parameters, false);
		}

		/// <summary>
		/// GetChannelMediaCounts
		/// </summary>
		public static DataRow GetChannelMediaCounts (int channelId)
		{
			Hashtable parameters = new Hashtable ();

			string sqlString = "SELECT " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.VIDEO + ",count,0)) AS sum_video, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.MUSIC + ",count,0)) AS sum_music, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.PICTURE + ",count,0)) AS sum_picture, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.GAME + ",count,0)) AS sum_game, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.PATTERN + ",count,0)) AS sum_pattern, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.TV + ",count,0)) AS sum_tv, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.WIDGET + ",count,0)) AS sum_widget, " +
				" sum(count) as sum_total " +
				" FROM summary_assets_by_type_by_channel " +
				" WHERE channel_id = @channelId ";

			parameters.Add ("@channelId", channelId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlString, parameters, false);
		}

		/// <summary>
		/// GetUserMediaCounts
		/// </summary>
		public static DataRow GetUserMediaCounts (int userId)
		{
			Hashtable parameters = new Hashtable ();

			string sqlString = "SELECT " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.VIDEO + ",count,0)) AS sum_video, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.MUSIC + ",count,0)) AS sum_music, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.PICTURE + ",count,0)) AS sum_picture, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.GAME + ",count,0)) AS sum_game, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.PATTERN + ",count,0)) AS sum_pattern, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.TV + ",count,0)) AS sum_tv, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.WIDGET + ",count,0)) AS sum_widget, " +
				" sum(count) as sum_total " +
				" FROM summary_assets_by_type_by_user " +
				" WHERE user_id = @userId ";

			parameters.Add ("@userId", userId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlString, parameters, false);
		}

		/// <summary>
		/// GetUserMediaCounts
		/// </summary>
		public static DataRow GetUserPublicMediaCounts (int userId)
		{
			Hashtable parameters = new Hashtable ();

			string sqlString = "SELECT " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.VIDEO + ",count,0)) AS sum_video, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.MUSIC + ",count,0)) AS sum_music, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.PICTURE + ",count,0)) AS sum_picture, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.GAME + ",count,0)) AS sum_game, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.PATTERN + ",count,0)) AS sum_pattern, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.TV + ",count,0)) AS sum_tv, " +
				" sum(IF(asset_type_id=" + (int) Constants.eASSET_TYPE.WIDGET + ",count,0)) AS sum_widget, " +
				" sum(count) as sum_total " +
				" FROM summary_public_assets_by_type_by_user " +
				" WHERE user_id = @userId ";

			parameters.Add ("@userId", userId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlString, parameters, false);
		}

		/// <summary>
		/// GetConnectedMediaCount
		/// </summary>
		public static int GetConnectedMediaCount (int channelId)
		{
			Hashtable parameters = new Hashtable ();

			// Make sure this asset name does not already exist
			string sqlString = "SELECT count " +
				" FROM summary_assets_by_connection " +
				" WHERE channel_id = @channelId";

			parameters.Add ("@channelId", channelId);
			DataRow drAssetConnection = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlString, parameters, false);

			if (drAssetConnection == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32 (drAssetConnection ["count"]);
			}
		}

		/// <summary>
		/// GetConnectedMedia
		/// </summary>
		public static PagedDataTable GetConnectedMedia (int channelId, int userId, string orderBy, int pageNumber, int pageSize)
		{
			return GetConnectedMedia (channelId, userId, "", orderBy, pageNumber, pageSize);
		}
		/// <summary>
		/// GetConnectedMedia
		/// </summary>
		public static PagedDataTable GetConnectedMedia (int channelId, int userId, string filter, string orderBy, int pageNumber, int pageSize)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlSelect = "SELECT file_size, " +
				" a.content_extension, a.asset_id, a.asset_type_id, a.name, a.short_description, a.created_date, " +
				" a.asset_rating_id, a.amount, a.publish_status_id,  a.status_id, a.run_time_seconds, " +
				" a.owner_id, a.owner_id as user_id, a.owner_username, a.owner_username as username, a.teaser, " +
				" a.keywords, a.permission, a.permission_group, " +
				" a.thumbnail_medium_path, a.thumbnail_gen " +
				" FROM assets a " +
				" INNER JOIN asset_channels ac ON a.asset_id = ac.asset_id " +
				" WHERE ac.channel_id = @channelId " +
				" AND a.owner_id <> @userId ";

			if (filter.Length > 0)
			{
				sqlSelect += " AND " + filter;
			}

			parameters.Add ("@channelId", channelId);
			parameters.Add ("@userId", userId);
			return dbUtility.GetPagedDataTableUnion (sqlSelect, orderBy, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// Get a list of Assets
		/// </summary>
		public static PagedDataTable GetAssetsInChannel (int channelId, bool bOnlyPublished, bool bGetMature, int assetTypeId, string filter, string orderby, int pageNumber, int pageSize)
		{
			return	 GetAssetsInChannel (channelId, bOnlyPublished, bGetMature, assetTypeId, 0, filter, orderby, pageNumber, pageSize);
		}

		/// <summary>
		/// Get a list of Assets
		/// </summary>
		public static PagedDataTable GetAssetsInChannel (int channelId, bool bOnlyPublished, bool bGetMature,
			int assetTypeId, int userId, string filter, string orderby, int pageNumber, int pageSize)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string selectList = "DISTINCT file_size, " +
				" a.content_extension, a.asset_id, a.asset_type_id, a.name, a.short_description, a.created_date, " +
				" a.asset_rating_id, a.amount, a.publish_status_id,  a.status_id, a.run_time_seconds, " +
				" a.owner_id, a.owner_id as user_id, a.owner_username, a.owner_username as username, a.teaser, " +
				" a.keywords, a.permission, a.permission_group, " +
				" CONCAT_WS(',', ac1.name, ac2.name, ac3.name) as categories, a.category1_id, a.category2_id, a.category3_id, " +
				" a.thumbnail_medium_path, a.thumbnail_assetdetails_path, a.thumbnail_small_path, a.thumbnail_large_path, a.thumbnail_gen, " +
				" ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs, " + 
				" IF(a.asset_rating_id = 3, true, IF(a.asset_rating_id = 6, true, IF(a.asset_rating_id = 9, true, false ))) AS mature_profile ";

			string tableList = "asset_channels ac, " +
				" assets a " +
				" INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id " +
				" LEFT OUTER JOIN asset_categories ac1 ON ac1.category_id = a.category1_id " +
				" LEFT OUTER JOIN asset_categories ac2 ON ac2.category_id = a.category2_id " +
				" LEFT OUTER JOIN asset_categories ac3 ON ac3.category_id = a.category3_id ";

			string whereClause =  " ac.asset_id = a.asset_id " +
				" AND ac.channel_id = @channelId ";

			if (userId > 0)
			{
				selectList += ", COALESCE(d.digg_id, 0) as digg_id";
				tableList += " LEFT OUTER JOIN asset_diggs d ON d.asset_id = a.asset_id AND d.user_id = @userId";
				parameters.Add ("@userId", userId);
			}
			else
			{
				selectList += ", 0 as digg_id";
			}

			if (bOnlyPublished)
			{
				// Only show assets that have been processed
				whereClause += SQLCommon_GetPublishedAssetsSQL ();
			}
			else
			{
				whereClause += SQLCommon_GetOnlyActiveStatusAssetsSQL ();
			}

			if (!bGetMature)
			{
				whereClause += SQLCommon_GetNonMatureAssets ();
			}

			// Specific asset type?
			if (assetTypeId > 0)
			{
				parameters.Add ("@assetTypeId", assetTypeId);
				whereClause += " AND a.asset_type_id = @assetTypeId ";
			}

			if (filter.Length > 0)
			{
				whereClause += " AND " + filter;
			}

			parameters.Add ("@channelId", channelId);
			return dbUtility.GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// Get a list of Assets accessible by user
		/// </summary>
		public static PagedDataTable GetAccessibleAssetsInChannel (int channelId, int assetGroupId, bool bGetMature,
			int assetTypeId, int userId, string filter, string orderby, int pageNumber, int pageSize)
		{
			Hashtable parameters = new Hashtable ();
			parameters.Add ("@channelId", channelId);

			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			//common select and where clauses for every sub query
			StringBuilder basicSelectList = new StringBuilder() ;
			basicSelectList.Append (" DISTINCT a.asset_id, file_size, ");
			basicSelectList.Append (" a.content_extension, a.asset_type_id, a.asset_sub_type_id, a.name, ");
			basicSelectList.Append (" a.short_description, a.created_date, a.asset_rating_id, a.amount, ");
			basicSelectList.Append (" a.status_id, ");
			basicSelectList.Append (" a.run_time_seconds, a.owner_id, ");
			basicSelectList.Append (" a.owner_id as user_id, a.owner_username, a.owner_username as username, ");
			basicSelectList.Append (" a.teaser, a.keywords, ");
			basicSelectList.Append (" a.permission, a.permission_group, ");
			basicSelectList.Append (" a.image_full_path, a.thumbnail_small_path, thumbnail_medium_path, thumbnail_large_path, thumbnail_xlarge_path, thumbnail_gen, ");
			basicSelectList.Append (" ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs ");

			// Build the basic FROM Clause
			StringBuilder basicFromClause = new StringBuilder () ;
			basicFromClause.Append (" asset_channels ac, ");

			if (bGetMature)
			{
				basicFromClause.Append (" vw_published_mature_assets  a ");
			}
			else
			{
				basicFromClause.Append (" vw_published_assets  a ");
			}

			basicFromClause.Append (" INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id ");

			if (assetGroupId > 0)
			{
				basicFromClause.Append(" ,asset_groups ag, asset_group_assets aga ");
			}

			// Build the basic WHERE Clause
			StringBuilder basicWhereClause = new StringBuilder () ;
			basicWhereClause.Append (" ac.channel_id= @channelId ");
			basicWhereClause.Append (" AND ac.asset_id=a.asset_id ");

			// Specific asset type
			if (assetTypeId > 0)
			{
				parameters.Add ("@assetTypeId", assetTypeId);
				basicWhereClause.Append (" AND a.asset_type_id = @assetTypeId ");
			}

			if (filter.Length > 0)
			{
				basicWhereClause.Append (" AND ").Append (filter);
			}

			if( assetGroupId > 0)
			{
				parameters.Add ("@assetGroupId", assetGroupId);
				basicWhereClause.Append (" AND ag.asset_group_id = @assetGroupId ");
				basicWhereClause.Append (" AND ag.channel_id = @channelId ");
				basicWhereClause.Append (" AND ag.asset_group_id = aga.asset_group_id ");
				basicWhereClause.Append (" AND aga.asset_id = a.asset_id ");
			}

			//build sub queries

			// Public
			StringBuilder sb = new StringBuilder () ;
			sb.Append (" SELECT " ).Append (basicSelectList.ToString());
			sb.Append (" FROM ");
			sb.Append (basicFromClause.ToString());
			sb.Append (" WHERE ").Append (basicWhereClause.ToString());
			sb.Append (" AND a.permission= ").Append ((int) Constants.eASSET_PERMISSION.PUBLIC);

			// PRIVATE - Only need to add these if user is logged in
			if (userId > 0)
			{
				sb.Append (" UNION ");
				sb.Append (" SELECT " ).Append(basicSelectList.ToString());
				sb.Append (" FROM ");
				sb.Append (basicFromClause.ToString());
				sb.Append (" WHERE ").Append(basicWhereClause.ToString());
				sb.Append (" AND a.permission = ").Append((int) Constants.eASSET_PERMISSION.PRIVATE);
				sb.Append (" AND a.owner_id = @userId");
			}
			
            ////KANEVA MEMBER - Only need to add these if user is logged in
            //if (userId > 0)
            //{
            //    sb.Append (" UNION ");
            //    sb.Append (" SELECT " ).Append(basicSelectList.ToString());
            //    sb.Append (" FROM ");
            //    sb.Append (basicFromClause.ToString());
            //    sb.Append (" WHERE ").Append(basicWhereClause.ToString());
            //    sb.Append (" AND a.permission = ").Append((int) Constants.eASSET_PERMISSION.KANEVA_MEMBERS);
            //    sb.Append (" AND @userId > 0");
            //}

            ////FRIENDS 1 - Only need to add these if user is logged in
            //if (userId > 0)
            //{
            //    sb.Append (" UNION ");
            //    sb.Append (" SELECT " ).Append(basicSelectList.ToString());
            //    sb.Append (" FROM ");
            //    sb.Append (basicFromClause.ToString());
            //    sb.Append (" WHERE ").Append(basicWhereClause.ToString());
            //    sb.Append (" AND a.permission = ").Append((int) Constants.eASSET_PERMISSION.FRIENDS);
            //    sb.Append (" AND (a.permission_group IS NULL or a.permission_group = 0) ");
            //    sb.Append (" AND  a.owner_id = @userId ");
            //}

            ////FRIENDS 2 - Only need to add these if user is logged in
            //if (userId > 0)
            //{
            //    sb.Append (" UNION ");
            //    sb.Append (" SELECT " ).Append(basicSelectList.ToString());
            //    sb.Append (" FROM ");
            //    sb.Append (basicFromClause.ToString());
            //    sb.Append (" ,friends f");
            //    sb.Append (" WHERE ").Append(basicWhereClause.ToString());
            //    sb.Append (" AND a.permission = ").Append((int) Constants.eASSET_PERMISSION.FRIENDS);
            //    sb.Append (" AND (a.permission_group IS NULL or a.permission_group = 0) ");
            //    sb.Append (" AND f.friend_id = a.owner_id AND f.user_id = @userId ");
            //}

            ////FRIENDS 3 - Only need to add these if user is logged in
            //if (userId > 0)
            //{
            //    sb.Append (" UNION ");
            //    sb.Append (" SELECT " ).Append(basicSelectList.ToString());
            //    sb.Append (" FROM ");
            //    sb.Append (basicFromClause.ToString());
            //    sb.Append (" ,friends f");
            //    sb.Append (" WHERE ").Append(basicWhereClause.ToString());
            //    sb.Append (" AND a.permission = ").Append((int) Constants.eASSET_PERMISSION.FRIENDS);
            //    sb.Append (" AND (a.permission_group IS NULL or a.permission_group = 0) ");
            //    sb.Append (" AND f.friend_id = @userId AND f.user_id = a.owner_id ");
            //}

            ////friend group part 1- Only need to add these if user is logged in
            //if (userId > 0)
            //{
            //    sb.Append (" UNION ");
            //    sb.Append (" SELECT " ).Append(basicSelectList.ToString());
            //    sb.Append (" FROM ");
            //    sb.Append (basicFromClause.ToString());
            //    sb.Append (" ,friend_group_friends fgf, ");
            //    sb.Append (" friend_groups fg");
            //    sb.Append (" WHERE ").Append(basicWhereClause.ToString());
            //    sb.Append (" AND a.permission = ").Append((int) Constants.eASSET_PERMISSION.FRIENDS);
            //    sb.Append (" AND a.owner_id = fg.owner_id ");
            //    sb.Append (" AND fg.friend_group_id = fgf.friend_group_id ");
            //    sb.Append (" AND fgf.friend_id = @userId ");
            //    sb.Append (" AND fgf.friend_group_id = a.permission_group ");
            //}

            ////friend group part 2- Only need to add these if user is logged in
            //if (userId > 0)
            //{
            //    sb.Append (" UNION ");
            //    sb.Append (" SELECT " ).Append(basicSelectList.ToString());
            //    sb.Append (" FROM ");
            //    sb.Append (basicFromClause.ToString());
            //    sb.Append (" WHERE ").Append(basicWhereClause.ToString());
            //    sb.Append (" AND a.permission = ").Append((int) Constants.eASSET_PERMISSION.FRIENDS);
            //    sb.Append (" AND a.owner_id = @userId ");
            //}

			return dbUtility.GetPagedDataTableUnion(sb.ToString(), orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// Get a list of Assets accessible by user for OMM
		/// 
		/// JB NOTE : I am splitting these up becuase OMM needs to join community table to get the owners thumbnail, and 
		/// I don't want to add this overhead to all GetAccessibleAssetsInChannel calls
		/// </summary>
		public static PagedDataTable GetAccessibleOMMAssetsInChannel (int channelId, int assetGroupId, bool bGetMature,
			int assetTypeId, int userId, string filter, string orderby, int pageNumber, int pageSize)
		{
			Hashtable parameters = new Hashtable ();
			parameters.Add ("@channelId", channelId);

			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			//common select and where clauses for every sub query
			StringBuilder basicSelectList = new StringBuilder() ;
			basicSelectList.Append (" DISTINCT a.asset_id, file_size, ");
			basicSelectList.Append (" a.content_extension, a.asset_type_id, a.asset_sub_type_id, a.name, ");
			basicSelectList.Append (" a.short_description, a.created_date, a.asset_rating_id, a.mature, a.amount, ");
			basicSelectList.Append (" a.status_id, ");
			basicSelectList.Append (" a.run_time_seconds, a.owner_id, ");
			basicSelectList.Append (" a.owner_id as user_id, a.owner_username, a.owner_username as username, ");
			basicSelectList.Append (" a.teaser, a.keywords, ");
			basicSelectList.Append (" a.permission, a.permission_group, ");
			basicSelectList.Append (" a.image_full_path, a.thumbnail_small_path, a.thumbnail_medium_path, a.thumbnail_large_path, a.thumbnail_xlarge_path, a.thumbnail_gen, ");
			basicSelectList.Append (" ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs, ass.number_of_channels, ");
			basicSelectList.Append (" com.thumbnail_small_path as owner_thumbnail_small_path ");

			// Build the basic FROM Clause
			StringBuilder basicFromClause = new StringBuilder () ;
			basicFromClause.Append (" asset_channels ac, ");

			if (bGetMature)
			{
				basicFromClause.Append (" vw_published_mature_assets  a ");
			}
			else
			{
				basicFromClause.Append (" vw_published_assets  a ");
			}

			basicFromClause.Append (" INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id ");
			basicFromClause.Append (" INNER JOIN communities_personal com ON a.owner_id = com.creator_id ");
			
			if (assetGroupId > 0)
			{
				basicFromClause.Append(" ,asset_groups ag, asset_group_assets aga ");
			}

			// Build the basic WHERE Clause
			StringBuilder basicWhereClause = new StringBuilder () ;
			basicWhereClause.Append (" ac.channel_id= @channelId ");
			basicWhereClause.Append (" AND ac.asset_id=a.asset_id ");

			// Specific asset type
			if (assetTypeId > 0)
			{
				parameters.Add ("@assetTypeId", assetTypeId);
				basicWhereClause.Append (" AND a.asset_type_id = @assetTypeId ");
			}

			if (filter.Length > 0)
			{
				basicWhereClause.Append (" AND ").Append (filter);
			}

			if( assetGroupId > 0)
			{
				parameters.Add ("@assetGroupId", assetGroupId);
				basicWhereClause.Append (" AND ag.asset_group_id = @assetGroupId ");
				basicWhereClause.Append (" AND ag.channel_id = @channelId ");
				basicWhereClause.Append (" AND ag.asset_group_id = aga.asset_group_id ");
				basicWhereClause.Append (" AND aga.asset_id = a.asset_id ");
			}

			//build sub queries

			// Public
			StringBuilder sb = new StringBuilder () ;
			sb.Append (" SELECT " ).Append (basicSelectList.ToString());
			sb.Append (" FROM ");
			sb.Append (basicFromClause.ToString());
			sb.Append (" WHERE ").Append (basicWhereClause.ToString());
			sb.Append (" AND a.permission= ").Append ((int) Constants.eASSET_PERMISSION.PUBLIC);

			// PRIVATE - Only need to add these if user is logged in
			if (userId > 0)
			{
				sb.Append (" UNION ");
				sb.Append (" SELECT " ).Append(basicSelectList.ToString());
				sb.Append (" FROM ");
				sb.Append (basicFromClause.ToString());
				sb.Append (" WHERE ").Append(basicWhereClause.ToString());
				sb.Append (" AND a.permission = ").Append((int) Constants.eASSET_PERMISSION.PRIVATE);
				sb.Append (" AND a.owner_id = @userId");
			}
			
            ////KANEVA MEMBER - Only need to add these if user is logged in
            //if (userId > 0)
            //{
            //    sb.Append (" UNION ");
            //    sb.Append (" SELECT " ).Append(basicSelectList.ToString());
            //    sb.Append (" FROM ");
            //    sb.Append (basicFromClause.ToString());
            //    sb.Append (" WHERE ").Append(basicWhereClause.ToString());
            //    sb.Append (" AND a.permission = ").Append((int) Constants.eASSET_PERMISSION.KANEVA_MEMBERS);
            //    sb.Append (" AND @userId > 0");
            //}

            ////FRIENDS 1 - Only need to add these if user is logged in
            //if (userId > 0)
            //{
            //    sb.Append (" UNION ");
            //    sb.Append (" SELECT " ).Append(basicSelectList.ToString());
            //    sb.Append (" FROM ");
            //    sb.Append (basicFromClause.ToString());
            //    sb.Append (" WHERE ").Append(basicWhereClause.ToString());
            //    sb.Append (" AND a.permission = ").Append((int) Constants.eASSET_PERMISSION.FRIENDS);
            //    sb.Append (" AND (a.permission_group IS NULL or a.permission_group = 0) ");
            //    sb.Append (" AND  a.owner_id = @userId ");
            //}

            ////FRIENDS 2 - Only need to add these if user is logged in
            //if (userId > 0)
            //{
            //    sb.Append (" UNION ");
            //    sb.Append (" SELECT " ).Append(basicSelectList.ToString());
            //    sb.Append (" FROM ");
            //    sb.Append (basicFromClause.ToString());
            //    sb.Append (" ,friends f");
            //    sb.Append (" WHERE ").Append(basicWhereClause.ToString());
            //    sb.Append (" AND a.permission = ").Append((int) Constants.eASSET_PERMISSION.FRIENDS);
            //    sb.Append (" AND (a.permission_group IS NULL or a.permission_group = 0) ");
            //    sb.Append (" AND f.friend_id = a.owner_id AND f.user_id = @userId ");
            //}

            ////FRIENDS 3 - Only need to add these if user is logged in
            //if (userId > 0)
            //{
            //    sb.Append (" UNION ");
            //    sb.Append (" SELECT " ).Append(basicSelectList.ToString());
            //    sb.Append (" FROM ");
            //    sb.Append (basicFromClause.ToString());
            //    sb.Append (" ,friends f");
            //    sb.Append (" WHERE ").Append(basicWhereClause.ToString());
            //    sb.Append (" AND a.permission = ").Append((int) Constants.eASSET_PERMISSION.FRIENDS);
            //    sb.Append (" AND (a.permission_group IS NULL or a.permission_group = 0) ");
            //    sb.Append (" AND f.friend_id = @userId AND f.user_id = a.owner_id ");
            //}

            ////friend group part 1- Only need to add these if user is logged in
            //if (userId > 0)
            //{
            //    sb.Append (" UNION ");
            //    sb.Append (" SELECT " ).Append(basicSelectList.ToString());
            //    sb.Append (" FROM ");
            //    sb.Append (basicFromClause.ToString());
            //    sb.Append (" ,friend_group_friends fgf, ");
            //    sb.Append (" friend_groups fg");
            //    sb.Append (" WHERE ").Append(basicWhereClause.ToString());
            //    sb.Append (" AND a.permission = ").Append((int) Constants.eASSET_PERMISSION.FRIENDS);
            //    sb.Append (" AND a.owner_id = fg.owner_id ");
            //    sb.Append (" AND fg.friend_group_id = fgf.friend_group_id ");
            //    sb.Append (" AND fgf.friend_id = @userId ");
            //    sb.Append (" AND fgf.friend_group_id = a.permission_group ");
            //}

            ////friend group part 2- Only need to add these if user is logged in
            //if (userId > 0)
            //{
            //    sb.Append (" UNION ");
            //    sb.Append (" SELECT " ).Append(basicSelectList.ToString());
            //    sb.Append (" FROM ");
            //    sb.Append (basicFromClause.ToString());
            //    sb.Append (" WHERE ").Append(basicWhereClause.ToString());
            //    sb.Append (" AND a.permission = ").Append((int) Constants.eASSET_PERMISSION.FRIENDS);
            //    sb.Append (" AND a.owner_id = @userId ");
            //}

			return dbUtility.GetPagedDataTableUnion(sb.ToString(), orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// Get a list of Assets in a channel owned by a user
		/// </summary>
		public static PagedDataTable GetSharedAssetsInChannel (bool bGetMature, int communityId, int userId,
			int assetTypeId, string filter, string orderby, int pageNumber, int pageSize)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string selectList = "a.file_size, " +
				" a.content_extension, a.asset_id, a.asset_type_id, a.name, a.short_description, a.created_date, " +
				" a.asset_rating_id, a.amount, a.publish_status_id,  a.status_id, " +
				" a.owner_id, a.owner_id as user_id, a.owner_username, a.owner_username as username, a.teaser, " +
				" a.keywords, a.permission, a.permission_group, " +
				" a.category1_id, a.category2_id, a.category3_id, a.thumbnail_assetdetails_path, a.thumbnail_medium_path, a.thumbnail_small_path, a.thumbnail_large_path, a.thumbnail_gen, " +
				" ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs, " +
				" IF(a.asset_rating_id = 3, true, IF(a.asset_rating_id = 6, true, IF(a.asset_rating_id = 9, true, false ))) AS mature_profile ";

			string tableList = "asset_channels ac, assets a " +
				" INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id ";

			string whereClause = " ac.asset_id = a.asset_id " +
				" AND ac.channel_id = @communityId " +
				" AND a.owner_id = @userId " +
				SQLCommon_GetPublishedAssetsSQL ();

			if (!bGetMature)
			{
				whereClause += SQLCommon_GetNonMatureAssets ();
			}

			// Specific asset type?
			if (assetTypeId > 0)
			{
				parameters.Add ("@assetTypeId", assetTypeId);
				whereClause += " AND a.asset_type_id = @assetTypeId ";
			}

			if (filter.Length > 0)
			{
				whereClause += " AND " + filter;
			}

			parameters.Add ("@communityId", communityId);
			parameters.Add ("@userId", userId);
			return dbUtility.GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}


		/// <summary>
		/// get assets to migrate thumbnails
		/// </summary>
		/// <returns></returns>
		public static DataTable GetAssetsGenThumbs (int startAssetId, int endAssetId, int assetTypeId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sb = "SELECT a.asset_id, a.owner_id, a.target_dir, a.content_extension, a.image_path, com.community_id " +
				" FROM assets a, communities_personal com " +
				" WHERE a.owner_id = com.creator_id " +
				" AND asset_id > @startAssetId " +
				" AND asset_id < @endAssetId " +
				" AND asset_type_id = @assetTypeId ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@startAssetId", startAssetId);
			parameters.Add ("@endAssetId", endAssetId);
			parameters.Add ("@assetTypeId", assetTypeId);
			return dbUtility.GetDataTable (sb, parameters);
		}

		/// <summary>
		/// get videos to migrate thumbnails
		/// </summary>
		/// <returns></returns>
		public static DataTable GetVideoGenThumbs (int startAssetId, int endAssetId, int assetTypeId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sb = "SELECT a.asset_id, a.owner_id, a.target_dir, a.content_extension, a.image_path, com.community_id " +
				" FROM assets a, communities_personal com " +
				" WHERE a.owner_id = com.creator_id " +
				" AND asset_id > @startAssetId " +
				" AND asset_id < @endAssetId " +
				" AND asset_type_id = @assetTypeId " +
				" AND a.thumbnail_gen = 0 ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@startAssetId", startAssetId);
			parameters.Add ("@endAssetId", endAssetId);
			parameters.Add ("@assetTypeId", assetTypeId);
			return dbUtility.GetDataTable (sb, parameters);
		}

		/// <summary>
		/// get phtots to migrate thumbnails
		/// </summary>
		/// <returns></returns>
		public static DataTable GetAssetsPhotoGenThumbsFuncky ()
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sb = "SELECT a.asset_id, a.owner_id, a.target_dir, a.content_extension, com.community_id " +
				" FROM assets a, communities_personal com " +
				" WHERE asset_type_id = " + (int) Constants.eASSET_TYPE.PICTURE +
				" AND a.owner_id = com.creator_id " +
				" AND (content_extension like '%\\%5%' " +
				" OR content_extension like '%\\%2%' " +
				" OR content_extension like '%\\'%'" +
				" OR content_extension like '%#%')";

			Hashtable parameters = new Hashtable ();
			return dbUtility.GetDataTable (sb, parameters);
		}

		public static string CleanImageFilename (string filename)
		{
			filename = filename.Replace ("%2","22");
			filename = filename.Replace ("#", "N");
			filename = filename.Replace ("%5", "55");
			filename = filename.Replace ("'", "Q");
			filename = filename.Replace ("%", "P");
			//filename = filename.Replace ("?", "C");

			// "[\\d_-a-zA-Z0-9\(\)]" - Numbers, underscore, dash, numbers, parens

			// Fix really bad files
			filename = System.Text.RegularExpressions.Regex.Replace (filename, "[^\\(\\)\\[\\]-_.\\da-zA-Z0-9]", "U");
			return filename;
		}


		/// <summary>
		/// get assets to migrate thumbnails
		/// </summary>
		/// <returns></returns>
		public static DataTable GetAssetsGenThumbsNotPhoto ()
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sb = "SELECT a.asset_id, a.owner_id, a.target_dir, a.content_extension, com.community_id " +
				" FROM assets a, communities_personal com " +
				" WHERE asset_type_id <> " + (int) Constants.eASSET_TYPE.PICTURE +
				" AND a.owner_id = com.creator_id ";

			Hashtable parameters = new Hashtable ();
			return dbUtility.GetDataTable (sb, parameters);
		}

		/// <summary>
		/// Update an asset thumb
		/// </summary>
		public static int UpdateAssetThumb (int assetId, string thumbnailPath, string dbColumn)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "UPDATE assets " +
				" SET " +
				dbColumn + " = @thumbnailPath " +
				" WHERE asset_id = @assetId";

			Hashtable parameters = new Hashtable ();

			if (thumbnailPath.Trim ().Length.Equals (0))
			{
				parameters.Add ("@thumbnailPath", DBNull.Value);
			}
			else
			{
				parameters.Add ("@thumbnailPath", thumbnailPath);
			}

			parameters.Add ("@assetId", assetId);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
			return 0;
		}

		/// <summary>
		/// Update an asset thumb
		/// </summary>
		public static int MarkAssetThumbAsGenerated (int assetId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "UPDATE assets " +
				" SET " +
				" thumbnail_gen = 1 " +
				" WHERE asset_id = @assetId";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
			return 0;
		}

        #region WOK Item Categories Administration

        /// <summary>
        /// get all the currently available categories )
        /// all columns
        /// </summary>
        public static DataTable GetAllItemCategories()
        {
            return GetItemCategories(-1, null);
        }

        /// <summary>
        /// get all the currently available in world items (1 parameters)
        /// all columns
        /// </summary>
        /// <param name="filter"></param>
        public static DataTable GetItemCategoriesByFilter(string filter)
        {
            return GetItemCategories(-1, filter);
        }

        /// <summary>
        /// get all the currently available categories )
        /// all columns
        /// </summary>
        public static DataTable GetItemCategoryById(int catId)
        {
            return GetItemCategories(catId, null);
        }

        public static DataTable GetItemCategories(int itemID, string filter)
        {
            string sql = "SELECT category_id, category, parent_category_id, marketing_path " +
                " FROM item_categories ic ";

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            Hashtable theParams = new Hashtable();

            //limit by item id if one is provided
            if (itemID > 0)
            {
                sql += " WHERE ic.category_id = @itemId ";
                theParams.Add("@itemId", itemID);
            }

            //append the where clause if a filter is provided
            if ((filter != null) && (filter != ""))
            {
                if (itemID > 0)
                {
                    sql += " AND " + filter;
                }
                else
                {
                    sql += " WHERE " + filter;
                }
            }

            sql += " ORDER BY category ASC";


            return dbUtility.GetDataTable(sql, theParams);

        }

        public static DataTable GetItemCategoriesByParentId(int parentID)
        {
            string sql = "SELECT category_id, category, parent_category_id, marketing_path " +
                " FROM item_categories ic WHERE parent_category_id = @parentCatId ORDER BY category ASC ";

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            Hashtable parameters = new Hashtable();
            parameters.Add("@parentCatId", parentID);

            return dbUtility.GetDataTable(sql, parameters);

        }

        /// <summary>
        /// InsertWOKCategory - inserts a new category
        /// </summary>
        /// <returns></returns>
        public static int InsertWOKCategory(string category, int parentCategoryId, string marketingPath, int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            // Record User Asset Subscriptions
            string sqlInsert = "INSERT INTO item_categories " +
                "(category, parent_category_id, marketing_path, modifiers_id) VALUES (@category, @parentCategoryId, @marketingPath, @modifiers_id)";

            int insertItemId = 0;

            Hashtable parameters = new Hashtable();
            parameters.Add("@category", category);
            parameters.Add("@parentCategoryId", parentCategoryId);
            parameters.Add("@marketingPath", marketingPath);
            parameters.Add("@modifiers_id", userId);

            int rowsAffected = dbUtility.ExecuteIdentityInsert(sqlInsert, parameters, ref insertItemId);

            return insertItemId;
        }

        /// <summary>
        /// UpdateWOKCategory - updates WOK categories 
        /// </summary>
        /// <param name="featuredAssetId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static int UpdateWOKCategory(int categoryId, string category, int parentCategoryId, string marketingPath, int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlUpdate = "UPDATE item_categories ic SET " +
                "category = @category, " +
                "parent_category_id = @parentCategoryId, " +
                "marketing_path = @marketingPath, " +
                "modifiers_id = @modifiers_id " +
                " WHERE category_id = @categoryId ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@category", category);
            parameters.Add("@parentCategoryId", parentCategoryId);
            parameters.Add("@marketingPath", marketingPath);
            parameters.Add("@modifiers_id", userId);
            parameters.Add("@categoryId", categoryId);
            return dbUtility.ExecuteNonQuery(sqlUpdate, parameters);
        }

        public static void RecursiveDeleteParentWOKCategory(int categoryId, int parentCatId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            //find all children of this category
            DataTable childCategories = GetItemCategoriesByParentId(categoryId);

            //process the removal of this category
            DeleteWOKCategory(categoryId, parentCatId);

            //recursively process child categories now parent categories
            foreach (DataRow row in childCategories.Rows)
            {
                RecursiveDeleteParentWOKCategory(Convert.ToInt32(row["category_id"]), 0);
            }
        }

        public static int DeleteWOKCategory(int categoryId, int parentCatId)
        {
            //find all occurances of this category
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            //update all categories having this deleted one as their parent to have the delete items parent as their new parent
            string sqlUpdate = "UPDATE item_categories ic SET " +
                "parent_category_id = @parentCategoryId " +
                " WHERE parent_category_id = @categoryId ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@parentCategoryId", parentCatId);
            parameters.Add("@categoryId", categoryId);
            dbUtility.ExecuteNonQuery(sqlUpdate, parameters);

            //move all items that might be under that category to the parent category
            //if category being removed is a toplevel remove all the items under it.
            if (parentCatId == 0)
            {
                DeleteWOKCategoryItems(categoryId);
            }
            else
            {
                //pull back all items and attempt update individually to avoid group failure due to duplicate keys
                DataTable categoryItems = GetWOKCategoryItemsByCategoryId(categoryId);
                foreach (DataRow row in categoryItems.Rows)
                {
                    MoveWOKCategoryItem(categoryId, parentCatId, Convert.ToInt32(row["item_id"]));
                }

                //originally tried a group update but al updates fail if one update
                //creates aduplicate key
                //MoveWOKCategoryItems(categoryId, parentCatId);
            }

            //once complete delete from item_categories table
            string sqlString = "DELETE FROM item_categories WHERE category_id = @categoryId ";

            parameters = new Hashtable();
            parameters.Add("@categoryId", categoryId);

            return KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sqlString, parameters);
        }

        public static DataTable GetAllWOKCategoryItems()
        {
            return GetWOKCategoryItems(-1, -1);
        }

        public static DataTable GetWOKCategoryItemsByItemId(int itemId)
        {
            return GetWOKCategoryItems(itemId, -1);
        }

        public static DataTable GetWOKCategoryItemsByCategoryId(int categoryId)
        {
            return GetWOKCategoryItems(-1, categoryId);
        }

        public static DataTable GetWOKCategoryItems(int itemID, int categoryId)
        {
            string sql = "SELECT item_id, category_id FROM items_to_categories itc ";

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            Hashtable theParams = new Hashtable();

            //limit by item id if one is provided
            if (itemID > 0)
            {
                sql += " WHERE itc.item_id = @itemId ";
                theParams.Add("@itemId", itemID);
            }

            //limit by category id if one is provided
            if ((categoryId > 0) && (itemID <= 0))
            {
                sql += " WHERE itc.category_id = @categoryId ";
                theParams.Add("@categoryId", categoryId);
            }
            else if (((categoryId > 0) && (itemID > 0)))
            {
                sql += " AND itc.category_id = @categoryId ";
                theParams.Add("@categoryId", categoryId);
            }

            sql += " ORDER BY category_id, item_id ASC";


            return dbUtility.GetDataTable(sql, theParams);
        }
        /// <summary>
        /// InsertWOKCategoryItem - inserts a new category to item relationship
        /// </summary>
        /// <returns></returns>
        public static int InsertWOKCategoryItem(int itemId, int categoryId, int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlInsert = "INSERT INTO items_to_categories " +
                "(item_id, category_id, modifiers_id) VALUES (@itemId, @categoryId, @modifiers_id)";

            int insertItemId = 0;

            Hashtable parameters = new Hashtable();
            parameters.Add("@itemId", itemId);
            parameters.Add("@categoryId", categoryId);
            parameters.Add("@modifiers_id", userId);

            return dbUtility.ExecuteIdentityInsert(sqlInsert, parameters, ref insertItemId);
        }

        /// <summary>
        /// MoveWOKCategoryItems - moves WOK Category Items from one category to another 
        /// </summary>
        /// <param name="oldCategoryId"></param>
        /// <param name="newCategoryId"></param>
        /// <returns></returns>
        public static int MoveWOKCategoryItems(int oldCategoryId, int newCategoryId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlUpdate = "UPDATE items_to_categories itc SET " +
                "category_id = @newCategoryId " +
                "WHERE category_id = @oldCategoryId ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@oldCategoryId", oldCategoryId);
            parameters.Add("@newCategoryId", newCategoryId);
            return dbUtility.ExecuteNonQuery(sqlUpdate, parameters);
        }

        /// <summary>
        /// MoveWOKCategoryItems - moves WOK Category Items from one category to another 
        /// </summary>
        /// <param name="oldCategoryId"></param>
        /// <param name="newCategoryId"></param>
        /// <returns></returns>
        public static int MoveWOKCategoryItem(int oldCategoryId, int newCategoryId, int itemId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlUpdate = "UPDATE items_to_categories itc SET " +
                "category_id = @newCategoryId " +
                "WHERE category_id = @oldCategoryId AND item_id = @itemId ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@oldCategoryId", oldCategoryId);
            parameters.Add("@newCategoryId", newCategoryId);
            parameters.Add("@itemId", itemId);
            return dbUtility.ExecuteNonQuery(sqlUpdate, parameters);
        }

        /// <summary>
        /// DeleteWOKCategoryItems - deletes all items from the items_to_categories
        /// table that are under a certain category
        /// </summary>
        public static int DeleteWOKCategoryItems(int categoryId)
        {
            //delete from items_to_categories table
            string sqlString = "DELETE FROM items_to_categories WHERE category_id = @categoryId ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@categoryId", categoryId);

            return KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sqlString, parameters);
        }

        /// <summary>
        /// DeleteWOKCategoryItem - deletes specific item from the items_to_categories
        /// table 
        /// </summary>
        public static int DeleteWOKCategoryItem(int itemId, int categoryId)
        {
            //delete from items_to_categories table
            string sqlString = "DELETE FROM items_to_categories WHERE item_id = @itemId AND category_id = @categoryId ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@itemId", itemId);
            parameters.Add("@categoryId", categoryId);

            return KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sqlString, parameters);
        }

        #endregion 

        #region WOK Items Catalog Functions


        /// <summary>
        /// get all the currently available in world items by a filter(3 parameters)
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        public static PagedDataTable GetRelatedWOKItems(int itemID, int pageNumber, int pageSize)
        {
            int categoryId = -1;
            PagedDataTable wokItems = new PagedDataTable();

            //retreive the category id of the current item
            string sql = "SELECT category_id FROM items_to_categories WHERE item_id = @itemID";

            Hashtable parameters = new Hashtable();
            parameters.Add("@itemID", itemID);

            DataRow drCategoryId = KanevaGlobals.GetDatabaseUtility().GetDataRow(sql, parameters, false);

            try
            {
                if (drCategoryId != null)
                {
                    categoryId = Convert.ToInt32(drCategoryId["category_id"]);
                    wokItems = GetWOKCatalogItems(itemID, null, categoryId, pageNumber, pageSize);
                }
            }
            catch (Exception)
            {
            }

            return wokItems;
        }

        /// <summary>
        /// get all the currently available in world items by a filter(3 parameters)
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        public static PagedDataTable GetWOKCatalogItemsById(int itemID, int pageNumber, int pageSize)
        {
            return GetWOKCatalogItems(itemID, null, -1, pageNumber, pageSize);
        }

        /// <summary>
        /// get all the currently available in world items by a filter(3 parameters)
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        public static PagedDataTable GetWOKCatalogItemsByFilter(string filter, int pageNumber, int pageSize)
        {
            return GetWOKCatalogItems(-1, filter, -1, pageNumber, pageSize);
        }

        /// <summary>
        /// get the currently available in world items (4 parameters)
        /// </summary>
        /// <param name="itemID"></param>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        public static PagedDataTable GetWOKCatalogItems(int itemID, string filter, int categoryId, int pageNumber, int pageSize)
        {
            string sqlSelectList = "";
            string sqlTableList = "";
            string sqlWhereClause = "";

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            Hashtable theParams = new Hashtable();

            //attach primary key to columns and attach items table to columns to form SQL string
            sqlSelectList = "SELECT wci.global_id, wci.name, wci.market_cost, wci.selling_price, " +
            "wci.display_name, wci.access_type, wci.product_availability, wci.product_exclusivity, " +
            "wci.currency_type, wci.access_type_id, wci.product_availability_id, wci.product_exclusivity_id, " +
            "wci.currency_type_id, wci.product_amount, wci.date_added, wci.item_creator_id, " +
            "wci.item_active, wci.description, wci.item_image_path, c.thumbnail_medium_path, " +
            "IF(wci.item_creator_id=0,'Kaneva',u.username) as ownername, " +
            "IF(wci.item_creator_id=0,'_wok_',c.name_no_spaces) as ownername_nospaces, " + 
            "wcis.views_count, wcis.raves_count, wcis.comments_count ";

            sqlTableList = "wok_catalog_items wci LEFT JOIN users u ON wci.item_creator_id = u.user_id " +
                "LEFT JOIN communities c ON u.user_id = c.creator_id  AND c.is_personal = 1 " +
                " LEFT JOIN wok_catalog_items_stats wcis ON wci.global_id = wcis.item_id ";

            //limit by item id if one is provided
            if (itemID > 0)
            {
                sqlWhereClause = "wci.global_id = @itemId";
                theParams.Add("@itemId", itemID);
            }

            //limit by item id if one is provided
            if (categoryId > 0)
            {
                sqlTableList += "LEFT JOIN items_to_categories itc ON wci.global_id = itc.item_id ";
                if (sqlWhereClause != "")
                {
                    sqlWhereClause += " AND itc.category_id = @categoryId";
                }
                else
                {
                    sqlWhereClause = "itc.category_id = @categoryId";
                }
                
                theParams.Add("@categoryId", categoryId);
            }

            //append the where clause if a filter is provided
            if ((filter != null) && (filter != ""))
            {
                if (sqlWhereClause != "")
                {
                    sqlWhereClause += " AND " + filter;
                }
                else
                {
                    sqlWhereClause += filter;
                }
            }

            string orderBy = "wci.display_name ASC";


            sqlSelectList += " FROM " + sqlTableList + (sqlWhereClause.Length > 0 ? " WHERE " + sqlWhereClause : "");
            return dbUtility.GetPagedDataTableUnion(sqlSelectList, orderBy, theParams, pageNumber, pageSize);

        }

        /// <summary>
        /// Get the WOK item for the given assetID
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static DataRow GetWOKItem(int itemID)
        {
            Hashtable parameters = new Hashtable();

            string sqlSelect = "SELECT wci.global_id, wci.name, wci.market_cost, wci.selling_price, " +
            "wci.display_name, wci.access_type, wci.product_availability, wci.product_exclusivity, " +
            "wci.currency_type, wci.access_type_id, wci.product_availability_id, wci.product_exclusivity_id, " +
            "wci.currency_type_id, wci.product_amount, wci.date_added, wci.item_creator_id, " +
            "wci.item_active, wci.description, wci.item_image_path, c.thumbnail_medium_path, " +
            "IF(wci.item_creator_id=0,'Kaneva',u.username) as ownername, " +
            "IF(wci.item_creator_id=0,'_wok_',c.name_no_spaces) as ownername_nospaces, " + 
            "wcis.views_count, wcis.raves_count, wcis.comments_count " + 
            "FROM wok_catalog_items wci LEFT JOIN users u ON wci.item_creator_id = u.user_id " +
            "LEFT JOIN communities c ON u.user_id = c.creator_id  AND c.is_personal = 1 " +
            "LEFT JOIN wok_catalog_items_stats wcis ON wci.global_id = wcis.item_id " + 
            "WHERE wci.global_id = @itemId";

            parameters.Add("@itemId", itemID);
            return KanevaGlobals.GetDatabaseUtility().GetDataRow(sqlSelect, parameters, true);
        }

        /// <summary>
        /// Get WOK Details Link
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static string GetWOKDetailsLink(int itemId, Page page)
        {
            return page.ResolveUrl("~/asset/" + itemId.ToString() + ".vworld");
        }

        /// <summary>
        /// GetPhotoImageURL
        /// </summary>
        /// <param name="imagePath">The relative path to the image on the NAS. Example - 4/22/sports.gif</param>
        /// <param name="defaultSize">The default size of the image if the user has not uploaded a photo. Example - 'sm', 'me', 'la', 'xl'</param>
        /// <returns></returns>
        public static string GetWOKItemImageURL(string imagePath, string defaultSize)
        {
            if (imagePath.Length.Equals(0))
            {
                return KanevaGlobals.WOKImageServer + "/KanevaIcon01_" + defaultSize + ".gif";
            }
            else
            {
                return KanevaGlobals.WOKImageServer + "/" + imagePath;
            }
        }

        /// <summary>
        /// Get a wok item rave for a user
        /// </summary>
        public static DataRow GetWOKDig(int userId, int itemId)
        {
            string sqlSelect = "select wd.item_digg_id " +
                " FROM wok_catalog_item_diggs wd " +
                " WHERE user_id = @userId " +
                " AND item_id = @itemId ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);
            parameters.Add("@itemId", itemId);
            return KanevaGlobals.GetDatabaseUtility().GetDataRow(sqlSelect, parameters, false);
        }

        /// <summary>
        /// get stores that the item is found in
        /// </summary>
        /// <returns>DataTable</returns>
        public static DataTable GetWOKItemStores(int itemId)
        {
            Hashtable theParams = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sql = "SELECT wis.store_id, wis.name, wis.channel_id FROM wok_catalog_item_stores wis " +
                "WHERE wis.global_id = @itemId ";

            theParams.Add("@itemId", itemId);

            return dbUtility.GetDataTable(sql, theParams);
        }

        /// <summary>
        /// get sets that the item is in
        /// </summary>
        /// <returns>DataTable</returns>
        public static DataTable GetWOKItemsSets(int itemId)
        {
            Hashtable theParams = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sql = "SELECT wis.item_set_id, wis.set_name FROM wok_catalog_item_sets wis WHERE wis.item_id = @itemId ";

            theParams.Add("@itemId", itemId);

            return dbUtility.GetDataTable(sql, theParams);
        }

        /// <summary>
        /// UpdateMediaViews
        /// </summary>
        public static int UpdateWOKItemViews(int itemId, string userIpAddress, int userId)
        {
            // Update download counts for the item's views
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            string sqlUpdate = "UPDATE wok_catalog_items_stats " +
                " SET views_count = views_count + 1 " +
                " WHERE item_id = @itemId";
            Hashtable parameters = new Hashtable();
            parameters.Add("@itemId", itemId);
            dbUtility.ExecuteNonQuery(sqlUpdate, parameters);

            string sqlInsert = "INSERT INTO wok_catalog_item_views " +
            " (user_id, item_id, date_viewed, ip_address) " +
            " VALUES " +
            " (@userId, @itemId, " + dbUtility.GetCurrentDateFunction() + ", INET_ATON(@ipAddress))";

            // Get the next item views Id from shard db
            //the sharding table and function already created but can't be used until
            //StoreUtility is ported to DataLayer
            //int itemViewsId = int.Parse(Db.Shard.GetScalar("SELECT shard_info.get_wok_item_views_id()", 2).ToString());

            // Insert the item_views record
            //string sqlInsert = "INSERT INTO wok_catalog_item_views " +
            //    " (item_views_id, user_id, item_id, date_viewed, ip_address) " +
            //    " VALUES " +
            //    " (@itemViewsId, @userId, @itemId, " + dbUtility.GetCurrentDateFunction() + ", INET_ATON(@ipAddress))";

            parameters = new Hashtable();
            //parameters.Add("@itemViewsId", itemViewsId);
            parameters.Add("@userId", userId);
            parameters.Add("@itemId", itemId);
            parameters.Add("@ipAddress", userIpAddress);
            dbUtility.ExecuteNonQuery(sqlInsert, parameters);

            return 0;
        }

        /// <summary>
        /// Insert a digg
        /// </summary>
        public static int InsertWOKDigg(int userId, int itemID, string ipAddress)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            // Must be logged in to digg an item
            if (userId < 1)
            {
                return 1;
            }

            //added "hammer to let admins rave as mnay times as they like
            if (!UsersUtility.IsUserAdministrator())
            {
                // Make sure they have not already digged
                DataRow drDigg = GetWOKDig(userId, itemID);

                // If they are already a friend, don't do anything
                if (drDigg != null)
                {
                    // Already dugg
                    return 1;
                }
            }

            string sql = "INSERT INTO wok_catalog_item_diggs " +
                "(user_id, item_id, date_dug, ip_address " +
                ") VALUES (" +
                "@userId, @itemID," + dbUtility.GetCurrentDateFunction() + ", INET_ATON(@ipAddress))";

            Hashtable parameters = new Hashtable();
            parameters.Add("@userId", userId);
            parameters.Add("@itemID", itemID);
            parameters.Add("@ipAddress", ipAddress);
            dbUtility.ExecuteNonQuery(sql, parameters);

            string sqlUpdate = "UPDATE wok_catalog_items_stats " +
                " SET raves_count = raves_count + 1 " +
                " WHERE item_id = @itemId";
            parameters = new Hashtable();
            parameters.Add("@itemId", itemID);
            dbUtility.ExecuteNonQuery(sqlUpdate, parameters);

            return 0;
        }


        #endregion


        #region Contest Administration

        /// <summary>
		/// Gets All Contests
		/// </summary>
		/// <returns>DataTable</returns>
		public static DataTable GetAllContests()
		{
			//retreives a list of all the current contests
			string selectList = "SELECT contest_id, name, terms, upload_start_date, upload_end_date, vote_start_date, vote_end_date, channel_id, contest_picture, vote_yes_image, vote_no_image, " +  
				" (upload_start_date < NOW()) AS upload_started, (upload_end_date < NOW()) AS upload_ended, " +
				" (vote_start_date < NOW()) AS vote_started, (vote_end_date < NOW()) AS vote_ended " +
				" FROM contests "; 

			string sqlSelect = selectList;

			DataTable dtResult = KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect);

			return dtResult;
		}

		/// <summary>
		/// Gets selected Contest
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns>DataRow</returns>
		public static DataTable GetContestsByChannelId (int channelId)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string selectList = "SELECT contest_id, c.name, terms, upload_start_date, upload_end_date, vote_start_date, vote_end_date, channel_id, contest_picture, vote_yes_image, vote_no_image, " +  
				" (upload_start_date < NOW()) AS upload_started, (upload_end_date < NOW()) AS upload_ended, " +
				" (vote_start_date < NOW()) AS vote_started, (vote_end_date < NOW()) AS vote_ended, com.template_id " +
				" FROM contests c INNER JOIN communities com ON c.channel_id = com.community_id ";

			string whereClause = " WHERE channel_id = @channelId ";

			string sqlSelect = selectList + whereClause;

			parameters.Add ("@channelId", channelId);
			return dbUtility.GetDataTable (sqlSelect, parameters);
		}

		/// <summary>
		/// gets the most recent contest id for a given channel
		/// </summary>
		/// <returns></returns>
		public static int GetContestId (int channelId)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlSelect = "SELECT c.contest_id " +
				" FROM contests c " +
				" WHERE c.channel_id = @channelId " +
				" ORDER BY c.contest_id Desc";

			parameters.Add ("@channelId", channelId);
			DataRow result = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters,false);
			if (result == null)
			{
				return -2;
			}

			return Convert.ToInt32(result[0]);
		}
 
		public static DataRow GetContestByContestId(int contestId)
		{
			//retreives a list of all the current contests
			string selectList = "SELECT contest_id, c.name, terms, upload_start_date, upload_end_date, vote_start_date, vote_end_date, channel_id, contest_picture, vote_yes_image, vote_no_image, " +  
				" (upload_start_date < NOW()) AS upload_started, (upload_end_date < NOW()) AS upload_ended, " +
				" (vote_start_date < NOW()) AS vote_started, (vote_end_date < NOW()) AS vote_ended, com.template_id " +
				" FROM contests c INNER JOIN communities com ON c.channel_id = com.community_id ";

			string whereClause = " WHERE contest_id = @contestId ";
			
			string sqlSelect = selectList + whereClause;

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@contestId", contestId);

			DataRow drResult = KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, false);

			return drResult;
		}

		/// <summary>
		/// insert a new contest into the contests table
		/// </summary>
		/// <returns></returns>
		public static int InsertNewContest(string contestName, string terms, DateTime uploadStartDate, DateTime uploadEndDate,
			DateTime voteStartDate, DateTime voteEndDate, int channelId, string contestPictPath, string yesButtonPath, string noButtonPath)
		{

			string query = " INSERT INTO contests " + 
				" (name, terms, upload_start_date, upload_end_date, vote_start_date, vote_end_date, channel_id, contest_picture, vote_yes_image, vote_no_image) " +
				" VALUES(@contestName, @terms, @uploadStartDate, @uploadEndDate, @voteStartDate, @voteEndDate, @channelId, @contestPictPath, @yesButtonPath, @noButtonPath) " ;

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@contestName", contestName);
			parameters.Add ("@terms", terms);
			parameters.Add ("@uploadStartDate", uploadStartDate);
			parameters.Add ("@uploadEndDate", uploadEndDate);
			parameters.Add ("@voteStartDate", voteStartDate);
			parameters.Add ("@voteEndDate", voteEndDate);
			parameters.Add ("@channelId", channelId);
			parameters.Add ("@contestPictPath", contestPictPath);
			parameters.Add ("@yesButtonPath", yesButtonPath);
			parameters.Add ("@noButtonPath", noButtonPath);

			int retVal = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert(query, parameters, ref retVal);

			return retVal;
		}

		/// <summary>
		/// save changes to the contest int the contests table
		/// </summary>
		/// <param name="contestId"></param>
		/// <returns></returns>
		/// 
		public static void SaveContestChanges(int contestId, string contestName, string terms, DateTime uploadStartDate, DateTime uploadEndDate,
			DateTime voteStartDate, DateTime voteEndDate, int channelId, string contestPictPath, string yesButtonPath, string noButtonPath)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" UPDATE contests ");
			sb.Append(" SET name = @contestName, ");
			sb.Append(" terms = @terms, ");
			sb.Append(" upload_start_date = @uploadStartDate, ");
			sb.Append(" upload_end_date = @uploadEndDate, ");
			sb.Append(" vote_start_date = @voteStartDate, ");
			sb.Append(" vote_end_date = @voteEndDate, ");
			sb.Append(" channel_id = @channelId, ");
			sb.Append(" contest_picture = @contestPictPath, ");
			sb.Append(" vote_yes_image = @yesButtonPath, ");
			sb.Append(" vote_no_image = @noButtonPath ");
			sb.Append(" WHERE contest_id = @contestId ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@contestName", contestName);
			parameters.Add ("@terms", terms);
			parameters.Add ("@uploadStartDate", uploadStartDate);
			parameters.Add ("@uploadEndDate", uploadEndDate);
			parameters.Add ("@voteStartDate", voteStartDate);
			parameters.Add ("@voteEndDate", voteEndDate);
			parameters.Add ("@channelId", channelId);
			parameters.Add ("@contestPictPath", contestPictPath);
			parameters.Add ("@yesButtonPath", yesButtonPath);
			parameters.Add ("@noButtonPath", noButtonPath);
			parameters.Add ("@contestId", contestId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery(sb.ToString(), parameters);
		}

		/// <summary>
		/// delete an existing contest
		/// </summary>
		/// <param name="id">the id of the contest</param>
		/// <returns></returns>
		public static void DeleteCurrentContest(int contestId)
		{
			//delete from contest table
			string sqlString = "DELETE FROM contests WHERE contest_id = @contestId ";

			Hashtable parameters = new Hashtable ();			
			parameters.Add ("@contestId", contestId);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		#endregion


		/// <summary>
		/// GetAssetForLightBox
		/// </summary>
		public static DataRow GetAssetForLightBox (int assetId)
		{
			string sqlSelect = "SELECT a.asset_id, a.asset_type_id, a.name, a.short_description, a.created_date, " +
				" a.asset_rating_id, a.amount, a.publish_status_id,  a.status_id,  " +
				" a.owner_id, a.owner_id as user_id, a.owner_username, a.owner_username as username, " +
				" ass.number_of_diggs " +
				" FROM assets a INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id " +
				" WHERE a.asset_id = @assetId " +
				" AND a.permission = " + (int) Constants.eASSET_PERMISSION.PUBLIC +
				SQLCommon_GetPublishedAssetsSQL ();

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			DataTable dtResult = KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);

			if (dtResult.Rows.Count > 0)
			{
				return dtResult.Rows [0];
			}

			// Existing row was not found, get a new one
			return null;
		}

		/// <summary>
		/// GetAssetInContest
		/// </summary>
		public static DataRow GetAssetInContest (int contestId, int assetId)
		{
			string sqlSelect = "SELECT ca.contest_id, a.asset_id, a.asset_type_id, a.name, a.short_description, a.created_date, " +
				" a.asset_rating_id, a.amount, a.publish_status_id,  a.status_id, " +
				" a.owner_id, a.owner_id as user_id, a.owner_username, a.owner_username as username, " +
				" ca.number_of_votes, " +
				" aav.attribute_value as artist_name " +
				" FROM contest_assets ca, " +
				" assets a LEFT OUTER JOIN asset_attribute_values aav ON a.asset_id = aav.asset_id and aav.attribute_id = 33 " +
				" WHERE ca.contest_id = @contestId " +
				" AND a.asset_id = @assetId " +
				" AND ca.asset_id = a.asset_id " +
				" AND a.permission = " + (int) Constants.eASSET_PERMISSION.PUBLIC +
				SQLCommon_GetPublishedAssetsSQL ();

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@contestId", contestId);
			parameters.Add ("@assetId", assetId);
			DataTable dtResult = KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);

			if (dtResult.Rows.Count > 0)
			{
				return dtResult.Rows [0];
			}

			// Existing row was not found, get a new one
			return null;
		}

		/// <summary>
		/// GetTopArtistInContest
		/// </summary>
		/// <param name="contestId"></param>
		/// <param name="resultCount"></param>
		/// <returns>DataTable</returns>
		/// <remarks>one of two methods to determine top performers; this one returns top performing
		/// artist based on media votes - picks the artists' top performing media if more than on
		/// has been submitted</remarks>
		public static DataTable GetTopArtistInContest (int contestId, int resultCount, bool bOnlyPublished, bool bGetMature, int channelId)
		{

			//creates first result set as temp table via inner select
			string selectList = "SELECT " + 
				"a.asset_id, a.name, ca.number_of_votes, a.created_date, a.asset_rating_id, " + 
				"a.publish_status_id, a.status_id, a.owner_id, a.owner_username, a.asset_type_id, " + 
				" a.thumbnail_small_path, a.thumbnail_medium_path, a.thumbnail_large_path, a.thumbnail_xlarge_path, a.thumbnail_gen, " +
				"ca.contest_id, aav.attribute_value as artist_name " + 
				"FROM contest_assets ca LEFT OUTER JOIN assets a ON ca.asset_id = a.asset_id " +
				"LEFT OUTER JOIN asset_attribute_values aav ON a.asset_id = aav.asset_id AND aav.attribute_id = 33 " +
				"LEFT OUTER JOIN asset_channels ac ON ac.asset_id = a.asset_id ";

			string whereClause = " WHERE ca.contest_id = @contestId " + 
				" AND ac.channel_id = @channelId " + 
				" AND a.permission = " + (int) Constants.eASSET_PERMISSION.PUBLIC +
				SQLCommon_GetPublishedAssetsSQL ();

			if (!bGetMature)
			{
				whereClause += SQLCommon_GetNonMatureAssets ();
			}

			string innerOrderByClause = " ORDER BY number_of_votes desc, a.created_date asc " ;
			string innerSelect = selectList + whereClause + innerOrderByClause;

			//create final result set with a select from the "temp table" 
			string outerSelect = "SELECT * from ( " + innerSelect + " ) tempTable ";
			string groupByClause = " GROUP BY artist_name ";
			string outerOrderByClause = " ORDER BY number_of_votes desc " ;
			string limitClause = " LIMIT 0, @resultCount ";

			string sqlSelect = outerSelect + groupByClause + outerOrderByClause + limitClause;

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@contestId", contestId);
			parameters.Add ("@resultCount", resultCount);
			parameters.Add ("@channelId", channelId);
			DataTable dtResult = KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);

			return dtResult;
		}

		/// <summary>
		/// GetTopAssetInContest
		/// </summary>
		/// <param name="contestId"></param>
		/// <param name="resultCount"></param>
		/// <returns>DataTable</returns>
		/// <remarks>one of two methods to determine top performers; this one returns top performing
		/// media regardless of who the artist is</remarks>
		public static DataTable GetTopAssetsInContest (int contestId, int resultCount, bool bOnlyPublished, bool bGetMature, int channelId)
		{
			
			string selectList = "SELECT " + 
				"a.asset_id, a.name, ca.number_of_votes, a.created_date, a.asset_rating_id, " + 
				"a.publish_status_id, a.status_id, a.owner_id, a.owner_username, a.asset_type_id, " + 
				" a.thumbnail_small_path, a.thumbnail_medium_path, a.thumbnail_large_path, a.thumbnail_xlarge_path, a.thumbnail_gen, " +
				"ca.contest_id, aav.attribute_value as artist_name " + 
				"FROM contest_assets ca LEFT OUTER JOIN assets a ON ca.asset_id = a.asset_id " +
				"LEFT OUTER JOIN asset_attribute_values aav ON a.asset_id = aav.asset_id AND aav.attribute_id = 33 " +
				"LEFT OUTER JOIN asset_channels ac ON ac.asset_id = a.asset_id ";

			string whereClause = " WHERE ca.contest_id = @contestId " + 
				" AND ac.channel_id = @channelId " + 
				" AND a.permission = " + (int) Constants.eASSET_PERMISSION.PUBLIC +
				SQLCommon_GetPublishedAssetsSQL ();

			if (!bGetMature)
			{
				whereClause += SQLCommon_GetNonMatureAssets ();
			}

			string orderByClause = " ORDER BY number_of_votes desc " ;

			string limitClause = " LIMIT 0, @resultCount ";

			string sqlSelect = selectList + whereClause + orderByClause + limitClause;

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@contestId", contestId);
			parameters.Add ("@resultCount", resultCount);
			parameters.Add ("@channelId", channelId);
			DataTable dtResult = KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);

			return dtResult;
		}

		/// <summary>
		/// GetBillboardItems
		/// </summary>
		/// <param name="contestId"></param>
		/// <param name="resultCount"></param>
		/// <returns>DataTable</returns>
		/// <remarks>returns top raved assets for a channel for the last 30 days
		/// </remarks>
		public static PagedDataTable GetBillboardItems (string image_path, string image_size, int resultCount, bool bGetMature, bool bOnlyPublished, int pageNumber, int channelId, int dateRange)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string selectList = " a.asset_id, a.name, ac.created_date, " + 
				" a.publish_status_id, a.status_id, a.owner_id, a.owner_username, a.asset_type_id, " + 
				" a." + image_path + " AS thumbnail_path, '" + image_size + "' AS thumbnail_size, a.thumbnail_gen, " +
				" IF(a.asset_rating_id = 3, true, IF(a.asset_rating_id = 6, true, IF(a.asset_rating_id = 9, true, false ))) AS mature_profile, " +
				" ass.number_of_diggs ";

			string tableList = " assets a INNER JOIN ";

			//select the correct table based on time frame desired
			if(dateRange <= 1)
			{
				tableList += " summary_asset_diggs_1_days ass ";
			}
			else if(dateRange <= 7)
			{
				tableList += " summary_asset_diggs_7_days ass ";
			}
			else if(dateRange <= 31)
			{
				tableList += " summary_asset_diggs_30_days ass ";
			}
			else
			{
				tableList += " summary_asset_diggs_1_year ass ";
			}

			tableList += " ON a.asset_id = ass.asset_id " +
			"LEFT OUTER JOIN asset_channels ac ON ac.asset_id = a.asset_id ";

			string whereClause = " ac.channel_id = @channelId " + 
				" AND a.permission = " + (int) Constants.eASSET_PERMISSION.PUBLIC +
				SQLCommon_GetPublishedAssetsSQL ();

			if (!bGetMature)
			{
				whereClause += SQLCommon_GetNonMatureAssets ();
			}

			string orderby = " number_of_diggs desc ";

			parameters.Add ("@channelId", channelId);
			return dbUtility.GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, resultCount);
		}

		/// <summary>
		/// GetAssetsInContest
		/// </summary>
		/// <param name="contestId"></param>
		/// <returns></returns>
		public static PagedDataTable GetAssetsInContest (int contestId, int channelId, int ownerId, bool bOnlyPublished, bool bGetMature, string searchString, string filter, string orderby, int pageNumber, int pageSize)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string selectList = " rand() as RandomOrder, ca.contest_id, a.asset_id, a.asset_type_id, a.name, a.short_description, a.created_date, " +
				" a.asset_rating_id, a.amount, a.publish_status_id,  a.status_id, " +
				" a.owner_id, a.owner_id as user_id, a.owner_username, a.owner_username as username, a.keywords, " +
				" a.thumbnail_small_path, a.thumbnail_medium_path, a.thumbnail_large_path, a.thumbnail_xlarge_path, a.thumbnail_gen, " +
				" ca.number_of_votes, ass.number_of_downloads, " +
				" aav.attribute_value as artist_name ";

			string tableList = " asset_channels ac, contest_assets ca, " +
				" assets a " +
				" INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id " +
				" LEFT OUTER JOIN asset_attribute_values aav ON a.asset_id = aav.asset_id and aav.attribute_id = 33 ";

			string whereClause = " ac.asset_id = a.asset_id " +
				" AND ac.channel_id = @channelId " +
				" AND ca.contest_id = @contestId " +
				" AND ca.asset_id = a.asset_id " +
				" AND a.permission = " + (int) Constants.eASSET_PERMISSION.PUBLIC +
				SQLCommon_GetPublishedAssetsSQL ();

			if (!bGetMature)
			{
				whereClause += SQLCommon_GetNonMatureAssets ();
			}

			if (searchString.Length > 0)
			{
				whereClause += " AND MATCH (a.name,a.teaser, a.body_text, a.owner_username, a.short_description,a.keywords) AGAINST (@searchString) ";
				parameters.Add ("@searchString", searchString);
			}

			if (ownerId > 0)
			{
				whereClause += " AND a.owner_id = @ownerId";
				parameters.Add ("@ownerId", ownerId);
			}

			if (filter.Length > 0)
			{
				whereClause += " AND " + filter;
			}

			parameters.Add ("@channelId", channelId);
			parameters.Add ("@contestId", contestId);
			return dbUtility.GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}

        /// <summary>
        /// Get a list of Assets for CSR
        /// </summary>
        public static PagedDataTable GetCSRAssets (string filter, string orderby, int pageNumber, int pageSize)
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly ();

            string selectList = " file_size, " +
                " a.content_extension, a.asset_id, a.asset_type_id, a.name, a.short_description, a.created_date, a.last_updated_date, " +
                " a.asset_rating_id, a.amount, a.publish_status_id, " +
                " a.owner_id as user_id, a.owner_username as username, a.teaser, " +
                " a.category1_id, a.category2_id, a.category3_id, a.status_id, " +
                " a.keywords, a.permission, a.permission_group, a.thumbnail_small_path, a.thumbnail_medium_path, a.thumbnail_xlarge_path, a.thumbnail_gen, " +
                " ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs, ass.number_of_streams ";

            string tableList = " assets a " +
				" INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id ";

			string whereClause = "";

            if (filter.Length > 0)
            {
                //whereClause += " AND " + filter;
                whereClause = filter;
            }

            return dbUtility.GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// GetAssetBatch
        /// </summary>
        /// <param name="assetIds"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public static DataTable GetAssetsBatch (string assetIds, int ownerId)
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "SELECT a.asset_id, a.asset_type_id, a.name, a.teaser, a.amount, a.keywords, " +
				" a.thumbnail_medium_path, a.thumbnail_gen, a.permission, a.permission_group " +
                " FROM assets a " +
                " WHERE a.asset_id IN (" + assetIds + ") " +
                " AND a.owner_id = @ownerId " +
				SQLCommon_GetOnlyActiveStatusAssetsSQL () +
                " ORDER BY a.name";

            //parameters.Add ("@assetIds", assetIds);
            parameters.Add ("@ownerId", ownerId);
            return dbUtility.GetDataTable (sqlSelect, parameters);
        }

        /// <summary>
        /// UpdatePublishedTorrentUploadPercents
        /// </summary>
        /// <param name="userId"></param>
        public static void UpdatePublishedTorrentUploadPercents (int userId)
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "SELECT t.info_hash" +
                " FROM torrents t, assets a " +
                " WHERE a.torrent_id = t.torrent_id" +
                " AND t.user_id = @userId " +
                SQLCommon_GetOnlyActiveStatusAssetsSQL () +
                " AND (t.torrent_status = " + (int) Constants.eTORRENT_STATUS.SEEDING +
                " OR t.torrent_status = " + (int) Constants.eTORRENT_STATUS.NOSCRAPE +
                " OR t.torrent_status = " + (int) Constants.eTORRENT_STATUS.UPLOADING + ")" +
                " AND t.percent_complete <> 100 ";

            parameters.Add ("@userId", userId);
            DataTable dtTorrents = dbUtility.GetDataTable (sqlString, parameters);

            string strTorrents = "";

            for (int i = 0; i < dtTorrents.Rows.Count; i++)
            {
                strTorrents += dtTorrents.Rows [i]["info_hash"].ToString ();
                if (i+1 != dtTorrents.Rows.Count)
                {
                    strTorrents += ",";
                }
            }
        }

		/// <summary>
		/// Update torrent Status by asset id
		/// </summary>
		/// <param name="assetId"></param>
		/// <param name="statusId"></param>
		/// <returns></returns>
		public static int UpdateAssetTorrentStatus (int assetId, int statusId)
		{
			DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

			// Record User License Subscriptions
			string sqlInsert = "UPDATE torrents " +
				"INNER JOIN assets ON assets.torrent_id = torrents.torrent_id " +
				"SET torrent_status = @statusId " +
				"WHERE assets.asset_id = @assetId ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@statusId", statusId);
			parameters.Add ("@assetId", assetId);
			dbUtility.ExecuteNonQuery (sqlInsert, parameters);
			return 0;
		}

        /// <summary>
        /// Get a list of a user's published Games
        /// </summary>
        public static PagedDataTable GetPublishedGames (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly ();

            string selectList = " a.file_size, a.asset_id, a.asset_type_id, a.name as current_name, a.short_description, a.created_date, " +
                " a.asset_rating_id, a.amount, a.status_id, " +
                " u.user_id, u.username, a.teaser, a.publish_status_id, " +
				" a.thumbnail_small_path, a.thumbnail_medium_path, a.thumbnail_gen ";

            string tableList = "users u, assets a ";

            string whereClaus = " a.owner_id = u.user_id " +
                " AND a.is_kaneva_game = @isKanevaGame " +
                SQLCommon_GetOnlyActiveStatusAssetsSQL ();

			if (userId > 0)
			{
				whereClaus += " AND a.owner_id = @userId ";
				parameters.Add ("@userId", userId);

			}

            if (filter.Length > 0)
            {
                whereClaus += " AND " + filter;
            }

            parameters.Add ("@isKanevaGame", 1);
            return dbUtility.GetPagedDataTable (selectList, tableList, whereClaus, orderby, parameters, pageNumber, pageSize);
        }

		/// <summary>
		/// Get a list of published Games
		/// </summary>
		public static PagedDataTable GetItGameAssets (string filter, string orderby, int pageNumber, int pageSize)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly ();

			string selectList = " a.file_size, a.asset_id, a.asset_type_id, a.name, a.short_description, a.created_date, " +
				" a.asset_rating_id, a.amount, a.status_id, " +
				" a.thumbnail_medium_path, a.thumbnail_gen, " +
				" u.user_id, u.username, a.teaser, a.publish_status_id";

            string tableList = "users u, assets a ";

			string whereClaus = " a.owner_id = u.user_id " +
				" AND a.is_kaneva_game = @isKanevaGame " +
				SQLCommon_GetOnlyActiveStatusAssetsSQL ();

			if (filter.Length > 0)
			{
				whereClaus += " AND " + filter;
			}

			parameters.Add ("@assetTypeId", (int) Constants.eASSET_TYPE.GAME);
			parameters.Add ("@isKanevaGame", 1);
			return dbUtility.GetPagedDataTable (selectList, tableList, whereClaus, orderby, parameters, pageNumber, pageSize);
		}

        /// <summary>
        /// Is this asset id a pilot game?
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static bool IsPilotGame (int assetId)
        {
            return IsGameType (assetId, Constants.eLICENSE_SUBSCRIPTIONS.PILOT);
        }

        /// <summary>
        /// Is this asset id a commercial game?
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static bool IsCommercialGame (int assetId)
        {
            return IsGameType (assetId, Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL);
        }

        /// <summary>
        /// IsGameType
        /// </summary>
        /// <returns></returns>
        private static bool IsGameType (int assetId, Constants.eLICENSE_SUBSCRIPTIONS licenseType)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string selectList = "SELECT gl.game_license_id " +
                " FROM game_license gl " +
                " WHERE gl.license_type = @licenseType " +
                " AND gl.asset_id = @assetId " +
                " AND gl.status_id != @statusId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@licenseType", (int) licenseType);
            parameters.Add ("@statusId", (int) Constants.eGAME_LICENSE_STATUS.DELETED);
            DataRow drResult = dbUtility.GetDataRow (selectList, parameters, false);
            return (drResult != null);
        }

		/// <summary>
		/// IsKanevaGame
		/// </summary>
		/// <returns>True if it is a Kaneva Game, i.e. Game built with KGP</returns>
		public static bool IsKanevaGame (DataRow drAsset)
		{
			if (drAsset ["asset_type_id"].Equals (DBNull.Value) || drAsset ["is_kaneva_game"].Equals (DBNull.Value))
			{
				return false;
			}

			// Is it a game?
			return (((int) Constants.eASSET_TYPE.GAME).Equals (Convert.ToInt32 (drAsset ["asset_type_id"])) && Convert.ToInt32 (drAsset ["is_kaneva_game"]).Equals (1));
		}

		/// <summary>
		/// GetRelatedItems
		/// 
		/// Do not pass an orderby if you want it to come out as it is put into the database via the scheduled task
		/// 
		/// </summary>
		public static PagedDataTable GetRelatedItems (int assetId, bool bGetMature, string filter, string orderby, int pageNumber, int pageSize)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string selectList = " a.asset_id, a.file_size, " +
                " a.asset_type_id, a.name, a.teaser, a.created_date, " +
                " a.asset_rating_id, a.owner_id, a.keywords, " +
				" a.thumbnail_medium_path, a.thumbnail_gen, " +
                " u.user_id, u.username, " +
				" com.name_no_spaces, " +
				" ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs ";

			string tableList;

			if (bGetMature)
			{
				tableList = "related_items ri USE INDEX (baid) " +
					" INNER JOIN  vw_published_public_mature_assets a ON ri.related_asset_id = a.asset_id " +
					" INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id " +
					" INNER JOIN users u ON a.owner_id = u.user_id, " + 
					" communities_personal com ";



			}
			else
			{
				tableList = "related_items ri USE INDEX (baid) " +
					" INNER JOIN  vw_published_public_assets a ON ri.related_asset_id = a.asset_id " +
					" INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id " +
					" INNER JOIN users u ON a.owner_id = u.user_id, " + 
					" communities_personal com ";
			}

            string whereClause = " ri.base_asset_id = @assetId " +
				" AND u.user_id = com.creator_id ";

            if (filter.Length > 0)
            {
                whereClause += " AND " + filter;
            }

			parameters.Add ("@assetId", assetId);
			return dbUtility.GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}

        /// <summary>
        /// Get the game icon used to construct the game launcher
        /// </summary>
        /// <param name="topicID"></param>
        /// <returns></returns>
        public static DataRow GetGameIconImage (int assetId)
        {
            string sqlSelect = "SELECT icon_image_data, icon_image_path, icon_image_type " +
                " FROM assets " +
                " WHERE asset_id = @assetId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            DataTable dtResult = KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);

            if (dtResult.Rows.Count > 0)
            {
                return dtResult.Rows [0];
            }

            // Existing row was not found, get a new one
            return null;
        }

        /// <summary>
        /// Get a user asset rating
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static DataRow GetUserAssetRating (int assetRatingId)
        {
            string sqlSelect = "SELECT uar.asset_rating_id, uar.asset_id, uar.user_id, uar.rating, uar.comment, uar.asset_rating_type, " +
                " uar.last_updated_date, uar.created_date, uar.status_id " +
                " FROM user_asset_ratings uar " +
                " WHERE uar.asset_rating_id = @assetRatingId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetRatingId", assetRatingId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// Update user asset rating comment
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static int UpdateUserAssetRating (int assetRatingId, int userId, string comment)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "UPDATE user_asset_ratings " +
                " SET comment = @comment, " +
                " last_updated_date = " + dbUtility.GetCurrentDateFunction () + ", " +
                " last_updated_user_id = @lastUpdatedUserId " +
                " WHERE asset_rating_id = @assetRatingId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetRatingId", assetRatingId);
            parameters.Add ("@comment", comment);
            parameters.Add ("@lastUpdatedUserId", userId);
            return dbUtility.ExecuteNonQuery (sqlSelect, parameters);
        }

		/// <summary>
		/// DeleteAssetRating - Used for deleting ratings only
		/// </summary>
		public static int DeleteAssetRating (int assetRatingId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "DELETE FROM user_asset_ratings WHERE asset_rating_id = @assetRatingId";
			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetRatingId", assetRatingId);
			return dbUtility.ExecuteNonQuery (sqlString, parameters);
		}

        /// <summary>
        /// DeleteAssetRating - Used for deleting comments
        /// </summary>
        public static int DeleteAssetRating (int assetId, int assetRatingId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "DELETE FROM user_asset_ratings WHERE asset_rating_id = @assetRatingId";
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetRatingId", assetRatingId);
            dbUtility.ExecuteNonQuery (sqlString, parameters);

            // Update the reply count
            sqlString = "UPDATE assets_stats " +
                " SET number_of_comments = (number_of_comments - 1) " +
                " WHERE asset_id = @assetId";

            // Reset params
            parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// Get a list of asset comments
        /// </summary>
        /// <param name="assetId"></param>
        public static PagedDataTable GetAssetComments (int assetId, int pageNumber, int pageSize)
        {
            string sqlSelectList = "a.asset_rating_id, a.asset_id, a.comment, a.parent_comment_id, a.created_date, a.last_updated_date, " +
                " u.username, u.user_id, u2.username as updatedUsername ";

            string sqlTableList = "users u, user_asset_ratings a LEFT OUTER JOIN users u2 ON a.last_updated_user_id = u2.user_id";

            string sqlWhereClause = "a.asset_id = @assetId " +
                " AND a.asset_rating_type = " + (int) Constants.eASSET_RATING_TYPE.COMMENT +
                " AND a.user_id = u.user_id" +
                " AND a.status_id = " + (int) Constants.eFORUM_STATUS.ACTIVE;

            string orderby = "a.asset_rating_id DESC";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            return KanevaGlobals.GetDatabaseUtility ().GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// Get a list of asset comments
        /// </summary>
        /// <param name="assetId"></param>
        public static PagedDataTable GetAssetCommentsByCommunityId (int communityId, DateTime sinceDate, int pageNumber, int pageSize)
        {
            string sqlSelectList = "a.asset_rating_id, a.asset_id, ass.name, a.comment, a.parent_comment_id, a.created_date, a.last_updated_date, " +
                " u.username, u.user_id, u2.username as updatedUsername ";

            string sqlTableList = "users u, assets ass, user_asset_ratings a LEFT OUTER JOIN users u2 ON a.last_updated_user_id = u2.user_id";

            string sqlWhereClause = "ass.community_id = @communityId " +
                " AND a.asset_rating_type = " + (int) Constants.eASSET_RATING_TYPE.COMMENT +
                " AND a.asset_id = ass.asset_id" +
                " AND a.user_id = u.user_id" +
                " AND a.status_id = " + (int) Constants.eFORUM_STATUS.ACTIVE +
                " AND a.created_date > @sinceDate ";

            string orderby = "a.asset_rating_id DESC";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@communityId", communityId);
            parameters.Add ("@sinceDate", sinceDate);
            return KanevaGlobals.GetDatabaseUtility ().GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// Get the asset image data
        /// </summary>
        public static DataRow GetAssetImage (int assetId)
        {
              string sqlSelect = "SELECT image_path, image_type, asset_type_id, target_dir, content_extension " +
                " FROM assets " +
                " WHERE asset_id = @assetId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            DataTable dtResult = KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);

            if (dtResult.Rows.Count > 0)
            {
                return dtResult.Rows [0];
            }

            // Existing row was not found, get a new one
            return null;
        }

        /// <summary>
        /// DoesAssetNameExist
        /// </summary>
        /// <returns></returns>
        public static bool AssetNameAlreadyExists (string name, int assetId)
        {
            Hashtable parameters = new Hashtable ();

            // Make sure this asset name does not already exist
            string sqlString = "SELECT COUNT(*) " +
                " FROM assets where name = @name " +
                " AND asset_id <> @assetId" +
                " AND status_id <> " + (int) Constants.eASSET_STATUS.DELETED;

            parameters.Add ("@assetId", assetId);
            parameters.Add ("@name", name);
            int count = KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sqlString, parameters);
            return (count > 0);
        }

		/// <summary>
		/// Update a asset
		/// </summary>
		public static int UpdateAsset (int ownerId, int assetId, string name, string teaser,
			double amount)
		{
			return UpdateAsset(ownerId, assetId, name, teaser, amount, -1);
		}

        /// <summary>
        /// Update a asset
        /// </summary>
        public static int UpdateAsset (int ownerId, int assetId, string name, string teaser,
			double amount, int statusId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string sqlString = "UPDATE assets " +
                " SET " +
                " amount = @amount" +
                (statusId > 0 ? ",status_id = @statusId" : "") +
                " WHERE asset_id = @assetId" +
                " and owner_id = @ownerId ";

            parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@ownerId", ownerId);
            parameters.Add ("@amount", amount);
			if(statusId > 0)
			{
				parameters.Add ("@statusId", statusId);
			}

            KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			UpdateAssetsFT (assetId, name, teaser);

            return 0;
        }

		/// <summary>
		/// UpdateAssetsFT
		/// </summary>
		/// <returns></returns>
		public static int UpdateAssetsFT (int assetId, string ownerUsername)
		{
			Hashtable parameters = new Hashtable ();

			string sqlString = "UPDATE assets " +
				" SET " +
				" owner_username = @ownerUsername " +
				" WHERE asset_id = @assetId";

			parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@ownerUsername", ownerUsername);
			return KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// UpdateAssetsFT
		/// </summary>
		/// <returns></returns>
		public static int UpdateAssetsFT (int assetId, string name, string teaser)
		{
			Hashtable parameters = new Hashtable ();

			string sqlString = "UPDATE assets " +
				" SET " +
				" name = @name," +
				" teaser = @teaser " +
				" WHERE asset_id = @assetId";

			parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@name", name);
			parameters.Add ("@teaser", teaser);
			return KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// UpdateAssetsFT
		/// </summary>
		/// <returns></returns>
		public static int UpdateAssetsFT (int assetId, string name, string bodyText, string teaser, string shortDescription)
		{
			Hashtable parameters = new Hashtable ();

			string sqlString = "UPDATE assets " +
				" SET " +
				" name = @name," +
				" body_text = @bodyText," +
				" teaser = @teaser, " +
				" short_description = @shortDescription " +
				" WHERE asset_id = @assetId";

			parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@name", name);
			parameters.Add ("@teaser", teaser);
			parameters.Add ("@shortDescription", shortDescription);
			parameters.Add ("@bodyText", bodyText);
			return KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// UpdateAssetsFT
		/// </summary>
		/// <returns></returns>
		public static int UpdateAssetsFT (int assetId, string name, string description, string teaser, string bodyText,
			string ownerUsername, string shortDescription, string categoryDescription, string subCategoryDescription, string keywords)
		{
			Hashtable parameters = new Hashtable ();

			string sqlString = "UPDATE assets " +
				" SET " +
				" name = @name," +
				" teaser = @teaser," +
				" body_text = @bodyText," +
				" owner_username = @ownerUsername," +
				" short_description = @shortDescription," +
				" keywords = @keywords" +
				" WHERE asset_id = @assetId";

			parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@name", name);
			parameters.Add ("@teaser", teaser);
			parameters.Add ("@bodyText", bodyText);
			parameters.Add ("@ownerUsername", ownerUsername);
			parameters.Add ("@subCategoryDescription", subCategoryDescription);
			parameters.Add ("@keywords", keywords);

			return KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

        /// <summary>
        /// Update a asset
        /// </summary>
        public static int UpdateAsset (int userId, int assetId, int assetTypeId, string name, string bodyText, string shortDescription, string teaser,
            int category1Id, int category2Id, int category3Id, double amount, string keiPointId, string imageCaption, string allowComments,
            string allowRatings, int assetRatingId, string patchServerURL,
            string licenseName, string licenseCCType, string licenseURL, string licenseType, string licenseAdditional,
			int permission, int permissionGroup)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string aRId = (assetRatingId == 0)? "NULL": assetRatingId.ToString ();

            string sqlString = "UPDATE assets " +
                " SET " +
                " asset_type_id = @assetTypeId," +
                " category1_id = @category1Id," +
                " category2_id = @category2Id," +
				" category3_id = @category3Id," +
                " amount = @amount," +
                " kei_point_id = @keiPointId," +
                " image_caption = @imageCaption," +
                " allow_comments = @allowComments," +
                " allow_ratings = @allowRatings," +
                " asset_rating_id = " + aRId + "," +
                " patch_server_url = @patchServerURL," +
                " last_updated_date = " + dbUtility.GetCurrentDateFunction () + "," +
                " last_updated_user_id = @userId," +
                " license_name = @licenseName," +
                " license_additional = @licenseAdditional," +
                " license_cc  = @licenseCc," +
                " license_URL  = @licenseURL," +
                " license_type = @licenseType," +
				" permission = @permission," +
				" permission_group = @permissionGroup," +
                " status_id = @statusId";

            parameters = new Hashtable ();

            sqlString += " WHERE asset_id = @assetId";

            parameters.Add ("@assetId", assetId);
            parameters.Add ("@assetTypeId", assetTypeId);
            parameters.Add ("@category1Id", category1Id);
            parameters.Add ("@category2Id", category2Id);
			parameters.Add ("@category3Id", category3Id);
            parameters.Add ("@amount", amount);
            parameters.Add ("@keiPointId", keiPointId);
            parameters.Add ("@imageCaption", imageCaption);
            parameters.Add ("@allowComments", allowComments);
            parameters.Add ("@allowRatings", allowRatings);
            parameters.Add ("@patchServerURL", patchServerURL);
            parameters.Add ("@userId", userId);
            parameters.Add ("@licenseName", licenseName);
            parameters.Add ("@licenseAdditional", licenseAdditional);
            parameters.Add ("@licenseCc", licenseCCType);
            parameters.Add ("@licenseURL", licenseURL);
            parameters.Add ("@licenseType", licenseType);
			parameters.Add ("@permission", permission);
			parameters.Add ("@permissionGroup", permissionGroup);
            parameters.Add ("@statusId", (int) Constants.eASSET_STATUS.ACTIVE);
            KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			// 
			UpdateAssetsFT (assetId, name, bodyText, teaser, shortDescription);

			// Tell search it is restricted
			if (assetRatingId.Equals ((int) Constants.eASSET_RATING.MATURE))
			{
				// Delete it now from the Search boxes
				sqlString = "CALL flag_search_asset ( @assetId )";
				parameters.Clear ();            
				parameters.Add ("@assetId", assetId);
				KanevaGlobals.GetDatabaseUtilityMasterSearch ().ExecuteNonQuery (sqlString, parameters, 3);
			}

            return 0;
        }

		public static void UpdateAssetBatch (string assetIds, int ownerId, int userId, int assetTypeId,
			int category1Id, int category2Id, int category3Id,
			double amount, int assetRatingId,
			string licenseType, string licenseName, string licenseAdditional, string licenseCC, string licenseURL)
		{
			 UpdateAssetBatch (assetIds, ownerId, userId, assetTypeId, category1Id, category2Id, 
				 category3Id, amount, assetRatingId, licenseType, licenseName, licenseAdditional, 
				 licenseCC, licenseURL, 0, 0);
		}

        /// <summary>
        /// UpdateAssetBatch
        /// </summary>
        public static void UpdateAssetBatch (string assetIds, int ownerId, int userId, int assetTypeId,
            int category1Id, int category2Id, int category3Id,
            double amount, int assetRatingId,
            string licenseType, string licenseName, string licenseAdditional, string licenseCC, 
			string licenseURL, int permission, int permissionsGroup)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string sqlString = "UPDATE assets " +
                " SET " +
                " last_updated_date = " + dbUtility.GetCurrentDateFunction () + "," +
                " last_updated_user_id = @userId," +
                " category1_id = @category1Id," +
                " category2_id = @category2Id," +
                " category3_id = @category3Id," +
                " asset_rating_id = @assetRatingId";

			if (assetTypeId > 0)
			{
				parameters.Add ("@assetTypeId", assetTypeId);
				sqlString += ",asset_type_id = @assetTypeId";
			}

			if (permission > 0)
			{
				parameters.Add ("@permission", permission);
				sqlString += ",permission = @permission";
			}

			if (permissionsGroup > 0)
			{
				parameters.Add ("@permissionsGroup", permissionsGroup);
				sqlString += ",permission_group = @permissionsGroup";
			}


            if (!amount.Equals (-99.0))
            {
                parameters.Add ("@amount", amount);
                sqlString += ",amount = @amount";
            }

            if (licenseType.Trim ().Length > 0)
            {
                parameters.Add ("@licenseType", licenseType);
                parameters.Add ("@licenseName", licenseName);
                parameters.Add ("@licenseAdditional", licenseAdditional);
                parameters.Add ("@licenseCC", licenseCC);
                parameters.Add ("@licenseURL", licenseURL);

                sqlString += ",license_type = @licenseType, license_name = @licenseName, " +
                    " license_additional = @licenseAdditional, license_cc = @licenseCC, " +
                    " license_url = @licenseURL";
            }

            sqlString += " WHERE owner_id = @ownerId " +
                " AND asset_id IN (" + assetIds + ")";

            parameters.Add ("@userId", userId);
            parameters.Add ("@ownerId", ownerId);
            parameters.Add ("@category1Id", category1Id);
            parameters.Add ("@category2Id", category2Id);
            parameters.Add ("@category3Id", category3Id);
            parameters.Add ("@assetRatingId", assetRatingId);
            KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
        }

		/// <summary>
		/// Update a asset
		/// </summary>
		public static int UpdateAssetFilePath (int assetId, string filename, string filepath, long fileSize)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			string sqlString = "UPDATE assets " +
				" SET " +
				" content_extension = @content_extension," +
				" target_dir = @target_dir," +
				" file_size = @file_size" +
				" WHERE asset_id = @asset_id";

			parameters = new Hashtable ();
			parameters.Add ("@asset_id", assetId);
			parameters.Add ("@content_extension", filename);
			parameters.Add ("@target_dir", filepath);
			parameters.Add ("@file_size", fileSize);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			return 0;
		}

        /// <summary>
        /// update the status_id of a asset row
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static void UpdateAssetStatus(int assetId, int statusId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "UPDATE assets " +
                " SET " +
                " status_id = @status_id " +
                " WHERE asset_id = @assetId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@status_id", statusId);
            KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
        }

		/// <summary>
		/// TransferItem
		/// </summary>
		public static void TransferItem (int assetId, int userId, string username, int userIdUpdating)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "UPDATE assets " +
				" SET " +
				" owner_id = @userId, " +
				" last_updated_date = " + dbUtility.GetCurrentDateFunction () + "," +
				" last_updated_user_id = @userIdUpdating " +
				" WHERE asset_id = @assetId";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@userId", userId);
			parameters.Add ("@userIdUpdating", userIdUpdating);
			parameters.Add ("@assetId", assetId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);

			//
			UpdateAssetsFT (assetId, username);

			// Add it to the new members channel
			InsertAssetChannel (assetId, CommunityUtility.GetPersonalChannelId (userId));
		}

		/// <summary>
		/// Update the Asset Asset Sort order
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="avatar"></param>
		public static void UpdateAssetSortOrder (int assetId, int sortOrder)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "UPDATE assets " +
				" SET sort_order = @sortOrder " +
				" WHERE asset_id = @assetId";

			parameters.Add ("@sortOrder", sortOrder);
			parameters.Add ("@assetId", assetId);

			dbUtility.ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// Update the Asset Asset Sort order
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="avatar"></param>
		public static void UpdateAssetGroupAssetSortOrder (int assetId, int sortOrder, int assetGroupId)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "UPDATE asset_group_assets " +
				" SET sort_order = @sortOrder " +
				" WHERE asset_id = @assetId" + 
				" AND asset_group_id = @assetGroupId";

			parameters.Add ("@sortOrder", sortOrder);
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@assetGroupId", assetGroupId);

			dbUtility.ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
        /// Update the Asset Image
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="avatar"></param>
		public static void UpdateAssetImage (int assetId, string imagePath, string imageType)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "UPDATE assets " +
				" SET image_path = @imagePath, image_type = @imageType " +
				" WHERE asset_id = @assetId";

			parameters.Add ("@assetId", assetId);

			if (imagePath.Trim ().Length.Equals (0))
			{
				parameters.Add ("@imagePath", DBNull.Value);
			}
			else
			{
				parameters.Add ("@imagePath", imagePath);
			}

			parameters.Add ("@imageType", imageType);
			dbUtility.ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// Delete asset thumbs
		/// </summary>
		public static int DeleteAssetThumbs (int assetId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			
			string sqlString = "UPDATE assets " +
				" SET " +
				" image_path = @imagePath," +
				" image_type = @imageType, " +
				" thumbnail_small_path = @thumbnailSmallPath," +
				" thumbnail_medium_path = @thumbnailMediumPath," +
				" thumbnail_large_path = @thumbnailLargePath," +
				" thumbnail_xlarge_path = @thumbnailXLargePath, " +
				" thumbnail_assetdetails_path = @thumbnailAssetdetailsPath " +
				" WHERE asset_id = @assetId";

			Hashtable parameters = new Hashtable ();

			parameters.Add ("@imagePath", DBNull.Value);
			parameters.Add ("@thumbnailSmallPath", "");
			parameters.Add ("@thumbnailMediumPath", "");
			parameters.Add ("@thumbnailLargePath", "");
			parameters.Add ("@thumbnailXLargePath", "");
			parameters.Add ("@thumbnailAssetdetailsPath", "");
			parameters.Add ("@imageType", "");
			parameters.Add ("@assetId", assetId);

			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
			return 0;
		}

        /// <summary>
        /// Update the Asset Icon Image for a game asset. Used to make the gamelauncher.
        /// </summary>
        public static void UpdateGameIconImage (int assetId, byte [] imageData, string imagePath, string imageType)
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "UPDATE assets " +
                " SET icon_image_data = @imageData, icon_image_path = @imagePath, icon_image_type = @imageType " +
                " WHERE asset_id = @assetId";

            parameters.Add ("@assetId", assetId);
            parameters.Add ("@imageData", imageData);
            parameters.Add ("@imagePath", imagePath);
            parameters.Add ("@imageType", imageType);
            dbUtility.ExecuteNonQuery (sqlString, parameters);
        }


        /// <summary>
        /// GetGameLicense
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataRow GetGameLicense (int gameLicenseId)
        {
            string sqlSelect = "SELECT gl.game_license_id, gl.asset_id, gl.license_type, gl.game_key, gl.server_name, " +
                " gl.purchase_date, gl.patch_url, gl.patch_url_v2, gl.admin_url, gl.status_id, " +
                " gls.name as status_name, gls.description as status_description, " +
				" gl.IT_game_server, gl.IT_game_directory , gl.IT_game_binaries_directory, gl.server_version " +
                " FROM game_license gl, game_license_status gls " +
                " WHERE game_license_id = @gameLicenseId" +
                " AND gl.status_id = gls.status_id ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@gameLicenseId", gameLicenseId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// GetGameLicense
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataTable GetGameLicenses (int assetId, string filter)
        {
            string sqlSelect = "SELECT ls.name, ls.max_server_users, " +
                " gl.game_license_id, gl.asset_id, gl.license_type, gl.game_key, gl.server_name, " +
                " gl.purchase_date, gl.status_id, gl.patch_url, " +
                " gls.name as status_name, gls.description as status_description, " +
				" cge.last_ping_datetime, COALESCE(cge.engine_id,0) as engine_id " +
                " FROM game_license gl LEFT OUTER JOIN current_game_engines cge ON gl.game_key  = cge.registration_key, " +
				" license_subscription ls, game_license_status gls " +
                " WHERE asset_id = @assetId" +
                " AND gl.status_id = gls.status_id " +
                " AND gl.license_type = ls.license_subscription_id " +
                " AND gl.status_id != @statusId ";

            if (filter.Length > 0)
            {
                sqlSelect += "AND " + filter;
            }

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@statusId", (int) Constants.eGAME_LICENSE_STATUS.DELETED);
            return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
        }



        /// <summary>
        /// InsertGameLicense
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static int InsertGameLicense (int assetId, int licenseType, int userId, string serverName, string patchURL, Constants.eGAME_LICENSE_STATUS status)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            // Make sure this user is the game owner
            if (!IsAssetOwner (userId, assetId))
            {
                return -1;
            }

            string sqlString = "INSERT INTO game_license " +
                " (asset_id, license_type, game_key, server_name, purchase_date, patch_url, status_id) " +
                " VALUES " +
                "(@assetId, @licenseType, @gameKey, @serverName, " + dbUtility.GetCurrentDateFunction () + ", @patchURL, @statusId)";

            int id = 0;
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@licenseType", licenseType);
            parameters.Add ("@gameKey", userId + "U" + assetId + "K" + KanevaGlobals.GenerateUniqueString (15));
            parameters.Add ("@serverName", serverName);
            parameters.Add ("@patchURL", patchURL);
            parameters.Add ("@statusId", (int) status);
            dbUtility.ExecuteIdentityInsert (sqlString, parameters, ref id);
            return id;
        }

		/// <summary>
		/// InsertITGameLicense
		/// </summary>
		public static int InsertITGameLicense (int assetId, int licenseType, int userId, string serverName, string patchURL, string patchURLV2, Constants.eGAME_LICENSE_STATUS status, string ITGameServer, string ITGameDirectory, string ITBinaryDirectory, string serverVersion)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// Make sure this user has IT rights
			if (!UsersUtility.IsUserIT (userId))
			{
				return -1;
			}

			string sqlString = "INSERT INTO game_license " +
                " (asset_id, license_type, game_key, server_name, purchase_date, patch_url, patch_url_v2, status_id, " +
				" IT_game_server, IT_game_directory, IT_game_binaries_directory, server_version) " +
				" VALUES " +
                "(@assetId, @licenseType, @gameKey, @serverName, " + dbUtility.GetCurrentDateFunction() + ", @patchURL, @patchURLV2, @statusId, " +
				" @ITGameServer, @ITGameDirectory, @ITBinaryDirectory, @serverVersion)";

			int id = 0;
			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@licenseType", licenseType);
			parameters.Add ("@gameKey", userId + "U" + assetId + "K" + KanevaGlobals.GenerateUniqueString (15));
			parameters.Add ("@serverName", serverName);
			parameters.Add ("@patchURL", patchURL);
            parameters.Add("@patchURLV2", patchURLV2);
			parameters.Add ("@ITGameServer", ITGameServer);
			parameters.Add ("@ITGameDirectory", ITGameDirectory);
			parameters.Add ("@ITBinaryDirectory", ITBinaryDirectory);
			parameters.Add ("@statusId", (int) status);
			parameters.Add ("@serverVersion", serverVersion);
			dbUtility.ExecuteIdentityInsert (sqlString, parameters, ref id);
			return id;
		}

        /// <summary>
        /// UpdateGameLicense
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static int UpdateGameLicense (int gameLicenseId, int assetId, int userId, string serverName, string patchURL)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            // Make sure this user is the game owner
            if (!IsAssetOwner (userId, assetId))
            {
                return -1;
            }

            string sqlString = "UPDATE game_license " +
                " SET " +
                " server_name = @serverName," +
                " patch_url = @patchURL " +
                " WHERE game_license_id = @gameLicenseId" +
                " AND asset_id = @assetId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@gameLicenseId", gameLicenseId);
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@serverName", serverName);
            parameters.Add ("@patchURL", patchURL);
            dbUtility.ExecuteNonQuery (sqlString, parameters);
            return 0;
        }

		/// <summary>
		/// UpdateITGameLicense
		/// </summary>
        public static int UpdateITGameLicense(int gameLicenseId, int userId, int assetId, string serverName, string patchURL, string patchURLV2, string ITGameServer, string ITGameDirectory, string ITBinaryDirectory, string serverVersion)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// Make sure this user has IT rights
			if (!UsersUtility.IsUserIT (userId))
			{
				return -1;
			}

			string sqlString = "UPDATE game_license " +
				" SET " +
				" server_name = @serverName," +
				" patch_url = @patchURL," +
                " patch_url_v2 = @patchURLV2, " +
				" IT_game_server = @ITGameServer," +
				" IT_game_directory = @ITGameDirectory," +
				" IT_game_binaries_directory = @ITBinaryDirectory," +
				" server_version = @serverVersion " +
				" WHERE game_license_id = @gameLicenseId" +
				" AND asset_id = @assetId";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@gameLicenseId", gameLicenseId);
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@serverName", serverName);
			parameters.Add ("@patchURL", patchURL);
            parameters.Add("@patchURLV2", patchURLV2);
			parameters.Add ("@ITGameServer", ITGameServer);
			parameters.Add ("@ITGameDirectory", ITGameDirectory);
			parameters.Add ("@ITBinaryDirectory", ITBinaryDirectory);
			parameters.Add ("@serverVersion", serverVersion);
			dbUtility.ExecuteNonQuery (sqlString, parameters);
			return 0;
		}

        /// <summary>
        /// UpdateGameLicense
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static int UpdateGameLicense (int gameLicenseId, int assetId, int userId, Constants.eGAME_LICENSE_STATUS statusId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            // Make sure this user is the game owner
            if (!IsAssetOwner (userId, assetId))
            {
                return -1;
            }

            string sqlString = "UPDATE game_license " +
                " SET " +
                " status_id = @statusId" +
                " WHERE game_license_id = @gameLicenseId" +
                " AND asset_id = @assetId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@gameLicenseId", gameLicenseId);
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@statusId", (int) statusId);
            dbUtility.ExecuteNonQuery (sqlString, parameters);
            return 0;
        }

		/// <summary>
		/// UpdateITGameLicense
		/// </summary>
		public static int UpdateITGameLicense (int gameLicenseId, int assetId, int userId, Constants.eGAME_LICENSE_STATUS statusId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// Make sure this user has IT rights
			if (!UsersUtility.IsUserIT (userId))
			{
				return -1;
			}

			string sqlString = "UPDATE game_license " +
				" SET " +
				" status_id = @statusId" +
				" WHERE game_license_id = @gameLicenseId" +
				" AND asset_id = @assetId";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@gameLicenseId", gameLicenseId);
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@statusId", (int) statusId);
			dbUtility.ExecuteNonQuery (sqlString, parameters);
			return 0;
		}

		/// <summary>
		/// Insert a new contest asset
		/// </summary>
		public static int InsertContestAsset(int assetId, int contestId, string phoneNumber)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "INSERT INTO contest_assets ( " +
				" asset_id, contest_id, added_date, number_of_votes, phone_number) " +
				" VALUES (@assetId, @contestId, " + dbUtility.GetCurrentDateFunction () + ", 0, @phoneNumber)";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@contestid", contestId);
			parameters.Add ("@phoneNumber", phoneNumber);
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert (sqlString, parameters, ref assetId);

			return assetId;
		}

		/// <summary>
		/// Insert a new offsite asset
		/// </summary>
		public static int InsertOffSiteAsset (string offSiteAssetId, int assetTypeId, int assetSubTypeID, string title,
			int owner_id, string ownerUsername, int publish_status_id, int permission, int assetRatingId, string teaser, int categoryId )
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "INSERT INTO assets ( " +
				" asset_offsite_id, asset_type_id, asset_sub_type_id, is_kaneva_game, owner_id, " +
				" file_size, kei_point_id, " +
				" allow_comments, allow_ratings, created_date, " +
				" status_id, last_updated_date, last_updated_user_id, " +
				" game_encryption_key, permission, asset_rating_id, category1_id, " +
                " name, owner_username, teaser ";

			if (publish_status_id > 0)
			{
				sqlString += ", publish_status_id";
			}

			sqlString += " ) VALUES (" +
				"@offSiteAssetId, @assetTypeId, @assetSubTypeID, 0, @owner_id, " +
				" 0, @keiPointId," +
				" 'Y', 'Y'," + dbUtility.GetCurrentDateFunction () + "," +
				(int) Constants.eASSET_STATUS.ACTIVE + ", " + dbUtility.GetCurrentDateFunction () + ", @owner_id," +
				" @gameEncryptionKey, @permission, @assetRatingId, @categoryId, " +
                " @name, @ownerUsername, @teaser ";


			if(publish_status_id > 0)
			{
				sqlString += ", @publish_status_id";
			}

			sqlString += ")";

			int assetId = 0;
			Hashtable parameters = new Hashtable ();
			parameters.Add ("@offSiteAssetId", offSiteAssetId);
			parameters.Add ("@assetTypeId", assetTypeId);
			parameters.Add ("@assetSubTypeID", assetSubTypeID);
			parameters.Add ("@owner_id", owner_id);
			parameters.Add ("@keiPointId", Constants.CURR_KPOINT);
			parameters.Add ("@gameEncryptionKey", KanevaGlobals.GenerateUniqueString (16));
			parameters.Add ("@permission", permission);
			parameters.Add ("@assetRatingId", assetRatingId);
			parameters.Add ("@categoryId", categoryId);

            parameters.Add ("@name", title);
            parameters.Add ("@ownerUsername", ownerUsername);
            parameters.Add ("@teaser", teaser == string.Empty ? null : teaser);

			if(publish_status_id > 0)
			{
				parameters.Add ("@publish_status_id", publish_status_id);
			}
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert (sqlString, parameters, ref assetId);

			return assetId;
		}
	
		/// <summary>
		/// Insert a new asset
		/// </summary>
		public static int InsertAsset (int assetTypeId, string name,
			int owner_id, string ownerUsername,
			int publish_status_id, int permission, int assetRatingId, int category1_id, string teaser)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "INSERT INTO assets ( " +
				" asset_type_id, is_kaneva_game, owner_id, " +
				" file_size, kei_point_id, " +
				" allow_comments, allow_ratings, created_date, " +
				" status_id, last_updated_date, last_updated_user_id, " +
				" game_encryption_key, permission, asset_rating_id, category1_id, " +
                " name, owner_username, teaser ";

			if (publish_status_id > 0)
			{
				sqlString += ", publish_status_id";
			}

			sqlString += " ) VALUES (" +
				"@assetTypeId, 0, @owner_id, " +
				" 0, @keiPointId," +
				" 'Y', 'Y'," + dbUtility.GetCurrentDateFunction () + "," +
				(int) Constants.eASSET_STATUS.NEW + ", " + dbUtility.GetCurrentDateFunction () + ", @owner_id," +
				" @gameEncryptionKey, @permission, @assetRatingId, @category1_id,  " +
                " @name, @ownerUsername, @teaser ";


			if(publish_status_id > 0)
			{
				sqlString += ", @publish_status_id";
			}

			sqlString += ")";

			int assetId = 0;
			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetTypeId", assetTypeId);
			parameters.Add ("@owner_id", owner_id);
			parameters.Add ("@keiPointId", Constants.CURR_KPOINT);
			parameters.Add ("@gameEncryptionKey", KanevaGlobals.GenerateUniqueString (16));
			parameters.Add ("@permission", permission);
			parameters.Add ("@assetRatingId", assetRatingId);
			parameters.Add ("@category1_id", category1_id);
            parameters.Add("@name", name);
            parameters.Add("@ownerUsername", ownerUsername);
            parameters.Add("@teaser", teaser == string.Empty ? null : teaser);

			if(publish_status_id > 0)
			{
				parameters.Add ("@publish_status_id", publish_status_id);
			}
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert (sqlString, parameters, ref assetId);

			return assetId;
		}

		/// <summary>
		/// InsertAssetChannel
		/// </summary>
		public static int InsertAssetChannel (int assetId, int channelId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// Make sure it does not already exist
			string sqlSelect = "SELECT asset_id " +
				" FROM asset_channels " +
				" WHERE asset_id = @assetId " +
				" AND channel_id = @channelId ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@channelId", channelId);

			DataRow drAssetChannel = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);

			if (drAssetChannel != null)
			{
				return -1;
			}

			string sqlString = "INSERT INTO asset_channels ( " +
				" asset_id, channel_id, created_date, status_id " +
				" ) VALUES (" +
				"@assetId, @channelId," + dbUtility.GetCurrentDateFunction () + ", @statusId" +
				")";

			parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@channelId", channelId);
			parameters.Add ("@statusId", 1);

			int assetChannelId = 0;
			KanevaGlobals.GetDatabaseUtility ().ExecuteIdentityInsert (sqlString, parameters, ref assetChannelId);
			return assetChannelId;
		}

		/// <summary>
		/// GetAssetChannels
		/// </summary>
		public static PagedDataTable GetAssetChannels (int assetId, bool bGetMature, int pageNumber, int pageSize)
		{
			string sqlSelectList = "c.community_id, c.is_adult, c.name, c.is_personal, c.name_no_spaces, c.thumbnail_small_path, c.creator_id, " +
			" cs.number_of_diggs, cs.number_of_members ";

			string sqlTableList = "asset_channels ac, communities c " +
				" INNER JOIN channel_stats cs ON cs.channel_id = c.community_id ";

			string sqlWhereClause = "ac.channel_id = c.community_id " +
				" AND ac.asset_id = @assetId " +
				" AND c.status_id = " + (int) Constants.eFORUM_STATUS.ACTIVE;

			if (!bGetMature)
			{
				sqlWhereClause += " AND c.is_adult <> '" + Constants.CONTENT_TYPE_ADULT + "'";
			}

			string orderby = "cs.number_of_diggs DESC";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			return KanevaGlobals.GetDatabaseUtility ().GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// GetChannelsSharingAsset
		/// </summary>
		public static PagedDataTable GetChannelsSharingAsset (int assetId, bool bGetMature, bool bGetMemberCount, string filter, int pageNumber, int pageSize)
		{
			string sqlSelectList = "sac.asset_id, sac.community_id, sac.is_adult, sac.name, sac.is_personal, sac.name_no_spaces, sac.thumbnail_small_path, " +
				" sac.creator_id, sac.created_date ";

			string sqlTableList = "summary_asset_channels sac";

			string sqlWhereClause = "asset_id = @assetId "; 

			if (bGetMemberCount)
			{
				sqlSelectList += ", cs.number_of_members ";
				sqlTableList += " INNER JOIN channel_stats cs ON cs.channel_id = sac.community_id ";
			}

			if (!bGetMature)
			{
				sqlWhereClause += " AND is_adult <> '" + Constants.CONTENT_TYPE_ADULT + "'";
			}

			if (filter.Trim ().Length > 0)
			{
				sqlWhereClause += " AND " + filter;							 
			}

			string orderby = "created_date DESC";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			return KanevaGlobals.GetDatabaseUtility ().GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// RemoveAssetFromChannel
		/// </summary>
		public static void RemoveAssetFromChannel (int userId, int assetId, int channelId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "DELETE FROM asset_channels " +
				" WHERE " +
				" channel_id = @channelId " +
				" AND asset_id = @assetId ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@channelId", channelId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlString, parameters);
		}

        /// <summary>
        /// IsAssetOwner
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static bool IsAssetOwner (int userId, int assetId)
        {
            string sqlSelect = "SELECT COUNT(*) " +
                " FROM assets " +
                " WHERE owner_id = @userId " +
                " AND asset_id = @assetId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@userId", userId);
            int result = KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sqlSelect, parameters);
            return (result > 0);
        }

        /// <summary>
        /// Is the user a asset owner
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="assetId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static bool HasAssetDeleteRights (DataRow drAsset, int userId)
        {
            // New rule is only asset owner may delete it!!!!
            if (UsersUtility.IsUserAdministrator () || UsersUtility.IsUserCSR ())
            {
                return true;
            }
            else
            {
                return (Convert.ToInt32 (drAsset ["owner_id"]).Equals (userId));
            }
        }

        /// <summary>
        /// Delete an asset
        /// </summary>
        /// <param name="assetId"></param>
        public static int DeleteAsset (int assetId, int userId)
        {
            return DeleteAsset (assetId, userId, false);
        }

        /// <summary>
        /// Delete an asset
        /// </summary>
        /// <param name="assetId"></param>
        public static int DeleteAsset (int assetId, int userId, bool bPerminent)
        {
            int result = 0;

            DataRow drAsset = GetAsset (assetId);

            // Was the asset found?
            if (drAsset == null)
            {
                return 1;
            }
            else
            {
                // Make sure they are an admin or moderator of the community to delete the asset
                // NO, now rule is only asset owner may delete it!!!! Moderators may only remove it.
                if (!HasAssetDeleteRights (drAsset, userId))
                {
                    return 2;
                }
            }

            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            // Delete the asset
            Hashtable parameters = new Hashtable ();
            string sqlString = "";

            if (bPerminent)
            {
                sqlString = " DELETE FROM assets " +
                    " WHERE asset_id = @assetId ";
                parameters.Add ("@assetId", assetId);
                dbUtility.ExecuteNonQuery (sqlString, parameters);
            }
            else
            {
                sqlString = "UPDATE assets " +
                    " SET status_id = " + (int) Constants.eASSET_STATUS.MARKED_FOR_DELETION +
                    " ,last_updated_user_id = @userId, " +
                    " last_updated_date = " + dbUtility.GetCurrentDateFunction () +
                    " WHERE asset_id = @assetId " +
                    " AND status_id <> " + (int) Constants.eASSET_STATUS.DELETED;

                parameters.Add ("@assetId", assetId);
                parameters.Add ("@userId", userId);
                dbUtility.ExecuteNonQuery (sqlString, parameters);
            }

			// Delete all connections
			sqlString = "DELETE FROM asset_channels " +
				" WHERE asset_id = @assetId ";

			parameters.Clear ();
			parameters.Add ("@assetId", assetId);
			dbUtility.ExecuteNonQuery (sqlString, parameters);

			// Delete it now from the Search boxes
			sqlString = "CALL delete_search_asset ( @assetId )";
			parameters.Clear ();            
			parameters.Add ("@assetId", assetId);
			KanevaGlobals.GetDatabaseUtilityMasterSearch ().ExecuteNonQuery (sqlString, parameters, 3);

            return result;
        }

        /// <summary>
        /// GetAssetTypes
        /// </summary>
        public static DataTable GetAssetTypes()
        {
            Hashtable parameters = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlSelect = "SELECT asset_type_id, name, description, icon_path " +
                " FROM asset_types " +
                " ORDER BY name ASC";

            return dbUtility.GetDataTable(sqlSelect, parameters);
        }

        /// <summary>
        /// GetAssetPermissions
        /// </summary>
        public static DataTable GetAssetPermissions()
        {
            Hashtable parameters = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlSelect = "SELECT permission_id, name, description " +
                " FROM asset_permissions " +
                " ORDER BY permission_id ASC";

            return dbUtility.GetDataTable(sqlSelect, parameters);
        }

        ///// <summary>
        ///// Get the Asset Ratings
        ///// </summary>
        ///// <returns></returns>
        //public static DataTable GetAssetRatings (int assetTypeId)
        //{
        //    string sqlSelect = "SELECT asset_rating_id, name, description, minium_age " +
        //        " FROM asset_ratings " +
        //        " WHERE asset_type_id = @assetTypeId " +
        //        " ORDER BY display_order ";

        //    Hashtable parameters = new Hashtable ();
        //    parameters.Add ("@assetTypeId", assetTypeId);
        //    return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
        //}

        /// <summary>
        /// IsUserAllowedToDownload
        /// </summary>
        public static bool IsUserAllowedToDownload (int userId, int assetId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlInsert = "SELECT oi.order_item_id " +
                " FROM orders o, order_items oi " +
                " WHERE o.order_id = oi.order_id " +
                " AND o.user_id = @userId " +
                " AND oi.asset_id = @assetId " +
                " AND o.download_end_date >= " + dbUtility.GetCurrentDateFunction () +
                " AND o.transaction_status_id = " + (int) Constants.eORDER_STATUS.COMPLETED +
                " AND o.purchase_type = " + (int) Constants.ePURCHASE_TYPE.ASSET;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@assetId", assetId);
            DataRow drPt = dbUtility.GetDataRow (sqlInsert, parameters, false);
            return (drPt != null);
        }

        /// <summary>
        /// GetActiveAssetOrderRecord
        /// </summary>
        public static DataRow GetActiveAssetOrderRecord (int userId, int assetId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlInsert = "SELECT o.order_id, o.download_end_date, oi.order_item_id, " +
                " oi.number_of_downloads, oi.number_of_downloads_allowed, oi.order_item_id, oi.token, oi.number_of_streams " +
                " FROM orders o, order_items oi " +
                " WHERE o.order_id = oi.order_id " +
                " AND o.user_id = @userId " +
                " AND oi.asset_id = @assetId " +
                " AND o.download_end_date >= " + dbUtility.GetCurrentDateFunction () +
                " AND o.transaction_status_id = " + (int) Constants.eORDER_STATUS.COMPLETED;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@assetId", assetId);
            return dbUtility.GetDataRow (sqlInsert, parameters, false);
        }

        /// <summary>
        /// Get the List of possible torrent status
        /// </summary>
        /// <returns></returns>
        public static DataTable GetTorrentStatus ()
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "SELECT status_id, name, description " +
                " FROM torrent_status " +
                " ORDER BY name ";

            return dbUtility.GetDataTable (sqlSelect);
        }

        /// <summary>
        /// Get the List of possible game status
        /// </summary>
        /// <returns></returns>
        public static DataTable GetGameStatus ()
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "SELECT status_id, name, description " +
                " FROM game_status " +
                " ORDER BY name ";

            return dbUtility.GetDataTable (sqlSelect);
        }

        /// <summary>
        /// Get the List of possible game license status
        /// </summary>
        /// <returns></returns>
        public static DataTable GetGameLicenseStatus ()
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "SELECT status_id, name, description " +
                " FROM game_license_status " +
                " ORDER BY name ";

            return dbUtility.GetDataTable (sqlSelect);
        }

        /// <summary>
        /// Get the List of possible user asset subscription status
        /// </summary>
        /// <returns></returns>
        public static DataTable GetUserAssetSubscriptionStatus ()
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "SELECT status_id, name, description " +
                " FROM user_asset_subscription_status " +
                " ORDER BY name ";

            return dbUtility.GetDataTable (sqlSelect);
        }

		/// <summary>
		/// GetStatusText
		/// </summary>
		/// <param name="statusId"></param>
		public static string GetStatusText (int publishStatus, int statusId, int assetId)
		{
			string strText = "";

			if (statusId.Equals ((int) Constants.eASSET_STATUS.MARKED_FOR_DELETION))
			{
				return "<font color=\"red\">Scheduled for deletion</font>";
			}

			switch (publishStatus)
			{
					//status DELETED is removed from publish status, we should look at asset status to determine
					//whether an asset is marked for deletion or has been deleted
					//				case (int) Constants.ePUBLISH_STATUS.DELETED:
					//				{
					//					strText = "Deleted";
					//					break;
					//				}
				case (int) Constants.ePUBLISH_STATUS.ERROR:
				{
					strText = "Error";
					break;
				}
				case (int) Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE:
				{
					return "<font color=\"green\">Live on site</font>";
//					if (statusId.Equals ((int) Constants.eASSET_STATUS.NEW))
//					{
//						strText = "Requires editing to broadcast on channel";
//					}
//					else
//					{
//						return "<font color=\"green\">Live on site</font>";
//					}
//					break;
				}
				case (int) Constants.ePUBLISH_STATUS.TORRENT_CREATED:
				{
					strText = "Content Imported";
					break;
				}
				case (int) Constants.ePUBLISH_STATUS.TORRENT_IMPORTED:
				{
					strText = "Content Imported";
					break;
				}
				case (int) Constants.ePUBLISH_STATUS.UPLOADED:
				{
					strText = "Content Uploaded";
					break;
				}
				case (int) Constants.ePUBLISH_STATUS.UPLOADING:
				{
					// Show Expired (Scheduled for deletion) if past configuration value
					if (StoreUtility.HasUploadExpired (assetId))
					{
						strText = "Expired (Scheduled for deletion)";
					}
					else
					{
						strText = "Uploading";
					}

					break;
				}
				default:
				{
					strText = "Unknown status...";
					break;
				}
			}

			return "<font color=\"red\">" + strText + "</font>";
		}

		/// <summary>
		/// Insert a digg
		/// </summary>
		public static int InsertDigg (int userId, int assetId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// Must be logged in to digg someone
			if (userId < 1)
			{
                //return 2;
                return 1;
            }

			//added "hammer to let admins rave as mnay times as they like
			if(!UsersUtility.IsUserAdministrator())
			{
				// Make sure they have not already digged
				DataRow drDigg = GetDigg (userId, assetId);

				// If they are already a friend, don't do anything
				if (drDigg != null)
				{
					// Already dugg
					return 1;
				}
			}

			string sql = "INSERT INTO asset_diggs " +
				"(user_id, asset_id, created_date " +
				") VALUES (" +
				"@userId, @assetId," + dbUtility.GetCurrentDateFunction () + ")";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@userId", userId);
			parameters.Add ("@assetId", assetId);
			dbUtility.ExecuteNonQuery (sql, parameters);

			return 0;
		}

		/// <summary>
		/// Get a digg for a user
		/// </summary>
		public static DataRow GetDigg (int userId, int assetId)
		{
			string sqlSelect = "select d.digg_id " +
				" FROM asset_diggs d " +
				" WHERE user_id = @userId " +
				" AND asset_id = @assetId ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@userId", userId);
			parameters.Add ("@assetId", assetId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// Insert a Vote
		/// </summary>
		public static int InsertVote (int userId, int assetId, int contestId, bool yesVote, string ipAddress)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// Must be logged in to vote
			if (userId < 1)
			{
				return 2;
			}

			// Only do this if in the valid date ranges
			if (!IsContestVotingAllowed (contestId))
			{
				return 3;
			}

			// Make sure they have not already voted
			DataRow drVote = GetVote (userId, assetId, contestId);
			if (drVote != null)
			{
				// Already voted
				return 1;
			}
			
			if(VotingAbuseCheck(ipAddress, assetId, contestId) >= 5)
			{
				// maximum vote from the IP exceeded
				return 4;
			}

			string sql = "INSERT INTO contest_asset_votes " +
				"(user_id, contest_id, asset_id, vote_date, yes, ip_address " +
				") VALUES (" +
				"@userId, @contestId, @assetId," + dbUtility.GetCurrentDateFunction () + ", @yesVote, INET_ATON(@ipAddress))";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@userId", userId);
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@yesVote", yesVote ? 1 : 0);
			parameters.Add ("@contestId", contestId);
			parameters.Add ("@ipAddress", ipAddress);
			dbUtility.ExecuteNonQuery (sql, parameters);

			if (yesVote)
			{
				// Update the contest yes vote cound
				sql = "UPDATE contest_assets " +
					" SET number_of_votes = number_of_votes + 1 " +
					" WHERE asset_id = @assetId " +
					" AND contest_id = @contestId";
				parameters = new Hashtable ();
				parameters.Add ("@assetId", assetId);
				parameters.Add ("@contestId", contestId);
				dbUtility.ExecuteNonQuery (sql, parameters);
			}

			return 0;
		}

		/// <summary>
		/// Check for voting abuse
		/// </summary>
		public static int VotingAbuseCheck (string currentUsersIP, int assetId, int contest_id)
		{
			string sqlSelect = "select COUNT(ip_address) " +
				" FROM contest_asset_votes cav " +
				" WHERE ip_address = INET_ATON(@ip_address) " +
				" AND asset_id = @asset_id " + 
				" AND yes = 1 " + 
				" AND contest_id = @contest_id ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@ip_address", currentUsersIP);
			parameters.Add ("@asset_id", assetId);
			parameters.Add ("@contest_id", contest_id);

			return KanevaGlobals.GetDatabaseUtility ().ExecuteScalar(sqlSelect, parameters);
		}

		/// <summary>
		/// Get a vote for a user
		/// </summary>
		public static DataRow GetVote (int userId, int assetId, int contestId)
		{
			string sqlSelect = "select cav.asset_id " +
				" FROM contest_asset_votes cav " +
				" WHERE user_id = @userId " +
				" AND asset_id = @assetId " +
				" AND contest_id = @contestId";;

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@userId", userId);
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@contestId", contestId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// IsContestVotingAllowed
		/// </summary>
		/// <param name="contestId"></param>
		/// <returns></returns>
		public static bool IsContestVotingAllowed (int contestId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlSelect = "SELECT c.contest_id " +
				" FROM contests c " +
				" WHERE contest_id = @contestId " +
				" AND c.vote_start_date <= " + dbUtility.GetCurrentDateFunction () +
                " AND c.vote_end_date >= " + dbUtility.GetCurrentDateFunction ();

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@contestId", contestId);
			DataRow drContest = dbUtility.GetDataRow (sqlSelect, parameters, false);
			return (drContest != null);
		}

		/// <summary>
		/// IsContestUploadAllowed
		/// </summary>
		/// <param name="contestId"></param>
		/// <returns></returns>
		public static bool IsContestUploadAllowed (int contestId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlSelect = "SELECT c.contest_id " +
				" FROM contests c " +
				" WHERE contest_id = @contestId " +
				" AND c.upload_start_date <= " + dbUtility.GetCurrentDateFunction () +
				" AND c.upload_end_date >= " + dbUtility.GetCurrentDateFunction ();

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@contestId", contestId);
			DataRow drContest = dbUtility.GetDataRow (sqlSelect, parameters, false);
			return (drContest != null);
		}

		/// <summary>
		/// GetAssetTags
		/// </summary>
		public static string GetAssetTags (string keywords, Page page)
		{
			return GetAssetTags (keywords, page, 99);	
		}
	
		/// <summary>
		/// GetAssetTags
		/// </summary>
		public static string GetAssetTags (string keywords, Page page, int type_id)
		{
			Hashtable htKeywords = new Hashtable ();
			StoreUtility.AddToHash (htKeywords, KanevaGlobals.GetNormalizedTags(keywords));

			string type = string.Empty;
			if (type_id != 99)
			{
				type = "&type="+type_id.ToString();
			}

			string strKeywords = "";

			IDictionaryEnumerator en = htKeywords.GetEnumerator ();
			while (en.MoveNext ())
			{
				strKeywords += "<a href=\"" + page.ResolveUrl ("~/watch/watch.kaneva?kwd=" + page.Server.UrlEncode(en.Value.ToString ())) + type + "\">" + en.Value + "</a>&nbsp; ";
			}

			return strKeywords;
		}

		/// <summary>
		/// GetAssetCategories
		/// </summary>
		public static string GetAssetCategoryList (string categories, Page page)
		{
			Hashtable htCategories = new Hashtable ();
			StoreUtility.AddToHash (',', htCategories, categories);

			string strCategories = "";

			IDictionaryEnumerator en = htCategories.GetEnumerator ();
			while (en.MoveNext ())
			{
				strCategories += "<a href=\"" + page.ResolveUrl ("~/watch/media.aspx?cat=" + page.Server.UrlEncode(en.Value.ToString ().Trim ())) + "\" class=\"Text12\">" + en.Value + "</a>&nbsp; ";
			}

			if (strCategories.Length == 0)
			{
				strCategories = "<span class=\"Text12\">None</span>";
			}

			return strCategories;
		}

		/// <summary>
		/// Get the order link
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public static string GetOrderLink (int assetId, Page page)
		{
			return page.ResolveUrl ("~/checkout/kPointSelection.aspx?assetId=" + assetId);
		}

		/// <summary>
		/// IsItemInConnectedMedia
		/// </summary>
		public static bool IsItemInConnectedMedia (int userId, int assetId)
		{
			int channelId = CommunityUtility.GetPersonalChannelId (userId);

			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			string sql = "SELECT COUNT(*) " +
				" FROM asset_channels ac " +
				" where ac.channel_id = @channel_id " +
				" AND ac.asset_id = @assetId ";

			parameters.Add ("@channelId", channelId);
			parameters.Add ("@assetId", assetId);
			int count = dbUtility.ExecuteScalar( sql, parameters );
			return count > 0;
		}

		/// <summary>
		/// GetRecommendedAssets
		/// </summary>
		public static DataTable GetRecommendedAssets (bool bGetMature, int assetTypeId, int numToShow)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlString = "SELECT a.name, a.asset_type_id, a.asset_id FROM assets a " +
				" WHERE is_Recommended = 1 ";

			if (!bGetMature)
			{
				sqlString += SQLCommon_GetNonMatureAssets ();
			}

			if (assetTypeId > 0)
			{
				parameters.Add ("@assetTypeId", assetTypeId);
				sqlString += " AND a.asset_type_id = @assetTypeId";
			}

			sqlString += " ORDER BY name DESC " +
				" LIMIT @num_to_show ";

			parameters.Add ("@num_to_show", numToShow);

			return dbUtility.GetDataTable(sqlString, parameters);
		}

		/// <summary>
		/// return all assets and their ownerid
		/// </summary>
		/// <returns></returns>
		public static DataTable GetAssetsAndOwners()
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			string query = " SELECT a.asset_id, a.owner_id FROM assets a ";

			return dbUtility.GetDataTable (query, parameters);
		}

		#endregion

        // **********************************************************************************************
        // Category, SubCategory, Attribute Functions
        // **********************************************************************************************
        #region Category, SubCategory, Attribute Functions

        /// <summary>
        /// GetCategory
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public static DataRow GetCategory (int categoryId)
        {
            string sqlSelect = "SELECT parent_category_id, category_id, asset_type_id, name, description " +
                " FROM categories " +
                " WHERE category_id = @categoryId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@categoryId", categoryId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// GetCategoryTree
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public static DataRow GetCategoryTree (int categoryId)
        {
            string sqlSelect = "SELECT c1.category_id, c1.parent_category_id, c1.name, c1.description, "+
                " c2.category_id as p1_category_id, c2.parent_category_id as p1_parent_category_id, c2.name as p1_name, c2.description as p1_description, " +
                " c3.category_id as p2_category_id, c3.parent_category_id as p2_parent_category_id, c3.name as p2_name, c3.description as p2_description " +
                " FROM categories c1 " +
                " LEFT OUTER JOIN categories c2 ON c1.parent_category_id = c2.category_id  " +
                " LEFT OUTER JOIN categories c3 ON c2.parent_category_id = c3.category_id " +
                " WHERE c1.category_id = @categoryId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@categoryId", categoryId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// GetCategory
        /// </summary>
        /// <returns></returns>
        public static int GetTopCategoryId (string name, int assetTypeId)
        {
            string sqlSelect = "SELECT category_id " +
                " FROM categories " +
                " WHERE name = @name" +
                " AND asset_type_id = @assetTypeId " +
                " AND parent_category_id IS NULL ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@name", name);
            parameters.Add ("@assetTypeId", assetTypeId);
            DataRow drResult = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);

            if (drResult != null && !drResult ["category_id"].Equals (DBNull.Value))
            {
                return Convert.ToInt32 (drResult ["category_id"]);
            }
            return 0;
        }

		/// <summary>
		/// Get the Asset Categories
		/// </summary>
		/// <returns></returns>
		public static DataTable GetAssetCategories (Constants.eASSET_TYPE assetType, string orderby)
		{
			return GetAssetCategories ((int) assetType, orderby);
		}

		/// <summary>
		/// Get the Asset Categories
		/// </summary>
		/// <returns></returns>
		public static DataTable GetAssetCategories (int assetTypeId, string orderby)
		{
			string sqlSelect = "SELECT ac.category_id, ac.name, ac.description, ac.asset_type_id " +
				" FROM asset_categories ac " +
				" WHERE asset_type_id = @assetTypeId " +
				" ORDER BY " + orderby;

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetTypeId", assetTypeId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
		}

        /// <summary>
        /// Get the Asset Categories
        /// </summary>
        /// <returns></returns>
        public static DataTable GetCategories (Constants.eASSET_TYPE assetType, string orderby)
        {
            return GetCategories ((int) assetType, orderby);
        }

        /// <summary>
        /// Get the Asset Categories
        /// </summary>
        /// <returns></returns>
        public static DataTable GetCategories (int assetTypeId, string orderby)
        {
            string sqlSelect = "SELECT cat.parent_category_id, cat.category_id, cat.asset_type_id, cat.name, cat.description, cat.sort_order " +
                " FROM categories cat " +
                " WHERE cat.asset_type_id = @assetTypeId " +
                " AND cat.parent_category_id is NULL " +
                " ORDER BY " + orderby;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetTypeId", assetTypeId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
        }


        /// <summary>
        /// Get the Asset Categories
        /// </summary>
        /// <returns></returns>
        public static DataTable GetGameCategories (int assetTypeId, bool bIsPilot, string orderby)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "SELECT cat.category_id, cat.asset_type_id, cat.name, cat.description " +
                " FROM categories cat " +
                " WHERE cat.asset_type_id = @assetTypeId " +
                " AND cat.parent_category_id is NULL " +
                " ORDER BY " + orderby;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetTypeId", assetTypeId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
        }

		/// <summary>
		/// Get the Category Attributes
		/// </summary>
		/// <returns></returns>
		public static DataTable GetAttributes (int categoryId, string orderby)
		{
			return GetAttributes (categoryId, orderby, "");
		}

		/// <summary>
        /// Get the Category Attributes
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAttributes (int categoryId, string orderby, string filter)
        {
            string sqlSelect = "SELECT attribute_id, category_id, name, description, type " +
                " FROM category_attributes " +
                " WHERE category_id = @categoryId ";

			if (filter.Trim ().Length > 0)
			{
				sqlSelect += " AND " + filter;
			}

			sqlSelect += " ORDER BY " + orderby;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@categoryId", categoryId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
        }

		/// <summary>
		/// Get the Category Attributes
		/// </summary>
		/// <returns></returns>
		public static DataTable GetAttribute (string filter, string orderby)
		{
			string sqlSelect = "SELECT attribute_id, category_id, name, description, type " +
				" FROM category_attributes ";

			if (filter.Trim ().Length > 0)
			{
				sqlSelect += " WHERE " + filter;
			}

			if (orderby.Trim ().Length > 0)
			{
				sqlSelect += " ORDER BY " + orderby;
			}

			Hashtable parameters = new Hashtable ();
			return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
		}

		/// <summary>
        /// Insert Attribute
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="attributeId"></param>
        /// <param name="categoryAttributeValueId"></param>
        /// <param name="attributeValue"></param>
        public static void InsertAssetAttributeValue (int assetId, int attributeId, int categoryAttributeValueId, string attributeValue)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            string sCategoryAttributeValueId = (categoryAttributeValueId == 0)? "NULL": categoryAttributeValueId.ToString ();

            string sqlInsert = "INSERT INTO asset_attribute_values " +
                " (asset_id, attribute_id, category_attribute_value_id, attribute_value) " +
                " VALUES " +
                " (@assetId, @attributeId, "+ sCategoryAttributeValueId + ", @attributeValue)";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@attributeId", attributeId);
            parameters.Add ("@attributeValue", attributeValue);
            dbUtility.ExecuteNonQuery (sqlInsert, parameters);
        }

        /// <summary>
        /// DeleteAssetAttributes
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static int DeleteAssetAttributes (int assetId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "DELETE" +
                " FROM asset_attribute_values " +
                " WHERE asset_id = @assetId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// Get the Attribute possible values
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAttributeValues (int attributeId, string orderby)
        {
            string sqlSelect = "SELECT attribute_id, category_attribute_value_id, name, description " +
                " FROM category_attribute_values " +
                " WHERE attribute_id = @attributeId " +
                " ORDER BY " + orderby;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@attributeId", attributeId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
        }

        /// <summary>
        /// Get the Current Asset Attribute Values
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAssetAttributeValues (int assetId)
        {
            string sqlSelect = "SELECT aav.asset_id, aav.category_attribute_value_id, aav.attribute_value, aav.attribute_id, " +
                " ca.name as attribute_name, ca.description as attribute_description, COALESCE(cav.name,attribute_value) as value_name " +
                " FROM category_attributes ca, asset_attribute_values aav LEFT OUTER JOIN category_attribute_values cav ON aav.category_attribute_value_id = cav.category_attribute_value_id  " +
                " WHERE aav.asset_id = @assetId" +
                " AND aav.attribute_id = ca.attribute_id ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);
        }

        #endregion

        // **********************************************************************************************
        // Credit Card Transactions to purchase points/point buckets
        // **********************************************************************************************
        #region Credit Card Transactions

        /// <summary>
        /// Get the List of possible transaction status
        /// </summary>
        /// <returns></returns>
        public static DataTable GetTransactionStatus ()
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "SELECT transaction_status_id, name, description, long_description " +
                " FROM purchase_point_transactions_status " +
                " ORDER BY name ";

            return dbUtility.GetDataTable (sqlSelect);
        }

        /// <summary>
        /// Get the List of point buckets
        /// </summary>
        /// <returns></returns>
        public static DataTable GetActivePromotions(string keiPointType, Double minimumPointAmount)
        {
            return GetActivePromotions(keiPointType, minimumPointAmount, (int)Constants.ePROMOTION_TYPE.CREDITS);
        }

        /// <summary>
        /// Get the List of point buckets
        /// </summary>
        /// <returns></returns>
        public static DataTable GetActivePromotions(string keiPointType, Double minimumPointAmount, int promoOfferTypeID)
        {
            return GetActivePromotions(keiPointType, minimumPointAmount, promoOfferTypeID, DateTime.Now);
        }

        /// <summary>
        /// Get the List of point buckets
        /// </summary>
        /// <returns></returns>
        public static DataTable GetActivePromotions(string keiPointType, Double minimumPointAmount, DateTime dateNOW)
        {
            return GetActivePromotions(keiPointType, minimumPointAmount, (int)Constants.ePROMOTION_TYPE.CREDITS, dateNOW);
        }

        /// <summary>
        /// Get the List of point buckets
        /// </summary>
        /// <returns></returns>
        public static DataTable GetActivePromotions(string keiPointType, Double minimumPointAmount, int promoOfferTypeID, DateTime dateNOW)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            Hashtable parameters = new Hashtable ();

            string sqlSelect = "SELECT promotion_id, bundle_title, bundle_subheading1, bundle_subheading2," +
                " dollar_amount, kei_point_amount, promotion_start, promotion_end, is_special," +
                " promotion_list_heading, promotion_description, highlight_color, special_background_color," +
                " special_background_image, special_sticker_image, promotional_package_label," +
                " kei_point_id, free_points_awarded_amount, free_kei_point_ID, promotional_offers_type_id," +
                " wok_pass_group_id, sku, special_font_color, value_of_credits " +
                " FROM promotional_offers " +
                " WHERE kei_point_id = @keiPointType " +
                " AND promotional_offers_type_id = @promoOfferTypeID " +
                " AND (@dateNOW between promotion_start AND promotion_end) ";

            if (minimumPointAmount > 0)
            {
                parameters.Add ("@minimumPointAmount", minimumPointAmount);
                sqlSelect += " AND ((kei_point_amount + free_points_awarded_amount) >= @minimumPointAmount)";
            }

            sqlSelect += " ORDER BY dollar_amount ";

            parameters.Add ("@keiPointType", keiPointType);
            parameters.Add("@promoOfferTypeID", promoOfferTypeID);
            parameters.Add("@dateNOW", dateNOW);
            DataTable dtResult = dbUtility.GetDataTable(sqlSelect, parameters);

            // Add formating for dropdowns
            dtResult.Columns.Add (new DataColumn ("display", System.Type.GetType("System.String") ));
            for (int i = 0; i < dtResult.Rows.Count; i ++)
            {
                //dtResult.Rows [i]["display"] = KanevaGlobals.FormatKPoints (Convert.ToDouble (dtResult.Rows [i]["kei_point_amount"]) + Convert.ToDouble (dtResult.Rows [i]["free_points_awarded_amount"]), false, false) + " (" + KanevaGlobals.FormatCurrency (Convert.ToDouble (dtResult.Rows [i]["dollar_amount"])) + ")";
                SetPointDisplay (dtResult.Rows [i]);
            }

            return dtResult;
        }

		
		/// <summary>
		/// Get a GiftCard
		/// </summary>
		/// <returns></returns>
		public static DataRow GetGiftCard (double dollarAmount)
		{
			string sqlSelect = "SELECT gift_card_id, dollar_amount, upc, kei_point_id, kei_point_amount, " +
				" description " +
				" FROM gift_cards " +
				" WHERE dollar_amount = @dollarAmount";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@dollarAmount", dollarAmount);
			return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

        /// <summary>
        /// Get a promotion
        /// </summary>
        /// <returns></returns>
        public static DataRow GetPromotion(int promotionId)
        {
            string sqlSelect = "SELECT promotion_id, bundle_title, bundle_subheading1, bundle_subheading2," +
                " dollar_amount, kei_point_amount, promotion_start, promotion_end, is_special," +
                " promotion_list_heading, promotion_description, highlight_color, special_background_color," +
                " special_background_image, special_sticker_image, promotional_package_label," + 
                " kei_point_id, free_points_awarded_amount, free_kei_point_ID, promotional_offers_type_id," +
                " wok_pass_group_id, sku, special_font_color, value_of_credits" +
                " FROM promotional_offers " +
                " WHERE promotion_id = @promotionId";

            Hashtable parameters = new Hashtable ();
            parameters.Add("@promotionId", promotionId);
            DataTable dtPointBucket = KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);

            dtPointBucket.Columns.Add (new DataColumn ("display", System.Type.GetType("System.String") ));

            if (dtPointBucket.Rows.Count > 0)
            {
                DataRow drPointBucket = dtPointBucket.Rows [0];
                SetPointDisplay (drPointBucket);
                return drPointBucket;
            }
            else
            {
                return null;
            }
        }


        public static DataTable GetFreeWokItems(int promotionId)
		{
			DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

            string sqlSelect = "SELECT promotion_offer_id, market_price, promotion_id, gift_card_id, community_id, quantity, wok_item_id," +
                " item_description, gender, alternate_description " +
				" FROM promotional_offer_items " +
                " WHERE promotion_id = @promotionId" +
                " ORDER BY wok_item_id ";

            parameters.Add("@promotionId", promotionId);
			DataTable dtResult = dbUtility.GetDataTable (sqlSelect, parameters);

			return dtResult;
		}

		public static DataTable GetFreeWokItemsGiftCard (int giftCardId, string gender)
		{
			DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

            string sqlSelect = "SELECT promotion_offer_id, market_price, promotion_id, gift_card_id, community_id, quantity, wok_item_id," +
                " item_description, gender, alternate_description " +
				" FROM promotional_offer_items " +
                " WHERE gift_card_id = @giftCardId" +
                " AND gender IN ( @gender, 'U')" +
                " ORDER BY gift_card_id ";

			parameters.Add ("@gender", gender);
			parameters.Add ("@giftCardId", giftCardId);
			DataTable dtResult = dbUtility.GetDataTable (sqlSelect, parameters);

			return dtResult;
		}

        public static DataTable GetFreeWokItemsCommunity (int communityId, string gender)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            Hashtable parameters = new Hashtable();

            string sqlSelect = "SELECT promotion_offer_id, market_price, promotion_id, gift_card_id, community_id, quantity, wok_item_id," +
                " item_description, gender, alternate_description " +
                " FROM promotional_offer_items " +
                " WHERE community_id = @communityId" +
                " AND gender IN ( @gender, 'U')" +
                " ORDER BY community_id ";

            parameters.Add("@gender", gender);
            parameters.Add("@communityId", communityId);
            DataTable dtResult = dbUtility.GetDataTable(sqlSelect, parameters);

            return dtResult;
        }


		/// <summary>
        /// SetPointDisplay
        /// </summary>
        /// <param name="drPointBucket"></param>
        private static void SetPointDisplay (DataRow drPointBucket)
        {
            //drPointBucket ["display"] = KanevaGlobals.FormatKPoints (Convert.ToDouble (drPointBucket ["kei_point_amount"]) + Convert.ToDouble (drPointBucket ["free_points_awarded_amount"]), false, false) + " for " + KanevaGlobals.FormatCurrency (Convert.ToDouble (drPointBucket ["dollar_amount"])) + " - " + KanevaGlobals.FormatKPoints (Convert.ToDouble (drPointBucket ["free_points_awarded_amount"])) + " Free";
			drPointBucket ["display"] = KanevaGlobals.FormatKPoints (Convert.ToDouble (drPointBucket ["kei_point_amount"]) + Convert.ToDouble (drPointBucket ["free_points_awarded_amount"]), false, false) + " for " + KanevaGlobals.FormatCurrency (Convert.ToDouble (drPointBucket ["dollar_amount"]));
        }

        /// <summary>
        /// Get a purchase point transaction record.
        /// </summary>
        /// <param name="purchaseTransactionId"></param>
        /// <returns></returns>
        public static DataRow GetPurchasePointTransaction (int purchasePointTransactionId)
        {
            string sqlSelect = "SELECT (SELECT SUM(ppta.amount) FROM purchase_point_transaction_amounts ppta WHERE ppt.point_transaction_id = ppta.point_transaction_id) as totalPoints, " +
				" u.username, " +
				" pm.name, pm.description as pm_description, " +
				" ppts.transaction_status_id ,ppts.description as status_description, " +
				" ppt.point_transaction_id, ppt.user_id, ppt.description, ppt.order_id, " +
                " ppt.payment_method_id, ppt.amount_debited, ppt.amount_credited, ppt.transaction_id, ppt.transaction_status, ppt.transaction_date, " +
                " ppt.error_description, ppt.user_license_subscription_id, ppt.point_bucket_id, " +
                " ppt.billing_information_id, ppt.order_billing_information_id, address_id " +
                " FROM users u, purchase_point_transactions ppt LEFT OUTER JOIN purchase_point_transactions_status ppts ON ppt.transaction_status = ppts.transaction_status_id, " +
				" payment_methods pm " +
                " WHERE ppt.point_transaction_id = @ptId" +
				" AND u.user_id = ppt.user_id " +
				" AND ppt.payment_method_id = pm.payment_method_id ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@ptId", purchasePointTransactionId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// GetPurchasePointTransactionAmount
        /// </summary>
        /// <param name="purchasePointTransactionId"></param>
        /// <param name="keiPointId"></param>
        /// <returns></returns>
        public static Double GetPurchasePointTransactionAmount (int purchasePointTransactionId, string keiPointId)
        {
            string sqlSelect = "SELECT ppta.amount " +
                " FROM purchase_point_transaction_amounts ppta" +
                " WHERE ppta.point_transaction_id = @ptId" +
                " AND ppta.kei_point_id = @keiPointId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@ptId", purchasePointTransactionId);
            parameters.Add ("@keiPointId", keiPointId);
            DataRow drPPTA = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);

            if (drPPTA == null)
            {
                return 0.0;
            }

            return Convert.ToDouble (drPPTA ["amount"]);
        }

        /// <summary>
        /// GetUserNonAccessPassTransactionCount
        /// </summary>
        /// <returns></returns>
        public static int GetUserNonAccessPassTransactionCount(int userId)
        {
            string filter = "promotional_offers_type_id != " + (int)Constants.ePROMOTION_TYPE.ACCESS;
            return GetUserTransactionCount(userId, filter);
        }

        /// <summary>
        /// GetUserTransactionCount
        /// </summary>
        /// <returns></returns>
        public static int GetUserTransactionCount(int userId, string filter)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
            Hashtable parameters = new Hashtable();

            string sqlString = "SELECT COUNT(ppt.user_id) FROM purchase_point_transactions ppt " +
                " INNER JOIN orders o ON ppt.order_id = o.order_id";

            if (filter != string.Empty)
            {
                sqlString += " INNER JOIN promotional_offers po ON ppt.point_bucket_id = po.promotion_id " +
                    " WHERE ppt.user_id = @userId AND o.transaction_status_id=2 AND " + filter;
            }
            else
            {
                sqlString += " WHERE ppt.user_id = @userId AND o.transaction_status_id=2";
            }
            
            parameters.Add("@userId", userId);

            return dbUtility.ExecuteScalar(sqlString, parameters);
        }

		/// <summary>
		/// GetUserCreditTransactions
		/// </summary>
		/// <returns></returns>
		public static PagedDataTable GetUserCreditTransactions (int userId, string orderby, int pageNumber, int pageSize)
		{
			return GetUserCreditTransactions (userId, string.Empty, orderby, pageNumber, pageSize);	
		}

		/// <summary>
        /// GetUserCreditTransactions
        /// </summary>
        /// <returns></returns>
        public static PagedDataTable GetUserCreditTransactions (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

            string sqlString = "SELECT SUM(ppta.amount) as kpoints, pm.name, pm.description, ppt.point_transaction_id, ppt.user_id, ppt.description as ppt_description, ppt.order_id, ppt.billing_information_id, " +
				" ppt.payment_method_id, ppt.amount_debited, ppt.amount_credited, o.transaction_status_id as order_status, ppt.transaction_status, ppt.transaction_date, " +
				" ppt.error_description, ppts.transaction_status_id ,ppts.description as status_description, bi.name_on_card, bi.card_number";

            sqlString += " FROM purchase_point_transactions ppt " + 
                " INNER JOIN purchase_point_transaction_amounts ppta ON ppt.point_transaction_id = ppta.point_transaction_id " +
                " INNER JOIN payment_methods pm ON pm.payment_method_id = ppt.payment_method_id " +
                " INNER JOIN purchase_point_transactions_status ppts ON ppt.transaction_status = ppts.transaction_status_id " +
                " INNER JOIN orders o  ON ppt.order_id = o.order_id" +
                " LEFT OUTER JOIN billing_information bi ON ppt.billing_information_id = bi.billing_information_id ";

            sqlString += " WHERE ppt.user_id = @userId ";

			if (filter != string.Empty)
			{
                sqlString += " AND " + filter;
			}

            sqlString += " GROUP BY ppt.point_transaction_id ";

			parameters.Add ("@userId", userId);

            return dbUtility.GetPagedDataTableUnion(sqlString, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// UpdatePointTransaction
        /// </summary>
        /// <returns></returns>
        public static int UpdatePointTransaction (int pointTransactionId, Constants.ePAYMENT_METHODS paymentMethodId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "UPDATE purchase_point_transactions SET " +
                " payment_method_id = @paymentMethodId " +
                " WHERE point_transaction_id = @ptId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@paymentMethodId", (int) paymentMethodId);
            parameters.Add ("@ptId", pointTransactionId);
            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// UpdatePointTransactionBillingInfoId
        /// </summary>
        /// <returns></returns>
        public static int UpdatePointTransactionBillingInfoId (int pointTransactionId, int billingInfoId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            string sBillInfoId = (billingInfoId == 0)? "NULL": billingInfoId.ToString ();

            string sqlString = "UPDATE purchase_point_transactions SET " +
                " billing_information_id = " + sBillInfoId +
                " WHERE point_transaction_id = @ptId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@ptId", pointTransactionId);
            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// UpdatePointTransactionOrderBillingInfoId
        /// </summary>
        /// <returns></returns>
        public static int UpdatePointTransactionOrderBillingInfoId (int pointTransactionId, int orderbillingInfoId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            string sOrderBillInfoId = (orderbillingInfoId == 0)? "NULL": orderbillingInfoId.ToString ();

            string sqlString = "UPDATE purchase_point_transactions SET " +
                " order_billing_information_id = " + sOrderBillInfoId +
                " WHERE point_transaction_id = @ptId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@ptId", pointTransactionId);
            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// UpdatePointTransactionAddressId
        /// </summary>
        /// <returns></returns>
        public static int UpdatePointTransactionAddressId (int pointTransactionId, int addressId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            string sAddressId = (addressId == 0)? "NULL": addressId.ToString ();

            string sqlString = "UPDATE purchase_point_transactions SET " +
                " address_id = " + sAddressId +
                " WHERE point_transaction_id = @ptId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@ptId", pointTransactionId);
            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// UpdatePointTransaction
        /// </summary>
        /// <returns></returns>
        public static int UpdatePointTransaction (int pointTransactionId, int transactionStatus, string errDescription, string transactionId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string sqlExtra = "";

            if (errDescription.Length > 0)
            {
                parameters.Add ("@errDescription", errDescription);
                sqlExtra += " , error_description = @errDescription" ;
            }

            if (transactionId.Length > 0)
            {
                parameters.Add ("@transactionId", transactionId);
                sqlExtra += " , transaction_id = @transactionId" ;
            }

            string sqlString = "UPDATE purchase_point_transactions SET " +
                " transaction_status = @transStatus, " +
                " transaction_date = " + dbUtility.GetCurrentDateFunction () +
                sqlExtra +
                " WHERE point_transaction_id = @ptId";

            parameters.Add ("@ptId", pointTransactionId);
            parameters.Add ("@transStatus", transactionStatus);
            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// UpdatePointTransaction
        /// </summary>
        /// <param name="purchasePointTransactionId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public static int UpdatePointTransaction (int purchasePointTransactionId, int orderId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string sqlString = "UPDATE purchase_point_transactions SET " +
                " order_id = @orderId " +
                " WHERE point_transaction_id = @ptId";

            parameters.Add ("@ptId", purchasePointTransactionId);
            parameters.Add ("@orderId", orderId);
            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// UpdatePointTransactionPointBucket
        /// </summary>
        /// <param name="purchasePointTransactionId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public static int UpdatePointTransactionPointBucket (int purchasePointTransactionId, int pointBucketId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string sqlString = "UPDATE purchase_point_transactions SET " +
                " point_bucket_id = @pointBucketId " +
                " WHERE point_transaction_id = @ptId";

            parameters.Add ("@ptId", purchasePointTransactionId);
            parameters.Add ("@pointBucketId", pointBucketId);
            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

		/// <summary>
		/// UpdatePointTransactionGiftCard
		/// </summary>
		public static int UpdatePointTransactionGiftCard (int purchasePointTransactionId, int giftCardId)
		{
			DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			string sqlString = "UPDATE purchase_point_transactions SET " +
				" gift_card_id = @giftCardId " +
				" WHERE point_transaction_id = @ptId";

			parameters.Add ("@ptId", purchasePointTransactionId);
			parameters.Add ("@giftCardId", giftCardId);
			return dbUtility.ExecuteNonQuery (sqlString, parameters);
		}

		/// <summary>
		/// UpdatePointTransactionPointBucket
		/// </summary>
		/// <param name="purchasePointTransactionId"></param>
		/// <param name="orderId"></param>
		/// <returns></returns>
		public static void UpdatePointTransactionPointBucket (int purchasePointTransactionId, int pointBucketId, string description, Double dollarAmount, Double KpointAmount, Double MpointAmount)
		{
			DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			string sqlString = "UPDATE purchase_point_transactions SET " +
				" point_bucket_id = @pointBucketId, " +
				" amount_debited = @dollarAmount, " +
				" description = @description " +
				" WHERE point_transaction_id = @pptId";

			parameters.Add ("@pptId", purchasePointTransactionId);
			parameters.Add ("@pointBucketId", pointBucketId);
			parameters.Add ("@dollarAmount", dollarAmount);
			parameters.Add ("@description", description);
			dbUtility.ExecuteNonQuery (sqlString, parameters);

			// Insert into amounts
			string sqlUpdateTransactionAmounts = "UPDATE purchase_point_transaction_amounts " +
				" SET kei_point_id = '" + Constants.CURR_KPOINT + "', " +
				" amount = @KpointAmount " +
				" WHERE point_transaction_id = @pptId";

			// Reset Params
			parameters = new Hashtable ();
			parameters.Add ("@pptId", purchasePointTransactionId);
			parameters.Add ("@KpointAmount", KpointAmount);
			dbUtility.ExecuteNonQuery (sqlUpdateTransactionAmounts, parameters);

			if (MpointAmount > 0.0)
			{
				// Insert into amounts
				sqlUpdateTransactionAmounts = "UPDATE purchase_point_transaction_amounts " +
					" SET kei_point_id = '" + Constants.CURR_MPOINT + "' " +
					" amount = @MpointAmount " +
					" WHERE point_transaction_id = @pptId";

				// Reset Params
				parameters = new Hashtable ();
				parameters.Add ("@pptId", purchasePointTransactionId);
				parameters.Add ("@MpointAmount", MpointAmount);
				dbUtility.ExecuteNonQuery (sqlUpdateTransactionAmounts, parameters);
			}
		}

		/// <summary>
        /// PurchasePoints
        /// </summary>
        /// <returns></returns>
        public static int PurchasePoints (int userId, int transactionStatus, string description, int billingInformationId, int paymentMethodId, Double dollarAmount, Double KpointAmount, Double MpointAmount, int userLicenseSubscriptionId, string ipAddress)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            // Insert into Purchase point transactions
            // Record amount in Purchase Transaction and purchase transaction amounts
            string sqlInsert = "INSERT INTO purchase_point_transactions " +
                " (user_id, description, billing_information_id, payment_method_id, " +
                " amount_debited, amount_credited, transaction_status, created_date, transaction_date, user_license_subscription_id, ip_address) " +
                " VALUES " +
                " (@userId, @description, NULL, @paymentMethodId, " +
                "@dollarAmount,0, @transStatus," + dbUtility.GetCurrentDateFunction () + "," +
                dbUtility.GetCurrentDateFunction ()  + ", @ulsId, @ipAddress)";

            int purchase_point_transaction_id = 0;
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@description", description);
            parameters.Add ("@paymentMethodId", paymentMethodId);
            parameters.Add ("@dollarAmount", dollarAmount);
            parameters.Add ("@transStatus", transactionStatus);
            parameters.Add ("@ulsId", userLicenseSubscriptionId);
            parameters.Add ("@ipAddress", ipAddress);
            dbUtility.ExecuteIdentityInsert (sqlInsert, parameters, ref purchase_point_transaction_id);

            // Insert into amounts
            string sqlInsertTransactionAmounts = "INSERT INTO purchase_point_transaction_amounts " +
                " (point_transaction_id, kei_point_id, amount) " +
                " VALUES ";

            // Reset Params
            parameters = new Hashtable ();
            parameters.Add ("@pptId", purchase_point_transaction_id);
            parameters.Add ("@KpointAmount", KpointAmount);
            dbUtility.ExecuteNonQuery (sqlInsertTransactionAmounts + "(@pptId, '" + Constants.CURR_KPOINT + "', @KpointAmount)", parameters);

            if (MpointAmount > 0.0)
            {
                // Reset Params
                parameters = new Hashtable ();
                parameters.Add ("@pptId", purchase_point_transaction_id);
                parameters.Add ("@MpointAmount", MpointAmount);
                dbUtility.ExecuteNonQuery (sqlInsertTransactionAmounts + "(@pptId,'" + Constants.CURR_MPOINT + "', @MpointAmount)", parameters);
            }

            return purchase_point_transaction_id;
        }

        /// <summary>
        /// GetOrderBillingInfo
        /// </summary>
        /// <returns></returns>
        public static DataRow GetOrderBillingInfo (int orderBillingInformationId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "SELECT name_on_card, card_number, card_type, exp_month, exp_day, exp_year, " +
                " address_name, phone_number, address1, address2, city, state_code, " +
                " zip_code, c.country_id, c.country " +
                " FROM order_billing_information obi, countries c" +
                " WHERE order_billing_information_id = @orderBillingInfoId" +
				" AND obi.country_id = c.country_id";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@orderBillingInfoId", orderBillingInformationId);
            return dbUtility.GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// GetOrderPaymentMethod
        /// </summary>
        /// <returns>payment_method_id</returns>
        public static int GetOrderPaymentMethod (int orderId, int pointTransactionId)
        {
            Hashtable parameters = new Hashtable ();

            string sqlSelect = "SELECT payment_method_id " +
                " FROM purchase_point_transactions " +
                " WHERE order_id = @orderId " +
                " AND point_transaction_id = @pointTransactionId";

            parameters.Add ("@orderId", orderId);
            parameters.Add ("@pointTransactionId", pointTransactionId);
            DataRow drPPT = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);

            if (drPPT == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32 (drPPT["payment_method_id"]);
            }
        }
        
        /// <summary>
        /// CopyBillingInfoToOrder
        ///
        /// Copies the billing information from address and billing_information
        /// to order_billing_information for historical purposes. i.e. In case
        /// they change thier address in the future.
        /// </summary>
        public static int CopyBillingInfoToOrder (int orderBillingInformationId, int billingInformationId, int addressId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            // If it already has one, delete it
            if (orderBillingInformationId > 0)
            {
                string sqlDelete = "DELETE FROM order_billing_information" +
                    " WHERE order_billing_information_id = @orderBillingInfoId ";
                parameters.Add ("@orderBillingInfoId", orderBillingInformationId);
                dbUtility.ExecuteNonQuery (sqlDelete, parameters);
            }

            // Insert a new one
            string sqlInsert = "INSERT INTO order_billing_information " +
                "( name_on_card, card_number, card_type, exp_month, exp_day, exp_year, " +
                " address_name, phone_number, address1, address2, city, state_code, " +
                " zip_code, country_id ) " +
                " SELECT name_on_card, card_number, card_type, exp_month, exp_day, exp_year, " +
                " name AS address_name, phone_number, address1, address2, city, state_code, " +
                " zip_code, country_id " +
                " FROM billing_information, address a " +
                " WHERE billing_information_id = @billingInformationId " +
                " AND a.address_id = @addressId ";

            int orderBillingInformationIdReturn = 0;
            parameters = new Hashtable ();
            parameters.Add ("@billingInformationId", billingInformationId);
            parameters.Add ("@addressId", addressId);
            dbUtility.ExecuteIdentityInsert (sqlInsert, parameters, ref orderBillingInformationIdReturn);
            return orderBillingInformationIdReturn;
        }

        /// <summary>
        /// CopySubscriptionBillingInfoToOrder
        /// </summary>
        public static int CopySubscriptionBillingInfoToOrder (int subBillingInformationId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            // Insert a new one
            string sqlInsert = "INSERT INTO order_billing_information " +
                "( name_on_card, card_number, card_type, exp_month, exp_day, exp_year, " +
                " address_name, phone_number, address1, address2, city, state_code, " +
                " zip_code, country_id ) " +
                " SELECT name_on_card, card_number, card_type, exp_month, exp_day, exp_year, " +
                " address_name, phone_number, address1, address2, city, state_code, " +
                " zip_code, country_id " +
                " FROM subscription_billing_information " +
                " WHERE sub_billing_information_id = @subBillingInformationId ";

            int orderBillingInformationIdReturn = 0;
            parameters = new Hashtable ();
            parameters.Add ("@subBillingInformationId", subBillingInformationId);
            dbUtility.ExecuteIdentityInsert (sqlInsert, parameters, ref orderBillingInformationIdReturn);
            return orderBillingInformationIdReturn;
        }

        /// <summary>
        /// CopySubscriptionBillingInfoToOrder
        /// </summary>
        public static int CopyOrderBillingInfoToSubscription (int orderBillingInformationId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            // Insert a new one
            string sqlInsert = "INSERT INTO subscription_billing_information " +
                "( name_on_card, card_number, card_type, exp_month, exp_day, exp_year, " +
                " address_name, phone_number, address1, address2, city, state_code, " +
                " zip_code, country_id ) " +
                " SELECT name_on_card, card_number, card_type, exp_month, exp_day, exp_year, " +
                " address_name, phone_number, address1, address2, city, state_code, " +
                " zip_code, country_id " +
                " FROM order_billing_information " +
                " WHERE order_billing_information_id = @orderBillingInformationId ";

            int subBillingInformationIdReturn = 0;
            parameters = new Hashtable ();
            parameters.Add ("@orderBillingInformationId", orderBillingInformationId);
            dbUtility.ExecuteIdentityInsert (sqlInsert, parameters, ref subBillingInformationIdReturn);
            return subBillingInformationIdReturn;
        }

        #endregion

        // **********************************************************************************************
        // Purchase Assets/Cart Functions
        // **********************************************************************************************
        #region Purchase Assets/Cart Functions Functions

		/// <summary>
		/// GenerateToken
		/// </summary>
		public static string GenerateToken (int orderId)
		{
			try
			{
				string token = KanevaGlobals.GenerateUniqueString (20);
				return ("KANEVA" +  orderId.ToString () + token).Substring (0, 20);
			}
			catch (Exception){};

			return ("KANEVA" + orderId.ToString ()).Substring (0, 20);
		}

		/// <summary>
		/// UpdateMediaViews
		/// </summary>
		public static int UpdateMediaViews (int assetId, string userIpAddress, int userId)
		{
			// Update download counts for the assets
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			string sqlUpdate = "UPDATE assets_stats " +
				" SET number_of_downloads = number_of_downloads + 1 " +
				" WHERE asset_id = @assetId";
			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			dbUtility.ExecuteNonQuery (sqlUpdate, parameters);

			// Insert the asset_views record
			string sqlInsert = "INSERT INTO asset_views " +
				" (user_id, asset_id, created_date, ip_address) " +
				" VALUES " +
				" (@userId, @assetId, " + dbUtility.GetCurrentDateFunction () + ", INET_ATON(@ipAddress))";

			parameters = new Hashtable ();
			parameters.Add ("@userId", userId);
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@ipAddress", userIpAddress);
			dbUtility.ExecuteNonQuery (sqlInsert, parameters);
			
			return 0;
		}

		/// <summary>
		/// GetUserOrders
		/// </summary>
		public static PagedDataTable GetUserViewHistory (int userId,  string orderBy, int pageNumber, int pageSize)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
			Hashtable parameters = new Hashtable ();

			string sqlSelectList = " av.view_id, av.user_id, av.created_date, a.name, a.asset_id, "+
				" a.keywords, a.asset_type_id, a.asset_rating_id, a.thumbnail_small_path, a.thumbnail_gen ";

			string sqlTableList = "asset_views av, assets a ";

			string sqlWhereClause = "av.user_id = @userId " +
				" AND av.asset_id = a.asset_id ";

			parameters.Add ("@userId", userId);
			return dbUtility.GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, orderBy, parameters, pageNumber, pageSize);
		}

        /// <summary>
        /// GetUserOrderItems
        /// </summary>
        /// <returns></returns>
        public static PagedDataTable GetUserOrderItems (int userId, int orderId, string orderBy, int pageNumber, int pageSize)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string sqlSelectList = "oi.order_item_id, oi.order_id, oi.asset_id, oi.user_asset_subscription_id, oi.featured_asset_id, oi.asset_subscription_id, oi.community_member_subscription_id, " +
                "oi.point_amount, oi.royalty_user_id_paid, oi.royalty_paid, oi.royalty_amount, a.name, a.asset_type_id, " +
                "uas.start_datetime, uas.end_datetime, o.download_end_date";

            string sqlTableList = "order_items oi LEFT OUTER JOIN user_asset_subscriptions uas ON oi.user_asset_subscription_id = uas.user_asset_subscription_id, " +
                "orders o, assets a";

            string sqlWhereClause = "oi.order_id = @orderId" +
                " AND oi.asset_id = a.asset_id" +
                " AND o.order_id = oi.order_id " +
                " AND o.user_id = @userId";

            parameters.Add ("@orderId", orderId);
            parameters.Add ("@userId", userId);
            return dbUtility.GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, orderBy, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// and purchase transaction amounts
        /// </summary>
        private static int DeductPointBalances (int userId, Double dAssetAmount, Double dUserMpointBalance, int purchaseTransactionId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            // Are they purchasing an asset or community subscription?
            string sqlInsertTransactionAmounts = "INSERT INTO purchase_transaction_amounts " +
                " (purchase_transaction_id, kei_point_id, amount) " +
                " VALUES ";

            // Deduct Point balances (Use MPoint balances first, since they have the possibility to expire)
            if (dUserMpointBalance > 0)
            {
                // Will MPoints cover everything?
                if (dAssetAmount > dUserMpointBalance)
                {
                    // Use up all MPoints, the rest to KPoints
                    Double dDiffernce = dAssetAmount - dUserMpointBalance;
                    //UsersUtility.DeductFromUserBalance (userId, Constants.CURR_MPOINT, dUserMpointBalance);
                    //UsersUtility.DeductFromUserBalance (userId, Constants.CURR_KPOINT, dDiffernce);

					UsersUtility.AdjustUserBalance (userId, Constants.CURR_MPOINT, -dUserMpointBalance, Constants.CASH_TT_BOUGHT_ITEM);
					UsersUtility.AdjustUserBalance (userId, Constants.CURR_KPOINT, -dDiffernce, Constants.CASH_TT_BOUGHT_ITEM);

                    // Insert the purchase transaction amounts
                    parameters.Add ("@ptId", purchaseTransactionId);
                    parameters.Add ("@dUserMpointBalance", dUserMpointBalance);
                    dbUtility.ExecuteNonQuery (sqlInsertTransactionAmounts + "(@ptId,'" + Constants.CURR_MPOINT + "', @dUserMpointBalance)", parameters);

                    // Reset params
                    parameters = new Hashtable ();
                    parameters.Add ("@ptId", purchaseTransactionId);
                    parameters.Add ("@dDiffernce", dDiffernce);
                    dbUtility.ExecuteNonQuery (sqlInsertTransactionAmounts + "(@ptId,'" + Constants.CURR_KPOINT + "', @dDiffernce)", parameters);
                }
                else
                {
                    // We have enough Mpoints to cover it all
                    //UsersUtility.DeductFromUserBalance (userId, Constants.CURR_MPOINT, dAssetAmount);
					UsersUtility.AdjustUserBalance (userId, Constants.CURR_MPOINT, -dAssetAmount, Constants.CASH_TT_BOUGHT_ITEM);

                    // Insert the purchase transaction amounts
                    parameters.Add ("@ptId", purchaseTransactionId);
                    parameters.Add ("@dAssetAmount", dAssetAmount);
                    dbUtility.ExecuteNonQuery (sqlInsertTransactionAmounts + "(@ptId,'" + Constants.CURR_MPOINT + "', @dAssetAmount)", parameters);
                }
            }
            else
            {
                // Use all K-Points
                //UsersUtility.DeductFromUserBalance (userId, Constants.CURR_KPOINT, dAssetAmount);
				UsersUtility.AdjustUserBalance (userId, Constants.CURR_KPOINT, -dAssetAmount, Constants.CASH_TT_BOUGHT_ITEM);

                // Insert the purchase transaction amounts
                parameters.Add ("@ptId", purchaseTransactionId);
                parameters.Add ("@dAssetAmount", dAssetAmount);
                dbUtility.ExecuteNonQuery (sqlInsertTransactionAmounts + "(@ptId,'" + Constants.CURR_KPOINT + "', @dAssetAmount)", parameters);
            }

            return 0;
        }

        /// <summary>
        /// GetItemsSoldForUser
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static int GetItemsSoldForUser (int userId, string filter)
        {
            string sqlSelect = "SELECT COALESCE(COUNT(*),0) " +
                " FROM order_items oi, orders o " +
                " WHERE oi.royalty_user_id_paid = @userId " +
                " AND o.order_id = oi.order_id " +
                " AND o.transaction_status_id = " + (int) Constants.eORDER_STATUS.COMPLETED;

            if (filter.Trim ().Length > 0)
            {
                sqlSelect += " AND " + filter;
            }

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sqlSelect, parameters);
        }

		/// <summary>
		/// GetLastItemSoldForUser
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static DataRow GetLastItemSoldForUser (int userId)
		{
			string sqlSelectList = "a.asset_id, a.name";

			string sqlTableList = "order_items oi, orders o, assets a";

			string sqlWhereClause = "oi.royalty_user_id_paid = @userId " +
				" and oi.asset_id = a.asset_id " +
				" AND o.transaction_status_id = " + (int) Constants.eORDER_STATUS.COMPLETED +
				" AND o.order_id = oi.order_id";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@userId", userId);
			PagedDataTable pdt =  KanevaGlobals.GetDatabaseUtility ().GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, "o.purchase_date DESC", parameters, 1, 1);

			if (pdt.Rows.Count > 0)
			{
				return (DataRow) pdt.Rows [0];
			}
			else
			{
				return null;
			}
		}

        /// <summary>
        /// Create a new cart
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static int CreateOrder (int userId, string ipAddress, int transactionStatusId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlInsert = "INSERT INTO orders " +
                " (purchase_type, user_id, transaction_status_id, gross_point_amount, ip_address) " +
                " VALUES " +
                " (@purchaseType, @userId, @transactionStatusId, @grossPointAmount, @ipAddress)";

            int orderId = 0;
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@purchaseType", (int) Constants.ePURCHASE_TYPE.NOT_SET);
            parameters.Add ("@userId", userId);
            parameters.Add ("@transactionStatusId", transactionStatusId);
            parameters.Add ("@grossPointAmount", 0);
            parameters.Add ("@ipAddress", ipAddress);
            dbUtility.ExecuteIdentityInsert (sqlInsert, parameters, ref orderId);
            return orderId;
        }

        /// <summary>
        /// SetPurchaseType
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="purchaseType"></param>
        public static int SetPurchaseType (int orderId, Constants.ePURCHASE_TYPE purchaseType)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlInsert = "UPDATE orders " +
                " SET purchase_type = @purchaseType" +
                " WHERE order_id = @orderId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@purchaseType", (int) purchaseType);
            parameters.Add ("@orderId", orderId);
            return dbUtility.ExecuteNonQuery (sqlInsert, parameters);
        }

        /// <summary>
        /// Get Order
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public static DataRow GetOrder (int orderId)
        {
            return GetOrder (orderId, -1);
        }

        /// <summary>
        /// Get Order
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public static DataRow GetOrder (int orderId, int userId)
        {
            Hashtable parameters = new Hashtable ();

            string sqlSelect = "SELECT o.order_id, o.purchase_type, o.user_id, o.description, o.gross_point_amount, " +
                " o.purchase_date, o.error_description, o.download_end_date, o.download_stop_date, o.point_transaction_id, ppt.point_bucket_id," +
                " o.transaction_status_id, pts.description as status_description " +
                " FROM purchase_transaction_status pts, orders o " +
				" LEFT JOIN purchase_point_transactions ppt ON ppt.order_id = o.order_id" +                
				" WHERE o.order_id = @orderId " +
                " AND o.transaction_status_id = pts.transaction_status_id";

            if (userId > -1)
            {
                sqlSelect += " AND o.user_id = @userId";
                parameters.Add ("@userId", userId);
            }

            parameters.Add ("@orderId", orderId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// Get Order
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public static DataRow GetOrder (int assetId, int userId, Constants.eORDER_STATUS orderStatus)
        {
            Hashtable parameters = new Hashtable ();

            string sqlSelect = "SELECT o.order_id, o.purchase_type, o.user_id, o.description, o.gross_point_amount, " +
                " o.purchase_date, o.error_description, o.download_end_date, o.download_stop_date, o.point_transaction_id " +
                " FROM orders o, order_items oi " +
                " WHERE oi.asset_id = @assetId " +
                " AND o.order_id = oi.order_id " +
                " AND o.transaction_status_id = @orderStatus";

            if (userId > -1)
            {
                sqlSelect += " AND o.user_id = @userId";
                parameters.Add ("@userId", userId);
            }

            parameters.Add ("@orderStatus", (int) orderStatus);
            parameters.Add ("@assetId", assetId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
        }

		/// <summary>
        /// GetOrderItems
        /// </summary>
        /// <returns></returns>
        public static PagedDataTable GetOrderItems (int orderId, string orderBy)
        {
            return GetOrderItems (orderId, orderBy, 1, Int32.MaxValue);
        }

        /// <summary>
        /// GetOrderItems
        /// </summary>
        /// <returns></returns>
        public static PagedDataTable GetOrderItems (int orderId, string orderBy, int pageNumber, int pageSize)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string sqlSelectList = "oi.token, oi.order_item_id, oi.order_id, oi.asset_id, oi.user_asset_subscription_id, oi.asset_subscription_id, oi.community_member_subscription_id, " +
                "oi.point_amount, oi.royalty_user_id_paid, oi.royalty_paid, oi.royalty_amount, oi.quantity, oi.featured_asset_id, a.name, a.asset_type_id, " +
                "oi.number_of_downloads, oi.number_of_downloads_allowed, oi.game_license_id";

            string sqlTableList = "order_items oi, assets a";

            string sqlWhereClause = "oi.order_id = @orderId" +
                " AND oi.asset_id = a.asset_id";

            parameters.Add ("@orderId", orderId);
            return dbUtility.GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, orderBy, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// InsertOrderItem
        /// </summary>
        /// <returns></returns>
        public static int InsertOrderItem (DataRow drOrder, int assetId, int quantity, string token)
        {
            return InsertOrderItem (drOrder, assetId, quantity, token, 0.0, false);
        }

        /// <summary>
        /// InsertOrderItem
        /// </summary>
        /// <returns></returns>
        public static int InsertOrderItem (DataRow drOrder, int assetId, int quantity, string token, Double dAssetOverridePrice, bool bOverridePrice)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters;
            Double pointAmount = 0.0;
            Double royaltyAmount = 0.0;
            int royaltyUserIdPaid = 0;

            int orderId = Convert.ToInt32 (drOrder ["order_id"]);

            // Is it an asset? Calculate the royalties
            if (assetId > 0)
            {
                DataRow drAsset = GetAsset (assetId);

                // May want to override amount (Used for grandfathered game subscriptions)
                if (bOverridePrice)
                {
                    pointAmount = dAssetOverridePrice;
                }
                else
                {
                    pointAmount = Convert.ToDouble (drAsset ["amount"]);
                }

                // Get the royalties for this price.
                GetAssetRoyalties (drAsset, pointAmount, ref royaltyAmount, ref royaltyUserIdPaid);
            }

            // Does the item already exist? (For now, make it so you can add a subscription twice?)
            if (assetId > 0)
            {
                string sqlSelect = "SELECT order_item_id " +
                    " FROM order_items " +
                    " WHERE asset_id = @assetID" +
                    " AND order_id = @orderId";

                parameters = new Hashtable ();
                parameters.Add ("@assetID", assetId);
                parameters.Add ("@orderId", orderId);
                DataRow drOrderItem = dbUtility.GetDataRow (sqlSelect, parameters, false);

                // Increase quanity?
                // Did we find it?
                if (drOrderItem != null)
                {
                    // Update to the current price and royalties here
                    string sqlUpdate = "UPDATE order_items " +
                        " SET point_amount = @pointAmount, " +
                        " royalty_amount = @royaltyAmount, " +
                        " royalty_paid = @royaltyPaid, " +
                        " royalty_user_id_paid = @royaltyUserIdPaid " +
                        " WHERE order_item_id = @orderItemId ";

                    // quantity = (quantity + 1),
                    parameters = new Hashtable ();
                    parameters.Add ("@pointAmount", pointAmount);
                    parameters.Add ("@royaltyAmount", royaltyAmount);
                    parameters.Add ("@royaltyPaid", Constants.ROYALTY_NOT_PAID);
                    parameters.Add ("@royaltyUserIdPaid", royaltyUserIdPaid);
                    parameters.Add ("@orderItemId", Convert.ToInt32 (drOrderItem ["order_item_id"]));
                    dbUtility.ExecuteNonQuery (sqlUpdate, parameters);

                    // Update the order total
                    UpdateOrderTotal (orderId);

                    return Convert.ToInt32 (drOrderItem ["order_item_id"]);
                }
            }

            // Add the item to the cart
            string sqlInsert = "INSERT INTO order_items " +
                " (order_id, asset_id, token, " +
                "point_amount, quantity, royalty_amount, royalty_paid, royalty_user_id_paid) " +
                " VALUES " +
                " (@orderId, @assetId, @token, "+
                " @pointAmount, @quantity, @royaltyAmount, @royaltyPaid, @royaltyUserIdPaid)";

            int orderItemId = 0;
            parameters = new Hashtable ();
            parameters.Add ("@orderId", Convert.ToInt32 (drOrder ["order_id"]));
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@token", token);
            parameters.Add ("@pointAmount", pointAmount);
            parameters.Add ("@quantity", quantity);
            parameters.Add ("@royaltyAmount", royaltyAmount);
            parameters.Add ("@royaltyPaid", Constants.ROYALTY_NOT_PAID);
            parameters.Add ("@royaltyUserIdPaid", royaltyUserIdPaid);
            dbUtility.ExecuteIdentityInsert (sqlInsert, parameters, ref orderItemId);

            // Update the order total
            UpdateOrderTotal (orderId);

            return orderItemId;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="orderItemId"></param>
        public static void UpdateOrderItemAsset (int assetId, int orderItemId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            // No royalties for Featuring an item
            // Set the status to completed
            string sqlUpdate = "UPDATE order_items " +
                " SET featured_asset_id = NULL, "+
                " asset_subscription_id = NULL, " +
                " asset_id = @assetId " +
                " WHERE " +
                " order_item_id = @orderItemId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@orderItemId", orderItemId);
            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);

            // Update the order total
            string sqlSelect = "SELECT order_id " +
                " FROM order_items " +
                " WHERE " +
                " order_item_id = @orderItemId ";
            parameters = new Hashtable ();
            parameters.Add ("@orderItemId", orderItemId);
            DataRow drOrder = dbUtility.GetDataRow (sqlSelect, parameters, false);
            UpdateOrderTotal (Convert.ToInt32 (drOrder ["order_id"]));
        }

        /// <summary>
        /// UpdateOrderItemFeatured
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="orderItemId"></param>
        public static void UpdateOrderItemFeatured (int featuredAssetId, Double featuredItemPrice, int orderItemId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            // No royalties for Featuring an item
            // Set the status to completed
            string sqlUpdate = "UPDATE order_items " +
                " SET featured_asset_id = @featuredAssetId, "+
                " asset_subscription_id = NULL, " +
                " point_amount = @pointAmount, royalty_amount = 0, royalty_user_id_paid = 0 " +
                " WHERE " +
                " order_item_id = @orderItemId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@featuredAssetId", featuredAssetId);
            parameters.Add ("@orderItemId", orderItemId);
            parameters.Add ("@pointAmount", featuredItemPrice);
            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);

            // Update the order total
            string sqlSelect = "SELECT order_id " +
                " FROM order_items " +
                " WHERE " +
                " order_item_id = @orderItemId ";
            parameters = new Hashtable ();
            parameters.Add ("@orderItemId", orderItemId);
            DataRow drOrder = dbUtility.GetDataRow (sqlSelect, parameters, false);
            UpdateOrderTotal (Convert.ToInt32 (drOrder ["order_id"]));
        }

        /// <summary>
        /// UpdateOrderItemPilotOneDay
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="pilotDayPrice"></param>
        /// <param name="orderItemId"></param>
        public static void UpdateOrderItemPilotOneDay (Double pilotDayPrice, int orderItemId, int assetId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            // No royalties for Pilot Purchase,
            // But still fill in user id paid so it shows up in the history
            int royaltyUserIdPaid = 0;
            Double royaltyAmount = 0;
            DataRow drAsset = GetAsset (assetId);

            GetAssetRoyalties (drAsset, pilotDayPrice, ref royaltyAmount, ref royaltyUserIdPaid);

            string sqlUpdate = "UPDATE order_items " +
                " SET point_amount = @pointAmount, " +
                " royalty_amount = 0, " +
                " royalty_user_id_paid = @royaltyUserIdPaid, " +
                " featured_asset_id = NULL, " +
                " asset_subscription_id = NULL " +
                " WHERE " +
                " order_item_id = @orderItemId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@orderItemId", orderItemId);
            parameters.Add ("@pointAmount", pilotDayPrice);
            parameters.Add ("@royaltyUserIdPaid", royaltyUserIdPaid);
            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);

            // Update the order total
            string sqlSelect = "SELECT order_id " +
                " FROM order_items " +
                " WHERE " +
                " order_item_id = @orderItemId ";
            parameters = new Hashtable ();
            parameters.Add ("@orderItemId", orderItemId);
            DataRow drOrder = dbUtility.GetDataRow (sqlSelect, parameters, false);
            UpdateOrderTotal (Convert.ToInt32 (drOrder ["order_id"]));
        }

        /// <summary>
        /// UpdateOrderItemAssetSubscription - Only use this one when updating royalties and price based on the
        /// original asset subscription
        /// </summary>
        public static void UpdateOrderItemAssetSubscription (int assetId, int assetSubscriptionId, int orderItemId)
        {
            // Get the asset subscription
            DataRow drAssetSubscription = GetAssetSubscription (assetSubscriptionId);
            UpdateOrderItemAssetSubscription (assetId, assetSubscriptionId, orderItemId, Convert.ToDouble (drAssetSubscription ["amount"]));
        }

        /// <summary>
        /// UpdateOrderItemAssetSubscription - Called from the billing service with grandfathered subscription price. Difference between this one and above
        /// it royalites are calculated from the grandfathered price.
        /// </summary>
        public static void UpdateOrderItemAssetSubscription (int assetId, int assetSubscriptionId, int orderItemId, Double dSubscriptionPrice)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            Double royaltyAmount = 0.0;
            int royaltyUserIdPaid = 0;

            // Calculate the royalities since we have a subscription length
            DataRow drAsset = GetAsset (assetId);

            // Don't call GetAssetSubscriptionRoyalties, this is called from billing server, prices
            // are grandfathered in, not taken from the asset_subscription table
            GetAssetRoyalties (drAsset, dSubscriptionPrice, ref royaltyAmount, ref royaltyUserIdPaid);

            // Update the order items
            string sqlUpdate = "UPDATE order_items " +
                " SET asset_subscription_id = @assetSubscriptionId, " +
                " point_amount = @pointAmount, " +
                " royalty_amount = @royaltyAmount, " +
                " royalty_user_id_paid = @royaltyUserIdPaid, " +
                " featured_asset_id = NULL " +
                " WHERE " +
                " order_item_id = @orderItemId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetSubscriptionId", assetSubscriptionId);
            parameters.Add ("@orderItemId", orderItemId);
            parameters.Add ("@pointAmount", dSubscriptionPrice);
            parameters.Add ("@royaltyAmount", royaltyAmount);
            parameters.Add ("@royaltyUserIdPaid", royaltyUserIdPaid);
            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);

            // Update the order total
            string sqlSelect = "SELECT order_id " +
                " FROM order_items " +
                " WHERE " +
                " order_item_id = @orderItemId ";
            parameters = new Hashtable ();
            parameters.Add ("@orderItemId", orderItemId);
            DataRow drOrder = dbUtility.GetDataRow (sqlSelect, parameters, false);
            UpdateOrderTotal (Convert.ToInt32 (drOrder ["order_id"]));
        }

        /// <summary>
        /// UpdateOrderItemCommunitySubscription
        /// </summary>
        public static void UpdateOrderItemCommunitySubscription (int communityMemberSubscriptionId, int orderItemId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            Double royaltyAmount = 0;
            int royaltyUserIdPaid = 0;

            // Get the community member subscription
            DataRow drCommunityMemberSubscription = StoreUtility.GetCommunityMemberSubscriptionById (communityMemberSubscriptionId);

            // Get the community subscription
            DataRow drCommunitySubscription = CommunityUtility.GetCommunitySubscription (Convert.ToInt32 (drCommunityMemberSubscription ["community_subscription_id"]));

            // Calculate the royalities since we have a subscription length
            GetCommunitySubscriptionRoyalties (drCommunitySubscription, ref royaltyAmount, ref royaltyUserIdPaid);

            // Set the status to completed
            string sqlUpdate = "UPDATE order_items " +
                " set community_member_subscription_id = @communityMemberSubscriptionId, asset_id = NULL, " +
                " point_amount = @pointAmount, royalty_amount = @royaltyAmount, royalty_user_id_paid = @royaltyUserIdPaid " +
                " WHERE " +
                " order_item_id = @orderItemId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@communityMemberSubscriptionId", communityMemberSubscriptionId);
            parameters.Add ("@orderItemId", orderItemId);
            parameters.Add ("@pointAmount", Convert.ToDouble (drCommunitySubscription ["subscription_amount"]));
            parameters.Add ("@royaltyAmount", royaltyAmount);
            parameters.Add ("@royaltyUserIdPaid", royaltyUserIdPaid);
            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);

            // Update the order total
            string sqlSelect = "SELECT order_id " +
                " FROM order_items " +
                " WHERE " +
                " order_item_id = @orderItemId ";
            parameters = new Hashtable ();
            parameters.Add ("@orderItemId", orderItemId);
            DataRow drOrder = dbUtility.GetDataRow (sqlSelect, parameters, false);
            UpdateOrderTotal (Convert.ToInt32 (drOrder ["order_id"]));
        }

        /// <summary>
        /// UpdateOrderItemCommercialServerAuth
        /// </summary>
        public static void UpdateOrderItemGameLicenseAuth (int gameLicenseId, int orderItemId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            // No royalties for Featuring an item
            // Set the status to completed
            string sqlUpdate = "UPDATE order_items " +
                " SET featured_asset_id = NULL, "+
                " asset_subscription_id = NULL, " +
                " game_license_id = @gameLicenseId, " +
                " point_amount = 0, royalty_amount = 0, royalty_user_id_paid = 0 " +
                " WHERE " +
                " order_item_id = @orderItemId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@orderItemId", orderItemId);
            parameters.Add ("@gameLicenseId", gameLicenseId);
            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// GetAssetRoyalties
        /// </summary>
        private static void GetAssetRoyalties (DataRow drAsset, Double assetPrice, ref Double royaltyAmount, ref int royaltyUserIdPaid)
        {
            royaltyAmount = 0;
            royaltyUserIdPaid = 0;

            // Is there a community owner for the asset?
            if (drAsset == null)
            {
                return;
            }

            royaltyUserIdPaid = Convert.ToInt32 (drAsset ["owner_id"]);
            royaltyAmount = assetPrice * (50 * .01);
        }

        /// <summary>
        /// GetCommunitySubscriptionRoyalties
        /// </summary>
        private static void GetCommunitySubscriptionRoyalties (DataRow drCommunitySubscription, ref Double royaltyAmount, ref int royaltyUserIdPaid)
        {
            royaltyAmount = 0;
            royaltyUserIdPaid = 0;

            int communityId = Convert.ToInt32 (drCommunitySubscription ["community_id"]);

            // Business Rule is, pay the community owner.
            // The user who gets paid is the owner of the community
            DataRow drCommunity = CommunityUtility.GetCommunity (communityId);

            royaltyUserIdPaid = Convert.ToInt32 (drCommunity ["creator_id"]);
            royaltyAmount = Convert.ToDouble (drCommunitySubscription ["subscription_amount"]) * (Convert.ToInt32 (drCommunity ["percent_royalty_paid"]) * .01);
        }

        /// <summary>
        /// RemoveOrderItem
        /// </summary>
        /// <returns></returns>
        public static int RemoveOrderItem (DataRow drOrder, int assetId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            if (drOrder != null)
            {
                Hashtable parameters = new Hashtable ();

                // Delete item from cart
                string sqlString = "DELETE" +
                    " FROM order_items" +
                    " WHERE order_id = @orderId ";

                if (assetId > 0)
                {
                    sqlString += " AND asset_id = @assetId";
                    parameters.Add ("@assetId", assetId);
                }

                parameters.Add ("@orderId", Convert.ToInt32 (drOrder ["order_id"]));
                dbUtility.ExecuteNonQuery (sqlString, parameters);

                // Update the order total
                UpdateOrderTotal (Convert.ToInt32 (drOrder ["order_id"]));
            }

            return 0;
        }

        /// <summary>
        /// UpdateAssetTags that are stored in the asset table
        /// </summary>
        /// <param name="channelId"></param>
        public static void UpdateAssetTags (int assetId, string strKeywords)
        {
            string sqlUpdate = "UPDATE assets SET keywords = @keywords WHERE asset_id = @assetId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@keywords", strKeywords.Trim ());
            KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlUpdate, parameters);
        }

		/// <summary>
		/// UpdateAssetNumberOfStreams
		/// </summary>
		public static void UpdateAssetNumberOfStreams (int assetId)
		{
			string sqlSelect = "UPDATE assets_stats SET " +
				" number_of_streams = (number_of_streams + 1) " +
				" WHERE asset_id = @assetId";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlSelect, parameters);
		}

		/// <summary>
		/// UpdateOrderNumberOfDownloads
		/// </summary>
		public static void UpdateOrderNumberOfDownloads (int orderItemId)
		{
			string sqlSelect = "UPDATE order_items SET " +
				" number_of_downloads = (number_of_downloads + 1) " +
				" WHERE order_item_id = @orderItemId";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@orderItemId", orderItemId);
			KanevaGlobals.GetDatabaseUtility ().ExecuteNonQuery (sqlSelect, parameters);
		}

        /// <summary>
        /// UpdateOrderTotal
        /// </summary>
        public static void UpdateOrderTotal (int orderId)
        {
            UpdateOrderTotal (0, orderId);
        }

        /// <summary>
        /// UpdateOrderTotal
        /// </summary>
        public static void UpdateOrderTotal (int userId, int orderId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            // Set the status to completed
            string sqlUpdate = "UPDATE orders " +
                " set gross_point_amount = (SELECT SUM(point_amount) FROM order_items WHERE order_id = @orderId) " +
                " WHERE " +
                " order_id = @orderId ";

            if (userId > 0)
            {
                parameters.Add ("@userId", userId);
                sqlUpdate += " AND user_id = @userId";
            }

            parameters.Add ("@orderId", orderId);
            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

		/// <summary>
        /// PuchaseOrderSubscriptions - Adds subscriptions to appropriate tables
        /// </summary>
        private static int PuchaseOrderSubscriptions (int userId, int orderId, DataRow drOrder)
        {
			DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

			int userAssetSubscriptionId = 0;
			int assetSubscriptionId = 0;
			string endDate = "";
			string sqlUpdate = "";

            // Fill in any subscriptions
            PagedDataTable dtCartItems = GetOrderItems (orderId, "a.name");

            for (int i = 0; i < dtCartItems.Rows.Count; i ++)
            {
                // If it is a game and not free, add the subscription
                if (!dtCartItems.Rows [i]["asset_id"].Equals (DBNull.Value) && StoreUtility.IsGameSubscription (Convert.ToInt32 (dtCartItems.Rows [i]["asset_id"])))
                {
                    // Make sure they are not just featuring it
                    if (dtCartItems.Rows [i]["featured_asset_id"].Equals (DBNull.Value))
                    {
						// This could be already tied to an existing subscription
						if (!dtCartItems.Rows [i]["user_asset_subscription_id"].Equals (DBNull.Value))
						{
							userAssetSubscriptionId = Convert.ToInt32 (dtCartItems.Rows [i]["user_asset_subscription_id"]);
							assetSubscriptionId = Convert.ToInt32 (dtCartItems.Rows [i]["asset_subscription_id"]);

							// Extend the end date of the current subscription
							DataRow drAssetSubscription = GetAssetSubscription (assetSubscriptionId);
							int lengthOfSubscription = Convert.ToInt32 (drAssetSubscription ["length_of_subscription"]);

							endDate = GetAssetSubscriptionEndDate (lengthOfSubscription, "end_datetime");

							sqlUpdate = "UPDATE user_asset_subscriptions " +
								" SET end_datetime = " + endDate +
								" WHERE user_asset_subscription_id = @userAssetSubscriptionId ";

							Hashtable parameters = new Hashtable ();
							parameters.Add ("@userAssetSubscriptionId", userAssetSubscriptionId);
							dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
						}
						else
						{
							// This is a new subscription
							userAssetSubscriptionId = InsertUserAssetSubscription (userId, Convert.ToInt32 (dtCartItems.Rows [i]["asset_id"]), orderId, drOrder, Convert.ToInt32 (dtCartItems.Rows [i]["asset_subscription_id"]), Convert.ToDouble (dtCartItems.Rows [i]["point_amount"]), (int) Constants.eUSER_ASSET_SUBSCRIPTION_STATUS.ACTIVE);
							UpdateOrderItemUserAssetSubscription (Convert.ToInt32 (dtCartItems.Rows [i]["order_item_id"]), userAssetSubscriptionId);
						}
                    }
                }

                // If it is a pilot game, add a day pilot subscription subscription
                if (!dtCartItems.Rows [i]["asset_id"].Equals (DBNull.Value) && StoreUtility.IsPilotGame (Convert.ToInt32 (dtCartItems.Rows [i]["asset_id"])))
                {
                    // Make sure they are not just featuring it
                    if (dtCartItems.Rows [i]["featured_asset_id"].Equals (DBNull.Value))
                    {
                        userAssetSubscriptionId = InsertUserPilotAssetSubscription (userId, Convert.ToInt32 (dtCartItems.Rows [i]["asset_id"]), Convert.ToDouble (dtCartItems.Rows [i]["point_amount"]), "N", (int) Constants.eUSER_ASSET_SUBSCRIPTION_STATUS.ACTIVE);
                        UpdateOrderItemUserAssetSubscription (Convert.ToInt32 (dtCartItems.Rows [i]["order_item_id"]), userAssetSubscriptionId);
                    }
                }

                // Are they purchasing a featured item?
                // If it is featured asset, mark it as active
                if (!dtCartItems.Rows [i]["featured_asset_id"].Equals (DBNull.Value))
                {
                    // Make it active
                    UpdateFeaturedAsset (Convert.ToInt32 (dtCartItems.Rows [i]["featured_asset_id"]), Constants.eFEATURED_ASSET_STATUS.ACTIVE);
                }

                // OLD CODE - No channel subscriptions for now
                //// If there are community subscriptions, update those as well
                //if (!dtCartItems.Rows [i]["community_member_subscription_id"].Equals (DBNull.Value)))
                //{
                //  userAssetSubscriptionId = StoreUtility.InsertCommunityMemberSubscription (userId, Convert.ToInt32 (dtCartItems.Rows [i]["asset_id"]), Convert.ToInt32 (dtCartItems.Rows [i]["asset_subscription_id"]), Convert.ToDouble (dtCartItems.Rows [i]["point_amount"]), "N", (int) Constants.eUSER_ASSET_SUBSCRIPTION_STATUS.ACTIVE);
                //  UpdateOrderItemUserAssetSubscription (Convert.ToInt32 (dtCartItems.Rows [i]["order_item_id"]), userAssetSubscriptionId);
                //}
                //// Channel Subscription purchase?
                //if (!dtOrderItems.Rows [i]["community_member_subscription_id"].Equals (DBNull.Value))
                //{
                //  int commMemSubscriptionId = Convert.ToInt32 ( dtOrderItems.Rows [i]["community_member_subscription_id"]);
                //  StoreUtility.UpdateCommunityMemberSubscription (userId, commMemSubscriptionId, orderId,(int) Constants.eCOMMUNITY_MEMBER_SUBSCRIPTION_STATUS.COMPLETED, true);
                //
                //  // Make them a validated member of the community
                //  DataRow drCommMemSub = StoreUtility.GetCommunityMemberSubscriptionById (commMemSubscriptionId);
                //  CommunityUtility.UpdateCommunityMember (Convert.ToInt32 ( drCommMemSub["community_id"]), userId, (int) Constants.eCOMMUNITY_MEMBER_STATUS.ACTIVE);
                //}
            }

            return 0;
        }

        /// <summary>
        /// UpdateOrderItemUserAssetSubscription
        /// </summary>
        /// <param name="orderItemId"></param>
        /// <param name="userAssetSubscriptionId"></param>
        /// <returns></returns>
        public static int UpdateOrderItemUserAssetSubscription (int orderItemId, int userAssetSubscriptionId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            // Set the status to completed
            string sqlUpdate = "UPDATE order_items " +
                " set user_asset_subscription_id = @userAssetSubscriptionId " +
                " WHERE " +
                " order_item_id = @orderItemId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userAssetSubscriptionId", userAssetSubscriptionId);
            parameters.Add ("@orderItemId", orderItemId);
            return dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// Purchase an order now and deduct user balances
        /// </summary>
        public static int PurchaseOrderNow (int userId, int orderId, string ipAddress)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            // Look up order total
            DataRow drOrder = GetOrder (orderId);
            Double dOrderAmount = Convert.ToDouble (drOrder ["gross_point_amount"]);

            // Do a double check here to make sure they have enough points?
            Double userPointTotal = UsersUtility.GetUserPointTotal (userId);
            if (userPointTotal < dOrderAmount)
            {
                // **** Not enough points to purchase!!!!, Fail the order
                FailOrder (orderId, userId, "Not enough Points to complete purchase.");
                return -1;
            }

            // Insert the subscriptions for an order
            PuchaseOrderSubscriptions (userId, orderId, drOrder);

            // Set the status to completed
			UpdateOrderToComplete (orderId, userId);

            // Deduct user balances
            DeductOrderPointBalances (userId, dOrderAmount, orderId);

            // Credit Royalties
            CreditRoyalties (orderId, drOrder);
            return orderId;
        }

		/// <summary>
		/// UpdateOrderToComplete
		/// </summary>
		public static int UpdateOrderToComplete (int orderId, int userId)
		{
			DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

			string sqlUpdate = "UPDATE orders " +
				" set transaction_status_id = " + (int) Constants.eORDER_STATUS.COMPLETED + "," +
				" purchase_date = " + dbUtility.GetCurrentDateFunction () +
				" WHERE " +
				" order_id = @orderId " +
				" AND user_id = @userId";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@orderId", orderId);
			parameters.Add ("@userId", userId);
			return dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
		}

        /// <summary>
        /// UpdateOrderToPending
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public static int UpdateOrderToPending (int orderId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            // Set the status to completed
            string sqlUpdate = "UPDATE orders " +
                " set transaction_status_id = " + (int) Constants.eORDER_STATUS.PENDING_PURCHASE +
                " WHERE " +
                " order_id = @orderId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@orderId", orderId);
            return dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

		/// <summary>
		/// UpdateOrderPurchaseDate
		/// </summary>
		public static int UpdateOrderPurchaseDate (int orderId)
		{
			DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

			// Set the status to completed
			string sqlUpdate = "UPDATE orders " +
				" set purchase_date = " + dbUtility.GetCurrentDateFunction () +
				" WHERE " +
				" order_id = @orderId";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@orderId", orderId);
			return dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
		}

        /// <summary>
        /// UpdateOrderToPending
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public static int UpdateFreeOrder (int orderId, int assetId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlUpdate = "UPDATE orders " +
                // Set the status to completed
                " set purchase_date = " + dbUtility.GetCurrentDateFunction () +
				" , purchase_type = " + (int) Constants.ePURCHASE_TYPE.ASSET +
                " WHERE " +
                " order_id = @orderId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@orderId", orderId);
            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);

			// Set the status to completed
			sqlUpdate = "UPDATE orders " +
				" set transaction_status_id = " + (int) Constants.eORDER_STATUS.COMPLETED + "," +
				" purchase_date = " + dbUtility.GetCurrentDateFunction () +
				" , download_end_date = " + dbUtility.GetDatePlusDays (KanevaGlobals.NumberOfDaysDownloadIsAvailable) +
				" , download_stop_date = " + dbUtility.GetDatePlusDays (KanevaGlobals.NumberOfDaysDownloadStopDate) +
				" WHERE " +
				" order_id = @orderId ";

			parameters = new Hashtable ();
			parameters.Add ("@orderId", orderId);
			//parameters.Add ("@userId", userId);
			return dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// UpdateOrderPointTranasactionId
        /// </summary>
        /// <param name="purchasePointTransactionId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public static int UpdateOrderPointTranasactionId (int orderId, int purchasePointTransactionId)
        {
            string sPPTId = (purchasePointTransactionId == 0)? "NULL": purchasePointTransactionId.ToString ();

            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string sqlString = "UPDATE orders SET " +
                " point_transaction_id = " + sPPTId +
                " WHERE order_id = @orderId";

            parameters.Add ("@orderId", orderId);
            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// Fail the order
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="userId"></param>
        /// <param name="reason"></param>
        public static int FailOrder (int orderId, int userId, string errorDescription)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            // Set the status to completed
            string sqlUpdate = "UPDATE orders " +
                " set transaction_status_id = " + (int) Constants.eORDER_STATUS.FAILED + "," +
                " error_description = @errorDescription " +
                " WHERE " +
                " order_id = @orderId " +
                " AND user_id = @userId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@orderId", orderId);
            parameters.Add ("@userId", userId);
            parameters.Add ("@errorDescription", errorDescription);
            return dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// Credit user royalties
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        private static int CreditRoyalties (int orderId, DataRow drOrder)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            string sqlUpdate = "";

            int purchaseType = Convert.ToInt32 (drOrder ["purchase_type"]);

            // Only credit royalties for asset purchases
            if (purchaseType.Equals ((int) Constants.ePURCHASE_TYPE.ASSET) || purchaseType.Equals ((int) Constants.ePURCHASE_TYPE.GAME_SUBSCRIPTION))
            {
                PagedDataTable dtCartItems = GetOrderItems (orderId, "a.name");

                for (int i = 0; i < dtCartItems.Rows.Count; i ++)
                {
                    // If user found
                    if (!dtCartItems.Rows [i]["royalty_user_id_paid"].Equals (DBNull.Value) )
                    {
                        int userIdToPay = Convert.ToInt32 (dtCartItems.Rows [i]["royalty_user_id_paid"]);

                        // If valid user and not credited or paid yet
                        if (userIdToPay > 0 && dtCartItems.Rows [i]["royalty_paid"].ToString ().Equals ("N"))
                        {
                            // Pay the royalty (in dollars)
                            Double dDollarRoyaltyAmount = ConvertKPointsToDollars (Convert.ToDouble (dtCartItems.Rows [i]["royalty_amount"]));
                            //UsersUtility.AddToUserBalance (userIdToPay, Constants.CURR_DOLLAR, dDollarRoyaltyAmount);
							UsersUtility.AdjustUserBalance (userIdToPay, Constants.CURR_DOLLAR, dDollarRoyaltyAmount, Constants.CASH_TT_ROYALITY);

                            // Update status to credited
                            sqlUpdate = "UPDATE order_items " +
                                " SET royalty_paid = @royaltyPaid " +
                                " WHERE " +
                                " order_id = @orderId";

                            Hashtable parameters = new Hashtable ();
                            parameters.Add ("@orderId", orderId);
                            parameters.Add ("@royaltyPaid", Constants.ROYALTY_CREDITED);
                            dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
                        }
                    }
                }
            }

            return 0;
        }

        /// <summary>
        /// DeductOrderPointBalances
        /// </summary>
        private static int DeductOrderPointBalances (int userId, Double dOrderAmount, int orderId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            Double dUserMpointBalance = UsersUtility.getUserBalance (userId, Constants.CURR_MPOINT);

            // Are they purchasing an asset or community subscription?
            string sqlInsertTransactionAmounts = "INSERT INTO order_transaction_amounts_paid " +
                " (order_id, kei_point_id, amount) " +
                " VALUES ";

            // Deduct Point balances (Use MPoint balances first, since they have the possibility to expire)
            if (dUserMpointBalance > 0)
            {
                // Will MPoints cover everything?
                if (dOrderAmount > dUserMpointBalance)
                {
                    // Use up all MPoints, the rest to KPoints
                    Double dDiffernce = dOrderAmount - dUserMpointBalance;
                    //UsersUtility.DeductFromUserBalance (userId, Constants.CURR_MPOINT, dUserMpointBalance);
                    //UsersUtility.DeductFromUserBalance (userId, Constants.CURR_KPOINT, dDiffernce);

					UsersUtility.AdjustUserBalance (userId, Constants.CURR_MPOINT, -dUserMpointBalance, Constants.CASH_TT_BOUGHT_ITEM);
					UsersUtility.AdjustUserBalance (userId, Constants.CURR_KPOINT, -dDiffernce, Constants.CASH_TT_BOUGHT_ITEM);

                    // Insert the order transaction amounts
                    parameters.Add ("@orderId", orderId);
                    parameters.Add ("@dUserMpointBalance", dUserMpointBalance);
                    dbUtility.ExecuteNonQuery (sqlInsertTransactionAmounts + "(@orderId,'" + Constants.CURR_MPOINT + "', @dUserMpointBalance)", parameters);

                    // Reset params
                    parameters = new Hashtable ();
                    parameters.Add ("@orderId", orderId);
                    parameters.Add ("@dDiffernce", dDiffernce);
                    dbUtility.ExecuteNonQuery (sqlInsertTransactionAmounts + "(@orderId,'" + Constants.CURR_KPOINT + "', @dDiffernce)", parameters);
                }
                else
                {
                    // We have enough Mpoints to cover it all
                    //UsersUtility.DeductFromUserBalance (userId, Constants.CURR_MPOINT, dOrderAmount);

					UsersUtility.AdjustUserBalance (userId, Constants.CURR_MPOINT, -dOrderAmount, Constants.CASH_TT_BOUGHT_ITEM);

                    // Insert the order transaction amounts
                    parameters.Add ("@orderId", orderId);
                    parameters.Add ("@dOrderAmount", dOrderAmount);
                    dbUtility.ExecuteNonQuery (sqlInsertTransactionAmounts + "(@orderId,'" + Constants.CURR_MPOINT + "', @dOrderAmount)", parameters);
                }
            }
            else
            {
                // Use all K-Points
                //UsersUtility.DeductFromUserBalance (userId, Constants.CURR_KPOINT, dOrderAmount);

				UsersUtility.AdjustUserBalance (userId, Constants.CURR_KPOINT, -dOrderAmount, Constants.CASH_TT_BOUGHT_ITEM);

                // Insert the order transaction amounts
                parameters.Add ("@orderId", orderId);
                parameters.Add ("@dOrderAmount", dOrderAmount);
                dbUtility.ExecuteNonQuery (sqlInsertTransactionAmounts + "(@orderId,'" + Constants.CURR_KPOINT + "', @dOrderAmount)", parameters);
            }

            return 0;
        }

        /// <summary>
        /// Get Point Amount of Gift Card Redeem
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="userId"></param>
        /// <param name="reason"></param>
        public static int GetLastRedeemedGiftCardCreditAmount (int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            // Set the status to completed
            string sqlSelect = "SELECT kei_point_amount " +
                " FROM gift_cards gc " +
                " INNER JOIN purchase_point_transactions ppt ON ppt.gift_card_id = gc.gift_card_id " +
                " INNER JOIN orders o ON o.point_transaction_id = ppt.point_transaction_id " +
                " WHERE o.purchase_type = " + (int) Constants.ePURCHASE_TYPE.GIFT_CARD +
                " AND o.user_id = @userId " +
                " ORDER BY o.order_id DESC LIMIT 1";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            DataRow drResult = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);

            if (drResult == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32 (drResult["kei_point_amount"]);
            }
        }



        public static int AddOrderItemsToUserInventory (int orderId, int pointBucketId, int userId, string userGender)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            // If this is an access pass purchase, then make the appropriate updates so
            // WOK will know this user has an access pass
            int passGroupId = 0;
            int ret = 0;

            // ACCESS PASS
            try
            {
                DataRow drPointBucket = StoreUtility.GetPromotion (pointBucketId);

                if (drPointBucket != null)
                {
                    passGroupId = Convert.ToInt32 (drPointBucket["wok_pass_group_id"]);

                    // Only add user to pass group if the point bucket  
                    // purchased is associated with a WOK pass group
                    if (passGroupId > 0)
                    {
                        ret = UsersUtility.AddUserToPassGroup (userId, passGroupId);
                    }

                    if (ret > 0)
                    {
                        m_logger.Error ("Error adding user to wok pass group: ");
                        m_logger.Error ("orderId=" + orderId.ToString () + " : userId=" + userId.ToString () + " : pass_group_id=" + passGroupId.ToString ());
                    }

                }
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error adding user to wok pass group: ", exc);
                m_logger.Error ("orderId=" + orderId.ToString () + " : userId=" + userId.ToString () + " : pass_group_id=" + passGroupId.ToString ());
            }

            int globalId = 0;
            int qty = 0;

            // WOK ITEMS
            try
            {
                // If user gets free gifts then send the gifts
                DataTable dtFreeItems = StoreUtility.GetFreeWokItems (pointBucketId);

                for (int i = 0; i < dtFreeItems.Rows.Count; i++)
                {
                    string oppositeGender = userGender == Constants.GENDER_FEMALE ? Constants.GENDER_MALE : Constants.GENDER_FEMALE;
                    if (dtFreeItems.Rows[i]["gender"].ToString () != oppositeGender)
                    {

                        globalId = Convert.ToInt32 (dtFreeItems.Rows[i]["wok_item_id"]);
                        qty = Convert.ToInt32 (dtFreeItems.Rows[i]["quantity"]);

                        UsersUtility.AddItemToUserInventory (userId, Constants.WOK_INVENTORY_TYPE_PERSONAL, globalId, qty);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error adding item to users inventory: ", exc);
                m_logger.Error ("orderId=" + orderId.ToString () + " : userId=" + userId.ToString () + " : globalId=" + globalId.ToString () + " : qty=" + qty.ToString ());
            }

            return ret;						 
        }



		#endregion

        // **********************************************************************************************
        // Searching
        // **********************************************************************************************
        #region Search Functions

//		/// <summary>
//		/// Search assets
//		/// </summary>
//		public static PagedDataTable SearchAssets (bool bGetMature, int assetTypeId, int userId, bool bGetPrivate, bool bThumbnailRequired, string searchString,
//			string categories, int pastDays, string country, string zipCode, int zipMiles,string filter, string orderBy, int pageNumber, int pageSize)
//		{
//			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly ();
//			Hashtable parameters = new Hashtable ();
//
//			// Select List
//			string sqlSelectList = "SELECT a.asset_id, a.owner_id, a.owner_username, a.asset_type_id, a.name, " +
//				" a.teaser, a.asset_rating_id, a.status_id, a.created_date, at.name as asset_type_name, a.run_time_seconds, " +
//				" a.file_size, a.amount, " +
//				" a.publish_status_id, a.keywords, a.content_extension, " +
//				" a.thumbnail_small_path, a.thumbnail_medium_path, a.thumbnail_gen, " +
//				" owner_com.name_no_spaces as owner_name_no_spaces, owner_com.thumbnail_small_path as owner_thumbnail_small_path, " +
//				" ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs, 0 as digg_id ";
//
//			// Table List
//			string sqlTableList = " communities_personal owner_com ";
//			if (bGetMature)
//			{
//				sqlTableList += " INNER JOIN vw_published_public_mature_assets a ON owner_com.creator_id = a.owner_id ";
//			}
//			else
//			{
//				sqlTableList += " INNER JOIN vw_published_public_assets a ON owner_com.creator_id = a.owner_id ";
//			}
//
//			sqlTableList += " INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id " +
//				" ,asset_types at ";
//
//			// Where Clause
//			string sqlWhereClause = "";
//
//			if (searchString.Trim ().Length > 0)
//			{
//				sqlWhereClause = " MATCH (a.name,a.teaser, a.body_text, a.owner_username, a.short_description,a.keywords) AGAINST (@searchString) ";
//				parameters.Add ("@searchString", searchString);
//				sqlWhereClause = sqlWhereClause + " AND ";
//			}
//
//			sqlWhereClause += " at.asset_type_id = a.asset_type_id ";
//
//			if (assetTypeId > 0)
//			{
//				parameters.Add ("@assetTypeId", assetTypeId);
//				sqlWhereClause += " AND a.asset_type_id = @assetTypeId";
//			}
//
//			if (bThumbnailRequired && !assetTypeId.Equals ((int) Constants.eASSET_TYPE.PICTURE))
//			{
//				sqlWhereClause += " AND a.image_path IS NOT NULL ";
//			}
//
//			if (categories.Trim ().Length > 0)
//			{
//				sqlWhereClause += " AND (a.category1_id IN (" + categories + ")" +
//					" OR a.category2_id IN (" + categories + ")" +
//					" OR a.category3_id IN (" + categories + "))";
//			}
//
//			// Distance or country
//			if (country.Length > 0)
//			{
//				// Country
//				sqlTableList += " ,users u ";
//				sqlWhereClause += " AND a.owner_id = u.user_id " +
//					" AND u.country = @country ";
//
//				parameters.Add ("@country", country);
//
//				// Zip
//				if (zipMiles > 0 && zipCode.Length > 0)
//				{
//					string selectZip = " SELECT latitude, longitude " +
//						" FROM zip_codes " +
//						" WHERE zip_code = @zipCode ";
//					Hashtable zipParams = new Hashtable ();
//					zipParams.Add ("@zipCode", zipCode);
//					DataRow drZip = dbUtility.GetDataRow (selectZip, zipParams, false);
//
//					if (drZip == null)
//					{
//						sqlWhereClause += " AND u.zip_code = @zipCode ";
//						parameters.Add ("@zipCode", zipCode);
//					}
//					else
//					{
//						double upperLat = 0.0, upperLong = 0.0, lowerLat = 0.0, lowerLong = 0.0;
//						GetCoordinates (zipMiles, Convert.ToDouble (drZip ["latitude"]), Convert.ToDouble (drZip ["longitude"]), ref upperLat, ref upperLong, ref lowerLat, ref lowerLong);
//
//						sqlWhereClause += " AND u.zip_code IN " +
//							" (SELECT zip_code from zip_codes WHERE " +
//								" latitude > @lowerLat " +
//								" AND latitude < @upperLat " +
//								" AND longitude > @lowerLong " +
//								" AND longitude < @upperLong )";
//						parameters.Add ("@lowerLat", lowerLat);
//						parameters.Add ("@upperLat", upperLat);
//						parameters.Add ("@lowerLong", lowerLong);
//						parameters.Add ("@upperLong", upperLong);
//					}
//				}
//			}
//
//			// Only get last x number of days?
//			if (pastDays > 0)
//			{
//				sqlWhereClause += " AND a.created_date > " + dbUtility.GetDatePlusDays (-pastDays);
//			}
//
//			// Filter
//			if (filter.Length > 0)
//			{
//				sqlWhereClause += " AND " + filter;
//			}
//
//			sqlSelectList +=  " FROM " + sqlTableList + (sqlWhereClause.Length > 0 ? " WHERE " + sqlWhereClause:"");
//			return dbUtility.GetPagedDataTableUnion (sqlSelectList, orderBy, parameters, pageNumber, pageSize);
//		}

		/// <summary>
		/// Search assets
		/// </summary>
		public static PagedDataTable SearchAssetsNew (bool bGetMature, int assetTypeId, int userId, bool bGetPrivate, bool bThumbnailRequired, string searchString,
			string categories, int pastDays, string country, string zipCode, int zipMiles, string filter, string orderBy, int pageNumber, int pageSize)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilitySearch ();
			Hashtable parameters = new Hashtable ();

			// Select List
			string sqlSelectList = "SELECT sar.asset_id, sar.owner_id, sar.owner_username, sar.asset_type_id, sar.name, " +
				" sar.teaser, asset_rating_id, status_id, created_date, created_date as dt_created, asset_type_name, run_time_seconds, " +
				" file_size, amount, publish_status_id, sar.keywords, content_extension, " +
				" thumbnail_small_path, thumbnail_medium_path, thumbnail_gen, " +
				" name_no_spaces as owner_name_no_spaces, owner_thumbnail_small_path, " +
				" sa.views as number_of_downloads, sa.raves as number_of_diggs ";

			// Table List
			string sqlTableList = " searchable_assets sa INNER JOIN searchable_assets_results sar ON sa.asset_id = sar.asset_id ";

			// Where Clause
			string sqlWhereClause = " sa.asset_type_id = @assetTypeId ";
			parameters.Add ("@assetTypeId", assetTypeId);

			// Mature
			if (!bGetMature)
			{
				sqlWhereClause += " AND sa.mature = 'N' ";
			}

			// Search
			if (searchString.Trim ().Length > 0)
			{
				char [] splitter  = {' '};
				string [] arKeywords = null;

				// Did they enter multiples?
				arKeywords = searchString.Split (splitter);

				if (arKeywords.Length > 1)
				{
					searchString = "";
					for (int j = 0; j < arKeywords.Length; j ++)
					{
							searchString += arKeywords [j].ToString () + "* ";
					}
				}
				else
				{
					searchString = searchString + "*";
				}

				sqlWhereClause += " AND MATCH (sa.name, sa.teaser, sa.body_text, sa.owner_username, sa.short_description, sa.keywords) AGAINST (@searchString IN BOOLEAN MODE) ";
				parameters.Add ("@searchString", searchString);

				// If no order by, do relevancy
				if (orderBy.Length.Equals (0))
				{
					parameters.Add ("@searchStringOrig", searchString);
					sqlSelectList += ", MATCH (sa.name, sa.teaser, sa.body_text, sa.owner_username, sa.short_description, sa.keywords) AGAINST (@searchStringOrig) as rel";
					orderBy = " rel DESC ";
				}
			}

			if (bThumbnailRequired && !assetTypeId.Equals ((int) Constants.eASSET_TYPE.PICTURE) && !assetTypeId.Equals ((int) Constants.eASSET_TYPE.PATTERN))
			{
				sqlWhereClause += " AND sa.has_image = 'Y' ";
			}

			if (categories.Trim ().Length > 0)
			{
				sqlWhereClause += " AND (sa.category1_id IN (" + categories + ")" +
					" OR sa.category2_id IN (" + categories + ")" +
					" OR sa.category3_id IN (" + categories + "))";
			}

			// Distance or country
			if (country.Length > 0)
			{
				// Country
				sqlWhereClause += " AND sa.country = @country ";
				parameters.Add ("@country", country);

				// Zip
				if (zipMiles > 0 && zipCode.Length > 0)
				{
					string selectZip = " SELECT latitude, longitude " +
						" FROM zip_codes " +
						" WHERE zip_code = @zipCode ";
					Hashtable zipParams = new Hashtable ();
					zipParams.Add ("@zipCode", zipCode);
					DataRow drZip = dbUtility.GetDataRow (selectZip, zipParams, false);

					if (drZip == null)
					{
						sqlWhereClause += " AND sa.zip_code = @zipCode ";
						parameters.Add ("@zipCode", zipCode);
					}
					else
					{
						double upperLat = 0.0, upperLong = 0.0, lowerLat = 0.0, lowerLong = 0.0;
						GetCoordinates (zipMiles, Convert.ToDouble (drZip ["latitude"]), Convert.ToDouble (drZip ["longitude"]), ref upperLat, ref upperLong, ref lowerLat, ref lowerLong);

						sqlWhereClause += " AND sa.zip_code IN " +
							" (SELECT zip_code from zip_codes WHERE " +
							" latitude > @lowerLat " +
							" AND latitude < @upperLat " +
							" AND longitude > @lowerLong " +
							" AND longitude < @upperLong )";
						parameters.Add ("@lowerLat", lowerLat);
						parameters.Add ("@upperLat", upperLat);
						parameters.Add ("@lowerLong", lowerLong);
						parameters.Add ("@upperLong", upperLong);
					}
				}
			}

			// Only get last x number of days?
			if (pastDays > 0)
			{
				sqlWhereClause += " AND sa.dt_created > " + dbUtility.GetDatePlusDays (-pastDays);
			}

			// Filter
			if (filter.Length > 0)
			{
				sqlWhereClause += " AND " + filter;
			}

			sqlSelectList +=  " FROM " + sqlTableList + (sqlWhereClause.Length > 0 ? " WHERE " + sqlWhereClause:"");
			return dbUtility.GetPagedDataTableUnion (sqlSelectList, orderBy, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// get a list of channels that have user's media
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static DataTable GetSharingChannelsByUserId(int userId)
		{
			StringBuilder sb = new StringBuilder() ;
			sb.Append(" SELECT DISTINCT c.name, c.community_id ");
			sb.Append(" FROM asset_channels ac ");
			sb.Append(" INNER JOIN assets a ON a.asset_id = ac.asset_id ");
			sb.Append(" INNER JOIN communities_public c ON c.community_id = ac.channel_id ");
			sb.Append(" WHERE a.owner_id = @userId ");
			sb.Append (SQLCommon_GetOnlyActiveStatusAssetsSQL ());
			sb.Append(" ORDER BY c.name");

			Hashtable parameters = new Hashtable() ;
			parameters.Add("@userId", userId);

			return KanevaGlobals.GetDatabaseUtility().GetDataTable(sb.ToString(), parameters);
		}

		#endregion

        // **********************************************************************************************
        // Subscriptions/Licenses
        // **********************************************************************************************
        #region Subscriptions/Licenses

        /// <summary>
        /// GetCommunityMemberSubscriptionById
        /// </summary>
        /// <param name="communityMemberSubscriptionId"></param>
        /// <returns></returns>
        public static DataRow GetCommunityMemberSubscriptionById (int communityMemberSubscriptionId)
        {
            string sqlSelect = "SELECT community_member_subscription_id, community_id, user_id, community_subscription_id, " +
                " start_datetime, end_datetime,  gross_point_amount, purchase_date, status_id, refund_amount " +
                " FROM community_member_subscriptions" +
                " WHERE community_member_subscription_id = @cmsId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@cmsId", communityMemberSubscriptionId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// GetUserAssetSubscripsion
        /// </summary>
        /// <returns></returns>
        public static DataRow GetUserAssetSubscription (int userAssetSubscriptionId)
        {
            string sqlSelect = "SELECT user_asset_subscription_id, user_id, asset_subscription_id, " +
                " asset_id, start_datetime, end_datetime, gross_point_amount, renewable, " +
                " purchase_date, status_id, refunded_amount " +
                " FROM user_asset_subscriptions " +
                " WHERE user_asset_subscription_id = @uasId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@uasId", userAssetSubscriptionId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// GetLicenseSubscriptionEndDate
        /// </summary>
        /// <param name="Length"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        private static string GetLicenseSubscriptionEndDate (int Length, int quantity)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            return GetLicenseSubscriptionEndDate (Length, quantity, dbUtility.GetCurrentDateFunction ());
        }

        /// <summary>
        /// GetLicenseSubscriptionEndDate
        /// </summary>
        /// <param name="Length"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        private static string GetLicenseSubscriptionEndDate (int Length, int quantity, string column)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            switch (Length)
            {
                case (int) Constants.eLICENSE_SUBSCRIPTION_LENGTH.INFINITY:
                {
                    return "NULL";
                }
                case (int) Constants.eLICENSE_SUBSCRIPTION_LENGTH.MONTHLY:
                {
                    return dbUtility.GetDatePlusMonths (quantity, column);
                }
                case (int) Constants.eLICENSE_SUBSCRIPTION_LENGTH.YEARLY:
                {
                    return dbUtility.GetDatePlusYears (quantity, column);
                }
                case (int) Constants.eLICENSE_SUBSCRIPTION_LENGTH.DAILY:
                {
                    return dbUtility.GetDatePlusDays (quantity, column);
                }
                default:
                {
                    return dbUtility.GetCurrentDateFunction ();
                }
            }
        }

        /// <summary>
        /// GetPayPalLenthCodes
        /// </summary>
        /// <param name="Length"></param>
        /// <returns></returns>
        public static string GetPayPalLengthCodes (int Length)
        {
            switch (Length)
            {
                case (int) Constants.eLICENSE_SUBSCRIPTION_LENGTH.INFINITY:
                {
                    return "M";
                }
                case (int) Constants.eLICENSE_SUBSCRIPTION_LENGTH.MONTHLY:
                {
                    return "M";
                }
                case (int) Constants.eLICENSE_SUBSCRIPTION_LENGTH.YEARLY:
                {
                    return "Y";
                }
                case (int) Constants.eLICENSE_SUBSCRIPTION_LENGTH.DAILY:
                {
                    return "D";
                }
                default:
                {
                    return "M";
                }
            }
        }

        /// <summary>
        /// InsertUserLicenseSubscription
        /// </summary>
        /// <returns></returns>
        public static int InsertUserLicenseSubscription (int userId, int licenseSubscriptionId, string renewable, int statusId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            // Look up the subscription to get the end date
            DataRow drLicenseSubscription = GetLicenseSubscription (licenseSubscriptionId);

            string endDate = GetLicenseSubscriptionEndDate (Convert.ToInt32 (drLicenseSubscription ["length_of_subscription"]), 1);

            // Record User License Subscriptions
            string sqlInsert = "INSERT INTO user_license_subscription " +
                " (user_id, license_subscription_id, " +
                " start_date, end_date, renewable, purchase_date, " +
                " status_id) " +
                " VALUES " +
                " (@userId, @lsId, " +
                dbUtility.GetCurrentDateFunction () + "," + endDate + ", @renewable, " + dbUtility.GetCurrentDateFunction () + "," +
                statusId + ")";

            int userLicenseSubscriptionId = 0;
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@lsId", licenseSubscriptionId);
            parameters.Add ("@renewable", renewable);
            dbUtility.ExecuteIdentityInsert (sqlInsert, parameters, ref userLicenseSubscriptionId);

            return userLicenseSubscriptionId;
        }

        /// <summary>
        /// Update an community Subscription
        /// </summary>
        public static int UpdateUserLicenseSubscription (int userLicenseSubscriptionId, int statusId, bool updatePurchaseDate)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            string sqlMore = "";

            if (updatePurchaseDate)
            {
                sqlMore += " , purchase_date = " + dbUtility.GetCurrentDateFunction ();
            }

            string sqlInsert = "UPDATE user_license_subscription " +
                " SET status_id = @statusId " +
                sqlMore +
                " WHERE user_license_subscription_id = @ulsId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@ulsId", userLicenseSubscriptionId);
            parameters.Add ("@statusId", statusId);
            return dbUtility.ExecuteNonQuery (sqlInsert, parameters);
        }

        /// <summary>
        /// Update a Subscription, mark it as cancelled
        /// </summary>
        public static int UpdateUserLicenseSubscriptionMarkAsCancelled (int userLicenseSubscriptionId, string userCancelled)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlInsert = "UPDATE user_license_subscription " +
                " SET user_cancelled = @userCancelled" +
                " WHERE user_license_subscription_id = @ulsId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@ulsId", userLicenseSubscriptionId);
            parameters.Add ("@userCancelled", userCancelled);
            return dbUtility.ExecuteNonQuery (sqlInsert, parameters);
        }

        /// <summary>
        /// Update a Subscription, set the paypal subscription id
        /// </summary>
        public static int UpdateUserLicenseSubscriptionSetSubscriptionId (int userLicenseSubscriptionId, string paypalSubscriptionId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlInsert = "UPDATE user_license_subscription " +
                " SET subscription_id = @paypalSubscriptionId" +
                " WHERE user_license_subscription_id = @ulsId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@ulsId", userLicenseSubscriptionId);
            parameters.Add ("@paypalSubscriptionId", paypalSubscriptionId);
            return dbUtility.ExecuteNonQuery (sqlInsert, parameters);
        }

        /// <summary>
        /// Update an community Subscription, adds a new month (or day, etc)
        /// </summary>
        public static int UpdateUserLicenseSubscriptionAddPeriod (int userLicenseSubscriptionId)
        {
            // Get the length of the subscription
            DataRow drULS = GetUserLicenseSubscription (userLicenseSubscriptionId);
            int length = Convert.ToInt32 (drULS ["length_of_subscription"]);

            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            string endDate = GetLicenseSubscriptionEndDate (length, 1, "end_date");

            string sqlInsert = "UPDATE user_license_subscription " +
                " SET end_date = " + endDate +
                " WHERE user_license_subscription_id = @ulsId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@ulsId", userLicenseSubscriptionId);
            return dbUtility.ExecuteNonQuery (sqlInsert, parameters);
        }

        /// <summary>
        /// GetUserLicenseSubscription
        /// </summary>
        /// <param name="userLicenseSubscriptionId"></param>
        /// <returns></returns>
        private static DataRow GetUserLicenseSubscription (int userLicenseSubscriptionId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "SELECT uls.user_license_subscription_id, uls.license_subscription_id, uls.user_id, " +
                " uls.start_date, uls.end_date, uls.renewable, uls.status_id, ls.length_of_subscription " +
                " FROM user_license_subscription uls, license_subscription ls " +
                " WHERE uls.license_subscription_id = ls.license_subscription_id " +
                " AND uls.user_license_subscription_id = @userLicenseSubscriptionId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userLicenseSubscriptionId", userLicenseSubscriptionId);
            return dbUtility.GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// InsertFeaturedAsset
        /// </summary>
        /// <returns></returns>
        public static int InsertFeaturedAsset (int userId, int assetId, int numberOfMonths, Constants.eFEATURED_ASSET_STATUS status)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            string endDate = dbUtility.GetDatePlusMonths (numberOfMonths);

            // Record User Asset Subscriptions
            string sqlInsert = "INSERT INTO featured_assets " +
                " (user_id, asset_id, " +
                " start_datetime, end_datetime, shown_count, " +
                " status_id) " +
                " VALUES " +
                " (@userId, @assetId, " +
                dbUtility.GetCurrentDateFunction () + "," + endDate + ", 0," +
                " @statusId)";

            int featuredAssetId = 0;
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@statusId", (int) status);
            dbUtility.ExecuteIdentityInsert (sqlInsert, parameters, ref featuredAssetId);
            return featuredAssetId;
        }

        /// <summary>
        /// UpdateFeaturedAsset
        /// </summary>
        /// <param name="featuredAssetId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static int UpdateFeaturedAsset (int featuredAssetId, Constants.eFEATURED_ASSET_STATUS status)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlUpdate = "UPDATE featured_assets " +
                " SET status_id = @statusId " +
                " WHERE featured_asset_id = @faId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@statusId", (int) status);
            parameters.Add ("@faId",  featuredAssetId);
            return dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
        }

        /// <summary>
        /// GetAssetSubscriptionEndDate
        /// </summary>
        private static string GetAssetSubscriptionEndDate (int lengthOfSubscription)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            return GetAssetSubscriptionEndDate (lengthOfSubscription, dbUtility.GetCurrentDateFunction ());
        }

        /// <summary>
        /// GetAssetSubscriptionEndDate
        /// </summary>
        private static string GetAssetSubscriptionEndDate (int lengthOfSubscription, string column)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            string endDate = "";

            switch (lengthOfSubscription)
            {
                case (int) Constants.eASSET_SUBSCRIPTION_LENGTH.DAILY:
                {
                    endDate = dbUtility.GetDatePlusDays (1, column);
                    break;
                }
                case (int) Constants.eASSET_SUBSCRIPTION_LENGTH.MONTHLY:
                {
                    endDate = dbUtility.GetDatePlusMonths (1, column);
                    break;
                }
                case (int) Constants.eASSET_SUBSCRIPTION_LENGTH.SIXMONTHLY:
                {
                    endDate = dbUtility.GetDatePlusMonths (6, column);
                    break;
                }
                case (int) Constants.eASSET_SUBSCRIPTION_LENGTH.YEARLY:
                {
                    endDate = dbUtility.GetDatePlusYears (1, column);
                    break;
                }
                default:
                {
                    endDate = dbUtility.GetCurrentDateFunction ();
                    break;
                }
            }

            return endDate;
        }

        /// <summary>
        /// insert a user asset subscription
        /// </summary>
        private static int InsertUserAssetSubscription (int userId, int assetId, int orderId, DataRow drOrder, int assetSubscriptionId, Double dSubscriptionAmount, int userAssetSubscriptionStatus)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();
            int userAssetSubscriptionId = 0;
            string endDate = "";

            // Look up the subscription to get the end date
            DataRow drAssetSubscription = GetAssetSubscription (assetSubscriptionId);

            int lengthOfSubscription = Convert.ToInt32 (drAssetSubscription ["length_of_subscription"]);

            // Monthly is renewable
            bool IsRenewable = lengthOfSubscription.Equals ((int) Constants.eASSET_SUBSCRIPTION_LENGTH.MONTHLY);

            // If they already have a  subscription on this item? We are just extending the end_date here!!!!
            //DataRow drActiveSub = GetActiveUserAssetSubscription (assetId, userId);

//            if (!drAssetSub ["user_asset_subscription_id"].Equals (DBNull.Value))
//            {
//                userAssetSubscriptionId = Convert.ToInt32 (drActiveSub ["user_asset_subscription_id"]);
//
//                // Based of current month end
//                endDate = GetAssetSubscriptionEndDate (lengthOfSubscription, "end_datetime");
//
//                string sqlUpdate = "UPDATE user_asset_subscriptions " +
//                    " SET end_datetime = " + endDate +
//                    " WHERE user_asset_subscription_id = @userAssetSubscriptionId ";
//
//                parameters = new Hashtable ();
//                parameters.Add ("@userAssetSubscriptionId", userAssetSubscriptionId);
//                dbUtility.ExecuteNonQuery (sqlUpdate, parameters);
//            }
//            else
//            {
                // Based of today's date
                endDate = GetAssetSubscriptionEndDate (lengthOfSubscription);

                // Record User Asset Subscriptions
                string sqlInsert = "INSERT INTO user_asset_subscriptions " +
                    " (user_id, asset_subscription_id, asset_id, " +
                    " start_datetime, end_datetime, gross_point_amount, renewable, purchase_date, " +
                    " status_id, refunded_amount) " +
                    " VALUES " +
                    " (@userId, @assetSubscriptionId, @assetId, " +
                    dbUtility.GetCurrentDateFunction () + "," + endDate + ", @dSubscriptionAmount, @renewable, " + dbUtility.GetCurrentDateFunction () + "," +
                    " @statusId, 0)";

                parameters = new Hashtable ();
                parameters.Add ("@userId", userId);
                parameters.Add ("@assetSubscriptionId", assetSubscriptionId);
                parameters.Add ("@assetId", assetId);
                parameters.Add ("@dSubscriptionAmount", dSubscriptionAmount);
                parameters.Add ("@renewable", IsRenewable ? "Y" : "N");
                parameters.Add ("@statusId", userAssetSubscriptionStatus);
                dbUtility.ExecuteIdentityInsert (sqlInsert, parameters, ref userAssetSubscriptionId);

                // Copy the billing information over for renewable subs.
                if (IsRenewable)
                {
                    // Copy the billing information over to the subscription billing information
                    if (!drOrder ["point_transaction_id"].Equals (DBNull.Value))
                    {
                        DataRow drPPT = GetPurchasePointTransaction (Convert.ToInt32 (drOrder ["point_transaction_id"]));

                        if (!drPPT ["order_billing_information_id"].Equals (DBNull.Value))
                        {
                            int subBillingInformationId = StoreUtility.CopyOrderBillingInfoToSubscription (Convert.ToInt32 (drPPT ["order_billing_information_id"]));
                            UpdateUserAssetSubscription (userAssetSubscriptionId, subBillingInformationId);
                        }
                    }
                }
  //          }

            return userAssetSubscriptionId;
        }

        /// <summary>
        /// insert a user Pilot asset subscription
        /// </summary>
        private static int InsertUserPilotAssetSubscription (int userId, int assetId, Double dSubscriptionAmount, string renewable, int userAssetSubscriptionStatus)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            string endDate = dbUtility.GetDatePlusDays (1);

            // Record User Asset Subscriptions
            string sqlInsert = "INSERT INTO user_asset_subscriptions " +
                " (user_id, asset_subscription_id, asset_id, " +
                " start_datetime, end_datetime, gross_point_amount, renewable, purchase_date, " +
                " status_id, refunded_amount) " +
                " VALUES " +
                " (@userId, @assetSubscriptionId, @assetId, " +
                dbUtility.GetCurrentDateFunction () + "," + endDate + ", @dSubscriptionAmount, @renewable, " + dbUtility.GetCurrentDateFunction () + "," +
                " @statusId, 0)";

            int userAssetSubscriptionId = 0;
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@assetSubscriptionId", 0);
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@dSubscriptionAmount", dSubscriptionAmount);
            parameters.Add ("@renewable", renewable);
            parameters.Add ("@statusId", userAssetSubscriptionStatus);
            dbUtility.ExecuteIdentityInsert (sqlInsert, parameters, ref userAssetSubscriptionId);

            return userAssetSubscriptionId;
        }

        /// <summary>
        /// insert a community subscription
        /// </summary>
        public static int InsertCommunityMemberSubscription (int userId, int communityId, int communitySubscriptionId, Double dSubscriptionAmount, int transactionStatus)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            string endDate = "";

            // Look up the subscription to get the end date
            DataRow drCommunitySubscription = CommunityUtility.GetCommunitySubscription (communitySubscriptionId);

            switch (Convert.ToInt32 (drCommunitySubscription ["length_of_subscription"]))
            {
                case (int) Constants.eCOMMUNITY_SUBSCRIPTION_LENGTH.DAILY:
                {
                    endDate = dbUtility.GetDatePlusDays (1);
                    break;
                }
                case (int) Constants.eCOMMUNITY_SUBSCRIPTION_LENGTH.MONTHLY:
                {
                    endDate = dbUtility.GetDatePlusMonths (1);
                    break;
                }
                case (int) Constants.eCOMMUNITY_SUBSCRIPTION_LENGTH.YEARLY:
                {
                    endDate = dbUtility.GetDatePlusYears (1);
                    break;
                }
                default:
                {
                    endDate = dbUtility.GetCurrentDateFunction ();
                    break;
                }
            }

            // Record amount in Purchase Transaction
            string sqlInsert = "INSERT INTO community_member_subscriptions " +
                " (community_id, user_id, community_subscription_id, start_datetime, end_datetime, " +
                " gross_point_amount, purchase_date, status_id, refund_amount) " +
                " VALUES " +
                " (@communityId, @userId, @csId, " + dbUtility.GetCurrentDateFunction () + "," + endDate +
                ", @dSubscriptionAmount," + dbUtility.GetCurrentDateFunction () + "," + (int) Constants.eCOMMUNITY_MEMBER_SUBSCRIPTION_STATUS.COMPLETED + ", 0)";

            int communityMemberSubscriptionId = 0;
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            parameters.Add ("@communityId", communityId);
            parameters.Add ("@csId", communitySubscriptionId);
            parameters.Add ("@dSubscriptionAmount", dSubscriptionAmount);
            dbUtility.ExecuteIdentityInsert (sqlInsert, parameters, ref communityMemberSubscriptionId);

            return communityMemberSubscriptionId;
        }

        /// <summary>
        /// Update a user asset Subscription
        /// </summary>
        public static int UpdateUserAssetSubscription (int userId, int userAssetSubscriptionId, int statusId, bool updatePurchaseDate)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            string sqlMore = "";

            if (updatePurchaseDate)
            {
                sqlMore += " , purchase_date = " + dbUtility.GetCurrentDateFunction ();
            }

            string sqlInsert = "UPDATE user_asset_subscriptions " +
                " SET status_id = @statusId " +
                sqlMore +
                " WHERE user_asset_subscription_id = @uasId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@statusId", statusId);
            parameters.Add ("@uasId", userAssetSubscriptionId);
            return dbUtility.ExecuteNonQuery (sqlInsert, parameters);
        }

        /// <summary>
        /// Update a user asset Subscription
        /// </summary>
        public static int UpdateUserAssetSubscription (int userId, int userAssetSubscriptionId, int statusId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlInsert = "UPDATE user_asset_subscriptions " +
                " SET status_id = @statusId " +
                " WHERE user_asset_subscription_id = @uasId " +
                " AND user_id = @userId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@statusId", statusId);
            parameters.Add ("@uasId", userAssetSubscriptionId);
            parameters.Add ("@userId", userId);
            return dbUtility.ExecuteNonQuery (sqlInsert, parameters);
        }

        /// <summary>
        /// Update a user asset Subscription
        /// </summary>
        public static int UpdateUserAssetSubscription (int userAssetSubscriptionId, int subBillingInformationId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            string sqlInsert = "UPDATE user_asset_subscriptions " +
                " SET sub_billing_information_id = @subBIId " +
                " WHERE user_asset_subscription_id = @uasId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@subBIId", subBillingInformationId);
            parameters.Add ("@uasId", userAssetSubscriptionId);
            return dbUtility.ExecuteNonQuery (sqlInsert, parameters);
        }

        /// <summary>
        /// Update an community Subscription
        /// </summary>
        public static int UpdateCommunityMemberSubscription (int userId, int commMemSubscriptionId, int purchaseTransactionId, int statusId, bool updatePurchaseDate)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();
            string sqlMore = "";

            if (updatePurchaseDate)
            {
                sqlMore += " , purchase_date = " + dbUtility.GetCurrentDateFunction ();
            }

            string sqlInsert = "UPDATE community_member_subscriptions " +
                " SET status_id = @statusId " +
                sqlMore +
                " WHERE community_member_subscription_id = @commMemSubscriptionId ";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@statusId", statusId);
            parameters.Add ("@commMemSubscriptionId", commMemSubscriptionId);
            return dbUtility.ExecuteNonQuery (sqlInsert, parameters);

            // NOTE, now handled in PurchaseOrderNow
            //          // Do we need to update the user balances?
            //          if (statusId == (int) Constants.eCOMMUNITY_MEMBER_SUBSCRIPTION_STATUS.COMPLETED)
            //          {
            //              // Get the transaction to look up asset amount, get the user mpoint balance.
            //              DataRow drTransaction = GetCommunityMemberSubscriptionById (commMemSubscriptionId);
            //
            //              if (drTransaction == null)
            //              {
            //                  m_logger.Error ("Subscription transaction was not found, user " + userId +  " was not deducted k-points for member subscription = " + commMemSubscriptionId);
            //              }
            //              else
            //              {
            //                  // Deduct user balances
            //                  DeductPointBalances (userId, Convert.ToDouble (drTransaction ["gross_point_amount"]), UsersUtility.getUserBalance (userId, Constants.CURR_MPOINT), purchaseTransactionId);
            //              }
            //          }
        }

        /// <summary>
        /// GetAssetSubscriptions
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAssetSubscriptions (int assetId, bool onlyActive)
        {
            string sqlSelect = "SELECT asset_subscription_id, community_id, name, description, amount, amount_kei_point_id, " +
                " length_of_subscription, status_id, updated_date, created_date " +
                " FROM asset_subscriptions " +
                " WHERE asset_id = @assetId ";

            if (onlyActive)
            {
                sqlSelect += " AND status_id = " + (int) Constants.eASSET_SUBSCRIPTION_STATUS.ACTIVE;
            }

            sqlSelect += " ORDER BY amount ASC";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            DataTable dtAssetSubscriptions = KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);

            // Add a description field
            dtAssetSubscriptions.Columns.Add (new DataColumn ("display", System.Type.GetType("System.String") ));
            for (int i = 0; i < dtAssetSubscriptions.Rows.Count; i ++)
            {
                dtAssetSubscriptions.Rows [i]["display"] = KanevaGlobals.FormatKPoints (Convert.ToDouble (dtAssetSubscriptions.Rows [i]["amount"])) + " for " + dtAssetSubscriptions.Rows [i]["name"].ToString ();
            }

            return dtAssetSubscriptions;
        }

        /// <summary>
        /// GetCommunitySubscripsion
        /// </summary>
        /// <returns></returns>
        public static DataRow GetAssetSubscription (int assetSubscriptionId)
        {
            string sqlSelect = "SELECT asset_subscription_id, asset_id, name, description, amount, amount_kei_point_id, " +
                " length_of_subscription, status_id, updated_date, created_date " +
                " FROM asset_subscriptions " +
                " WHERE asset_subscription_id = @assetSubscriptionId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetSubscriptionId", assetSubscriptionId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// GetLicenseSubscripsion
        /// </summary>
        /// <returns></returns>
        public static DataRow GetLicenseSubscription (int licenseSubscriptionId)
        {
            string sqlSelect = "SELECT license_subscription_id, name, length_of_subscription, amount, " +
                " amount_kei_point_id, max_server_users, disk_quota  " +
                " FROM license_subscription " +
                " WHERE license_subscription_id = @licenseSubscriptionId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@licenseSubscriptionId", licenseSubscriptionId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// GetActiveUserAssetSubscription
        /// </summary>
        /// <returns></returns>
        public static DataRow GetActiveUserAssetSubscription (int assetId, int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            int iBufferInDays = 4;

            string sqlSelect = "SELECT uas.user_asset_subscription_id, uas.user_id, uas.asset_subscription_id, uas.asset_id, uas.status_id, " +
                " uas.start_datetime, uas.end_datetime, uas.purchase_date " +
                " FROM user_asset_subscriptions uas " +
                " WHERE uas.user_id = @userId " +
                " AND uas.asset_id = @assetId " +

                // For recurring asset subscriptions, add an extra iBufferInDays days to account for billing system being down.
                // No iBufferInDays if Day or Year purchase (i.e. Only give buffer for renewable = "Y").
                // No iBufferInDays if status is cancelled

                " AND uas.start_datetime <= " + dbUtility.GetCurrentDateFunction () +
                " AND ( " +
                    " (uas.renewable = 'Y' AND uas.status_id = " + (int) Constants.eUSER_ASSET_SUBSCRIPTION_STATUS.ACTIVE + " AND " +  dbUtility.GetDatePlusDays (iBufferInDays, "uas.end_datetime") + " >= " + dbUtility.GetCurrentDateFunction () + ")" +
                    " OR " +
                    " (uas.renewable <> 'Y' AND uas.status_id = " + (int) Constants.eUSER_ASSET_SUBSCRIPTION_STATUS.ACTIVE + " AND uas.end_datetime >= " + dbUtility.GetCurrentDateFunction () + ")" +
                    " OR " +
                    " (uas.status_id = " + (int) Constants.eUSER_ASSET_SUBSCRIPTION_STATUS.USER_CANCELLED + " AND uas.end_datetime >= " + dbUtility.GetCurrentDateFunction () + ") " +
                ")";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@userId", userId);
            return dbUtility.GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// Does the user have an active subscription to the asset?
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static bool HasActiveUserAssetSubscription (int assetId, int userId)
        {
            DataRow drAssetSub = GetActiveUserAssetSubscription (assetId, userId);
            return (drAssetSub != null);
        }

        /// <summary>
        /// GetActiveUserLicenseSubscription
        /// </summary>
        /// <returns></returns>
        public static DataRow GetActiveUserLicenseSubscription (int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "SELECT uls.user_license_subscription_id, uls.license_subscription_id, uls.user_id, " +
                " uls.start_date, uls.end_date, uls.renewable, uls.status_id, user_cancelled, ls.disk_quota, ls.name " +
                " FROM user_license_subscription uls, license_subscription ls " +
                " WHERE uls.user_id = @userId " +
                " AND uls.license_subscription_id = ls.license_subscription_id " +
                GetLicenseSubscriptionDateRangeQuery ();

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return dbUtility.GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// Get start and end date query for license subscripion start and end
        /// </summary>
        /// <returns></returns>
        public static string GetLicenseSubscriptionDateRangeQuery ()
        {
            // For license subscriptions, add an extra cBILLING_LICENSE_BUFFER_DAYS days to account for billing system being down.
            // It's ok cause if they cancel, end of term status will trump end date

            // See Mantis-0000706: Kaneva Billing "End of Month" algorithm does not match Paypal's
            // So instead of --uls.end_date-- useing --dbUtility.GetDatePlusDays (5, "uls.end_date")--
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            return " AND uls.start_date <= " + dbUtility.GetCurrentDateFunction () +
                " AND ((uls.status_id = " + (int) Constants.eUSER_LICENSE_SUBSCRIPTION_STATUS.ACTIVE + " AND " + dbUtility.GetDatePlusDays (cBILLING_LICENSE_BUFFER_DAYS, "uls.end_date") + " >= " + dbUtility.GetCurrentDateFunction () + " OR uls.end_date IS NULL)" +
                " OR " +
                " (uls.status_id = " + (int) Constants.eUSER_LICENSE_SUBSCRIPTION_STATUS.CANCELLED + " AND uls.end_date >= " + dbUtility.GetCurrentDateFunction () + " OR uls.end_date IS NULL))";
        }

        /// <summary>
        /// GetUserGameSubscriptionCount
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static int GetUserGameSubscriptionCount (int userId, bool onlyExpiringSoon)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelect = "SELECT COALESCE(COUNT(*),0) " +
                " FROM user_asset_subscriptions uls " +
                " WHERE uls.user_id = @userId " +
                " AND uls.start_datetime <= " + dbUtility.GetCurrentDateFunction () +
                " AND (uls.end_datetime >= " + dbUtility.GetCurrentDateFunction () + " OR uls.end_datetime IS NULL)" +
                " AND uls.status_id = " + (int) Constants.eUSER_LICENSE_SUBSCRIPTION_STATUS.ACTIVE;

            if (onlyExpiringSoon)
            {
                sqlSelect += " AND uls.end_datetime < " + dbUtility.GetDatePlusDays (7);
            }

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sqlSelect, parameters);
        }

        /// <summary>
        /// Does the user have an active subscription a license?
        /// </summary>
        public static bool HasActivePilotLicense (int userId)
        {
            DataRow drLicenseSub = GetActiveUserLicenseSubscription (userId);

            if (drLicenseSub == null)
            {
                return false;
            }

            return (drLicenseSub ["license_subscription_id"].Equals ((int) Constants.eLICENSE_SUBSCRIPTIONS.PILOT) || drLicenseSub ["license_subscription_id"].Equals ((int) Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL));
        }

        /// <summary>
        /// Does the user have an active subscription a license?
        /// </summary>
        public static bool HasActiveCommercialLicense (int userId)
        {
            DataRow drLicenseSub = GetActiveUserLicenseSubscription (userId);

            if (drLicenseSub == null)
            {
                return false;
            }

            return (drLicenseSub ["license_subscription_id"].Equals ((int) Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL));
        }

        /// <summary>
        /// Insert a asset subscription
        /// </summary>
        public static int InsertAssetSubscription (int assetId, string name, string description, double subscriptionAmount,
            int subscriptionLength, int statusId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlString = "INSERT INTO asset_subscriptions " +
                "(asset_id, name, description, amount, amount_kei_point_id, " +
                " length_of_subscription, status_id, updated_date, created_date " +
                ") VALUES (" +
                "@assetId, @name, @description, @subscriptionAmount,'KPoint'," +
                " @subscriptionLength, @statusId," + dbUtility.GetCurrentDateFunction () + "," + dbUtility.GetCurrentDateFunction () +
                ")";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@name", name);
            parameters.Add ("@description", description);
            parameters.Add ("@subscriptionAmount", subscriptionAmount);
            parameters.Add ("@subscriptionLength", subscriptionLength);
            parameters.Add ("@statusId", statusId);
            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// Update a asset subscription
        /// </summary>
        public static int UpdateAssetSubscription (int assetId, string name, double subscriptionAmount, int subscriptionLength, int statusId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            // If it does not exist, just insert it
            string sqlString = "SELECT COUNT(*) " +
                " FROM asset_subscriptions " +
                " WHERE asset_id = @assetId " +
                " AND length_of_subscription = @subscriptionLength";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@subscriptionLength", subscriptionLength);
            int result = KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sqlString, parameters);
            if (result == 0)
            {
                return InsertAssetSubscription (assetId, name, name, subscriptionAmount, subscriptionLength, statusId);
            }

            // Update it
            sqlString = "UPDATE asset_subscriptions " +
                " SET amount = @subscriptionAmount " +
                " ,status_id = @statusId " +
                " ,updated_date = " + dbUtility.GetCurrentDateFunction () +
                " WHERE asset_id = @assetId " +
                " AND length_of_subscription = @subscriptionLength";

            parameters.Add ("@subscriptionAmount", subscriptionAmount);
            parameters.Add ("@statusId", statusId);
            return dbUtility.ExecuteNonQuery (sqlString, parameters);
        }

        /// <summary>
        /// IsGameSubscription
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static bool IsGameSubscription (int assetId)
        {
            DataRow drAsset = StoreUtility.GetAsset (assetId);
            return IsGameSubscription (drAsset);
        }

        /// <summary>
        /// IsGameSubscription
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static bool IsGameSubscription (DataRow drAsset)
        {
            if (!IsKanevaGame (drAsset))
            {
                return false;
            }

            // Not a subscription if it is free!
            if (Convert.ToDouble (drAsset ["amount"]).Equals (0.0))
            {
                return false;
            }

            return true;
        }

        #endregion

        // **********************************************************************************************
        // User Inventory Functions
        // **********************************************************************************************
        #region User Inventory Functions


        /// <summary>
        /// Get the Asset Sub Categories
        /// </summary>
        /// <returns></returns>
        public static PagedDataTable GetUserGameHistory (int userId, string orderby, int pageNumber, int pageSize)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string sqlSelectList = "a.asset_id, a.name, uas.gross_point_amount as price, uas.purchase_date, uas.user_asset_subscription_id, "+
                " uas.end_datetime, uas.renewable, uas.status_id as uas_status_id, uass.name as status_name, ass.name as type, " +
                " (uas.end_datetime > " + dbUtility.GetCurrentDateFunction () + ") as expired "; // 0 means expired, 1 means not expired

            string sqlTableList = "user_asset_subscription_status uass, assets a, user_asset_subscriptions uas LEFT OUTER JOIN asset_subscriptions ass ON uas.asset_subscription_id = ass.asset_subscription_id";

            string sqlWhereClause = "uas.user_id = @userId " +
                " AND a.asset_id = uas.asset_id " +
                " AND uas.status_id = uass.status_id " +
                " AND a.asset_type_id = " + (int) Constants.eASSET_TYPE.GAME +
				" AND a.is_kaneva_game = 1";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return dbUtility.GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause, orderby, parameters, pageNumber, pageSize);
        }

        /// <summary>
        /// Obtain the user/password used to authenticate torrents on Azureus.
        /// </summary>
        /// <returns>DataRow for user/pass</returns>
        public static DataRow GetContentCredentials()
        {
            string sqlSelect = "SELECT torrent_username, torrent_password FROM configuration";

            Hashtable parameters = new Hashtable ();
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, true);
        }

        /// <summary>
        /// Obtain the total content size that has been published for a given user.
        /// </summary>
        /// <returns>DataRow for user/pass</returns>
        public static DataRow GetUserPublishedSize( int userId)
        {
            string sqlSelect = "SELECT SUM(t.content_size) as content_size FROM torrents t WHERE " +
                "t.user_id=@user_id AND t.secured <> 0";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@user_id", userId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow(sqlSelect, parameters, true);
        }

        /// <summary>
        /// Obtain the torrent status for a given set of info hashes.
        /// </summary>
        /// <param name="userId">not used</param>
        /// <param name="hashes">the given set of info hashes</param>
        /// <returns>DataTable</returns>
        public static DataTable GetTorrentStatus (int userId, ArrayList hashes)
        {
            if ( hashes.Count > 0)
            {
                //              UpdateSecureTorrentStatus( hashes, -1);

                string sqlSelect = "SELECT t.initial_info_hash, t.torrent_status, t.secured, t.number_of_seeds, t.seed_updated_datetime " +
                    " FROM torrents t WHERE (";

                int ii = 0;
                while ( ii < hashes.Count)
                {
                    if ( ii > 0)
                    {
                        sqlSelect += " OR ";
                    }
                    sqlSelect += " t.initial_info_hash = @hashId" + ii;
                    ii++;
                }

                sqlSelect += ")";

                Hashtable parameters = new Hashtable ();

                ii = 0;
                IEnumerator assetEnumerator = hashes.GetEnumerator();
                while ( assetEnumerator.MoveNext() )
                {
                    parameters.Add ("@hashId" + ii, assetEnumerator.Current);
                    ii++;
                }

                return KanevaGlobals.GetDatabaseUtility ().GetDataTable(sqlSelect, parameters);
            }

            return null;
        }

        /// <summary>
        /// Obtain the torrent status for a given set of info hashes.
        /// </summary>
        /// <param name="userId">not used</param>
        /// <param name="hashes">the given set of info hashes</param>
        /// <returns>DataTable</returns>
        public static DataTable GetTorrentAssets (int userId, ArrayList hashes)
        {
            if ( hashes.Count > 0)
            {
                string sqlSelect = "SELECT t.initial_info_hash, a.asset_id, c.name as community_name  " +
                    " FROM assets a LEFT OUTER JOIN communities c ON a.community_id=c.community_id " +
                    " , torrents t " +
                    " WHERE a.torrent_id=t.torrent_id " +
                    " AND t.user_id = @user_id " +
                    " AND a.owner_id = @owner_id " +
                    " AND (";

                int ii = 0;
                while ( ii < hashes.Count)
                {
                    if ( ii > 0)
                    {
                        sqlSelect += " OR ";
                    }
                    sqlSelect += " t.initial_info_hash = @hashId" + ii;
                    ii++;
                }

                sqlSelect += ")";

                Hashtable parameters = new Hashtable ();

                parameters.Add ("@user_id", userId);
                parameters.Add ("@owner_id", userId);

                ii = 0;
                IEnumerator assetEnumerator = hashes.GetEnumerator();
                while ( assetEnumerator.MoveNext() )
                {
                    parameters.Add ("@hashId" + ii, assetEnumerator.Current);
                    ii++;
                }

                return KanevaGlobals.GetDatabaseUtility ().GetDataTable(sqlSelect, parameters);
            }

            return null;
        }


        #endregion

        // **********************************************************************************************
        // Conversions
        // **********************************************************************************************
        #region Conversions

        /// <summary>
        /// KPointsToDollars
        /// </summary>
        /// <param name="dKPoints"></param>
        /// <returns></returns>
        public static Double ConvertKPointsToDollars (Double dKPoints)
        {
            return (dKPoints * 1/100);
        }

        /// <summary>
        /// DollarsToKPoints
        /// </summary>
        /// <param name="dDollars"></param>
        /// <returns></returns>
        public static Double ConvertDollarsToKPoints (Double dDollars)
        {
            return (dDollars * 100);
        }

        #endregion

        // **********************************************************************************************
        // Sales and Income
        // **********************************************************************************************
        #region Sales and Income

        /// <summary>
        /// GetTotalRoyaltiesEarned for a user
        /// </summary>
        public static Double GetTotalRoyaltiesEarned (int userId)
        {
            string sqlSelect = "SELECT COALESCE(SUM(oi.royalty_amount),0) as total_royalty_amount" +
                " FROM order_items oi, orders o " +
                " WHERE oi.order_id = o.order_id " +
                " AND o.transaction_status_id = " + (int) Constants.eORDER_STATUS.COMPLETED +
                " AND oi.royalty_user_id_paid = @userId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            DataRow drResult = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);

            if (drResult == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToDouble (drResult ["total_royalty_amount"]);
            }
        }


        /// <summary>
        /// GetMonthlyTotal for a user
        /// </summary>
        public static DataRow GetMonthlyTotal (int userId, DateTime dtSearch)
        {
            string sqlSelect = "SELECT COALESCE(SUM(oi.royalty_amount),0) as total_royalty_amount, COALESCE(COUNT(oi.order_item_id),0) as items_sold " +
                " FROM order_items oi, orders o " +
                " WHERE oi.order_id = o.order_id " +
                " AND o.transaction_status_id = " + (int) Constants.eORDER_STATUS.COMPLETED +
                " AND oi.royalty_user_id_paid = @userId";

            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            int iDaysInMonth = DateTime.DaysInMonth (dtSearch.Year, dtSearch.Month);
            string startDate = dbUtility.FormatDate (new DateTime (dtSearch.Year, dtSearch.Month, 1, 0, 0, 0));
            string endDate = dbUtility.FormatDate (new DateTime (dtSearch.Year, dtSearch.Month, iDaysInMonth, 23, 59, 59));
            sqlSelect += " AND o.purchase_date >= '" + startDate + "' AND o.purchase_date <= '" + endDate + "'";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return dbUtility.GetDataRow (sqlSelect, parameters, false);
        }


		/// <summary>
		/// GetSoldItemsForMonth
		/// </summary>
		/// <returns></returns>
		public static PagedDataTable GetSoldItems (int userId, string orderBy, int pageNumber, int pageSize)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityReadOnly ();
			Hashtable parameters = new Hashtable ();

			string sqlSelectList = "COUNT(*) as count, oi.asset_id, " +
				" SUM(oi.royalty_paid) as royalty_paid, SUM(oi.royalty_amount) as royalty_amount, " +
				" a.name, a.short_description, a.asset_type_id, a.amount, oi.point_amount, oi.royalty_user_id_paid, a.created_date ";

			string sqlTableList = "order_items oi, orders o, assets a";

			string sqlWhereClause = " oi.asset_id = a.asset_id" +
				" AND o.order_id = oi.order_id " +
				" AND o.transaction_status_id = " + (int) Constants.eORDER_STATUS.COMPLETED +
				" AND oi.royalty_user_id_paid = @userId";

			string sqlWhereClauseGROUP = " GROUP BY oi.asset_id ";

			parameters.Add ("@userId", userId);
			PagedDataTable pdt = dbUtility.GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause + sqlWhereClauseGROUP, orderBy, parameters, pageNumber, pageSize);
			pdt.TotalCount = dbUtility.ExecuteScalar ("SELECT COUNT(DISTINCT oi.asset_id) FROM " + sqlTableList + " WHERE " + sqlWhereClause, parameters);
			return pdt;
		}

        /// <summary>
        /// GetSoldItemsForMonth
        /// </summary>
        /// <returns></returns>
        public static PagedDataTable GetSoldItemsForMonth (int userId, DateTime dtMonth, string orderBy, int pageNumber, int pageSize)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
            Hashtable parameters = new Hashtable ();

            string sqlSelectList = "COUNT(*) as count, oi.asset_id, " +
                " SUM(oi.royalty_paid) as royalty_paid, SUM(oi.royalty_amount) as royalty_amount, a.file_size, " +
                " a.name, a.short_description, a.asset_type_id, a.amount, oi.point_amount, oi.royalty_user_id_paid, " +
				" a.thumbnail_medium_path, a.thumbnail_gen ";

            string sqlTableList = "order_items oi, orders o, assets a";

            string sqlWhereClause = " oi.asset_id = a.asset_id" +
                " AND o.order_id = oi.order_id " +
                " AND o.transaction_status_id = " + (int) Constants.eORDER_STATUS.COMPLETED +
                " AND oi.royalty_user_id_paid = @userId";

			int iDaysInMonth = DateTime.DaysInMonth (dtMonth.Year, dtMonth.Month);
			string startDate = dbUtility.FormatDate (new DateTime (dtMonth.Year, dtMonth.Month, 1, 0, 0, 0));
			string endDate = dbUtility.FormatDate (new DateTime (dtMonth.Year, dtMonth.Month, iDaysInMonth, 23, 59, 59));
			sqlWhereClause += " AND o.purchase_date >= '" + startDate + "' AND o.purchase_date <= '" + endDate + "'";

			string sqlWhereClauseGROUP = " GROUP BY oi.asset_id ";

			parameters.Add ("@userId", userId);
			PagedDataTable pdt = dbUtility.GetPagedDataTable (sqlSelectList, sqlTableList, sqlWhereClause + sqlWhereClauseGROUP, orderBy, parameters, pageNumber, pageSize);
			pdt.TotalCount = dbUtility.ExecuteScalar ("SELECT COUNT(DISTINCT oi.asset_id) FROM " + sqlTableList + " WHERE " + sqlWhereClause, parameters);
			return pdt;
		}

        /// <summary>
        /// GetRoyaltyPaidTotals for a user
        /// </summary>
        public static DataRow GetRoyaltyPaidTotals (int userId)
        {
            string sqlSelect = "SELECT COALESCE(SUM(total_amount),0) as total_amount, COALESCE(SUM(amount_paid),0) as amount_paid, " +
                " COALESCE(SUM(tax_withheld),0) as tax_withheld, MAX(payment_date) as payment_date " +
                " FROM royalty_payments " +
                " WHERE user_id_paid = @userId";

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@userId", userId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
        }

        /// <summary>
        /// GetPayments
        /// </summary>
        public static PagedDataTable GetPayments (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            Hashtable parameters = new Hashtable ();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            string selectList = " payment_id, user_id_paid, check_number, payment_date, period_start_date, period_end_date, " +
                " total_amount, amount_paid, tax_withheld, notes, user_id_created, created_date ";

            string tableList = " royalty_payments ";

            string whereClaus = " user_id_paid = @userId";

            if (filter.Length > 0)
            {
                whereClaus += " AND " + filter;
            }

            parameters.Add ("@userId", userId);
            return dbUtility.GetPagedDataTable (selectList, tableList, whereClaus, orderby, parameters, pageNumber, pageSize);
        }

        #endregion

        // **********************************************************************************************
        // Services
        // **********************************************************************************************
        #region Services

        /// <summary>
        /// Sums file sizes for all files within given path.
        /// </summary>
        /// <param name="path">Path to check</param>
        /// <returns>size of all files in given folder</returns>
        private static long FolderFileSize (string path)
        {
            long size = 0;

            FileInfo [] files = (new DirectoryInfo(path)).GetFiles();
            foreach(FileInfo file in files)
            {
                size += file.Length;
            }

            return size;
        }

        /// <summary>
        /// FolderSize
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static long FolderSize (string path)
        {
            long fsize = 0;

            fsize = FolderFileSize(path);

            DirectoryInfo [] folders = (new DirectoryInfo(path)).GetDirectories();

            foreach(DirectoryInfo folder in folders)
            {
                fsize += FolderSize(folder.FullName);
            }

            return fsize;
        }

        /// <summary>
        /// Get the current set of assets that has been uploaded and processed but
        /// second phase of publishing was not completed in Kaneva.
        /// </summary>
        /// <returns></returns>
        public static DataTable GetOrphanedAssets()
        {
            StringBuilder sb = new StringBuilder() ;
            sb.Append(" SELECT a.asset_id, a.name, au.completed_datetime ");
            sb.Append(" FROM assets a ");
            sb.Append(" INNER JOIN asset_upload au ON au.asset_id = a.asset_id ");
            sb.Append(" WHERE a.status_id =  ").Append((int)Constants.eASSET_STATUS.NEW);
            sb.Append(" AND a.publish_status_id =  ").Append((int) Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE);
            sb.Append(" AND au.completed_datetime IS NOT NULL ");

            return KanevaGlobals.GetDatabaseUtility ().GetDataTable(sb.ToString());
        }

        /// <summary>
        /// Get the current set of assets entries that have a uploaded date less than (currentdate - daysSinceCreation)
        /// </summary>
        /// <returns>DataTable of all returned torrents. Caller will contain business login to operate
        /// on this result set</returns>
        public static DataTable GetContentNotProcessed(int daysSinceCreation)
        {
            StringBuilder sb = new StringBuilder() ;
            sb.Append(" SELECT a.asset_id, a.name, a.publish_status_id, au.completed_datetime ");
            sb.Append(" FROM assets a ");
            sb.Append(" INNER JOIN asset_upload au ON au.asset_id = a.asset_id ");
            sb.Append(" WHERE a.publish_status_id <> ").Append((int) Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE);
            sb.Append(" AND a.status_id NOT IN (");
            sb.Append((int) Constants.eASSET_STATUS.DELETED);
            sb.Append(" ,");
            sb.Append((int) Constants.eASSET_STATUS.MARKED_FOR_DELETION);
            sb.Append(")");
            sb.Append(" AND au.completed_datetime IS NOT NULL ");
            sb.Append(" AND au.completed_datetime < date_sub(CURDATE(), interval ").Append(daysSinceCreation).Append(" day)");
            return KanevaGlobals.GetDatabaseUtility ().GetDataTable(sb.ToString());
        }

		/// <summary>
		/// Mark a list of assets and associated torrents as deleted
		/// </summary>
		/// <param name="assetIds">A array list of asset ids to delete</param>
		/// <returns>none</returns>
		public static void DeleteOrphanedAssets(ArrayList assetIds)
		{
			if ( assetIds.Count > 0)
			{
				//mark asset deleted
				DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

				string sqlString = "UPDATE assets a SET a.status_id = " + (int) Constants.eASSET_STATUS.MARKED_FOR_DELETION +
					" WHERE (";

				int ii = 0;
				while ( ii < assetIds.Count)
				{
					if ( ii > 0)
					{
						sqlString += " OR ";
					}
					sqlString += " a.asset_id = @assetId" + ii;
					ii++;
				}

				sqlString += ")";

				Hashtable parameters = new Hashtable ();

				ii = 0;
				IEnumerator assetEnumerator = assetIds.GetEnumerator();
				while ( assetEnumerator.MoveNext() )
				{
					parameters.Add ("@assetId" + ii, assetEnumerator.Current);
					ii++;
				}

				dbUtility.ExecuteNonQuery (sqlString, parameters);

				//mark torrent deleted
				StringBuilder sb = new StringBuilder() ;
				sb.Append(" UPDATE torrents ");
				sb.Append(" SET status_id = ").Append((int) Constants.eTORRENT_ENTRY_STATUS.DELETED);
				sb.Append(" WHERE torrent_id IN ( ");
				sb.Append(" SELECT torrent_id FROM torrents WHERE (");
				ii = 0;
				while ( ii < assetIds.Count)
				{
					if ( ii > 0)
					{
						sb.Append(" OR ");
					}
					sb.Append(" a.asset_id = @assetId").Append(ii);
					ii++;
				}
				sb.Append(" ) ");
				sb.Append(" ) ");

				parameters = new Hashtable ();
				ii = 0;
				assetEnumerator = assetIds.GetEnumerator();
				while ( assetEnumerator.MoveNext() )
				{
					parameters.Add ("@assetId" + ii, assetEnumerator.Current);
					ii++;
				}

				dbUtility.ExecuteNonQuery (sb.ToString(), parameters);
			}
		}

        #endregion

        // **********************************************************************************************
        // Enums
        // **********************************************************************************************
        #region Enums

        private enum eRETURN_CODES
        {
            BIT_TORRENT_LINK_FAIL = 1,  // Unable to obtain the bit torrent for an asset
            EXCEPTION_OBTAINING_ASSET_SERVERS,
            EXCEPTION_OBTAINING_INVENTORY,
            EXCEPTION_OBTAINING_ASSET_FILE,
            EXCEPTION_POSTING_ASSET_FILE,
            ERROR_POST_MULTIPLE_ASSET,
            ERROR_POST_EMPTY_ASSET,
            ERROR_POST_TRACKER,
            EXCEPTION_POSTING_TO_TRACKER,
            ERROR_POST_FILE_CORRUPT_OR_INVALID,
            ERROR_POST_FILE_NOT_TORRENT,
            ERROR_POST_FILE_CATEGORY_TAG_INVALID,
            ERROR_POST_FILE_ALREADY_EXISTS,
            ERROR_POST_FILE_HASH_CONFLICT,
            EXCEPTION_DELETING_FROM_TRACKER,
            ERROR_DELETE_FILE_FAILURE,
            ERROR_POST_DB_ENTRY_ALREADY_EXISTS,
            ERROR_POST_DB_INSERT_FAILURE
        }

        #endregion

        // **********************************************************************************************
        // Common SQL
        // **********************************************************************************************
        #region Common SQL

		/// <summary>
		/// SQLCommon_GetOnlyActiveStatusAssetsSQL
		/// </summary>
		/// <returns></returns>
		public static string SQLCommon_GetOnlyActiveStatusAssetsSQL ()
		{
			return " AND a.status_id IN (" + (int) Constants.eASSET_STATUS.ACTIVE + "," + (int) Constants.eASSET_STATUS.NEW +")";
		}

		/// <summary>
		/// SQLCommon_GetPublishedAssetsSQL
		/// </summary>
		/// <returns></returns>
		public static string SQLCommon_GetPublishedAssetsSQL ()
		{
			return " AND a.published = 'Y' ";
		}

		public static string MATURE_RATINGS = "3,6,9";

		/// <summary>
		/// return true if rating is mature
		/// </summary>
		/// <param name="rating"></param>
		/// <returns></returns>
		public static bool IsMatureRating( int rating )
		{
			string [] ratings = MATURE_RATINGS.Split(",".ToCharArray());
			for ( int i = 0; i < ratings.Length; i++ )
			{
				if ( rating == Int32.Parse( ratings[i] ) )
					return true;
			}
			return false;
		}

		/// <summary>
		/// return true if rating is mature
		/// </summary>
		/// <param name="rating"></param>
		/// <returns></returns>
		public static bool IsMatureRating ( string mature )
		{
			return mature.Equals ("Y");
		}

		/// <summary>
		/// SQLCommon_GetNonMature
		/// </summary>
		/// <returns></returns>
		public static string SQLCommon_GetNonMatureAssets ()
		{
			return " AND a.mature = 'N' ";
		}

		/// <summary>
		/// Gets list of bad words
		/// </summary>
		/// <returns></returns>
		public static DataTable GetPottyMouth ()
		{
			string sqlSelect = "SELECT id, bad_word, replace_with " +
				" FROM potty_mouth ";

			return KanevaGlobals.GetDatabaseUtility ().GetDataTable(sqlSelect);
		}

		public static string FormPivotTableQuery(string subtableName, string idColumn, string valueColumn, string descriptionColumn)
		{
			return FormPivotTableQuery(subtableName, idColumn, valueColumn, descriptionColumn, null);
		}

		/// <summary>
		/// generates a pivot table query and returns it
		/// </summary>
		/// <param name="subtableName"></param>
		/// <param name="idColumn"></param>
		/// <param name="mainTableName"></param>
		/// <param name="columnPrefix"></param>
		/// <returns>query string</returns>
		public static string FormPivotTableQuery(string subtableName, string idColumn, string valueColumn, string descriptionColumn, string groupBy)
		{
			string query = "";
			int index = 0;

			//return current subtable - should be a 2nd normal form identity table
			string selectSubTable = "SELECT " + idColumn + ", " + descriptionColumn + " FROM " + subtableName;
			if (groupBy != null)
			{
				selectSubTable += " GROUP BY " + groupBy;
			}

			DataTable subTable = KanevaGlobals.GetDatabaseUtility ().GetDataTable(selectSubTable);

			if (subTable.Rows.Count > 0)
			{
				//loops through results to process all rows
				foreach(DataRow row in subTable.Rows)
				{
					query += "SUM(IF(" + idColumn + "=" + row[idColumn].ToString() + ", " + valueColumn + ", 0)) AS " + row[descriptionColumn].ToString() + ", ";
					index++;
				}
				//append the total
				query += " SUM(" + valueColumn + ") as total_points";

			}
			else
			{
				return null;
			}
			
			return query;
		}

        #endregion

        #region asset_upload related

        /// <summary>
        /// Obtain the max size of a file user can upload, in MB
        /// </summary>
        /// <returns></returns>
        public static int GetMaxUploadFileSize()
        {
            string sqlSelect = "SELECT max_upload_filesize FROM configuration";
            return KanevaGlobals.GetDatabaseUtility ().ExecuteScalar(sqlSelect);
        }

        /// <summary>
        /// Has the upload expired?
        /// </summary>
        public static bool HasUploadExpired (int assetId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            int timeOutInHours = GetFileUploadTimeout ();

            string sqlSelect = "SELECT au.created_datetime " +
                " FROM asset_upload au, assets a " +
                " WHERE au.asset_id = @assetId " +
                " AND au.asset_id = a.asset_id " +
                " AND a.publish_status_id = @publishStatusId " +
                " AND " + dbUtility.GetDatePlusMinutes (60 * timeOutInHours, "au.created_datetime") + " > " + dbUtility.GetCurrentDateFunction () ;

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@publishStatusId", (int) Constants.ePUBLISH_STATUS.UPLOADING);
            DataRow drAsset = dbUtility.GetDataRow (sqlSelect, parameters, false);
            return (drAsset == null);
        }

        /// <summary>
        /// Obtain the time a user has to complete the uploading, in hours
        /// </summary>
        /// <returns></returns>
        public static int GetFileUploadTimeout()
        {
            string sqlSelect = "SELECT file_upload_timeout_hours FROM configuration";
            return KanevaGlobals.GetDatabaseUtility ().ExecuteScalar(sqlSelect);
        }

        /// <summary>
        /// see if asset with same md5 hash is marked for deletion
        /// </summary>
        /// <param name="hash"></param>
        /// <returns></returns>
        public static bool CheckAssetMarkedForDeletion(string hash, int userId)
        {
            Hashtable parameters = new Hashtable ();

            // Make sure this file name does not already exist
            StringBuilder sb = new StringBuilder() ;
            sb.Append(" SELECT COUNT(*) ");
            sb.Append(" FROM asset_upload au ");
            sb.Append(" INNER JOIN assets a ON au.asset_id = a.asset_id ");
            sb.Append(" WHERE au.MD5_hash = @hash ");
            sb.Append(" AND a.status_id = ").Append((int) Constants.eASSET_STATUS.MARKED_FOR_DELETION);
            sb.Append(" AND a.owner_id = @ownerId");

            parameters.Add ("@ownerId", userId);
            parameters.Add ("@hash", hash);
            int count = KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sb.ToString(), parameters);
            return count > 0;
        }

        /// <summary>
        /// true if asset with same hash already exists
        /// </summary>
        /// <param name="hash"></param>
        /// <returns></returns>
        public static bool CheckAssetUploadFileExist(string hash)
        {
            Hashtable parameters = new Hashtable ();

            // Make sure this file name does not already exist
            StringBuilder sb = new StringBuilder() ;
            sb.Append("SELECT COUNT(*) ");
            sb.Append("FROM asset_upload au ");
            sb.Append("INNER JOIN assets a ON au.asset_id = a.asset_id ");
            sb.Append("WHERE au.MD5_hash = @hash ");
            sb.Append("AND a.status_id <> ").Append((int) Constants.eASSET_STATUS.DELETED);

            parameters.Add ("@hash", hash);
            int count = KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sb.ToString(), parameters);
            return count > 0;
        }

        /// <summary>
        /// if requesting hash, filename, etc info match a file that's being uploaded, return the id of that record
        /// </summary>
        /// <param name="userId">id of the requesting user</param>
        /// <param name="fileName">name of the file</param>
        /// <param name="hash">md5 hash of the file</param>
        /// <returns></returns>
        public static int GetUploadingAssetUploadId(int userId, string fileName, string hash)
        {
            Hashtable parameters = new Hashtable ();

            // Make sure this file name does not already exist
            StringBuilder sb = new StringBuilder() ;
            sb.Append(" SELECT a.asset_id ");
            sb.Append(" FROM asset_upload au ");
            sb.Append(" INNER JOIN assets a ON au.asset_id = a.asset_id ");
            sb.Append(" WHERE au.fileName = @fileName ");
            sb.Append(" AND a.owner_id = @user_id ");
            sb.Append(" AND au.MD5_hash = @MD5_hash ");
            sb.Append(" AND a.publish_status_id = ").Append((int)Constants.ePUBLISH_STATUS.UPLOADING);
            sb.Append(" AND a.status_id <> ").Append((int) Constants.eASSET_STATUS.DELETED);
            //sb.Append(" AND a.status_id NOT IN (");
            //sb.Append((int) Constants.eASSET_STATUS.DELETED);
            //sb.Append(" ,");
            //sb.Append((int) Constants.eASSET_STATUS.MARKED_FOR_DELETION);
            //sb.Append(")");

            parameters.Add ("@fileName", fileName);
            parameters.Add ("@user_id", userId);
            parameters.Add ("@MD5_hash", hash);

            int id = KanevaGlobals.GetDatabaseUtility ().ExecuteScalar (sb.ToString(), parameters);

            return id;
        }

        /// <summary>
        /// insert a row in asset_upload table
        /// </summary>
        /// <param name="userId">id of the requesting user</param>
        /// <param name="fileName">name of the file</param>
        /// <param name="path">parent directory where the file is to be stored</param>
        /// <param name="size">length (in bytes) of the file</param>
        /// <param name="md5Hash">MD5 hash</param>
        /// <param name="typeId">game or asset</param>
        /// <param name="assetId">assetId</param>
        /// <returns></returns>
        public static int InsertAssetUpload(int userId,
            string fileName,
            string path,
            long size,
            string md5Hash,
            int typeId,
            int assetId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            StringBuilder sb = new StringBuilder() ;
            sb.Append("INSERT into asset_upload ");
            sb.Append("(user_id, asset_id, path, filename, size, MD5_hash, type_id, created_datetime) ");
            sb.Append("VALUES ");
            sb.Append("(@user_id, @assetId, @path, @filename, @size, @MD5_hash, @type_id, ");
            sb.Append(dbUtility.GetCurrentDateFunction ());
            sb.Append(")");

            int assetUploadId = 0;
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@user_id", userId);
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@path", path);
            parameters.Add ("@filename", fileName);
            parameters.Add ("@size", size >= 0? size.ToString() : null);
            parameters.Add ("@MD5_hash", md5Hash);
            parameters.Add ("@type_id", typeId);
            //parameters.Add ("@created_datetime", dbUtility.GetCurrentDateFunction ());
            dbUtility.ExecuteIdentityInsert (sb.ToString(), parameters, ref assetUploadId);

            return assetUploadId;
        }

        /// <summary>
        /// insert a row in game_upload table
        /// </summary>
        /// <param name="userId">id of the requesting user</param>
        /// <param name="path">parent directory where the file is to be stored</param>
        /// <param name="assetId">assetId</param>
        /// <returns></returns>
        public static int InsertGameUpload(
            int userId,
            string path,
            int assetId)
        {
            DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

            StringBuilder sb = new StringBuilder() ;
            sb.Append(" INSERT into game_upload ");
            sb.Append(" (user_id, asset_id, path, created_datetime, publish_status_id) ");
            sb.Append(" VALUES ");
            sb.Append(" (@user_id, @assetId, @path, ");
            sb.Append( dbUtility.GetCurrentDateFunction ());
            sb.Append(" , ");
            sb.Append((int) Constants.eGAME_UPLOAD_PUBLISH_STATUS.UPLOADING);
            sb.Append(" )");

            int assetUploadId = 0;
            Hashtable parameters = new Hashtable ();
            parameters.Add ("@user_id", userId);
            parameters.Add ("@assetId", assetId);
            parameters.Add ("@path", path);
            dbUtility.ExecuteIdentityInsert (sb.ToString(), parameters, ref assetUploadId);

            return assetUploadId;
        }

        /// <summary>
        /// update path of an asset upload row
        /// </summary>
        /// <param name="assetUploadId"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static int UpdateAssetUploadPath(int assetUploadId, string path)
        {
            //update status
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            StringBuilder sb = new StringBuilder() ;
            sb.Append("UPDATE asset_upload ");
            sb.Append(" SET path = @path ");
            sb.Append(" WHERE asset_upload_id = @asset_upload_id ");

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@path", path);
            parameters.Add ("@asset_upload_id", assetUploadId);
            int result = dbUtility.ExecuteNonQuery (sb.ToString(), parameters);
            return result;
        }

        /// <summary>
        /// Get the Asset upload record for the given assetId
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public static DataRow GetAssetUploadByAssetId(int assetId)
        {
            StringBuilder sb = new StringBuilder() ;
            sb.Append(" SELECT a.asset_id, a.status_id, a.owner_id, au.path, au.filename, au.size, au.MD5_hash, ");
            sb.Append(" au.type_id, au.created_datetime, au.completed_datetime ");
            sb.Append(" FROM asset_upload au ");
            sb.Append(" INNER JOIN assets a on au.asset_id = a.asset_id ");
            sb.Append(" WHERE a.asset_id = @assetId");

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@assetId", assetId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sb.ToString(), parameters, true);
        }

        /// <summary>
        /// Get the Asset upload record for the given assetUploadId, applies to game only
        /// </summary>
        /// <param name="gameUploadId"></param>
        /// <returns></returns>
        public static DataRow GetGameUpload(int gameUploadId)
        {
            StringBuilder sb = new StringBuilder() ;
            sb.Append(" SELECT a.asset_id, a.owner_id, gu.game_upload_id, gu.path, gu.publish_status_id ");
            sb.Append(" FROM game_upload gu ");
            sb.Append(" INNER JOIN assets a ON a.asset_id = gu.asset_id  ");
            sb.Append(" WHERE gu.game_upload_id = @gameUploadId");

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@gameUploadId", gameUploadId);
            return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sb.ToString(), parameters, true);
        }

		/// <summary>
		/// update the runtime of a asset row
		/// </summary>
		/// <param name="assetId"></param>
		/// <param name="status"></param>
		/// <returns></returns>
		public static int UpdateAssetRuntime(int assetId, ulong runTime)
		{
			//update status
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			StringBuilder sb = new StringBuilder() ;
			sb.Append("UPDATE assets ");
			sb.Append("SET run_time_seconds = @runTime, ");
			sb.Append(" last_updated_date = ");
			sb.Append(dbUtility.GetCurrentDateFunction ());
			sb.Append(" WHERE asset_id = @asset_id ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@asset_id", assetId);
			parameters.Add ("@runTime", runTime);

			return dbUtility.ExecuteNonQuery (sb.ToString(), parameters);
		}

        /// <summary>
        /// update the publish_status_id of a asset row
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static int UpdateAssetPublishStatus(int assetId, int status, ulong runTime)
        {
            //update status
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            StringBuilder sb = new StringBuilder() ;
            sb.Append("UPDATE assets ");
            sb.Append("SET publish_status_id = @publish_status_id, ");
			sb.Append(" run_time_seconds = @runTime, ");
            sb.Append(" last_updated_date = ");
            sb.Append(dbUtility.GetCurrentDateFunction ());
            sb.Append(" WHERE asset_id = @asset_id ");

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@publish_status_id", status);
            parameters.Add ("@asset_id", assetId);
            parameters.Add ("@runTime", runTime);

            return dbUtility.ExecuteNonQuery (sb.ToString(), parameters);
        }

		/// <summary>
		/// update the percent_complete for media that is being converted to a different format
		/// (current .flv)
		/// </summary>
		/// <param name="assetId"></param>
		/// <param name="percent_complete"></param>
		/// <returns></returns>
		public static int UpdateAssetConversionPercentComplete(int assetId, int percent_complete)
		{
			//update status
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			StringBuilder sb = new StringBuilder() ;
			sb.Append("UPDATE assets ");
			sb.Append("SET percent_complete = @percentComplete");
			sb.Append(" WHERE asset_id = @asset_id ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@percentComplete", percent_complete);
			parameters.Add ("@asset_id", assetId);

			return dbUtility.ExecuteNonQuery (sb.ToString(), parameters);
		}

		/// <summary>
		/// update statistics for the publishing process to help identify when new media converter machines need
		/// to be brought online.
		/// </summary>
		/// <param name="assetsToConvert">the number of assets that were identified for conversion in a single publishing run</param>
		/// <param name="assetsConverted">the number of assets that were converted</param>
		/// <returns></returns>
		public static int UpdatePublishingStats(int assetsToConvert, int assetsConverted)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			StringBuilder sb = new StringBuilder() ;
			sb.Append("INSERT INTO stats_publishing_processor ");
			sb.Append(" (start_time, num_processed, num_to_process) VALUES (");
			sb.Append(" @start_time, @num_processed, @num_to_process) ");

			parameters.Add ("@start_time",  dbUtility.GetCurrentDateTime());
			parameters.Add ("@num_processed", assetsConverted);
			parameters.Add ("@num_to_process", assetsToConvert);

			return dbUtility.ExecuteNonQuery (sb.ToString(), parameters);
		}

        /// <summary>
        /// update the publish_status_id of a asset row, also completed_date if status is uploaded
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static int UpdateAssetUploadStatus(int assetId, int status)
        {
            return UpdateAssetUploadStatus(assetId, status, null);
        }

        /// <summary>
        /// update the publish_status_id of a asset row, also completed_date if status is uploaded
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="status"></param>
        /// <param name="ip">ip address that completed that update</param>
        /// <returns></returns>
        public static int UpdateAssetUploadStatus(int assetId, int status, string ip)
        {
            //update status
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            StringBuilder sb = new StringBuilder() ;
            sb.Append(" UPDATE assets ");
            sb.Append(" SET publish_status_id = @publish_status_id, ");
            sb.Append(" last_updated_date = ");
            sb.Append(dbUtility.GetCurrentDateFunction ());
            sb.Append(" WHERE asset_id = @assetId ");

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@publish_status_id", status);
            parameters.Add ("@assetId", assetId);
            int result = dbUtility.ExecuteNonQuery (sb.ToString(), parameters);

            //update completed_date
            if(result > 0 && status == (int) Constants.ePUBLISH_STATUS.UPLOADED)
            {
                sb = new StringBuilder() ;
                sb.Append(" UPDATE asset_upload au ");
                sb.Append(" INNER JOIN assets a ON au.asset_id = a.asset_id ");
                sb.Append(" SET au.completed_datetime = ");
                sb.Append(dbUtility.GetCurrentDateFunction ());
                sb.Append(" WHERE a.asset_id = @assetId");

                parameters = new Hashtable ();
                parameters.Add ("@assetId", assetId);
                result = dbUtility.ExecuteNonQuery (sb.ToString(), parameters);

                //update ip address
                sb = new StringBuilder() ;
                sb.Append(" UPDATE assets ");
                sb.Append(" SET ip_address = @ip");
                sb.Append(" WHERE asset_id = @assetId");

                parameters = new Hashtable ();
                parameters.Add ("@ip", ip);
                parameters.Add ("@assetId", assetId);
                result = dbUtility.ExecuteNonQuery (sb.ToString(), parameters);
            }
            return result;
        }

        /// <summary>
        /// update the publish_status_id of a game_upload row, also completed_date if status is uploaded
        /// </summary>
        /// <param name="gameUploadId"></param>
        /// <param name="statusId"></param>
        /// <param name="ip">ip address that completed that update, pass null if not completed</param>
        /// <returns></returns>
        public static int UpdateGameUploadStatus(int gameUploadId, int statusId, string ip)
        {
            //update status
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            StringBuilder sb = new StringBuilder() ;
            sb.Append(" UPDATE game_upload ");
            sb.Append(" SET publish_status_id = @statusId ");
            sb.Append(" WHERE game_upload_id = @gameUploadId ");

            Hashtable parameters = new Hashtable ();
            parameters.Add ("@statusId", statusId);
            parameters.Add ("@gameUploadId", gameUploadId);
            int result = dbUtility.ExecuteNonQuery (sb.ToString(), parameters);

            //update completed_date and ip address
            if(result > 0 && statusId == (int) Constants.eGAME_UPLOAD_PUBLISH_STATUS.UPLOADED)
            {
                sb = new StringBuilder() ;
                sb.Append(" UPDATE game_upload ");
                sb.Append(" SET completed_datetime = ");
                sb.Append(dbUtility.GetCurrentDateFunction ());
                sb.Append(" , ");
                sb.Append(" ip_address = @ip ");
                sb.Append(" WHERE game_upload_id = @gameUploadId ");

                parameters = new Hashtable ();
                parameters.Add ("@gameUploadId", gameUploadId);
                parameters.Add ("@ip", ip);
                result = dbUtility.ExecuteNonQuery (sb.ToString(), parameters);
            }
            return result;
        }

		/// <summary>
		/// get assets to migrate, by looking up publish status id
		/// </summary>
		/// <returns></returns>
		public static DataTable GetAssetsToMigrateImages ()
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sb = "SELECT a.asset_id, a.image_data, a.image_path, a.image_type, a.owner_id " +
				" FROM assets a " +
				" WHERE a.status_id NOT IN (" + (int) Constants.eASSET_STATUS.MARKED_FOR_DELETION + "," + (int) Constants.eASSET_STATUS.DELETED + ")" +
				" AND a.image_data IS NOT NULL ";

			Hashtable parameters = new Hashtable ();
			return dbUtility.GetDataTable (sb, parameters);
		}
		/// <summary>
        /// get assets to process, by looking up publish status id
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAssetsToProcess()
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            StringBuilder sb = new StringBuilder();
            sb.Append(" SELECT a.asset_id, a.owner_id, au.filename, au.path, a.publish_status_id ");
            sb.Append(" FROM assets a ");
            sb.Append(" INNER JOIN asset_upload au ON au.asset_id = a.asset_id ");
            sb.Append(" WHERE a.publish_status_id IN ( ");
            sb.Append((int) Constants.ePUBLISH_STATUS.UPLOADED);
            sb.Append(" , ");
            sb.Append((int) Constants.ePUBLISH_STATUS.TORRENT_IMPORTED);
            sb.Append(" ) ");
            sb.Append(" AND a.status_id NOT IN (");
            sb.Append((int) Constants.eASSET_STATUS.MARKED_FOR_DELETION);
            sb.Append(", ");
            sb.Append((int) Constants.eASSET_STATUS.DELETED);
            sb.Append(")");
            sb.Append(" AND (a.asset_type_id <> ").Append((int) Constants.eASSET_TYPE.GAME);
            sb.Append(" OR a.is_kaneva_game = 0 ) AND au.user_id <> 0");

            Hashtable parameters = new Hashtable ();
            return dbUtility.GetDataTable (sb.ToString(), parameters);
        }

        /// <summary>
        /// get game upload to process, by looking up publish status id
        /// </summary>
        /// <returns></returns>
        public static DataTable GetGameUploadsToProcess()
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            StringBuilder sb = new StringBuilder();
            sb.Append(" SELECT gu.game_upload_id, a.asset_id, a.owner_id, ");
            sb.Append(" gu.path, gl.IT_game_directory, gu.publish_status_id, ");
            sb.Append(" a.name, u.email ");
            sb.Append(" FROM assets a ");
            sb.Append(" INNER JOIN game_upload gu ON gu.asset_id = a.asset_id ");
            sb.Append(" INNER JOIN game_license gl ON gl.asset_id = a.asset_id ");
            sb.Append(" INNER JOIN users u ON u.user_id = a.owner_id ");
            sb.Append(" WHERE gu.publish_status_id = ").Append(
                (int) Constants.eGAME_UPLOAD_PUBLISH_STATUS.UPLOADED);
            sb.Append(" AND a.status_id NOT IN (");
            sb.Append((int) Constants.eASSET_STATUS.MARKED_FOR_DELETION);
            sb.Append(", ");
            sb.Append((int) Constants.eASSET_STATUS.DELETED);
            sb.Append(")");
            sb.Append(" AND gl.license_type = ").Append((int) Constants.eLICENSE_SUBSCRIPTIONS.STAGING);
            sb.Append(" AND gl.status_id = ").Append((int) Constants.eGAME_LICENSE_STATUS.ACTIVE);
            sb.Append(" AND a.asset_type_id = ").Append((int) Constants.eASSET_TYPE.GAME);

            Hashtable parameters = new Hashtable ();
            return dbUtility.GetDataTable (sb.ToString(), parameters);
        }

        /// <summary>
        /// get partially uploaded assets that are marked for deletion
        /// </summary>
        /// <returns></returns>
        public static DataTable GetDeletedPartialUploads()
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            StringBuilder sb = new StringBuilder();
            sb.Append(" SELECT a.asset_id, a.publish_status_id, au.asset_upload_id, ");
            sb.Append(" path, filename FROM assets a ");
            sb.Append(" INNER JOIN asset_upload au ON au.asset_id = a.asset_id ");
            sb.Append(" WHERE a.asset_type_id <> ").Append((int) Constants.eASSET_TYPE.GAME);
            sb.Append(" AND a.status_id = ").Append((int) Constants.eASSET_STATUS.MARKED_FOR_DELETION);
            sb.Append(" AND a.publish_status_id IN (");
            sb.Append((int) Constants.ePUBLISH_STATUS.UPLOADING);
            sb.Append(", ");
            sb.Append((int) Constants.ePUBLISH_STATUS.UPLOADED);
            sb.Append(")");

            Hashtable parameters = new Hashtable ();
            return dbUtility.GetDataTable (sb.ToString(), parameters);
        }

        /// <summary>
        /// get partially uploaded assets that are expired
        /// </summary>
        /// <param name="cutoffTime">assets posted on or before this time that are not finished are selected</param>
        /// <returns></returns>
        public static DataTable GetExpiredUploads(DateTime cutoffTime)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            StringBuilder sb = new StringBuilder();

            sb.Append(" SELECT a.asset_id, a.publish_status_id, au.asset_upload_id, ");
            sb.Append(" au.created_datetime, path, filename FROM assets a ");
            sb.Append(" INNER JOIN asset_upload au ON au.asset_id = a.asset_id ");
            sb.Append(" WHERE a.asset_type_id <> ").Append((int) Constants.eASSET_TYPE.GAME);
            sb.Append(" AND a.status_id NOT IN (");
            sb.Append((int) Constants.eASSET_STATUS.MARKED_FOR_DELETION);
            sb.Append(", ");
            sb.Append((int) Constants.eASSET_STATUS.DELETED);
            sb.Append(")");
            sb.Append(" AND a.publish_status_id = ").Append((int) Constants.ePUBLISH_STATUS.UPLOADING);
            sb.Append(" AND au.created_datetime < @cutoffTime ");

            Hashtable parameters = new Hashtable ();
            parameters.Add("@cutoffTime", cutoffTime);
            return dbUtility.GetDataTable (sb.ToString(), parameters);
        }

        /// <summary>
        /// returns a list of commercial license keys, if any, for a game
        /// </summary>
        /// <param name="gameId"></param>
        /// <returns></returns>
        public static DataTable GetCommercialLicenses(int gameId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            StringBuilder sb = new StringBuilder();

            sb.Append(" SELECT gl.game_key ");
            sb.Append(" FROM assets a ");
            sb.Append(" INNER JOIN game_license gl ON gl.asset_id = a.asset_id ");
            sb.Append(" WHERE a.asset_type_id = ").Append((int)Constants.eASSET_TYPE.GAME);
            sb.Append(" AND gl.status_id = ").Append((int)Constants.eGAME_LICENSE_STATUS.ACTIVE);
            sb.Append(" AND gl.license_type = ").Append((int) Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL);
            sb.Append(" AND a.asset_id = @gameId");

            Hashtable parameters = new Hashtable ();
            parameters.Add("@gameId", gameId);
            return dbUtility.GetDataTable (sb.ToString(), parameters);
        }

		/// <summary>
		/// returns a list of pilot license keys, if any, for a game
		/// </summary>
		/// <param name="gameId"></param>
		/// <returns></returns>
		public static DataTable GetPilotLicenses(int gameId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			StringBuilder sb = new StringBuilder();

			sb.Append(" SELECT gl.game_key ");
			sb.Append(" FROM assets a ");
			sb.Append(" INNER JOIN game_license gl ON gl.asset_id = a.asset_id ");
			sb.Append(" WHERE a.asset_type_id = ").Append((int)Constants.eASSET_TYPE.GAME);
			sb.Append(" AND gl.status_id = ").Append((int)Constants.eGAME_LICENSE_STATUS.ACTIVE);
			sb.Append(" AND gl.license_type = ").Append((int) Constants.eLICENSE_SUBSCRIPTIONS.PILOT);
			sb.Append(" AND a.asset_id = @gameId");

			Hashtable parameters = new Hashtable ();
			parameters.Add("@gameId", gameId);
			return dbUtility.GetDataTable (sb.ToString(), parameters);
		}

		/// <summary>
		/// returns a list of dev license keys, if any, for a game
		/// </summary>
		/// <param name="gameId"></param>
		/// <returns></returns>
		public static DataTable GetDevLicenses(int gameId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			StringBuilder sb = new StringBuilder();

			sb.Append(" SELECT gl.game_key ");
			sb.Append(" FROM assets a ");
			sb.Append(" INNER JOIN game_license gl ON gl.asset_id = a.asset_id ");
			sb.Append(" WHERE a.asset_type_id = ").Append((int)Constants.eASSET_TYPE.GAME);
			sb.Append(" AND gl.status_id = ").Append((int)Constants.eGAME_LICENSE_STATUS.ACTIVE);
			sb.Append(" AND gl.license_type = ").Append((int) Constants.eLICENSE_SUBSCRIPTIONS.DEVELOPER);
			sb.Append(" AND a.asset_id = @gameId");

			Hashtable parameters = new Hashtable ();
			parameters.Add("@gameId", gameId);
			return dbUtility.GetDataTable (sb.ToString(), parameters);
		}

        /// <summary>
        /// returns a list of gameids, encryption keys, and dev licenses for given user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static DataTable GetAvailableGames(int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

            StringBuilder sb = new StringBuilder();

            sb.Append(" SELECT a.name, a.asset_id, a.game_encryption_key, gl_dev.game_key, ");
            sb.Append(" count(gl_comm.game_license_id) as num_comm_licenses ");
            sb.Append(" FROM assets a ");
            sb.Append(" INNER JOIN game_license gl_dev ON gl_dev.asset_id = a.asset_id ");
            sb.Append(" LEFT JOIN game_license gl_comm ON gl_comm.asset_id = a.asset_id  ");
            sb.Append("     AND gl_comm.status_id = ").Append((int)Constants.eGAME_LICENSE_STATUS.ACTIVE);
            sb.Append("     AND gl_comm.license_type = ").Append((int)Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL);
            sb.Append(" WHERE a.asset_type_id = ").Append((int)Constants.eASSET_TYPE.GAME);
            sb.Append(" AND gl_dev.status_id = ").Append((int)Constants.eGAME_LICENSE_STATUS.ACTIVE);
            sb.Append(" AND gl_dev.license_type = ").Append((int) Constants.eLICENSE_SUBSCRIPTIONS.DEVELOPER);
            sb.Append(" AND a.status_id NOT IN (");
            sb.Append((int) Constants.eASSET_STATUS.MARKED_FOR_DELETION);
            sb.Append(", ");
            sb.Append((int) Constants.eASSET_STATUS.DELETED);
            sb.Append(")");
			sb.Append(" AND a.name <> \"\"");
            sb.Append(" AND a.owner_id = @userId");
            sb.Append(" GROUP BY a.name, a.asset_id, a.game_encryption_key, gl_dev.game_key ");

            Hashtable parameters = new Hashtable ();
            parameters.Add("@userId", userId);
            return dbUtility.GetDataTable (sb.ToString(), parameters);
        }

        /// <summary>
        /// Obtain the staging directory for a game
        /// if more than one staging licenses are setupin db (which is wrong), the first record will be used
        /// </summary>
        /// <returns></returns>
        public static string GetGameStagingDirectory(int gameId)
        {
            string sqlSelect = " SELECT gl.IT_game_directory " +
                " FROM game_license gl " +
                " WHERE gl.license_type = " + (int) Constants.eLICENSE_SUBSCRIPTIONS.STAGING +
                " AND gl.asset_id = @gameId";
            Hashtable parameters = new Hashtable ();
            parameters.Add("@gameId", gameId);

            DataTable dt = KanevaGlobals.GetDatabaseUtility ().GetDataTable(sqlSelect, parameters);

            string retVal = null;
            if(dt.Rows.Count > 0)
            {
                retVal = dt.Rows[0].IsNull("IT_game_directory") ? null :
                    dt.Rows[0]["IT_game_directory"].ToString();
            }
            return retVal;
        }

        /// <summary>
        /// returns a true if a new version of given game is uploaded but not processed
        /// </summary>
        /// <param name="gameId"></param>
        /// <returns></returns>
        public static bool IsGameUploadBeingProcessed(int gameId)
        {
            string sqlSelect = " SELECT COUNT(game_upload_id) " +
                " FROM game_upload " +
                " WHERE asset_id = @gameId AND publish_status_id = " +
                (int) Constants.eGAME_UPLOAD_PUBLISH_STATUS.UPLOADED;

            Hashtable parameters = new Hashtable ();
            parameters.Add("@gameId", gameId);

            return KanevaGlobals.GetDatabaseUtility().ExecuteScalar(sqlSelect, parameters) > 0;
        }

        #endregion

        // **********************************************************************************************
        // Promotional Offers
        // **********************************************************************************************
        #region Promotional Offer Administration

        /// <summary>
        /// Get a GiftCard - for pull down
        /// </summary>
        /// <returns></returns>
        public static DataTable GetGiftCards()
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

           string sqlSelect = "SELECT gift_card_id, description FROM gift_cards ";

           return dbUtility.GetDataTable(sqlSelect);
        }

        /// <summary>
        /// Get KEITypes - for pull down
        /// </summary>
        /// <returns></returns>
        public static DataTable GetKEITypes()
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlSelect = "SELECT kei_point_id, description FROM kei_points ";

            return dbUtility.GetDataTable(sqlSelect);
        }

        /// <summary>
        /// Get KEITypes - for pull down
        /// </summary>
        /// <returns></returns>
        public static DataTable GetPromotionalOfferTypes()
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlSelect = "SELECT promotional_offer_type_id, promotional_offer_type, description FROM promotional_offer_type ";

            return dbUtility.GetDataTable(sqlSelect);
        }


        /// <summary>
        /// GetPromotionalOffers
        /// </summary>
        /// <returns></returns>
        public static DataTable GetPromotionalOffers()
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlSelect = "SELECT po.promotion_id, bundle_title, bundle_subheading1, bundle_subheading2," +
                " dollar_amount, kei_point_amount, promotion_start, promotion_end, is_special," +
                " promotion_list_heading, promotion_description, highlight_color, special_background_color," +
                " special_background_image, special_sticker_image, promotional_package_label," + 
                " kei_point_id, free_points_awarded_amount, free_kei_point_ID, promotional_offers_type_id," +
                " wok_pass_group_id, sku, special_font_color, value_of_credits" +
                " FROM promotional_offers po ";

            return dbUtility.GetDataTable(sqlSelect);
        }

        public static int UpdatePromotionalOffers(int promotionid, string bundletitle, string bundlesubheading1, string bundlesubheading2, decimal pricedollars, decimal pricecredits,
            DateTime promotionstart, DateTime promotionend, bool isspecial, string promotionlistheading, string promotiondescription, string highlightcolor, string specialbackgroundcolor,
            string specialbackgroundimage, string specialstickerimage, string promotionalpackagelabel, string keipointId, decimal freepointsawardedamount, string freekeipointID,
            int promotionalofferstypeid, int wokpassgroupid, int sku, decimal valueofcredits, string specialfontcolor, int CSRid)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string query = " UPDATE promotional_offers " +
            " SET bundle_title = @bundletitle, " +
            " bundle_subheading1 = @bundlesubheading1, " +
            " bundle_subheading2 = @bundlesubheading2, " +
            " dollar_amount = @pricedollars, " +
            " kei_point_amount = @pricecredits, " +
            " promotion_start = @promotionstart, " +
            " promotion_end = @promotionend, " +
            " is_special = @isspecial, " +
            " promotion_list_heading = @promotionlistheading, " +
            " promotion_description = @promotiondescription, " +
            " highlight_color = @highlightcolor, " +
            " special_background_color = @specialbackgroundcolor, " +
            " special_background_image = @specialbackgroundimage, " +
            " special_sticker_image = @specialstickerimage, " +
            " promotional_package_label = @promotionalpackagelabel, " +
            " kei_point_id = @keipointId, " +
            " free_points_awarded_amount = @freepointsawardedamount, " +
            " free_kei_point_ID = @freekeipointID, " +
            " promotional_offers_type_id = @promotionalofferstypeid, " +
            " wok_pass_group_id = @wokpassgroupid, " +
            " sku = @sku, " +
            " special_font_color = @specialfontcolor, " +
            " value_of_credits = @valueofcredits, " +
            " modifiers_id = @CSRid " +
            " WHERE promotion_id = @promotionid ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@bundletitle", bundletitle);
            parameters.Add("@bundlesubheading1", bundlesubheading1);
            parameters.Add("@bundlesubheading2", bundlesubheading2);
            parameters.Add("@pricedollars", pricedollars);
            parameters.Add("@pricecredits", pricecredits);
            parameters.Add("@promotionstart", promotionstart);
            parameters.Add("@promotionend", promotionend);
            parameters.Add("@isspecial", isspecial ? 1 : 0);
            parameters.Add("@promotionlistheading", promotionlistheading);
            parameters.Add("@promotiondescription", promotiondescription);
            parameters.Add("@highlightcolor", highlightcolor);
            parameters.Add("@specialbackgroundcolor", specialbackgroundcolor);
            parameters.Add("@specialbackgroundimage", specialbackgroundimage);
            parameters.Add("@specialstickerimage", specialstickerimage);
            parameters.Add("@promotionalpackagelabel", promotionalpackagelabel);
            parameters.Add("@keipointId", keipointId);
            parameters.Add("@freepointsawardedamount", freepointsawardedamount);
            parameters.Add("@freekeipointID", freekeipointID);
            parameters.Add("@promotionalofferstypeid", promotionalofferstypeid);
            parameters.Add("@wokpassgroupid", wokpassgroupid);
            parameters.Add("@sku", sku);
            parameters.Add("@specialfontcolor", specialfontcolor);
            parameters.Add("@valueofcredits", valueofcredits);
            parameters.Add("@CSRid", CSRid);
            parameters.Add("@promotionid", promotionid);


            return dbUtility.ExecuteNonQuery(query, parameters);
        }

        public static int InsertPromotionalOffers(string bundletitle, string bundlesubheading1, string bundlesubheading2, decimal pricedollars, decimal pricecredits, 
            DateTime promotionstart, DateTime promotionend, bool isspecial, string promotionlistheading, string promotiondescription, string highlightcolor, string specialbackgroundcolor,
            string specialbackgroundimage, string specialstickerimage, string promotionalpackagelabel, string keipointId, decimal freepointsawardedamount, string freekeipointID,
            int promotionalofferstypeid, int wokpassgroupid, int sku, decimal valueofcredits, string specialfontcolor, int CSRid)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            // Record User Asset Subscriptions
            string sqlInsert = "INSERT INTO promotional_offers " +
                " (bundle_title, bundle_subheading1, bundle_subheading2, dollar_amount, kei_point_amount, promotion_start, promotion_end, " +
                " is_special, promotion_list_heading, promotion_description, highlight_color, special_background_color, special_background_image, " +
                " special_sticker_image, promotional_package_label, kei_point_id, free_points_awarded_amount, free_kei_point_ID, promotional_offers_type_id, " +
                " wok_pass_group_id, sku, special_font_color, value_of_credits, modifiers_id) " +
                " VALUES " +
                " (@bundletitle, @bundlesubheading1, @bundlesubheading2, @pricedollars, @pricecredits, @promotionstart, @promotionend, " +
                " @isspecial, @promotionlistheading, @promotiondescription, @highlightcolor, @specialbackgroundcolor, @specialbackgroundimage, " +
                " @specialstickerimage, @promotionalpackagelabel, @keipointId, @freepointsawardedamount, @freekeipointID, @promotionalofferstypeid, " +
                " @wokpassgroupid, @sku, @specialfontcolor, @valueofcredits, @CSRid)";

            Hashtable parameters = new Hashtable();
            parameters.Add("@bundletitle", bundletitle);
            parameters.Add("@bundlesubheading1", bundlesubheading1);
            parameters.Add("@bundlesubheading2", bundlesubheading2);
            parameters.Add("@pricedollars", pricedollars);
            parameters.Add("@pricecredits", pricecredits);
            parameters.Add("@promotionstart", promotionstart);
            parameters.Add("@promotionend", promotionend);
            parameters.Add("@isspecial", isspecial ? 1 : 0);
            parameters.Add("@promotionlistheading", promotionlistheading);
            parameters.Add("@promotiondescription", promotiondescription);
            parameters.Add("@highlightcolor", highlightcolor);
            parameters.Add("@specialbackgroundcolor", specialbackgroundcolor);
            parameters.Add("@specialbackgroundimage", specialbackgroundimage);
            parameters.Add("@specialstickerimage", specialstickerimage);
            parameters.Add("@promotionalpackagelabel", promotionalpackagelabel);
            parameters.Add("@keipointId", keipointId);
            parameters.Add("@freepointsawardedamount", freepointsawardedamount);
            parameters.Add("@freekeipointID", freekeipointID);
            parameters.Add("@promotionalofferstypeid", promotionalofferstypeid);
            parameters.Add("@wokpassgroupid", wokpassgroupid);
            parameters.Add("@sku", sku);
            parameters.Add("@specialfontcolor", specialfontcolor);
            parameters.Add("@valueofcredits", valueofcredits);
            parameters.Add("@CSRid", CSRid);

            int promotionid = -1;
            int entryId = dbUtility.ExecuteIdentityInsert(sqlInsert, parameters, ref promotionid);

            return promotionid;
        }

        public static void DeletePromotionalOffer(int id)
        {
            //delete from promotional_offer_items table
            string sqlString = "DELETE FROM promotional_offer_items WHERE promotion_id = @id ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@id", id);

            KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sqlString, parameters);
            
            //delete from contest table
            sqlString = "DELETE FROM promotional_offers WHERE promotion_id = @id ";

            KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sqlString, parameters);
        }

        public static DataTable GetPromotionalOfferItems(int promoId)
        {
            return GetPromotionalOfferItems(promoId, null);
        }

        public static DataTable GetPromotionalOfferItems(int promoId, string additionalFilter)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlSelect = "SELECT promotion_offer_id, market_price, promotion_id, gift_card_id, community_id, quantity, wok_item_id," +
                " item_description, alternate_description, gender " +
                " FROM promotional_offer_items poi " + 
                " WHERE promotion_id = @promoId";

            Hashtable parameters = new Hashtable();
            parameters.Add("@promoId", promoId);

            //check for additional filter
            if ((additionalFilter != null) && (additionalFilter.Length >= 1))
            {
                sqlSelect += " AND " + additionalFilter;
            }
            
            return dbUtility.GetDataTable(sqlSelect,parameters);
        }

        public static DataTable GetPromotionalOfferItemsByGender(int promoId, string gender)
        {
            return GetPromotionalOfferItemsByGender(promoId, gender, true);
        }

        public static DataTable GetPromotionalOfferItemsByGender(int promoId, string gender, bool nonGenderItemsIncluded)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string sqlSelect = "SELECT promotion_offer_id, market_price, promotion_id, gift_card_id, community_id, quantity, wok_item_id," +
                " item_description, alternate_description, gender " +
                " FROM promotional_offer_items poi " +
                " WHERE promotion_id = @promoId ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@promoId", promoId);
            parameters.Add("@gender", gender);

            //check for additional filter
            if (nonGenderItemsIncluded)
            {
                sqlSelect += " AND (gender = @gender OR gender = '" + Constants.GENDER_BOTH + "')";
            }
            else
            {
                sqlSelect += " AND gender = @gender";
            }

            return dbUtility.GetDataTable(sqlSelect, parameters);
        }

        public static int UpdatePromotionalOfferItems(int promotionItemId, int quantity, int wokItemId, string description, string gender,
            int giftcardid, int marketprice, int communityid, string alternatedescription, int CSRid)
        {
            Hashtable parameters = new Hashtable();
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            string query = " UPDATE promotional_offer_items " +
            " SET quantity = @quantity, " +
            " wok_item_id = @wokItemId, " +
            " item_description = @description, " +
            " gift_card_id = @giftcardid, " +
            " community_id = @communityid, " +
            " market_price = @marketprice, " +
            " gender = @gender, " +
            " alternate_description = @alternatedescription, " +
            " modifiers_id = @CSRid " +
            " WHERE promotion_offer_id =@promotionItemId ";

            parameters.Add("@quantity", quantity);
            parameters.Add("@wokItemId", wokItemId);
            parameters.Add("@description", description);
            parameters.Add("@giftcardid", giftcardid);
            parameters.Add("@communityid", communityid);
            parameters.Add("@marketprice", marketprice);
            parameters.Add("@gender", gender);
            parameters.Add("@alternatedescription", alternatedescription);
            parameters.Add("@CSRid", CSRid);
            parameters.Add("@promotionItemId", promotionItemId);

            return dbUtility.ExecuteNonQuery(query, parameters);
        }

        public static int InsertPromotionalOfferItems(int promotionId, int quantity, int wokItemId, string description, string gender,
            int giftcardid, int marketprice, int communityid, string alternatedescription, int CSRid)

        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            // Record User Asset Subscriptions
            string sqlInsert = "INSERT INTO promotional_offer_items " +
                " (promotion_id, quantity, wok_item_id, item_description, gender, market_price, community_id, gift_card_id, alternate_description, modifiers_id) " +
                " VALUES " +
                " (@promotionId, @quantity, @wokItemId, @description, @gender, @marketprice, @communityid, @giftcardid, @alternatedescription, @CSRid)";

            Hashtable parameters = new Hashtable();
            parameters.Add("@quantity", quantity);
            parameters.Add("@wokItemId", wokItemId);
            parameters.Add("@description", description);
            parameters.Add("@giftcardid", giftcardid);
            parameters.Add("@communityid", communityid);
            parameters.Add("@marketprice", marketprice); 
            parameters.Add("@gender", gender);
            parameters.Add("@promotionId", promotionId);
            parameters.Add("@alternatedescription", alternatedescription);
            parameters.Add("@CSRid", CSRid);

            int promotionItemsId = -1;
            int entryId = dbUtility.ExecuteIdentityInsert(sqlInsert, parameters, ref promotionItemsId);
            return promotionItemsId;
        }

        public static void DeletePromotionalOfferItem(int id)
        {
            //delete from promotional_offer_items table
            string sqlString = "DELETE FROM promotional_offer_items WHERE promotion_offer_id = @id ";

            Hashtable parameters = new Hashtable();
            parameters.Add("@id", id);

            KanevaGlobals.GetDatabaseUtility().ExecuteNonQuery(sqlString, parameters);
        }

        #endregion 

        #region ShowCase Widgets

        /// <summary>
		/// GetHotNewStuffAssets
		/// </summary>
		/// <param name="contestId"></param>
		/// <returns></returns>
		public static PagedDataTable GetHotNewStuffAssets (string image_path, string image_size, int channelId, bool bGetMature, bool bOnlyPublished, int pageNumber, int pageSize, DateTime startDate, DateTime endDate)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string selectList = " a.asset_id, a.asset_type_id, a.name, a.thumbnail_gen, " +
				" a.asset_rating_id, a.status_id, a.owner_id as user_id, a.owner_username as username, " +
				" a." + image_path + " AS thumbnail_path, '" + image_size + "' AS thumbnail_size, a.thumbnail_gen, ass.number_of_diggs, " +
				" IF(a.asset_rating_id = 3, true, IF(a.asset_rating_id = 6, true, IF(a.asset_rating_id = 9, true, false ))) AS mature_profile ";

			string tableList = " assets a " +
				" INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id " +
				" LEFT OUTER JOIN asset_channels ac ON a.asset_id = ac. asset_id ";

			string whereClause = " ac.channel_id = @channelId " +
				" AND DATE(ac.created_date) >= @endDate " +
				" AND DATE(ac.created_date) <= @startDate " +
				" AND ass.number_of_diggs > 0 " +
				" AND a.permission = " + (int) Constants.eASSET_PERMISSION.PUBLIC +
				SQLCommon_GetPublishedAssetsSQL ();

			if (!bGetMature)
			{
				whereClause += SQLCommon_GetNonMatureAssets ();
			}

			string orderby = " ass.number_of_diggs desc, ac.created_date desc ";

			parameters.Add ("@channelId", channelId);
			parameters.Add ("@endDate", endDate);
			parameters.Add ("@startDate", startDate);
			return dbUtility.GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// GetNewestMediaAssets
		/// </summary>
		/// <param name="contestId"></param>
		/// <returns></returns>
		public static PagedDataTable GetNewestMediaAssets (string image_path, string image_size, int channelId, bool bGetMature, bool bOnlyPublished, int pageNumber, int pageSize, DateTime startDate, DateTime endDate)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string selectList = " a.asset_id, a.asset_type_id, a.name, a.thumbnail_gen, " +
				" a.asset_rating_id, a.status_id, a.owner_id as user_id, a.owner_username as username, " +
				" a." + image_path + " AS thumbnail_path, '" + image_size + "' AS thumbnail_size, ac.created_date, " + 
				" IF(a.asset_rating_id = 3, true, IF(a.asset_rating_id = 6, true, IF(a.asset_rating_id = 9, true, false ))) AS mature_profile ";

			string tableList = " assets a " +
				" LEFT OUTER JOIN asset_channels ac ON a.asset_id = ac. asset_id ";

			string whereClause = " ac.channel_id = @channelId " +
				" AND DATE(ac.created_date) >= @endDate " +
				" AND DATE(ac.created_date) <= @startDate " +
				" AND a.permission = " + (int) Constants.eASSET_PERMISSION.PUBLIC +
				SQLCommon_GetPublishedAssetsSQL ();
			
			if (!bGetMature)
			{
				whereClause += SQLCommon_GetNonMatureAssets ();
			}

			string orderby = " ac.created_date desc ";

			parameters.Add ("@channelId", channelId);
			parameters.Add ("@endDate", endDate);
			parameters.Add ("@startDate", startDate);
			return dbUtility.GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// GetNewestMostViewedAssets
		/// </summary>
		/// <param name="contestId"></param>
		/// <returns></returns>
		public static PagedDataTable GetNewestMostViewedAssets (string image_path, string image_size, int channelId, bool bGetMature, bool bOnlyPublished, int pageNumber, int pageSize, DateTime startDate, DateTime endDate)
		{
			Hashtable parameters = new Hashtable ();
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string selectList = " a.asset_id, a.asset_type_id, a.name, a.thumbnail_gen, " +
				" a.asset_rating_id, a.status_id, a.owner_id as user_id, a.owner_username as username, " +
				" a." + image_path + " AS thumbnail_path, '" + image_size + "' AS thumbnail_size, a.thumbnail_gen, ac.created_date, " +
				" IF(a.asset_rating_id = 3, true, IF(a.asset_rating_id = 6, true, IF(a.asset_rating_id = 9, true, false ))) AS mature_profile, " +
				" ass.number_of_downloads ";

			string tableList = " assets a " +
				" INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id " +
				" LEFT OUTER JOIN asset_channels ac ON a.asset_id = ac. asset_id ";

			string whereClause = " ac.channel_id = @channelId " +
				" AND DATE(ac.created_date) >= @endDate " +
				" AND DATE(ac.created_date) <= @startDate " +
				" AND a.permission = " + (int) Constants.eASSET_PERMISSION.PUBLIC +
				SQLCommon_GetPublishedAssetsSQL ();

			if (!bGetMature)
			{
				whereClause += SQLCommon_GetNonMatureAssets ();
			}

			string orderby = " ass.number_of_downloads desc ";

			parameters.Add ("@channelId", channelId);
			parameters.Add ("@endDate", endDate);
			parameters.Add ("@startDate", startDate);
			return dbUtility.GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}

		#endregion

		#region Leader Board

		/// <summary>
		/// GetTopArtistInContest
		/// </summary>
		/// <param name="contestId"></param>
		/// <param name="resultCount"></param>
		/// <returns>PagedDataTable</returns>
		/// <remarks>one of two methods to determine top performers; this one returns top performing
		/// artist based on media votes - picks the artists' top performing media if more than on
		/// has been submitted</remarks>
		public static PagedDataTable GetLeaderBoard_User2User (int pageSize, int dateRange, string userId, int pageNumber)
		{
			//creates select string
			string selectList = " s.rank, s.user_id, s.channel_id, s.name, s.profile_picture, s.raves_channel, s.channels_created, " +
				" s.friend_accepts, s.raves_media, s.channel_members, s.join_wok, s.photos_uploaded, " + 
				" s.video_uploaded, s.music_uploaded, s.people_invited, s.page_pimped, s.raves_your_profile, " + 
				" s.total_points, s.thumbnail_small_path, s.name_no_spaces "; 

			//determine which table to use
			string tableList = "";

			if(dateRange <= 1)
			{
				tableList += " summary_lb_user2user_day s ";
			}
			else if(dateRange <= 7)
			{
				tableList += " summary_lb_user2user_week s ";
			}
			else if(dateRange <= 31)
			{
				tableList += " summary_lb_user2user_month s ";
			}
			else
			{
				tableList += " summary_lb_user2user_year s ";
			}
		
			Hashtable parameters = new Hashtable ();
			string whereClause = "";
			string orderby = " s.rank ";

			if(userId != null)
			{
				whereClause += " s.user_id = @user_id ";
				parameters.Add ("@user_id", userId);
			}

			return KanevaGlobals.GetDatabaseUtilityStats ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}


		/// <summary>
		/// GetTopArtistInContest
		/// </summary>
		/// <param name="contestId"></param>
		/// <param name="resultCount"></param>
		/// <returns>PagedDataTable</returns>
		/// <remarks>one of two methods to determine top performers; this one returns top performing
		/// artist based on media votes - picks the artists' top performing media if more than on
		/// has been submitted</remarks>
		public static PagedDataTable GetLeaderBoard_UsersNChannel (int channelId, int pageSize, int dateRange, string userId, int pageNumber)
		{
			//creates select string
			string selectList = " s.rank, s.user_id, s.channel_id, s.name, s.profile_picture, s.raves_channel, s.channels_created, " +
				" s.friend_accepts, s.raves_media, s.channel_members, s.join_wok, s.photos_uploaded, " + 
				" s.video_uploaded, s.music_uploaded, s.people_invited, s.page_pimped, s.raves_your_profile, " + 
				" s.total_points, s.thumbnail_small_path, s.name_no_spaces "; 

			//determine which table to use
			string tableList = "";

			if(dateRange <= 1)
			{
				tableList += " summary_lb_userNchannel_day s ";
			}
			else if(dateRange <= 7)
			{
                tableList += " summary_lb_userNchannel_week s ";
			}
			else if(dateRange <= 31)
			{
                tableList += " summary_lb_userNchannel_month s ";
			}
			else
			{
                tableList += " summary_lb_userNchannel_year s ";
			}

			string whereClause = " s.channel_id = @channelId "; 

			if(userId != null)
			{
				whereClause += " AND s.user_id = @user_id ";
			}

			string orderby = "";
			Hashtable parameters = new Hashtable ();
			parameters.Add ("@channelId", channelId);

			if(userId != null)
			{
				parameters.Add ("@user_id", userId);
			}

			return KanevaGlobals.GetDatabaseUtilityStats ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// GetTopAssetInContest
		/// </summary>
		/// <param name="contestId"></param>
		/// <param name="resultCount"></param>
		/// <returns>PagedDataTable</returns>
		/// <remarks>one of two methods to determine top performers; this one returns top performing
		/// media regardless of who the artist is</remarks>
		public static PagedDataTable GetLeaderBoard_Channel2Channel (int pageSize, int dateRange, string channelId, int pageNumber)
		{
			
			//creates select string
			string selectList = " s.rank, s.user_id, s.channel_id, s.name, s.profile_picture, s.raves_channel, s.channels_created, " +
				" s.friend_accepts, s.raves_media, s.channel_members, s.join_wok, s.photos_uploaded, " + 
				" s.video_uploaded, s.music_uploaded, s.people_invited, s.page_pimped, s.raves_your_profile, " + 
				" s.total_points, s.thumbnail_small_path, s.name_no_spaces "; 

			//determine which table to use
			string tableList = "";

			if(dateRange <= 1)
			{
				tableList += " summary_lb_channel2channel_day s ";
			}
			else if(dateRange <= 7)
			{
				tableList += " summary_lb_channel2channel_week s ";
			}
			else if(dateRange <= 31)
			{
				tableList += " summary_lb_channel2channel_month s ";
			}
			else
			{
				tableList += " summary_lb_channel2channel_year s ";
			}

			string orderby = "";
			string whereClause = ""; 
			Hashtable parameters = new Hashtable ();
			if(channelId != null)
			{
				whereClause += " s.channel_id = @channel_id ";
				parameters.Add ("@channel_id", channelId);
			}
			return KanevaGlobals.GetDatabaseUtilityStats ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// GetAssetsInContest
		/// </summary>
		/// <param name="contestId"></param>
		/// <returns></returns>
		public static PagedDataTable GetLeaderBoard_MediaNChannel (int channelId, int pageSize, DateTime startDate, DateTime endDate, int pageNumber)
		{
			//generate the pivot table query
			string pivotTableList = FormPivotTableQuery("point_system", "activity_group", "points_rewarded", "activity_description", "activity_group");

			//creates first result set as temp table via inner select
			string selectList = " CONCAT(u.first_name,' ',u.last_name) AS name, " + pivotTableList; 

			string tableList = 	" upd_summary upds LEFT OUTER JOIN users u ON upds.user_id = u.user_id " + 
				" INNER JOIN point_system ps ON upds.activity_code = ps.activity_code ";


			string whereClause = " upds.channel_id = @channelId " +
				" AND upds.date_stamp >= @endDate " + 
				" AND upds.date_stamp <= @startDate "; 				
			
			whereClause += " GROUP BY upds.user_id ";
			string orderby = " total_points desc " ;

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@channelId", channelId);
			parameters.Add ("@startDate", startDate);
			parameters.Add ("@endDate", endDate);

			return KanevaGlobals.GetDatabaseUtilityStats ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}

		#endregion

		#region feedback

		/// <summary>
		/// insert a row in feedback table
		/// </summary>
		/// <param name="subject"></param>
		/// <param name="summary"></param>
		/// <param name="ip"></param>
		/// <param name="file_link"></param>
		/// <param name="type">whether it's an error report or feedback report</param>
		/// <returns></returns>
		public static int InsertFeedback(
			string subject,
			string summary,
			string ip,
			string file_link,
			int	type)
		{
			DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

			StringBuilder sb = new StringBuilder() ;
			sb.Append("INSERT into feedback ");
			sb.Append("(subject, summary, ip, file_link, type, created_datetime) ");
			sb.Append("VALUES ");
			sb.Append("(@subject, @summary, @ip, @file_link, @type, ");
			sb.Append(dbUtility.GetCurrentDateFunction ());
			sb.Append(")");

			int feedbackId = 0;
			Hashtable parameters = new Hashtable ();
			parameters.Add ("@subject", subject);
			parameters.Add ("@summary", summary);
			parameters.Add ("@ip", ip);
			parameters.Add ("@file_link", file_link);
			parameters.Add ("@type", type);
			dbUtility.ExecuteIdentityInsert (sb.ToString(), parameters, ref feedbackId);

			return feedbackId;
		}


		/// <summary>
		/// update file_link in a feedback record
		/// </summary>
		/// <param name="feedbackId"></param>
		/// <param name="file_link"></param>
		/// <returns></returns>
		public static void UpdateFeedbackFileLink(
			int feedbackId,
			string file_link)
		{
			DatabaseUtility dbUtility =  KanevaGlobals.GetDatabaseUtility ();

			StringBuilder sb = new StringBuilder() ;
			sb.Append("UPDATE feedback ");
			sb.Append("set file_link = @file_link ");
			sb.Append("where feedback_id = @feedbackId ");

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@feedbackId", feedbackId);
			parameters.Add ("@file_link", file_link);
			dbUtility.ExecuteNonQuery(sb.ToString(), parameters);
		}

		/// <summary>
		/// GetFeedback
		/// </summary>
		public static DataRow GetFeedback (int feedbackId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlSelect = "SELECT file_link " +
				" FROM feedback " +
				" WHERE feedback_id = @feedbackId ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@feedbackId", feedbackId);
			return dbUtility.GetDataRow (sqlSelect, parameters, false);
		}

		#endregion

		// **********************************************************************************************
		// Asset Groups
		// **********************************************************************************************
		#region Media Groups

		/// <summary>
		/// Specific Media Library Query
		/// modified to slim down and combine multiple queries into one due to new
		/// media library set up
		/// </summary>
		/// 
		public static PagedDataTable GetMediaLibraryAssets (int assetGroupId, int communityId, int assetTypeId, int communityFilterId, int ownerId,
			bool bOnlyPublished, bool bGetMature, bool bOnlyActiveNew, string orderby, NameValueCollection filterBy, int pageNumber, int pageSize)
		{
            string selectList = " a.asset_id, a.name, a.file_size, a.public, a.asset_rating_id, a.mature, a.asset_type_id, a.publish_status_id, " +
                " a.owner_id, a.owner_username as username, a.teaser, a.created_date, a.status_id, a.keywords, " +
                " a.thumbnail_gen, a.thumbnail_small_path, a.category1_id, a.category2_id, a.category3_id, " +
                " IF(a.mature = 'Y', true, false) AS mature_profile ";

			string tableList = " asset_channels ac, assets a " +
				" INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id " +
				" LEFT OUTER JOIN asset_categories ac1 ON ac1.category_id = a.category1_id " +
				" LEFT OUTER JOIN asset_categories ac2 ON ac2.category_id = a.category2_id " +
				" LEFT OUTER JOIN asset_categories ac3 ON ac3.category_id = a.category3_id ";

			string whereClause = "ac.asset_id = a.asset_id ";

			Hashtable parameters = new Hashtable ();

			if(communityFilterId > 0)
			{
				whereClause += " AND ac.channel_id = @communityId "; 
				whereClause += " AND a.owner_id = @ownerId "; 
				parameters.Add ("@communityId", communityFilterId);
				parameters.Add ("@ownerId", ownerId);
			}
			else
			{
				parameters.Add ("@communityId", communityId);
				whereClause += " AND ac.channel_id = @communityId "; 
			}

			//filter for pulling back the playlist
			if(assetGroupId > 0)
			{
				selectList = "aga.sort_order, " + selectList;
				tableList += " INNER JOIN asset_group_assets aga ON a.asset_id  = aga.asset_id ";
				parameters.Add ("@assetGroupId", assetGroupId);
				whereClause += " AND aga.asset_group_id = @assetGroupId ";
			}
			else
			{
				selectList = "a.sort_order, " + selectList;
				// Specific asset type?
				if(assetTypeId > 0)
				{
					parameters.Add ("@assetTypeId", assetTypeId);
					whereClause += " AND a.asset_type_id = @assetTypeId "; 
				}
			}

			//filter for pulling back published items
			if (bOnlyPublished)
			{
				// Only show assets that have been processed
				whereClause += SQLCommon_GetPublishedAssetsSQL ();
			}

			//filter for pulling back only non-deleted or non-deletion marked items
			if (bOnlyActiveNew)
			{
				whereClause += SQLCommon_GetOnlyActiveStatusAssetsSQL ();
			}

			//filter for pulling back mature items
			if (!bGetMature)
			{
				whereClause += SQLCommon_GetNonMatureAssets ();
			}

			//appends any filters to the where clause
			for(int i=0; i < filterBy.Count; i++)
			{
				whereClause += " AND " + filterBy.GetKey(i) + filterBy[i];
			}

			return KanevaGlobals.GetDatabaseUtility ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// Insert a asset group
		/// </summary>0
		/// <returns></returns>
		public static int InsertAssetGroup (int channelId, string groupName)
		{
			return InsertAssetGroup(channelId, groupName, "");
		}

		/// <summary>
		/// Insert a asset group
		/// </summary>
		/// <returns></returns>
		public static int InsertAssetGroup (int channelId, string groupName, string groupDescription)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sql = "INSERT INTO asset_groups " +
				"(channel_id, name, created_date, description " +
				") VALUES (" +
				"@channelId, @groupName, " + dbUtility.GetCurrentDateFunction () + ", @groupDescription)";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@channelId", channelId);
			parameters.Add ("@groupName", groupName);
			parameters.Add ("@groupDescription", groupDescription);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		/// <summary>
		/// Update an asset group
		/// </summary>
		/// <returns></returns>
		public static int UpdateAssetGroup (int channelId, int assetGroupId, string groupName)
		{
			return UpdateAssetGroup ( channelId, assetGroupId, groupName, "");
		}

		/// <summary>
		/// Update an asset group
		/// </summary>
		/// <returns></returns>
		public static int UpdateAssetGroup (int channelId, int assetGroupId, string groupName, string groupDescription)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sql = "UPDATE asset_groups " +
				" SET name = @groupName, " +
				" description = @groupDescription " +
				" WHERE channel_id = @channelId " +
				" AND asset_group_id = @assetGroupId ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@groupName", groupName);
			parameters.Add ("@channelId", channelId);
			parameters.Add ("@assetGroupId", assetGroupId);
			parameters.Add ("@groupDescription", groupDescription);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		/// <summary>
		/// Delete a friend group
		/// </summary>
		/// <returns></returns>
		public static int DeleteAssetGroup (int channelId, int assetGroupId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// Delete all friends from group
			DeleteAllAssetsFromGroup (assetGroupId);

			// Delete the friend group
			string sql = "DELETE FROM asset_groups " +
				" WHERE channel_id = @channelId " +
				" AND asset_group_id = @assetGroupId ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@channelId", channelId);
			parameters.Add ("@assetGroupId", assetGroupId);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		/// <summary>
		/// DeleteAllAssetsFromGroup
		/// </summary>
		/// <returns></returns>
		public static int DeleteAllAssetsFromGroup (int assetGroupId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// Delete any friends in the group
			string sql = "DELETE FROM asset_group_assets " +
				" WHERE asset_group_id = @assetGroupId ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetGroupId", assetGroupId);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		/// <summary>
		/// GetAssetGroup
		/// </summary>
		public static DataRow GetAssetGroup (int assetGroupId, int channelId)
		{
			string sqlSelect = "SELECT fg.asset_group_id, fg.name, fg.channel_id " +
				" FROM asset_groups fg " +
				" WHERE fg.asset_group_id = @assetGroupId" +
				" AND fg.channel_id = @channelId ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@channelId", channelId);
			parameters.Add ("@assetGroupId", assetGroupId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);
		}

		/// <summary>
		/// Insert an asset into a group
		/// </summary>
		public static int InsertAssetInGroup (int channelId, int assetGroupId, int assetId)
		{
			// Make sure they are not already in the group
			if (IsAssetInGroup (assetGroupId, assetId))
			{
				return 0;
			}

			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sql = "INSERT INTO asset_group_assets " +
				"(asset_group_id, asset_id" +
				") VALUES (" +
				"@assetGroupId, @assetId)";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@assetGroupId", assetGroupId);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		/// <summary>
		/// Remove an asset from a group
		/// </summary>
		/// <returns></returns>
		public static int RemoveAssetFromGroup (int assetGroupId, int assetId)
		{
			// Make sure they are not already in the group
			if (!IsAssetInGroup (assetGroupId, assetId))
			{
				return 0;
			}

			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sql = "DELETE FROM asset_group_assets " +
				" WHERE asset_id = @assetId " +
				" AND asset_group_id = @assetGroupId ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			parameters.Add ("@assetGroupId", assetGroupId);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		/// <summary>
		/// Remove an asset from a group
		/// </summary>
		/// <returns></returns>
		public static int RemoveAssetFromAllGroups (int assetId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sql = "DELETE FROM asset_group_assets " +
				" WHERE asset_id = @assetId ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		/// <summary>
		/// Remove an asset from a group
		/// </summary>
		/// <returns></returns>
		public static int RemoveAssetFromGroupsByChannel (int channelId, int assetId)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			string sqlSelect = "SELECT asset_group_id " +
				" FROM asset_groups ag " +
				" WHERE ag.channel_id = @channelId ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@channelId", channelId);
			DataTable dtAssetGroupIds = KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect, parameters);

			//if no play list found abort
			if(dtAssetGroupIds.Rows.Count <= 0)
			{
				return 0;
			}

			string inValues = "";
			int i;
			for(i = 0; i<dtAssetGroupIds.Rows.Count - 1; i++)
			{
				inValues += (dtAssetGroupIds.Rows[i].ItemArray.GetValue(0).ToString() + ", ");
			}
			inValues += dtAssetGroupIds.Rows[i].ItemArray.GetValue(0).ToString();

			//select asset group ids separately due to trigger conflicts
			string sql = "DELETE FROM asset_group_assets " +
				" WHERE asset_id = @assetId " +
				" AND asset_group_id IN (" + inValues + ") ";

			parameters = new Hashtable ();
			parameters.Add ("@assetId", assetId);
			return dbUtility.ExecuteNonQuery (sql, parameters);
		}

		/// <summary>
		/// GetAssetsInGroup
		/// </summary>
		public static PagedDataTable GetAssetsInGroup (int assetGroupId, int channelId,
			int assetTypeId,
			string orderby,
			int pageNumber, int pageSize)
		{
			return GetAssetsInGroup (assetGroupId, channelId,
				assetTypeId,
				orderby,
				null,
				pageNumber, pageSize);
		}

		/// <summary>
		/// GetAssetsInGroup
		/// </summary>
		public static PagedDataTable GetAssetsInGroup (int assetGroupId, int channelId,
			int assetTypeId,
			string orderby, string filterBy,
			int pageNumber, int pageSize)
		{
			string selectList = " a.asset_id, a.name, a.amount, a.asset_rating_id, a.mature, a.asset_type_id, " +
				" a.content_extension, a.owner_id, a.owner_username, a.owner_username as username, a.teaser, a.created_date, a.keywords, " +
				" a.run_time_seconds, CONCAT_WS(',', ac1.name, ac2.name, ac3.name) as categories, " +
				" a.thumbnail_medium_path, a.thumbnail_gen, " +
				" ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs ";

			string tableList = "asset_groups ag, asset_group_assets aga, assets a " +
				" INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id " +
				" LEFT OUTER JOIN asset_categories ac1 ON ac1.category_id = a.category1_id " +
				" LEFT OUTER JOIN asset_categories ac2 ON ac2.category_id = a.category2_id " +
				" LEFT OUTER JOIN asset_categories ac3 ON ac3.category_id = a.category3_id ";

			string whereClause = "ag.asset_group_id = @assetGroupId" +
				" AND ag.channel_id = @channelId " +
				" AND ag.asset_group_id = aga.asset_group_id " +
				" AND aga.asset_id = a.asset_id " +
				" AND (@assetTypeId = " + (int) Constants.eASSET_TYPE.ALL +
				" OR a.asset_type_id = @assetTypeId) " +
				SQLCommon_GetOnlyActiveStatusAssetsSQL ();

			if(filterBy != null && filterBy.Length > 0)
			{
				whereClause += " AND " + filterBy;
			}

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@channelId", channelId);
			parameters.Add ("@assetGroupId", assetGroupId);
			parameters.Add ("@assetTypeId", assetTypeId);
			return KanevaGlobals.GetDatabaseUtility ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// s
		/// </summary>
		public static PagedDataTable GetChannelMixedMedia(int assetGroupId, int channelId,
			string orderby, int pageNumber, int pageSize)
		{
			string selectList = " DISTINCT a.asset_id, a.short_description, a.name, a.amount, a.asset_rating_id, " +
				" a.asset_type_id, a.amount, a.teaser, " +
				" ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs ";;

			string tableList = "assets a " +
				" INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id " +
				" INNER JOIN asset_channels ac ON ac.asset_id = a.asset_id AND ac.status_id = 1"; //TODO we need to define the asset channel statuses

			string whereClause = " ac.channel_id = @channelId " +
				SQLCommon_GetPublishedAssetsSQL ();

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@channelId", channelId);
			if(assetGroupId > 0)
			{
				tableList += " INNER JOIN asset_groups ag ON ag.channel_id = ac.channel_id " +
					" INNER JOIN asset_group_assets aga ON aga.asset_group_id = ag.asset_group_id";

				whereClause += " AND ag.asset_group_id = @assetGroupId ";

				parameters.Add ("@assetGroupId", assetGroupId);
			}

			return KanevaGlobals.GetDatabaseUtility ().GetPagedDataTable (selectList, tableList, whereClause,
				orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// Get the asset groups for a channel
		/// </summary>
		public static PagedDataTable GetAssetGroups (int channelId, string filter, string orderby, int pageNumber, int pageSize)
		{
			string selectList = " ag.asset_group_id, ag.name, ag.asset_count, ag.description ";

			string tableList = " asset_groups ag ";

			string whereClause = " ag.channel_id = @channelId ";

			// Filter it?
			if (filter.Length > 0)
			{
				whereClause += " AND " + filter;
			}

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@channelId", channelId);
			return KanevaGlobals.GetDatabaseUtility ().GetPagedDataTable (selectList, tableList, whereClause, orderby, parameters, pageNumber, pageSize);
		}

		/// <summary>
		/// Get the asset groups for a channel
		/// </summary>
		public static DataTable GetAssetGroups (int channelId, string assetGroupIds)
		{
			string sqlString = " SELECT ag.asset_group_id, ag.name, asset_count " +
				" FROM asset_groups ag " +
				" WHERE ag.channel_id = @channelId " +
				" AND ag.asset_group_id IN (" + assetGroupIds + ")" +
				" ORDER BY ag.name ASC " +
				" LIMIT 10";  // Limit to to prevent users from abusing system adding groups to channels

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@channelId", channelId);
			return KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlString, parameters);
		}

		/// <summary>
		/// IsAssetInGroup
		/// </summary>
		public static bool IsAssetInGroup (int assetGroupId, int assetId)
		{
			string sqlSelect = "SELECT asset_group_id " +
				" FROM asset_group_assets " +
				" WHERE asset_group_id = @assetGroupId" +
				" AND asset_id = @assetId ";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@assetGroupId", assetGroupId);
			parameters.Add ("@assetId", assetId);
			DataRow drAssetGroup = KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, parameters, false);

			return (drAssetGroup != null);
		}

		#endregion

		public static bool AddTag( int userId, int channelId, int assetId, string userHostAddress  )
		{
			//add asset to channel
			StoreUtility.InsertAssetChannel(assetId, channelId);

			return true;
		}

		public static int AddMantisIssue( string category, string summary, string comments )
		{

			Futureware.MantisConnect.Session session;

			string mantisConnectUrl = System.Configuration.ConfigurationManager.AppSettings ["MantisConnectUrl"];
            string mantisUserName = System.Configuration.ConfigurationManager.AppSettings["MantisUserName"];
            string mantisPassword = System.Configuration.ConfigurationManager.AppSettings["MantisPassword"];
            string mantisProject = System.Configuration.ConfigurationManager.AppSettings["mantisProject"];
            int mantisProjectId = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["mantisProjectId"]);

			NetworkCredential nc = null;
			session = new Session (mantisConnectUrl, mantisUserName, mantisPassword, nc);

			session.Connect();

			// Create the issue in memory
			Issue issue = new Issue();

			issue.Project = new ObjectRef (mantisProject);
			issue.Priority = new ObjectRef ("normal");
			issue.Severity = new ObjectRef ( "minor");
			issue.Reproducibility = new ObjectRef ("N/A");


			issue.Category = category;
			issue.ProductVersion = "Alpha [" + KanevaGlobals.Version () + "]";
			issue.Summary = summary;
			issue.Description = comments;
			issue.ReportedBy = new ObjectRef (session.Username);

			return session.Request.IssueAdd ( issue ); // issueId
		}

		#region Zip Code Searching

		// This function establishes a "square" around a point on the globe (latitude/longitude).  This method was used
		// to make the query for "zipcode inclusion" scalable; Computing whether a point (A) lies inside the radius of a given distance from a point (B) would not scale
		// effectively.  Also, it was determined to be more efficient for the use in the kaneva system, since the zipcode data itself will produce in-exact
		// results using the correct formula.
		//
		// The actual algorithm for determining if a point on a sphere lies inside a given radius (given an initial reference point and radius)
		// can be found at the following url: http://mathforum.org/library/drmath/view/51882.html#assoc
		//
		// USEAGE:
		// p_distance is input as "miles" (positive value)
		// p_current_lat is input as degrees with number(s) after the decimal representing decimal portions of a degree (not minutes)
		// p_current_lat is input as degrees with number(s) after the decimal representing decimal portions of a degree (not minutes)
		// The input varibles 'p_upper_lat', 'p_upper_long', 'p_lower_lat', 'p_lower_long' are updated by the function
		//
		// When a latitude is greater or less than +/-90 degrees, +/-90 is used as the boundary
		// When a longitude is greater or less than +/-180 degrees, +/-180 is used as the boundary
		public static Constants.eCoordinateErrorCode GetCoordinates (int p_distance, double p_current_lat, double p_current_long, ref double p_upper_lat, ref double p_upper_long, ref double p_lower_lat, ref double p_lower_long)
		{
			// Constants
			const double MILE_CONVERSION = 1.15;		// 1 nautical mile  = 1.15 miles
			const double MINUTE_CONVERSION = 1;			// 1 minute = 1 nautical miles
			const double DEGREE_CONVERSION = 60;		// 1 degree = 60 nautical miles
			const double MAX_LATITUDE = 90;				// Absolute Value
			const double MAX_LONGITUDE = 180;			// Absolute Value
			const double MAX_INPUT_DISTANCE = 6210;		// Max allowed 'p_distance' is equal to 90 degrees = (90 * 60 * 1.15) = 6210 miles

			double t_nautical_miles, t_minutes, t_degrees;
			Constants.eCoordinateErrorCode get_coordinates = Constants.eCoordinateErrorCode.NoError;

			if (p_distance <= MAX_INPUT_DISTANCE)
			{

				// Covert Distance to Degrees
				t_nautical_miles = (p_distance / MILE_CONVERSION);
				t_minutes = (t_nautical_miles / MINUTE_CONVERSION);
				t_degrees = (t_minutes / DEGREE_CONVERSION);

				// Calculate latitude boundries
				p_upper_lat = p_current_lat + t_degrees;
				p_lower_lat = p_current_lat - t_degrees;

				// If latitude is greater than 90 (or less than -90) set to 90 (or -90)
				if ((p_upper_lat > 0) && (p_upper_lat > MAX_LATITUDE))
				{
					p_upper_lat = MAX_LATITUDE;
				}
				else if ((p_lower_lat < 0) && (p_lower_lat < -MAX_LATITUDE))
				{
					p_lower_lat = -MAX_LATITUDE;
				}

				// Calculate longitude boundries
				p_upper_long = p_current_long + t_degrees;
				p_lower_long = p_current_long - t_degrees;

				// If longitude is greater than 180 (or less than -180) set to 180 (or -180)
				if ((p_upper_long > 0) && (p_upper_long > MAX_LATITUDE))
				{
					p_upper_long = MAX_LONGITUDE;
				}
				else if ((p_lower_long < 0) && (p_lower_long < -MAX_LONGITUDE))
				{
					p_lower_long = -MAX_LONGITUDE;
				}

				  // No Error
			}
			else
			{
				//  Error - Max miles input exceeded
				get_coordinates = Constants.eCoordinateErrorCode.MaxDistanceExceeded;
			}

			return get_coordinates;
		}

		#endregion


		#region asset sharing

//		public static DataRow GetAssetShareConnectionByUserAndAsset(int userId, int assetId)
//		{
//			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
//			Hashtable parameters = new Hashtable ();
//
//			string query = " SELECT c.id, c.asset_id, c.user_id, c.path, c.depth, u.username, u.zip_code " +
//				" FROM asset_share_connection c " +
//				" INNER JOIN users u ON u.user_id = c.user_id " +
//				" WHERE u.user_id = @userId AND c.asset_id = @assetId";
//			parameters.Add("@userId", userId);
//			parameters.Add("@assetId", assetId);
//
//			return dbUtility.GetDataRow(query, parameters, false);
//		}
//
//
//		/// <summary>
//		/// Note that this method does not update stats, path and depth are calculated based on parent
//		/// </summary>
//		/// <param name="assetId"></param>
//		/// <param name="assetOwnerId">This is redundant, but pass this in anyway to save a db call</param>
//		/// <param name="userId">optional, applicable when the user is logged in</param>
//		/// <param name="userIp"></param>
//		/// <param name="parentId"></param>
//		/// <returns></returns>
//		public static int InsertAssetShareConnection(int assetId, int assetOwnerId,
//			int userId, string userIp,
//			int parentId)
//		{
//			//dont insert if asset/user pair already exists
//			//maybe this should be combined with the insert so we only make one call?
//			if (GetAssetShareConnectionByUserAndAsset(userId, assetId) != null)
//				return -1;
//
//			string path = "";
//			int depth = 0;
//			if(parentId == assetOwnerId)
//			{
//				//short cut when parent user is the asset owner
//				path = parentId.ToString();
//				depth = 1;
//			}
//			else if(parentId > 0)
//			{
//				DataRow drParent = GetAssetShareConnectionByUserAndAsset(parentId, assetId);
//				if(drParent != null)
//				{
//					if(drParent["path"] != DBNull.Value &&
//						drParent["path"].ToString().Trim().Length > 0)
//					{
//						path = drParent["path"].ToString() + "," + parentId;
//					}
//					else
//					{
//						path = parentId.ToString();
//					}
//					depth = Convert.ToInt32(drParent["depth"]) + 1;
//				}
//				else
//				{
//					//if the passed in parent user has never viewed the asset, use asset owner instead as
//					//parent
//					return InsertAssetShareConnection(assetId, assetOwnerId, userId, userIp, assetOwnerId);
//				}
//			}
//			//SELECT u.id, u.asset_id, u.user_id, u.parent_id, u.path, u.depth,
//			//u.created_datetime FROM asset_share_connection u;
//			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
//			Hashtable parameters = new Hashtable ();
//
//			StringBuilder sb = new StringBuilder() ;
//			sb.Append(" INSERT INTO asset_share_connection (");
//			sb.Append(" asset_id, user_id, user_ip, parent_id, path, ");
//			sb.Append(" depth, created_datetime) VALUES ( ");
//			sb.Append(" @asset_id, @user_id, @user_ip, @parent_id, @path, ");
//			sb.Append(" @depth, @created_datetime) ");
//
//			parameters.Add("@asset_id", assetId);
//			parameters.Add("@user_id", userId > 0 ? userId : 0);
//			parameters.Add("@user_ip", userIp != null ? userIp : "NULL");
//			parameters.Add("@parent_id", parentId > 0 ? parentId : 0);
//			parameters.Add("@path", path);
//			parameters.Add("@depth", depth);
//			parameters.Add("@created_datetime", dbUtility.GetCurrentDateTime());
//
//			//log.Debug("Executing " + sb.ToString());
//			int retVal = 0;
//			dbUtility.ExecuteIdentityInsert(sb.ToString(), parameters, ref retVal);
//
//			return retVal;
//		}
//
//		/// <summary>
//		/// warning! calling this will clear all counts to 0 in asset_share_connection_stats table
//		/// It's meant to be called from the daily update service only to recalculate all the counts
//		/// </summary>
//		/// <returns></returns>
//		public static void ClearAllAssetShareConnectionStats()
//		{
//			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
//
//			string query = "UPDATE asset_share_connection_stats SET count = 0";
//
//			dbUtility.ExecuteNonQuery(query);
//		}
//
//		/// <summary>
//		///
//		/// </summary>
//		/// <param name="parentId">pass in 0 to return root level users</param>
//		/// <param name="assetId">pass in 0 to return all assets</param>
//		/// <returns></returns>
//		public static DataTable GetAssetShareConnectionsByParentId(int parentId, int assetId)
//		{
//			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
//			Hashtable parameters = new Hashtable ();
//
//			string query = " SELECT uc.id, uc.asset_id, uc.user_id, uc.path, u.zip_code " +
//				" FROM asset_share_connection uc " +
//				" INNER JOIN users u ON u.user_id = uc.user_id";
//
//			if(parentId <= 0)
//			{
//				query += " WHERE (parent_id IS NULL OR parent_id = 0)";
//			}
//			else
//			{
//				query += " WHERE parent_id = @parent_id";
//				parameters.Add("@parent_id", parentId);
//			}
//			if(assetId > 0)
//			{
//				query += " AND asset_id = @asset_id";
//				parameters.Add("@asset_id", assetId);
//			}
//
//			return dbUtility.GetDataTable(query, parameters);
//		}
//
//		public static int GetAssetShareConnectionStats(int assetShareConnectionId, int depth)
//		{
//			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
//			Hashtable parameters = new Hashtable ();
//
//			string query = "SELECT id FROM asset_share_connection_stats " +
//				" WHERE asset_share_connection_id = @assetShareConnectionId AND depth = @depth";
//
//			parameters.Add("@assetShareConnectionId", assetShareConnectionId);
//			parameters.Add("@depth", depth);
//
//			DataTable dt = dbUtility.GetDataTable(query, parameters);
//			if(dt.Rows.Count > 0)
//			{
//				return Convert.ToInt32(dt.Rows[0]["id"]);
//			}
//			else
//			{
//				return 0;
//			}
//		}
//
//		/// <summary>
//		/// populate a user's invited user count at level <c>depth</c>
//		/// </summary>
//		/// <param name="assetShareConnectionId"></param>
//		/// <param name="depth"></param>
//		/// <param name="count"></param>
//		/// <returns></returns>
//		public static int InsertAssetShareConnectionStats(int assetShareConnectionId,
//			int depth, int count)
//		{
//			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
//			Hashtable parameters = new Hashtable ();
//
//			StringBuilder sb = new StringBuilder() ;
//			sb.Append(" INSERT INTO asset_share_connection_stats (");
//			sb.Append(" asset_share_connection_id, depth, count, created_datetime) ");
//			sb.Append(" VALUES ( ");
//			sb.Append(" @asset_share_connection_id, @depth, @count, @created_datetime) ");
//
//			parameters.Add("@asset_share_connection_id", assetShareConnectionId);
//			parameters.Add("@depth", depth);
//			parameters.Add("@count", count);
//			parameters.Add("@created_datetime", dbUtility.GetCurrentDateTime());
//
//			//log.Debug("Executing " + sb.ToString());
//			int retVal = 0;
//			dbUtility.ExecuteIdentityInsert(sb.ToString(), parameters, ref retVal);
//
//			return retVal;
//		}
//
//		/// <summary>
//		///
//		/// </summary>
//		/// <param name="assetShareConnectionId"></param>
//		/// <param name="depth"></param>
//		/// <param name="count"></param>
//		/// /// <param name="createNew">true to create a new row if there isn't an existing one</param>
//		public static void UpdateAssetShareConnectionStatsCount(int assetShareConnectionId,
//			int depth, int count, bool createNew)
//		{
//			if(GetAssetShareConnectionStats(assetShareConnectionId, depth) > 0)
//			{
//				//update
//				DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
//				Hashtable parameters = new Hashtable ();
//
//				StringBuilder sb = new StringBuilder() ;
//				sb.Append(" UPDATE asset_share_connection_stats ");
//				sb.Append(" SET count = @count ");
//				sb.Append(" WHERE asset_share_connection_id = @assetShareConnectionId AND depth = @depth ");
//
//				parameters.Add("@assetShareConnectionId", assetShareConnectionId);
//				parameters.Add("@depth", depth);
//				parameters.Add("@count", count);
//
//				//log.Debug("Executing " + sb.ToString());
//				dbUtility.ExecuteNonQuery(sb.ToString(), parameters);
//			}
//			else if(createNew)
//			{
//				//insert
//				InsertAssetShareConnectionStats(assetShareConnectionId, depth, count);
//			}
//			else
//			{
//				//return if no row is created or updated
//				return;
//			}
//
//			if(depth != 0)
//			{
//				//increase the total count too
//				IncrementAssetShareConnectionStatsCount(assetShareConnectionId, 0, count);
//				//increase the total count for parent at the level
//				IncrementParentAssetShareConnectionStatsCount(assetShareConnectionId, depth, count);
//			}
//		}
//
//		/// <summary>
//		/// increment children nodes count of a user at certain level
//		/// </summary>
//		/// <param name="assetShareConnectionId"></param>
//		/// <param name="depth"></param>
//		/// <param name="val"></param>
//		public static void IncrementAssetShareConnectionStatsCount(int assetShareConnectionId,
//			int depth, int val)
//		{
//			int count = val;
//			if(GetAssetShareConnectionStats(assetShareConnectionId, depth) > 0)
//			{
//				//update
//				DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
//				Hashtable parameters = new Hashtable ();
//
//				StringBuilder sb = new StringBuilder() ;
//				sb.Append(" SELECT count FROM asset_share_connection_stats ");
//				sb.Append(" WHERE asset_share_connection_id = @assetShareConnectionId AND depth = @depth ");
//
//				parameters.Add("@assetShareConnectionId", assetShareConnectionId);
//				parameters.Add("@depth", depth);
//
//				//log.Debug("Executing " + sb.ToString());
//				int currCount = dbUtility.ExecuteScalar(sb.ToString(), parameters);
//				count += currCount;
//			}
//
//			UpdateAssetShareConnectionStatsCount(assetShareConnectionId, depth,
//				count, true);
//		}
//
//		/// <summary>
//		/// increment children nodes count of a parent user at certain level
//		/// </summary>
//		/// <param name="assetShareConnectionId"></param>
//		/// <param name="depth"></param>
//		/// <param name="val"></param>
//		public static void IncrementParentAssetShareConnectionStatsCount(int assetShareConnectionId,
//			int depth, int val)
//		{
//			DataRow drAssetShareConn = GetParentAssetShareConnection(assetShareConnectionId);
//			if(drAssetShareConn != null)
//			{
//				int id = Convert.ToInt32(drAssetShareConn["id"]);
//				if(id > 0)
//				{
//					IncrementAssetShareConnectionStatsCount(id, depth, val);
//				}
//			}
//		}
//
//		public static DataRow GetParentAssetShareConnection(int assetShareConnectionId)
//		{
//			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
//			Hashtable parameters = new Hashtable ();
//
//			string query = " SELECT uc.id, uc.asset_id, uc.user_id, uc.parent_id, uc.path " +
//				" FROM asset_share_connection uc " +
//				" INNER JOIN asset_share_connection uc2 ON uc2.parent_id = uc.user_id AND uc2.asset_id = uc.asset_id" +
//				" WHERE uc2.id = @id AND uc.user_id IS NOT NULL AND uc.user_id > 0";
//			parameters.Add("@id", assetShareConnectionId);
//
//			return dbUtility.GetDataRow(query, parameters, false);
//		}
//
//		public static DataTable GetAllAssetShareConnections()
//		{
//			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
//			Hashtable parameters = new Hashtable ();
//
//			string query = " SELECT id, asset_id, user_id, user_ip, parent_id, path, depth " +
//				" FROM asset_share_connection WHERE user_id IS NOT NULL AND user_id > 0";
//
//			return dbUtility.GetDataTable(query, parameters);
//		}
//
//		public static DataRow GetAssetShareConnection(int assetShareConnectionId)
//		{
//			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
//			Hashtable parameters = new Hashtable ();
//
//			string query = " SELECT uc.id, uc.asset_id, uc.user_id, uc.user_ip, uc.parent_id, " +
//				" uc.path, uc.depth, u.username "
//				+ " FROM asset_share_connection uc"
//				+ " INNER JOIN users u ON u.user_id = uc.user_id "
//				+ " WHERE id = @id";
//			parameters.Add("@id", assetShareConnectionId);
//
//			return dbUtility.GetDataRow(query, parameters, false);
//		}
//
//		public static DataRow GetAssetShareConnectionMapDataByAssetShareConnectionId(int assetShareConnectionId)
//		{
//			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
//			Hashtable parameters = new Hashtable ();
//
//			string query = "SELECT id, asset_share_connection_id, map_data, created_datetime " +
//				" FROM asset_share_connection_map_data " +
//				" WHERE asset_share_connection_id = @assetShareConnectionId ";
//
//			parameters.Add("@assetShareConnectionId", assetShareConnectionId);
//
//			return dbUtility.GetDataRow(query, parameters, false);
//		}
//
//		/// <summary>
//		/// recursive method that collects map data at each level
//		/// </summary>
//		/// <param name="userId"></param>
//		/// <param name="assetId"></param>
//		/// <param name="depth"></param>
//		/// <param name="degrees"></param>
//		private static void CollectAssetShareConnectionMapMetric(int userId,
//			int assetId, int depth, IDictionary degrees)
//		{
//			DataTable dtChildren = StoreUtility.GetAssetShareConnectionsByParentId(userId, assetId);
//			if(dtChildren.Rows.Count > 0)
//			{
//				if(!degrees.Contains(depth))
//				{
//					degrees.Add(depth, new Hashtable());
//				}
//				IDictionary zipCounts = (IDictionary) degrees[depth];
//				foreach(DataRow drChild in dtChildren.Rows)
//				{
//					string zip = drChild["zip_code"] != DBNull.Value ?
//						drChild["zip_code"].ToString() : "";
//					int childAssetShareConnId = Convert.ToInt32(drChild["id"]);
//					int childUserId = Convert.ToInt32(drChild["user_id"]);
//					string zipAreaCode = GetZipAreaCode(zip);
//
//					if(zipAreaCode != null)
//					{
//						if(!zipCounts.Contains(zipAreaCode))
//						{
//							zipCounts.Add(zipAreaCode, 0);
//						}
//						zipCounts[zipAreaCode] = Convert.ToInt32(zipCounts[zipAreaCode]) + 1;
//					}
//					CollectAssetShareConnectionMapMetric(childUserId, assetId, depth +1,
//						degrees);
//				}
//			}
//		}
//
//		public static void UpdateAssetShareConnectionMapData(int id, int assetShareConnectionId, string mapData)
//		{
//			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
//			Hashtable parameters = new Hashtable ();
//
//			StringBuilder sb = new StringBuilder() ;
//			sb.Append(" UPDATE asset_share_connection_map_data ");
//			sb.Append(" SET asset_share_connection_id = @asset_share_connection_id, ");
//			sb.Append(" map_data = @map_data ");
//			sb.Append(" WHERE id = @id ");
//
//			parameters.Add("@id", id);
//			parameters.Add("@map_data", mapData);
//			parameters.Add("@asset_share_connection_id", assetShareConnectionId);
//
//			dbUtility.ExecuteNonQuery(sb.ToString(), parameters);
//		}
//
//		/// <summary>
//		/// populate a user's map data
//		/// </summary>
//		/// <param name="assetShareConnectionId"></param>
//		/// <param name="mapData"></param>
//		/// <returns></returns>
//		public static int InsertAssetShareConnectionMapData(int assetShareConnectionId, string mapData)
//		{
//			//SELECT u.id, u.asset_share_connection_id, u.map_data,
//			//u.created_datetime FROM asset_share_connection_map_data u;
//			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
//			Hashtable parameters = new Hashtable ();
//
//			StringBuilder sb = new StringBuilder() ;
//			sb.Append(" INSERT INTO asset_share_connection_map_data (");
//			sb.Append(" asset_share_connection_id, map_data, created_datetime) ");
//			sb.Append(" VALUES ( ");
//			sb.Append(" @asset_share_connection_id, @map_data, @created_datetime) ");
//
//			parameters.Add("@asset_share_connection_id", assetShareConnectionId);
//			parameters.Add("@map_data", mapData);
//			parameters.Add("@created_datetime", dbUtility.GetCurrentDateTime());
//
//			//log.Debug("Executing " + sb.ToString());
//			int retVal = 0;
//			dbUtility.ExecuteIdentityInsert(sb.ToString(), parameters, ref retVal);
//
//			return retVal;
//		}
//
//		/// <summary>
//		/// generate asset share connection mapdata in a dictionary
//		/// </summary>
//		/// <param name="assetShareConnectionId"></param>
//		/// <returns></returns>
//		public static IDictionary GetAssetShareConnectionMapData(int assetShareConnectionId)
//		{
//			IDictionary retVal = null;
//			DataRow drUserCollection = GetAssetShareConnection(assetShareConnectionId);
//			if(drUserCollection != null)
//			{
//				retVal = new SortedList();
//				CollectAssetShareConnectionMapMetric(Convert.ToInt32(drUserCollection["user_id"]),
//					Convert.ToInt32(drUserCollection["asset_id"]),
//					1, retVal);
//			}
//			return retVal;
//		}
//
//		/// <summary>
//		/// return the number of shares at each degree
//		/// </summary>
//		/// <param name="assetId"></param>
//		/// <returns></returns>
//		public static DataTable GetAssetShareConnectionsCountByDegree(int assetId)
//		{
//			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
//			Hashtable parameters = new Hashtable ();
//
//			StringBuilder sb = new StringBuilder() ;
//			sb.Append(" SELECT COUNT(user_id) AS count, depth ");
//			sb.Append(" FROM asset_share_connection ");
//			sb.Append(" WHERE asset_id = @assetId AND user_id IS NOT NULL AND user_id > 0");
//			sb.Append(" GROUP BY depth ");
//			sb.Append(" HAVING depth > 0 ");
//			sb.Append(" ORDER BY depth ");
//
//			parameters.Add("@assetId", assetId);
//
//			return dbUtility.GetDataTable(sb.ToString(), parameters);
//		}

		public static string GetZipAreaCode(string zip)
		{
			string retVal = null;
			try
			{
				//if this is a valid number, then returns first 3 digits
				int zipNum = Convert.ToInt32(zip);
				if(zipNum > 10000)
				{
					retVal = zip.Substring(0,3);
				}
			}
			catch(Exception){}
			return retVal;
		}

		#endregion

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        private const int cTOP_LIST_NUMBER_OF_MONTHS = -3;

        public const int cCOMMUNITY_POPULAR_KEYWORD_TYPE = -1;

        public const int cBILLING_LICENSE_BUFFER_DAYS = 10;

        public const int cASSET_SUBSCRIPTION_BUFFER_DAYS = 4;

		private static Int16 m_embedCounter = 1; // used to make embed tag unique

		/// <summary>
		/// Database name for tables that are no in the cluster
		/// </summary>
		private static string m_DbNameKanevaNonCluster = "my_kaneva";

    }
}
