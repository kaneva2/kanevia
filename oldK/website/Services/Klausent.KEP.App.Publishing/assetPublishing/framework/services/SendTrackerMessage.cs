///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Net.Sockets;
using System.IO;
using System.Data;
using System.Collections;

// Import log4net classes.
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for SendTrackerMessage.
	/// </summary>
	public class SendTrackerMessage
	{
		public SendTrackerMessage ()
		{
		}

		/// <summary>
		/// The UserId to send
		/// </summary>
		public int UserId
		{
			set
			{
				m_UserId = value;
			}
			get
			{
				return m_UserId;
			}
		}

		/// <summary>
		/// The info hash to send
		/// </summary>
		public string InfoHash
		{
			set
			{
				m_infoHash = value;
			}
			get
			{
				return m_infoHash;
			}
		}

		/// <summary>
		/// The AssetId to send
		/// </summary>
		public int AssetId
		{
			set
			{
				m_AssetId = value;
			}
			get
			{
				return m_AssetId;
			}
		}

		/// <summary>
		/// The CommunityId to send
		/// </summary>
		public int CommunityId
		{
			set
			{
				m_CommunityId = value;
			}
			get
			{
				return m_CommunityId;
			}
		}

		/// <summary>
		/// The OrderId to send
		/// </summary>
		public int OrderId
		{
			set
			{
				m_OrderId = value;
			}
			get
			{
				return m_OrderId;
			}
		}

		/// <summary>
		/// The user's IP Address
		/// </summary>
		public string IPAddress
		{
			set
			{
				m_IPAddress = value;
			}
			get
			{
				return m_IPAddress;
			}
		}

		/// <summary>
		/// The torrents
		/// </summary>
		public string Torrents
		{
			set
			{
				m_Torrents = value;
			}
			get
			{
				return m_Torrents;
			}
		}

		/// <summary>
		/// SendUpdateUserIP
		/// </summary>
		public void SendUpdateUserIP ()
		{
			try
			{
				DataRow drUser = UsersUtility.GetUser (UserId);
				SendMessage ("5," + drUser ["username"].ToString () + "," + IPAddress);
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error sending update user ip", exc);
			}
		}

		/// <summary>
		/// SendDeleteTorrent
		/// </summary>
		public void SendDeleteTorrent ()
		{
			try
			{
				SendMessage ("6," + m_infoHash);
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error sending delete of info_hash <" + m_infoHash + "> ", exc);
			}
		}

		public void SendDeleteTorrentsFromTracker(Hashtable assetIds)
		{
			if (assetIds.Count == 0)
			{
				return;
			}	

			// Get the list of trackers
			System.Collections.Hashtable htTrackers = KanevaGlobals.Trackers;

			if (htTrackers == null)
			{
				m_logger.Warn ("No trackers defined in web.config");
			}
			else
			{
				System.Collections.IDictionaryEnumerator myEnumerator = htTrackers.GetEnumerator ();

				int iteration = 0;

				while ( myEnumerator.MoveNext() )
				{
					try
					{
						string strMessage = @"7";

						bool contentToSend = false;

						iteration++;

						Tracker tracker = (Tracker) myEnumerator.Value;

						foreach( int assetId in assetIds.Keys)
						{
							contentToSend = true;
							strMessage += "," + assetId;
						}

						if ( contentToSend)
						{
							m_logger.Info ("Sending message '" + strMessage + "' to " + tracker.IP + ":" + tracker.CacheUpdatePort);

							// Connect to the tracker
							TcpClient tc = new TcpClient (tracker.IP, tracker.CacheUpdatePort);	// ip and port    

							NetworkStream ns = tc.GetStream ();
							StreamWriter sw = new StreamWriter (ns);
							sw.WriteLine (strMessage);//csv message
							sw.Flush ();

							ns.Close ();
							sw.Close ();
						}
					}
					catch (Exception exc)
					{
						m_logger.Error ("Error sending message to tracker cache", exc);
					}
				}
			}
		}

		public void SendDeleteTorrentsFromContentServers(Hashtable assetIds)
		{
			if (assetIds.Count == 0)
			{
				return;
			}	

			// Get the list of content servers
			System.Collections.Hashtable htContentServers = KanevaGlobals.ContentServers;

			if (htContentServers == null)
			{
				m_logger.Warn ("No content servers defined in web.config");
			}
			else
			{
				System.Collections.IDictionaryEnumerator myEnumerator = htContentServers.GetEnumerator ();

				while ( myEnumerator.MoveNext() )
				{
					try
					{
						string strMessage = @"8";

						bool contentToSend = false;

						ContentServer contentServer = (ContentServer) myEnumerator.Value;

						m_logger.Info ("Content Server=" + contentServer.IP + ":" + contentServer.Port);
						
						foreach( int assetId in assetIds.Keys)
						{
							string homeNumber = (string) assetIds[assetId];

							if ( System.Convert.ToInt32(homeNumber) == contentServer.HomeId)
							{
								contentToSend = true;
								strMessage += "," + assetId;
							}
						}

						if ( contentToSend)
						{
							m_logger.Info ("Sending message '" + strMessage + "' to content server " + contentServer.IP + ":" + contentServer.Port);

							// Connect to the tracker
							TcpClient tc = new TcpClient (contentServer.IP, contentServer.Port);	// ip and port    

							NetworkStream ns = tc.GetStream ();
							StreamWriter sw = new StreamWriter (ns);
							sw.WriteLine (strMessage);//csv message
							sw.Flush ();

							ns.Close ();
							sw.Close ();
						}
					}
					catch (Exception exc)
					{
						m_logger.Error ("Error sending message to content server", exc);
					}
				}
			}
		}

		/// <summary>
		/// SendUpdateUserInventory
		/// </summary>
		public void SendUpdateUserInventory ()
		{
//			try
//			{
//				DataRow drUser = UsersUtility.GetUser (UserId);
//				DataRow drAsset;
//				DataTable dtOrderItems = StoreUtility.GetOrderItems (OrderId, "");
//
//				for (int i = 0; i < dtOrderItems.Rows.Count; i ++)
//				{
//					drAsset = StoreUtility.GetAsset (Convert.ToInt32 (dtOrderItems.Rows [i]["asset_id"]));
//					// Shui requested to remove (6/21/2005)
//					//SendMessage ("0," + drUser ["username"].ToString () + "," + drAsset ["info_hash"].ToString () + "," + dtOrderItems.Rows [i]["number_of_downloads"].ToString () + "," + dtOrderItems.Rows [i]["number_of_downloads_allowed"].ToString ());
//
//					// Also send the message to the tracker that the user is allowed to download from this IP with this token
//					DataRow drOrder = StoreUtility.GetOrder (OrderId);
//					string strStartDate = "";
//					if (drOrder != null && !drOrder ["download_end_date"].Equals (DBNull.Value))
//					{
//						strStartDate = Convert.ToDateTime (drOrder ["download_end_date"]).ToString ("yyyyMMddhhmm");
//					}
//					string strEndDate = "";
//					if (drOrder != null && !drOrder ["download_stop_date"].Equals (DBNull.Value))
//					{
//						strEndDate = Convert.ToDateTime (drOrder ["download_stop_date"]).ToString ("yyyyMMddhhmm");
//					}
//
//					SendMessage ("4," + dtOrderItems.Rows [i]["token"].ToString () + "," + drUser ["username"].ToString () + "," + IPAddress + "," + 
//						drAsset ["info_hash"].ToString () + "," + strStartDate + "," + strEndDate
//						+"," + dtOrderItems.Rows [i]["number_of_downloads"].ToString () +
//						"," + dtOrderItems.Rows [i]["number_of_downloads_allowed"].ToString ());
//				}
//			}
//			catch (Exception exc)
//			{
//				m_logger.Error ("Error sending update inventory message", exc);
//			}
		}

		/// <summary>
		/// SendJoinUserCommunity
		/// </summary>
		public void SendJoinUserCommunity ()
		{
			// Shui requested to remove (6/21/2005)
//			try
//			{
//				if (!CommunityUtility.IsCommunityPublic (CommunityId))
//				{
//					DataRow drUser = UsersUtility.GetUser (UserId);
//					SendMessage ("1," + drUser ["username"].ToString () + "," + CommunityId + ",1");
//				}
//			}
//			catch (Exception exc)
//			{
//				m_logger.Error ("Error sending join user community message", exc);
//			}
		}

		/// <summary>
		/// SendRemoveUserCommunity
		/// </summary>
		public void SendRemoveUserCommunity ()
		{
			// Shui requested to remove (6/21/2005)
//			try
//			{
//				if (!CommunityUtility.IsCommunityPublic (CommunityId))
//				{
//					DataRow drUser = UsersUtility.GetUser (UserId);
//					SendMessage ("1," + drUser ["username"].ToString () + "," + CommunityId + ",0");
//				}
//			}
//			catch (Exception exc)
//			{
//				m_logger.Error ("Error sending remove user community message", exc);
//			}
		}

		/// <summary>
		/// SendUpdateUserStatus
		/// </summary>
		public void SendUpdateUserStatus ()
		{
			// Shui requested to remove (6/21/2005)
//			try
//			{
//				DataRow drUser = UsersUtility.GetUser (UserId);
//				string password = "";
//				string salt = "";
//				int role = 0;
//				int userId = 0;
//				int userStatus = 0;
//				UsersUtility.GetUser (drUser ["username"].ToString (), ref password, ref salt, ref role, ref userId, ref userStatus); 
//				SendMessage ("2," + drUser ["username"].ToString () + "," + password + "," + salt + "," + userStatus.ToString () + "");
//			}
//			catch (Exception exc)
//			{
//				m_logger.Error ("Error sending update user status message", exc);
//			}
		}

		/// <summary>
		/// SendUpdateAsset
		/// </summary>
		public void SendUpdateAsset ()
		{
////			try
////			{
////				DataRow drAsset = StoreUtility.GetAsset (AssetId, true);
////				
////				// Don't send it for games
////				if (StoreUtility.IsKanevaGame (drAsset))
////				{
////					return;
////				}
////
////				bool isPrivate = false;
////				int communityId = 0;
//////				int communityId = Convert.ToInt32 (drAsset ["community_id"]);
//////				bool isPrivate = communityId > 0 && 
//////					!CommunityUtility.IsCommunityPublic (Convert.ToInt32 (drAsset ["community_id"]));
////
////				int torrentStatus = -1;
////				
////				if (!drAsset ["torrent_status"].Equals (DBNull.Value))
////				{
////					torrentStatus = Convert.ToInt32(drAsset ["torrent_status"]);
////				}
////
////				bool uploading = torrentStatus == (int) Constants.eTORRENT_STATUS.NOSCRAPE ||
////					torrentStatus == (int) Constants.eTORRENT_STATUS.UPLOADING ||
////					torrentStatus == (int) Constants.eTORRENT_STATUS.TORRENT_POSTED;
////
////				SendMessage ("3," + drAsset ["info_hash"].ToString () + "," + (Convert.ToDouble (drAsset ["amount"]).Equals (0.0)?"1":"0") + "," + communityId.ToString ()
////					+"," + (uploading? "1" : "0") +"," + drAsset ["file_size"].ToString () +
////					"," + (isPrivate? "1" : "0") +
////					"," + drAsset ["status_id"].ToString ());
////			}
////			catch (Exception exc)
////			{
////				m_logger.Error ("Error sending update asset message", exc);
////			}
		}

		/// <summary>
		/// Send a message to all trackers
		/// </summary>
		/// <param name="message"></param>
		/// <returns></returns>
		public void SendContentServerMessage (string strMessage)
		{
			if (strMessage.Trim ().Length == 0)
			{
				return;
			}	

			// Get the list of trackers
			System.Collections.Hashtable htContentServers = KanevaGlobals.ContentServers;

			if (htContentServers == null)
			{
				m_logger.Warn ("No content servers defined in web.config");
			}
			else
			{
				System.Collections.IDictionaryEnumerator myEnumerator = htContentServers.GetEnumerator ();

				while ( myEnumerator.MoveNext() )
				{
					try
					{
						ContentServer contentServer = (ContentServer) myEnumerator.Value;
						
						m_logger.Info ("Sending message '" + strMessage + "' to content server " + contentServer.IP + ":" + contentServer.Port);

						// Connect to the tracker
						TcpClient tc = new TcpClient (contentServer.IP, contentServer.Port);	// ip and port    

						NetworkStream ns = tc.GetStream ();
						StreamWriter sw = new StreamWriter (ns);
						sw.WriteLine (strMessage);//csv message
						sw.Flush ();

						ns.Close ();
						sw.Close ();
					}
					catch (Exception exc)
					{
						m_logger.Error ("Error sending message to content server", exc);
					}
				}
			}
			
		}

		/// <summary>
		/// Send a message to all trackers
		/// </summary>
		/// <param name="message"></param>
		/// <returns></returns>
		private void SendMessage (string strMessage)
		{
//			if (strMessage.Trim ().Length == 0)
//			{
//				return;
//			}	
//
//			// Get the list of trackers
//			System.Collections.Hashtable htTrackers = KanevaGlobals.Trackers;
//
//			if (htTrackers == null)
//			{
//				m_logger.Warn ("No trackers defined in web.config");
//			}
//			else
//			{
//				System.Collections.IDictionaryEnumerator myEnumerator = htTrackers.GetEnumerator ();
//
//				while ( myEnumerator.MoveNext() )
//				{
//					try
//					{
//						Tracker tracker = (Tracker) myEnumerator.Value;
//						
//						m_logger.Info ("Sending message '" + strMessage + "' to " + tracker.IP + ":" + tracker.CacheUpdatePort);
//
//						// Connect to the tracker
//						TcpClient tc = new TcpClient (tracker.IP, tracker.CacheUpdatePort);	// ip and port    
//
//						NetworkStream ns = tc.GetStream ();
//						StreamWriter sw = new StreamWriter (ns);
//						sw.WriteLine (strMessage);//csv message
//						sw.Flush ();
//
//						ns.Close ();
//						sw.Close ();
//					}
//					catch (Exception exc)
//					{
//						m_logger.Error ("Error sending message to tracker cache", exc);
//					}
//				}
//			}
		}

		/// <summary>
		/// SendUpdateTorrentUploadPercents
		/// </summary>
		public void SendUpdateTorrentUploadPercents ()
		{
			try
			{
				SendContentServerMessage ("6," + m_Torrents);
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error sending SendUpdateTorrentUploadPercents", exc);
			}
		}

		/// <summary>
		/// The userId to send
		/// </summary>
		private int m_UserId = 0;
		private int m_CommunityId = 0;
		private int m_AssetId = 0;
		private int m_OrderId = 0;
		private string m_IPAddress = "";
		private string m_infoHash = "";
		private string m_Torrents = "";

		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

	}
}
