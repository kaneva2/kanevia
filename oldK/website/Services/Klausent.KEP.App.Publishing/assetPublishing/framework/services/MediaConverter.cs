///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace KlausEnt.KEP.AssetPublishing
{
	/// <summary>
	/// Summary description for MediaConverter.
	/// </summary>
	public class MediaConverter
	{
		public MediaConverter(string IP, int Port)
		{
			m_IP = IP;
			m_Port = Port;
		}

		public string IP
		{
			get 
			{
				return m_IP;
			}
		}

		public int Port
		{
			get 
			{
				return m_Port;
			}
		}

		private string m_IP;
		private int m_Port;
	}
}
