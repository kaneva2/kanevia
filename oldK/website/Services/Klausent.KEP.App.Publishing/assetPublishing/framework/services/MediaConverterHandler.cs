///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.Xml;

// Import log4net classes.
using log4net;

namespace KlausEnt.KEP.AssetPublishing
{
	/// <summary>
	/// Summary description for MediaConverterSectionHandler.
	/// </summary>
	public class MediaConverterSectionHandler : IConfigurationSectionHandler
	{
		public MediaConverterSectionHandler()
		{
		}

		public object Create (object parent, object configContext, System.Xml.XmlNode section)
		{
			XmlNodeList mediaConverterSettings;

			System.Collections.Hashtable htMediaConverterServers = new System.Collections.Hashtable ();

			mediaConverterSettings = section.SelectNodes ("converters//mediaConverter");

			foreach( XmlNode nodeTracker in mediaConverterSettings)
			{
				try
				{
					MediaConverter mediaConverter = new MediaConverter (nodeTracker.Attributes.GetNamedItem ("IP").Value, Convert.ToInt32 (nodeTracker.Attributes.GetNamedItem ("Port").Value));

					htMediaConverterServers.Add (nodeTracker.Attributes.GetNamedItem ("IP").Value + nodeTracker.Attributes.GetNamedItem ("Port").Value, mediaConverter);
				}
				catch (Exception exc)
				{
					m_logger.Error ("Error reading media converter configuration", exc);
				}

			}

			return htMediaConverterServers;
		}

		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

	}

}
