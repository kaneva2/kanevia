///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using KlausEnt.KEP.GameUploadPublishing;

namespace KlausEnt.KEP.AssetPublishing
{
	/// <summary>
	/// Summary description for IGameUploadProcessor.
	/// </summary>
	public interface IGameUploadProcessor
	{
		void Process(GameUpload gameUpload);
	}
}
