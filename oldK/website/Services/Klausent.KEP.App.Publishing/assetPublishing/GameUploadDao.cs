///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace KlausEnt.KEP.GameUploadPublishing
{
	/// <summary>
	/// This class uses StoreUtility to populate/save gameUpload records
	/// </summary>
	public class GameUploadDao
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(GameUploadDao));
		private static GameUploadDao instance;

		private GameUploadDao()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static GameUploadDao GetInstance()
		{
			if(instance == null)
			{
				instance = new GameUploadDao() ;
			}
			return instance;
		}

		/// <summary>
		/// get all gameUploads to be processed
		/// </summary>
		/// <returns></returns>
		public IList GetAll()
		{
			IList retVal = new ArrayList();
            //DataTable table = StoreUtility.GetGameUploadsToProcess();
            //if(table != null && table.Rows.Count > 0)
            //{
            //    foreach(DataRow row in table.Rows)
            //    {
            //        GameUpload gameUpload = new GameUpload();
            //        gameUpload.GameId = int.Parse(row["asset_id"].ToString());
            //        gameUpload.GameUploadId = int.Parse(row["game_upload_id"].ToString());
            //        gameUpload.UserId = int.Parse(row["owner_id"].ToString());
            //        gameUpload.UploadedDir = row["path"].ToString();
            //        gameUpload.StagingDir = row["IT_game_directory"].ToString();
            //        gameUpload.PublishStatusId = int.Parse(row["publish_status_id"].ToString());
            //        gameUpload.Name = row["name"].ToString();
            //        gameUpload.UserEmail = row["email"].ToString();
            //        retVal.Add(gameUpload);
            //    }
            //}
			return retVal;
		}
	}
}
