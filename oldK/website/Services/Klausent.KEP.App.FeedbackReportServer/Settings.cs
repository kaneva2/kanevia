///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace ErrorReporting
{
	/// <summary>
	/// Summary description for Settings.
	/// </summary>
	public class Settings
	{
		private static Settings instance;
		private string _errorReportPath;
		private string _mantisConnectUrl;
		private string _mantisUserName;
		private string _mantisPassword;
		private string _mantisProject;
		private string _mantisCategory;

		private Settings()
		{
			_errorReportPath = 
				System.Configuration.ConfigurationSettings.AppSettings ["ErrorReportPath"];

			_mantisConnectUrl = System.Configuration.ConfigurationSettings.AppSettings ["MantisConnectUrl"];
			_mantisUserName = System.Configuration.ConfigurationSettings.AppSettings ["MantisUserName"];
			_mantisPassword = System.Configuration.ConfigurationSettings.AppSettings ["MantisPassword"];
			_mantisProject = System.Configuration.ConfigurationSettings.AppSettings ["MantisProject"];
			_mantisCategory = System.Configuration.ConfigurationSettings.AppSettings ["MantisCategory"];
		}

		public static Settings GetInstance()
		{
			if(instance == null)
			{
				instance = new Settings() ;
			}

			return instance;
		}

		public string ErrorReportPath
		{
			get { return _errorReportPath; }
			set { _errorReportPath = value; }
		}

		public string MantisConnectUrl
		{
			get { return _mantisConnectUrl; }
			set { _mantisConnectUrl = value; }
		}

		public string MantisUserName
		{
			get { return _mantisUserName; }
			set { _mantisUserName = value; }
		}

		public string MantisPassword
		{
			get { return _mantisPassword; }
			set { _mantisPassword = value; }
		}

		public string MantisProject
		{
			get { return _mantisProject; }
			set { _mantisProject = value; }
		}

		public string MantisCategory
		{
			get { return _mantisCategory; }
			set { _mantisCategory = value; }
		}
	}
}
