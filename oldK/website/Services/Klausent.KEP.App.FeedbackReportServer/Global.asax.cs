///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Security.Principal;
using System.Web;
using System.Web.SessionState;
using log4net;
using log4net.Config;


namespace ErrorReporting 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(Global));

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		public Global()
		{
			InitializeComponent();
		}	
		
		protected void Application_Start(Object sender, EventArgs e)
		{
			DOMConfigurator.Configure (new System.IO.FileInfo(
				System.Configuration.ConfigurationSettings.AppSettings ["LogConfigFile"]));
			if(log.IsInfoEnabled)
			{
				log.Info("Setup log4net");
			}
		}
 
		protected void Session_Start(Object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_Error(Object sender, EventArgs e)
		{

		}

		protected void Session_End(Object sender, EventArgs e)
		{

		}

		protected void Application_End(Object sender, EventArgs e)
		{

		}
			
		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

namespace KlausEnt.KEP.Kaneva 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		/// <summary>
		/// Get the version
		/// </summary>
		/// <returns></returns>
		public static string Version ()
		{
			if (version == null)
			{
				// Get the current version
				System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly ();
				System.Reflection.AssemblyName an = a.GetName();
				version = "version " + an.Version.Major + "." + an.Version.Minor + "." + an.Version.Build + "." + an.Version.Revision;
			}

			return version;
		}
		/// <summary>
		/// Get the current cache
		/// </summary>
		/// <returns></returns>
		public static System.Web.Caching.Cache Cache ()
		{
			return HttpRuntime.Cache;
		}

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		private static DatabaseUtility m_DatabaseUtilty;

		/// <summary>
		/// Get the database utility
		/// </summary>
		/// <returns></returns>
		public static DatabaseUtility GetDatabaseUtility ()
		{
			if (m_DatabaseUtilty == null)
			{
				switch (KanevaGlobals.DatabaseType)
				{
					case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
					{
						m_DatabaseUtilty = new SQLServerUtility (KanevaGlobals.ConnectionString);
						break;
					}
					case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
					{
						m_DatabaseUtilty = new MySQLUtility (KanevaGlobals.ConnectionString);
						break;
					}
					default:
					{
						throw new Exception ("This database type is not supported");
					}
				}
			}
			return m_DatabaseUtilty;
		}

		public Global()
		{
			InitializeComponent();
		}	
		
		protected void Application_Start(Object sender, EventArgs e)
		{
			
		}
 
		protected void Session_Start(Object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{
			if (Request.IsAuthenticated)
			{
				//HttpContext.Current.User = new GenericPrincipal (User.Identity, UsersUtility.GetUserRoles (User.Identity.Name));
			}
		}

		protected void Application_Error(Object sender, EventArgs e)
		{
			
		}

		protected void Session_End(Object sender, EventArgs e)
		{

		}

		protected void Application_End(Object sender, EventArgs e)
		{

		}
			
		/// <summary>
		/// Version number
		/// </summary>
		private static string version = null;

		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

