///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Web.Mail;
using System.Web.Services;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace ErrorReporting
{
	/// <summary>
	/// Summary description for ErrorReporting.
	/// </summary>
	[WebService(Namespace="http://www.kaneva.com/webservices/",Description="Kaneva Error Reporting Service.")]
	public class ErrorReporting : System.Web.Services.WebService
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(ErrorReporting));

		public ErrorReporting()
		{
			//CODEGEN: This call is required by the ASP.NET Web Services Designer
			InitializeComponent();
		}

		#region Component Designer generated code
		
		//Required by the Web Services Designer 
		private IContainer components = null;
				
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);		
		}
		
		#endregion
		
		/// <summary>
		/// </summary>
		/// <returns>Zero if successful, error code otherwise.</returns>
		[WebMethod]
		public int RecordErrorReportV2 ( string subject, string description, int feedbackType,
			byte[] report)
		{
            return RecordErrorReportLocal(subject, description, feedbackType, report);
        }


        /// <summary>
		/// </summary>
		/// <returns>Zero if successful, error code otherwise.</returns>
		[WebMethod]
		public int RecordErrorReport( string subject, string description, bool isErrorReport, byte[] report)
        {
            return RecordErrorReportLocal (subject, description, (int) (isErrorReport ? Constants.eFeedback.ERROR : Constants.eFeedback.FEEDBACK), report);
        }


        private int RecordErrorReportLocal (string subject, string description, int feedbackType,
			byte[] report)
        {
			int ret = (int) KlausEnt.KEP.Kaneva.Constants.eERROR_REPORTING_RETURNCODES.SUCCESS;

			//summary and desc are required
			if(subject.Length == 0)
			{
				subject = "N/A";
			}
			if(description.Length == 0)
			{
				description = "N/A";
			}

			int feedbackId = StoreUtility.InsertFeedback(subject,description,
				this.Context.Request.UserHostAddress,"",
				feedbackType);

			if ( (feedbackId == 0) || (feedbackId < 0) )
			{
				return (int) KlausEnt.KEP.Kaneva.Constants.eERROR_REPORTING_RETURNCODES.ERROR_RECORDING_ISSUE_IN_DB;
			}
			else
			{
				String filePath = Settings.GetInstance().ErrorReportPath 
					+ Path.DirectorySeparatorChar + feedbackId + ".zip";
				try
				{
					FileStream attachmentFile = new FileStream(filePath
						, FileMode.Create, FileAccess.Write);
					attachmentFile.Write(report, 0, report.Length);
					attachmentFile.Close();
					StoreUtility.UpdateFeedbackFileLink(feedbackId, filePath);
				}catch(Exception e)
				{
					ret = (int) KlausEnt.KEP.Kaneva.Constants.eERROR_REPORTING_RETURNCODES.ERROR_SAVING_REPORT_FILE;
					log.Error("Failed to save report file to " + filePath, e);
				}
			}

			return ret;
		}

		
		[WebMethod]
		public int SendNotificationEmail( string adminEmail, string adminEmailSubject, string adminEmailBody)
		{
			int ret = (int) KlausEnt.KEP.Kaneva.Constants.eERROR_REPORTING_RETURNCODES.SUCCESS;

			if(ret == (int)Constants.eERROR_REPORTING_RETURNCODES.SUCCESS)
			{
				//send admin an email
				try
				{
					MailUtility.SendEmail(KanevaGlobals.FromEmail,adminEmail,adminEmailSubject,adminEmailBody,false, false, 2);
				}catch(Exception e)
				{
					log.Error("Failed to send email notification to " + adminEmail, e);
					ret = (int) KlausEnt.KEP.Kaneva.Constants.eERROR_REPORTING_RETURNCODES.ERROR_SENDING_EMAIL_NOTIFICATION;
				}
			}

			return ret;
		}
	}
}
