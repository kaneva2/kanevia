///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace ErrorReporting
{
    /// <summary>
    /// Summary description for ReportUserHardware
    /// </summary>
    [WebService(Namespace = "http://www.kaneva.com/webservices/", Description = "Kaneva User Hardware Reporting Service.")]
    public class ReportUserHardware : System.Web.Services.WebService
    {
        /// <summary>
        /// RecordUserHardware
        /// </summary>
        /// <returns>Zero if successful, error code otherwise</returns>
        [WebMethod]
        public int RecordUserHardware (int userId, string operatingSystem, string systemManufacturer, string systemModel, string bios,
            string processor, string memory, string directXVersion, string cardName, string manufacturer, string chipType, string dacType,
            string displayMemory, string currentMode)
        {
            return UsersUtility.InsertUserHardware(userId, operatingSystem, systemManufacturer, systemModel, bios,
                processor, memory, directXVersion, cardName, manufacturer, chipType, dacType,
                displayMemory, currentMode);
        }
    }
}
