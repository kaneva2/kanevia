///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Timers;
using System.IO;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.Billing
{
	public class Billing : System.ServiceProcess.ServiceBase
	{

		static Billing ()
		{
			// Set up log for net
			string s = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
			FileInfo l_fi = new FileInfo (s);
			log4net.Config.DOMConfigurator.Configure (l_fi);
            log.Info("Startup");
		}

		public Billing ()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent ();

			string servicePollInterval = System.Configuration.ConfigurationSettings.AppSettings ["servicePollInterval"];

			double minuteInterval = 60;

			try 
			{
				minuteInterval = Convert.ToDouble (servicePollInterval);
			}
			catch (Exception) {}

			if (log.IsInfoEnabled)
			{
				log.Info ("Kaneva Billing Service is scheduled to run every " + minuteInterval + " minutes");
			}
	
			tmrBilling = new System.Timers.Timer (60000 * minuteInterval);
			tmrBilling.Elapsed += new ElapsedEventHandler (this.tmrBilling_Tick);

		//	RunBilling ();

            // Run A test

		}

		// The main entry point for the process
		static void Main ()
		{
			System.ServiceProcess.ServiceBase [] ServicesToRun;
	
			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = new System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
			//
			ServicesToRun = new System.ServiceProcess.ServiceBase [] { new Billing () };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
			components = new System.ComponentModel.Container ();
			this.ServiceName = "Kaneva Subscription Billing Service";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose (bool disposing)
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose ();
				}
			}
			base.Dispose (disposing);
		}

		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		protected override void OnStart (string [] args)
		{
            //// Run on start up?
            //if (System.Configuration.ConfigurationSettings.AppSettings ["runOnStartup"] != null)
            //{
            //    if (Convert.ToBoolean (System.Configuration.ConfigurationSettings.AppSettings ["runOnStartup"]))
            //    {
            //        RunBilling ();
            //    }
            //}

			tmrBilling.AutoReset = true;
			tmrBilling.Enabled = true;
			tmrBilling.Start ();
		}
 
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop ()
		{
			tmrBilling.AutoReset = false;
			tmrBilling.Enabled = false;
		}

		/// <summary>
		/// Pause this service
		/// </summary>
		protected override void OnPause () 
		{
			tmrBilling.Stop ();
		}

		/// <summary>
		/// Continue this service
		/// </summary>
		protected override void OnContinue () 
		{
			tmrBilling.Start ();
		}


		/// <summary>
		/// tmrBilling_Tick
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tmrBilling_Tick (object sender, System.Timers.ElapsedEventArgs e) 
		{
			tmrBilling.Stop ();
			RunBilling ();
			tmrBilling.Start ();
		}

		/// <summary>
		/// RunBilling
		/// </summary>
		private void RunBilling ()
		{
			log.Info ("Billing Service Process Start");

            //// Run a test 
            //log.Info("Start Test");
            //try
            //{
            //    bool bSystemDownTest = false;
            //    bool bSuccessTest = false;
            //    string userErrorMessageTest = "";
            //    CybersourceAdaptor cybersourceAdaptorTest = new CybersourceAdaptor();
            //    SubscriptionInfo subscriptionInfoTest = cybersourceAdaptorTest.GetSubscription(ref bSuccessTest, ref bSystemDownTest, ref userErrorMessageTest, "2457831534150008415107", 1);
            //    log.Info("Status = " + subscriptionInfoTest.Status.ToString());
            //}
            //catch (Exception exc)
            //{
            //    log.Error("Sub Test Error ",exc);
            //}
            //log.Info("End Test");

			try 
			{
                // Get all renewable subscriptions that have expired and have not
                // cancelled (still in active state)
                string sqlSelect = "SELECT us.user_subscription_id, us.user_id, us.subscription_id, us.end_date, us.purchase_date, us.price, " +
                    " us.auto_renew, us.status_id, us.subscription_identifier, s.term, u.email, us.is_legacy " +
                    " FROM user_subscriptions us, subscriptions s, users u " +
                    " WHERE us.user_id = u.user_id " +
                    " AND us.auto_renew = 'Y' " +
                    " AND us.end_date < NOW() " +
                    " AND us.status_id = " + (int) Subscription.SubscriptionStatus.Active +
                    " AND us.subscription_id = s.subscription_id ";

				DataTable dtExpired = Global.GetDatabaseUtility ().GetDataTable (sqlSelect);
				Double dSubscriptionAmount;
				int userId;
                uint userSubscriptionId = 0;
                string subscription_identifier = "";
                int subscriptionId = 0;
                string userErrorMessage = "";
                int numberOfPendingDays = 6;

                SubscriptionFacade subscriptionFacade = new SubscriptionFacade();
                CybersourceAdaptor cybersourceAdaptor = new CybersourceAdaptor();
                UserFacade userFacade = new UserFacade();

				bool bSystemDown = false;
                bool bSuccess = false;

				// Extend each subscription
                for (int i = 0; i < dtExpired.Rows.Count; i++)
                {
                    userId = Convert.ToInt32(dtExpired.Rows[i]["user_id"]);
                    userSubscriptionId = Convert.ToUInt32(dtExpired.Rows[i]["user_subscription_id"]);
                    dSubscriptionAmount = Convert.ToDouble(dtExpired.Rows[i]["price"]);
                    subscription_identifier = dtExpired.Rows[i]["subscription_identifier"].ToString();
                    subscriptionId = Convert.ToInt32(dtExpired.Rows[i]["subscription_id"]);


                    // ***********************************************
                    // Handle Legacy Subscriptions
                    // ***********************************************
                    if (dtExpired.Rows[i]["is_legacy"].ToString().ToUpper().Equals("Y"))
                    {
                        // Get Cybersource Subscription
                        SubscriptionInfo subscriptionInfo = cybersourceAdaptor.GetSubscription(ref bSuccess, ref bSystemDown, ref userErrorMessage, subscription_identifier, subscriptionId);

                        if (bSuccess)
                        {
                            // Possible Status are 
                            //Status of the subscription. Possible values:
                            //� Canceled: Subscription�s payment schedule has been canceled
                            //� Completed: Subscription schedule is completed and no more payments are scheduled
                            //� Current: Subscription is active
                            //� Hold: Subscription is on hold. See Handling Exceptions for information about why subscriptions go on hold.
                            if (subscriptionInfo.Status.ToUpper().Equals("CURRENT"))
                            {
                                // Extend it another Term
                                UserSubscription userSubscription = subscriptionFacade.GetUserSubscription(userSubscriptionId);
                                userSubscription.EndDate = subscriptionFacade.GetNextEndDate(Convert.ToDateTime (dtExpired.Rows[i]["purchase_date"]),
                                    Convert.ToDateTime(dtExpired.Rows[i]["end_date"]), userSubscription.RenewalTerm); 
                                subscriptionFacade.UpdateUserSubscription(userSubscription);

                                log.Info("User Subscription Id " + userSubscriptionId + " is current extend end date");

                                try
                                {
                                    DataRow[] matchingPassGroups = subscriptionFacade.GetPassGroupsToSubscription().Select("subscription_id = " + userSubscription.SubscriptionId);

                                    //process for each pass group associated with the subscription
                                    for (int j = 0; j < matchingPassGroups.Length; j++)
                                    {
                                        //does extra features and emailing based on pass type
                                        if (Convert.ToInt32(matchingPassGroups[j]["pass_group_id"]) == (int)KanevaGlobals.VipPassGroupID)
                                        {
                                            (new UserFacade()).ChangePlayerNameColor(userSubscription.UserId, (int)Constants.ePLAYER_NAME_COLORS.VIP);
                                        }
                                    }
                                }
                                catch (Exception exc)
                                {
                                    log.Error("Error setting VIP color", exc);
                                }


                                // Insert into pending Allowance
                                if (userSubscription.MontlyAllowance > 0)
                                {
                                    log.Info("Add pending allowance " + userSubscriptionId.ToString() + " " + userSubscription.MontlyAllowance.ToString() + " in " + numberOfPendingDays + " days ");

                                    string sqlInsert = "INSERT INTO subscription_pending_allowance (user_subscription_id, award_attempt_date) " +
                                        " VALUES " +
                                        " (@userSubscriptionId, DATE_ADD(NOW(), INTERVAL " + numberOfPendingDays + " DAY)) ";
                                    //userSubscriptionId
                                    Hashtable parameters = new Hashtable();
                                    parameters.Add("@userSubscriptionId", userSubscriptionId);
                                    Global.GetDatabaseUtility().ExecuteNonQuery(sqlInsert, parameters);
                                }


                                // Get subscription billing history


                            }
                            else if (subscriptionInfo.Status.ToUpper().Equals("HOLD"))
                            {
                                // Table 2 Failures that Immediately Place a Subscription on Hold without Retries
                                // Description
                                // Expired credit card
                                // Lost or stolen credit card
                                // Inactive card or card not authorized for card-not-present transactions
                                // Invalid credit card number
                                // Card type not supported by the processor
                                // General decline by the processor for invalid data
                                // The customer�s bank account is frozen
                                // The check processor declined the transaction based on a general issue with the customer�s account
                                // The customer matched an entry on the check processor�s negative file

                                UserSubscription userSubscription = subscriptionFacade.GetUserSubscription(userSubscriptionId);
                                userSubscription.StatusId = Subscription.SubscriptionStatus.NoPayment;
                                subscriptionFacade.UpdateUserSubscription(userSubscription);

                                log.Info("Subscription Id " + subscriptionId + " on HOLD changed to No Payment");

                                MailUtility.SendEmail(KanevaGlobals.FromEmail, dtExpired.Rows[i]["email"].ToString(), "Subscription Billing Ended", "Your Kaneva Subcription has ended", false, false, 1);
                            }
                            else if (subscriptionInfo.Status.ToUpper().Equals("COMPLETED") || subscriptionInfo.Status.ToUpper().Equals("CANCELED"))
                            {
                                UserSubscription userSubscription = subscriptionFacade.GetUserSubscription(userSubscriptionId);
                                userSubscription.StatusId = Subscription.SubscriptionStatus.Expired;
                                subscriptionFacade.UpdateUserSubscription(userSubscription);

                                log.Info("Subscription Id " + subscriptionId + " on Complete or Canceled changed to Expired");

                                MailUtility.SendEmail(KanevaGlobals.FromEmail, dtExpired.Rows[i]["email"].ToString(), "Subscription Billing Ended", "Your Kaneva Subcription has ended", false, false, 1);
                            }
                            else
                            {
                                log.Warn("Unknown status");
                            }
                        }
                        else
                        {
                            // If bSystemDown ignore, just try again later
                            log.Error("Call failed " + userErrorMessage);
                        }
                    }


                    // ***********************************************
                    // These are NOT Legacy subscriptions
                    // ***********************************************
                    else
                    {
                        log.Info("Check userSubscriptionId " + userSubscriptionId.ToString());

                        UserSubscription userSubscription = subscriptionFacade.GetUserSubscription(userSubscriptionId);
                        int orderId = StoreUtility.CreateOrder(userId, "", (int)Constants.eORDER_STATUS.COMPLETED);

                        int transactionId = StoreUtility.PurchasePoints(userId, (int)Constants.eTRANSACTION_STATUS.PENDING, "Subscription Credits", 0,
                            (int)Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE, userSubscription.Price,
                            (double)userSubscription.MontlyAllowance, 0,
                            0, "");

                        StoreUtility.UpdatePointTransaction(transactionId, orderId);
                        StoreUtility.UpdateOrderPointTranasactionId(orderId, transactionId);

                        // Associate it with an order
                        subscriptionFacade.InsertUserSubscriptionOrder(userSubscriptionId, orderId);

                        DataRow drOrder = StoreUtility.GetOrder (orderId);
                        DataRow drPpt = StoreUtility.GetPurchasePointTransaction(transactionId);

                        log.Info("Calling Cybersource");

                        bool processSuccesful = cybersourceAdaptor.ProcessPaymentUsingSubscription (drOrder, drPpt, subscription_identifier, "", ref userErrorMessage, ref bSystemDown, true);

                        if (processSuccesful)
                        {
                            log.Info("Sucess");

                            StoreUtility.UpdatePointTransaction(transactionId, (int)Constants.eTRANSACTION_STATUS.VERIFIED, "", subscription_identifier);

                            // Extend it another Term
                            userSubscription.EndDate = subscriptionFacade.GetNextEndDate(Convert.ToDateTime(dtExpired.Rows[i]["purchase_date"]),
                                Convert.ToDateTime(dtExpired.Rows[i]["end_date"]), userSubscription.RenewalTerm); 
                            subscriptionFacade.UpdateUserSubscription(userSubscription);

                            log.Info("User Subscription Id " + userSubscriptionId + " is current extend end date");

                            try
                            {
                                DataRow[] matchingPassGroups = subscriptionFacade.GetPassGroupsToSubscription().Select("subscription_id = " + userSubscription.SubscriptionId);

                                //process for each pass group associated with the subscription
                                for (int j = 0; j < matchingPassGroups.Length; j++)
                                {
                                    //does extra features and emailing based on pass type
                                    if (Convert.ToInt32(matchingPassGroups[j]["pass_group_id"]) == (int)KanevaGlobals.VipPassGroupID)
                                    {
                                        (new UserFacade()).ChangePlayerNameColor(userSubscription.UserId, (int)Constants.ePLAYER_NAME_COLORS.VIP);
                                    }
                                }
                            }
                            catch (Exception exc)
                            {
                                log.Error("Error setting VIP color", exc);
                            }

                            // Insert into pending Allowance
                            if (userSubscription.MontlyAllowance > 0)
                            {
                                userFacade.AdjustUserBalance(userSubscription.UserId, Constants.CURR_KPOINT, (double)userSubscription.MontlyAllowance, Constants.CASH_TT_SUBSCRIPTIONS);
                                log.Info("Awarded monthly allowance " + userSubscriptionId.ToString() + " " + userSubscription.MontlyAllowance.ToString());
                            }
                        }
                        else
                        {
                            log.Info("Failed to make payment see purchase_point_transaction for details " + userErrorMessage);

                            // Failed to make payment
                            StoreUtility.UpdatePointTransaction(transactionId, (int)Constants.eTRANSACTION_STATUS.FAILED, userErrorMessage, subscription_identifier);

                            userSubscription = subscriptionFacade.GetUserSubscription(userSubscriptionId);
                            userSubscription.StatusId = Subscription.SubscriptionStatus.NoPayment;
                            subscriptionFacade.UpdateUserSubscription(userSubscription);
                        }
                    }
                }









                // ***********************************************
                // Only Legacy Subscriptions do this for fraud reasons
                // Get subscriptions that are pending credits/rewards allowance
                // ***********************************************
                string sql = "SELECT sub_pending_allowance_id, user_subscription_id " +
                    " FROM subscription_pending_allowance " +
                    " WHERE processed = 'N' " + 
                    " AND award_attempt_date < NOW() ";

                DataTable dtSubscripttionPendingAllowance = Global.GetDatabaseUtility().GetDataTable(sql);
                int subPendingAllowanceId = 0;

                for (int i = 0; i < dtSubscripttionPendingAllowance.Rows.Count; i++)
                {
                    userSubscriptionId = Convert.ToUInt32(dtSubscripttionPendingAllowance.Rows[i]["user_subscription_id"]);
                    subPendingAllowanceId = Convert.ToInt32(dtSubscripttionPendingAllowance.Rows[i]["sub_pending_allowance_id"]);
                    UserSubscription userSubscription = subscriptionFacade.GetUserSubscription(userSubscriptionId);

                    // Award the promotions
                    if (userSubscription.MontlyAllowance > 0)
                    {
                        // Check subscription status
                        SubscriptionInfo subscriptionInfo = cybersourceAdaptor.GetSubscription(ref bSuccess, ref bSystemDown, ref userErrorMessage, userSubscription.SubscriptionIdentifier, Convert.ToInt32 (userSubscription.SubscriptionId));

                        if (bSuccess)
                        {
                            // Possible Status are 
                            //Status of the subscription. Possible values:
                            //� Canceled: Subscription�s payment schedule has been canceled
                            //� Completed: Subscription schedule is completed and no more payments are scheduled
                            //� Current: Subscription is active
                            //� Hold: Subscription is on hold. See Handling Exceptions for information about why subscriptions go on hold.
                            if (subscriptionInfo.Status.ToUpper().Equals("CURRENT"))
                            {
                                int orderId = StoreUtility.CreateOrder(userSubscription.UserId, "", (int)Constants.eORDER_STATUS.COMPLETED);

                                int transactionId = StoreUtility.PurchasePoints(userSubscription.UserId, (int)Constants.eTRANSACTION_STATUS.VERIFIED, "Subscrription Credits", 0,
                                    (int)Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE, userSubscription.Price,
                                    (double)userSubscription.MontlyAllowance, 0,
                                    0, "");

                                StoreUtility.UpdatePointTransaction(transactionId, orderId);
                                StoreUtility.UpdateOrderPointTranasactionId(orderId, transactionId);

                                userFacade.AdjustUserBalance(userSubscription.UserId, Constants.CURR_KPOINT, (double)userSubscription.MontlyAllowance, Constants.CASH_TT_SUBSCRIPTIONS);

                                log.Info("Awarded Legacy monthly allowance " + userSubscriptionId.ToString() + " " + userSubscription.MontlyAllowance.ToString());

                                string sqlUpdate = " UPDATE subscription_pending_allowance " +
                                    " SET processed = 'Y', " +
                                    " success = 'Y', " +
                                    " order_id = " + orderId +
                                    " WHERE sub_pending_allowance_id = " + subPendingAllowanceId;
                                Global.GetDatabaseUtility().ExecuteNonQuery (sqlUpdate);

                                // Associate it with an order
                                subscriptionFacade.InsertUserSubscriptionOrder(userSubscriptionId, orderId);
                            }
                            else
                            {
                                // Subscription is not CURRENT
                                string sqlUpdate = " UPDATE subscription_pending_allowance " +
                                    " SET processed = 'Y', " +
                                    " success = 'N' " +
                                    " WHERE sub_pending_allowance_id = " + subPendingAllowanceId;
                                Global.GetDatabaseUtility().ExecuteNonQuery (sqlUpdate);
                            }



                        }

                    }
                }




			}
			catch (Exception exc)
			{
				log.Error ("Error processing billing", exc);
			}

			log.Info ("Billing Service Process Finished");
		}

		/// <summary>
		/// Timers
		/// </summary>
		System.Timers.Timer tmrBilling = new Timer ();

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Logger
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger (typeof (Billing));

		/// <summary>
		/// Used in db tables where user's ip address is expected
		/// </summary>
		private const string cORDER_HOST_ADDRESS = "Billing Service";

	}
}
