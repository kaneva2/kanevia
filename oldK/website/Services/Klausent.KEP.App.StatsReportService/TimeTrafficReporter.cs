///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using Klausent.KEP.App.StatsReportService.framework.reports;

namespace Klausent.KEP.App.StatsReportService
{
	/// <summary>
	/// Summary description for TimeTrafficReporter.
	/// </summary>
	public class TimeTrafficReporter : IReporter
	{
		private DateTime _inputDate;
		public TimeTrafficReporter()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		/// </summary>
		/// <returns></returns>
		public bool Generate()
		{
			bool retVal = true;
			ReportUtility.GenerateAssetFileTimeTraffic(InputDate);
			return retVal;
		}

		/// <summary>
		/// generate report for this date
		/// </summary>
		public DateTime InputDate
		{
			get { return _inputDate; }
			set { _inputDate = value; }
		}
	}
}
