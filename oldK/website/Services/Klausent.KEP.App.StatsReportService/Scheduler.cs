///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using Klausent.KEP.App.StatsReportService.framework.reports;
using log4net;

namespace Klausent.KEP.App.StatsReportService
{
	/// <summary>
	/// this class schedules different reporters to execute, in order if necessary
	/// </summary>
	public class Scheduler
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(Scheduler));

		private static Scheduler inst;
		private Scheduler()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static Scheduler GetInstance()
		{
			if ( inst == null)
			{
				inst = new Scheduler() ;
			}

			return inst;
		}

		public bool Run()
		{
			lock(this)
			{
				//sync this method access
				bool retVal = true;
//				Test();
				DoNewUserSignups();
				DoNewFileUploads();
				DoTimeTraffic();
				return retVal;
			}
		}

		private void Test()
		{
//			log.Info("running GetTimeTrafficHistogram");
//			DataSet ds1 = new ReportUtility().GetTimeTrafficHistogram(2017, new DateTime(2006,1,25), 
//				new DateTime(2006,2,2),1);
//			ds1.WriteXml("C:\\KanevaLogs\\test1.xml");
//			DataSet ds2 = new ReportUtility().GetTimeTrafficHistogram(2017, new DateTime(2006,1,25), 
//				new DateTime(2006,2,2),3);
//			ds2.WriteXml("C:\\KanevaLogs\\test3.xml");
//			DataSet ds3 = new ReportUtility().GetTimeTrafficHistogram(2017, new DateTime(2006,1,25), 
//				new DateTime(2006,2,2),4);
//			ds3.WriteXml("C:\\KanevaLogs\\test4.xml");
		}
		
		private void DoNewUserSignups()
		{
			DateTime fromDate = ReportUtility.GetLastCalculatedDateNewUserSignup().AddDays(1);
			fromDate = fromDate < Settings.GetInstance().GenerateReportFromDate ?
				Settings.GetInstance().GenerateReportFromDate : fromDate;

			DateTime toDate = DateTime.Now.AddDays(-1); //calculate till yesterday
			for(DateTime inputDate = fromDate; inputDate <= toDate; 
				inputDate = inputDate.AddDays(1))
			{
				log.Info("Calculating new user signup for date " + inputDate.ToShortDateString());
				NewUserSignupReporter newUserSignupReporter = new NewUserSignupReporter() ;
				newUserSignupReporter.InputDate = inputDate;
				newUserSignupReporter.Generate();
			}
		}

		private void DoNewFileUploads()
		{
			DateTime fromDate = ReportUtility.GetLastCalculatedDateFileUpload().AddDays(1);
			fromDate = fromDate < Settings.GetInstance().GenerateReportFromDate ?
				Settings.GetInstance().GenerateReportFromDate : fromDate;

			DateTime toDate = DateTime.Now.AddDays(-1); //calculate till yesterday
			for(DateTime inputDate = fromDate; inputDate <= toDate; 
				inputDate = inputDate.AddDays(1))
			{
				log.Info("Calculating new file upload for date " + inputDate.ToShortDateString());
				NewFileUploadReporter newFileUploadReporter = new NewFileUploadReporter() ;
				newFileUploadReporter.InputDate = inputDate;
				newFileUploadReporter.Generate();
			}
		}
		
		private void DoTimeTraffic()
		{
			DateTime fromDate = ReportUtility.GetLastCalculatedTimeTraffic().AddDays(1);
			fromDate = fromDate < Settings.GetInstance().GenerateReportFromDate ?
				Settings.GetInstance().GenerateReportFromDate : fromDate;

			DateTime toDate = DateTime.Now.AddDays(-1); //calculate till yesterday
			for(DateTime inputDate = fromDate; inputDate <= toDate; 
				inputDate = inputDate.AddDays(1))
			{
				log.Info("Calculating new time traffic for date " + inputDate.ToShortDateString());
				TimeTrafficReporter timeTrafficReporter = new TimeTrafficReporter() ;
				timeTrafficReporter.InputDate = inputDate;
				timeTrafficReporter.Generate();
			}
		}
	}
}
