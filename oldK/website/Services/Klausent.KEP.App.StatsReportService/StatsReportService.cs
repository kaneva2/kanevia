///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using log4net;
using Timer = System.Timers.Timer;

namespace Klausent.KEP.App.StatsReportService
{
	public class StatsReportService : System.ServiceProcess.ServiceBase
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(StatsReportService));
		
		private static readonly Settings settings = Settings.GetInstance();

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Timers
		/// </summary>
		System.Timers.Timer _timerStarter = new Timer();

		static StatsReportService()
		{
			string s = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
			FileInfo l_fi = new FileInfo(s);
			log4net.Config.DOMConfigurator.Configure(l_fi);
			if(log.IsInfoEnabled)
			{
				log.Info("Setup log4net");
			}
		}

		public StatsReportService()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();
		}

		// The main entry point for the process
		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
	
			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = new System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
			//
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new StatsReportService() };

			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = "Service1";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		protected override void OnStart(string[] args)
		{
			if(log.IsInfoEnabled)
			{
				log.Info("SERVICE STARTED");
			}

			ScheduleNextRun();
		}
 
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
			_timerStarter.Stop();
			log.Fatal("SERVICE STOPPED");
		}

		protected void ScheduleNextRun()
		{
			//kind of a trick to get real timer started on scheduled time
			DateTime now = DateTime.Now;
			DateTime scheduledTime = new DateTime(now.Year,now.Month, now.Day, 
				settings.RunAtHour, settings.RunAtMinute, 0);
			int waitMinutes = 0;

			if(scheduledTime.CompareTo(now) < 0)
			{
				waitMinutes = 60 * 24 - now.Subtract(scheduledTime).Minutes;
			}
			else
			{
				waitMinutes = scheduledTime.Subtract(now).Minutes;
			}

			//hack: just add two more minutes so that we know the task completes after
			//the scheduled date
			waitMinutes += 2;

			_timerStarter.Enabled = false;
			_timerStarter.Stop();
			_timerStarter.Interval = 1000 * 60 * waitMinutes;
			_timerStarter.Elapsed += new System.Timers.ElapsedEventHandler (TimerStarter_Elapsed);
			_timerStarter.Enabled = true;
			_timerStarter.Start();

			log.Info("Reports are scheduled to run in " + waitMinutes + " minutes");
			log.Info("Task scheduled to run at " + scheduledTime.ToShortTimeString());
		}

		/// <summary>
		/// starts the real timer
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void TimerStarter_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{

			try
			{
				//_timer.Stop ();
				log.Info ("Stats Report STARTED: ");
				Scheduler.GetInstance().Run(); //run the report
				log.Info ("Stats Report ENDED: ");
				ScheduleNextRun();
			}
			catch (Exception exc)
			{
				log.Error ("Exception generating reports", exc);
			}
			finally
			{
				//_timer.Start ();
			}
		}
		
	}
}
