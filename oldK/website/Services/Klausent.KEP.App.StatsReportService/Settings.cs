///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using log4net;

namespace Klausent.KEP.App.StatsReportService
{
	/// <summary>
	/// Summary description for Settings.
	/// </summary>
	public class Settings
	{
		private static readonly ILog log = LogManager.GetLogger (typeof(Settings));

		private DateTime _generateReportFromDate;
		private int		_runAtHour;
		private int		_runAtMinute;
		private static Settings inst;
		private Settings()
		{
			//
			// TODO: Add constructor logic here
			//

			_generateReportFromDate = Convert.ToDateTime(
				System.Configuration.ConfigurationSettings.AppSettings ["GenerateReportFromDate"]);

			string runAt = 
				System.Configuration.ConfigurationSettings.AppSettings ["RunAt"];

			try
			{
				_runAtHour = Convert.ToInt32(runAt.Substring(0, runAt.IndexOf(":")));
				_runAtMinute = Convert.ToInt32(runAt.Substring(runAt.IndexOf(":")+1));
			}catch(Exception)
			{
				log.Error("Invalid \"RunAt\" value" + runAt + " using default value 03:00");
				_runAtHour = 3;
				_runAtMinute = 0;
			}
		}

		public static Settings GetInstance()
		{
			if( inst == null)
			{
				inst = new Settings() ;
			}

			return inst;
		}

		/// <summary>
		/// calculate report for date generated from this date on
		/// </summary>
		public DateTime GenerateReportFromDate
		{
			get { return _generateReportFromDate; }
		}

		public int RunAtHour
		{
			get { return _runAtHour; }
		}

		public int RunAtMinute
		{
			get { return _runAtMinute; }
		}
	}
}
