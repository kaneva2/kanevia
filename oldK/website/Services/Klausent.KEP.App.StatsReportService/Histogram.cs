///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Text;

namespace Klausent.KEP.App.StatsReportService
{
	public class Histogram
	{
		private double[] _binBoundaries;
		private int[] _counts;
		private int _numSmaller = 0;
		private int _numLarger = 0;


		/// <summary>
		/// Constructs a Histogram with the specified number of bins and the 
		/// specified maximum and mininum values.
		/// </summary>
		/// <param name="numBins">The number of bins. Must be greated than zero</param>
		/// <param name="minValue">The maximum value for the Histogram.</param>
		/// <param name="maxValue">The minimum value for the Histogram.</param>
		/// <exception cref="ArgumentException">Thrown if the specified maximum
		/// and minimum values are the same. Or if the specified number of bins cannot
		/// be created.</exception>
		public Histogram( int numBins, double minValue, double maxValue )
		{
			if ( minValue == maxValue )
			{
				throw new ArgumentException( "Maximum and minimum values are equal" );
			}
			_counts = new int[numBins];
			if ( maxValue > minValue )
			{
				CalcBinBoundaries( numBins, minValue, maxValue );
			}
			else
			{
				CalcBinBoundaries( numBins, maxValue, minValue );
			}
			//CheckBinBoundaries( this._binBoundaries );
		}

		/// <summary>
		/// Create a Histogram with the specified bin boundaries.
		/// </summary>
		/// <param name="binBoundaries">Bin boundaries. Most be strictly
		/// monotonically increasing, i.e. <c>binBoundares[i]</c> is strictly
		/// less than <c>binBoundaries[i+1]</c> for each i.</param>
		public Histogram( double[] binBoundaries )
		{
			//CheckBinBoundaries( binBoundaries );
			_binBoundaries = new double[binBoundaries.Length];
			Array.Copy( binBoundaries, _binBoundaries, binBoundaries.Length );
			_counts = new int[_binBoundaries.Length-1];
		}

		/// <summary>
		/// Construct a Histogram from the data in <c>data</c> with
		/// <c>numBins</c> bins. The bins are of eqaul size and scaled
		/// with the maximim and minimum data in <c>data</c>. The counts
		/// in the Histogram are initialized with the contents of <c>data</c>.
		/// </summary>
		/// <param name="numBins">Desired number of bins.</param>
		/// <param name="data">Vector of data to place in the Histogram.</param>
		public Histogram( int numBins, double[] data )
		{
			double[] sortedData = new double[data.Length];
			Array.Copy( data, sortedData, data.Length );
			Array.Sort( sortedData );
			_counts = new int[numBins];
			CalcBinBoundaries( numBins, sortedData[0], sortedData[data.Length-1] );
			//CheckBinBoundaries( this._binBoundaries );
			AddSortedData( sortedData );
		}

		/// <summary>
		/// Gets the bin boundaries of the Histogram.
		/// </summary>
		public double[] BinBoundaries
		{
			get
			{
				return _binBoundaries;
			}
		}

		/// <summary>
		/// Gets the counts for each bin.
		/// </summary>
		public int[] Counts
		{
			get
			{
				return _counts;
			}
		}

		/// <summary>
		/// Gets the number of bins in the Histogram.
		/// </summary>
		public int NumBins
		{
			get
			{
				return _counts.Length;
			}
		}

		/// <summary>
		/// Gets the number of data points that were smaller
		/// than the smallest bin boundary.
		/// </summary>
		public int NumSmaller
		{
			get
			{
				return _numSmaller;
			}
		}

		/// <summary>
		/// Gets the number of data points that were larger than
		/// the larges bin boundary.
		/// </summary>
		public int NumLarger
		{
			get
			{
				return _numLarger;
			}
		}

		/// <summary>
		/// Gets the count for the specifed bin.
		/// </summary>
		/// <param name="binNumber">Bin number to get the count for.
		/// <c>binNumber</c> must be between 0 and (number of bins - 1)</param>
		/// <returns>The count for the specified bin.</returns>
		public int Count( int binNumber )
		{
			return _counts[binNumber];
		}

		/// <summary>
		/// Updates the Histogram counts for the bin that the data
		/// point <c>d</c> falls into.
		/// </summary>
		/// <param name="d">The data point.</param>
		public void AddData( double d )
		{
			if ( d < _binBoundaries[0] )
			{
				++_numSmaller;
			}
			else if ( d > _binBoundaries[_binBoundaries.Length-1] )
			{
				++_numLarger;
			}
			else
			{
				int bin = 1;
				while ( bin < _binBoundaries.Length && d >= _binBoundaries[bin] )
				{
					++bin;
				}
				if ( bin == _binBoundaries.Length )
				{
					++_counts[_counts.Length-1];
				}
				else
				{
					++_counts[bin-1];
				}
			}
		}

		/// <summary>
		/// Updates the Histograms bin counts with the given data points.
		/// </summary>
		/// <param name="data">Data to update bin counts with.</param>
		public void AddData( double[] data )
		{
			double[] sortedData = (double[])data.Clone();
			Array.Sort( sortedData );
			AddSortedData( sortedData );
		}

		/// <summary>
		/// Resets all counts to zero. The number of bins and bin boundaries 
		/// stay the same.
		/// </summary>
		public void Reset()
		{
			_numSmaller = 0;
			_numLarger = 0;
			for ( int i = 0; i < _counts.Length; ++i )
			{
				_counts[i] = 0;
			}
		}

		private void AddSortedData( double[] data )
		{
			int i = 0;
			while ( data[i] < _binBoundaries[0] ) 
			{
				++i;
				++_numSmaller;
			}

			int binNumber;
			for ( binNumber = 0; binNumber < _counts.Length; ++binNumber )
			{
				if ( i >= data.Length )
				{
					break;
				}
				for ( ; ; )
				{
					if ( i >= data.Length )
					{
						break;
					}
					if ( (binNumber == _counts.Length - 1 && data[i] <= _binBoundaries[binNumber+1]) ||
						data[i] < _binBoundaries[binNumber+1] )
					{
						++_counts[binNumber];
						++i;
					}
					else
					{
						break;
					}
				}
			}

			if ( i < data.Length )
			{
				_numLarger += data.Length - i;
			}
		}

		/// <summary>
		/// Given the minimum and maximum data values and the number of bins
		/// this method fills in the boundaries vector.
		/// </summary>
		/// <param name="numBins">The number of bins desired.</param>
		/// <param name="minValue">The lower bin boundary.</param>
		/// <param name="maxValue">The upper bin boundary.</param>
		private void CalcBinBoundaries( int numBins, double minValue, double maxValue )
		{
			_binBoundaries = new double[numBins + 1];
			_binBoundaries[0] = minValue;
			_binBoundaries[_binBoundaries.Length-1] = maxValue;
			double binSize = (maxValue - minValue)/numBins;
			for ( int i = 1; i < _binBoundaries.Length-1; ++i )
			{
				_binBoundaries[i] = _binBoundaries[0] + i*binSize;
			}
		}

		/// <summary>
		/// Verifies that the given vector contains a strictly monotonically
		/// increasing sequence of numbers.
		/// </summary>
		/// <param name="boundaries">The boundaries to check.</param>
		/// <exception cref="ArgumentException">Thrown if the vector is not
		/// strictly monotonically increasing.</exception>
		private void CheckBinBoundaries( double[] boundaries )
		{
			for ( int i = 0; i < boundaries.Length-1; ++i )
			{
				if ( boundaries[i] >= boundaries[i+1] )
				{
					string msg = string.Format( "Bin boundary {0} is >= bin boundary {1}", 
						boundaries[i], boundaries[i+1] );
					throw new ArgumentException( msg );
				}
			}
		}
		
	}  // class
     
}
