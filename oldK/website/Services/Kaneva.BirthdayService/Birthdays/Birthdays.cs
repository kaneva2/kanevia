///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.IO;
using log4net;
using KlausEnt.KEP.Kaneva;

namespace Birthdays
{
    public partial class Birthdays : Form
    {
        #region Declarations

        private UserFacade userFacade;
        private ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        #endregion

        #region Initialization
        
        public Birthdays ()
        {
            InitializeComponent();
        }

        private void Birthdays_Load (object sender, EventArgs e)
        {
            RunBirthdayService();

            this.Dispose();
            this.Close();
        }
        
        #endregion

        private void RunBirthdayService()
        {
            try
            {
                // Set up log for net
                string s = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
                FileInfo l_fi = new FileInfo (s);
                log4net.Config.XmlConfigurator.Configure (l_fi);

                m_logger.Info (" ");
                m_logger.Info ("-- Starting Birthday Service --");

                int numRecords = int.MaxValue;
                
                // If running in test mode, then override some values.
                if (RunInTestMode)
                {
                    numRecords = TestMode_MaxBirthdays;
                }

                // Get The upcoming birthdays                                                              //-12 MONTH
                PagedList<FriendDates> birthdays = GetUserFacade.GetUsersBirthdaysWithFriendsList ("7 DAY", "", " u.user_id, DATE_FORMAT(u2.birth_date,'%m%d'), u2.last_login DESC ", numRecords, 1);

                m_logger.Info ("Number of birthdays = " + birthdays.Count);

                if (birthdays.Count > 0)
                {
                    // Create the body of the email
                    int prevFriendUserId = 0;
                    List<FriendDates> fdPerEmail = new List<FriendDates> ();

                    // Get the last item in the list
                    FriendDates fdLast = birthdays[birthdays.Count - 1];

                    foreach (FriendDates fd in birthdays)
                    {
                        if (prevFriendUserId == fd.FriendUserId)
                        {
                            fdPerEmail.Add (fd);
                        }
                        else
                        {
                            // New Friend UserId so we need to send email before we start
                            // working on the new userid
                            if (fdPerEmail.Count > 0)
                            {
                                // Send Email
                                try
                                {
                                    // Send reminder emails
                                    if (fd.FriendSendBirthdayEmail && fd.FriendStatusId.Equals ((int) Constants.eUSER_STATUS.REGVALIDATED))
                                    {
                                        FormatAndSendBirthdayEmail (fdPerEmail);
                                    }
                                }
                                catch (Exception exc)
                                {
                                    m_logger.Error ("Error in sending Birthday email to userId " + fd.FriendUserId, exc);
                                }
                                finally
                                {
                                    fdPerEmail.Clear ();
                                }
                            }

                            fdPerEmail.Add (fd);
                        }

                        prevFriendUserId = fd.FriendUserId;

                        // Handle the last item in the list
                        if (fd == fdLast)
                        {
                            // Send reminder emails
                            if (fd.FriendSendBirthdayEmail && fd.FriendStatusId.Equals ((int) Constants.eUSER_STATUS.REGVALIDATED))
                            {
                                FormatAndSendBirthdayEmail (fdPerEmail);
                            }
                        }
                    }
                }

                m_logger.Info ("-- Birthday Service Completed --");
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error Running Birthday Service : " + ex.Message);
            }
        }

        private void FormatAndSendBirthdayEmail (List<FriendDates> fdates)
        {
            string userMsg = string.Empty;
            string subject = string.Empty;
            DateTime dtBday = DateTime.MinValue;
            bool isFirstRecord = true;
            int currCount = 1;

            foreach (FriendDates fd in fdates)
            {
                DateTime currBday = new DateTime (DateTime.Now.Year, fd.BirthDate.Month, fd.BirthDate.Day);

                if (currCount > 5)
                {
                    // Checking to see if we have more than 5 birthdays on the same date, 
                    // if so then ignore records until we get to next birth date
                    if (currBday == dtBday)
                    {
                        // Go to next record
                        continue;
                    }
                }
                                       
                // Generate the tracking ids for each link type 
                string requestId = "";
                string requestTypeIdVisitProfile = "";
                string requestTypeIdSendMessage = "";
                string requestTypeIdPlanParty = "";
                
                try
                {
                    requestId = GetUserFacade.GetTrackingRequestId ("", Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_BIRTHDAY_EMAIL_VISIT_PROFILE, 1, 0, 0);
                    requestTypeIdVisitProfile = GetUserFacade.InsertTrackingTypeSent (requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_EMAIL, fd.FriendUserId);
                }
                catch { }

                try
                {
                    requestId = GetUserFacade.GetTrackingRequestId ("", Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_BIRTHDAY_EMAIL_SEND_MESSAGE, 1, 0, 0);
                    requestTypeIdSendMessage = GetUserFacade.InsertTrackingTypeSent (requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_EMAIL, fd.FriendUserId);
                } 
                catch { }

                try
                {
                    requestId = GetUserFacade.GetTrackingRequestId ("", Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_BIRTHDAY_EMAIL_PLAN_PARTY, 1, 0, 0);
                    requestTypeIdPlanParty = GetUserFacade.InsertTrackingTypeSent (requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_EMAIL, fd.FriendUserId);
                } 
                catch { }
                
                string profilePageUrl = Common.GetPersonalChannelUrl (fd.Username) + "?" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeIdVisitProfile;
                string profilePageSendMsgUrl = Common.GetPersonalChannelUrl (fd.Username) + "?" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeIdSendMessage;
                string eventPageUrl = "http://" + KanevaGlobals.SiteName + "/mykaneva/events/eventForm.aspx?bdid=" + fd.UserId.ToString () + "&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeIdPlanParty;

                if (currBday != dtBday)
                {
                    // If not first record then add the closing table tags
                    if (!isFirstRecord)
                    {
                        userMsg += "</td>" +
                                    "</tr>" +
                                "</tbody>" +
                            "</table>";

                        isFirstRecord = false;
                    }
                    
                    // Create new container table for this date
                    userMsg += "<table border='0' cellpadding='0' cellspacing='0' style='border-top:1px solid #cccccc;'>" +
                            "<tbody>" +
                                "<tr style='padding-top:10px;'>" +
                                    "<td style='font:normal 11px Verdana,Geneva,sans-serif;color:#666666;'>" + FormateBirthdate (fd.BirthDate) + "</td>" +
                                "</tr>" +
                                    "<td>";

                    // Set the date
                    dtBday = currBday;
                    currCount = 1;
                }

                userMsg += "<p><table border='0' cellpadding='5' cellspacing='5' style='border-bottom:1px solid #eeeeee;'>" + 
	                "<tbody>" +
		                "<tr>" + 
			                "<td align='left' valign='top' width='120'><img src='" + Common.GetProfileImageURL (fd.FriendThumbnailMediumPath, "me", fd.Gender) + "' border='0'><br /><br /></td>" +
                            "<td align='left' valign='middle' width='160' style='font:normal 12px Verdana,Geneva,sans-serif;color:#666666;'><a href='" + profilePageUrl + "' target='_blank' style='color:#3d77c4;font-weight:bold;font-size:13px;text-decoration:none;'>" + fd.Username + "</a><br />" + fd.DisplayName + "<br /><br /></td>" +
			                "<td align='left' valign='top'>" + 
				                "<table border='0' cellpadding='0' cellspacing='0'>" + 
					                "<tbody>" +
						                "<tr>" +
                                            "<td colspan='2' bgcolor='333333' height='24' width='200' align='center'><a href='" + profilePageSendMsgUrl + "' style='text-decoration:none;color:#ffffff; font:bold 12px Verdana,Geneva,sans-serif;'>Send a Birthday Message</a></td>" +
						                "</tr>" +
						                "<tr><td colspan='2' height='15'></td></tr>" +
						                "<tr>" +
                                            "<td bgcolor='cccccc' height='24' width='110' align='center'><a href='" + eventPageUrl + "' style='text-decoration:none;color:#000000; font:bold 12px Verdana,Geneva,sans-serif;'>Plan a Party</a></td>" +
							                "<td width='90'>&nbsp;</td>" +
						                "</tr>" +
					                "</tbody>" +
				                "</table>" +
			                "</td>" + 
		                "</tr>" + 
	                "</tbody>" +
                "</table></p>";

                currCount++;
            }

            // Add the closing tags for the last record
            userMsg +=  "</td>" +
                        "</tr>" +
                    "</tbody>" +
                "</table>";

            if (fdates.Count > 1)
            {
                subject = "Your friends' birthdays this week";
            }
            else
            {
                subject = "Wish " + fdates[0].Username + " a happy birthday this week";
            }

            string toEmail = fdates[0].FriendEmail;
            int toUserId = fdates[0].FriendUserId;
            
            // If running in test mode, override some values
            if (RunInTestMode)
            {
                toEmail = TestMode_ToEmail;
                toUserId = TestMode_ToUserId;
            }

            SendBirthdayEmail (subject, toEmail, toUserId, 1, userMsg); 
        }

        /// <summary>
        /// SendEventEmail
        /// </summary>
        private void SendBirthdayEmail (string subject, string toEmail, int toUserId, int fromUserId, string userMessage)
        {
            // store subject and message as overload fields		
            int overloadId = 0;
            overloadId = KlausEnt.KEP.Kaneva.Mailer.Queuer.AddOverloadField (overloadId, "#subject#", subject);

            //if overloadid did not work don't send the email.
            if (overloadId != 0)
            {
                int typeId = KlausEnt.KEP.Kaneva.Mailer.EmailTypeUtility.GetTypeIdByName ("FriendBirthdayNotification");

                int i = KlausEnt.KEP.Kaneva.Mailer.TemplateSherpa.DressAndSendTemplate (typeId, 0, toEmail, 0, 1, toUserId, "", 0, userMessage, "", 0, overloadId, "");
            }
        }

        private string FormateBirthdate (DateTime birthdate)
        {
            DateTime date = new DateTime (DateTime.Now.Year, birthdate.Month, birthdate.Day);
            return date.ToString ("dddd, MMMM ") + Common.GetDaySuffix (birthdate.Day);
        }

        #region Properties

        /// <summary>
        /// Gets the UserFacadeInstance.
        /// </summary>
        private UserFacade GetUserFacade
        {
            get
            {
                if (userFacade == null)
                {
                    userFacade = new UserFacade();
                }
                return userFacade;
            }
        }

        private bool RunInTestMode
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["Run_Birthday_Service_In_Test_Mode"] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["Run_Birthday_Service_In_Test_Mode"]);
                }
            }
        }

        private string TestMode_ToEmail
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["TestMode_ToEmail"] == null)
                {
                    return "bhuff@kaneva.com";
                }
                else
                {
                    return Convert.ToString (System.Configuration.ConfigurationManager.AppSettings["TestMode_ToEmail"]);
                }
            }
        }

        private int TestMode_ToUserId
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["TestMode_ToUserId"] == null)
                {
                    return 9544;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["TestMode_ToUserId"]);
                }
            }
        }

        private int TestMode_MaxBirthdays
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["TestMode_MaxBirthdays"] == null)
                {
                    return 10;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["TestMode_MaxBirthdays"]);
                }
            }
        }

        #endregion
    }
}
