///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using KlausEnt.KEP.Kaneva;

// Import log4net classes.
using log4net;
using log4net.Config;

namespace KEPPing
{

	/// <summary>
	/// Summary description for KEPPingV3.
	/// </summary>
	/// 
	[WebService(Namespace="http://www.kaneva.com/webservices/V3",Description="Kaneva Server Ping Service V3.")]
	public class KEPPingV3 : System.Web.Services.WebService
	{
		private static readonly ILog m_pingLog = LogManager.GetLogger("KEPPing");
		private static readonly ILog m_auditLog = LogManager.GetLogger("Audit");

		static KEPPingV3()
		{
			// BasicConfigurator replaced with DOMConfigurator.
            XmlConfigurator.Configure(new System.IO.FileInfo(System.Configuration.ConfigurationManager.AppSettings["LogConfigFileV3"]));
		}

		public KEPPingV3 ()
		{
			//CODEGEN: This call is required by the ASP.NET Web Services Designer
			InitializeComponent();
		}

		private enum InfoRet
		{
			irOk,
			irNotFound,
			irDupeLicense,
			irRevoked		// bad watermark
		}

		#region Component Designer generated code
		
		//Required by the Web Services Designer 
		private IContainer components = null;
				
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);		
		}
		
		#endregion

		/// <summary>
		/// Initialize the ping ws
		/// </summary>
		/// <param name="key">the game key, empty or null if anonymous</param>
		/// <param name="watermark"/>the library's watermark
		/// <param name="type">type of server</param>
		/// <param name="ipAddress">ip address or name to show up on Kaneva</param>
		/// <param name="port">port of server</param>
		/// <param name="engineId">returned id</param>
		/// <param name="maxCount">returned max count allowed by license</param>
		/// <param name="status">returns whether the license if valid or not</param>
		/// <param name="gameId">returns gameID for the given key</param>
		/// <returns>hash used to authenticate server</returns>
		[WebMethod]
		public string Initialize( string key, string watermark, int type, string ipAddress, int port, ref int engineId, ref int maxCount, ref bool status, ref int gameId)
		{
			// Log it?
			if ( m_pingLog.IsDebugEnabled )
			{
				m_pingLog.Debug( "Ping.Initialize: " + key + " " + watermark + " " + ipAddress + " " + port + ", Real IP = " + Context.Request.UserHostAddress);
			}

			engineId = 0;
			Hashtable parameters;
			string originalIpAddress = ipAddress;

			// Get the IP address from the request if it is set to AUTO
			bool bUseAutoIP = ipAddress.ToUpper ().Equals ("AUTO");
			if (bUseAutoIP)
			{
				ipAddress = Context.Request.UserHostAddress;
			}

			InfoRet ir = checkWatermark( watermark );
			if ( ir != InfoRet.irOk )
			{
				engineId = 0;
				m_auditLog.Error( "Revoked watermark used: \"" + watermark + "\" from ipAddress of is " + ipAddress + " Real IP = " + Context.Request.UserHostAddress);
				return "You license is invalid";
			}
			
			// Get the status info (including number of server users)
			ir = getInfo ( key, ref ipAddress, port, ref status, ref maxCount, ref engineId, ref gameId);

			if ( ir == InfoRet.irDupeLicense )
			{
				engineId = 0;
				m_auditLog.Info( "Key \"" + key + "\" is already marked as running on the Kaneva server" );
				return "Your license is already marked as running on the Kaneva server";
			}
			else if ( ir != InfoRet.irOk )
			{
				engineId = 0;
				m_pingLog.Info( "Can not get details about key: " + key );
				return "Can not get info for server!";
			}

			if ( status ) // only update database if active
			{
				// get the existing record
				DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

				if ( engineId != 0 ) // found in table
				{
					// exists, so update it
					string update = "UPDATE current_game_engines " +
						"SET number_of_players = 0, " +
						"    max_players = " + maxCount + ", " +
						"    status_id = " + (int)Constants.eGAME_SERVER_STATUS.STARTING + ", " +
						"    last_ping_datetime = " + dbUtility.GetCurrentDateFunction() + ", " +
						"    ip_address = '" + dbUtility.CleanText(ipAddress) + "', " + 
						"    port = " + port + ", " + 
						"    engine_started_date = " + dbUtility.GetCurrentDateFunction() + ", " + 
						"    registration_key = '" + dbUtility.CleanText( key ) + "'" +
						" WHERE engine_id = @engineId";

					parameters = new Hashtable ();
					parameters.Add ("@engineId", engineId);
					dbUtility.ExecuteNonQuery( update, parameters);
				}
				else
				{
					// if doesn't exist, add it
					string insert = "INSERT INTO current_game_engines " +
						" ( type, internally_hosted, engine_started_date, ip_address, port, status_id, " +
						"   last_ping_datetime, number_of_players, max_players, registration_key ) " + 
						" VALUES (@type, 'N', " +	// internally hosted
						dbUtility.GetCurrentDateFunction() + ", " + 
						"@ipAddress, @port, @status, " +
						dbUtility.GetCurrentDateFunction() + ", " + 
						0 + ", @maxCount, @key)";

					// Insert and return the game engine id
					parameters = new Hashtable ();
					parameters.Add ("@type", type);
					parameters.Add ("@ipAddress", ipAddress);
					parameters.Add ("@port", port);
					parameters.Add ("@status", (int)Constants.eGAME_SERVER_STATUS.STARTING);
					parameters.Add ("@maxCount", maxCount);
					parameters.Add ("@key", key);
					dbUtility.ExecuteIdentityInsert ( insert, parameters,  ref engineId);
				}
			}

			if ( m_pingLog.IsDebugEnabled )
			{
				m_pingLog.Debug( "Ping.Initialize Returning: " + engineId + " " + maxCount + " " + status );
			}

			return makeHash( "KEPPING" + key + originalIpAddress + port );
		}

		// helper to safely cast to an int
		int makeInt( Object o )
		{
			if ( o is Int32 )
				return (int)(Int32)o;
			else if ( o is Int64 )
				return (int)(Int64)o;
			else if ( o is int )
				return (int)o;
			else if ( o is Double )
				return (int)(Double)o;
			else
				return 0;
		}

		/// <summary>
		/// called occasionally by the server to check on the count and expiration
		/// </summary>
		/// <param name="key">the game key, empty or null if anonymous</param>
		/// <param name="statusId">the engine's status</param>
		/// <param name="numberOfPlayers">current logged on players</param>
		/// <param name="maxNumberOfPlayers">current max players configured by the server</param>
		/// <param name="status">returns whether the license if valid or not</param>
		/// <returns>hash used to authenticate server</returns>
		[WebMethod]
		public string GetCount( string key, int statusId, int numberOfPlayers, ref int maxNumberOfPlayers, ref bool status )
		{
			if ( m_pingLog.IsDebugEnabled )
			{
				m_pingLog.Debug( "Ping.GetCount: " + key + " " + statusId + " " + numberOfPlayers );
			}

			status = false;
			maxNumberOfPlayers = 1;
			int engineId = 0;

			int gameId = 0;

			string ipAddress = "";
			InfoRet ir = getInfo (key, ref ipAddress, -1, ref status, ref maxNumberOfPlayers, ref engineId, ref gameId);

			if ( ir == InfoRet.irDupeLicense )
			{
				m_auditLog.Info( "Key \"" + key + "\" is already marked as running on the Kaneva server" );
				return "Your license is already marked as running on the Kaneva server";
			}
			else if ( ir != InfoRet.irOk )
			{
				m_pingLog.Info( "Can not get details about key: " + key );
				return "Can not get info for server!";
			}
			else
				return Ping (engineId, key, statusId, numberOfPlayers, maxNumberOfPlayers );
		}

		private InfoRet checkWatermark( string watermark )
		{
			InfoRet ret = InfoRet.irOk;
			
			Hashtable parameters = new Hashtable ();
			string sql = "";
			
			// They passed in a watermark, let's see if it is bad
			sql = "SELECT count(*) " +
				" FROM elite_source_watermarks " +
				" WHERE watermark = @wm " + 
				" AND revoked <> 0";
				
			parameters = new Hashtable ();
			parameters.Add ("@wm", watermark);
            DataRow dr = null;
            try
            {
                dr = KanevaGlobals.GetDatabaseUtility().GetDataRow(sql, parameters, false);
            }
            catch (Exception e)
            {
                m_pingLog.Debug( "Ping exception " + e.ToString() );
            }

			if ( dr != null )
			{
				if ( dr.Table.Rows.Count > 0 )
				{
					if ( dr.Table.Rows.Count == 1 )
					{
						// found it, check the validity of the license
						if ( makeInt( dr[0] ) != 0 )
							ret = InfoRet.irRevoked;
					}
				}
			}

			return ret;
		}
		
		// returns true if ok, false if bad data in database
		// engineId will be 0 if no record in current engines table
		private InfoRet getInfo ( string key, ref string ipAddress, int port, ref bool status, ref int maxNumberOfPlayers, ref int engineId, ref int gameId)
		{
			engineId = 0;
			maxNumberOfPlayers = 1;
			gameId = 0;
			status = true; // now default them to active
			int licenseType = (int) Constants.eLICENSE_SUBSCRIPTIONS.ANONYMOUS;

			Hashtable parameters = new Hashtable ();
			
			string sql = "";
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// No key, default to developer
			if ( key == null || key.Trim().Length == 0 )
			{
				if ( m_pingLog.IsDebugEnabled )
				{
					m_pingLog.Debug( "Ping.getInfo: " + key + " " + ipAddress + " " + port + ".  No key, reverting to dev license" );
				}
				maxNumberOfPlayers = GetNoKeyNumberOfUsers ();
				gameId = 0;
			}
			else
			{
				// They passed in a key, get the max based on the key
				sql = "SELECT ls.max_server_users, gl.asset_id, gl.license_type " +
					" FROM game_license gl, license_subscription ls " +
					" WHERE gl.license_type = ls.license_subscription_id " +
					" AND gl.game_key = @key" +
					" AND (gl.end_date >= " + dbUtility.GetCurrentDateFunction () + " OR gl.end_date IS NULL)" +
					" AND gl.status_id = @status";
					
				parameters = new Hashtable ();
				parameters.Add ("@key", key);
				parameters.Add ("@status", (int) Constants.eGAME_LICENSE_STATUS.ACTIVE);
				DataRow drLicense = dbUtility.GetDataRow (sql, parameters, false);

				if (drLicense == null)
				{	
					if ( m_pingLog.IsDebugEnabled )
					{
						m_pingLog.Debug( "Ping.getInfo: " + key + " " + ipAddress + " " + port + ", key not found, setting to 1" );
					}
					// Key not found
					maxNumberOfPlayers = 1;
					gameId = 0;
				}
				else
				{
					// Key found
					maxNumberOfPlayers = Convert.ToInt32 (drLicense ["max_server_users"]);
					gameId = Convert.ToInt32 (drLicense ["asset_id"]);

					// Used to see if we are commercial, since only commerical may not use Auto IP.
					licenseType = Convert.ToInt32 (drLicense ["license_type"]);
				}

			}

			// If they are not using a commercial license, read the IP from the Request.
			// Get Count does not pass an IP Address, check length so we don't have error (dups) on commercial.
			if (!licenseType.Equals ((int) Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL) && ipAddress.Length > 0)
			{
				ipAddress = Context.Request.UserHostAddress;
			}

			DataRow dr;

			if ( key != null && key.Trim().Length > 0 )
			{
				// get the engineId based on key, may not exist if first ping into server
				// 1/07 updated the last column to let MySQL do date diff calculation since ran into overflow problems.
				sql = "SELECT engine_id, ip_address, port, status_id, (last_ping_datetime > DATE_SUB(NOW(),INTERVAL 7 MINUTE)) as active " + 
					"  FROM current_game_engines " + 
					" WHERE registration_key = @key";

				parameters = new Hashtable ();
				parameters.Add ("@key", key);
				dr = dbUtility.GetDataRow( sql, parameters, false );

				if ( dr != null )
				{
					if ( dr.Table.Rows.Count > 0 )
					{
						if ( dr.Table.Rows.Count == 1 )
						{
							// found it, check the validity of the license
							engineId = makeInt( dr[0] );

							if ( ipAddress != null && port > 0 )
							{
								int isActive = makeInt( dr[4] );
								int gameStatus = makeInt( dr[3] );
								int oldPort = makeInt( dr[2] );
								string oldIpAddress = (string)dr[1];

								// check the status
								// if changing IP/port, must not be running, or > 7 minutes old
								if ( (ipAddress != oldIpAddress || port != oldPort) && gameStatus == (int)Constants.eGAME_SERVER_STATUS.RUNNING &&
									isActive != 0 ) // it's running and they want to change port?? no way
								{
									if ( m_pingLog.IsDebugEnabled )
									{
										m_pingLog.Debug( "Ping.getInfo: " + key + " " + ipAddress + " " + port + ", duplicate license." );
									}
									maxNumberOfPlayers = 1; // set them to one user
									return InfoRet.irDupeLicense;
								}
							}

						}
						else
						{
							// more that one row with that key is an error!
							return InfoRet.irNotFound;
						}
					}
				}
				// key not found, first time coming in
			}

			//JMB - Commented the below out since you are allowed to have multiple records for same IP/Port combination
			// Maybe you are starting up a new server with a different key.

//			if ( engineId == 0 && port > 0 && ipAddress != null)
//			{
//				// try to find engineId by ip, port since didn't get engine Id yet
//				sql = "SELECT engine_id " +
//					"FROM current_game_engines " +
//					"WHERE ip_address = @ipAddress " + 
//					"AND port = @port";
//
//				parameters = new Hashtable ();
//				parameters.Add ("@ipAddress", ipAddress);
//				parameters.Add ("@port", port);
//				dr = dbUtility.GetDataRow( sql, parameters, false );
//
//				if ( dr != null )
//				{
//					if ( dr.Table.Rows.Count > 0 )
//					{
//						if ( dr.Table.Rows.Count == 1 )
//						{
//							engineId = makeInt( dr[0] );
//						}
//						else
//						{
//							// more that one row with that ip, port!
//							return InfoRet.irNotFound;
//						}
//					}
//				}
//			}

			return InfoRet.irOk;
		}

		/// <summary>
		/// Returns the number of users if no key was given
		/// </summary>
		/// <returns></returns>
		private int GetNoKeyNumberOfUsers ()
		{
			DataRow drDevLicense = GetLicenseSubscription ((int) Constants.eLICENSE_SUBSCRIPTIONS.DEVELOPER);

			if (drDevLicense != null && drDevLicense ["max_server_users"] != DBNull.Value)
			{
				return Convert.ToInt32 (drDevLicense ["max_server_users"]);
			}
			else
			{
				return 5;
			}
		}

		/// <summary>
		/// GetLicenseSubscripsion
		/// </summary>
		/// <returns></returns>
		private DataRow GetLicenseSubscription (int licenseSubscriptionId)
		{
			string sqlSelect = "SELECT license_subscription_id, name, length_of_subscription, amount, amount_kei_point_id, max_server_users  " +
				" FROM license_subscription " +
				" WHERE license_subscription_id = " + licenseSubscriptionId;

			return KanevaGlobals.GetDatabaseUtility ().GetDataRow (sqlSelect, false);
		}

		/// <summary>
		/// Ping method called by servers to check in 
		/// </summary>
		/// <param name="engineId">the unique if returned from Initialize</param>
		/// <param name="statusId">the engine's status</param>
		/// <param name="numberOfPlayers">current logged on players</param>
		/// <param name="maxNumberOfPlayers">current max players configured by the server</param>
		/// <returns>hash used to authenticate server</returns>
		[WebMethod]
		public string Ping (int engineId, string key, int statusId, int numberOfPlayers, int maxNumberOfPlayers)
		{
			if ( m_pingLog.IsDebugEnabled )
			{
				m_pingLog.Debug( "Ping.Ping: " + engineId + " " + statusId + " " + numberOfPlayers + "/" + maxNumberOfPlayers );
			}
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// engine id must exist, so just update it
			string update = "UPDATE current_game_engines " +
				"SET number_of_players = @numberOfPlayers, " +
				"           max_players = @maxNumberOfPlayers, " +
				"    last_ping_datetime = " + dbUtility.GetCurrentDateFunction() + ", " +
				"             status_id = @statusId";

			if ( statusId == (int)Constants.eGAME_SERVER_STATUS.STARTING )
			{
				update += ", engine_started_date = " + dbUtility.GetCurrentDateFunction();
			}

			update += " WHERE engine_id = @engineId " +
				" AND registration_key = @key";

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@engineId", engineId);
			parameters.Add ("@key", key);
			parameters.Add ("@numberOfPlayers", numberOfPlayers);
			parameters.Add ("@maxNumberOfPlayers", maxNumberOfPlayers);
			parameters.Add ("@statusId", statusId);
			int count = dbUtility.ExecuteNonQuery( update, parameters);

			// Store some history
			StoreHistory (engineId, key, numberOfPlayers, Context.Request.UserHostAddress);

			return makeHash( "KEPPING" + engineId + statusId + numberOfPlayers );
		}

		/// <summary>
		/// StoreHistory
		/// </summary>
		public void StoreHistory (int engineId, string gameKey, int numberOfPlayers, string ipAddress)
		{
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();

			// See if history has been stored in the last hour
			string sql = "SELECT engine_history_id, player_count " +
				" FROM engine_history " +
				" WHERE engine_id = @engineId " +
				" AND game_key = @gameKey " +
				" AND created_date >= " + dbUtility.GetDatePlusMinutes (-60);

			Hashtable parameters = new Hashtable ();
			parameters.Add ("@engineId", engineId);
			parameters.Add ("@gameKey", gameKey);
			DataRow drHistory = dbUtility.GetDataRow (sql, parameters, false);

			// If it does not already exist, add a new history record
			if (drHistory == null)
			{
				sql = "INSERT INTO engine_history " +
					" (engine_id, player_count, created_date, game_key, ip_address) " +
					" VALUES " +
					"(@engineId, @playerCount, " + dbUtility.GetCurrentDateFunction () + ", @gameKey, @ipAddress)";

				parameters = new Hashtable ();
				parameters.Add ("@engineId", engineId);
				parameters.Add ("@gameKey", gameKey);
				parameters.Add ("@playerCount", numberOfPlayers);
				parameters.Add ("@ipAddress", ipAddress);
				dbUtility.ExecuteNonQuery (sql, parameters);
			}
			else
			{
				// If so, update if number of users is greater than current
				if (numberOfPlayers > Convert.ToInt32 (drHistory ["player_count"]))
				{
					sql = "UPDATE engine_history " +
						" SET player_count = @playerCount, " +
						" ip_address = @ipAddress " +
						" WHERE engine_id = @engineId " +
						" AND game_key = @gameKey" +
						" AND player_count < @playerCount";
					parameters = new Hashtable ();
					parameters.Add ("@engineId", engineId);
					parameters.Add ("@gameKey", gameKey);
					parameters.Add ("@playerCount", numberOfPlayers);
					parameters.Add ("@ipAddress", ipAddress);
					dbUtility.ExecuteNonQuery (sql, parameters);
				}
			}

		}


		private string makeHash( string s )
		{
			// This is one implementation of the abstract class MD5.
			System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();

			System.Text.Encoder encoder = System.Text.ASCIIEncoding.UTF8.GetEncoder();
			int byteCount = encoder.GetByteCount( s.ToCharArray(), 0, s.Length, true );
			byte[] bytes = new byte[byteCount];
			encoder.GetBytes( s.ToCharArray(), 0, s.Length, bytes, 0, true );
			byte[] result = md5.ComputeHash(bytes );

			string ret = "";
			for ( int i = 0; i < result.Length; i++ )
			{
				ret = ret + result[i].ToString("x2");
			}
			return ret; 
		}
	}
}
