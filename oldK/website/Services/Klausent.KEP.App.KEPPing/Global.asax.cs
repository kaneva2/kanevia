///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using KlausEnt.KEP.Kaneva;

// Import log4net classes.
using log4net;
using log4net.Config;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		public Global()
		{
			InitializeComponent();
		}	
		/// <summary>
		/// Get the version
		/// </summary>
		/// <returns></returns>
		public static string Version ()
		{
			if (version == null)
			{
				// Get the current version
				System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly ();
				System.Reflection.AssemblyName an = a.GetName();
				version = "version " + an.Version.Major + "." + an.Version.Minor + "." + an.Version.Build + "." + an.Version.Revision;
			}

			return version;
		}

		/// <summary>
		/// Get the current cache
		/// </summary>
		/// <returns></returns>
		public static System.Web.Caching.Cache Cache ()
		{
			return null;
		}

		protected void Application_Start(Object sender, EventArgs e)
		{
            // BasicConfigurator replaced with DOMConfigurator.
           // XmlConfigurator.Configure(new System.IO.FileInfo(System.Configuration.ConfigurationManager.AppSettings["LogConfigFileV5"]));
		}
 
		protected void Session_Start(Object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_Error(Object sender, EventArgs e)
		{
            m_logger.Error("Unhandled application error in " + Request.Url.ToString(), Server.GetLastError());
		}

		protected void Session_End(Object sender, EventArgs e)
		{

		}

		protected void Application_End(Object sender, EventArgs e)
		{

		}
		/// <summary>
		/// Version number
		/// </summary>
		private static string version = null;

        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
			
		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

