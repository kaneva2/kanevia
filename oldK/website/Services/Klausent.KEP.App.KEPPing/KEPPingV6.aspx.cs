///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KlausEnt.KEP.Kaneva;
using System.Collections;
using System.Data;
using log4net;
using log4net.Config;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KEPPing
{
    public partial class KEPPingV6 : KEPPingBaseV2
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["action"] == null)
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action param not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            string actionreq = Request.Params["action"];

             if (actionreq.Equals("Initialize"))
             {
                 if ((Request.Params["key"] == null) || (Request.Params["hostname"] == null) || (Request.Params["type"] == null)
                     || (Request.Params["ipAddress"] == null) || (Request.Params["port"] == null) || (Request.Params["protocolVersion"] == null)
                     || (Request.Params["schemaVersion"] == null) || (Request.Params["serverVersion"] == null) || (Request.Params["assetVersion"] == null))
                 {
                     string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>key, hostname, type, ipAddress, port, protocolVersion, schemaVersion, serverVersion, or assetVersion param not specified</ResultDescription>\r\n</Result>";
                     Response.Write(errorStr);
                     return;
                 }

                 string key = "";
                 string hostname = "";
                 string watermark = "";
                 int type = 0;
                 string ipAddress = "";
                 int port = 0;
                 uint protocolVersion = 0;
                 int schemaVersion = 0;
                 string serverVersion = "";
                 uint assetVersion = 0;
                               
                 int serverId = 0;
                 int maxCount = 0;
                 bool status = false;
                 int gameId = 0;
                 int parentGameId = 0;
                 string gameName = "";

                

                 try
                 {
                     key = Request.Params["key"];
                     hostname = Request.Params["hostname"];
                     watermark = Request.Params["watermark"];
                     type = Convert.ToInt32(Request.Params["type"]);
                     ipAddress = Request.Params["ipAddress"];
                     port = Convert.ToInt32(Request.Params["port"]);
                     protocolVersion = Convert.ToUInt32(Request.Params["protocolVersion"]);
                     schemaVersion = Convert.ToInt32(Request.Params["schemaVersion"]);
                     serverVersion = Request.Params["serverVersion"];
                     assetVersion = Convert.ToUInt32(Request.Params["assetVersion"]);
                 }
                 catch (Exception)
                 {
                     string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Invalid parameter value passed in</ResultDescription>\r\n</Result>";
                     Response.Write(errorStr);
                     return;
                 }

                 string resultInitialize = Initialize(key, hostname, watermark, type, ipAddress, port, protocolVersion, schemaVersion, serverVersion, assetVersion, 
                      ref serverId, ref maxCount, ref status, ref gameId, ref parentGameId, ref gameName);

                 string resultString = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Initialize</ResultDescription>\r\n" +
                    "<Initialize serverId=\"" + serverId + "\" maxCount=\"" + maxCount + "\"  status=\"" + status + "\"  gameId=\"" + gameId + "\"  parentGameId=\"" + parentGameId + "\"  gameName=\"" + gameName + "\">" + resultInitialize + "</Initialize></Result>";

                 Response.Write(resultString);
                 return;


             }
             else if (actionreq.Equals("Ping"))
             {
                   if ((Request.Params["serverId"] == null) || (Request.Params["port"] == null) || (Request.Params["statusId"] == null)
                     || (Request.Params["numberOfPlayers"] == null) || (Request.Params["maxNumberOfPlayers"] == null) || (Request.Params["adminOnly"] == null))
                 {
                     string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>serverId, port, statusId, numberOfPlayers, maxNumberOfPlayers, or adminOnly param not specified</ResultDescription>\r\n</Result>";
                     Response.Write(errorStr);
                     return;
                 }

                 int serverId = 0;
                 int port = 0;
                 int statusId = 0;
                 int numberOfPlayers = 0;
                 int maxNumberOfPlayers = 0;
                 bool adminOnly = false;

                 try
                 {
                     serverId = Convert.ToInt32(Request.Params["serverId"]);
                     port = Convert.ToInt32(Request.Params["port"]);
                     statusId = Convert.ToInt32(Request.Params["statusId"]);
                     numberOfPlayers = Convert.ToInt32(Request.Params["numberOfPlayers"]);
                     maxNumberOfPlayers = Convert.ToInt32(Request.Params["maxNumberOfPlayers"]);
                     adminOnly = Convert.ToBoolean(Request.Params["adminOnly"]);
                 }
                 catch (Exception)
                 {
                     string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Invalid parameter value passed in</ResultDescription>\r\n</Result>";
                     Response.Write(errorStr);
                     return;
                 }

                 string resultPing = Ping(serverId, port, statusId, numberOfPlayers, maxNumberOfPlayers, adminOnly);

                 string resultStringPing = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Ping</ResultDescription>\r\n" +
                   "<Ping>" + resultPing + "</Ping></Result>";

                 Response.Write(resultStringPing);
                 return;

             }
             else if (actionreq.Equals("GetCount"))
             {
                   if ((Request.Params["key"] == null) || (Request.Params["serverId"] == null) || (Request.Params["port"] == null)
                     || (Request.Params["statusId"] == null) || (Request.Params["numberOfPlayers"] == null) || (Request.Params["adminOnly"] == null))
                 {
                     string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>key, serverId, port, statusId, numberOfPlayers,  or adminOnly param not specified</ResultDescription>\r\n</Result>";
                     Response.Write(errorStr);
                     return;
                 }


                string key = "";
                int serverId = 0;
                int port = 0;
                int statusId = 0;
                int numberOfPlayers = 0;
                bool adminOnly = false;
                 
                try
                 {
                    key = Request.Params["key"];
                    serverId = Convert.ToInt32(Request.Params["serverId"]);
                    port = Convert.ToInt32(Request.Params["port"]);
                    statusId = Convert.ToInt32(Request.Params["statusId"]);
                    numberOfPlayers = Convert.ToInt32(Request.Params["numberOfPlayers"]);
                    adminOnly = Convert.ToBoolean(Request.Params["adminOnly"]);
                 }
                 catch (Exception)
                 {
                     string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Invalid parameter value passed in</ResultDescription>\r\n</Result>";
                     Response.Write(errorStr);
                     return;
                 }
                 
                 
                 int maxNumberOfPlayers = 0;
                 bool status = false;

                 string resultGetCount = GetCount(key, serverId, port, statusId, numberOfPlayers, adminOnly, ref maxNumberOfPlayers, ref status);

                   string resultStringGetCount = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Initialize</ResultDescription>\r\n" +
                    "<Initialize  maxNumberOfPlayers=\"" + maxNumberOfPlayers + "\"  status=\"" + status + "\">" + resultGetCount + "</Initialize></Result>";

                 Response.Write(resultStringGetCount);
                 return;

             }
             else
             {
                 string errorStr = "<Result>\r\n  <ReturnCode>-2</ReturnCode>\r\n  <ResultDescription>Unknown Action</ResultDescription>\r\n</Result>";
                 Response.Write(errorStr);
                 return;
             }

            


        }

        static string MAIL_SUBJECT = "Your 3DApp \"{0}\" has been marked private by Kaneva";
        static string MAIL_BODY = "Kaneva can not reach your game or patch servers for the game \"{0}\".<br><br>" +
                                  "The error was: <em><strong>{1}</strong></em><br><br>"+
                                  "Check <a href='http://docs.kaneva.com'>http://docs.kaneva.com</a> for potential resolutions";

        const int PING_OK = 0;
        const int PING_BAD_IP = 1;
        const int PING_NOT_FOUND = 2;
        const int PING_ASSET_VERSION = 3;
        const int PING_PROTOCOL_VERSION = 4;
        const string BANNED_MESSAGE = "This app has been banned";
        const string NOT_FOUND_MESSAGE = "ServerId not found";

        /// <summary>
        /// Initialize the ping ws
        /// </summary>
        /// <param name="key">the game key, empty or null if anonymous</param>
        /// <param name="hostname">hostname to lookup on this key</param>
        /// <param name="watermark"/>the library's watermark</param>
        /// <param name="type">type of server</param>
        /// <param name="ipAddress">ip address or name to show up on Kaneva</param>
        /// <param name="port">port of server</param>
        /// <param name="protocolVersion">client-server protocol</param>
        /// <param name="schemaVersion">db version on server</param>
        /// <param name="serverVersion">file version of serverengine.dll</param>
        /// <param name="assetVersion">version from versioninfo.dat</param>
        /// <param name="serverId">returned id</param>
        /// <param name="maxCount">returned max count allowed by license</param>
        /// <param name="status">returns whether the license if valid or not</param>
        /// <param name="gameId">returns gameID for the given key</param>
        /// <returns>hash used to authenticate server</returns>
        public string Initialize(string key, string hostname, string watermark, int type, string ipAddress, int port,
                                 uint protocolVersion, int schemaVersion, string serverVersion, uint assetVersion,
                                ref int serverId, ref int maxCount, ref bool status, ref int gameId, ref int parentGameId, ref string gameName)
        {
            // Log it?
            if (m_pingLog.IsDebugEnabled)
            {
                m_pingLog.Debug("Ping.Initialize: " + key + " " + hostname + " " + watermark + " " + ipAddress + " " + port + ", Real IP = " + Context.Request.UserHostAddress);
            }

            parentGameId = 0;
            serverId = 0;
            string originalIpAddress = ipAddress;

            // Get the IP address from the request if it is set to AUTO
            bool bUseAutoIP = ipAddress.ToUpper().Equals("AUTO");
            if (bUseAutoIP)
            {
                ipAddress = Context.Request.UserHostAddress;
            }

            InfoRet ir = checkWatermark(watermark);
            if (ir != InfoRet.irOk)
            {
                serverId = 0;
                m_auditLog.Error("Revoked watermark used: \"" + watermark + "\" from ipAddress of is " + ipAddress + " Real IP = " + Context.Request.UserHostAddress);
                return "Your license is invalid";
            }

            // Get the status info (including number of server users)
            ir = getInfo(key, hostname, ref ipAddress, port, type, ref status, ref maxCount, ref serverId, ref gameId, ref parentGameId, ref gameName);

            if (ir == InfoRet.irDupeLicense)
            {
                serverId = 0;
                m_auditLog.Info("Key \"" + key + "\" is already marked as running on the Kaneva server");
                return "Your license is already marked as running on the Kaneva server";
            }
            else if (ir != InfoRet.irOk)
            {
                serverId = 0;
                m_pingLog.Info("Can not get details about key: " + key);
                return "Can not get info for server!";
            }

            if (status) // only update database if active
            {
                if (serverId == 0)
                {
                    m_pingLog.Info("Server not found for hostname: " + hostname + " at: " + ipAddress + ":" + port.ToString());
                    return "Server not found for hostname: " + hostname + " at: " + ipAddress +":" + port.ToString() + ".  Probably not registered";
                }

                checkGameServers(ipAddress, gameId, parentGameId, serverId);

                // get the existing record
                DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityDeveloper();

                GameFacade gf = new GameFacade();
                eGAME_ACCESS access = (eGAME_ACCESS)gf.GetGameAccessByServerId(serverId);
                if (access == eGAME_ACCESS.BANNED)
                    return BANNED_MESSAGE;
                else if (access == eGAME_ACCESS.NOT_FOUND)
                    return NOT_FOUND_MESSAGE;

                if (serverId != 0) // found in table
                {
                    // exists, so update it
                    DataRow dr = gf.UpdateGameServerStart(serverId,
                      maxCount, (int)Constants.eGAME_SERVER_STATUS.STARTING,
                      dbUtility.CleanText(ipAddress), port, dbUtility.CleanText(Context.Request.UserHostAddress), type,
                      protocolVersion, schemaVersion, serverVersion, assetVersion, parentGameId);


                    if (dr != null)
                    {
                        int ret = Convert.ToInt32(dr[0]);

                        if (ret != PING_OK)
                        {
                            // don't return good values if failed
                            gameName = "";
                            maxCount = 1;
                            serverId = 0;
                            gameId = 0;
                        }

                        switch (ret)
                        {
                            case PING_OK:
                                break; // do nothing
                            case PING_NOT_FOUND:
                                return "Parent game id " + parentGameId.ToString() + " not found or no servers running";
                            case PING_PROTOCOL_VERSION:
                                return "Parent game's protcol version doesn't match this one's";
                            case PING_ASSET_VERSION:
                                return "Parent game's asset version doesn't match this one's";
                            default:
                                return "Unknown return code from initalize: " + ret.ToString();
                        }
                    }
                    else
                    {
                        // don't return good values if failed
                        gameName = "";
                        parentGameId = 0;
                        maxCount = 1;
                        serverId = 0;
                        gameId = 0;
                        return "Database error on initialize";
                    }

                }
                else
                {
                    // V4+, must exist, never add on the fly
                    m_pingLog.Error("Ping.Initialize: Failed to find server record for: " + key + " " + hostname + " " + watermark + " " + ipAddress + " " + port + ", Real IP = " + Context.Request.UserHostAddress);
                    return "Server " + hostname + " not registered";
               }
            }

            if (m_pingLog.IsDebugEnabled)
            {
                m_pingLog.Debug("Ping.Initialize Returning: " + serverId + " " + maxCount + " " + status + " for actual ip of " + Context.Request.UserHostAddress );
            }

            return makeHash("KEPPING" + key + originalIpAddress + port);
        }

        /// <summary>
        /// Ping method called by servers to check in 
        /// </summary>
        /// <param name="serverId">the unique if returned from Initialize</param>
        /// <param name="statusId">the engine's status</param>
        /// <param name="numberOfPlayers">current logged on players</param>
        /// <param name="maxNumberOfPlayers">current max players configured by the server</param>
        /// <param name="adminOnly">server's admin only state</param>
        /// <returns>hash used to authenticate server</returns>
        public string Ping(int serverId, int port, int statusId, int numberOfPlayers, int maxNumberOfPlayers, bool adminOnly)
        {
            if (m_pingLog.IsDebugEnabled)
            {
                m_pingLog.Debug("Ping.Ping: " + serverId + " " + statusId + " " + numberOfPlayers + "/" + maxNumberOfPlayers);
            }
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityDeveloper();

            GameFacade gf = new GameFacade();
            eGAME_ACCESS access = (eGAME_ACCESS)gf.GetGameAccessByServerId(serverId);
            if (access == eGAME_ACCESS.BANNED)
                return BANNED_MESSAGE;
            else if (access == eGAME_ACCESS.NOT_FOUND)
                return NOT_FOUND_MESSAGE;
            else if (access == eGAME_ACCESS.INACCESSIBLE)
            {
                Game g = gf.GetGameByServerId(serverId);  // may return null
                if (g != null && g.GameId != 0) 
                    checkGameServers(null, g.GameId, g.ParentGameId, serverId);
            }


            // engine id must exist, so just update it
            DataRow dr = gf.UpdateGameServerPingCheck(serverId, dbUtility.CleanText(Context.Request.UserHostAddress), numberOfPlayers, maxNumberOfPlayers, statusId, adminOnly);

            if ( dr != null )
            {
                int ret = Convert.ToInt32(dr["ret"]);
                switch (ret)
                {
                    case PING_OK: // ok
                        break;
                    case PING_BAD_IP:
                        m_pingLog.Warn("Ping.Ping: " + serverId + " pinged in with wrong actual ip address of " + Context.Request.UserHostAddress );
                        return "Ip address invalid for server id";
                    case PING_NOT_FOUND:
                        return "Server Id not found";
                    case PING_PROTOCOL_VERSION:
                        return "Parent game's protcol version doesn't match this one's";
                    case PING_ASSET_VERSION:
                        return "Parent game's asset version doesn't match this one's";
                    default:
                        return "Unknown return code";
                }
            }

            // Store some history
            StoreHistory(serverId, numberOfPlayers, Context.Request.UserHostAddress, port);

            return makeHash("KEPPING" + serverId + statusId + numberOfPlayers);
        }

        /// <summary>
        /// called occasionally by the server to check on the count and expiration
        /// </summary>
        /// <param name="key">the game key, empty or null if anonymous</param>
        /// <param name="serverId"></param>
        /// <param name="port"></param>
        /// <param name="statusId">the engine's status</param>
        /// <param name="numberOfPlayers">current logged on players</param>
        /// <param name="adminOnly">server's admin only state</param>
        /// <param name="maxNumberOfPlayers">current max players configured by the server</param>
        /// <param name="status">returns whether the license if valid or not</param>
        /// <returns>hash used to authenticate server</returns>
        public string GetCount(string key, int serverId, int port, int statusId, int numberOfPlayers, bool adminOnly, ref int maxNumberOfPlayers, ref bool status)
        {
            if (m_pingLog.IsDebugEnabled)
            {
                m_pingLog.Debug("Ping.GetCount: " + key + " " + statusId + " " + numberOfPlayers);
            }

            GameFacade gf = new GameFacade();
            eGAME_ACCESS access = (eGAME_ACCESS)gf.GetGameAccessByServerId(serverId);
            if (access == eGAME_ACCESS.BANNED)
                return BANNED_MESSAGE;
            else if (access == eGAME_ACCESS.NOT_FOUND)
                return NOT_FOUND_MESSAGE;


            status = false;
            maxNumberOfPlayers = 1;

            int gameId = 0;
            int parentGameId = 0;

            string ipAddress = "";
            string gameName = "";
            InfoRet ir = getLicenseInfo(key, ref ipAddress, -1, ref status, ref maxNumberOfPlayers, ref gameId, ref parentGameId, ref gameName);

            if (ir == InfoRet.irDupeLicense)
            {
                m_auditLog.Info("Key \"" + key + "\" is already marked as running on the Kaneva server");
                return "Your license is already marked as running on the Kaneva server";
            }
            else if (ir != InfoRet.irOk)
            {
                m_pingLog.Info("Can not get details about key: " + key);
                return "Can not get info for server!";
            }
            else
            {
                string ret = Ping(serverId, port, statusId, numberOfPlayers, maxNumberOfPlayers, adminOnly);

                if (ret.Length == 0 && parentGameId != 0 )
                {
                    checkGameServers(null, gameId, parentGameId, serverId);
                }
                return ret;
            }
        }

        // returns true if ok, gameId will be 0 if no license record
        protected InfoRet getLicenseInfo(string key, ref string ipAddress, int port, ref bool status, ref int maxNumberOfPlayers, ref int gameId, ref int parentGameId, ref string gameName)
        {
            maxNumberOfPlayers = 1;
            gameId = 0;
            status = true; // now default them to active
            int licenseType = (int) GameLicenseSubscription.LicenseSubscriptionIdTypes.BetaTrial;

            Hashtable parameters = new Hashtable();

            string sql = "";
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityDeveloper();

            // They passed in a key, get the max based on the key
            sql = "SELECT ls.max_server_users, gl.game_id, g.parent_game_id, gl.license_subscription_id, g.game_name" +
                " FROM game_licenses gl, game_license_subscriptions ls, games g " +
                " WHERE gl.game_id = g.game_id" +
                " AND gl.license_subscription_id = ls.license_subscription_id" +
                " AND gl.game_key = @key" +
                " AND gl.license_status_id = @status";

            parameters = new Hashtable();
            parameters.Add("@key", key);
            parameters.Add("@status", (int) GameLicense.LicenseStatus.Active);
            DataRow drLicense = dbUtility.GetDataRow(sql, parameters, false);

            if (drLicense == null)
            {
                if (m_pingLog.IsDebugEnabled)
                {
                     m_pingLog.Debug("Ping.getInfo: " + key + " " + ipAddress + " " + port + ", key not found, setting to 1");
                }
                // Key not found
                maxNumberOfPlayers = 1;
                gameId = 0;
            }
            else
            {
                // Key found
                maxNumberOfPlayers = Convert.ToInt32(drLicense["max_server_users"]);
                gameId = Convert.ToInt32(drLicense["game_id"]);
                parentGameId = Convert.ToInt32(drLicense["parent_game_id"]);
                gameName = drLicense["game_name"].ToString();

                // Used to see if we are commercial, since only commerical may not use Auto IP.
                licenseType = Convert.ToInt32(drLicense["license_subscription_id"]);
            }

            // If they are not using a commercial license, read the IP from the Request.
            // Get Count does not pass an IP Address, check length so we don't have error (dups) on commercial.
            if (!licenseType.Equals((int) GameLicenseSubscription.LicenseSubscriptionIdTypes.Professional) && ipAddress.Length > 0)
            {
                ipAddress = Context.Request.UserHostAddress;
            }

            return InfoRet.irOk;
        }

        // returns true if ok, false if bad data in database
        // serverId will be 0 if no record in current engines table
        protected InfoRet getInfo(string key, string hostname, ref string ipAddress, int port, int serverType, ref bool status, ref int maxNumberOfPlayers, ref int serverId, ref int gameId, ref int parentGameId, ref string gameName )
        {
            serverId = 0;
            maxNumberOfPlayers = 1;
            gameId = 0;
            status = true; // now default them to active

            getLicenseInfo(key, ref ipAddress, port, ref status, ref maxNumberOfPlayers, ref gameId, ref parentGameId, ref gameName);

            DataRow dr;

            // if call from initialize (port >= 0) see if IP changed
            if ( port > -1 )
            {
                string sql = "";
                DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityDeveloper();

                // get the serverId based on key, may not exist if first ping into server
                // 1/07 updated the last column to let MySQL do date diff calculation since ran into overflow problems.
                // 4/08 V4 requires hostname to already be in the current_game_engines
                sql = "SELECT server_id, ip_address, port, server_status_id, (last_ping_datetime > DATE_SUB(NOW(),INTERVAL 7 MINUTE)) as active " +
                    "  FROM game_servers gs inner join games g on gs.game_id = g.game_id" +
                    " WHERE g.game_id = @gameId" + 
                    " AND server_name = @hostname" +
                    " AND port = @port" +
                    " AND server_type_id = @serverType";

                Hashtable parameters = new Hashtable();

                parameters.Add("@port", port);
                parameters.Add("@gameId", gameId);
                parameters.Add("@hostname", hostname);
                parameters.Add("@serverType", serverType);
                dr = dbUtility.GetDataRow(sql, parameters, false);

                if (dr != null)
                {
                    if (dr.Table.Rows.Count > 0)
                    {
                        if (dr.Table.Rows.Count == 1)
                        {
                            // found it, check the validity of the license
                            serverId = makeInt(dr[0]);

                            if (ipAddress != null && port > 0)
                            {
                                int isActive = makeInt(dr[4]);
                                int gameStatus = makeInt(dr[3]);
                                int oldPort = makeInt(dr[2]);
                                string oldIpAddress = (string)dr[1];

                                // check the status
                                // if changing IP/port, must not be running, or > 7 minutes old
                                if ((ipAddress != oldIpAddress || port != oldPort) && gameStatus == (int)Constants.eGAME_SERVER_STATUS.RUNNING &&
                                    isActive != 0) // it's running and they want to change port?? no way
                                {
                                    if (m_pingLog.IsDebugEnabled)
                                    {
                                        m_pingLog.Debug("Ping.getInfo: " + key + " " + ipAddress + " " + port + ", duplicate license.");
                                    }
                                    maxNumberOfPlayers = 1; // set them to one user
                                    return InfoRet.irDupeLicense;
                                }
                            }

                        }
                        else
                        {
                            // more that one row with that key is an error!
                            return InfoRet.irNotFound;
                        }
                    }
                }
                else
                {
                    gameId = 0;
                }
                // key not found, first time coming in
            }

            return InfoRet.irOk;
        }


        private void checkGameServers(string overrideIpAddress, int gameId, int parentGameId, int serverId)
        {
            string ret = "";

            // if child game, check the server is accessible and patch url, too
            if (parentGameId != 0)
            {
                GameFacade gf = new GameFacade();
                bool internalError = false;
                Game game = gf.GetGameByGameId(gameId);

                if (game != null && game.GameId == gameId)
                {
                    if (game.IsIncubatorHosted)
                    {
                        //Skip incubator hosted games
                        m_pingLog.Debug("CheckGameServers skipped incubator-hosted server " + serverId.ToString());
                    }
                    else
                    {
                        // if they are public, or were inaccessible, check again
                        if (game.GameAccessId == (int)eGAME_ACCESS.PUBLIC || game.GameAccessId == (int)eGAME_ACCESS.INACCESSIBLE)
                        {
                            ret = gf.CheckGameServers(overrideIpAddress, gameId, serverId, out internalError);
                            if (ret.Length > 0)
                            {
                                if (internalError)
                                    m_pingLog.Error("Internal error from CheckGameServers: " + ret);
                                else
                                    m_pingLog.Warn("Error from CheckGameServers: " + ret);

                                if (game.GameAccessId == (int)eGAME_ACCESS.PUBLIC)
                                {
                                    game.GameAccessId = (int)eGAME_ACCESS.INACCESSIBLE; // set as private since can't access it
                                    gf.SaveGame(game);

                                    // get the first user in the game's company, which there is only one of for now
                                    GameDeveloperFacade gdf = new GameDeveloperFacade();
                                    PagedList<GameDeveloper> devs = gdf.GetGameDevelopersList(game.OwnerId, "", "", 1, 1);
                                    if (devs.Count > 0 && devs[0].UserId != 0)
                                    {

                                        // Insert the message to let them know what we did
                                        string subject = String.Format(MAIL_SUBJECT, game.GameName);
                                        string body = String.Format(MAIL_BODY, game.GameName, ret);
                                        Message message = new Message(0, devs[0].UserId, devs[0].UserId, subject,
                                            body, new DateTime(), 0, (int)Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                                        new UserFacade().InsertMessage(message);
                                    }
                                }
                            }
                            else if (game.GameAccessId == (int)eGAME_ACCESS.INACCESSIBLE)
                            {
                                game.GameAccessId = (int)eGAME_ACCESS.PUBLIC;
                                gf.SaveGame(game);
                            }
                        }

                        if (ret == "")
                        {
                            m_pingLog.Debug("CheckGameServers validated server " + serverId.ToString());
                        }
                    }
                }
                else
                {
                    m_pingLog.Warn("checkGameServers cannot load game with id of " + gameId.ToString());
                }
            }
        }
    }
}
