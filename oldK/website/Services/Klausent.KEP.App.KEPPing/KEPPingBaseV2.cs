///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// Import log4net classes.
using log4net;
using log4net.Config;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections;
using System.Data;
using KlausEnt.KEP.Kaneva;

using System.IO;
using System.Diagnostics;
using Kaneva.BusinessLayer.BusinessObjects.Metrics;
using Kaneva.BusinessLayer.Facade;

namespace KEPPing
{
       /// <summary>
    /// helper base class for KEPPing to avoid copying classes, again,
    /// created for v4
    /// </summary>
    public class KEPPingBaseV2 : System.Web.UI.Page
    {
        protected static readonly ILog m_pingLog = LogManager.GetLogger("KEPPing");
        protected static readonly ILog m_auditLog = LogManager.GetLogger("Audit");

        /// <summary>
        /// OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(System.EventArgs e)
        {
            if (Configuration.MetricPageTimingEnabled)
            {
                // Metric Timing
                Stopwatch stopwatch = new Stopwatch();
                this.Context.Items["MetricPageTiming"] = stopwatch;
                stopwatch.Start();
            }
        }


        /// <summary>
        /// OnPreRender
        /// </summary>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (Configuration.MetricPageTimingEnabled)
            {
                // Metric Timing
                Stopwatch stopwatch = (Stopwatch)this.Context.Items["MetricPageTiming"];
                stopwatch.Stop();

                TimeSpan ts = stopwatch.Elapsed;

                string pageName = Path.GetFileName(Page.Request.Url.AbsolutePath).ToUpper();

                // Is this a page to record metrics on
                if (Configuration.MetricsPageTiming.ContainsKey(pageName))
                {
                    PageTiming pt = (PageTiming)Configuration.MetricsPageTiming[pageName];
                    MetricsFacade metricsFacade = new MetricsFacade();

                    if (metricsFacade.CanLogPageMetrics(pt.URL.ToUpper()))
                    {
                        string action = "";
                        if (Request.Params["action"] != null)
                        {
                            action = Request.Params["action"].ToUpper();

                            if (pt.Action.ToUpper().IndexOf(action, 0) > -1)
                            {
                                metricsFacade.InsertMetricsPageTiming(pt.Name + " " + action, ts.TotalMilliseconds);
                            }
                        }


                    }
                }
            }
        }

        public enum InfoRet
        {
            irOk,
            irNotFound,
            irDupeLicense,
            irRevoked		// bad watermark
        }

        public KEPPingBaseV2()
        {
            //CODEGEN: This call is required by the ASP.NET Web Services Designer
            InitializeComponent();
        }
        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        protected void InitializeComponent()
        {
        }

        #endregion

        // helper to safely cast to an int
        protected int makeInt(Object o)
        {
            if (o is Int32)
                return (int)(Int32)o;
            else if (o is Int64)
                return (int)(Int64)o;
            else if (o is int)
                return (int)o;
            else if (o is Double)
                return (int)(Double)o;
            else
                return 0;
        }

        /// <summary>
        /// Returns the number of users if no key was given
        /// </summary>
        /// <returns></returns>
        protected int GetNoKeyNumberOfUsers()
        {
            DataRow drDevLicense = GetLicenseSubscription((int)GameLicenseSubscription.LicenseSubscriptionIdTypes.BetaTrial);

            if (drDevLicense != null && drDevLicense["max_server_users"] != DBNull.Value)
            {
                return Convert.ToInt32(drDevLicense["max_server_users"]);
            }
            else
            {
                return 5;
            }
        }

        /// <summary>
        /// GetLicenseSubscripsion
        /// </summary>
        /// <returns></returns>
        protected DataRow GetLicenseSubscription(int licenseSubscriptionId)
        {
            string sqlSelect = "SELECT license_subscription_id, name, length_of_subscription, amount, amount_kei_point_id, max_server_users  " +
                " FROM game_license_subscriptions " +
                " WHERE license_subscription_id = " + licenseSubscriptionId;

            return KanevaGlobals.GetDatabaseUtilityDeveloper().GetDataRow(sqlSelect, false);
        }

        protected InfoRet checkWatermark(string watermark)
        {
            InfoRet ret = InfoRet.irOk;
            return ret;
        }

        /// <summary>
        /// StoreHistory
        /// </summary>
        public void StoreHistory(int serverId, int numberOfPlayers, string ipAddress, int port)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityDeveloper();

            // See if history has been stored in the last hour
            string sql = "SELECT server_history_id, player_count " +
                " FROM game_server_history " +
                " WHERE server_id = @serverId " +
                " AND created_date between DATE_FORMAT(NOW(),'%Y-%m-%d %H:00:00') AND DATE_FORMAT(NOW(),'%Y-%m-%d %H:59:59')";

            Hashtable parameters = new Hashtable();
            parameters.Add("@serverId", serverId);

            DataRow drHistory = dbUtility.GetDataRow(sql, parameters, false);

            // If it does not already exist, add a new history record
            if (drHistory == null)
            {
                sql = "INSERT INTO game_server_history " +
                    " (server_id, player_count, created_date, ip_address, port) " +
                    " VALUES " +
                    "(@serverId, @playerCount, " + dbUtility.GetCurrentDateFunction() + ", @ipAddress, @port)";

                parameters = new Hashtable();
                parameters.Add("@serverId", serverId);
                parameters.Add("@playerCount", numberOfPlayers);
                parameters.Add("@ipAddress", ipAddress);
                parameters.Add("@port", port);
                dbUtility.ExecuteNonQuery(sql, parameters);
            }
            else
            {
                // If so, update if number of users is greater than current
                if (numberOfPlayers > Convert.ToInt32(drHistory["player_count"]))
                {
                    sql = "UPDATE game_server_history " +
                        " SET player_count = @playerCount, " +
                        " ip_address = @ipAddress, " +
                        " created_date = now() " +
                        " WHERE server_id = @serverId " +
                        " AND player_count < @playerCount" +
                        " AND created_date between DATE_FORMAT(NOW(),'%Y-%m-%d %H:00:00') AND DATE_FORMAT(NOW(),'%Y-%m-%d %H:59:59') ";
                    parameters = new Hashtable();
                    parameters.Add("@serverId", serverId);
                    parameters.Add("@playerCount", numberOfPlayers);
                    parameters.Add("@ipAddress", ipAddress);
                    dbUtility.ExecuteNonQuery(sql, parameters);
                }
            }

        }


        protected string makeHash(string s)
        {
            // This is one implementation of the abstract class MD5.
            System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();

            System.Text.Encoder encoder = System.Text.ASCIIEncoding.UTF8.GetEncoder();
            int byteCount = encoder.GetByteCount(s.ToCharArray(), 0, s.Length, true);
            byte[] bytes = new byte[byteCount];
            encoder.GetBytes(s.ToCharArray(), 0, s.Length, bytes, 0, true);
            byte[] result = md5.ComputeHash(bytes);

            string ret = "";
            for (int i = 0; i < result.Length; i++)
            {
                ret = ret + result[i].ToString("x2");
            }
            return ret;
        }
    }
}
