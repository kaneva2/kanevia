///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using KlausEnt.KEP.Kaneva;

// Import log4net classes.
using log4net;
using log4net.Config;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KEPPing
{
    /// <summary>
    /// helper base class for KEPPing to avoid copying classes, again,
    /// created for v4
    /// </summary>
    public class KEPPingBase : System.Web.Services.WebService
    {
        protected static readonly ILog m_pingLog = LogManager.GetLogger("KEPPing");
        protected static readonly ILog m_auditLog = LogManager.GetLogger("Audit");

        public enum InfoRet
        {
            irOk,
            irNotFound,
            irDupeLicense,
            irRevoked		// bad watermark
        }

        public KEPPingBase()
        {
            //CODEGEN: This call is required by the ASP.NET Web Services Designer
            InitializeComponent();
        }
        #region Component Designer generated code

        //Required by the Web Services Designer 
        protected IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        protected void InitializeComponent()
        {
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion

        // helper to safely cast to an int
        protected int makeInt(Object o)
        {
            if (o is Int32)
                return (int)(Int32)o;
            else if (o is Int64)
                return (int)(Int64)o;
            else if (o is int)
                return (int)o;
            else if (o is Double)
                return (int)(Double)o;
            else
                return 0;
        }

        /// <summary>
        /// Returns the number of users if no key was given
        /// </summary>
        /// <returns></returns>
        protected int GetNoKeyNumberOfUsers()
        {
            DataRow drDevLicense = GetLicenseSubscription((int) GameLicenseSubscription.LicenseSubscriptionIdTypes.BetaTrial);

            if (drDevLicense != null && drDevLicense["max_server_users"] != DBNull.Value)
            {
                return Convert.ToInt32(drDevLicense["max_server_users"]);
            }
            else
            {
                return 5;
            }
        }

        /// <summary>
        /// GetLicenseSubscripsion
        /// </summary>
        /// <returns></returns>
        protected DataRow GetLicenseSubscription(int licenseSubscriptionId)
        {
            string sqlSelect = "SELECT license_subscription_id, name, length_of_subscription, amount, amount_kei_point_id, max_server_users  " +
                " FROM game_license_subscriptions " +
                " WHERE license_subscription_id = " + licenseSubscriptionId;

            return KanevaGlobals.GetDatabaseUtilityDeveloper().GetDataRow(sqlSelect, false);
        }

        protected InfoRet checkWatermark(string watermark)
        {
            InfoRet ret = InfoRet.irOk;
            return ret;
        }

        protected InfoRet OLD_checkWatermark(string watermark)
        {
            InfoRet ret = InfoRet.irOk;

            Hashtable parameters = new Hashtable();
            string sql = "";

            // They passed in a watermark, let's see if it is bad
            sql = "SELECT count(*) " +
                " FROM elite_source_watermarks " +
                " WHERE watermark = @wm " +
                " AND revoked <> 0";

            parameters = new Hashtable();
            parameters.Add("@wm", watermark);
            DataRow dr = null;
            try
            {
                dr = KanevaGlobals.GetDatabaseUtilityDeveloper().GetDataRow(sql, parameters, false);
            }
            catch (Exception e)
            {
                m_pingLog.Debug("Ping exception " + e.ToString());
            }

            if (dr != null)
            {
                if (dr.Table.Rows.Count > 0)
                {
                    if (dr.Table.Rows.Count == 1)
                    {
                        // found it, check the validity of the license
                        if (makeInt(dr[0]) != 0)
                            ret = InfoRet.irRevoked;
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// Ping method called by servers to check in 
        /// </summary>
        /// <param name="serverId">the unique if returned from Initialize</param>
        /// <param name="statusId">the engine's status</param>
        /// <param name="numberOfPlayers">current logged on players</param>
        /// <param name="maxNumberOfPlayers">current max players configured by the server</param>
        /// <param name="adminOnly">server's admin only state</param>
        /// <returns>hash used to authenticate server</returns>
        [WebMethod]
        public string Ping(int serverId, int port, int statusId, int numberOfPlayers, int maxNumberOfPlayers, bool adminOnly )
        {
            if (m_pingLog.IsDebugEnabled)
            {
                m_pingLog.Debug("Ping.Ping: " + serverId + " " + statusId + " " + numberOfPlayers + "/" + maxNumberOfPlayers);
            }
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityDeveloper();

            // engine id must exist, so just update it
            string update = "CALL update_game_servers_ping( @serverId, @numberOfPlayers, @maxNumberOfPlayers, @statusId, " +
                                                        (adminOnly ? "'T'" : "'F'") + ");";

            Hashtable parameters = new Hashtable();
            parameters.Add("@serverId", serverId);
            parameters.Add("@numberOfPlayers", numberOfPlayers);
            parameters.Add("@maxNumberOfPlayers", maxNumberOfPlayers);
            parameters.Add("@statusId", statusId);
            int count = dbUtility.ExecuteNonQuery(update, parameters);

            // Store some history
            StoreHistory(serverId, numberOfPlayers, Context.Request.UserHostAddress, port);

            return makeHash("KEPPING" + serverId + statusId + numberOfPlayers);
        }

        /// <summary>
        /// StoreHistory
        /// </summary>
        public void StoreHistory(int serverId, int numberOfPlayers, string ipAddress, int port)
        {
			DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtilityDeveloper();

            // See if history has been stored in the last hour
            string sql = "SELECT server_history_id, player_count " +
                " FROM game_server_history " +
                " WHERE server_id = @serverId " +
				" AND created_date between DATE_FORMAT(NOW(),'%Y-%m-%d %H:00:00') AND DATE_FORMAT(NOW(),'%Y-%m-%d %H:59:59')" ;
			
            Hashtable parameters = new Hashtable();
            parameters.Add("@serverId", serverId);
			
            DataRow drHistory = dbUtility.GetDataRow(sql, parameters, false);

            // If it does not already exist, add a new history record
            if (drHistory == null)
            {
                sql = "INSERT INTO game_server_history " +
                    " (server_id, player_count, created_date, ip_address, port) " +
                    " VALUES " +
                    "(@serverId, @playerCount, " + dbUtility.GetCurrentDateFunction() + ", @ipAddress, @port)";

                parameters = new Hashtable();
                parameters.Add("@serverId", serverId);
                parameters.Add("@playerCount", numberOfPlayers);
                parameters.Add("@ipAddress", ipAddress);
                parameters.Add("@port", port);
                dbUtility.ExecuteNonQuery(sql, parameters);
            }
            else
            {
                // If so, update if number of users is greater than current
                if (numberOfPlayers > Convert.ToInt32(drHistory["player_count"]))
                {
                    sql = "UPDATE game_server_history " +
                        " SET player_count = @playerCount, " +
                        " ip_address = @ipAddress, " +
						" created_date = now() " +
                        " WHERE server_id = @serverId " +
                        " AND player_count < @playerCount" +
						" AND created_date between DATE_FORMAT(NOW(),'%Y-%m-%d %H:00:00') AND DATE_FORMAT(NOW(),'%Y-%m-%d %H:59:59') ";
                    parameters = new Hashtable();
                    parameters.Add("@serverId", serverId);
                    parameters.Add("@playerCount", numberOfPlayers);
                    parameters.Add("@ipAddress", ipAddress);
                    dbUtility.ExecuteNonQuery(sql, parameters);
                }
            }

        }


        protected string makeHash(string s)
        {
            // This is one implementation of the abstract class MD5.
            System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();

            System.Text.Encoder encoder = System.Text.ASCIIEncoding.UTF8.GetEncoder();
            int byteCount = encoder.GetByteCount(s.ToCharArray(), 0, s.Length, true);
            byte[] bytes = new byte[byteCount];
            encoder.GetBytes(s.ToCharArray(), 0, s.Length, bytes, 0, true);
            byte[] result = md5.ComputeHash(bytes);

            string ret = "";
            for (int i = 0; i < result.Length; i++)
            {
                ret = ret + result[i].ToString("x2");
            }
            return ret;
        }


    }

}
