///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.IO;
using log4net;
using KlausEnt.KEP.Kaneva;

namespace Events
{
    public partial class Events : Form
    {
        #region Declarations

        private EventFacade eventFacade;
        private UserFacade userFacade;
        private CommunityFacade communityFacade;
        private GameFacade gameFacade;
        private ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Initialization
        public Events()
        {
            InitializeComponent();
        }

        private void Events_Load(object sender, EventArgs e)
        {
            RunEventsService();

            this.Dispose();
            this.Close();
        }
        #endregion

        #region Main Execution

        private void RunEventsService()
        {
            // Set up log for net
            string s = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
            FileInfo l_fi = new FileInfo(s);
            log4net.Config.XmlConfigurator.Configure(l_fi);

            // Grab the current system time. Doing this here because if we use 'NOW()' in MySQL we're betting that runtime is under 1 minute.
            DateTime currentTime = DateTime.Now;

            // Generate top world events at midnight
            DateTime roundedDate = RoundToNearestHalfHour(currentTime);

            //if (roundedDate.Hour == 0 && roundedDate.Minute == 0)
            //{
            //    // Generate top 10 worlds event, every hour starting at 12:00 noon.
            //    GenerateTop10WorldsEvents(new DateTime(roundedDate.Year, roundedDate.Month, roundedDate.Day, 12, 0, 0), 60, "", null, "");

            //    // Generate top 10 battle events, every hour starting at 12:30pm.
            //    string[] battleTemplates = { "Battle", "Epic Battle" };
            //    GenerateTop10WorldsEvents(new DateTime(roundedDate.Year, roundedDate.Month, roundedDate.Day, 12, 30, 0), 60, "", battleTemplates, "Battle");

            //    // If Sunday, generate top 10 church events, every half hour starting at 8:00am.
            //    if (roundedDate.DayOfWeek == DayOfWeek.Sunday)
            //    {
            //        GenerateTop10WorldsEvents(new DateTime(roundedDate.Year, roundedDate.Month, roundedDate.Day, 8, 0, 0), 30, "Faith-Based", null, "Faith-Based");
            //    }
            //}

            // Have we already processed random invites?
            bool automatedInvitesProcessed = false;

            // Process each event reminder
            PagedList<EventReminder> eventReminders = GetEventFacade.GetEventReminders("reminder_time_offset ASC", 1, Int32.MaxValue);
            foreach (EventReminder reminder in eventReminders)
            {
                // Process Each Event
                PagedList<Event> ongoingEvents = GetEventFacade.GetEventsByTimestampOffset(currentTime, reminder.ReminderTimeOffset, "", 1, Int32.MaxValue);
                foreach (Event evt in ongoingEvents)
                {
                    Community evtCommunity = GetCommunityFacade.GetCommunity(evt.CommunityId);
                    int evtGameId = evtCommunity.WOK3App.GameId;
                    string fromUserName = GetUserFacade.GetUserName(evt.UserId);
                    string stpUrl = GetSTPUrl(evtCommunity);
                    string eventDetailsUrl = "http://" + KanevaGlobals.SiteName + "/mykaneva/events/eventDetail.aspx?event=" + evt.EventId;
                    string playWokUrl = GetPlayWokUrl(evtCommunity);


                    // Reward everyone who is currently at the event (Handles case for people who are at the event when the event starts)
                    if (reminder.ReminderTimeOffset == 0)
                    {
                        PagedDataTable pdtUsers = GetUsersInEventWorld(evtCommunity);

                        // Try to reward each user
                        for (int i = 0; i < pdtUsers.Rows.Count; i++)
                        {
                            string useridColumn = (evtCommunity.WOK3App.GameId > 0 ? "user_id" : "kaneva_user_id");
                            GetEventFacade.RewardUserForEvent(Convert.ToInt32(pdtUsers.Rows[i][useridColumn]), evt.EventId);
                            GetEventFacade.UpdateEventInviteeAttendance(evt.EventId, Convert.ToInt32(pdtUsers.Rows[i][useridColumn]));
                        }

                        // If this is a premium event and we haven't already invited random members, do so
                        if (evt.IsPremium && !automatedInvitesProcessed)
                        {
                            automatedInvitesProcessed = ProcessRandomEventInvites(evt, fromUserName, evtCommunity, stpUrl);
                        }
                    }


                    // Process each event attendee
                    PagedList<EventInvitee> eventAttendees = GetEventFacade.GetEventInviteesByStatus(evt.EventId, (int)EventInvitee.Invite_Status.ACCEPTED, true, "", 1, Int32.MaxValue);
                    foreach (EventInvitee attendee in eventAttendees)
                    {
                        string requestId = string.Empty;

                        // Only send notifications/emails if the world owner is also the creator of the event
                        if (evt.UserId == evtCommunity.CreatorId)
                        {
                            // Only send client tickler notification or kim invite for events starting now
                            if (reminder.ReminderTimeOffset == 0)
                            {
                                try
                                {
                                    DataTable dtPlaceInfo = UsersUtility.GetCurrentPlaceInfo (attendee.UserId);

                                    // Only send to users in WoK that are not already in the world.
                                    if (dtPlaceInfo != null && dtPlaceInfo.Rows.Count > 0 && Convert.ToInt32 (dtPlaceInfo.Rows[0]["community_id"]) != evt.CommunityId)
                                    {
                                        // Generate the request type send id and send the email
                                        requestId = GetUserFacade.GetTrackingRequestId (requestId, Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_REMINDER, evt.UserId, evtGameId, evt.CommunityId);
                                        string requestTypeSentIdInWorld = userFacade.InsertTrackingTypeSent (requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_IN_WORLD, attendee.UserId);
                                        GetUserFacade.SendEventReminderToPlayer (fromUserName, attendee.Username, string.Empty, evt.Title, stpUrl, requestTypeSentIdInWorld);
                                    }
                                    else if (dtPlaceInfo == null && UsersUtility.IsUserOnlineInKIM (attendee.UserId))
                                    {
                                        // Generate the request type send id and send the kim invite
                                        requestId = GetUserFacade.GetTrackingRequestId (requestId, Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_REMINDER, evt.UserId, evtGameId, evt.CommunityId);
                                        string requestTypeSentIdInKIM = userFacade.InsertTrackingTypeSent (requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_KIM, attendee.UserId);

                                        string kimMessage = fromUserName + "'s event<br/>" + evt.Title + "<br/>in " + evt.Location + "<br/>is starting now.<br/><br/>Earn 1000 Rewards a day for going to an event.";
                                        string kimPlayWoKLink = GetKIMPlayWokUrl (evtCommunity, requestTypeSentIdInKIM);

                                        GetUserFacade.SendKIMInviteToPlayer (fromUserName, attendee.Username, kimMessage, kimPlayWoKLink);
                                    }
                                }
                                catch (Exception exc)
                                {
                                    m_logger.Error ("Error in sending Events In World and KIM Reminders " + evt.EventId, exc);
                                }
                            }
                            // Don't send any of the other reminders to the event owner
                            else if (attendee.UserId != evt.UserId)
                            {
                                try
                                {
                                    if (KanevaGlobals.EnableEventKMails)
                                    {
                                        // Send reminder PMs
                                        requestId = GetUserFacade.GetTrackingRequestId (requestId, Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_REMINDER, evt.UserId, evtGameId, evt.CommunityId);
                                        string requestTypeSentIdPM = userFacade.InsertTrackingTypeSent (requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_PM, attendee.UserId);

                                        Event.eMESSAGE_TYPE pmType = (Event.eMESSAGE_TYPE) Enum.Parse (typeof (Event.eMESSAGE_TYPE), reminder.EmailType);

                                        Event.EventMessage em = GetEventFacade.GetEventMessageDetails (pmType, evt, fromUserName, eventDetailsUrl, string.Empty, requestTypeSentIdPM, playWokUrl);

                                        Kaneva.BusinessLayer.BusinessObjects.Message message = new Kaneva.BusinessLayer.BusinessObjects.Message (0, evt.UserId, attendee.UserId, em.Subject, em.Message,
                                            new DateTime (), 0, (int) Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                                        GetUserFacade.InsertMessage (message);
                                    }
                                }
                                catch (Exception exc)
                                {
                                    m_logger.Error ("Error in sending Event reminder private messages " + evt.EventId, exc);
                                }

                                try
                                {
                                    // Send reminder emails
                                    if (!string.IsNullOrEmpty (reminder.EmailType) && attendee.EmailNotifications && attendee.UserStatusId.Equals ((int) Constants.eUSER_STATUS.REGVALIDATED))
                                    {
                                        requestId = GetUserFacade.GetTrackingRequestId (requestId, Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_REMINDER, evt.UserId, evtGameId, evt.CommunityId);
                                        string requestTypeSentIdEmail = userFacade.InsertTrackingTypeSent (requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_EMAIL, attendee.UserId);
                                        SendEventEmail (evt, reminder.EmailType, attendee.Email, attendee.UserId, evt.UserId, string.Empty, requestTypeSentIdEmail, eventDetailsUrl, playWokUrl);
                                    }
                                }
                                catch (Exception exc)
                                {
                                    m_logger.Error ("Error in sending Event reminder emails " + evt.EventId, exc);
                                }
                            }
                        }
                    }

                    try
                    {
                        //TODO: More granular logging of reminder sent success, if needed later
                        GetEventFacade.LogEventReminderNotification(evt.EventId, reminder.ReminderId, currentTime);
                    }
                    catch (Exception exc)
                    {
                        m_logger.Error("Error in logging event reminder notification " + evt.EventId, exc);
                    }
                }
            }

            // Process any recurring events
            ProcessRecurringEvents ();
        }

        #endregion Main Execution

        #region Helper Functions

        /// <summary>
        /// SendEventEmail
        /// </summary>
        private void SendEventEmail(Event evt, string emailTypeName, string toEmail, int toUserId, int fromUserId, string userMessage, string trackingMetric, string eventUrl, string playWoKUrl)
        {
            // calculate formatted event date & time
            string ampm = evt.StartTime.ToString("tt").ToLower();
            string eventDateTime = evt.StartTime.ToString("dddd, MMMM ") + evt.StartTime.Day + " at " +
                string.Format("{0:h:mm}{1}", evt.StartTime, ampm) + " EST";

            // store subject and message as overload fields		
            int overloadId = 0;
            overloadId = KlausEnt.KEP.Kaneva.Mailer.Queuer.AddOverloadField(overloadId, "#EventName#", evt.Title);
            overloadId = KlausEnt.KEP.Kaneva.Mailer.Queuer.AddOverloadField(overloadId, "#EventDetails#",
                (KanevaGlobals.TruncateWithEllipsis(evt.Details, 50) + "<br/>" + eventDateTime + "<br/>" + evt.Location));
            overloadId = KlausEnt.KEP.Kaneva.Mailer.Queuer.AddOverloadField(overloadId, "#EventLocation#", evt.Location);
            overloadId = KlausEnt.KEP.Kaneva.Mailer.Queuer.AddOverloadField(overloadId, "#EventUrl#", eventUrl);
            overloadId = KlausEnt.KEP.Kaneva.Mailer.Queuer.AddOverloadField(overloadId, "#PlayWoKUrl#", playWoKUrl);

            //if overloadid did not work don't send the email.
            if (overloadId != 0)
            {
                int typeId = KlausEnt.KEP.Kaneva.Mailer.EmailTypeUtility.GetTypeIdByName(emailTypeName);
                KlausEnt.KEP.Kaneva.Mailer.TemplateSherpa.DressAndSendTemplate(typeId, 0, toEmail, 0, fromUserId, toUserId, "", 0, userMessage, "", 0, overloadId, trackingMetric);
            }
        }

        /// <summary>
        /// Rounds a DateTime to the nearest half-hour.
        /// </summary>
        /// <param name="input">DateTime base to round.</param>
        /// <returns>A new DateTime based on the input, rounded to the nearest half-hour.</returns>
        private DateTime RoundToNearestHalfHour(DateTime input)
        {
            DateTime dt = new DateTime(input.Year, input.Month, input.Day, input.Hour, 0, 0);

            if (input.Minute < 15)
                return dt;
            if (input.Minute < 45)
                return dt.AddMinutes(30);
            else
                return dt.AddHours(1);
        }

        /// <summary>
        /// Gets the current top 10 index to use in generated events at a given DateTime.
        /// </summary>
        /// <param name="dt">The current datetime for which to return an index.</param>
        /// <param name="seed">The DateTime at which item 10 is set to occur each day.</param>
        /// <param name="minuteFrequency">The frequency of event generation.</param>
        /// <returns>1-based index for the appropriate event slot, or -1 if error.</returns>
        private int GetEventTop10IndexForDateTime(DateTime dt, DateTime seed, int minuteFrequency)
        {
            TimeSpan diff = dt.Subtract(seed);
            int index = (10 - (int)(diff.TotalMinutes / minuteFrequency));

            if (index > 0 && index < 11)
                return index;
            else
                return -1;
        }

        private void SendMessages (PagedList<EventInvitee> invitees, Event.eMESSAGE_TYPE emt, Event evt, User CurrentUser)
        {
            if (invitees.Count > 0)
            {
                Community community = GetCommunityFacade.GetCommunity (evt.CommunityId);
                int evtGameId = community.WOK3App.GameId;
                string eventDetailsUrl = "http://" + KanevaGlobals.SiteName + "/mykaneva/events/eventDetail.aspx?event=" + evt.EventId;
                    
                Kaneva.BusinessLayer.BusinessObjects.Message message = new Kaneva.BusinessLayer.BusinessObjects.Message ();

                // Send The emails/kmails
                foreach (EventInvitee ei in invitees)
                {
                    string requestId = string.Empty;
                    string requestTypeSentIdPM = string.Empty;
                    string requestTypeSentIdEmail = string.Empty;

                    // Determine appropriate tracking metric and email type
                    switch (emt)
                    {
                        case Event.eMESSAGE_TYPE.EventInvite:
                            requestId = GetUserFacade.GetTrackingRequestId(requestId, Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_INVITE, CurrentUser.UserId, evtGameId, evt.CommunityId);
                            requestTypeSentIdPM = GetUserFacade.InsertTrackingTypeSent (requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_PM, ei.UserId);

                            if (ei.EmailNotifications && ei.UserStatusId.Equals ((int) Constants.eUSER_STATUS.REGVALIDATED))
                            {
                                requestTypeSentIdEmail = GetUserFacade.InsertTrackingTypeSent (requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_EMAIL, ei.UserId);

                                SendEventEmail (evt, Event.eMESSAGE_TYPE.EventInvite.ToString (), ei.Email, ei.UserId, CurrentUser.UserId, string.Empty, requestTypeSentIdEmail, eventDetailsUrl, "");
                            }
                            break;
                    }

                    if (KanevaGlobals.EnableEventKMails)
                    {
                        Event.EventMessage em = GetEventFacade.GetEventMessageDetails (emt, evt, CurrentUser.Username, eventDetailsUrl, string.Empty, requestTypeSentIdPM);

                        message = new Kaneva.BusinessLayer.BusinessObjects.Message (0, CurrentUser.UserId, ei.UserId, em.Subject, em.Message,
                            new DateTime (), 0, (int) Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                        // Send KMail
                        GetUserFacade.InsertMessage (message);
                    }
                }
            }
        }

        private void SendInvites (int eventId, bool includeFriends, int currentUserId)
        {
            Event evt = GetEventFacade.GetEvent (eventId);

            // Add the Invitees to the event_invites table
            GetEventFacade.InsertEventInvitees (evt.CommunityId, evt.UserId, evt.EventId,
                (int) EventInvitee.Invite_Status.INVITED, includeFriends);

            // Get members of world in which event is being held
            PagedList<EventInvitee> invitees = GetEventFacade.GetEventInviteesByStatus (evt.EventId,
                (int) EventInvitee.Invite_Status.INVITED, "", 1, int.MaxValue);

            // Send the mail/kmail invites
            SendMessages (invitees, Event.eMESSAGE_TYPE.EventInvite, evt, GetUserFacade.GetUser (currentUserId));
        }

        private void ProcessRecurringEvents ()
        {
            // Get recurring events that have already occurred and need to be rescheduled
            string filter = "recurring_status_id = " + (int) Event.eRECURRING_STATUS_TYPE.INVITES_SENT +
                " AND event_status_id = " + (int) Event.eSTATUS_TYPE.ACTIVE + 
                " AND end_time < NOW()";
            PagedList<Event> evt = GetEventFacade.GetEvents (filter, "", 1, int.MaxValue);

            foreach (Event e in evt)
            {
                try
                {
                    // Create a new recurring event 
                    Event new_evt = (Event) e.Clone ();
                    new_evt.EventId = 0;
                    new_evt.StartTime = e.StartTime.AddDays (7);
                    new_evt.EndTime = e.EndTime.AddDays (7);
                    new_evt.RecurringEventParentId = e.EventId;
                    new_evt.RecurringStatusId = (int) Event.eRECURRING_STATUS_TYPE.CREATED;

                    // Check for any concurrency errors
                    int ret = GetEventFacade.ConcurrencyCheckRecurring (new_evt.EventId, new_evt.UserId, new_evt.CommunityId, new_evt.StartTime, new_evt.EndTime, new_evt.IsRecurring, new_evt.RecurringEventParentId);

                    // If no conflicts, then add new event and update old one
                    if (ret == (int) Event.eCONCURRENCY_ERROR.NONE)
                    {
                        // Metrics tracking for accepts
                        Community community = GetCommunityFacade.GetCommunity (new_evt.CommunityId);
                        string requestId = GetUserFacade.InsertTrackingRequest (Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_INVITE, new_evt.UserId, 0, 0);
                        string requestTypeSentIdEvent = GetUserFacade.InsertTrackingTypeSent (requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_WEBSITE, community.WOK3App.GameId);

                        // If event is created successfully, then update the old event
                        int eventId = GetEventFacade.InsertEvent (new_evt, requestTypeSentIdEvent);
                        if (eventId > 0)
                        {
                            // Update status of old event to show that the recurring event has been created
                            e.RecurringStatusId = (int) Event.eRECURRING_STATUS_TYPE.NEXT_OCCURANCE_SCHEDULED;
                            GetEventFacade.UpdateEvent (e);

                            // Insert Event Created Blast
                            GetEventFacade.InsertEventBlast (eventId, new_evt.UserId, "created this event.");
                        }
                        else
                        {
                            m_logger.Error ("Could not create recurring event from event_id " + e.EventId);
                        }
                    }
                }
                catch (Exception exc)
                {
                    m_logger.Error ("Error in creating recurring event " + e.EventId, exc);
                }
            }

            // Get recurring events that have been created but invites have 
            // not been sent and need to be sent now
            // Get recurring events that have already occurred and need to be rescheduled
            filter = "recurring_status_id = " + (int) Event.eRECURRING_STATUS_TYPE.CREATED +
                " AND event_status_id = " + (int) Event.eSTATUS_TYPE.ACTIVE +
                " AND NOW() > DATE_ADD(start_time, INTERVAL -3 DAY) " +
                " AND start_time > NOW()";
            evt.Clear();
            evt = GetEventFacade.GetEvents (filter, "", 1, int.MaxValue);

            foreach (Event e in evt)
            {
                try
                {
                    // Send out the invites
                    SendInvites (e.EventId, e.InviteFriends, e.UserId);

                    e.RecurringStatusId = (int) Event.eRECURRING_STATUS_TYPE.INVITES_SENT;
                    GetEventFacade.UpdateEvent (e);
                }
                catch (Exception exc)
                {
                    m_logger.Error ("Error sending invites for recurring event " + e.EventId, exc);
                }
            }
        }

        private bool ProcessRandomEventInvites(Event evt, string fromUserName, Community evtCommunity, string stpUrl)
        {
            PagedList<EventInvitee> automatedEventInvitees = GetEventFacade.GetAutomatedEventInvitees("", 1, Int32.MaxValue);

            foreach (EventInvitee invitee in automatedEventInvitees)
            {
                // Send the clienttickler notifications
                try
                {
                    // Grab an event invitee object for this event. Should be set to NOT_INVITED if user hasn't already been invited to this event.
                    EventInvitee inviteeCheck = GetEventFacade.GetEventInvitee(invitee.UserId, evt.EventId);
                    
                    // Don't sent them an invite message if they're already attending the event or if they own it
                    if ((inviteeCheck.UserId <= 0 || inviteeCheck.InviteStatus == EventInvitee.Invite_Status.NOT_INVITED) && evt.UserId != invitee.UserId)
                    {
                        DataTable dtPlaceInfo = UsersUtility.GetCurrentPlaceInfo(invitee.UserId);

                        // Only send to users in WoK that are not already in the world.
                        if (dtPlaceInfo != null && dtPlaceInfo.Rows.Count > 0 && Convert.ToInt32(dtPlaceInfo.Rows[0]["community_id"]) != evt.CommunityId)
                        {
                            // Insert the invitee
                            GetEventFacade.InsertEventInvitee(evt.EventId, invitee.UserId, (int)EventInvitee.Invite_Status.INVITED);

                            // Generate the request type send id and send the email
                            string requestId = string.Empty;
                            requestId = GetUserFacade.GetTrackingRequestId(requestId, Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_RANDOM_EVENT_INVITE, evt.UserId, evtCommunity.WOK3App.GameId, evt.CommunityId);
                            string requestTypeSentIdInWorld = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_IN_WORLD, invitee.UserId);
                            GetUserFacade.SendEventReminderToPlayer(fromUserName, invitee.Username, evt.Title + " is starting now.<br><br>Earn 1000 Rewards a day for going<br>to an event.", evt.Title, stpUrl, requestTypeSentIdInWorld);
                        }
                        else if (dtPlaceInfo == null && UsersUtility.IsUserOnlineInKIM(invitee.UserId))
                        {
                            // Insert the invitee
                            GetEventFacade.InsertEventInvitee(evt.EventId, invitee.UserId, (int)EventInvitee.Invite_Status.INVITED);

                            // Generate the request type send id and send the kim invite
                            string requestId = string.Empty;
                            requestId = GetUserFacade.GetTrackingRequestId(requestId, Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_RANDOM_EVENT_INVITE, evt.UserId, evtCommunity.WOK3App.GameId, evt.CommunityId);
                            string requestTypeSentIdInKIM = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_KIM, invitee.UserId);

                            string kimMessage = evt.Title + " is starting now.<br/><br/>Earn 1000 Rewards a day for going to an event.";
                            string kimPlayWoKLink = GetKIMPlayWokUrl(evtCommunity, requestTypeSentIdInKIM);

                            GetUserFacade.SendKIMInviteToPlayer(fromUserName, invitee.Username, kimMessage, kimPlayWoKLink);
                        }
                    }
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in sending Events In World and KIM Reminders " + evt.EventId, exc);
                    return false;
                }
            }
            return true;
        }

        private string GetPlayWokUrl(Community evtCommunity)
        {
            string playWokUrl = string.Empty;

            if (evtCommunity.WOK3App.GameId > 0)
            {
                playWokUrl = "http://" + KanevaGlobals.SiteName + "/kgp/playwok.aspx?goto=U" + evtCommunity.WOK3App.GameId + "&ILC=MM3D&link=private";
            }
            else
            {
                playWokUrl = "http://" + KanevaGlobals.SiteName + "/kgp/playwok.aspx?goto=C" + evtCommunity.CommunityId + "&ILC=MM3D&link=private";
            }

            return playWokUrl;
        }

        private string GetKIMPlayWokUrl(Community evtCommunity, string requestTypeSentId)
        {
            string playWokUrl = string.Empty;

            if (evtCommunity.WOK3App.GameId > 0)
            {
                playWokUrl = @"<a href=""http://" + KanevaGlobals.SiteName + "/kgp/playwok.aspx?goto=U" + evtCommunity.WOK3App.GameId.ToString() +
                                       "&amp;ILC=MM3D&amp;link=private&amp;utm_medium=email&amp;utm_campaign=WorldSendRequest&amp;utm_source=viral&amp;utm_term=&amp;utm_content=1&amp;RTSID=" +
                                       requestTypeSentId + @"""><br/><br/><font color=""#3189fe"">CLICK HERE TO ACCEPT &amp; GO</font></a>";
            }
            else
            {
                playWokUrl = @"<a href=""http://" + KanevaGlobals.SiteName + "/kgp/playwok.aspx?goto=C" + evtCommunity.CommunityId.ToString() +
                                       "&amp;ILC=MM3D&amp;link=private&amp;utm_medium=email&amp;utm_campaign=WorldSendRequest&amp;utm_source=viral&amp;utm_term=&amp;utm_content=1&amp;RTSID=" +
                                       requestTypeSentId + @"""><br/><br/><font color=""#3189fe"">CLICK HERE TO ACCEPT &amp; GO</font></a>";
            }

            return playWokUrl;
        }

        private PagedDataTable GetUsersInEventWorld(Community evtCommunity)
        {
            PagedDataTable pdtUsers = null;

            if (evtCommunity.WOK3App.GameId > 0)
            {
                pdtUsers = UsersUtility.GetUsersInGame(evtCommunity.WOK3App.GameId);
            }
            else if (evtCommunity.CommunityTypeId == (int)CommunityType.HOME)
            {
                User evtCreator = GetUserFacade.GetUser(evtCommunity.CreatorId);
                WOK3DPlace zone = GetCommunityFacade.Get3DPlace(evtCreator.WokPlayerId, Constants.APARTMENT_ZONE);
                pdtUsers = UsersUtility.GetUsersInZone(0, zone.ZoneInstanceId, zone.ZoneIndex, "p.name desc", 1, Int32.MaxValue, null);
            }
            else
            {
                WOK3DPlace zone = GetCommunityFacade.Get3DPlace(evtCommunity.CommunityId, Constants.BROADBAND_ZONE);
                pdtUsers = UsersUtility.GetUsersInZone(0, zone.ZoneInstanceId, zone.ZoneIndex, "p.name desc", 1, Int32.MaxValue, null);
            }

            return pdtUsers;
        }

        private string GetSTPUrl(Community evtCommunity)
        {
            string stpUrl = StpUrl.MakeUrlPrefix(KanevaGlobals.WokGameId.ToString());

            if (evtCommunity.WOK3App.GameId > 0)
            {
                stpUrl = StpUrl.MakeUrlPrefix(evtCommunity.WOK3App.GameId.ToString());
            }
            else
            {
                stpUrl += "/" + StpUrl.MakeCommunityUrlPath(evtCommunity.Name);
            }

            return stpUrl;
        }

        #endregion Helper Functions

        #region Properties

        /// <summary>
        /// Gets the EventFacade instance.
        /// </summary>
        private EventFacade GetEventFacade
        {
            get
            {
                if (eventFacade == null)
                {
                    eventFacade = new EventFacade();
                }
                return eventFacade;
            }
        }

        /// <summary>
        /// Gets the UserFacadeInstance.
        /// </summary>
        private UserFacade GetUserFacade
        {
            get
            {
                if (userFacade == null)
                {
                    userFacade = new UserFacade();
                }
                return userFacade;
            }
        }

        /// <summary>
        /// Gets the CommunityFacade Instance.
        /// </summary>
        private CommunityFacade GetCommunityFacade
        {
            get
            {
                if (communityFacade == null)
                {
                    communityFacade = new CommunityFacade();
                }
                return communityFacade;
            }
        }

        /// <summary>
        /// Gets the GameFacade Instance.
        /// </summary>
        private GameFacade GetGameFacade
        {
            get
            {
                if (gameFacade == null)
                {
                    gameFacade = new GameFacade();
                }
                return gameFacade;
            }
        }

        #endregion Properties
    }
}
