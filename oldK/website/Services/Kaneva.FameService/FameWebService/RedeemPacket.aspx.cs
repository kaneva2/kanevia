///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Xml;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace FameWebService
{
    public partial class RedeemPacket : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if ((Request.Params["userId"] == null) || (Request.Params["packetId"] == null) || (Request.Params["fameTypeId"] == null))
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>userId or packetId or fameTypeId not specified</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }

            int userId = Int32.Parse(Request.Params["userId"].ToString());
            int packetId = Int32.Parse(Request.Params["packetId"].ToString());
            int fameTypeId = Int32.Parse(Request.Params["fameTypeId"].ToString());

            FameFacade fameFacade = new FameFacade();

            //// Reject all Packets from web besides one time packets
            //Packet packet = fameFacade.GetPacket(packetId);
            //if (packet.TimeInterval.Equals((int)PacketTimeInterval.Daily))
            //{
            //    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>Daily packets not valid</ResultDescription>\r\n</Result>";
            //    Response.Write(errorStr);
            //    return;
            //}

            int ret = fameFacade.RedeemPacket(userId, packetId, fameTypeId);

            XmlDocument doc = new XmlDocument();
            XmlNode root = doc.CreateNode(XmlNodeType.Element, "Result", "");

            XmlElement elem = doc.CreateElement("ReturnCode");

            elem.InnerText = ret.ToString();

            root.InsertBefore(elem, root.FirstChild);

            elem = doc.CreateElement("ReturnDescription");
            elem.InnerText = ret.ToString();

            root.InsertBefore(elem, root.FirstChild);
            Response.Write(root.OuterXml);
        }

    }
}
