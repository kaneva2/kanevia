///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace FameWebService
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>

    [WebService(Namespace = "http://www.kaneva.com/webservices/Fame/", Description = "Kaneva Fame Web Service.")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class FameService : System.Web.Services.WebService
    {
        [WebMethod]
        public int RedeemPacket(int userId, int packetId, int fameTypeId)
        {
            FameFacade fameFacade = new FameFacade();

            //// Reject all Daily Packets from web
            //Packet packet = fameFacade.GetPacket(packetId);
            //if (packet.TimeInterval.Equals((int)PacketTimeInterval.Daily))
            //{               
            //    return -101;
            //}

            return fameFacade.RedeemPacket(userId, packetId, fameTypeId);
        }


               /// <summary>
       /// Gets user's fame
       /// </summary>
       [WebMethod]
       public UserFame GetUserFame(int userId, int fameTypeId)
       {
            FameFacade fameFacade = new FameFacade();
            return fameFacade.GetUserFame(userId, fameTypeId);
        }
 
        /// <summary>
       /// Gets a list of users
       /// </summary>
       [WebMethod]
       public PagedList<UserFame> GetAllUserFame(int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
                  {
            FameFacade fameFacade = new FameFacade();
            return fameFacade.GetUserFame(fameTypeId, filter, orderby, pageNumber, pageSize);
        }
 
        /// <summary>
       /// Gets a Packet
       /// </summary>
       [WebMethod]
       public Packet GetPacket(int packetId)
                  {
            FameFacade fameFacade = new FameFacade();
            return fameFacade.GetPacket(packetId);
        }
 
        /// <summary>
       /// Gets a list of packets
       /// </summary>
       [WebMethod]
        public PagedList<Packet> GetPackets(int fameTypeId, string filter, string orderby, string groupby, int pageNumber, int pageSize)
                  {
            FameFacade fameFacade = new FameFacade();
            return fameFacade.GetPackets(fameTypeId, filter, orderby, groupby, pageNumber, pageSize);
        }
 
        /// <summary>
       /// Gets a Level
       /// </summary>
       [WebMethod]
        public Level GetLevel(int levelId)
                  {
            FameFacade fameFacade = new FameFacade();
            return fameFacade.GetLevel(levelId);
        }
 
 
        /// <summary>
       /// Gets a list of levels
       /// </summary>
       [WebMethod]
        public PagedList<Level> GetLevels(int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
                  {
            FameFacade fameFacade = new FameFacade();
            return fameFacade.GetLevels(fameTypeId, filter, orderby, pageNumber, pageSize);
        }
 
        /// <summary>
       /// Gets a list of users packet history
       /// </summary>
       [WebMethod]
        public PagedList<PacketHistory> GetPacketHistory(int userId, int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
                  {
            FameFacade fameFacade = new FameFacade();
            return fameFacade.GetPacketHistory(userId, fameTypeId, filter, orderby, pageNumber, pageSize);
        }
 
        /// <summary>
       /// Gets a list of users
       /// </summary>
       [WebMethod]
        public PagedList<LevelHistory> GetLevelHistory(int userId, int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
                  {
            FameFacade fameFacade = new FameFacade();
            return fameFacade.GetLevelHistory(userId, fameTypeId, filter, orderby, pageNumber, pageSize);
        }
 
        /// <summary>
       /// IsPacketRedeemed
       /// </summary>
       [WebMethod]
       public bool IsOneTimePacketRedeemed (int userId, int packetId)
                  {
            FameFacade fameFacade = new FameFacade();
            return fameFacade.IsOneTimePacketRedeemed(userId, packetId);
        }
 
        /// <summary>
       /// IsUserAtMaxRedemptions
       /// </summary>
       [WebMethod]
       public bool IsUserAtMaxRedemptions(int userId, int packetId, int fameTypeId, int maxPerDay)
                  {
            FameFacade fameFacade = new FameFacade();
            return fameFacade.IsUserAtMaxRedemptions(userId, packetId, fameTypeId, maxPerDay);
        }
 
        /// <summary>
       /// Gets a list of nudges belong to a checklist
       /// </summary>
       [WebMethod]
        public PagedList<Nudge> GetChecklist (int userId, int checklistId, string filter, string orderby, int pageNumber, int pageSize)
                  {
            FameFacade fameFacade = new FameFacade();
            return fameFacade.GetChecklist(userId, checklistId, filter, orderby, pageNumber, pageSize);
        }
 
 
        /// <summary>
       /// Gets a list of nudges user has not completed
       /// </summary>
       [WebMethod]
        public PagedList<Nudge> GetNudges (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            FameFacade fameFacade = new FameFacade();
            return fameFacade.GetNudges(userId, filter, orderby, pageNumber, pageSize);
        }
 
        /// <summary>
       /// IsPacketRedeemed
       /// </summary>
       [WebMethod]
       public bool IsChecklistComplete (int userId, int checklistId)
        {
            FameFacade fameFacade = new FameFacade();
            return fameFacade.IsChecklistComplete(userId, checklistId);
        }








    }
}
