///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.IO;
using log4net;

namespace FameDaily
{
    public partial class FameDaily : Form
    {
        public FameDaily()
        {
            InitializeComponent();
        }

        private void RunDailyService()
        {
            // Set up log for net
            string s = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
            FileInfo l_fi = new FileInfo(s);
            log4net.Config.DOMConfigurator.Configure(l_fi);

            ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            FameFacade fameFacade = new FameFacade();

            m_logger.Debug("Starting Fame Daily Service");
            List<User> iUser;

            // Creator fame 
            try
            {
                GameFacade gameFacade = new GameFacade();

                // Web last Logged in
                DataTable dtVisitCounts = gameFacade.GetUnique3DAppVisits(1, Int32.MaxValue);
                int gameId = 0;
                int visitCountUnique = 0;
                int fameResult = 0;

                for (int i = 0; i < dtVisitCounts.Rows.Count; i++)
                {

                    try
                    {
                        gameId = Convert.ToInt32(dtVisitCounts.Rows[i]["game_id"]);
                        visitCountUnique = Convert.ToInt32(dtVisitCounts.Rows[i]["visit_count_unique"]);

                        Game game = gameFacade.GetGameByGameId(gameId);

                        if (visitCountUnique > 9999)
                        {
                            fameResult = fameFacade.RedeemPacket(game.OwnerId, 5019, (int)FameTypes.Creator);
                        }

                        // -2 is already redeemed, if they already redeemed a higher one, dont even attempt a lower one
                        if (fameResult != -2 && visitCountUnique > 4999)
                        {
                            fameResult = fameFacade.RedeemPacket(game.OwnerId, 5014, (int)FameTypes.Creator);
                        }

                        if (fameResult != -2 && visitCountUnique > 999)
                        {
                            fameResult = fameFacade.RedeemPacket(game.OwnerId, 5013, (int)FameTypes.Creator);
                        }

                        if (fameResult != -2 && visitCountUnique > 499)
                        {
                            fameResult = fameFacade.RedeemPacket(game.OwnerId, 5012, (int)FameTypes.Creator);
                        }

                        if (fameResult != -2 && visitCountUnique > 99)
                        {
                            fameResult = fameFacade.RedeemPacket(game.OwnerId, 5011, (int)FameTypes.Creator);
                        }

                        if (fameResult != -2 && visitCountUnique > 1)
                        {
                            fameResult = fameFacade.RedeemPacket(game.OwnerId, 5010, (int)FameTypes.Creator);
                        }
                    }
                    catch (Exception exc)
                    {
                        m_logger.Error("Error in daily creator uniquie app visits for gameId " + gameId, exc);
                    }


                }

            }
            catch (Exception exc)
            {
                m_logger.Error("Error in creator fame centry marks", exc);
            }


            /*Moving Daily Login and Daily Wok Login to other sections
            try
            {

                // Web last Logged in
                iUser = fameFacade.GetYesterdayWebLogins();

                foreach (User user in iUser)
                {
                    try
                    {
                        userId = user.UserId;
                        count++;
                        fameFacade.RedeemPacket(user.UserId, (int)PacketId.WEB_DAILY_SIGNIN, (int)FameTypes.World);
                    }
                    catch (Exception exc)
                    {
                        m_logger.Error("Error in daily fame web logins for user " + user.UserId, exc);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error in daily fame web logins", exc);
            }

            try
            {
                // Yesterday wok login
                iUser = fameFacade.GetYesterdayWOKLogins();

                foreach (User user in iUser)
                {
                    try
                    {
                        fameFacade.RedeemPacket(user.UserId, (int)PacketId.WORLD_DAILY_SIGNIN, (int)FameTypes.World);
                    }
                    catch (Exception exc)
                    {
                        m_logger.Error("Error in daily fame WOK logins for user " + user.UserId, exc);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error in daily fame WOK logins", exc);
            }
            */
            try
            {

                // Yesterday 10 mins in world
                iUser = fameFacade.GetYesterday10MinsInWorld();

                foreach (User user in iUser)
                {
                    try
                    {
                        fameFacade.RedeemPacket(user.UserId, (int)PacketId.WORLD_DAILY_10_MINS, (int)FameTypes.World);
                    }
                    catch (Exception exc)
                    {
                        m_logger.Error("Error in daily fame 10 mins in world for user " + user.UserId, exc);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error in daily fame 10 mins in world", exc);
            }

            try
            {
                // Yesterday 3 hours in world
                iUser = fameFacade.GetYesterday3HoursInWorld();

                foreach (User user in iUser)
                {
                    try
                    {
                        fameFacade.RedeemPacket(user.UserId, (int)PacketId.WORLD_DAILY_3_HOURS, (int)FameTypes.World);
                    }
                    catch (Exception exc)
                    {
                        m_logger.Error("Error in daily fame in world 3 hours for user " + user.UserId, exc);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error in daily fame in world 3 hours", exc);
            }

            try
            {
                // Yesterday Dance level 10
                iUser = fameFacade.GetYesterdayDanceLevel10();

                foreach (User user in iUser)
                {
                    try
                    {
                        fameFacade.RedeemPacket(user.UserId, (int)PacketId.WORLD_DAILY_DANCE_10, (int)FameTypes.World);
                    }
                    catch (Exception exc)
                    {
                        m_logger.Error("Error in daily fame dance level 10 for user " + user.UserId, exc);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error in daily fame dance level 10", exc);
            }

            //try
            //{
            //    // Yesterday Visit Forum
            //    iUser = fameFacade.GetYesterdayVisitForum();

            //    foreach (User user in iUser)
            //    {
            //        try
            //        {
            //            fameFacade.RedeemPacket(user.UserId, (int)PacketId.WEB_DAILY_VISIT_FORUM, (int)FameTypes.World);
            //        }
            //        catch (Exception exc)
            //        {
            //            m_logger.Error("Error in daily fame visit forum for user " + user.UserId, exc);
            //        }
            //    }
            //}
            //catch (Exception exc)
            //{
            //    m_logger.Error("Error in daily fame visit forum", exc);
            //}

            //try
            //{
            //    // Yesterday Visit Forum
            //    iUser = fameFacade.GetYesterdayRead3Posts();

            //    foreach (User user in iUser)
            //    {
            //        try
            //        {
            //            fameFacade.RedeemPacket(user.UserId, (int)PacketId.WEB_DAILY_FORUM_POSTS_3, (int)FameTypes.World);
            //        }
            //        catch (Exception exc)
            //        {
            //            m_logger.Error("Error in daily fame Read 3 posts for user " + user.UserId, exc);
            //        }

            //    }
            //}
            //catch (Exception exc)
            //{
            //    m_logger.Error("Error in daily fame Read 3 posts", exc);
            //}

            try
            {
                // Yesterday view 3 Media
                iUser = fameFacade.GetYesterdayView3Media();

                foreach (User user in iUser)
                {
                    if (user.UserId > 0)
                    {
                        try
                        {
                            fameFacade.RedeemPacket(user.UserId, (int)PacketId.WEB_DAILY_MEDIA_VIEWS_3, (int)FameTypes.World);
                        }
                        catch (Exception exc)
                        {
                            m_logger.Error("Error in daily fame View 3 Media for user " + user.UserId, exc);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error in daily fame View 3 Media", exc);
            }


            m_logger.Debug("End Fame Daily Service");

        }

        private void FameDaily_Load(object sender, EventArgs e)
        {
            RunDailyService();

            this.Dispose();
            this.Close();
        }

    }
}
