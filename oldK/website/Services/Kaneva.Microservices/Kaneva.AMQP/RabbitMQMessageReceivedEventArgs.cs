///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿#region Includes

using RabbitMQ.Client.Events;

#endregion

namespace Kaneva.AMQP
{
    public class RabbitMQMessageReceivedEventArgs : AMQPMessageReceivedEventArgs
    {
        internal BasicDeliverEventArgs EventArgs { get; set; }
        public override string CorrelationId
        {
            get { return (EventArgs != null ? EventArgs.BasicProperties.CorrelationId : null); }
        }
    }
}