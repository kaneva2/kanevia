///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿#region Includes

using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

#endregion

namespace Kaneva.AMQP
{
    public class RabbitMQAdapter : AMQPAdapter
    {
        private IConnection _connection;

        public RabbitMQAdapter(string hostName, int port, string userName, string password, ushort heartbeat)
            : base(hostName, port, userName, password, heartbeat)
        { }

        public RabbitMQAdapter(string hostName, string virtualHost, int port, string userName, string password, ushort heartbeat)
            : base(hostName, virtualHost, port, userName, password, heartbeat)
        { }

        public override bool IsConnected
        {
            get { return _connection != null && _connection.IsOpen; }
        }

        public override void Connect()
        {
            var connectionFactory = new ConnectionFactory
            {
                HostName = _hostName,
                Port = _port,
                UserName = _userName,
                Password = _password,
                RequestedHeartbeat = _heartbeat
            };

            if (!string.IsNullOrEmpty(_virtualHost)) connectionFactory.VirtualHost = _virtualHost;
            _connection = connectionFactory.CreateConnection();
        }

        public override void Disconnect()
        {
            if (_connection != null) _connection.Dispose();
        }

        public override object GetConnection()
        {
            return _connection;
        }

        public override void Publish(string message, string queueName, bool createQueue = true,
            IDictionary<string, object> queueArgs = null)
        {
            if (!IsConnected) Connect();
            using (var channel = _connection.CreateModel())
            {
                if (createQueue) channel.QueueDeclare(queueName, true, false, false, queueArgs);
                var payload = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(string.Empty, queueName,
                    RabbitMQProperties.CreateDefaultProperties(channel), payload);
            }
        }

        public override void Publish(string message, string exchangeName, string routingKey)
        {
            if (!IsConnected) Connect();
            using (var channel = _connection.CreateModel())
            {
                var payload = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchangeName, routingKey,
                    RabbitMQProperties.CreateDefaultProperties(channel), payload);
            }
        }

        public override bool TryGetNextMessage(string queueName, out string message, int timeout, bool createQueue = true, 
            bool noAck = false, bool implicitAck = true, IDictionary<string, object> queueArgs = null)
        {
            if (!IsConnected) Connect();
            using (var channel = _connection.CreateModel())
            {
                if (createQueue) channel.QueueDeclare(queueName, true, false, false, queueArgs);
                channel.BasicQos(0, 1, false);

                var consumer = new QueueingBasicConsumer(channel);
                channel.BasicConsume(queueName, noAck, consumer);

                BasicDeliverEventArgs eventArgs;
                var messageIsAvailable = consumer.Queue.Dequeue(timeout * 1000, out eventArgs);

                if (messageIsAvailable)
                {
                    message = Encoding.UTF8.GetString(eventArgs.Body);
                    if (implicitAck && !noAck) channel.BasicAck(eventArgs.DeliveryTag, false);
                    return true;
                }

                message = null;
                return false;
            }
        }

        public override void AcknowledgeMessage(ulong deliveryTag)
        {
            if (!IsConnected) Connect();
            using (var channel = _connection.CreateModel())
                channel.BasicAck(deliveryTag, false);
        }
    }
}