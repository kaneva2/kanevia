///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿#region Includes

using System.Collections.Generic;

#endregion

namespace Kaneva.AMQP
{
    public abstract class AMQPConsumerFactory
    {
        public abstract AMQPConsumer CreateAMQPConsumer(string queueName, int timeout, bool catchAll, ushort prefetchCount = 1, bool noAck = false,
            bool createQueue = true, bool implicitAck = true, IDictionary<string, object> queueArgs = null);
    }
}