///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using Kaneva.AMQP;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Kaneva.AMQP
{
    public abstract class Microservice
    {
        protected AMQPAdapter _adapter;
        protected AMQPConsumerFactory _consumerFactory;
        protected readonly List<AMQPConsumer> _consumers = new List<AMQPConsumer>();
        protected ILog _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public virtual void Init()
        {
            // Configure logging.
            var s = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
            var l_fi = new FileInfo(s);
            log4net.Config.XmlConfigurator.Configure(l_fi);
        }

        public void Shutdown()
        {
            _logger.Info("Shutting down service...");

            if (_adapter == null) return;

            if (_consumers != null && _consumers.Count > 0)
            {
                foreach (var consumer in _consumers)
                {
                    _adapter.StopConsumingAsync(consumer);
                }
            }

            // Make sure we're all shut down before cleanup.
            foreach (var consumer in _consumers)
            {
                while (!consumer.ShutdownComplete)
                    Thread.Sleep(1000);
            }
            _consumers.Clear();

            _adapter.Disconnect();

            _logger.Info("Shutdown Complete!");
        }
    }
}
