///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿#region Includes

using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Threading;

#endregion

namespace Kaneva.AMQP
{
    public abstract class RabbitMQConsumer : AMQPConsumer
    {
        protected RabbitMQConsumer(string queueName, int timeout, ushort prefetchCount = 1, bool noAck = false,
            bool createQueue = true, bool implicitAck = true, IDictionary<string, object> queueArgs = null)
            : base(queueName, timeout, prefetchCount, noAck, createQueue, implicitAck, queueArgs)
        { }

        public void Start(AMQPAdapter amqpAdapter, bool catchAllExceptions)
        {
            base.Start(amqpAdapter);
            try
            {
                var connection = (IConnection)amqpAdapter.GetConnection();

                using (var channel = connection.CreateModel())
                {
                    if (_createQueue) channel.QueueDeclare(_queueName, true, false, false, _queueArgs);
                    channel.BasicQos(0, _prefetchCount, false);

                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (model, ea) =>
                    {
                        try
                        {
                            var payload = ea.Body;
                            var message = Encoding.UTF8.GetString(payload);
                            OnMessageReceived(new RabbitMQMessageReceivedEventArgs
                            {
                                Message = message,
                                EventArgs = ea
                            });

                            if (_implicitAck && !_noAck) channel.BasicAck(ea.DeliveryTag, false);
                        }
                        catch (Exception exception)
                        {
                            OnMessageReceived(new RabbitMQMessageReceivedEventArgs
                            {
                                Exception = new AMQPConsumerProcessingException(exception)
                            });

                            if (_implicitAck && !_noAck) channel.BasicAck(ea.DeliveryTag, false);
                            if (!catchAllExceptions) Stop();
                        }
                    };
                    channel.BasicConsume(_queueName, _noAck, consumer);

                    while (!_stopConsuming)
                    {
                        Thread.Sleep(10000);
                    }
                }
            }
            catch (Exception exception)
            {
                OnMessageReceived(new RabbitMQMessageReceivedEventArgs
                {
                    Exception = new AMQPConsumerInitialisationException(exception)
                });
            }

            // Indicate that we're free to shutdown.
            _shutdownComplete = true;
        }
    }
}