///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#region Includes

using System.Collections.Generic;

#endregion

namespace Kaneva.AMQP
{
    public class RabbitMQConsumerFactory : AMQPConsumerFactory
    {
        public override AMQPConsumer CreateAMQPConsumer(string queueName, int timeout, bool catchAll, ushort prefetchCount = 1, bool noAck = false,
            bool createQueue = true, bool implicitAck = true, IDictionary<string, object> queueArgs = null)
        {
            if (catchAll)
                return new RabbitMQConsumerCatchAll(queueName, timeout, prefetchCount, noAck, createQueue, implicitAck, queueArgs);
            else
                return new RabbitMQConsumerCatchOne(queueName, timeout, prefetchCount, noAck, createQueue, implicitAck, queueArgs);
        }
    }
}