///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿#region Includes

using System.Collections.Generic;

#endregion

namespace Kaneva.AMQP
{
    public class RabbitMQConsumerCatchAll : RabbitMQConsumer
    {
        public RabbitMQConsumerCatchAll(string queueName, int timeout, ushort prefetchCount = 1, bool noAck = false,
            bool createQueue = true, bool implicitAck = true, IDictionary<string, object> queueArgs = null) :
                base(queueName, timeout, prefetchCount, noAck, createQueue, implicitAck, queueArgs)
        { }

        public override void Start(AMQPAdapter amqpAdapter)
        {
            base.Start(amqpAdapter, true);
        }
    }
}