///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿#region Includes

using System;
using System.Collections.Generic;

#endregion

namespace Kaneva.AMQP
{
    public abstract class AMQPConsumer
    {
        protected readonly string _queueName;
        protected readonly ushort _prefetchCount;
        protected readonly bool _noAck;
        protected readonly bool _createQueue;
        protected readonly int _timeout;
        protected readonly bool _implicitAck;
        protected readonly IDictionary<string, object> _queueArgs;
        protected volatile bool _stopConsuming;
        protected volatile bool _shutdownComplete;

        public event EventHandler<AMQPMessageReceivedEventArgs> MessageReceived;

        protected AMQPConsumer(string queueName, int timeout, ushort prefetchCount = 1, bool noAck = false,
            bool createQueue = true, bool implicitAck = true, IDictionary<string, object> queueArgs = null)
        {
            _queueName = queueName;
            _prefetchCount = prefetchCount;
            _noAck = noAck;
            _createQueue = createQueue;
            _timeout = timeout;
            _implicitAck = implicitAck;
            _queueArgs = queueArgs;
        }

        public virtual void Start(AMQPAdapter amqpAdapter)
        {
            _stopConsuming = false;
        }

        public void Stop()
        {
            _stopConsuming = true;
        }

        protected void OnMessageReceived(AMQPMessageReceivedEventArgs e)
        {
            MessageReceived?.Invoke(this, e);
        }

        public bool ShutdownComplete { get { return _shutdownComplete; } }
    }
}