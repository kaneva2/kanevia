///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿namespace Kaneva.AMQP {
    public class AMQPQueue
    {
        public string Name { get; set; }
        public bool IsNew { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}