///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿#region Includes

using System;
using System.Collections.Generic;
using System.Threading;

#endregion

namespace Kaneva.AMQP
{
    public abstract class AMQPAdapter : IDisposable
    {
        protected string _hostName;
        protected string _virtualHost;
        protected int _port;
        protected string _userName;
        protected string _password;
        protected ushort _heartbeat;

        protected AMQPAdapter(string hostName, int port, string userName, string password, ushort heartbeat)
        {
            _hostName = hostName;
            _port = port;
            _userName = userName;
            _password = password;
            _heartbeat = heartbeat;
        }

        protected AMQPAdapter(string hostName, string virtualHost, int port, string userName, string password, ushort heartbeat)
        {
            _hostName = hostName;
            _virtualHost = virtualHost;
            _port = port;
            _userName = userName;
            _password = password;
            _heartbeat = heartbeat;
        }

        public abstract bool IsConnected { get; }

        public abstract void Connect();

        public abstract void Disconnect();

        public abstract object GetConnection();

        public abstract void Publish(string message, string queueName, bool createQueue = true,
            IDictionary<string, object> queueArgs = null);

        public abstract void Publish(string message, string exchangeName, string routingKey);

        public abstract bool TryGetNextMessage(string queueName, out string message, int timeout, bool createQueue = true, 
            bool noAck = false, bool implicitAck = true, IDictionary<string, object> queueArgs = null);

        public abstract void AcknowledgeMessage(ulong deliveryTag);

        public void ConsumeAsync(AMQPConsumer consumer)
        {
            if (!IsConnected) Connect();

            var thread = new Thread(o => consumer.Start(this));
            thread.Start();

            while (!thread.IsAlive)
                Thread.Sleep(1);
        }

        public void StopConsumingAsync(AMQPConsumer consumer)
        {
            consumer.Stop();
        }

        void IDisposable.Dispose()
        {
            Disconnect();
        }
    }
}