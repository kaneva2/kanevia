///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#region Includes

using System;

#endregion

namespace Kaneva.AMQP
{
    [Serializable]
    public class AMQPConsumerProcessingException : Exception
    {
        public AMQPConsumerProcessingException(Exception innerException) :
            base("An Exception occured while consuming the Queue.", innerException)
        { }
    }
}