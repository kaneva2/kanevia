///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿namespace Kaneva.Microservices.WorldCreation
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.WorldCreationServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.WorldCreationMicroserviceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // WorldCreationServiceProcessInstaller
            // 
            this.WorldCreationServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.NetworkService;
            this.WorldCreationServiceProcessInstaller.Password = null;
            this.WorldCreationServiceProcessInstaller.Username = null;
            // 
            // WorldCreationMicroserviceInstaller
            // 
            this.WorldCreationMicroserviceInstaller.ServiceName = "Kaneva.WorldCreationMicroservice";
            this.WorldCreationMicroserviceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.WorldCreationServiceProcessInstaller,
            this.WorldCreationMicroserviceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller WorldCreationServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller WorldCreationMicroserviceInstaller;
    }
}