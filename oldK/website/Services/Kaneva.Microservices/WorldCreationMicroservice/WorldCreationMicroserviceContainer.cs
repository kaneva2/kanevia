///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿#region Includes

using System.ServiceProcess;

#endregion

namespace Kaneva.Microservices.WorldCreation
{
    partial class WorldCreationMicroserviceContainer : ServiceBase
    {
        private WorldCreationMicroservice _worldCreationMicroservice;

        public WorldCreationMicroserviceContainer()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _worldCreationMicroservice = new WorldCreationMicroservice();
            _worldCreationMicroservice.Init();
        }

        protected override void OnStop()
        {
            _worldCreationMicroservice.Shutdown();
        }
    }
}
