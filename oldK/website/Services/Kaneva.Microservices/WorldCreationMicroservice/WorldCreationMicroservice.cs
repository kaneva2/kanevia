///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿#region Includes

using System;
using Kaneva.AMQP;
using Newtonsoft.Json;
using Kaneva.BusinessLayer.BusinessObjects;
using KanevaFacade = Kaneva.BusinessLayer.Facade;
using System.Net;

#endregion

namespace Kaneva.Microservices.WorldCreation
{
    public class WorldCreationMicroservice : Microservice
    {
        private const string WORLD_CREATION_QUEUE = "world_creation";
        private const string SCALE_QUEUE = "scale_world_creation_service";

        public override void Init()
        {
            base.Init();

            _logger.Info("Starting World Creation Service...");

            _adapter = new RabbitMQAdapter(KanevaFacade.Configuration.RabbitMQHost, KanevaFacade.Configuration.RabbitMQVHost, 
                KanevaFacade.Configuration.RabbitMQPort, KanevaFacade.Configuration.RabbitMQUsername, KanevaFacade.Configuration.RabbitMQPassword,
                KanevaFacade.Configuration.RabbitMQHeartbeat);

            _consumerFactory = new RabbitMQConsumerFactory();

            _logger.Info("Creating worker threads...");
            var initialWorkerCount = int.Parse(System.Configuration.ConfigurationManager.AppSettings["WorkerCount"] ?? "1");
            for (var i = 0; i < initialWorkerCount; i++)
            {
                var workerConsumer = _consumerFactory.CreateAMQPConsumer(WORLD_CREATION_QUEUE, 10, true);
                workerConsumer.MessageReceived += CreateWorldMessageReceived;
                _consumers.Add(workerConsumer);
            }
            _logger.Info(initialWorkerCount + " workers created!");

            _logger.Info("Creating scaling thread...");
            var scaleConsumer = _consumerFactory.CreateAMQPConsumer(SCALE_QUEUE, 10, true);
            scaleConsumer.MessageReceived += ScalingMessageReceived;
            _consumers.Add(scaleConsumer);
            _logger.Info("Scaling thread created!");

            _logger.Info("Connecting to RabbitMQ host " + KanevaFacade.Configuration.RabbitMQHost + "...");
            _adapter.Connect();
            _logger.Info("Connected to RabbitMQ!");

            _logger.Info("Starting consumers...");
            foreach (var consumer in _consumers)
            {
                _adapter.ConsumeAsync(consumer);
            }
            _logger.Info("Consumer threads ready!");

            _logger.Info("World Creation Service ready!");
        }

        public void CreateWorldMessageReceived(object sender, AMQPMessageReceivedEventArgs e)
        {
            var gameFacade = new KanevaFacade.GameFacade();
            var userFacade = new KanevaFacade.UserFacade();

            if (e.Exception == null)
            {
                string result;
                string resultDesc;

                try
                {
                    _logger.Info("Creating world from message: " + e.Message);

                    var cwMessage = JsonConvert.DeserializeObject<CreateWorldMessage>(e.Message);

                    var user = userFacade.GetUser(cwMessage.UserId);
                    var urlSafeName = WebUtility.UrlEncode(cwMessage.WorldName);
                    var urlSafeDescription = WebUtility.UrlEncode(cwMessage.WorldDescription);

                    int errorCode;
                    string errorMsg;

                    var template = gameFacade.GetWorldTemplate(cwMessage.TemplateId);
                    var communityId = gameFacade.CreateWorldFromTemplate(template, cwMessage.WorldName, urlSafeName,
                        cwMessage.WorldDescription, urlSafeDescription, user, userFacade.IsUserGM(cwMessage.UserId),
                        false, KanevaFacade.Configuration.WokGameName, out errorCode, out errorMsg);

                    if (communityId > 0)
                    {
                        // Set parent id if this is a child zone
                        if (cwMessage.ParentId > 0)
                        {
                            (new KanevaFacade.CommunityFacade()).UpdateCommunityParentId(communityId, cwMessage.ParentId);
                        }

                        // Build the world url
                        var stpUrl = KlausEnt.KEP.Kaneva.StpUrl.MakeUrlPrefix(KanevaFacade.Configuration.WokGameId.ToString());
                        stpUrl += "/" + KlausEnt.KEP.Kaneva.StpUrl.MakeCommunityUrlPath(cwMessage.WorldName);

                        result = "Success";
                        resultDesc = stpUrl;
                        _logger.Info("Successfully created world from message: " + e.Message + "!");
                    }
                    else
                    {
                        result = "Error";
                        resultDesc = errorMsg;
                        _logger.Warn("Failed to create world from message: " + e.Message + "!\nError: " + errorMsg);
                    }
                }
                catch (Exception ex)
                {
                    result = "Error";
                    resultDesc = ex.Message;
                    _logger.Error("Failed to create world from message: " + e.Message + "!\nError: " + ex.Message);
                }

                try
                {
                    if (!string.IsNullOrWhiteSpace(e.CorrelationId))
                        (new KanevaFacade.Common()).SetAsyncTaskCompleteKey(e.CorrelationId, result, resultDesc ?? string.Empty);
                    else
                        _logger.Error("No correlation id found for message " + e.Message + "!");
                }
                catch (Exception ex)
                {
                    _logger.Warn("Unable to set completion key in memcached: " + ex.Message);
                }
            }
        }

        private void ScalingMessageReceived(object sender, AMQPMessageReceivedEventArgs e)
        {
            if (e.Message.Contains("scale-out"))
            {
                _logger.Info("Scaling out World Creation Service...");

                var consumer = _consumerFactory.CreateAMQPConsumer(WORLD_CREATION_QUEUE, 10, true);
                consumer.MessageReceived += CreateWorldMessageReceived;
                _adapter.ConsumeAsync(consumer);
                _consumers.Add(consumer);

                _logger.Info("Scaling complete! Worker count: " + (_consumers.Count - 1));
            }
            else
            {
                _logger.Info("Scaling back World Creation Service...");

                if (_consumers.Count <= 1) return;
                var lastConsumer = _consumers[_consumers.Count - 1];

                _adapter.StopConsumingAsync(lastConsumer);
                _consumers.RemoveAt(_consumers.Count - 1);

                _logger.Info("Scaling complete! Worker count: " + (_consumers.Count - 1));
            }
        }
    }
}