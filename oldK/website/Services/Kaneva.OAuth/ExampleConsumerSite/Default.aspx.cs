///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using Kaneva.OAuth.Consumer;
using Kaneva.OAuth.Framework;

namespace ExampleConsumerSite
{
  public partial class _Default : OAuthPage
  {
    protected void oauthRequest_Click(object sender, EventArgs e)
    {
      var session = CreateSession();

      IToken requestToken = session.GetRequestToken();

      if (string.IsNullOrEmpty(requestToken.Token))
      {
        throw new Exception("The request token was null or empty");
      }

      Session[requestToken.Token] = requestToken;
      
      string authorizationUrl = session.GetUserAuthorizationUrlForToken(requestToken);

      Response.Redirect(authorizationUrl, true);
    }
  }
}
