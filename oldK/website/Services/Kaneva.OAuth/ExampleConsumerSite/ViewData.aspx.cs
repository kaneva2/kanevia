///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Web;
using Kaneva.OAuth.Consumer;
using Kaneva.OAuth.Framework;
using Kaneva.OAuth.Utility;
using ExampleConsumerSite.Properties;

namespace ExampleConsumerSite
{
  public partial class ViewData : OAuthPage
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      var session = CreateSession();

      string accessTokenString = Request[Parameters.OAuth_Token];

      session.AccessToken = ((IToken) Session[accessTokenString]) ?? new TokenBase { ConsumerKey = "fake", Token = "fake"};

      try
      {
        string response = session.Request()
          .Get()
          .ForUrl(Settings.Default.DataUrl)
          .SignWithToken()
          .ReadBody();

        xmlFeed.DocumentContent = response;
      }
      catch (WebException webEx)
      {
        var response = (HttpWebResponse) webEx.Response;

        if (response.StatusCode == HttpStatusCode.Forbidden)
        {
          ShowOAuthProblemDetails(response);
        }
        else
        {
          ShowStatusCodeDetails(response);
        }
      }
    }

    void ShowStatusCodeDetails(HttpWebResponse response)
    {
      ResultsPanel.Visible = false;

      NameValueCollection parameters = HttpUtility.ParseQueryString(response.ReadToEnd());

      ErrorInfo.Text = string.Format("Request failed, status code was: {0}, status description: {1}", response.StatusCode, response.StatusDescription);
    }

    void ShowOAuthProblemDetails(WebResponse response)
    {
      ResultsPanel.Visible = false;

      NameValueCollection parameters = HttpUtility.ParseQueryString(response.ReadToEnd());

      ErrorInfo.Text = "Access was denied to resource.<br/><br/>";

      foreach (string key in parameters.Keys)
        ErrorInfo.Text += key + " => " + parameters[key] + "<br/>";
    }
  }
}
