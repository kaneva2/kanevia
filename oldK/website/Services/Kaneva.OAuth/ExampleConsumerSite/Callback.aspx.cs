///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using Kaneva.OAuth.Consumer;
using Kaneva.OAuth.Framework;

namespace ExampleConsumerSite
{
  public partial class Callback : OAuthPage
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      IOAuthSession session = CreateSession();

      string requestTokenString = Request[Parameters.OAuth_Token];
      string verifier = Request[Parameters.OAuth_Verifier];

      if (string.IsNullOrEmpty(verifier))
      {
        throw new Exception("Expected a non-empty verifier value");
      }

      var requestToken = (IToken) Session[requestTokenString];

      IToken accessToken;

      try
      {
        accessToken = session.ExchangeRequestTokenForAccessToken(requestToken, verifier);
      }
      catch (OAuthException authEx)
      {
        Session["problem"] = authEx.Report;
        Response.Redirect("AccessDenied.aspx");        
        return;
      }

      Session[requestTokenString] = null;
      Session[accessToken.Token] = accessToken;

      Response.Redirect("ViewData.aspx?oauth_token=" + accessToken.Token);
    }
  }
}
