///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System.IO;
using System.Net;

namespace Kaneva.OAuth.Utility
{
  public static class StreamExtensions
  {
    public static string ReadToEnd(this Stream stream)
    {
      using (var reader = new StreamReader(stream))
      {
        return reader.ReadToEnd();
      }
    }

    public static string ReadToEnd(this WebResponse response)
    {
      return response.GetResponseStream().ReadToEnd();
    }
  }
}
