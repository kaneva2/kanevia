///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using Kaneva.OAuth.Framework;
using Kaneva.OAuth.Storage;

namespace Kaneva.OAuth.Testing
{
  /// <summary>
  /// A simple nonce store that just tracks all nonces by consumer key in memory.
  /// </summary>
  public class TestNonceStore : INonceStore
  {
    readonly Dictionary<string, List<string>> _nonces = new Dictionary<string, List<string>>();

    public bool RecordNonceAndCheckIsUnique(IConsumer consumer, string nonce)
    {
      List<string> list = GetNonceListForConsumer(consumer.ConsumerKey);
      lock (list)
      {
        if (list.Contains(nonce)) return false;
        list.Add(nonce);
        return true;
      }
    }

    List<string> GetNonceListForConsumer(string consumerKey)
    {
      var list = new List<string>();

      if (!_nonces.TryGetValue(consumerKey, out list))
      {
        lock (_nonces)
        {
          if (!_nonces.TryGetValue(consumerKey, out list))
          {
            list = new List<string>();
            _nonces[consumerKey] = list;
          }
        }
      }

      return list;
    }
  }
}
