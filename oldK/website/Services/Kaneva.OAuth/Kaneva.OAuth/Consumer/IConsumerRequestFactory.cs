///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using Kaneva.OAuth.Framework;

namespace Kaneva.OAuth.Consumer
{
  public interface IConsumerRequestFactory
  {
    /// <summary>
    /// Creates the consumer request.
    /// </summary>
    /// <param name="context">The context.</param>
    /// <param name="consumerContext">The consumer context.</param>
    /// <param name="token">The token.</param>
    /// <returns></returns>
    IConsumerRequest CreateConsumerRequest(IOAuthContext context, IOAuthConsumerContext consumerContext, IToken token);
  }
}
