///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System.Security.Cryptography;
using Kaneva.OAuth.Framework;

namespace Kaneva.OAuth.Consumer
{
  /// <summary>
  /// A consumer context is used to identify a consumer, and to sign a context on behalf 
  /// of a consumer using an optional supplied token.
  /// </summary>
  public interface IOAuthConsumerContext
  {
    string Realm { get; set; }
    string ConsumerKey { get; set; }
    string ConsumerSecret { get; set; }
    string SignatureMethod { get; set; }
    AsymmetricAlgorithm Key { get; set; }
    bool UseHeaderForOAuthParameters { get; set; }
    void SignContext(IOAuthContext context);
    void SignContextWithToken(IOAuthContext context, IToken token);
  }
}
