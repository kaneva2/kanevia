///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using Kaneva.OAuth.Framework;

namespace Kaneva.OAuth.Consumer
{
  public class DefaultConsumerRequestFactory : IConsumerRequestFactory
  {
    public static readonly DefaultConsumerRequestFactory Instance = new DefaultConsumerRequestFactory();

    public IConsumerRequest CreateConsumerRequest(IOAuthContext context, IOAuthConsumerContext consumerContext, IToken token)
    {
      return new ConsumerRequest(context, consumerContext, token);
    }
  }
}
