///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Specialized;

namespace Kaneva.OAuth.Consumer
{
  public class RequestDescription
  {
    public RequestDescription()
    {
      Headers = new NameValueCollection();
    }

    public Uri Url { get; set; }
    public string Method { get; set; }
    public string ContentType { get; set; }
    public string Body { get; set; }
    public NameValueCollection Headers { get; private set; }
  }
}
