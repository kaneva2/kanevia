///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System.Collections.Specialized;
using System.Net;
using System.Xml.Linq;
using Kaneva.OAuth.Framework;

namespace Kaneva.OAuth.Consumer
{
  public interface IConsumerRequest
  {
    IOAuthConsumerContext ConsumerContext { get; }
    IOAuthContext Context { get; }
    XDocument ToDocument();
    byte[] ToBytes();
    HttpWebRequest ToWebRequest();
    HttpWebResponse ToWebResponse();
    NameValueCollection ToBodyParameters();
    RequestDescription GetRequestDescription();
    IConsumerRequest SignWithoutToken();
    IConsumerRequest SignWithToken();
    IConsumerRequest SignWithToken(IToken token);
  }
}
