///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Security.Cryptography;
using Kaneva.OAuth.Framework;
using Kaneva.OAuth.Framework.Signing;

namespace Kaneva.OAuth.Consumer
{
  [Serializable]
  public class OAuthConsumerContext : IOAuthConsumerContext
  {
    INonceGenerator _nonceGenerator = new GuidNonceGenerator();
    IOAuthContextSigner _signer = new OAuthContextSigner();

    public OAuthConsumerContext()
    {
      SignatureMethod = Framework.SignatureMethod.PlainText;
    }

    public IOAuthContextSigner Signer
    {
      get { return _signer; }
      set { _signer = value; }
    }

    public INonceGenerator NonceGenerator
    {
      get { return _nonceGenerator; }
      set { _nonceGenerator = value; }
    }

    public string Realm { get; set; }
    public string ConsumerKey { get; set; }
    public string ConsumerSecret { get; set; }
    public string SignatureMethod { get; set; }
    public AsymmetricAlgorithm Key { get; set; }
    public bool UseHeaderForOAuthParameters { get; set; }

    public void SignContext(IOAuthContext context)
    {
      EnsureStateIsValid();


      context.UseAuthorizationHeader = UseHeaderForOAuthParameters;
      context.Nonce = _nonceGenerator.GenerateNonce(context);
      context.ConsumerKey = ConsumerKey;
      context.Realm = Realm;
      context.SignatureMethod = SignatureMethod;
      context.Timestamp = DateTime.Now.Epoch().ToString();
      context.Version = "1.0";

      string signatureBase = context.GenerateSignatureBase();

      _signer.SignContext(context,
                          new SigningContext
                            {Algorithm = Key, SignatureBase = signatureBase, ConsumerSecret = ConsumerSecret});
    }

    public void SignContextWithToken(IOAuthContext context, IToken token)
    {
      context.Token = token.Token;
      context.TokenSecret = token.TokenSecret;

      SignContext(context);
    }

    void EnsureStateIsValid()
    {
      if (string.IsNullOrEmpty(ConsumerKey)) throw Error.EmptyConsumerKey();
      if (string.IsNullOrEmpty(SignatureMethod)) throw Error.UnknownSignatureMethod(SignatureMethod);
      if ((SignatureMethod == Framework.SignatureMethod.RsaSha1)
          && (Key == null)) throw Error.ForRsaSha1SignatureMethodYouMustSupplyAssymetricKeyParameter();
    }
  }
}
