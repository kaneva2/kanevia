///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using Kaneva.OAuth.Framework;

namespace Kaneva.OAuth.Consumer
{
  public interface IOAuthSession
  {
    IOAuthConsumerContext ConsumerContext { get; set; }
    Uri RequestTokenUri { get; set; }
    Uri AccessTokenUri { get; set; }
    Uri UserAuthorizeUri { get; set; }
    IToken AccessToken { get; set; }
    IConsumerRequest Request();
    IConsumerRequest Request(IToken accessToken);
    IToken GetRequestToken();
    IToken ExchangeRequestTokenForAccessToken(IToken requestToken);
    IToken ExchangeRequestTokenForAccessToken(IToken requestToken, string verificationCode);
    IToken ExchangeRequestTokenForAccessToken(IToken requestToken, string method, string verificationCode);
    IConsumerRequest BuildRequestTokenContext(string method);
    IConsumerRequest BuildExchangeRequestTokenForAccessTokenContext(IToken requestToken, string method, string verificationCode);
    string GetUserAuthorizationUrlForToken(IToken token, string callbackUrl);
    string GetUserAuthorizationUrlForToken(IToken token);
    IOAuthSession WithFormParameters(IDictionary dictionary);
    IOAuthSession WithFormParameters(object anonymousClass);
    IOAuthSession WithQueryParameters(IDictionary dictionary);
    IOAuthSession WithQueryParameters(object anonymousClass);
    IOAuthSession WithCookies(IDictionary dictionary);
    IOAuthSession WithCookies(object anonymousClass);
    IOAuthSession WithHeaders(IDictionary dictionary);
    IOAuthSession WithHeaders(object anonymousClass);
    IOAuthSession RequiresCallbackConfirmation();
  }
}
