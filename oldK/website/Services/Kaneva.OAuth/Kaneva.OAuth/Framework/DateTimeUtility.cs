///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace Kaneva.OAuth.Framework
{
  public static class DateTimeUtility
  {
    public static long Epoch(this DateTime d)
    {
      return (long) (d.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds;
    }

    public static DateTime FromEpoch(long epoch)
    {
      var d = new DateTime(1970, 1, 1);
      d = d.AddSeconds(epoch);
      return d.ToLocalTime();
    }
  }
}
