///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using Kaneva.OAuth.Framework;

namespace Kaneva.OAuth
{
  /// <summary>
  /// Generates unique nonces (via Guids) to let the server detect duplicated requests.
  /// </summary>
  public class GuidNonceGenerator : INonceGenerator
  {
    protected Random random = new Random();

    public string GenerateNonce(IOAuthContext context)
    {
      return Guid.NewGuid().ToString();
    }
  }
}
