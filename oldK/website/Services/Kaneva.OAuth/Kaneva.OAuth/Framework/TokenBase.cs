///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace Kaneva.OAuth.Framework
{
  [Serializable]
  public class TokenBase : IToken
  {
    public string TokenSecret { get; set; }

    public string Token { get; set; }

    public string ConsumerKey { get; set; }

    public string Realm { get; set; }

    public override string ToString()
    {
      return UriUtility.FormatTokenForResponse(this);
    }
  }
}
