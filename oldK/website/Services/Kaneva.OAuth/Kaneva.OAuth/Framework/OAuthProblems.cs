///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace Kaneva.OAuth.Framework
{
  public static class OAuthProblems
  {
    public const string AdditionalAuthorizationRequired = "additional_authorization_required";
    public const string ConsumerKeyRefused = "consumer_key_refused";
    public const string ConsumerKeyRejected = "consumer_key_rejected";
    public const string ConsumerKeyUnknown = "consumer_key_unknown";
    public const string NonceUsed = "nonce_used";
    public const string ParameterAbset = "parameter_absent";
    public const string ParameterRejected = "parameter_rejected";
    public const string PermissionDenied = "permission_denied";
    public const string PermissionUnknown = "permission_unknown";
    public const string SignatureInvalid = "signature_invalid";
    public const string SignatureMethodRejected = "signature_method_rejected";
    public const string TimestampRefused = "timestamp_refused";
    public const string TokenExpired = "token_expired";
    public const string TokenRejected = "token_rejected";
    public const string TokenRevoked = "token_revoked";
    public const string TokenUsed = "token_used";
    public const string UserRefused = "user_refused";
    public const string VersionRejected = "version_rejected";
  }
}
