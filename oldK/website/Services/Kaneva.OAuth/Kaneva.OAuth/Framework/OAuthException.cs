///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Runtime.Serialization;

namespace Kaneva.OAuth.Framework
{
  public class OAuthException : Exception
  {
    public OAuthException()
    {
    }

    public OAuthException(string message, Exception innerException)
      : base(message, innerException)
    {      
    }

    public OAuthException(IOAuthContext context, string problem, string advice) : base(advice)
    {
      Context = context;
      Report = new OAuthProblemReport() {Problem = problem, ProblemAdvice = advice};
    }

    public OAuthException(IOAuthContext context, string problem, string advice, Exception innerException)
      : base(advice, innerException)
    {
      Context = context;
      Report = new OAuthProblemReport() {Problem = problem, ProblemAdvice = advice};
    }

    public OAuthException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }

    public OAuthProblemReport Report { get; set; }
    public IOAuthContext Context { get; set; }
  }
}
