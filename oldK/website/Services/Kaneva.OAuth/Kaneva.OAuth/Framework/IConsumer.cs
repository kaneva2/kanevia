///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace Kaneva.OAuth.Framework
{
  public interface IConsumer
  {
    string ConsumerKey { get; set; }
    string Realm { get; set; }
  }
}
