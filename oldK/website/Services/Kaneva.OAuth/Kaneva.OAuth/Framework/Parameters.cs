///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace Kaneva.OAuth.Framework
{
  public static class Parameters
  {
    public const string OAuth_Acceptable_Timestamps = "oauth_acceptable_timestamps";
    public const string OAuth_Acceptable_Versions = "oauth_acceptable_versions";
    public const string OAuth_Authorization_Header = "Authorization";
    public const string OAuth_Callback = "oauth_callback";
    public const string OAuth_Callback_Confirmed = "oauth_callback_confirmed";
    public const string OAuth_Consumer_Key = "oauth_consumer_key";
    public const string OAuth_Nonce = "oauth_nonce";
    public const string OAuth_Parameters_Absent = "oauth_parameters_absent";
    public const string OAuth_Parameters_Rejected = "oauth_parameters_rejected";
    public const string OAuth_Problem = "oauth_problem";
    public const string OAuth_Problem_Advice = "oauth_problem_advice";
    public const string OAuth_Signature = "oauth_signature";
    public const string OAuth_Signature_Method = "oauth_signature_method";
    public const string OAuth_Timestamp = "oauth_timestamp";
    public const string OAuth_Token = "oauth_token";
    public const string OAuth_Token_Secret = "oauth_token_secret";
    public const string OAuth_Verifier = "oauth_verifier";
    public const string OAuth_Version = "oauth_version";
    public const string OAuthParameterPrefix = "oauth_";
    public const string Realm = "realm";
  }
}
