///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace Kaneva.OAuth.Framework
{
  public static class SignatureMethod
  {
    public const string HmacSha1 = "HMAC-SHA1";
    public const string PlainText = "PLAINTEXT";
    public const string RsaSha1 = "RSA-SHA1";
  }
}
