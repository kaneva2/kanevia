///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace Kaneva.OAuth.Framework.Signing
{
  public class PlainTextSignatureImplementation : IContextSignatureImplementation
  {
    public string MethodName
    {
      get { return SignatureMethod.PlainText; }
    }

    public void SignContext(IOAuthContext authContext, SigningContext signingContext)
    {
      authContext.Signature = GenerateSignature(authContext, signingContext);
    }

    public bool ValidateSignature(IOAuthContext authContext, SigningContext signingContext)
    {
      return (authContext.Signature == GenerateSignature(authContext, signingContext));
    }

    string GenerateSignature(IOAuthContext authContext, SigningContext signingContext)
    {
      //return UriUtility.UrlEncode(string.Format("{0}&{1}", signingContext.ConsumerSecret, authContext.TokenSecret));
        return UriUtility.UrlEncode(string.Format("{0}", signingContext.ConsumerSecret));
    }
  }
}
