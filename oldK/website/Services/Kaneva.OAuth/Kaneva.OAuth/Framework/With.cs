///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace Kaneva.OAuth.Framework
{
  public static class With
  {
    public static IDisposable NoCertificateValidation()
    {
      RemoteCertificateValidationCallback oldCallback = ServicePointManager.ServerCertificateValidationCallback;
      ServicePointManager.ServerCertificateValidationCallback = CertificateAlwaysValidCallback;
      return new DisposableAction(delegate { ServicePointManager.ServerCertificateValidationCallback = oldCallback; });
    }

    static bool CertificateAlwaysValidCallback(object sender, X509Certificate certificate, X509Chain chain,
                                               SslPolicyErrors sslPolicyErrors)
    {
      return true;
    }
  }

  public class DisposableAction : IDisposable
  {
    readonly Action _action;

    public DisposableAction(Action action)
    {
      if (action == null) throw new ArgumentNullException("action");
      _action = action;
    }

    public void Dispose()
    {
      _action();
    }
  }
}
