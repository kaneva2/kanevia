///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System.Security.Cryptography;

namespace Kaneva.OAuth.Framework
{
  public class SigningContext
  {
    public AsymmetricAlgorithm Algorithm { get; set; }
    public string ConsumerSecret { get; set; }
    public string SignatureBase { get; set; }
  }
}
