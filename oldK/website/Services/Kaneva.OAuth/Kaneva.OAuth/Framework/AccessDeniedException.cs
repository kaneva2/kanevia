///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using Kaneva.OAuth.Provider;

namespace Kaneva.OAuth.Framework
{
  public class AccessDeniedException : Exception
  {
    readonly AccessOutcome _outcome;

    public AccessDeniedException(AccessOutcome outcome)
      : this(outcome, null)
    {
    }

    public AccessDeniedException(AccessOutcome outcome, string message) : base(message)
    {
      _outcome = outcome;
    }

    public AccessOutcome Outcome
    {
      get { return _outcome; }
    }
  }
}
