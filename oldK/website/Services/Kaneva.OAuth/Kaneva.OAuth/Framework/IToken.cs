///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace Kaneva.OAuth.Framework
{  
  public interface IToken : IConsumer
  {
    string TokenSecret { get; set; }
    string Token { get; set; }
  }
}
