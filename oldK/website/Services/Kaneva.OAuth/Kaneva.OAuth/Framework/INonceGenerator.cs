///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using Kaneva.OAuth.Framework;

namespace Kaneva.OAuth
{
  /// <summary>
  /// Generates a nonce, which should be unique for the selected consumer (i.e. never generated
  /// by subsequent calls to <see cref="GenerateNonce" />)
  /// </summary>
  public interface INonceGenerator
  {
    string GenerateNonce(IOAuthContext context);
  }
}
