///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Specialized;

namespace Kaneva.OAuth.Framework
{
  public interface IOAuthContext : IToken
  {
    NameValueCollection Headers { get; set; }
    NameValueCollection QueryParameters { get; set; }
    NameValueCollection Cookies { get; set; }
    NameValueCollection FormEncodedParameters { get; set; }
    NameValueCollection AuthorizationHeaderParameters { get; set; }

    Uri RawUri { get; set; }

    string NormalizedRequestUrl { get; }
    string RequestMethod { get; set; }
    string Nonce { get; set; }
    string Signature { get; set; }
    string SignatureMethod { get; set; }
    string Timestamp { get; set; }
    string Version { get; set; }
    string CallbackUrl { get; set; }
    string Verifier { get; set; }

    bool UseAuthorizationHeader { get; set; }

    Uri GenerateUri();
    string GenerateUrl();
    string GenerateOAuthParametersForHeader();
    Uri GenerateUriWithoutOAuthParameters();
    string GenerateSignatureBase();
    string ToString();
  }
}
