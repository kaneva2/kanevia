///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using Kaneva.OAuth.Framework;

namespace Kaneva.OAuth.Provider
{
  public interface IOAuthProvider
  {
    IToken GrantRequestToken(IOAuthContext context);
    IToken ExchangeRequestTokenForAccessToken(IOAuthContext context);
    void AccessProtectedResourceRequest(IOAuthContext context);
  }
}
