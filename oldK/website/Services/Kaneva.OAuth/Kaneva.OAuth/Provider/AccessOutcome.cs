///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using Kaneva.OAuth.Framework;

namespace Kaneva.OAuth.Provider
{
  public class AccessOutcome
  {
    public bool Granted { get; set; }
    public string AdditionalInfo { get; set; }
    public IOAuthContext Context { get; set; }
  }
}
