///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using Kaneva.OAuth.Framework;
using Kaneva.OAuth.Storage;

namespace Kaneva.OAuth.Provider.Inspectors
{
  public class ConsumerValidationInspector : IContextInspector
  {
    readonly IConsumerStore _consumerStore;

    public ConsumerValidationInspector(IConsumerStore consumerStore)
    {
      if (consumerStore == null) throw new ArgumentNullException("consumerStore");
      _consumerStore = consumerStore;
    }

    public void InspectContext(ProviderPhase phase, IOAuthContext context)
    {
      if (!_consumerStore.IsConsumer(context))
      {
        throw Error.UnknownConsumerKey(context);
      }
    }
  }
}
