///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System.Security.Cryptography.X509Certificates;
using Kaneva.OAuth.Framework;
using Kaneva.OAuth.Framework.Signing;
using Kaneva.OAuth.Storage;

namespace Kaneva.OAuth.Provider.Inspectors
{
  public class SignatureValidationInspector : IContextInspector
  {
    readonly IConsumerStore _consumerStore;
    readonly IOAuthContextSigner _signer;

    public SignatureValidationInspector(IConsumerStore consumerStore)
      : this(consumerStore, new OAuthContextSigner())
    {
    }

    public SignatureValidationInspector(IConsumerStore consumerStore, IOAuthContextSigner signer)
    {
      _consumerStore = consumerStore;
      _signer = signer;
    }

    public virtual void InspectContext(ProviderPhase phase, IOAuthContext context)
    {
      SigningContext signingContext = CreateSignatureContextForConsumer(context);

      if (!_signer.ValidateSignature(context, signingContext))
      {
        throw Error.FailedToValidateSignature(context);
      }
    }

    protected virtual bool SignatureMethodRequiresCertificate(string signatureMethod)
    {
      return ((signatureMethod != SignatureMethod.HmacSha1) && (signatureMethod != SignatureMethod.PlainText));
    }

    protected virtual SigningContext CreateSignatureContextForConsumer(IOAuthContext context)
    {
      var signingContext = new SigningContext {ConsumerSecret = _consumerStore.GetConsumerSecret(context)};

      if (SignatureMethodRequiresCertificate(context.SignatureMethod))
      {
        X509Certificate2 cert = _consumerStore.GetConsumerCertificate(context);
        signingContext.Algorithm = cert.PublicKey.Key;
      }

      return signingContext;
    }
  }
}
