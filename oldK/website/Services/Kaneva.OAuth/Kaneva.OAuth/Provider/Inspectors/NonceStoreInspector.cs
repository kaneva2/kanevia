///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using Kaneva.OAuth.Framework;
using Kaneva.OAuth.Storage;

namespace Kaneva.OAuth.Provider.Inspectors
{
  public class NonceStoreInspector : IContextInspector
  {
    readonly INonceStore _nonceStore;

    public NonceStoreInspector(INonceStore nonceStore)
    {
      if (nonceStore == null) throw new ArgumentNullException("nonceStore");
      _nonceStore = nonceStore;
    }

    public void InspectContext(ProviderPhase phase, IOAuthContext context)
    {
      if (!_nonceStore.RecordNonceAndCheckIsUnique(context, context.Nonce))
      {
        throw Error.NonceHasAlreadyBeenUsed(context);
      }
    }
  }
}
