///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using Kaneva.OAuth.Framework;

namespace Kaneva.OAuth.Provider.Inspectors
{
  public interface IContextInspector
  {
    void InspectContext(ProviderPhase phase, IOAuthContext context);
  }
}
