///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using Kaneva.OAuth.Framework;

namespace Kaneva.OAuth.Storage.Basic
{
  /// <summary>
  /// Simple access token model, this would hold information required to enforce policies such as expiration, and association
  /// with a user accout or other information regarding the information the consumer has been granted access to.
  /// </summary>
  [Serializable]
  public class AccessToken : TokenBase
  {
    public string UserName { get; set; }
    public string[] Roles { get; set; }
    public DateTime ExpireyDate { get; set; }
  }
}
