///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using Kaneva.OAuth.Framework;

namespace Kaneva.OAuth.Storage.Basic
{
  /// <summary>
  /// A simplistic repository for access and request of token models - the example implementation of
  /// <see cref="ITokenStore" /> relies on this repository - normally you would make use of repositories
  /// wired up to your domain model i.e. NHibernate, Entity Framework etc.
  /// </summary>    
  public interface ITokenRepository<T> where T : TokenBase
  {
    /// <summary>
    /// Gets an existing token from the underlying store
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    T GetToken(string token);

    /// <summary>
    /// Saves the token in the underlying store
    /// </summary>
    /// <param name="token"></param>
    void SaveToken(T token);
  }
}
