///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System.Collections.Generic;
using Kaneva.OAuth.Framework;

using System;
using Kaneva.DataLayer;

namespace Kaneva.OAuth.Storage.Basic
{
  /// <summary>
  /// Memcached implementation of a token repository
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class MemcachedTokenRepository<T> : ITokenRepository<T>
    where T : TokenBase
  {
    //readonly Dictionary<string, T> _tokens = new Dictionary<string, T>();

    public T GetToken(string token)
    {      
      //return _tokens[token];
        T theToken = (T) Kaneva.DataLayer.DataObjects.CentralCache.Get(Kaneva.DataLayer.DataObjects.CentralCache.keyAPIToken + token);
        if (theToken == null)
        {
            throw new Exception ("Token not found");
        }
        return theToken;
    }

    public void SaveToken(T token)
    {
      //_tokens[token.Token] = token;
        Kaneva.DataLayer.DataObjects.CentralCache.Store(Kaneva.DataLayer.DataObjects.CentralCache.keyAPIToken + token.Token, token, TimeSpan.FromDays(29));
    }

  }
}
