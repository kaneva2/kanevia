///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System.Collections.Generic;
using Kaneva.OAuth.Framework;

namespace Kaneva.OAuth.Storage.Basic
{
  /// <summary>
  /// In-Memory implementation of a token repository
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class InMemoryTokenRepository<T> : ITokenRepository<T>
    where T : TokenBase
  {
    readonly Dictionary<string, T> _tokens = new Dictionary<string, T>();

    public T GetToken(string token)
    {      
      return _tokens[token];
    }

    public void SaveToken(T token)
    {
      _tokens[token.Token] = token;
    }
  }
}
