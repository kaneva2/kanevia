///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System.Security.Cryptography.X509Certificates;
using Kaneva.OAuth.Framework;

namespace Kaneva.OAuth.Storage
{
  public interface IConsumerStore
  {
    bool IsConsumer(IConsumer consumer);
    void SetConsumerSecret(IConsumer consumer, string consumerSecret);
    string GetConsumerSecret(IConsumer consumer);
    void SetConsumerCertificate(IConsumer consumer, X509Certificate2 certificate);
    X509Certificate2 GetConsumerCertificate(IConsumer consumer);
  }
}
