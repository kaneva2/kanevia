///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Security.Cryptography.X509Certificates;
using Kaneva.OAuth.Framework;
using Kaneva.OAuth.Storage;
using Kaneva.OAuth.Tests;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;

namespace Kaneva.OAuth.Storage
{
    public class MySQLConsumerStore : IConsumerStore
    {
        public bool IsConsumer(IConsumer consumer)
        {
            //return (consumer.ConsumerKey == "3DApp" && string.IsNullOrEmpty(consumer.Realm));
            CommunityFacade gameFacade = new CommunityFacade();
            APIAuthentication apiAuth = gameFacade.GetAPIAuth(consumer.ConsumerKey);

            return (apiAuth.CommunityId > 0 && string.IsNullOrEmpty(consumer.Realm));
        }

        public void SetConsumerSecret(IConsumer consumer, string consumerSecret)
        {
            throw new NotImplementedException();
        }

        public string GetConsumerSecret(IConsumer consumer)
        {
            CommunityFacade gameFacade = new CommunityFacade();
            APIAuthentication apiAuth = gameFacade.GetAPIAuth(consumer.ConsumerKey);
            return apiAuth.ConsumerSecret;
        }

        public void SetConsumerCertificate(IConsumer consumer, X509Certificate2 certificate)
        {
            throw new NotImplementedException();
        }

        public X509Certificate2 GetConsumerCertificate(IConsumer consumer)
        {
            return TestCertificates.OAuthTestCertificate();
        }
    }
}
