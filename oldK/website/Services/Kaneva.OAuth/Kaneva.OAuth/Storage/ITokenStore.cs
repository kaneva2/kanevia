///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using Kaneva.OAuth.Framework;

namespace Kaneva.OAuth.Storage
{
  public interface ITokenStore
  {
    /// <summary>
    /// Creates a request token for the consumer.
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    IToken CreateRequestToken(IOAuthContext context);

    /// <summary>
    /// Should consume a use of the request token, throwing a <see cref="OAuthException" /> on failure.
    /// </summary>
    /// <param name="requestContext"></param>
    void ConsumeRequestToken(IOAuthContext requestContext);

    /// <summary>
    /// Should consume a use of an access token, throwing a <see cref="OAuthException" /> on failure.
    /// </summary>
    /// <param name="accessContext"></param>
    void ConsumeAccessToken(IOAuthContext accessContext);

    /// <summary>
    /// Get the access token associated with a request token.
    /// </summary>
    /// <param name="requestContext"></param>
    /// <returns></returns>
    IToken GetAccessTokenAssociatedWithRequestToken(IOAuthContext requestContext);

    /// <summary>
    /// Returns the status for a request to access a consumers resources.
    /// </summary>
    /// <param name="requestContext"></param>
    /// <returns></returns>
    RequestForAccessStatus GetStatusOfRequestForAccess(IOAuthContext requestContext);

    /// <summary>
    /// Returns the callback url that is stored against this token.
    /// </summary>
    /// <param name="requestContext"></param>
    /// <returns></returns>
    string GetCallbackUrlForToken(IOAuthContext requestContext);

    /// <summary>
    /// Retrieves the verification code for a token
    /// </summary>
    /// <param name="requestContext"></param>
    /// <returns>verification code</returns>
    string GetVerificationCodeForRequestToken(IOAuthContext requestContext);

    /// <summary>
    /// Gets the token secret for the supplied request token
    /// </summary>
    /// <param name="context"></param>
    /// <returns>token secret</returns>
    string GetRequestTokenSecret(IOAuthContext context);

    /// <summary>
    /// Gets the token secret for the supplied access token
    /// </summary>
    /// <param name="context"></param>
    /// <returns>token secret</returns>
    string GetAccessTokenSecret(IOAuthContext context);
  }
}
