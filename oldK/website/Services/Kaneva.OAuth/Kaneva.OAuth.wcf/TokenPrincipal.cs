///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System.Security.Principal;
using Kaneva.OAuth.Framework;

namespace Kaneva.OAuth.Wcf
{
  public class TokenPrincipal : GenericPrincipal
  {
    readonly TokenBase _token;

    public TokenPrincipal(IIdentity identity, string[] roles, TokenBase token)
      : base(identity, roles)
    {
      _token = token;
    }

    public TokenBase Token
    {
      get { return _token; }
    }
  }
}
