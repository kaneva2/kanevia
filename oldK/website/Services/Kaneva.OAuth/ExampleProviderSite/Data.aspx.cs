///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Linq;
using System.Web.UI;
using System.Xml.Linq;
using Kaneva.OAuth.Framework;
using Kaneva.OAuth.Provider;
using ExampleProviderSite.Repositories;

namespace ExampleProviderSite
{
  public partial class Data : Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      var context = new OAuthContextBuilder().FromHttpRequest(Request);

      IOAuthProvider provider = OAuthServicesLocator.Services.Provider;

      var tokenRepository = OAuthServicesLocator.Services.AccessTokenRepository;

      try
      {
        provider.AccessProtectedResourceRequest(context);

        var accessToken = tokenRepository.GetToken(context.Token);

        string userName = accessToken.UserName;

        XDocument contactsDocument = GetContactsForUser(userName);

        Response.ContentType = "text/xml";
        Response.Write(contactsDocument);
        Response.End();
      }
      catch (OAuthException authEx)
      {
        // fairly naieve approach to status codes, generally you would want to examine eiter the inner exception of the 
        // problem report to determine an appropriate status code for your technology / architecture.

        Response.StatusCode = 403;
        Response.Write(authEx.Report);
        Response.End();
      }
    }

    public XDocument GetContactsForUser(string userName)
    {
      var repository = new ContactsRepository();

      return new XDocument(
        new XElement("contacts",
                     new XAttribute("for", userName),
                     repository.GetContactsForUser(userName)
                       .Select(contact => new XElement("contact",
                                                       new XAttribute("name", contact.FullName),
                                                       new XAttribute("email", contact.Email)))));
    }
  }
}
