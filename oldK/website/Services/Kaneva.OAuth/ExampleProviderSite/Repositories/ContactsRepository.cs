///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;

namespace ExampleProviderSite.Repositories
{
  public class ContactsRepository
  {
    public List<Contact> GetContactsForUser(string userName)
    {
      switch (userName)
      {
          case "jason":
              return new List<Contact>
            {
              new Contact {FullName = "Michael Jordan", Email = "jake@test.com"},
              new Contact {FullName = "Bart Simpson", Email = "simpson@test.com"}
            };
          case "biggs":
              return new List<Contact>
            {
              new Contact {FullName = "Donny Darko", Email = "darko@test.com"},
              new Contact {FullName = "Miley Sirus", Email = "miley@test.com"}
            };
          default:
              throw new Exception("unknown user");
      }
    }
  }

  public class Contact
  {
    public string FullName { get; set; }
    public string Email { get; set; }
  }
}
