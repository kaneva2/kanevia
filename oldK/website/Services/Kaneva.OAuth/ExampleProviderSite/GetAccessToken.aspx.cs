///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;
using Kaneva.OAuth.Framework;
using Kaneva.OAuth.Provider;

namespace ExampleProviderSite
{
  public partial class GetAccessToken : Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      try
      {
        IOAuthContext context = new OAuthContextBuilder().FromHttpRequest(Request);

        IOAuthProvider provider = OAuthServicesLocator.Services.Provider;

        IToken accessToken = provider.ExchangeRequestTokenForAccessToken(context);

        Response.Write(accessToken);
      }
      catch (OAuthException ex)
      {
        // fairly naieve approach to status codes, generally you would want to examine either the inner exception or the 
        // problem report to determine an appropriate status code for your technology / architecture.

        Response.StatusCode = 400;
        Response.Write(ex.Report.ToString());
      }

      Response.End();
    }
  }
}
