///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using Kaneva.OAuth.Provider;
using Kaneva.OAuth.Provider.Inspectors;
using Kaneva.OAuth.Storage.Basic;
using Kaneva.OAuth.Testing;

namespace ExampleProviderSite
{
  public class Global : HttpApplication, IOAuthServices
  {
    static ITokenRepository<Kaneva.OAuth.Storage.Basic.AccessToken> _accessTokenRepository;
    static IOAuthProvider _provider;
    static ITokenRepository<Kaneva.OAuth.Storage.Basic.RequestToken> _requestTokenRepository;

    public IOAuthProvider Provider
    {
      get { return _provider; }
    }

    public ITokenRepository<Kaneva.OAuth.Storage.Basic.AccessToken> AccessTokenRepository
    {
      get { return _accessTokenRepository; }
    }

    public ITokenRepository<Kaneva.OAuth.Storage.Basic.RequestToken> RequestTokenRepository
    {
      get { return _requestTokenRepository; }
    }

    protected void Application_Start(object sender, EventArgs e)
    {
      _requestTokenRepository = new InMemoryTokenRepository<Kaneva.OAuth.Storage.Basic.RequestToken>();
      _accessTokenRepository = new InMemoryTokenRepository<Kaneva.OAuth.Storage.Basic.AccessToken>();

      var consumerStore = new TestConsumerStore();

      var nonceStore = new TestNonceStore();

      var tokenStore = new SimpleTokenStore(_accessTokenRepository, _requestTokenRepository);

      _provider = new OAuthProvider(tokenStore,
                                    new SignatureValidationInspector(consumerStore),
                                    new NonceStoreInspector(nonceStore),
                                    new TimestampRangeInspector(new TimeSpan(1, 0, 0)),
                                    new ConsumerValidationInspector(consumerStore),
                                    new OAuth10AInspector(tokenStore));
    }
  }
}
