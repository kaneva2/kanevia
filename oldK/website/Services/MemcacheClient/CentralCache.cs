///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Runtime.InteropServices;
using System.Collections;
using System.Text;
using System.Configuration;
using System.Diagnostics;

using Enyim.Caching;

namespace Kaneva.MemcacheClient
{
    /// <summary>
    /// Interface class for Memecached wrapper
    /// </summary>
    [Guid("D490B0ED-A555-4cc4-945F-5EF5532E21EC")]
    public interface ICentralCache
    {
        /// <summary>
        /// Call to check if initialize and check enabled.  Must call this before any other calls.
        /// </summary>
        /// <param name="enabled">out param indicating if even enabled</param>
        /// <returns>true if initialize or disabled in config, otherwise call LastError to get error msg</returns>
        [DispId(1)]
        bool Initialize(out bool enabled);

        /// <summary>
        /// get the last error message if any other methods returned false
        /// </summary>
        /// <param name="asString">the last error message</param>
        /// <returns>true if there is an error message</returns>
        [DispId(2)]
        bool LastError(out string asString);

        /// <summary>
        /// store a string value in the cache
        /// </summary>
        /// <param name="key">the key</param>
        /// <param name="value">the value</param>
        /// <param name="minutes">number of minutes to keep in cache.  Zero means no expiration</param>
        /// <returns>true if ok, otherwise call LastError to get error msg</returns>
        [DispId(3)]
        bool StoreString(string key, string value, int minutes);

        /// <summary>
        /// store an int value in the cache
        /// </summary>
        /// <param name="key">the key</param>
        /// <param name="value">the value</param>
        /// <param name="minutes">number of minutes to keep in cache.  Zero means no expiration</param>
        /// <returns>true if ok, otherwise call LastError to get error msg</returns>
        [DispId(4)]
        bool StoreInt(string key, int value, int minutes);

        /// <summary>
        /// store a double value in the cache
        /// </summary>
        /// <param name="key">the key</param>
        /// <param name="value">the value</param>
        /// <param name="minutes">number of minutes to keep in cache.  Zero means no expiration</param>
        /// <returns>true if ok, otherwise call LastError to get error msg</returns>
        [DispId(5)]
        bool StoreDouble(string key, double value, int minutes);

        /// <summary>
        /// get a string value from the cache
        /// </summary>
        /// <param name="key">the key</param>
        /// <param name="found">returned as true if key found</param>
        /// <param name="value">the return value</param>
        /// <returns>true if ok, otherwise call LastError to get error msg</returns>
        [DispId(6)]
        bool GetString(string key, out bool found, out string value);

        /// <summary>
        /// get an int value from the cache
        /// </summary>
        /// <param name="key">the key</param>
        /// <param name="found">returned as true if key found</param>
        /// <param name="value">the return value</param>
        /// <returns>true if ok, otherwise call LastError to get error msg</returns>
        [DispId(7)]
        bool GetInt(string key, out bool found, out int value);

        /// <summary>
        /// get a double value from the cache
        /// </summary>
        /// <param name="key">the key</param>
        /// <param name="found">returned as true if key found</param>
        /// <param name="value">the return value</param>
        /// <returns>true if ok, otherwise call LastError to get error msg</returns>
        [DispId(8)]
        bool GetDouble(string key, out bool found, out double value);

        /// <summary>
        /// flush the entire cache
        /// </summary>
        /// <returns>true if ok, otherwise call LastError to get error msg</returns>
        [DispId(9)]
        bool FlushAll();

        /// <summary>
        /// remove a key from the cache
        /// </summary>
        /// <param name="key">the key</param>
        /// <returns>true if ok, otherwise call LastError to get error msg</returns>
        [DispId(10)]
        bool Remove(string key);

    }

    [Guid("90B5AECA-AA75-40c6-9516-5F53D55495A7"),
    ClassInterface(ClassInterfaceType.None)]
    public class CentralCache : ICentralCache
    {
        public bool Initialize(out bool bCacheEnabled)
        {
            bCacheEnabled = false;
            m_lastError = "";
            try
            {
                // If it enabled
                if (ConfigurationManager.AppSettings.Get("MemcachedEnabled") != null)
                {
                    bCacheEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("MemcachedEnabled"));
                }

                if (bCacheEnabled)
                {
                    _MemcachedClient = new MemcachedClient();
                    if (_MemcachedClient == null)
                        m_lastError = "new failed for MemcachedClient";
                }
                else
                {
                    m_lastError = "Memcached is turned off. MemcachedEnabled is false.";
                    return true; // ok, just turned off
                }
            }
            catch (Exception exc)
            {
                if (exc.InnerException != null)
                    m_lastError = exc.InnerException.Message;
                else
                    m_lastError = exc.Message;

                if (_MemcachedClient != null)
                    _MemcachedClient = null;
            }
            return _MemcachedClient != null;
        }

        public bool LastError(out string msg)
        {
            if (m_lastError.Length == 0)
            {
                msg = "";
                return false;
            }
            else
            {
                msg = m_lastError;
                if ( _MemcachedClient != null )
                    m_lastError = "";
                return true;
            }
        }

        public bool StoreString(string key, string value, int minutes )
        {
            return store( key, value, minutes );
        }

        public bool StoreInt(string key, int value, int minutes)
        {
            return store(key, value, minutes);
        }

        public bool StoreDouble(string key, double value, int minutes)
        {
            return store(key, value, minutes);
        }

        public bool GetString(string key, out bool found, out string value)
        {
            value = "";
            found = false;
            try
            {
                bool error = false;
                object o = get(key, out error);
                if (error)
                {
                    return false;
                }

                if (o != null)
                {
                    found = true;
                    value = o.ToString();
                }
                return true;
            }
            catch (Exception exc)
            {
                if (exc.InnerException != null)
                    m_lastError = exc.InnerException.Message;
                else
                    m_lastError = exc.Message;
                return false;
            }

        }

        public bool GetInt(string key, out bool found, out int value)
        {
            value = 0;
            found = false;
            try
            {
                bool error = false;
                object o = get(key, out error);
                if (error)
                {
                    return false;
                }

                if (o != null)
                {
                    found = true;
                    value = Convert.ToInt32(o); // let it throw if fails
                }
                return true;
            }
            catch (Exception exc)
            {
                if (exc.InnerException != null)
                    m_lastError = exc.InnerException.Message;
                else
                    m_lastError = exc.Message;
                return false;
            }

        }

        public bool GetDouble(string key, out bool found, out double value)
        {
            value = 0.0;
            found = false;
            try
            {
                bool error = false;
                object o = get(key, out error);
                if (error)
                {
                    return false;
                }

                if (o != null)
                {
                    found = true;
                    value = Convert.ToDouble(o); // let it throw if fails
                }
                return true;
            }
            catch (Exception exc)
            {
                if (exc.InnerException != null)
                    m_lastError = exc.InnerException.Message;
                else
                    m_lastError = exc.Message;
                return false;
            }

        }

        public bool Remove(string strKey)
        {
            try
            {
                if (_MemcachedClient != null)
                {
                    if (!_MemcachedClient.Remove(strKey))
                    {
                        m_lastError = "Failed to remove key " + strKey;
                        return false;
                    }
                    else
                    {
                        m_lastError = "";
                        return true;
                    }
                }
            }
            catch (Exception exc)
            {
                if (exc.InnerException != null)
                    m_lastError = exc.InnerException.Message;
                else
                    m_lastError = exc.Message;
                return false;
            }
            return true;
        }

        /// <summary>
        /// FlushAll
        /// </summary>
        public bool FlushAll()
        {
            try
            {
                if (_MemcachedClient != null)
                {
                    _MemcachedClient.FlushAll();
                    m_lastError = "";
                }
                return true;
            }
            catch (Exception exc)
            {
                if (exc.InnerException != null)
                    m_lastError = exc.InnerException.Message;
                else
                    m_lastError = exc.Message;
                return false;
            }
        }

        private bool store(string strKey, Object oObject, int minutes)
        {
            try
            {
                if (_MemcachedClient != null)
                {
                    m_lastError = "";
                    if (!oObject.GetType().IsSerializable)
                    {
                        Debug.Assert(false);
                        m_lastError = "Failed to store key " + strKey + " since object not serializable " + oObject.GetType().ToString();
                        return false;
                    }
                    // StoreMode
                    // set -- unconditionally sets a given key with a given value (update_foo() should use this) 
                    // add -- adds to the cache, only if it doesn't already exist (get_foo() should use this) 
                    // replace -- sets in the cache only if the key already exists (not as useful, only for completeness) 
                    if ( minutes == 0 )
                    {
                        if (!_MemcachedClient.Store(Enyim.Caching.Memcached.StoreMode.Set, strKey, oObject))
                        {
                            m_lastError = "Failed to store key " + strKey;
                            return false;
                        }
                    }
                    else
                    {
                        if (!_MemcachedClient.Store(Enyim.Caching.Memcached.StoreMode.Set, strKey, oObject, TimeSpan.FromMinutes(minutes)))
                        {
                            m_lastError = "Failed to store key " + strKey;
                            return false;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                if (exc.InnerException != null)
                    m_lastError = exc.InnerException.Message;
                else
                    m_lastError = exc.Message;
                return false;
            }
            return true;
        }

        private Object get(string strKey, out bool error )
        {
            error = false;
            try
            {
                if (_MemcachedClient != null)
                {
                    m_lastError = "";
                    return _MemcachedClient.Get (strKey);
                }
            }
            catch (Exception exc)
            {
                if (exc.InnerException != null)
                    m_lastError = exc.InnerException.Message;
                else
                    m_lastError = exc.Message;
                error = true;
            }
            return null;
        }

        private MemcachedClient _MemcachedClient;
        private string m_lastError = "IsEnable() not called yet";
 
    }
}
