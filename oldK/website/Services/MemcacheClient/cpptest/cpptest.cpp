#include <windows.h>
#ifdef _DEBUG
#import "..\\bin\\Debug\\MemCacheClient.tlb" 
#else
#import "..\\bin\\Release\\MemCacheClient.tlb" 
#endif

using namespace MemcacheClient;

void storeString( ICentralCachePtr &pp, LPCTSTR key, LPCTSTR value )
{
	BSTR bstr;
	if ( !pp->StoreString( key, value, 1 ) )
	{
		pp->LastError(&bstr);
		printf( "StoreString \"%s\" got error %S\n", key, bstr );
		::SysFreeString(bstr);
	}
	else
	{
		printf( "Stored \"%s\" ok\n", key );
	}
}
void printString( ICentralCachePtr &pp, LPCTSTR key )
{
	BOOL b;
	BSTR bstr;
	VARIANT_BOOL bb;
	b = pp->GetString( key, &bb, &bstr );
	if ( b )
	{
		if ( bb )
			printf( "value \"%s\" is \"%S\"\n", key, bstr );
		else
			printf ("value \"%s\" is not found\n", key );

		SysFreeString( bstr);
	}
	else
	{
		pp->LastError(&bstr);
		printf( "GetString of key \"%s\" got error %S\n", key, bstr );
		SysFreeString(bstr);
	}
}

void printInt( ICentralCachePtr &pp, LPCTSTR key )
{
	BOOL b;
	BSTR bstr;
	long i;
	VARIANT_BOOL bb;
	b = pp->GetInt( key, &bb, &i );
	if ( b )
	{
		if ( bb )
			printf( "int value \"%s\" is %d\n", key, i);
		else
			printf ("int value \"%s\" is not found\n", key );
	}
	else
	{
		pp->LastError(&bstr);
		printf( "GetInt of key \"%s\" got error %S\n", key, bstr );
		SysFreeString(bstr);
	}
}

void printDouble( ICentralCachePtr &pp, LPCTSTR key )
{
	BOOL b;
	BSTR bstr;
	double i;
	VARIANT_BOOL bb;
	b = pp->GetDouble( key, &bb, &i );
	if ( b )
	{
		if ( bb )
			printf( "double value \"%s\" is %.4f\n", key, i);
		else
			printf ("double value \"%s\" is not found\n", key );
	}
	else
	{
		pp->LastError(&bstr);
		printf( "GetDouble of key \"%s\" got error %S\n", key, bstr );
		SysFreeString(bstr);
	}
}

void main()
{
	::CoInitialize(NULL);

	ICentralCachePtr pp;
	HRESULT hr  = pp.CreateInstance( "Kaneva.MemcacheClient.CentralCache");

	printf( "HR for creating COM object is 0x%x\n", hr );
	if ( SUCCEEDED( hr ) )
	{
		BSTR bstr;
		VARIANT_BOOL bb;
		BOOL b = pp->Initialize( &bb );
		if ( b )
			printf ("IsEnabled returned true with enabled set to %d\n", bb );
		else
		{
			pp->LastError( &bstr );
			printf ("IsEnabled got error %S\n", bstr);
			::SysFreeString(bstr);
		}

		printf( "\n---------------\n" );
		storeString( pp, "delme", "remove this string next step" );
		storeString( pp, "flushme", "flush all to remove this" );
		printString( pp, "delme" );
		printString( pp, "flushme" );

		if ( !pp->Remove( "delme" ) )
		{
			pp->LastError(&bstr);
			printf( "Remove got error %S\n", bstr );
			::SysFreeString(bstr);
		}
		else
		{
			printf( "Remove \"delme\" ok\n" );
		}
		printString( pp, "delme" );
		if ( !pp->FlushAll() )
		{
			pp->LastError(&bstr);
			printf( "FlushAll got error %S\n", bstr );
			::SysFreeString(bstr);
		}
		else
		{
			printf( "FlushAll ok\n" );
		}
		printString( pp, "flushme" );


		printf( "\n---------------\n" );
		storeString( pp, "test", "this is a string" );
		storeString( pp, "testNumPi", "3.14159" );
		storeString( pp, "testNumInt", "2680" );

		if ( !pp->StoreInt( "testInt", 1234, 1 ) )
		{
			pp->LastError(&bstr);
			printf( "StoreInt got error %S\n", bstr );
			::SysFreeString(bstr);
		}
		else
		{
			printf( "Stored \"testInt\" ok\n" );
		}

		if ( !pp->StoreDouble( "testDouble", 567.987, 1 ) )
		{
			pp->LastError(&bstr);
			printf( "StoreDouble got error %S\n", bstr );
			::SysFreeString(bstr);
		}
		else
		{
			printf( "Stored \"testDouble\" ok\n" );
		}

		printf( "\n---------------\n" );
		printString(pp, "test");
		printInt(pp,"testInt");
		printDouble(pp,"testDouble");
		printf( "\n--------------- Get numbers as string\n" );
		printString(pp, "testInt");
		printString(pp, "testDouble");
		printf( "\n--------------- Get strings as numbers\n" );
		printInt(pp,"test");
		printDouble(pp,"test");
		printInt(pp,"testNumPi");
		printDouble(pp,"testNumPi");
		printInt(pp,"testNumInt");
		printDouble(pp,"testNumInt");

		printf( "\n---------------\n" );
		printf( "...waiting 60 sec for entries to expire...\n" );
		printf( "\n---------------\n" );
		::Sleep( 61000 );

		printString(pp, "test");
		printInt(pp,"testInt");
		printDouble(pp,"testDouble");
		printf( "\n---------------\n" );

	}

	char s[100];
	printf ( "\n\nPress enter to end test\n" );
	gets_s(s, _countof(s));

	::CoUninitialize();
}
