// kgptest.ccc : Defines the entry point for the console acclication.
//

#include "stdafx.h"
#include "CentralCache.h"

#define CHECK_RET( fn ) { if (!fn) printf ("Error: %s\n", cc->LastError() ); }

void printString( CentralCache *pp, LPCTSTR key )
{
	bool b, bb;
	string bstr;
	b = pp->Get( key, bb, bstr );
	if ( b )
	{
		if ( bb )
			printf( "value \"%s\" is \"%s\"\n", key, bstr.c_str() );
		else
			printf ("value \"%s\" is not found\n", key );
	}
	else
	{
		printf( "GetString of key \"%s\" got error %s\n", key, pp->LastError() );
	}
}


void printInt( CentralCache *pp, LPCTSTR key )
{
	bool b;
	long i;
	bool bb;
	b = pp->Get( key, bb, i );
	if ( b )
	{
		if ( bb )
			printf( "int value \"%s\" is %d\n", key, i);
		else
			printf ("int value \"%s\" is not found\n", key );
	}
	else
	{
		printf( "GetInt of key \"%s\" got error %s\n", key, pp->LastError() );
	}
}

void printDouble( CentralCache *pp, LPCTSTR key )
{
	bool b;
	double i;
	bool bb;
	b = pp->Get( key, bb, i );
	if ( b )
	{
		if ( bb )
			printf( "double value \"%s\" is %.4f\n", key, i);
		else
			printf ("double value \"%s\" is not found\n", key );
	}
	else
	{
		printf( "GetDouble of key \"%s\" got error %s\n", key, pp->LastError() );
	}
}


void zctest( CentralCache *cc )
{
	printf( "\n---------------\n" );
	printString( cc, "wok.zc.pt.805306376,12271,0,0,0;" );
	printString( cc, "wok.zc.zones.805306376,12271" );

	if ( !cc->Remove( "wok.zc.pt.805306376,12271,0,0,0;" ) )
		printf( "Failed to remove wok.zc.pt.805306376,12271,0,0,0; %s\n", cc->LastError() );
	if ( !cc->Remove( "wok.zc.zones.805306376,12271" ) )
		printf( "Failed to remove wok.zc.zones.805306376,12271 %s\n", cc->LastError() );
}

int _tmain(int argc, _TCHAR* argv[])
{
	::CoInitialize(0);

	CentralCache *cc = new CentralCache();

	if ( !cc->Initialize() )
	{
		printf( "Error initializing: %s\n", cc->LastError() );
	}
	else
	{
		zctest(cc);

		string bstr;

		printf( "\n---------------\n" );
		CHECK_RET( cc->Store( "delme", "remove this string next step", 1 ) );
		CHECK_RET( cc->Store( "flushme", "flush all or wait 1 min to remove this", 1 ) );
		printString( cc, "delme" );
		printString( cc, "flushme" );

		if ( !cc->Remove( "delme" ) )
		{
			printf( "Remove got error %s\n", cc->LastError() );
		}
		else
		{
			printf( "Remove \"delme\" ok\n" );
		}
		printString( cc, "delme" );
		/*
		if ( !cc->FlushAll() )
		{
			cc->LastError(&bstr);
			printf( "FlushAll got error %S\n", bstr );
			::SysFreeString(bstr);
		}
		else
		{
			printf( "FlushAll ok\n" );
		}
		printString( cc, "flushme" );
		*/

		printf( "\n---------------\n" );
		CHECK_RET( cc->Store( "test", "this is a string", 1 ));
		CHECK_RET( cc->Store( "testNumPi", "3.14159", 1 ));
		CHECK_RET( cc->Store( "testNumInt", "2680", 1 ));

		if ( !cc->Store( "testInt", 1234L, 1 ) )
		{
			printf( "StoreInt got error %s\n", cc->LastError() );
		}
		else
		{
			printf( "Stored \"testInt\" ok\n" );
		}

		if ( !cc->Store( "testDouble", 567.987, 1 ) )
		{
			printf( "StoreDouble got error %s\n", cc->LastError() );
		}
		else
		{
			printf( "Stored \"testDouble\" ok\n" );
		}

		printf( "\n---------------\n" );
		printString(cc, "test");
		printInt(cc,"testInt");
		printDouble(cc,"testDouble");
		printf( "\n--------------- Get numbers as string\n" );
		printString(cc, "testInt");
		printString(cc, "testDouble");
		printf( "\n--------------- Get strings as numbers\n" );
		printInt(cc,"test");
		printDouble(cc,"test");
		printInt(cc,"testNumPi");
		printDouble(cc,"testNumPi");
		printInt(cc,"testNumInt");
		printDouble(cc,"testNumInt");

		printf( "\n---------------\n" );
		printf( "...waiting 60 sec for entries to expire...\n" );
		printf( "\n---------------\n" );
		::Sleep( 61000 );

		printString(cc, "test");
		printInt(cc,"testInt");
		printDouble(cc,"testDouble");
		printf( "\n---------------\n" );

	}

	char s[100];

	printf ( "\n\nPress enter to end test\n" );
	gets_s(s, _countof(s));


	::CoUninitialize();

	return 0;
}

