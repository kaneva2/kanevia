<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="gameDirectoryResults.ascx.cs" Inherits="KlausEnt.KEP.Developer.gameDirectoryResults" %>
<!--<%@ Register TagPrefix="Kaneva" TagName="SearchFilter" Src="../usercontrols/SearchFilter.ascx" %>-->
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<!--<link href="../css/new.css" type="text/css" rel="stylesheet">-->
<!--<link href="../css/shadow.css" type="text/css" rel="stylesheet">-->
<!--[if IE]> <style type="text/css">@import "../css/new_IE.css";</style> <![endif]-->

<script src="../jscript/prototype.js" type="text/javascript"></script>
<script src="../jscript/scriptaculous/scriptaculous.js" type="text/javascript"></script>

<div id="contentWide">

		<div id="main">

			<div id="main_content">				

												
				<div id="colSplit" class="twoCol">

					<div class="column seventy" id="colLeft">
																		

						<table border="0" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td align="center">
						
										<ajax:ajaxpanel id="ajpSearch1" runat="server">
										
										<!-- basic search -->
										<table cellpadding="0" cellspacing="0" border="0" width="98%" align="center">
											<tr>
												<td style="width:80px" align="left"><span id="spnFindTitle" runat="server" style="font-weight:bold">Find Stars</span></td>
												<td style="width:250px"><asp:textbox id="txtKeywords" Width="300px" maxlength="50" runat="server"></asp:textbox>
													<span id="spnSearchInst" runat="server"></span>
												</td>
												<td width="90" align="center"><asp:button runat="server" id="btnSearch" text="Search" onclick="btnSearch_Click"/></td>
											</tr>
											<tr>
												<td valign="top" class="small" colspan="3">
													
													<table border="0" cellspacing="0" cellpadding="0" width="100%">
														<tr style="padding-top:10px">
															<td align="left">
																<div id="divRestricted" runat="server">Show Restricted Content: <asp:label id="lblShowRestricted" runat="server">No</asp:label>
																<asp:hyperlink id="hlChangeProfile" runat="server">Change Profile Setting</asp:hyperlink></div>
															</td>
															<td><asp:checkbox id="chkPhotoRequired" runat="server" checked />Must Contain Thumbnail Photo</td>
														</tr>
												</table>
			
												</td>
											</tr>
										</table>
										
										</ajax:ajaxpanel>
										
								</td>
							</tr>
						</table>
	
	
						<table border="0" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td valign="top" align="center">
								
									<ajax:ajaxpanel id="Ajaxpanel2" runat="server">
									
										<table border="0" cellspacing="0" cellpadding="0" width="92%" class="nopadding">
											<tr>
												<td width="170" rowspan="2"><h1 class="starsDir png">Stars</h1></td>
												<td height="16" class="searchnav" align="right"><kaneva:searchfilter visible="false" runat="server" id="filterTop" loc="top" hidethumbview="true" showfilterlists="false" assetsperpagelist="12,24,40" />
												</td>
											</tr>
											<tr>
												<td height="16">
													<table cellpadding="0" cellspacing="0" border="0" width="100%">
														<tr>
															<td class="searchnav" align="right" valign="bottom" nowrap>
																<kaneva:pager runat="server" isajaxmode="True" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td colspan="2"><h2><span id="spnSearchFilterDesc" runat="server"></span></h2></td>
											</tr>
										</table>
																																						  
										<!-- MEDIA THUMB VIEW -->
									<!--	<asp:datalist visible="False" runat="server" enableviewstate="True" 
											width="99%" showfooter="False" id="dlSTARsThumb"
											cellpadding="0" cellspacing="0" border="0" itemstyle-width="25%" 
											itemstyle-horizontalalign="Center" repeatcolumns="4" 
											repeatdirection="Horizontal" cssclass="thumb_table">
						
											<itemtemplate>
																																						
												<div class="framesize-medium">
													<div id="Div1" class="restricted" runat="server" visible='<%# IsMature (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "game_rating_id")))%>'></div>
													<div class="frame">
														<span class="ct"><span class="cl"></span></span>
														<div class="imgconstrain">
														    <a title='View <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_name").ToString ())%>' href='<%# GetGameDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "game_id")))%>'>
																<img border="0" src='<%#GetGameImageURL (DataBinder.Eval(Container.DataItem, "game_image_path").ToString () ,"me")%>' border="0"/>
															</a>
														</div>	
														<span class="cb"><span class="cl"></span></span>
													</div>
												</div>
												<p><a title='View <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_name").ToString ())%>' href='<%# GetGameDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "game_id")))%>'>
													<%#TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_name").ToString ()), 16) %></a></p>
												<p>Owner:<a href='<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "owner_username").ToString ()) %>' title="<%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "owner_username").ToString()) %>"><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "owner_username").ToString(), 10) %></a></p> 
												<p>Owner: <b><%#TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "owner_username").ToString(), 10) %></b></p>
												<p class="small">Pop: <%# ShowGamePopulation(Convert.ToInt32(DataBinder.Eval (Container.DataItem, "number_of_players"))) %> | Raves: <%# DataBinder.Eval (Container.DataItem, "number_of_raves") %></p>
												<p id="P1" class="small" runat="server" visible="<%#IsAdministrator () || IsCSR ()%>">																 
													<asp:hyperlink visible='<%#IsAdministrator () || IsCSR ()%>' runat="server" navigateurl='<%#GetGameEditLink (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "game_id")))%>' id="Hyperlink1" name="Hyperlink1">edit</asp:hyperlink>&nbsp;
													<asp:hyperlink visible='<%#IsAdministrator () || IsCSR ()%>' runat="server" navigateurl='<%#GetDeleteScript ((int) DataBinder.Eval(Container.DataItem, "game_id")) %>' id="Hyperlink2" name="Hyperlink2">delete</asp:hyperlink>&nbsp;
													<asp:hyperlink visible="<%#IsAdministrator () || IsCSR ()%>" runat="server" navigateurl='<%# "javascript:RestrictAsset(" + DataBinder.Eval (Container.DataItem, "game_id").ToString () + ");" %>' id="lnkRestrict">restrict</asp:hyperlink>
												</p>																																			 
																																
											</itemtemplate>																					
										
										</asp:datalist>-->
										<!-- END MEDIA THUMB VIEW -->


			    <!-- MEDIA DETAIL VIEW -->
			    <asp:datalist visible="true" runat="server" enableviewstate="False" showfooter="False" width="590" id="dlSTARsDetail" style="margin-top:10px;"
				    cellpadding="5" cellspacing="0" border="0" itemstyle-horizontalalign="Center" repeatcolumns="2" repeatdirection="Horizontal" cssclass="detail_table">
					
				    <itemtemplate>	
				 		
					   
						    <table cellpadding="0" cellspacing="0" border="0" width="280">
							    <tr>
								    <td width="85" valign="top" align="center">
									    <div class="framesize-small">
										    <div id="Div2" class="restricted" runat="server" visible='<%# IsMature( Convert.ToInt32(DataBinder.Eval(Container.DataItem, "game_rating_id")) )%>'></div>
										    <div class="frame">
											    <span class="ct"><span class="cl"></span></span>
											    <div class="imgconstrain">
								                    <a title='View <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_name").ToString ())%>' href='<%# GetGameDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "game_id")))%>'>
									                    <img border="0" src='<%#GetGameImageURL (DataBinder.Eval(Container.DataItem, "game_image_path").ToString () ,"sm")%>' border="0"/>
									                </a>
											    </div>	
											    <span class="cb"><span class="cl"></span></span>
										    </div>
									    </div>
								    </td>																		 
								    <td align="left" valign="top" width="264" style="padding-bottom:0px;">
									    <p><a href='<%# GetGameDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "game_id")))%>' title='View <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_name").ToString ())%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_name").ToString ()), 20) %></a></p>
									    <div style="height:56px;">
									    <p><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_synopsis").ToString ()), 85) %></p>
									    </div>
								    </td>
							    </tr>
							    <tr>
								    <td colspan="2" style="padding:0 5px 0 10px;">
									    <table cellpadding="0" cellspacing="0" width="100%" border="0" class="stats">
										    <tr>
											    <td align="left" width="36%" style="padding-top:0px;">
												    <p class="small">Raves: <%# (Convert.ToDouble(DataBinder.Eval (Container.DataItem, "number_of_raves"))).ToString("N0") %></p>
												    <p class="small">Visits: <%# (Convert.ToDouble(DataBinder.Eval (Container.DataItem, "number_of_views"))).ToString("N0") %></p>
												    <p class="small">Pop: <%# ShowGamePopulation(Convert.ToInt32(DataBinder.Eval (Container.DataItem, "number_of_players"))) %> </p>
											    </td>
											    <td>
												    <table cellpadding="0" cellspacing="0" width="100%" border="0" class="stats">
													    <tr>
														    <td align="right" width="85%">
															    <!--<p class="small">Creator:</p>-->
															    <!--<p class="small"><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "owner_id").ToString ()) %> title="<%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "owner_id").ToString()) %>"><%# TruncateWithEllipsis (Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "owner_id").ToString()), 15) %></a></p>-->
														    </td>
														    <td>
															    <!--<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "owner_id").ToString ()) %> style="cursor:hand;" title="<%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "owner_id").ToString()) %>">
															    <img id="Img5" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "owner_id").ToString (), "me", "M")%>' width="30" height="30" border="0" hspace="5"/></a>-->
														    </td>
													    </tr>
												    </table>
											    </td>	
										    </tr>
									    </table>	
								    </td>
							    </tr>
						    </table>
						   
						
				    </itemtemplate>
			    </asp:datalist>
			    <!-- END MEDIA DETAIL VIEW -->
		 

										<div id="divNoResults" runat="server" visible="false"></div>
										
										<div class="formspacer"></div>
										
										<table cellpadding="0" cellspacing="0" border="0" width="92%">
											<tr>
												<td align="left">
													<asp:label id="lblResultsBottom" runat="server" visible="false"></asp:label>
												</td>
												<td class="searchnav" align="right" valign="bottom" nowrap>
													<kaneva:pager runat="server" isajaxmode="True" id="pgBottom" maxpagestodisplay="5" shownextprevlabels="true" />
												</td>
											</tr>
										</table>

									
									</ajax:ajaxpanel>

								</td>
							</tr>
						</table>
					</div>
				
				
				
					<div class="column thirty" id="colRight">
				
						<h2>Filter By:</h2>
						<ajax:ajaxpanel id="Ajaxpanel1" runat="server">
										
						<div id="sortlinks">																	
							<ul>
								<li class="header">Categories</li>
								<asp:repeater id="rptCategories" runat="server">
								<itemtemplate>
									<li id="liCategory" runat="server"><asp:linkbutton id="lbCategory" runat="server" /></li>		
								</itemtemplate>		   
								</asp:repeater>
							</ul>
							<ul id="ulTime" runat="server">
								<li class="header">Time</li>
								<li id="liTime_Today" runat="server"><asp:linkbutton text="Today" id="lbTime_Today" commandname="today" oncommand="lbTime_Click" tooltip="View communities added today" runat="server" /></li>
								<li id="liTime_Week" runat="server"><asp:linkbutton text="This Week" id="lbTime_Week" commandname="week" oncommand="lbTime_Click" tooltip="View communities added within the past week" runat="server" /></li>
								<li id="liTime_Month" runat="server"><asp:linkbutton text="This Month" id="lbTime_Month" commandname="month" oncommand="lbTime_Click" tooltip="View communities added within the past month" runat="server" /></li>
								<li id="liTime_All" runat="server"><asp:linkbutton text="All Time" id="lbTime_All" commandname="alltime" oncommand="lbTime_Click" tooltip="View all communities" runat="server" /></li>
							</ul>
							<ul>
								<li class="header" id="liBrowseLabel" runat="server">Popularity</li>
								<li id="liBrowse_Population" runat="server"><asp:linkbutton text="Current Population" id="lbBrowse_Population" commandname="current_population" oncommand="lbBrowse_Click" tooltip="View results by current population" runat="server" /></li>
								<li id="liBrowse_Raves" runat="server"><asp:linkbutton text="Most Raves" id="lbBrowse_Raves" commandname="raves" oncommand="lbBrowse_Click" tooltip="View results by number of raves" runat="server" /></li>
								<li id="liBrowse_Visits" runat="server"><asp:linkbutton text="Most Visits" id="lbBrowse_Visits" commandname="visits" oncommand="lbBrowse_Click" tooltip="View results by number of visits" runat="server" /></li>
								<li id="liBrowse_Newest" runat="server"><asp:linkbutton text="Newest" id="lbBrowse_Newest" commandname="newest" oncommand="lbBrowse_Click" tooltip="View results by newest added" runat="server" /></li>
							</ul>
						</div>
					
					</ajax:ajaxpanel>
			 
				
				</div>
				
				
			</div>
		
		</div>
	</div>
