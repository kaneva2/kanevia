<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Navigation.ascx.cs" Inherits="KlausEnt.KEP.Developer.usercontrols.Navigation" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<div id="container">
    <div id="header">
        <div id="logo"><span><asp:LinkButton CausesValidation="False" CommandName="home" OnCommand="Nav_Command" runat="server" visible="true" title="Star Platform Home" id="lb_Home"></asp:LinkButton></span></div>
        <ul id="tertiary">
		    <li id="nav-signin" class="<%= SetClass("SIGN_IN") %><%= SetClass("LOG_OUT") %>"><asp:LinkButton runat="server" CausesValidation="False" CommandName="signin" OnCommand="Nav_Command" id="aSignIn">Sign In</asp:LinkButton><asp:linkbutton id="lnkLogout" onclick="lnkLogout_Click" runat="server" text="Sign Out" causesvalidation="false">Sign Out</asp:linkbutton></li>
	    </ul>
        <div class="clear"><!-- clear the floats --></div>
            <div id="navigation">
                <div id="navDivDev" runat="server">
                    <ul id="nav2" >
                        <li id="nav-stars" class="<%= SetClass("MY_GAMES") %>"><asp:LinkButton CausesValidation="False" CommandName="mygames" OnCommand="Nav_Command" runat="server" visible="true" id="aMyGames">My Star</asp:LinkButton></li>
                        
                        <li id="nav-directory" class="<%= SetClass("DIRECTORY") %>"><asp:LinkButton CausesValidation="False" runat="server" CommandName="directory" OnCommand="Nav_Command" id="aDirectory">Star Directory</asp:LinkButton></li>
                    </ul>
 		            <ul id="secondary">
                        <li id="nav-acctmanagement" class="<%= SetClass("MANAGEMENT") %>"><asp:LinkButton CausesValidation="False" runat="server" CommandName="management" OnCommand="Nav_Command" id="aManagement"> Account Management</asp:LinkButton></li>
	                        <li id="nav-resources" class="<%= SetClass("RESOURCES") %>"><asp:LinkButton CausesValidation="False" runat="server" CommandName="resources" OnCommand="Nav_Command" id="aResources">Star Resources</asp:LinkButton></li>
                    </ul>
               </div>
		        
	    </div>
	    <asp:placeholder runat="server" id="phSubNav" />	
    </div>			
</div>
				
			
