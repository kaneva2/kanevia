///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Collections;
using KlausEnt.KEP.Kaneva;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Developer
{
	/// <summary>
	/// Summary description for BaseUserControl.
	/// </summary>
	public class BaseUserControl : System.Web.UI.UserControl
	{
        #region Declarations
        //Common Functions
        private DeveloperCommonFunctions _devComnFunc; 

        // Sorting
        protected Button btnSort;
        protected HtmlInputHidden hidSortColumn;
        #endregion


        /// <summary>
        /// Gets an instance of the Developer Common Functions class
        /// </summary>
        /// <returns>DeveloperCommonFunctions</returns>
        public DeveloperCommonFunctions GetDCFInstance
        {
            get
            {
                if (_devComnFunc == null)
                {
                    _devComnFunc = new DeveloperCommonFunctions();
                }
                return _devComnFunc;
            }
        }

        /// <summary>
        /// Return the personal channel link
        /// </summary>
        /// <param name="channelId"></param>
        /// <returns></returns>
        public static string GetPersonalChannelUrl(string nameNoSpaces)
        {
            return KanevaGlobals.GetPersonalChannelUrl(nameNoSpaces);
        }

        // ***********************************************
        // Data Presentation functions
        // ***********************************************
        #region DataGrid Helper Functions

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        public virtual int DEFAULT_PAGE
        {
            get
            {
                return 1;
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        public virtual string DEFAULT_SORT
        {
            get
            {
                return "name";
            }
        }

        /// <summary>
        /// DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        public virtual string DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }


        //overridable paging event for gridview
        protected virtual void gridview_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            currentPage = e.NewPageIndex;
        }

        //overridable sorting event for gridview
        protected virtual void gridview_Sorting(Object sender, GridViewSortEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
        }

        //function to switch the sort order given a new sort command - for gridview
        private void SortSwitch(string newSort)
        {
            if (CurrentSort == newSort)
            {
                if (CurrentSortOrder == "DESC")
                {
                    CurrentSortOrder = "ASC";
                }
                else
                {
                    CurrentSortOrder = "DESC";
                }
            }
            else
            {
                CurrentSort = newSort;
                CurrentSortOrder = "DESC";
            }
        }

        /// <summary>
        /// They clicked to sort a column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void btnSort_Click(object sender, System.EventArgs e)
        {
            // Set the sort order
            if (CurrentSortOrder == "DESC")
            {
                CurrentSortOrder = "ASC";
            }
            else if (CurrentSortOrder == "ASC")
            {
                CurrentSortOrder = "DESC";
            }
            else
            {
                CurrentSortOrder = "ASC";
            }

            // Set the sort column
            if (!CurrentSort.Equals(hidSortColumn.Value))
            {
                // Changing sort expression, so set to ASC
                CurrentSortOrder = "ASC";
                CurrentSort = hidSortColumn.Value;
            }
        }

        /// <summary>
        /// Set Header Sort column Text
        /// </summary>
        protected void SetHeaderSortText(DataGrid dgrdToSort)
        {
            DataGridColumn dgrdColumn;
            string strippedHeader;

            // Which arrow to use?
            string arrowImage = "arrow_sort_up.gif";
            if (CurrentSortOrder.Equals("DESC"))
            {
                arrowImage = "arrow_sort.gif";
            }

            // Loop through all sortable columns
            for (int i = 0; i < dgrdToSort.Columns.Count; i++)
            {
                dgrdColumn = dgrdToSort.Columns[i];

                // Is it a sortable column?
                if (dgrdColumn.SortExpression.Length > 0 && dgrdToSort.AllowSorting)
                {
                    strippedHeader = dgrdColumn.HeaderText;

                    if (dgrdToSort.EnableViewState)
                    {
                        strippedHeader = strippedHeader.Replace("<img src=" + ResolveUrl("~/images/arrow_sort.gif") + " border=0/>", "");
                        strippedHeader = strippedHeader.Replace("<img src=" + ResolveUrl("~/images/arrow_sort_up.gif") + " border=0/>", "");
                    }

                    // Is this column the current sorted?
                    if (CurrentSort.Equals(dgrdColumn.SortExpression))
                    {
                        dgrdColumn.HeaderText = "<a href='#' onclick='javascript:document.frmMain." + hidSortColumn.ClientID + ".value=\"" + dgrdColumn.SortExpression + "\";javascript:document.frmMain." + btnSort.ClientID + ".click();'>" + strippedHeader + "<img src=" + ResolveUrl("~/images/" + arrowImage) + " border=0/></a>";
                    }
                    else
                    {
                        dgrdColumn.HeaderText = "<a href='#' onclick='javascript:document.frmMain." + hidSortColumn.ClientID + ".value=\"" + dgrdColumn.SortExpression + "\";javascript:document.frmMain." + btnSort.ClientID + ".click();'>" + strippedHeader + "</a>";
                    }
                }
            }
        }

        /// <summary>
        /// Current page of datagrid
        /// </summary>
        public int currentPage
        {
            get
            {
                if (ViewState["_currentPage"] == null)
                {
                    ViewState["_currentPage"] = DEFAULT_PAGE;
                }
                return (int)ViewState["_currentPage"];
            }
            set
            {
                ViewState["_currentPage"] = value;
            }
        }

        /// <summary>
        /// Current sort expression
        /// </summary>
        public string CurrentSort
        {
            get
            {
                if (ViewState["cs"] == null)
                {
                    return DEFAULT_SORT;
                }
                else
                {
                    return ViewState["cs"].ToString();
                }
            }
            set
            {
                ViewState["cs"] = value;
            }
        }

        /// <summary>
        /// Current sort order
        /// </summary>
        public string CurrentSortOrder
        {
            get
            {
                if (ViewState["cso"] == null)
                {
                    return DEFAULT_SORT_ORDER;
                }
                else
                {
                    return ViewState["cso"].ToString();
                }
            }
            set
            {
                ViewState["cso"] = value;
            }
        }

        #endregion

        // ***********************************************
        // Formatting functions
        // ***********************************************
        #region Formatting functions

        /// <summary>
        /// Format currency
        /// </summary>
        /// <param name="dblCurrency"></param>
        /// <returns></returns>
        protected string FormatCurrencyForTextBox(Object dblCurrency)
        {
            return GetDCFInstance.FormatCurrencyForTextBox(dblCurrency);
        }

        /// <summary>
        /// Format the date into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public string FormatDate(Object dtDate)
        {
            return GetDCFInstance.FormatDate(dtDate);
        }

        public string FormatDateNumbersOnly(Object dtDate)
        {
            return GetDCFInstance.FormatDateNumbersOnly(dtDate);
        }

        /// <summary>
        /// Format the date into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public string FormatDateTime(Object dtDate)
        {
            return GetDCFInstance.FormatDateTime(dtDate);
        }
        /// <summary>
        /// Format date as a time span
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        protected string FormatDateTimeSpan(Object dtDate)
        {
            return GetDCFInstance.FormatDateTimeSpan(dtDate);
        }
        /// <summary>
        /// Format image size
        /// </summary>
        /// <param name="imageSize"></param>
        /// <returns></returns>
        public string FormatImageSize(Object imageSize)
        {
            return GetDCFInstance.FormatImageSize(imageSize);
        }

        /// <summary>
        /// FormatCurrency
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public string FormatCurrency(Object amount)
        {
            return GetDCFInstance.FormatCurrency(amount);
        }

        /// <summary>
        /// Format the date into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public string FormatDateTimeSpan(Object dtDate, Object dtDateNow)
        {
            return GetDCFInstance.FormatDateTimeSpan(dtDate, dtDateNow);
        }

        /// <summary>
        /// TruncateWithEllipsis
        /// </summary>
        public string TruncateWithEllipsis(string text, int length)
        {
            return GetDCFInstance.TruncateWithEllipsis(text, length);
        }

        /// <summary>
        /// TruncateWithEllipsis
        /// </summary>
        public string FormatSecondsAsTime(Object seconds)
        {
            return GetDCFInstance.FormatSecondsAsTime(seconds);
        }

        /// <summary>
        /// Get a string
        /// </summary>
        public string FormatString(Object dtColumn, int statusId)
        {
            return GetDCFInstance.FormatString(dtColumn, statusId);
        }

        /// <summary>
        /// Get a string
        /// </summary>
        public string FormatString(Object dtColumn, int statusId, string notSetText)
        {
            return GetDCFInstance.FormatString(dtColumn, statusId, notSetText);
        }


        #endregion

        // ***********************************************
        // Javascript functions
        // ***********************************************
        #region JavaScripts

        /// <summary>
        /// Return the delete javascript
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetDeleteScript(int gameId)
        {
            return GetDCFInstance.GetDeleteScript(gameId);
        }

        /// <summary>
        /// AddKeepAlive - Avoid timeout issues
        /// </summary>
        public void AddKeepAlive()
        {
            // Set timer 30 seconds before the timeout will expire
            int int_MilliSecondsTimeOut = (this.Session.Timeout * 60000) - 30000;

            string str_Script = "<script type='text/javascript'>\n" +
                "//Number of Reconnects\n" +
                "var count=0;\n" +
                //Maximum reconnects setting\n" +
                "var max = 5;\n\n" +
                "function Reconnect()\n" +
                "{\n" +
                "	count++;\n" +
                "	if (count < max)\n" +
                "	{\n" +
                "		window.status = 'Session refreshed ' + count.toString()+' time(s)' ;\n" +
                "		var img = new Image(1,1);\n" +
                "		img.src = '" + ResolveUrl("~/sessionKeepAlive.aspx") + "';\n" +
                "	}\n" +
                "}\n\n" +
                "window.setInterval('Reconnect()'," + int_MilliSecondsTimeOut.ToString() + @");" +
                "</script>";

            if (!Page.ClientScript.IsClientScriptBlockRegistered(this.GetType(), "Reconnect"))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Reconnect", str_Script);
            }
        }

        /// Show an error message on startup
        /// </summary>
        /// <param name="errorMessage"></param>
        protected void ShowErrorOnStartup(string errorMessage)
        {
            ShowErrorOnStartup(errorMessage, true);
        }

        /// <summary>
        /// Show an error message on startup
        /// </summary>
        /// <param name="errorMessage"></param>
        protected void ShowErrorOnStartup(string errorMessage, bool bShowPleaseMessage)
        {
            string scriptString = "<script language=JavaScript>";
            if (bShowPleaseMessage)
            {
                scriptString += "alert ('Please correct the following errors:\\n\\n" + errorMessage + "');";
            }
            else
            {
                scriptString += "alert ('" + errorMessage + "');";
            }
            scriptString += "</script>";

            if (!Page.ClientScript.IsClientScriptBlockRegistered(this.GetType(), "ShowError"))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowError", scriptString);
            }
        }

        #endregion

        // ***********************************************
        // Helper functions
        // ***********************************************
        #region Helper Functions

        /// <summary>
        /// Converts server players count to established levels
        /// </summary>
        public string ShowGamePopulation(int count)
        {
            return GetDCFInstance.ShowGamePopulation(count);
        }
        
        /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public string GetGameStatusDescription(int statusId)
        {
            return GetDCFInstance.GetGameStatusDescription(statusId);
        }

        /// <summary>
        /// Queries server visibility data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public string GetGameVisibiltyDescription(int visibilityId)
        {
            return GetDCFInstance.GetGameVisibiltyDescription(visibilityId);
        }

        /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public string GetGameRatingDescription(int ratingId)
        {
            return GetDCFInstance.GetGameRatingDescription(ratingId);
        }

        /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public int IsGameMature(int ratingId)
        {
            return GetDCFInstance.IsGameMature(ratingId);
        }

        /// <summary>
        /// Queries server status data set to get the description
        /// </summary>
        public string GetServerStatusDescription(int statusId)
        {
            return GetDCFInstance.GetServerStatusDescription(statusId);
        }


        /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public string GetGameAccessDescription(int accessId)
        {
            return GetDCFInstance.GetGameAccessDescription(accessId);
        }

        /// <summary>
        /// Get the status link
        /// </summary>
        public string GetStatusLink(int torrentStatus)
        {
            return "javascript:window.open('" + ResolveUrl("~/asset/publishStatus.aspx#" + torrentStatus + "','add','toolbar=no,width=600,height=450,menubar=no,scrollbars=yes,status=yes').focus();return false;");
        }

        /// <summary>
        /// Get game edit Link
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetGameEditLink(int gameId)
        {
            return Page.ResolveUrl("~/games/gameEdit.aspx?gameId=" + gameId);
        }

        /// <summary>
        /// Get game edit Link
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetGameDetailsLink(int gameId)
        {
            return Page.ResolveUrl("~/games/gameDetails.aspx?gameId=" + gameId);
            //return Page.ResolveUrl("~/games/" + GetDCFInstance.GetGameDetailsLink(gameId));
        }

        public DevelopmentCompanyFacade GetDevelopmentCompanyFacade()
        {
            return GetDCFInstance.GetDevelopmentCompanyFacade;
        }
        
        public GameFacade GetGameFacade()
        {
            return GetDCFInstance.GetGameFacade;
        }

        public CommentFacade GetCommentFacade()
        {
            return GetDCFInstance.GetCommentFacade;
        }

        public BlastFacade GetBlastFacade()
        {
            return GetDCFInstance.GetBlastFacade;
        }

        public GameDeveloperFacade GetGameDeveloperFacade()
        {
            return GetDCFInstance.GetGameDeveloperFacade;
        }

        public UserFacade GetUserFacade()
        {
             return GetDCFInstance.GetUserFacade;
        }

        public ListItem CreateListItem(string name, string theValue)
        {
            return GetDCFInstance.CreateListItem(name, theValue);
        }

        public ListItem CreateListItem(string name, string theValue, bool bold)
        {
           return GetDCFInstance.CreateListItem(name, theValue, bold);
        }

        /// <summary>
        /// SetDropDownIndex
        /// </summary>
        public void SetDropDownIndex(DropDownList drp, string theValue)
        {
            GetDCFInstance.SetDropDownIndex(drp, theValue);
        }

        /// <summary>
        /// SetDropDownIndex
        /// </summary>
        public void SetDropDownIndex(DropDownList drp, string theValue, bool bAddValue)
        {
            GetDCFInstance.SetDropDownIndex(drp, theValue, bAddValue, theValue);
        }

        /// <summary>
        /// SetDropDownIndex
        /// </summary>
        public void SetDropDownIndex(DropDownList drp, string theValue, bool bAddValue, string theDisplayName)
        {
            GetDCFInstance.SetDropDownIndex(drp, theValue, bAddValue, theDisplayName);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return GetDCFInstance.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return GetDCFInstance.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }


        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
        {
            return GetDCFInstance.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
        {
            return GetDCFInstance.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts);
        }

        /// <summary>
        /// AddHrefNewWindow
        /// </summary>
        /// <param name="theText"></param>
        /// <returns></returns>
        public string AddHrefNewWindow(string theText)
        {
            return GetDCFInstance.AddHrefNewWindow(theText);
        }

        /// <summary>
        /// GetLoginURL
        /// </summary>
        /// <returns></returns>
        public string GetLoginURL()
        {
            return ResolveUrl("~/login.aspx?logretURL=" + Server.UrlEncode(GetCurrentURL()));
        }

        /// <summary>
        /// GetLoginURL
        /// </summary>
        /// <returns></returns>
        public string GetHomePageURL()
        {
            return ResolveUrl("~/vision-3d-internet.aspx");
        }

        /// <summary>
        /// GetCurrentURL
        /// </summary>
        /// <returns></returns>
        public string GetCurrentURL()
        {
            return Request.CurrentExecutionFilePath + "?" + Request.QueryString.ToString();
        }

        /// <summary>
        /// Redirect to login page
        /// </summary>
        public void RedirectToLogin()
        {
            Response.Redirect(GetLoginURL());
        }

        /// <summary>
        /// Redirect to home page
        /// </summary>
        public void RedirectToHomePage()
        {
            Response.Redirect(GetHomePageURL());
        }

        /// <summary>
        /// removeHTML
        /// </summary>
        /// <param name="strMessageInput"></param>
        /// <returns></returns>
        public string RemoveHTML(string strMessageInput)
        {
            return GetDCFInstance.RemoveHTML(strMessageInput);
        }

        #endregion

        // ***********************************************
        // Security Functions
        // ***********************************************
        #region Security Functions

        public bool IsUserAllowedToEditGame(int gameId)
        {
            return GetDCFInstance.IsUserAllowedToEditGame(gameId);
        }

        /// <summary>
        /// Is the current session user a site administrator?
        /// </summary>
        /// <returns></returns>
        public bool IsAdministrator()
        {
            return GetDCFInstance.IsAdministrator();
        }

        /// <summary>
        /// Is this user a site administrator?
        /// </summary>
        /// <returns></returns>
        public bool IsAdministrator(int userId)
        {
            return GetDCFInstance.IsAdministrator(userId);
        }

        /// <summary>
        /// Is item a mature item?
        /// </summary>
        /// <returns>bool</returns>
        public bool IsMature(int gameRatingId)
        {
            return GetDCFInstance.IsMature(gameRatingId);
        }

        /// <summary>
        /// PassRequired
        /// </summary>
        public bool PassRequired(int accessTypeId)
        {
            return GetDCFInstance.PassRequired(accessTypeId);
        }

        /// <summary>
        /// Is this user a site administrator?
        /// </summary>
        /// <returns></returns>
        public bool IsAdministratorByUserRole(int role)
        {
            return GetDCFInstance.IsAdministratorByUserRole(role);
        }

        /// <summary>
        /// Is the current session user a CSR?
        /// </summary>
        /// <returns></returns>
        public bool IsCSR()
        {
            return GetDCFInstance.IsCSR();
        }

        /// <summary>
        /// Is this user a CSR?
        /// </summary>
        /// <returns></returns>
        public bool IsCSR(int userId)
        {
            return GetDCFInstance.IsCSR(userId);
        }

        /// <summary>
        /// Is this user a CSR?
        /// </summary>
        /// <returns></returns>
        public bool IsCSRByUserRole(int role)
        {
            return GetDCFInstance.IsCSRByUserRole(role);
        }

        /// <summary>
        /// Is the current session user a IT?
        /// </summary>
        /// <returns></returns>
        public bool IsIT()
        {
            return GetDCFInstance.IsIT();
        }

        /// <summary>
        /// Is this user a IT?
        /// </summary>
        /// <returns></returns>
        public bool IsIT(int userId)
        {
            return GetDCFInstance.IsIT(userId);
        }

        /// <summary>
        /// Is this user a IT?
        /// </summary>
        /// <returns></returns>
        public bool IsITByUserRole(int role)
        {
            return GetDCFInstance.IsITByUserRole(role);
        }

        /// <summary>
        /// determines if the user is a minor? 
        /// throws it exceptions
        /// </summary>
        /// <param name="age">age</param>
        /// <returns>bool</returns>
        public bool IsUserAMinor(int age)
        {
            return GetDCFInstance.IsUserAMinor(age);
        }

        /// <summary>
        /// determines if the user is a minor? 
        /// throws its exceptions
        /// </summary>
        /// <param name="birthday">birthday</param>
        /// <returns>bool</returns>
        public bool IsUserAMinor(DateTime birthday)
        {
            return GetDCFInstance.IsUserAMinor(birthday);
        }

        /// <summary>
        /// determines if the user is the games owner? 
        /// throws its exceptions
        /// </summary>
        /// <param name="birthday">birthday</param>
        /// <returns>bool</returns>
        public bool IsGameOwner(int gameId)
        {
            return GetDCFInstance.IsGameOwner(gameId);
        }

        /// <summary>
        /// Is the current user adult?
        /// </summary>
        /// <returns></returns>
        public bool IsCurrentUserAdult()
        {
            // Now we are showing all if they are not logged in.
            return IsCurrentUserAdult(true);
        }

        /// <summary>
        /// Is the current user adult?
        /// </summary>
        /// <returns></returns>
        public bool IsCurrentUserAdult(bool defaultForNotLoggedIn)
        {
            return GetDCFInstance.IsCurrentUserAdult(defaultForNotLoggedIn);
        }

        public string CleanJavascriptFull(string strText)
        {
            return GetDCFInstance.CleanJavascriptFull(strText);
        }

        #endregion

        // ***********************************************
        // Game Functions
        // ***********************************************
        #region Game Functions

        /// <summary>
        /// GetMyPublishedItemURL
        /// </summary>
        /// <returns></returns>
        public string GetMyGamesURL()
        {
            return ResolveUrl("~/game/gameLibrary.aspx");
        }

        /// <summary>
        /// GetAssetTags
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public string GetAssetTags(string keywords)
        {
            return GetDCFInstance.GetAssetTags(keywords, Page);
        }

        /// <summary>
        /// GetAssetTags
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public string GetAssetTags(string keywords, int type)
        {
            return GetDCFInstance.GetAssetTags(keywords, type, Page);
        }

        /// <summary>
        /// GetAssetCategoryList
        /// </summary>
        /// <param name="categories"></param>
        /// <returns></returns>
        public string GetAssetCategoryList(string categories)
        {
            return GetDCFInstance.GetAssetCategoryList(categories, Page);
        }

        /// <summary>
        /// Return the game image URL
        /// </summary>
        public string GetGameImageURL(string imagePath, string defaultSize)
        {
            return GetDCFInstance.GetGameImageURL(imagePath, defaultSize);
        }

        public void UploadGameImage(int userId, int itemId, HtmlInputFile browseTHUMB)
        {
            GetDCFInstance.UploadGameImage(userId, itemId, browseTHUMB);
        }


        /// <summary>
        /// Get Asset Details Link
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetAssetDetailsLink(int assetId)
        {
            return GetDCFInstance.GetAssetDetailsLink(assetId, Page);
        }

        #endregion

        // ***********************************************
        // User Functions
        // ***********************************************
        #region User Functions

        /// <summary>
        /// Return the current user id
        /// </summary>
        /// <returns></returns>
        public int GetUserId()
        {
            return DeveloperCommonFunctions.GetUserId();
        }

        public GameDeveloper GetCurrentUser()
        {
            return DeveloperCommonFunctions.CurrentUser;
        }

        public int AuthorizeUser(string email, string password, int gameId, ref int roleMembership, string UserHostAddress, bool bSendTrackerMessage)
        {
            return GetDCFInstance.AuthorizeUser(email, password, gameId, ref roleMembership, UserHostAddress, bSendTrackerMessage);
        }

        public int UpdateLastLogin(int userId, string UserHostAddress, string serverName)
        {
            return GetDCFInstance.UpdateLastLogin(userId, UserHostAddress, serverName);
        }

        /// <summary>
        /// Return the user image URL
        /// </summary>
        public string GetProfileImageURL(string imagePath)
        {
            return GetDCFInstance.GetProfileImageURL(imagePath);
        }

        public string GetOnlineText(string ustate)
        {
            //implement catch here to prevent error page from being displayed for a simple online stauts check
            //default to no status is error occurs
            try
            {
                string strUserState = DeveloperCommonFunctions.GetUserState(ustate);
                if (strUserState.Length > 0)
                {
                    return "<p class=\"online\">" + strUserState + "</p>";
                }
                else
                {
                    return "<p >&nbsp;</p>";
                }
            }
            catch (InvalidCastException)
            {
            }

            return "";
        }

        /// <summary>
        /// Return the user image URL
        /// </summary>
        public string GetProfileImageURL(string imagePath, string defaultSize, string gender)
        {
            return GetDCFInstance.GetProfileImageURL(imagePath, defaultSize, gender);
        }

        #endregion

        // ***********************************************
        // Bread Crumbs
        // ***********************************************
        #region Bread Crumbs
        /// <summary>
        /// Gets New BreadCrumb From Kaneva Frame Work
        /// </summary>
        /// <returns></returns>
        public BreadCrumb GetNewBreadCrumb(string crumbName, string URL, string sort, int pageNumber)
        {
            return new BreadCrumb(crumbName, URL, sort, pageNumber);
        }


        /// <summary>
        /// GetBreadCrumbs
        /// </summary>
        /// <returns></returns>
        public BreadCrumbs GetBreadCrumbs()
        {
            if (Session["breadCrumb"] == null)
            {
                BreadCrumbs breadCrumbs = new BreadCrumbs();
                Session["breadCrumb"] = breadCrumbs;
                return breadCrumbs;
            }

            return (BreadCrumbs)Session["breadCrumb"];
        }

        /// <summary>
        /// GetLastBreadCrumb
        /// </summary>
        /// <returns>the last entered bread crumb or null if none found</returns>
        public BreadCrumb GetLastBreadCrumb()
        {
            BreadCrumb crumb = null;
            BreadCrumbs crumbList = GetBreadCrumbs();
            if (crumbList.Count > 0)
            {
                crumb = crumbList.Item(crumbList.Count - 1);
            }
            return crumb;
        }


        /// <summary>
        /// ResetBreadCrumb
        /// </summary>
        public void ResetBreadCrumb()
        {
            Session["breadCrumb"] = null;
        }

        public void AddBreadCrumb(BreadCrumb newBreadCrumb)
        {
            BreadCrumbs breadCrumbs = GetBreadCrumbs();
            BreadCrumb breadCrumb;

            // See if this already exists, if it does, remove that one and add new one
            for (int i = 0; i < breadCrumbs.Count; i++)
            {
                breadCrumb = (BreadCrumb)breadCrumbs.Item(i);

                if (breadCrumb.Text.Equals(newBreadCrumb.Text))
                {
                    breadCrumbs.Remove(i);
                }
            }

            breadCrumbs.Add(newBreadCrumb);
        }


        /// <summary>
        /// DisplayBreadCrumb
        /// </summary>
        [Obsolete("This is for backward compatibilities, will be removed in next version")]
        public void DisplayBreadCrumb(PlaceHolder phBreadCrumb)
        {
        }

        public BreadCrumb GetBreadCrumbByName(string breadCrumbText)
        {
            BreadCrumbs breadCrumbs = GetBreadCrumbs();
            return breadCrumbs.FindByText(breadCrumbText);
        }

        /// <summary>
        /// NavigateBackToBreadCrumb
        /// </summary>
        public void NavigateBackToBreadCrumb(int backCount)
        {
            // Make sure it exists first
            BreadCrumbs breadCrumbs = GetBreadCrumbs();
            string qs = "";

            if (breadCrumbs.Count >= backCount)
            {
                BreadCrumb bcPage = (BreadCrumb)breadCrumbs.Item(breadCrumbs.Count - backCount);
                if (bcPage.PageNumber > 0)
                {
                    qs += "&" + Constants.PARAM_PAGE_NUMBER + "=" + bcPage.PageNumber;
                }
                if (bcPage.Sort.Length > 0)
                {
                    qs += "&" + Constants.PARAM_SORT + "=" + bcPage.Sort;
                }
                if (bcPage.SortOrder.Length > 0)
                {
                    qs += "&" + Constants.PARAM_SORT_ORDER + "=" + bcPage.SortOrder;
                }
                if (bcPage.Filter.Length > 0)
                {
                    qs += "&" + Constants.PARAM_FILTER + "=" + bcPage.Filter;
                }
                Response.Redirect(ResolveUrl(bcPage.Hyperlink) + qs);
            }
            else
            {
                // Go home
                RedirectToHomePage();
            }
        }

        public void NavigateBackToBreadCrumb(string breadCrumbText)
        {
            // Make sure it exists first
            BreadCrumbs breadCrumbs = GetBreadCrumbs();
            BreadCrumb requestedCrumb = breadCrumbs.FindByText(breadCrumbText);
            string qs = "";

            if (requestedCrumb != null)
            {
                if (requestedCrumb.PageNumber > 0)
                {
                    qs += "&" + Constants.PARAM_PAGE_NUMBER + "=" + requestedCrumb.PageNumber;
                }
                if (requestedCrumb.Sort.Length > 0)
                {
                    qs += "&" + Constants.PARAM_SORT + "=" + requestedCrumb.Sort;
                }
                if (requestedCrumb.SortOrder.Length > 0)
                {
                    qs += "&" + Constants.PARAM_SORT_ORDER + "=" + requestedCrumb.SortOrder;
                }
                if (requestedCrumb.Filter.Length > 0)
                {
                    qs += "&" + Constants.PARAM_FILTER + "=" + requestedCrumb.Filter;
                }
                Response.Redirect(ResolveUrl(requestedCrumb.Hyperlink) + qs);
            }
            else
            {
                // Go home
                RedirectToHomePage();
            }

        }
        #endregion

    }


}
