<%@ Control Language="c#" AutoEventWireup="false" Codebehind="SearchFilter.ascx.cs" Inherits="KlausEnt.KEP.Developer.SearchFilter" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<!-- Begin Filter --->
<table cellpadding="2" cellspacing="2" border="0" id="tblFilter" runat="server">
<tr id="trFilterList" runat="server">
<td align="center">
	<asp:linkbutton cssclass="glossaryLink" text="All" commandargument="" oncommand="lbSearch_Click"  runat="server" id="Linkbutton1"/>
	<asp:linkbutton cssclass="glossaryLink" text="0-9" commandargument="0-9" oncommand="lbSearch_Click"  runat="server" id="Linkbutton2"/>
	<asp:linkbutton cssclass="glossaryLink" text="Sym" tooltip="Symbols" commandargument="@" oncommand="lbSearch_Click"  runat="server" id="Linkbutton3"/>
	<asp:linkbutton cssclass="glossaryLink" text="A" commandargument="A" oncommand="lbSearch_Click"  runat="server" id="Linkbutton4"/>
	<asp:linkbutton cssclass="glossaryLink" text="B" commandargument="B" oncommand="lbSearch_Click"  runat="server" id="Linkbutton5"/>
	<asp:linkbutton cssclass="glossaryLink" text="C" commandargument="C" oncommand="lbSearch_Click"  runat="server" id="Linkbutton6"/>
	<asp:linkbutton cssclass="glossaryLink" text="D" commandargument="D" oncommand="lbSearch_Click"  runat="server" id="Linkbutton7"/>
	<asp:linkbutton cssclass="glossaryLink" text="E" commandargument="E" oncommand="lbSearch_Click"  runat="server" id="Linkbutton8"/>
	<asp:linkbutton cssclass="glossaryLink" text="F" commandargument="F" oncommand="lbSearch_Click"  runat="server" id="Linkbutton9"/>
	<asp:linkbutton cssclass="glossaryLink" text="G" commandargument="G" oncommand="lbSearch_Click"  runat="server" id="Linkbutton10"/>
	<asp:linkbutton cssclass="glossaryLink" text="H" commandargument="H" oncommand="lbSearch_Click"  runat="server" id="Linkbutton11"/>
	<asp:linkbutton cssclass="glossaryLink" text="I" commandargument="I" oncommand="lbSearch_Click"  runat="server" id="Linkbutton12"/>
	<asp:linkbutton cssclass="glossaryLink" text="J" commandargument="J" oncommand="lbSearch_Click"  runat="server" id="Linkbutton13"/>
	<asp:linkbutton cssclass="glossaryLink" text="K" commandargument="K" oncommand="lbSearch_Click"  runat="server" id="Linkbutton14"/>
	<asp:linkbutton cssclass="glossaryLink" text="L" commandargument="L" oncommand="lbSearch_Click"  runat="server" id="Linkbutton15"/>
	<asp:linkbutton cssclass="glossaryLink" text="M" commandargument="M" oncommand="lbSearch_Click"  runat="server" id="Linkbutton16"/>
	<asp:linkbutton cssclass="glossaryLink" text="N" commandargument="N" oncommand="lbSearch_Click"  runat="server" id="Linkbutton17"/>
	<asp:linkbutton cssclass="glossaryLink" text="O" commandargument="O" oncommand="lbSearch_Click"  runat="server" id="Linkbutton18"/>
	<asp:linkbutton cssclass="glossaryLink" text="P" commandargument="P" oncommand="lbSearch_Click"  runat="server" id="Linkbutton19"/>
	<asp:linkbutton cssclass="glossaryLink" text="Q" commandargument="Q" oncommand="lbSearch_Click"  runat="server" id="Linkbutton20"/>
	<asp:linkbutton cssclass="glossaryLink" text="R" commandargument="R" oncommand="lbSearch_Click"  runat="server" id="Linkbutton21"/>
	<asp:linkbutton cssclass="glossaryLink" text="S" commandargument="S" oncommand="lbSearch_Click"  runat="server" id="Linkbutton22"/>
	<asp:linkbutton cssclass="glossaryLink" text="T" commandargument="T" oncommand="lbSearch_Click"  runat="server" id="Linkbutton23"/>
	<asp:linkbutton cssclass="glossaryLink" text="U" commandargument="U" oncommand="lbSearch_Click"  runat="server" id="Linkbutton24"/>
	<asp:linkbutton cssclass="glossaryLink" text="V" commandargument="V" oncommand="lbSearch_Click"  runat="server" id="Linkbutton25"/>
	<asp:linkbutton cssclass="glossaryLink" text="W" commandargument="W" oncommand="lbSearch_Click"  runat="server" id="Linkbutton26"/>
	<asp:linkbutton cssclass="glossaryLink" text="X" commandargument="X" oncommand="lbSearch_Click"  runat="server" id="Linkbutton27"/>
	<asp:linkbutton cssclass="glossaryLink" text="Y" commandargument="Y" oncommand="lbSearch_Click"  runat="server" id="Linkbutton28"/>
	<asp:linkbutton cssclass="glossaryLink" text="Z" commandargument="Z" oncommand="lbSearch_Click"  runat="server" id="Linkbutton29"/>
</td>
</tr>
<tr>
<td align="center">
	<span class="insideBoxText11">Display:</span>&nbsp;<span id="spanViewFilter" runat="server"><asp:dropdownlist runat="server" size="1" class="formKanevaSelect" id="drpView" autopostback="True" onselectedindexchanged="drpView_Change">											
	</asp:dropdownlist> <span class="insideBoxText11">W/</span></span> <asp:dropdownlist runat="server" size="1" class="formKanevaSelect" id="drpPages" autopostback="True" onselectedindexchanged="drpPages_Change">	
	</asp:dropdownlist> <span class="insideBoxText11"></span>
</td>
</tr>	
</table>
