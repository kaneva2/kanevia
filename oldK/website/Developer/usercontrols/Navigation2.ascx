<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Navigation2.ascx.cs" Inherits="KlausEnt.KEP.Developer.Navigation2" %>
<div id="navigation">
    <center>
    <div id="navBar">
        <div id="logo">
            <a href="http://<%= System.Configuration.ConfigurationManager.AppSettings["SiteName"] %>/default.aspx" ><img border="0" src="~/images/Brochure/kaneva_developers_logo_152x55.gif" alt="Developer" runat="server"  /></a>
        </div>
        <div id="menu">
            <%-- <ul id="menuList">
                <li><div class="navItemContainer"><h5><a href="#" >Sign in</a></h5></div></li>
                <li><div class="navItemContainer"><h5><a href="#" >Manage My Worlds</a></h5></div></li>
                <li><div class="navItemContainer"><h5><a href="#" >Learn More</a></h5></div></li>
                <li><div class="navItemContainer"><h5><a href="#" >Downloads & Resources</a></h5></div></li>
                <li><div class="navItemContainerEnd"><h5><a href="#" >Forum</a></h5></div></li>
            </ul>--%>
            <!--<div class="navItemContainer"><h5><a href="#" >Sign in</a></h5></div>-->
            <div class="navItemContainer"><h5><a id="A1" href="~/default.aspx" runat="server" >Developer Home</a></h5></div>
            <div class="navItemContainer"><h5><a href="#" target="_blank" id="manageLink" runat="server">My Worlds</a></h5></div>            
            <div class="navItemContainer"><h5><a href="http://www.kaneva.com/developer/TreasureHuntContest.aspx" id="challengeLink" runat="server">Developer Challenge</a></h5></div>
            <div class="navItemContainer"><h5><a href="#" target="_blank" id="downNResLink" runat="server">Resources</a></h5></div>
            <div class="navItemContainerEnd"><h5><a href="#" target="_blank" id="forumLink" runat="server">Forums</a></h5></div>
        </div>
    </div> 
    </center>   
</div>
