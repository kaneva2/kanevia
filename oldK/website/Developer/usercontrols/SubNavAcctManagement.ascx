<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SubNavAcctManagement.ascx.cs" Inherits="KlausEnt.KEP.Developer.usercontrols.SubNavAcctManagement" %>
		<div id="subnavigation">
			<ul>
				<li class="<%= SetClass("PROFILE") %>"><!--<asp:LinkButton ID="lb_profile" CausesValidation="False" runat="server" CommandName="profile" OnCommand="Nav_CommandMot">My Profile</asp:LinkButton>--></li>
				<li class="<%= SetClass("TEAM") %>"><asp:LinkButton ID="lb_team" CausesValidation="False" runat="server" CommandName="team" OnCommand="Nav_CommandMot">My Team</asp:LinkButton></li>
				<li class="<%= SetClass("ACCOUNT") %>"><!--<asp:LinkButton ID="lb_account" CausesValidation="False" runat="server" CommandName="account" OnCommand="Nav_CommandMot">My Account</asp:LinkButton>--></li>
				<li class="<%= SetClass("INCOME") %>"><!--<asp:LinkButton ID="lb_income" CausesValidation="False" runat="server" CommandName="income" OnCommand="Nav_CommandMot">My Income</asp:LinkButton>--></li>
			</ul>
		</div>
		<asp:placeholder runat="server" id="phSubNav" />