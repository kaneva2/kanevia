///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using MagicAjax;
using Kaneva.BusinessLayer.Facade;
using Kaneva.DataLayer.DataObjects;
using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Developer
{
    public partial class gameDirectoryResults : BaseUserControl
    {
        #region Declarations

        protected SearchFilter filterTop;
        protected LinkButton lbBrowse_Adds, lbBrowse_Newest, lbRestrict;
        protected LinkButton lbType_Videos, lbType_Photos, lbType_Music, lbType_Games, lbType_Patterns, lbType_Widgets, lbType_TV;
        protected Label lblCount;
        protected Literal litAdTest, litPng, litJSOpenConst;
        protected AdRotator arHeader;
        protected Literal litHeaderText;
        protected HtmlImage imgRegister;
        protected PlaceHolder phBreadCrumb;
        protected HtmlContainerControl liBrowse_Adds, liBrowse_Newest;
        protected HtmlContainerControl liType_Videos, liType_Photos, liType_Music, liType_Games, liType_Patterns, liType_TV, liType_Widgets;
        protected HtmlContainerControl ifrmTagCloud;
        protected HtmlContainerControl spnHotTip, spnInstruction;
        protected HtmlContainerControl d1, d2, d3;	 //Hot Tips div tags
        protected HtmlInputHidden AssetId;
        private string cCSS_CLASS_SELECTED = "selected";
        private const string cORDER_BY_DEFAULT = Kaneva.Constants.SEARCH_ORDERBY_CURRENT_POPULATION;
        private const int cNEW_WITHIN_DEFAULT = (int)Kaneva.Constants.eSEARCH_TIME_FRAME.MONTH;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Page Load
        
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                //get any parameters for configuration
                GetRequestParams();
                InitializeSearch(true);
            }

            txtKeywords.Attributes.Add("onkeydown", "CheckKey()");

            // Is user logged in?
            if (Request.IsAuthenticated)
            {
                if (IsCurrentUserAdult())
                {
                    hlChangeProfile.NavigateUrl = ResolveUrl(DeveloperCommonFunctions.GetKanevaURL + "/mykaneva/settings.aspx");
                    lblShowRestricted.Text = DeveloperCommonFunctions.CurrentUser.UserInfo.ShowMature ? "Yes" : "No";
                }
                else
                {
                    divRestricted.Visible = false;
                }
            }
            else
            {
                hlChangeProfile.NavigateUrl = GetLoginURL();
            }

        }

        #endregion

        #region Primary Methods

        /// <summary>
        /// InitializeNewSearch
        /// </summary>
        private void InitializeSearch(bool Set_Keyword)
        {
            if (!IsPostBack)
            {
                // Get the Category list
                GetStarsCategories();
            }

            // Set Search keyword
            if (Set_Keyword)
            {
                SetSearchKeywords();
            }

            // Set OrderBy Type
            SetOrderByType();

            // Set Within Type
            SetWithinTimeFrameType();

            // These functions set the class to indicate to the user
            // what options are currently set
            SetPopularityLinks();
            SetTimeLinks();

            BindData(1, "");
        }

        /// <summary>
        /// BindData
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="filter"></param>
        private void BindData(int pageNumber, string filter)
        {
            //// How many results do we display?
            int pgSize = filterTop.NumberOfPages;

            //// Set current page
            pgTop.CurrentPageNumber = pageNumber;
            pgBottom.CurrentPageNumber = pageNumber;

            //// Clear existing results
            dlSTARsThumb.Visible = false;
            dlSTARsDetail.Visible = false;

            divNoResults.Visible = false;

            spnSearchFilterDesc.InnerText = GetSearchFilterSummary();

            pgSize = 10;

            SearchSTARs(filter, pageNumber, pgSize);

            // Show Pager
            pgTop.DrawControl();
            pgBottom.DrawControl();

        }

        /// <summary>
        /// SearchSTARs
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="filter"></param>
        /// <param name="pgNumber"></param>
        /// <param name="pgSize"></param>
        private void SearchSTARs(string filter, int pgNumber, int pgSize)
        {
            string categories = string.Empty;
            /*if (GameCategory_Id > 0)
            {
                categories = GameCategory_Id.ToString();
            }*/
            if ((GameCategory_Id != null) && (GameCategory_Id.Trim() != ""))
            {
                categories = GameCategory_Id;
            }

            PagedDataTable pds = null;

            pds = GetGameFacade().SearchForGames(Server.HtmlEncode(Search_String), filter, categories, DeveloperCommonFunctions.CurrentUser.UserInfo.ShowMature, chkPhotoRequired.Checked, New_Within, OwnerId, SetOrderByValue(), pgNumber, pgSize);

            // this is used to limit the number of pages to 5 when browsing
            if (Search_String.Length > 0)
            {
                pgTop.NumberOfPages = Math.Ceiling((double)pds.TotalCount / pgSize).ToString();
                pgBottom.NumberOfPages = Math.Ceiling((double)pds.TotalCount / pgSize).ToString();

                // Set up browse area
                ulTime.Visible = false;
                liBrowseLabel.InnerText = "Sort by";
            }
            else
            {
                if (Math.Ceiling((double)pds.TotalCount / pgSize) < 5 || IsAdministrator())
                {
                    pgTop.NumberOfPages = Math.Ceiling((double)pds.TotalCount / pgSize).ToString();
                    pgBottom.NumberOfPages = Math.Ceiling((double)pds.TotalCount / pgSize).ToString();
                }
                else
                {
                    pgTop.NumberOfPages = "5";
                    pgBottom.NumberOfPages = "5";
                }

                // Set up browse area
                ulTime.Visible = true;
                liBrowseLabel.InnerText = "Browse";
            }

            // Display thumbnail or detail results?
            if (pds.TotalCount > 0)
            {
                dlSTARsDetail.Visible = true;
                dlSTARsDetail.DataSource = pds;
                dlSTARsDetail.DataBind();
            }
            else
            {
                ShowNoResults();
            }

            ShowResultsCount((int)pds.TotalCount, pgTop.CurrentPageNumber, filterTop.NumberOfPages, pds.Rows.Count);
        }
        
        #endregion 

        #region Helper Methods

        /// <summary>
        /// ShowResultsCount
        /// </summary>
        /// <param name="total_count"></param>
        /// <param name="page_num"></param>
        /// <param name="count_per_page"></param>
        /// <param name="current_page_count"></param>
        private void ShowResultsCount(int total_count, int page_num, int count_per_page, int current_page_count)
        {
            // Display the showing #-# of ### count
            string results = string.Empty;

            if (total_count > 0)
            {
                results = "Results " + Kaneva.KanevaGlobals.GetResultsText(total_count, page_num, count_per_page,
                    current_page_count, true, "%START%-%END% of %TOTAL%");
            }

            lblResultsBottom.Text = results;
        }
 
        /// <summary>
        /// ShowNoResults
        /// </summary>
        private void ShowNoResults()
        {
            divNoResults.InnerHtml = "<div class='noresults'><br/>Your search did not find any matching Stars.</div>";
            divNoResults.Visible = true;
        }
        /// <summary>
        /// SetOrderByValue
        /// </summary>
        private string SetOrderByValue()
        {
            switch (Order_By)
            {
                case Kaneva.Constants.SEARCH_ORDERBY_RAVES:
                    CurrentSort = "number_of_raves";
                    break;
                case Kaneva.Constants.SEARCH_ORDERBY_VISITS:
                    CurrentSort = "number_of_views";
                    break;
                case Kaneva.Constants.SEARCH_ORDERBY_CURRENT_POPULATION:
                    CurrentSort = "number_of_players";
                    break;
                default:
                    CurrentSort = "game_creation_date";
                    break;
            }

            // Set sort and order by
            return CurrentSort;
        }

        /// <summary>
        /// GetStarsCategories
        /// </summary>
        private void GetStarsCategories()
        {
            List<CommunityCategory> dtCategories = WebCache.GetGameCategories();

            rptCategories.DataSource = dtCategories;
            rptCategories.DataBind();
        }

        private void SetOrderByType()
        {
            try
            {
                if (Order_By == string.Empty) // Set order by if not already in viewstate
                {
                    if (Request[Kaneva.Constants.QUERY_STRING_ORDERBY] != null && Request[Kaneva.Constants.QUERY_STRING_ORDERBY] != string.Empty)
                    {
                        switch (Server.UrlDecode(Request[Kaneva.Constants.QUERY_STRING_ORDERBY].ToString().ToLower()))
                        {
                            case Kaneva.Constants.SEARCH_ORDERBY_CURRENT_POPULATION:
                            case Kaneva.Constants.SEARCH_ORDERBY_RAVES:
                            case Kaneva.Constants.SEARCH_ORDERBY_VISITS:
                            case Kaneva.Constants.SEARCH_ORDERBY_NEWEST:
                                Order_By = Server.UrlDecode(Request[Kaneva.Constants.QUERY_STRING_ORDERBY].ToString());
                                break;
                            default:
                                Order_By = cORDER_BY_DEFAULT;
                                break;
                        }
                    }
                    else
                    {
                        Order_By = cORDER_BY_DEFAULT;
                    }
                }
            }
            catch (Exception)
            {
                Order_By = cORDER_BY_DEFAULT;
            }
        }

        /// <summary>
        /// SetWithinTimeFrameType
        /// </summary>
        private void SetWithinTimeFrameType()
        {
            try
            {
                if (New_Within < 0) // Get the type from the radio button selected
                {
                    if (Request[Kaneva.Constants.QUERY_STRING_WITHIN] != null && Request[Kaneva.Constants.QUERY_STRING_WITHIN] != string.Empty)
                    {
                        switch (Convert.ToInt32(Request[Kaneva.Constants.QUERY_STRING_WITHIN]))
                        {
                            case 0:
                            case 1:
                            case 7:
                            case 30:
                                New_Within = Convert.ToInt32(Request[Kaneva.Constants.QUERY_STRING_WITHIN]);
                                break;
                            default:
                                New_Within = cNEW_WITHIN_DEFAULT;
                                break;
                        }
                    }
                    else
                    {
                        New_Within = cNEW_WITHIN_DEFAULT;   //all time
                    }
                }
            }
            catch (Exception)
            {
                New_Within = cNEW_WITHIN_DEFAULT;	 //all time
            }
        }
        
        /// <summary>
        /// SetSearchKeywords
        /// </summary>
        private void SetSearchKeywords()
        {
            try
            {
                if (Search_String == string.Empty)
                {
                    if (Request[Kaneva.Constants.QUERY_STRING_KEYWORD] != null && Request[Kaneva.Constants.QUERY_STRING_KEYWORD] != string.Empty)
                    {
                        Search_String = Server.UrlDecode(Request[Kaneva.Constants.QUERY_STRING_KEYWORD].ToString());

                        Order_By = cORDER_BY_DEFAULT;
                        New_Within = 0;

                        txtKeywords.Text = Search_String;
                        txtKeywords.Style.Add("color", "#535353");
                    }
                    else
                    {
                        Search_String = string.Empty;
                    }
                }
            }
            catch (Exception)
            {
                Search_String = string.Empty;
            }
        }

        /// <summary>
        /// SetPopularityLinks
        /// </summary>
        private void SetPopularityLinks()
        {
            liBrowse_Population.Attributes.Add("class", "");
            liBrowse_Raves.Attributes.Add("class", "");
            liBrowse_Visits.Attributes.Add("class", "");
            liBrowse_Newest.Attributes.Add("class", "");

            switch (Order_By)
            {
                case Kaneva.Constants.SEARCH_ORDERBY_CURRENT_POPULATION:
                    liBrowse_Population.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case Kaneva.Constants.SEARCH_ORDERBY_RAVES:
                    liBrowse_Raves.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case Kaneva.Constants.SEARCH_ORDERBY_VISITS:
                    liBrowse_Visits.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                default:
                    liBrowse_Newest.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
            }
        }
         
        /// <summary>
        /// SetTimeLinks
        /// </summary>
        private void SetTimeLinks()
        {
            //reset the list item classes
            liTime_Today.Attributes.Add("class", "");
            liTime_Week.Attributes.Add("class", "");
            liTime_Month.Attributes.Add("class", "");
            liTime_All.Attributes.Add("class", "");

            //set the new selected item's class
            switch (New_Within)
            {
                case (int)Kaneva.Constants.eSEARCH_TIME_FRAME.TODAY:
                    liTime_Today.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case (int)Kaneva.Constants.eSEARCH_TIME_FRAME.WEEK:
                    liTime_Week.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case (int)Kaneva.Constants.eSEARCH_TIME_FRAME.MONTH:
                    liTime_Month.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case (int)Kaneva.Constants.eSEARCH_TIME_FRAME.ALL_TIME:
                    liTime_All.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
            }
        }

        /// <summary>
        /// GetSearchFilterSummary
        /// </summary>
        private string GetSearchFilterSummary()
        {
            string browse_txt = string.Empty;
            string time_txt = string.Empty;

            switch (Order_By)
            {
                case Kaneva.Constants.SEARCH_ORDERBY_VISITS:
                    browse_txt = "Most Visits";
                    break;
                case Kaneva.Constants.SEARCH_ORDERBY_RAVES:
                    browse_txt = "Most Raves";
                    break;
                case Kaneva.Constants.SEARCH_ORDERBY_NEWEST:
                    browse_txt = "Recently Added";
                    break;
                case Kaneva.Constants.SEARCH_ORDERBY_CURRENT_POPULATION:
                default:
                    browse_txt = "Highest Population";
                    break;
            }

            switch (New_Within)
            {
                case (int)Kaneva.Constants.eSEARCH_TIME_FRAME.TODAY:
                    time_txt = "Today";
                    break;
                case (int)Kaneva.Constants.eSEARCH_TIME_FRAME.WEEK:
                    time_txt = "This Week";
                    break;
                case (int)Kaneva.Constants.eSEARCH_TIME_FRAME.MONTH:
                    time_txt = "This Month";
                    break;
                case (int)Kaneva.Constants.eSEARCH_TIME_FRAME.ALL_TIME:
                    time_txt = "All Time";
                    break;
            }

            return browse_txt + " - " + time_txt;
        }

        private bool GetRequestParams()
        {
            bool retVal = false;
            if (Request["catId"] != null)
            {
                //GameCategory_Id = Convert.ToInt32(Request["catId"]);
                GameCategory_Id = Request["catId"].ToString();
                New_Within = (int)Kaneva.Constants.eSEARCH_TIME_FRAME.ALL_TIME;
                chkPhotoRequired.Checked = false;
                retVal = true;
            }
            try
            {
                if (Request["typeId"] != null)
                {
                    OwnerId = Convert.ToInt32(Request["typeId"]);
                    retVal = true;
                }
            }
            catch (FormatException)
            {
            }
            return retVal;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Execute when the user clicks the the Search button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (DeveloperCommonFunctions.ContainsInjectScripts(txtKeywords.Text))
            {
                m_logger.Warn("User " + DeveloperCommonFunctions.GetUserId() + " tried to script from IP " + Common.GetVisitorIPAddress());
                ShowErrorOnStartup("Your input contains invalid scripting, please remove script code and try again.", true);
                return;
            }

            //Search_String = txtKeywords.Text.ToLower().Trim() != cTEXTBOX_INSTRUCTION ? txtKeywords.Text : "";
            Search_String = txtKeywords.Text.Trim();

            Order_By = Kaneva.Constants.SEARCH_ORDERBY_NEWEST;
            if (Search_String.Length > 0)
            {
                Order_By = Kaneva.Constants.SEARCH_ORDERBY_RELEVANCE;
            }

            New_Within = (int)Kaneva.Constants.eSEARCH_TIME_FRAME.ALL_TIME;
            //GameCategory_Id = 0;
            GameCategory_Id = null;

            lblResultsBottom.Visible = true;
            if (Search_String.Length == 0)
            {
                lblResultsBottom.Visible = false;
                New_Within = 30;
            }

            InitializeSearch(false);
        }

        /// <summary>
        /// Execute when the user clicks the Browse type link button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbBrowse_Click(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case Kaneva.Constants.SEARCH_ORDERBY_VISITS:
                case Kaneva.Constants.SEARCH_ORDERBY_CURRENT_POPULATION:
                case Kaneva.Constants.SEARCH_ORDERBY_RAVES: 
                    Order_By = e.CommandName;
                    break;
                default:
                    Order_By = Kaneva.Constants.SEARCH_ORDERBY_NEWEST;
                    break;
            }

            SetPopularityLinks();

            BindData(1, filterTop.CurrentFilter);
        }
 
        /// <summary>
        /// Execute when the user clicks the Time range link button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbTime_Click(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "today":
                    New_Within = (int)Kaneva.Constants.eSEARCH_TIME_FRAME.TODAY;
                    break;
                case "week":
                    New_Within = (int)Kaneva.Constants.eSEARCH_TIME_FRAME.WEEK;
                    break;
                case "month":
                    New_Within = (int)Kaneva.Constants.eSEARCH_TIME_FRAME.MONTH;
                    break;
                case "alltime":
                    New_Within = (int)Kaneva.Constants.eSEARCH_TIME_FRAME.ALL_TIME;
                    break;
            }

            SetTimeLinks();

            BindData(1, filterTop.CurrentFilter);
        }

        /// <summary>
        /// rptCategories_ItemDataBound
        /// </summary>
        private void rptCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lbCategory = (LinkButton)e.Item.FindControl("lbCategory");
                lbCategory.CommandArgument = DataBinder.Eval(e.Item.DataItem, "category_id").ToString();
                lbCategory.CommandName = DataBinder.Eval(e.Item.DataItem, "description").ToString();
                lbCategory.Text = DataBinder.Eval(e.Item.DataItem, "description").ToString();
                lbCategory.ToolTip = "View items in the " + DataBinder.Eval(e.Item.DataItem, "description").ToString() + " category";

                HtmlContainerControl liCategory = (HtmlContainerControl)e.Item.FindControl("liCategory");

                // select All as the default
                if (lbCategory.CommandArgument == "0" && liCategory != null)
                {
                    liCategory.Attributes.Add("class", cCSS_CLASS_SELECTED);
                }
            }
        }

        /// <summary>
        /// rptAssets_ItemCommand
        /// </summary>
        private void rptCategories_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string command = e.CommandName;
            string cmdarg = e.CommandArgument.ToString();

            //GameCategory_Id = Convert.ToInt32(e.CommandArgument);
            GameCategory_Id = e.CommandArgument.ToString();

            LinkButton lbCat = (LinkButton)e.Item.FindControl("lbCategory");

            Repeater rptCategories = (Repeater)e.Item.Parent;
            if (rptCategories != null)
            {
                for (int i = 0; i < rptCategories.Items.Count; i++)
                {
                    if (rptCategories.Items[i].ItemType == ListItemType.Item || rptCategories.Items[i].ItemType == ListItemType.AlternatingItem)
                    {
                        HtmlContainerControl liCategory = (HtmlContainerControl)rptCategories.Items[i].FindControl("liCategory");
                        liCategory.Attributes.Add("class", "");
                    }
                }
            }

            if (lbCat != null)
            {
                //GameCategory_Id = Convert.ToInt32(lbCat.CommandArgument);
                GameCategory_Id = lbCat.CommandArgument.ToString();

                ((HtmlContainerControl)lbCat.Parent).Attributes.Add("class", cCSS_CLASS_SELECTED);
            }

            BindData(1, filterTop.CurrentFilter);
        }

        /// <summary>
        /// Delete an asset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_Asset(Object sender, EventArgs e)
        {
            if (IsAdministrator() || IsCSR())
            {
                //StoreUtility.DeleteAsset(Convert.ToInt32(AssetId.Value), GetUserId());
                //pg_PageChange(this, new PageChangeEventArgs(pgTop.CurrentPageNumber));
                //BindData (1, filterTop.CurrentFilter);
            }
        }

        /// <summary>
        /// Restrict an asset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRestrictAsset_Click(Object sender, EventArgs e)
        {
            if (IsAdministrator() || IsCSR())
            {
                //StoreUtility.UpdateAssetRestriction(Convert.ToInt32(AssetId.Value), true);
                //pg_PageChange (this, new PageChangeEventArgs (pgTop.CurrentPageNumber));
                //BindData(pgTop.CurrentPageNumber, filterTop.CurrentFilter);
            }
        }

        #endregion

        #region Pager & Filter control events
                
                /// <summary>
                /// Execute when the user selects a page change link from the Pager control
                /// </summary>
                /// <param name="sender"></param>
                /// <param name="e"></param>
                private void pg_PageChange(object sender, PageChangeEventArgs e)
                {
                    BindData(e.PageNumber, filterTop.CurrentFilter);
                }
                /// <summary>
                /// Execute when the user selects an item from the Store Filter
                /// Items to display drop down list
                /// </summary>
                /// <param name="sender"></param>
                /// <param name="e"></param>
                private void FilterChanged(object sender, FilterChangedEventArgs e)
                {
                    BindData(1, e.Filter);
                }
                
        #endregion

        #region Properties

        /*
                private int STARs_Type_Id
                {
                    set
                    {
                        ViewState["Star_Type_Id"] = value;
                    }
                    get
                    {
                        if (ViewState["Star_Type_Id"] == null)
                        {
                            return -1;
                        }
                        else
                        {
                            return Convert.ToInt32(ViewState["Star_Type_Id"]);
                        }
                    }
                }
        */
                /// <summary>
                /// OwnerId
                /// </summary>
                private int OwnerId
                {
                    get
                    {
                        if (ViewState["ownerid"] == null)
                        {
                            ViewState["ownerid"] = -1;
                        }
                        return (int)ViewState["ownerid"];
                    }
                    set
                    {
                        ViewState["ownerid"] = value;
                    }
                }


                /// <summary>
                /// New_Within
                /// </summary>
                private int New_Within
                {
                    set
                    {
                        ViewState["New_Within"] = value;
                    }
                    get
                    {
                        if (ViewState["New_Within"] == null)
                        {
                            return -1;
                        }
                        else
                        {
                            return Convert.ToInt32(ViewState["New_Within"]);
                        }
                    }
                }


                /// <summary>
                /// Search_String
                /// </summary>
                private string Search_String
                {
                    set
                    {
                        ViewState["Search_String"] = value.Trim();
                    }
                    get
                    {
                        if (ViewState["Search_String"] == null)
                        {
                            return string.Empty; //cORDER_BY_DEFAULT;
                        }
                        else
                        {
                            return ViewState["Search_String"].ToString();
                        }
                    }
                }

                /// <summary>
                /// orderBy
                /// </summary>
                private string Order_By
                {
                    set
                    {
                        ViewState["Order_By"] = value;
                    }
                    get
                    {
                        if (ViewState["Order_By"] == null)
                        {
                            return string.Empty; //cORDER_BY_DEFAULT;
                        }
                        else
                        {
                            return ViewState["Order_By"].ToString();
                        }
                    }
                }

              /*  /// <summary>
                /// WOKCategory_Id
                /// </summary>
                private int GameCategory_Id
                {
                    set
                    {
                        ViewState["GameCategory_Id"] = value;
                    }
                    get
                    {
                        if (ViewState["GameCategory_Id"] == null)
                        {
                            return 0;
                        }
                        else
                        {
                            return Convert.ToInt32 (ViewState["GameCategory_Id"]);
                        }
                    }
                }*/

                /// <summary>
                /// GameCategory_Id
                /// </summary>
                private string GameCategory_Id
                {
                    set
                    {
                        ViewState["GameCategory_Id"] = value;
                    }
                    get
                    {
                        if (ViewState["GameCategory_Id"] == null)
                        {
                            return null;
                        }
                        else
                        {
                            return ViewState["GameCategory_Id"].ToString();
                        }
                    }
                }

        # endregion

        #region Web Form Designer generated code

                override protected void OnInit(EventArgs e)
                {
                    //
                    // CODEGEN: This call is required by the ASP.NET Web Form Designer.
                    //
                    InitializeComponent();
                    base.OnInit(e);
                }

                /// <summary>
                /// Required method for Designer support - do not modify
                /// the contents of this method with the code editor.
                /// </summary>
                private void InitializeComponent()
                {
                    this.Load += new System.EventHandler(this.Page_Load);
                    rptCategories.ItemCommand += new RepeaterCommandEventHandler(rptCategories_ItemCommand);
                    rptCategories.ItemDataBound += new RepeaterItemEventHandler(rptCategories_ItemDataBound);
                    pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
                    pgBottom.PageChanged += new PageChangeEventHandler(pg_PageChange);
                    filterTop.FilterChanged += new FilterChangedEventHandler(FilterChanged);
                }
        #endregion 
    
    }
}
