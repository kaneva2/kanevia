<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="relatedGames.ascx.cs" Inherits="KlausEnt.KEP.Developer.relatedGames" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<div id="divContainer" runat="server">
    <ajax:ajaxpanel id="Ajaxpanel1" runat="server">
        <div id="related_header">
	        <ul class="related_tabs">
		        <li style="width:86px;" id="liRelated" runat="server"><asp:linkbutton id="lbRelated" runat="server" commandname="related" oncommand="RelatedTab_Changed">Related</asp:linkbutton></li> 
		        <li style="width:170px;" id="liMore" runat="server"><asp:linkbutton id="lbMore" runat="server" commandname="more" oncommand="RelatedTab_Changed">More from this member</asp:linkbutton></li> 
	        </ul>
        </div>
        <div id="related_content">
            <div class="relatedWrapper">
		        <asp:datalist runat="server" enableviewstate="True" showfooter="False" width="100%" id="dlRelated" cellpadding="0" cellspacing="0" repeatcolumns="1" repeatdirection="Horizontal">
			        <itemstyle horizontalalign="left" verticalalign="top" />
			        <itemtemplate>
			            <div class="itemWrapper">
						    <div class="framesize-small frameFloat">
					            <div class="passrequired" visible='<%# PassRequired (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "game_access_id")))%>' runat="server" id="divPassRequired"></div>
					            <div class="frame ">
						            <span class="ct"><span class="cl"></span></span>							
							            <div class="imgconstrain">
							                <a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_name").ToString ())%>' href='<%# GetGameDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "game_id")))%>'>
									            <img border="0" src="<%#GetGameImageURL (DataBinder.Eval(Container.DataItem, "game_image_path").ToString () ,"me")%>" border="0" />
								            </a>
							            </div>																																				
						            <span class="cb"><span class="cl"></span></span>
					            </div>
						     </div>
						     <div class="relatedUser">
					            <a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_name").ToString ())%>' href='<%# GetGameDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "game_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_name").ToString ()), 100) %></a>
					            <br />
					            <!-- Q: is there an a tag missing here? -->Shared by: <b><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "owner_username").ToString(), 10) %></b></a>
					            <br />
					            Visits: <%# DataBinder.Eval(Container.DataItem, "number_of_players")%> | Raves: <%# DataBinder.Eval(Container.DataItem, "number_of_raves")%>
					        </div>
                        </div>		
			        </itemtemplate>
		        </asp:datalist>
		    </div>
		    <div class="relatedWrapper">
		        <asp:datalist runat="server" enableviewstate="True" showfooter="False" width="100%" id="dlMore" cellpadding="0" cellspacing="0" border="0" repeatcolumns="1" repeatdirection="Horizontal">
			        <itemstyle horizontalalign="left" verticalalign="top" />
			        <itemtemplate>
			            <div class="itemWrapper">
						    <div class="framesize-small frameFloat">
							    <div class="passrequired" visible='<%# PassRequired (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "game_access_id")))%>' runat="server" id="divPassRequired"></div>
						        <div class="frame ">
							        <span class="ct"><span class="cl"></span></span>							
								        <div class="imgconstrain">
								            <a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_name").ToString ())%>' href='<%# GetGameDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "game_id")))%>'>
										        <img border="0" src="<%#GetGameImageURL (DataBinder.Eval(Container.DataItem, "game_image_path").ToString () ,"me")%>" border="0" />
									        </a>
								        </div>																																				
							        <span class="cb"><span class="cl"></span></span>
						        </div>
					        </div>
					        <div class="relatedUser">
					            <a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_name").ToString ())%>' href='<%# GetGameDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "game_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_name").ToString ()), 100) %></a>
					            <br />
					            Visits: <%# DataBinder.Eval(Container.DataItem, "number_of_players")%> | Raves: <%# DataBinder.Eval(Container.DataItem, "number_of_raves")%>
					        </div>
					    </div>
			        </itemtemplate>
		        </asp:datalist>				
	        </div>				
        </div>
        <div id="related_footer"><span id="spnShowing" runat="server"/><a id="aAllResults" runat="server">See More Media</a></div>
    </ajax:ajaxpanel>
</div>
