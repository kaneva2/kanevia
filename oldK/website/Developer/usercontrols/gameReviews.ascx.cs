///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using MagicAjax;
using Kaneva.BusinessLayer.Facade;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;

namespace KlausEnt.KEP.Developer
{
    public partial class gameReviews : BaseUserControl
    {
        #region Declarations

        private int commentsPerPage = 10;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    BindData(1);
                }
                catch (Exception)
                {
                }
            }

            // Add the javascript for deleting comments
            string scriptString = "<script language=\"javaScript\">\n<!--\n function DeleteComment (id, owner_id) {\n";
            scriptString += "	$('" + hidCommentID.ClientID + "').value = id;\n";											// save the page number clicked to the hidden field
            scriptString += "	$('" + hidCommentOwnerID.ClientID + "').value = owner_id;\n";								// save the owner id of the comment to the hidden field
            scriptString += "	" + MagicAjax.AjaxCallHelper.GetAjaxCallEventReference(DeleteThread) + ";\n";				// call the __doPostBack function to post back the form and execute the PageClick event
            scriptString += "}\n// -->\n";
            scriptString += "</script>";

            if (!Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "DeleteComment"))
            {
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "DeleteComment", scriptString);
            }

            litControlId.Text = this.ClientID.ToString();

            litMaxLen.Text = DeveloperCommonFunctions.GetMaxCommentLength().ToString();
        }


        #region Helper Methods
        /// <summary>
        /// Bind the data
        /// </summary>
        /// <param name="pageNumber"></param>
        private void BindData(int pageNumber)
        {
            if (Request.IsAuthenticated)
            {
                lnkAddComment.InnerText = "Post A Review";

                lnkAddComment.Attributes["onclick"] = "ShowCommentBox(0, 0,this);return false;";

                lnkAddCommentImg.Attributes["onclick"] = "javascript: $('" + lnkAddComment.ClientID + "').click(); return false;";
            }
            else
            {
                // if user not logged in
                lnkAddComment.InnerText = "Login To Post A Review";
                lnkAddComment.HRef = ResolveUrl("~/login.aspx");
                lnkAddCommentImg.HRef = ResolveUrl("~/login.aspx");
            }

            BindComments(pageNumber);
        }

        /// <summary>
        /// BindComments
        /// </summary>
        private void BindComments(int pageNumber)
        {
            //populate attributes
            if (GameObject.GameId > 0)
            {
                nocomments.Text = "Be the first to leave a review for this STAR!";
                int numberOfComments = GameObject.NumberOfComments;
                int numberOfCommentsShowing = 0;

                // if commenting on an asset
                IList<Comment> comments = GetCommentFacade().GetGameReviews(GameObject.GameId, pageNumber, commentsPerPage);
                numberOfCommentsShowing = comments.Count;

                rptComments.DataSource = comments;
                rptComments.DataBind();

                pgTop.NumberOfPages = Math.Ceiling((double)numberOfComments / commentsPerPage).ToString();
                pgTop.DrawControl();

                // The results
                if (numberOfComments == 0)
                {
                    lblSearch.Visible = false;
                    tblFooter.Visible = false;
                    divCallOut.Visible = true;
                }
                else
                {
                    lblSearch.Visible = true;
                    tblFooter.Visible = true;
                    divCallOut.Visible = false;
                    lblSearch.Text = GetResultsText(numberOfComments, pageNumber, commentsPerPage, numberOfCommentsShowing);
                }
            }

        }

        public bool CanUserDeleteComment()
        {
            if (IsAdministrator() || this.IsGameOwner(GameObject.GameId))
                return true;
            else
                return false;
        }

        /// <summary>
        /// GetThreadEditedText
        /// </summary>
        /// <param name="updatedUser"></param>
        /// <param name="lastUpdatedDate"></param>
        /// <returns></returns>
        protected string GetThreadEditedText(string updatedUser, object lastUpdatedDate, object currentDate)
        {
            if (!updatedUser.Length.Equals(0))
            {
                return "Edited by " + updatedUser.ToString() + " " + FormatDateTimeSpan(lastUpdatedDate, currentDate);
            }

            return "";
        }

        /// <summary>
        /// Return the delete javascript
        /// </summary>
        /// <returns></returns>
        public string GetDeleteCommentScript(int commentID, int commentOwnerID)
        {
            return "javascript:if (confirm('Are you sure you want to delete this comment?')){DeleteComment (" + commentID + ", " + commentOwnerID + ")};";
        }

        /// <summary>
        /// Return the edit javascript
        /// </summary>
        /// <returns></returns>
        public string GetEditCommentScript(int user_id, int commentID, string comment)
        {
            return "javascript:EditComment(" + user_id + "," + commentID + ",'" + CleanJavascriptFull(comment) + "');";
        }

        protected string GetReplyLinkScript(int comment_id, int parent_id)
        {
            if (Request.IsAuthenticated)
            {
                return "href=\"javascript:void(0);\" onclick=\"ShowCommentBox(" + comment_id + "," + parent_id + ",this);return false;\"";
            }
            else
            {
                return "href=\"" + GetLoginURL() + "\"";
            }
        }

        protected string GetUserState(string user_status)
        {
            return DeveloperCommonFunctions.GetUserState(user_status);
        }
        #endregion

        #region Event Handlers

        /// <summary>
        /// Delete a Comment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_Reviews(Object sender, EventArgs e)
        {
            Comment comment = GetCommentFacade().GetGameReview(Convert.ToInt32(hidCommentID.Value));

            // Admin, asset owner, or comment owner
            if (IsAdministrator() || IsGameOwner(GameObject.GameId) || comment.UserId == GetUserId())
            {
                if (Convert.ToInt32(hidCommentID.Value) > 0)
                {
                    GetCommentFacade().DeleteGameReview(comment);
                    pgTop.CurrentPageNumber = 1;
                    BindComments(1);
                }
            }
        }
        /// <summary>
        /// User's response to a comment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReplyComment_Click(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                int maxCommentLentgh = DeveloperCommonFunctions.GetMaxCommentLength();

                // Check for any inject scripts
                if (DeveloperCommonFunctions.ContainsInjectScripts(hidCommentText.Value))
                {
                    m_logger.Warn("User " + GetUserId() + " tried to script from IP " + Common.GetVisitorIPAddress());
                    ShowErrorOnStartup("Your input contains invalid scripting, please remove script code and try again.", true);
                    return;
                }

                if (hidCommentText.Value.Length == 0)
                {
                    AjaxCallHelper.WriteAlert("Please enter a comment.  You can not submit a blank comment.");
                    return;
                }

                if (hidCommentText.Value.Length > maxCommentLentgh)
                {
                    AjaxCallHelper.WriteAlert("Your comment is too long.  Comments must be " + maxCommentLentgh.ToString() + " characters or less");
                    return;
                }

                if (GameObject.GameId > 0)
                {
                    GameDeveloper user = DeveloperCommonFunctions.CurrentUser;
                    Comment review = new Comment(0, user.UserId, GameObject.GameId, 0, Convert.ToInt32(hidParentCommentID.Value), Convert.ToInt32(hidCommentID.Value), Server.HtmlEncode(hidCommentText.Value), 0, Common.GetVisitorIPAddress(),
                        new DateTime(), new DateTime(), 0, 0);

                    if (GetCommentFacade().InsertGameReview(review).Equals(0))
                    {
                        AjaxCallHelper.WriteAlert("Error inserting review.");
                        return;
                    }
                }
            }

            hidCommentText.Value = "";
            pgTop.CurrentPageNumber = 1;
            BindComments(1);
        }
        /// <summary>
        /// updating to a comment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditComments_Click(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                int maxCommentLentgh = DeveloperCommonFunctions.GetMaxCommentLength();
                
                // Check for any inject scripts
                if (DeveloperCommonFunctions.ContainsInjectScripts(hidCommentText.Value))
                {
                    m_logger.Warn("User " + GetUserId() + " tried to script from IP " + Common.GetVisitorIPAddress());
                    ShowErrorOnStartup("Your input contains invalid scripting, please remove script code and try again.", true);
                    return;
                }

                //if (txtComment.Value.Length == 0)
                if (hidCommentText.Value.Length == 0)
                {
                    AjaxCallHelper.WriteAlert("Please enter a comment.  You can not submit a blank comment.");
                    return;
                }

                if (hidCommentText.Value.Length > maxCommentLentgh)
                {
                    AjaxCallHelper.WriteAlert("Your comment is too long.  Comments must be " + maxCommentLentgh.ToString() + " characters or less");
                    return;
                }

                int user_id = Convert.ToInt32(hidUserID.Value);

                if (IsAdministrator() || user_id == GetUserId())
                {
                    if (Convert.ToInt32(hidCommentID.Value) > 0)
                    {
                        Comment review = new Comment();
                        review.CommentId = Convert.ToInt32(hidCommentID.Value);
                        review.CommentText = Server.HtmlEncode(hidCommentText.Value);
                        review.LastUpdatedUserId = user_id;

                        GetCommentFacade().UpdateGameReview(review);

                        BindComments(pgTop.CurrentPageNumber);
                    }
                }
            }
            else
            {
                BindComments(1);
            }
        }

        /// <summary>
        /// Page Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            BindData(e.PageNumber);
        }

        #endregion

        #region Properties

        public Game GameObject
        {
            get
            {
                if (ViewState["Game"] != null)
                    return (Game)ViewState["Game"];
                else
                    return null;
            }
            set
            {
                ViewState["Game"] = value;
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
        }
        #endregion
    }
}
