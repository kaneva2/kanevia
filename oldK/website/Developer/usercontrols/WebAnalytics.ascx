<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="webanalytics.ascx.cs" Inherits="KlausEnt.KEP.Developer.usercontrols.webanalytics" %>

<script type="text/javascript">

	var _gaq = _gaq || [];
	_gaq.push(
		['_setAccount', <asp:literal id="litGAAcct" runat="server"></asp:literal>], ['_setDomainName', '.kaneva.com']		
		<asp:literal id="litGender" runat="server"></asp:literal>
		<asp:literal id="litAge" runat="server"></asp:literal>		
		<asp:literal id="litPageTracker" runat="server"></asp:literal>		
		<asp:literal id="litGAEvents" runat="server"></asp:literal>
	);

	(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();

</script>

<script type="text/javascript">
    function callGAEvent(section, action, label) {
        try {
            if (section == "Category") {
                var gaCat = "Website";
                var url = document.location.href;
                if (url.indexOf("/default.aspx") != -1) section = "MyKaneva";
                if (url.indexOf(".channel") != -1) section = "Community";
                if (url.indexOf(".people") != -1) section = "Profile";
                if (url.indexOf("ProfilePage.aspx") != -1) section = "Profile";
            }
            _gaq.push(['_trackEvent', section, action, label, 1]);
        } catch (err) { }
    }
</script>

<!-- <asp:literal ID="litServer" runat="server"></asp:literal> -->