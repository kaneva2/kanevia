///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace KlausEnt.KEP.Developer.usercontrols
{
  public partial class webanalytics : System.Web.UI.UserControl
  {
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            // Google Analytics 
            litGAAcct.Text = "'" + System.Configuration.ConfigurationManager.AppSettings["GoogleAnalyticsAccount"].ToString() + "'";
        }
        catch (Exception exc)
        {
            m_logger.Error("Error in web anayltics getting account. ", exc);
        }

        #region Custom Variables
        try
        {

            //if (Request.IsAuthenticated)
            //{
            //    // Gender
            //    litGender.Text = ",['_setCustomVar', 1, 'Gender', '" + KanevaWebGlobals.CurrentUser.Gender + "', 2]";

            //    // Age
            //    string ageRange = "0";

            //    int userAge = KanevaWebGlobals.CurrentUser.Age;
            //    if ((userAge >= 13) && (userAge <= 17))
            //        ageRange = "13-17";
            //    if ((userAge >= 18) && (userAge <= 24))
            //        ageRange = "18-24";
            //    if ((userAge >= 25) && (userAge <= 34))
            //        ageRange = "25-34";
            //    if ((userAge >= 35) && (userAge <= 44))
            //        ageRange = "35-44";
            //    if ((userAge >= 45) && (userAge <= 54))
            //        ageRange = "45-54";
            //    if ((userAge >= 55) && (userAge <= 64))
            //        ageRange = "55-64";
            //    if ((userAge >= 65))
            //        ageRange = "65+";

            //    litAge.Text = ",['_setCustomVar', 2, 'Age', '" + ageRange + "', 2]";
            //}

        }
        catch (Exception exc)
        {
            m_logger.Error("Error in web anayltics custom variables", exc);
        }
        #endregion

        #region Error conditions
        try
        {
            if (Request["aspxerrorpath"] != null)
            {
                //litPageTracker.Text = "pageTracker._trackPageview(\"/500.html?page=\" + document.location.pathname + document.location.search + \"&from=\" + document.referrer);";
                litPageTracker.Text = ",['_trackPageview','/500.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer]";
            }
            else if (Request.FilePath.Contains("fileNotFound.aspx"))
            {
                litPageTracker.Text = ",['_trackPageview',['/404.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer]";
            }
            else
            {
                litPageTracker.Text = ",['_trackPageview']";
            }
            litServer.Text = System.Environment.MachineName.ToString();
        }
        catch (Exception exc)
        {
            m_logger.Error("Error in web anayltics error conditions", exc);
        }
        #endregion


    }

    // Logger
    private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
  }
}