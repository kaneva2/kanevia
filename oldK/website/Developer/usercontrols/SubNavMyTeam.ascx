<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SubNavMyTeam.ascx.cs" Inherits="KlausEnt.KEP.Developer.usercontrols.SubNavMyTeam" %>
		<div id="subNav2">
			<ul>
				<li class="<%= SetClass("TEAM_MEMBERS") %>"><asp:LinkButton ID="lb_teamMember" CausesValidation="False" runat="server" CommandName="teamMembers" OnCommand="Nav_CommandMot">Team Members</asp:LinkButton></li>
				<li class="<%= SetClass("TEAM_ADMIN") %>"><asp:LinkButton ID="lb_teamAdmin" CausesValidation="False" runat="server" CommandName="teamAdmin" OnCommand="Nav_CommandMot">Team Administration</asp:LinkButton></li>
			</ul>
		</div>
		<asp:placeholder runat="server" id="phSubNav" />