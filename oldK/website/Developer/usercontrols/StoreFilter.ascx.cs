///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Developer
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
    using KlausEnt.KEP.Kaneva;

	public delegate void FilterChangedEventHandler (object sender, FilterChangedEventArgs e);

	/// <summary>
	/// FilterChangedEventArgs
	/// </summary>
	public class FilterChangedEventArgs: EventArgs
	{
	
		/// <summary>
		/// FilterChangedEventArgs
		/// </summary>
		/// <param name="newFilter"></param>
		public FilterChangedEventArgs (string newFilter, string newSort)
		{
			m_Filter = newFilter;
			m_Sort = newSort;
		}

		/// <summary>
		/// Filter
		/// </summary>
		public string Filter
		{
			get
			{
				return m_Filter;
			}
		}

		/// <summary>
		/// Sort
		/// </summary>
		public string Sort
		{
			get
			{
				return m_Sort;
			}
		}

        /// <summary>
        /// View
        /// </summary>
        public string View
        {
            set
            {
                m_View = value;
            }
            get
            {
                return m_View;
            }
        }

        /// <summary>
		/// Sort
		/// </summary>
		private string m_Sort;

		/// <summary>
		/// Filter
		/// </summary>
		private string m_Filter;

        /// <summary>
        /// View
        /// </summary>
        private string m_View = "";

    }

	/// <summary>
	///		Summary description for StoreFilter.
	/// </summary>
	/// 
	public class StoreFilter : System.Web.UI.UserControl
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				SetupFilter ();
			}
		}

		private void SetupFilter ()
		{
			// View
			drpView.Items.Add (new ListItem ("view...", ""));
			drpView.Items.Add (new ListItem ("----------", " "));
			drpView.Items.Add (new ListItem ("detail list", "li"));

			if (!HideThumbView)
			{
				drpView.Items.Add (new ListItem ("thumbs only", "th"));
				spanViewFilter.Visible = true;
			}
			else
			{
				//hide view filter if there's only one option
				spanViewFilter.Visible = false;
			}

			if (DefaultView.Trim ().Length > 0)
			{
				try
				{
					drpView.SelectedValue = DefaultView.Trim ();
				}
				catch (Exception) {}
			}
			
			// Pages
			// *********** NOTE *************************************************************************
			// IF YOU CHANGE THESE, MAKE SURE YOU HAVE ONE EQUAL TO THE CURRENT SETTING OF AssetsPerPage 
			// ****************************************************************************************** 
			drpPages.Items.Add (new ListItem ("10", "10"));
			drpPages.Items.Add (new ListItem ("30", "30"));
            drpPages.Items.Add(new ListItem("60", "60"));
            drpPages.Items.Add(new ListItem("100", "100"));
            drpPages.Items.Add(new ListItem("200", "200"));
            //			drpPages.Items.Add (new ListItem ("6", "6"));
//			drpPages.Items.Add (new ListItem ("12", "12"));
//			drpPages.Items.Add (new ListItem ("30", "30"));
			//			drpPages.Items.Add (new ListItem ("30", "30"));
			//			drpPages.Items.Add (new ListItem ("50", "50"));
			//			drpPages.Items.Add (new ListItem ("100", "100"));
			try
			{
				drpPages.SelectedValue = NumberOfPages.ToString ();
			}
			catch (Exception){}

			// populate alphanumeric filter
			drpSort.Items.Add(new ListItem ("All","All"));
			drpSort.Items.Add(new ListItem ("0-9" ,"0-9"));
			drpSort.Items.Add(new ListItem ("Sym" ,"Sym"));
			drpSort.Items.Add(new ListItem ("A" ,"A"));
			drpSort.Items.Add(new ListItem ("B" ,"B"));
			drpSort.Items.Add(new ListItem ("C" ,"C"));
			drpSort.Items.Add(new ListItem ("D" ,"D"));
			drpSort.Items.Add(new ListItem ("E" ,"E"));
			drpSort.Items.Add(new ListItem ("F" ,"F"));
			drpSort.Items.Add(new ListItem ("G" ,"G"));
			drpSort.Items.Add(new ListItem ("H" ,"H"));
			drpSort.Items.Add(new ListItem ("I" ,"I"));
			drpSort.Items.Add(new ListItem ("J" ,"J"));
			drpSort.Items.Add(new ListItem ("K" ,"K"));
			drpSort.Items.Add(new ListItem ("L" ,"L"));
			drpSort.Items.Add(new ListItem ("M" ,"M"));
			drpSort.Items.Add(new ListItem ("N" ,"N"));
			drpSort.Items.Add(new ListItem ("O" ,"O"));
			drpSort.Items.Add(new ListItem ("P" ,"P"));
			drpSort.Items.Add(new ListItem ("Q" ,"Q"));
			drpSort.Items.Add(new ListItem ("R" ,"R"));
			drpSort.Items.Add(new ListItem ("S" ,"S"));
			drpSort.Items.Add(new ListItem ("T" ,"T"));
			drpSort.Items.Add(new ListItem ("U" ,"U"));
			drpSort.Items.Add(new ListItem ("V" ,"V"));
			drpSort.Items.Add(new ListItem ("W" ,"W"));
			drpSort.Items.Add(new ListItem ("X" ,"X"));
			drpSort.Items.Add(new ListItem ("Y" ,"Y"));
			drpSort.Items.Add(new ListItem ("Z" ,"Z"));



			//show or hide the alphaNumeric row
			this.alphaNumerics.Visible = !HideAlphaNumerics;

			//show or hide the alphaNumeric row
			this.itemsPerPage.Visible = !HideItemsPerPagePullDown;

			//show or hide the alphaNumeric row
			this.pullDownAlphaNumerics.Visible = !HideAlphaNumericsPullDown;


		}

		/// <summary>
		/// lbSearch_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbSearch_Click (Object sender, CommandEventArgs e)
		{
			ProcessAlphaNumericChoice(e.CommandArgument.ToString());
			FilterChanged (this, new FilterChangedEventArgs (CurrentFilter, CurrentSort));
		}

		private void ProcessAlphaNumericChoice(string selection)
		{
			if (selection.Length > 0)
			{
				if(selection.ToUpper().Equals("ALL"))
				{
					CurrentFilter = "";
				}
				else if (selection.Equals ("0-9"))
				{
                    CurrentFilter = ColumnToSortBy + " REGEXP '^[0-9]'";
				}
				else if (selection.Equals ("Sym"))
				{
                    CurrentFilter = ColumnToSortBy + " REGEXP '^[^a-zA-Z0-9_]'";
				}
				else
				{
					string filter = selection.Substring (0,1);
					System.Text.RegularExpressions.Regex regexSingleLetter = new System.Text.RegularExpressions.Regex ("[a-zA-Z]");
					if (regexSingleLetter.IsMatch (filter))
					{
                        CurrentFilter = ColumnToSortBy + " like '" + Server.HtmlEncode(filter) + "%'";
					}
				}
			}
			else
			{
				CurrentFilter = "";
			}
		}

		/// <summary>
		/// Change View
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpSort_Change (Object sender, EventArgs e)
		{
			ProcessAlphaNumericChoice(drpSort.SelectedValue.ToString());
			FilterChanged (this, new FilterChangedEventArgs (CurrentFilter, CurrentSort));
		}

		/// <summary>
		/// Change View
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpView_Change (Object sender, EventArgs e)
		{
			FilterChangedEventArgs fcea = new FilterChangedEventArgs (CurrentFilter, CurrentSort);
			fcea.View = Server.HtmlEncode (drpView.SelectedValue.Trim ());

			FilterChanged (this, fcea);
		}

		/// <summary>
		/// Change Pages
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpPages_Change (Object sender, EventArgs e)
		{
			FilterChanged (this, new FilterChangedEventArgs (CurrentFilter, CurrentSort));
		}

		/// <summary>
		/// The current filter
		/// </summary>
		public string CurrentFilter
		{
			get 
			{
				if (ViewState ["filter"] != null)
				{
					return ViewState ["filter"].ToString ();
				}
				else
				{
					return "";
				}
			} 
			set
			{
				ViewState ["filter"] = value;
			}
		}

		/// <summary>
		/// Hide AlphaNumberic Row
		/// </summary>
		public bool HideAlphaNumerics
		{
			get 
			{
				if (ViewState ["HideAlphaNumerics"] != null)
				{
					return (bool) ViewState ["HideAlphaNumerics"];
				}
				else
				{
					return false;
				}
			} 
			set
			{
				ViewState ["HideAlphaNumerics"] = value;
			}
		}

		/// <summary>
		/// Hide AlphaNumberic Row
		/// </summary>
		public bool HideItemsPerPagePullDown
		{
			get 
			{
				if (ViewState ["HideItemsPerPagePullDown"] != null)
				{
					return (bool) ViewState ["HideItemsPerPagePullDown"];
				}
				else
				{
					return false;
				}
			} 
			set
			{
				ViewState ["HideItemsPerPagePullDown"] = value;
			}
		}

		/// <summary>
		/// HideThumbView
		/// </summary>
		public bool HideAlphaNumericsPullDown
		{
			get 
			{
				if (ViewState ["HideAlphaNumericsPullDown"] != null)
				{
					return (bool) ViewState ["HideAlphaNumericsPullDown"];
				}
				else
				{
					return true;
				}
			} 
			set
			{
				ViewState ["HideAlphaNumericsPullDown"] = value;
			}
		}

		/// <summary>
		/// The current Sort
		/// </summary>
		public string CurrentSort
		{
			get 
			{
				if (ViewState ["sort"] != null)
				{
					return ViewState ["sort"].ToString ();
				}
				else
				{
                    return ColumnToSortBy;
				}
			} 
			set
			{
				ViewState ["sort"] = value;
			}
		}

        /// <summary>
        /// The current Sort
        /// </summary>
        public string ColumnToSortBy
        {
            get
            {
                if (ViewState["sortColumn"] != null)
                {
                    return ViewState["sortColumn"].ToString();
                }
                else
                {
                    return DEFAULT_STORE_SORT;
                }
            }
            set
            {
                ViewState["sortColumn"] = value;
            }
        }


		/// <summary>
		/// The current Number of Pages to display
		/// </summary>
		public int NumberOfPages
		{
			get 
			{
				if (drpPages.SelectedValue.Length > 0 && IsPostBack)
				{
					Response.Cookies ["sipp"].Value = drpPages.SelectedValue;
					Response.Cookies ["sipp"].Expires = DateTime.Now.AddYears (1);
					return Convert.ToInt32 (drpPages.SelectedValue);
				}
				else
				{
					// Read it from the cookie?
					if (Request.Cookies ["sipp"] == null)
					{
						return KanevaGlobals.AssetsPerPage;
					}
					else
					{
						return Convert.ToInt32 (Request.Cookies ["sipp"].Value);
					}
				}
			} 
		}

		/// <summary>
		/// The Current View
		/// </summary>
		public string CurrentView
		{
			get 
			{
				if (!IsPostBack && DefaultView.Trim ().Length > 0)
				{
					return DefaultView.Trim ();
				}
				
				if (UseViewFromCookie && Request.Cookies ["sview"] != null)
				{
					drpView.SelectedValue = Request.Cookies ["sview"].Value;		
				}

				// Save the current view
				if (IsPostBack && drpView.SelectedValue.Length > 0)
				{
					
					Response.Cookies ["sview"].Value = drpView.SelectedValue;
					Response.Cookies ["sview"].Expires = DateTime.Now.AddYears (1);
				}

				return Response.Cookies ["sview"].Value;
			}
			set
			{
				drpView.SelectedValue = value;	
			}
		}
		/// <summary>
		/// The Current View for Photos
		/// </summary>
		public string CurrentViewPhoto
		{
			get 
			{
				if (Request.Cookies ["sviewphoto"] == null)
				{
					Response.Cookies ["sviewphoto"].Value = "th";
					Response.Cookies ["sviewphoto"].Expires = DateTime.Now.AddYears (1);
					drpView.SelectedValue = "th";
				}

				if (UseViewFromCookie)
				{
					drpView.SelectedValue = Request.Cookies ["sviewphoto"].Value;		
				}

				// Save the current view
				if (IsPostBack && drpView.SelectedValue.Length > 0)
				{
					Response.Cookies ["sviewphoto"].Value = drpView.SelectedValue;
					Response.Cookies ["sviewphoto"].Expires = DateTime.Now.AddYears (1);
				}

				return Response.Cookies ["sviewphoto"].Value;
			}
			set
			{
				drpView.SelectedValue = value;	
			}
		}
		/// <summary>
		/// The default view
		/// </summary>
		public string DefaultView
		{
			get 
			{
				if (ViewState ["DefaultView"] != null && IsPostBack)
				{
					return (string) ViewState ["DefaultView"];
				}
				else
				{
					if (Request.Cookies ["sview"] == null)
					{
						Response.Cookies ["sview"].Value = "li";
						return "li";
					}
					else
					{
						return Request.Cookies ["sview"].Value;
					}
				}
			} 
			set
			{
				ViewState ["DefaultView"] = value;
			}

		}

		/// <summary>
		/// HideThumbView
		/// </summary>
		public bool HideThumbView
		{
			get 
			{
				if (ViewState ["HideThumbView"] != null)
			{
				return (bool) ViewState ["HideThumbView"];
			}
				else
				{
					return false;
				}
			} 
			set
			{
				ViewState ["HideThumbView"] = value;
			}
		}


		public bool UseViewFromCookie
		{
			get { return m_useCookie; }
			set { m_useCookie = value; }
		}
		private const string DEFAULT_STORE_SORT = "game_name";
		private bool m_useCookie = false;

		public event FilterChangedEventHandler FilterChanged;

        protected DropDownList drpMembers, drpView, drpPages, drpSort;
		protected System.Web.UI.HtmlControls.HtmlGenericControl spanViewFilter;
		protected HtmlTableRow alphaNumerics, itemsPerPage, pullDownAlphaNumerics;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
