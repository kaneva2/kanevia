///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Developer.usercontrols
{
    public partial class SubNavMotivation : System.Web.UI.UserControl
    {
        #region Declarations

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SetSubNavigation();
        }

        public enum TAB
        {
            BUILD,
            VIRTUAL,
            WHY
        }

        /// <summary>
        /// The SubTab to display
        /// </summary>
        public TAB ActiveTab
        {
            get
            {
                return Session[DeveloperCommonFunctions.ACTIVE_SUB_NAV] == null ?
                    TAB.BUILD : (TAB)Session[DeveloperCommonFunctions.ACTIVE_SUB_NAV];
            }
            set
            {
                Session[DeveloperCommonFunctions.ACTIVE_SUB_NAV] = value;
            }
        }

        public string SetClass(string currTab)
        {
            string strTab = ActiveTab.ToString();
            if (strTab == currTab)
                return "selected";
            else
                return "";
        }

        private void SetSubNavigation()
        {
            try
            {
                string userControl = "";
                switch (ActiveTab)
                {
                    case TAB.WHY:
                        userControl = "";
                        break;
                    case TAB.VIRTUAL:
                        userControl = "";
                        break;
                    case TAB.BUILD:
                    default:
                        userControl = "";
                        break;
                }
                if (userControl != "")
                {
                    phSubNav.Controls.Add(Page.LoadControl(userControl));
                    phSubNav.Visible = true;
                }
            }
            catch (Exception)
            {
            }
        }

        protected void Nav_CommandMot(Object sender, CommandEventArgs e)
        {
            string url = "";
            switch (e.CommandName)
            {
                case "virtual":
                    url = "~/universal-3d-browser.aspx";
                    break;
                case "why":
                    url = "~/kaneva-star-platform.aspx";
                    break;
                case "build":
                default:
                    url = "~/build-3d-virtual-worlds.aspx";
                    break;
            }
            Response.Redirect(ResolveUrl(url));
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}