<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Footer.ascx.cs" Inherits="KlausEnt.KEP.Developer.usercontrols.Footer" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<div id="footer">
	<p>
		<a href="../vision-3d-internet.aspx">An Ecosystem of 3D Worlds</a> 
		<a href="../build-3d-virtual-worlds.aspx">Create Immersive Virtual Worlds</a> 
		<a href="../kaneva-virtual-reality-platform.aspx">Kaneva's Virtual Reality Platform</a> 
		<a href="../be-kaneva-star-builder.aspx">Become a Kaneva Star Builder</a>
		<a href="http://www.kaneva.com/overview/aboutLanding.aspx" target="_blank">About Kaneva</a>
	</p>
	<p class="copyright">&#169; 2000-2008  Kaneva, Inc.</p>
</div>
