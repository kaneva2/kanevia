///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Developer
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for SearchFilter.
	/// </summary>
	/// 
	public class SearchFilter : System.Web.UI.UserControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				SetupFilter ();
			}
		}

		private void SetupFilter ()
		{
			// View
			drpView.Items.Add (new ListItem ("view...", ""));
			drpView.Items.Add (new ListItem ("----------", " "));
			drpView.Items.Add (new ListItem ("detail list", "li"));

			if (!HideThumbView)
			{
				drpView.Items.Add (new ListItem ("thumbs only", "th"));
				spanViewFilter.Visible = true;
			}
			else
			{
				//hide view filter if there's only one option
				spanViewFilter.Visible = false;
			}

			if (!ShowFilterLists)
			{
				trFilterList.Visible = false;	
			}

			if (DefaultView.Trim ().Length > 0)
			{
				try
				{
					drpView.SelectedValue = DefaultView.Trim ();
				}
				catch (Exception) {}
			}
			
			tblFilter.Width = FilterWidth;

			// Pages
			// *********** NOTE *************************************************************************
			// IF YOU CHANGE THESE, MAKE SURE YOU HAVE ONE EQUAL TO THE CURRENT SETTING OF AssetsPerPage 
			// ****************************************************************************************** 
			
			
			if (m_arItems.Length > 0)
			{
				foreach (string s in m_arItems) 
				{
					if (s.Trim() != "")
						drpPages.Items.Add (new ListItem (s, s));
				}
			}
			else
			{
				drpPages.Items.Add (new ListItem ("12", "12"));
				drpPages.Items.Add (new ListItem ("24", "24"));
				drpPages.Items.Add (new ListItem ("40", "40"));
			}

			try
			{
				drpPages.SelectedValue = NumberOfPages.ToString ();
			}
			catch (Exception){}
		}

		/// <summary>
		/// lbSearch_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbSearch_Click (Object sender, CommandEventArgs e)
		{
			if (e.CommandArgument.ToString ().Length > 0)
			{
				if (e.CommandArgument.ToString ().Equals ("0-9"))
				{
					CurrentFilter = " a.name REGEXP '^[0-9]'";
				}
				else if (e.CommandArgument.ToString ().Equals ("@"))
				{
					CurrentFilter = " a.name REGEXP '^[^a-zA-Z0-9_]'";
				}
				else
				{
					string filter = e.CommandArgument.ToString ().Substring (0,1);
					System.Text.RegularExpressions.Regex regexSingleLetter = new System.Text.RegularExpressions.Regex ("[a-zA-Z]");
					if (regexSingleLetter.IsMatch (filter))
					{
						CurrentFilter = " a.name like '" + Server.HtmlEncode (filter) + "%'";
					}
				}
			}
			else
			{
				CurrentFilter = "";
			}

			FilterChanged (this, new FilterChangedEventArgs (CurrentFilter, CurrentSort));
		}

		/// <summary>
		/// Change View
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpView_Change (Object sender, EventArgs e)
		{
			FilterChangedEventArgs fcea = new FilterChangedEventArgs (CurrentFilter, CurrentSort);
			fcea.View = Server.HtmlEncode (drpView.SelectedValue.Trim ());

			FilterChanged (this, fcea);
		}

		/// <summary>
		/// Change Pages
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpPages_Change (Object sender, EventArgs e)
		{
			FilterChanged (this, new FilterChangedEventArgs (CurrentFilter, CurrentSort));
		}

		
		public void SetPagesDropValue(string drop_val)
		{
			drpPages.SelectedValue = drop_val;
		}
		
		/// <summary>
		/// The current filter
		/// </summary>
		public string CurrentFilter
		{
			get 
			{
				if (ViewState ["filter"] != null)
				{
					return ViewState ["filter"].ToString ();
				}
				else
				{
					return "";
				}
			} 
			set
			{
				ViewState ["filter"] = value;
			}
		}

		/// <summary>
		/// The current Sort
		/// </summary>
		public string CurrentSort
		{
			get 
			{
				if (ViewState ["sort"] != null)
				{
					return ViewState ["sort"].ToString ();
				}
				else
				{
					return DEFAULT_STORE_SORT;
				}
			} 
			set
			{
				ViewState ["sort"] = value;
			}
		}

		/// <summary>
		/// The current Number of Pages to display
		/// </summary>
		public int NumberOfPages
		{
			get 
			{

				if (drpPages.SelectedValue.Length > 0 && IsPostBack)
				{
					//Response.Cookies ["srchipp"].Value = drpPages.SelectedValue;
					//Response.Cookies ["srchipp"].Expires = DateTime.Now.AddYears (1);
					Response.Cookies [CurrentPage].Value = drpPages.SelectedValue;
					Response.Cookies [CurrentPage].Expires = DateTime.Now.AddYears (1);
					return Convert.ToInt32 (drpPages.SelectedValue);
				}
				else
				{
					// Read it from the cookie?
					//if (Request.Cookies ["srchipp"] == null)
					if (Request.Cookies [CurrentPage] == null)
					{
						return 12; //KanevaGlobals.AssetsPerPage;
					}
					else
					{
						//return Convert.ToInt32 (Request.Cookies ["srchipp"].Value);
						return Convert.ToInt32 (Request.Cookies [CurrentPage].Value);
					}
				}
			} 
		}

		/// <summary>
		/// The Current View
		/// </summary>
		public string CurrentView
		{
			get 
			{
				if (!IsPostBack && DefaultView.Trim ().Length > 0)
				{
					return DefaultView.Trim ();
				}
				
				if (UseViewFromCookie && Request.Cookies ["srchview"] != null)
				{
					drpView.SelectedValue = Request.Cookies ["srchview"].Value;		
				}

				// Save the current view
				if (IsPostBack && drpView.SelectedValue.Length > 0)
				{
					
					Response.Cookies ["srchview"].Value = drpView.SelectedValue;
					Response.Cookies ["srchview"].Expires = DateTime.Now.AddYears (1);
				}

				return Response.Cookies ["srchview"].Value;
			}
			set
			{
				drpView.SelectedValue = value;	
			}
		}
		/// <summary>
		/// The Current View for Photos
		/// </summary>
		public string CurrentViewPhoto
		{
			get 
			{
				if (Request.Cookies ["srchviewphoto"] == null)
				{
					Response.Cookies ["srchviewphoto"].Value = "th";
					Response.Cookies ["srchviewphoto"].Expires = DateTime.Now.AddYears (1);
					drpView.SelectedValue = "th";
				}

				if (UseViewFromCookie)
				{
					drpView.SelectedValue = Request.Cookies ["srchviewphoto"].Value;		
				}

				// Save the current view
				if (IsPostBack && drpView.SelectedValue.Length > 0)
				{
					Response.Cookies ["srchviewphoto"].Value = drpView.SelectedValue;
					Response.Cookies ["srchviewphoto"].Expires = DateTime.Now.AddYears (1);
				}

				return Response.Cookies ["srchviewphoto"].Value;
			}
			set
			{
				drpView.SelectedValue = value;	
			}
		}
		/// <summary>
		/// The default view
		/// </summary>
		public string DefaultView
		{
			get 
			{
				if (ViewState ["DefaultView"] != null && IsPostBack)
				{
					return (string) ViewState ["DefaultView"];
				}
				else
				{
					if (Request.Cookies ["srchview"] == null)
					{
						Response.Cookies ["srchview"].Value = "li";
						return "li";
					}
					else
					{
						return Request.Cookies ["srchview"].Value;
					}
				}
			} 
			set
			{
				ViewState ["DefaultView"] = value;
			}

		}

		/// <summary>
		/// The width of the filter
		/// </summary>
		public string FilterWidth
		{
			get 
			{
				if (ViewState ["filterwidth"] != null)
				{
					return ViewState ["filterwidth"].ToString ();
				}
				else
				{
					return "100%";
				}
			} 
			set
			{
				ViewState ["filterwidth"] = value;
			}
		}

		public string CurrentPage
		{
			get 
			{
				if (m_current_page == string.Empty)
				{
					return "srchipp";
				}
				else
				{
					return m_current_page;
				}
			} 
			set
			{
				m_current_page = value.ToLower();
			}
		}
		
		
		/// <summary>
		/// HideThumbView
		/// </summary>
		public bool HideThumbView
		{
			get 
			{
				if (ViewState ["HideThumbView"] != null)
				{
					return (bool) ViewState ["HideThumbView"];
				}
				else
				{
					return false;
				}
			} 
			set
			{
				ViewState ["HideThumbView"] = value;
			}
		}

		public bool ShowFilterLists
		{
			get 
			{
				if (ViewState ["ShowFilterLists"] != null)
				{
					return (bool) ViewState ["ShowFilterLists"];
				}
				else
				{
					return true;
				}
			} 
			set
			{
				ViewState ["ShowFilterLists"] = value;
			}
		}

		public bool UseViewFromCookie
		{
			get { return m_useCookie; }
			set { m_useCookie = value; }
		}
		
		public string AssetsPerPageList
		{
			set
			{
				m_arItems = value.Split(new char[]{','});
			}
		}


		private const string DEFAULT_STORE_SORT = "a.name";
		private bool m_useCookie = false;
		private string[] m_arItems = new string[0];
		private string	m_current_page = string.Empty;

		public event FilterChangedEventHandler FilterChanged;

		protected DropDownList	drpMembers, drpView, drpPages;
		protected System.Web.UI.HtmlControls.HtmlGenericControl spanViewFilter;
		protected HtmlTableRow	trFilterList;
		protected HtmlTable		tblFilter;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
