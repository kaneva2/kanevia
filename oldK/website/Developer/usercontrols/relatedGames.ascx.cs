///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using MagicAjax;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Developer
{
    public partial class relatedGames : BaseUserControl
    {
        #region Declarations
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private int _games_per_page = 5;
        private bool _is_related_items = true;
        private string _game_categories = "";
        private string _game_categoryNames = "";

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                try
                {
                    //bind the data
                    BindData();
                }
                catch (Exception)
                {
                }
            }
        }

        #region Primary Functions

        private void BindData()
        {
            int _game_id = GameObject.GameId;
            int _owner_id = GameObject.OwnerId;
            bool _show_mature = DeveloperCommonFunctions.CurrentUser.UserInfo.ShowMature;

            //populate attributes
            if (_game_id > 0)
            {
                lbMore.CssClass = "";
                if (_owner_id < 1)
                {
                    lbMore.Text = "More Kaneva Items...";
                }
                lbRelated.CssClass = "";

                dlRelated.Visible = false;
                dlMore.Visible = false;

                int count = 0;

                // Cleaning up compiler warning for deprecated call.  This page is deprecated, so no longer needs to be maintained.
                PagedDataTable pdtRelated = new PagedDataTable();
                pdtRelated.TotalCount = 0;

                PagedDataTable pdtMore = GetGameFacade().GetGamesByOwnerIdExclusive(_show_mature, _owner_id, _game_id, 1, this.GamesPerPage);

                // display the related items 
                if (this.IsRelatedGameSearch && pdtRelated.TotalCount > 0)
                {
                    lbRelated.CssClass = "selected";

                    dlRelated.DataSource = pdtRelated;
                    dlRelated.DataBind();
                    dlRelated.Visible = true;

                    // Show the results
                    if (pdtRelated.TotalCount > GamesPerPage)
                    {
                        //pull all categories the item is in
                        gameCategories = string.Empty;
                        gameCategoryNames = string.Empty;

                        count = GamesPerPage;
                        aAllResults.InnerText = "See More " + TruncateWithEllipsis(Server.HtmlDecode(gameCategoryNames), 20);
                        aAllResults.HRef = GetRelatedItemsMoreLink();
                        aAllResults.Visible = true;
                    }
                    else
                    {
                        count = (int)pdtRelated.TotalCount;
                        aAllResults.Visible = false;
                    }

                    spnShowing.InnerHtml = "Showing 1 - " + count.ToString() + " of " + pdtRelated.TotalCount.ToString();

                    if (pdtMore.TotalCount == 0)
                    {
                        liMore.Visible = false;
                        lbRelated.Attributes["onclick"] = "javascript:return false;";
                        lbRelated.Attributes["onmouseover"] = "javascript:this.style.cursor='default';";
                    }
                }
                else if (pdtMore.TotalCount > 0)	// display the more items
                {
                    lbMore.CssClass = "selected";

                    dlMore.DataSource = pdtMore;
                    dlMore.DataBind();
                    dlMore.Visible = true;

                    // Show the results										  
                    if (pdtMore.TotalCount > GamesPerPage)
                    {
                        //FUTURE -- Add back when we get new search browse working so can view media for a specific user

                        count = GamesPerPage;

                        aAllResults.InnerText = "See More Items By This User";

                        aAllResults.HRef = GetMoreUserItemsLink();

                        aAllResults.Visible = true;
                    }
                    else
                    {
                        count = (int)pdtMore.TotalCount;
                        aAllResults.Visible = false;
                    }

                    spnShowing.InnerHtml = "Showing 1 - " + count.ToString() + " of " + pdtMore.TotalCount.ToString();

                    if (pdtRelated.TotalCount == 0)
                    {
                        liRelated.Visible = false;
                        lbMore.Attributes["onclick"] = "javascript:return false;";
                        lbMore.Attributes["onmouseover"] = "javascript:this.style.cursor='default';";
                    }
                }
                else
                {
                    divContainer.Visible = false;
                }
            }
        }

        #endregion


        #region Helper Methods

        /// <summary>
        /// generate a link for the virtual world page that will auto display any WOK items in the same category 
        /// </summary>
        protected string GetRelatedItemsMoreLink()
        {
            return ResolveUrl("~/games/gameDirectory.aspx?catId=" + gameCategories);
        }

        /// <summary>
        /// generate a link for the virtual world page that will auto display any WOK items in the same category 
        /// </summary>
        protected string GetMoreUserItemsLink()
        {
            return ResolveUrl("~/games/gameDirectory.aspx?typeId=" + GameObject.OwnerId.ToString());
        }


        #endregion

        #region Event Handlers
        /// <summary>
        /// Event fired when one of the tabs is clicked
        /// </summary>
        protected void RelatedTab_Changed(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "related":
                    IsRelatedGameSearch = true;
                    break;
                case "more":
                    IsRelatedGameSearch = false;
                    break;
            }

            BindData();
        }
        #endregion

        #region Properties

        private string gameCategories
        {
            get { return _game_categories; }
            set { _game_categories = value; }
        }

        private string gameCategoryNames
        {
            get { return _game_categoryNames; }
            set { _game_categoryNames = value; }
        }

        private bool IsRelatedGameSearch
        {
            get { return _is_related_items; }
            set { _is_related_items = value; }
        }

        public int GamesPerPage
        {
            get { return _games_per_page; }
            set { _games_per_page = value; }
        }

        public Game GameObject
        {
            get
            {
                if (ViewState["Game"] != null)
                    return (Game)ViewState["Game"];
                else
                    return null;
            }
            set
            {
                ViewState["Game"] = value;
            }
        }


        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion


    }
}
