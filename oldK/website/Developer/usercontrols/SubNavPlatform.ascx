<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SubNavPlatform.ascx.cs" Inherits="KlausEnt.KEP.Developer.usercontrols.SubNavPlatform" %>
		<div id="subnavigation">
			<ul>
				<li class="<%= SetClass("ELEMENTS") %>"><asp:LinkButton ID="LinkButton1" runat="server" CommandName="elements" OnCommand="Nav_Command">The Elements</asp:LinkButton></li>
				<li class="<%= SetClass("SERVICES") %>"><asp:LinkButton ID="LinkButton2" runat="server" CommandName="services" OnCommand="Nav_Command">Kaneva Star Services</asp:LinkButton></li>
			</ul>
		</div>
		<asp:placeholder runat="server" id="phSubNav" />