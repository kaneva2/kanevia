///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System.Web.UI;

using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Developer.usercontrols
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Web.Security;

	/// <summary>
	///	Summary description for Header.
	/// </summary>
	public partial class Navigation : System.Web.UI.UserControl
	{
        #region Declarations

        protected HtmlInputText txtSearch;
        protected HtmlSelect selCategory;
        protected HtmlAnchor aSearchReset;
        protected ImageButton imgSearch;

        protected Label lblPMCount;
        protected Label lblUserName;
        protected Label lblRole;

        protected LinkButton aVision, aMotivation, aPlatform, aSignIn, aMyGames, aJoin, lb_Home;
        protected LinkButton lnkFeedback, lnkLogout, aMyGames2, aResources, aDirectory, aManagement, aMarketing;
        protected HtmlTableRow trSubNav, trValidator, tr_adminMenu;
        protected HtmlGenericControl navReports, navCSR, navIT, navWOK, navCatalog, liUserName;
        protected HtmlGenericControl nav, nav2, navDivMrkt, navDivDev;

        protected PlaceHolder phSubNav;

        protected HtmlAnchor aMyProfile;
        protected HtmlContainerControl spnMail;

        protected HtmlAnchor aHelp;
        private const bool DEVPAGE = true;
        private const bool MRKTPAGE = false;

        #endregion

        /// <summary>
		/// Page_Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load (object sender, System.EventArgs e)
		{
            SetNavigation();
		}

        #region Properties
        public enum TAB
        {
            HOME,
            VISION,
            MOTIVATION,
            PLATFORM,
            SIGN_IN,
            LOG_OUT,
            JOIN,
            HELP,
            NONE,
            MY_GAMES,
            MANAGEMENT,
            DIRECTORY,
            RESOURCES,
            ABOUT
        }

        /// <summary>
        /// The SubTab to display
        /// </summary>
        public TAB ActiveTab
        {
            get
            {
                return Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV] == null ?
                    TAB.VISION : (TAB)Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV];
            }
            set
            {
                Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV] = value;
            }
        }

        #endregion
		
		#region Helper Methods

        /// <summary>
        /// Sets navigation settings 
        /// </summary>
        private void SetNavigation()
        {
            try
            {
                //configure the main menu
                if (Request.IsAuthenticated)
                {
                    navDivDev.Visible = true;
                    lnkLogout.Visible = true;
                    aSignIn.Visible = false;

                    //set any sub navs as necessary
                    SetSubNavigation();
               }
                else
                {
                    navDivDev.Visible = false;
                    aSignIn.Visible = true;
                    lnkLogout.Visible = false;
                    //if an active tab set from a logged in state page and they are not authenticated
                    //send them back to the home page
                    if (ActiveTab != TAB.SIGN_IN)
                    {
                        Response.Redirect(ResolveUrl("~/login.aspx"));
                    }
                }

            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Sets Sub navigation if any
        /// </summary>
        private void SetSubNavigation()
        {
            try
            {
                string userControl = "";
                switch (ActiveTab)
                {
                    case TAB.MANAGEMENT:
                        userControl = "~/usercontrols/SubNavAcctManagement.ascx";
                        break;
                    default:
                        break;
                }
                if (userControl != "")
                {
                    phSubNav.Controls.Add(Page.LoadControl(userControl));
                    phSubNav.Visible = true;
                }
            }
            catch (Exception)
            {
            }
        }

		public string SetClass(string currTab)
		{
			string strTab = ActiveTab.ToString() ;
			if (strTab == currTab)
				return "selected";
			else
				return "";
		}


		#endregion

		#region Event Handlers
		/// <summary>
		/// Log out of the site
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lnkLogout_Click (object sender, EventArgs e) 
		{
			System.Web.HttpCookie aCookie;
			int limit = Request.Cookies.Count - 1;
			for (int i = Request.Cookies.Count - 1; i != 0; i--)
			{
				aCookie = Request.Cookies [i];
				aCookie.Expires = DateTime.Now.AddDays (-1);
				Response.Cookies.Add (aCookie);
			}


			FormsAuthentication.SignOut ();
			Session.Abandon ();

            navDivDev.Visible = false;
            aSignIn.Visible = true;
            lnkLogout.Visible = false;

            Response.Redirect(ResolveUrl("~/Default.aspx"));
		}

        public void Nav_Command(Object sender, CommandEventArgs e)
        {
            string url = "";
            switch (e.CommandName)
            {
                case "signin":
                    url = "~/login.aspx";
                    break;
                case "management":
                    //url = "~/management/MyProfile.aspx";
                    url = "~/management/MyTeam.aspx";
                    break;
                case "directory":
                    url = "~/games/gameDirectory.aspx";
                    break;
                case "resources":
                    url = "~/games/gameResources.aspx";
                    break;
                case "mygames":
                    url = "~/games/gameLibrary.aspx";
                    break;
                case "home":
                default:
                    url = "~/Default.aspx";
                    break;
            }
            Response.Redirect(ResolveUrl(url));
        }

		#endregion
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}

}
