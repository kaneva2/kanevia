///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;

namespace KlausEnt.KEP.Developer
{
    public partial class Navigation2 : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //set menu links
            manageLink.HRef = ResolveUrl("http://" + DeveloperCommonFunctions.GetKanevaURL + "/myKaneva/my3dApps.aspx");
            downNResLink.HRef = DeveloperCommonFunctions.GetDocumentationLink;
            forumLink.HRef = DeveloperCommonFunctions.GetForumLink;
        }
    }
}