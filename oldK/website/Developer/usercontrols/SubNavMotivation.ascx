<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SubNavMotivation.ascx.cs" Inherits="KlausEnt.KEP.Developer.usercontrols.SubNavMotivation" %>
		<div id="subnavigation">
			<ul>
				<li class="<%= SetClass("BUILD") %>"><asp:LinkButton ID="lb_WhyBuild" CausesValidation="False" runat="server" CommandName="build" OnCommand="Nav_CommandMot">Why Build in 3D?</asp:LinkButton></li>
				<li class="<%= SetClass("VIRTUAL") %>"><asp:LinkButton ID="lb_VirtualUnv" CausesValidation="False" runat="server" CommandName="virtual" OnCommand="Nav_CommandMot">The Virtual Universe</asp:LinkButton></li>
				<li class="<%= SetClass("WHY") %>"><asp:LinkButton ID="lb_WhyKaneva" CausesValidation="False" runat="server" CommandName="why" OnCommand="Nav_CommandMot">Why Kaneva?</asp:LinkButton></li>
			</ul>
		</div>
		<asp:placeholder runat="server" id="phSubNav" />	
