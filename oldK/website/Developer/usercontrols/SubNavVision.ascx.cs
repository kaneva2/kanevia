///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Developer.usercontrols
{
    public partial class SubNavVision : System.Web.UI.UserControl
    {
        protected PlaceHolder phSubNav;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            SetSubNavigation();
        }

        public enum TAB
        {
            DEFAULT
        }

        /// <summary>
        /// The SubTab to display
        /// </summary>
        public TAB ActiveTab
        {
            get
            {
                return Session[DeveloperCommonFunctions.ACTIVE_SUB_NAV] == null ?
                    TAB.DEFAULT : (TAB)Session[DeveloperCommonFunctions.ACTIVE_SUB_NAV];
            }
            set
            {
                Session[DeveloperCommonFunctions.ACTIVE_SUB_NAV] = value;
            }
        }

        public string SetClass(string currTab)
        {
            string strTab = ActiveTab.ToString();
            if (strTab == currTab)
                return "selected";
            else
                return "";
        }

        private void SetSubNavigation()
        {
            try
            {
                string userControl = "";
                switch (ActiveTab)
                {
                    default:
                        userControl = "";
                        break;
                }
                if (userControl != "")
                {
                    phSubNav.Controls.Add(Page.LoadControl(userControl));
                    phSubNav.Visible = true;
                }
            }
            catch (Exception)
            {
            }
        }

        protected void Nav_Command(Object sender, CommandEventArgs e)
        {
            string url = "";
            switch (e.CommandName)
            {
                default:
                    break;
            }
            Response.Redirect(ResolveUrl(url));
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion


    }
}