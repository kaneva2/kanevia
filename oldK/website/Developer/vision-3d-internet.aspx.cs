///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Developer
{
    public partial class vision_3d_internet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //set Active Tab
            Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV] = usercontrols.Navigation.TAB.VISION;

            if (!Page.IsPostBack)
            {
                //set the metadata
                ((MainTemplatePage)Master).Title = "Extending Web Sites to Virtual Experiences | The Kaneva Star Platform";
                ((MainTemplatePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\" 3d internet, avatar world, 3d technology, 3d virtual reality, virtual universe, 3d worlds, kaneva 3d star system \">";
                ((MainTemplatePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Imagine visitors, via their avatars, moving freely between theme parks, showrooms, entertainment venues, and caf�s, fully engaged in an experience that goes beyond the Web, bringing real people together to laugh and learn, shop or play, virtually in person. Kaneva will make this virtual universe a reality.\">";
            }
        }
    }
}
