<%@ Page Language="C#"  MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="true" CodeBehind="build-3d-virtual-worlds.aspx.cs" Inherits="KlausEnt.KEP.Developer.build_3d_virtual_worlds" %>

<asp:Content ID="cnt_starBuilder" runat="server" ContentPlaceHolderID="cph_Body">
	<script language="javascript">AC_FL_RunContent = 0;</script>
	<script src="jscript/AC_RunActiveContent.js" language="javascript"></script>
	
    <script language="JavaScript" type="text/javascript">
	<!--
	// (C) F. Permadi
	function getFlashMovieObject(movieName)
	{
	  if (window.document[movieName])
	  {
		return window.document[movieName];
	  }
	  if (navigator.appName.indexOf("Microsoft Internet")==-1)
	  {
		if (document.embeds && document.embeds[movieName])
		  return document.embeds[movieName];
	  }
	  else // if (navigator.appName.indexOf("Microsoft Internet")!=-1)
	  {
		return document.getElementById(movieName);
	  }
	}

	function HumanRollover()
	{
		var flashMovie=getFlashMovieObject("right-star-boxes");
		flashMovie.LoadMovie(0, "human-mover.swf");
	}

	function AtmosphereRollover()
	{
		var flashMovie=getFlashMovieObject("right-star-boxes");
		flashMovie.LoadMovie(0, "atmosphere-mover.swf");
	}

	function AnythingRollover()
	{
		var flashMovie=getFlashMovieObject("right-star-boxes");
		flashMovie.LoadMovie(0, "anything-mover.swf");
	}

	function HumanMain()
	{
		var flashMovie=getFlashMovieObject("right-star-boxes");
		flashMovie.LoadMovie(0, "human-main.swf");
	}

	function AtmosphereMain()
	{
		var flashMovie=getFlashMovieObject("right-star-boxes");
		flashMovie.LoadMovie(0, "atmosphere-main.swf");
	}

	function AnythingMain()
	{
		var flashMovie=getFlashMovieObject("right-star-boxes");
		flashMovie.LoadMovie(0, "anything-main.swf");
	}

	//-->
	</script>

	<div id="content" >

		<div id="main">

			<div id="main_content">
				<h1 class="two png">Why Build in 3D?</h1>
			  	<h2>(To Bring Back the Human Element)</h2>
			  	<p>MySpace and Facebook have built their businesses around answering the question <span class="highlight">"Where is everyone?"</span></p>
			  	<p>Let's face it&#8230; <span class="highlight">browsing the Web is generally something you do by yourself.</span> While the resources out there are astounding, the experience is somewhat akin to reading a magazine. One-way communication.</p>
				<p><span class="highlight">Social Networking web sites have brought us a step closer</span> by showing us how to communicate, make friends, and socialize online. But there's a big difference between leaving a message on someone's profile page, and bumping into a friend at a club.</p>
			  	<p><span class="highlight">Virtual spaces get us together online - same time, same place.</span> They bring back the <a href="#" onMouseOver="HumanRollover();" onClick="HumanMain();" class="flashTrigger">human element</a>, and <a href="#" onMouseOver="AtmosphereRollover();" onClick="AtmosphereMain();" class="flashTrigger">real-world perspective</a>. They can be grandiose, or incredibly simple: Let your customers walk through a virtual showroom, immersed in your brand, viewing a product display that rivals any brick-and-mortar retail space. Or create a virtual conference room that gives remote employees the opportunity to meet face to face, removing the geographic barrier. Bring your novel or television show to life, and let fans walk the halls with the characters they love.</p>
			  	<p><span class="highlight">The applications for entertainment, commerce, business, education, and government are <a href="#" onMouseOver="AnythingRollover();" onClick="AnythingMain();" class="flashTrigger">endless</a>.</span> And with the Kaneva Star Platform, you're not just getting a place to call your own, you're getting everything you need to make it successful.</p>

			 	<div class="quote"><p><span class="mark left">&#8220;</span>80% of all active Internet users will participate in virtual worlds by 2011.<span class="mark right">&#8221;</span> <span class="reference">(Gartner Group)</span></p></div>

			 	<div class="quote"><p><span class="mark left">&#8220;</span>Within 5 years, the 3D Internet will be as important for work as the Web is today.<span class="mark right">&#8221;</span> <span class="reference">(Forrester Research)</span></p></div>


			 </div>
			<div id="sidebar">
			<div id="sidebar_content">

				<div id="flashRail">
					<script language="javascript">
						if (AC_FL_RunContent == 0) {
							alert("This page requires AC_RunActiveContent.js.");
						} else {
							AC_FL_RunContent(
								'codebase', 'http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0',
								'width', '276',
								'height', '760',
								'src', 'right-star-boxes',
								'quality', 'high',
								'pluginspage', 'http://www.macromedia.com/go/getflashplayer',
								'align', 'middle',
								'play', 'true',
								'loop', 'true',
								'scale', 'showall',
								'wmode', 'transparent',
								'devicefont', 'false',
								'id', 'right-star-boxes',
								'bgcolor', '#ff6600',
								'name', 'right-star-boxes',
								'menu', 'true',
								'allowFullScreen', 'false',
								'allowScriptAccess','sameDomain',
								'movie', 'right-star-boxes',
								'salign', ''
								); //end AC code
						}
					</script>
					<noscript>
						<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="276" height="760" id="right-star-boxes" align="middle">
						<param name="allowScriptAccess" value="sameDomain" />
						<param name="allowFullScreen" value="false" />
						<param name="movie" value="right-star-boxes.swf" /><param name="quality" value="high" /><param name="wmode" value="transparent" /><param name="bgcolor" value="#ff6600" /><embed src="right-star-boxes.swf" quality="high" wmode="transparent" bgcolor="#ff6600" width="276" height="760" name="right-star-boxes" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" SWLIVECONNECT="true"/>
						</object>
					</noscript>
				</div>
				
			</div>
		</div>
		</div>

		<div id="nextPage" class="virtualuniverse"><a href="universal-3d-browser.aspx">Next - The Virtual Universe</a></div>
	</div>

</asp:Content>