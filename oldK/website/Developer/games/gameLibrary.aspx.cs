///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using System.Threading;
using MagicAjax;
using Kaneva.BusinessLayer.Facade;
using Kaneva.DataLayer.DataObjects;

namespace KlausEnt.KEP.Developer
{
    public partial class gameLibrary : BasePage
    {
 		#region Declarations

		protected Pager pgTop, pgBottom;
        protected StoreFilter filStore, pageSort;
		protected PlaceHolder phBreadCrumb;
		protected Repeater rptGames;
        protected HtmlInputHidden GameId;
		protected Label lblSearch, lblSearch2, messages;
		protected HtmlTableCell tdOrderHeading;
		protected System.Web.UI.HtmlControls.HtmlGenericControl spanGsmeLibrary;
        protected System.Web.UI.WebControls.Button DeleteGame;
        protected MagicAjax.UI.Controls.AjaxPanel ajGameLibrary;
		protected System.Web.UI.HtmlControls.HtmlImage Img1;
		protected System.Web.UI.HtmlControls.HtmlImage Img4;
		protected System.Web.UI.HtmlControls.HtmlImage Img5;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist1;
        protected System.Web.UI.WebControls.LinkButton lnkDeleteGame, lnkAddGame;
		protected System.Web.UI.HtmlControls.HtmlAnchor A1;
		protected System.Web.UI.HtmlControls.HtmlImage Img3;
		protected System.Web.UI.HtmlControls.HtmlImage Img7;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#endregion

        protected gameLibrary() 
		{
			Title = "My STARS";
		}

        #region PageLoad

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //set Main Navigation
                Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV] = usercontrols.Navigation.TAB.MY_GAMES;

                //check security level
                switch (CheckUserAccess(MANAGE_STARS))
                {
                    case ACCESS_FULL:
                        //do nothing
                        break;
                    case ACCESS_READ:
                        ReadOnly();
                        break;
                    case ACCESS_NONE:
                    default:
                        NavigateBackToBreadCrumb(GetLastBreadCrumb().Text);
                        break;
                }

                if (!IsPostBack)
                {
                    // Set the category on the filter
                    pgTop.CurrentPageNumber = 1;
                    pgBottom.CurrentPageNumber = 1;

                    BindGameData(1);

                    //set the number to display per page to the max
                    //set alert box to delete button
                    StringBuilder sb = new StringBuilder();
                    sb.Append(" javascript: ");
                    sb.Append(" if(!confirm('Are you sure you want to delete the selected game(s) from your STARS library? \\n This cannot be undone.')) return false;");
                    lnkDeleteGame.Attributes.Add("onClick", sb.ToString());

                    //ResetBreadCrumb ();
                    AddBreadCrumb(GetNewBreadCrumb("My STARs", GetCurrentURL(), "", 0));
                }

                //clear message
                messages.Text = "";
            }
            else
            {
                RedirectToLogin();
            }
        }
        #endregion

        #region Attributes/Properties

        //disables key controls for read only mode
        private void ReadOnly()
        {
            lnkAddGame.Enabled = false;
            lnkDeleteGame.Enabled = false;
            lnkDeleteGame.Attributes.Remove("onClick");
        }

        protected int ORDER_COUNT
        {
            get
            {
                if (ViewState["countOrder"] == null)
                {
                    ViewState["countOrder"] = ((pgTop.CurrentPageNumber - 1) * filStore.NumberOfPages) + 1;
                }
                return (int)ViewState["countOrder"];
            }
            set
            {
                ViewState["countOrder"] = value;
            }
        }

        /// <summary>
        /// Current filtering
        /// </summary>
        protected string CURRENT_FILTERING
        {
            get
            {
                if (ViewState["returnFilter"] == null)
                {
                    return "";
                }
                else
                {
                    return ViewState["returnFilter"].ToString();
                }
            }
            set
            {
                ViewState["returnFilter"] = value;
            }
        }

        protected string STARSLIBRARY_SORT
        {
            get
            {
                return "game_creation_date desc";
            }
        }


        #endregion

        #region Primary Functions

        /// <summary>
        /// Delete selected items
        /// </summary>
        protected void DeleteChecked()
        {
            CheckBox chkEdit;
            HtmlInputHidden hidGameId;
            int deletionCount = 0;

            foreach (RepeaterItem dgiGame in rptGames.Items)
            {
                chkEdit = (CheckBox)dgiGame.FindControl("chkEdit");

                if (chkEdit.Checked)
                {
                    hidGameId = (HtmlInputHidden)dgiGame.FindControl("hidGameId");
                    GetGameFacade().DeleteGame(Convert.ToInt32(hidGameId.Value),GetUserId());
                    deletionCount++;
                }
            }

            //message to user
            if (deletionCount == 0)
            {
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "No games were selected.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
            else
            {
                BindGameData(pgTop.CurrentPageNumber);
                messages.ForeColor = System.Drawing.Color.DarkGreen;
                messages.Text = deletionCount + " games" + (deletionCount > 1 ? "s were " : " was ") + "deleted from your library.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        private void BindGameData(int pageNumber)
        {
            //sets page configuration
            ORDER_COUNT = ((pageNumber - 1) * filStore.NumberOfPages) + 1;

            string orderby = STARSLIBRARY_SORT;

            //get the appropriate media assets.
            PagedDataTable pds = null;

            //set all filters
			string titleFilterID = "";
            if ((CURRENT_FILTERING != "") && (CURRENT_FILTERING.ToUpper() != "ALL"))
			{
                titleFilterID = CURRENT_FILTERING; 
			}

            //retreive the games list
            pds = GetGameFacade ().GetGamesByOwner (GetCurrentUser().UserId, titleFilterID, orderby, pageNumber, filStore.NumberOfPages);

            pgTop.NumberOfPages = Math.Ceiling((double)pds.TotalCount / filStore.NumberOfPages).ToString();
            pgTop.DrawControl();

            pgBottom.NumberOfPages = Math.Ceiling((double)pds.TotalCount / filStore.NumberOfPages).ToString();
            pgBottom.DrawControl();

            rptGames.DataSource = pds;
            rptGames.DataBind();

            // The results
            lblSearch2.Text = lblSearch.Text = GetResultsText(pds.TotalCount, pageNumber, filStore.NumberOfPages, pds.Rows.Count);

        }

        #endregion

        #region Event Handlers

        private void lnkDeleteGame_Click(object sender, System.EventArgs e)
        {
            //calls function to delete all checked media
            try
            {
                DeleteChecked();
            }
            catch (Exception ex)
            {
                m_logger.Error("Media Library error during media deletion.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Deletion Error: Sorry we were unable to delete your media.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

        }

        /// <summary>
        /// lnkAddStar_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void lnkAddGame_Click(object sender, System.EventArgs e)
        {
            Response.Redirect(GetGameEditLink(-1));
        }

        /// <summary>
        /// Delete an game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_Game(Object sender, EventArgs e)
        {
            int userId = GetUserId();
            int gameId = 0;
            try
            {
                gameId = Convert.ToInt32(GameId.Value);
            }
            catch (FormatException ex)
            {
                m_logger.Error("STARs Library error deleting STAR.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Submission Error: Sorry we were unable to delete your STAR.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
                return;
            }

            if (!IsGameOwner(gameId))
            {
                m_logger.Warn("User " + userId + " from IP " + Common.GetVisitorIPAddress() + " tried to delete STAR " + GameId.Value + " but is not the STAR owner");
                return;
            }

            pg_PageChange(this, new PageChangeEventArgs(pgTop.CurrentPageNumber));
        }

        /// <summary>
        /// rptGames_ItemCommand
        /// </summary>
        private void rptGames_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string command = e.CommandName;

            HtmlInputHidden hidGameId;

            switch (command)
            {
                case "cmdEdit":
                    {
                        int index = e.Item.ItemIndex;
                        hidGameId = (HtmlInputHidden)rptGames.Items[index].FindControl("hidGameId");

                        Response.Redirect(GetGameEditLink(Convert.ToInt32(hidGameId.Value)));
                        break;
                    }
            }
        }

        private void rptGames_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            HtmlAnchor thumbnail = (HtmlAnchor)e.Item.FindControl("thumbnail");
            HtmlAnchor gameTitle = (HtmlAnchor)e.Item.FindControl("gameTitle");

            //create the tool summary data
            string name = "\\<b>Name: </b>" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "game_name")) + "<br>";
            string description = "\\<b>Description: </b>" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "game_synopsis")) + "<br>";
            //string owner = "\\<b>Owner: </b>" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "username")) + "<br>";
            string restricted = "\\<b>Rating: </b>" + GetGameRatingDescription(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "game_rating_id"))) + "<br>";
            //string assetType = "\\<b>Type: </b>"; //+ GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "asset_type_id"))) + "<br>";
            string _public = "\\<b>Access: </b>" + GetGameAccessDescription(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "game_access_id"))) + "<br>";
            string createdOn = "\\<b>Added On: </b>" + FormatDateTime(Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "game_creation_date"))) + "<br>";

            string toolTip = "\\ <h4>Asset Details</h4>" + name + description + restricted + _public + createdOn;
            thumbnail.Attributes.Add("onmouseover", "whiteBalloonSans.showTooltip(event,'" + CleanJavascriptFull(toolTip) + "')");
            gameTitle.Attributes.Add("onmouseover", "whiteBalloonSans.showTooltip(event,'" + CleanJavascriptFull(toolTip) + "')");
        }

        private void rptGames_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lnkEdit = (LinkButton)e.Item.FindControl("lnkEdit");
                CheckBox chkEdit = (CheckBox)e.Item.FindControl("chkEdit");
                Label lblMessage = (Label)e.Item.FindControl("lblMessage");

                int statusId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "game_status_id"));
                int ownerId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "owner_id"));
                string teaser = DataBinder.Eval(e.Item.DataItem, "game_synopsis") == null ? "" : DataBinder.Eval(e.Item.DataItem, "game_synopsis").ToString();

                //only asset owner and admin can edit the asset
                bool canEditGame = ((ownerId == GetCurrentUser ().UserId) || IsAdministrator ());

                chkEdit.Visible = lnkEdit.Visible = canEditGame;
                lblMessage.Visible = !canEditGame;

                HtmlAnchor hlStatus = (HtmlAnchor)e.Item.FindControl("hlStatus");
                HtmlAnchor hlAddData = (HtmlAnchor)e.Item.FindControl("hlAddData");

                hlStatus.InnerHtml = GetGameStatusDescription(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "game_status_id")));
                hlStatus.HRef = GetGameEditLink(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "game_id")));

                string keywords = DataBinder.Eval(e.Item.DataItem, "game_keywords") == null ? "" : DataBinder.Eval(e.Item.DataItem, "game_keywords").ToString();

                //pull in the categories for the game and get the counts
                int categoryCount = 0;

                //pick the categories for the game.
                if (keywords.Length.Equals(0) || teaser.Length.Equals(0)
                    || categoryCount.Equals(0))
                {
                    hlAddData.InnerHtml = ": Add details";
                    hlAddData.HRef = GetGameEditLink(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "game_id")));
                }

            }
        }

        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            pgTop.CurrentPageNumber = e.PageNumber;
            pgTop.DrawControl();
            pgBottom.CurrentPageNumber = e.PageNumber;
            pgBottom.DrawControl();
            BindGameData(e.PageNumber);
        }

        private void filStore_FilterChanged(object sender, FilterChangedEventArgs e)
        {
            pgTop.CurrentPageNumber = 1;
            pgBottom.CurrentPageNumber = 1;
            BindGameData(1);
        }

        private void pageSort_FilterChanged(object sender, FilterChangedEventArgs e)
        {
            pgTop.CurrentPageNumber = 1;
            pgBottom.CurrentPageNumber = 1;
            CURRENT_FILTERING = e.Filter;
            BindGameData(1);
        }

        #endregion


        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lnkAddGame.Click += new System.EventHandler(this.lnkAddGame_Click);
            this.rptGames.ItemCreated += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptGames_ItemCreated);
            this.rptGames.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptGames_ItemDataBound);
            this.rptGames.ItemCommand += new System.Web.UI.WebControls.RepeaterCommandEventHandler(this.rptGames_ItemCommand);
            this.Load += new System.EventHandler(this.Page_Load);
            this.lnkDeleteGame.Click += new System.EventHandler(this.lnkDeleteGame_Click);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
            pgBottom.PageChanged += new PageChangeEventHandler(pg_PageChange);
            filStore.FilterChanged += new FilterChangedEventHandler(filStore_FilterChanged);
            pageSort.FilterChanged += new FilterChangedEventHandler(pageSort_FilterChanged);
        }
        #endregion

    }
}