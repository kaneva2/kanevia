///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using MagicAjax;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Developer
{
    public partial class gameDetails : BasePage
    {   
       
        #region Declarations
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected Literal litStream = new Literal();
        private string m_tag = string.Empty;
        private int _assetOwnerId = 0;

        private const string COMMUNITY_GUIDELINES = "<br/><br/><u><b>Kaneva Community Guidelines</b></u><br/>" +
            "Kaneva defines restricted content as anything that contains strong language, or depictions of nudity, violence " +
            "or substance abuse.  When you mark content as Restricted, you protect our visitors -- and give youself greater " +
            "freedom of expression.  Only Kaneva members 18 years of age or older may post or view Restricted content.";

        private const string PRIVACY_SETTINGS = "<br/><br/><u><b>Privacy Settings for Kaneva Members</b></u><br/>" +
            "Kaneva members have the option to set their profiles and communities to private, only firends within their private " +
            "network are allowed to view detailed information such as personal interests and friends. These settings are " +
            "part of a broader effort to Kaneva to help protect the privacy of our community members and provide a safe " +
            "environment for all members to connect online.";

        // Share Media 

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //set Main Navigation
                Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV] = usercontrols.Navigation.TAB.DIRECTORY;

                if (!Page.IsPostBack)
                {
                    //get the parameters for the details page
                    //if this fails retrieve the last bread crumb and navigate there
                    if (GetRequestParams())
                    {
                        //configure the page for use
                        InitializeGameDetailsPage();

                        //set bread crumb
                        AddBreadCrumb(GetNewBreadCrumb("Stars Details", "~/games/gameDetails.aspx?gameId=" + GameId, "", 0));
                    }
                    else
                    {
                        //get the last bread crumb
                        string LastBreadCrumbText = (GetLastBreadCrumb() != null ? GetLastBreadCrumb().Text : null);

                        //if there is one navigate there if not default to directory
                        if (LastBreadCrumbText != null)
                        {
                            NavigateBackToBreadCrumb(LastBreadCrumbText);
                        }
                        else
                        {
                            Response.Redirect("~/games/gameDirectory.aspx");
                        }
                    }

                }
            }
            else
            {
                RedirectToLogin();
            }

        }

        #endregion

        #region Helper Functions

        private bool GetRequestParams()
        {
            bool retVal = true;
            try
            {
                GameId = Convert.ToInt32(Request["gameId"]);
            }
            catch (Exception)
            {
                //invalid numbers
                retVal = false;
            }
            return retVal;
        }


        #endregion

        # region Primary Methods

        /// <summary>
        /// Initializes the page data
        /// </summary>
        private void InitializeGameDetailsPage()
        {

            Game gameObject = null;
            string gameName = "";
            string gameDescription = "";

            gameObject = GetGameFacade().GetGameByGameId(GameId);

            if (gameObject != null)
            {
                ItemOwnerId = gameObject.OwnerId;

                divDetails.Visible = true;

                BindData(gameObject);

                // Show asset name
                gameTitle.InnerText = Server.HtmlDecode(gameObject.GameName);

                // set all meta data
                NameValueCollection tagList = new NameValueCollection();
                gameName = gameObject.GameName;
                gameDescription = gameObject.GameDescription;

                string[] tags = gameObject.GameKeyWords.Split(' ');
                //default game name as one of the tags
                tagList.Add(gameName, gameName);

                for (int i = 0; i < tags.Length; i++)
                {
                    tagList.Add(tags[i], tags[i]);
                }

                ((MainTemplatePage)Master).Title = Kaneva.MetaDataGenerator.GenerateTitle(gameObject.GameName, "", (int)Kaneva.Constants.eWEB_PAGE_TYPE.MEDIA) + " - Kaneva ";
                ((MainTemplatePage)Master).MetaDataDescription = Kaneva.MetaDataGenerator.GenerateMetaDescription(gameObject.GameSynopsis, tagList, (int)Kaneva.Constants.eWEB_PAGE_TYPE.MEDIA);
                ((MainTemplatePage)Master).MetaDataKeywords = Kaneva.MetaDataGenerator.GenerateMetaKeywords(gameObject.GameName, tagList, (int)Kaneva.Constants.eWEB_PAGE_TYPE.MEDIA);
            
            }


            // Set the elements for sharing
            shareLink.Text = txtShare.Text;
            shareMailto.NavigateUrl = "mailto:?subject=You%27ve%20received%20media%20shared%20from%20Kaneva%21&body=" + txtShare.Text;
            akst_link_1.Attributes.Add("onclick", "akst_configure_tag(" + Request.IsAuthenticated.ToString().ToLower() + ");akst_share('" + akst_link_1.ClientID + "', '" + txtShare.Text.Replace("'", "") + "', '" + ((MainTemplatePage)Master).Title.ToString().Replace("'", "") + "'); return false; ");

            //// Show correct advertisements
            //AdvertisementSettings Settings = (AdvertisementSettings)Application["Advertisement"];

            //// Set mode of Google Ads
            //if (Settings.Mode.Equals(AdvertisingMode.Live))
            //{
            //    litAdTest.Visible = false;
            //}
            //else if (Settings.Mode.Equals(AdvertisingMode.Testing))
            //{
            //    litAdTest.Text = "google_adtest = \"on\"";
            //}
        }


        /// <summary>
        /// Bind the page data
        /// </summary>
        private void BindData(Game gameObject)
        {
            try
            {
                int gameId = gameObject.GameId;
                int userId = GetUserId();
                int itemCreatorId = gameObject.OwnerId;
                string game_Name = gameObject.GameName;

                // -- ACTION BAR SECTION --lblBodyText
                // ------------------------

                // Share
                //hlShare.NavigateUrl = GetWOKShareLink(gameId);

                // Shopping List


                // Raves	 
                if (gameObject.NumberOfRaves > 0 && !IsAdministrator())
                {
                    lbRave.Text = "<img src=\"" + ResolveUrl("~/images/raveit_bt_disabled.gif") + "\" runat=\"server\" width=\"16\" height=\"16\" border=\"0\"> Raved";
                    lbRave.Enabled = false;
                    lbRave.ToolTip = "You Raved this Item";
                }

                {
                    //int height = 378;
                    int width = 398;
                    // Only displays images from world
                    litStream.Text = "<div style=\"width: " + width.ToString() + "px; overflow: hidden; \">" +
                        "<img src=\"" + GetGameImageURL(gameObject.GameImagePath, "la") + "\" border=\"0\" alt=\"" + game_Name + "\" title=\"" + game_Name + "\" /></div>";
                    phStream.Controls.Add(litStream);
                }

                // -- ITEM DETAILS SECTION --
                // --------------------------

                // Is a pass required	  
                divPassRequired.Visible = IsGameMature(gameObject.GameRatingId).Equals(1);

                // description
                if (!gameObject.GameDescription.Equals("") && gameObject.GameDescription.Equals(null))
                {
                    lblDescription.Text = gameObject.GameDescription;
                }

                // stats
                lblCreatedDate.Text = FormatDate(gameObject.GameCreationDate) + " <span class=\"note\">(" + FormatDateTimeSpan(gameObject.GameCreationDate) + ")</span>";
                lblAccess.Text = GetGameAccessDescription(gameObject.GameAccessId);
                lblRaves.Text = gameObject.NumberOfRaves.ToString();
                lblReviews.Text = gameObject.NumberOfComments.ToString();
                lblNumViews.Text = gameObject.NumberOfViews.ToString();
                lblCurrentPopulation.Text = ShowGamePopulation(gameObject.NumberOfPlayers);

                // owner - if owner = zero use Kaneva defaults
                if (itemCreatorId > 0)
                {
                    //User userOwner = GetUserFacade().GetUser(itemCreatorId);
                    DevelopmentCompany devComp = GetDevelopmentCompanyFacade().GetDevelopmentCompany(itemCreatorId);

                    //imgAvatar.Src = GetProfileImageURL(userOwner.ThumbnailSmallPath, "sm", userOwner.Gender);
                    imgAvatar.Visible = false;
                    imgAvatar.Alt = devComp.CompanyName;

                    //aUserName.Attributes.Add("href", GetPersonalChannelUrl(userOwner.NameNoSpaces));
                    aUserName.InnerText = devComp.CompanyName;

                    /**** Additional Info Section - Owner Information ****/
                    // Username and URL 
                    //aUserName3.Attributes.Add("href", GetPersonalChannelUrl(userOwner.NameNoSpaces));
                }
                else
                {
                    imgAvatar.Src = "";
                    imgAvatar.Alt = "Kaneva";

                    //aUserName.Attributes.Add("href", ResolveUrl("~/3d-virtual-world/virtual-life.kaneva"));
                    aUserName.InnerText = "Kaneva";
                }

                // Show edit link?
                if (IsAdministrator() || IsCSR() || (itemCreatorId.Equals(this.GetCurrentUser().CompanyId)))
                {
                    aEdit.HRef = this.GetGameEditLink(gameId);
                    aEdit.Visible = true;
                }

                // share link
                txtShare.Text = Server.HtmlDecode("http://" + Request.Url.Host + GetGameDetailsLink(gameId));


                // -- ADS SECTION --
                // -----------------


                // -- OTHER MEDIA FROM OWNER SECTION --
                // ------------------------------------
                bool showMature = (DeveloperCommonFunctions.CurrentUser.UserInfo.IsAdult ? true : false);

                //string filter = "";
                //string orderby = "a.created_date DESC";

                //PagedDataTable pdtOwnersOtherMedia = StoreUtility.GetAssetsInChannel(
                //    CommunityUtility.GetPersonalChannelId(itemCreatorId), true, showMature,
                //    assetTypeId, filter, orderby, 1, 1);

                // -- RELATED ITEMS SECTION --
                // ---------------------------
                ucRelatedGames.GameObject = gameObject;
                ucRelatedGames.GamesPerPage = 5;

                //// -- COMMENTS SECTION --
                //// ----------------------
                ucReviews.GameObject = gameObject;
            }
            catch (Exception)
            {
            }

        }


        protected void RaveGame()
        {
            if (!Request.IsAuthenticated)
            {
                AjaxCallHelper.WriteAlert("You must be signed in to rave this item.");
                return;
            }


            if (!GetGameFacade().InsertGameRave(GetUserId(), GameId).Equals(1))
            {
                lblRaves.Text = Convert.ToString(Convert.ToInt32(lblRaves.Text) + 1);
            }

            //disables buttons if user is not an admin
            if (!this.IsAdministrator())
            {
                lbRave.Text = "<img src=\"" + ResolveUrl("~/images/icons/bb_raves.gif") + "\" runat=\"server\" width=\"16\" height=\"16\" border=\"0\"> Raved";
                lbRave.Enabled = false;
                lbRave.ToolTip = "You Raved this Item";
            }
        }

        private string GetAssetAddedToLibraryLink()
        {
            return "<img src=\"" + ResolveUrl("~/images/add_16_disabled.gif") + "\" runat=\"server\" width=\"16\" height=\"16\" border=\"0\"> Added to my library";
        }
        #endregion

        # region Event Handlers

        /// <summary>
        /// Handle an asset rave
        /// </summary>
        protected void lbRave_Click(Object sender, EventArgs e)
        {
            RaveGame();
        }

        /// <summary>
        /// Share with a blast
        /// </summary>
        protected void btnBlast_Click(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                // Check for any inject scripts
                if (DeveloperCommonFunctions.ContainsInjectScripts(txtBlast.Text))
                {
                    m_logger.Warn("User " + DeveloperCommonFunctions.GetUserId() + " tried to script from IP " + Common.GetVisitorIPAddress());
                    ShowErrorOnStartup("Your input contains invalid scripting, please remove script code and try again.", true);
                    return;
                }

                if (txtBlast.Text.Length == 0)
                {
                    AjaxCallHelper.WriteAlert("Please enter a blast.  You can not submit a blank blast.");
                    return;
                }

                if (txtBlast.Text.Length > DeveloperCommonFunctions.GetMaxCommentLength())
                {
                    AjaxCallHelper.WriteAlert("Your blast is too long.  Blasts must be " + Kaneva.KanevaGlobals.MaxCommentLength.ToString() + " characters or less");
                    return;
                }

                if (GetBlastFacade().SendLinkBlast(GetUserId(), 0, DeveloperCommonFunctions.CurrentUser.UserInfo.Username, DeveloperCommonFunctions.CurrentUser.UserInfo.NameNoSpaces, txtBlastTitle.Text, txtShare.Text, txtBlast.Text, "", DeveloperCommonFunctions.CurrentUser.UserInfo.ThumbnailSmallPath).Equals(1))
                {
                    spnBlastAlertMsg.InnerText = "Blast was sent.";
                    btnBlast.Visible = false;
                }
                else
                {
                    spnBlastAlertMsg.InnerText = "Failure sending Blast.";
                }

                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
            }

            txtBlast.Text = "";
        }

        /// <summary>
        /// Share with a private message
        /// </summary>
        protected void btnPMSend_Click(object sender, EventArgs e)
        {

            int toUserId = 0;
            string strSubject = Server.HtmlEncode(txtPrivateMsgSubject.Text.Trim());

            // Default the subject
            if (strSubject.Length.Equals(0))
            {
                strSubject = "No subject";
            }

            // Look up the user id via the username
            toUserId = Kaneva.UsersUtility.GetUserIdFromUsername(txtPrivateMsgTo.Text.Trim());

            if (txtPrivateMsgTo.Text.Trim() == "" || toUserId == 0)
            {
                spnPMAlertMsg.InnerText = "Please provide a valid Kaneva username.";
                return;
            }


            // Send the message
            if (SendMessage(toUserId, strSubject).Equals(-99))
            {
                spnPMAlertMsg.InnerText = "Message not sent. This member has blocked you from sending messages to them.";
                return;
            }
            else
            {
                spnPMAlertMsg.InnerText = "Message Sent!";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnPMAlertMsg', 4000);");
                //// If this is an asset share log it
                //if (Request["gameId"] != null)
                //{
                //    Kaneva.StoreUtility.ShareAsset(Convert.ToInt32(Request["gameId"]),
                //        GetUserId(), Common.GetVisitorIPAddress(), null, toUserId);
                //}
            }
        }

        /// <summary>
        /// Send Message
        /// </summary>
        /// <param name="toId"></param>
        private int SendMessage(int toId, string strSubject)
        {
            string strUrl = "<a href=\"" + txtShare.Text + "\">" + txtShare.Text + "</a><br />";

            // Insert a private message
            Message message = new Message(0, GetUserId(), toId, strSubject,
                strUrl + txtPrivateMsgBody.Text, new DateTime(), 0, (int)Kaneva.Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

            return GetUserFacade().InsertMessage(message);
        }


        #endregion

        #region Properties

        public int ItemOwnerId
        {
            get { return _assetOwnerId; }
            set { _assetOwnerId = value; }
        }

        public int GameId
        {
            get
            {
                if (ViewState["gameId"] != null)
                    return (int)ViewState["gameId"];
                else
                    return 0;
            }
            set
            {
                ViewState["gameId"] = value;
            }
        }


        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}
