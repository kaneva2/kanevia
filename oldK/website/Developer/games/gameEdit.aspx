<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="false" CodeBehind="gameEdit.aspx.cs" Inherits="KlausEnt.KEP.Developer.gameEdit" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<asp:Content ID="cnt_gameEdit" runat="server" ContentPlaceHolderID="cph_Body">
<script type="text/javascript" src="../jscript/dw_tooltip/dw_event.js"></script>
<script type="text/javascript" src="../jscript/dw_tooltip/dw_viewport.js"></script>
<script type="text/javascript" src="../jscript/dw_tooltip/dw_tooltip.js"></script>
<script type="text/javascript" src="../jscript/prototype.js"></script>
<script type="text/javascript">
<!--
var selectedCategories = new Array()

window.onload = ttINIT;

function ttINIT ()
{
	Tooltip.init();
	OnLoad();
}

function categorySelected(obj)
{  
  if (obj.checked)
  {    
    //a new checkbox is selected, pop the first selection if 3 are already selected
    if (selectedCategories.length == 3)
    {
      firstEle = selectedCategories.shift();
      firstEle.checked = false;
    }
    selectedCategories.push(obj);  
  }else
  {
    var index = -1;
    //remove it from the array if it exists
    for(var i = 0; i < selectedCategories.length; i++)
    {
      if(selectedCategories[i] == obj)
      {
        index = i;
        break;          
      }
    }
    if(index >= 0)
    {
      //and rearrange the list
      for(var i = index; i < selectedCategories.length -1; i++)
      {
        selectedCategories[i] = selectedCategories[i+1];
      }
      selectedCategories.pop();
    }
  }
}
//--></script>


<div id="contentWide">					
<div id="main">
<div id="main_content">



						    <!-- THIS IS THE TOP BANNER ROW WITH LABELING AND MESSAGING -->
							<div>
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										
													<h1>Modify Your Star<span><asp:hyperlink runat="server" visible="true" id="back" title="return to Previous location" onClick="NavigateToPreviousPage()">&#060;&#060; Back</asp:hyperlink></span></h1>
													
															<div class="warning">
																<asp:Label CssClass="formError" runat="server" id="lblMissingMessage" Visible="False" />	
															</div>
												
									</div>
									
									<div style="text-align:center">
									<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" cssclass="errBox black" style="width:70%;" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>
									</div>
									
									<!-- END TOP STATUS BAR -->	
							</div>
								
							<asp:Panel ID="pnl_Security" runat="server" Enabled="true">		
							<!-- THIS IS THE DATA BLOCK THAT SUMMARIZES THE GAME BEING MADE OR EDITED -->	
							
									<!--BASIC INFO -->
									
												<div class="module dataContainer">
													<h3>Basic Information - <IMG id="test" onmouseover="doTooltip(event,'<u>Thumbnail</u> : Displays a small image that represents this game. Set from the Description section. <br /><br /><u>Owner</u>: Displays the name of the organization of which this account is a member. <br /><br /><u>STAR Name </u>: The final name of your game project. Choose carefully, as this name cannot change later. <br /><br /><u>Access</u> : If Public, this game is visible to all Kaneva members. If Private, only members of your organization can access this game. <br /><br /><u>Select License</u> : Sets the capabilities of the game, depending on the license level. For the beta 0 test, this value must be set to Professional.')" onmouseout="hideTip();" src="../images/button_tooltip.gif" >
                                                        </h3>
																<div class="imgBasic">
                                                                  <div class="framesize-medium">
																	<div class="frame">
																		<span class="ct"><span class="cl"></span></span>
																			<div class="imgconstrain">
																				<img runat="server" id="img_gameIcon" border="0"/>
																			</div>
																		<span class="cb"><span class="cl"></span></span>
																	</div>
																</div>
                                                              </div>
															
																<table class="tblBasic">
																	<tr>
																		<th>Owner</th>
																		<td><asp:Label class="filter2" id="lblCreatedBy" runat="server"/></td>
																		<th>Access</th>
																		<td>
																			<asp:DropDownList class="formKanevaText" id="drpPermission" style="width:130px" runat="Server"/>
																			<asp:RequiredFieldValidator ID="rfdrpPermission" ControlToValidate="drpPermission" Text="*" ErrorMessage="Permissions is a required field." Display="Dynamic" runat="server"/>
																		</td>
																	</tr>
																	<tr>
																		<th>STAR Name</th>
																		<td>
																			<asp:TextBox ID="txtGameName" class="formKanevaText" style="width:300px" MaxLength="40" runat="server"/>
																			<asp:RequiredFieldValidator ID="rfItemName" ControlToValidate="txtGameName" Text="*" ErrorMessage="STAR name is a required field." Display="Dynamic" runat="server"/>
																			<b><asp:Label runat="server" id="Label2"/></b>
																		</td>
																		<td>
																		<asp:CheckBox id="chkGameStar" runat="server" Checked='false'/><asp:Label CssClass="filter2" runat="server" ID="Label1">Is GameSTAR</asp:label>
																		</td>
																		<!--<th>Status</th>
																		<td>
																			<asp:DropDownList class="formKanevaText" id="drpGameStatus" style="width:130px" runat="Server"/>
																			<asp:RequiredFieldValidator ID="rfdrpStatus" ControlToValidate="drpGameStatus" Text="*" ErrorMessage="Permissions is a required field." Display="Dynamic" runat="server"/>
																		</td>-->
																	</tr>
																	<tr>
																		<!--<th>Licensing</th>
																		<td colspan="2"><asp:DropDownList class="formKanevaText" id="ddlLicenseTypes" style="width:130px" runat="server"/></td>-->
																	</tr>
																</table>
															    <div class="clear"></div>
												</div>
																			
									<!-- CATEGORIZATION -->	
									
												<div class="module dataContainer">
													<h3>Categorization - <IMG id="IMG1" onmouseover="doTooltip(event,'All settings in this section are optional. Define the rating, search keywords, and genre(s) for your game. <br /><br /><u>Content Level</u> : Sets the appropriate age level for your game. This level should the most restrictive, based on the most mature content in your game. For example, if your game has 10 zones, of which 9 are All Ages and 1 is Mature, then set the Content Level to Mature. Currently, this value is for display purposes only, but will be enforced in future versions. <br /><br /><u>Tags</u> : Optionally specify single words representing your game. For example, a tags list for the World of Kaneva might be <i>&quot;social dance video entertainment music&quot;</i>. <br /><br /><u>Category</u> : Choose up to 3 genres that best describe your game. ')" onmouseout="hideTip();" src="../images/button_tooltip.gif" >
													</h3>
													<table class="tblCat">
														<tr>
															<th>Content Level</th>
															<td>
																<asp:DropDownList class="formKanevaText" id="drpMaturityLevel" style="width:130px" runat="Server"/>
																<asp:RequiredFieldValidator ID="rfdrpMature" ControlToValidate="drpMaturityLevel" Text="*" ErrorMessage="Permissions is a required field." Display="Dynamic" runat="server"/>
															</td>
														</tr>
														<tr>
															<th>Tags</th>
															<td>
																<asp:TextBox ID="txtKeywords" class="formKanevaText" style="width:400px" MaxLength="250" runat="server"/>
																<asp:RequiredFieldValidator ID="rftxtKeywords" ControlToValidate="txtKeywords" Text="*" ErrorMessage="Tags is a required field." Display="Dynamic" runat="server" Enabled="False"/>
																<asp:RegularExpressionValidator id="regtxtKeywords" ControlToValidate="txtKeywords" Text="*" Display="Dynamic" EnableClientScript="True" runat="server"/>
																<span class="blueInfo">(use spaces between words)<br><img runat="server" src="~/images/spacer.gif" width="10" height="1" ID="Img9"/>Tags are unique keywords that help to categorize your item to enable user searches. Use spaces between words.</span>
															</td>
														</tr>
														<tr>
															<th>Category</th>
															<td>
																<ajax:ajaxpanel ID="AjaxpanelAsset" runat="server">
																<asp:DataList runat="server" EnableViewState="True" ShowFooter="False" Width="100%" id="dlCategories"
																	cellpadding="0" cellspacing="0" border="0" style="margin-left: 10px;" RepeatColumns="4" RepeatDirection="Horizontal">
																	<ItemStyle CssClass="insideBoxText11" HorizontalAlign="left"/>
																	<ItemTemplate>
																		<asp:CheckBox id="chkCategory" runat="server" Checked='<%#GetCatChecked (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "category_id")))%>' onclick="javascript:categorySelected(this)"/><%# DataBinder.Eval(Container.DataItem, "description")%>
																		<input type="hidden" runat="server" id="hidCatId" value='<%#DataBinder.Eval(Container.DataItem, "category_id")%>' NAME="hidCatId"> 
																	</ItemTemplate>
																</asp:DataList>
																</ajax:ajaxpanel>                       
																<asp:CustomValidator id="cvDlCategories" Text="*" Display="Dynamic" ErrorMessage="You must select at least one category." runat="server" Enabled="False"/>
																<span class="blueInfo" style="margin:20px 0 0 0">You may choose up to 3 categories</span>
															</td>
														</tr>
													</table>
												</div>
											
									<!-- DESCRIPTION -->	
									
												<div class="module dataContainer">
													<span class="ct"><span class="cl"></span></span>
													<h3>Description - <IMG id="IMG2" onmouseover="doTooltip(event,'All settings in this section are optional. Provide a graphic and textual description of your game. <br /><br /><u>Image path</u> : A small JPG image file on your system to represent your game. <br /><br /><u>Short Description</u>: A brief (one-line) text-only description of your game. <br /><br /><u>Detailed Description</u>: Additional details about your game. May contain special formatting and HTML if necessary.')" onmouseout="hideTip();" src="../images/button_tooltip.gif" >
													</h3>
										            <table class="tblPreview">
														<tr>
															<th>
															    <!-- START ASP controlled keep this -->
																<table id="tblThumbPreview" runat="server">
																	<tr><td><img runat="server" id="imgPreview" style="border: 1px solid #ededed" /><br />Preview</td></tr>
																</table>
															    <!-- END ASP controlled keep this -->
															</th>
															<td >Image Path
												                <div id="divbrowseTHUMB">
                                                                    <input type="file" runat="server" id="browseTHUMB" style="width:360px;" />
                                                                </div>
															</td>
														</tr>
													</table>
													<table class="tblDescription">
														<!-- SHORT DESCRIPTION -->	
														<tr>
															<th>Short Description</th>
															<td>
																<asp:TextBox ID="txtTeaser" TextMode="multiline" Rows="3" class="formKanevaText" style="width:95%" MaxLength="1000" runat="server"/>
															</td>
														</tr>
														<!-- DETAILED DESCRIPTION -->	
														<tr>
															<th>Detailed Description</th>
															<td>
															    <ajax:ajaxpanel ID="ajp_advanced" runat="server">
																	<asp:CheckBox id="chkAdvanced" runat="server" AutoPostBack="True" OnCheckedChanged="chkAdvanced_Click"/>
																	<asp:Label CssClass="filter2" runat="server" ID="Label3">Show advanced buttons</asp:label><br>
																	<CE:Editor id="txtBody" BackColor="#ededed" runat="server" MaxTextLength="2000" width="95%" Height="200px" ShowHtmlMode="True" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/asset.config" AutoConfigure="None" ></CE:Editor>
																</ajax:ajaxpanel>
															</td>
														</tr>
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
																					
									<!-- PATCH URL -->	
									<ajax:ajaxpanel ID="ajp_PatchURL" runat="server">
									
												<div class="module dataContainer">
													<span class="ct"><span class="cl"></span></span>
													<h3>Patch URL - <IMG id="IMG12" onmouseover="doTooltip(event,'<u>Add New Patch URL</u> : You don&rsquo;t need to provide a URL until you want your game code to be available externally and you have a publicly available web address for your game software. Provide an HTTP web address the public can use to download your game software. The Kaneva updater program uses this address to download or update the member&rsquo;s system with your STAR software and data.')" onmouseout="hideTip();" src="../images/button_tooltip.gif" >
													</h3>
													<table class="tblPatchURL">
													    <tr>
                                                        <td><asp:LinkButton id="lbn_NewPatchURL" runat="server" causesvalidation="false" onclick="lbn_NewPatchURL_Click">Add New PatchURL</asp:LinkButton></td>
                                                        </tr>
													    <tr>
															<td>
                                                                <asp:gridview id="dg_PatchURLS" runat="server" onRowDataBound="dg_PatchURLS_RowDataBound" onsorting="dg_PatchURLS_Sorting" OnRowCreated="dg_PatchURLS_OnRowCreated" autogeneratecolumns="False" width="98%" OnPageIndexChanging="dg_PatchURLS_PageIndexChanging" OnRowEditing="dg_PatchURLS_RowEditing" OnRowDeleting="dg_PatchURLS_RowDeleting" AllowSorting="True" border="1" cellpadding="2" PageSize="10" AllowPaging="True">
                                                                    <RowStyle BackColor="White"></RowStyle>
                                                                    <HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
                                                                    <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
                                                                    <FooterStyle BackColor="#FFffff"></FooterStyle>
                                                                    <Columns>
                                                                        <asp:BoundField HeaderText="URL Patch ID" DataField="patchURL_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                                                                        <asp:BoundField HeaderText="Patch ID" DataField="patch_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                                                                        <asp:BoundField HeaderText="Game ID" DataField="game_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                                                                        <asp:BoundField HeaderText="Patch URL" DataField="patch_url" SortExpression="patch_url" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                                                                        <asp:CommandField CausesValidation="False" ShowEditButton="True" ></asp:CommandField>        
                                                                        <asp:TemplateField >
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton id="lbn_confirmPatchDelete" causesvalidation="false" Runat="server" OnClientClick="return confirm('Are you sure you want to delete this patch URL?');" CommandName="Delete">Delete</asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:gridview>
														    </td>
														</tr>
														<tr>
														    <td align="left">
														        <table id="tbl_PatchData" visible="false" runat="server">
														            <tr>
														                <th>Patch URL:</th>
														                <td><asp:Label id="hdn_PatchIndex" visible="false" runat="server" /><asp:Label id="hdn_patchURLID" runat="server" visible="false" /><asp:TextBox ID="txtPatchURL" class="formKanevaText" style="width:300px" runat="server"/><asp:Label runat="server" style="color:red" id="patchURLRequired" visible="false">*</asp:Label></td>
														                <td><asp:button id="btnUpdatePatchURL" runat="Server" onClick="btnUpdatePatchURL_Click" CausesValidation="False" class="Filter2" Text="Save"/>&nbsp;&nbsp;<asp:button id="btnPatchURLCancel" runat="Server" CausesValidation="False" onClick="btnPatchURLCancel_Click" class="Filter2" Text="Cancel"/></td>
														            </tr>
														        </table>
														    </td>
														</tr>
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</ajax:ajaxpanel>
									
									
									<!-- SERVER MANAGEMENT -->
									<ajax:ajaxpanel ID="ajp_gameServers" runat="server">	
												<div class="module dataContainer">
													<span class="ct"><span class="cl"></span></span>
													<h3>Game Servers - <IMG id="IMG13" onmouseover="doTooltip(event,'In addition to the game server(s) available to the public, you need to specify each Private game server that team members use to test your game environment in-house.  Additions and changes are not set until you click the Commit Changes button at the bottom of the form. <br /><br /><u>Add New Server</u> : Allows you to define a new game server for testing or public deployment. Displays the fields listed below. <br /><br /><u>Server name</u> : Must be the Computer Name of the computer where the game server process is running. The Computer Name is available from the computer&rsquo;s System control panel > Computer Name tab. <br /><br /><u>Port</u> : The communications port used for game communications. Your firewall will need to permit UDP traffic to this port. Keep the default unless you have a good reason to change it. <br /><br /><u>Visibility</u> : Who can see and use this game server? <br /><i>Public</i> : Any Kaneva member can see and use this game server. <br /><i>Private</i> : Only your team members can see and use this game server.')" onmouseout="hideTip();" src="../images/button_tooltip.gif" >
													</h3>
													
													<table class="tblServers">
													    <tr>
                                                        <td><asp:LinkButton id="lbn_NewServer" runat="server" causesvalidation="false" onclick="lbn_NewServer_Click">Add New Server</asp:LinkButton></td>
                                                        </tr>
													    <tr>
															<td>
                                                                <asp:gridview id="dg_GameServers" runat="server" onRowDataBound="dg_GameServers_RowDataBound" onsorting="dg_GameServers_Sorting" OnRowCreated="dg_GameServers_OnRowCreated" OnRowDeleting="dg_GameServers_RowDeleting" autogeneratecolumns="False" width="98%" OnPageIndexChanging="dg_GameServers_PageIndexChanging" OnRowEditing="dg_GameServers_RowEditing" AllowSorting="True" border="1" cellpadding="2" PageSize="10" AllowPaging="True">
                                                                    <RowStyle BackColor="White"></RowStyle>
                                                                    <HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
                                                                    <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
                                                                    <FooterStyle BackColor="#FFffff"></FooterStyle>
                                                                    <Columns>
                                                                        <asp:BoundField HeaderText="Server ID" DataField="server_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                                                                        <asp:BoundField HeaderText="Game ID" DataField="game_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                                                                        <asp:BoundField HeaderText="Visibility ID" DataField="visibility_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                                                                        <asp:BoundField HeaderText="Modifiers ID" DataField="modifiers_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                                                                        <asp:BoundField HeaderText="Server Name" DataField="server_name" SortExpression="server_name" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
																		<asp:TemplateField HeaderText="Visible" itemstyle-horizontalalign="center" headerstyle-horizontalalign="center" >
																			<ItemTemplate>
																				<%#GetGameVisibiltyDescription(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "visibility_id")))%>
																			</ItemTemplate>
																		</asp:TemplateField>																	
																		<asp:TemplateField HeaderText="Status" itemstyle-horizontalalign="center" headerstyle-horizontalalign="center" >
																			<ItemTemplate>
																				<%#GetServerStatusDescription(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "server_status_id")))%>
																			</ItemTemplate>
																		</asp:TemplateField>
                                                                       <asp:BoundField HeaderText="Server Started" DataField="server_started_date" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                                                                       <asp:BoundField HeaderText="IP Address" DataField="ip_address" ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-horizontalalign="center" headerstyle-horizontalalign="center" ></asp:BoundField>
                                                                       <asp:BoundField HeaderText="Port" DataField="port" ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-horizontalalign="center" headerstyle-horizontalalign="center" ></asp:BoundField>
                                                                       <asp:BoundField HeaderText="Concurrency" DataField="number_of_players" SortExpression="number_of_players" ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-horizontalalign="center" headerstyle-horizontalalign="center" ></asp:BoundField>
                                                                       <asp:BoundField HeaderText="Max Players" DataField="max_players" Visible="false" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                                                                       <asp:BoundField HeaderText="Last Ping Date" DataField="last_ping_datetime" SortExpression="last_ping_datetime" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                                                                       <asp:BoundField HeaderText="Last Start Date" DataField="server_started_date" SortExpression="server_started_date" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                                                                       <asp:CommandField CausesValidation="False" ShowEditButton="True"></asp:CommandField>        
                                                                       <asp:TemplateField>
                                                                           <ItemTemplate>
                                                                               <asp:LinkButton id="lbn_confirmServerDelete" causesvalidation="false" Runat="server" OnClientClick="return confirm('Are you sure you want to delete this server?');" CommandName="Delete">Delete</asp:LinkButton>
                                                                           </ItemTemplate>
                                                                       </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:gridview>
														    </td>
														</tr>
														<tr>
														    <td>
														        <table runat="server" id="tbl_editServer" visible="false">
														            <tr>
														                <th>Server Name:</th>
														                <td><asp:Label id="hdn_ServerListIndex" visible="false" runat="server" /><asp:Label id="hdn_ServerID" visible="false" runat="server" /><asp:TextBox ID="txtServerName" class="formKanevaText" style="width:150px" runat="server"/><asp:Label runat="server" style="color:red" id="serverNameRequired" visible="false">*</asp:Label></td>
														                <th>Port:</th>
														                <td><asp:TextBox id="tbx_PortNumber" runat="server" class="formKanevaText" style="width:50px"/><asp:Label runat="server" style="color:red" id="portNumberRequired" visible="false">*</asp:Label></td>
														                <th>Visibility:</th>
														                <td><asp:DropDownList class="formKanevaText" id="drpServerVisibility" style="width:100px" runat="Server"/></td>
														                <td><asp:button id="btnUpdateServer" runat="Server" onClick="btnUpdateServer_Click" CausesValidation="False" class="Filter2" Text="Save"/>&nbsp;&nbsp;<asp:button id="btnServerCancel" runat="Server" CausesValidation="False" onClick="btnServerCancel_Click" class="Filter2" Text="Cancel"/></td>
														            </tr>
														        </table>
														    </td>
														</tr>

													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											
										
									</ajax:ajaxpanel>

									<!-- SUBMIT BUTTONS -->
									<div style="width:100%;text-align:right;margin-top:40px"><asp:button id="btnUpdate" runat="Server" onClick="btnUpdate_Click" CausesValidation="False" class="Filter2" Text="Commit Changes"/>&nbsp;&nbsp;<asp:button id="btnCancel" runat="Server" CausesValidation="False" onClick="btnCancel_Click" class="Filter2" Text="Cancel"/></div>
									
									<!-- END TABLE DESIGNATED SECTIONS -->
					
</asp:Panel>		
</div>
</div>
</div>

</asp:Content>
