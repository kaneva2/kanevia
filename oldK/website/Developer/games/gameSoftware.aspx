﻿<%@ Page Language="C#"  MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="false" CodeBehind="gameSoftware.aspx.cs" Inherits="KlausEnt.KEP.Developer.gameSoftware" %>
<asp:Content ID="cnt_gameResource" runat="server" ContentPlaceHolderID="cph_Body">


<div id="contentWide">					
    <div id="main">
        <div id="main_content">
            <h1 class="thirdparty png">Third Party Software<span><asp:LinkButton runat="server" visible="true" id="back" title="return to Previous location" onClick="NavigateToPreviousPage">&#060;&#060; Back</asp:LinkButton></span></h1>
            <div>
                <div class="clear"><!-- Clear the floats --></div>
                <div class="licsStyle">
                    <h3>Software Sources</h3>
                     <ul>
                        <li>AES - texture encryption, looks like a GPL </li>
                        <li>ColladaDOM - SCEA license - http://research.scea.com/scea_shared_source_license.html</li>
                        <li>* libogg-1.1.4 and libvorbis-1.2.3 - audio format processing - BSD license </li>
                        <li>Lua - openly free</li>
                        <li>mysql++ - LGPL</li>
                        <li>nvidia-texture-tools - GPL</li>
                        <li>
                            Smart Property Grid - used in Editor - free <br />
                            The main points subject to the terms of the License are:
                            <ul>
                                <li>Source Code and Executable Files can be used in commercial applications;</li> 
                                <li>Source Code and Executable Files can be redistributed; and</li>  
                                <li>Source Code can be modified to create derivative works.</li>  
                                <li>No claim of suitability, guarantee, or any warranty whatsoever is provided. The software is provided "as-is".</li>  
                                <li>The Article(s) accompanying the Work may not be distributed or republished without the Author's consent </li> 
                            </ul>
                        </li>
                        <li>TCL - used in Editor - free</li>
                        <li>TinyXML - looks free </li>
                        <li>WMSDK - Windows Media SDK - Microsoft </li>
                        <li>Xalan - XSLT transformations - GPL like</li>
                        <li>Xerces-2.6 - XML parser - Apache license - GPL like - http://www.apache.org/licenses/LICENSE-2.0</li>
                        <li>ZLIB - compression library GPL like</li>
                     </ul>
                     <br />
                </div>
            </div>
        </div>
    </div>
</div>
</asp:Content>
