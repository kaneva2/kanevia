<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="false" validateRequest="false" CodeBehind="gameDetails.aspx.cs" Inherits="KlausEnt.KEP.Developer.gameDetails" %>
<%@ Register TagPrefix="Kaneva" TagName="RelatedGames" Src="../usercontrols/relatedGames.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Reviews" Src="../usercontrols/gameReviews.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>


<asp:Content ID="cnt_starsDetails" runat="server" ContentPlaceHolderID="cph_Body">
<script type="text/javascript" src="../jscript/prototype.js"></script>

<script type="text/javascript" src="../jscript/media-share.js"></script>
<link href="../css/media-share.css" rel="stylesheet" type="text/css" />

<div id="contentWide">					
    <div id="main">
        <div id="main_content">
            <div id="divAccessMessage" runat="server" visible="false">
                <div class="module whitebg">
                    <span class="ct"><span class="cl"></span></span>
			            <h2 id="h2Title" runat="server"></h2>
			            <table cellpadding="6" cellspacing="0" border="0" width="95%">
				            <tr>
					            <td style="width:80%" align="left" valign="top" id="tdActionText" runat="server"></td>
					            <td style="width:20%" align="right" valign="top" id="tdButton" runat="server">
						            <asp:button id="btnAction" runat="server"></asp:button>
					            </td>
				            </tr>
				            <tr>
				                <td colspan="2" align="left" valign="top" id="tdPolicyText" runat="server"></td>
				            </tr>
			            </table>
		            <span class="cb"><span class="cl"></span></span>
	            </div>
            </div>
            <div id="divDetails" runat="server" visible="false">
	            <div id="pageheader"><h1 id="gameTitle" runat="server"></h1></div>
		        <!-- ABOUT MEDIA -->
    	        <p class="formspacer_small" />
		        <div class="passrequired" visible="false" runat="server" id="divPassRequired"></div>
		        <div id="contentLeft">
                    <div style="background-color:#192e3a; width:398px; overflow:hidden; border:1px solid #1f3946;">
			            <!-- PLAYER / IMAGE -->
			            <asp:placeholder runat="server" id="phStream"/>
			        </div>
			        <ajax:ajaxpanel id="ajpActionBar" runat="server">																
			            <!-- TOOLBAR -->
			            <ul id="mediaNav">
				            <li id="rave"><asp:linkbutton id="lbRave" onclick="lbRave_Click" visible="true" runat="server" tooltip="Rave this Item"><img id="Img6" src="~/images/raveit_bt.gif" runat="server" alt="raveit_bt.gif" border="0" />Rave this</asp:linkbutton></li>
				            <li id="share">
				                <a href="#"  title="E-mail this, post to del.icio.us, etc." id="akst_link_1" class="akst_share_link" rel="nofollow" runat="server">
				                <img id="Img9" src="~/images/icon_share.gif" runat="server" alt="icon_share.gif" border="0" />Share This</a>
				            </li>
			            </ul>
			            <div class="clear"><!-- clearing floats --></div>
			        </ajax:ajaxpanel>																
			        <table id="avatarData">
				        <!-- OWNER AVATAR -->
				        <tr>
					        <td width="150px" valign="top" style="text-align:left"><strong>Owner:</strong></td>
					        <td valign="top" align="left" >
						        <!--<div class="framesize-xsmall">
							        <div class="restricted" visible="false" runat="server" id="div2"></div>
                                    <div class="frame">
                                        <span class="ct"><span class="cl"></span></span>
                                        <div class="imgconstrain">
                                            <a class="bodytext" id="aUserName3" runat="server"><img runat="server" id="imgAvatar" src="~/images/KanevaIcon01.gif" border="0" /></a>
                                        </div>
                                        <span class="cb"><span class="cl"></span></span>
                                    </div>
						        </div>-->
						        <a id="aUserName" runat="server"></a>
					        </td>
				        </tr>		 
				        <tr>
					        <td valign="top" style="text-align:left"><strong>AccessType:</strong></td>
					        <td valign="top" style="text-align:left"><asp:label id="lblAccess" runat="server" /></td>
				        </tr>		 
				        <tr>
					        <td valign="top" style="text-align:left"><strong>Created:</strong></td>
					        <td valign="top" style="text-align:left"><asp:label id="lblCreatedDate" runat="server"/></td>
				        </tr>		 
				        <tr> 
					        <td valign="top" style="text-align:left"><strong>Description:</strong></td>
					        <td valign="top" style="text-align:left"><asp:label id="lblDescription" runat="server" Text="N/A" /></td>
				        </tr>
				        <tr>
					        <td valign="top" style="text-align:left"><strong>Population:</strong></td>
					        <td valign="top" style="text-align:left"><asp:label id="lblCurrentPopulation" runat="server" text="Empty"/></td>
				        </tr>		 
				        <tr>
					        <td valign="top" style="text-align:left"><strong>Raves:</strong></td>
					        <td valign="top" style="text-align:left"><ajax:ajaxpanel id="Ajaxpanel1" runat="server"><asp:label id="lblRaves" runat="server" text="0"/></ajax:ajaxpanel></td>
				        </tr>		 
				        <tr>
					        <td valign="top" style="text-align:left"><strong>Reviews:</strong></td>
					        <td valign="top" style="text-align:left"><asp:label id="lblReviews" runat="server" /></td>
				        </tr>		 
				        <tr>
					        <td valign="top" style="text-align:left"><strong>Visits:</strong></td>
					        <td valign="top" style="text-align:left"><asp:label id="lblNumViews" runat="server" /></td>
				        </tr>		 
				        <tr>
					        <td>
					            <asp:textbox style="width: 355px;" cssclass="formKanevaText" id="txtShare" runat="server" columns="60" readonly="True" Visible="false"></asp:textbox><br />
					        </td>
                            <td>&nbsp;</td>
				        </tr>
			        </table>
		            <span style="width:90%;text-align:right;"><a id="aEdit" runat="server" visible="False">Edit this item</a></span>
			        <span class="cb"><span class="cl"></span></span>
	           </div>
               <div id="contentRight">
		            <!--
	                <div id="promo1" class="fullbanner">
		                <a id="A1" href="~/community/communitypage.aspx?communityId=1118&pageId=772341" runat="server"><img id="Img12" src="~/images/promo_kaneva.gif" runat="server" border="0"/></a>
	                </div>
	                <div id="promo2" class="fullbanner">
		                <table id="_ctl51_tblGoogleAdds" cellpadding="0" cellspacing="0" border="0" width="468px">
			                <tr>
				                <td align="center">
				                <script type="text/javascript">-->
			                <!--
					                google_ad_client = "pub-7044624796952740"; google_ad_width = 468; google_ad_height = 60; google_ad_format = "468x60_as"; google_ad_type = "text"; google_ad_channel ="7248390549";google_color_border = "FFFFFF";google_color_bg = "FFFFFF";google_color_link = "018AAA";google_color_text = "737373";google_color_url = "018AAA";<asp:Literal runat="server" id="litAdTest"/>; //--><!--</script> 
					                <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
					                </script>
				                </td>
			                </tr>
		                </table>
	                </div>
		            -->
	                <!--START RELATED MEDIA-->
		            <div id="related_media">
			            <Kaneva:RelatedGames runat="server" id="ucRelatedGames" />
		            </div>																
		            <!--END RELATED MEDIA-->
		            <!--START COMMENTS-->
		            <div class="module whitebg">
			            <span class="ct"><span class="cl"></span></span>
            								
				            <Kaneva:Reviews runat="server" id="ucReviews" />

			            <span class="cb"><span class="cl"></span></span>
		            </div>
		            <!--END COMMENTS-->
                </div>
            </div>
        </div>
    </div>
</div>
											
<!-- Share This BEGIN -->
<asp:literal id="litNotLoggedShareStyle"></asp:literal>
    <div id="akst_form">
		<a href="javascript:void($('akst_form').style.display='none');" class="akst_close">Close</a>
		<ul class="tabs">
			<li id="akst_tab1" onclick="akst_share_tab('1');" >Blast</li>
			<li id="akst_tab2" onclick="akst_share_tab('2');" >Private</li>			
			<li id="akst_tab3" onclick="akst_share_tab('3');" >E-mail</li>
			<li id="akst_tab4" onclick="akst_share_tab('4');" >Social Web</li>						
		</ul>
		<div class="clear"><!-- Clear the floats --></div>
		<div id="akst_blast">
		    <fieldset>
		        <ajax:ajaxpanel id="ajpShareBlast" runat="server">		  
		            <ul>
		                <li>Blast this to your friends!</li>
		                <li>Link Title: <asp:textbox id="txtBlastTitle" runat="server" width="265"></asp:textbox></li>
		                <li>Your Message (Link will automatically be inserted):</li>	    
		                <li><asp:textbox TextMode="multiline" id="txtBlast" runat="server" width="325" height="80"></asp:textbox></li>
		                <li><asp:button id="btnBlast" onClick="btnBlast_Click" Text="Blast It!" runat="server" />
		                    <h2 class="alertmessage"><span id="spnBlastAlertMsg" runat="server"></span></h2>
		                </li>		  
		            </ul>	
		        </ajax:ajaxpanel>	  		 
		    </fieldset>
		</div>		
	    <div id="akst_private">
		    <fieldset>	
		        <ajax:ajaxpanel id="ajpSharePM" runat="server">	  
		            <ul>
		                <li>Private Message To Kaneva Member:</li>
		                <li><asp:textbox  id="txtPrivateMsgTo" runat="server" width="325"></asp:textbox></li>
		                <li>Subject:</li>
		                <li><asp:textbox  id="txtPrivateMsgSubject" runat="server" width="325"></asp:textbox></li>
		                <li>Your Message (Link will automatically be inserted):</li>
		                <li><asp:textbox TextMode="multiline" id="txtPrivateMsgBody" runat="server" width="325" height="50"></asp:textbox></li>
		                <li><asp:button id="btnPMSend" onClick="btnPMSend_Click" Text="Send" runat="server" />
		                 <h2 class="alertmessage"><span id="spnPMAlertMsg" runat="server"></span></h2>
		                </li>		  
		            </ul>	
		        </ajax:ajaxpanel>	  		 
		    </fieldset>
		</div>
		<div id="akst_email">			
		    <fieldset>
			    <legend>E-mail It</legend>
					<ul>
						<li>
							<label>1. Copy and paste this link into an email or instant message:</label>
							<asp:textbox id="shareLink" runat="server" class="akst_text" width="325" onclick="this.select();"></asp:textbox>							
						</li>
						<li>
							<label>2. Send this media using your computer's email application:</label>
							<asp:hyperlink id="shareMailto" runat="server" NavigateUrl="" text="Email Media" ></asp:hyperlink>
						</li>					
					</ul>
					<input type="hidden" name="akst_action" value="send_mail" />
					<input type="hidden" name="akst_post_id" id="akst_post_id" value="" />
			</fieldset>			
		</div>		
		<div id="akst_social">
			<ul>
				<li><a href="#" id="akst_delicious" target="_blank">del.icio.us</a></li>
				<li><a href="#" id="akst_digg" target="_blank">Digg</a></li>
				<li><a href="#" id="akst_furl" target="_blank">Furl</a></li>
				<li><a href="#" id="akst_netscape" target="_blank">Netscape</a></li>
				<li><a href="#" id="akst_yahoo_myweb" target="_blank">Yahoo! My Web</a></li>
				<li><a href="#" id="akst_stumbleupon" target="_blank">StumbleUpon</a></li>
				<li><a href="#" id="akst_google_bmarks" target="_blank">Google Bookmarks</a></li>
				<li><a href="#" id="akst_technorati" target="_blank">Technorati</a></li>
				<li><a href="#" id="akst_blinklist" target="_blank">BlinkList</a></li>
				<li><a href="#" id="akst_newsvine" target="_blank">Newsvine</a></li>
				<li><a href="#" id="akst_magnolia" target="_blank">ma.gnolia</a></li>
				<li><a href="#" id="akst_reddit" target="_blank">reddit</a></li>
				<li><a href="#" id="akst_windows_live" target="_blank">Windows Live</a></li>
				<li><a href="#" id="akst_tailrank" target="_blank">Tailrank</a></li>
			</ul>
			<div class="clear"><!-- Clear the Floats --></div>
		</div>
	</div>
<!-- Share This END -->
</asp:Content>