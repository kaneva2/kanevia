<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="false" CodeBehind="gameDirectory.aspx.cs" Inherits="KlausEnt.KEP.Developer.gameDirectory" %>
<%@ Register TagPrefix="Developer" TagName="Directory" Src="../usercontrols/gameDirectoryResults.ascx" %>

<asp:Content ID="cnt_starsDirectory" runat="server" ContentPlaceHolderID="cph_Body">
<div>
    <Developer:Directory ID="starsDirectory" runat="server" />
</div>
</asp:Content>