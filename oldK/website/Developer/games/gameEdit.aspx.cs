///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using log4net;
using System.Drawing;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using KlausEnt.KEP.Kaneva;

namespace KlausEnt.KEP.Developer
{
    /// <summary>
    /// This page is used to add and edit games and all the related items such as servers and patch URLs. 
    /// This page is based on the original assetEdit page but is built based on some fundamental differences this page (at present) 
    /// is ONLY used for the management/creation of a game. The assetEdit page served as the edit page for many different things.  
    /// </summary>
    public partial class gameEdit : BasePage
    {
        #region Declarations

        protected Pager pgTop, pgBottom;
        protected StoreFilter filStore, pageSort;
        protected System.Web.UI.HtmlControls.HtmlInputHidden GameLicId;
        protected HtmlInputFile browseTHUMB;

        // License stuff
        protected HtmlInputRadioButton optPublic, optCC, optPersonal, optCommercial, optOther;
        protected DropDownList drpCCLicense, ddlLicenseTypes, drpGameStatus, drpMaturityLevel, drpServerVisibility, drpServerStatus, drpServerType;
        protected TextBox txtPerAdditional, txtCommAdditional, txtLicenseName, txtLicenseURL;
        protected HtmlInputFile inpAvatar;
        protected Label lblAssetSize, hdn_ServerID, hdn_patchURLID;
        protected Literal lblItemType;
        protected TextBox txtTeaser;
        protected CuteEditor.Editor txtBody;
        protected TextBox txtGameName, txtKeywords, txtImageCaption;
        protected Label lblCreatedBy, lblPrice, lblAssetId;
        protected DropDownList drpType, drpPermission;
        protected CheckBox chkMature;
        protected HtmlTableRow trGameInfo, trServers, trGameIconInfo1, trGameIconInfo2, trGameIconInfo3;
        protected Label lblItemName, lblMissingMessage;
        protected HtmlInputButton btnUpload, btnUploadIcon, btnUploadImage, btnClearThumb;
        protected Button btnClearIcon;
        //protected HtmlInputCheckBox chkDaily, chkMonthly, chkYearly;
        protected CheckBox chkAdvanced;
        protected HyperLink back;

        // Categories
        protected DataList dlCategories;
        protected int m_category1, m_category2, m_category3;

        // Thumbnail
        protected HtmlTableRow trThumbCaption, trAssetType;

        // Game info
        protected Repeater rptGameServers;
        protected RegularExpressionValidator regtxtKeywords;

        // Channels
        protected Repeater rptChannels;
        protected Button btnRemoveChannel;
        protected HtmlInputHidden hidChannelId;

        // License Info
        protected Label lblLicense, lblLicenseURL;
        protected CustomValidator cvDlCategories;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected System.Web.UI.WebControls.ValidationSummary valSum;
        protected System.Web.UI.WebControls.RequiredFieldValidator rfdrpType;
        protected System.Web.UI.WebControls.RequiredFieldValidator rfItemName;
        protected System.Web.UI.WebControls.Label Label2;
        protected System.Web.UI.WebControls.RequiredFieldValidator rfdrpPermission;
        protected System.Web.UI.WebControls.RangeValidator rngAmount;
        protected System.Web.UI.WebControls.RequiredFieldValidator rfdtxtAmount;
        protected System.Web.UI.WebControls.RequiredFieldValidator rfdtxtAmountDay;
        protected System.Web.UI.WebControls.RequiredFieldValidator rfdtxtAmountMonthly;
        protected System.Web.UI.WebControls.RequiredFieldValidator rfdtxtAmountYear;
        protected System.Web.UI.WebControls.RequiredFieldValidator rftxtKeywords;
        protected MagicAjax.UI.Controls.AjaxPanel AjaxpanelAsset;
        protected System.Web.UI.WebControls.Label Label3;
        protected MagicAjax.UI.Controls.AjaxPanel Ajaxpanel2;
        protected System.Web.UI.WebControls.LinkButton lnkAddChannel;
        protected System.Web.UI.WebControls.Button btnCancel;
        protected System.Web.UI.WebControls.Button btnUpdate;
        protected MagicAjax.UI.Controls.AjaxPanel Ajaxpanel1;
        protected System.Web.UI.WebControls.Button DeleteServer;
        protected System.Web.UI.HtmlControls.HtmlImage Img1;
        protected System.Web.UI.HtmlControls.HtmlImage Img2;
        protected System.Web.UI.HtmlControls.HtmlImage Img10;
        protected System.Web.UI.HtmlControls.HtmlImage Img5;
        protected System.Web.UI.HtmlControls.HtmlImage Img6;
        protected System.Web.UI.HtmlControls.HtmlImage Img7;
        protected System.Web.UI.HtmlControls.HtmlImage Img3;
        protected System.Web.UI.HtmlControls.HtmlImage Img8;
        protected System.Web.UI.HtmlControls.HtmlImage Img9;
        protected System.Web.UI.HtmlControls.HtmlImage Img24;
        protected System.Web.UI.HtmlControls.HtmlImage Img23;
        protected System.Web.UI.HtmlControls.HtmlTableRow Tr1;
        protected System.Web.UI.HtmlControls.HtmlAnchor aBrowse;
        protected System.Web.UI.HtmlControls.HtmlImage Img12;
        protected System.Web.UI.HtmlControls.HtmlAnchor A1;
        protected System.Web.UI.HtmlControls.HtmlImage Img13;
        protected System.Web.UI.HtmlControls.HtmlImage Img14;
        protected System.Web.UI.HtmlControls.HtmlAnchor A2;
        protected System.Web.UI.HtmlControls.HtmlImage Img15;
        protected System.Web.UI.HtmlControls.HtmlAnchor A3;
        protected System.Web.UI.HtmlControls.HtmlAnchor A4;
        protected System.Web.UI.HtmlControls.HtmlImage Img16;
        protected System.Web.UI.HtmlControls.HtmlImage Img17;
        protected System.Web.UI.HtmlControls.HtmlImage Img18;
        protected System.Web.UI.HtmlControls.HtmlAnchor A7;
        protected System.Web.UI.HtmlControls.HtmlImage Img21;
        protected System.Web.UI.HtmlControls.HtmlImage Img22;

        #endregion

        protected gameEdit() 
        {
            Title = "STARS Edit";
        }

        #region PageLoad

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Load (object sender, System.EventArgs e)
        {

            //set bread crumbs
            if (!IsPostBack)
            {
                //set Main Navigation
                Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV] = usercontrols.Navigation.TAB.MY_GAMES;

                //store the previous bread crumb in this pages viewstate incase it needs to be recalled
                //this is done to prevent navigation back to the wrong page when a user has multiple tabs
                //open on the same browser
                if (OriginalBreadCrumb == null)
                {
                    string LastBreadCrumbText = (GetLastBreadCrumb() != null ? GetLastBreadCrumb().Text : "");
                    OriginalBreadCrumb = (LastBreadCrumbText != "Game Edit" ? LastBreadCrumbText : "");
                    //set default URL
                    back.NavigateUrl = ResolveUrl("~/games/gameLibrary.aspx");
                    //try and get last URL
                    try
                    {
                        back.NavigateUrl = GetBreadCrumbByName(OriginalBreadCrumb).Hyperlink;
                    }
                    catch (Exception)
                    {
                        AddBreadCrumb(GetNewBreadCrumb("My STARs", "~/games/gameLibrary.aspx", "", 0));
                        OriginalBreadCrumb = "My STARs";
                    }
                }

                //check security level
                switch (CheckUserAccess(MANAGE_STARS))
                {
                    case ACCESS_FULL:
                        //do nothing
                        break;
                    case ACCESS_READ:
                        ReadOnly();
                        break;
                    case ACCESS_NONE:
                    default:
                        NavigateBackToBreadCrumb(OriginalBreadCrumb);
                        break;
                }
                
                //get needed parameters if valid proceed if not redirect
                //to last page
                if (GetRequestParams())
                {
                    //check to see if the user is logged in and is allowed to edit the game
                    if (Request.IsAuthenticated)
                    {
                        if ((GameToEditsID < 0) || IsUserAllowedToEdit)
                        {
                            // Extend the timeout for this page
                            this.AddKeepAlive ();

                            // Tag validator
                            regtxtKeywords.ValidationExpression = Kaneva.Constants.VALIDATION_REGEX_TAG;
                            regtxtKeywords.ErrorMessage = Kaneva.Constants.VALIDATION_REGEX_TAG_ERROR_MESSAGE;

                            //configure page
                            ConfigurePageForUse ();

                            //set bread crumb
                            AddBreadCrumb (GetNewBreadCrumb ("Game Edit", GetCurrentURL (), "", 0));
                        }
                        else
                        {
                            Response.Redirect ("~/games/gameLibrary.aspx");
                        }
                    }
                    //redirect to login page if user is not logged in nor the owner
                    else
                    {
                        RedirectToLogin();
                    }
                }
                else
                {
                    NavigateBackToBreadCrumb(OriginalBreadCrumb);
                }
            }
        }

        #endregion

        #region Attributes

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        public string DEFAULT_SERVER_SORT
        {
            get
            {
                return "server_name";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        public string DEFAULT_PATCH_SORT
        {
            get
            {
                return "patch_url";
            }
        }

        /// <summary>
        /// stores the name of the breadcrumb from the previous page
        /// </summary>
        /// <returns>string</returns>
        private string OriginalBreadCrumb
        {
            get
            {
                if (ViewState["breadCrumb"] == null)
                {
                    return null;
                }
                return ViewState["breadCrumb"].ToString();
            }
            set
            {
                ViewState["breadCrumb"] = value;
            }
        }

        /// <summary>
        /// stores the game id of the game to be added/edited
        /// </summary>
        /// <returns>int</returns>
        private int GameToEditsID
        {
            get
            {
                if (ViewState["gameId"] == null)
                {
                    return -1;
                }
                return (int)ViewState["gameId"];
            }
            set
            {
                ViewState["gameId"] = value;
            }
        }

        /// <summary>
        /// stores the game id of the game to be added/edited
        /// </summary>
        /// <returns>int</returns>
        private DataTable GamesCategories
        {
            get
            {
                if (ViewState["gamesCategories"] == null)
                {
                    return null;
                }
                return (DataTable)ViewState["gamesCategories"];
            }
            set
            {
                ViewState["gamesCategories"] = value;
            }
        }

        /// <summary>
        /// stores the game data to reduce database round trips
        /// </summary>
        /// <returns>Game</returns>
        private Game GameToEdit
        {
            get
            {
                if (ViewState["game"] == null)
                {
                    return null;
                }
                return (Game)ViewState["game"];
            }
            set
            {
                ViewState["game"] = value;
            }
        }

 /*       /// <summary>
        /// stores the patchURL data to reduce database round trips
        /// </summary>
        /// <returns>GamePatchURLs</returns>
        private IList<GameServer> GameServers
        {
            get
            {
                if (ViewState["gameServers"] == null)
                {
                    ViewState["gameServers"] = new List<GameServer>();
                }
                return (IList<GameServer>)ViewState["gameServers"];
            }
            set
            {
                ViewState["gameServers"] = value;
            }
        }*/

        /// <summary>
        /// stores the patchURL data to reduce database round trips
        /// </summary>
        /// <returns>GamePatchURLs</returns>
        private DataTable GameServers
        {
            get
            {
                if (ViewState["gameServers"] == null)
                {
                    ViewState["gameServers"] = new DataTable();
                }
                return (DataTable)ViewState["gameServers"];
            }
            set
            {
                ViewState["gameServers"] = value;
            }
        }

        /// <summary>
        /// stores the patchURL data to reduce database round trips
        /// </summary>
        /// <returns>GamePatchURLs</returns>
        private DataTable GamePatchURLs
        {
            get
            {
                if (ViewState["patchURLs"] == null)
                {
                    ViewState["patchURLs"] = new DataTable();
                }
                return (DataTable)ViewState["patchURLs"];
            }
            set
            {
                ViewState["patchURLs"] = value;
            }
        }

        /// <summary>
        /// stores the gamelicense data to reduce database round trips
        /// </summary>
        /// <returns>GameLicense</returns>
        private GameLicense GameLicense
        {
            get
            {
                if (ViewState["gameLicense"] == null)
                {
                    return null;
                }
                return (GameLicense)ViewState["gameLicense"];
            }
            set
            {
                ViewState["gameLicense"] = value;
            }
        }

        /// <summary>
        /// stores the current page of the server gridview
        /// </summary>
        /// <returns>int</returns>
        private int CurrentServerPage
        {
            get
            {
                if(ViewState["serverPage"] == null)
                {
                    ViewState["serverPage"] = 0;
                }
                return (int)ViewState["serverPage"];
            }
            set
            {
                ViewState["serverPage"] = value;
            }
        }

        /// <summary>
        /// stores the current page of the patchURL gridview
        /// </summary>
        /// <returns>int</returns>
        private int CurrentPatchURLPage
        {
            get
            {
                if(ViewState["patchURLPage"] == null)
                {
                    ViewState["patchURLPage"] = 0;
                }
                return (int)ViewState["patchURLPage"];
            }
            set
            {
                ViewState["patchURLPage"] = value;
            }
        }

        /// <summary>
        /// returns true if current user is allowed to edit this channel
        /// </summary>
        /// <returns>bool</returns>
        private bool IsUserAllowedToEdit
        {
            get
            {
                if(ViewState["validEditor"] == null)
                {
                    ViewState["validEditor"] = IsUserAllowedToEditGame(GameToEditsID);
                }
                return (bool)ViewState["validEditor"];
            }
        }

        #endregion

        #region Helper Functions

        //disables key controls for read only mode
        private void ReadOnly()
        {
            pnl_Security.Enabled = false;
        }

        private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
                GameToEditsID = Convert.ToInt32(Request["gameId"]);
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

        private void PatchURLPrep()
        {
            ClearPatchURLDataFields();
            hdn_patchURLID.Text = "-1";
            tbl_PatchData.Visible = true;
            btnUpdatePatchURL.Text = "Add";
        }

        private void PatchURLReset()
        {
            ClearPatchURLDataFields();
            tbl_PatchData.Visible = false;
            btnUpdatePatchURL.Text = "Add";
        }

        private void ServerFieldsPrep()
        {
            ClearServerDataFields();
            hdn_ServerID.Text = "-1";
            tbl_editServer.Visible = true;
            btnUpdateServer.Text = "Add";
        }

        private void ServerReset()
        {
            ClearServerDataFields();
            tbl_editServer.Visible = false;
            btnUpdateServer.Text = "Add";
        }

        /// <summary>
        /// Populates License Type Pull down
        /// </summary>
        private void PopulateLicenseTypeDropDown()
        {
            ddlLicenseTypes.DataSource = WebCache.GetGameLicenseTypes();
            ddlLicenseTypes.DataTextField = "Name";
            ddlLicenseTypes.DataValueField = "LicenseSubscriptionId";
            ddlLicenseTypes.DataBind();
        }

        /// <summary>
        /// Populates Game Access Options Pull down
        /// </summary>
        private void PopulateGameAccessDropDown()
        {
            drpPermission.DataSource = WebCache.GetGameAccess();
            drpPermission.DataTextField = "game_access";
            drpPermission.DataValueField = "game_access_id";
            drpPermission.DataBind();
        }

        /// <summary>
        /// Populates Game Status Options Pull down
        /// </summary>
        private void PopulateGameStatusDropDown()
        {
            drpGameStatus.DataSource = WebCache.GetGameStatus();
            drpGameStatus.DataTextField = "game_status";
            drpGameStatus.DataValueField = "game_status_id";
            drpGameStatus.DataBind();
        }

        /// <summary>
        /// Populates Server visibility options Pull down
        /// </summary>
        private void PopulateGameServerVisibilityDropDown()
        {
            drpServerVisibility.DataSource = WebCache.GetGameServerVisbility();
            drpServerVisibility.DataTextField = "name";
            drpServerVisibility.DataValueField = "visibility_id";
            drpServerVisibility.DataBind();
        }


        /// <summary>
        /// Populates Restrcition Levels Pull down
        /// </summary>
        private void PopulateRestrictionLevelDropDown()
        {
            drpMaturityLevel.DataSource = WebCache.GetGameRatings();
            drpMaturityLevel.DataTextField = "game_rating";
            drpMaturityLevel.DataValueField = "game_rating_id";
            drpMaturityLevel.DataBind();
        }

        protected void NavigateToPreviousPage()
       {
           NavigateBackToBreadCrumb(OriginalBreadCrumb);
       }

        /// <summary>
        /// Get a subscription for a given length
        /// </summary>
        /// <param name="dtCommunitySubscription"></param>
        /// <param name="lengthOfSubscription"></param>
        /// <returns></returns>
        private DataRow GetSubscription(DataTable dtAssetSubscription, int lengthOfSubscription)
        {
            DataRow[] drResult = dtAssetSubscription.Select("length_of_subscription = " + lengthOfSubscription);
            if (drResult.Length > 0)
            {
                return drResult[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Return the delete javascript
        /// </summary>
        /// <param name="TopicID"></param>
        /// <returns></returns>
        public string GetChannelRemoveScript(int communityId, int isPersonal)
        {
            if (isPersonal.Equals(1))
            {
                return "javascript: alert(\"You cannot remove this item from a profile.\")";
            }
            return "javascript:if (confirm(\"Are you sure you want to remove this item from the community?\")){RemoveChannel (" + communityId + ")};";
        }

        /// <summary>
        /// Return the delete javascript
        /// </summary>
        /// <param name="TopicID"></param>
        /// <returns></returns>
        public string GetServerDeleteScript(int gameLicenseId)
        {
            return "javascript:if (confirm(\"Are you sure you want to delete this server?\")){DeleteServer (" + gameLicenseId + ")};";
        }

         //clear the patchURL fields
        private void ClearPatchURLDataFields()
        {
            txtPatchURL.Text = "";
            patchURLRequired.Visible = false;
            hdn_PatchIndex.Text = "";
            hdn_patchURLID.Text = "";
        }

        //clear the server data fields
        private void ClearServerDataFields()
        {
            txtServerName.Text = "";
            hdn_ServerID.Text = "";
            hdn_ServerListIndex.Text = "";
            drpServerVisibility.SelectedIndex = 0;
            serverNameRequired.Visible = false;
            tbx_PortNumber.Text = Convert.ToString(DeveloperCommonFunctions.DEFAULT_PORT);
            portNumberRequired.Visible = false;
        }

        /// <summary>
        /// Is the category checked?
        /// </summary>
        /// <param name="catId"></param>
        /// <returns></returns>
        protected bool GetCatChecked(int catId)
        {
            bool isChecked = false;
            //searh stored categories
            if ((GamesCategories != null) && (GamesCategories.Rows.Count > 0))
            {
                isChecked = ((GamesCategories.Select("category_id = " + catId).Length) > 0 ? true : false);
            }
            return isChecked;
        }


        public void UploadGameImage()
        {
            UploadGameImage(GetUserId(),GameToEditsID, browseTHUMB);
        }

        #endregion

        #region Primary Functions

        /// <summary>
        /// gets the most recent data for this game being edited from database and populates 
        /// the appropriate fields, lists, and views
        /// </summary>
        private void BindData()
        {
            //get the data for the game being edited
            BindGameData();

            //get the current servers associate with the game
            BindGameServers();

            //get the current license associated with the game
            BindPatchURLs();
        }

        private void BindGameData()
        {
            //get the game to be edited from the database
            GameToEdit = GetGameFacade().GetGameByGameId(GameToEditsID);

            //get the related license for the game
            try
            {
                GameLicense = GetGameFacade().GetGameLicenseByGameId(GameToEditsID);
            }
            catch (Exception ex)
            {
                lblMissingMessage.ForeColor = Color.DarkRed;
                lblMissingMessage.Text = ex.Message;
                m_logger.Error("Error on GameLicense Load for editing", ex);
            }

            //get the games servers for the game
            try
            {
                string orderby = DEFAULT_SERVER_SORT + " " + base.DEFAULT_SORT_ORDER;
                GameServers = GetGameFacade().GetServerByGameId(GameToEditsID, "", orderby);
            }
            catch (Exception ex)
            {
                lblMissingMessage.ForeColor = Color.DarkRed;
                lblMissingMessage.Text = ex.Message;
                m_logger.Error("Error on GameServer Load for editing", ex);
            }
            //get the patchURLs for the game
            try
            {
                string orderby = DEFAULT_PATCH_SORT + " " + base.DEFAULT_SORT_ORDER;
                GamePatchURLs = GetGameFacade().GetPatchURLsByGameId(GameToEditsID, "", orderby);
            }
            catch (Exception ex)
            {
                lblMissingMessage.ForeColor = Color.DarkRed;
                lblMissingMessage.Text = ex.Message;
                m_logger.Error("Error on Patch URL Load for editing", ex);
            }

            //if game could not be loaded stop processing and alert the user
            if (GameToEdit != null)
            {
                try
                {
                    string gameName = Server.HtmlDecode(GameToEdit.GameName);
                    drpPermission.SelectedValue = GameToEdit.GameAccessId.ToString();
                    drpGameStatus.SelectedValue = GameToEdit.GameStatusId.ToString();
                    drpMaturityLevel.SelectedValue = GameToEdit.GameRatingId.ToString();
                    txtKeywords.Text = GameToEdit.GameKeyWords;

                    if (gameName.IndexOf('-') >= 0)
                    {
                        gameName = gameName.Substring(gameName.IndexOf('-') + 1);
                    }
                    txtGameName.Text = gameName;
                    txtTeaser.Text = Server.HtmlDecode(GameToEdit.GameSynopsis);
                    txtBody.Text = Server.HtmlDecode(GameToEdit.GameDescription);
                    chkGameStar.Checked = GameToEdit.ParentGameId != 0;
                    chkGameStar.Enabled = false; // can't change it after creation

                    // attempt to load and display the thumbnail
                    if ((GameToEdit.GameImagePath != null) && (GameToEdit.GameImagePath != ""))
                    {
                        tblThumbPreview.Visible = true;
                        //generate the full path from config and data base values
                        img_gameIcon.Src = GetGameImageURL(GameToEdit.GameImagePath, "me");
                        imgPreview.Src = GetGameImageURL(GameToEdit.GameImagePath, "sm");
                    }
                    else
                    {
                        tblThumbPreview.Visible = false;
                    }

                    GamesCategories = new DataTable();
                    lblCreatedBy.Text = (GetDevelopmentCompanyFacade().GetDevelopmentCompany(GameToEdit.OwnerId)).CompanyName;
                    ddlLicenseTypes.SelectedValue = GameLicense.LicenseSubscriptionId.ToString();
                }
                catch (Exception ex)
                {
                    lblMissingMessage.ForeColor = Color.DarkRed;
                    lblMissingMessage.Text = "Error while loading game data";
                    m_logger.Error("Error gameEdit: while loading game data", ex);
                }
            }
            else
            {
                lblMissingMessage.ForeColor = Color.DarkRed;
                lblMissingMessage.Text = "No Game Information found";

            }
       }

        private void BindGameServers()
        {
            //dg_GameServers.Sort
            dg_GameServers.PageIndex = CurrentServerPage;
            dg_GameServers.DataSource = GameServers;
            dg_GameServers.DataBind();
        }

        private void BindPatchURLs()
        {
            dg_PatchURLS.PageIndex = CurrentPatchURLPage;
            dg_PatchURLS.DataSource = GamePatchURLs;
            dg_PatchURLS.DataBind();
        }

        /// <summary>
        /// Configures the page for use 
        /// </summary>
        private void ConfigurePageForUse()
        {
            // Enable Max Lengths of TextAreas
            //txtTeaser.Attributes.Add("MaxLength", "1000");
            //txtPerAdditional.Attributes.Add("MaxLength", "255");
            //txtCommAdditional.Attributes.Add("MaxLength", "255");

            //if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "taMaxLength"))
            //{
            //    string strScript = "<script language=JavaScript> addEvent(window, \"load\", textAreasInit);</script>";
            //    ClientScript.RegisterClientScriptBlock(GetType(), "taMaxLength", strScript);
            //}

            //// Tag validator
            //regtxtKeywords.ValidationExpression = Constants.VALIDATION_REGEX_TAG;
            //regtxtKeywords.ErrorMessage = Constants.VALIDATION_REGEX_TAG_ERROR_MESSAGE;
            
            //populate the page's drop downs
            PopulateLicenseTypeDropDown();
            PopulateGameAccessDropDown();
            PopulateGameStatusDropDown();
            PopulateGameServerVisibilityDropDown();
            PopulateRestrictionLevelDropDown();

            //if gameId is greater than 0 then this is an edit request so get the data. 
            //else this is a new game creation do nothing.
            if (GameToEditsID > 0)
            {
                BindData();
            }
            else
            {
                DevelopmentCompany devCompany = this.GetDevelopmentCompanyFacade().GetCompanyByDeveloperId(this.GetUserId());
                lblCreatedBy.Text = devCompany.CompanyName;

            }
            //populate the available game categories
            //must happen after the bind for editing to capture the checked fields
            dlCategories.DataSource = WebCache.GetGameCategories();
            dlCategories.DataBind();

        }

        private void PopulateGameServerEditFields(GridViewRow gvr)
        {
            try
            {   
                drpServerVisibility.SelectedValue = gvr.Cells[2].Text.Trim();
                txtServerName.Text = Server.HtmlDecode(gvr.Cells[4].Text.Trim());
                tbx_PortNumber.Text = Server.HtmlDecode(gvr.Cells[9].Text.Trim());
            }
            catch(Exception)
            {
                lblMissingMessage.ForeColor = System.Drawing.Color.DarkRed;
                lblMissingMessage.Text = "Error populating Server fields for editing.";
                //MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('lblMissingMessage', 5000);");
            }
        }

        private void PopulatePatchURLEditFields(GridViewRow gvr)
        {
            try
            {
                hdn_patchURLID.Text = gvr.Cells[0].Text.Trim();
                txtPatchURL.Text = Server.HtmlDecode(gvr.Cells[3].Text.Trim());
            }
            catch (Exception)
            {
                lblMissingMessage.ForeColor = System.Drawing.Color.DarkRed;
                lblMissingMessage.Text = "Error populating patchURL fields for editing.";
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// chkAdvanced_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkAdvanced_Click(Object sender, EventArgs e)
        {
            if (chkAdvanced.Checked)
            {
                txtBody.ConfigurationPath = ResolveUrl("~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/assetAdvanced.config");
            }
            else
            {
                txtBody.ConfigurationPath = ResolveUrl("~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/asset.config");
            }

            txtBody.Focus = true;
        }

        /// <summary>
        /// Update Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            bool errorOccured = false;
            bool gameImageChanged = true;

            try
            {
                // Check for any inject scripts in large text fields
                if ((DeveloperCommonFunctions.ContainsInjectScripts(txtBody.Text)) || (DeveloperCommonFunctions.ContainsInjectScripts(txtTeaser.Text)))
                {
                    m_logger.Warn("User " + DeveloperCommonFunctions.GetUserId() + " tried to script from IP " + Common.GetVisitorIPAddress());
                    ShowErrorOnStartup("Your input contains invalid scripting, please remove script code and try again.", true);
                    return;
                }

                // Check for any inject scripts in smaller text fields
                if ((DeveloperCommonFunctions.ContainsInjectScripts(txtGameName.Text)) || (DeveloperCommonFunctions.ContainsInjectScripts(this.txtKeywords.Text)))
                {
                    m_logger.Warn("User " + DeveloperCommonFunctions.GetUserId() + " tried to script from IP " + Common.GetVisitorIPAddress());
                    ShowErrorOnStartup("Your input contains invalid scripting, please remove script code and try again.", true);
                    return;
                }

                if (txtGameName.Text.IndexOfAny(new char[] { '-', '\\', '/', ':', '*', '<', '>', '?', '|', '"' }) != -1)
                {
                    ShowErrorOnStartup("The name cannot contain any of the following characters: -<>:\"/\\|?*", true);
                    return;
                }

                // Server validation
                Page.Validate();
                if (!Page.IsValid)
                {
                    return;
                }

                string gameName;
                gameName = txtGameName.Text;

                //save Game
                if (GameToEditsID < 1)
                {
                    Game game = new Game();

                    if (chkGameStar.Checked)
                    {
                        game.ParentGameId = KanevaGlobals.WokGameId;
                    }
                    game.GameId = GameToEditsID;
                    game.GameAccessId = Convert.ToInt32(drpPermission.SelectedValue);
                    game.GameCreationDate = DateTime.Now;
                    game.GameDescription = txtBody.Text;
                    game.GameImagePath = ((browseTHUMB != null) ? browseTHUMB.Value : null);
                    game.GameKeyWords = txtKeywords.Text;
                    game.GameModifiersId = GetUserId();
                    game.GameName = gameName;
                    game.GameRatingId = Convert.ToInt32(drpMaturityLevel.SelectedValue);
                    game.GameStatusId = Convert.ToInt32(drpGameStatus.SelectedValue);
                    game.GameSynopsis = txtTeaser.Text;
                    game.LastUpdated = DateTime.Now;
                    game.OwnerId = GetCurrentUser().CompanyId;
                    game.GameEncryptionKey = GenerateUniqueString(16);

                    GameToEditsID = GetGameFacade().SaveGame(game);

                    if (GameToEditsID < 1)
                    {
                        throw new Exception("Unable to add game");
                    }
                }
                else
                {
                    GameToEdit.GameId = GameToEditsID;
                    GameToEdit.GameAccessId = Convert.ToInt32(drpPermission.SelectedValue);
                    GameToEdit.GameDescription = txtBody.Text;
                    if ((browseTHUMB != null) && (browseTHUMB.Value != "") && (GameToEdit.GameImagePath != browseTHUMB.Value))
                    {
                        GameToEdit.GameImagePath = browseTHUMB.Value;
                    }
                    else
                    {
                        gameImageChanged = true;
                    }
                    GameToEdit.GameKeyWords = txtKeywords.Text;
                    GameToEdit.GameModifiersId = GetUserId();
                    GameToEdit.GameName = gameName;
                    GameToEdit.GameRatingId = Convert.ToInt32(drpMaturityLevel.SelectedValue);
                    GameToEdit.GameStatusId = Convert.ToInt32(drpGameStatus.SelectedValue);
                    GameToEdit.GameSynopsis = txtTeaser.Text;
                    GameToEdit.LastUpdated = DateTime.Now;

                    int rowsAffected = GetGameFacade().SaveGame(GameToEdit);

                    if (rowsAffected < 1)
                    {
                        throw new Exception("Unable to update game");
                    }
                }

                try
                {
                    //save thumbnail
                    if (gameImageChanged && (browseTHUMB.Value != ""))
                    {
                        UploadGameImage(GetUserId(), GameToEditsID, browseTHUMB);
                    }
                }
                catch (Exception)
                {
                    //do Not stop if Image fails
                }

                //save Categories
                //first clear them all out
                //GetGameFacade().DeleteGameCategories(GameToEditsID);
                //then add them all back in
                //foreach (DataListItem dliCategory in dlCategories.Items)
                //{
                //    CheckBox chkCategory = (CheckBox)dliCategory.FindControl("chkCategory");

                //    if (chkCategory.Checked)
                //    {
                //        HtmlInputHidden hidCatId = (HtmlInputHidden)dliCategory.FindControl("hidCatId");
                //        GetGameFacade().AddGameCategory(GameToEditsID, Convert.ToInt32(hidCatId.Value));
                //    }
                //}

                //save licenseType
                if (GameLicense == null)
                {
                    GameLicense license = new GameLicense();

                    //temporarily hard coded to be professional for closed beta
                    //license.LicenseSubscriptionId = Convert.ToInt32(ddlLicenseTypes.SelectedValue);
                    license.LicenseSubscriptionId = 3;
                    license.GameId = GameToEditsID;
                    license.GameKey = GetUserId () + "U" + GameToEditsID + "G" + GenerateUniqueString (15);
                    license.ModifierId = GetUserId();
                    license.LicenseStatusId = DeveloperCommonFunctions.DEFAULT_LICENSE_STATUS_ID;

                    int licenseId = GetGameFacade().AddNewGameLicense(license, 1000);

                    if (licenseId < 1)
                    {
                        throw new Exception("Unable to add license");
                    }
                }
                else
                {
                    //temporarily hard coded to be professional for closed beta
                    //license.LicenseSubscriptionId = Convert.ToInt32(ddlLicenseTypes.SelectedValue);
                    GameLicense.LicenseSubscriptionId = 3;
                    GameLicense.ModifierId = GetUserId();

                    int rowsAffected = GetGameFacade().UpdateGameLicense(GameLicense);

                    if (rowsAffected < 1)
                    {
                        throw new Exception("Unable to update license");
                    }
                }


                //save servers
                DataTable serverChanges = GameServers.GetChanges();
                if (serverChanges != null)
                {
                    //enumeration shouldn't cause over head issues due to low concurrent users
                    foreach (DataRow dr in serverChanges.Rows)
                    {
                        //decide which database operation to do
                        switch (dr.RowState.ToString().ToUpper())
                        {
                            case "ADDED":
                                try
                                {
                                    GameServer server = new GameServer();
                                    server.ServerId = Convert.ToInt32(dr["server_id"]);
                                    server.GameId = GameToEditsID;
                                    server.VisibiltyId = Convert.ToInt32(dr["visibility_id"]);
                                    server.GameServerModifiersId = Convert.ToInt32(dr["modifiers_id"]);
                                    server.ServerName = dr["server_name"].ToString();
                                    server.ServerStartedDate = Convert.ToDateTime(dr["server_started_date"]);
                                    server.IPAddress = dr["ip_address"].ToString();
                                    server.Port = Convert.ToInt32(dr["port"]);
                                    server.NumberOfPlayers = Convert.ToInt32(dr["number_of_players"]);
                                    server.MaxPlayers = Convert.ToInt32(dr["max_players"]);
                                    server.LastPingDate = Convert.ToDateTime(dr["last_ping_datetime"]);

                                    GetGameFacade().AddNewGameServer(server);
                                }
                                catch (Exception ex)
                                {
                                    m_logger.Error("Error on game editing - Update", ex);
                                    throw new Exception("Unable to add server");
                                }
                                break;
                            case "DELETED":
                                try
                                {
                                    dr.RejectChanges(); //this is used to un delete the row so that we can retreive the id
                                    GetGameFacade().DeleteGameServer(Convert.ToInt32(dr["server_id"]));
                                }
                                catch (Exception ex)
                                {
                                    m_logger.Error("Error on game editing - Update", ex);
                                    throw new Exception("Unable to delete server");
                                }
                                break;
                            case "MODIFIED":
                                try
                                {
                                    GameServer server = new GameServer();
                                    server.ServerId = Convert.ToInt32(dr["server_id"]);
                                    server.GameId = GameToEditsID;
                                    server.VisibiltyId = Convert.ToInt32(dr["visibility_id"]);
                                    server.GameServerModifiersId = Convert.ToInt32(dr["modifiers_id"]);
                                    server.ServerName = dr["server_name"].ToString();
                                    server.ServerStartedDate = Convert.ToDateTime(dr["server_started_date"]);
                                    server.IPAddress = dr["ip_address"].ToString();
                                    server.Port = Convert.ToInt32(dr["port"]);
                                    server.NumberOfPlayers = Convert.ToInt32(dr["number_of_players"]);
                                    server.MaxPlayers = Convert.ToInt32(dr["max_players"]);
                                    server.LastPingDate = Convert.ToDateTime(dr["last_ping_datetime"]);

                                    GetGameFacade().UpdateGameServer(server);
                                }
                                catch (Exception ex)
                                {
                                    m_logger.Error("Error on game editing - Update", ex);
                                    throw new Exception("Unable to update server");
                                }
                                break;
                        }
                    }
                }

                //save patchURls
                DataTable patchChanges = GamePatchURLs.GetChanges();
                if (patchChanges != null)
                {
                    //enumeration shouldn't cause over head issues due to low concurrent users
                    foreach (DataRow dr in patchChanges.Rows)
                    {
                        //decide which database operation to do
                        switch (dr.RowState.ToString().ToUpper())
                        {
                            case "ADDED":
                                try
                                {
                                    GetGameFacade().AddNewPatchURLs(GameToEditsID, dr["patch_url"].ToString());
                                }
                                catch (Exception ex)
                                {
                                    m_logger.Error("Error on game editing - Update", ex);
                                    throw new Exception("Unable to add server");
                                }
                                break;
                            case "DELETED":
                                try
                                {
                                    dr.RejectChanges(); //this is used to un delete the row so that we can retreive the id
                                    GetGameFacade().DeletePatchURLs(GameToEditsID, Convert.ToInt32(dr["patchURL_id"]));
                                }
                                catch (Exception ex)
                                {
                                    m_logger.Error("Error on game editing - Update", ex);
                                    throw new Exception("Unable to delete server");
                                }
                                break;
                            case "MODIFIED":
                                try
                                {
                                    GetGameFacade().UpdatePatchURLs(GameToEditsID, Convert.ToInt32(dr["patchURL_id"]), dr["patch_url"].ToString());
                                }
                                catch (Exception ex)
                                {
                                    m_logger.Error("Error on game editing - Update", ex);
                                    throw new Exception("Unable to update server");
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                errorOccured = true;
                m_logger.Error("Error saving game ", exc);
                lblMissingMessage.ForeColor = System.Drawing.Color.DarkRed;
                lblMissingMessage.Text = "Error saving game " + exc.Message;
            }

            if (!errorOccured)
            {
                NavigateToPreviousPage();
            }
        }

        /// <summary>
        /// Cancel Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            NavigateBackToBreadCrumb(OriginalBreadCrumb);
        }

        //---event handlers for the Game server Gridview------------------
        protected void dg_GameServers_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
        }

        protected void dg_GameServers_OnRowCreated(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            //logic check to work around initial row creation wheere there is only one column
            if (e.Row.Cells.Count > 10)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[7].Visible = false;
            }
        }

        /// <summary>
        /// They clicked to edit the pathc URL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_GameServers_RowEditing(object source, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            //passin the selected row and populate the fields
            ServerFieldsPrep();
            btnUpdateServer.Text = "Update";
            hdn_ServerListIndex.Text = e.NewEditIndex.ToString();
            PopulateGameServerEditFields(dg_GameServers.Rows[e.NewEditIndex]);
        }

        /// <summary>
        /// They clicked to change pages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_GameServers_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            CurrentServerPage = e.NewPageIndex;
            //bind data
            BindGameServers();
        }

        /// <summary>
        /// They clicked to sort a column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_GameServers_Sorting(object sender, GridViewSortEventArgs e)
        {
            base.gridview_Sorting(sender, e);
            //bind data
            BindGameServers();
        }

        /// <summary>
        /// lbn_NewServer_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbn_NewServer_Click(Object sender, EventArgs e)
        {
            ServerFieldsPrep();
        }

        /// <summary>
        /// btnServerCancel_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnServerCancel_Click(Object sender, EventArgs e)
        {
            ServerReset();
        }

        protected void dg_GameServers_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GameServers.Rows[e.RowIndex].Delete();
            BindGameServers();
        }

        /// <summary>
        /// btnUpdateServer_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdateServer_Click(Object sender, EventArgs e)
        {
            //set error message variable
            string error = "";

            //validate the required fields
            //validate the required fields
            if (txtServerName.Text == "")
            {
                error += "Server Name is required. ";
                serverNameRequired.Visible = true;
            }
            if (tbx_PortNumber.Text == "")
            {
                error += "Port Number is required. ";
                portNumberRequired.Visible = true;
            }
            if (error.Length > 0)
            {
                this.lblMissingMessage.ForeColor = Color.DarkRed;
                lblMissingMessage.Text = error;
                lblMissingMessage.Visible = true;
                return;
            }

            try
            {
                if (btnUpdateServer.Text.ToUpper().Equals("ADD"))
                {
                    error = "Error During Add: ";

                    if (GameServers.Columns.Count <= 0)
                    {
                        GameServers.Columns.Add("server_id");
                        GameServers.Columns.Add("game_id");
                        GameServers.Columns.Add("visibility_id");
                        GameServers.Columns.Add("server_status_id");
                        GameServers.Columns.Add("server_type_id");
                        GameServers.Columns.Add("modifiers_id");
                        GameServers.Columns.Add("server_name");
                        GameServers.Columns.Add("server_started_date");
                        GameServers.Columns.Add("ip_address");
                        GameServers.Columns.Add("port");
                        GameServers.Columns.Add("number_of_players");
                        GameServers.Columns.Add("max_players");
                        GameServers.Columns.Add("last_ping_datetime");
                    }

                    DataRow newServer = GameServers.NewRow();
                    //set default values
                    newServer["server_id"] = Convert.ToInt32(hdn_ServerID.Text);
                    newServer["game_id"] = GameToEditsID;
                    newServer["visibility_id"] = drpServerVisibility.SelectedValue;
                    newServer["server_status_id"] = 0; //stopped
                    newServer["server_type_id"] = 1; //game
                    newServer["modifiers_id"] = GetUserId();
                    newServer["server_name"] = txtServerName.Text;
                    newServer["server_started_date"] = DateTime.Now;
                    newServer["ip_address"] = 0;
                    newServer["port"] = tbx_PortNumber.Text; //default provided by wok team
                    newServer["number_of_players"] = 0;
                    newServer["max_players"] = 0;
                    newServer["last_ping_datetime"] = DateTime.Now;

                    GameServers.Rows.Add(newServer);
                }
                else  // edit server
                {
                    error = "Error During Edit: ";

                    int index = Convert.ToInt32 (hdn_ServerListIndex.Text);
                    DataRow server = GameServers.Rows[index];

                    server["visibility_id"] = drpServerVisibility.SelectedValue;
                    //    server["server_status_id"] = drpServerStatus.SelectedValue;
                    //    server["server_type_id"] = drpServerType.SelectedValue;
                    server["modifiers_id"] = GetUserId ();
                    server["server_name"] = txtServerName.Text;
                    server["port"] = tbx_PortNumber.Text;
                }

                //clear the fields
                ServerReset();

                //bindthe data
                BindGameServers();
            }
            catch (Exception ex)
            {
                lblMissingMessage.ForeColor = Color.DarkRed;
                lblMissingMessage.Text = error + ex.Message;
                m_logger.Error("Error on Game Server Management ", ex);
            }
        }

        //---event handlers for the Patch URL Gridview------------------
        protected void dg_PatchURLS_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GamePatchURLs.Rows[e.RowIndex].Delete();
            BindPatchURLs();
        }

        /// <summary>
        /// btnUpdatePatchURL_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdatePatchURL_Click(Object sender, EventArgs e)
        {
            //set error message variable
            string error = "";

            //validate the required fields
            //validate the required fields
            if (txtPatchURL.Text == "")
            {
                error += "Patch URL is required. ";
                patchURLRequired.Visible = true;
            }
            if (error.Length > 0)
            {
                this.lblMissingMessage.ForeColor = Color.DarkRed;
                lblMissingMessage.Text = error;
                lblMissingMessage.Visible = true;
                return;
            }

            try
            {
                if (btnUpdatePatchURL.Text.ToUpper().Equals("ADD"))
                {
                    error = "Error During Add: ";

                    if (GamePatchURLs.Columns.Count <= 0)
                    {
                        //dt = new DataTable();
                        GamePatchURLs.Columns.Add("patchURL_id");
                        GamePatchURLs.Columns.Add("patch_id");
                        GamePatchURLs.Columns.Add("game_id");
                        GamePatchURLs.Columns.Add("patch_url");
                    }

                    DataRow newPatch = GamePatchURLs.NewRow();
                    //set default values
                    newPatch["patchURL_id"] = Convert.ToInt32(hdn_patchURLID.Text);
                    newPatch["patch_id"] = 1;
                    newPatch["game_id"] = GameToEditsID;
                    newPatch["patch_url"] = txtPatchURL.Text;

                    GamePatchURLs.Rows.Add(newPatch);
                }
                else
                {
                    error = "Error During Edit: ";

                    int index = Convert.ToInt32(hdn_PatchIndex.Text);
                    DataRow patchRow = GamePatchURLs.Rows[index];

                    patchRow["patch_url"] = txtPatchURL.Text;
                }


                //clear the fields
                PatchURLReset();

                //bind the data
                BindPatchURLs();
            }
            catch (Exception ex)
            {
                lblMissingMessage.ForeColor = Color.DarkRed;
                lblMissingMessage.Text = error + ex.Message;
                m_logger.Error("Error on patchURL Management", ex);
            }
        }

        /// <summary>
        /// lbn_NewPatchURL_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbn_NewPatchURL_Click(Object sender, EventArgs e)
        {
            PatchURLPrep();
        }

        /// <summary>
        /// btnPatchURLCancel_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPatchURLCancel_Click(Object sender, EventArgs e)
        {
            PatchURLReset();
        }

        protected void dg_PatchURLS_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
        }
          
        protected void dg_PatchURLS_OnRowCreated(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            //logic check to work around initial row creation wheere there is only one column
            if (e.Row.Cells.Count > 4)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
            }
        }

        /// <summary>
        /// They clicked to edit the pathc URL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_PatchURLS_RowEditing(object source, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            PatchURLPrep();
            btnUpdatePatchURL.Text = "Update";
            //passin the selected row and populate the fields
            hdn_PatchIndex.Text = e.NewEditIndex.ToString();
            PopulatePatchURLEditFields(dg_PatchURLS.Rows[e.NewEditIndex]);
        }

        /// <summary>
        /// They clicked to change pages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_PatchURLS_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            CurrentPatchURLPage = e.NewPageIndex;

            //bind data
            BindPatchURLs();
        }

        /// <summary>
        /// They clicked to sort a column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_PatchURLS_Sorting(object sender, GridViewSortEventArgs e)
        {
            base.gridview_Sorting(sender, e);
            //bind data
            BindPatchURLs();
        }

        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
 
  
	}
}
