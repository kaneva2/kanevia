<%@ Page Language="C#"  MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="false" CodeBehind="gameResources.aspx.cs" Inherits="KlausEnt.KEP.Developer.gameResources" %>
<asp:Content ID="cnt_gameResource" runat="server" ContentPlaceHolderID="cph_Body">

<script type="text/javascript" language="javascript">
<!--
    function SiteInNewWindow(url, comdarg)
    {
        window.open(url,"STARS","width=750,height=650,location=yes,scrollbars=yes,status=yes,resizable=yes,directories=yes,menubar=yes,toolbar=yes");
        
        var buttonId =  '<%= btn_DataCollect2.ClientID %>';
        var fieldId =  '<%= hdn_docchoice.ClientID %>';
        $(fieldId).value = comdarg;
        $(buttonId).click();
    }
 
     function SiteInSameWindow(url, comdarg)
    {
       
        var buttonId =  '<%= btn_DataCollect2.ClientID %>';
        var fieldId =  '<%= hdn_docchoice.ClientID %>';
        $(fieldId).value = comdarg;
        $(buttonId).click();
        
        window.location = url;
    }
       
    function LicenseAgreement()
    {
        alert("By downloading this source you hereby agree to abide by the Kaneva Community Source license.");
    }
//-->
</script>
<div id="contentWide">					
    <div id="main">
        <div id="main_content">
        <h1 class="resource png">Resources </h1>
        <asp:Button id="btn_DataCollect2" runat="server" style="display:none" onclick="btnDocDownload_Click" />
        <input type="hidden" runat="server" id="hdn_docchoice" />
<div>
	<div class="clear"><!-- Clear the floats --></div>
    <div class="docStyle" >
        <h3>Documentation</h3>
        <ul>
            <li><a href="#" onclick="SiteInNewWindow('<%= GetStarsDocumentation() %>', 0)">Instructions</a></li>
        </ul>
    </div>
    <div class="downStyle">
        <h3>Downloads</h3>
            <h4>Platform Install</h4>
             <ul title="Platform">
                <li><a href="#" onclick="SiteInNewWindow('<%= GetStarsPlatformLink() %>', 1)">Download Platform</a></li>
            </ul>
            <h4>Templates</h4>
            <ul title="Templates">
                <li><a href="#" onclick="SiteInNewWindow('<%= GetCardBoardWorldLink() %>', 3)">Download Cardboard World Template</a></li>
            </ul>
            <h4>Source</h4>
            <ul title="Source">
                <li><a href="#" onclick="LicenseAgreement();SiteInNewWindow('<%= GetSourceCodeLink() %>', 2)">Download Source </a></li>
            </ul>
    </div>
    <div class="docStyle" >
        <h3>License Agreements</h3>
        <ul>
            <li><a href="#" onclick="SiteInNewWindow('<%= GetLicenseInfo() %>', 0)" >View License Agreement</a></li>
        </ul>
    </div>
    <div class="docStyle" >
        <h3>Third Party Software</h3>
        <ul>
            <li><a href="#" onclick="SiteInNewWindow('<%= GetThirdPartySoftware() %>', 0)" >View Software</a></li>
        </ul>
    </div>
    <div class="docStyle" >
        <h3>Community</h3>
        <ul>
            <li><a href="#" onclick="SiteInNewWindow('<%= GetStarsForum() %>', 5)">STAR Forums</a></li>
        </ul>
    </div>
</div>
</div>
</div>
</div>
</asp:Content>
