///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Developer
{
    public partial class gameResources : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //set Main Navigation
                Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV] = usercontrols.Navigation.TAB.RESOURCES;

                if (!IsPostBack)
                {
                    //ResetBreadCrumb ();
                    AddBreadCrumb(GetNewBreadCrumb("Resources", GetCurrentURL(), "", 0));
                }
            }
            else
            {
                RedirectToLogin();
            }
        }

        #region Link Functions
        protected string GetStarsPlatformLink()
        {
            return DeveloperCommonFunctions.GetPlatformLink;
        }

        protected string GetSourceCodeLink()
        {
            return DeveloperCommonFunctions.GetSourceCodeLink;
        }

        protected string GetCardBoardWorldLink()
        {
            return DeveloperCommonFunctions.GetCardboardLink;
        }

        protected string GetStarsDocumentation()
        {
            return DeveloperCommonFunctions.GetDocumentationLink;
        }

        protected string GetStarsForum()
        {
            return DeveloperCommonFunctions.GetForumLink;
        }

        protected string GetLicenseInfo()
        {
            return DeveloperCommonFunctions.GetLicenseInfo;
        }

        protected string GetThirdPartySoftware()
        {
            return DeveloperCommonFunctions.GetThirdPartySoftware;
        }

        #endregion

        #region Events

        public void btnDocDownload_Click(object sender, System.EventArgs e)
        {
            try
            {
                //if you don't want to track the click enter 0 in the aspx javascript function
                string itemDownLoaded = "";

                switch (hdn_docchoice.Value)
                {
                    case "1":
                        itemDownLoaded = GetStarsPlatformLink();
                        break;
                    case "2":
                        itemDownLoaded = GetSourceCodeLink();
                        break;
                    case "3":
                        itemDownLoaded = GetCardBoardWorldLink();
                        break;
                    //case "4":
                    //    itemDownLoaded = GetStarsDocumentation();
                    //    break;
                    //case "5":
                    //    itemDownLoaded = GetCommunitySourceLink();
                    //    break;
                    //case "6":
                    //    itemDownLoaded = GetStarsForum();
                    //    break;
                    //case "7":
                    //    itemDownLoaded = GetLicenseInfo();
                    //    break;
                    //case "8":
                    //    itemDownLoaded = GetThirdPartySoftware();
                    //    break;
                }

                if (itemDownLoaded != "")
                {
                    GetGameDeveloperFacade().TrackDownLoads(GetCurrentUser().UserInfo, itemDownLoaded);
                }

            }
            catch (Exception) { }
        }

        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

    }
}
