<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="false" CodeBehind="gameLibrary.aspx.cs" Inherits="KlausEnt.KEP.Developer.gameLibrary" %>
<%@ Register TagPrefix="Kaneva" TagName="StoreFilter" Src="../usercontrols/StoreFilter.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>


<asp:Content ID="cnt_gameLibrary" runat="server" ContentPlaceHolderID="cph_Body">

<style>
	img { behavior: url(../css/iepngfix.htc); }
</style>

<!--<link href="../css/new.css" rel="stylesheet" type="text/css">
<LINK href="../css/kanevaSC.css" type="text/css" rel="stylesheet">-->

<script src="../jscript/xEvent.js" type="text/javascript"></script>
<script src="../jscript/balloon.js" type="text/javascript"></script>
<script src="../jscript/yahoo-dom-event.js"></script> 
 
<script type="text/JavaScript">

function changeBG(obj) {
	obj.style.background = "url('../images/media/tabsMedia.gif')";
}
function changeBG2(objeto) {
	objeto.style.background = "url('../images/media/tabsMedia_on.gif')";
}
function changeBGB(objeto) {
	objeto.style.background = "url('../images/media/tabsMedia2.gif')";
}
function changeBGB2(objeto) {
	objeto.style.background = "url('../images/media/tabsMedia2_on.gif')";
}

function CheckAll() 
{
	Select_All(document.getElementById("cbxSelectAll").checked);
}

// white balloon with mostly default configuration
// (see http://www.wormbase.org/wiki/index.php/Balloon_Tooltips)
var whiteBalloon    = new Balloon;
whiteBalloon.balloonTextSize  = '100%';

// white ballon with some custom config:
var whiteBalloonSans  = new Balloon;
whiteBalloonSans.upLeftConnector    = '../images/balloons/balloonbottom_sans.png';
whiteBalloonSans.upRightConnector   = '../images/balloons/balloonbottom_sans.png';
whiteBalloonSans.downLeftConnector  = '../images/balloons/balloontop_sans.png';
whiteBalloonSans.downRightConnector = '../images/balloons/balloontop_sans.png';
whiteBalloonSans.upBalloon          = '../images/balloons/balloon_up_top.png';
whiteBalloonSans.downBalloon        = '../images/balloons/balloon_down_bottom.png';
whiteBalloonSans.paddingConnector = '22px';
   
</script>

<div id="contentWide">					
<div id="main">
<div id="main_content">

<div id="divLoading">
	<table height="50" cellSpacing="0" cellPadding="0" width="100%" border="0" id="Table1">
		<tr>
			<th class="loadingText" id="divLoadingText">
				Loading...</th></tr>
	</table>
</div>
					
<div id="divData" style="DISPLAY: block">
	<ajax:ajaxpanel id="ajGameLibrary" runat="server">
		<!-- Main Body structure table -->
											<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR> 
													<!-- BEGIN GAME DISPLAY AREA -->
													<TD vAlign="top" width="100%"><br />
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD>
																		<!--
																		<table cellSpacing="0" cellPadding="0" width="98%" border="0">
																			<tr style="height:42px">
																				<td align="left" valign="top">
															                        <SPAN id="spanGsmeLibrary2" runat="server">
																                        <H1>My STARS Management</H1>
															                        </SPAN>																									
															                    </td>
																			    <TD class="searchnav" noWrap align="right">
																				    <asp:label id="lblSearch2" runat="server" cssclass="insideTextNoBold" /><BR>
																				    <Kaneva:Pager id="pgTop" MaxPagesToDisplay="5" runat="server" IsAjaxMode="True" />
																			    </td> 
																			</tr>
																		</table>-->
																		<TABLE id="controlbox" cellSpacing="0" cellPadding="0" width="99%" border="0"> 
																			<!-- Begin Game management controls -->
																			<TR>
																				<!-- add new games (button) -->
																				<TD align="center">
																					<TABLE id="TABLE4" cellSpacing="0" cellPadding="0" width="100%" border="0">
																						<TR> 
																							<TD align="left">																													
																								<asp:LinkButton class="button" id="lnkAddGame" onClick="lnkAddGame_Click" title="Add a new STARS." runat="server" CausesValidation="False" Text="Add STAR">Add STAR
																									<img src="../images/btnico_add.gif" title="Add a new STAR." width="24" height="24" border="0">
																								</asp:LinkButton>
																							</td> 
																						</tr>
																					</table> 
																				</td>
																				<!-- delete games (button) -->
																				<TD align="center">
																					<TABLE id="TABLE2" cellSpacing="0" cellPadding="0" width="100%" border="0 ">
																						<TR> 
																							<TD align="left">																													
																								<asp:LinkButton class="button" id="lnkDeleteGame" title="Delete selected STARS." runat="server" CausesValidation="False" Text="Delete STAR">Delete STAR
																									<img src="../images/btnico_delete.gif" title="Delete selected STARS." width="24" height="24" border="0">
																								</asp:LinkButton>
																							</td> 
																						</tr>
																					</table> 
																				</td>
																				<!-- show filter -->
																				<TD align="center">
																					<TABLE id="TABLE5" cellSpacing="0" cellPadding="0" width="100%" border="0">
																						<TR> 
																							<TD align="left">																													
																							<Kaneva:StoreFilter id="pageSort" runat="server" AssetType="0" HideAlphaNumericsPullDown="false" HideAlphaNumerics="true"
																								HideItemsPerPagePullDown="true" HideThumbView="True" IsAjaxMode="True"></Kaneva:StoreFilter>
																							</td> 
																						</tr>
																					</table> 
																				</td>
																				<!-- show per page -->
																				<TD align="center">
																					<TABLE id="TABLE6" cellSpacing="0" cellPadding="0" width="100%" border="0">
																						<TR> 
																							<TD align="left">																													
																							    <Kaneva:StoreFilter id="filStore" runat="server" AssetType="0" HideAlphaNumerics="true" HideThumbView="True" IsAjaxMode="True">
																							    </Kaneva:StoreFilter>
																							</td> 
																						</tr>
																					</table> 
																				</td>
																			</tr>
																		</table>
																		<!-- END Game management controls -->
																		<!-- Begin Media display region -->
																		<table width="99%" cellspacing="0" cellpadding="0" border="0" class="data">
																			<tr>
																				<td colspan="6" height="20px">
																					    <asp:label style="position:relative; font-size:18px; padding: 10px 0 10px 0" runat="server" id="messages" visible="true" />
																				</td>
																			</tr>
																			<tr>
																				<TD width="15" align="left" bgcolor="#fdfdfc">
																					<input type=checkbox id="cbxSelectAll" onclick="CheckAll()" class="Filter2" />
																				</td>
																				<td runat="server" id="tdOrderHeading" align="left" bgcolor="#13252f" width="20px">Name</td>
																				<td align="center" bgcolor="#13252f" width="50">Access</td>
																				<td align="left" bgcolor="#13252f">Details</td>
																				<td align="left" bgcolor="#13252f" width="15"></td>
																			</tr>
																			<asp:Repeater id="rptGames" runat="server">
																				<ItemTemplate>
																					<tr>
																						<td align="center" width="15">
																							<span style="horizontal-align: right;">
																								<asp:checkbox id="chkEdit" CssClass="Filter2" runat="server" Visible="true"/>
																								<input type="hidden" runat="server" id="hidGameId" value='<%#DataBinder.Eval(Container.DataItem, "game_id")%>' game_name="hidGameId">
																							</span>
																						</td>
																						<td>
																							<!--image holder-->
																							<table border="0" cellpadding="0" cellspacing="0" width="100%" class="nopadding">
																								<tr>
																									<td align="right" width="36">
																										<div class="framesize-tiny">
																											<div class="frame">
																												<div class="restricted" runat="server" visible='<%# IsGameMature(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "game_rating_id"))).Equals(1) %>' ID="Div2"></div>
																												<span class="ct">
																													<span class="cl"></span>
																												</span>
																												<div class="imgconstrain">
																													<a runat="server" id="thumbnail" href='<%# GetGameDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "game_id")))%>' >
																														<img src='<%#GetGameImageURL (DataBinder.Eval(Container.DataItem, "game_image_path").ToString () ,"sm")%>' border="0"/>
																													</a>
																												</div>
																												<span class="cb">
																													<span class="cl"></span>
																												</span>
																											</div>
																										</div>
																									</td>
																									<td align="left">
																										<span class="insideTextNoBold"></span>&nbsp;<a runat="server" id="gameTitle" style="font-size:110%; text-decoration:none" href='<%# GetGameDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "game_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_name").ToString ()), 30) %></a>
																									</td>
																								</tr>
																							</table>
																							<asp:Label ID="lblMessage" runat="server" />
																							<!--end image holder-->
																						</td>
																						<td align="center" width="50">
																							<span style="horizontal-align: center;">
																							    <asp:label id="lbl_gameAccess" runat="server" alt='<%#GetGameAccessDescription(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "game_access_id")))%>' text='<%#GetGameAccessDescription(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "game_access_id")))%>' visible="true" />
																							</span>
																						</td>
																						<td valign="middle" align="left">
																							<span>Status:&nbsp;<A runat="server" HREF='#' id="hlStatus" />&nbsp;<A runat="server" HREF='#' id="hlAddData" /></span><span class="insideTextNoBold">
																								<br />Added on: <%# FormatDateTime(DataBinder.Eval(Container.DataItem, "game_creation_date"))%></span>
																								<br />
																						</td>
																						<td align="center">
																							<asp:LinkButton id="lnkEdit" Text="Edit" ToolTip="Edit" CommandName="cmdEdit" runat="server" style="text-decoration:underline;" />
																						</td>
																					</tr>
																				</ItemTemplate>
																				<AlternatingItemTemplate>
																					<tr class="altrow">
																						<td align="center" width="15">
																							<span style="horizontal-align: right;">
																								<asp:checkbox id="chkEdit" CssClass="Filter2" runat="server" Visible="true"/>
																								<input type="hidden" runat="server" id="hidGameId" value='<%#DataBinder.Eval(Container.DataItem, "game_id")%>' game_name="hidGameId">
																							</span>
																						</td>
																						<td>
																							<!--image holder-->
																							<table border="0" cellpadding="0" cellspacing="0" width="100%" class="nopadding">
																								<tr>
																									<td align="right" width="36">
																										<div class="framesize-tiny">
																											<div class="frame">
																												<div class="restricted" runat="server" visible='<%# IsGameMature(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "game_rating_id"))).Equals(1) %>' ID="Div1"></div>
																												<span class="ct">
																													<span class="cl"></span>
																												</span>
																												<div class="imgconstrain">
																													<a runat="server" id="thumbnail" href='<%# GetGameDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "game_id")))%>'>
																														<img src='<%#GetGameImageURL (DataBinder.Eval(Container.DataItem, "game_image_path").ToString () ,"sm")%>' border="0"/>
																													</a>
																												</div>
																												<span class="cb">
																													<span class="cl"></span>
																												</span>
																											</div>
																										</div>
																									</td>
																									<td align="left">
																										<span class="insideTextNoBold"></span>&nbsp;<a runat="server" id="gameTitle" style="font-size:110%; text-decoration:none" href='<%# GetGameDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "game_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_name").ToString ()), 30) %></a>
																									</td>
																								</tr>
																							</table>
																							<asp:Label ID="lblMessage" runat="server" />
																							<!--end image holder-->
																						</td>
																						<td align="center" width="50">
																							<span style="horizontal-align: center;">
																							    <asp:label id="lbl_gameAccess" runat="server"  alt='<%#GetGameAccessDescription(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "game_access_id")))%>' text='<%#GetGameAccessDescription(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "game_access_id")))%>' visible="true" />
																							</span>
																						</td>
																						<td valign="middle" align="left">
																							<span>Status:&nbsp;<A runat="server" HREF='#' id="hlStatus" />&nbsp;<A runat="server" HREF='#' id="hlAddData" /></span><span class="insideTextNoBold">
																								<br />Added on: <%# FormatDateTime(DataBinder.Eval(Container.DataItem, "game_creation_date"))%></span>
																								<br />
																						</td>
																						<td align="center">
																							<asp:LinkButton id="lnkEdit" Text="Edit" ToolTip="Edit" CommandName="cmdEdit" runat="server" style="text-decoration:underline;" />
																						</td>
																					</tr>
																				</AlternatingItemTemplate>
																			</asp:Repeater>
																																																																			
																		</table>
																	</div> 
																<!--END RIGHTCOL-->
																</td>
															</tr>
														</table>
													</td> 
													<!-- END MEDIA DISPLAY AREA -->
												</tr>
											</table>
											<table cellSpacing="0" cellPadding="0" width="98%" border="0">
											    <tr style="height:42px">
				    								<td align="left" valign="top">
								                        <span id="spanGsmeLibrary" runat="server">
									                        <h1 class="management png">My Star Management</h1>
								                        </span>																									
								                    </td>
											        <td class="searchnav" noWrap align="right" >
				                                        <asp:label id="lblSearch" runat="server" cssclass="insideTextNoBold"></asp:label><br />
				                                        <Kaneva:Pager id="pgBottom" MaxPagesToDisplay="5" runat="server" IsAjaxMode="True"></Kaneva:Pager>
											        </td>
											    </tr>
											</table>

		<!-- END MAIN BODY -->
		<INPUT id="GameId" type="hidden" value="0" runat="server" name="GameId">
		<asp:button id="DeleteGame" onclick="Delete_Game" runat="server" Visible="false"></asp:button>

	</ajax:ajaxpanel>
</div>
</div>
</div>
</div>
</asp:Content>
