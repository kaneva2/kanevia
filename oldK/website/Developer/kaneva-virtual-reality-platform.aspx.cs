///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Developer
{
    public partial class kaneva_virtual_reality_platform : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //set Main Navigation
            Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV] = usercontrols.Navigation.TAB.PLATFORM;
            //set 2ndary nav
            Session[DeveloperCommonFunctions.ACTIVE_SUB_NAV] = usercontrols.SubNavPlatform.TAB.ELEMENTS;

            if (!Page.IsPostBack)
            {
                //set the metadata
                ((MainTemplatePage)Master).Title = "The Elements | The Kaneva Star Platform";
                ((MainTemplatePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"kaneva virtual reality platform, virtual reality program, game developers platform \">";
                ((MainTemplatePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"With the Kaneva Star Platform, you're not just getting a place to call your own, you're getting everything you need to make it successful. The Kaneva Star Platform delivers an array of critical services designed to help market, manage and support your 3D world. \">";
            }
        }
    }
}
