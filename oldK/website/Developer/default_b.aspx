﻿<%@ Page Language="C#" MasterPageFile="~/masterpages/AppBrochureTemplatePage.Master" AutoEventWireup="true" CodeBehind="default_b.aspx.cs" Inherits="KlausEnt.KEP.Developer.default_b" %>

<asp:Content ID="cntProfileHead" runat="server" contentplaceholderid="cphHeadData" >
  	<meta name="title" content="Kaneva. Make your own 3D Games." /> 
 	<meta name="description" content="Kaneva - Create your avatar and make your own 3D Games!"/> 
 	<meta name="keywords" content="computer game making, video game creator, game designing programs, design a videogame online, making a computer game, make own videogame, 3d game development, programming a game, 3d engine game, online game developer, 3d game design"/>  
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>
    <link href="css/Brochure/Default_b.css" type="text/css" rel="stylesheet" />  
    <script type="text/javascript" src="jscript/prototype.js"></script>
    <script type="text/javascript" src="jscript/Brochure/Default.js"></script>
    <script type="text/jscript">
    <!--
    	PageReLoad();
    //-->
     </script>
<!-- Google Website Optimizer Tracking Script -->
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['gwo._setAccount', 'UA-5755114-4']);
	_gaq.push(['gwo._trackPageview', '/4177057029/test']);
	(function () {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
<!-- End of Google Website Optimizer Tracking Script -->
</asp:Content>

<asp:Content id="cntProfielLeft" runat="server" contentplaceholderid="cphBody"> 
    <div id="sideBar" style="float:right;">
		<br />
		<a href="info/Kaneva-3D-Apps-Contest.aspx"><img src="images/Brochure/Events/monthly_contest_banner_220x494.jpg" /></a>
    </div>
    <div id="overview">
        <div class="header">
            <span>3D App Game Developer Program</span><br /><br />
            <h5>Make your own 3D Game with Kaneva’s Free 3D Virtual World Engine</h5>
        </div>    
        
		<div id="emailContainer" style="margin-left:10px;">
			<div id="email">
				<div>Email Address</div> 
				<asp:textbox id="txtEmail" clientidmode="Static" runat="server" maxlength="100"></asp:textbox>
				<div class="err"><asp:regularexpressionvalidator id="revEmail" runat="server" controltovalidate="txtEmail" text="Please enter a valid email address." 
					errormessage="Please enter a valid email address" enableclientscript="True"></asp:regularexpressionvalidator></div>
			</div>
			<div id="dialogbig">
				<a runat="server" onclick="if($('txtEmail').value.length>0){_gaq.push(['_trackPageview','/EmailEntered_Old'])};callGAEvent('Download','DownloadBtn_Old','DownloadBtn_ClickOld');" onserverclick="aDownload_Click" id="aDownload"><span>FREE Download >></span></a>			
			</div>
 		</div>						  
		
        <div id="overviewInfoArea">
            <div class="overviewInfoBlock">
                <a href="learn/develop.aspx" >
                    <img id="developImage" alt="Develop" src="images/Brochure/dev_overview_develop_202x162.jpg" />
                </a>    
                <div id="developBlurb" class="blurb">
                    <p><span>1. Develop</span></p>
                    Start developing your 3D App Games right away. Access the resource center and discover videos, sample scripts, technical information, and much more.
                </div>
                <a href="learn/develop.aspx">Learn More >></a>    
             </div>       
            <div class="overviewInfoBlock" >
                <a href="learn/playtest.aspx">
                    <img id="Img1" alt="Develop" src="images/Brochure/dev_overview_playtest_202x162.jpg" />
                </a>    
                <div id="playtestBlurb" class="blurb">
                    <p><span>2. Playtest</span></p>
                    Let the Kaneva community become your official game testers.  They can provide real time feedback and assist with fine tuning your 3D App experience.  
                </div>
                <a href="learn/playtest.aspx">Learn More >></a>    
             </div>       
            <div class="overviewInfoBlock">
                <a href="learn/publish.aspx">
                    <img id="Img2" alt="Develop" src="images/Brochure/dev_overview_publish_202x162.jpg" />
                </a>    
                <div id="publishBlurb" class="blurb">
                    <p><span>3. Publish</span></p>
                    Leverage Kaneva’s social network by using powerful viral marketing features to build your community and enable revenue opportunities for your 3D Apps. 
                </div>
                <a href="learn/publish.aspx">Learn More >></a>    
             </div>                                
        </div>        		
		<div id="infoLinks" style="display:none;">
			<hr />
			<a href="info/Develop-3D-Social-Game.aspx">Develop-3D-Social-Game</a> | 
			<a href="info/Kaneva-3D-Apps-Launch-Contest.aspx">Kaneva-3D-Apps-Launch-Contest</a> |
			<a href="info/Kaneva-3D-Game-Engine.aspx">Kaneva-3D-Game-Engine</a>
		</div>   
    </div>
	
	<script type="text/javascript">
		$('txtEmail').focus();
	</script>     
</asp:Content>