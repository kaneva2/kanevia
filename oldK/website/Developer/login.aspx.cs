///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Web.Security;
using System.Security.Principal;
using log4net;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Developer
{
    public partial class login : BasePage
    {
        #region Declarations

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion


        protected login()
        {
        }

        #region Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            //set Main Navigation
            Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV] = usercontrols.Navigation.TAB.SIGN_IN;

            if (!IsPostBack)
            {
                //sets a custom CSS for this page
                ((MainTemplatePage)Master).CustomCSS = "<test>";

                // Check to see if cookies are enabled, If they don't have cookies enabled, 
                // they cannot log in, redirect to noCookies page. 
                //
                // NOTE : Response.cookies and Request.cookies share same collection, 
                // so read the actual headers to see if cookies are enabled.
                string sCookieHeader = Page.Request.Headers["Cookie"];
                if ((null == sCookieHeader) || (sCookieHeader.IndexOf("ASP.NET_SessionId").Equals(null)))
                {
                    Response.Redirect(ResolveUrl("~/noCookies.aspx"));
                }

                // Set the focus
                SetFocus(txtUserName);

                if (Request.UrlReferrer != null)
                {
                    ViewState["rurl"] = Request.UrlReferrer.AbsoluteUri.ToString();
                }

                aLostPassword.HRef = ResolveUrl(DeveloperCommonFunctions.GetLostPassword);
                //aRegister.HRef = ResolveUrl (DeveloperCommonFunctions.);
            }

            string strJavascript = "<script language=\"JavaScript\">" +
                "function checkEnter(event){\n" +
                "if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13))\n" +
                "{event.returnValue=false;event.cancel=true;" + ClientScript.GetPostBackEventReference(this.imgLogin, "", false) + ";return false;}\n else \n{return true;}" +
                "}</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "checkEnter"))
            {
                ClientScript.RegisterClientScriptBlock(GetType(), "checkEnter", strJavascript);
            }
        }

        #endregion

        #region Main Functions


        /// <summary>
        /// LoginUser
        /// </summary>
        private void LoginUser(string email, string password, bool bPersistLogin)
        {
            // set needed variables
            int roleMembership = 0;
            int validLogin = -1;
            string errorMessage = null;
            string url = "";
            GameDeveloper gameDev = null;

            // Valdiate the user against the Kaneva database.
            validLogin = AuthorizeUser(email, password, 0, ref roleMembership, Common.GetVisitorIPAddress(), true);

            //check to see if any error message needs to be displayed to the user based on validation status
            switch (validLogin)
            {
                case (int)Kaneva.Constants.eLOGIN_RESULTS.SUCCESS:
                    break;
                case (int)Kaneva.Constants.eLOGIN_RESULTS.NOT_VALIDATED:
                case (int)Kaneva.Constants.eLOGIN_RESULTS.USER_NOT_FOUND:
                    errorMessage = "Email address " + email + " was not found.";
                    SetFocus(txtUserName);
                    break;
                case (int)Kaneva.Constants.eLOGIN_RESULTS.INVALID_PASSWORD:
                    m_logger.Warn("Failed login (invalid password) for email '" + email + "' from IP " + Common.GetVisitorIPAddress());
                    errorMessage = "Invalid password.";
                    SetFocus(txtPassword);
                    break;
                case (int)Kaneva.Constants.eLOGIN_RESULTS.NO_GAME_ACCESS:
                    errorMessage = "No access to this game.";
                    break;
                case (int)Kaneva.Constants.eLOGIN_RESULTS.ACCOUNT_DELETED:
                    errorMessage = "This account has been deleted.";
                    SetFocus(txtUserName);
                    break;
                case (int)Kaneva.Constants.eLOGIN_RESULTS.ACCOUNT_LOCKED:
                    m_logger.Warn("Locked account " + email + " tried to sign in from IP " + Common.GetVisitorIPAddress());
                    errorMessage = "This account has been locked by the Kaneva administrator";
                    SetFocus(txtUserName);
                    break;
                default:
                    errorMessage = "Not authenticated.";
                    break;
            }

            //if no error message found proceed with loggin in the use to kaneva and checking
            //developer status. Otherwise show the error messsage
            if (errorMessage == null)
            {

                try
                {
                    //we want the process to continue even if the user isnt found
                    //the facade throws the error
                    try
                    {
                        //pull users developer information 
                        gameDev = this.GetGameDeveloperFacade().GetGameDeveloperByEmail(email);
                    }
                    catch
                    { }

                    //if user is found to have a developer account redirect them to the landing page
                    //else prompt them to become a developer
                    if ((gameDev != null) && (gameDev.CompanyId > 0))
                    {


                        //temporary logic check to prevent auto registration from working without Kaneva intervention
                        DevelopmentCompany dc = GetDevelopmentCompanyFacade().GetDevelopmentCompany(gameDev.CompanyId);
                        if (!dc.Approved)
                        {
                            errorMessage = "You are not an authorized STAR developer. Please register to be a STAR Developer.";
                            throw new Exception(); ;
                        }

                        //this code temporary and only for beta testing purposes
                        if (!dc.AuthorizedTester)
                        {
                            errorMessage = "You are not an authorized Tester.";
                            throw new Exception(); ;
                        }
                        //temporary logic check to prevent auto registration from working without Kaneva intervention



                        //log the user in, up date authenication cookie and update their login record
                        FormsAuthentication.SetAuthCookie(gameDev.UserInfo.UserId.ToString(), bPersistLogin);
                        UpdateLastLogin(gameDev.UserInfo.UserId, Common.GetVisitorIPAddress(), Server.MachineName);

                        // Set the userId in the session for keeping track of current users online
                        Session["userId"] = gameDev.UserInfo.UserId;

                        
                        //set the bread crumb for browsing history
                        Kaneva.BreadCrumb breadCrumb = GetLastBreadCrumb();
                        if (breadCrumb != null)
                        {
                            url = ResolveUrl(breadCrumb.Hyperlink);
                        }
                        else if (Request["logretURL"] != null)
                        {
                            url = Server.UrlDecode(Request.QueryString.ToString()).Replace("logretURL=", "");
                        }

                        //redirect them to the landing page for signed in STAR developers
                        Response.Redirect(GetLandingPageLink());
                    }
                    else
                    {
                        //pull the user information and store the user id in the session
                        try
                        {
                            Session["userId"] = (new UserFacade()).GetUserByEmail(email).UserId;
                        }
                        catch { }

                        //sends user to the registration page as a user with no company and no developer account
                        Response.Redirect(ResolveUrl("~/be-kaneva-star-builder.aspx?vastat=" + GameDeveloper.NEWCOMPANY_NEWDEVELOPER.ToString()));
                    }

                }
                catch (Exception ex)
                {
                    m_logger.Warn("Error During Login: after validation complete " + ex.Message);
                    if (errorMessage == null)
                    {
                        errorMessage = "Sorry we are unable to log you in at this time. Please try back later.";
                    }
                    DisplayErrors(errorMessage);
                }
            }
            else
            {
                DisplayErrors(errorMessage);
            }


        }

        private void DisplayErrors(string errorMessage)
        {
            // Show an alert that the log in failed
            string strScript = "<script language=JavaScript>";
            strScript += "alert(\"Login Failed: " + errorMessage + "\");";
            strScript += "</script>";

            if (!ClientScript.IsStartupScriptRegistered(GetType(), "invalidLogin"))
            {
                ClientScript.RegisterStartupScript(GetType(), "invalidLogin", strScript);
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// The login click event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgLogin_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            //gather login values
            string email = Server.HtmlEncode(txtUserName.Value);
            string password = Server.HtmlEncode(txtPassword.Value);
            bool bPersistLogin = chkRememberLogin.Checked;

            //Process user login
            LoginUser(email, password, bPersistLogin);
        }

        /// <summary>
        /// The register click event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgRegister_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            //sends user to the registration page as a user with no company and no developer account
            Response.Redirect(ResolveUrl("~/be-kaneva-star-builder.aspx?vastat=" + GameDeveloper.NEWCOMP_NEWUSER.ToString()));
        }

        #endregion


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
