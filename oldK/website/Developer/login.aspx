<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="false" CodeBehind="login.aspx.cs" Inherits="KlausEnt.KEP.Developer.login" %>



<asp:Content ID="cnt_logIn" runat="server" ContentPlaceHolderID="cph_Body">
<script type="text/javascript">
<!--
WebForm_AutoFocus('_ctl0_cph_Body_txtUserName');// -->
</script>	
    <div id="content">
		<div id="main">
			<div id="main_content">				

				<div id="signinBox">
					<div id="signinContent">
						<h1 class="welcome png">Welcome Back</h1>
						
						<div id="signupForm">
						
							<fieldset>
								<legend><h2>Kaneva Members Sign In</h2></legend>
								
								<div class="required"><label for="email">Email:</label> <input type="text" style="width: 220px;" class="biginput" maxlength="100" id="txtUserName" runat="server" name="txtUserName" /></div>
								<div class="required"><label for="password">Password:</label><input type="password" style="width: 220px;" class="biginput" maxlength="30" id="txtPassword" runat="server" onkeydown="javascript:checkEnter(event)" name="txtPassword"></div>
								<div><label for="password">&nbsp;</label><span title="Remember Password"><asp:checkbox runat="server" id="chkRememberLogin" tooltip="Remember Password"/></span> Remember me on this computer</div>
							    <div style="float:right"><a runat="server" id="aLostPassword" target="_blank" style="vertical-align:top;" title="Hint: Use the email address and password associated with your Kaneva account." >Forgot password?</a></div>
							
								<p class="centered"><asp:imagebutton id="imgLogin" runat="server" causesvalidation="False" alternatetext="Sign in to Kaneva" imageurl="~/images/login/btn_signin.gif" onclick="imgLogin_Click" border="0" TabIndex="2"/></p>
							</fieldset>
						</div>
						
						
					</div>
				
				</div>
			</div>
			<div id="sidebar">
				<div id="sidebar_content" class="login_sidbar">
					<p>If you&#8217;re not a member of Kaneva, register a new account here:</p>
					<p class="centered"><asp:imagebutton id="imgRegister" runat="server" causesvalidation="False" alternatetext="Register with Kaneva" imageurl="~/images/btn_register.gif" onclick="imgRegister_Click" border="0" TabIndex="2"/></p>
				</div>
			</div>
		</div>
	</div>
								
</asp:Content>
