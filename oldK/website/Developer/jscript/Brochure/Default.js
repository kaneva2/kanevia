var selectedArea = '';

function HideBrochureAreas()
{
    /* Hide the two main div area*/
   $('overview').style.display = "none";
   $('steps').style.display = "none";
   
   /*Hide the main informational areas */
   $('step1').style.display = "none";
   $('step2').style.display = "none";
   $('step3').style.display = "none";
   
   /*Hide the associated summary areas*/
   $('step1Summary').style.display = "none";
   $('step2Summary').style.display = "none";
   $('step3Summary').style.display = "none";
   
   /*remove the selected style from the menu*/
   $("mioverview").setAttribute("class", "menuItemBox");
   $("mistep1").setAttribute("class", "menuItemBox");
   $("mistep2").setAttribute("class", "menuItemBox");
   $("mistep3").setAttribute("class", "menuItemBox");
}

function DisplaySelectedArea(stepArea)
{

   /*Reset all areas to invisible*/
   HideBrochureAreas();

   /*make the selected area visible*/ 
   $(stepArea).style.display = "block";

   /*set the selected style on the menu*/
   $("mi" + stepArea).setAttribute("class", "menuSelected");

   /*extra logic to handle turning all the step areas on*/
   if( stepArea != 'overview')
   {
       $('steps').style.display = "block";
       $(stepArea + "Summary").style.display = "block";
   }

    /*set the selected area for page refreshing purposes*/
    selectedArea = stepArea;
}

function PageReLoad()
{
    if(selectedArea != '')
    {
        DisplaySelectedArea(selectedArea);
    }
}
