/*
 From http://alexking.org/projects/wordpress/readme?project=share-this
 With only slight modifications for using 4 tabs.
*/

function akst_share(id, url, title) {
	var form = $('akst_form');
	var post_id = $('akst_post_id');
	
	if (form.style.display == 'block' && post_id.value == id) {
		form.style.display = 'none';
		return;
	}
	
	var link = $(id);
	var offset = Position.cumulativeOffset(link);

	$("akst_delicious").href = akst_share_url("http://del.icio.us/post?url={url}&title={title}", url, title);
	$("akst_digg").href = akst_share_url("http://digg.com/submit?phase=2&url={url}&title={title}", url, title);
	$("akst_furl").href = akst_share_url("http://furl.net/storeIt.jsp?u={url}&t={title}", url, title);
	$("akst_netscape").href = akst_share_url(" http://www.netscape.com/submit/?U={url}&T={title}", url, title);
	$("akst_yahoo_myweb").href = akst_share_url("http://myweb2.search.yahoo.com/myresults/bookmarklet?u={url}&t={title}", url, title);
	$("akst_stumbleupon").href = akst_share_url("http://www.stumbleupon.com/submit?url={url}&title={title}", url, title);
	$("akst_google_bmarks").href = akst_share_url("  http://www.google.com/bookmarks/mark?op=edit&bkmk={url}&title={title}", url, title);
	$("akst_technorati").href = akst_share_url("http://www.technorati.com/faves?add={url}", url, title);
	$("akst_blinklist").href = akst_share_url("http://blinklist.com/index.php?Action=Blink/addblink.php&Url={url}&Title={title}", url, title);
	$("akst_newsvine").href = akst_share_url("http://www.newsvine.com/_wine/save?u={url}&h={title}", url, title);
	$("akst_magnolia").href = akst_share_url("http://ma.gnolia.com/bookmarklet/add?url={url}&title={title}", url, title);
	$("akst_reddit").href = akst_share_url("http://reddit.com/submit?url={url}&title={title}", url, title);
	$("akst_windows_live").href = akst_share_url("https://favorites.live.com/quickadd.aspx?marklet=1&mkt=en-us&url={url}&title={title}&top=1", url, title);
	$("akst_tailrank").href = akst_share_url("http://tailrank.com/share/?link_href={url}&title={title}", url, title);

	post_id.value = id;

	form.style.left = offset[0] + 'px';
	form.style.top = (offset[1] + link.offsetHeight + 3) + 'px';
	form.style.display = 'block';
}

function akst_configure_tag(authenticated) 
{
    // get the needed elements
	var tab1 = document.getElementById("akst_tab1");
	var tab2 = document.getElementById("akst_tab2");
	var tab3 = document.getElementById("akst_tab3");
	var lit = document.getElementById("litNotLoggedShareStyle");

    if(!authenticated)
    {
       tab1.style.display = "none";
       tab2.style.display = "none";
       tab3.className = "selected";
       lit.Text = "<style>" +
                  "  #akst_email { display: block; } " +
                  "  #akst_blast { display: none; } " +
                  "  </style>";
    }
    else
    {
        tab1.className = "selected";
    }
}

function akst_share_url(base, url, title) {
	base = base.replace('{url}', url);
	return base.replace('{title}', title);
}

function akst_share_tab(tab) {
	var tab1 = document.getElementById('akst_tab1');
	var tab2 = document.getElementById('akst_tab2');
	var tab3 = document.getElementById('akst_tab3');
	var tab4 = document.getElementById('akst_tab4');
	var body1 = document.getElementById('akst_blast');
	var body2 = document.getElementById('akst_private');
	var body3 = document.getElementById('akst_email');
	var body4 = document.getElementById('akst_social');
	
	switch (tab) {
		case '1':
		    tab4.className = '';
		    tab3.className = '';
			tab2.className = '';
			tab1.className = 'selected';
			body4.style.display = 'none';
			body3.style.display = 'none';
			body2.style.display = 'none';
			body1.style.display = 'block';
			break;
		case '2':
			tab1.className = '';
			tab2.className = 'selected';
			tab3.className = '';
			tab4.className = '';
			body1.style.display = 'none';
			body2.style.display = 'block';
			body3.style.display = 'none';
			body4.style.display = 'none';
			break;
		case '3':
			tab1.className = '';
			tab2.className = '';
			tab3.className = 'selected';
			tab4.className = '';
			body1.style.display = 'none';
			body2.style.display = 'none';
			body3.style.display = 'block';
			body4.style.display = 'none';
			break;
         case '4':
			tab1.className = '';
			tab2.className = '';
			tab3.className = '';
			tab4.className = 'selected';
			body1.style.display = 'none';
			body2.style.display = 'none';
			body3.style.display = 'none';
			body4.style.display = 'block';
			break;			
	}
}

function akst_xy(id) {
	var element = $(id);
	var x = 0;
	var y = 0;
}