///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Web.UI.WebControls;

namespace KlausEnt.KEP.Developer
{
    public partial class Default : System.Web.UI.Page
    {
        #region Declarations
        string _basepath = "http://" + System.Configuration.ConfigurationManager.AppSettings["SiteName"] + "/";

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                Response.Redirect("default.aspx");
            }

            //sets a custom CSS for this page
            ((HomepageTest)Master).CustomCSS = "<link href=\"" + _basepath + "css/home/signup/homeTestSignupBase.css?v1\" rel=\"stylesheet\" type=\"text/css\" />\n" +
                                            "<style>#wrapper {background:#000000 url('images/home_signup/no_elephant/image_1920x1280_noElephant.jpg') no-repeat center top fixed; background-size:cover;}</style>\n" +
                                            "<!--[if lte IE 8]><style>.bottom_inner_wrapper { max-width:1920px; } .signupForm input { line-height:24px; }</style><![endif]-->\n";
            ((HomepageTest)Master).CustomJavaScript = "<script type=\"text/javascript\" src=\"http://code.jquery.com/jquery-latest.min.js\"></script>\n" +
                                                    "<script type=\"text/javascript\" src=\"" + _basepath + "jscript/selectBox/jquery.selectbox.min.js\"></script>\n" +
                                                    "<script type=\"text/javascript\">\n" +
                                                    "    jQuery(document).ready(function () {\n" +
                                                    "        jQuery('select').selectBox('create');\n" +
                                                    "        var neededHeight = 778;\n" +
                                                    "        var footerHeight = 234;\n" +
                                                    "        var height = jQuery(window).height();\n" +
                                                    "        var newMinHeight;\n" +
                                                    "        if (height < neededHeight && (neededHeight - height < footerHeight)) {\n" +
                                                    "            newMinHeight = neededHeight + (footerHeight - (neededHeight - height));\n" +
                                                    "            if (newMinHeight > neededHeight) {\n" +
                                                    "                jQuery('#wrapper').css('min-height', newMinHeight + 'px');\n" +
                                                    "            }\n" +
                                                    "        } else {\n" +
                                                    "            jQuery('#wrapper').css('min-height', neededHeight + 'px');\n" +
                                                    "        }\n" +
                                                    "    });\n" +
                                                    "</script>";

            
            this.seo.Attributes.Add("style", "display:block");
            ((HomepageTest)Master).Title = "Kaneva. Imagine What You Can Do.";
            ((HomepageTest)Master).MetaDataTitle = "<meta name=\"title\" content=\"Kaneva. A Unique Online Community and 3D Social Network.\" />";
            ((HomepageTest)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva\" />";
            ((HomepageTest)Master).MetaDataDescription = "<meta name=\"description\" content=\"Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!  \"/>";

            // Initialize dropdowns
            LoadCountries();
            LoadYears();
            
            // Set the postback url for the signup form
            String strTestDNS;
            strTestDNS = Request.Url.Host;
            strTestDNS = strTestDNS.ToLower();
            strTestDNS = strTestDNS.Replace("3d-developer.kaneva.com", "3dapps.kaneva.com");
            strTestDNS = strTestDNS.Replace("pv-developer.kaneva.com", "preview.kaneva.com");
            strTestDNS = strTestDNS.Replace("developer.kaneva.com", "www.kaneva.com");
            strTestDNS = strTestDNS.Replace("localhost", "localhost/kaneva");

            string signupUrl = Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["SSLLogin"]) ? "https://" : "http://";
            signupUrl += strTestDNS + "/register/kaneva/registerInfo.aspx";

            btnRegister.PostBackUrl = btnRegister.PostBackUrl = signupUrl;
            aFooterRegister.HRef = signupUrl;

            string signInUrl = (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["SSLLogin"]) ? "https://" : "http://") + strTestDNS + "/loginsecure.aspx";
            aSignIn.HRef = signInUrl;
        }

        #region Helper Functions
        /// <summary>
        /// Initialize the Country dropbox on the signup form
        /// </summary>
        private void LoadCountries()
        {
            drpCountry.DataTextField = "Name";
            drpCountry.DataValueField = "CountryId";
            drpCountry.DataSource = WebCache.GetCountries();
            drpCountry.DataBind();

            drpCountry.Items.Insert(0, new ListItem("United States", KlausEnt.KEP.Kaneva.Constants.COUNTRY_CODE_UNITED_STATES));

            // Script to enable/disable zip code field base on selected country
            drpCountry.Attributes.Add("onchange", "CountryCheck ();");

            string scriptString = @"
            <script language=JavaScript>
                function CountryCheck (){
                    if (document.getElementById('" + drpCountry.ClientID + @"').value == 'US')
                    {document.getElementById('" + divPostalCode.ClientID + @"').style.display = 'block';
                    } else { 
                        document.getElementById('" + divPostalCode.ClientID + @"').style.display = 'none';
                    }
                }
            </script>";

            if (!ClientScript.IsClientScriptBlockRegistered("CountryCheck"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "CountryCheck", scriptString);
            }
        }

        /// <summary>
        /// Initialize the years dropdown on the signup form
        /// </summary>
        private void LoadYears()
        {
            int startYear = Convert.ToInt16(DateTime.Now.Year) - 13;

            drpYear.Items.Add(new ListItem("> " + startYear.ToString(), (startYear + 1).ToString()));

            int currentYear = Convert.ToInt16(DateTime.Now.Year) - 13;

            for (int i = currentYear; i > 1940; i--)
            {
                drpYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            drpYear.Items.Add(new ListItem("< 1940", "1940"));
            drpYear.SelectedIndex = 0;
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
