///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KlausEnt.KEP.Developer
{
    public partial class fileNotFound : BasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            string requested = "Unknown";

            if (Request["aspxerrorpath"] != null)
            {
                requested = Request["aspxerrorpath"].ToString ();
            }

            lblURL.Text = requested;

        }
    }
}