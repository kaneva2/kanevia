﻿<%@ Page Language="C#" MasterPageFile="~/masterpages/AppBrochureTemplatePage.Master" AutoEventWireup="true" CodeBehind="fileNotFound.aspx.cs" Inherits="KlausEnt.KEP.Developer.fileNotFound" %>


<asp:Content ID="cntFileNotFoundPage" runat="server" ContentPlaceHolderID="cphBody">	
  
  <br /><br />
  <table border="0" cellpadding="10" cellspacing="0" width="560">
	<tr align="center">
		<td align="left" class="formError"><span class="formError"><h1>Page Not Found</h1></span>
		<br />
		The page you are looking for could have been 
		removed, had its name changed, or is temporarily unavailable. Please review the 
		URL and make sure that it is spelled correctly.
		 
		<br /><br /> <br />
		 
			<b>Requested URL is </b> &nbsp;&nbsp;&nbsp;'<asp:Label runat="server" id="lblURL"/>' 
		</td>
	</tr>
</table>

</asp:Content>