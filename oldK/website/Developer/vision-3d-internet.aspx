<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="true" CodeBehind="vision-3d-internet.aspx.cs" Inherits="KlausEnt.KEP.Developer.vision_3d_internet" %>

<asp:Content ID="cnt_vision" runat="server" ContentPlaceHolderID="cph_Body">

	<div id="content" class="homePage">
		<div id="homeFlash"></div>
		
		<div id="main">
			<div id="main_content">
				<h1 class="png">Evolution of Online Interaction</h1>
			  	<h2>Extending Web Sites to  Virtual Experiences</h2>
			  	<p><span class="highlight">In 1990 the World Wide Web was a few  pages on a single Web server.</span>  It was the brainchild of scientists, a technology that held a promise to break  down barriers to the pursuit of knowledge. </p>
			  	<p>It was a grand vision. Even so, <span class="highlight">could  anyone have imagined that it would so thoroughly permeate our culture</span>, becoming an indispensable companion to  business, education, entertainment, and communication? Science from the  frontiers of knowledge became a part of life as natural as waving to a friend.</p>
			  	
			  	<h2>Get  ready to watch it happen again.</h2>
			  	<p><span class="highlight">Imagine</span> a virtual  universe where every business, school, and medical facility has its  own 3D environment, as large as a world, or as small as a "Mom and Pop" store.</p>
			  	<p><span class="highlight">Imagine</span> visitors,  via their  avatars, moving freely between theme parks, showrooms,  entertainment venues, and quiet caf�s, fully engaged in an experience that goes   beyond the Web, bringing real people together to laugh or  learn, shop or play, virtually in person. </p>
			  	<p><span class="highlight">Now imagine</span> that  from the moment you hang up your shingle in this universe, you're part of a  bustling, vibrant community where business happens, and people from all across  the globe see eye to eye.</p>
			  	<p class="highlight">Kaneva will make this virtual universe a reality.</p>
			 </div>
		
		
			<div id="sidebar">
				<div id="sidebar_content" class="platform_sidbar">
					<h2>The Kaneva Star Platform</h2>
					<ul>
						<li class="first"><span></span>Interconnected virtual experiences with integrated social networking</li>
						<li class="second"><span></span>Media sharing, and support for user-generated content</li>
						<li class="third"><span></span>The next level of virtual interaction and gameplay</li>
					</ul>
				</div>
			</div>
		</div>
		
		<div id="nextPage" class="whybuild3d"><a href="build-3d-virtual-worlds.aspx">Next - Why Build in 3D?</a></div>
	</div>

</asp:Content>