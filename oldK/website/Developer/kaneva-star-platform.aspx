<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="true" CodeBehind="kaneva-star-platform.aspx.cs" Inherits="KlausEnt.KEP.Developer.kaneva_star_platform" %>

<asp:Content ID="cnt_starPlatform" runat="server" ContentPlaceHolderID="cph_Body">
	<div id="content">

		<div id="main">
			
			<div id="main_content">
			
			<h1 class="four png">Why Kaneva - The Kaneva Star Platform</h1>
			<h2>(We've Done the Hard Part for You)</h2>
			<p>What does it take to extend your web site to a 3D experience? In most cases, an extraordinary amount of time and resources. Or, you could purchase a plot of land in someone else's virtual world, and hope it doesn't end up in a red-light district.</p>
			<p class="highlight">With Kaneva, you get all the control, and all the tools and supporting services you need to build a rock-star virtual experience as quickly and cost-effectively as possible.</p>
			<p class="highlight">We provide you with</p>
				
				<ul class="checkList">
					<li class="png"><span class="highlight">The software you need to build your own Star</span></li>
					<li class="png"><span class="highlight">The server software you need</span> to host and maintain your Star</li>
					<li class="png"><span class="highlight">Centralized services</span> for creating accounts, e-commerce, promoting your Star, searching for people, media, places, and goods and gifts.</li>
					<li class="png">Integration with <span class="highlight">Kaneva's web-based social networking suite</span>, including personal profiles, messaging, event management, and much more.</li>
					<li class="png">A <span class="highlight">scalable and flexible platform</span>, so you'll be ready when your Star hits the evening news.</li>
				    <li class="png"><span class="highlight">Service and support</span>, including online forums, so you can communicate and collaborate with the entire Star-buildling community.</li>
				</ul>
				
			<p>Plus, we support industry-standards, so you can leverage the experience and skills of your creative talent, rather than taking the time and expense to retrain them.</p>
			<ul>
            	<li>3D Modeling: Autodesk 3ds Max&reg;</li>
              <li>Scripting: LUA and Python</li>
            </ul>
			<p>And because Kaneva is built on a MMORPG architechture, we deliver the next level of online interactivity. You'll be building more than just a place... you'll be creating an experience where people can do more than just get together. <span class="highlight">They can climb the mountain, capture the flag, watch the game, dance till dawn... they can do things together.</span></p>
			
			  </div>
			 <div id="sidebar">
			<div id="sidebar_content">
				<ul id="why_sidebar">
					<li class="experience png"><span class="highlight">3D Experience or World - you decide.</span> Whether you are looking to build a focused experience like a Haunted House, an intraverse, or an entire theme-based virtual world, the Star Platform has all you need.</li>
					<li class="control png"><span class="highlight">You have complete control.</span> With Kaneva, you maintain control,  ensuring a safe, non chaotic environment for efficient development and maintenance of your Star.</li>
					<li class="network png"><span class="highlight">Social network & media sharing.</span> Kaneva seamlessly integrates videos, photos, music, and games in a social networking environment that lets Star visitors connect on new levels.</li>
					<li class="interaction png"><span class="highlight">Next-level virtual interaction.</span> Easily incorporate games and quests into your Star, giving visitors opportunities for rich interaction.</li>
					<li class="economy png"><span class="highlight">Integrated economy.</span> The Kaneva platform includes a built-in economy that enables you to set up a local currency for your Star, and minimizes the risk of fraud and liability.</li>					
                    <li class="scalability png"><span class="highlight">Scalability.</span> Kaneva Stars are highly scalable and support an industry-first instancing model for simultaneous play of up to millions of people.</li>
					<li class="persistence png"><span class="highlight">Persistence.</span> A robust database backend allows you to provide a rich environment with storylines that stretch between sessions.</li>
					<li class="security png"><span class="highlight">Security and privacy. </span>Kaneva is the only platform where you can self-host your 3D experience, allowing a secure and private environment and complete ownership of data.</li>

			    </ul>
		  </div>
	  </div>
		
		</div>
		<div id="nextPage" class="theelements"><a href="kaneva-virtual-reality-platform.aspx">Next - The Elements</a></div>
	</div>

</asp:Content>