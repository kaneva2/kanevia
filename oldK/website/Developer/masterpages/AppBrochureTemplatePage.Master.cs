///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Developer
{
    public partial class AppBrochureTemplatePage : System.Web.UI.MasterPage
    {
        #region Declarations

        private string m_Title = "Kaneva - Create Your 3D Game. Make Fun Games for Free.  Win Prizes.";

        #endregion

        #region Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            // populates title bar
            if (m_Title.Length > 0)
            {
                litTitle.Text = m_Title;
            }
        }
        #endregion

        #region Attribute Functions

        /// <summary>
        /// The page title property
        /// </summary>
        public string Title
        {
            set
            {
                if (value != string.Empty)
                {
                    m_Title = value;
                }
            }
            get
            {
                return m_Title;
            }
        }

        #endregion
    }
}
