///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Developer
{
    public partial class MainTemplatePage : System.Web.UI.MasterPage
    {
        #region Declarations

        private string m_Title = "Kaneva - Imagine What You Can Do";
        private string m_CustomCSS = "";
        private string m_metaTitle = "";
        //private string m_metaTitle = "<meta name=\"title\" content=\" Kaneva. The Online Community and Social Network.\">";
        private string m_metaDescription = "<meta name=\"description\" content=\"Imagine a virtual universe filled with thousands of interconnected worlds and 3D experiences. Kaneva is making this a reality with the introduction of the Kaneva Star Platform where everyone from consumers to developers, to corporations and educational institutions can create immersive 3D virtual worlds and experiences.\">";
        private string m_metaKeywords = "<meta name=\"keywords\" content=\" 3d technology, virtual reality software, virtual reality technology, vrml 3d, mmog platform, game developer platform, 3d worlds, kaneva star platform, kaneva star builder, kaneva star editor, kaneva star explorer \">";
        protected System.Web.UI.WebControls.Literal litJSFile, litCustomVars, litCustomEvents,
            litStyleSheet, litJavascripts, litJavascriptDetect, litTitle, litMetaDataKeywords, litMetaDataDescription, litMetaDataTitle;

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            // Style sheets
            litStyleSheet.Text = "<link id=\"styleSheet\" rel=\"stylesheet\" href=\"" + ResolveUrl("~/css/developer.css") + "\" type=\"text/css\"/>" + CustomCSS +
                "<!--[if IE 6]><link rel=\"stylesheet\" href=\"css/new_IE.css\" type=\"text/css\" /><![endif]-->";

            // Global Javascripts
            litJavascripts.Text = "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" + ResolveUrl("~/jscript/kaneva.js") + "\"></script>" +
                "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" + ResolveUrl("~/jscript/prototype.js") + "\"></script>" +
                "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" + ResolveUrl("~/jscript/SWFObject/swfobject.js") + "\"></script>";

            litJavascriptDetect.Text = "<NOSCRIPT><META HTTP-EQUIV=\"refresh\" CONTENT=\"1; URL=" + ResolveUrl("~/noJavascript.aspx") + "\"></NOSCRIPT>";

            // populates title bar
            if (m_Title.Length > 0)
            {
                litTitle.Text = m_Title;
            }

            // MetadataTitle
            //litMetaDataTitle.Text	= MetaDataTitle;

            // MetadataKeywords	
            litMetaDataKeywords.Text = MetaDataKeywords;

            // MetadataDescription
            litMetaDataDescription.Text = MetaDataDescription;

        }

        #region Attribute Functions

        public usercontrols.Navigation MainNav
        {
            get { return (usercontrols.Navigation)defaultNav; }
        }

        /// <summary>
        /// The page title property
        /// </summary>
        public string Title
        {
            set
            {
                if (value != string.Empty)
                {
                    m_Title = value;
                }
            }
            get
            {
                return m_Title;
            }
        }

        public string CustomCSS
        {
            set
            {
                if (value != string.Empty)
                {
                    m_CustomCSS = value;
                }
            }
            get
            {
                if (m_CustomCSS == null)
                {
                    m_CustomCSS = "";
                }
                return m_CustomCSS;
            }
        }

        public string MetaDataTitle
        {
            set
            {
                if (value != string.Empty)
                {
                    m_metaTitle = value;
                }
            }
            get
            {
                return m_metaTitle;
            }
        }

        /// <summary>
        /// sets or gets meta data keyword html
        /// </summary>
        public string MetaDataKeywords
        {
            set
            {
                if (value != string.Empty)
                {
                    m_metaKeywords = value;
                }
            }
            get
            {
                return m_metaKeywords;
            }
        }

        /// <summary>
        /// sets or gets meta data description html
        /// </summary>
        public string MetaDataDescription
        {
            set
            {
                if (value != string.Empty)
                {
                    m_metaDescription = value;
                }
            }
            get
            {
                return m_metaDescription;
            }
        }

        #endregion

    }
}
