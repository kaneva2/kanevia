///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Developer
{
    public partial class HomepageTest : System.Web.UI.MasterPage
    {
        #region Declarations

        private string m_Title = "Kaneva - Imagine What You Can Do";
        private string m_CustomCSS = "";
        private string m_CustomJS = "";
        private string m_metaTitle = "";
        private string m_metaDescription = "<meta name=\"description\" content=\"Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!  \"/>";
        private string m_metaKeywords = "<meta name=\"keywords\" content=\"free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva\" />";
        protected System.Web.UI.WebControls.Literal litJSFile, litCustomVars, litCustomEvents;

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            // Style sheets
            litStyleSheet.Text = CustomCSS;

            // Global Javascripts
            litJavascripts.Text = CustomJavaScript;
            
            // populates title bar
            if (m_Title.Length > 0)
            {
                litTitle.Text = m_Title;
            }

            // MetadataKeywords	
            litMetaDataKeywords.Text = MetaDataKeywords;

            // MetadataDescription
            litMetaDataDescription.Text = MetaDataDescription;
        }

        #region Attribute Functions

        /// <summary>
        /// The page title property
        /// </summary>
        public string Title
        {
            set
            {
                if (value != string.Empty)
                {
                    m_Title = value;
                }
            }
            get
            {
                return m_Title;
            }
        }

        public string CustomCSS
        {
            set
            {
                if (value != string.Empty)
                {
                    m_CustomCSS = value;
                }
            }
            get
            {
                if (m_CustomCSS == null)
                {
                    m_CustomCSS = "";
                }
                return m_CustomCSS;
            }
        }

        public string CustomJavaScript
        {
            set
            {
                if (value != string.Empty)
                {
                    m_CustomJS = value;
                }
            }
            get
            {
                if (m_CustomJS == null)
                {
                    m_CustomJS = "";
                }
                return m_CustomJS;
            }
        }

        public string MetaDataTitle
        {
            set
            {
                if (value != string.Empty)
                {
                    m_metaTitle = value;
                }
            }
            get
            {
                return m_metaTitle;
            }
        }

        /// <summary>
        /// sets or gets meta data keyword html
        /// </summary>
        public string MetaDataKeywords
        {
            set
            {
                if (value != string.Empty)
                {
                    m_metaKeywords = value;
                }
            }
            get
            {
                return m_metaKeywords;
            }
        }

        /// <summary>
        /// sets or gets meta data description html
        /// </summary>
        public string MetaDataDescription
        {
            set
            {
                if (value != string.Empty)
                {
                    m_metaDescription = value;
                }
            }
            get
            {
                return m_metaDescription;
            }
        }

        #endregion

    }
}
