<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="true" CodeBehind="worldwide-3d-community.aspx.cs" Inherits="KlausEnt.KEP.Developer.worldwide_3d_community" %>
<asp:Content ID="cnt_worldwideCommunity" runat="server" ContentPlaceHolderID="cph_Body">
	<div id="contentWide">

		<div id="main">

			<div id="main_content">
			<h1 class="six png">Kaneva Star Services</h1>
						<p>At the center of the Star Platform is Kaneva Star Services, supporting social networking, user authentication, media management, membership, search, communications, and many other functions that help fulfill the promise of a world-wide 3D community.</p>

				<div id="colSplit" class="twoCol">

					<div class="column" id="colLeft">


						<h2>Membership</h2>
						<p>We provide an <span class="highlight">automated, customizable user sign-up process</span>, with email validation. And since signing up only has to be done once, your audience is immediately as large as the collective membership of all Stars.</p>


						<h2>Microtransactions</h2>
						<p>Sell virtual goods and gifts &ndash;  Kaneva has made it easy by handling credit card and Paypal transactions for you, and converting cash to your Star's local currency. </p>
				      <h2>Communities &amp; Communications</h2>
					    <p>It&rsquo;s all about the people. So we&rsquo;ve made it easy for your community to thrive, with communication tools like 3D Chat, Private Messaging, Event Notification, threaded commenting, and Blasts (micro blogging).</p>
				        <h2>Kaneva.com Social Network Integration</h2>
				        <p>Again, it&rsquo;s about the people, so we&rsquo;ve provided an entire wish list of social networking tools: individual Profile pages, global Friends list, and Contact List importing.</p>
				  </div>

					<div class="column" id="colRight">
						<h2>Media Sharing</h2>
						<p>People love their music and videos, and they love to share them. That's why we've built in complete Media Library functionality. Your members will be able to find new videos and music, upload them, share them, and best of all, enjoy them along with their friends in their 3D space.</p>
						<h2>Search</h2>
						<p>Until now, once a video, object, or person entered a virtual world, it was lost to the rest of the world - no way to find it.</p>
						<p>No longer. Simply type a keyword or two in the Search field at the top of Star Explorer, and you'll find everything you're looking for: Stars, people, media, virtual goods and gifts, communities - you name it. And you can travel in 3D to anything you find, simply by clicking a link in the search results.</p>
						<h2>Authentication</h2>
						<p>Because users travel through Stars in person as an avatar, Step 1 is for them to say who they are (and prove it). So we've built authentication in to the Star Explorer. A quick call to Star Services to verify their identity and password, and Explorer lets them in - no need to authenticate when they travel from Star to Star.</p>
				  </div>
			  </div>
			 </div>
		</div>
		<div id="nextPage" class="applynow"><a href="be-kaneva-star-builder.aspx">Next - Apply Now</a></div>
	</div>
</asp:Content>




