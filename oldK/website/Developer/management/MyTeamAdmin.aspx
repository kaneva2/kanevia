<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="false" CodeBehind="MyTeamAdmin.aspx.cs" Inherits="KlausEnt.KEP.Developer.MyTeamAdmin" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<asp:Content ID="cnt_myTeamAdmin" runat="server" ContentPlaceHolderID="cph_Body">
<!-- CSS to put in -->

<div class="contentWrapper">
      <h1 class="editRole png">Edit Team Roles</h1>
      <asp:Panel ID="pnl_Security" runat="server" Enabled="true">
<ajax:ajaxpanel id="ajMessage" runat="server">

    <asp:label style="position:relative; font-size:18px; padding: 10px 0 10px 0" runat="server" id="messages" visible="true" />

    <div class="col1" id="div_EditPrivileges">
<!-- div that contains the Team listing, labeling, and buttons --> 
        <div id="roles">
		    <asp:label id="lbl_SelectRole" runat="server" visible="true" Text="Select Role" />
            <asp:DropDownList ID="ddl_CompanyRoles" runat="server" AutoPostBack="true" OnSelectedIndexChanged="CompanyRole_SelectedIndexChanged" style="width: 130px" />
            <asp:button id="ibn_DeleteRole" runat="server" causesvalidation="False" OnClick="ibn_DeleteRole_Click" alternatetext="Delete Selected Role" text="Delete Selected Role" />
        </div>
        <div id="privileges">
            <asp:Label ID="lbl_MyTeam" runat="server" Text="My Current Team"></asp:Label>
            <asp:Repeater ID="rpt_Privileges" runat="server" >
                <HeaderTemplate>
                        <table>
                            <tr>
                                <th align="left" style="width:125px;">Feature</th>
                                <th align="left">Description</th>
                                <th align="center" style="width:110px;">Access Level</th>
					        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                            <tr>
                                <td>
								    <asp:label id="lbl_Privilege" runat="server" visible="true" />
                                     <input type="hidden" runat="server" id="hidPrivilegeId"  />
                                </td>
                                <td>
								    <asp:label id="lbl_Description" runat="server" visible="true" />
                                </td>
                                 <td>
                                     <asp:DropDownList ID="ddl_Rights" runat="server"  />
                                </td>
                           </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                            <tr>
                                <td>
								    <asp:label id="lbl_Privilege" runat="server" visible="true" />
                                     <input type="hidden" runat="server" id="hidPrivilegeId"  />
                                </td>
                                <td>
								    <asp:label id="lbl_Description" runat="server" visible="true" />
                                </td>
                                 <td>
                                     <asp:DropDownList ID="ddl_Rights" runat="server"  />
                                </td>
                           </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <asp:button id="ibn_CommitChanges" runat="server" causesvalidation="False" OnClick="ibn_CommitChanges_Click" text="Submit" TabIndex="2" style="width:150px" />
        </div>
        
    </div>
    
    <div class="col2" id="div_AddRole">
        <div id="div_AddNewRole" runat="server" >
            <h3>Create a New Team Role</h3>
          <p>Roles determine which Star Platform features a team member has access to and their ability to make changes.</p>
          <p>You can create new roles and customize the access level for each feature.</p>
            <div class="enterRole">
               <asp:TextBox ID="tbx_RoleName" runat="server" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_RoleName" ControlToValidate="tbx_RoleName" Text="Required" ForeColor="#ce3b3b" Font-Size="13px" Display="Dynamic" runat="server"></asp:RequiredFieldValidator>
            </div>
            <asp:Button ID="ibn_AddRole" runat="server" OnClick="ibn_AddRole_Click" text="Create New Role" CausesValidation="true" />
        </div>
         <div id="div_RoleAdded" runat="server" visible="false">
            <h3>Role Created</h3>
            <p>You have created the following role:</p>
            <p><asp:Label ID="lbl_AddedRole" runat="server"></asp:Label></p>
            <p>Customize this role by changing the access level for each feature using the menus on the left.</p>
            <asp:Button ID="ibn_AnotherRole" runat="server" onClick="ibn_AnotherRole_Click" text="Create New Role" CausesValidation="false" />
        </div>
  </div>
    <div class="clear"><!-- clear float --></div>
    </ajax:ajaxpanel>
    </asp:Panel>
    </div>
</asp:Content>

