///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Developer
{
    public partial class MyProfile : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //set Main Navigation
                Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV] = usercontrols.Navigation.TAB.MANAGEMENT;
                //set 2ndary nav
                Session[DeveloperCommonFunctions.ACTIVE_SUB_NAV] = usercontrols.SubNavAcctManagement.TAB.PROFILE;

                if (!IsPostBack)
                {
                    //ResetBreadCrumb ();
                    AddBreadCrumb(GetNewBreadCrumb("My Profile", GetCurrentURL(), "", 0));
                }

            }
            else
            {
                RedirectToLogin();
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

    }
}
