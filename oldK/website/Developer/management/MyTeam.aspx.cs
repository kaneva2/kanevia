///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using log4net;
using MagicAjax;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;
using System.Text;

namespace KlausEnt.KEP.Developer
{
    public partial class MyTeam : BasePage
    {
        #region Declarations

        protected Pager pgTop, pgBottom, ivTop, ivBottom;
        private string teamSort = "username";
        private string invitesort = "username";
        private int selectedUserId = 0;
        private int currentInvitationRoleId = 0;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess(MANAGE_TEAM))
                {
                    case ACCESS_FULL:
                        //do nothing
                        break;
                    case ACCESS_READ:
                        ReadOnly();
                        break;
                    case ACCESS_NONE:
                    default:
                        NavigateBackToBreadCrumb(GetLastBreadCrumb().Text);
                        break;
                }

                //sets a custom CSS for this page
                ((MainTemplatePage)Master).CustomCSS = "<link href=\""+ ResolveUrl("~/css/MyTeam.css") + "\" rel=\"stylesheet\" type=\"text/css\" />";

                //set Main Navigation
                Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV] = usercontrols.Navigation.TAB.MANAGEMENT;
                //set 2ndary nav
                Session[DeveloperCommonFunctions.ACTIVE_SUB_NAV] = usercontrols.SubNavAcctManagement.TAB.TEAM;
                //set tertiary nav
                Session[DeveloperCommonFunctions.ACTIVE_TERTIARY_NAV] = usercontrols.SubNavMyTeam.TAB.TEAM_MEMBERS;

                if (!IsPostBack)
                {

                    //set alert box to delete button
                    StringBuilder sb = new StringBuilder();
                    sb.Append(" javascript: ");
                    sb.Append(" if(!confirm('Are you sure you want to delete the selected user(s) from your Company? \\n This cannot be undone.')) return false;");
                    imgdeleteUsers.Attributes.Add("onClick", sb.ToString());

                    //set alert box to cancel invite button
                    StringBuilder sb2 = new StringBuilder();
                    sb2.Append(" javascript: ");
                    sb2.Append(" if(!confirm('Are you sure you want to delete the invitation(s)? \\n This cannot be undone.')) return false;");
                    imb_CancelInvite.Attributes.Add("onClick", sb2.ToString());

                    // Set the page on the paging control for team members
                    pgTop.CurrentPageNumber = 1;
                    pgBottom.CurrentPageNumber = 1;

                    // Set the page on the paging control for invitations
                    ivTop.CurrentPageNumber = 1;
                    ivBottom.CurrentPageNumber = 1;

                    //set regular expressions
                    revEmail.ValidationExpression = Kaneva.Constants.VALIDATION_REGEX_EMAIL;

                    //clear the invite fields
                    ClearInviteFields();

                    //get all company roles and store for later binding
                    GetCompanyRoles();
 
                    //bind datagrids
                    BindTeamData(1);
                    BindInviteData(1);

                    //ResetBreadCrumb ();
                    AddBreadCrumb(GetNewBreadCrumb("My Team", GetCurrentURL(), "", 0));
                }

                //clear message
                messages.Text = "";
            }
            else
            {
                RedirectToLogin();
            }
        }

        #region Attributes

        protected string TEAM_SORT
        {
            get
            {
                return teamSort;
            }
            set
            {
                teamSort = value;
            }
        }

        protected string INVITE_SORT
        {
            get
            {
                return invitesort;
            }
            set
            {
                invitesort = value;
            }
        }

        protected List<SiteRole> COMPANY_ROLES
        {
            get
            {

                if (ViewState["companyRoles"] == null)
                {
                    ViewState["companyRoles"] = new List<SiteRole>();
                }

                return (List<SiteRole>)ViewState["companyRoles"];
            }
            set
            {
                ViewState["companyRoles"] = value;
            }
        }

        /// <summary>
        /// stores the current list of administrators
        /// </summary>
        /// <returns>int</returns>
        private List<DeveloperInvitation> OUTSTANDING_INVITATIONS
        {
            get
            {
                if (ViewState["invitations"] == null)
                {
                    return null;
                }
                return (List<DeveloperInvitation>)ViewState["invitations"];
            }
            set
            {
                ViewState["invitations"] = value;
            }
        }


        /// <summary>
        /// stores the list of company developers
        /// </summary>
        /// <returns>int</returns>
        private List<GameDeveloper> COMPANY_GAME_DEVELOPERS
        {
            get
            {
                if (ViewState["companyGameDevelopers"] == null)
                {
                    return null;
                }
                return (List<GameDeveloper>)ViewState["companyGameDevelopers"];
            }
            set
            {
                ViewState["companyGameDevelopers"] = value;
            }
        }

        /// <summary>
        /// stores the current company administrators
        /// </summary>
        /// <returns>int</returns>
        private List<GameDeveloper> COMPANY_ADMINISTRATORS
        {
            get
            {
                if (ViewState["companyAdministrators"] == null)
                {
                    return null;
                }
                return (List<GameDeveloper>)ViewState["companyAdministrators"];
            }
            set
            {
                ViewState["companyAdministrators"] = value;
            }
        }


        #endregion

        #region Helper Functions

        //disables key controls for read only mode
        private void ReadOnly()
        {
            pnl_Security.Enabled = false;
        }

        //delegate function used in LIst FindAll search
        private bool IsDeveloper(GameDeveloper dev)
        {
            return dev.UserId == selectedUserId;
        }

        //delegate function used in LIst FindAll search
        private bool IsIndicatedRole(SiteRole role)
        {
            return role.SiteRoleId == currentInvitationRoleId;
        }

        //clears the invitation fields
        private void ClearInviteFields()
        {
            tbx_InviteeName.Text = "";
            tbx_InviteeEmail.Text = "";
            tbx_InviteMessage.Text = "";
        }

        //function to check for last administrator
        private bool IsLastAdministrator(int userIdToCheck)
        {
            bool lastAdministrator = false; 

            //if there is only one admin make sure they are not the one being changed if so reject the change
            if (COMPANY_ADMINISTRATORS.Count == 1)
            {
                int AdminId = ((GameDeveloper)COMPANY_ADMINISTRATORS[0]).UserId;
                if (AdminId == userIdToCheck)
                {
                    messages.ForeColor = System.Drawing.Color.Crimson;
                    messages.Text = "Operation Not Allowed: You must always have atleast one company administrator.";
                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
                    lastAdministrator = true;
                }
            }
            if (COMPANY_ADMINISTRATORS.Count == 0)
            {
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Urgent! You must always have atleast one company administrator. Please designate one now!";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

            return lastAdministrator;
        }

        private void GetCompanyAdmins()
        {
            COMPANY_ADMINISTRATORS = GetGameDeveloperFacade().GetCompanyAdminsList(GetCurrentUser().CompanyId);
        }

        #endregion

        #region Main Functionality

        private void BindTeamData(int pageNumber)
        {
            try
            {
                string orderby = "";

                //set all filters
                string teamFilter = "";

                //get default pagesize
                int itemsPerPage = DeveloperCommonFunctions.GetResultsPerPage;

                //retrieve the list of company administrators 
                GetCompanyAdmins();

                //retreive the games list
                PagedList<GameDeveloper> pdl = this.GetGameDeveloperFacade().GetGameDevelopersList(GetCurrentUser().CompanyId, teamFilter, orderby, pageNumber, itemsPerPage);

                //store developers
                COMPANY_GAME_DEVELOPERS = (List<GameDeveloper>)pdl.List;

                //bind the result set to the repeater control
                rpt_MyTeam.DataSource = pdl;
                rpt_MyTeam.DataBind();

                //set the paging controls
                pgTop.NumberOfPages = Math.Ceiling((double)pdl.TotalCount / itemsPerPage).ToString();
                pgTop.DrawControl();

                pgBottom.NumberOfPages = Math.Ceiling((double)pdl.TotalCount / itemsPerPage).ToString();
                pgBottom.DrawControl();

                // Tset the results display message
                lblSearch2.Text = lblSearch.Text = GetResultsText(pdl.TotalCount, pageNumber, itemsPerPage, pdl.Count);
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during Team member load.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error: Sorry we were unable to load your team members.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
            
        }

        private void BindInviteData(int pageNumber)
        {
            try
            {
                string orderby = "";

                //set all filters
                string teamFilter = "";

                //get default pagesize
                int itemsPerPage = DeveloperCommonFunctions.GetResultsPerPage;

                //retreive the games list
                PagedList<DeveloperInvitation> pdl = this.GetGameDeveloperFacade().GetCompanyInvitationList(GetCurrentUser().CompanyId, teamFilter, orderby, pageNumber, itemsPerPage);

                //store developers
                OUTSTANDING_INVITATIONS = (List<DeveloperInvitation>)pdl.List;

                //bind the result set to the repeater control
                rpt_MyInvites.DataSource = pdl;
                rpt_MyInvites.DataBind();

                //set the paging controls
                ivTop.NumberOfPages = Math.Ceiling((double)pdl.TotalCount / itemsPerPage).ToString();
                ivTop.DrawControl();

                ivBottom.NumberOfPages = Math.Ceiling((double)pdl.TotalCount / itemsPerPage).ToString();
                ivBottom.DrawControl();

                // Tset the results display message
                lblSearch4.Text = lblSearch3.Text = GetResultsText(pdl.TotalCount, pageNumber, itemsPerPage, pdl.Count);
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during Invitation load.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error: Sorry we were unable to load your outstanding invitations.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        private void GetCompanyRoles()
        {
            try
            {
                COMPANY_ROLES = (List<SiteRole>)(GetSiteSecurityFacade().GetCompanyRolesList(GetCurrentUser().CompanyId));

                //bind roles to pull down
                ddl_inviteAsRole.DataSource = COMPANY_ROLES;
                ddl_inviteAsRole.DataTextField = SiteRole.ROLE_NAME;
                ddl_inviteAsRole.DataValueField = SiteRole.SITE_ROLE_ID;
                ddl_inviteAsRole.DataBind();

            }
            catch (Exception ex)
            {
                m_logger.Error("MyTeam error during retrieval of company roles.", ex);
            }
        }

        #endregion

        #region Events

        protected void imbCancelInvite_Click(object sender, System.EventArgs e)
        {
            //calls function to delete all checked media
            try
            {
                DeleteCheckedInvitations();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during Inivitation revokation.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Cancelation Error: Sorry we were unable to cancel your invite.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

        }

        protected void imgDeleteUsers_Click(object sender, System.EventArgs e)
        {
            //calls function to delete all checked media
            try
            {
                DeleteCheckedTeamMembers();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during Team member deletion.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Deletion Error: Sorry we were unable to delete your team members.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

        }

        protected void ibtn_ClearInvite_Click(object sender, System.EventArgs e)
        {
            ClearInviteFields();
            div_inviteMembers.Visible = false;
            div_AddMembersStart.Visible = true;
        }

        protected void ibtn_ConfirmInvite_Click(object sender, System.EventArgs e)
        {
            div_Finished.Visible = false;
            div_inviteMembers.Visible = true;
        }

        protected void ibtn_AddMember_Click(object sender, System.EventArgs e)
        {
            div_inviteMembers.Visible = true;
            div_AddMembersStart.Visible = false;
        }

        protected void ibtn_inviteMember_Click(object sender, System.EventArgs e)
        {
            //calls function to delete all checked media
            try
            {
                if (Page.IsValid)
                {
                    
                    //gather information for mail message
                    string name = tbx_InviteeName.Text;
                    string email = tbx_InviteeEmail.Text;
                    string message = tbx_InviteMessage.Text;
                    int role = Convert.ToInt32(ddl_inviteAsRole.SelectedValue);
                    string subject = "Welcome To STAR Development!";
                    string inviter = GetCurrentUser().UserInfo.FirstName + " " + GetCurrentUser().UserInfo.LastName;
                    string inviterEmail = GetCurrentUser().UserInfo.Email;
                    
                    //generate Validation key
                    string validationKey = GenerateUniqueString(16);

                    // Build the Mail Message
                    string invMessage = "<br /><br />You have been invited to STAR development by " + inviter + "<br /><br />" + message +
                        "To accept your invitation click the link below: <br /><br />" +
                        "<a href=\"http://" + DeveloperCommonFunctions.GetCurrentSiteName + "/be-kaneva-star-builder.aspx?ivNo=" + validationKey + "\" >Join " + inviter + "'s Team</a>";
                    
                    //write the invitation to the database
                    DeveloperInvitation newInvite = new DeveloperInvitation();
                    newInvite.CompanyId = GetCurrentUser().CompanyId;
                    newInvite.Email = email;
                    newInvite.InviteDate = DateTime.Now;
                    newInvite.Message = invMessage;
                    newInvite.ValidationKey = validationKey;
                    newInvite.Name = name;
                    newInvite.RoleId = role;

                    int result = GetGameDeveloperFacade().InsertInvitation(newInvite);

                    //check for and communicate if insert failed
                    if (result < 1)
                    {
                        throw new Exception("Invitation not saved!");
                    }

                    //send the message
                    Kaneva.MailUtility.SendEmail(inviterEmail, email, subject, invMessage, true, false, 2);
                   

                    //confirm message was sent and reconfigure the page
                    BindInviteData(ivTop.CurrentPageNumber);

                    //assignment for messaging
                    lbl_innvitee.Text = tbx_InviteeName.Text;

                    div_Finished.Visible = true;
                    div_inviteMembers.Visible = false;
                    ClearInviteFields();

                    messages.ForeColor = System.Drawing.Color.Green;
                    messages.Text = "Invitation successfully sent!";
                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");

                }
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during Team member invite.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Invitation Error: Sorry we process your invitation.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

        }

        protected void CompanyRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //first get a reference to the repeater item the drop down is in
                RepeaterItem item = (RepeaterItem)((DropDownList)sender).Parent;

                //next get the user id of the developer in that repeater item
                HtmlInputHidden hidUserId = (HtmlInputHidden)item.FindControl("hidUserId");
                selectedUserId = Convert.ToInt32(hidUserId.Value);

                //get the Drop down causing the change
                DropDownList activeDropDown = (DropDownList)item.FindControl("ddl_CompanyRoles");

                //if there is only one admin make sure they are not the one being changed if so reject the change
                if (IsLastAdministrator(selectedUserId))
                {
                    //reset drop down to original value
                    try
                    {
                        activeDropDown.SelectedValue = (COMPANY_GAME_DEVELOPERS[item.ItemIndex]).DeveloperRoleId.ToString();
                    }
                    catch (Exception)
                    { }

                    return;
                }

                //if no constraints find the Game Developer
                GameDeveloper modifiedDev = COMPANY_GAME_DEVELOPERS.Find(IsDeveloper);
                modifiedDev.DeveloperRoleId = Convert.ToInt32(activeDropDown.SelectedValue);
                
                //save change to the Database
                int result = GetGameDeveloperFacade().UpdateGameDeveloper(modifiedDev);
                 selectedUserId = 0;

               //check for and communicate if insert failed
                if (result < 1)
                {
                    throw new Exception("Update failed!");
                }

                BindTeamData(pgTop.CurrentPageNumber);
                messages.ForeColor = System.Drawing.Color.Green;
                messages.Text = modifiedDev.UserInfo.Username + " has been changed to the role of " + activeDropDown.SelectedItem.Text;
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");

            }
            catch (Exception ex)
            {
                m_logger.Error("Error during Team member edit.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Editing Error: Sorry we were unable to save your team member changes.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        /// <summary>
        /// Delete selected Invitations
        /// </summary>
        protected void DeleteCheckedInvitations()
        {
            CheckBox chkEdit;
            int deletionCount = 0;

            foreach (RepeaterItem rptInvites in rpt_MyInvites.Items)
            {
                chkEdit = (CheckBox)rptInvites.FindControl("chkInviteEdit");

                if (chkEdit.Checked)
                {
                    int hidInviteId = Convert.ToInt32(((HtmlInputHidden)rptInvites.FindControl("hidInviteId")).Value);

                    //remove from database
                    DeveloperInvitation invite = new DeveloperInvitation();
                    invite.InvitationId = hidInviteId;
                    GetGameDeveloperFacade().DeleteInvite(invite);
                    deletionCount++;
                }
            }

            //message to user
            if (deletionCount == 0)
            {
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "No invitations were selected.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
            else
            {
                BindInviteData(ivTop.CurrentPageNumber);
                messages.ForeColor = System.Drawing.Color.DarkGreen;
                messages.Text = deletionCount + " invitation" + (deletionCount > 1 ? "s were " : " was ") + "cancelled.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        /// <summary>
        /// Delete selected Team Members
        /// </summary>
        protected void DeleteCheckedTeamMembers()
        {
            CheckBox chkEdit;
            int deletionCount = 0;
            int refusedDeletions = 0;

            foreach (RepeaterItem rptTeamMember in rpt_MyTeam.Items)
            {
                chkEdit = (CheckBox)rptTeamMember.FindControl("chkEdit");

                if (chkEdit.Checked)
                {
                    int hidUserId = Convert.ToInt32(((HtmlInputHidden)rptTeamMember.FindControl("hidUserId")).Value);

                    //check to see if this person is the last company administrator if so sont delete them.
                    if (!IsLastAdministrator(hidUserId))
                    {
                        //delete the user
                        GetGameDeveloperFacade().DeleteGameDeveloper(hidUserId);
                        deletionCount++;

                        //update company admins
                        GetCompanyAdmins();
                    }
                    else
                    {
                        refusedDeletions++;
                    }
                }
            }

            //message to user
            if ((deletionCount == 0) && (refusedDeletions == 0))
            {
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "No users were selected.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
            else if((deletionCount == 0) && (refusedDeletions > 0))
            {
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = refusedDeletions + " deletion request" + (refusedDeletions > 1 ? "s were " : " was ") + "denied. Can not delete the last Administrator.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
            else
            {
                BindTeamData(pgTop.CurrentPageNumber);
                messages.ForeColor = System.Drawing.Color.DarkGreen;
                messages.Text = deletionCount + " user" + (deletionCount > 1 ? "s were " : " was ") + "deleted from your company.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            pgTop.CurrentPageNumber = e.PageNumber;
            pgTop.DrawControl();
            pgBottom.CurrentPageNumber = e.PageNumber;
            pgBottom.DrawControl();
            BindTeamData(e.PageNumber);
        }

        private void iv_PageChange(object sender, PageChangeEventArgs e)
        {
            ivTop.CurrentPageNumber = e.PageNumber;
            ivTop.DrawControl();
            ivBottom.CurrentPageNumber = e.PageNumber;
            ivBottom.DrawControl();
            BindInviteData(e.PageNumber);
        }

        private void rpt_MyInvites_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //find all the controls in the repeater
                Label lbName = (Label)e.Item.FindControl("lb_RecipName");
                Label lblEmail = (Label)e.Item.FindControl("lbl_RecipEmail");
                Label lblDate = (Label)e.Item.FindControl("lbl_InviteDate");
                Label lblRole = (Label)e.Item.FindControl("lbl_RecipRole");
                HtmlInputHidden hidInviteId = (HtmlInputHidden)e.Item.FindControl("hidInviteId");

                //set the data for each field
                lbName.Text = ((DeveloperInvitation)e.Item.DataItem).Name;
                lblEmail.Text = ((DeveloperInvitation)e.Item.DataItem).Email;
                lblDate.Text = this.FormatDate(((DeveloperInvitation)e.Item.DataItem).InviteDate);
                //pull which role it is and display in grid
                try
                {
                    currentInvitationRoleId = ((DeveloperInvitation)e.Item.DataItem).RoleId;
                    lblRole.Text = ((SiteRole)COMPANY_ROLES.Find(IsIndicatedRole)).RolesName;
                }
                catch { }
                hidInviteId.Value = ((DeveloperInvitation)e.Item.DataItem).InvitationId.ToString();
            }
        }


        private void rpt_MyTeam_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
        }

        private void rpt_MyTeam_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //find all the controls in the repeater
                HtmlAnchor lbUserName = (HtmlAnchor)e.Item.FindControl("lb_UserName");
                Label lblRealName = (Label)e.Item.FindControl("lbl_RealName");
                DropDownList ddlRolesList = (DropDownList)e.Item.FindControl("ddl_CompanyRoles");
                HtmlInputHidden hidUserId = (HtmlInputHidden)e.Item.FindControl("hidUserId");

                //set the pull down data
                ddlRolesList.DataSource = COMPANY_ROLES;
                ddlRolesList.DataTextField = SiteRole.ROLE_NAME;
                ddlRolesList.DataValueField = SiteRole.SITE_ROLE_ID;
                ddlRolesList.DataBind();

                //set the data for each field
                string firstName = ((GameDeveloper)e.Item.DataItem).UserInfo.FirstName;
                string lastName = ((GameDeveloper)e.Item.DataItem).UserInfo.LastName;
                lblRealName.Text = firstName + " " + lastName;
                ddlRolesList.SelectedValue = ((GameDeveloper)e.Item.DataItem).DeveloperRoleId.ToString();
                lbUserName.HRef = GetPersonalChannelUrl(((GameDeveloper)e.Item.DataItem).UserInfo.NameNoSpaces);
                lbUserName.InnerText = ((GameDeveloper)e.Item.DataItem).UserInfo.Username;
                hidUserId.Value = ((GameDeveloper)e.Item.DataItem).UserId.ToString();
            }
        }

        private void rpt_MyInvites_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            ivTop.PageChanged += new PageChangeEventHandler(iv_PageChange);
            ivBottom.PageChanged += new PageChangeEventHandler(iv_PageChange);

            // Set the page on the paging control for invitations
            ivTop.CurrentPageNumber = 1;
            ivBottom.CurrentPageNumber = 1;
        }

        #endregion


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.rpt_MyTeam.ItemCreated += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_MyTeam_ItemCreated);
            this.rpt_MyTeam.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_MyTeam_ItemDataBound);
            this.rpt_MyInvites.ItemCreated += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_MyInvites_ItemCreated);
            this.rpt_MyInvites.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_MyInvites_ItemDataBound);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
            pgBottom.PageChanged += new PageChangeEventHandler(pg_PageChange);
            ivTop.PageChanged += new PageChangeEventHandler(iv_PageChange);
            ivBottom.PageChanged += new PageChangeEventHandler(iv_PageChange);
        }
        #endregion
 
    }
}
