///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;
using System.Text;
using log4net;

namespace KlausEnt.KEP.Developer
{
    public partial class MyTeamAdmin : BasePage
    {
        #region Declarations

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private int selectedSiteRoleId = 0;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess(MANAGE_SECURITY))
                {
                    case ACCESS_FULL:
                        //do nothing
                        break;
                    case ACCESS_READ:
                        ReadOnly();
                        break;
                    case ACCESS_NONE:
                    default:
                        NavigateBackToBreadCrumb(GetLastBreadCrumb().Text);
                        break;
                }

                //sets a custom CSS for this page
                ((MainTemplatePage)Master).CustomCSS = "<link href=\"" + ResolveUrl("~/css/MyTeam.css") + "\" rel=\"stylesheet\" type=\"text/css\" />";

                //set Main Navigation
                Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV] = usercontrols.Navigation.TAB.MANAGEMENT;
                //set 2ndary nav
                Session[DeveloperCommonFunctions.ACTIVE_SUB_NAV] = usercontrols.SubNavAcctManagement.TAB.TEAM;
                //set tertiary nav
                Session[DeveloperCommonFunctions.ACTIVE_TERTIARY_NAV] = usercontrols.SubNavMyTeam.TAB.TEAM_ADMIN;

                if (!IsPostBack)
                {
                    //set alert box to delete button
                    StringBuilder sb = new StringBuilder();
                    sb.Append(" javascript: ");
                    sb.Append(" if(!confirm('Are you sure you want to delete the selected site role \\n This cannot be undone.')) return false;");
                    ibn_DeleteRole.Attributes.Add("onClick", sb.ToString());

                    //get all company roles and store for later binding
                    GetCompanyRoles();

                    //get access levels for privileges
                    GetPrivilegeAccessLevels();

                    //gets Available user privileges
                    GetAvailableUserPrivileges();

                    //dont bing if there are no editable roles
                    if (COMPANY_ROLES.Count > 0)
                    {
                        //bind datagrids
                        BindPrivilegeData();
                        ibn_DeleteRole.Enabled = true;
                    }
                    else
                    {
                        ibn_DeleteRole.Enabled = false;
                        ibn_CommitChanges.Enabled = false;
                    }
                    
                    //ResetBreadCrumb ();
                    AddBreadCrumb(GetNewBreadCrumb("My Team Admin", GetCurrentURL(), "", 0));
                }

            }
            else
            {
                RedirectToLogin();
            }
        }

        #region Main Functionality

        private void CommitPrivilegeChanges()
        {
            try
            {
                //get the need parameters
                int companyId = GetCurrentUser().CompanyId;
                selectedSiteRoleId = Convert.ToInt32(ddl_CompanyRoles.SelectedValue);

                //delete all existing privilege to role relationships
                GetSiteSecurityFacade().DeleteAllPrivilege2Role(selectedSiteRoleId, companyId);


                //loop through each  privilege and save to database
                foreach (RepeaterItem rptPrivilege in rpt_Privileges.Items)
                {
                    DropDownList ddlRights = (DropDownList)rptPrivilege.FindControl("ddl_Rights");
                    HtmlInputHidden hidPrivilegeId = (HtmlInputHidden)rptPrivilege.FindControl("hidPrivilegeId");
                   
                    int accessLevelId = Convert.ToInt32(ddlRights.SelectedValue);
                    int privilegeId = Convert.ToInt32(hidPrivilegeId.Value);

                    GetSiteSecurityFacade().AddNewPrivilege2Role(selectedSiteRoleId, companyId, privilegeId, accessLevelId);
                }

                //give success message
                messages.ForeColor = System.Drawing.Color.Green;
                messages.Text = "Your privilege settings have been saved.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");

            }
            catch (Exception ex)
            {
                m_logger.Error("Error new role creation.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error: Sorry we were unable to create your new role.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

        }

        private bool AddNewRole()
        {
            bool success = false;

            try
            {
                //create a new user role
                SiteRole role = new SiteRole();
                role.CompanyId = GetCurrentUser().CompanyId;
                role.RolesName = tbx_RoleName.Text;
                role.RoleId = 0;

                //save to Database
                int roleId = GetSiteSecurityFacade().AddUserRole(role);

                //check for and communicate if insert failed
                if (roleId >= 1)
                {
                    //indicate the new role was successfully created
                    success = true;

                    //rebind the pulldown 
                    GetCompanyRoles();

                    //set the new role as the active one
                    ddl_CompanyRoles.SelectedValue = roleId.ToString();

                    //repull the privileges
                    BindPrivilegeData();

                    //reactivate incase it was deactivated on due t lack of editable roles
                    ibn_DeleteRole.Enabled = true;
                    ibn_CommitChanges.Enabled = true;

                }
            }
            catch(Exception ex)
            {
                m_logger.Error("Error new role creation.", ex);
            }

            return success;
        }

        private void BindPrivilegeData()
        {
            try
            {
                //gets currently selected company role
                selectedSiteRoleId = Convert.ToInt32(ddl_CompanyRoles.SelectedValue);

                //retrieves the privileges assigned to that role.
                ROLES_2_PRIVELEGES = this.GetSiteSecurityFacade().GetRolePrivileges(selectedSiteRoleId, GetCurrentUser().CompanyId);

                //bind available privileges to repeater
                rpt_Privileges.DataSource = AVAIABLE_PRIVILEGES;
                rpt_Privileges.DataBind();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during role privileges load.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error: Sorry we were unable to load your role's privileges.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        private void GetPrivilegeAccessLevels()
        {
            try
            {
                ACCESS_LEVELS = WebCache.GetAccessLevels();
            }
            catch (Exception ex)
            {
                m_logger.Error("MyTeam error during retrieval of access levels.", ex);
            }
        }

        private void GetAvailableUserPrivileges()
        {
            try
            {
                //get the available privileges
                AVAIABLE_PRIVILEGES = (List<SitePrivilege>) WebCache.GetAvailableUserPrivileges();
            }
            catch (Exception ex)
            {
                m_logger.Error("MyTeam error during retrieval of privileges", ex);
            }
        }

        private void GetCompanyRoles()
        {
            try
            {
                COMPANY_ROLES = (List<SiteRole>)GetSiteSecurityFacade().GetCompanyRolesList(GetCurrentUser().CompanyId);

                //remove the default roles so that they can not be edited
                int i = 0;
                while (i < COMPANY_ROLES.Count)
                {
                    if (COMPANY_ROLES[i].RoleId > 0)
                    {
                        COMPANY_ROLES.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }
                
                //bind roles to pull down
                ddl_CompanyRoles.DataSource = COMPANY_ROLES;
                ddl_CompanyRoles.DataTextField = SiteRole.ROLE_NAME;
                ddl_CompanyRoles.DataValueField = SiteRole.SITE_ROLE_ID;
                ddl_CompanyRoles.DataBind();

            }
            catch (Exception ex)
            {
                m_logger.Error("MyTeam error during retrieval of company roles.", ex);
            }
        }

        #endregion

        #region Attributes

        //protected IList<SiteRole> COMPANY_ROLES
        protected List<SiteRole> COMPANY_ROLES
        {
            get
            {

                if (ViewState["companyRoles"] == null)
                {
                    ViewState["companyRoles"] = new List<SiteRole>();
                }

                //return (IList<SiteRole>)ViewState["companyRoles"];
                return (List<SiteRole>)ViewState["companyRoles"];
            }
            set
            {
                ViewState["companyRoles"] = value;
            }
        }

        protected DataTable ROLES_2_PRIVELEGES
        {
            get
            {

                if (ViewState["roles2Privileges"] == null)
                {
                    ViewState["roles2Privileges"] = new DataTable();
                }

                return (DataTable)ViewState["roles2Privileges"];
            }
            set
            {
                ViewState["roles2Privileges"] = value;
            }
        }

        protected DataTable ACCESS_LEVELS
        {
            get
            {

                if (ViewState["accessLevels"] == null)
                {
                    ViewState["accessLevels"] = WebCache.GetAccessLevels();
                }

                return (DataTable)ViewState["accessLevels"];
            }
            set
            {
                ViewState["accessLevels"] = value;
            }
        }

       // protected IList<SitePrivilege> AVAIABLE_PRIVILEGES
        protected List<SitePrivilege> AVAIABLE_PRIVILEGES
        {
            get
            {

                if (ViewState["availablePrivileges"] == null)
                {
                    ViewState["availablePrivileges"] = new List<SitePrivilege>();
                }

                //return (IList<SitePrivilege>)ViewState["availablePrivileges"];
                return (List<SitePrivilege>)ViewState["availablePrivileges"];
            }
            set
            {
                ViewState["availablePrivileges"] = value;
            }
        }

        #endregion

        #region Helper Functions

        //disables key controls for read only mode
        private void ReadOnly()
        {
            pnl_Security.Enabled = false;
        }

        //delegate function used in LIst FindAll search
        private bool GetRole(SiteRole role)
        {
            return role.SiteRoleId == selectedSiteRoleId;
        }

        #endregion

        #region Events


        protected void ibn_DeleteRole_Click(object sender, System.EventArgs e)
        {
            try
            {
                //gets currently selected company role
                string roleName = ddl_CompanyRoles.SelectedItem.Text;
                selectedSiteRoleId = Convert.ToInt32(ddl_CompanyRoles.SelectedValue);

                //check to see if it is one of the default roles if it is dont allow deletion
                //currently they are designated by a role_id (not site_role_id)  greater than 0
                //this may change after open beta release
                SiteRole roleToDelete = COMPANY_ROLES.Find(GetRole);
                if (roleToDelete.RoleId <= 0)
                {
                    //remove the user role
                    int result = GetSiteSecurityFacade().RemoveUserRole(selectedSiteRoleId, GetCurrentUser().CompanyId);

                    //check for and communicate if delete failed
                    if (result < 1)
                    {
                        throw new Exception("Role deletion failed!");
                    }
                    //rebind the pulldown 
                    GetCompanyRoles();

                    //dont bing if there are no editable roles
                    if (COMPANY_ROLES.Count > 0)
                    {
                        //bind datagrids
                        BindPrivilegeData();
                        ibn_DeleteRole.Enabled = true;
                        ibn_CommitChanges.Enabled = true;
                    }
                    else
                    {
                        ibn_CommitChanges.Enabled = false;
                        ibn_DeleteRole.Enabled = false;
                        this.rpt_Privileges.DataSource = new List<SitePrivilege>();
                        this.rpt_Privileges.DataBind();
                    }

                    messages.ForeColor = System.Drawing.Color.Green;
                    messages.Text = "The role " + roleName + " has been removed.";
                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
                }
                else
                {
                    messages.ForeColor = System.Drawing.Color.Crimson;
                    messages.Text = "You are not allowed to remove the default user roles.";
                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
                }

            }
            catch (Exception ex)
            {
                m_logger.Error("Error during role deletion", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Editing Error: Sorry we were unable to delete the selected role.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

        }

        protected void ibn_CommitChanges_Click(object sender, System.EventArgs e)
        {
            CommitPrivilegeChanges();
        }

        protected void ibn_AddRole_Click(object sender, System.EventArgs e)
        {
            if (AddNewRole())
            {
                div_AddNewRole.Visible = false;
                div_RoleAdded.Visible = true;
            }
            else
            {
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error: Sorry we were unable to create your new role.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        protected void ibn_AnotherRole_Click(object sender, System.EventArgs e)
        {
            tbx_RoleName.Text = "";
            div_AddNewRole.Visible = true;
            div_RoleAdded.Visible = false;
        }

        protected void CompanyRole_SelectedIndexChanged(object sender, EventArgs e)
        {
             BindPrivilegeData();
        }

        private void rpt_Privileges_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //find all the controls in the repeater
                Label lblPrivilege = (Label)e.Item.FindControl("lbl_Privilege");
                Label lblDescription = (Label)e.Item.FindControl("lbl_Description");
                DropDownList ddlRights = (DropDownList)e.Item.FindControl("ddl_Rights");
                HtmlInputHidden hidPrivilegeId = (HtmlInputHidden)e.Item.FindControl("hidPrivilegeId");

                //set the pull down data
                ddlRights.DataSource = ACCESS_LEVELS;
                ddlRights.DataTextField = "access_level";
                ddlRights.DataValueField = "access_level_id";
                ddlRights.DataBind();

                //get the privilege's access level for this role
                int privilegeId = ((SitePrivilege)e.Item.DataItem).PrivilegeId;
                DataRow[] results = ROLES_2_PRIVELEGES.Select("privilege_id = " + privilegeId);

                string accessLevelId = GetSiteSecurityFacade().GetDefaultAccessLevel().ToString();
                //check first to see if anything was returned
                if (results.Length > 0)
                {
                    accessLevelId = results[0]["access_level_id"].ToString();
                }

                //set the data for each field
                lblDescription.Text = ((SitePrivilege)e.Item.DataItem).PrivilegeDescription;
                hidPrivilegeId.Value = privilegeId.ToString();
                lblPrivilege.Text = ((SitePrivilege)e.Item.DataItem).PrivilegeName;
                ddlRights.SelectedValue = accessLevelId;
            }
        }

        #endregion


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.rpt_Privileges.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_Privileges_ItemDataBound);
        }
        #endregion
    }
}
