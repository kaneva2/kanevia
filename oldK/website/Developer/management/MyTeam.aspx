<%@ Page Language="C#"  MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="false" CodeBehind="MyTeam.aspx.cs" Inherits="KlausEnt.KEP.Developer.MyTeam" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>


<asp:Content ID="cnt_myTeam" runat="server" ContentPlaceHolderID="cph_Body">

<div class="contentWrapper">
  <h1 class="team png">Team Members</h1>
  
<script type="text/JavaScript">

function CheckAll(div, checkBox) 
{
    var checkAllBox = document.getElementById(checkBox).checked;
    try
    {
	    SelectAllByParent(div, checkAllBox);
	}
	catch(e)
	{
	    alert('unable to auto select, please select your choices' );
	}
}

</script>



<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    
<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" ForeColor="#ffffff" CssClass="formError" id="ValidationSummary1" DisplayMode="BulletList" runat="server" HeaderText="Please provide the missing information as indicated below:"/>

<ajax:ajaxpanel id="ajMessage" runat="server">

    <asp:label style="position:relative; font-size:18px; padding: 10px 0 10px 0" runat="server" id="messages" visible="true" />

    <div class="col1">
<!-- div that contains the Team listing, labeling, and buttons --> 
        <div id="div_MyTeam">
            <div class="colL"><asp:Label ID="lbl_MyTeam" runat="server" Text="My Current Team"></asp:Label></div>
            <div class="colR result"><asp:label id="lblSearch" runat="server" cssclass="insideTextNoBold" /><Kaneva:Pager id="pgTop" MaxPagesToDisplay="5" runat="server" IsAjaxMode="false" /></div>
            <div class="clear"><!-- clear the floats --></div>
            
            <asp:Repeater ID="rpt_MyTeam" runat="server" >
                <HeaderTemplate>
                        <table>
                            <tr>
							    <th style="width:15px">
								    <input type="checkbox" id="cbxSelectAll" onclick="CheckAll('div_MyTeam','cbxSelectAll')" class="Filter2" />
							    </th>
                                <th align="left">Nickname</th>
                                <th align="center">Name</th>
                                <th align="left">Role</th>
					        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                            <tr>
                                <td>
                                     <asp:checkbox id="chkEdit" CssClass="Filter2" runat="server" Visible="true"/>
                                     <input type="hidden" runat="server" id="hidUserId"  />
                                </td>
                                <td>
                                    <a id="lb_UserName" runat="server" target="_blank" />
                                </td>
                                <td>
								    <asp:label id="lbl_RealName" runat="server" visible="true" />
                                </td>
                                 <td>
                                     <asp:DropDownList ID="ddl_CompanyRoles" runat="server" AutoPostBack="true" OnSelectedIndexChanged="CompanyRole_SelectedIndexChanged" />
                                </td>
                           </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                            <tr>
                                <td>
                                     <asp:checkbox id="chkEdit" CssClass="Filter2" runat="server" Visible="true"/>
                                     <input type="hidden" runat="server" id="hidUserId" />
                                </td>
                                <td>
                                    <a id="lb_UserName" runat="server" target="_blank" />
                                </td>
                                <td>
								    <asp:label id="lbl_RealName" runat="server" visible="true" />
                                </td>
                                 <td>
                                     <asp:DropDownList ID="ddl_CompanyRoles" runat="server" AutoPostBack="true" OnSelectedIndexChanged="CompanyRole_SelectedIndexChanged" />
                                </td>
                           </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <div class="colL"><asp:button id="imgdeleteUsers" runat="server" causesvalidation="False" text="Delete Selected Members" onclick="imgDeleteUsers_Click" border="0" TabIndex="2"/></div>
            <div class="colR result"><asp:label id="lblSearch2" runat="server" cssclass="insideTextNoBold" /><Kaneva:Pager id="pgBottom" MaxPagesToDisplay="5" runat="server" IsAjaxMode="false"></Kaneva:Pager></div>
            <div class="clear"><!-- clear the floats --></div>
		    
        </div>

        
    <!-- div that contains the invite listing, labeling, and buttons --> 
        <div id="div_MyInvites" >
            <div class="colL"><asp:Label ID="lbl_MyInvites" runat="server" Text="My Team Invites"></asp:Label></div>
            <div class="colR result"><asp:label id="lblSearch3" runat="server" cssclass="insideTextNoBold" /><Kaneva:Pager id="ivTop" MaxPagesToDisplay="5" runat="server" IsAjaxMode="false" /></div>
            <div class="clear"><!-- clear the floats --></div>
            <asp:Repeater ID="rpt_MyInvites" runat="server">
                <HeaderTemplate>
                        <table>
                            <tr>
							    <th style=" width:15px">
								    <input type="checkbox" id="cbxSelectAllInv" onclick="CheckAll('div_MyInvites','cbxSelectAllInv')" class="Filter2" />
							    </th>
                                <th align="left">Name</th>
                                <th align="center">Email</th>
                                <th>Role</th>
                                <th align="left">Date</th>
					        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                            <tr>
                                <td>
                                     <asp:checkbox id="chkInviteEdit" CssClass="Filter2" runat="server" Visible="true"/>
                                     <input type="hidden" runat="server" id="hidInviteId"  />
                                </td>
                                <td>
                                    <asp:label id="lb_RecipName" runat="server" visible="true" />
                                </td>
                                <td>
								    <asp:label id="lbl_RecipEmail" runat="server" visible="true" />
                                </td>
                                <td>
								    <asp:label id="lbl_RecipRole" runat="server" visible="true" />
                                </td>
                                 <td>
								    <asp:label id="lbl_InviteDate" runat="server" visible="true" />
                                </td>
                           </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                            <tr>
                                <td>
                                     <asp:checkbox id="chkInviteEdit" CssClass="Filter2" runat="server" Visible="true"/>
                                     <input type="hidden" runat="server" id="hidInviteId" />
                                </td>
                                <td>
                                    <asp:label id="lb_RecipName" runat="server" visible="true" />
                                </td>
                                <td>
								    <asp:label id="lbl_RecipEmail" runat="server" visible="true" />
                                </td>
                                <td>
								    <asp:label id="lbl_RecipRole" runat="server" visible="true" />
                                </td>
                                 <td>
								    <asp:label id="lbl_InviteDate" runat="server" visible="true" />
                                </td>
                           </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <div class="colL"><asp:button id="imb_CancelInvite" runat="server" causesvalidation="False" text="Delete Selected Invites" onclick="imbCancelInvite_Click" border="0" TabIndex="2"/></div>
            <div class="colR result"><asp:label id="lblSearch4" runat="server" cssclass="insideTextNoBold" /><Kaneva:Pager id="ivBottom" MaxPagesToDisplay="5" runat="server" IsAjaxMode="false"></Kaneva:Pager></div>
             <div class="clear"><!-- clear the floats --></div>
 		    
            
        </div>
        
    </div>
    
    <div class="col2" id="column2">
        <div id="div_AddMembersStart" runat="server" >
            <h3>Add Team Member</h3>
            <p>Add a team member to your Star Platform account and customize their access level.</p>
            <asp:Button ID="ibtn_AddMember" runat="server" text="Add Team Member" CausesValidation="false" onclick="ibtn_AddMember_Click"  />
        </div>
        <div id="div_inviteMembers" runat="server" visible="false">
            <h3>Add Team Member</h3>
            <div>
                <div class="frmField"><asp:Label ID="lbl_RecName" runat="server" Text="Name" />:<br />
                  <asp:TextBox ID="tbx_InviteeName" runat="server" style="width:250px" ></asp:TextBox></div>
              <asp:RequiredFieldValidator ID="rfv_RecName" ControlToValidate="tbx_InviteeName" Text="Required" ForeColor="#ce3b3b" Font-Size="13px" Display="Dynamic" runat="server"></asp:RequiredFieldValidator>
            </div>
            <div>
                <div class="frmField"><asp:Label ID="lbl_RecEmail" runat="server" Text="Email" />:<br />
              <asp:TextBox ID="tbx_InviteeEmail" runat="server" style="width:250px"></asp:TextBox></div>
                <asp:regularexpressionvalidator id="revEmail" runat="server" controltovalidate="tbx_InviteeEmail" Text="Invalid Email" ForeColor="#ce3b3b" Font-Size="13px" display="Dynamic" enableclientscript="True"></asp:regularexpressionvalidator>										
			    <asp:RequiredFieldValidator ID="rfv_Email" ControlToValidate="tbx_InviteeEmail" Text="Required" ForeColor="#ce3b3b" Font-Size="13px" Display="Dynamic" runat="server"/><br>
            </div>
            <div>
                <div class="frmField"><asp:Label ID="lbl_Message" runat="server" Text="Personal Message" />:<br />
                  <asp:TextBox ID="tbx_InviteMessage" runat="server" style="width:250px"></asp:TextBox></div>
          </div>
            <div>
                <div class="frmField"><asp:Label ID="lbl_AssgRole" runat="server" Text="Role" />:&nbsp;<asp:DropDownList ID="ddl_inviteAsRole" runat="server" style="width:150px"></asp:DropDownList></div>
           	  <asp:RequiredFieldValidator ID="rfv_projectstatus" ControlToValidate="ddl_inviteAsRole" Text="Required" ForeColor="#ce3b3b" Font-Size="13px" Display="Dynamic" runat="server"/><br>
            </div>
            <p>To add a team member, send them an email invitation. Enter their name, email address, message, and choose a Role. When they join, they&#8217;ll automatically be added to your Team Member list.</p>
            <asp:Button ID="ibtn_inviteMember" runat="server" CausesValidation="true" Text="Send Invitation" onclick="ibtn_inviteMember_Click" style="width:120px" />
            &nbsp;&nbsp;
          <asp:Button ID="ibtn_CancelInvite" runat="server" CausesValidation="false" text="Cancel" onclick="ibtn_ClearInvite_Click"  style="width:120px" />
        </div>
          <div id="div_Finished" runat="server" visible="false">
            <h3>Add Team Member</h3>        
            <p>An email invitation has been sent to <asp:Label ID="lbl_innvitee" runat="server" />.</p>
            <p>When this persons registers for a developer account, they will automatically be added to your team.</p>
            <p>Until they register, they will be listed in Team Invites".</p>
            <p>You can delete a team member or invitation at any time.</p>
            <asp:Button ID="ibtn_ConfirmInvite" runat="server" text="Continue" CausesValidation="false" onclick="ibtn_ConfirmInvite_Click"  />
        </div>
   </div>
    <div class="clear"><!-- clear float --></div>
    </ajax:ajaxpanel>
    
    </asp:Panel>     
    </div>
  
</asp:Content>
