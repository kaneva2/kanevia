///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Developer
{
    public partial class universal_3d_browser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //set Main Navigation
            Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV] = usercontrols.Navigation.TAB.MOTIVATION;
            //set 2ndary nav
            Session[DeveloperCommonFunctions.ACTIVE_SUB_NAV] = usercontrols.SubNavMotivation.TAB.VIRTUAL;

            if (!Page.IsPostBack)
            {
                //set the metadata
                ((MainTemplatePage)Master).Title = "Making the Virtual Universe a Reality | The Kaneva Star Platform";
                ((MainTemplatePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"universal 3d browser, virtual reality application, virtual reality program, virtual reality software, virtual reality technology, mmog platform, game developers, kaneva star platform \">";
                ((MainTemplatePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Imagine a virtual universe where everyone has his or her own 3d place. Kaneva�s virtual reality platform empowers everyone from developers and studios, to corporations and educational institutions.... anyone to create high-quality virtual environments.\">";
            }

        }
    }
}
