///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;
using System.Collections.Generic;
using System.Security.Cryptography;
using MagicAjax;

using log4net;

namespace KlausEnt.KEP.Developer
{
    public partial class be_kaneva_star_builder : BasePage
    {
        #region declarations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private User INVITED_USER = null;
        private string INVITATION_KEY = "";

        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            //sets a custom CSS for this page
            ((MainTemplatePage)Master).CustomCSS = "<link href=\"" + ResolveUrl("~/css/be-kaneva-star-builder.css") + "\" rel=\"stylesheet\" type=\"text/css\" />";

            //set Main Navigation
            Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV] = usercontrols.Navigation.TAB.JOIN;
            if (!Page.IsPostBack)
            {
                //get the parameters from the URL if any
                GetRequestParams();

                //configure the page 
                ConfigurePage();

                //set the metadata
                ((MainTemplatePage)Master).Title = "The Kaneva Star Platform | Sign Up ";
                ((MainTemplatePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"kaneva star builder, kaneva 3d star system, virtual reality platform, mmog platform, game developers platform \">";
                ((MainTemplatePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Imagine creating your own branded 3D star. Kaneva�s virtual reality platform empowers everyone from developers and studios, to corporations and educational institutions... anyone to create high-quality virtual environments. Sign up and learn more today. \">";
            }
        }

        #endregion

        #region Attributes

        /// <summary>
        /// stores the invitation key from the URL
        /// </summary>
        /// <returns>bool</returns>
        private DeveloperInvitation INVITATION
        {
            get
            {
                if (ViewState["Invitation"] == null)
                {
                    ViewState["Invitation"] = "";
                }
                return (DeveloperInvitation)ViewState["Invitation"];
            }
            set
            {
                ViewState["Invitation"] = value;
            }

        }

        /// <summary>
        /// stores the validation status of the user from the URL parameters
        /// the default is 0 for completely new user
        /// </summary>
        /// <returns>integer</returns>
        private int VALIDATION_STATUS
        {
            get
            {
                if (ViewState["ValidationStatus"] == null)
                {
                    ViewState["ValidationStatus"] = 0;
                }
                return (int)ViewState["ValidationStatus"];
            }
            set
            {
                ViewState["ValidationStatus"] = value;
            }
        }

        /// <summary>
        /// stores the company id between page loads
        /// </summary>
        /// <returns>integer</returns>
        private int COMPANY_ID
        {
            get
            {
                if (ViewState["CompanyId"] == null)
                {
                    ViewState["CompanyId"] = 0;
                }
                return (int)ViewState["CompanyId"];
            }
            set
            {
                ViewState["CompanyId"] = value;
            }
        }

        /// <summary>
        /// stores the user id between page loads
        /// </summary>
        /// <returns>integer</returns>
        private int USER_ID
        {
            get
            {
                if (ViewState["userId"] == null)
                {
                    ViewState["userId"] = 0;
                }
                return (int)ViewState["userId"];
            }
            set
            {
                ViewState["userId"] = value;
            }
        }

        /// <summary>
        /// stores the game developer object between page loads
        /// </summary>
        /// <returns>integer</returns>
        private GameDeveloper GAME_DEVELOPER
        {
            get
            {
                if (ViewState["gameDev"] == null)
                {
                    ViewState["gameDev"] = new GameDeveloper();
                }
                return (GameDeveloper)ViewState["gameDev"];
            }
            set
            {
                ViewState["gameDev"] = value;
            }
        }

        #endregion


        #region Pulldown Functions

        protected void SetCountryPullDown()
        {
            //set up country dropdown for user
            drp_Country.DataTextField = "name";
            drp_Country.DataValueField = "countryId";
            drp_Country.DataSource = WebCache.GetCountries();
            drp_Country.DataBind();

            drp_Country.Items.Insert(0, new ListItem("select...", ""));
            drp_Country.SelectedValue = Kaneva.Constants.COUNTRY_CODE_UNITED_STATES;

        }


        #endregion

        #region Helper Functions

        /// <summary>
        /// Checks for validation errors and hilites approppriate section text
        /// </summary>
        private void ShowUserRegistrationErrors()
        {
            string errClassname = "bold red";

            spnEmail.Attributes["class"] = "";

            spnBirthday.Attributes["class"] = "";
            spnUsername.Attributes["class"] = "";
            spnPassword.Attributes["class"] = "";


            // first name
            spnFirstName.Attributes["class"] = !rfFirstName.IsValid ? errClassname : "";

            // last name
            spnLastName.Attributes["class"] = !rfLastName.IsValid ? errClassname : "";

            // email
            if (!rfEmail.IsValid || !revEmail.IsValid)
            {
                spnEmail.Attributes["class"] = errClassname;
            }

            // country
            spnCountry.Attributes["class"] = !rfd_rpCountry.IsValid ? errClassname : "";

            // zip code
            spnPostalCode.Attributes["class"] = !rftxtPostalCode.IsValid ? errClassname : "";

            // birthday
            if (!rfMonth.IsValid || !rfDay.IsValid || !rfYear.IsValid)
            {
                spnBirthday.Attributes["class"] = errClassname;
            }

            // gender
            if (!rbMale.Checked && !rbFemale.Checked)
            {
                cvGender.IsValid = false;
            }
            spnGender.Attributes["class"] = !cvGender.IsValid ? errClassname : "";

            // username
            if (!rfUsername.IsValid || !revUsername.IsValid)
            {
                spnUsername.Attributes["class"] = errClassname;
            }

            // password
            if (!rfPassword.IsValid || !revPassword.IsValid)
            {
                spnPassword.Attributes["class"] = errClassname;
            }
        }

        //populate all pull downs
        private void PopulatePulldowns()
        {
            //set all pulldowns
            SetCountryPullDown();
        }

        //set any needed regular expressions
        private void SetRegularExpressions()
        {
            //set regular expressions
            revEmail.ValidationExpression = Kaneva.Constants.VALIDATION_REGEX_EMAIL;
            revPassword.ValidationExpression = Kaneva.Constants.VALIDATION_REGEX_PASSWORD;
            revUsername.ValidationExpression = Kaneva.Constants.VALIDATION_REGEX_USERNAME;

        }

        //configure page
        private void ConfigurePage()
        {
            GameDeveloper gameDeveloper = null;

            PopulatePulldowns();

            SetRegularExpressions();

            LoadYears();

            // Script to enable/disable zip code field base on selected country
            drp_Country.Attributes.Add("onchange", "CountryCheck ();");

            string scriptString = "<script language=JavaScript>\n";
            scriptString += "function CountryCheck (){\n";
            scriptString += " if (document.getElementById('" + drp_Country.ClientID + "').value == 'US')\n";
            scriptString += " {document.getElementById('" + txt_PostalCode.ClientID + "').disabled = false;\n";
            scriptString += " } else { \n";
            scriptString += "  document.getElementById('" + txt_PostalCode.ClientID + "').disabled = true;\n";
            scriptString += "}}</script>";

            if (!ClientScript.IsClientScriptBlockRegistered("CountryCheck"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "CountryCheck", scriptString);
            }


            //if there is an invitation id pull back by the invitation information and set the 
            //validation status accordingly
            if (INVITATION_KEY != "")
            {
                try
                {
                    //want the process to proceed if no invitation found
                    //facade throws an exception
                    try
                    {
                        //get the invitation information 
                        INVITATION = GetGameDeveloperFacade().GetCompanyInvitation(INVITATION_KEY);
                    }
                    catch { }

                    //if invitation is found get the invited user
                    if (INVITATION != null)
                    {
                        //store the company Id
                        COMPANY_ID = INVITATION.CompanyId;

                        //attempt to pull the user from kaneva by email
                        //want the process to proceed if no user found
                        //facade throws an exception
                        try
                        {
                            //get the invited users information
                            INVITED_USER = GetUserFacade().GetUserByEmail(INVITATION.Email);
                        }
                        catch { }

                        //if the user is found then they already exist on kaneva
                        if (INVITED_USER.UserId > 0)
                        {
                            //first store the user id from the invited user object
                            USER_ID = INVITED_USER.UserId;

                            //want the process to proceed if no developer found
                            //facade throws an exception
                            try
                            {
                                //check to see if they are already registered on this site for the inviting company 
                                gameDeveloper = GetGameDeveloperFacade().GetGameDeveloperNoUserInfo(USER_ID, INVITATION.CompanyId);
                            }
                            catch { }

                            if (gameDeveloper != null)
                            {
                                //redirect them to the landing page for signed in STAR developers
                                Response.Redirect(GetLandingPageLink());
                            }
                            else
                            {
                                //pull the invited
                                VALIDATION_STATUS = GameDeveloper.COMPEXISTS_NEWDEVELOPER;
                            }
                        }
                        else
                        {
                            VALIDATION_STATUS = GameDeveloper.COMPEXISTS_NEWUSER;
                        }
                    }
                }
                catch (Exception ex)
                {
                    m_logger.Error("Registration Error loading the invitation information", ex);
                }
            }

            //process the page according to the validation status id
            switch (VALIDATION_STATUS)
            {
                case GameDeveloper.COMPEXISTS_NEWDEVELOPER: //company exists, user exists but is not a developer
                    newUserForm.Visible = false;
                    displayUser.Visible = true;
                    //grab the user information
                    DisplayUserInfo();
                    break;
                case GameDeveloper.COMPEXISTS_NEWUSER: //company exists, user does not exist
                    newUserForm.Visible = true;
                    displayUser.Visible = false;
                    break;
                case GameDeveloper.NEWCOMPANY_NEWDEVELOPER: //company doesnt exist, user exists AND is not a developer 
                    newUserForm.Visible = false;
                    displayUser.Visible = true;
                    DisplayUserInfo();
                    break;
                case GameDeveloper.NEWCOMP_NEWUSER: //company doesnt exist, user exists AND is not a developer 
                default: //company doesn't exist, user doesn't exist
                    newUserForm.Visible = true;
                    displayUser.Visible = false;
                    break;
            }

        }

        //get all possible parameters
        private void GetRequestParams()
        {
            //check to see if there is an invitation id or not
            try
            {
                INVITATION_KEY = Request["ivNo"].ToString();
            }
            catch (Exception)
            {
            }

            //check to see if there is a validation status or not
            try
            {
                VALIDATION_STATUS = Convert.ToInt32(Request["vastat"]);
            }
            catch (Exception)
            {
            }
        }

        private void DisplayUserInfo()
        {
            try
            {
                //this logic is meant to cover when the user isnt invited but is a logged in registered
                //kaneva user
                if (INVITED_USER == null)
                {
                    //assumes the user is signed in thus (userId is in the session) but not a developer
                    INVITED_USER = GetUserFacade().GetUser(Convert.ToInt32(Session["userId"].ToString()));
                    //store the id for between page loads
                    USER_ID = INVITED_USER.UserId;
                }

                //display the user info
                lbl_dispUsername.Text = INVITED_USER.Username;
                lbl_dispFirstName.Text = INVITED_USER.FirstName;
                lbl_dispLastName.Text = INVITED_USER.LastName;
                lbl_dispGender.Text = INVITED_USER.Gender;

                //pull country Name for display
                lbl_dispCountry.Text = (drp_Country.Items.FindByValue(INVITED_USER.Country)).Text;
                lbl_ZipCode.Text = INVITED_USER.ZipCode;
                lbl_dispBirthday.Text = INVITED_USER.BirthDate;
                lbl_dispEmail.Text = INVITED_USER.Email;

                // set the user id of the user
                //set the company contact fields to default to the logged in user
                txt_Email.Text = lbl_dispEmail.Text;
                txt_FirstName.Text = lbl_dispFirstName.Text;
                txt_LastName.Text = lbl_dispLastName.Text;


            }
            catch (Exception ex)
            {
                m_logger.Error("Error during user information retrieval in registration", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error unable to get your information for display. ";
                //MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        private void LoadYears()
        {
            int startYear = Convert.ToInt16(DateTime.Now.Year) - 13;

            drpYear.Items.Add(new ListItem("> " + startYear.ToString(), startYear.ToString()));

            int currentYear = Convert.ToInt16(DateTime.Now.Year) - 14;
            for (int i = currentYear; i > 1940; i--)
            {
                drpYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            drpYear.Items.Add(new ListItem("< 1940", "1940"));
            drpYear.SelectedIndex = 0;
        }


        #endregion

        #region Primary Functions

        private void LogUserIn(GameDeveloper gameDev)
        {
            
            //log the user in, up date authenication cookie and update their login record
            FormsAuthentication.SetAuthCookie(gameDev.UserId.ToString(), false);
            UpdateLastLogin(gameDev.UserId, Request.UserHostAddress);

            // Set the userId in the session for keeping track of current users online
            Session["userId"] = gameDev.UserId;
            
        }


        private int RegisterNewUser()
        {
            int userId = 0;
            int result = -1;

            cvBlank.IsValid = true;
            cvBlank.ErrorMessage = string.Empty;

            if (drp_Country.SelectedValue == "US")
            {
                rftxtPostalCode.Enabled = true;
            }
            else
            {
                rftxtPostalCode.Enabled = false;
            }

            Page.Validate();
            if (!Page.IsValid || (!rbFemale.Checked && !rbMale.Checked))
            {
                ShowUserRegistrationErrors();

                cvBlank.IsValid = false;
                cvBlank.ErrorMessage = "Some required fields are blank or invalid.";
                return userId;
            }

            //compile users birthday
            DateTime dtBirthDate = new DateTime();
            try
            {
                dtBirthDate = new DateTime(Convert.ToInt32(drpYear.SelectedValue), Convert.ToInt32(drpMonth.SelectedValue), Convert.ToInt32(drpDay.SelectedValue));
            }
            catch
            {
                cvBlank.IsValid = false;
                cvBlank.ErrorMessage = "Birthdate is an invalid date.";
                return userId;
            }

            //check to see if applicant is a minor before proceeding
            if (IsUserAMinor(dtBirthDate))
            {
                //display message alerting user they are under aged
                cvBlank.IsValid = false;
                cvBlank.ErrorMessage = "We apologize but users under the age of " + Kaneva.KanevaGlobals.MinorCutOffAge + " are not allowed to register.";
                return userId;
            }

            string strUserName = Server.HtmlEncode(txtUserName.Text.Trim());
            string strPassword = Server.HtmlEncode(txtPassword.Text.Trim());

            // Hash password
            byte[] salt = new byte[9];
            new RNGCryptoServiceProvider().GetBytes(salt);
            string hashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(MakeHash(strPassword + strUserName.ToLower()) + Convert.ToBase64String(salt), "MD5");

            // Generate a unique key_code for registration purposes
            string keyCode = GenerateUniqueString(20);

            // Create a registration Key
            string regKey = GenerateUniqueString(50);

            string gender = rbMale.Checked ? "M" : "F";

            // Set the default Display Name
            string displayName = "";
            int displayNameLen = (Server.HtmlEncode(txt_FirstName.Text.Trim()) + Server.HtmlEncode(txt_LastName.Text.Trim())).Length;
            if (displayNameLen >= 30)
            {
                if (displayNameLen == 30)
                {
                    displayName = String.Concat(Server.HtmlEncode(txt_FirstName.Text), Server.HtmlEncode(txt_LastName.Text));
                }
                else
                {
                    displayName = String.Concat(Server.HtmlEncode(txt_FirstName.Text) + Server.HtmlEncode(txt_LastName.Text)).Substring(0, 29);
                }
            }
            else
            {
                displayName = String.Concat(Server.HtmlEncode(txt_FirstName.Text), " ", Server.HtmlEncode(txt_LastName.Text));
            }

            try
            {
                result = this.GetUserFacade().InsertUser(strUserName, hashPassword, Convert.ToBase64String(salt), 2, 1,
                    Server.HtmlEncode(txt_FirstName.Text), Server.HtmlEncode(txt_LastName.Text), displayName, Server.HtmlEncode(gender), "", Server.HtmlEncode(txt_Email.Text), dtBirthDate, keyCode,
                    Server.HtmlEncode(drp_Country.SelectedValue), Server.HtmlEncode(txt_PostalCode.Text), regKey, Request.UserHostAddress, (int)Kaneva.Constants.eUSER_STATUS.REGNOTVALIDATED, 0);
            }
            catch (Exception ex)
            {
                m_logger.Error("Registration Error creating the user", ex);
                cvBlank.ErrorMessage = "We apologize but we are unable to register you at this time.";
                return userId;
            }

            switch (Math.Abs(result))
            {
                case 1:
                    {
                        cvBlank.IsValid = false;
                        cvBlank.ErrorMessage = "The user name already exists or is not allowed. Please change your username.";
                        break;
                    }
                case 2:
                    {
                        cvBlank.IsValid = false;
                        cvBlank.ErrorMessage = "The email address you provided is already associated with a Kaneva account. Please provide another email address and re-enter your password.";
                        break;
                    }
                default: //adjusted from a set value of 0 to default because the insert new user has been altered to return the actual userId instead of 0
                    {
                        // Get the userId
                        try
                        {
                            userId = result;

                            // Create thier home page
                            if (CreateDefaultUserHomePage(strUserName, userId))
                            {

                               
                            }
                            else
                            {
                                try
                                {
                                    // Problem creating default home page
                                    GetUserFacade().CleanUpRegistrationIssue(strUserName);
                                }
                                catch { }

                                cvBlank.IsValid = false;
                                cvBlank.ErrorMessage = "The default home page for this user could not be created. registration aborted.";
                                return userId;
                            }


                            // Send out verification email
                            //MailUtilityWeb.SendRegistrationEmail(strUserName, Server.HtmlEncode(txt_Email.Text), keyCode);
                        }
                        catch (Exception ex)
                        {
                            m_logger.Error("Registration Error creating the user - switch", ex);
                            cvBlank.ErrorMessage = "We apologize but we are unable to register you at this time.";
                            return userId;
                        }

                        break;
                    }
            }

            return userId;
        }


        //send interest email
        private void SendEmail()
        {
            try
            {

                // Build the Description
                string strComments = "KGP Star Platform Inquiry Form<br><br>" +
                    "Contact information<br><br>" +
                    "Name:                " + txt_FirstName.Text + " " + txt_LastName.Text + "<br>" +
                    "Email:               " + txt_Email.Text + "<br><br>";

                Kaneva.MailUtility.SendEmail(Kaneva.KanevaGlobals.FromEmail, Kaneva.KanevaGlobals.STARApplyToEmail, 
                    "KGP Star Platform Inquiry", strComments, true, false, 2);
            }
            catch (Exception ex)
            {
                m_logger.Error("Error sending mail on Platform application page", ex);
            }

        }

        //this function is being modified to automatically create a company for users
        //the company will be created based on the user information
        //we can provide a user tool to change the company info if they so desire later
        // Changed 11-09-2009
        private int RegisterNewCompany(int creatorID)
        {
            //create the new company
            int companyId = -1;
            int gameDevId = -1;

            try
            {
                //create a default company for the user
                companyId = GetDevelopmentCompanyFacade().CreateDefaultCompanyForUser(creatorID);
            }
            catch (Exception ex)
            {
                m_logger.Error("Registration Error: company creation error", ex);
                messages.Text = "Registration Error: Unable to create company.";
            }

            //add the creator to the company as a developer and administrator if company creation was successful
            if (companyId > 0)
            {
                try
                {
                    //get the administrators role id
                    SiteRole adminRole = this.GetSiteSecurityFacade().GetCompanyRole(companyId, GameDeveloper.ADMIN_ROLEID);

                    //save the developer to the company
                    gameDevId = AddDeveloperToCompany(companyId, creatorID, adminRole.SiteRoleId);
                }
                catch (Exception ex)
                {
                    m_logger.Error("Registration Error: default administrator/initial developer error", ex);
                    messages.Text = "Registration Error: Unable to create intial developer.";
                }

                //temporary interest email while auto registration is in cloed beta 
                //SendEmail();
            }

            return companyId;
        }

        //adds a user to a development company
        private int AddDeveloperToCompany(int companyId, int userId, int roleId)
        {
            int recordsAffected = 0;

            //set the user as a developer and the administator
            GAME_DEVELOPER.CompanyId = companyId;
            GAME_DEVELOPER.DeveloperRoleId = roleId;
            GAME_DEVELOPER.UserId = userId;

            //save the developer to the company
            recordsAffected = GetGameDeveloperFacade().InsertGameDeveloper(GAME_DEVELOPER);

            try
            {
                //get user information
                GAME_DEVELOPER.UserInfo = GetUserFacade().GetUser(userId);
            }
            catch { }

            return recordsAffected;
        }

        //adds a user to a development company
        private void DeleteUserInvitation()
        {
            try
            {
                GetGameDeveloperFacade().DeleteInvite(INVITATION);
            }
            catch (Exception ex)
            {
                m_logger.Error("Registration Error: default administrator/initial developer error", ex);
                messages.Text = "Registration Error: Unable to delete the user's invitation.";
            }
        }

        #endregion

        #region Event Handlers

         /// <summary>
        /// btnCheckUsername_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCheckUsername_Click(object sender, EventArgs e)
        {
            revUsername.Validate();
            int userNamesFound = 0;

            if (revUsername.IsValid)
            {
                if (txtUserName.Text.Trim() != string.Empty)
                {
                    try
                    {
                        userNamesFound = GetUserFacade().GetUserIdFromUsername(txtUserName.Text.Trim());
                    }
                    catch { }

                    if (userNamesFound > 0)
                    {
                        spnCheckUsername.InnerText = "Not available";
                        spnCheckUsername.Attributes.Add("class", "failure");
                    }
                    else
                    {
                        spnCheckUsername.InnerText = "Available";
                        spnCheckUsername.Attributes.Add("class", "success");
                    }
                }
            }
            else
            {
                spnCheckUsername.InnerText = "Nicknames must be 4-20 characters, and can contain upper or lower-case letters, numbers, and underscores (_) only. No spaces.";
                spnCheckUsername.Attributes.Add("class", "failure");
            }
        }

        /// <summary>
        /// lbn_NewPatchURL_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSubmit_Click(Object sender, EventArgs e) 
        {
            int result = 0;
            int roleId = 0;
            
            //send Email anyway as both notification and secondary collection method
                //SendEmail();

            //process the page according to the validation status id
            switch (VALIDATION_STATUS)
            {
                case GameDeveloper.COMPEXISTS_NEWDEVELOPER: //company exists, user exists but is not a developer

                    //get role id from invitation if invitation found
                    roleId = INVITATION.Equals("") ? 0 : INVITATION.RoleId;

                    //add user to the company
                    result = AddDeveloperToCompany(COMPANY_ID, USER_ID, roleId);

                    //log the user in if they were successfully added to the company
                    if (result > 0)
                    {
                        //remove the invitation
                        DeleteUserInvitation();

                        //log the user in
                        LogUserIn(GAME_DEVELOPER);

                        //added temporarily for closed beta
                        Response.Redirect(GetLandingPageLink());
                    }
                    break;
                case GameDeveloper.COMPEXISTS_NEWUSER: //company exists, user does not exist
                    int userId = RegisterNewUser();
                    if (userId > 0)
                    {
                        //get role id from invitation if invitation found
                        roleId = INVITATION.Equals("") ? 0 : INVITATION.RoleId;

                        //add user to the company
                        result = AddDeveloperToCompany(COMPANY_ID, userId, roleId);

                        //log the user in if they were successfully added to the company
                        if (result > 0)
                        {
                            //remove the invitation
                            DeleteUserInvitation();

                            //log the user in
                            LogUserIn(GAME_DEVELOPER);

                            //added temporarily for closed beta
                            Response.Redirect(GetLandingPageLink());
                        }
                    }
                    break;
                case GameDeveloper.NEWCOMPANY_NEWDEVELOPER: //company doesnt exist, user exists AND is not a developer
                    result = RegisterNewCompany(USER_ID);
                    //log the user in if they were successfully added to the company
                    if (result > 0)
                    {
                        LogUserIn(GAME_DEVELOPER);
                    }

                    break;
                case GameDeveloper.NEWCOMP_NEWUSER: //company doesnt exist, and user doesn't exist 
                default: //company doesn't exist, user doesn't exist
                    //create new user
                    userId = RegisterNewUser();

                    //if the user is successfully created create the company and register user as developer
                    if (userId > 0)
                    {
                        //create new company
                        result = RegisterNewCompany(userId);

                        //log the user in if they were successfully added to the company
                        if (result > 0)
                        {
                            LogUserIn(GAME_DEVELOPER);
                        }
                    }
                    break;
            }

            //proceed to next page if successful otherwise display error message
            if (result < 1)
            {
                messages.Visible = true;
                messages.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                Response.Redirect(ResolveUrl("registrationCompleted.aspx"));
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

    }
}
