///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Developer
{
    public partial class default_b : BasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            // Set up regular expression validators
            revEmail.ValidationExpression = VALIDATION_REGEX_EMAIL;
        }

        protected void aDownload_Click (object sender, EventArgs e)
        {
            string email = txtEmail.Text.Trim ();
            revEmail.Validate ();
            if (revEmail.IsValid)
            {
                if (email.Length > 0)
                {
                    // add email to db
                    GameDeveloperFacade gameDevFacade = new GameDeveloperFacade ();
                    gameDevFacade.InsertGameDeveloperEmail (email);
                }

                // To get the Documentation Link to open in a new window, we reload this page with a bogus query
                // string and some Javascript will be generated that will load the the new browser window
                Response.Redirect ("http://docs.kaneva.com/mediawiki/index.php/3DApp:Getting_Started_with_Kaneva_3D_Apps");
            }
        }

        private const string VALIDATION_REGEX_EMAIL = "^[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";

        #region Web Form Designer generated code
        override protected void OnInit (EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent ();
            base.OnInit (e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.Load += new System.EventHandler (this.Page_Load);
        }
        #endregion

    }
}
