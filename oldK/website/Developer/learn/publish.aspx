<%@ Page Language="C#" MasterPageFile="~/masterpages/AppBrochureTemplatePage.Master" AutoEventWireup="true" CodeBehind="publish.aspx.cs" Inherits="KlausEnt.KEP.Developer.publish" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeadData" runat="server">
    <meta name="title" content="Kaneva. The Online Community and Social Network." />
    <meta name="description" content="Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!"/>
    <meta name="keywords" content="free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva" />
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="../noJavascript.aspx" /></NOSCRIPT>
    <link href="../css/Brochure/Default.css" type="text/css" rel="stylesheet" />  
    <script type="text/javascript" src="../jscript/prototype.js"></script>
    <script type="text/javascript" src="../jscript/Brochure/Default.js"></script>
    <script type="text/jscript">
    <!--
        PageReLoad();
    //-->
     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
<div id="steps">
        <div id="NavigationArea">
            <div id="Navigation"> 
                <div class="navContainer">
                    <div id="mioverview"class="menuItemBox"><a href="../default.aspx" >Overview</a></div>
                    <div id="mistep1" class="menuItemBox"><a href="develop.aspx" >1. Develop</a></div>
                    <div id="mistep2" class="menuItemBox"><a href="playtest.aspx" >2. Playtest</a></div>
                    <div id="mistep3" class="menuItemBox menuSelected"><a href="publish.aspx" >3. Publish</a></div>
                </div>
                <div id="dialog">
					<a href="~/download.aspx" id="downNResLink" runat="server"><span>Create a 3D App >></span></a>						
				</div>
            </div>
            <div id="step3Summary" class="summary">
                <div class="summaryImage">
                    <img id="Img18" alt="Develop" src="../images/Brochure/dev_publish_spot_235x188.jpg" />
                </div>
                <div class="summaryText">
                    <p>
                        <span>3. Publish</span><br /><br />
                        Leverage Kaneva�s social network by using powerful viral marketing features to build your community and enable revenue opportunities for your 3D Apps. You keep 70% of your sales revenue!
                    </p>
                </div>
            </div>
            
        </div>
        <div id="InformationArea">
            <div id="step3">
                <div class="stepHeader"><h6><span>More about Publishing your 3D App to the Kaneva Community:</span></h6></div>
                <div class="stepBody">
                    <div class="ContainerLeft">
                        <div class="stepItemContainerLeft">
                            <div class="stepItemImage">
                                <img id="Img11" alt="Micro-transactions" src="../images/Brochure/dev_bigicon_buy_46x46.jpg" />
                            </div>
                            <div class="stepItemText">
                                <h4>Micro-transactions</h4>
                                <p>
                                    Sell virtual goods, gifts and premium items - Kaneva has made it easy by handling credit card, Pay Pal and other 
                                    transaction types for you, and converting virtual credits into cash from your 3D Apps.
                                </p>
                            </div>
                        </div>
                        <div class="stepItemContainerLeft">
                            <div class="stepItemImage">
                                <img id="Img12" alt="Search" src="../images/Brochure/dev_bigicon_search_46x46.jpg" />
                            </div>
                            <div class="stepItemText">
                                <h4>Search</h4>
                                <p>
                                    Simply type a keyword or two in the search field at the top of 3D Explorer, and you'll find everything  you're looking for:
                                     3D Apps, people, media, virtual goods and gifts, communities - you name it. And you can travel in 3D to anything you find, 
                                     simply by clicking a link in the search results.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="ContainerRight">
                        <div class="stepItemContainerRight">
                            <div class="stepItemImage">
                                <img id="Img13" alt="Social Networking" src="../images/Brochure/dev_bigicon_communities_46x46.jpg" />
                            </div>
                            <div class="stepItemText">
                                <h4>Social Networking</h4>
                                <p>
                                    Kaneva's social network provides a built-in way for you to promote your 3D Apps. Members can share their favorite apps and 
                                    Kaneva's 'Rave' system lets members recommend 3D Apps to the rest of the community
                                </p>
                            </div>
                        </div>
                        <div class="stepItemContainerRight">
                            <div class="stepItemImage">
                                <img id="Img14" alt="Media Sharing" src="../images/Brochure/dev_bigicon_media_46x46.jpg" />
                            </div>
                            <div class="stepItemText">
                                <h4>Media Sharing</h4>
                                <p>
                                    People love their music and videos, and they love to share them. That's why we've built in complete Media Library 
                                    functionality. Your members will be able to find new videos and music, upload them, share them, and best of all, 
                                    enjoy them along with their friends in their 3D space.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"><!-- clear the floats --></div>
                <div id="dialogbig" style="float:right;">
					<a  href="~/download.aspx" runat="server" id="getStartedLink"><span>Get Started >></span></a>									
				</div>

            </div>
           
        </div>
    </div>      
</asp:Content>
