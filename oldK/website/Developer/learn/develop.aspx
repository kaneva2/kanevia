<%@ Page Language="C#" MasterPageFile="~/masterpages/AppBrochureTemplatePage.Master" AutoEventWireup="true" CodeBehind="develop.aspx.cs" Inherits="KlausEnt.KEP.Developer.develop" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeadData" runat="server">
    <meta name="title" content="Kaneva. The Online Community and Social Network." />
    <meta name="description" content="Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!"/>
    <meta name="keywords" content="free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva" />
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="../noJavascript.aspx" /></NOSCRIPT>
    <link href="../css/Brochure/Default.css" type="text/css" rel="stylesheet" />  
    <script type="text/javascript" src="../jscript/prototype.js"></script>
    <script type="text/javascript" src="../jscript/Brochure/Default.js"></script>
    <script type="text/jscript">
    <!--
        PageReLoad();
    //-->
     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
<div id="steps">
        <div id="NavigationArea">
            <div id="Navigation"> 
                <div class="navContainer">
                    <div id="mioverview"class="menuItemBox"><a href="../default.aspx" >Overview</a></div>
                    <div id="mistep1" class="menuItemBox menuSelected"><a href="develop.aspx" >1. Develop</a></div>
                    <div id="mistep2" class="menuItemBox"><a href="playtest.aspx" >2. Playtest</a></div>
                    <div id="mistep3" class="menuItemBox"><a href="publish.aspx" >3. Publish</a></div>
                </div>
                <div id="dialog">
					<a href="~/download.aspx" id="downNResLink" runat="server"><span>Create a 3D App >></span></a>						
				</div>
            </div>
            <div id="step1Summary" class="summary">
                <div class="summaryImage">
                    <img id="Img16" alt="Develop" src="../images/Brochure/dev_develop_spot_235x188.jpg" />
                </div>
                <div class="summaryText">
                    <p>
                        <span>1. Develop 3D Games</span><br /><br />
                        Start developing your 3D app right away. Access the resource center and discover videos, sample scripts, technical 
                        information, and much more.
                    </p>
                </div>
            </div>
            
        </div>
        <div id="InformationArea">
            <div id="step1">
                <div class="stepHeader"><h6><span>More about Developing for the Kaneva Platform:</span></h6></div>
                <div class="stepBody">
                    <div class="ContainerLeft">
                        <div class="stepItemContainerLeft">
                            <div class="stepItemImage">
                                <img id="Img3" alt="Key Features" src="../images/Brochure/dev_bigicon_kaneva_46x46.jpg" />
                            </div>
                            <div class="stepItemText">
                                <h4>Key Features</h4>
                                <ul id="keyFeatures">
                                    <li>Access over 100,000 low cost UGC assests</li>
                                    <li>Upload and use your own 3D assets</li>
                                    <li>Free library of sample templates to use and reference</li>                                    
                                    <li>Protect your Intellectual Property (full control of what code you share)</li>
                                    <li>Kaneva Platform Community Source Code available</li>
                                </ul>
                            </div>
                        </div>
                        <div class="stepItemContainerLeft">
                            <div class="stepItemImage">
                                <img id="Img4" alt="UGC Marketplace" src="../images/Brochure/dev_bigicon_shop_46x46.jpg" />
                            </div>
                            <div class="stepItemText">
                                <h4>User Generated Content (UGC) Marketplace</h4>
                                <p>
                                    Until now,  generating the 3D assets required for an online game experience was expensive and time consuming.  Leverage the UGC Market place to  choose low cost assets like 3D objects, animations, sound effects and other assets needed to create your 3D App.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="ContainerRight">
                        <div class="stepItemContainerRight">
                            <div class="stepItemImage">
                                <img id="Img6" alt="3D Explorer" src="../images/Brochure/dev_bigicon_kaneva_46x46.jpg" />
                            </div>
                            <div class="stepItemText">
                                <h4>3D Explorer</h4>
                                <p>
                                    The Kaneva 3D Explorer allows members to browse and play 3D Apps with their friends.  The 3D Explorer includes a powerful easy to use �build mode� for creating exciting 3D Apps.  The 3D Explorer brings the best elements of a web browser to the immersive environment of 3D App Game play.
                                </p>
                            </div>
                        </div>
                        <div class="stepItemContainerRight">
                            <div class="stepItemImage">
                                <img id="Img5" alt="3D App Server" src="../images/Brochure/dev_bigicon_server_46x46.jpg" />
                            </div>
                            <div class="stepItemText">
                                <h4>3D App Server</h4>
                                <p>
                                    The Kaneva 3D App Game Server powers the social networking, user authentication, media sharing, virtual interaction and array of critical services designed to help market, manage, support and monetize your 3D Apps.  The 3D App Game Server supports a variety hosting options from self-hosting to cloud services. 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"><!-- clear the floats --></div>
                <div id="dialogbig" style="float:right;">
					<a  href="playtest.aspx" ><span>Next >></span></a>									
				</div>
				
              
                
            </div>
           
        </div>
    </div>      
</asp:Content>
