<%@ Page Language="C#" MasterPageFile="~/masterpages/AppBrochureTemplatePage.Master" AutoEventWireup="true" CodeBehind="playtest.aspx.cs" Inherits="KlausEnt.KEP.Developer.playtest" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeadData" runat="server">
    <meta name="title" content="Kaneva. The Online Community and Social Network." />
    <meta name="description" content="Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!"/>
    <meta name="keywords" content="free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva" />
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="../noJavascript.aspx" /></NOSCRIPT>
    <link href="../css/Brochure/Default.css" type="text/css" rel="stylesheet" />  
    <script type="text/javascript" src="../jscript/prototype.js"></script>
    <script type="text/javascript" src="../jscript/Brochure/Default.js"></script>
    <script type="text/jscript">
    <!--
        PageReLoad();
    //-->
     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
<div id="steps">
        <div id="NavigationArea">
            <div id="Navigation"> 
                <div class="navContainer">
                    <div id="mioverview"class="menuItemBox"><a href="../default.aspx" >Overview</a></div>
                    <div id="mistep1" class="menuItemBox"><a href="develop.aspx" >1. Develop</a></div>
                    <div id="mistep2" class="menuItemBox menuSelected"><a href="playtest.aspx" >2. Playtest</a></div>
                    <div id="mistep3" class="menuItemBox"><a href="publish.aspx" >3. Publish</a></div>
                </div>
                
				<div id="dialog">
					<a href="~/download.aspx" id="downNResLink" runat="server"><span>Create a 3D App >></span></a>						
				</div>
                
                
                
            </div>
			<div id="step2Summary" class="summary">
                 <div class="summaryImage">
                    <img id="Img17" alt="Develop" src="../images/Brochure/dev_playtest_spot_235x188.jpg" />
                </div>
               <div class="summaryText">
                    <p>
                        <span>2. Playtest</span><br /><br />
						Let the Kaneva community become your official game testers.  They can provide real time feedback and assist with fine tuning your 3D App experience.
                    </p>
                </div>
            </div>
            
        </div>
        <div id="InformationArea">
			<div id="step2">
                <div class="stepHeader"><h6><span>More about Publishing your 3D App to the Kaneva Community:</span></h6></div>
                <div class="stepBody">
                    <div class="ContainerLeft">
                        <div class="stepItemContainerLeft">
                            <div class="stepItemImage">
                                <img id="Img7" alt="Membership" src="../images/Brochure/dev_bigicon_membership_46x46.jpg" />
                            </div>
                            <div class="stepItemText">
                                <h4>Membership</h4>
                                <p>
                                    We provide an automated, customizable user sign-up process, with email validation. Since signing up only has to be done
                                    once, your audience is immediately as large as as the collective membership of the World of Kaneva.
                                </p>
                            </div>
                        </div>
                        <div class="stepItemContainerLeft">
                            <div class="stepItemImage">
                                <img id="Img8" alt="Authentication" src="../images/Brochure/dev_bigicon_auth_46x46.jpg" />
                            </div>
                            <div class="stepItemText">
                                <h4>Authentication</h4>
                                <p>
                                    Because users travel through 3D Apps in person as an avatar, Step one is for them to say who they are (and prove it). So
                                    we've built authentication in to the 3D explorer. A quick call to the 3D Apps Services to verify their identity and password,
                                    and 3D Explorer lets then in - no need to authenticate when the travel from 3D App to 3D App.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="ContainerRight">
                        <div class="stepItemContainerRight">
                            <div class="stepItemImage">
                                <img id="Img10" alt="Kaneva.com Social Network Integration" src="../images/Brochure/dev_bigicon_communities_46x46.jpg" />                                
                            </div>
                            <div class="stepItemText">
                                 <h4>Kaneva.com Social Network Integration</h4>
                                <p>
                                    Again, it's about the people, so we've provided an entire wish list of social networking tools: individual Profile pages, 
                                    global Friends list, and Contact List importing.
                                </p>
                                
                            </div>
                        </div>
                        <div class="stepItemContainerRight">
                            <div class="stepItemImage">
								<img id="Img9" alt="Communities and Communications" src="../images/Brochure/dev_bigicon_social_46x46.jpg" />                                
                            </div>
                            <div class="stepItemText">
                               <h4>Communities and Communications</h4>
                                <p>
                                    It's all about the people. So we've made it easy for your community to thrive, with communication tools like viral invites, 3D Chat, Private Messaging, 
                                    Event Notification, threaded commenting, and Blast (micro blogging).
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
               <div class="clear"><!-- clear the floats --></div>
                <div id="dialogbig" style="float:right;">
					<a  href="publish.aspx" ><span>Next >></span></a>									
				</div>
            </div>
           
        </div>
    </div>      
</asp:Content>
