<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="false" CodeBehind="be-kaneva-star-builder.aspx.cs" Inherits="KlausEnt.KEP.Developer.be_kaneva_star_builder" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<asp:Content ID="cnt_starBuilder" runat="server" ContentPlaceHolderID="cph_Body">

	<div id="content">

			<div class="registerContent">
				<h1 class="account png">Account Information</h1>
                <!--<p>Enter the information below to create a public Kaneva account. (You can change this to Private later.)</p>-->

                <ajax:ajaxpanel id="ajRegister" runat="server">
                
                <asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" ForeColor="#ffffff" CssClass="formError" id="ValidationSummary1" DisplayMode="BulletList" runat="server" HeaderText="Please provide the missing information as indicated below:"/>
				<asp:customvalidator id="cvBlank" runat="server" display="None" enableclientscript="False"></asp:customvalidator>

                <asp:Label ID="messages" runat="server" Visible="true" />
                <div id="valSum" class="formError" style="color:Red;display:none;"></div>
                
                <div class="col1" id="newUserForm" runat="server">
                <table>
          			<tr>
           				<th style="width:90px"><span id="spnUsername" runat="server">Nickname</span></th>
            			<td><asp:textbox class="biginput" id="txtUserName" runat="server" maxlength="20" tabindex="1" width="115px"></asp:textbox> 
			        <asp:requiredfieldvalidator id="rfUsername" runat="server" controltovalidate="txtUserName" text="*" errormessage=""	display="none"></asp:requiredfieldvalidator>
				    <asp:regularexpressionvalidator id="revUsername" runat="server" controltovalidate="txtUserName" text="*" errormessage="Member/Avater names should be 4-20 characters and a combination of upper and lower-case letters, numbers, and underscores (_) only. Do not use spaces." display="none"></asp:regularexpressionvalidator>																							
			        
				    <ajax:ajaxpanel id="ajpCheckUsername" runat="server">
				        <asp:button id="btnCheckUsername" onclick="btnCheckUsername_Click" width="130px" tabindex="2" runat="server" Text="Check Availability" causesvalidation="False"></asp:button>
				        <div><div class="note">Your name in the World of Kaneva.</div><span id="spnCheckUsername" runat="server"></span>
				        </div>
				    </ajax:ajaxpanel></td>
          		</tr>
          		<tr>
            		<th><span id="spnFirstName" runat="server">First Name:</span></th>
            		<td><asp:textbox class="biginput" id="txt_FirstName" runat="server" maxlength="50" tabindex="3" width="250px"></asp:textbox>
    				<asp:requiredfieldvalidator id="rfFirstName" runat="server" controltovalidate="txt_FirstName" text="*" errormessage="" display="none"></asp:requiredfieldvalidator></td>
          		</tr>
         		 <tr>
            		<th><span id="spnLastName" runat="server">Last Name:</span></th>
            		<td><asp:textbox class="biginput" id="txt_LastName" runat="server" maxlength="50" tabindex="4" width="250px"></asp:textbox>
					<asp:requiredfieldvalidator id="rfLastName" runat="server" controltovalidate="txt_LastName" text="*" errormessage="" display="none"></asp:requiredfieldvalidator></td>
          		</tr>
          		<tr>
            		<th><span id="spnGender" runat="server">Gender:</span></th>
            		<td><asp:radiobutton groupname="rblGender" id="rbFemale" runat="server"  tabindex="5" />																																																											
					<label id="lbl_Female" runat="server">Female</label>
					<asp:radiobutton groupname="rblGender" id="rbMale" Checked="true" runat="server"  tabindex="6" />
					<label id="lbl_Male" runat="server">Male</label>
					<asp:textbox id="hidGender" runat="server" style="display:none;"/>
					<asp:customvalidator id="cvGender" runat="server" controltovalidate="hidGender" text="*" errormessage="" display="none"></asp:customvalidator></td>
          		</tr>
          		<tr>
            		<th><span id="spnCountry" runat="server">Country:</span></th>
            		<td><asp:dropdownlist class="formKanevaText" id="drp_Country" runat="server" tabindex="7" width="250px"></asp:dropdownlist> 
					<asp:requiredfieldvalidator id="rfd_rpCountry" runat="server" controltovalidate="drp_Country" text="*" errormessage="" tabindex="6" display="none"></asp:requiredfieldvalidator></td>
          		</tr>
          		<tr>
            		<th><span id="spnPostalCode" runat="server">ZIP Code:</span></th>
                    <td><asp:textbox class="biginput" id="txt_PostalCode" runat="server" maxlength="25" tabindex="8" width="100"></asp:textbox> 
					<span class="note">(United States only)</span>																						    
					<asp:requiredfieldvalidator id="rftxtPostalCode" runat="server" controltovalidate="txt_PostalCode" text="*" errormessage="Zip code is a required field for U.S. residents." display="none" enabled="False"></asp:requiredfieldvalidator></td>
          		</tr>
          		<tr>
            		<th><span id="spnBirthday" runat="server">Birthday:</span></th>
            		<td><asp:requiredfieldvalidator id="rfMonth" runat="server" controltovalidate="drpMonth" text="*" errormessage="" display="none"></asp:requiredfieldvalidator>
					<asp:requiredfieldvalidator id="rfDay" runat="server" controltovalidate="drpDay" text="*" errormessage="" display="none"></asp:requiredfieldvalidator>
					<asp:requiredfieldvalidator id="rfYear" runat="server" controltovalidate="drpYear" text="*" errormessage=""	display="none"></asp:requiredfieldvalidator>

                    <asp:dropdownlist class="insideBoxText11" id="drpMonth" runat="server" tabindex="9">
						<asp:listitem value="">Month</asp:listitem>
						<asp:listitem value="1">January</asp:listitem>
						<asp:listitem value="2">February</asp:listitem>
						<asp:listitem value="3">March</asp:listitem>
						<asp:listitem value="4">April</asp:listitem>
						<asp:listitem value="5">May</asp:listitem>
						<asp:listitem value="6">June</asp:listitem>
						<asp:listitem value="7">July</asp:listitem>
						<asp:listitem value="8">August</asp:listitem>
						<asp:listitem value="9">September</asp:listitem>
						<asp:listitem value="10">October</asp:listitem>
						<asp:listitem value="11">November</asp:listitem>
						<asp:listitem value="12">December</asp:listitem>
					</asp:dropdownlist>&nbsp;/&nbsp;
					<asp:dropdownlist runat="server" class="insideBoxText11" id="drpDay" tabindex="10">
						<asp:listitem value="">Day</asp:listitem>
						<asp:listitem value="1">01</asp:listitem>
						<asp:listitem value="2">02</asp:listitem>
						<asp:listitem value="3">03</asp:listitem>
						<asp:listitem value="4">04</asp:listitem>
						<asp:listitem value="5">05</asp:listitem>
						<asp:listitem value="6">06</asp:listitem>
						<asp:listitem value="7">07</asp:listitem>
						<asp:listitem value="8">08</asp:listitem>
						<asp:listitem value="9">09</asp:listitem>
						<asp:listitem value="10">10</asp:listitem>
						<asp:listitem value="11">11</asp:listitem>
						<asp:listitem value="12">12</asp:listitem>
						<asp:listitem value="13">13</asp:listitem>
						<asp:listitem value="14">14</asp:listitem>
						<asp:listitem value="15">15</asp:listitem>
						<asp:listitem value="16">16</asp:listitem>
						<asp:listitem value="17">17</asp:listitem>
						<asp:listitem value="18">18</asp:listitem>
						<asp:listitem value="19">19</asp:listitem>
						<asp:listitem value="20">20</asp:listitem>
						<asp:listitem value="21">21</asp:listitem>
						<asp:listitem value="22">22</asp:listitem>
						<asp:listitem value="23">23</asp:listitem>
						<asp:listitem value="24">24</asp:listitem>
						<asp:listitem value="25">25</asp:listitem>
						<asp:listitem value="26">26</asp:listitem>
						<asp:listitem value="27">27</asp:listitem>
						<asp:listitem value="28">28</asp:listitem>
						<asp:listitem value="29">29</asp:listitem>
						<asp:listitem value="30">30</asp:listitem>
						<asp:listitem value="31">31</asp:listitem>
					</asp:dropdownlist>&nbsp;/&nbsp;
					<asp:dropdownlist runat="server" class="insideBoxText11" id="drpYear" tabindex="11">
						<asp:listitem value="">Year</asp:listitem>
					</asp:dropdownlist></td>
         		</tr>
          		<tr>
           			<th><span id="spnEmail" runat="server">Email:</span></th>
           			<td><asp:textbox class="biginput"  id="txt_Email" runat="server" maxlength="100" tabindex="12" width="250px"></asp:textbox> 																							   
					<asp:requiredfieldvalidator id="rfEmail" runat="server" controltovalidate="txt_Email" text="*" errormessage="" display="none"></asp:requiredfieldvalidator>
					<asp:regularexpressionvalidator id="revEmail" runat="server" controltovalidate="txt_Email" text="*" errormessage="Invalid email address." display="none" enableclientscript="True"></asp:regularexpressionvalidator></td>
          		</tr>
          		<tr>
            		<th><span id="spnPassword" runat="server">Password:</span></th>
            		<td><asp:textbox class="biginput" id="txtPassword" runat="server" maxlength="20" textmode="Password" tabindex="13" width="250px"></asp:textbox>
					<asp:requiredfieldvalidator id="rfPassword" runat="server" controltovalidate="txtPassword" text="*" errormessage="" display="none"></asp:requiredfieldvalidator>
					<asp:regularexpressionvalidator id="revPassword" runat="server" controltovalidate="txtPassword" text="*" errormessage="Passwords should be 4-20 characters and a combination of upper and lower-case letters, numbers, and underscores (_) only. Do not use spaces." display="none"></asp:regularexpressionvalidator>
					<asp:requiredfieldvalidator id="rfConfirmPassword" runat="server" controltovalidate="txtPassword" text="*" errormessage="" display="none"></asp:requiredfieldvalidator></td>
          		</tr>
        	</table>
                </div>
 
                 <div class="col1" id="displayUser" runat="server">
                 <p>The following information is associated with your account:</p>
          <table>
          		<tr>
           			<th style="width:90px"><span id="Span1" runat="server">Nickname:</span></th>
            		<td><asp:Label ID="lbl_dispUsername" runat="server" /></td>
         		</tr>
          		<tr>
           			<th><span id="Span3" runat="server">First Name:</span></th>
            		<td><asp:Label ID="lbl_dispFirstName" runat="server" /></td>
         		</tr>
          		<tr>
           			<th><span id="Span4" runat="server">Last Name:</span></th>
            		<td><asp:Label ID="lbl_dispLastName" runat="server" /></td>
         		</tr>
          		<tr>
           			<th><span id="Span5" runat="server">Gender:</span></th>
            		<td><asp:Label ID="lbl_dispGender" runat="server" /></td>
         		</tr>
          		<tr>
           			<th><span id="Span6" runat="server">Country:</span></th>
            		<td><asp:Label ID="lbl_dispCountry" runat="server" /></td>
         		</tr>
          		<tr>
           			<th><span id="Span7" runat="server">ZIP Code:</span></th>
            		<td><asp:Label ID="lbl_ZipCode" runat="server" /></td>
         		</tr>
          		<tr>
           			<th><span id="Span8" runat="server">Birthday:</span></th>
            		<td><asp:Label ID="lbl_dispBirthday" runat="server" /></td>
         		</tr>
          		<tr>
           			<th><span id="Span9" runat="server">Email:</span></th>
            		<td><asp:Label ID="lbl_dispEmail" runat="server" /></td>
          		</tr>
        	</table>
            <p>To edit this information, go to <a href="#">My Account</a></p>
                </div>
               
                              
                <div class="clear"><!-- clear the floats --></div>
                <div class="submitForm"><asp:Button id="btnSubmit" CausesValidation="false" tabindex="23" onclick="btnSubmit_Click" runat="server" text="Submit" style="width:150px" /></div>

                </ajax:ajaxpanel>
		</div>
	</div>


</asp:Content>