///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using KlausEnt.KEP.Kaneva;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Developer
{
    public partial class BillingInformation : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {

            }
            else
            {
                RedirectToLogin();
            }

            if (!IsPostBack)
            {
                GameFacade gameFacade = new GameFacade();
                GameLicenseSubscription gameLicenseSubscription = gameFacade.GetGameLicenseType(GameLicenseSubscriptionType());
                lblLicense.Text = gameLicenseSubscription.Name;

                // Countries
                drpCountry.DataTextField = "Name";
                drpCountry.DataValueField = "CountryId";
                drpCountry.DataSource = WebCache.GetCountries();
                drpCountry.DataBind();

                drpCountry.Items.Insert(0, new ListItem("United States", Constants.COUNTRY_CODE_UNITED_STATES));
                drpCountry.Items.Insert(0, new ListItem("select...", ""));

                // States
                drpState.DataTextField = "StateCode";
                drpState.DataValueField = "StateCode";
                drpState.DataSource = WebCache.GetStates();
                drpState.DataBind();
                drpState.Items.Insert(0, new ListItem("select...", ""));

            }
        }

        private int GameLicenseSubscriptionType()
        {
            if (Request["gls"] != null)
            {
                return Convert.ToInt32(Request["gls"]);
            }
            else
            {
                return 0;
            }
        }

        private int GetOrderId()
        {
            if (Request["oid"] != null)
            {
                return Convert.ToInt32(Request["oid"]);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Back Screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, ImageClickEventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                RedirectToLogin();
                return;
            }

            Response.Redirect(ResolveUrl("~/license/licenseOptions.aspx?oid=" + GetOrderId()));
        }

        /// <summary>
        /// Next Screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNext_Click(object sender, ImageClickEventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                RedirectToLogin();
                return;
            }

            int userId = GetUserId();
            int orderId = GetOrderId();

            // Save contact data
            DataRow drOrder = StoreUtility.GetOrder(orderId, GetUserId());

            int addressId = UsersUtility.InsertAddress(userId, Server.HtmlEncode(txtFullName.Text), Server.HtmlEncode(txtAddress1.Text),
                Server.HtmlEncode(txtAddress2.Text), Server.HtmlEncode(txtCity.Text), Server.HtmlEncode(drpState.SelectedValue),
                Server.HtmlEncode(txtPostalCode.Text), Server.HtmlEncode(txtPhoneNumber.Text), Server.HtmlEncode(drpCountry.SelectedValue));

            // Save the address to the order
            StoreUtility.UpdatePointTransactionAddressId(Convert.ToInt32(drOrder["point_transaction_id"]), addressId);

            Response.Redirect(ResolveUrl("~/license/confirmPurchase.aspx?oid=" + orderId));
        }

        /// <summary>
        /// Skip billing info go right to confirm
        /// </summary>
        protected void lbPaypal_Click(object sender, EventArgs e)
        {
            Response.Redirect(ResolveUrl("~/license/confirmPurchase.aspx?oid=" + GetOrderId()));
        }

        protected void chkUseCompany_onCheckChanged(object sender, EventArgs e)
        {
            CheckBox chkUseCompany = (CheckBox)sender;

            if (chkUseCompany.Checked)
            {
                
                DevelopmentCompanyFacade developmentCompanyFacade = new DevelopmentCompanyFacade();
                DevelopmentCompany dc = developmentCompanyFacade.GetDevelopmentCompany(DeveloperCommonFunctions.CurrentUser.CompanyId);

                txtAddress1.Text = dc.CompanyAddressI;
                txtAddress2.Text = dc.CompanyAddressII;
                txtCity.Text = dc.City;
                txtPhoneNumber.Text = dc.ContactsPhone;
                txtPostalCode.Text = dc.ZipCode;
                txtFullName.Text = dc.ContactsFirstName + " " + dc.ContactsLastName;
            }
        }
    }
}
