///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using KlausEnt.KEP.Kaneva;

namespace KlausEnt.KEP.Developer
{
    public partial class LicenseSelect : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {

            }
            else
            {
                RedirectToLogin();
            }

            rdoLicense.Attributes.Add("onclick", "JavaScript: radioButtonListOnClick(this);");
        }

        /// <summary>
		/// They want to purchase a license
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        protected void btnPurchase_Click(object sender, ImageClickEventArgs e)
        {
            GameFacade gameFacade = new GameFacade();

            if (!Request.IsAuthenticated)
            {
                RedirectToLogin();
                return;
            }

            string gls = "1";
            int iGLS = 1;
            int gameId = GetGameId ();

            // Do they even have a game yet?
            if (gameId.Equals(0))
            {
                IList<Game> games = gameFacade.GetGamesByOwner(DeveloperCommonFunctions.CurrentUser.CompanyId, "", "");
                if (games.Count > 0)
                {
                    gameId = games[0].GameId;
                   
                }
                else
                {
                    Game game = new Game();
                    game.OwnerId = GetUserId();
                    game.GameName = DeveloperCommonFunctions.CurrentUser.CompanyId + "New Star *";
                    game.GameImagePath = "";
                    game.LastUpdated = DateTime.Now;
                    gameId = gameFacade.SaveNewGame(game);
                }
                _GameId = gameId;
            }


            if (rdoLicense.SelectedValue != null)
            {
                gls = rdoLicense.SelectedValue.ToString();
            }

            iGLS = Convert.ToInt32 (gls.ToString ());
            GameLicenseSubscription gameLicenseSubscription = gameFacade.GetGameLicenseType (iGLS);

            // Check to see if they already have an active license
            GameLicense gameLicense = gameFacade.GetGameLicenseByGameId(gameId);

            if (gameLicense.GameLicenseId > 0)
            {
                // Make sure it is not active
                if (gameLicense.LicenseStatusId == (int)GameLicense.LicenseStatus.Active)
                {
                    ShowErrorOnStartup("You already have an active license or you trial period has ended.\\nYou must cancell active licneses before you can change the license type.");
                    return;
                }
                else if (gameLicense.LicenseStatusId == (int)GameLicense.LicenseStatus.PendingAuth ||
                    gameLicense.LicenseStatusId == (int)GameLicense.LicenseStatus.FailedPaymentAuth)
                {
                    // Change the type
                    gameLicense.LicenseSubscriptionId = iGLS;
                    gameFacade.UpdateGameLicense(gameLicense);
                }
                else
                {
                    ShowErrorOnStartup("License is in an unhandled state " + gameLicense.LicenseStatusId);
                    return;
                }
            }

            // Independent Studio, Consumer, Trial get contact info and right to Trial
            if (iGLS.Equals((int)GameLicenseSubscription.LicenseSubscriptionIdTypes.Professional) ||
                iGLS.Equals((int)GameLicenseSubscription.LicenseSubscriptionIdTypes.Educational) ||
                    iGLS.Equals((int)GameLicenseSubscription.LicenseSubscriptionIdTypes.StandAlone))
            {
                // Non-Payment License Types
                if (CreateNonPaymentLicense(gameId, gameLicenseSubscription, gameLicense))
                {
                    Response.Redirect(ResolveUrl("~/license/licenseInformation.aspx?gls=" + gls + "&oid=" + GetOrderId()));
                }
            }
            else
            {
                // Pay license types
                if (CreatePayLicense(gameId, gameLicenseSubscription, gameLicense))
                {
                    Response.Redirect(ResolveUrl("~/license/billingInformation.aspx?gls=" + gls + "&oid=" + _OrderId));
                }
            }

        }

        /// <summary>
        /// CreateNonPaymentLicense
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="gameLicenseSubscription"></param>
        private bool CreateNonPaymentLicense(int gameId, GameLicenseSubscription gameLicenseSubscription, GameLicense gameLicense)
        {
            GameFacade gameFacade = new GameFacade();
            int userId = GetUserId();

            if (gameLicense.GameLicenseId.Equals(0))
            {
                // Create the license
                gameLicense = new GameLicense(0, gameId, gameLicenseSubscription.LicenseSubscriptionId, (int)GameLicense.LicenseStatus.Active,
                    0, userId + "U" + gameId.ToString() + "G" + GenerateUniqueString(16), new DateTime(), new DateTime(), new DateTime(),2000);
                gameLicense.GameLicenseId = gameFacade.AddNewGameLicense(gameLicense, gameLicenseSubscription.LengthOfSubscription);
            }
            else
            {
                // Update it to active
                gameLicense.LicenseSubscriptionId = gameLicenseSubscription.LicenseSubscriptionId;
                gameLicense.LicenseStatusId = (int) GameLicense.LicenseStatus.Active;
                gameFacade.UpdateGameLicense(gameLicense);
            }

            if (gameLicense.GameLicenseId < 1)
            {
                ShowErrorOnStartup("License could not be created", false);
                return false;
            }
            else
            {
                // Insert form info
                GameLicenseContact gameLicenseContact = new GameLicenseContact(0, Server.HtmlEncode(txtName.Text), Server.HtmlEncode(txtOrgName.Text),
                    Server.HtmlEncode(txtPhone.Text), Server.HtmlEncode(txtEmail.Text), Server.HtmlEncode(txtAbout.Text), gameLicense.GameLicenseId);
                gameFacade.InsertGameLicenseContact(gameLicenseContact);
            }

            return true;
        }

        /// <summary>
        /// CreatePayLicense
        /// </summary>
        private bool CreatePayLicense(int gameId, GameLicenseSubscription gameLicenseSubscription, GameLicense gameLicense)
        {
            GameFacade gameFacade = new GameFacade();
            int userId = GetUserId();

            if (gameLicense.GameLicenseId.Equals(0))
            {
                // Create the game license
                gameLicense = new GameLicense(0, gameId, gameLicenseSubscription.LicenseSubscriptionId, (int)GameLicense.LicenseStatus.PendingAuth,
                    0, userId + "U" + gameId.ToString() + "G" + GenerateUniqueString(16), new DateTime(), new DateTime(), new DateTime(), 2000);
                gameLicense.GameLicenseId = gameFacade.AddNewGameLicense(gameLicense, gameLicenseSubscription.LengthOfSubscription);
            }

            if (gameLicense.GameLicenseId < 1)
            {
                ShowErrorOnStartup("License could not be created", false);
                return false;
            }

            // Profesional, Educational, Stand-Alone, Trial go to Billing and download
            // These require a purchase
            _OrderId = StoreUtility.CreateOrder(userId, Request.UserHostAddress, (int)Constants.eORDER_STATUS.CHECKOUT);

            // Set the purchase type
            StoreUtility.SetPurchaseType(_OrderId, Constants.ePURCHASE_TYPE.STAR_LICENSE);

            // Record the transaction in the database, marked as checkout
            int transactionId = StoreUtility.PurchasePoints(userId, (int)Constants.eTRANSACTION_STATUS.CHECKOUT, "Star License " + gameLicenseSubscription.Name + " Payment", 0, (int)Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE,
                gameLicenseSubscription.Amount, 0, 0, 0, Request.UserHostAddress);

            // Set the order id on the point purchase transaction
            StoreUtility.UpdatePointTransaction(transactionId, _OrderId);
            StoreUtility.UpdateOrderPointTranasactionId(_OrderId, transactionId);

            // Create the subcription records 
            GameSubscription gameSubscription = new GameSubscription(0, gameLicense.GameLicenseId, gameId, gameLicenseSubscription.Amount, true, new DateTime(),
                (uint)GameSubscription.SubscriptionStatus.Pending, (int)Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE, "", false);
            int gameSubscriptionId = gameFacade.InsertGameSubscription(gameSubscription);

            // Tie License Subscription to the Order
            gameFacade.InsertGameSubscriptionOrder(gameSubscriptionId, _OrderId);

            return true;
        }

        private int GetOrderId()
        {
            if (Request["oid"] != null)
            {
                return Convert.ToInt32(Request["oid"]);
            }
            else
            {
                return _OrderId;
            }
        }

        private int GetGameId()
        {
            if (Request["gameId"] != null)
            {
                return Convert.ToInt32(Request["gameId"]);
            }
            else
            {
                return _GameId;
            }
        }

        private int _OrderId = 0;
        private int _GameId = 0;
    }
}
