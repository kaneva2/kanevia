<%@ Page MasterPageFile="~/masterpages/MainTemplatePage.Master" Language="C#" AutoEventWireup="true" CodeBehind="confirmPurchase.aspx.cs" Inherits="KlausEnt.KEP.Developer.ConfirmPurchase" %>
<%@ Register TagPrefix="Kaneva" Namespace="KlausEnt.KEP.Kaneva.WebControls" Assembly="Kaneva.WebControls" %>

<asp:Content ID="cnt_confirmPurchase" runat="server" ContentPlaceHolderID="cph_Body">

    <script type="text/javascript" src="../jscript/prototype.js"></script>

    <asp:validationsummary showmessagebox="False" showsummary="True" id="valSum" displaymode="BulletList" runat="server" headertext=""/>

    <asp:Label ID="lblErrors" runat="server"></asp:Label>

    <div style="float:left;width:400px;">
        <table cellspacing="0" cellpadding="2" align="left" border="1">
            <tr>
	            <td><input type="radio" runat="server" onclick="javascript:$('divCC').style.display='block';" id="radPmtCC" name="paymentType" value="cc" checked />Credit Card</td>
	            <td style="padding-left:10px;"><img id="Img7" runat="server" src="http://www.kaneva.com/images/credit_card_icons.gif" alt="Kaneva accepts American Express, Discover/NOVUS, MasterCard, and Visa."  height="20" border="0"></td>
            </tr>
            <tr id="trPaypal">
	            <td><input type="radio" runat="server" onclick="javascript:$('divCC').style.display='none';" id="radPmtPP" name="paymentType" value="pp" />Paypal</td>
	            <td style="padding-left:10px;"><a href="#" onclick="javascript:window.open('https://www.paypal.com/us/cgi-bin/webscr?cmd=xpt/cps/popup/OLCWhatIsPayPal-outside','olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=350');"><img  src="https://www.paypal.com/en_US/i/logo/PayPal_mark_37x23.gif" border="0" alt="Acceptance Mark"></a></td>
            </tr>
        </table>
        <br/><br/><br/><br/><br/>
        <div id="divCC">
            Name as it appears on card&nbsp;<asp:requiredfieldvalidator id="rftxtNameOnCard" controltovalidate="txtNameOnCard" text="*" errormessage="Name as it appears on card is a required field" display="Dynamic" runat="server"/><br>
		        <asp:textbox id="txtNameOnCard" class="biginput" style="width:300px" maxlength="100" runat="server"/>
		    <br/><br/>
		    Card Type <kaneva:cardtypeslistbox class="biginput" id="drpCardType" runat="server" style="width:150px" rows="1">
							        <asp:listitem value="CardTypes Here">CardTypes Here</asp:listitem>
						        </kaneva:cardtypeslistbox>
			<br/><br/>
		    Card Number&nbsp;<kaneva:creditcardvalidator id="ccvNumber" controltovalidate="txtCardNumber" errormessage="Please enter a valid credit card number for the card type selected" display="Dynamic" text="*" runat="server" cardtypeslistbox="drpCardType" /><asp:requiredfieldvalidator id="rfvCardNumber" controltovalidate="txtCardNumber" text="*" errormessage="Card Number is a required field" display="Dynamic" runat="server"/><br>
		        <asp:textbox id="txtCardNumber" class="biginput" style="width:255px" maxlength="16" runat="server" enableviewstate="false" /><br/><span class="note">Please do not enter spaces.<br/>For security purposes Kaneva does NOT store credit card numbers.</span>
		    <br/><br/> 
		    Security Code&nbsp;<asp:requiredfieldvalidator id="rftxtSecurityCode" controltovalidate="txtSecurityCode" text="*" errormessage="Security Code is a required field" display="Dynamic" runat="server"/><br>
			        <asp:textbox id="txtSecurityCode" class="biginput" style="width:100px" maxlength="4" runat="server"/> 
			        &nbsp;&nbsp;<a onmouseover="this.style.cursor='pointer'" onfocus="this.blur();" onclick="document.getElementById('PopUp').style.display = 'block';  return false;" href="#">What is this?</a>
			        <div id="PopUp" style="display: none; position: absolute; left: 190px; bottom: 110px; margin: 0; padding: 15px 15px 5px 15px; background: transparent url(../images/popup_point.gif) no-repeat bottom left; text-align: justify; font-size: 12px; width: auto;">
				        <div style="display:block; border: 3px solid #f7ce52; padding: 10px 10px 0 10px; background-color: #FFFFFF;">
					        <table border="0" cellspacing="0" cellpadding="3">
						        <tr>
							        <td><img id="Img8" src="http://www.kaneva.com/images/mini_amex.gif" runat="server" alt="American Express displays the 4 digit number on the front right of the card." width="118" height="73" border="0"></td>
							        <td><img id="Img9" src="http://www.kaneva.com/images/mini_discover.gif" runat="server" alt="Discover displays the 3 digit number on the back of the card." width="118" height="73" border="0"></td>
						        </tr>
						        <tr>
							        <td><img id="Img10" src="http://www.kaneva.com/images/mini_mc.gif" runat="server" alt="MasterCard displays the 3 digit number on the back of the card." width="118" height="73" border="0"></td>
							        <td><img id="Img11" src="http://www.kaneva.com/images/mini_visa.gif" runat="server" alt="Visa displays the 3 digit number on the back of the card." width="118" height="73" border="0"></td>
						        </tr>
						        <tr>
							        <td colspan="2">Please enter your 3 or 4 digit Card ID Number</td>
						        </tr>
    							
						        <tr>
							        <td colspan="2"><div style="text-align: right;"><a href="#" onmouseover='' onfocus='this.blur();' onclick="document.getElementById('PopUp').style.display = 'none';  return false; " >Close</a></div></td>
						        </tr>												 
					        </table>
				        </div>
			        </div>    
		    <br /><br />
		    Expiration Date<br>
			        <asp:dropdownlist class="biginput" id="drpMonth" runat="Server" style="width:150px">
				        <asp:listitem value="01">(01) January</asp:listitem>
				        <asp:listitem value="02">(02) February</asp:listitem>
				        <asp:listitem value="03">(03) March</asp:listitem>
				        <asp:listitem value="04">(04) April</asp:listitem>
				        <asp:listitem value="05">(05) May</asp:listitem>
				        <asp:listitem value="06">(06) June</asp:listitem>
				        <asp:listitem value="07">(07) July</asp:listitem>
				        <asp:listitem value="08">(08) August</asp:listitem>
				        <asp:listitem value="09">(09) September</asp:listitem>
				        <asp:listitem value="10">(10) October</asp:listitem>
				        <asp:listitem value="11">(11) November</asp:listitem>
				        <asp:listitem value="12">(12) December</asp:listitem>
			        </asp:dropdownlist>
			        &nbsp;&nbsp;
			        <asp:dropdownlist class="biginput" runat="server" id="drpYear" style="width:100px"> 
				        <asp:listitem value="2007">2007</asp:listitem>
				        <asp:listitem value="2008">2008</asp:listitem>
				        <asp:listitem value="2009">2009</asp:listitem>
				        <asp:listitem value="2010">2010</asp:listitem>
				        <asp:listitem value="2011">2011</asp:listitem>
				        <asp:listitem value="2012">2012</asp:listitem>
				        <asp:listitem value="2013">2013</asp:listitem>
				        <asp:listitem value="2014">2014</asp:listitem>
				        <asp:listitem value="2015">2015</asp:listitem>
				        <asp:listitem value="2016">2016</asp:listitem>
				        <asp:listitem value="2017">2017</asp:listitem>
				        <asp:listitem value="2018">2018</asp:listitem>
				        <asp:listitem value="2019">2019</asp:listitem>
				        <asp:listitem value="2020">2020</asp:listitem>
			        </asp:dropdownlist>
	        <br /><br />
	        
	        
        </div>
        
    
    </div>
    
    <div style="float:left;width:400px;">
        <h1>Confirm Your Purchase</h1>
        <br/><br/><br/>
	    You have selected the following items for puchase...
	    <div style="border:solid 2 blue;background-color:Gray">
	        Our <asp:Label runat="server" ID="lblLicense"></asp:Label> License...
	        <asp:Label runat="server" ID="lblLicenseDesc"></asp:Label>
	        Lorem ipsum dolor sit amet, consectetuer
            adipiscing elit. Suspendisse ut purus. Suspendisse
            tristique. Cras et sem at odio porttitor mattis. Donec
            dignissim ornare tortor.
	    </div>
    </div>
    
    <div style="clear:both"> 
        <asp:imagebutton id="Imagebutton1" CausesValidation="false" imageurl="~/images/Back.gif" runat="Server" onclick="btnBack_Click" alternatetext="Choose License" width="200" height="41" border="0"/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:imagebutton id="btnNext" CausesValidation="true" imageurl="~/images/Next.gif" runat="Server" onclick="btnMakePurchase_Click" alternatetext="Submit For Purchase" width="200" height="41" border="0"/>
    </div>	
    

<asp:button id="btnMakePurchase" runat="Server" style="display:none;width:1px;height:1px;" causesvalidation="False"  onclick="btnMakePurchase_Click" class="Filter2"/>
																		
    																												
<script type="text/javascript">
function makePurchase ()
{
    if ($('radPmtCC').checked)
    {
	    if (typeof(Page_ClientValidate) == 'function') 
		    if (!Page_ClientValidate()){return false;};
		
	    this.disabled = true;
	    ShowLoading(true,'Please wait while we process your order...<br/><img src=\"../images/progress_bar.gif\">');
	    <asp:literal id="litPostBackRef" runat="server" />;
    }
}
</script>

</asp:Content>