<%@ Page MasterPageFile="~/masterpages/MainTemplatePage.Master" Language="C#" AutoEventWireup="true" CodeBehind="LicenseOptions.aspx.cs" Inherits="KlausEnt.KEP.Developer.LicenseSelect" %>

<asp:Content ID="cnt_licenseOptions" runat="server" ContentPlaceHolderID="cph_Body">

<script type="text/javascript" src="../jscript/prototype.js"></script>

<script language=javascript>

function getRadioSelectedValue(radioList){
    var options = radioList.getElementsByTagName('input');
    for(i=0;i<options.length;i++){
        var opt = options[i];
        if(opt.checked)
        {
            return opt.value;
        }    
    }
}

function radioButtonListOnClick(elementRef)
{
    showContact(getRadioSelectedValue(elementRef));
}

function showContact(val)
{
  if ((val==3) || (val==6) || (val==5))
  {
    $(divContactForm).style.display='block';
    $(divPurchaseStats).style.display='none';
  }
  else
  {
    $(divContactForm).style.display='none'
    $(divPurchaseStats).style.display='block';
  }
}

</script>

<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" cssclass="errBox black" style="width:70%;" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

Premium
<asp:radiobuttonlist id="rdoLicense" runat="server">
  <asp:listitem id="option1" runat="server" Value="3" Text="Professional" />
  <asp:listitem id="option2" runat="server" Value="6" Text="Educational" />
  <asp:listitem id="option3" runat="server" Value="5" Text="Standalone" />
  <asp:listitem id="option1d" runat="server" Value="1" Text="Consumer"/>
  <asp:listitem id="option2d" runat="server" Value="2" Text="Independent Studio" />
  <asp:listitem id="option3d" runat="server" Value="4" Text="Trial" />
</asp:radiobuttonlist>

<div id="divContactForm" style="display:none;background-color:Blue;">
Please contact us to inquire about purchase options for this product.<br/><br/>
You can reach us toll free at 1-800-XXX-XXXX.<br/><br/>
Or you can request that we get in touch with you by filling out the
form below:<br/><br/>

    Your Name<asp:textbox id="txtName" style="width:455" maxlength="100" runat="server"/>
    <asp:requiredfieldvalidator id="rfname" controltovalidate="txtName" text="" errormessage="Name is a required field." display="None" runat="server"/><br/>
    Org Name<asp:textbox id="txtOrgName" style="width:455" maxlength="100" runat="server"/>
    <asp:requiredfieldvalidator id="rfOrgName" controltovalidate="txtOrgName" text="" errormessage="Org Name is a required field." display="None" runat="server"/><br/>
    Phone<asp:textbox id="txtPhone" style="width:455" maxlength="50" runat="server"/>
    <asp:requiredfieldvalidator id="rfPhone" controltovalidate="txtPhone" text="" errormessage="Phone is a required field." display="None" runat="server"/><br/>
    Email<asp:textbox id="txtEmail" style="width:455" maxlength="100" runat="server"/>
    <asp:requiredfieldvalidator id="rfEmail" controltovalidate="txtEmail" text="" errormessage="Email is a required field." display="None" runat="server"/><br/>
    About your project<asp:textbox id="txtAbout" style="width:455" maxlength="255" runat="server"/>
    <asp:requiredfieldvalidator id="rfAbout" controltovalidate="txtAbout" text="" errormessage="About is a required field." display="None" runat="server"/><br/>
    <asp:imagebutton id="btnSubmitContact" CausesValidation="true" imageurl="~/images/Next.gif" runat="Server" onclick="btnPurchase_Click" alternatetext="Submit width="200" height="41" border="0"/>
</div>

<div id="divPurchaseStats" style="display:none;background-color:Gray;">
    Our Independent Studio License...<br/><br/>
    Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
    Suspendisse ut purus. Suspendisse tristique. Cras et sem at odio
    porttitor mattis. Donec dignissim ornare tortor.<br/><br/>
    Nice Chart goes here...<br/><br/><br/><br/>
    Pricing subject to change:<br/><br/>

    <asp:imagebutton id="btnPurchase" CausesValidation="false" imageurl="~/images/Next.gif" runat="Server" onclick="btnPurchase_Click" alternatetext="Purchase width="200" height="41" border="0"/>
</div>
</asp:Content> 