///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Developer
{
    public partial class LicenseInformation : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {

            }
            else
            {
                RedirectToLogin();
            }

            if (!IsPostBack)
            {
                // Show license info
                GameFacade gameFacade = new GameFacade ();
                GameLicenseSubscription gameLicenseSubscription = gameFacade.GetGameLicenseType(GameLicenseSubscriptionType());
                lblLicenseInfo.Text = gameLicenseSubscription.Name;
            }
        }

        private int GameLicenseSubscriptionType()
        {
            if (Request["gls"] != null)
            {
                return Convert.ToInt32(Request["gls"]);
            }
            else
            {
                return 0;
            }
        }

        private int GetOrderId()
        {
            if (Request["oid"] != null)
            {
                return Convert.ToInt32(Request["oid"]);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Next Screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNext_Click(object sender, ImageClickEventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                RedirectToLogin();
                return;
            }

            Response.Redirect(ResolveUrl("~/games/gameResources.aspx"));
        }
    }
}
