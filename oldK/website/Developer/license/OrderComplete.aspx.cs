///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using log4net;
using KlausEnt.KEP.Kaneva;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Developer
{
    public partial class OrderComplete : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //bool bSystemDown = false;
            //string userErrorMessage = "";

            //CybersourceAdaptor cybersourceAdaptor = new CybersourceAdaptor();
            //cybersourceAdaptor.GetSubscription(ref bSystemDown, ref userErrorMessage, "2240036459700008402434", 243701);

            //GameFacade gameFacade = new GameFacade();
            //GameSubscription gameSubscription = gameFacade.GetGameSubscription(331);
            //cybersourceAdaptor.CancelSubscription(ref bSystemDown, ref userErrorMessage, gameSubscription, 243731);
        }
    }
}
