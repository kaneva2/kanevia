///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using log4net;
using KlausEnt.KEP.Kaneva;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Developer
{
    public partial class ConfirmPurchase : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {

            }
            else
            {
                RedirectToLogin();
            }
            VerifyUserAccess();

            // Clear any previous errors
            lblErrors.Text = "";

            Response.Expires = 0;
            Response.CacheControl = "no-cache";

            if (!IsPostBack)
            {
                DisplayScreen();
            }

            // Set up purchase button																																																								  
            btnMakePurchase.CausesValidation = false;

            btnMakePurchase.Attributes.Add("onclick", "makePurchase();");
            // litPostBackRef.Text = ClientScript.GetPostBackEventReference (this.btnMakePurchase, "", false);


            //imgMakePurchase.Attributes.Add("onclick", "document.getElementById('" + btnMakePurchase.ClientID + "').click();");

            //string orderId = GetOrderId;
            //string orderId = "0";

            //aChangeAddress.HRef = ResolveUrl("~/checkout/billinginfo.aspx?orderId=" + orderId);
            //aPopUpEnterAddress.HRef = ResolveUrl("~/checkout/billinginfo.aspx?orderId=" + orderId);
            //aEditOrder.HRef = ResolveUrl("~/mykaneva/buyCredits.aspx?orderId=" + orderId);

        }


        #region Helper Methods

        /// <summary>
        /// DisplayScreen
        /// </summary>
        private void DisplayScreen()
        {
            //int userId = GetUserId();

            //if (GetOrderId == null)
            //{
            //    Response.Redirect("~/mykaneva/managecredits.aspx");
            //}

            //try
            //{
            //    // Get the order
            //    int orderId = GetOrderId;
            //    DataRow drOrder = StoreUtility.GetOrder(orderId, GetUserId());

            //    VerifyOrder(drOrder);

            //    int purchaseType = Convert.ToInt32(drOrder["purchase_type"]);

            //    // Where any K-Points purchased?
            //    int pointTransactionId = 0;
            //    Double dCurrentPointAmountPurchased = 0.0;

            //    // Get the billing info
            //    pointTransactionId = Convert.ToInt32(drOrder["point_transaction_id"]);
            //    DataRow drPpt = StoreUtility.GetPurchasePointTransaction(pointTransactionId);

            //    spnPrice.InnerText = Convert.ToDouble(drPpt["amount_debited"]).ToString();
            //    spnTotal.InnerText = spnPrice.InnerText;

            //    spnItemDescription.InnerText = drPpt["description"].ToString();
            //    dCurrentPointAmountPurchased = Convert.ToDouble(drPpt["totalPoints"]);

            //    // What is the payment method?
            //    int paymentMethodId = Convert.ToInt32(drPpt["payment_method_id"]);

            //    if (paymentMethodId.Equals((int)Constants.ePAYMENT_METHODS.PAYPAL))
            //    {
            //        radPmtPP.Checked = true;
            //        tblcc.Style["display"] = "none";
            //        spnAddress2.InnerText = "Address not required for Paypal";
            //    }
            //    else
            //    {
            //        radPmtCC.Checked = true;
            //        tblpp.Style["display"] = "none";
            //    }

            //    string noAddressScript = ";$('PopUpAddr').style.display='block';$('imgMakePurchase').style.display='none';";  //$('btnMakePurchase').disabled=true;";

            //    // Address
            //    if (drPpt["address_id"] != System.DBNull.Value)
            //    {
            //        int addressId = Convert.ToInt32(drPpt["address_id"]);
            //        DataRow drAddress = UsersUtility.GetAddress(userId, addressId);

            //        if (drAddress != null)
            //        {
            //            spnFullName.InnerText = drAddress["name"].ToString();
            //            spnAddress1.InnerText = drAddress["address1"].ToString();
            //            spnAddress2.InnerText = drAddress["address2"].ToString();
            //            spnCity.InnerText = drAddress["city"].ToString() + ", ";
            //            spnState.InnerText = drAddress["state_name"].ToString();
            //            spnZip.InnerText = drAddress["zip_code"].ToString();
            //            spnCountry.InnerText = drAddress["country_name"].ToString();
            //            spnPhone.InnerText = drAddress["phone_number"].ToString();

            //            aChangeAddress.InnerText = "Change Address";
            //            noAddressScript = "";
            //        }
            //    }


            //    radPmtCC.Attributes.Add("onclick", "javascript:$('tblcc').style.display='block';$('tblpp').style.display='none';" + noAddressScript);

            //    // Show rest of labels
            //    Double dUserKPointTotal = GetUserPointTotal();
            //    spnCreditBalance.InnerText = Convert.ToUInt64(dUserKPointTotal).ToString("N0");
            //    spnCreditPurchasing.InnerText = Convert.ToUInt64(dCurrentPointAmountPurchased).ToString("N0");
            //    spnNewCreditBalance.InnerText = Convert.ToUInt64(dUserKPointTotal + dCurrentPointAmountPurchased).ToString("N0");

            //    drpMonth.SelectedIndex = DateTime.Now.Month - 1;
            //}
            //catch (Exception exc)
            //{
            //    m_logger.Error("Error loading order information for order " + GetOrderId.ToString(), exc);
            //    ShowMessage("There was a problem loading the order	information.  Please try reloading this page or creating a new order.");
            //    return;
            //}
        }


        /// <summary>
        /// MakeCyberSourcePayment
        /// </summary>
        /// <param name="drOrder"></param>
        /// <param name="drPpt"></param>
        /// <returns></returns>
        private bool MakeCyberSourcePayment(DataRow drOrder, DataRow drPpt, string ccNumEncrypt, string ccSecCodeEncrypt, ref string userErrorMessage, ref string subscriptionId)
        {
            bool bSystemDown = false;
            CybersourceAdaptor cybersourceAdaptor = new CybersourceAdaptor();
            return cybersourceAdaptor.CreateSubscription(drOrder, drPpt, ccNumEncrypt, ccSecCodeEncrypt, Request.UserHostAddress, ref bSystemDown,
                ref userErrorMessage, ref subscriptionId, 1, 0, Subscription.SubscriptionTerm.None, Subscription.SubscriptionTerm.Monthly);
        }

        /// <summary>
        /// MakePayPalPayment
        /// </summary>
        /// <param name="drOrder"></param>
        /// <param name="drPpt"></param>
        /// <returns></returns>
        private bool MakePayPalPayment(int orderId, DataRow drPpt)
        {
            try
            {
                GameFacade gameFacade = new GameFacade();

                // Mark it as pending
                StoreUtility.UpdateOrderToPending(orderId);

                //// Get the license subscriptions
                GameSubscription gameSubscription = gameFacade.GetGameSubscriptionByOrderId(orderId);

                string itemName = Server.HtmlEncode("Star License Subscription");
                string itemNumber = Server.HtmlEncode("SLic-SUB" + gameSubscription.SubscriptionId + "-O" + orderId.ToString ());
                string amount = gameSubscription.GrossPointAmount.ToString("#0.00");

                // Reattemp? sra=1 means reattempt on failure based on paypal's rules, remove this if we DO NOT want to reattempt
                //string strReattemp = "&sra=1";  
                string strReattemp = "";
                string strAmount = "&a3=" + amount;
                //string strCycle = "&t3=" + StoreUtility.GetPayPalLengthCodes(Convert.ToInt32(drLicenseSubscription["length_of_subscription"]));
                string strCycle = "&t3=M"; // "M for monthly"

                string successURL = Server.HtmlEncode ("http://" + KanevaGlobals.SiteName + "/license/orderComplete.aspx");
                string failureURL = Server.HtmlEncode("http://" + KanevaGlobals.SiteName + "/license/paypalFailure.aspx");


                // no_note=1 means not promted for a note
                // p3=1 means length of each billing cycle
                // a3=10.00 means the subscription is 10 dollars
                // t3=M is monthy, t3=D is daily, W Weekly, Y Yearly
                // src=1 means it is a recurring subscription

                Response.Redirect(KanevaGlobals.PayPalURL + "/subscriptions/business=" + Server.HtmlEncode(KanevaGlobals.PayPalBusiness) + "&item_name=" + itemName + "&item_number=" + itemNumber + "&no_shipping=1&return=" + successURL + "&cancel_return=" + failureURL + "&no_note=1&currency_code=USD" + strAmount + "&p3=1" + strCycle + "&src=1" + strReattemp);

                // Cancel code
                // https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=test%40test222.com



                return true;
            }
            catch (Exception exc)
            {
                m_logger.Error("Error sending user to PayPal", exc);
                return false;
            }
        }

        /// <summary>
        /// ShowMessage
        /// </summary>
        /// <param name="strMessage"></param>
        private void ShowMessage(string strMessage)
        {
            lblErrors.Text = strMessage;
        }

        /// <summary>
        /// VerifyUserAccess
        /// </summary>
        private void VerifyUserAccess()
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
            }
        }

        /// <summary>
        /// VerifyOrder
        /// </summary>
        private void VerifyOrder()
        {
            if (Request["oId"] == null)
            {
                Response.Redirect("~/license/licenseOptions.aspx");
            }

            try
            {
                int orderId = Convert.ToInt32(Request["oId"]);
                VerifyOrder(StoreUtility.GetOrder(orderId, GetUserId()));
            }
            catch
            {
                Response.Redirect("~/license/licenseOptions.aspx");
            }
        }
        /// <summary>
        /// VerifyOrder
        /// </summary>
        private void VerifyOrder(DataRow drOrder)
        {
            bool err = false;

            try
            {
                // Verify user is owner of this order		  
                if (drOrder == null)
                {
                    // User may be trying to view someone else's order
                    m_logger.Warn("User " + GetUserId() + " tried to view orderId " + GetOrderId().ToString () + " from IP " + Request.UserHostAddress);
                    err = true;
                }

                if (Convert.ToInt32(drOrder["transaction_status_id"]) != (int)Constants.eORDER_STATUS.CHECKOUT)
                {
                    m_logger.Warn("User " + GetUserId() + " tried to view orderId " + GetOrderId().ToString() + " from IP " + Request.UserHostAddress + ".  Order does not have a status of CART.");
                    err = true;
                }

                //// Check to see if user is trying to purchase an access pass
                //DataRow drPointBucket = StoreUtility.GetPromotion(Convert.ToInt32(drOrder["point_bucket_id"]));
                //if (Convert.ToInt32(drPointBucket["wok_pass_group_id"]) > 0)
                //{
                //    // verify user is old enough to purchase an access pass
                //    if (!KanevaWebGlobals.CurrentUser.IsAdult)
                //    {
                //        m_logger.Warn("User " + GetUserId() + " tried to process orderId " + GetOrderId().ToString() + " from IP " + Request.UserHostAddress + ".  Order is for Adult Pass and user is UNDER 18.");
                //        err = true;
                //    }
                //}
            }
            catch
            {
                err = true;
            }

            if (err)
                Response.Redirect("~/license/licenseOptions.aspx");
        }

        /// <summary>
        /// AddPaymentDetails
        /// </summary>
        private void AddPaymentDetails(string ccNumLastFourDigits)
        {
            int orderId = GetOrderId();
            DataRow drOrder = StoreUtility.GetOrder(orderId);

            int pointTransactionId = Convert.ToInt32(drOrder["point_transaction_id"]);

            // Update the payment method
            //StoreUtility.UpdatePointTransaction (pointTransactionId, Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE);

            // Add the new credit card
            int billingInfoId = UsersUtility.InsertBillingInfo(GetUserId(),
                Constants.eBILLING_INFO_TYPE.PROFILE, "",
                Server.HtmlEncode(txtNameOnCard.Text),
                KanevaGlobals.Encrypt(Server.HtmlEncode(ccNumLastFourDigits)),
                Server.HtmlEncode(drpCardType.SelectedItem.Text),
                Server.HtmlEncode(drpMonth.SelectedValue), "",
                Server.HtmlEncode(drpYear.SelectedValue), 0);

            // Update purchase transaction
            StoreUtility.UpdatePointTransactionBillingInfoId(pointTransactionId, billingInfoId);
        }

        /// <summary>
        /// Back Screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, ImageClickEventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                RedirectToLogin();
                return;
            }

            Response.Redirect(ResolveUrl("~/license/licenseOptions.aspx?oid=" + GetOrderId()));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// The make purchase event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnMakePurchase_Click(object sender, EventArgs e)
        {
            VerifyUserAccess();
            VerifyOrder();

            try
            {
                // Get the order
                int orderId = GetOrderId ();
                DataRow drOrder = StoreUtility.GetOrder(orderId);
                int pointTransactionId = Convert.ToInt32(drOrder["point_transaction_id"]);

                // Get the payment type
                if (radPmtCC.Checked)
                {
                    Page.Validate();
                    if (!Page.IsValid)
                    {
                        return;
                    }

                    AddPaymentDetails(txtCardNumber.Text.Substring(txtCardNumber.Text.Length - 4));
                }
                else if (radPmtPP.Checked)
                {
                    StoreUtility.UpdatePointTransaction(pointTransactionId, Constants.ePAYMENT_METHODS.PAYPAL);
                }

                int userId = GetUserId();
                string userErrorMessage = "";

                int orderBillingInformationId = 0;
                int billingInformationId = 0;
                int addressId = 0;

                // They are purchasing credit
                DataRow drPpt = StoreUtility.GetPurchasePointTransaction(pointTransactionId);

                // Is it PayPal?
                int paymentMethodId = Convert.ToInt32(drPpt["payment_method_id"]);

                if (paymentMethodId.Equals((int)Constants.ePAYMENT_METHODS.PAYPAL))
                {
                    // PayPal
                    if (!MakePayPalPayment(orderId, drPpt))
                    {
                        ShowMessage("Please correct the following errors:<br><br>Error processing order for PayPal");
                        return;
                    }
                }
                else if (paymentMethodId.Equals((int)Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE))
                {
                    string ccNumEncrypt = KanevaGlobals.Encrypt(txtCardNumber.Text);
                    string ccSecCodeEncrypt = KanevaGlobals.Encrypt(txtSecurityCode.Text);

                    //                    AddPaymentDetails (txtCardNumber.Text.Substring (txtCardNumber.Text.Length - 4));

                    // Did we already copy the billing information over to history?
                    if (!drPpt["order_billing_information_id"].Equals(DBNull.Value))
                    {
                        orderBillingInformationId = Convert.ToInt32(drPpt["order_billing_information_id"]);
                    }

                    // Payment info and address are required here for Cybersource transactions				   
                    if (drPpt["address_id"].Equals(DBNull.Value) || drPpt["billing_information_id"].Equals(DBNull.Value))
                    {
                        ShowMessage("Please correct the following errors:<br><br>Credit card and billing address are required to continue this purchase");
                        return;
                    }

                    billingInformationId = Convert.ToInt32(drPpt["billing_information_id"]);
                    addressId = Convert.ToInt32(drPpt["address_id"]);

                    // Copy payment/billing address to order history
                    orderBillingInformationId = StoreUtility.CopyBillingInfoToOrder(orderBillingInformationId, billingInformationId, addressId);

                    // Update the purchase_transaction order_billing_id
                    StoreUtility.UpdatePointTransactionOrderBillingInfoId(pointTransactionId, orderBillingInformationId);

                    // Get it again to update the orderBillingInformationId to be used in the next call
                    drPpt = StoreUtility.GetPurchasePointTransaction(pointTransactionId);

                    string subscriptionId = "";

                    // CyberSource
                    if (!MakeCyberSourcePayment(drOrder, drPpt, ccNumEncrypt, ccSecCodeEncrypt, ref userErrorMessage, ref subscriptionId))
                    {
                        // Failure
                        ShowMessage("Please correct the following errors:<br><br>" + userErrorMessage);
                        return;
                    }

                    // Mark the subscription and license as active
                    GameFacade gameFacade = new GameFacade();
                    GameSubscription gameSubscription = gameFacade.GetGameSubscriptionByOrderId(orderId);
                    gameSubscription.GsStatusId = (int) GameSubscription.SubscriptionStatus.Active;
                    gameSubscription.SubscriptionIdentifier = subscriptionId;
                    gameFacade.UpdateGameSubscription(gameSubscription);

                    GameLicense gameLicense = gameFacade.GetGameLicenseByGameLicenseId (gameSubscription.GameLicenseId);
                    gameLicense.LicenseStatusId = (int)GameLicense.LicenseStatus.Active;
                    gameFacade.UpdateGameLicense(gameLicense);

                    // Successfull k-point purchase here
                    // XXX
                    //MailUtility.SendKpointPurchaseEmail(userId, orderId);

                    Response.Redirect(ResolveUrl("~/license/orderComplete.aspx?orderId=" + orderId), false);
                }
                else
                {
                    ShowMessage("<br/>Please correct the following errors:<br/><br/>Invalid Payment Method specified!");
                    return;
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error trying to process purchase order: ", exc);
                ShowMessage("<br/>An error occurred while trying to process your order.  Please retry your request.");
            }
        }

        private int GetOrderId()
        {
            if (Request["oid"] != null)
            {
                return Convert.ToInt32(Request["oid"]);
            }
            else
            {
                return 0;
            }
        }

        #endregion

        #region Declerations

        protected Label lblErrors;
        protected Literal litPostBackRef;

        protected Button btnMakePurchase;
        protected TextBox txtNameOnCard, txtCardNumber, txtSecurityCode;
        protected DropDownList drpMonth, drpYear;

        protected HtmlImage imgMakePurchase;
        protected HtmlAnchor aChangeAddress, aEditOrder, aPopUpEnterAddress;
        protected HtmlTable tblpp, tblcc;
        protected HtmlContainerControl spnFullName, spnAddress1, spnAddress2, spnCity, spnState, spnZip, spnCountry, spnPhone;
        protected HtmlContainerControl spnPrice, spnTax, spnTotal, spnItemDescription;
        protected HtmlContainerControl spnCreditBalance, spnCreditPurchasing, spnNewCreditBalance;
        protected HtmlInputRadioButton radPmtCC, radPmtPP;

        protected KlausEnt.KEP.Kaneva.WebControls.CardTypesListBox drpCardType;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
