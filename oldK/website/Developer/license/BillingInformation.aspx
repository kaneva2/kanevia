<%@ Page MasterPageFile="~/masterpages/MainTemplatePage.Master" Language="C#" AutoEventWireup="true" CodeBehind="BillingInformation.aspx.cs" Inherits="KlausEnt.KEP.Developer.BillingInformation" %>

<asp:Content ID="cnt_billingInfo" runat="server" ContentPlaceHolderID="cph_Body">

    <asp:validationsummary showmessagebox="False" showsummary="True" id="valSum" displaymode="BulletList" runat="server" headertext=""/>
    
    <div style="float:left;">
        <h1>Billing Information</h1><br/>
        Pleaes enter your billing information below<br/>

        <asp:CheckBox runat="server" ID="chkUseCompany" AutoPostBack="true" OnCheckedChanged="chkUseCompany_onCheckChanged"/> Use Company Address<br/><br/><br/><br/>

        Full Name*<asp:textbox id="txtFullName" class="biginput" style="width:455" maxlength="100" runat="server"/>
	    <asp:requiredfieldvalidator id="rfUsername" controltovalidate="txtFullName" text="" errormessage="Full Name is a required field." display="None" runat="server"/>
        <br/>
        
        Address 1*<asp:textbox id="txtAddress1" class="biginput" style="width:455px" maxlength="100" runat="server"/>
        <asp:requiredfieldvalidator id="rfEmail" controltovalidate="txtAddress1" text="" errormessage="Address1 is a required field." display="None" runat="server"/>
        <br/>

        Address 2<asp:textbox id="txtAddress2" class="biginput" style="width:455px" maxlength="80" runat="server"/>
        <br />
        
        City*<asp:textbox id="txtCity" class="biginput" style="width:200px" maxlength="100" runat="server"/>
	    <asp:requiredfieldvalidator id="rftxtCity" controltovalidate="txtCity" text="" errormessage="City is a required field." display="None" runat="server"/>
        <br />
    												
        State**<asp:dropdownlist runat="server" class="biginput" id="drpState" style="width:80px"></asp:dropdownlist>
        <asp:requiredfieldvalidator id="rfdrpState" controltovalidate="drpState" text="" errormessage="State is a required field when country is United States or Canada." display="None" runat="server"/>
        <br/>
        
        Zip Code**<asp:textbox id="txtPostalCode" class="biginput" style="width:100px" size="9" maxlength="25" runat="server"/>
        <asp:requiredfieldvalidator id="rftxtPostalCode" controltovalidate="txtPostalCode" text="" errormessage="Postal code is a required field." display="None" runat="server"/><img src="images/spacer.gif" height="1" width="15" />
	    <br/>
    												
        Country*<asp:dropdownlist runat="server" class="biginput" id="drpCountry" style="width: 300px;"></asp:dropdownlist>
	    <asp:requiredfieldvalidator id="rfdrpCountry" controltovalidate="drpCountry" text="" errormessage="Country is a required field." display="None" runat="server"/>
        <br/>

	    Phone Number*<asp:textbox id="txtPhoneNumber" class="biginput" style="width:200px" maxlength="15" runat="server"/>
        <asp:requiredfieldvalidator id="rftxtPhoneNumber" controltovalidate="txtPhoneNumber" text="" errormessage="Phone Number is a required field." display="None" runat="server"/>
	    <br/>
    	
        * Indicates Required Field.<br>** Indicates required fields for United States and Canada only.
        <br/><br/>
        
        <span style="font-weight:bold;">Using Paypal?</span><br /><asp:linkbutton runat="server" causesvalidation="false"  id="lbPaypal" onclick="lbPaypal_Click">Skip this step >></asp:linkbutton>
   </div>
								
    <div style="float:left;">
        <h1>Payment Options</h1><br/>
        We have several options for you to chose from.<br/>
    
		<table cellspacing="0" cellpadding="2" align="left" border="0">
		    <tr>
			    <td><input type="radio" runat="server" id="Radio1" name="paymentType" value="cc" checked />Credit Card</td>
			    <td style="padding-left:10px;"><img id="Img7" runat="server" src="http://www.kaneva.com/images/credit_card_icons.gif" alt="Kaneva accepts American Express, Discover/NOVUS, MasterCard, and Visa."  height="20" border="0"></td>
		    </tr>
		    <tr id="tr1" runat="server">
			    <td><input type="radio" runat="server" id="Radio2" name="paymentType" value="pp" />Paypal</td>
			    <td style="padding-left:10px;"><a href="#"><img  src="https://www.paypal.com/en_US/i/logo/PayPal_mark_37x23.gif" border="0" alt="Acceptance Mark"></a></td>
		    </tr>
	    </table>
	    
	    <br/><br/><br/>
	    You have selected 
	    <div style="border:solid 2 blue;background-color:Gray">
	        Our <asp:Label runat="server" ID="lblLicense"></asp:Label> License...
	        <asp:Label runat="server" ID="lblLicenseDesc"></asp:Label>
	        Lorem ipsum dolor sit amet, consectetuer
            adipiscing elit. Suspendisse ut purus. Suspendisse
            tristique. Cras et sem at odio porttitor mattis. Donec
            dignissim ornare tortor.
	    </div>
    
    </div>			
    
     <div style="clear:both"> 
        <asp:imagebutton id="Imagebutton1" CausesValidation="false" imageurl="~/images/Back.gif" runat="Server" onclick="btnBack_Click" alternatetext="Choose License" width="200" height="41" border="0"/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:imagebutton id="btnNext" CausesValidation="true" imageurl="~/images/Next.gif" runat="Server" onclick="btnNext_Click" alternatetext="Continue Purchase" width="200" height="41" border="0"/>
    </div>	
</asp:Content>