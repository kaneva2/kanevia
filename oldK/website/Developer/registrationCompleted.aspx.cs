///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Developer
{
    public partial class registrationCompleted : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string scriptString = "<script language=\"JavaScript\">alert(\"Success! Your contact information has been sent to Kaneva. \\n\\n Thank you for your interest in the Kaneva Star Platform. \" );" +
            "window.location = \"" + ResolveUrl(GetLandingPageLink()) + "\";</script>";
            
            ClientScript.RegisterStartupScript(this.GetType(), "confirmSubmission", scriptString);

            //Response.Redirect(GetLandingPageLink());
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion

    }

}
