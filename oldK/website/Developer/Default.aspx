<%@ Page Language="C#" MasterPageFile="~/masterpages/HomepageTest.Master" EnableViewState="False" AutoEventWireup="false" CodeBehind="Default.aspx.cs" Inherits="KlausEnt.KEP.Developer.Default" %>

<asp:Content ID="cnt_Header" runat="server" ContentPlaceHolderID="cph_Header" >
<div class="signIn">Already a Kaneva Member? <a id="aSignIn" runat="server">Sign In</a></div>
</asp:Content>

<asp:Content ID="cnt_mainHome" runat="server" ContentPlaceHolderID="cph_MainContent" >
    <div class="welcomeMessage">
        <h1>Create Your Own Game World</h1>
        <h2>Build and explore game worlds with your friends.</h2>
    </div>
    <div class="signupForm">
        <h2>Join today - it's free.</h2>
        <div>
            <label id="lblUserName" runat="server" for='<%=txtUserName.ClientID%>'>Username</label>
            <asp:textbox id="txtUserName" runat="server" maxlength="20" tabindex="5"></asp:textbox>
            <br class="clear" />
        </div>
        <div>
            <label id="lblFirstName" runat="server" for='<%=txtFirstName.ClientID%>'>First Name</label>
            <asp:textbox id="txtFirstName" runat="server" maxlength="50" tabindex="6"></asp:textbox>
            <br class="clear" />
        </div>
        <div>
            <label id="lblLastName" runat="server" for='<%=txtLastName.ClientID%>'>Last Name</label>
            <asp:textbox id="txtLastName" runat="server" maxlength="50" tabindex="7"></asp:textbox>
            <br class="clear" />
        </div>
        <div>
            <label id="lblEmail" runat="server" for='<%=txtEmail.ClientID%>'>Email</label>
            <asp:textbox id="txtEmail" runat="server" maxlength="100" tabindex="8"></asp:textbox> 
            <br class="clear" />
        </div>
        <div>
            <label id="lblPassword" runat="server" for='<%=txtPassword.ClientID%>'>Password</label>
            <asp:textbox id="txtPassword" runat="server" maxlength="20" tabindex="9" TextMode="Password"></asp:textbox>
            <br class="clear" />
        </div>
        <div>
            <label id="lblGender" runat="server" for='<%=drpGender.ClientID%>'>I am</label>
            <asp:dropdownlist id="drpGender" runat="server" tabindex="10" class="genderSelect">
				<asp:listitem value="">Select Sex</asp:listitem>
				<asp:listitem value="Male">Male</asp:listitem>
				<asp:listitem value="Female">Female</asp:listitem>
			</asp:dropdownlist>
            <br class="clear" />
        </div>
        <div>
            <label id="lblBirthday" runat="server" for='<%=drpMonth.ClientID%>'>Birthday</label>
            <asp:dropdownlist id="drpMonth" runat="server" tabindex="11" class="monthSelect">
				<asp:listitem value="">Month</asp:listitem>
				<asp:listitem value="1">January</asp:listitem>
				<asp:listitem value="2">February</asp:listitem>
				<asp:listitem value="3">March</asp:listitem>
				<asp:listitem value="4">April</asp:listitem>
				<asp:listitem value="5">May</asp:listitem>
				<asp:listitem value="6">June</asp:listitem>
				<asp:listitem value="7">July</asp:listitem>
				<asp:listitem value="8">August</asp:listitem>
				<asp:listitem value="9">September</asp:listitem>
				<asp:listitem value="10">October</asp:listitem>
				<asp:listitem value="11">November</asp:listitem>
				<asp:listitem value="12">December</asp:listitem>
			</asp:dropdownlist>
            <asp:dropdownlist id="drpDay" runat="server" tabindex="12" class="daySelect">
				<asp:listitem value="">Day</asp:listitem>
				<asp:listitem value="1">01</asp:listitem>
				<asp:listitem value="2">02</asp:listitem>
				<asp:listitem value="3">03</asp:listitem>
				<asp:listitem value="4">04</asp:listitem>
				<asp:listitem value="5">05</asp:listitem>
				<asp:listitem value="6">06</asp:listitem>
				<asp:listitem value="7">07</asp:listitem>
				<asp:listitem value="8">08</asp:listitem>
				<asp:listitem value="9">09</asp:listitem>
				<asp:listitem value="10">10</asp:listitem>
				<asp:listitem value="11">11</asp:listitem>
				<asp:listitem value="12">12</asp:listitem>
				<asp:listitem value="13">13</asp:listitem>
				<asp:listitem value="14">14</asp:listitem>
				<asp:listitem value="15">15</asp:listitem>
				<asp:listitem value="16">16</asp:listitem>
				<asp:listitem value="17">17</asp:listitem>
				<asp:listitem value="18">18</asp:listitem>
				<asp:listitem value="19">19</asp:listitem>
				<asp:listitem value="20">20</asp:listitem>
				<asp:listitem value="21">21</asp:listitem>
				<asp:listitem value="22">22</asp:listitem>
				<asp:listitem value="23">23</asp:listitem>
				<asp:listitem value="24">24</asp:listitem>
				<asp:listitem value="25">25</asp:listitem>
				<asp:listitem value="26">26</asp:listitem>
				<asp:listitem value="27">27</asp:listitem>
				<asp:listitem value="28">28</asp:listitem>
				<asp:listitem value="29">29</asp:listitem>
				<asp:listitem value="30">30</asp:listitem>
				<asp:listitem value="31">31</asp:listitem>
			</asp:dropdownlist>
            <asp:dropdownlist id="drpYear" runat="server" tabindex="13" class="yearSelect">
				<asp:listitem value="">Year</asp:listitem>
			</asp:dropdownlist>
            <br class="clear" />
        </div>
        <div>
            <label id="lblCountry" runat="server" for='<%=drpCountry.ClientID%>'>Country</label>
            <asp:dropdownlist id="drpCountry" runat="server" tabindex="14" class="countrySelect"></asp:dropdownlist>
            <br class="clear" />
        </div>
        <div id="divPostalCode" runat="server">
            <label id="lblPostalCode" runat="server" for='<%=txtPostalCode.ClientID%>'>Zip Code</label>
            <asp:textbox id="txtPostalCode" runat="server" maxlength="25" tabindex="15" class="postalCode"></asp:textbox>
            <br class="clear" />
        </div>
        <div>
            <asp:linkbutton id="btnRegister" runat="server" alternatetext="Start Now!" tabindex="15" CssClass="startNow"></asp:linkbutton>
        </div>
        <div>
            <p class="terms">
                By clicking Next Step, you agree to<br /> our <a class="footlink" onclick="javascript:window.open('http://www.kaneva.com/overview/TermsAndConditions.aspx','TandC','width=500,height=400,resizable=yes,scrollbars=yes');false;"
						    href="#">terms</a> and <a class="footlink" onclick="javascript:window.open('http://www.kaneva.com/overview/privacy.aspx','Priv','width=500,height=400,resizable=yes,scrollbars=yes');false;"
						    href="#">privacy policy</a>.
            </p>
        </div>
        <input type="hidden" id="txtUserNameUID" name="txtUserNameUID" value='<%=txtUserName.UniqueID%>' />
        <input type="hidden" id="txtFirstNameUID" name="txtFirstNameUID" value='<%=txtFirstName.UniqueID%>' />
        <input type="hidden" id="txtLastNameUID" name="txtLastNameUID" value='<%=txtLastName.UniqueID%>' />
        <input type="hidden" id="txtEmailUID" name="txtEmailUID" value='<%=txtEmail.UniqueID%>' />
        <input type="hidden" id="txtPasswordUID" name="txtPasswordUID" value='<%=txtPassword.UniqueID%>' />
        <input type="hidden" id="drpGenderUID" name="drpGenderUID" value='<%=drpGender.UniqueID%>' />
        <input type="hidden" id="drpDayUID" name="drpDayUID" value='<%=drpDay.UniqueID%>' />
        <input type="hidden" id="drpMonthUID" name="drpMonthUID" value='<%=drpMonth.UniqueID%>' />
        <input type="hidden" id="drpYearUID" name="drpYearUID" value='<%=drpYear.UniqueID%>' />
        <input type="hidden" id="drpCountryUID" name="drpCountryUID" value='<%=drpCountry.UniqueID%>' />
        <input type="hidden" id="txtPostalCodeUID" name="txtPostalCodeUID" value='<%=txtPostalCode.UniqueID%>' />
        <input type="hidden" id="postFromHome" name="postFromHome" value="1" />
    </div>
   <div class="clear"><!-- clear the floats --></div>
</asp:Content>
<asp:Content ID="cnt_seoHome" runat="server" ContentPlaceHolderID="cph_SEO" >
    <div class="seo" id="seo" runat="server" style="display:none">
        <br />
        <h1 class="seo">Create Your Own Game World</h1>
        <br />
	    <p>
            Kaneva allows millions of people to explore, interact and create their own worlds. You can make a virtual
            world or a 3D MMO Game World. Explore a large number of Worlds made by our community and have fun with friends. Bring your imagination and
            step into your own World on Kaneva.
        </p>
        <br /><a id="aFooterRegister" runat="server">Join Kaneva</a>
    </div>
</asp:Content>