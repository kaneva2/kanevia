﻿<%@ Page Language="C#" MasterPageFile="~/masterpages/AppBrochureTemplatePage.Master" AutoEventWireup="true" CodeBehind="download.aspx.cs" Inherits="Developer.download" %>

<asp:Content ID="cntProfileHead" ContentPlaceHolderID="cphHeadData" runat="server">
 	<meta name="title" content="Kaneva. Make your own 3D Games." /> 
 	<meta name="description" content="Kaneva - Create your avatar and make your own 3D Games!"/> 
 	<meta name="keywords" content="computer game making, video game creator, game designing programs, design a videogame online, making a computer game, make own videogame, 3d game development, programming a game, 3d engine game, online game developer, 3d game design"/>  
 	<NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>
    <link href="css/Brochure/Default.css" type="text/css" rel="stylesheet" />  
    <script type="text/javascript" src="jscript/prototype.js"></script>
    <script type="text/javascript" src="jscript/Brochure/Default.js"></script>
<!-- Google Website Optimizer Tracking Script -->
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['gwo._setAccount', 'UA-5755114-4']);
	_gaq.push(['gwo._trackPageview', '/4177057029/goal']);
	(function () {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
<!-- End of Google Website Optimizer Tracking Script -->

<!-- Google Code for Dev Download Page Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1065938872;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "byxWCNi4mwIQuN-j_AM";
var google_conversion_value = 0;
if (1.00) {
  google_conversion_value = 1.00;
}
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1065938872/?value=1.00&amp;label=byxWCNi4mwIQuN-j_AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">

	<div id="download">
		
		<div class="header">
			<span>You're minutes away from creating a 3D App on Kaneva…</span>
			<div>Your download should automatically start in a few seconds. If it doesn't, <a id="downloadSetup" href="http://patch.kaneva.com/patchdata/Developer/Kaneva3DAppSetup.exe" onclick="javascript:_gaq.push(['_trackPageview', '/download/Kaneva3DAppSetup']);">try again</a>.</div>
		</div>

		<div class="clear"></div>

		<div id="install">
			<div class="step">
				<div class="imgcontainer"><img src="images/leavebehind_illus_01_215x132.jpg" /></div>
				<span>1.</span> Double-click on the setup file to  start he Kaneve for Developers installation.
			</div>
			<div class="step">
				<div class="imgcontainer"><img src="images/leavebehind_illus_02_216x120.jpg" /></div>
				<span>2.</span> Click Yes to accept the Kaneva setup program to run (if User Account Control is enabled).
			</div>
			<div class="step">
				<div class="imgcontainer"><img src="images/leavebehind_illus_03_215x167.jpg" /></div>
				<span>3.</span> Follow the setup instruction to install Kaneva for Developers.
			</div>
			<div class="step">
				<div class="imgcontainer"><img src="images/leavebehind_illus_04_214_187.jpg" /></div>
				<span>4.</span> After installation is complete, select create 3D App to get started.
			</div>
			<div id="linky"><a href="http://docs.kaneva.com/mediawiki/index.php/3DAPP:Install_Guide" target="_blank">Need help with installation or setup? Click Here</a></div>
		</div>
		
		<div id="downloadSummary">

			<div id="downloadInfoArea">
				<div class="downloadInfoBlock" style="text-align:left;">
					You're on your way to making casual, social games and apps in an immersive virtual world. 
					The Kaneva 3D App Platform allows you to quickly and easily implement social features in 
					your apps with a simple, visual development tool and the power of LUA scripting.    
				</div>       
				<div class="downloadInfoBlock secondblock" style="text-align:left;">
						<p>While the Kaneva 3D App Server is installing,<br />
						feel free to browse our developer documentation and examples.</p>
						<div id="getstartedlisting">
						<a href="http://docs.kaneva.com/mediawiki/index.php/Main_Page"><img src="images/creatorresources.png" width="363" height="50" boirder="0" /></a>
					</div>
				</div>       
			</div>								

		</div>

	</div>

<script type="text/javascript">
	window.onload = function () {
		_gaq.push(['_trackPageview', '/download/Kaneva3DAppSetup']);
		document.location = 'http://patch.kaneva.com/patchdata/Developer/Kaneva3DAppSetup.exe';
	}
</script>

<style type="text/css">
#menu div {display:none;}
</style>
</asp:Content>