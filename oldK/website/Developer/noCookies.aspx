<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="false" CodeBehind="noCookies.aspx.cs" Inherits="KlausEnt.KEP.Developer.noCookies" %>
	
<asp:Content ID="cnt_noCookies" runat="server" ContentPlaceHolderID="cph_Body">	
<table cellpadding="0" cellspacing="1" style="float:left; text-align:left" border="0" width="700" >
	<tr>
	<td style="padding-left:20px"><br><br>
	<b style="font-size: 14px; ">You are seeing this page because your internet browser currently does not allow cookies. 
		developer.kaneva.com requires an Internet browser setting that allows cookies.</B>
		<BR><BR>
		Cookies are small text files stored on your computer that tell 
		developer.kaneva.com when you're signed in. To learn how to allow cookies, see online help in your web browser.
		<BR><BR>
		Once you have set your browser to allow cookies, close it, open a new browser 
		window and return to <A href="http://developer.kaneva.com/">http://developer.kaneva.com</A> to continue your digital entertainment experience.<br><br><br>

	</td>
	</tr>
</table>
</asp:Content >