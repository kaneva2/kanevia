///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Developer
{
    public partial class kaneva_star_platform : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //set Main Navigation
            Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV] = usercontrols.Navigation.TAB.MOTIVATION;
            //set 2ndary nav
            Session[DeveloperCommonFunctions.ACTIVE_SUB_NAV] = usercontrols.SubNavMotivation.TAB.WHY;

            if (!Page.IsPostBack)
            {
                //set the metadata
                ((MainTemplatePage)Master).Title = "The Kaneva Star Platform | Your Branded Virtual Experience ";
                ((MainTemplatePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"kaneva star platform, star builder, 3d star system, star server, star editor, star explorer \">";
                ((MainTemplatePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Imagine that from the moment you hang your first shingle in this universe, you�re part of a bustling, vibrant community where business happens, and people from all across the globe see eye to eye. With The Kaneva Star Platform, builders benefit immediately from being part of a broader ecosystem of 3D experiences. \">";
            }

        }
    }
}
