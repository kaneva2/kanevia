<%@ Page Language="C#" MasterPageFile="~/masterpages/AppBrochureTemplatePage.Master" AutoEventWireup="true" CodeBehind="Kaneva-3D-Apps-Alpha-Festival.aspx.cs" Inherits="Developer.info.Kaneva_3D_Apps_Alpha_Festival" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeadData" runat="server">
    <meta name="title" content="Kaneva. The Online Community and Social Network." />
    <meta name="description" content="Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!"/>
    <meta name="keywords" content="free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva" />
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="../noJavascript.aspx" /></NOSCRIPT>
    <link href="../css/Brochure/Default.css" type="text/css" rel="stylesheet" />  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
	<br />
	<img src="../images/Brochure/Events/Alpha/header_contest_alpha_895x243.jpg" />
	<br />
	
	<div id="overview2" >
	
	<div id="rightSideBar">
		<div id="rightSideBarBox1">
		<h2 class="developerh2">Learn more...</h2>
		<p>about creating games & content on the Kaneva Development Platform!</p>
		<div id="dialogbig">
		<a href="3dapps_registration_form.pdf"><span>Register Today! >></span></a>
		</div>
		<p><a href="../default.aspx">Learn more about the Kaneva 3D App Developer program</a></p>
			
		</div>
		<!-- 
		<div id="rightSideBarBox2">
			Sponsors Here.<br />
			<br /><br /><br /><br /><br /><br /><br />
		</div>
		-->
	</div>
	
	
    <h2>The Kaneva 3D Apps Alpha Festival</h2>
    <p>The Kaneva 3D Apps Alpha Festival is an invitation only program for game developers to participate 
    in the newly created 3D Apps feature for the World of Kaneva. These developers will have a "first in" 
    opportunity to work with Kaneva to publish and market their 3D Apps. The contest will allow independent 
    game developers to test their creative prowess through their 3D Apps using the Kaneva Platform.</p>  
    
    <h2>Description</h2> 
    <p>The 3D Apps Alpha Festival will feature the sample applications to be included with the 3D Apps 
    product commercial release. This contest is targeted to assist with flushing out the issues with 
    the new 3D Apps product. The contest will be marketed to a small set of existing World of Kaneva 
    members as well as potential 3D App developers who would likely participate in the commercial 
    release of the product. Feedback from the participants will be used to provide a quality experience 
    for the production release in April.</p>   
    
    <h2>Goal</h2> 
    <p>The goal of the Alpha 3D Apps Festival is to incent 
    potential 3D App developers to modify the sample templates provided with the core 3D Apps product 
    to develop and publish 3D App Casinos.</p>     
    
    <h2>Timeline</h2>
	<p>The contest is live and ends on April 14th, 2010.</p>   
    
    <h2>Winning 3D App</h2> 
    <p>The winning 3D Apps will be announced at the Official 3D Apps Launch event to be 
    held at the Kaneva Offices.</p>   
    <strong>Location</strong>
    <table>
    <tr>        
		<td>Place:</td>
		<td>Kaneva HQ, The Palisades, Main conference room 
		5901B Peachtree Dunwoody, Atlanta, GA 30328</td>
	</tr>		
    <tr><td>Date:</td> 
		<td>April 15, 2010</td></tr>
    <tr><td>Time:</td> 
		<td>6:00pm � 9:00pm</td></tr>  
    <tr><td>Agenda:</td>
		<td>6:00pm � 7:00pm Networking<br />
		7:00pm � 8:00pm Presentation on 3D Apps<br />		
		8:00pm � 9:00pm Q&A and Demos<br />		
		8:00pm � 9:00pm Celebrate</td></tr>
    </table>
    
    <h2>Prizes</h2> 
    <p>The following prizes ($ 1,500 Total) will be awarded to the 
    top two (2) 3D Apps as rated by their total score:</p>  	
	<ul>
		<li>First Place = $ 1,000</li>
		<li>Second Place = $ 500</li>
	</ul>
  
    
    <h2>3D Apps Contest Rating and Judging</h2>
    <p>The 3D Apps will be judged and 
    ranked by Kaneva Staff according to the following criteria:</p>  	
    <ul>
		<li>20 Points � Category Fit</li>	
		<li>20 Points � Game Play</li> 	
		<li>20 Points � Look and Feel</li> 	
		<li>20 Points � Originality</li>	
		<li>20 Points � Fun!</li>
	</ul> 
    <p>Each 3D App can be awarded up to 100 Points.</p>
      
    <strong>Rules</strong><br />
    The following rules will govern the contest: 
    <ul>
    <li>Must be 18 years of age or older to 
    participate. (Or have a legal guardian approve and agree to the terms of this contest.)</li> 
    <li>Each participant must complete an official entry registration form and email it to: 
    <a href="mailto:contest@kaneva.com">contest@kaneva.com</a></li> 
    <li>Developers must create a Kaneva account and fully register with the 3D Apps website and comply 
    with all the Kaneva terms and conditio ns, policies, including the Developer Principles and 
    Policies that govern 3D Apps within Kaneva.</li> 
    <li>Employees of Kaneva are not eligible.</li> 
    <li>3D Apps must be mad he World of Kaneva.</li> 
    <li>Kaneva reserves the right to disqualify the 3D App 
    does not comply with the Developer Principles and Policies.</li> 
    <li>A minimum of 5 participants is required for any prizes to be awarded.</li> 
    <li>Kaneva reserves the right to modify this contest at any time.</li> 
    <li>To qualify for any prizes or awards, all registration forms must be received by the close of 
    business on March 31st, 2010. Contest ends on April 14th, 2010.</li> 
    <li>Developers must enhance the sample template scripts of 3D Apps provided as the basis for 
    their new 3D App experience.</li> 
	</ul>
	
</asp:Content>