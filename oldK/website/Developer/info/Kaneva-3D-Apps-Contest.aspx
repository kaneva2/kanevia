<%@ Page Language="C#" MasterPageFile="~/masterpages/AppBrochureTemplatePage.Master" AutoEventWireup="true" CodeBehind="Kaneva-3D-Apps-Contest.aspx.cs" Inherits="KlausEnt.KEP.Developer.Kaneva_3D_Apps_Contest" Title="Kaneva Developers - 3D App Contest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeadData" runat="server">
    <meta name="title" content="Kaneva. The Online Community and Social Network." />
    <meta name="description" content="Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!"/>
    <meta name="keywords" content="free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva" />
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="../noJavascript.aspx" /></NOSCRIPT>
    <link href="../css/Brochure/Default.css" type="text/css" rel="stylesheet" />  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
	<style>h2 {margin-top:26px;}</style>
	<div id="container" style="text-align:center;">
	
	<center>
	<div id="contest" style="text-align:left;width:910px;" >
	
	<div style="margin:20px 0;"><img src="../images/devchallenge_banner_921x248.jpg" width="921" height="248" /></div>
	
	
	<p>
	<h2>Enter the Kaneva Developer Challenge for a chance to win</h2>	
	We're looking for the next great 3D casual game our users will love and invite all their friends 
	to play. The most innovative, social, and fun games will be eligible for a total of $1,750 cash 
	prizes and be showcased throughout Kaneva's 3D Virtual World. 
	</p>
	<p>Ready to start developing the winning entry?</p>
	
	<div id="download" style="text-align:left;">
	&nbsp;<strong>Download Kaneva for Developers</strong><br />
	<a runat="server" id="aDownload" href="http://patch.kaneva.com/patchdata/Developer/Kaneva3DAppSetup.exe"><img src="../images/freedownload_257x50.png" width="257" height="50" border="0" /></a>
    </div>

    <p>We've even started you off with an easy-to-modify template which includes all of the social 
	features which will make your game even more exciting, including leaderboards, gifting, invite/share 
	with friends, in-app premium item purchases, custom animations, and more.
	</p>
	<p><a id="a1" target="_blank" href="http://docs.kaneva.com/mediawiki/index.php/Default_Template:_Beginner%27s_Guide">Read the tutorials/walkthroughs</a> 
	to see how easy it is to make games for the Developer Challenge.</p>
	
	<p>
	<h2>Awards</h2>
	The top 3 Developer Challenge entries are eligible for the following prizes:</p>
	<ul>
		<li>1st prize: $1,000</li>	
		<li>2nd prize: $500</li> 	
		<li>3rd prize: $250</li>
    </ul> 

    <p><i>Note: Winner is solely responsible for any tax that may arise from acceptance of the prize. 
	Independent financial advice should be sought by Winners themselves. Kaneva accepts no responsibility 
	in this regard.</i></p>
	

	<p>
	<h2>Game Entry Specification</h2>
	Games published between October 1st and November 30th are automatically entered into 
	the Kaneva Developer Challenge.</p>
	<p>Entries into the Kaneva Developer Challenge should begin with the Default Template 
	included with the Kaneva For Developers download.</p>
	<p>The default template includes a sample coin collection game which you are free to modify for your 
	entry. You can add additional game mechanics including timers, new rule sets, new items, complete 
	game environments, whatever can imagine and create.</p>
	<p>You can also submit an alternate game format, as long as you implement the following 
	features, which are included in the default template:</p>
	<ul>
		<li>Welcome screen</li>
		<li>Leaderboards</li>
		<li>Badges/Achievements</li>
		<li>Premium Items</li>
		<li>Invite Friends</li>
		<li>Blast Achievements</li>
	</ul>
	<p>For more information and tips on game creation, refer to the Developer Challenge Resources section below.</p>


	<p>
	<h2>Judging</h2>
	Kaneva will judge all entries based on gameplay, popularity, engagement, and use of social components included 
	in the default template.</p>


	<h2>Timeline</h2>
	<ol>
		<li>Oct 1: Competition starts </li>
		<li>Nov 30: Final submission date (submissions close on EST 8:00P) </li>
		<li>Dec 1-Dec 9: Private/People's Choice judging </li>
		<li>Dec 12: Winners announced</li>
	</ol>
	&nbsp;&nbsp;<i>Note: Kaneva reserves the right to postpone or change the date on the website without individual notice.</i>


	<p>
	<h2>Developer Challenge Resources</h2>
	We have extensive information on our tools and resources to help you on our <a id="aWiki" target="_blank" runat="server">Developer Wiki</a>.</p>
	<p>Have additional questions?  Join the <a id="aForums" target="_blank" runat="server">Kaneva 3D Apps Forums</a> and ask!</p>


	<p>
	<h2>Eligibility</h2>
	The Kaneva Developer Challenge is open to all submissions which were not already publicly 
	available through the Kaneva 3D App directory prior to October 1, 2011.</p>
	<p>Upon publishing your entry, your app will be made available for free during the entirety of 
	the contest. Premium functions may be enabled and supported after judging concludes and 
	winners are announced.</p>
	<p>Additional Eligibility Requirements:</p>
	<ul>
		<li>A 3D game can only win once. A 3D Developer can repeatedly win with different games.</li>
		<li>All games/apps submitted must be in English.</li>
		<li>All games/apps must be submitted to Kaneva, published, and publicly available by the November 30th deadline.</li>
		<li>A Kaneva account with a valid email address is necessary for winner notification.</li>
		<li>Entrants must be 18 years of age or older to participate (or have a legal guardian approve and agree to the terms of this contest.)</li>
		<li>Kaneva reserves the right to modify the Contest at any time; including but not limited to, extending the time, updating the rules and/or modifying the prizes to be awarded on completion of the contest.</li>
		<li>Kaneva reserves the right to disqualify any entry if the 3D App does not comply with the Developer Principles and Policies.</li>
		<li>Employees of Kaneva are not eligible.</li>
		<li>Void where prohibited by law.</li>
	</ul>
	  
    </div>
	</center>

	</div>
	<br /><br />
</asp:Content>