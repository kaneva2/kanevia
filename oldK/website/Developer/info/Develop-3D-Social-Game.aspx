<%@ Page Language="C#" MasterPageFile="~/masterpages/AppBrochureTemplatePage.Master" AutoEventWireup="true" CodeBehind="Develop-3D-Social-Game.aspx.cs" Inherits="Developer.info.Develop_3D_Social_Game" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeadData" runat="server">
    <meta name="title" content="Kaneva. The Online Community and Social Network." />
    <meta name="description" content="Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!"/>
    <meta name="keywords" content="free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva" />
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="../noJavascript.aspx" /></NOSCRIPT>
    <link href="../css/Brochure/Default.css" type="text/css" rel="stylesheet" />  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
	<br />
	<img src="../images/Brochure/dev_develop_spot_235x188.jpg" width="895" height="174"  />
	<br />
	
	<div id="overview">
	<img src="../images/Brochure/dev_develop_spot_235x188.jpg" width="200" height="300" style="float:right;" />
    <h1>Casual 3D Social Games & Entertainment Made Easy</h1>
    <h2>Develop Your Own Casual 3D Social Game</h2>  
      
    <p>Start developing your 3D game application right away. Access the resource center and 
    discover videos, sample LUA scripts, technical information, and much more.</p>   
    
    <p>Let the Kaneva game developer and player community become your official game testers.  
    They can provide real time feedback and assist with fine tuning your 3D social game experience.</p> 
           
    <p>Leverage Kaneva�s social network by using powerful viral marketing features to build your 
    community and enable revenue opportunities and microtransactions for your 3D social games and entertainment.</p>
    
    <p>The Kaneva 3D game platform provides a fully immersive casual 3d game engine for you to quickly and easily 
    develop and publish low-cost social interactive casual 3D games and virtual experiences.</p>
	
	<div id="dialogbig" >
		<a href="../learn/develop.aspx"><span>Next >></span></a>									
	</div>
	
    </div>
	
	
</asp:Content>
