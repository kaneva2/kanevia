<%@ Page Language="C#" MasterPageFile="~/masterpages/AppBrochureTemplatePage.Master" AutoEventWireup="true" CodeBehind="Kaneva-3D-Apps-Launch-Contest.aspx.cs" Inherits="Developer.info.Kaneva_3D_Apps_Launch_Contest" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeadData" runat="server">
    <meta name="title" content="Kaneva. The Online Community and Social Network." />
    <meta name="description" content="Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!"/>
    <meta name="keywords" content="free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva" />
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="../noJavascript.aspx" /></NOSCRIPT>
    <link href="../css/Brochure/Default.css" type="text/css" rel="stylesheet" />  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
	<br />
	<img src="../images/Brochure/Events/Casino/header_contest_casinonight_895x243.jpg"  />
	<br />
	
	<div id="overview2" >
	
	<div id="rightSideBar">
		<div id="rightSideBarBox1">
		<h2 class="developerh2">Learn more...</h2>
		<p>about creating games & content on the Kaneva Development Platform!</p>
		<div id="dialogbig">
		<a href="3dapps_registration_form.pdf"><span>Register Today! >></span></a>
		</div>
		<p><a href="../default.aspx">Learn more about the Kaneva 3D App Developer program</a></p>
			
		</div>
		<!-- 
		<div id="rightSideBarBox2">
			Sponsors Here.<br />
			<br /><br /><br /><br /><br /><br /><br />
		</div>
		-->
	</div>
	
	
    <h2>Kaneva 3D App Game Launch Contest</h2>
    <p>The Kaneva 3D App Game Launch Contest is a program for game developers to participate in the newly created 3D Apps feature 
    for the World of Kaneva.  These developers will have a "first in" opportunity to work with Kaneva to develop, play test and 
    publish their 3D Apps. The contest will allow independent game developers to test their creative prowess through their 3D Apps 
    using the Kaneva Platform.</p>  
    
    <h2>Description</h2> 
    <p>The 3D App Game Launch Contest will feature the sample script templates included with the 3D Apps product release.  
    The contest will be marketed to existing World of Kaneva members as well as potential 3D App developers who would likely 
    participate in the commercial release of the product.  Developers will have approximately 60 days to assemble their 
    3D App Casino.  The top five (5) 3D Apps will be awarded prizes at a grand finale event.</p>   
    
    <h2>Goal</h2> 
    <p>The goal of the 3D Apps Game Contest is to incent potential 3D App game developers to modify the sample templates 
    provided with the core 3D Apps product to develop and publish 3D App Casinos. </p>     
    
    <h2>Timeline</h2>
	<p>The contest will begin on April 15th and ends on June 17th, 2010.</p>   
    
    <h2>Winning 3D App</h2> 
    <p>The contest will be announced at the Launch event to be held at the Kaneva Offices.  
    The launch event will be held at the following location:</p>   
    
    <strong>Launch Event - in conjunction with the IGDA</strong>
    <dl>
    <dt><strong>Place:</strong></dt> 
		<dd>Kaneva HQ, The Palisades, Main conference room</dd> 
		<dd>5901B Peachtree Dunwoody, Atlanta, GA 30328</dd> 
    <dt><strong>Date:</strong></dt> 
		<dd>April 15, 2010</dd>
    <dt><strong>Time:</strong></dt> 
		<dd>6:00pm � 9:00pm</dd>  
    <dt><strong>Agenda:</strong></dt>
		<dd>6:00pm � 7:00pm Networking</dd>
		<dd>7:00pm � 8:00pm Presentation on 3D Apps</dd>		
		<dd>8:00pm � 9:00pm Q&amp;A and Demos</dd>			
    </dl>
    
    <strong>Grand Finale Event</strong>
    <dl>
    <dt><strong>Place:</strong></dt> 
		<dd>TBD</dd> 
    <dt><strong>Date:</strong></dt> 
		<dd>June 17, 2010</dd>
    <dt><strong>Time:</strong></dt> 
		<dd>6:00pm � 9:00pm</dd>  
    <dt><strong>Agenda:</strong></dt>
		<dd>6:00pm � 7:00pm Networking</dd>
		<dd>7:00pm � 8:00pm Presentation of Awards & Demo of Top 3D Games</dd>		
		<dd>8:00pm � 9:00pm Celebrate</dd>			
    </dl>
    
    
    <h2>Prizes</h2> 
    <p>The top 3D Apps, as awarded by the judges, can win over $10,000 in cash and prizes.  
    The following prizes may be awarded to the top five (5) 3D Apps as rated by their total score:</p>  	
	<ul> 
    	<li>First Place = $5,000 Value</li>
    	<li>Second Place = $2,500 Value</li>
    	<li>Third = $1,250 Value</li>
    	<li>Forth = $500 Value</li>
    	<li>Fifth = $250 Value</li>
    	<li>Others = Special Prizes TBD</li>
    </ul>
    
    <h2>3D Apps Contest Rating and Judging</h2>
    <p>The 3D Apps will be judged and 
    ranked by Kaneva Staff according to the following criteria:</p>  	
    <ul>
		<li>20 Points � Category Fit</li>	
		<li>20 Points � Game Play</li> 	
		<li>20 Points � Look and Feel</li> 	
		<li>20 Points � Originality</li>	
		<li>20 Points � Fun!</li>
	</ul> 
    <p>Each 3D App Game can be awarded up to 100 Points.  In addition, the Kaneva Staff will review and consider 
    the feedback from the community with regard to the quality, game play, originality and fun.</p>
      
    <strong>Rules</strong><br />
    The following rules will govern the contest: 
    <ul>
    <li>Must be 18 years of age or older to 
    participate. (Or have a legal guardian approve and agree to the terms of this contest.)</li> 
    <li>Each participant must complete an official entry registration form and fax (770-352-0077) or email it to: 
    <a href="mailto:contest@kaneva.com">contest@kaneva.com</a></li> 
    <li>Developers must create a Kaneva account and fully register with the 3D Apps website and comply 
    with all the Kaneva terms and conditio ns, policies, including the Developer Principles and 
    Policies that govern 3D Apps within Kaneva.</li> 
    <li>Employees of Kaneva are not eligible.</li> 
    <li>3D Apps must be made available for members of the World of Kaneva to experience and rate.</li> 
    <li>Kaneva reserves the right to disqualify the 3D App 
    does not comply with the Developer Principles and Policies.</li>     
	<li>Kaneva reserves the right to modify the Launch Contest at any time; including but not limited to, extending the time,
	updating the rules and/or modifying the prizes to be awarded on completion of the contest.</li> 
	<li>To qualify for any prizes or awards, all registration forms must be received by the close of 
	business on May 31, 2010. Contest ends on June 17, 2010.</li> 
	<li>A minimum of 10 participants is required for any and all prizes to be awarded. 
	<li>Developers must use and enhance the sample templates provided with the 3D Apps product as the 
	basis for their new 3D App Casino experience.</li> 
	<li>Void where prohibited by law.</li> 
    
    
	</ul>
	
</asp:Content>