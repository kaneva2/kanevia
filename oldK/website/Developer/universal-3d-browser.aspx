<%@ Page Language="C#"  MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="true" CodeBehind="universal-3d-browser.aspx.cs" Inherits="KlausEnt.KEP.Developer.universal_3d_browser" %>

<asp:Content ID="cnt_universalBrowser" runat="server" ContentPlaceHolderID="cph_Body">
	<div id="content">
		<div id="main">
			
			<div id="main_content">
			
				<h1 class="three png">The Virtual Universe - according to Kaneva</h1>
				<h2>(One Browser, One Profile, Thousands of Worlds)</h2>
				<p class="highlight">How successful do you think the Web would be today, if you had to download a different browser for every web site?</p>
				<p>Yet, that's exactly the state of the 3D Internet. Each virtual world has its own software and methods - visitors have to sign up, download, create a profile, and learn the ropes before they can even poke their heads in.</p>
				<p>When you build with the Kaneva Star Platform, <span class="highlight">you'll be joining an entire universe of interconnected virtual experiences.</span> We call them Stars (get ready for the acronym) Synthetic Theme-based Augmented Realities. The beauty of it is, once they've visited one Star, people can jump to your Star as easily as typing a URL into the Kaneva Star Explorer.</p>
				
				<p class="centered highlight">star.kaneva.com</p>
				
				<p class="centered highlight small">star.turner.com</p>
				
				<p class="centered highlight smallest">star.you.com</p>
				
				<p>That means that <span class="highlight">as soon as you open your doors, over one million people can just walk right in.</span> Your audience is immediately as large as the collective membership of all Stars.</p>
				<p>And the benefits of joining this system don't stop there.</p>
				<p><span class="highlight">You have complete control over the user experience in your Star</span>, so if you're looking to open the doors to members only (employees, for example), or just adults, or children, it's entirely up to you.</p>
				<p><span class="highlight">You can be easily found.</span> Having your own Star address allows people to find you immediately... no matter where they are. Just like a Web address, you can include it in marketing collateral, on business cards, and link to it from your web sites.</p>
				
			
			
			 </div>
			<div id="sidebar">
			<div id="sidebar_content">
				
                <ul id="universe_sidebar" class="png">
					<li class="one">With the Kaneva Star Platform, users can take the ultimate step &ndash; creating virtual experiences to augment their web sites.</li>
		  			<li class="two">Into the code. The more ambitious among us delved into creating animated entertainment, casual games, and 3D objects.</li>
					<li class="three">College kids with video cameras, Indie bands, and loads of us with a few minutes on our hands, created endless hours of entertainment.</li>
					<li class="four">Sites that allowed visitors to upload their own photos and music let everyday users  contribute to the Web.</li>
			  </ul>
			
			</div>
		</div>
		
		</div>
		<div id="nextPage" class="whykaneva"><a href="kaneva-star-platform.aspx">Next - Why Kaneva</a></div>
	</div>
</asp:Content>