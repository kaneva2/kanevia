///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Developer
{
    public partial class worldwide_3d_community : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //set Main Navigation
            Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV] = usercontrols.Navigation.TAB.PLATFORM;
            //set 2ndary nav
            Session[DeveloperCommonFunctions.ACTIVE_SUB_NAV] = usercontrols.SubNavPlatform.TAB.SERVICES;

            if (!Page.IsPostBack)
            {
                //set the metadata
                ((MainTemplatePage)Master).Title = "The Kaneva Star Platform | Kaneva Origins";
                ((MainTemplatePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"worldwide 3d community, kaneva origins, hosted 3d environment, 3d universe, kaneva star platform \">";
                ((MainTemplatePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Imagine a virtual universe where every business, school, and medical facility has its own 3D environment, as large as a world, or as small as a \"Mom and Pop\" store. A worldwide 3D community that offers its members social networking, media sharing and the next level of virtual interaction (real game play). \">";
            }
        }
    }
}
