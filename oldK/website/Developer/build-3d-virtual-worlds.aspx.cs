///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Developer
{
    public partial class build_3d_virtual_worlds : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //set Main Navigation
            //set Main Navigation
            Session[DeveloperCommonFunctions.ACTIVE_MAIN_NAV] = usercontrols.Navigation.TAB.MOTIVATION;
            //set 2ndary nav
            Session[DeveloperCommonFunctions.ACTIVE_SUB_NAV] = usercontrols.SubNavMotivation.TAB.BUILD;

            if (!Page.IsPostBack)
            {
                //set the metadata
                ((MainTemplatePage)Master).Title = "The Kaneva Star Platform | Why Build in 3D?";
                ((MainTemplatePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\" build 3d virtual world, 3d internet, 3d technology, 3d virtual reality, vrml 3d, 3d worlds, kaneva 3d star system \">";
                ((MainTemplatePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Imagine virtual 3D spaces bringing us together online. They can be grandiose, or incredibly simple: Let your customers walk through a virtual showroom with a sales representative from your company. Bring your novel or television show to life, and let fans walk the halls with the characters they love.\">";
            }

        }
    }
}
