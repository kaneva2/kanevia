///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using System.Reflection;
using System.Resources;
using System.Globalization;
using System.Xml;
using CuteEditor;
using System.IO;
using System.Web.UI.WebControls.WebParts;
using System.Text.RegularExpressions;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using KlausEnt.KEP.Kaneva;
using KlausEnt.KEP.Kaneva.framework.widgets;


namespace KlausEnt.KEP.Developer
{
	/// <summary>
    /// Summary description for DeveloperCommonFunctions.
	/// </summary>
	public class DeveloperCommonFunctions
	{
        #region Declarations

        private static ResourceManager resmgr = new ResourceManager("Developer", Assembly.GetExecutingAssembly());
        private GameFacade _gameFacade;
        private UserFacade _userFacade;
        private CommentFacade _commentFacade;
        private BlastFacade _blastFacade;
        private GameDeveloperFacade _gameDeveloperFacade;
        private DevelopmentCompanyFacade _developmentCompanyFacade;
        private SiteSecurityFacade _siteSecurityFacade;

        #endregion

        // ***********************************************
        // Static functions
        // ***********************************************
        #region Static Functions 

            // ***********************************************
            // Static Miscellaneous functions
            // ***********************************************
            #region Static Miscellaneous

        /// <summary>
        /// GetJoinLink
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetJoinLink
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["JoinLocation"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["JoinLocation"];
                }
                else
                {
                    return "~/join.aspx";
                }


            }
        }

        /// <summary>
        /// GetPlatformLink
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetPlatformLink
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["PlatformLink"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["PlatformLink"];
                }
                else
                {
                    return "#";
                }


            }
        }

        /// <summary>
        /// GetSourceCodeLink
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetSourceCodeLink
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["SourceCodeLink"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["SourceCodeLink"];
                }
                else
                {
                    return "#";
                }


            }
        }

        /// <summary>
        /// GetCardboardLink
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetCardboardLink
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["CardboardWorldLink"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["CardboardWorldLink"];
                }
                else
                {
                    return "#";
                }


            }
        }

        /// <summary>
        /// GetDocumentationLink
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetDocumentationLink
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["DocumentationLink"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["DocumentationLink"];
                }
                else
                {
                    return "#";
                }


            }
        }

        /// <summary>
        /// GetForumLink
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetForumLink
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["STARsForumLink"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["STARsForumLink"];
                }
                else
                {
                    return "#";
                }


            }
        }

        /// <summary>
        /// GetForumLink
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetLicenseInfo
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["LicenseAgreementLink"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["LicenseAgreementLink"];
                }
                else
                {
                    return "#";
                }


            }
        }

        /// <summary>
        /// GetForumLink
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetThirdPartySoftware
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ThirdPartySoftware"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["ThirdPartySoftware"];
                }
                else
                {
                    return "#";
                }


            }
        }

        /// <summary>
        /// GetMaxCommentLength
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static int GetMaxCommentLength()
        {
            return KanevaGlobals.MaxCommentLength;
        }

        /// <summary>
        /// GetMaxCommentLength
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static int GetResultsPerPage
        {
            get
            {
               int numPerPage = 10; 
               if (System.Configuration.ConfigurationManager.AppSettings["ResultsPerPage"] != null)
                {

                    try
                    {
                        numPerPage = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ResultsPerPage"]);
                    }
                    catch(FormatException)
                    {
                    }
                }
                
                return numPerPage;

            }
        }


        /// <summary>
        /// GetKanevaURL
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetKanevaURL
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["kanevaURL"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["kanevaURL"];
                }
                else
                {
                    return "http://www.kaneva.com";
                }


            }
        }

        /// <summary>
        /// GetKanevaURL
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetCurrentSiteName
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["SiteName"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["SiteName"];
                }
                else
                {
                    return GetKanevaURL;
                }


            }
        }

        /// <summary>
        /// GetKanevaURL
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetLostPassword
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["lostPswdURL"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["lostPswdURL"];
                }
                else
                {
                    return "http://www.kaneva.com/lostPassword.aspx";
                }


            }
        }

        /// <summary>
		    /// GetResourceString
		    /// </summary>
		    /// <param name="name"></param>
		    /// <returns></returns>
		    public static string GetResourceString (string name)
		    {
			    return resmgr.GetString (name, System.Globalization.CultureInfo.CurrentCulture);
		    }

		    /// <summary>
		    /// GetResourceString
		    /// </summary>
		    public static string GetResourceString (string name, params string [] args)
		    {
			    string [] newParams = new string [args.Length];
    			
			    for (int i = 0; i < args.Length; i++)
			    {
				    newParams [i] = GetResourceString (args [i]);
			    }

			    return String.Format (GetResourceString (name), newParams);
            }

            #endregion

            // ***********************************************
            // Static User functions
            // ***********************************************
            #region Static User Functions
            /// <summary>
            /// Get the user info from the current logged in user
            /// </summary>
            /// <returns></returns>
            public static GameDeveloper CurrentUser
            {
                get
                {
                    if (HttpContext.Current.Request.IsAuthenticated)
                    {
                        return (GameDeveloper)HttpContext.Current.Items["User"];
                    }
                    else
                    {
                        return new GameDeveloper();
                    }
                }
            }

            /// <summary>
            /// Get the user id from the current logged in user
            /// </summary>
            /// <returns></returns>
            public static int GetUserId()
            {
                return CurrentUser.UserId;
            }

            /// <summary>
            /// Get the user id from the current logged in user
            /// </summary>
            /// <returns></returns>
            public static string GetUserState(string ustate)
            {
                switch (ustate)
                {
                    case Constants.ONLINE_USTATE_OFF:
                        return "";
                    case Constants.ONLINE_USTATE_ON:		// logged into web site
                        return "on-web";
                    case Constants.ONLINE_USTATE_INWORLD:	// logged into WOK
                        return "in-world";
                    case Constants.ONLINE_USTATE_ONINWORLD:	// logged into web site and WOK
                        return "in-world";
                    default:
                        return "";
                }
            }

             #endregion

            // ***********************************************
            // Static Security functions
            // ***********************************************
            #region Static Security

            /// <summary>
		    /// RemoveScripts
		    /// </summary>
		    /// <param name="text"></param>
		    /// <returns></returns>
		    public static bool ContainsInjectScripts (string text)
		    {
			    bool bInvalid = false;

			    try
			    {
				    XmlDocument doc = EditorUtility.ConvertToXmlDocument (text);
				    bInvalid = ContainsScriptsRecursive (doc.DocumentElement);
			    }
			    catch (Exception) {}

			    return bInvalid;
		    }


            /// <summary>
            /// RemoveScriptsRecursive
            /// </summary>
            /// <param name="element"></param>
            private static bool ContainsScriptsRecursive(XmlElement element)
            {
                if (ShouldRemove(element))
                {
                    return true;
                }
                foreach (XmlElement child in element.SelectNodes("*"))
                {
                    return (ContainsScriptsRecursive(child));
                }

                return false;
            }

            /// <summary>
            /// ShouldRemove
            /// </summary>
            /// <param name="element"></param>
            /// <returns></returns>
            private static bool ShouldRemove(XmlElement element)
            {
                string name = element.LocalName.ToLower();
                //check the element name
                switch (name)
                {
                    case "link"://maybe link to another css that contains scripts(behavior,expression)
                    case "script":
                        return true;
                }
                //check the attribute
                foreach (XmlAttribute attr in element.Attributes)
                {
                    string attrname = attr.LocalName.ToLower();
                    //<img onclick=....
                    if (attrname.StartsWith("on"))
                        return true;
                    string val = attr.Value.ToLower();
                    //<a href="javascript:scriptcode()"..
                    if (val.IndexOf("script") != -1)
                        return true;
                    //<a style="behavior:url(http://another/code.htc)"
                    if (attrname == "style")
                    {
                        if (val.IndexOf("behavior") != -1)
                            return true;
                        if (val.IndexOf("expression") != -1)
                            return true;
                    }

                }
                return false;
            }

            #endregion

        #endregion

        // ***********************************************
        // CONSTANTS
        // ***********************************************
        #region Constants

        public const string ACTIVE_MAIN_NAV = "activeMainNav";
        public const string ACTIVE_SUB_NAV = "activeSubNav";
        public const string ACTIVE_TERTIARY_NAV = "activeTertiaryNav";
        public const int DEFAULT_PORT = 25857; //default provided by wok team
        public const int DEFAULT_LICENSE_STATUS_ID = 1; //default provided by wok team

        #endregion

        // ***********************************************
        // Formatting functions
        // ***********************************************
        #region Formatting functions

        /// <summary>
        /// Format currency
        /// </summary>
        /// <param name="dblCurrency"></param>
        /// <returns></returns>
        public string FormatCurrencyForTextBox(Object dblCurrency)
        {
            return KanevaGlobals.FormatCurrencyForTextBox(dblCurrency);
        }

        /// <summary>
        /// Format the date into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public string FormatDate(Object dtDate)
        {
            if (dtDate.Equals(DBNull.Value))
                return "";
            return KanevaGlobals.FormatDate((DateTime)dtDate);
        }

        public string FormatDateNumbersOnly(Object dtDate)
        {
            if (dtDate.Equals(DBNull.Value))
                return "";
            return KanevaGlobals.FormatDateNumbersOnly((DateTime)dtDate);
        }

        /// <summary>
        /// Format the date into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public string FormatDateTime(Object dtDate)
        {
            if (dtDate.Equals(DBNull.Value))
                return "";
            return KanevaGlobals.FormatDateTime((DateTime)dtDate);
        }
        /// <summary>
        /// Format date as a time span
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public string FormatDateTimeSpan(Object dtDate)
        {
            return KanevaGlobals.FormatDateTimeSpan(dtDate);
        }
        /// <summary>
        /// Format image size
        /// </summary>
        /// <param name="imageSize"></param>
        /// <returns></returns>
        public string FormatImageSize(Object imageSize)
        {
            if (imageSize.Equals(DBNull.Value))
                return "";
            return KanevaGlobals.FormatImageSize(Convert.ToInt64(imageSize));
        }


        /// <summary>
        /// FormatCurrency
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public string FormatCurrency(Object amount)
        {
            return KanevaGlobals.FormatCurrency(Convert.ToDouble(amount));
        }

        /// <summary>
        /// Get a string
        /// </summary>
        public string FormatString(Object dtColumn, int statusId)
        {
            return FormatString(dtColumn, statusId, "");
        }

        /// <summary>
        /// Get a string
        /// </summary>
        public string FormatString(Object dtColumn, int statusId, string notSetText)
        {
            //if (statusId.Equals((int)Constants.eASSET_STATUS.NEW))
            return "<span class=\"offline\">" + notSetText + "</span>";
        }

        /// <summary>
        /// Format the date into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public string FormatDateTimeSpan(Object dtDate, Object dtDateNow)
        {
            if (dtDate.Equals(DBNull.Value))
                return "";
            return KanevaGlobals.FormatDateTimeSpan(dtDate, dtDateNow);
        }

        /// <summary>
        /// TruncateWithEllipsis
        /// </summary>
        public string TruncateWithEllipsis(string text, int length)
        {
            return KanevaGlobals.TruncateWithEllipsis(text, length);
        }

        /// <summary>
        /// TruncateWithEllipsis
        /// </summary>
        public string FormatSecondsAsTime(Object seconds)
        {
            if (seconds.Equals(DBNull.Value))
                return "00:00:00";

            return KanevaGlobals.FormatSecondsAsTime(Convert.ToInt32(seconds));
        }

        #endregion

        // ***********************************************
        // Image functions
        // ***********************************************
        #region Image Functions
        public bool UploadWOKItemImage(int userId, int itemId, HtmlInputFile inputFile)
        {
            bool success = false;
            try
            {
                ImageHelper.UploadImageFromUser(userId, itemId, inputFile, KanevaGlobals.GameImagesServerPath, ImageHelper.GAME_IMAGE);
                success = true;
            }
            catch (Exception)
            { }
            return success;
        }

        #endregion

        // ***********************************************
        // Helper functions
        // ***********************************************
        #region Helper Functions

        /// <summary>
        /// Creates the default home page for a newly registered user. Places a set of default
        /// widgets on the page.
        /// </summary>
        /// <returns>bool - true on success</returns>
        public bool CreateDefaultUserHomePage(string username, int userId)
        {
            DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

            if (userId.Equals(0))
            {
                return false;
            }

            return false;
        }

        /// <summary>
        /// GetJoinLink
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string GenerateUniqueString(int maxsize)
        {
            return KanevaGlobals.GenerateUniqueString(maxsize);
        }


        /// <summary>
        /// Return the delete javascript
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetDeleteScript(int gameId)
        {
            return GetDeleteScript(gameId, false);
        }

        /// <summary>
        /// Return the delete javascript
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetDeleteScript(int gameId, bool removeOnly)
        {
            if (removeOnly)
            {
                return "javascript:if (confirm(\"Are you sure you want to remove this Star from the channel?\")){DeleteSTAR (" + gameId + ")};";
            }
            else
            {
                return "javascript:if (confirm(\"Are you sure you want to delete this Star?\")){DeleteSTAR (" + gameId + ")};";
            }
        }

        /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public string GetGameStatusDescription(int statusId)
        {
            string status = "";

            DataRow[] results = WebCache.GetGameStatus().Select("game_status_id = " + statusId);
            if ((results != null) && (results.Length > 0))
            {
                status = results[0]["game_status"].ToString();
            }
            return status;
        }

                /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public string GetGameVisibiltyDescription(int visibilityId)
        {
            string visibility = "";

            DataRow[] results = WebCache.GetGameServerVisbility().Select("visibility_id = " + visibilityId);
            if ((results != null) && (results.Length > 0))
            {
                visibility = results[0]["name"].ToString();
            }
            return visibility;
        }


        /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public string GetGameRatingDescription(int ratingId)
        {
            string status = "";

            DataRow[] results = WebCache.GetGameRatings().Select("game_rating_id = " + ratingId);
            if ((results != null) && (results.Length > 0))
            {
                status = results[0]["game_rating"].ToString();
            }
            return status;
        }

        public string ShowGamePopulation(int count)
        {
            if (count > 0 && count < 11)
            {
                return "Low";
            }
            else if (count > 10 && count < 81)
            {
                return "Medium";
            }
            else if (count > 80)
            {
                return "High";
            }
            else
            {
                return "Empty";
            }
        }

        /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public int IsGameMature(int ratingId)
        {
            int mature = 0;

            DataRow[] results = WebCache.GetGameRatings().Select("game_rating_id = " + ratingId);
            if ((results != null) && (results.Length > 0))
            {
                mature = Convert.ToInt32(results[0]["is_mature"]);
            }
            return mature;
        }

        /// <summary>
        /// Queries server status data set to get the description
        /// </summary>
        public string GetServerStatusDescription(int statusId)
        {
            string status = "";

            DataRow[] results = WebCache.GetGameServerStatus().Select("server_status_id = " + statusId);
            if ((results != null) && (results.Length > 0))
            {
                status = results[0]["name"].ToString();
            }
            return status;
        }


        /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public string GetGameAccessDescription(int accessId)
        {
            string status = "";

            DataRow[] results = WebCache.GetGameAccess().Select("game_access_id = " + accessId);
            if ((results != null) && (results.Length > 0))
            {
                status = results[0]["game_access"].ToString();
            }
            return status;
        }

        /// <summary>
        /// Get game edit Link
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetGameDetailsLink(int gameId)
        {
            return gameId.ToString () + ".game";
        }

        public GameFacade GetGameFacade
        {
            get
            {
                if (_gameFacade == null)
                {
                    _gameFacade = new GameFacade();
                }
                return _gameFacade;
            }
        }

        public DevelopmentCompanyFacade GetDevelopmentCompanyFacade
        {
            get
            {
                if (_developmentCompanyFacade == null)
                {
                    _developmentCompanyFacade = new DevelopmentCompanyFacade();
                }
                return _developmentCompanyFacade;
            }
        }
        
        public GameDeveloperFacade GetGameDeveloperFacade
        {
            get
            {
                if (_gameDeveloperFacade == null)
                {
                    _gameDeveloperFacade = new GameDeveloperFacade();
                }
                return _gameDeveloperFacade;
            }
        }
       
        public UserFacade GetUserFacade
        {
            get
            {
                if (_userFacade == null)
                {
                    _userFacade = new UserFacade();
                }
                return _userFacade;
            }
        }

        public CommentFacade GetCommentFacade
        {
            get
            {
                if (_commentFacade == null)
                {
                    _commentFacade = new CommentFacade();
                }
                return _commentFacade;
            }
        }

        public BlastFacade GetBlastFacade
        {
            get
            {
                if (_blastFacade == null)
                {
                    _blastFacade = new BlastFacade();
                }
                return _blastFacade;
            }
        }

        public SiteSecurityFacade GetSiteSecurityFacade
        {
            get
            {
                if (_siteSecurityFacade == null)
                {
                    _siteSecurityFacade = new SiteSecurityFacade();
                }
                return _siteSecurityFacade;
            }
        }

        public ListItem CreateListItem(string name, string theValue)
        {
            return CreateListItem(name, theValue, false);
        }

        public ListItem CreateListItem(string name, string theValue, bool bold)
        {
            ListItem liNew = new ListItem(name, theValue);
            if (bold)
            {
                liNew.Attributes.Add("style", "font-weight: bold;");
            }
            return liNew;
        }

        /// <summary>
        /// SetDropDownIndex
        /// </summary>
        public void SetDropDownIndex(DropDownList drp, string theValue)
        {
            try
            {
                drp.SelectedValue = theValue;
            }
            catch (Exception)
            { }
        }

        /// <summary>
        /// SetDropDownIndex
        /// </summary>
        public void SetDropDownIndex(DropDownList drp, string theValue, bool bAddValue)
        {
            SetDropDownIndex(drp, theValue, bAddValue, theValue);
        }

        /// <summary>
        /// SetDropDownIndex
        /// </summary>
        public void SetDropDownIndex(DropDownList drp, string theValue, bool bAddValue, string theDisplayName)
        {
            if (bAddValue)
            {
                ListItem li = drp.Items.FindByValue(theValue);
                if (li == null)
                {
                    drp.Items.Insert(drp.Items.Count, new ListItem(theDisplayName, theValue));
                }
            }
            SetDropDownIndex(drp, theValue);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return KanevaGlobals.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return KanevaGlobals.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }


        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
        {
            return KanevaGlobals.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
        {
            return KanevaGlobals.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts);
        }

        /// <summary>
        /// AddHrefNewWindow
        /// </summary>
        /// <param name="theText"></param>
        /// <returns></returns>
        public string AddHrefNewWindow(string theText)
        {
            return theText.Replace("<A href=", "<A target=_blank href=");
        }

        /// <summary>
        /// removeHTML
        /// </summary>
        /// <param name="strMessageInput"></param>
        /// <returns></returns>
        public string RemoveHTML(string strMessageInput)
        {
            int lngMessagePosition = 0;		// Holds the message position
            int intHTMLTagLength = 0;		// Holds the length of the HTML tags
            string strHTMLMessage = "";			// Holds the HTML message
            string strTempMessageInput = "";	// Temp store for the message input

            // Get out quick?
            if (strMessageInput.Length == 0)
            {
                return "";
            }

            // Place the message input into a temp store
            strTempMessageInput = strMessageInput;

            // If an HTML tag is found then jump to the end so we can strip it
            lngMessagePosition = strMessageInput.IndexOf("<", lngMessagePosition);

            // Loop through each character in the post message
            while (lngMessagePosition > -1)
            {
                // Get the length of the HTML tag
                intHTMLTagLength = (strMessageInput.IndexOf(">", lngMessagePosition) - lngMessagePosition);

                if (intHTMLTagLength > 0)
                {
                    // Place the HTML tag back into the temporary message store
                    strHTMLMessage = strMessageInput.Substring(lngMessagePosition, intHTMLTagLength + 1);

                    // Strip the HTML from the temp message store
                    strTempMessageInput = strTempMessageInput.Replace(strHTMLMessage, "");
                }

                // If an HTML tag is found then jump to the end so we can strip it
                lngMessagePosition = strMessageInput.IndexOf("<", lngMessagePosition + 1);
            }

            // Replace a few characters in the remaining text
            strTempMessageInput = strTempMessageInput.Replace("<", "&lt;");
            strTempMessageInput = strTempMessageInput.Replace(">", "&gt;");
            strTempMessageInput = strTempMessageInput.Replace("'", "&#039;");
            strTempMessageInput = strTempMessageInput.Replace("\"", "&#034;");
            strTempMessageInput = strTempMessageInput.Replace("&nbsp;", " ");

            //Return the function
            return strTempMessageInput;
        }

        #endregion

        // ***********************************************
        // Security Functions
        // ***********************************************
        #region Security Functions

        public bool IsUserAllowedToEditGame(int gameId)
        {
            return (IsAdministrator() || IsGameOwner(gameId));
        }

        public string MakeHash(string s)
        {
            return UsersUtility.MakeHash(s);
        }

        /// <summary>
        /// Is the current session user a site administrator?
        /// </summary>
        /// <returns></returns>
        public bool IsAdministrator()
        {
            return UsersUtility.IsUserAdministrator();
        }


        /// <summary>
        /// Is this user a site administrator?
        /// </summary>
        /// <returns></returns>
        public bool IsAdministrator(int userId)
        {
            return UsersUtility.IsUserAdministrator(userId);
        }

        /// <summary>
        /// Is this user a site administrator?
        /// </summary>
        /// <returns></returns>
        public bool IsAdministratorByUserRole(int role)
        {
            return UsersUtility.IsUserAdministratorByUserRole(role);
        }

        /// <summary>
        /// Is the current session user a CSR?
        /// </summary>
        /// <returns></returns>
        public bool IsCSR()
        {
            return UsersUtility.IsUserCSR();
        }

        /// <summary>
        /// Is this user a CSR?
        /// </summary>
        /// <returns></returns>
        public bool IsCSR(int userId)
        {
            return UsersUtility.IsUserCSR(userId);
        }

        /// <summary>
        /// Is this user a CSR?
        /// </summary>
        /// <returns></returns>
        public bool IsCSRByUserRole(int role)
        {
            return UsersUtility.IsUserCSRByUserRole(role);
        }

        /// <summary>
        /// Is the current session user a IT?
        /// </summary>
        /// <returns></returns>
        public bool IsIT()
        {
            return UsersUtility.IsUserIT();
        }

        /// <summary>
        /// Is this user a IT?
        /// </summary>
        /// <returns></returns>
        public bool IsIT(int userId)
        {
            return UsersUtility.IsUserIT(userId);
        }

        /// <summary>
        /// Is this user a IT?
        /// </summary>
        /// <returns></returns>
        public bool IsITByUserRole(int role)
        {
            return UsersUtility.IsUserITByUserRole(role);
        }

        /// <summary>
        /// Is item a mature item?
        /// </summary>
        /// <returns>bool</returns>
        public bool IsMature(int gameRatingId)
        {
            return StoreUtility.IsMatureRating(gameRatingId);
        }

        /// <summary>
        /// determines if the user is a minor? 
        /// throws it exceptions
        /// </summary>
        /// <param name="age">age</param>
        /// <returns>bool</returns>
        public bool IsUserAMinor(int age)
        {
            //check age by comparing to cut off
            if (age >= KanevaGlobals.MinorCutOffAge)
            {
                return false;
            }
            //this return is for minor found
            return true;
        }

        /// <summary>
        /// determines if the user is a minor? 
        /// throws its exceptions
        /// </summary>
        /// <param name="birthday">birthday</param>
        /// <returns>bool</returns>
        public bool IsUserAMinor(DateTime birthday)
        {
            return IsUserAMinor(KanevaGlobals.GetAgeInYears(birthday));
        }

        /// <summary>
        /// determines if the user is the games owner? 
        /// throws its exceptions
        /// </summary>
        /// <param name="birthday">birthday</param>
        /// <returns>bool</returns>
        public bool IsGameOwner(int gameId)
        {
            int ownerId = GetGameFacade.GetGameOwner(gameId);
            return ownerId.Equals (CurrentUser.UserId);
        }

        /// <summary>
        /// Is the current user adult?
        /// </summary>
        /// <returns></returns>
        public bool IsCurrentUserAdult()
        {
            // Now we are showing all if they are not logged in.
            return IsCurrentUserAdult(true);
        }

        /// <summary>
        /// Is the current user adult?
        /// </summary>
        /// <returns></returns>
        public bool IsCurrentUserAdult(bool defaultForNotLoggedIn)
        {
            // If they are logged in and over 18, we can show the adult communities
            bool bIsAdult = false;

            if (HttpContext.Current.Request.IsAuthenticated)
            {
                bIsAdult = (CurrentUser.UserInfo.Age >= 17);
            }
            else
            {
                bIsAdult = defaultForNotLoggedIn;
            }

            return bIsAdult;
        }

        /// <summary>
        /// PassRequired
        /// </summary>
        public bool PassRequired(int accessTypeId)
        {
            bool passRequired = false;

            switch (accessTypeId)
            {
                case (int)Constants.eACCESS_TYPE.ACCESS_PASS:
                    passRequired = true;
                    break;
                case (int)Constants.eACCESS_TYPE.GENERAL:
                    passRequired = false;
                    break;
                default:
                    passRequired = false;
                    break;
            }

            return passRequired;
        }

        public string CleanJavascriptFull(string strText)
        {
            return KanevaGlobals.CleanJavascript(strText);
        }
        #endregion

        // ***********************************************
        // Game Functions
        // ***********************************************
        #region Game Functions

        /// <summary>
        /// GetAssetTags
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public string GetAssetTags(string keywords, System.Web.UI.Page page)
        {
            return StoreUtility.GetAssetTags(keywords, page);
        }

        /// <summary>
        /// GetAssetTags
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public string GetAssetTags(string keywords, int type, System.Web.UI.Page page)
        {
            return StoreUtility.GetAssetTags(keywords, page, type);
        }

        /// <summary>
        /// GetAssetCategoryList
        /// </summary>
        /// <param name="categories"></param>
        /// <returns></returns>
        public string GetAssetCategoryList(string categories, System.Web.UI.Page page)
        {
            return StoreUtility.GetAssetCategoryList(categories, page);
        }

        /// <summary>
        /// Return the game image URL
        /// </summary>
        public string GetGameImageURL(string imagePath, string defaultSize)
        {
            if (imagePath.Length.Equals(0))
            {
                return KanevaGlobals.GameImageServer + "/KanevaGameIcon_" + defaultSize + ".gif";
            }
            else
            {
                if ((defaultSize == null) || (defaultSize == ""))
                {
                    defaultSize = "me";
                }
                return KanevaGlobals.GameImageServer + "/" + Path.GetDirectoryName(imagePath).Replace("\\", "/") + "/" + Path.GetFileNameWithoutExtension(imagePath) + "_" + defaultSize + Path.GetExtension(imagePath);
            }
        }

        public void UploadGameImage(int userId, int itemId, HtmlInputFile browseTHUMB)
        {
            try
            {
                ImageHelper.UploadImageFromUser(userId, itemId, browseTHUMB, KanevaGlobals.GameImagesServerPath, ImageHelper.GAME_IMAGE);
            }
            catch (Exception)
            {
                //ShowErrorOnStartup(ex.Message, false);
            }
        }


        /// <summary>
        /// Get Asset Details Link
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetAssetDetailsLink(int assetId, Page page)
        {
            return StoreUtility.GetAssetDetailsLink(assetId, page);
        }

        #endregion

        // ***********************************************
        // User Functions
        // ***********************************************
        #region User Functions

        public int AuthorizeUser(string email, string password, int gameId, ref int roleMembership, string UserHostAddress, bool bSendTrackerMessage)
        {
            return UsersUtility.Authorize(email, password, gameId, ref roleMembership, UserHostAddress, bSendTrackerMessage);
        }
        /// <summary>
        /// Return the user image URL
        /// </summary>
        public string GetProfileImageURL(string imagePath)
        {
            return UsersUtility.GetProfileImageURL(imagePath);
        }

        public string GetPersonalChannelUrl(string userName)
        {
            return KanevaGlobals.GetPersonalChannelUrl(userName);
        }

        /// <summary>
        /// Return the user image URL
        /// </summary>
        public string GetProfileImageURL(string imagePath, string defaultSize, string gender)
        {
            return UsersUtility.GetProfileImageURL(imagePath, defaultSize, gender);
        }

        public int UpdateLastLogin(int userId, string UserHostAddress, string serverName)
        {
            return GetUserFacade.UpdateLastLogin(userId, UserHostAddress, serverName);
        }

        #endregion

        // ***********************************************
        // Community Functions
        // ***********************************************
        /// <summary>
        /// Return the broadcast channel link
        /// </summary>
        /// <param name="channelId"></param>
        /// <returns></returns>
        public string GetBroadcastChannelUrl (string nameNoSpaces)
        {
            return KanevaGlobals.GetBroadcastChannelUrl (nameNoSpaces);
        }
        /// <summary>
        /// Return the photo image URL
        /// </summary>
        public string GetBroadcastChannelImageURL (string imagePath, string defaultSize)
        {
            return CommunityUtility.GetBroadcastChannelImageURL (imagePath, defaultSize);
        }
    }
}
