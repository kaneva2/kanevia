///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web.Caching;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using KlausEnt.KEP.Kaneva.framework.widgets;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Developer
{
	/// <summary>
	/// Summary description for Cache.
	/// </summary>
	public class WebCache
	{
		public WebCache ()
		{
		}

        /// <summary>
        /// GetModuleTitle
        /// </summary>
        public static string GetModuleTitle(int module_id)
        {
            DataTable dtModules = (DataTable)Global.Cache()[cMODULE_TITLES];
            if (dtModules == null)
            {
                // Add to the cache
                dtModules = GetLayoutModules();
                Global.Cache().Insert(cMODULE_TITLES, dtModules, null, DateTime.Now.AddMinutes(15), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            DataRow[] dr = dtModules.Select("module_id = " + module_id);
            if (dr.Length > 0)
                return dr[0]["title"].ToString();
            else
                return "";
        }

        /// <summary>
        /// GetLayoutModules
        /// </summary>
        /// <returns></returns>
        public static DataTable GetLayoutModules()
        {
            DataTable dtLayoutModules = (DataTable)Global.Cache()[cLAYOUT_MODULES];
            if (dtLayoutModules == null)
            {
                // Add the fonts to the cache
                dtLayoutModules = PageUtility.GetLayoutModulesNonCached();
                Global.Cache().Insert(cLAYOUT_MODULES, dtLayoutModules, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtLayoutModules;
        }

        /// <summary>
        /// Get a specific country
        /// </summary>
        /// <returns></returns>
        public static Country GetCountry (string countryId)
        {
            IList <Country> dtCountries = GetCountries ();
            Country _country = null;

            foreach (Country c in dtCountries)
            {
                if (c.CountryId == countryId)
                {
                    _country = c;
                    break;
                }
            }

            return _country;
        }
        
        /// <summary>
		/// GetCountries
		/// </summary>
        public static IList<Country> GetCountries()
		{
			string cacheItemsName = "countries";

			// Load countries
            IList<Country> dtCountries = (IList<Country>) Global.Cache()[cacheItemsName];

			if (dtCountries == null)
			{
				// Get and add to cache
                UserFacade userFacade = new UserFacade();
				dtCountries = userFacade.GetCountries ();
				Global.Cache ().Insert (cacheItemsName, dtCountries, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
			}
            
			return dtCountries;
		}

        /// <summary>
        /// GetStates
        /// </summary>
        public static IList<State> GetStates()
        {
            string cacheItemsName = "states";

            // Load states
            IList<State> dtStates = (IList<State>)Global.Cache()[cacheItemsName];

            if (dtStates == null)
            {
                // Get and add to cache
                UserFacade userFacade = new UserFacade();
                dtStates = userFacade.GetStates();
                Global.Cache().Insert(cacheItemsName, dtStates, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtStates;
        }

     /*   public static DataRow GetZipCode (string zip_code)
        {
            DataTable dtZipCodes = GetZipCodes ();
            DataRow[] drZipCode = dtZipCodes.Select("zip_code='" + zip_code + "'");

            return drZipCode[0];
        }

        /// <summary>
        /// GetZipCodes
        /// </summary>
        public static DataTable GetZipCodes ()
        {
            string cacheZipCodes = "zipCodes";

            // Load asset perms
            DataTable dtZipCodes = (DataTable) Global.Cache ()[cacheZipCodes];
            if (dtZipCodes == null)
            {
                // Add the community list to the cache
                dtZipCodes = UsersUtility.GetZipCodes ();
                Global.Cache ().Insert (cacheZipCodes, dtZipCodes, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtZipCodes;
        }*/


        /// <summary>
        /// Get Virtual World Involvment
        /// </summary>
        public static DataTable GetVirtualWorldInvolvment()
        {
            DataTable dtVirtualInvolvement = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            dtVirtualInvolvement = (DataTable)cache[cPROSPECT_INVOLVEMENT];
            if (dtVirtualInvolvement == null)
            {
                DevelopmentCompanyFacade gDevCompany = new DevelopmentCompanyFacade();

                dtVirtualInvolvement = gDevCompany.GetVWorldInvolvment();
                cache.Insert(cPROSPECT_INVOLVEMENT, dtVirtualInvolvement, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtVirtualInvolvement;
        }

        /// <summary>
        /// Get Virtual World Involvment
        /// </summary>
        public static DataTable GetProjectStartDates()
        {
            DataTable dtProjectStartDates = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            dtProjectStartDates = (DataTable)cache[cPROSPECT_STARTDATES];
            if (dtProjectStartDates == null)
            {
                DevelopmentCompanyFacade gDevCompany = new DevelopmentCompanyFacade();

                dtProjectStartDates = gDevCompany.GetProjectStartDates();
                cache.Insert(cPROSPECT_STARTDATES, dtProjectStartDates, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtProjectStartDates;
        }

        /// <summary>
        /// Get Virtual World Involvment
        /// </summary>
        public static DataTable GetClientPlatforms()
        {
            DataTable dtClientPlatforms = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            dtClientPlatforms = (DataTable)cache[cPROSPECT_PLATFORMS];
            if (dtClientPlatforms == null)
            {
                DevelopmentCompanyFacade gDevCompany = new DevelopmentCompanyFacade();

                dtClientPlatforms = gDevCompany.GetClientPlatforms();
                cache.Insert(cPROSPECT_PLATFORMS, dtClientPlatforms, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtClientPlatforms;
        }

        /// <summary>
        /// Get Virtual World Involvment
        /// </summary>
        public static DataTable GetProjectStatuses()
        {
            DataTable dtProjectStatuses = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            dtProjectStatuses = (DataTable)cache[cPROSPECT_STATUSES];
            if (dtProjectStatuses == null)
            {
                DevelopmentCompanyFacade gDevCompany = new DevelopmentCompanyFacade();

                dtProjectStatuses = gDevCompany.GetProjectStatus();
                cache.Insert(cPROSPECT_STATUSES, dtProjectStatuses, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtProjectStatuses;
        }


        /// <summary>
        /// GetGameRatings
        /// </summary>
        public static DataTable GetGameRatings()
        {
            DataTable dtGameRatings = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            dtGameRatings = (DataTable)cache[cGAME_RATINGS];
            if (dtGameRatings == null)
            {
                GameFacade gFacade = new GameFacade();

                dtGameRatings = gFacade.GetGameRatingOptionsLevels();
                cache.Insert(cGAME_RATINGS, dtGameRatings, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtGameRatings;
        }

		/// <summary>
        /// GetGameCategories
		/// </summary>
        public static List<CommunityCategory> GetGameCategories()
		{
            List<CommunityCategory> dtGameCategories = null;
			Cache cache = Global.Cache ();
			int cacheTime = 15;

            dtGameCategories = (List<CommunityCategory>)cache[cGAME_CATEGORIES];
            if (dtGameCategories == null)
			{
                CommunityFacade communityFacade = new CommunityFacade();

                dtGameCategories = communityFacade.GetCommunityCategories();
                cache.Insert(cGAME_CATEGORIES, dtGameCategories, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
			}

            return dtGameCategories;
		}

        /// <summary>
        /// GetGameAccess
        /// </summary>
        public static DataTable GetGameAccess()
        {
            DataTable dtGameAccess = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            dtGameAccess = (DataTable)cache[cGAME_ACCESS];
            if (dtGameAccess == null)
            {
                GameFacade gFacade = new GameFacade();

                dtGameAccess = gFacade.GetGameAccessLevels();
                cache.Insert(cGAME_ACCESS, dtGameAccess, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtGameAccess;
        }

        /// <summary>
        /// GetGameStatus
        /// </summary>
        public static DataTable GetGameStatus()
        {
            DataTable dtGameStatus = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            dtGameStatus = (DataTable)cache[cGAME_STATUS];
            if (dtGameStatus == null)
            {
                GameFacade gFacade = new GameFacade();

                dtGameStatus = gFacade.GetGameStatusOptions();
                cache.Insert(cGAME_STATUS, dtGameStatus, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtGameStatus;
        }

        /// <summary>
        /// GetGameStatus
        /// </summary>
        public static List<GameLicenseSubscription> GetGameLicenseTypes()
        {
            List<GameLicenseSubscription> dtGameLicenseTypes = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            dtGameLicenseTypes = (List<GameLicenseSubscription>)cache[cGAME_LICENSETYPE];
            if (dtGameLicenseTypes == null)
            {
                GameFacade gFacade = new GameFacade();

                dtGameLicenseTypes = gFacade.GetGameLicenseTypes();
                cache.Insert(cGAME_LICENSETYPE, dtGameLicenseTypes, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtGameLicenseTypes;
        }

        /// GetGameStatus
        /// </summary>
        public static DataTable GetGameServerVisbility()
        {
            DataTable dtGameServerVisibility = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            dtGameServerVisibility = (DataTable)cache[cSERVER_VISIBILITY];
            if (dtGameServerVisibility == null)
            {
                GameFacade gFacade = new GameFacade();

                dtGameServerVisibility = gFacade.GetServerVisibilityOptions();
                cache.Insert(cSERVER_VISIBILITY, dtGameServerVisibility, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtGameServerVisibility;
        }

        /// GetGameStatus
        /// </summary>
        public static DataTable GetGameServerStatus()
        {
            DataTable dtGameServerStatus = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            dtGameServerStatus = (DataTable)cache[cSERVER_STATUS];
            if (dtGameServerStatus == null)
            {
                GameFacade gFacade = new GameFacade();

                dtGameServerStatus = gFacade.GetServerStatusOptions();
                cache.Insert(cSERVER_STATUS, dtGameServerStatus, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtGameServerStatus;
        }

        /// GetGameStatus
        /// </summary>
        public static DataTable GetGameServerTypes()
        {
            DataTable dtGameServerTypes = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            dtGameServerTypes = (DataTable)cache[cSERVER_TYPE];
            if (dtGameServerTypes == null)
            {
                GameFacade gFacade = new GameFacade();

                dtGameServerTypes = gFacade.GetServerTypeOptions();
                cache.Insert(cSERVER_TYPE, dtGameServerTypes, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtGameServerTypes;
        }

        /// <summary>
        /// GetAvailableWebsites
        /// </summary>
        public static DataTable GetAccessLevels()
        {
            // Load asset perms
            DataTable dtAccessLevels = (DataTable)Global.Cache()[cACCESS_LEVELS];
            if (dtAccessLevels == null)
            {
                // Add the community list to the cache
                dtAccessLevels = (new SiteSecurityFacade()).GetAllAccessLevels();
                Global.Cache().Insert(cACCESS_LEVELS, dtAccessLevels, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtAccessLevels;
        }

        /// <summary>
        /// GetGameStatus
        /// </summary>
        public static List<SitePrivilege> GetAvailableUserPrivileges()
        {
            List<SitePrivilege> availablePrivileges = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            availablePrivileges = (List<SitePrivilege>)cache[cAVAILABLE_PRIVILEDGES];
            if (availablePrivileges == null)
            {
                SiteSecurityFacade siteSecurityFacade = new SiteSecurityFacade();

                availablePrivileges = (List<SitePrivilege>)siteSecurityFacade.GetAvailableUserPrivileges();
                cache.Insert(cAVAILABLE_PRIVILEDGES, availablePrivileges, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return availablePrivileges;
        }

        // Featured item cache constants
        private const string cPROSPECT_PLATFORMS = "proPlatforms";
        private const string cPROSPECT_STATUSES = "proStatuses";
        private const string cPROSPECT_STARTDATES = "proStartDates";
        private const string cPROSPECT_INVOLVEMENT = "proVWorldInvolv";
        private const string cGAME_CATEGORIES = "gamCats";
        private const string cGAME_RATINGS = "gamRatings";
        private const string cGAME_ACCESS = "gamAccess";
        private const string cGAME_STATUS = "gamStatus";
        private const string cGAME_LICENSETYPE = "gamLicenseType";
        private const string cSERVER_VISIBILITY = "srvVisibility";
        private const string cSERVER_STATUS = "srvStatus";
        private const string cSERVER_TYPE = "srvType";
        private const string cACCESS_LEVELS = "accessLevels";
        private const string cSITE_PRIVILEDGES = "sitePrivileges";
        private const string cAVAILABLE_PRIVILEDGES = "availablePrivileges";
        private const string cMODULE_TITLES = "MODULE_TITLES";

        private const string cLAYOUT_MODULES = "kaneva.layout_modules";

	}
}
