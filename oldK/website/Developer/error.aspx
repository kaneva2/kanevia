<%@ Page language="c#" MasterPageFile="~/masterpages/MainTemplatePage.Master" Codebehind="error.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Developer.Error" %>
	
  <asp:Content ID="cnt_errorPage" runat="server" ContentPlaceHolderID="cph_Body">	
    <div class="clear"><!-- clear the floats --></div>
    <div class="formError">
        Application Error</font><br />
          <hr size="1px" noshade style="color:red;" width="65%"/>
          <br />
          We are sorry, but Kaneva experienced a problem completing your request. 
     </div>
     <div>   
        <p> You can:<br />
          -Use your back button and retry your request<br />
          -Return to the <a href="vision-3d-internet.aspx">home page</a> </p>
    </div>
  </asp:Content >
