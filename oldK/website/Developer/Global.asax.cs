///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Security.Principal;
using System.Data;
using System.Configuration;
using System.Web.Security;

// Import log4net classes.
using log4net;
using log4net.Config;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Developer
{
    public class Global : System.Web.HttpApplication
    {
        /// <summary>
        /// Get the current cache
        /// </summary>
        /// <returns></returns>
        public static System.Web.Caching.Cache Cache()
        {
            return HttpRuntime.Cache;
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            // Change to log4net to be consistent with other modules
            XmlConfigurator.Configure(new System.IO.FileInfo(Server.MapPath("~") + "\\" + System.Configuration.ConfigurationManager.AppSettings["LogConfigFile"]));
        }

        /// <summary>
        /// Application_AuthenticateRequest
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            // Get the user information and save it to context for later.
            // This is for speed so we only have to get this data once per request.

            //set this as default for non-authenticated user
            GameDeveloper gameDev = new GameDeveloper();
            HttpContext.Current.Items["User"] = gameDev;

            if (Request.IsAuthenticated)
            {
                try
                {
                    // Load users developer data
                    gameDev = (new GameDeveloperFacade()).GetGameDeveloperByUserId(Convert.ToInt32(User.Identity.Name));

                    //set the user context
                    HttpContext.Current.Items["User"] = gameDev;

                    //ArrayList ar = Kaneva.UsersUtility.GetUserRoleArray(gameDev.UserInfo.Role);
                    //string[] roles = (string[])ar.ToArray(Type.GetType("System.String"));

                    //HttpContext.Current.User = new GenericPrincipal(User.Identity, roles);
                }
                catch (Exception)
                {
                    System.Web.HttpCookie aCookie;
                    int limit = Request.Cookies.Count - 1;
                    for (int i = limit; i != -1; i--)
                    {
                        aCookie = Request.Cookies[i];
                        aCookie.Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies.Add(aCookie);
                    }

                    FormsAuthentication.SignOut();
                    //Session.Abandon();
                    Response.Redirect("login.aspx");
                }
            }
        }

        protected void Application_BeginRequest (Object sender, EventArgs e)
        {
            String strCurrentPath;
            strCurrentPath = Request.Path;
            strCurrentPath = strCurrentPath.ToLower ();

            // TODO:  need to make this into a method call
            string siteName = "http://" + System.Configuration.ConfigurationManager.AppSettings["databaseType"];
            string status301 = "301 Moved Permanently";

            if (strCurrentPath.IndexOf ("/games/") > -1)
            {
                // Linking to Game Details 
                if (strCurrentPath.EndsWith (".game"))
                {
                    // Get the game parameters
                    int gameId = 0;

                    try
                    {
                        gameId = Convert.ToInt32 (System.IO.Path.GetFileNameWithoutExtension (strCurrentPath));
                    }
                    catch (Exception) { }

                    string qs = Request.QueryString.ToString ();

                    string redirectUrl = siteName + "/games/gameDetails.aspx?gameId=" + gameId;
                    if (qs != null && qs.Length > 0)
                    {
                        redirectUrl += "&" + qs;
                    }

                    Response.Status = status301;
                    Response.AddHeader ("Location", redirectUrl);
                    Response.End ();

                    return;
                }
            }
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}