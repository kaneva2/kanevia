<%@ Page Language="C#"  MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="true" CodeBehind="kaneva-virtual-reality-platform.aspx.cs" Inherits="KlausEnt.KEP.Developer.kaneva_virtual_reality_platform" %>

<asp:Content ID="cnt_virtualPlatform" runat="server" ContentPlaceHolderID="cph_Body">
	<div id="content">
		<div id="main">
			
			<div id="main_content">
			
				<h1 class="five png">The Elements</h1>
				<p>Whether you're looking to develop a focused 3D experience or an entire virtual world, the <span class="highlight">Kaneva Star Platform</span> provides the tools and support you need to quickly and cost effectively create a 3D experience to connect with your constituents in entirely new ways.</p>
				<p><img class="png" src="images/platform_elements.png" alt="platform_elements.png" width="250" height="220" border="0" align="right" style="position: relative; right: -10px; top:-20px;" hspace="10" />The Star Platform not only features social networking, media sharing and the next level virtual interaction (real gameplay), but also delivers an array of critical services designed to help market, manage and support your world.</p>
				<p>Star builders come in many shapes and sizes. Kaneva's platform is designed to be used by everyone from consumers, developers and studios, to major brands, corporations and educational institutions.</p>
				<p>And because each Star is part of an interconnected universe, builders benefit from being part of a broader ecosystem of 3D experiences and worlds.</p>
			 </div>
			<div id="sidebar">
			<div id="sidebar_content" class="component_sidbar">
				<h2>Components of the Kaneva Star Platform include:</h2>
					<p><span class="highlight">The Kaneva Star Studio</span> is a Windows application that allows you to create your own Star, import and "rig" 3D elements, and design interactive elements. The output of the Star Studio is a compiled distribution, ready to be deployed to a Star Server.</p>
					<p><span class="highlight">The Kaneva Star Server</span> is a feature-packed server that allows groups to host their own Star as part of an open, interconnected network of 3D places (rooms, worlds, islands, etc.). Star Server runs on your own hardware, so your community will be unaffected by other Stars. It is also highly scalable, able to support up to millions of simultaneous players.</p>
					<p><span class="highlight">The Kaneva Star Explorer</span> is a Windows client that allows users to travel from Star to Star in 3D, and interact with their environment. Star Explorer brings the best elements of a web browser to the immersive environment of 3D gameplay.</p>
			</div>
		</div>
		
		</div>
		<div id="nextPage" class="kanevaorigins"><a href="worldwide-3d-community.aspx">Next - Kaneva Star Services</a></div>
	</div>
</asp:Content>