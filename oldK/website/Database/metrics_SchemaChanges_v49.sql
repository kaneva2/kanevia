-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

﻿DROP PROCEDURE IF EXISTS metrics_SchemaChanges;

DELIMITER //

CREATE PROCEDURE metrics_SchemaChanges ()
BEGIN

	IF NOT EXISTS (SELECT 1 FROM metrics.schema_versions WHERE version = 49) THEN
  
		ALTER TABLE `metrics`.`client_runtime_performance_log` 
		ADD COLUMN `msg_move_enabled_time_sec` DOUBLE UNSIGNED NULL AFTER `ripper_detected`;
  
	END IF;

END
//
DELIMITER ;

CALL metrics_SchemaChanges();
DROP PROCEDURE IF EXISTS metrics_SchemaChanges;

