-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- Replace "kaneva" with the name of the database you're updating.
USE fame;

SELECT 'Starting Promotion... ',NOW();

-- Be sure to use the latest available version number.
\. fame_SchemaChanges_v84.sql

-- add version number
REPLACE INTO `schema_versions` ( `version`, `description`, `date_applied` ) 
	VALUES (84, 'Added and updated packets for Fame 2.0', NOW());

SELECT 'Finished Promotion... ',NOW();
