-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

﻿DROP PROCEDURE IF EXISTS experiment_SchemaChanges;

DELIMITER //

CREATE PROCEDURE experiment_SchemaChanges ()
BEGIN

	IF NOT EXISTS (SELECT 1 FROM experiment.schema_versions WHERE version = 7) THEN
  
		ALTER TABLE `experiment`.`experiments` 
		ADD COLUMN `cleanup_jira` VARCHAR(12) NULL AFTER `assignment_target`;
        
        ALTER TABLE `experiment`.`experiments` 
		CHANGE COLUMN `status` `status` ENUM('C', 'R', 'S', 'E', 'A') NOT NULL DEFAULT 'C' COMMENT 'status of the experiment: (C)reated -> (R)eady -> (S)tarted -> (E)nded -> (A)rchived' ;

  
	END IF;

END
//
DELIMITER ;

CALL experiment_SchemaChanges();
DROP PROCEDURE IF EXISTS experiment_SchemaChanges;

