-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS wok_SchemaChanges;

DELIMITER //

CREATE PROCEDURE wok_SchemaChanges ()
BEGIN

  DECLARE __rollback BOOL DEFAULT 0;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET __rollback = 1;

  IF NOT EXISTS (SELECT 1 FROM wok.schema_versions WHERE version = 306) THEN
  
	-- Wrap updates in a single transaction
    START TRANSACTION;

		-- New game items: wok.items
		INSERT IGNORE INTO wok.items VALUES
		('1000230', 'Game Activator', 'Create the game worlds', '1100', '0', '-1', '0', '211', '4230696', '1', '1', '1', '0', '256', NULL, '0', '5', '0', '0', '0', '0', '0', '-1', '0', '2016-10-11 17:10:11');
		
		-- New game items: shopping.items_web
		INSERT IGNORE INTO shopping.items_web VALUES
		('1000230', '0', '', '', '', '', '5259919/4276230/2398181_sm.jpg', '5259919/4276230/2398181_me.jpg', '5259919/4276230/2398181_la.jpg', '5259919/4276230/2398181_ad.jpg', '0', '', '0', '0', '0', '0', '0', '1440', '0', '0', '0', '0', 'Game Activator', '2016-10-11 17:10:02', '0', '2016-10-11 17:10:02', '', '0', '0');
		
		-- New game items: wok.item_parameters
		INSERT IGNORE INTO wok.item_parameters VALUES 
		('1000230', '211', '0');
		
		-- New game items: wok.snapshot_script_game_items
		INSERT IGNORE INTO wok.snapshot_script_game_items VALUES
		('1000230', '4276230', 'Game Activator', 'tutorial', '1', 'Common');
		
		-- New game items: wok.snapshot_script_game_item_properties
		INSERT IGNORE INTO wok.snapshot_script_game_item_properties VALUES
		('1000230', 'behavior', 'shortcut'),
		('1000230', 'description', 'Create the game worlds');
		
		-- New game items: wok.default_script_game_items
		INSERT IGNORE INTO wok.default_script_game_items VALUES
		('410', '1000230', '2016-10-11 00:00:00', NULL);
		
		-- New game items: bundle wok.items N/A
		-- New game items: bundle shopping.items_web N/A
		-- New game items: bundle wok.item_parameters N/A
		-- New game items: wok.bundle_items N/A

		-- Add new game items to existing worlds
		INSERT IGNORE INTO wok.script_game_items
		SELECT cd.zone_instance_id, cd.zone_type, dsgi.game_item_id, dsgi.game_item_glid, sgi.glid, sgi.name, sgi.item_type, sgi.level, sgi.rarity
		FROM wok.script_game_framework_settings cd, wok.default_script_game_items dsgi, wok.snapshot_script_game_items sgi
		WHERE cd.framework_enabled = 'T' 
			AND sgi.game_item_glid = dsgi.game_item_glid
			AND dsgi.game_item_glid IN (1000230);
				
		-- Add new game items to shop worlds
		INSERT IGNORE INTO wok.starting_script_game_items
		SELECT cd.template_glid, dsgi.game_item_id, dsgi.game_item_glid, sn.name
		FROM wok.starting_script_game_framework_settings cd, wok.default_script_game_items dsgi, wok.snapshot_script_game_items sn
		WHERE cd.framework_enabled = 'T' 
			AND sn.game_item_glid = dsgi.game_item_glid
			AND dsgi.game_item_glid IN (1000230);
            
	-- Finalize the transaction
	IF __rollback THEN
        ROLLBACK;
    ELSE
        COMMIT;
    END IF;

  END IF;

END
//
DELIMITER ;

CALL wok_SchemaChanges();
DROP PROCEDURE IF EXISTS wok_SchemaChanges;
