-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP VIEW IF EXISTS communities_public;

DELIMITER ;;

 CREATE VIEW `kaneva`.`communities_public` AS SELECT `com`.`community_id` AS `community_id`,`com`.`place_type_id` AS `place_type_id`,`com`.`name_no_spaces` AS `name_no_spaces`,`com`.`url` AS `url`,`com`.`creator_id` AS `creator_id`,`com`.`created_date` AS `created_date`,`com`.`last_edit` AS `last_edit`,`com`.`status_id` AS `status_id`,`com`.`email` AS `email`,`com`.`is_public` AS `is_public`,`com`.`is_adult` AS `is_adult`,`com`.`thumbnail_path` AS `thumbnail_path`,`com`.`thumbnail_small_path` AS `thumbnail_small_path`,`com`.`thumbnail_medium_path` AS `thumbnail_medium_path`,`com`.`thumbnail_large_path` AS `thumbnail_large_path`,`com`.`thumbnail_xlarge_path` AS `thumbnail_xlarge_path`,`com`.`thumbnail_square_path` AS `thumbnail_square_path`,`com`.`thumbnail_type` AS `thumbnail_type`,`com`.`allow_publishing` AS `allow_publishing`,`com`.`allow_member_events` AS `allow_member_events`,`com`.`is_personal` AS `is_personal`,`com`.`community_type_id` AS `community_type_id`,`com`.`last_update` AS `last_update`,`com`.`template_id` AS `template_id`,`com`.`over_21_required` AS `over_21_required`,`com`.`_created_date` AS `_created_date`,`com`.`has_thumbnail` AS `has_thumbnail`,`com`.`name` AS `name`,`com`.`description` AS `description`,`com`.`background_rgb` AS `background_rgb`,`com`.`background_image` AS `background_image`,`com`.`creator_username` AS `creator_username`,`com`.`keywords` AS `keywords`,`com`.`parent_community_id` AS `parent_community_id`
 FROM `kaneva`.`communities` `com` 
 WHERE ( `com`.`community_type_id` 
 IN ( 2,3,5,6) ) 
;;
DELIMITER ;