-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- Replace "kaneva" with the name of the database you're updating.
USE kaneva;

SELECT 'Starting Promotion... ',NOW();

-- Be sure to use the latest available version number.
\. kaneva_SchemaChanges_v181.sql

-- add version number
REPLACE INTO `schema_versions` ( `version`, `description`, `date_applied` ) 
	VALUES (181, 'New transaction type for automated test user balance adjustment.', NOW());

SELECT 'Finished Promotion... ',NOW();
