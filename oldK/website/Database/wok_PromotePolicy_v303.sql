-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- Replace "wok" with the name of the database you're updating.
USE wok;

SELECT 'Starting Promotion... ',NOW();

-- Be sure to use the latest available version number.
\. wok_SchemaChanges_v303.sql

-- add version number
REPLACE INTO `schema_versions` ( `version`, `description`, `date_applied` ) 
	VALUES (303, 'Logs the status of asychronous tasks.', NOW());

SELECT 'Finished Promotion... ',NOW();
