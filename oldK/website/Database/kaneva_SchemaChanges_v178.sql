-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

﻿DROP PROCEDURE IF EXISTS kaneva_SchemaChanges;

DELIMITER //

CREATE PROCEDURE kaneva_SchemaChanges ()
BEGIN

	IF NOT EXISTS (SELECT 1 FROM kaneva.schema_versions WHERE version = 178) THEN
  
		ALTER TABLE `kaneva`.`user_system_configurations` 
		ADD COLUMN `cpu_code_name` VARCHAR(256) NULL AFTER `cpu_fma4`;
  
	END IF;

END
//
DELIMITER ;

CALL kaneva_SchemaChanges();
DROP PROCEDURE IF EXISTS kaneva_SchemaChanges;

