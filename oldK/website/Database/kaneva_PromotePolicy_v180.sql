-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- Replace "kaneva" with the name of the database you're updating.
USE kaneva;

SELECT 'Starting Promotion... ',NOW();

DROP PROCEDURE IF EXISTS kaneva_SchemaChanges;

DELIMITER //

CREATE PROCEDURE kaneva_SchemaChanges ()
BEGIN

	IF NOT EXISTS (SELECT 1 FROM information_schema.COLUMNS WHERE TABLE_SCHEMA='kaneva' AND TABLE_NAME = 'web_options' AND COLUMN_NAME='updated_date') THEN

		-- Add new column with NULL default value - this sets NULL value on existing records
		ALTER TABLE `web_options` ADD `updated_date` DATETIME DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP;

		-- Modify it again with correct default value this time - this allow future inserts get timestamps
		ALTER TABLE `web_options` MODIFY `updated_date` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;

	END IF;

END
//
DELIMITER ;

CALL kaneva_SchemaChanges();
DROP PROCEDURE IF EXISTS kaneva_SchemaChanges;

-- add version number
REPLACE INTO `schema_versions` ( `version`, `description`, `date_applied` ) 
	VALUES (180, 'web_options.updated_date column for timestamp', NOW());

SELECT 'Finished Promotion... ',NOW();
