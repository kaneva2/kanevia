-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

USE wok;

SELECT 'Starting Promotion... ',NOW();

-- Remap property ID 0x8100 (SOUND_DURATION) to 0x8000 (EFFECT_DURATION)

UPDATE ugc_validation_request_details SET property_level_id=0x80 WHERE property_level_id=0x81 AND property_id=0;
UPDATE ugc_validation_properties SET name='EFFECT_DURATION', description='Effect duration in milliseconds' WHERE property_level_id=0x80 AND property_id=0;
UPDATE ugc_validation_server_rules SET property_level_id=0x80 WHERE ugc_usage_id=120 AND property_level_id=0x81 AND property_id=0;
UPDATE ugc_price_tier_upcharges SET property_level_id=0x80 WHERE ugc_usage_id=120 AND property_level_id=0x81 AND property_id=0;

DELETE FROM ugc_validation_properties WHERE property_level_id=0x81 AND property_id=0;

-- add version number
REPLACE INTO `schema_versions` ( `version`, `description`, `date_applied` ) 
	VALUES (297, 'ED-3604: UGC sound duration property ID updated', NOW());

SELECT 'Finished Promotion... ',NOW();
