-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

﻿DROP PROCEDURE IF EXISTS wok_SchemaChanges;

DELIMITER //

CREATE PROCEDURE wok_SchemaChanges ()
BEGIN

	IF NOT EXISTS (SELECT 1 FROM wok.schema_versions WHERE version = 291) THEN
  
		UPDATE `wok`.`spawn_points` SET `position_y`='-10.9' WHERE `spawn_point_id`='38';
		UPDATE `wok`.`spawn_points` SET `position_y`='-10.9' WHERE `spawn_point_id`='95';
  
	END IF;

END
//
DELIMITER ;

CALL wok_SchemaChanges();
DROP PROCEDURE IF EXISTS wok_SchemaChanges;

