-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS snapshot_zone_dynamic_objects;

DELIMITER ;;
CREATE PROCEDURE `snapshot_zone_dynamic_objects`( IN __zone_index INT, IN __zone_instance_id INT, IN __template_global_id INT )
BEGIN
	DECLARE __done, __obj_placement_id, __last_sdo_id, __global_id INT;

	DECLARE dyo_cursor CURSOR FOR
		SELECT obj_placement_id, do.global_id
		FROM dynamic_objects do
		INNER JOIN shopping.items_web iw ON iw.global_id = do.global_id
		WHERE zone_instance_id = __zone_instance_id 
			AND zone_index = __zone_index;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET __done = 1;

	
	SET __done = 0;
	OPEN dyo_cursor;
	REPEAT
		FETCH dyo_cursor INTO __obj_placement_id, __global_id;
		IF NOT __done THEN
			START TRANSACTION;

				
				INSERT INTO starting_dynamic_objects ( `active`, `zone_index`, `object_id`,`global_id`,`position_x`,`position_y`,`position_z`,
				`rotation_x`,`rotation_y`,`rotation_z`, `apartment_template_id`)
				SELECT 1, __zone_index, `object_id`, `global_id`,`position_x`,`position_y`,`position_z`,
				`rotation_x`,`rotation_y`,`rotation_z`,	__template_global_id
				FROM dynamic_objects WHERE obj_placement_id = __obj_placement_id;

				SELECT LAST_INSERT_ID() INTO __last_sdo_id;

				
				INSERT INTO starting_dynamic_object_parameters (`id`, `param_type_id`, `value`)
				SELECT __last_sdo_id, `param_type_id`, `value`
				FROM dynamic_object_parameters dop 
				WHERE obj_placement_id = __obj_placement_id;

				
				INSERT INTO starting_dynamic_object_playlists (id, global_id, asset_group_id, video_range, audio_range, volume)
				SELECT __last_sdo_id, global_id, asset_group_id, video_range, audio_range, volume
				FROM dynamic_object_playlists dopl
				WHERE zone_instance_id = __zone_instance_id 
					AND zone_index = __zone_index 
					AND global_id = __global_id
					AND obj_placement_id = __obj_placement_id;

				
				INSERT INTO starting_sound_customizations (id, pitch, gain, max_distance, roll_off, `loop`, loop_delay, dir_x, dir_y, dir_z, cone_outer_gain, cone_inner_angle, cone_outer_angle)
				SELECT __last_sdo_id, pitch, gain, max_distance, roll_off, `loop`, loop_delay, dir_x, dir_y, dir_z, cone_outer_gain, cone_inner_angle, cone_outer_angle
                FROM wok.sound_customizations
                WHERE obj_placement_id = __obj_placement_id;

				
				UPDATE wok.starting_script_game_custom_data
				SET value = REPLACE(
								REPLACE(
									REPLACE(
										REPLACE(
											REPLACE(value, ' ', ''), CONCAT('[', __obj_placement_id, ']'), CONCAT('[', __last_sdo_id, ']')
										), CONCAT('[', __obj_placement_id, ','), CONCAT('[', __last_sdo_id, ',')
									), CONCAT(',', __obj_placement_id, ','), CONCAT(',', __last_sdo_id, ',')
								), CONCAT(',', __obj_placement_id, ']'), CONCAT(',', __last_sdo_id, ']')
							)
				WHERE template_glid = __template_global_id 
					AND attribute LIKE 'objectAssociations%';

				
                UPDATE wok.snapshot_script_game_item_properties sn
                INNER JOIN wok.starting_script_game_items st ON sn.game_item_glid = st.game_item_glid
                SET sn.property_value = __last_sdo_id
		        WHERE template_glid = __template_global_id
			        AND property_name = 'waypointPID'
                    AND property_value = __obj_placement_id;
                    
				-- We normally exlude "DNC_" properties. Make an exception for pinCode.
                INSERT INTO starting_script_game_custom_data
				SELECT __template_global_id, CONCAT('DNC_pinCode_', __last_sdo_id), value
				FROM script_game_custom_data
				WHERE attribute = CONCAT('DNC_pinCode_', __obj_placement_id)
					AND zone_instance_id = __zone_instance_id 
					AND zone_type = zoneType(__zone_index);

			COMMIT;
		END IF;
	UNTIL __done END REPEAT;
	CLOSE dyo_cursor;
	  					
END
;;
DELIMITER ;