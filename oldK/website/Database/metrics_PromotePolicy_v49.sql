-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- Replace "metrics" with the name of the database you're updating.
USE metrics;

SELECT 'Starting Promotion... ',NOW();

-- Be sure to use the latest available version number.
\. metrics_SchemaChanges_v49.sql

-- add version number
REPLACE INTO `schema_versions` ( `version`, `description`, `date_applied` ) 
	VALUES (49, 'Add msg metrics time column to client_performance_metrics.', NOW());

SELECT 'Finished Promotion... ',NOW();
