-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

﻿DROP PROCEDURE IF EXISTS kaneva_SchemaChanges;

DELIMITER //

CREATE PROCEDURE kaneva_SchemaChanges ()
BEGIN

	IF NOT EXISTS (SELECT 1 FROM kaneva.schema_versions WHERE version = 176) THEN
  
		CREATE TABLE `child_home_worlds` (
			`community_id` int(11) NOT NULL,   
			PRIMARY KEY (`community_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='stores the home worlds that are child zones to k-town';
  
	END IF;

END
//
DELIMITER ;

CALL kaneva_SchemaChanges();
DROP PROCEDURE IF EXISTS kaneva_SchemaChanges;

