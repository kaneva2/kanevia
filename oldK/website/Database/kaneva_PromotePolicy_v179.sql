-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- Replace "kaneva" with the name of the database you're updating.
USE kaneva;

SELECT 'Starting Promotion... ',NOW();

DROP PROCEDURE IF EXISTS kaneva_SchemaChanges;

DELIMITER //

CREATE PROCEDURE kaneva_SchemaChanges ()
BEGIN

	IF NOT EXISTS (SELECT 1 FROM information_schema.TABLES WHERE TABLE_SCHEMA='kaneva' AND TABLE_NAME = 'developer_auth_dropbox') THEN
  
		CREATE TABLE `developer_auth_dropbox`(
			`id` VARCHAR(50) PRIMARY KEY,
			`system_name` VARCHAR(256) NOT NULL,
			`content` VARBINARY(500) NOT NULL,
			`requester_id` INT NOT NULL,
			`requested_date` DATETIME NOT NULL,
			`expired_date` DATETIME NOT NULL);

	END IF;

END
//
DELIMITER ;

CALL kaneva_SchemaChanges();
DROP PROCEDURE IF EXISTS kaneva_SchemaChanges;

-- add version number
REPLACE INTO `schema_versions` ( `version`, `description`, `date_applied` ) 
	VALUES (179, 'developer_auth_dropbox', NOW());

SELECT 'Finished Promotion... ',NOW();
