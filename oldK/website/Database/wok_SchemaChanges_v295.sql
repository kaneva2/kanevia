-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS wok_SchemaChanges;

DELIMITER //

CREATE PROCEDURE wok_SchemaChanges ()
BEGIN

  DECLARE __rollback BOOL DEFAULT 0;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET __rollback = 1;

  IF NOT EXISTS (SELECT 1 FROM wok.schema_versions WHERE version = 295) THEN
  
	-- Wrap updates in a single transaction
    START TRANSACTION;
  
		-- New game items: wok.items
		INSERT IGNORE INTO wok.items VALUES
		('1000224', 'Pathing Waypoint', 'Waypoint used by system objects.', '1100', '0', '-1', '0', '211', '4230696', '1', '1', '1', '0', '256', NULL, '0', '5', '0', '0', '0', '0', '0', '-1', '0', NOW());
		
		-- New game items: shopping.items_web
		INSERT IGNORE INTO shopping.items_web VALUES
		('1000224', '0', '', '', '', '', '5259919/4276230/2398181_sm.jpg', '5259919/4276230/2398181_me.jpg', '5259919/4276230/2398181_la.jpg', '5259919/4276230/2398181_ad.jpg', '0', '', '0', '0', '0', '0', '0', '1440', '0', '0', '0', '0', 'Waypoint', NOW(), '0', NOW(), '', '0', '0');
		
		-- New game items: wok.item_parameters
		INSERT IGNORE INTO wok.item_parameters VALUES 
		('1000224', '211', '0');
		
		-- New game items: wok.snapshot_script_game_items
		INSERT IGNORE INTO wok.snapshot_script_game_items VALUES
		('1000224', '4465631', 'Pathing Waypoint', 'waypoint', '1', 'Common');
		
		-- New game items: wok.snapshot_script_game_item_properties
		INSERT IGNORE INTO wok.snapshot_script_game_item_properties VALUES
		('1000224', 'behavior', 'pathWaypoint'),
		('1000224', 'description', 'Waypoint used by pathing objects.');
		
		-- New game items: wok.default_script_game_items
		INSERT IGNORE INTO wok.default_script_game_items VALUES
		('405', '1000224', NOW(), NULL);
		
		-- New game items: bundle wok.items (N/A)
		-- New game items: bundle shopping.items_web (N/A)
		-- New game items: bundle wok.item_parameters (N/A)
		-- New game items: wok.bundle_items (N/A)
		
		-- Add new game items to existing worlds
		INSERT IGNORE INTO wok.script_game_items
		SELECT cd.zone_instance_id, cd.zone_type, dsgi.game_item_id, dsgi.game_item_glid, sgi.glid, sgi.name, sgi.item_type, sgi.level, sgi.rarity
		FROM wok.script_game_framework_settings cd, wok.default_script_game_items dsgi, wok.snapshot_script_game_items sgi
		WHERE cd.framework_enabled = 'T' 
			AND sgi.game_item_glid = dsgi.game_item_glid
			AND dsgi.game_item_glid IN (1000224);
				
		-- Add new game items to shop worlds
		INSERT IGNORE INTO wok.starting_script_game_items
		SELECT cd.template_glid, dsgi.game_item_id, dsgi.game_item_glid, 'Pathing Waypoint'
		FROM wok.starting_script_game_framework_settings cd, wok.default_script_game_items dsgi
		WHERE cd.framework_enabled = 'T' 
			AND dsgi.game_item_glid IN (1000224);
            
		-- Modified items: NPC Female
        UPDATE wok.snapshot_script_game_item_properties 
        SET property_value = '{"actorName":"NPC Female","actorTitle":"Information","dialog":"Welcome to Kaneva!","animation":4246971,"walkSpeed":10,"runSpeed":25,"attackRange":15,"wanderRange":25,"repEnabled":false,"repScale":"Normal","repUnlock":"Acquaintance","giftSpouse":404,"giftInterval":1440,"combatEnabled":false,"animations":{"idleAnim":4246971,"walkAnim":4246986,"runAnim":4246983,"attackAnim":3317748,"deathAnim":4154955}}'
        WHERE game_item_glid = 1000113 
        AND property_name = 'behaviorParams';
        
        -- Modified items: NPC Male
        UPDATE wok.snapshot_script_game_item_properties 
        SET property_value = '{"animation":4246971,"actorName":"NPC Male","actorTitle":"Information","dialog":"Welcome to Kaneva!","walkSpeed":10,"runSpeed":25,"attackRange":15,"wanderRange":25,"repEnabled":false,"repScale":"Normal","repUnlock":"Acquaintance","giftSpouse":404,"giftInterval":1440,"combatEnabled":false,"animations":{"idleAnim":4246971,"walkAnim":4246986,"runAnim":4246983,"attackAnim":3317748,"deathAnim":4154955}}'
        WHERE game_item_glid = 1000114 
        AND property_name = 'behaviorParams';

		-- Modified items: Vendor
		UPDATE wok.snapshot_script_game_item_properties 
        SET property_value = '{"actorName":"Vendor","animation":4246971,"walkSpeed":10,"runSpeed":25,"attackRange":15,"wanderRange":25,"repEnabled":false,"repScale":"Normal","repUnlock":"Acquaintance","giftSpouse":404,"giftInterval":1440,"combatEnabled":false,"animations":{"idleAnim":4246971,"walkAnim":4246986,"runAnim":4246983,"attackAnim":3317748,"deathAnim":4154955}}'
        WHERE game_item_glid = 1000159 
        AND property_name = 'behaviorParams';
        
        -- Modified items: Timed Vendor
        UPDATE wok.snapshot_script_game_item_properties 
        SET property_value = '4246971'
        WHERE game_item_glid = 1000172 
        AND property_name = 'animation';

		UPDATE wok.snapshot_script_game_item_properties 
        SET property_value = '{"timed":true,"lastVID":4,"walkSpeed":10,"runSpeed":25,"attackRange":15,"wanderRange":25,"repEnabled":false,"repScale":"Normal","repUnlock":"Acquaintance","giftSpouse":404,"giftInterval":1440,"combatEnabled":false,"animations":{"idleAnim":4246971,"walkAnim":4246986,"runAnim":4246983,"attackAnim":3317748,"deathAnim":4154955}}'
        WHERE game_item_glid = 1000172 
        AND property_name = 'behaviorParams';
        
        -- Modified items: Random Loot Vendor
        UPDATE wok.snapshot_script_game_item_properties 
        SET property_value = '{"actorName":"Random Loot Vendor","actorTitle":"Random Loot Vendor","random":true,"walkSpeed":10,"runSpeed":25,"attackRange":15,"wanderRange":25,"repEnabled":false,"repScale":"Normal","repUnlock":"Acquaintance","giftSpouse":404,"giftInterval":1440,"combatEnabled":false,"animations":{"idleAnim":208,"walkAnim":4246986,"runAnim":4246983,"attackAnim":3317748,"deathAnim":4154955}}'
        WHERE game_item_glid = 1000189 
        AND property_name = 'behaviorParams';

		-- Modified items: Credits/Rewards Vendor
        UPDATE wok.snapshot_script_game_item_properties 
        SET property_value = '{"actorName":"Credits/Rewards Vendor","actorTitle":"Credits/Rewards Vendor","credits":true,"animation":4246971,"walkSpeed":10,"runSpeed":25,"attackRange":15,"wanderRange":25,"repEnabled":false,"repScale":"Normal","repUnlock":"Acquaintance","giftSpouse":404,"giftInterval":1440,"combatEnabled":false,"animations":{"idleAnim":4246971,"walkAnim":4246986,"runAnim":4246983,"attackAnim":3317748,"deathAnim":4154955}}'
        WHERE game_item_glid = 1000191 
        AND property_name = 'behaviorParams';
        
        -- Modified items: New bundle glids
        INSERT IGNORE INTO wok.item_parameters VALUES
        ('1000208', '34', '1500146'),
		('1000178', '34', '1500120');
        
        -- Modified items: New bundle items
        INSERT IGNORE INTO wok.bundle_items VALUES
        ('1500036', '4697922', '1'),
		('1500072', '4697922', '1'),
		('1500073', '4697683', '1'),
		('1500074', '4697922', '1'),
		('1500146', '3356306', '1'),
		('1500146', '4628821', '1'),
		('1500102', '208', '1'),
		('1500102', '438', '1'),
		('1500102', '508', '1'),
		('1500102', '3317748', '1'),
		('1500102', '4154955', '1'),
		('1500103', '208', '1'),
		('1500103', '438', '1'),
		('1500103', '508', '1'),
		('1500103', '3317748', '1'),
		('1500103', '4154955', '1'),
		('1500120', '208', '1'),
		('1500120', '438', '1'),
		('1500120', '508', '1'),
		('1500120', '3317748', '1'),
		('1500120', '4154955', '1'),
		('1500120', '4310445', '1'),
		('1500128', '208', '1'),
		('1500128', '438', '1'),
		('1500128', '508', '1'),
		('1500128', '3317748', '1'),
		('1500128', '4154955', '1'),
		('1500152', '4697922', '1');

		-- Modified items: Wedding Ring
		UPDATE wok.default_script_game_items 
		SET automatic_insertion_start = NOW() 
		WHERE game_item_glid = 1000223;
        
        -- Modified items: Deleted bundle items
        DELETE FROM wok.bundle_items WHERE bundle_global_id = 1500036 AND item_global_id = 4465627;
        DELETE FROM wok.bundle_items WHERE bundle_global_id = 1500072 AND item_global_id = 4465626;
        DELETE FROM wok.bundle_items WHERE bundle_global_id = 1500073 AND item_global_id = 4465628;
        DELETE FROM wok.bundle_items WHERE bundle_global_id = 1500074 AND item_global_id = 4465629;

            
	-- Finalize the transaction
	IF __rollback THEN
        ROLLBACK;
    ELSE
        COMMIT;
    END IF;

  END IF;

END
//
DELIMITER ;

CALL wok_SchemaChanges();
DROP PROCEDURE IF EXISTS wok_SchemaChanges;