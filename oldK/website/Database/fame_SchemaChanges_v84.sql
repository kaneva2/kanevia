-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

﻿DROP PROCEDURE IF EXISTS fame_SchemaChanges;

DELIMITER //

CREATE PROCEDURE fame_SchemaChanges ()
BEGIN

	IF NOT EXISTS (SELECT 1 FROM fame.schema_versions WHERE version = 84) THEN
  
		-- Log in 2 days in a row
		UPDATE `fame`.`packets` SET points_earned = 0, rewards = 0 WHERE packet_id = 405;
		-- Log in 3 days in a row
		UPDATE `fame`.`packets` SET points_earned = 0, rewards = 0 WHERE packet_id = 443;
		-- In world for 15 minuts
		UPDATE `fame`.`packets` SET points_earned = 0, rewards = 0 WHERE packet_id = 485;
		-- Log in 1 day in a row
		UPDATE `fame`.`packets` SET points_earned = 0, rewards = 0 WHERE packet_id = 490;

		-- Place 1 objects
		UPDATE `fame`.`packets` SET time_interval = 3, max_per_day = 10 WHERE packet_id = 600;
		-- Craft 1 objects
		UPDATE `fame`.`packets` SET time_interval = 3, max_per_day = 10 WHERE packet_id = 596;
		-- Kill 1 monsters
		UPDATE `fame`.`packets` SET time_interval = 3, max_per_day = 10, badge_id = 0 WHERE packet_id = 592;
		-- Harvest 2 objects
		UPDATE `fame`.`packets` SET time_interval = 3, max_per_day = 10 WHERE packet_id = 612;

		-- 484
		INSERT INTO `fame`.`packets` VALUES (704, 1, 'Buy an item', 0, 0, 2, 1, 10, 32, 1, 0, 0);

		-- 440
		INSERT INTO `fame`.`packets` VALUES (705, 1, 'Send a Blast', 0, 0, 3, 0, 1, 32, 1, 0, 0);

		-- 478
		INSERT INTO `fame`.`packets` VALUES (706, 1, 'Upload Media 10 times', 0, 0, 2, 10, 1, 32, 1, 0, 0);

		-- 425
		INSERT INTO `fame`.`packets` VALUES (707, 1, 'View Someone\'s Profile', 0, 0, 1, 0, 0, 32, 1, 0, 0);

	END IF;

END
//
DELIMITER ;

CALL fame_SchemaChanges();
DROP PROCEDURE IF EXISTS fame_SchemaChanges;

