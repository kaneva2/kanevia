-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

﻿DROP PROCEDURE IF EXISTS kaneva_SchemaChanges;

DELIMITER //

CREATE PROCEDURE kaneva_SchemaChanges ()
BEGIN

	IF NOT EXISTS (SELECT 1 FROM kaneva.schema_versions WHERE version = 175) THEN
  
		ALTER TABLE `kaneva`.`communities` 
		ADD COLUMN `parent_community_id` INT(11) NULL AFTER `keywords`;

		ALTER TABLE `kaneva`.`communities` 
		ADD INDEX `idx_parent_id` (`parent_community_id`);

		-- Add new transaction type for linked zone purchase
		INSERT INTO `kaneva`.`transaction_type` VALUES (41, 'Slot for Linked World', 'TT_LINKED_WORLD_SLOT');
  
	END IF;

END
//
DELIMITER ;

CALL kaneva_SchemaChanges();
DROP PROCEDURE IF EXISTS kaneva_SchemaChanges;

