-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

-- Replace "experiment" with the name of the database you're updating.
USE experiment;

SELECT 'Starting Promotion... ',NOW();

-- Be sure to use the latest available version number.
\. experiment_SchemaChanges_v7.sql

-- add version number
REPLACE INTO `schema_versions` ( `version`, `description`, `date_applied` ) 
	VALUES (7, 'Add JIRA cleanup issue num to experiments.', NOW());

SELECT 'Finished Promotion... ',NOW();
