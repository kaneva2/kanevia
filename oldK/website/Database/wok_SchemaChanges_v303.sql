-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS wok_SchemaChanges;

DELIMITER //

CREATE PROCEDURE wok_SchemaChanges ()
BEGIN

  DECLARE __rollback BOOL DEFAULT 0;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET __rollback = 1;

  IF NOT EXISTS (SELECT 1 FROM wok.schema_versions WHERE version = 303) THEN
  
	 CREATE TABLE `wok`.`async_task_status_log` (
    `task_key` VARCHAR(256) NOT NULL,
    `updated_on` DATETIME NOT NULL,
    `status` VARCHAR(256) NOT NULL,
    `message` TEXT NOT NULL,
    PRIMARY KEY (`task_key`))
   COMMENT = 'Logs the status of asychronous tasks.';

  END IF;

END
//
DELIMITER ;

CALL wok_SchemaChanges();
DROP PROCEDURE IF EXISTS wok_SchemaChanges;
