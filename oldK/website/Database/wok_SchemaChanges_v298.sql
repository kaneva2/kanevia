-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

﻿DROP PROCEDURE IF EXISTS wok_SchemaChanges;

DELIMITER //

CREATE PROCEDURE wok_SchemaChanges ()
BEGIN

	IF NOT EXISTS (SELECT 1 FROM wok.schema_versions WHERE version = 298) THEN
  
		-- Create tutorials table
		CREATE TABLE `tutorials` (
		  `tutorial_id` varchar(50) NOT NULL COMMENT 'unique id of the tutorial',
		  `name` varchar(100) NOT NULL DEFAULT '',
		  `test_group_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'unique id of the a/b test group',
		  `is_active` smallint(1) unsigned NOT NULL DEFAULT '0',
		  PRIMARY KEY (`tutorial_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='stores the different tutorials';
    
		-- Create tutorial_pages table
		CREATE TABLE `tutorial_pages` (
		  `tutorial_page_id` varchar(50) NOT NULL COMMENT 'unique id of the tutorial page',
		  `file_name` varchar(100) NOT NULL COMMENT 'name of the tutorial xml file',
		  PRIMARY KEY (`tutorial_page_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='stores the different tutorial pages';
   
		-- Create tutorial_attributes table 
		CREATE TABLE `tutorial_attributes` (
		  `attribute_id` varchar(50) NOT NULL COMMENT 'unique id of the attribute',
		  `name` varchar(50) NOT NULL DEFAULT '',
		  `description` varchar(255) NOT NULL DEFAULT '',
		  PRIMARY KEY (`attribute_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='descriptive table for attributes/tutorial pages';

		-- Create tutorial_page_set table
		CREATE TABLE `tutorial_page_set` (
		  `tutorial_id` varchar(50) NOT NULL COMMENT 'unique id of the tutorial',
		  `tutorial_page_id` varchar(50) NOT NULL COMMENT 'unique id of the tutorial page',
		  `page_order` smallint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'order the page appears in tutorial',
		  PRIMARY KEY (`tutorial_id`,`tutorial_page_id`),
		  CONSTRAINT `tps_tutorial_id_fk` FOREIGN KEY (`tutorial_id`) REFERENCES `tutorials` (`tutorial_id`) ON DELETE CASCADE ON UPDATE CASCADE,
		  CONSTRAINT `tps_tutorial_page_id_fk` FOREIGN KEY (`tutorial_page_id`) REFERENCES `tutorial_pages` (`tutorial_page_id`) ON DELETE CASCADE ON UPDATE CASCADE
		  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='stores the different tutorial pages per tutorial';
   
		-- Create tutorial_page_attributes table
		CREATE TABLE `tutorial_page_attributes` (
		  `tutorial_id` varchar(50) NOT NULL COMMENT 'unique id of the tutorial',
		  `tutorial_page_id` varchar(50) NOT NULL COMMENT 'unique id of the tutorial page',
		  `attribute_id` varchar(50) NOT NULL COMMENT 'unique id of the attribute',
		  `attribute_value` varchar(1000) NOT NULL DEFAULT '' COMMENT 'arbitrary value, meaning depends on attribute id',
		  PRIMARY KEY (`tutorial_id`,`tutorial_page_id`,`attribute_id`),
		  CONSTRAINT `tpa_tutorial_id_fk2` FOREIGN KEY (`tutorial_id`) REFERENCES `tutorials` (`tutorial_id`) ON DELETE CASCADE ON UPDATE CASCADE,
		  CONSTRAINT `tpa_tutorial_page_id_fk2` FOREIGN KEY (`tutorial_page_id`) REFERENCES `tutorial_pages` (`tutorial_page_id`) ON DELETE CASCADE ON UPDATE CASCADE,
		  CONSTRAINT `tpa_attribute_id_fk` FOREIGN KEY (`attribute_id`) REFERENCES `tutorial_attributes` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='attributes for each tutorial page';
  

	END IF;

END
//
DELIMITER ;

CALL wok_SchemaChanges();
DROP PROCEDURE IF EXISTS wok_SchemaChanges;

