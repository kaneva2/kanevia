-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

USE wok;

SELECT 'Starting Promotion... ',NOW();

 \. sp#snapshot_zone_dynamic_objects.sql
 \. sp#change_zone_map.sql
 \. sp#import_channel.sql

-- add version number
REPLACE INTO `schema_versions` ( `version`, `description`, `date_applied` ) 
	VALUES (294, 'Fixed a bug with pin codes in templates.', NOW());

SELECT 'Finished Promotion... ',NOW();