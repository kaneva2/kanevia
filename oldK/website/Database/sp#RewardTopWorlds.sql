-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS RewardTopWorlds;

DELIMITER ;;
CREATE PROCEDURE RewardTopWorlds ()
BEGIN

  DECLARE finished INTEGER DEFAULT 0;
  DECLARE creator_id INTEGER DEFAULT 0;

  -- declare cursor for tour rewards
  DEClARE tour_rewards_cursor CURSOR FOR 
  SELECT pc.creator_id
  FROM  wok.worlds_top_tour_cache pc 
  WHERE loot_spawners > 10
  AND pc.number_of_diggs_past_7_days > 4 
  ORDER BY number_of_diggs_past_7_days DESC
  LIMIT 6;
 
  -- declare NOT FOUND handler
  DECLARE CONTINUE HANDLER 
  FOR NOT FOUND SET finished = 1;

  OPEN tour_rewards_cursor;
  
  get_tour_rewards: LOOP
    FETCH tour_rewards_cursor INTO creator_id;
    IF finished = 1 THEN 
    LEAVE get_tour_rewards;
    END IF;
 
    -- rewards
    CALL kaneva.apply_transaction_to_user_balance(creator_id, 25000, 34, 'GPOINT', @returnCode, @userBalance, @tranId);
  END LOOP get_tour_rewards;
 
  CLOSE tour_rewards_cursor;
  
END
;;
DELIMITER ;
