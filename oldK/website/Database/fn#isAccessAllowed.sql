-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP FUNCTION IF EXISTS isAccessAllowed;

DELIMITER ;;
CREATE FUNCTION `isAccessAllowed`( sourceKanevaId INT, destKanevaId INT, instanceId INT, accessType INT) RETURNS tinyint(1)
    READS SQL DATA
BEGIN
-- return 1 if allowed, 0 if not
	DECLARE returnCode INT(11) DEFAULT NULL;
	DECLARE accountType INT(11) DEFAULT 0;
    DECLARE statusId INT(11) DEFAULT 0;
	DECLARE communityId INT(11) DEFAULT 0;
	DECLARE isPublic CHAR(1) DEFAULT 'N';
	DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;

	IF accessType = 3 THEN 
		-- Trying to access a .home
		SELECT c.community_id, c.is_public INTO communityId, isPublic
		FROM players p 
		INNER JOIN channel_zones cz ON cz.zone_instance_id = p.player_id AND cz.zone_type = 3
		INNER JOIN kaneva.communities c ON c.creator_id = p.kaneva_user_id AND c.community_type_id = 5
		WHERE p.kaneva_user_id = destKanevaId;

	ELSEIF accessType = 6 THEN 
		-- Trying to access a .channel
		SELECT c.community_id, c.is_public INTO communityId, isPublic
		FROM kaneva.communities c
		WHERE c.community_id = instanceId;

	END IF;

	-- If community is public, let everyone in
	IF isPublic = 'Y' THEN
		SET returnCode = 1;
	ELSE
		-- Check access
		SELECT account_type_id, status_id INTO accountType, statusId
		FROM kaneva.community_members
		WHERE user_id=sourceKanevaId AND community_id=communityId;

		-- If community is private, let only owner and active members in
		IF isPublic = 'N' AND accountType IN (1, 2, 3) AND statusId = 1 THEN
			SET returnCode = 1;
		-- If community is internal (owner only) let only owners and moderators in
		ELSEIF isPublic = 'I' AND accountType IN (1, 2) THEN
			SET returnCode = 1;
		END IF;
	END IF;

	CALL logDebugMsg( 'isAccessAllowed', CONCAT( 'sourceKanevaId:', CAST( IFNULL(sourceKanevaId,'null') AS CHAR), ' destKanevaId:', CAST( IFNULL(destKanevaId,'null') AS CHAR), ' instanceId:', CAST( IFNULL(instanceId,'null') AS CHAR), ' accessType:', CAST( IFNULL(accessType,'null') AS CHAR), ' result:', IFNULL(returnCode, 'false') ) );

	RETURN returnCode IS NOT NULL;
END
;;
DELIMITER ;