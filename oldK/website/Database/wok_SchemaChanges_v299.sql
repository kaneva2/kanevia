-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

DROP PROCEDURE IF EXISTS wok_SchemaChanges;

DELIMITER //

CREATE PROCEDURE wok_SchemaChanges ()
BEGIN

  DECLARE __rollback BOOL DEFAULT 0;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET __rollback = 1;

  IF NOT EXISTS (SELECT 1 FROM wok.schema_versions WHERE version = 299) THEN
  
	-- Wrap updates in a single transaction
    START TRANSACTION;

		-- New game item type for tutorial.
		INSERT IGNORE INTO wok.script_game_item_types VALUES ('tutorial', 'Items used in the tutorial, but not part of the overall system.', 'F');
  
		-- New game items: wok.items
		INSERT IGNORE INTO wok.items VALUES
		('1000225', 'Starter Home', 'The guide to the magical world of Kaneva', '1100', '0', '-1', '0', '211', '4230696', '1', '1', '1', '0', '512', NULL, '0', '5', '0', '0', '0', '0', '0', '-1', '0', '2016-09-22 11:20:19'),
		('1000226', 'Tutorial Land Claim Flag', 'Allows Players to stake their own land claims upon which only they can build!', '1100', '0', '-1', '0', '211', '4230696', '1', '1', '1', '0', '512', NULL, '0', '5', '0', '0', '0', '0', '0', '-1', '0', '2016-09-22 11:12:32'),
		('1000227', 'Teleporter to K-Town', 'Rip your atoms apart and assemble them in another place with this linked teleporter system.', '1100', '0', '-1', '0', '211', '4230696', '1', '1', '1', '0', '512', NULL, '0', '5', '0', '0', '0', '0', '0', '-1', '0', '2016-09-22 11:21:42'),
		('1000228', 'Teleporter to Game Home', 'Rip your atoms apart and assemble them in another place with this linked teleporter system.', '1100', '0', '-1', '0', '211', '4230696', '1', '1', '1', '0', '512', NULL, '0', '5', '0', '0', '0', '0', '0', '-1', '0', '2016-09-22 10:58:34'),
		('1000229', 'Platform Placable', 'Default object for pathing object', '1100', '0', '-1', '0', '211', '4230696', '1', '1', '1', '0', '256', NULL, '0', '5', '0', '0', '0', '0', '0', '-1', '0', '2016-09-29 11:28:39');
		
		-- New game items: shopping.items_web
		INSERT IGNORE INTO shopping.items_web VALUES
		('1000225', '0', '', '', '', '', '', '', '', '', '0', '', '0', '0', '0', '0', '0', '1440', '0', '0', '0', '0', 'Tutorial Guide Eva', '2016-08-31 17:10:26', '0', '2016-08-31 17:10:26', '', '0', '0'),
		('1000226', '0', '', '', '', '', '4230696/4612626/playerLandClaimFlag_thumbnail_270_sm.jpg', '4230696/4612626/playerLandClaimFlag_thumbnail_270_me.jpg', '4230696/4612626/playerLandClaimFlag_thumbnail_270_la.jpg', '4230696/4612626/playerLandClaimFlag_thumbnail_270_ad.jpg', '0', '', '0', '0', '0', '0', '0', '1440', '0', '0', '0', '0', 'Tutorial Land Claim Flag', '2016-09-16 13:43:24', '0', '2016-09-16 13:43:24', '', '0', '0'),
		('1000227', '0', '', '', '', '', '4230696/4496120/teleporterPad_thumbnail_150_sm.jpg', '4230696/4496120/teleporterPad_thumbnail_150_me.jpg', '4230696/4496120/teleporterPad_thumbnail_150_la.jpg', '4230696/4496120/teleporterPad_thumbnail_150_ad.jpg', '0', '', '0', '0', '0', '0', '0', '1440', '0', '0', '0', '0', 'Teleporter to K-Town', '2016-09-19 14:03:22', '0', '2016-09-19 14:03:22', '', '0', '0'),
		('1000228', '0', '', '', '', '', '4230696/4496120/teleporterPad_thumbnail_150_sm.jpg', '4230696/4496120/teleporterPad_thumbnail_150_me.jpg', '4230696/4496120/teleporterPad_thumbnail_150_la.jpg', '4230696/4496120/teleporterPad_thumbnail_150_ad.jpg', '0', '', '0', '0', '0', '0', '0', '1440', '0', '0', '0', '0', 'Teleporter to Game Home', '2016-09-19 14:07:08', '0', '2016-09-19 14:07:08', '', '0', '0'),
		('1000229', '0', '', '', '', '', '4230696/4497863/MovingPlatform_thumbnail1_150_sm.jpg', '4230696/4497863/MovingPlatform_thumbnail1_150_me.jpg', '4230696/4497863/MovingPlatform_thumbnail1_150_la.jpg', '4230696/4497863/MovingPlatform_thumbnail1_150_ad.jpg', '0', '', '0', '0', '0', '0', '0', '1440', '0', '0', '0', '0', 'Pathing Object Platform', '2016-09-29 10:30:57', '1', '2016-09-29 10:30:57', '', '0', '0');
		
		-- New game items: wok.item_parameters
		INSERT IGNORE INTO wok.item_parameters VALUES 
		('1000225', '211', '0'),
		('1000226', '211', '0'),
		('1000227', '211', '0'),
		('1000228', '211', '0'),
		('1000229', '34', '1500162'),
		('1000229', '211', '0');
		
		-- New game items: wok.snapshot_script_game_items
		INSERT IGNORE INTO wok.snapshot_script_game_items VALUES
		('1000225', '4680203', 'Starter Home', 'tutorial', '5', 'Rare'),
		('1000226', '4612626', 'Tutorial Land Claim Flag', 'tutorial', '1', 'Common'),
		('1000227', '4496120', 'Teleporter to K-Town', 'tutorial', '3', 'Rare'),
		('1000228', '4496120', 'Teleporter to Game Home', 'tutorial', '3', 'Rare'),
		('1000229', '4497863', 'Platform Placable', 'placeable', '4', 'Rare');
		
		-- New game items: wok.snapshot_script_game_item_properties
		INSERT IGNORE INTO wok.snapshot_script_game_item_properties VALUES
		('1000225', 'description', 'A humble house'),
		('1000225', 'targetable', 'false'),
		('1000226', 'behavior', 'claim'),
		('1000226', 'behaviorParams', '{\"height\":1000,\"width\":3000,\"depth\":3000}'),
		('1000226', 'description', 'Allows Players to stake their own land claims upon which only they can build!'),
		('1000226', 'enabled', 'true'),
		('1000226', 'flagType', 'claimFlag'),
		('1000226', 'height', '50'),
		('1000226', 'noDestruct', 'false'),
		('1000227', 'behavior', 'teleporter'),
		('1000227', 'behaviorParams', '{''idleParticle'':0,''chargeParticle'':0, ''chargeSoundID'':0, ''arriveSoundID'':0, ''triggerRadius'':10, ''instantTeleport'':true, ''specialDestination'':"K-Town"}'),
		('1000227', 'description', 'Rip your atoms apart and assemble them in another place with this linked teleporter system.'),
		('1000227', 'targetable', 'false'),
		('1000228', 'behavior', 'teleporter'),
		('1000228', 'behaviorParams', '{''idleParticle'':0,''chargeParticle'':0, ''chargeSoundID'':0, ''arriveSoundID'':0, ''triggerRadius'':10, ''instantTeleport'':true, ''specialDestination'':"Home"}'),
		('1000228', 'description', 'Rip your atoms apart and assemble them in another place with this linked teleporter system.'),
		('1000228', 'targetable', 'false'),
		('1000229', 'description', 'Default object for pathing object'),
		('1000229', 'targetable', 'false');

		
		-- New game items: wok.default_script_game_items
		INSERT IGNORE INTO wok.default_script_game_items VALUES
		('406', '1000225', NULL, NULL),
		('407', '1000226', NULL, NULL),
		('408', '1000227', NULL, NULL),
		('409', '1000228', NULL, NULL),
		('53',  '1000229', NOW(), NULL);
		
		-- New game items: bundle wok.items
		INSERT IGNORE INTO wok.items VALUES
		('1500162', 'Pathing Object Platform Bundled Items', 'Pathing Object Platform Bundled Items', '0', '0', '-1', '0', '208', '4230696', '1', '1', '1', '0', '512', NULL, '0', '5', '0', '0', '0', '0', '0', '-1', '0', '2016-09-29 10:30:57');

		-- New game items: bundle shopping.items_web
		INSERT IGNORE INTO shopping.items_web VALUES 
		('1500162', '0', '', '', '', '', '', '', '', '', '0', '', '0', '0', '0', '0', '0', '1440', '0', '0', '0', '0', '', '2016-09-29 10:30:57', '0', '2016-09-29 10:30:57', '', '0', '0');

		-- New game items: bundle wok.item_parameters
		INSERT IGNORE INTO wok.item_parameters VALUES
		('1500162', '208', '0');

		-- New game items: wok.bundle_items
		INSERT IGNORE INTO wok.bundle_items VALUES
		('1500162', '4497863', '1');

		-- Add new game items to existing worlds
		INSERT IGNORE INTO wok.script_game_items
		SELECT cd.zone_instance_id, cd.zone_type, dsgi.game_item_id, dsgi.game_item_glid, sgi.glid, sgi.name, sgi.item_type, sgi.level, sgi.rarity
		FROM wok.script_game_framework_settings cd, wok.default_script_game_items dsgi, wok.snapshot_script_game_items sgi
		WHERE cd.framework_enabled = 'T' 
			AND sgi.game_item_glid = dsgi.game_item_glid
			AND dsgi.game_item_glid IN (1000229);

		-- Add all items to K-Town r80.
		INSERT IGNORE INTO wok.script_game_items
		SELECT 1009268045, 6, dsgi.game_item_id, dsgi.game_item_glid, sgi.glid, sgi.name, sgi.item_type, sgi.level, sgi.rarity
		FROM wok.default_script_game_items dsgi, wok.snapshot_script_game_items sgi
		WHERE sgi.game_item_glid = dsgi.game_item_glid
			AND dsgi.game_item_glid IN (1000225,1000226,1000227,1000228,1000229);
				
		-- Add new game items to shop worlds
		INSERT IGNORE INTO wok.starting_script_game_items
		SELECT cd.template_glid, dsgi.game_item_id, dsgi.game_item_glid, sn.name
		FROM wok.starting_script_game_framework_settings cd, wok.default_script_game_items dsgi, wok.snapshot_script_game_items sn
		WHERE cd.framework_enabled = 'T' 
			AND sn.game_item_glid = dsgi.game_item_glid
			AND dsgi.game_item_glid IN (1000229);
            
		-- Modified items: No Flag heights 250
        UPDATE wok.snapshot_script_game_item_properties 
        SET property_value = '{"height":50,"width":250,"depth":250}'
        WHERE game_item_glid IN (
			1000150,
			1000151,
			1000152,
			1000153,
			1000154,
			1000155
		)
        AND property_name = 'behaviorParams';
        
        -- Modified items: No Flag heights 25
        UPDATE wok.snapshot_script_game_item_properties 
        SET property_value = '{"height":50,"width":25,"depth":25}'
        WHERE game_item_glid = 1000193
        AND property_name = 'behaviorParams';

		-- Modified items: No Flag heights 50
        UPDATE wok.snapshot_script_game_item_properties 
        SET property_value = '{"height":50,"width":50,"depth":50}'
        WHERE game_item_glid = 1000199
        AND property_name = 'behaviorParams';

		-- Modified items: No Flag heights 100
        UPDATE wok.snapshot_script_game_item_properties 
        SET property_value = '{"height":50,"width":100,"depth":100}'
        WHERE game_item_glid = 1000200
        AND property_name = 'behaviorParams';

		-- Delete deprecated properties
		DELETE FROM wok.snapshot_script_game_item_properties
		WHERE property_name IN ('height', 'radius')
		AND game_item_glid IN (
			1000150,
			1000151,
			1000152,
			1000153,
			1000154,
			1000155,
			1000193,
			1000199,
			1000200
		);

		-- Change name of "Platform System" to "Moving Platform System"
		UPDATE wok.snapshot_script_game_items
		SET name = 'Moving Platform System'
		WHERE game_item_glid = 1000034;

		UPDATE wok.script_game_items
		SET name = 'Moving Platform System'
		WHERE game_item_id = 43;

		UPDATE wok.items
		SET name = 'Moving Platform System'
		WHERE global_id = 1000034;

		UPDATE shopping.items_web
		SET display_name = 'Moving Platform System'
		WHERE global_id = 1000034;
            
	-- Finalize the transaction
	IF __rollback THEN
        ROLLBACK;
    ELSE
        COMMIT;
    END IF;

  END IF;

END
//
DELIMITER ;

CALL wok_SchemaChanges();
DROP PROCEDURE IF EXISTS wok_SchemaChanges;