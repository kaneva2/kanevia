-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

﻿DROP PROCEDURE IF EXISTS kaneva_SchemaChanges;

DELIMITER //

CREATE PROCEDURE kaneva_SchemaChanges ()
BEGIN

	IF NOT EXISTS (SELECT 1 FROM kaneva.schema_versions WHERE version = 181) THEN
  
		INSERT INTO kaneva.transaction_type VALUES ('42', 'Automated test user balance adjustment', 'TT_AUTO_TEST_USER_BALANCE_ADJUST');
  
	END IF;

END
//
DELIMITER ;

CALL kaneva_SchemaChanges();
DROP PROCEDURE IF EXISTS kaneva_SchemaChanges;

