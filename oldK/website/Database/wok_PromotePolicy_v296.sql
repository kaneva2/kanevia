-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

USE wok;

SELECT 'Starting Promotion... ',NOW();

 \. fn#isAccessAllowed.sql

-- add version number
REPLACE INTO `schema_versions` ( `version`, `description`, `date_applied` ) 
	VALUES (296, 'Fixed a bug that allowed pending members into Private worlds.', NOW());

SELECT 'Finished Promotion... ',NOW();