-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

--
-- Current Database: `kgp_volatile`
--
CREATE DATABASE IF NOT EXISTS `kgp_volatile` DEFAULT CHARACTER SET latin1;

USE `kgp_volatile`;

--
-- Table structure for table `dynamic_objects` 
-- (Formerly known as shard_info.dynamic_objects)
--
CREATE TABLE `dynamic_objects` (
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `summary_active_population`
-- (formerly know as wok.summary_active_population)
--
CREATE TABLE `summary_active_population` (
  `server_id` int(10) unsigned NOT NULL COMMENT 'FK to servers',
  `zone_index` int(10) unsigned NOT NULL COMMENT 'zone id w/o instance',
  `zone_type` smallint(6) NOT NULL DEFAULT '0' COMMENT 'type (apt,channel,etc)',
  `zone_instance_id` int(10) unsigned NOT NULL COMMENT 'zone instance',
  `count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'current total in zone for server',
  PRIMARY KEY (`server_id`,`zone_instance_id`,`zone_index`),
  UNIQUE KEY `sap_zone_index_fields` (`zone_index`,`zone_type`,`zone_instance_id`),
  KEY `IDX_ap_type` (`zone_type`),
  KEY `IDX_ap_zone` (`zone_index`,`zone_instance_id`),
  KEY `IDX_ap_zone2` (`zone_instance_id`,`zone_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='summary table of players in zones on servers';

--
-- Table structure for table `schema_versions`
--
CREATE TABLE `schema_versions` (
  `version` int(11) NOT NULL COMMENT 'current schema version',
  `description` varchar(250) NOT NULL COMMENT 'description of update',
  `date_applied` timestamp NOT NULL default CURRENT_TIMESTAMP COMMENT 'when version was set',
  PRIMARY KEY  (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- set the initial schema version
INSERT INTO schema_versions( `version`, `description` ) VALUES (0, 'base version' );

