-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

﻿DROP PROCEDURE IF EXISTS wok_SchemaChanges;

DELIMITER //

CREATE PROCEDURE wok_SchemaChanges ()
BEGIN

	IF NOT EXISTS (SELECT 1 FROM wok.schema_versions WHERE version = 292) THEN
  
		INSERT INTO `wok`.`script_game_item_types` (`item_type`, `description`, `inventory_compatible`) VALUES ('path', 'Parent pathing object consists of all individual path waypoints and the rider.', 'F');
  
	END IF;

END
//
DELIMITER ;

CALL wok_SchemaChanges();
DROP PROCEDURE IF EXISTS wok_SchemaChanges;

