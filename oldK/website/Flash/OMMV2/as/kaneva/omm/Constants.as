﻿/**
 * Class that holds constant variables that are used throughout multiple classes
 * @author Scott
 */
class kaneva.omm.Constants
{
	/** Corner radius for the base of the widgets */
	public static var RADIUS:Number = 5;
}
