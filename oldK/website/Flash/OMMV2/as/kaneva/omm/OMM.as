﻿import mx.utils.Delegate;

import kaneva.events.Event;
import kaneva.events.ItemClickEvent;
import kaneva.events.MediaEvent;
import kaneva.events.StatusEvent;
import kaneva.FullscreenSharedObject;
import kaneva.lang.reflect.Method;
import kaneva.media.MediaAsset;
import kaneva.media.PlayListAsset;
import kaneva.omm.Config;
import kaneva.omm.CSS;
import kaneva.omm.io.XmlParser;
import kaneva.omm.Logger;
import kaneva.omm.Ticker;
import kaneva.omm.TickerMessage;
import kaneva.omm.ui.buttons.AbstractDetailsButton;
import kaneva.omm.ui.List;
import kaneva.omm.ui.MediaPlayer;
import kaneva.omm.ui.PlayList;
import kaneva.omm.widgets.AbstractWidget;
import kaneva.omm.widgets.WidgetData;
import kaneva.util.Strings;

/**
 * Represents the Open Media Module (OMM) as a whole.  Contains all widgets currently active in the OMM.
 * @author Kevin
 */
class kaneva.omm.OMM
{
	/** Singleton instance (should be an instance of a subclass of Client, set in the Client constructor) */
	private static var mInstance:OMM = null ;
	
	/** Configuration object for the OMM. */
	private static var mConfig:Config = new Config() ;
	
	/** Contains all widgets active in the OMM. Key is Number id, value is the AbstractWidget instance. */
	private var mWidgetMap:Object; // HashMap

	/** a temporary variable used to pass data between registerWidget and addWidgetToMap */
	private var mWidgetDataTempArray:Array;

	/** all 'pre' AbstractWidget objects.  The run() method on each widget is executed prior to any media play.*/
	private var mPreQueue:Array;
	
	/** all post AbstractWidget objects.  The run() method on each widget is executed after to any media play. */
	private var mPostQueue:Array;
	
	/** a temporary queue that will hold either pre or post widgets depending on when it is used*/
	private var mTempQueue:Array;
	
	/** Placeholder array containing WidgetData objects which are in the process of having their corresponding
	 * AbstractWidget code downloaded.  WidgetData are stored within the order they are received. */
	private var mWidgetDataDownloadArray:Array;
	
	/** 
	 * The instance names of the widgets/components used to show media (media player, play list, ticker, etc.)
	 * These are the instance names of the component that are on stage of the movie that is loaded into the place 
	 * holder movie clips (mMediaPlayerPH, mPlayListPH, mTickerPH)
	 */
	private static var MEDIA_PLAYER:String = "mMediaPlayer";
	private static var PLAY_LIST:String = "mPlayList";
	private static var TICKER:String = "mTicker";
	
	/** The id values for the widgets */
	private static var MEDIA_PLAYER_ID:String = "1";
	private static var PLAY_LIST_ID:String = "4";
	private static var TICKER_ID:String = "5";
	
	/** Place holder clips for the external swf files to be loaded into */
	private var mMediaPlayerPH:MovieClip;
	private var mPlayListPH:MovieClip;
	private var mTickerPH:MovieClip;
	
	/** References to the widget components that reside on the loaded external swf files */
	private var mMediaPlayerClip:MediaPlayer;
	private var mPlayListClip:PlayList;
	
	/** The ticker component now resides in the MediaVideoBase class.  We have a reference to it in the property */
	private static var mTickerClip:Ticker;
	
	/** Used to keep track of whether or not a specific widget exists in the current movie */
	private var isMediaPlayer:Boolean = false;
	private var isPlayList:Boolean = false;
	private var isTicker:Boolean = false;
	
	/** used to keep track of whether or not the currently loaded media item has been played*/
	private var mMoviePlayed:Boolean;
	
	/** The timeline which this class will create graphical objects on */
	private var mScope:MovieClip;
	
	/** parser for xml */
	public var mLoadXml:XmlParser;	
//
//	/** queue of filenames to be parsed*/
//	private var mXmlFileNames:Array;
//	
	/** a temporary mediaAsset object to be loaded into mMediaPlayerPH */
	private static var mCurrentMediaToPlay:MediaAsset;
	
	/** a temporary playlist to be loaded into mPlayListPH if it is loaded*/
	private var mTempPlayList:PlayListAsset;
		
	/** A reference to the list within the playlist */
	private var mList:List;
	
	/** Spacing used between widgets on stage */
	private var mGap:Number = 4;
	
	/** Movie clip loader object that loads any external content */
	private var mMCL:MovieClipLoader;
	
	/** Used to load up css files */
	private static var mCSS:CSS;
	
	/** 
	 * Constructor function
	 * @param pScope The timeline on which to create graphical elements
	 */
	private function OMM (pScope:MovieClip)
	{
		// Set the scope on which to create all visual graphics, movieclips, etc.
		mScope = pScope;
		
//		mXmlFileNames = new Array();
		
		this.mWidgetMap = new Object ();
		mPreQueue = new Array();
		mPostQueue = new Array();
		mTempQueue = new Array();
		mWidgetDataDownloadArray = new Array();

		// Create the movie clip loader object that loads any external content
		mMCL = new MovieClipLoader ();
		mMCL.addListener (this);
		
		// Set up stage stuff so that resizing of the Flash movie occurs when the user resizes the browser
		Stage.addListener(this);
		Stage.align = "TL";
		Stage.scaleMode = "noScale";

	}
	
	/**
	 * Retrieves the static instance of this class, ensuring only one is available.
	 * @param pScope The timeline on which to create graphical elements
	 * @return Static instance of the class.
	 */
	public static function getInstance(pScope:MovieClip):OMM
	{
		// If there is already an instance...then return it
		if (mInstance != null)
			return mInstance ;
		
		// Create the singleton instance of OMM and return it
		mInstance = new OMM(pScope) ;
		return mInstance;
	}

	/**
	 * @return The single configuration instance.
	 */
	public static function getConfig():Config
	{
		// Make sure everything is initialized first
		getInstance() ;
		return mConfig ;
	}
	
	/** @returns Currently loaded media */
	public static function getMedia():MediaAsset
	{
		// Make sure everything is initialized first
		getInstance();
		return mCurrentMediaToPlay;
	}
	
	/** 
	 * sets the configuration to hold any new configurable info
	 */
	private function setConfig(pConfig:Config):Void
	{
		for( var a:String in pConfig.mConfigMapping )
		{
			mConfig.mConfigMapping[a] = pConfig.mConfigMapping[a];
		}
		
//		hideButtons();
	}
	
	/**
	 * Invoked when a call to MovieClipLoader.loadClip() has begun to download a file
	 * @param pTarget A movie clip loaded by the MovieClipLoader.loadClip() method
	 */
	private function onLoadStart (pTarget:MovieClip):Void
	{
	}
	
	/**
	 * Invoked every time the loading content is written to the hard disk during the loading process 
	 * @param pTarget A movie clip loaded by the MovieClipLoader.loadClip() method
	 * @param pLoaded The number of bytes that had been loaded when the listener was invoked
	 * @param pTotal The total number of bytes in the file being loaded
	 */
	private function onLoadProgress (pTarget:MovieClip, pLoaded:Number, pTotal:Number):Void
	{
	}
	
	/**
	 * Invoked when a file loaded with MovieClipLoader.loadClip() has failed to load
	 * @param pTarget A movie clip loaded by the MovieClipLoader.loadClip() method
	 * @param pError A string that explains the reason for the failure, either "URLNotFound" or "LoadNeverCompleted".
	 */
	private function onLoadError (pTarget:MovieClip, pError:String):Void
	{
		trace ("onLoadError (" + pError + ")");
	}
	
	/**
	 * Method triggered by the onLoadInit event once an external swf has fully downloaded.
	 * 
	 * @param pTarget Reference to the clip that external content was loaded into
	 */
	private function onLoadInit (pTarget:MovieClip):Void
	{
//		trace("onLoadInit start, calling gotoAndStop(2)");
//		pTarget.gotoAndStop(2) ;
		
		//trace("onLoadInit after gotoAndStop(2), mWidgetDataTempArray has length of " + mWidgetDataTempArray.length);
		//var myDate:Date = new Date() ;
		//trace(myDate.getTime()) ;
		//trace("onLoadInit for : " + pTarget);
		
		// TODO How can we be sure that mCurrentMediaToPlay is not already playing?
		var media:MediaAsset = mCurrentMediaToPlay;
		
		// Retrieve the correct WidgetData for the target clip. 
		// Since we can not guarantee that movieclips will be received in the order they are requested for download
		// we search for the corresponding WidgetData by url/uri
		var widget:WidgetData = null ;
		var widgetTemp:WidgetData = null ;
		for (var i:Number = 0 ; i < mWidgetDataTempArray.length ; i++)
		{
			//trace("looping on widgetDatatempArray " + i );
			widgetTemp = mWidgetDataTempArray[i] ;

			// If the current widget data corresponds to the SWF just received...
			if (Strings.endsWith(pTarget._url, widgetTemp.uri))
			{
				// Remove the widget from the array
				mWidgetDataTempArray.splice(i, 1) ;
				// utilize the temp widget going forward
				widget = widgetTemp ;
				break ;
			}
		}
		
//		trace("widget data popped from temp array : " + widget.id);

		// If the id is that of a media player....
		if(widget.id == MEDIA_PLAYER_ID)
		{
			// since the media player isn't dynamic, there's no need to store it's object on the map,
			// but something should be stored so that we know it's been loaded, so set it to true
			this.mWidgetMap[widget.id] = true;
			
			// Set a reference to the media player component
			mMediaPlayerClip = mMediaPlayerPH[MEDIA_PLAYER];
			
			// Set boolean indicating that the media player exists and that no media has been played
			isMediaPlayer = true;
			mMoviePlayed  = false;
			
			// Set event listeners to listen out of the complete and status events
			mMediaPlayerClip.addEventListener (MediaEvent.MEDIA_COMPLETE, Delegate.create(this, onComplete));
			mMediaPlayerClip.addEventListener (StatusEvent.STATUS, Delegate.create(this, onStatus));
			//set event listeners to listen to whether the button has been updated via a successful xml load 
//			mMediaPlayerClip.buttonBase.raveButton.addEventListener("update", Delegate.create(this, onUpdateButton));
//			mMediaPlayerClip.buttonBase.addButton.addEventListener("update", Delegate.create(this, onUpdateButton));
			
			// As it says
//			hideButtons();
			
// TODO - Get rid of the direct reference to _root			
			// If fullscreen is set to true...this only happens when in full screen
			if(_root.fullScreen == "true")
			{
				// Get the media asset for the currently playing media
				mCurrentMediaToPlay = FullscreenSharedObject.getMediaAsset();
				playMedia(true, mCurrentMediaToPlay);
				getInstance().getMediaPlayer().getFullScreenButton().setEnabled(false);
			}
				
		}
		// If the id is that of a play list....
		else if(widget.id == PLAY_LIST_ID)
		{
			// since the playlist isn't dynamic, there's no need to store it's object on the map,
			// but something should be stored so that we know it's been loaded, so set it to true
			this.mWidgetMap[widget.id] = true;			
			
			// Set a reference to the play list component
			mPlayListClip = mPlayListPH[PLAY_LIST];
			
			// Set boolean indicating that the play list exists
			isPlayList = true;
			
			populatePlayList ();
		}
		// If the id is that of a ticker....
		else if(widget.id == TICKER_ID)
		{
			// since the ticker isn't dynamic, there's no need to store it's object on the map,
			// but something should be stored so that we know it's been loaded, so set it to true
			this.mWidgetMap[widget.id] = true;			
			
			// Set a reference to the ticker component
			mTickerClip = mTickerPH[TICKER];
			getInstance().getMediaPlayer().getMediaVideoBase().setTicker(mTickerClip);
			
//			var mvb:MediaVideoBase = getInstance().getMediaPlayer().getMediaVideoBase();
//			mvb.setSize(mvb.width, mvb.height);

			// Set boolean indicating that the ticker exists
			isTicker = true;
			
			createTicker(widget);	
		}

		

		// If there is anything left in the widget array that is to be loaded....then register the next element in 
		// that array.  Otherwise, play the media since everything is loaded
		//trace("in onLoadInit, mWidgetDataTempArray has length of " + mWidgetDataTempArray.length);

		// Since there are more widgets to receive, we should not proceed with executing the queue
		if (mWidgetDataTempArray.length > 0)
		{
			this.registerAllWidgetsFromTemp() ;
		}
		// Since all widgets are initialized, proceed with execution of the queue
		else
		{
			mTempQueue = copyArray(mPreQueue);
			//trace("invoking runQueuItem(true) from onLoadInit()");
			runQueueItem();
		}
		
		// We attempted to delete the pTarget (widget movie) here to clean up resources, but it proved too fast
		// for the client application at times.  Therefore, the MovieClip resource will remain in memory.  This is 
		// a small sacrifice, for the time being, as it will be extremely small in nature.  Furthermore, subsequent
		// references to the same widget index of dynamic widgets will not produce new MovieClips.  See comments
		// in registerWidget() for more information.
			
		onResize();
	}
	
	/**
	 * Retrieves a widget object from the OMM.
	 * @param   pId Key of the widget to retrieve.
	 * @return  Widget from the OMM, or null if it does not exist.
	 */
	public function getWidget (pId:Number):AbstractWidget
	{
		return this.mWidgetMap[pId];
	}

	
	/**
	 * Removes a widget from the widget map, prequeue and postqueue, if necessary.
	 * @param   pId Key of the widget to delete.
	 */
	public function deleteWidget(pId:Number):Void
	{
		this.mWidgetMap[pId] = null ;
		
		// TODO Revisit this loop.  can not use a simple int loop, because widgets are stored by index
		// can not use a for/in loop, because multiple null values may exist in between
		// Loop through prequeue searching for the given Id.
/*		var widget:AbstractWidget;
		for (widget in this.mPreQueue)
		{
			
		}
*/		// We could just check the queue during runQueueItem() or during CopyArray()
		// and check to see if that widget still exists within the map as a way of deleting from the queues...not the map
		// This is the method i plan to use for now...
	}
	
	/**
	 * Helper method used in invoking registerWidget() on all WidgetData objects in the mWidgetDataTempArray.
	 */
	private function registerAllWidgetsFromTemp():Void
	{
		// Do nothing if the temp array is empty
		if (this.mWidgetDataTempArray.length == 0)
			return ;

		// save a link to all the "temp" widgets
		var tempWidgetData:Array = this.mWidgetDataTempArray ;
		
		// Loop through the previous temp widgetdata, registering each.
		// registerAllWidgetsFromTemp() is recalled by onLoadInit which is automatically called each time a swf is loaded
		registerWidget(tempWidgetData[0]);

	}
	
	/**
	 * Registers a widget with the OMM, using the following steps.
	 * 
	 * <ol>
	 *   <li>Determine if the Widget already exists within a global map on the OMM.</li>
	 *     <ol>
	 *       <li>If the Widget does exist by the given key, retrieve it.</li>
	 *       <li>If the Widget does not exist, download the code, create 
	 *           an instance of the object, and store the Widget within the global 
	 *           map by the given key.</li>
	 *     </ol>
	 *   
	 * 
	 * </ol>
	 */
	public function registerWidget (pWidgetData:WidgetData):Void
	{
//		trace ("pWidgetData.id : " + pWidgetData.id);

		// If the widget does not already exist in the map
		if (getWidget(pWidgetData.id) == null)
		{
			switch (pWidgetData.id.toString())
			{
				case MEDIA_PLAYER_ID: 
				mMediaPlayerPH = mScope.createEmptyMovieClip ("mMediaPlayerPH", mScope.getNextHighestDepth ());
					mMCL.loadClip (pWidgetData.uri, mMediaPlayerPH);
					break;
				case PLAY_LIST_ID: 
					mPlayListPH = mScope.createEmptyMovieClip ("mPlayListPH", mScope.getNextHighestDepth ());
					mMCL.loadClip (pWidgetData.uri, mPlayListPH);
					break;
				case TICKER_ID: 
//					mTickerPH = mScope.createEmptyMovieClip ("mTickerPH", mScope.getNextHighestDepth ());

					// Now we want to load the ticker into the media video base rather than on the main timeline
					mTickerPH = getInstance().getMediaPlayer().getMediaVideoBase().getTickerPH();
					mMCL.loadClip (pWidgetData.uri, mTickerPH);
					break;
				// Attempt to download the unknown widget code
				default:
					//trace("UNKNOWN WIDGET TO DOWNLOAD!!!");
					mWidgetDataDownloadArray.push(pWidgetData);
					// Start downloading the widget code as encompassed in a custom movie clip
					// It is important to give a unique name for each widget clip, as clips will replace each other if they 
					// share the same name.  The ID is used to minimize the number of active clips in the player; it is fine
					// if the old clip containing WidgetA is replaced by a new clip containing the same WidgetA.
					// A unique depth is also equally important, but should not be duplicated.
					var widgetMovie:MovieClip = mScope.createEmptyMovieClip ("widgetMovie" + pWidgetData.id, mScope.getNextHighestDepth ());
					// When this clip has finished downloading, it will execute the onLoadInit() method
					mMCL.loadClip(pWidgetData.uri, widgetMovie);
			}
		}
		// The widget is already in the widgetmap
		else
		{
			Logger.logMsg("widget already loaded into map");
			if(pWidgetData.id == TICKER_ID)
			{
				// since adding to the ticker works two different ways, if nothing is in the ticker, use createTicker, else use populate
				if(mTickerClip.mDataProvider.length == 0)
					createTicker(pWidgetData);
				else
					populateTicker(pWidgetData);
			}
			else 
			{
				// Set all parameters, if applicable, on the widget
				this.setWidgetParams(pWidgetData) ;
				// Execute any methods, if applicable, on the widget
				this.invokeWidgetMethods(pWidgetData) ;
			}
		}
	}
	
	/**
	 * Sets values retrieved from a WidgetData object onto an existing and downloaded widget.
	 * @param pWidgetData  Details what parameters need to be set, along with the values to set.
	 */
	private function setWidgetParams(pWidgetData:WidgetData):Void
	{
		var widget:AbstractWidget = this.getWidget(pWidgetData.id) ;
		var len:Number = pWidgetData.fields.length;
		
		// Loop through all parameters/fields, if applicable, and set them on the widget
		// fields contain the follow variables:
		for (var i:Number = 0; i < len; i++)
		{
			// name:String - Name of the parameter.
			// text:String - Value of the parameter.			
			var memberName:String = pWidgetData.fields[i].name;
			var memberValue:String =  pWidgetData.fields[i].text;
			// this may not be the best way to assign the value, but it is all that we could come up with.
			// the object that we are setting the value to, is a child of abstractWidget.  The OMM has no 
			// reference to this subclass/child, therefore, there is no way for Flash to do any type of error
			// checking based on this subclass of abstractWidget.  If we were to set the value on the abstractWidget, we
			// may get an error (abstractwidget.memberName); to circumvent that, we use the array accessor to 
			// set the property value of that element (abstractWidget[memberName])... although this works, we get no
			// error checking from Flash
			widget[memberName] = memberValue;
			
		}
			
		
		
    }
	
	/**
	 * Invokes all methods as specified within a WidgetData onto an existing and downloaded widget.
	 * @param pWidgetData  Details what methods to invoke.
	 */
	private function invokeWidgetMethods(pWidgetData:WidgetData):Void
	{
		var widget:AbstractWidget = this.getWidget(pWidgetData.id) ;
		var len:Number = pWidgetData.methods.length;

		// Loop through all methods, if applicable, and invoke them on the widget
		for (var i:Number = 0; i < len; i++)
		{
			var func:Function = widget[pWidgetData.methods[i].name];
			var args:Array = pWidgetData.methods[i].args;
			// call the function on the abstractwidget and send it's arguments
			func.apply(widget, args);
		}
    }
	
	/** 
	 * Called by a widget that has loaded a .swf.  within the 2nd frame of the .swf's .fla, this method is invoked.
	 * @param pWidget Actual widget code created by the downloaded SWF.
	 * @param pSwfUri URI from which the SWF was downloaded.  Used in matching the AbstractWidget to the appropriate WidgetData
	 *                that started its download process, and contains execution information.
	 */
	public function receiveWidget(pWidget:AbstractWidget, pSwfUri:String):Void
	{
		//trace ("receiveWidget() " + pWidget + " " + pSwfUri);
		// check to see if the abstractWidget has been stored onto the widgetMap, if not, save it
		var len:Number = mWidgetDataDownloadArray.length;
		// Find the original WidgetData request object from the array
		for(var i:Number = 0; i< len; i++)
		{
			// If found, remove the WidgetData from the array, and fill the appropriate fields/methods here
			if(Strings.endsWith(pSwfUri, mWidgetDataDownloadArray[i].uri))
			{
				pWidget.widgetData = mWidgetDataDownloadArray[i];
				mWidgetDataDownloadArray.splice(i,1);
				break;
			}
		}
		
		if (this.getWidget(pWidget.widgetData.id) == null)
			this.mWidgetMap[pWidget.widgetData.id] = pWidget;		
		
		
		// Set all parameters, if applicable, on the widget
		this.setWidgetParams(pWidget.widgetData) ;
		// Execute any methods, if applicable, on the widget
		this.invokeWidgetMethods(pWidget.widgetData) ;
		
		// add the widget to the pre-queue if necessary
		if(pWidget.widgetData.queue.pre)
		{
			if(pWidget.widgetData.queue.index)
				mPreQueue[pWidget.widgetData.queue.index] = pWidget;
			else
				mPreQueue.push(pWidget);
		}

		// add the widget to the post-queue if necessary
		if(pWidget.widgetData.queue.post)
		{
			if(pWidget.widgetData.queue.index)
				mPostQueue[pWidget.widgetData.queue.index] = pWidget;
			else
				mPostQueue.push(pWidget);
		}
		
		// If mWidgetDataDownloadArray is now empty, we have received all widgets to be downloaded
		// We can therefore safely start the execution process
		// if tempQueue already has a length, then that means we are already attempting to runQueueItem, therefore, skip 		
		if(mWidgetDataDownloadArray.length == 0 && isMediaPlayer && mTempQueue.length == 0 )
		{
			mTempQueue = copyArray(mPreQueue);
			//trace("invoking runQueuItem(true) from receiveWidget()");
			runQueueItem();
		}
	}
	
	/**
	 * play the first item within the queue if any exist.
	 */
	private function runQueueItem():Void
	{
		if(isMediaPlayer && mTempQueue.length > 0)
		{
			var widget:AbstractWidget = AbstractWidget(mTempQueue.shift());
			widget.run() ;
		}
		else
			onComplete();
	}
	
	/**
	 * used to load xml to get widgets; be it playlists, media, etc
	 */
	public function loadXml(file:String):Void
	{
		Logger.logMsg("loadXml:" + file);
		mLoadXml = new XmlParser(file);
		mLoadXml.addEventListener (MediaEvent.COMPLETE, Delegate.create (this, onLoadXml));
		onResize();
	}
		

	/**
	 * determines which types of data have just been parsed, and then dispatches the appropriate
	 * function to be run
	 * @param pEvent An event object passed by the object that dispatched the event
	 */
	private function onLoadXml (pEvent:Event):Void
	{
		var config:Config = mLoadXml.mConfig;
		var configLength:Number = 0;
		
		/** determine if config was set */
		for (var z:String in config.mConfigMapping)
			configLength++;		
		
		// if configurations have been parsed, save those configurations either by setting them
		// or storing them into a temporary variable
		if(configLength > 0)
		{
			setConfig(config);
		}
		
		// Load the css files
		mCSS = new CSS();
		
		// Listen for the complete event for the CSS
		mCSS.addEventListener (MediaEvent.COMPLETE, Delegate.create(this, onLoadCSS));
		
		mCSS.loadStyleSheets();
	}	
	
	/** 
	 * Triggered once all the css files are loaded up.  Once we have all the CSS files loaded into CSS objects
	 * that can be used throught the widgets, then we start loading the widgets
	 */
	private function onLoadCSS ():Void
	{
		//trace ("onLoadCSS");
		
		mWidgetDataTempArray = mLoadXml.mWidgetArray;
		var media:MediaAsset = mLoadXml.mMediaAsset;
		var playList:PlayListAsset = mLoadXml.mPlayList;		

		// register within the hashMap, any widgets that have been parsed
		this.registerAllWidgetsFromTemp() ;
		
		// attempt to load media into the player
		if(media)
		{
			mCurrentMediaToPlay = media;
			if(isMediaPlayer)
			{
				mTempQueue = copyArray(mPreQueue);
				//trace("invoking runQueuItem(true) from onLoadCSS()");
				runQueueItem();
			}
		}
		
		// populate the playlist with any data
		if(playList)
		{
			mTempPlayList = playList;
			if(playList.mediaAssets[0])
				mCurrentMediaToPlay = playList.mediaAssets[0];
			if(isPlayList)
				populatePlayList();
		}
//		
//		// Check for more files
//		getNextFile();			
//			
		
	}
	
//	/** 
//	 * checks mXmlFileNames for any more files, and if any exist, calls loadXml on 
//	 * the next file within the filename queue
//	 */
//	private function getNextFile():Void
//	{
//		do 
//		{
//			mXmlFileNames.shift();
//			if(mXmlFileNames.length > 0 && mXmlFileNames[0] != undefined)
//			{
//				loadXml(mXmlFileNames[0]);
//				return;
//			}
//		} while ( mXmlFileNames.length > 0 && mXmlFileNames[0] == undefined );
//	}
//	
//	/** sets the file names that will be parsed */
//	public function setFileNames(files:Array):Void
//	{
//		mXmlFileNames = files;
//		loadXml(mXmlFileNames[0]);
//	}
//	
	/**
	 * populates the list within the mPlayListPH variable assuming the movie has been defined
	 */
	private function populatePlayList ():Void
	{
		// fill in the playList within the PlayListMovie object
		mList = mPlayListClip.getList ();
	
		var dataArray:Array = new Array ();
		for (var a:Number = 0; a < mTempPlayList.mediaAssets.length; a++)
		{
			dataArray.push(mTempPlayList.mediaAssets[a]);
		}
		
		mList.dataProvider = dataArray;
		mList.setRowColors (mConfig.getAsArrayOfNumbers("playlist.row.colors"));
		mList.selectedIndex = 0;
	}
	
	
	/** populates the ticker with appropriate text to be scrolled across the screen */
	private function populateTicker(pWidget:WidgetData):Void
	{
		var tickerMsg:TickerMessage;
		
		var method:Method ;
		var methodsCount:Number = pWidget.methods.length ;
			
		for(var count:Number = methodsCount-1 ; count >= 0  ; count-- )
		{
			method = pWidget.methods[count] ;
			if(method.name == "add")
			{
				tickerMsg = new TickerMessage();
				
				// add the id, text, max time in queue, and url if available
				for(var h:Number = 0; h < method.args.length; h++)
				{
					if ( h == 0)
						tickerMsg.id = method.args[h].toString();
					else if ( h == 1)
						tickerMsg.text = method.args[h].toString();
					else if ( h == 2)
						tickerMsg.max = Number(method.args[h]);
					else if ( h == 3)
						tickerMsg.url = method.args[h].toString();
				}
				mTickerClip.addItem (tickerMsg);
			}
			// if removing a message, check to see if the id exists, then remove it from the ueue
			else if(method.name == "remove")
			{
				var index:Number = mTickerClip.checkForId(method.args[0]);
				if(index >= 0)
					mTickerClip.removeItemAt(index);
			}
		}
	}
	
	/** creates the dataprovider for the ticker, and then starts the ticker once that dataprovider has been set*/
	private function createTicker(pWidget:WidgetData):Void
	{
		var tickerMsg:TickerMessage;
		
		var method:Method ;
		var methodsCount:Number = pWidget.methods.length ;
		var newDataProvider:Array = new Array();
			
		for(var count:Number = 0; count < methodsCount; count++ )
		{
			method = pWidget.methods[count] ;
			if(method.name == "add")
			{
				tickerMsg = new TickerMessage();
				
				// add the id, text, max time in queue, and url if available
				for(var h:Number = 0; h < method.args.length; h++)
				{
					if ( h == 0)
						tickerMsg.id = method.args[h].toString();
					else if ( h == 1)
						tickerMsg.text = method.args[h].toString();
					else if ( h == 2)
						tickerMsg.max = Number(method.args[h]);
					else if ( h == 3)
						tickerMsg.url = method.args[h].toString();
					
				}
				
				// add the tickerMsg to the end of the array
				newDataProvider.push(tickerMsg);
			}
		}
		// there should be nothing to initially remove from the ticker message bar since it is just now being created, 
		// therefore, i wont check to see if there is anything to remove within the data sent via xml/widget
		// instead, the dataprovider is now set, and messages should begin to scroll across the OMM
		mTickerClip.setDataProvider(newDataProvider);
	}


	/**
	 * plays whatever media is currently stored in the parameter pMedia
	 * @param pBool determines if the media is to start playing immediately
	 * @param pMedia is the media that should be played
	 * @param pViewing - used to tell the mediaplayer whether or not this piece of media should tell the server it's been viewed
	 */
	public function playMedia(pBool:Boolean, pMedia:MediaAsset, pViewing:Boolean):Void
	{
		if(isMediaPlayer && pMedia)
		{
			var detailMode:Boolean;
			var media:MediaAsset = pMedia;
			
			//if autosize and detailMode exist within the configMapping send them on load.
			//then remove them from the mapping since they should only be used on the initail load
			if(mConfig.getAsString("autoStart") && media)
			{
				pBool = mConfig.getAsBoolean("autoStart");
				mConfig.remove("autoStart");
			}
			// isMediaPlayer - is the mediaplayer, media is loaded into the player
			if(mConfig.getAsString("detailMode") && media)
			{
				detailMode = mConfig.getAsBoolean("detailMode");
				mConfig.remove("detailMode");
			}
			
			// load the media and play it
			mMediaPlayerClip.load (media, pBool, detailMode, true, pViewing);
		}
	}
	
	/** 
	 * Receives events from a playlist and loads/plays whatever media has been clicked on
	 */
	public function receiveListClick (pEvent:ItemClickEvent):Void
	{
		var mediaAsset:MediaAsset = MediaAsset(pEvent.item);
		
		// Make sure that the current media asset isn't the same as the one received from the playlist
		if(mCurrentMediaToPlay != mediaAsset)
		{
			mCurrentMediaToPlay = mediaAsset;
			// clear the ticker since the messages for the previously loaded media wont apply to the soon-to-be-playing media
			mTickerClip.clearTicker();
			
			mMoviePlayed = false;
			mTempQueue = copyArray(mPreQueue);
			
			//trace("invoking runQueuItem(true) from receiveListClick()");
			runQueueItem();
		}
		else // playlist play/pause button has been clicked
			mMediaPlayerClip.onPlayPauseClick();
	}
	
	/** 
	 * Figures out how tall to size the media player in order for it to maintain a 4:3 aspect ratio for the video 
	 * @param pWidth The stage width
	 * @return The height the media player needs to be
	 */
	private function calculate4by3Ratio (pWidth:Number):Number
	{
		// Figure out the height the video needs to be
		var videoHeight:Number = pWidth * 3/4;
		
		// Now take into consideration the dimensions of the other elements of the media player in order to calculate what 
		// the height the media player needs to end up being
//		var otherHeights:Number = mMediaPlayerClip.sliderControlHeight + mMediaPlayerClip.titleBaseHeight + mMediaPlayerClip.buttonBaseHeight;
		
		// The height the media player needs to be
		return videoHeight + mMediaPlayerClip.mButtonBaseHeight;
	}
	
	/** 
	 * Method called when the Stage is resized.  This will appropriately resize and position all the contents on stage  
	 * so that there is no distortion
	 */
	public function onResize():Void
	{
		// Reference to the stage width and height....we subtract a little so that the right and bottom edges don't get cut off.
		// This fixes the issue of the border not showing up
		var sWidth:Number = Stage.width - 0.5;

		var sHeight:Number = Stage.height - 0.5;

		// Check to see which widgets exist on the Flash movie and then size appropriately
		if (isMediaPlayer && isPlayList && isTicker)
		{
			// First size the media player....no need to position since it will be at the top (0, 0)
			mMediaPlayerClip.setSize (sWidth, calculate4by3Ratio (sWidth));
			
//			// Next, size the ticker width.....leave the height be the default
//			mTickerClip.setSize (sWidth, null);
//			
//			// Position the ticker so it is directly below the media player
//			mTickerClip._y = mMediaPlayerClip.height + mGap;
		
			// Then position the play list
//			mPlayListClip._y = mTickerClip._y + mTickerClip.height + mGap;
			mPlayListClip._y = mMediaPlayerClip.height + mGap;
			
			// Size the play list.....but only if it will be vertically sized to a positive value....otherwise it is probably off screen
			if (sHeight - mPlayListClip._y > 0)
				mPlayListClip.setSize (sWidth, sHeight - mPlayListClip._y);
		}
		else if (isMediaPlayer && !isPlayList && isTicker)
		{
//			// Size the ticker width.....leave the height be the default
//			mTickerClip.setSize (sWidth, null);
//			
//			// Position the ticker at the bottom of the stage
//			mTickerClip._y = sHeight - mTickerClip.height;
			
			// Size the media player
//			mMediaPlayerClip.setSize (sWidth, mTickerClip._y - mGap);
			mMediaPlayerClip.setSize (sWidth, sHeight);
			
			// We are done
			return;
		}
		else if (isMediaPlayer && isPlayList && !isTicker)
		{
			// First size the media player....no need to position since it will be at the top (0, 0)
			mMediaPlayerClip.setSize (sWidth, calculate4by3Ratio (sWidth));
			
			// Then position the play list
			mPlayListClip._y = mMediaPlayerClip._y + mMediaPlayerClip.height + mGap;
			
			// Size the play list.....but only if it will be vertically sized to a positive value....otherwise it is probably off screen
			if (sHeight - mPlayListClip._y > 0)
				mPlayListClip.setSize (sWidth, sHeight - mPlayListClip._y);
		}
		else if (isMediaPlayer && !isPlayList && !isTicker)
		{
			// Size the media player....no need to position since it will be at the top (0, 0)
			mMediaPlayerClip.setSize (sWidth, sHeight);
		}
		else if (!isMediaPlayer && isPlayList && !isTicker)
		{
			// Size the play list....no need to position since it will be at the top (0, 0)
			mPlayListClip.setSize (sWidth, sHeight);
		}
		else if (!isMediaPlayer && !isPlayList && isTicker)
		{
			// Size the ticker
//			mTickerClip.setSize (sWidth, null);
		}
	}	
	
	/** Method called once a media item is done playing */
	public function onComplete (pEvent:Event):Void
	{
		disableAllButtonClicks(false);
		
		// stop() is added to prevent browsers IEv6 and FF from 'running-down' the playlist without actually playing
		// any of the media.  For some reason, this is not needed within Opera or IEv7, none-the-less, it fixes
		// the issue, and prevents movies from 'completing' before they are 'played'
		mMediaPlayerClip.stop();
		
		//if there is any item left in the pre queue, load them
		if(mTempQueue.length > 0 && !mMoviePlayed)
		{
			runQueueItem();
		}

		//if there are no items left in the pre queue, load the media
		else if (!mMoviePlayed)
		{
			mMoviePlayed = true;
			// copy the postQueue so that any items marked as post will be ready to run once the current media is complete
			mTempQueue = copyArray(mPostQueue);
			//trace("prepare postQueue " + mTempQueue.length);
			// set the text for the media to be displayed.
			setMediaText();
			playMedia(true, mCurrentMediaToPlay, true);
		}
		else if (mMoviePlayed && mTempQueue.length > 0)
		{
			runQueueItem();
		}
		
		
		// If the current selection is not at the end of the list component
		//Logger.logMsg( "onComplete: " + String(mList.selectedIndex));
		else if (mList.selectedIndex < mList.length - 1)
		{
			// clear the ticker of any messages since the messages for this completed movie should no longer be displayed
			mTickerClip.clearTicker();
			
			// Set selection to the next index in the list
			mList.selectedIndex += 1;

			// Get the media asset from the currently selected item in the list
			var mediaAssetObj:MediaAsset = MediaAsset(mList.selectedItem);
			
			// Once the movie is complete, tell the playlist that the newly selected cell's playbutton has been 'clicked'
			// therefore swapping that playbutton to a pause button -- this also resets the previously selected cell back
			// to a play button since it is no longer being played
			var itemClickEvent:ItemClickEvent = new ItemClickEvent();
			itemClickEvent.type = ItemClickEvent.ITEM_CLICK;
			itemClickEvent.item = mediaAssetObj;
			itemClickEvent.index = mList.selectedIndex;
			itemClickEvent.relatedObject = mList;
			mList.dispatchEvent(itemClickEvent);
		}
		// no playlist exists...therefore, 'reload' the movie in stopped mode, beginning with any pre-queue ads.
		else
		{
			mTempQueue = copyArray(mPreQueue);
			mMoviePlayed = false;
			// reset autoStart to false so that when the movie is 'replayed' it stops, instead of constantly reloading and playing the media
			mConfig.setValue("autoStart", "false");			
			//trace("invoking runQueuItem(true) from onComplete() at end of playlist");
			// reload the media and any of it's ads
			runQueueItem();
			// .stop() is called to have the 'big-giant' play button appear over the maximized media
			mMediaPlayerClip.stop();
		}
	}	
	
//	/** displays the appropriate buttons based on what is stored in the config Map*/
//	private function hideButtons():Void
//	{
//		mMediaPlayerClip.buttonBase.raveButton._visible = mConfig.getAsBoolean("show.button.raveIt");
//		mMediaPlayerClip.buttonBase.addButton._visible = mConfig.getAsBoolean("show.button.add");
//		mMediaPlayerClip.buttonBase.shareButton._visible = mConfig.getAsBoolean("show.button.share");
//		mMediaPlayerClip.buttonBase.commentsButton._visible = mConfig.getAsBoolean("show.button.comment");
//	}
	
	/** 
	 * Event triggered by the status event of the media player
	 * @param pEvent The event object passed by the dispatcher
	 */
	private function onStatus (pEvent:StatusEvent):Void
	{
		var status:String = pEvent.code;
		if(isPlayList)
			mPlayListClip.playOrPauseMedia(status);	
	}
//	
//	/** 
//	 * updates the add value and add count within the list, for the currently loaded media.
//	 * this is done incase this item is reloaded into the mediaplayer at a later time
//	 */
//	private function onUpdateButton(pEvent:Object):Void
//	{
//		var count:Number = pEvent.target.getCount();
//		var buttonName:String = pEvent.target.getKey();
//
//		// set the values within the list
//		switch (buttonName)
//		{
//			case "add":
//				mCurrentMediaToPlay.channelCount++;
//				if (isPlayList)
//				{
//					mList.dataProvider[mPlayListClip.mCurrentlyPlayingIndex].inlib = true;
//					mList.dataProvider[mPlayListClip.mCurrentlyPlayingIndex].channelCount = count;
//				}
//				break;
//			case "raveIt":
//				mCurrentMediaToPlay.thumbsup++;
//				if (isPlayList)
//				{
//					mList.dataProvider[mPlayListClip.mCurrentlyPlayingIndex].raved = true;
//					mList.dataProvider[mPlayListClip.mCurrentlyPlayingIndex].thumbsup = count;
//					// update the count in the playlist too
//					mList.mPlayListItemArray[mPlayListClip.mCurrentlyPlayingIndex].setTitleTextField(mCurrentMediaToPlay);					
//				}
//				break;
//		}
//	}	
//	
	
	/** 
	 * updates the add value and add count within the list, for the currently loaded media.
	 * this is done incase this item is reloaded into the mediaplayer at a later time
	 * @param pButton The button that was pressed
	 */
	public function updateFeedbackButtons(pButton:AbstractDetailsButton):Void
	{
		// If there is a playlist....
		if (isPlayList)
		{
			// Set the values within the list
			switch (pButton)
			{
				// If it's a rave button....
				case getMediaPlayer().getMediaVideoBase().getRaveButton():
					
					// Iterate the thumbsup property for the media asset here in OMM
					mCurrentMediaToPlay.thumbsup++;
					
					// Update stuff in the list
					mList.dataProvider[mPlayListClip.mCurrentlyPlayingIndex].raved = true;
					mList.dataProvider[mPlayListClip.mCurrentlyPlayingIndex].thumbsup = this[pButton].getCount();
					
					// Update the count in the playlist too
					mList.mPlayListItemArray[mPlayListClip.mCurrentlyPlayingIndex].setTitleTextField(mCurrentMediaToPlay);					
					
					break;
			}
		}
	}	
	
	/** 
	 * copy sll the elements from one array to another, minus any null elements, returning the new array.
	 * if an element within a queue does not exist within the widgetMap, then it will be deleted from the queue.
	 * This is done because the position of the queue is based on it's index instead of it's widget id.  Had the position
	 * been based off of id, we could have just removed the element from the queue within deleteWidget()
	 * @param copyFrom  the array that is copied into the new array
	 * @return copyTo	the new array without any null values
	 */
	private function copyArray(copyFrom:Array):Array
	{
		var length:Number = copyFrom.length;
		var copyTo:Array = new Array();
		
		// sift thru the array, if a value is not null, push it onto the new array
		for(var i:Number = 0; i < length; i++)
		{
			if(copyFrom[i] != null)
			{
				//check to see if this widget has been deleted from mWidgetMap
				if(mWidgetMap[copyFrom[i].widgetData.id] != null)
					copyTo.push(copyFrom[i]);
				else
					copyFrom[i] = null;
			}
		}
		return copyTo;
	}
	
	
	/** 
	 * disables all button clicks on the OMM 
	 * @param pBool - if true, disable the buttons
	 */
	public function disableAllButtonClicks(pBool:Boolean):Void
	{
//		mMediaPlayerClip.buttonBase.raveButton.enabled = !pBool;
//		mMediaPlayerClip.buttonBase.addButton.enabled = !pBool;
//		mMediaPlayerClip.buttonBase.shareButton.enabled = !pBool;
//		mMediaPlayerClip.buttonBase.commentsButton.enabled = !pBool;
//		mMediaPlayerClip.buttonBase.mStopButton.enabled = !pBool;
//		mMediaPlayerClip.buttonBase.mPlayButton.enabled = !pBool;
//		mMediaPlayerClip.buttonBase.mSoundButton.enabled = !pBool;

		mMediaPlayerClip.getSliderControl().setEnabled(!pBool);
		
	}
	
	/**
	 * sets all of the text and button labels/counts for the currently loaded media on the OMM.
	 * this way, if there are any pre or post queue items, all of the text and button counts still show
	 * for the loaded media - not the pre/post-queue items
	 */
	public function setMediaText():Void
	{
//		mMediaPlayerClip.setTitle(Strings.HtmlDecode(mCurrentMediaToPlay.title));
		
//		mMediaPlayerClip.buttonBase.setRavedIcon(mCurrentMediaToPlay.raved);
//		mMediaPlayerClip.buttonBase.raveButton.setCount(mCurrentMediaToPlay.thumbsup.toString());
//		mMediaPlayerClip.buttonBase.raveButton.setLabel(mCurrentMediaToPlay.raved ? "RAVES" : "RAVE IT");
//		
//		mMediaPlayerClip.buttonBase.setAddedIcon(mCurrentMediaToPlay.inlib);
//		mMediaPlayerClip.buttonBase.addButton.setCount(mCurrentMediaToPlay.channelCount.toString());
//		mMediaPlayerClip.buttonBase.addButton.setLabel(mCurrentMediaToPlay.inlib ? "ADDED" : "ADD");
//		
//		mMediaPlayerClip.buttonBase.shareButton.setCount(mCurrentMediaToPlay.shares.toString());
//		mMediaPlayerClip.buttonBase.commentsButton.setCount(mCurrentMediaToPlay.total.toString());
		
		// Set counts
		mMediaPlayerClip.getMediaVideoBase().getRaveButton().setCount(mCurrentMediaToPlay.thumbsup);
		mMediaPlayerClip.getMediaVideoBase().getViewsButton().setCount(mCurrentMediaToPlay.views);
		mMediaPlayerClip.getMediaVideoBase().getCommentsButton().setCount(mCurrentMediaToPlay.total);
		
		mMediaPlayerClip.setTextField(mCurrentMediaToPlay);
		mMediaPlayerClip.setRatings(mCurrentMediaToPlay.mature);		
	}
	
	/** @return The media player */
	public function getMediaPlayer():MediaPlayer
	{
		return mMediaPlayerClip;
	}
//	
//	/**
//	 * Set reference to the ticker.  The ticker is now located in the MediaVideoBase class.  I don't know when I have 
//	 * reference to it until it is created. This allows me to set it on OMM from the MediaVideoBase class once it 
//	 * is created.
//	 * @param pTicker The ticker component whose reference we want to set on the OMM
//	 */
//	public static function setTicker(pTicker:Ticker):Void 
//	{
//		mTickerClip = pTicker;
//	}
//	
//	/**
//	 * Returns a reference to the ticker
//	 * @return The ticker clip located in the MediaVideoBase class
//	 */
//	public static function getTicker():Ticker
//	{
//		return mTickerClip;
//	}
//	
}
