﻿/**
 * Used for debugging
 * @author Kaneva
 */
class kaneva.omm.Logger{

	public static var sendToServer:Boolean = true;

	/**
	 * Sends the given message to the global trace() method.  If in debug mode, the message is also
	 * displayed in a small confirmation window in the OMM.
	 */
	public static function logMsg(pMsg:String):Void
	{
//		trace(pMsg) ;
		
		if (sendToServer)
		{
			// Creates a javascript alert box with the parameter as the message to show
			var sendingLoadVars:LoadVars = new LoadVars();
			var receivingXML:XML = new XML();
			receivingXML.ignoreWhite = true;
			sendingLoadVars["type"] = 99;
			sendingLoadVars["msg"] = pMsg;
			var s:String = _root._url;
			s.toLowerCase();
			var i:Number = s.indexOf("flash/omm");
			if ( i >= 0 )
			{
//				trace( s.substr( 0, i ) +  "services/omm/request.aspx" );
				sendingLoadVars.sendAndLoad( s.substr( 0, i ) +  "services/omm/request.aspx", receivingXML, "POST");
			}
		}
	}

}
