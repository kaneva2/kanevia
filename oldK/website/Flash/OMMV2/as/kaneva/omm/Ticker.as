﻿import mx.utils.Delegate;

import kaneva.omm.CSS;
import kaneva.omm.OMM;
import kaneva.omm.TickerMessage;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.shapes.Rect;

/**
 * Class that makes the ticker widget work.......sort of
 * @author Scott
 */
class kaneva.omm.Ticker extends ComponentDisplay
{
	/** The amount of time (seconds) it should take the text to cycle across the ticker normally */
	private var mTimeSlow:Number;
	
	/** The amount of time (seconds) it should take the text to cycle across the ticker while user presses arrow */
	public var mTimeFast:Number;
	
	/** If user is pressing an arrow, then this will be true */
	private var isFast:Boolean = false;
	
	/** The maximum number of ticker messages allowed in the ticker.  Any more added and the oldest message gets booted */
	private var maxCount:Number = 10;
	
	/** Acts as a button so when user presses and holds, the cycling stops.  On release, it starts again */
	private var mBaseClip:MovieClip;
	
	/** Clip that houses the text field.  This necessary in order to mask the container so it masks the text */
	private var mContainerClip:MovieClip;
	
	/** Clip that is set as the mask for the container clip */
	private var mMaskClip:MovieClip;

	/** The depth to use for the text fields */
	private var mTextDepth:Number = 0;
	
	/** Array that contains all the text fields that currently exist and that need to be dealt with */
	private var mTextFieldArray:Array;
	 
	/** The style sheet for the text fields */
	private var mCSS:TextField.StyleSheet;
	
	/** The back and forward arrow */
//	private var mArrowBack:MovieClip; //leftside
//	private var mArrowForward:MovieClip; //rightside
	
	/** Array that contains all the ticker messages to run through and show */
	public var mDataProvider:Array;
	
	/** 
	 * Frames per second....used in calculation of speed for text field.  This must coincide with the frames 
	 * per second set for the main movie.  Unfortunately, this cannot be dynamically set or retrieved for a Flash movie
	 */
	private var FPS:Number = 30;
	
	/** The speed of the text fields */
	private var mSpeed:Number;
	
	/** A set timeout that waits until a split second after this component is done resizing */
	private var mTimeoutId:Number;

	/** The amount of space between the messages */
	private var mGap:Number = 100;
	
	/** Constructor, private because this class is the AS 2.0 class for a clip */
	private function Ticker ()
	{
		super ();
		
	}
	
	/** Initialize stuff */
	private function init ():Void
	{
		super.init ();
		
		// Initialize the text field array that holds references to all the text fields that need to be set and positioned
		mTextFieldArray = new Array ();
		
		// Initialize the data provider array
		mDataProvider = new Array ();
		
		// Create the style sheet for the text fields
		mCSS = CSS.getTickerStyle();
		
		// Set the slow time from the config file
		mTimeSlow = OMM.getConfig().getAsNumber("ticker.time.slow");
		mTimeFast = OMM.getConfig().getAsNumber("ticker.time.fast");
	}
	
	/** Create clips and text fields and stuff */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create the base clip
		mBaseClip = this.createEmptyMovieClip("mBaseClip", this.getNextHighestDepth ());

		// Create the mask clip
		mMaskClip = this.createEmptyMovieClip("mMaskClip", this.getNextHighestDepth ());
		mMaskClip.onMouseMove = Delegate.create(this, isOver);
		
		// Create the container clip which holds the text field....this allows us to mask the text field
		mContainerClip = this.createEmptyMovieClip("mContainerClip", this.getNextHighestDepth ());
		
		// Set the mask 
		mContainerClip.setMask (mMaskClip);
		
		// Create clips which we will draw the arrows in
//		mArrowBack = this.createEmptyMovieClip ("mArrowBack", this.getNextHighestDepth ());
//		mArrowForward = this.createEmptyMovieClip ("mArrowForward", this.getNextHighestDepth ());
		
		// Draw the arrows....since they won't really be changing when the ticker changes size, we only need to 
		// draw them once here and not in the draw method
//		var triangleWidth:Number = 10;
//		mArrowBack.beginFill (0, 100);
//		Triangle.draw (mArrowBack, triangleWidth, triangleWidth, "left");
//		mArrowBack.endFill ();
//		mArrowForward.beginFill (0, 100);
//		Triangle.draw (mArrowForward, triangleWidth, triangleWidth, "right");
//		mArrowForward.endFill ();
//		
//		// Set button events on the arrows
//		mArrowForward.onPress = Delegate.create (this, onPressArrowBack);
//		mArrowForward.onRelease = mArrowForward.onReleaseOutside = mArrowBack.onRelease = mArrowBack.onReleaseOutside = Delegate.create (this, onReleaseArrowForward);
//		mArrowBack.onPress = Delegate.create (this, onPressArrowForward);
	}
	
	/** Draw stuff */
	private function draw ():Void
	{
		super.draw ();
		
		// Draw the base
		mBaseClip.clear ();
//		mBaseClip.lineStyle(0, 0xCCCCCC, 100, true);
		mBaseClip.beginFill (OMM.getConfig().getAsNumber("ticker.background.color"), 0);
		Rect.draw (mBaseClip, mWidth, mHeight);
		mBaseClip.endFill ();	
	}
	
	/** Arrange all the elements. This method gets called when the instance is first created as well as every time it is resized. */
	private function size ():Void
	{
		super.size ();
		
		// Position the back and forward arrows
		var arrowOffset:Number = 5;
//		mArrowBack._x = arrowOffset;
//		mArrowBack._y = (mHeight - mArrowBack._height) / 2;
//		mArrowForward._x = mWidth - mArrowForward._width - arrowOffset;
//		mArrowForward._y = (mHeight - mArrowForward._height) / 2;
		
		// Position the mask clip
//		mMaskClip._x = mArrowBack._x + mArrowBack._width;
		
		// Draw the mask
		mMaskClip.clear ();
		mMaskClip.beginFill (0xFFFFFF, 40);
//		Rect.draw (mMaskClip, mArrowForward._x - mMaskClip._x, mHeight);
		Rect.draw (mMaskClip, mWidth, mHeight);
		mMaskClip.endFill ();
		
		// Stop the cycling
		stop ();
		
		// Recalculate the speed
		calculateSpeed (isFast ? mTimeFast : mTimeSlow);

		// Wait a split second before trying to start the cycling again
		_global.clearTimeout (mTimeoutId);
		mTimeoutId = _global.setTimeout (this, "start", 100);
		
		onLoad();
	}
	
	/** 
	 * Creates a text field and populates it with the appropriate ticker message
	 * @param pObj The ticker message used to populate the text field
	 */
	private function createMessage (pObj:TickerMessage):Void
	{
		// If nothing is passed in for the parameter.....get down
		if (!pObj)
			return;
			
		// Figure out the depth to use....there shouldn't ever be more than say 100 on screen at once, so we will use this as our reset point
		mTextDepth = mTextDepth < 100 ? mTextDepth + 1 : 0;
		
		// Create the text field object
		var txt:TextField = mContainerClip.createTextField ("txt" + mTextDepth, mTextDepth, 0, 0, 0, 0);
		
		// Set other properties for the text field
		txt.html = true;
		txt.autoSize = true;
		//txt.border = true;
		txt.selectable = false;
		txt.styleSheet = mCSS;
		txt._x = mWidth;
		
		// Get the text and url properties from the ticker message
		var text:String = pObj.text;
		var url:String = pObj.url;
		
		// See if the url is defined
		if (url)
			text = "<a href='" + url + "'>" + text + "</a>";
			
		// Set the html text
		txt.htmlText = "<p class='body'>" + text + "</p>";
		
		// Set y position now that the text is in there to give dimension
		txt._y = (mHeight - txt._height) / 2;
		
		// Stick the newly created text field into the array
		mTextFieldArray.push (txt);
	}
	
	/**
	 * Calculate the speed necessary in order for the _x position of the text to go across the ticker in the specified time.  This is a close enough 
	 * calculation.  This is not the time for the text to be off screen on right to offscreen on left.  Since each text field will be different sizes, we 
	 * can't use the width of any one field.  So this calculation is GED (good enough dang it)
	 * @param pTime The amount of time (seconds) the text should take to get from one side to the other
	 */
	private function calculateSpeed (pTime:Number):Void
	{
		mSpeed = -mWidth / (pTime * FPS);
	}
	
	/** When ticker is scrolled backwards by user, this will rearrange the data provider so it will show the correct message */
	private function setPreviousMessage ():Void
	{
		// Get the last element
		var last:TickerMessage = TickerMessage (mDataProvider.pop());
		
		// Stick it at the beginning of the data provider
		mDataProvider.unshift(last);
	}
	
	/** 
	 * Gets the next ticker message to show and rearranges the data provider array so the next message is locked and loaded
	 * @return The ticker message to show in the text field
	 */
	private function getNextMessage ():TickerMessage
	{
		// If there are no elements in the data provider....
		if (!mDataProvider.length)
			return undefined;
		
		// Remove the first element from the data provider
		var first:TickerMessage = TickerMessage (mDataProvider.shift());
		
		// If the element has not expired, then stick it back on the end of the data provider
		if (!first.hasExpired())
			mDataProvider.push(first);
			
		// Return the ticker message
		return first;
	}
	
	/**
	 * Setter for the messages to show in the ticker
	 * @param pArray The array to set the data provider to
	 */
	public function setDataProvider (pArray:Array):Void
	{
		mDataProvider = pArray;
		
		// Set the initial message text
		createMessage (getNextMessage());
		
		// Set the initial speed
		calculateSpeed (mTimeSlow);
		
		// Start the cycling
		start ();
	}
	
	/**
	 * Checks the data provider to see if any messages of a particular id exist.  It doesn't take zero since we can have multiple zero id's
	 * @param pId The id value to check the data provider for
	 * @return The index value of the message object with the specified id.  If one does not exist, then -1 is returned
	 */
	public function checkForId (pId:String):Number
	{
		var max:Number = mDataProvider.length;
		for (var i:Number = 0; i < max; i++)
		{
			var message:TickerMessage = TickerMessage (mDataProvider[i]);
			var id:String = message.id;
			if (id == pId)
				return i;
		}
		
		return -1;
	}
	
	/**
	 * Checks the data provider to see which of the messages is the oldest
	 * @return The id value of the message object that has been in the data provider the longest
	 */
	private function getOldestMessage ():String
	{
		// Get a copy of the data provider array
		var temp:Array = mDataProvider.slice();
		
		// Sort the temp array by the timeStamp property.  The lowest of these values will be the oldest one
		temp.sortOn ("timeStamp");
		
		// Get the first in the array
		var first:TickerMessage = TickerMessage (temp[0]);
		
		// Return the oldest id
		return first.id;
	}
	
	/** 
	 * Add a ticker message to the beginning of the data provider 
	 * @param pMessage The ticker message object to add to the data provider
	 */
	public function addItem (pMessage:TickerMessage):Void 
	{
		addItemAt(0, pMessage);
		resetTextFieldPosition ();
	}
	
	/** 
	 * Add a ticker message to the specified index of the data provider 
	 * @param pIndex The index at which the message is added
	 * @param pMessage The ticker message object to add to the data provider
	 */
	private function addItemAt (pIndex:Number, pMessage:TickerMessage):Void 
	{
		// The id value of the ticker message
		var id:String = pMessage.id;
		
		// If the data provider was empty....
		var wasEmpty:Boolean = mDataProvider.length == 0;
		
		// If the id is not zero.....
		if (id != "0")
		{
			// Find out if a message with the specified id already exists
			var index:Number = checkForId(id);
		
			// If there is a message with the specified id, the get rid of it
			if (index >= 0)
				mDataProvider.splice (index, 1);
		}
		// If the length of the data provider is at it's max, then we got's to boot the oldest message
		if (mDataProvider.length >= maxCount)
		{
			var oldestId:String = getOldestMessage ();
			var oldestIndex:Number = checkForId(oldestId);
			removeItemAt (oldestIndex);
		}
	
		// Add the message to the data provider in the specified point
		mDataProvider.splice (pIndex, 0, pMessage);
		
		// Start cycling of the ticker if the data provider was empty before adding the most current message
		if (wasEmpty)
			start();
	}
	
	/** 
	 * Removes ticker message from the data provider
	 * @param pIndex The index value of the ticker message to be removed
	 */
	public function removeItemAt (pIndex:Number):Void
	{
		mDataProvider.splice (pIndex, 1);
	}
	
	/** Start the cycling of the ticker */
	private function start ():Void
	{
		// If the data provider is empty and the text field array is empty....get tooo da choppaaaa!!!
		if (!mDataProvider.length && !mTextFieldArray.length)
			return;
		
		// Get the text moving
		onEnterFrame = moveTextField;
	}
	
	/** Stop the cycling of the ticker */
	public function stop ():Void
	{
		delete onEnterFrame;
	}
	
	/** 
	 * Remove a text field from the stage and from the array
	 * @param pTxt The text field to remove
	 */
	private function removeMessage (pTxt:TextField):Void
	{
		// Loop thru the txt field array to find the specified text field
		var max:Number = mTextFieldArray.length;
		for (var i:Number = 0; i < max; i++)
		{
			// Current element 
			var txt:TextField = mTextFieldArray[i];
			
			// If the current element is the same as the specified parameter....
			if (txt == pTxt)
			{
				// We've found our text field....get rid of him
				mTextFieldArray.splice(i, 1);
				
				// Break out of the loop
				break;
			}
		}
		
		// Remove the text field from the stage
		pTxt.removeTextField();
	}
	
	/** Remove all of the messages from the data provider */
	public function clearTicker():Void
	{
		while (mDataProvider.length > 0)
			mDataProvider.shift();
	}
	
	/** Animates the text field in numeric order of the data provider index values */
	private function moveTextField ():Void
	{
		// Loop thru the txt field array and position them
		var max:Number = mTextFieldArray.length;
		for (var i:Number = 0; i < max; i++)
		{
			var txt:TextField = mTextFieldArray[i];
			txt._x += mSpeed;
		}
		
		// Get references to the first and last text fields
		var first:TextField = mTextFieldArray[0];
		var last:TextField = mTextFieldArray[max - 1];
		
		// If the first text field has past the far left side...
		if (first._x < -first._width)
			removeMessage (first);
		
		// If the last text field has past the far right side...
		if (last._x > mWidth)
		{
			removeMessage (last);
			setPreviousMessage ();
		}
		
		// If the last text field is a certain distance away from the right edge or if there are no text fields....
		if (last._x + last._width < mWidth - mGap || max == 0)
			createMessage (getNextMessage());
	}
	
	/** Removes all text fields from the stage and from the array */
	private function resetTextFieldPosition():Void
	{
		while (mTextFieldArray.length > 0)
		{
			var txt:TextField = TextField(mTextFieldArray.shift());
			txt.removeTextField();
		}
	}
	
	/** On press of the forward arrow */
	private function onPressArrowForward ():Void
	{
		calculateSpeed (mTimeFast);
		isFast = true;
	}
	
	/** On release of the forward arrow */
	private function onReleaseArrowForward ():Void
	{
		calculateSpeed (mTimeSlow);
		isFast = false;
	}
	
	/** On press of the Back arrow */
	private function onPressArrowBack ():Void
	{
		calculateSpeed (-mTimeFast);
		isFast = true;
	}
	
	/** Method called by the mouse move of the mask clip */
	private function isOver ():Void
	{
		// If the mouse is over the mask clip.....stop the ticker, otherwise start it
		if (mMaskClip.hitTest(_root._xmouse, _root._ymouse))
			stop ();
		else
			start ();
	}
}
