﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;

import kaneva.events.Event;
import kaneva.events.MediaEvent;
import kaneva.omm.IEventDispatcher;
import kaneva.omm.OMM;

/**
 * Contains all the CSS objects that are used throughout the widgets.  These StyleSheet objects
 * load external CSS files before any of the widgets try to use them.  This ensures that the css
 * is available when needed and there is not jumpiness when widgets try to set stylesheet properties
 * to the text fields
 * @author Scott
 */
class kaneva.omm.CSS implements IEventDispatcher
{
	/** All the necessary CSS objects */
	private static var AdditionalButtonStyle:TextField.StyleSheet = new TextField.StyleSheet ();
	private static var ButtonStyle:TextField.StyleSheet = new TextField.StyleSheet ();
	private static var MediaNavButtonStyle:TextField.StyleSheet = new TextField.StyleSheet ();
	private static var MediaBaseStyle:TextField.StyleSheet = new TextField.StyleSheet ();
	private static var MediaVideoBaseStyle:TextField.StyleSheet = new TextField.StyleSheet ();
	private static var ShareMenuStyle:TextField.StyleSheet = new TextField.StyleSheet ();
	private static var PlayListStyle:TextField.StyleSheet = new TextField.StyleSheet ();
	private static var SliderStyle:TextField.StyleSheet = new TextField.StyleSheet ();
	private static var TickerStyle:TextField.StyleSheet = new TextField.StyleSheet ();
	private static var ReportWindowStyle:TextField.StyleSheet = new TextField.StyleSheet ();
	private static var ShareWindowStyle:TextField.StyleSheet = new TextField.StyleSheet ();
	
	/** Points to which object we are currently loading */
	private var mIterator:Number = 0; 
	
	/** Array containing references to the css objects used throughout the widgets */
	private var mCSSArray:Array;
	
	/** mapping names for the css files contained within the Config map*/
	private var additionalButton:String = "css.button.additional";
	private var buttonButton:String = "css.button.button";
	private var mediaNavButton:String = "css.button.medianav";
	private var mediaBase:String = "css.media.base";
	private var mediaVideoBase:String = "css.media.video.base";
	private var shareMenu:String = "css.menu.share";
	private var playlist:String = "css.playlist";
	private var slider:String = "css.slider";
	private var ticker:String = "css.ticker";
	private var reportWindow:String = "css.window.report";
	private var shareWindow:String = "css.window.share";
	
	
	/** Constructor */
	public function CSS ()
	{
		// Give this object the ability to add and remove listeners, and dispatch events.
		EventDispatcher.initialize (this);
		
		// Create a multidimensional array that contains the style sheet object and the file to load
		mCSSArray = [
					  [AdditionalButtonStyle, additionalButton],
					  [ButtonStyle, buttonButton],
					  [MediaNavButtonStyle, mediaNavButton],
					  [MediaBaseStyle, mediaBase],
					  [MediaVideoBaseStyle, mediaVideoBase],
					  [ShareMenuStyle, shareMenu],
					  [PlayListStyle, playlist],
					  [SliderStyle, slider],
					  [TickerStyle, ticker],
					  [ReportWindowStyle, reportWindow],
					  [ShareWindowStyle, shareWindow]
				 	];
		

	}

	/** begins loading of all of the stylesheets.  Once the initial load is complete, getNextFile continues the loads -- used by OMM.as*/
	public function loadStyleSheets():Void
	{
		// Initializes the process of loading all the css files necessary
		mIterator = 0;
		load (OMM.getConfig().getAsString(mCSSArray[mIterator][1]));
	}
	
	/** 
	 * Loads the specified css file into the specified css object
	 * @param pUrl The css file to load
	 */
	private function load (pUrl:String):Void
	{
		if (pUrl == "")
		{
			getNextFile ();
		}
		else
		{
			// Load the specified css file into the specified css object
			var css:TextField.StyleSheet = mCSSArray[mIterator][0];
			css.onLoad = Delegate.create(this,onLoadCSS);
			css.load(pUrl);
		}
	}
	
	/** Increases the iterator by one and tries to load the next css file */
	private function getNextFile ():Void
	{
		if (mIterator < mCSSArray.length - 1)
		{
			mIterator++;
			load (OMM.getConfig().getAsString(mCSSArray[mIterator][1]));
		}
		else
			onComplete();
	}
	
	/** event dispatched to the OMM to let it know that all of the stylesheets have completed loading*/
	private function onComplete ():Void
	{
		var event:MediaEvent = new MediaEvent();
		event.type = MediaEvent.COMPLETE;
		dispatchEvent(event);
	}
	
	/** 
	 * Invoked when a load() operation has completed
	 * @param pSuccess A Boolean value that indicates whether the CSS file loaded successfully (true) or not (false).
	 */
	private function onLoadCSS (pSuccess:Boolean):Void
	{
		if(!pSuccess)
			OMM.getConfig().setValue(mCSSArray[mIterator][1],"");

		// whether the load was successful or not, continue loading the other css files if they exist
		if (mIterator < mCSSArray.length - 1)
			getNextFile ();
		else
			onComplete ();
	}
	
	/** @return StyleSheet object for the buttons on the pop up windows */
	public static function getButtonStyle():TextField.StyleSheet	
	{
		if(OMM.getConfig().getAsString("css.button.button") == "")
		{
			ButtonStyle.setStyle(".label", {color:"#5A5A5A", fontFamily:"Verdana", fontSize:"11", textAlign:"center"});
			ButtonStyle.setStyle(".over", {color:"#000000"});
			ButtonStyle.setStyle(".out", {color:"#5A5A5A"});		
		}
		
		return ButtonStyle;
	}
	
	/** @return StyleSheet object for the media nav buttons */
	public static function getMediaNavButtonStyle():TextField.StyleSheet	
	{
		if(OMM.getConfig().getAsString("css.button.medianav") == "")
		{
			MediaNavButtonStyle.setStyle(".label", {color:"#FFFFCC", fontFamily:"Verdana", fontSize:"12"});
			MediaNavButtonStyle.setStyle(".out", {textDecoration:"underline"});
			MediaNavButtonStyle.setStyle(".over", {textDecoration:"none"});			
		}
		return MediaNavButtonStyle;
	}
	
	/** @return StyleSheet object for the media base text */
	public static function getMediaBaseStyle():TextField.StyleSheet	
	{
		if(OMM.getConfig().getAsString("css.media.base") == "")
		{
			MediaBaseStyle.setStyle(".label", {color:"#656565", fontFamily:"Verdana", fontSize:"12", fontWeight:"bold"});
		}
		return MediaBaseStyle;
	}
	
	/** @return StyleSheet object for the video base text */
	public static function getMediaVideoBaseStyle():TextField.StyleSheet	
	{
		if(OMM.getConfig().getAsString("css.media.video.base") == "")
		{
			MediaVideoBaseStyle.setStyle (".body", {color:"#FFFFFD", fontFamily:"Verdana", fontSize:"10"});
			MediaVideoBaseStyle.setStyle (".content", {marginLeft:"15", marginRight:"30"/*, textAlign:"left"*/});
			MediaVideoBaseStyle.setStyle (".profile", {marginRight:"10", textAlign:"right"});
			MediaVideoBaseStyle.setStyle (".title", {color:"#FFFFCC", fontSize:"15", fontWeight:"bold"});
			MediaVideoBaseStyle.setStyle (".yellow", {color:"#FFFFCC"});
			MediaVideoBaseStyle.setStyle (".user", {fontWeight:"bold"});
			MediaVideoBaseStyle.setStyle ("a:hover", {textDecoration:"none"});
			MediaVideoBaseStyle.setStyle ("a:link", {textDecoration:"underline"});			
		}
		return MediaVideoBaseStyle;
	}
	
	/** @return StyleSheet object for the share menu */
	public static function getShareMenuStyle():TextField.StyleSheet	
	{
		if(OMM.getConfig().getAsString("css.menu.share") == "")
		{
			ShareMenuStyle.setStyle(".label", {color:"#666666", fontFamily:"Verdana", fontSize:"12", fontWeight:"bold"});
		}
		return ShareMenuStyle;
	}
	
	/** @return StyleSheet object for the play list*/
	public static function getPlayListStyle():TextField.StyleSheet	
	{
		if(OMM.getConfig().getAsString("css.menu.share") == "")
		{
			PlayListStyle.setStyle (".body", {color:"#000000", 
									fontFamily:"Verdana", 
									fontSize:"11", 
									marginLeft:"5", 
									marginRight:"0", 
									textAlign:"left"});
			PlayListStyle.setStyle (".title", {fontSize:"12", fontWeight:"bold"});
			PlayListStyle.setStyle (".subtitle", {fontSize:"11", fontWeight:"bold"});
			PlayListStyle.setStyle ("a:link", {textDecoration:"underline"});
			PlayListStyle.setStyle ("a:hover", {color:"#AAAAAA", textDecoration:"none"});
			// the color of the text within the unselected and selected cells of the playlist
			PlayListStyle.setStyle(".selected", {color:"#FFFFFF"});
			PlayListStyle.setStyle(".unselected", {color:"#000000"});			
		}
		return PlayListStyle;
	}
	
	/** @return StyleSheet object for the slider component text */
	public static function getSliderStyle():TextField.StyleSheet	
	{
		if(OMM.getConfig().getAsString("css.slider") == "")
		{
			SliderStyle.setStyle(".text", {color:"#484848", fontFamily:"Verdana", fontSize:"9"});
		}
		return SliderStyle;
	}
	
	/** @return StyleSheet object for the ticker text */
	public static function getTickerStyle():TextField.StyleSheet	
	{
		if(OMM.getConfig().getAsString("css.ticker") == "")
		{
			TickerStyle.setStyle (".body", {color:"#FFFFFF", fontFamily:"Verdana", fontSize:"12"});
			TickerStyle.setStyle ("a:link", {color:"#FFFF00", textDecoration:"underline"});
			TickerStyle.setStyle ("a:hover", {color:"#DDDD00", textDecoration:"none"});			
		}
		return TickerStyle;
	}
	
	/** @return StyleSheet object for the report pop up window */
	public static function getReportWindowStyle():TextField.StyleSheet	
	{
		if(OMM.getConfig().getAsString("css.window.report") == "")
		{
			ReportWindowStyle.setStyle(".label", {color:"#666666", fontFamily:"Verdana", fontSize:"11"});			
		}
		return ReportWindowStyle;
	}
	
	/** @return StyleSheet object for the share pop up window */
	public static function getShareWindowStyle():TextField.StyleSheet	
	{
		if(OMM.getConfig().getAsString("css.window.share") == "")
		{
			ShareWindowStyle.setStyle(".label", {fontFamily:"Verdana"});
			ShareWindowStyle.setStyle(".text", {color:"#666666", fontFamily:"Verdana", fontSize:"11"});
			ShareWindowStyle.setStyle(".help", {color:"#666666", fontFamily:"Verdana", fontSize:"11", fontWeight:"bold"});
		}
		return ShareWindowStyle;
	}
	
	/** 
	 * Implement the EventDispatcher methods
	 * @see mx.events.EventDispatcher
	 */
	public function addEventListener(pEvent:String, pListener:Function):Void {}
	public function dispatchEvent(pEventObj:Event):Void {}
	public function removeEventListener(pEvent:String, pListener:Function):Void {}
}