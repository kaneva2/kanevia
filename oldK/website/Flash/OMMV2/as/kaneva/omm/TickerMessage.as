﻿/**
 * The object that is added to the ticker's data provider array to show messages in the ticker component.  This object
 * keeps track of when it was created and when it should be done away with using the Date object.  Optional url property
 * can be specified so that the text is a link
 * @author Scott
 */
class kaneva.omm.TickerMessage
{
	/** 
	 * Id is the message type id. Only one message of a type will be in the ticker list at one time.  
	 * If another one comes down, it will replace the old one (text, hyperlink, and reset display time).  
	 * For example, rave messages will have an id of, say, 3.  So “You have raved the Item” will be 
	 * replaced with “You have already raved the item” if the user raves it again since both will 
	 * have the same message id. Any number of messages with an Id of zero (0) can be in the list
	 */
	public var id:String = "";
	
	/** The text to be displayed in the ticker */
	public var text:String = "";
	
	/** Max display time (seconds) is the maximum time a message will live in the list.  Zero (0) is forever. */
	public var max:Number = 0;
	
	/** Hyperlink will make the Text a hyperlink */
	public var url:String = null;
	
	/** Used to keep track of when this object was created.  This value should be in seconds */
	public var timeStamp:Number;
	
	/** Constructor function */
	public function TickerMessage ()
	{
		// Use the date object to set the time stamp so we know when this object was created
		var currentDate:Date = new Date();
		timeStamp = currentDate.getTime() / 1000;
	}
	
	/** 
	 * Checks if this message has expired or not 
	 * @return True if message has expired, false otherwise
	 */
	public function hasExpired ():Boolean
	{
		if (max > 0)
		{
			// Get the current time in seconds
			var currentDate:Date = new Date();
			var currentTime:Number = currentDate.getTime() / 1000;
			
			// Get the time which this message should last to
			var expirationTime:Number = timeStamp + max;
			
			// If the current time is greater than the expiration time, then return true.....else false
			return currentTime > expirationTime;
		}
		
		// If the time stamp is zero, then this will always return false
		return false;
	}
}
