﻿/**
 * for setting and getting xml share data
 * @author Kevin
 */
class kaneva.omm.io.ShareData
{
	/** url of the details for shared media*/
	public var detailsUrl:String = "";
	
	/** embedded info for shared media*/
	public var embedInfo:String = "";
}