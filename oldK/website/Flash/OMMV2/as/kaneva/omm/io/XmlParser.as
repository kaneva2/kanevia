﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;

import kaneva.events.Event;
import kaneva.events.MediaEvent;
import kaneva.lang.reflect.Field;
import kaneva.lang.reflect.Method;
import kaneva.media.MediaAsset;
import kaneva.media.PlayListAsset;
import kaneva.omm.Config;
import kaneva.omm.IEventDispatcher;
import kaneva.omm.io.ShareData;
import kaneva.omm.Logger;
import kaneva.omm.widgets.LocationParser;
import kaneva.omm.widgets.QueueParser;
import kaneva.omm.widgets.WidgetData;

/**
 * parse an XML file that is formatted for OMM usage
 * @author Kevin
 */
class kaneva.omm.io.XmlParser implements IEventDispatcher
{
	
	/** the xml that will be loaded and parsed*/
	public var mXML:XML;
	
	/** object where all configuration mapping metadata will be stored. */
	public var mConfig:Config;
	
	/** a temporary widget object arrary */
	public var mWidgetArray:Array;

	/** a temporary media object*/
	public var mMediaAsset:MediaAsset;
	
	/** a temporary playlist object*/
	public var mPlayList:PlayListAsset;
	
	/** a temporary share data object*/
	public var mShareData:ShareData;
	
	/** 
	 * Constructor 
	 * @param	input	a string(filename) that represents which file is to be parsed
	 */
	public function XmlParser(input:String){
		if(input)
			this.initializeXML(input);
		EventDispatcher.initialize (this);
		mConfig = new Config();
		mConfig.emptyConfigMap();
		mWidgetArray = new Array();
	}



	/**
	 * begin loading the file to be parsed.  Once it is loaded, 
	 * the function handleXML will be called to begin the parsing
	 * of the inputFile
	 */
	public function initializeXML(inputFile:String):Void
	{
		// load the XML file into memory
		// create xml object
		mXML = new XML();
		mXML.ignoreWhite = true;
		// onload
		mXML.onLoad = Delegate.create (this, handleXML);
		mXML.load(inputFile); 
	}

	
	

	/**
	 * begins reading the nodes within the given XML file.  Upon reading the
	 * <omm-resp> tag, the recursiveParsing function will be called on the rest
	 * of the nodes.

	 1. 0/many configs may be read.
	 2. there will then be 0/1 confirm or error tags
	 3. there will then be 0/1 widgets
	 4. finally, there will be 0/1 playlists
	 */
	public function handleXML(success:Boolean):Void
	{
		if(success)
		{
			// rootNode will always be <omm-resp>
			var rootNode:XMLNode = mXML.firstChild;
			
			// start reading in the useful metadata
			if(rootNode.hasChildNodes())
			{
				var rootChildren:Array = rootNode.childNodes;
				var numberOfRootChildren:Number = rootChildren.length;
				
				for (var a:Number = 0; a < numberOfRootChildren; a++)
				{
					var childNode:XMLNode = rootChildren[a];
					//check for configs firstly
					
					switch(childNode.nodeName)
					{
						case "config":
							mConfig.setValue(childNode.attributes.name, childNode.attributes.value);
//							Logger.logMsg("Setting config value: " + childNode.attributes.name + "=" + childNode.attributes.value);
							break;
						case "confirm":
							rootChildren = childNode.childNodes;
							numberOfRootChildren = rootChildren.length;
							
							if(rootChildren[0].nodeName == "widget")
							{
								for (var i:Number = 0; i < numberOfRootChildren; i++)
								{
									mMediaAsset = readMediaAssetXML(rootChildren[i]);
								}			
							}
							else if (rootChildren[0].nodeName == "media")
							{
								for (var j:Number = 0; j < numberOfRootChildren; j++)
								{
									mMediaAsset = readMediaAssetXML(rootChildren[j]);
								}			
							}
							break;
						case "playlist":
							mPlayList = readPlayListXML(childNode);
							break;
						case "widgets":
							mWidgetArray = readWidgetsXML(childNode);//.push(readWidgetXML(childNode));
							break;
						case "error":
							Logger.logMsg("error node sent");
							break;
						default:
							Logger.logMsg("Error reading XML");
					}					
									
				}
				
				// Dispatch a complete event
				var event:MediaEvent = new MediaEvent();
				event.type = MediaEvent.COMPLETE;
				dispatchEvent(event);
				Logger.logMsg("xml load complete");
			}
		}
	}
	 
	/** 
	 * parses a xml widgets tag
	 * @param pXmlNode is the current node
	 * @return array of widget objects
	 */
	public function readWidgetsXML(pXmlNode:XMLNode):Array
	{
		var tempWidgetArray:Array = new Array();
		
		var xmlNodeChildren:Array = pXmlNode.childNodes;
		var numberOfXmlChildren:Number = xmlNodeChildren.length;
		
		// this node contains other widgetNodes
		if(xmlNodeChildren[0].nodeName == "widget")
		{
			for(var h:Number = 0; h<numberOfXmlChildren; h++)
				tempWidgetArray.push(readWidgetXML(xmlNodeChildren[h]));
		}
		
		return tempWidgetArray;
		
	}
	
	
	/**
	 * reads through a widget xml node - one widgetNode can contain mulitple widgetsNodes
	 * @param	pXmlNode is the widget node
	 * @return	a WidgetData object
	 */
	public function readWidgetXML(pXmlNode:XMLNode):WidgetData
	{
		var widgetData:WidgetData = new WidgetData();
		
		var xmlNodeChildren:Array = pXmlNode.childNodes;
		var numberOfXmlChildren:Number = xmlNodeChildren.length;

		// read in the widget tag
		widgetData.id = pXmlNode.attributes.id;
		widgetData.uri = pXmlNode.attributes.uri;
		
		for (var i:Number = 0; i < numberOfXmlChildren; i++){
			var childNode:XMLNode = xmlNodeChildren[i];
			
			if(childNode.nodeName == "queue")
			{
				var qp:QueueParser = new QueueParser();
				widgetData.queue = qp.fromXml(childNode);
			}
			
			if(childNode.nodeName == "location")
			{
				var lp:LocationParser = new LocationParser();
				widgetData.location = lp.fromXml(childNode);
			}
			
			
			// read in field data
			if(childNode.nodeName == "param")
			{
				var field:Field = new Field();
				field.name = childNode.attributes.name;
				field.text = childNode.firstChild.nodeValue;
				widgetData.fields.push(field);
			}
			// read in the method data and args
			else if (childNode.nodeName == "method")
			{
				var method:Method = new Method();
				method.args = new Array();
				method.name = childNode.attributes.name;
				
				if(childNode.hasChildNodes())
				{
					
					var grandChildrenArray:Array = childNode.childNodes;
					var grandChildrenLength:Number = grandChildrenArray.length;

					// reading in the args
					for (var h:Number = 0; h < grandChildrenLength; h++)
					{
						var tempNodeValue:Object = null;
						var tempNumber:Number = Number.NaN;
	
						var grandChildNode:XMLNode = grandChildrenArray[h];
						tempNodeValue = grandChildNode.childNodes[0];
						tempNumber = parseFloat(tempNodeValue.toString()); // "3.75bert"  outputs: 3.75
	
						// add Number arg to array
						if(tempNumber == Number(tempNodeValue.toString())){
							method.args.push(tempNumber);
						}
						// add Boolean arg to array
						else if(tempNodeValue.toString() == "true")	{
							method.args.push(true);
						}
						else if(tempNodeValue.toString() == "false"){
							method.args.push(false);
						}
						// add String arg to array
						else{
							method.args.push(tempNodeValue.toString());
						}
					}
				widgetData.methods.push(method);				
				}
			}
		}//end for
		return widgetData;
	}
	
	
	/**
	 * reads through a media xml node
	 * @param	pXmlNode - the media node
	 * @return	void
	 */
	public function readMediaAssetXML(pXmlNode:XMLNode):MediaAsset
	{
		var mediaAsset:MediaAsset = new MediaAsset();
		
		var xmlNodeChildren:Array = pXmlNode.childNodes;
		var numberOfXmlChildren:Number = Number(xmlNodeChildren.length);
		// assuming media tag is the pXmlNode
		if(pXmlNode.attributes.id)
			mediaAsset.id = Number(pXmlNode.attributes.id);
		if(pXmlNode.attributes.type)
			mediaAsset.type = pXmlNode.attributes.type;
		if(pXmlNode.attributes.user)
			mediaAsset.user = pXmlNode.attributes.user;
		if(pXmlNode.attributes.released)
			mediaAsset.released = pXmlNode.attributes.released;
		if(pXmlNode.attributes.thumb)
			mediaAsset.thumb = pXmlNode.attributes.thumb;
		if(pXmlNode.attributes.size)
			mediaAsset.size = Number(pXmlNode.attributes.size);
		if(pXmlNode.attributes.url)
			mediaAsset.url = pXmlNode.attributes.url;
		if(pXmlNode.attributes.length)
			mediaAsset.length = pXmlNode.attributes.length;
		if(pXmlNode.attributes.rating)
			mediaAsset.rating = pXmlNode.attributes.rating;
		if(pXmlNode.attributes.mature == "true")
			mediaAsset.mature = new Boolean(true);
		if(pXmlNode.attributes.userThumb)
		   mediaAsset.userThumb = pXmlNode.attributes.userThumb;
		if(pXmlNode.attributes.userUrl)
		   mediaAsset.userUrl = pXmlNode.attributes.userUrl;
		if(pXmlNode.attributes.raved == "true")
			mediaAsset.raved = new Boolean(true);
		if(pXmlNode.attributes.inlib == "true")
			mediaAsset.inlib = new Boolean(true);
		
		// read through the rest of the nodes within the xml file
		for (var a:Number = 0; a < numberOfXmlChildren; a++){
			
			var childNode:XMLNode = xmlNodeChildren[a];
			
			// read in description data
			if(childNode.nodeName == "desc")
			{
				if(childNode.attributes.title)
					mediaAsset.title = childNode.attributes.title;
				if(childNode.attributes.author)
					mediaAsset.author = childNode.attributes.author;
				if(childNode.attributes.album)
					mediaAsset.album = childNode.attributes.album;
				if(childNode.childNodes[0])
				{
					if ( childNode.firstChild.nodeValue == null )
						mediaAsset.descriptionValue = childNode.nodeValue;
					else
						mediaAsset.descriptionValue = childNode.firstChild.nodeValue;
				}
			}
			// read in the stats data
			else if(childNode.nodeName == "stats")
			{
				if(childNode.attributes.views)
					mediaAsset.views = Number(childNode.attributes.views);
				if(childNode.attributes.thumbsup)
					mediaAsset.thumbsup = Number(childNode.attributes.thumbsup);
				if(childNode.attributes.channelCount)
					mediaAsset.channelCount = Number(childNode.attributes.channelCount);
				if(childNode.attributes.shares)
					mediaAsset.shares = Number(childNode.attributes.shares);
				if(childNode.attributes.cost)
					mediaAsset.cost = childNode.attributes.cost;
			}
			// read in the tags metadata
			// 'tags' is an Array within the MediaAsset.  Each item in the array is
			// obtained grabbing the data before each comma/delimiter 
			else if (childNode.nodeName == "tags")
			{
				// change to an array of strings using ',' as a delimiter
				if(childNode.childNodes[0])
					mediaAsset.tags = String(childNode.childNodes[0]).split( "," );
			}
			else if(childNode.nodeName == "comments")
			{
				if(childNode.attributes.total)
					mediaAsset.total = Number(childNode.attributes.total);
			}
		}
		
		return mediaAsset;
	}
	
	
	
	/**
	 * reads through a playlist xml node
	 * @param	pXmlNode	the playlist node
	 * @return	a playList object
	 */
	public function readPlayListXML(pXmlNode:XMLNode):PlayListAsset
	{
		var playList:PlayListAsset = new PlayListAsset();
		
		var xmlNodeChildren:Array = pXmlNode.childNodes;
		var numberOfXmlChildren:Number = xmlNodeChildren.length;
		
		// assuming playlist tag is the pXmlNode
		playList.id = Number(pXmlNode.attributes.id);
		playList.author = pXmlNode.attributes.author;
		playList.title = pXmlNode.attributes.title;
		
		for (var i:Number = 0; i < numberOfXmlChildren; i++){
			var childNode:XMLNode = xmlNodeChildren[i];
			
			// read in field data
			if(childNode.nodeName == "media")
			{
				var mediaAsset:MediaAsset = new MediaAsset();
				mediaAsset = readMediaAssetXML(childNode);
				playList.mediaAssets.push(mediaAsset);
			}
		}//end for
		
		return playList;
	}

	/** 
	 * Implement the EventDispatcher methods
	 * @see mx.events.EventDispatcher
	 */
	public function addEventListener(pEvent:String, pListener:Function):Void {}
	public function dispatchEvent(pEventObj:Event):Void {}
	public function removeEventListener(pEvent:String, pListener:Function):Void {}
}
