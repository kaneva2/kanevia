﻿/**
 * for setting and getting omm configuration data
 * @author Kevin
 */
class kaneva.omm.Config
{
	//////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	// If we are testing, then local links are used.....otherwise it will use kaneva links	//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
	
	/** this variable for debugging purposes only */
	public var isTesting:Boolean = false;
	
	/** hashMap of configuration data*/
	public var mConfigMapping:Object;

	/** constructor used to initalize the hashMap*/
	public function Config()
	{
		mConfigMapping = new Object();
		
		// Initialize default configurations
		
		// The css files for all the different text fields throughout the widgets -- default values are found within the CSS.as
		this.setValue("css.button.additional", "") ;
		this.setValue("css.button.button", "") ;
		this.setValue("css.button.medianav", "") ;
		this.setValue("css.media.base", "") ;
		this.setValue("css.media.video.base", "") ;
		this.setValue("css.menu.share", "") ;
		this.setValue("css.playlist", "") ;
		this.setValue("css.slider", "") ;
		this.setValue("css.ticker", "") ;
		this.setValue("css.window.report", "") ;
		this.setValue("css.window.share", "") ;
		

		
		// Set the debug mode to true
		this.setValue("debug", "false") ;
		
		// The number of seconds that images will be shown for before moving on to the next item in the play list
		this.setValue("image.time", "5") ;
		
		// Should the control buttons be shown?
		this.setValue("show.buttons", "true") ;
		
		// Should the SHARE button be shown?
		this.setValue("show.button.share", "true") ;
//		
//		// Should the ADD button be shown?
//		this.setValue("show.button.add", "true") ;
		
		// ADD button URL, {0} should be replaced with asset ID 
		this.setValue("uri.button.add", isTesting ? "http://192.168.2.102/temp/test.jsp" : "/Kaneva/services/omm/request.aspx?type=7&assetId={0}") ; 

		// Should the RAVEIT button be shown?
		this.setValue("show.button.raveIt", "true") ;
		
		// RAVEIT button URL, {0} should be replaced with asset ID 
		this.setValue("uri.button.raveIt", isTesting ? "RaveItTest.xml" : "/Kaneva/services/omm/request.aspx?type=6&assetId={0}") ;
		
		// Should the COMMENT button be shown?
		this.setValue("show.button.comment", "true") ;
		
		// COMMENT button URL, {0} should be replaced with asset ID 
		this.setValue("uri.button.comment", isTesting ? "http://www.google.com" : "/Kaneva/asset/{0}.storeItem#comments") ;

		// GETDETAILs link URL, {0} should be replaced with asset ID 
		this.setValue("uri.link.details", "/Kaneva/asset/{0}.storeItem") ;

		// REPORT link URL, {0} should be replaced with asset ID, {1} with the report radio selection, 
		// {2} with the user comment, and {3} with their email address, if applicable
		this.setValue("uri.link.report", isTesting ? "http://www.google.com" : "/Kaneva/services/omm/request.aspx?type=8&assetId={0}&reportType={1}&comment={2}&email={3}") ;
//
//		// BLOGIT link URL, {0} should be replaced with asset ID 
//		this.setValue("uri.link.blog", isTesting ? "http://192.168.2.102/temp/share.jsp" : "/Kaneva/services/omm/request.aspx?type=9&assetId={0}") ;
//
//		// EMAIL link URL, {0} should be replaced with asset id
//		this.setValue("uri.link.email", "/kaneva/mykaneva/newMessage.aspx?assetId={0}&URL=/asset/{0}.storeItem") ;	
//
		// DIGG IT link URL, {0} should be replaced with details link
		this.setValue("uri.link.digg", "/Kaneva/asset/{0}.storeItem") ;
//		
//		// DELICIOUS link URL, {0} should be replaced with details link
//		this.setValue("uri.link.delicious", "http://del.icio.us/post?url={0}") ;	
//		
		// Help Button link URL seen when the media is in a minimized state
		this.setValue("uri.link.help", "http://www.google.com");
		
		// request that is called whenever an item is played in the MediaPlayer so we can update the view counts for the viewer 
		// and viewee.  {0} will be replaced with the asset's id
		this.setValue("uri.link.viewItem", "~/services/omm/request.aspx?type=10&amp;assetId={0}" );
		
		// The link for sharing asset 
		this.setValue("uri.link.media.share", "/Kaneva/mykaneva/newMessage.aspx?assetId={0}&URL=/asset/{1}.storeItem" );
		
		// The link for the kaneva logo in the media area
		this.setValue("uri.link.media.base.logo", "http://www.kaneva.com" );
		
		// The link for the full screen html page.  This has a query string that allows you to specify the xml to use (startMe) and whether
		// to have it start out in detail mode or not (detailMode)
		this.setValue("uri.link.fullscreen", "FullScreen.html?fullScreen=true&startMe=fullScreen.xml&detailMode=true" );
		
		// The background color for the media base component
		this.setValue("media.base.background.color", "0xE6E6E6") ;

		// The background color for the media button base component
		this.setValue("media.button.base.background.color", "0xE6E6E6");
		
		// title of the playlist
		this.setValue("playlist.title", "Playlist");
		
		// The background color for the play list base component 
		this.setValue("playlist.background.color", "0xE6E6E6");
		
		// The "selected" color for a play list item component 
		this.setValue("playlist.item.selected.color", "0x804040");

		// The list colors for the play list component.  Add more colors to the array to have alternating row colors 
		// needs to be a comma delimited string with numbers for the values ... NO SPACING ALLOWED!!!
		this.setValue("playlist.row.colors", "0xE6E6E6");
		
		// The background color for the share menu 
		this.setValue("share.menu.background.color", "0xE6E6E6");
		
		// The outline color for the share menu 
		this.setValue("share.menu.outline.color", "0xCCCCCC");
		
		// The highlight color for the share menu item 
		this.setValue("share.menu.item.highlight.color", "0xF99F9F");
		
		// The outline color for the track of the slider control component 		
		this.setValue("slider.control.track.outline.color", "0xADADAD");
		
		// The fill color for the track of the slider control component 
		this.setValue("slider.control.track.fill.color", "0xEEEEEE");
		
		// The outline color for the scrub of the slider control component 
		this.setValue("slider.control.scrub.outline.color", "0xACACAC");
		
		// The list colors for the scrub of the slider control component.
		// Add more colors to the array to have alternating row colors 
		// needs to be a comma delimited string with numbers for the values ... NO SPACING ALLOWED!!!
		this.setValue("slider.control.scrub.fill.color", "0xFDFDFD,0xE5E5E3");
		
		// The background color for the ticker base component 
		this.setValue("ticker.background.color", "0xE6E6E6");
		
		// The number of seconds it takes for messages to get from one side of the ticker while arrow button is presed
		this.setValue("ticker.time.fast", "1");
		
		// The number of seconds it takes for messages to get from one side of the ticker to the other.....
		// this is the normal speed when users are not pressing the arrow buttons
		this.setValue("ticker.time.slow", "5");
		
	}
	
	/** 
	 * Sets a value within the config hashMap 
	 * @param pName Name of the property to set
	 * @param pValue Value of the property to set
	 */
	public function setValue(pName:String, pValue:String):Void
	{
		mConfigMapping[pName] = pValue;
	}
	
	/** @return A value from the config hashMap given the correct key */
	public function getAsString(name:String):String
	{
		return mConfigMapping[name];
	}
	
	/**
	 * @param pName name of the configuration value to retrieve
	 * @return  Value as a boolean, true or false.  Returns false if not found.
	 */
	public function getAsBoolean(pName:String):Boolean
	{
		return mConfigMapping[pName] == "true" ? true : false;
	}
	
    /**
	 * Takes in a string value and returns a number primative
	 * @param pName name of the configuration value to retrieve
	 * @return  Value as a number.  
	 */
	public function getAsNumber(pName:String):Number
	{
		return Number(mConfigMapping[pName]) ;
	}
	
	/** 
	 * Takes a comma delimited string and turns it into an array of numbers.  This is used for arrays of colors for example.
	 * Note: Don't have spaces between values or it won't work
	 * @param pValue The string to convert
	 * @return An array with numbers as it's elements
	 */
	public function getAsArrayOfNumbers(pValue:String):Array
	{
		var tempString:String = mConfigMapping[pValue];
		var tempNum:Number;
		var list:Array = tempString.split(",");
		
		for(var i:Number=0; i < list.length; i++)
		{
			tempNum = Number(list[i]);
			list[i] = tempNum;
		}
			
		return list;
	}
	
	/** Removes all values from the Config object */
	public function emptyConfigMap():Void
	{
		delete mConfigMapping;
		mConfigMapping = new Object();
	}
	
	/** 
	 * Removes a specified property from the config mapping
	 * @param pName The id of the property to be removed from the map
	 */
	public function remove(pName:String):Void
	{
		delete mConfigMapping[pName];
	}
}
