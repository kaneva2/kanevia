import kaneva.events.Event;

/**
 * Interface that forces a class that implements this interface to have the specified methods.
 * This interface makes sure that all the methods necessary for dipatching events are being 
 * implemented. 
 * 
 * Note that the class that implements this interface must call the initialize
 * method of the EventDispatcher class in order for the class to be able to use these methods.
 * 
 * @author Scott Enders
 * @see http://livedocs.macromedia.com/flash/8/main/00003473.html
 */
interface kaneva.omm.IEventDispatcher 
{
	/**
	 * Registers a listener object with a component instance that is broadcasting an event. 
	 * When the event occurs, the listener object or function is notified. You can call this method 
	 * from any component instance.
	 * @param pEvent A string that is the name of the event.
	 * @param pListener A reference to a listener object or function. 
	 */
	public function addEventListener(pEvent:String, pListener:Function):Void;
	
	/**
	 * Dispatches an event to any listener registered with an instance of the class.
	 * @param pEventObj A reference to an event object. The event object must have a type 
	 * property that is a string indicating the name of the event. Generally, the event object 
	 * also has a target property that is the name of the instance broadcasting the event. 
	 * You can define other properties on the event object that help a user capture 
	 * information about the event when it is dispatched. 
	 */
	public function dispatchEvent(pEventObj:Event):Void;
	
	/**
	 * Unregisters a listener object from a component instance that is broadcasting an event.
	 * @param pEvent A string that is the name of the event.
	 * @param pListener A reference to a listener object or function. 
	 */
	public function removeEventListener(pEvent:String, pListener:Function):Void;
}