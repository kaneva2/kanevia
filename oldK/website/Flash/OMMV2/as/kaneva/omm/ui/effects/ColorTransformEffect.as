import mx.transitions.easing.None;

import flash.geom.ColorTransform;
import flash.geom.Transform;

import kaneva.omm.ui.effects.Effect;
import kaneva.omm.ui.effects.IEffect;

/**
 * Used to easily create and customize color transform effects for buttons.
 * @author scott
 */
class kaneva.omm.ui.effects.ColorTransformEffect extends Effect implements IEffect
{
	/** An object with properties pertaining to a movie clip's color transform */
	private var mTransform:Transform;
	
	/**
	 * The ColorTransform class lets you mathematically adjust all of the color values 
	 * in a movie clip. The color adjustment function or color transformation can be 
	 * applied to all four channels: red, green, blue, and alpha transparency
	 */
	private var mColorTransform:ColorTransform;
	
	/** Constructor function */
	public function ColorTransformEffect()
	{
		super();
		
		// Create the color transform
		mColorTransform = new ColorTransform();
		
		// Set up the tween stuff
		mObject = mColorTransform;
		mProperty = "redOffset";
		mEasingFunction = None.easeOut;
		mBeginValue = 0;
		mEndValue = 35;
		mAnimationDuration = 0.25;
	}
	
	/**
	 * Set which movieclip will be the target for the color transform rollover effect
	 * @param pTarget The clip to apply the transformation to
	 */
	public function setTarget(pTarget:MovieClip):Void
	{
		super.setTarget(pTarget);
		
		// Create the transform and set the movieclip to which all transformations will be applied
		mTransform = new Transform(mEffectsTarget);
	}
	
	/**
	 * Sets the offset value for the color transform offset
	 * @param pOffset The number to set the red, green, and blue offset values to (between -255 and 255)
	 */
	public function setEffect(pOffset:Number):Void
	{
		super.setEffect(pOffset);
		
		// Want to have a bit of a colored tint when rolling over.  But don't want this on the 
		// press of the button.  So we only use multiplier when value is positive
		var multiplier:Number = pOffset > 0 ? 3 : 1;
		
		// Set the offset value to each component color offset property of the color transform object
		mColorTransform.redOffset = pOffset;
		mColorTransform.greenOffset = pOffset * multiplier; // Gives green tint
		mColorTransform.blueOffset = pOffset;
		
		// Apply the color transform
		mTransform.colorTransform = mColorTransform;
		mEffectsTarget.transform = mTransform;
	}
}