import kaneva.omm.ui.buttons.AbstractButton;
import kaneva.omm.ui.effects.GlowEffect;
import kaneva.omm.ui.shapes.Rect;

/**
 * This is the base class for the buttons that are in the video base.  This has all the 
 * methods and properties that are common to both Service and Feedback buttons.
 * @author scott
 */
class kaneva.omm.ui.buttons.AbstractDetailsButton extends AbstractButton 
{
	/** The text field or label for the button */
	private var mLabelTextField:TextField;
	
	/** The css for the label text*/
	private var mCSS:TextField.StyleSheet;
	
	/** Constructor.....abstract class should not be instantiated, only extended */
	private function AbstractDetailsButton() 
	{
		super();
	}

	/** Initialize stuff */
	private function init():Void
	{
		super.init();
		
		// Create an instance of the glow effect object
		createEffect(GlowEffect);
		
		// Override the effect target
		mEffect.setTarget(this);
	}
	
	/** Create assets stuff */
	private function createChildren():Void
	{
		super.createChildren();
		
		// Create the stylesheet
		mCSS = new TextField.StyleSheet();
		
		// Create the label text field
		mLabelTextField = this.createTextField("mLabelTextField", this.getNextHighestDepth(), 0, 0, 0, 0);
		mLabelTextField.autoSize = true;
		mLabelTextField.selectable = false;
		mLabelTextField.html = true;
		mLabelTextField.styleSheet = mCSS;
	}
	
	/** Draw pieces and stuff */
	private function draw():Void
	{
		super.draw ();
		
		// Draw a hit area
		this.clear();
		this.beginFill (0, 0);
//		this.lineStyle(0, 0xffffff, 100);
		Rect.draw (this, mWidth, mHeight);
		this.endFill ();
	}
	
	/**
	 * Set the label
	 * @param pLabel The label to give this button
	 */
	public function setLabel(pLabel:String):Void
	{
		mLabelTextField.htmlText = "<span class='label'>" + pLabel + "</span>";
		
		// Resize the button so it fits to the label text width
		setSize(mLabelTextField._width, null);
	}
}