import kaneva.omm.ui.buttons.AbstractButton;

/**
 * Represents the play button that appears over top the media in the media video base. This
 * is the pill shaped gray button that plays the media if it initially loads up but does
 * not play.
 * @author scott
 */
class kaneva.omm.ui.buttons.PlayButton extends AbstractButton 
{
	/** The linkage id for the graphic of this button */
	private static var BUTTON_GRAPHIC_LINKAGE_ID:String = "PlayButtonGraphic";
		
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class*/	
	private function PlayButton() 
	{
		super();
	}
	
	/** Create assets stuff */
	private function createChildren():Void
	{
		super.createChildren();
		
		// Create the graphic
		mGraphicClip = this.attachMovie(BUTTON_GRAPHIC_LINKAGE_ID, "mGraphicClip", this.getNextHighestDepth());
	}
}