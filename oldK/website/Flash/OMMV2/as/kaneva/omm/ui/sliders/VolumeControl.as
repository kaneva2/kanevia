﻿import mx.utils.Delegate;

import flash.geom.Matrix;

import kaneva.events.SliderEvent;
import kaneva.omm.OMM;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.shapes.Rect;
import kaneva.omm.ui.sliders.SliderThumb;

/**
 * Component that is used for controlling the volume
 * @author Scott
 */
class kaneva.omm.ui.sliders.VolumeControl extends ComponentDisplay
{
	/** Used for the name parameter of the shared object that stores the volume */
	private static var SO_VOLUME:String = "volume";
	
	/** The position the volume is currently at */
	private var mPosition:Number;
	
	/** The slider thumb clip that the user drags to control the volume */
	private var mThumbClip:SliderThumb;
	
	/** The track that the slider is bound to when being dragged */
	private var mTrackClip:MovieClip;

	/** The color that shows up on the left side of the thumb clip */
	private var mLevelBarClip:MovieClip;
	
	/** The mask clip for the level bar */
	private var mLevelBarMask:MovieClip;
	
	/** The shadow clip of this component...this is just a gradient rectangle over top of track */
	private var mShadowClip:MovieClip;
	
	/** The amount of space between the base and the shadow clip */
	private var mShadowGap:Number = 5;
	
	/** The outline clip of this component */
	private var mOutlineClip:MovieClip;
	
	/**
	 * Used to store the users last volume level.  This is a quick fix, this only works
	 * on the same computer the user last set volume on. If they go to another computer, this
	 * won't get anything from a users login settings for example.
	 */
	private var mVolumeSO:SharedObject;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class  */
	private function VolumeControl ()
	{
		super ();
	}
	
	/** Create movie clips and other things */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create the track
		mTrackClip = this.createEmptyMovieClip ("mTrackClip", this.getNextHighestDepth ());
		mTrackClip.onPress = Delegate.create (this, onTrackPress);
		mTrackClip.onRelease = mTrackClip.onReleaseOutside = Delegate.create (this, onTrackRelease);
			
		// Create the level bar
		mLevelBarClip = this.createEmptyMovieClip ("mLevelBarClip", this.getNextHighestDepth ());
		
		// Create the level bar mask
		mLevelBarMask = this.createEmptyMovieClip("mLevelBarMask", this.getNextHighestDepth());
		mLevelBarClip.setMask(mLevelBarMask);
		
		// Draw the level bar                           
		mLevelBarClip.beginFill (0xCCFF32, 100);
		Rect.draw (mLevelBarClip, 1, mHeight);
		mLevelBarClip.endFill ();

		// Create the shadow clip
		mShadowClip = this.createEmptyMovieClip ("mShadowClip", this.getNextHighestDepth ());
		
		// Create the outline
		mOutlineClip = this.createEmptyMovieClip ("mOutlineClip", this.getNextHighestDepth ());
		
		// Create the slider thumb 
		mThumbClip = SliderThumb(this.attachMovie("SliderThumb", "mThumbClip", this.getNextHighestDepth()));
		mThumbClip.setSize(7, 17);
		mThumbClip._y = (this.height - mThumbClip._height) / 2;
		
		// Set the button events for the thumb clip
		mThumbClip.onPress = Delegate.create(this, onThumbPress);
		mThumbClip.onRelease = mThumbClip.onReleaseOutside = Delegate.create(this, onThumbRelease);
	}
	
	/** Draws pieces and stuff */
	private function draw ():Void
	{
		super.draw ();
		
		// Draw the track
		mTrackClip.clear ();
		mTrackClip.beginFill (OMM.getConfig().getAsNumber("slider.control.track.fill.color"), 100);
		drawTriangle(mTrackClip, mWidth, mHeight);
		mTrackClip.endFill ();
		
		// Draw the level bar mask
		mLevelBarMask.clear();
		mLevelBarMask.beginFill(0x000000, 100);
		drawTriangle(mLevelBarMask, mWidth, mHeight);
		mLevelBarMask.endFill();
		
		// Draw shadow clip
		var w:Number = mWidth - mShadowGap*2;
		var h:Number = mHeight - mShadowGap;
		var colors:Array = [0x000000, 0x000000];
		var alphas:Array = [10, 20];
		var ratios:Array = [0, 255];
		var matrix:Matrix = new Matrix();
		matrix.createGradientBox(w, h, Math.PI/2, 0, mShadowGap/2);
		mShadowClip.clear();
		mShadowClip.beginGradientFill("linear", colors, alphas, ratios, matrix);
		drawTriangle(mShadowClip, w, h);
		mShadowClip.endFill ();
	}
	
	/** Sizes pieces and stuff */
	private function size ():Void
	{
		super.size ();
		
		// Position the thumb
		setThumbPosition ();
		
		// Set the size of the level bar clip
		setLevelBarWidth();
		
		// Position the shadow clip
		mShadowClip._x = mWidth - mShadowClip._width - mShadowGap/2 + 1;
		mShadowClip._y = mHeight - mShadowClip._height - mShadowGap/2 + 1;
	}
	
	/**
	 * Draws the triangles for the base and shadow
	 * @param pC The clip to draw in
	 * @param pW The width of the triangle
	 * @param pH The height of the triangle
	 */
	private function drawTriangle(pC:MovieClip, pW:Number, pH:Number):Void
	{
		pC.moveTo(0, pH);
		pC.lineTo(pW, pH);
		pC.lineTo(pW, 0);
		pC.lineTo(0, pH);
	}
	 
	/** Sets the position of the thumb */
	private function setThumbPosition ():Void
	{
		mThumbClip._x = mPosition * mWidth;
		setLevelBarWidth();
	}
	
	/** Method called when the thumb is pressed */
	private function onThumbPress ():Void
	{
		mThumbClip.startDrag (false, 0, mThumbClip._y, mTrackClip._width, mThumbClip._y);
		mThumbClip.onMouseMove = Delegate.create (this, onThumbDrag);
		
		// Dispatch a press event
		var event:SliderEvent = new SliderEvent();
		event.type = SliderEvent.THUMB_PRESS;
		dispatchEvent(event);
	}
	
	/** Method called by the onMouseMove when the thumb is being dragged */
	private function onThumbDrag ():Void
	{
		setLevelBarWidth();
		
		var percent:Number = mThumbClip._x / mWidth;
//		setPosition (percent);
		mPosition = percent;
		
		// Dispatch a drag event
		var event:SliderEvent = new SliderEvent();
		event.type = SliderEvent.THUMB_DRAG;
		event.value = percent;
		dispatchEvent (event);
		
		// Set the volume to the shared object
		mVolumeSO.data[SO_VOLUME] = getPosition();
		
		// Make the motion smooth
		updateAfterEvent ();
	}
	
	/** Method called when the slider thumb is released */
	private function onThumbRelease ():Void
	{
		mThumbClip.stopDrag ();
		delete mThumbClip.onMouseMove;
		
		// Dispatch a release event
		var event:SliderEvent = new SliderEvent();
		event.type = SliderEvent.THUMB_RELEASE;
		dispatchEvent(event);
	}
	
	/** Method called when the track is pressed */
	private function onTrackPress ():Void
	{
		// We want to send press event so that the media player pauses media
		var event:SliderEvent = new SliderEvent();
		event.type = SliderEvent.THUMB_PRESS;
		event.clickTarget = SliderEvent.TRACK;
		dispatchEvent(event);
		
		// Set the position of the slider
		mThumbClip._x = this._xmouse;
		
		// Call drag method so media is updated
		onThumbDrag ();
	}
	
	/** Method called when the track is released */
	private function onTrackRelease ():Void
	{
		// Send release event so that media player knows how to deal with media
		var event:SliderEvent = new SliderEvent();
		event.type = SliderEvent.THUMB_RELEASE;
		event.clickTarget = SliderEvent.TRACK;
		dispatchEvent(event);
	}
	
	/** 
	 * Get the percentage value of the position of the thumb
	 * @return The value (0 - 1) of the position of the thumb
	 */
	public function getPosition ():Number
	{
		return mPosition;
	}
	
	/** 
	 * Set the percentage value of the position of the thumb
	 * @param pValue The value (0 - 1) to set thumb position to
	 */
	public function setPosition (pValue:Number):Void
	{
		mPosition = pValue;
		setThumbPosition ();
	}
	
	/**
	 * Set the size of the level bar clip to the x position of the thumb clip
	 */
	private function setLevelBarWidth():Void
	{
		mLevelBarClip._width = mThumbClip._x;
	}
	
	/** Invoked when the movie clip is instantiated and appears in the timeline */
	private function onLoad():Void
	{
		super.onLoad();
		
		// Get a reference to the volume shared object if it exists.  If it does not, the create
		// the shared object
		mVolumeSO = SharedObject.getLocal(SO_VOLUME);
		
		// Try and get the volume value from the data object on the shared object
		var volume:Number = mVolumeSO.data[SO_VOLUME];
		
		// If there is a volume, then set the volume of this component to that value....otherwise
		// set it to a default value
		if(volume)
			setPosition(volume);
		else
			setPosition(0.75);
	}
}