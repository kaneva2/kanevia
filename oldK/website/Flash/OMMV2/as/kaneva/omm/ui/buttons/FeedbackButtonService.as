import mx.utils.Delegate;

import kaneva.events.Event;
import kaneva.media.MediaAsset;
import kaneva.omm.IEventDispatcher;
import kaneva.omm.io.XmlParser;
import kaneva.omm.OMM;
import kaneva.omm.ui.buttons.AbstractDetailsButton;
import kaneva.omm.ui.buttons.FeedbackButton;
import kaneva.omm.widgets.UrlRedirectWidget;
import kaneva.omm.widgets.WidgetData;
import kaneva.util.JavaScript;
import kaneva.util.Strings;
import kaneva.omm.ui.buttons.ServiceButton;

/**
 * Used to send and receive info from the server whenever a feedback button is clicked
 * @author scott
 */
class kaneva.omm.ui.buttons.FeedbackButtonService implements IEventDispatcher
{
	
	/** Receives the response from the server when this button is clicked */
	private var mReceivingXML:XML;
	
	/** 
	 * Used to execute a sendAndLoad method.  Properties are appended to this object and sent to the server.
	 * The response from the server are sent to the xml object specified in the second parameter of the sendAndLoad method
	 */
	private var mSendingLoadVars:LoadVars;
	
	/** The uri to send variables to via LoadVars */
	private var mUri:String;
	
	/** The button that was pressed */
	private var mButton:AbstractDetailsButton;
	
	/**
	 * Constructor function
	 */
	public function FeedbackButtonService() 
	{
		mReceivingXML = new XML();  
		mSendingLoadVars = new LoadVars();  
  		 
		// Set xml stuff up  
		mReceivingXML.ignoreWhite = true;  
		mReceivingXML.onLoad = Delegate.create(this, onReceivingXMLLoad); 
	}
	
	/** 
	 * Executed on click of this button.  This sends off variables to the server and specifies
	 * an xml object to receive the response.
	 * @param pConfigUri The config string that specifies the uri for the button that was clicked 
	 */
	public function clicked(pConfigUri:String):Void
	{
		var media:MediaAsset = OMM.getMedia();
		var uri:String = OMM.getConfig().getAsString(pConfigUri);
		var replaceUri:String;
		
		if (uri)
		{
			// add the id here
			replaceUri = Strings.replace(uri,"{0}", String(media.id));
			
			if (pConfigUri == "uri.button.comment")
				JavaScript.openWindow(replaceUri, "_blank");
			else
				mSendingLoadVars.sendAndLoad(replaceUri, mReceivingXML, "POST");  
		}
	}
	
	/**
	 * Set the button that was clicked
	 * @param pButton The button that was pressed
	 */
	public function setButton(pButton:AbstractDetailsButton):Void
	{
		mButton = pButton;
	}
	
	/** 
	 * The method executed when xml is received from the server
	 * @param pSuccess A Boolean value that evaluates to true if the XML object is successfully loaded; otherwise, it is false
	 */
	private function onReceivingXMLLoad(pSuccess:Boolean):Void  
	{ 
		if (pSuccess)  
		{
			// Get reference to the rave button and rave it button
			var rave:FeedbackButton = OMM.getInstance().getMediaPlayer().getMediaVideoBase().getRaveButton();
			var raveIt:ServiceButton = OMM.getInstance().getMediaPlayer().getMediaVideoBase().getRaveItButton();
							
			var rootNode:XMLNode = mReceivingXML.firstChild;
			
			if (rootNode.hasChildNodes())
			{
				var rootChildren:Array = rootNode.childNodes;
				var numberOfRootChildren:Number = rootChildren.length;
				
				for (var a:Number = 0; a < numberOfRootChildren; a++)
				{
					var childNode:XMLNode = rootChildren[a];
					
					switch(childNode.nodeName)
					{
						case "confirm":
							
							// If it's a raveit button...
							if(mButton == raveIt)
							{
								rave.setCount(rave.getCount() + 1);
							}
												
							//if widget is sent back - registerWidget should be called
							if(childNode.hasChildNodes())
							{
								var parser:XmlParser = new XmlParser();
								var widget:WidgetData = parser.readWidgetXML(childNode.childNodes[0]);
								var omm:OMM = OMM.getInstance();
								omm.registerWidget(widget);
							}
							
							// Update the button that was pressed
							OMM.getInstance().updateFeedbackButtons(mButton);
//							
							break;

						case "error":
							
							// If there is an error and if it's a raveit button...
							if(mButton == raveIt)
							{
								// Re-enabled the raveit button
								raveIt.setIcon("RaveIt");
								raveIt.setEnabled(true);
							}
							
							break;

						case "widgets":
							
							//if widget is sent back - registerWidget should be called
							if(childNode.hasChildNodes())
							{
								var parser:XmlParser = new XmlParser();
								var widget:WidgetData = parser.readWidgetXML(childNode.childNodes[0]);
								var omm:OMM = OMM.getInstance();
								omm.registerWidget(widget);
							}
							break;
							
						default:
							
							var redirect:UrlRedirectWidget = new UrlRedirectWidget();
							redirect.url = childNode.attributes.uri;
							redirect.run();
					}
				}
			}
		}
		else
		{
			trace("Error loading xml");
		}
	}
	
	/** 
	 * Implement the EventDispatcher methods
	 * @see mx.events.EventDispatcher
	 */
	public function addEventListener(pEvent:String, pListener:Function):Void {}
	public function dispatchEvent(pEventObj:Event):Void {}
	public function removeEventListener(pEvent:String, pListener:Function):Void {}
}