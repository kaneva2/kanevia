﻿import mx.utils.Delegate;

import kaneva.omm.Constants;
import kaneva.omm.CSS;
import kaneva.omm.OMM;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.shapes.RectRounded;
import kaneva.util.Strings;
import kaneva.util.JavaScript;

/**
 * Creates the base clip for the media player.  This deals with setting and adjusting 
 * the title for a piece of media.  It also positions the logo for kaneva
 * @author Scott
 */
class kaneva.omm.ui.MediaBase extends ComponentDisplay
{
	/** Contains the base graphic*/
	private var mBaseClip:MovieClip;
	
	/** The text field which displays the title of the media */
	private var mTitleTextField:TextField;
	
	/** stylesheet for the TextFields */
	private var mCSS:TextField.StyleSheet;
	
	/** The kaneva logo */
	private var mKanevaLogo:MovieClip;
	
	/** Whether or not to show the logo.  This is done so that this component can be reused */
	private var mShowLogo:Boolean = false;
	
	/** The title of the media which is shown in the title text field */
	private var mTitle:String = "";
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function MediaBase ()
	{
		super ();
	}
	
	/** Create movie clips and other things */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create the text format object
		mCSS = CSS.getMediaBaseStyle();
		
		// Create the base graphic
		mBaseClip = this.createEmptyMovieClip("mBaseClip", this.getNextHighestDepth());
		
		// Create the text field that displays the title
		mTitleTextField = this.createTextField ("mTitleTextField", this.getNextHighestDepth (), 0, 0, 0, 0);
		mTitleTextField.selectable = false;
		mTitleTextField.html = true;
		mTitleTextField.styleSheet = mCSS;
		mTitleTextField.htmlText = "<p class='label'>" + mTitle + "</p>";
		mTitleTextField._height = mTitleTextField.getTextFormat().getTextExtent(" ").textFieldHeight;
		
		// Create the logo
		mKanevaLogo = this.attachMovie ("KanevaLogo", "mKanevaLogo", this.getNextHighestDepth ());
		mKanevaLogo.onPress = Delegate.create (this, onClick);
		mKanevaLogo.useHandCursor = true;
	}
	
	
	/** Draws pieces and stuff */
	private function draw ():Void
	{
		super.draw ();
		
		// Draw the top base for the media player
		mBaseClip.clear ();
		//mBaseClip.lineStyle (1, 0xCBCBCB, 100, true);
		mBaseClip.beginFill (OMM.getConfig().getAsNumber ("media.base.background.color"), 100);
		RectRounded.draw (mBaseClip, mWidth, mHeight, Constants.RADIUS);
		mBaseClip.endFill ();
/*		
		// Draw separator line at the bottom of this component
		mBaseClip.lineStyle (1, 0xCBCBCB, 100, true);
		mBaseClip.moveTo (Constants.RADIUS, mHeight - 2);
		mBaseClip.lineTo (mWidth - Constants.RADIUS, mHeight - 2);
		
		mBaseClip.lineStyle (1, 0xF2EDEB, 100);
		mBaseClip.moveTo (Constants.RADIUS, mHeight - 1);
		mBaseClip.lineTo (mWidth - Constants.RADIUS, mHeight - 1);
*/
	}
	
	/** Sizes pieces and stuff */
	private function size ():Void
	{
		super.size ();
		
		// Size the text field
		mTitleTextField._width = (mShowLogo ? mKanevaLogo._x : mWidth) - mTitleTextField._x - 5;
		
		// Set the title
		mTitleTextField.htmlText = "<p class='label'>" + Strings.truncate(mTitle, mTitleTextField) + "</p>";
		
		// Position title text field
		mTitleTextField._x = 5;
		mTitleTextField._y = (mHeight - mTitleTextField._height) / 2;
		
		// Position the logo
		mKanevaLogo._x = mWidth - mKanevaLogo._width - 10;
		mKanevaLogo._y = (mHeight - mKanevaLogo._height) / 2;
		
		// Set the logo visibility
		mKanevaLogo._visible = mShowLogo;
	}
	
	/** 
	 * Set the title of the media
	 * @param pValue The title
	 */
	public function setTitle (pValue:String):Void
	{
		mTitle = pValue;
		size ();
	}
	
	/** 
	 * Get the title of the media
	 * @return The title
	 */
	public function getTitle ():String
	{
		return mTitle;
	}
	
	/** 
	 * Set the visibility of the logo
	 * @param pValue The visibility value
	 */
	public function showLogo (pValue:Boolean):Void
	{
		mShowLogo = pValue;
		size ();
	}
	
	/** open a link to kaneva.com */
	private function onClick():Void
	{
		JavaScript.openWindow("http://www.kaneva.com", "_blank");
	}
}