﻿import kaneva.omm.Constants;
import kaneva.omm.OMM;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.sliders.SliderControl;
import kaneva.omm.ui.sliders.VolumeControl;

/**
 * The base of the media play which houses the buttons.  This positions the buttons depending
 * on the size of this component.  This also sets up interactivity for the buttons.
 * @author Scott
 */
class kaneva.omm.ui.MediaButtonBase extends ComponentDisplay
{
//	/** The additional buttons */
//	public var raveButton:AdditionalButton;
//	public var addButton:AdditionalButton;
//	public var shareButton:AdditionalButton;
//	public var commentsButton:AdditionalButton;
//	
	/** Contains the base graphic*/
	private var mBaseClip:MovieClip;
	
	/** The interface buttons for the player */
//	public var mPlayButton:InterfaceButton;
//	public var mStopButton:InterfaceButton;
//	public var mSoundButton:InterfaceButton;
	
	/** The volume control clip */
	public var mVolumeControl:VolumeControl;
	
	/** The depth of the volume control */
	private var mVolumeDepth:Number = null;
	
	/** The component used to give users ability to drag a slider to control the position of a piece of media */
	public var mSliderControl:SliderControl;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function MediaButtonBase ()
	{
		super ();
	}
	
	/** Create movie clips and other things */
	private function createChildren ():Void
	{
		super.createChildren ();
	
		// Create the base graphic
		mBaseClip = this.createEmptyMovieClip("mBaseClip", this.getNextHighestDepth());
		
		// Create interface buttons
//		mStopButton = InterfaceButton (this.attachMovie ("InterfaceButton", "mStopButton", this.getNextHighestDepth ()));
//		mStopButton.setIcon ("Stop");
//		mPlayButton = InterfaceButton (this.attachMovie ("InterfaceButton", "mPlayButton", this.getNextHighestDepth ()));
//		mPlayButton.setIcon ("Play");
		
//		mSoundButton = InterfaceButton (this.attachMovie ("InterfaceButton", "mSoundButton", this.getNextHighestDepth ()));
//		mSoundButton.setIcon ("Sound");
//		
//		// Create additional buttons
//		raveButton = AdditionalButton (this.attachMovie ("AdditionalButton", "mRaveButton", this.getNextHighestDepth ()));
//		raveButton.setLabel ("RAVE IT");
//		raveButton.setKey("raveIt");
//		raveButton.setIcon ("Rave");
//		addButton = AdditionalButton (this.attachMovie ("AdditionalButton", "mAddButton", this.getNextHighestDepth ()));
//		addButton.setLabel ("ADD");
//		addButton.setKey("add");
//		addButton.setIcon ("Add");
//		shareButton = AdditionalButton (this.attachMovie ("AdditionalButton", "mShareButton", this.getNextHighestDepth ()));
//		shareButton.setLabel ("SHARE");
//		shareButton.setKey("share");
//		shareButton.setIcon ("Share");
//		commentsButton = AdditionalButton (this.attachMovie ("AdditionalButton", "mCommentsButton", this.getNextHighestDepth ()));
//		commentsButton.setLabel ("COMMENTS");
//		commentsButton.setKey("comment");
//		commentsButton.setIcon ("Comments");
//		
		// Set button events
//		mPlayButton.addEventListener ("click", Delegate.create (this, onPlayRelease));
//		mStopButton.addEventListener ("click", Delegate.create (this, onStopRelease));
//		mSoundButton.addEventListener ("click", Delegate.create (this, onSoundRelease));
	}
	
	/** Draws pieces and stuff */
	private function draw ():Void
	{
		super.draw ();
		
		var radius:Number = Constants.RADIUS;
		
		// Draw the button base for the media player
		mBaseClip.clear ();
		//mBaseClip.lineStyle (0, 0xCBCBCB, 100, true);
		mBaseClip.beginFill (OMM.getConfig().getAsNumber("media.button.base.background.color"), 100);
		mBaseClip.lineTo (mWidth, 0);
		mBaseClip.lineTo (mWidth, mHeight - radius);
		mBaseClip.curveTo (mWidth, mHeight, mWidth - radius, mHeight);
		mBaseClip.lineTo (radius, mHeight);
		mBaseClip.curveTo (0, mHeight, 0, mHeight - radius);
		mBaseClip.lineTo (0, 0);
		mBaseClip.endFill ();
	}
	
	/** Sizes pieces and stuff */
	private function size ():Void
	{
		super.size ();
		
		// Position buttons
//		mPlayButton._x = 10;
//		mPlayButton._y = (mHeight - mPlayButton._height)/2;
//		mSoundButton._x = mWidth - mSoundButton._width - 15;
//		mSoundButton._y = (mHeight - mSoundButton._height) / 2;
//		mPlayButton._x = mSoundButton._x - mPlayButton._width - 5;
//		mPlayButton._y = mSoundButton._y;
//		mStopButton._x = mPlayButton._x - mStopButton._width - 5;
//		mStopButton._y = mSoundButton._y;
//		
//		raveButton._x = 5;
//		addButton._x = raveButton._x + raveButton._width;
//		shareButton._x = addButton._x + addButton._width;
//		commentsButton._x = shareButton._x + shareButton._width + 10;
//		raveButton._y = addButton._y = shareButton._y = commentsButton._y = (mHeight - raveButton.height) / 2;
//		
//		// Position volume control
//		mVolumeControl._x = mSoundButton._x;
//		mVolumeControl._y = mSoundButton._y - mVolumeControl.height;
		
		// Check to see if the interface buttons and the additional buttons are going to overlap
//		hideAdditionalButtons ();
	}
	
//	/** Creates the volume control clip */
//	private function createVolumeControl ():Void
//	{
//		// Save the depth of the volume control so we can remove it and recreate it at the same depth
//		mVolumeDepth = mVolumeDepth == null ? this.getNextHighestDepth () : mVolumeDepth;
//		
//		// Create the clip
//		mVolumeControl = VolumeControl (this.attachMovie ("VolumeControl", "mVolumeControl", mVolumeDepth));
//	}
//	
//	/** If the interface buttons and the additional buttons are going to overlap, then hide the additional buttons */
//	private function hideAdditionalButtons ():Void
//	{
//		raveButton._visible = mStopButton._x > raveButton._x + raveButton.width;
//		addButton._visible = mStopButton._x > addButton._x + addButton.width;
//		shareButton._visible = mStopButton._x > shareButton._x + shareButton.width;
//		commentsButton._visible = mStopButton._x > commentsButton._x + commentsButton.width;
//	}
//	
//	/** Method called when the play button is clicked */
//	private function onPlayRelease ():Void
//	{
//		dispatchEvent ({type:"play", target:this});
//	}
//	
//	/** Method called when the stop button is clicked */
//	private function onStopRelease ():Void
//	{
//		dispatchEvent ({type:"stop", target:this});
//	}
//	
//	/** Method called when the volume/sound button is clicked */
//	private function onSoundRelease ():Void
//	{
//		if (mVolumeControl)
//			onVolumeRelease ();
//		else
//			createVolumeControl ();
//		
//		// Set volume control events
//		mVolumeControl.addEventListener ("drag", Delegate.create (this, onVolumeDrag));
//		mVolumeControl.addEventListener ("release", Delegate.create (this, onVolumeRelease));
//		
//		dispatchEvent ({type:"press", target:mVolumeControl});
//		
//		size ();
//	}
//	
//	/** Method called when the volume control is dragged */
//	private function onVolumeDrag (pEvent:Object):Void
//	{
//		//trace (pEvent.percent);
//		dispatchEvent ({type:"volume", target:this, percent:pEvent.percent});
//	}
//	
//	/** Method called when the volume control is release */
//	private function onVolumeRelease ():Void
//	{
//		mVolumeControl.removeMovieClip();
//		delete mVolumeControl;
//	}
//	
//	/** 
//	 * Sets the play button to show the appropriate icon (play or pause)
//	 * @param pIsPlaying Boolean that sets which icon to show
//	 */
//	public function isPlaying (pIsPlaying:Boolean):Void
//	{
//		mPlayButton.setIcon (pIsPlaying ? "Pause" : "Play");
//	}
//	
//	/** set the rave button to use the 'raved' icon if the user has already raved this media*/
//	public function setRavedIcon(pValue:Boolean):Void
//	{
//		if(pValue)
//			raveButton.setIcon ("Raved");
//		else
//			raveButton.setIcon ("Rave");
//	}
//	
//	/** set the add button to display the 'added' icon if the movie already exists within the user's library*/
//	public function setAddedIcon(pValue:Boolean):Void
//	{
//		if(pValue)
//			// currently, Views is the name of the 'added' icon within the Mediaplayer.fla
//			addButton.setIcon ("Added");
//		else
//			addButton.setIcon ("Add");
//	}
}