import kaneva.omm.ui.buttons.AbstractButton;

/**
 * Represents the share button in the media player interface
 * @author scott
 */
class kaneva.omm.ui.buttons.ShareButton extends AbstractButton 
{
	/** The linkage id for the graphic of this button */
	private static var BUTTON_GRAPHIC_LINKAGE_ID:String = "ShareButtonGraphic";
		
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class*/	
	private function ShareButton() 
	{
		super();
	}
	
	/** Create assets stuff */
	private function createChildren():Void
	{
		super.createChildren();
		
		// Create the graphic
		mGraphicClip = this.attachMovie(BUTTON_GRAPHIC_LINKAGE_ID, "mGraphicClip", this.getNextHighestDepth());
	}
}