import flash.geom.Matrix;

import kaneva.omm.OMM;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.shapes.RectRounded;

/**
 * Class that represents the thumb control for a slider component.
 * 
 * @author scott
 * @see kaneva.omm.ui.slider.SliderControl
 * @see kaneva.omm.ui.slider.VolumeControl
 */
class kaneva.omm.ui.sliders.SliderThumb extends ComponentDisplay 
{
	/** The corner radius for the base */
	private var mCornerRadius:Number = 3;
	
	/** The draggable doo dad that allows user to move the movie to a time in the media */
	public var mThumbClip:MovieClip;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class  */
	private function SliderThumb() 
	{
		super();
	}
	
	/** Create stuff */
	private function createChildren():Void
	{
		// Create the clip to draw in
		mThumbClip = this.createEmptyMovieClip("mThumbClip", this.getNextHighestDepth());
	}
	
	/** Draws stuff */
	private function draw():Void
	{
		// Draw the thumb bar
		mThumbClip.clear ();
		mThumbClip.lineStyle (0, OMM.getConfig().getAsNumber("slider.control.scrub.outline.color"), 100, true);
		var colors:Array = OMM.getConfig().getAsArrayOfNumbers("slider.control.scrub.fill.color");
		var alphas:Array = [100, 100];
		var ratios:Array = [0, 255];
		var matrix:Matrix = new Matrix();
		matrix.createGradientBox(mWidth, mHeight, Math.PI/2, 0, 0);
		mThumbClip.beginGradientFill("linear",colors, alphas, ratios, matrix);
		RectRounded.draw(mThumbClip, mWidth, mHeight, mCornerRadius);
		mThumbClip.endFill ();
	}
}