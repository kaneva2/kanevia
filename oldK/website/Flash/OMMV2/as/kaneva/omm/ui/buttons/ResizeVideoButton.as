import kaneva.omm.ui.buttons.AbstractButton;

/**
 * Represents the resize video button in the media player interface
 * @author scott
 */
class kaneva.omm.ui.buttons.ResizeVideoButton extends AbstractButton 
{
	/** The linkage id for the graphic of this button */
	private static var BUTTON_GRAPHIC_LINKAGE_ID:String = "ResizeVideoButtonGraphic";
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class*/	
	private function ResizeVideoButton() 
	{
		super();
	}
	
	/** Create assets stuff */
	private function createChildren():Void
	{
		super.createChildren();
		
		// Create the graphic
		mGraphicClip = this.attachMovie(BUTTON_GRAPHIC_LINKAGE_ID, "mGraphicClip", this.getNextHighestDepth());
	}
	
	/** 
	 * Set the icon to resize the video so it shows the details or just the video
	 * @param pFrame Which icon to show....set it to either "details" or "video" (due to frame labels)
	 */
	public function setIcon(pFrame:String):Void
	{
		mGraphicClip.gotoAndStop(pFrame);
	}
}