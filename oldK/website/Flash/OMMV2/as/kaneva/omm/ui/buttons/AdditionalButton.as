﻿// TODO - This is being done away with.  We no longer have need for this

import mx.transitions.easing.Strong;
import mx.transitions.Tween;
import mx.utils.Delegate;

import kaneva.events.MouseEvent;
import kaneva.media.MediaAsset;
import kaneva.omm.CSS;
import kaneva.omm.io.XmlParser;
import kaneva.omm.Logger;
import kaneva.omm.OMM;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.shapes.Rect;
import kaneva.omm.widgets.UrlRedirectWidget;
import kaneva.omm.widgets.WidgetData;
import kaneva.util.Strings;
import kaneva.util.JavaScript;


/**
 * Creates the additional button used in the media player. These are the rave it, add,
 * share, and comments buttons.
 * @author Scott
 */
class kaneva.omm.ui.buttons.AdditionalButton extends ComponentDisplay
{
	/** The icon clip that is attached to the button */
	private var mIcon:MovieClip;
	
	/** Linkage id of the icon from the library to use*/
	private var mIconLinkage:String = "Rave";
	
	/** The label of the button */
	private var mLabel:String = "";
	
	/** key to which the button will be referenced - should corrospond to how it's it referenced via xml config tags*/
	private var mButtonKey:String = "";
	
	/** The count of the button */
	private var mCount:String = "0";
	
	/** The text field that displays the label of the button */
	private var mLabelTextField:TextField;
	
	/** the css for the label text*/
	private var mCSS:TextField.StyleSheet;
	
	/** The text field that displays the count for the button */
	private var mCountTextField:TextField;
	
	/** Used for roll over effect */
	private var mColor:Color;
	private var mTransform:Object;
	private var mTransformProp:Number = 0;
	private var mTween:Tween;
	
	/** Receives the response from the server when this button is clicked */
	private var mReceivingXML:XML;
	
	/** 
	 * Used to execute a sendAndLoad method.  Properties are appended to this object and sent to the server.
	 * The response from the server are sent to the xml object specified in the second parameter of the sendAndLoad method
	 */
	private var mSendingLoadVars:LoadVars;
	
	/** The uri to send variables to via LoadVars */
	private var mUri:String;
	
	/** 
	 * Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class
	 */
	private function AdditionalButton ()
	{
		super ();
	}
	
	/** Initialize stuff */
	private function init ():Void
	{
		super.init ();
		
		mReceivingXML = new XML ();  
		mSendingLoadVars = new LoadVars ();  
  		 
		// Set xml stuff up  
		mReceivingXML.ignoreWhite = true;  
		mReceivingXML.onLoad = Delegate.create (this, onReceivingXMLLoad); 
		
		// Set your properties to send to the server....curently nothing is really being sent
		mSendingLoadVars["prop1"] = "prop1 value";
		mSendingLoadVars["prop2"] = "prop2 value";
		
		// For testing
		//setUri (mTempUrl);
	}
	
	/** Create movie clips and other things */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// get the css layout for the fonts
		mCSS = CSS.getAdditionalButtonStyle();
		
		// Create icon clip
		mIcon = this.createEmptyMovieClip("mIcon", this.getNextHighestDepth());
		
		// Create the text field that displays the label
		mLabelTextField = this.createTextField ("mLabelTextField", this.getNextHighestDepth(), 0, 0, 0, 0);
		mLabelTextField.autoSize = true;
		mLabelTextField.selectable = false;
		mLabelTextField.html = true;
		mLabelTextField.styleSheet = mCSS;
		
		// Create the text field that displays the count
		mCountTextField = this.createTextField ("mCountTextField", this.getNextHighestDepth (), 0, 0, 0, 0);
		mCountTextField.autoSize = true;
		mCountTextField.selectable = false;
		mCountTextField.html = true;
		mCountTextField.styleSheet = mCSS;
		
		// Set the button events for this button
		this.onRelease = Delegate.create (this, onClick);
		this.onRollOver = this.onDragOver = Delegate.create (this, onOverEvent);
		this.onRollOut = this.onDragOut = onReleaseOutside = Delegate.create (this, onOutEvent);
		
		// Set up the color object which is used for the rollover effect
		mColor = new Color (this);
		mTransform = {rb:mTransformProp, gb:mTransformProp, bb:mTransformProp};
	}
	
	/** Sizes pieces and stuff */
	private function size ():Void
	{
		super.size ();
		
		// Create a hit area
		this.clear ();
		this.beginFill (0, 0);
		Rect.draw (this, mWidth, mHeight);
		this.endFill ();
		
		// Set stuff
		var iconArea:Number = 20;
		mLabelTextField.htmlText = "<p class='label'>" + mLabel + "</p>";
		mCountTextField.htmlText = "<p class='count'>" + mCount + "</p>";
		
		// Position the elements
		mIcon._x = (mWidth - mIcon._width) / 2;
		mIcon._y = (iconArea - mIcon._height) / 2;
		mLabelTextField._x = (mWidth - mLabelTextField._width) / 2;
		mLabelTextField._y = iconArea;
		mCountTextField._x = (mWidth - mCountTextField._width) / 2;
		mCountTextField._y = mLabelTextField._y + mLabelTextField._height - 4;
	}
	
	/** 
	 * Executed on click of this button.  This sends off variables to the server and specifies
	 * an xml object to receive the response.
	 */
	private function onClick ():Void
	{
		var media:MediaAsset = OMM.getMedia();
		var uri:String = OMM.getConfig().getAsString("uri.button." + mButtonKey);
		Logger.logMsg( "AdditionalButton.onClick: " + String(mButtonKey) + " uri: " + uri );
		var replaceUri:String;
		if (uri)
		{
			// add the id here
			replaceUri = Strings.replace(uri,"{0}", String(media.id));
			Logger.logMsg("AdditionalButtons.onClick replaceURI is: " + replaceUri);
			if ( mButtonKey == "comment" )
				JavaScript.openWindow( replaceUri, "_blank" );
			else
				mSendingLoadVars.sendAndLoad (replaceUri, mReceivingXML, "POST");  
		}
		
		var mouseEvent:MouseEvent = new MouseEvent();
		mouseEvent.type = MouseEvent.CLICK;
		dispatchEvent(mouseEvent);
		
//		dispatchEvent ({type:"click", target:this});
	}
	
	/** 
	 * The method executed when xml is received from the server
	 * @param pSuccess A Boolean value that evaluates to true if the XML object is successfully loaded; otherwise, it is false
	 */
	private function onReceivingXMLLoad (pSuccess:Boolean):Void  
	{  
		if (pSuccess)  
		{
			var rootNode:XMLNode = mReceivingXML.firstChild;
			
			if (rootNode.hasChildNodes())
			{
				var rootChildren:Array = rootNode.childNodes;
				var numberOfRootChildren:Number = rootChildren.length;
				
				for (var a:Number = 0; a < numberOfRootChildren; a++)
				{
					var childNode:XMLNode = rootChildren[a];

					Logger.logMsg( "AdditionalButtons.onReceivingXMLLoad got node: " + childNode.nodeName );
					switch(childNode.nodeName)
					{
						case "confirm":
							// only 'raveIt' or 'add' buttons should increase the count
							if(mButtonKey == "raveIt")
							{
								setCount(String(1 + Number (getCount())));
								setIcon("Raved");
								setLabel("RAVES");
							}
							else if(mButtonKey == "add")
							{
								setCount(String(1 + Number (getCount())));
								setIcon("Added");
								setLabel("ADDED");
							}
							
							//if widget is sent back - registerWidget should be called
							if(childNode.hasChildNodes())
							{
								var parser:XmlParser = new XmlParser();
								var widget:WidgetData = parser.readWidgetXML(childNode.childNodes[0]);
								var omm:OMM = OMM.getInstance();
								omm.registerWidget(widget);
							}
							// tell the omm.as that the button count should be updated.
//							dispatchEvent({type:"update", target:this});
							break;

						case "error":
							break; // nothing in an error message

						case "widgets":
							//if widget is sent back - registerWidget should be called
							if(childNode.hasChildNodes())
							{
								var parser:XmlParser = new XmlParser();
								var widget:WidgetData = parser.readWidgetXML(childNode.childNodes[0]);
								var omm:OMM = OMM.getInstance();
								omm.registerWidget(widget);
							}
							break;
							
						default:
							var redirect:UrlRedirectWidget = new UrlRedirectWidget();
							redirect.url = childNode.attributes.uri;
							redirect.run();
					}
				}
			}
		}
	}
	
	/** Method executed on rollover of this button */
	private function onOverEvent ():Void
	{
		var top:Number = 100;
		mTween = new Tween (this, "mTransformProp", Strong.easeOut, mTransformProp, top, 0.5, true);
		mTween.onMotionChanged = Delegate.create (this, onPropChange);
	}
	
	/** Method executed on rollout of this button */
	private function onOutEvent ():Void
	{
		var bottom:Number = 0;
		mTween = new Tween (this, "mTransformProp", Strong.easeOut, mTransformProp, bottom, 0.5, true);
		mTween.onMotionChanged = Delegate.create (this, onPropChange);
	}
	
	/** Called on the motion changed event of the tween object */
	private function onPropChange ():Void
	{
		mTransform.rb = mTransformProp;
		mTransform.gb = mTransformProp;
		mTransform.bb = mTransformProp;
		mColor.setTransform (mTransform);
	}
	
	/** 
	 * Set the linkage id of the icon for the button
	 * @param pValue The linkage id
	 */
	public function setIcon (pValue:String):Void
	{
		mIconLinkage = pValue;
		
		var loadedIcon:MovieClip = mIcon.attachMovie (mIconLinkage, mIconLinkage, 0);
		loadedIcon.cacheAsBitmap = true;		
		
		size ();
	}
	
	/** 
	 * Set the label of the button
	 * @param pValue The label
	 */
	public function setLabel (pValue:String):Void
	{
		mLabel = pValue;
		size ();
	}
	
	/** 
	 * Set the count of the button
	 * @param pValue The count
	 */
	public function setCount (pValue:String):Void
	{
		mCount = pValue;
		size ();
	}
	
	/**
	 * Get the count of the button
	 * @return mCount The count
	 */
	public function getCount ():String
	{
		return mCount;
	}
	
	/** 
	 * Set the Uri to send the loadvars to
	 * @param pValue The uri
	 */
	public function setUri (pValue:String):Void
	{
		mUri = pValue;
	}
	
	/** set the key */
	public function setKey (pKey:String):Void
	{
		mButtonKey = pKey;
	}
	
	/** get the key*/
	public function getKey ():String
	{
		return mButtonKey;
	}
	
	/** a precautionary function to make sure the icons are loaded*/
	private function onLoad():Void
	{
		setIcon(mIconLinkage);
	}
}
