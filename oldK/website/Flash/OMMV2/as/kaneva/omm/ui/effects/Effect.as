import mx.transitions.Tween;
import mx.utils.Delegate;

import kaneva.omm.ui.effects.IEffect;

/**
 * This will be the base class for all the effects classes.  These effects will be used
 * for effects used for button effects for example.  These give the ability to animate
 * effects such as color transforms and glow effects for buttons.
 * 
 * @author scott
 */
class kaneva.omm.ui.effects.Effect implements IEffect
{
	/** The clip that is effected by the transform object */
	private var mEffectsTarget:MovieClip;
	
	/** Used to animate the rollover and rollout effect for the buttons */
	private var mEffectTween:Tween;
	
	/** The object whose property is to be animated */
	private var mObject:Object;
	
	/** The property that is to be tweened */
	private var mProperty:String;
	
	/** The type of easing to use */
	private var mEasingFunction:Function;
	
	/** The beginning value of the property being tweened */
	private var mBeginValue:Number;
	
	/** The ending value of the property being tweened */
	private var mEndValue:Number;
	
	/** The number of seconds it takes for the effect to take place */
	private var mAnimationDuration:Number;
	
	/** Constructor function */
	public function Effect()
	{
		
	}
	
	/**
	 * Sets the filter or transform to the graphic clip on the tween motion changed event.  This method
	 * will be overriden by the subclasses of this class.
	 * @param pValue The property value to set
	 */
	public function setEffect(pValue:Number):Void
	{
		
	}
	
	/**
	 * Set which movieclip will be the target for the effect. 
	 * @param pTarget The clip to apply the effect to
	 */
	public function setTarget(pTarget:MovieClip):Void
	{
		mEffectsTarget = pTarget;
	}
	
	/** Starts the animation from it's current position */
	public function forward():Void
	{
		startTween(mEndValue);
	}
	
	/** Reverses the animation from it's current position */
	public function backward():Void
	{
		startTween(mBeginValue);
	}
	
	/** Stops the animtion and sets it to the end */
	public function end():Void
	{
		mEffectTween.stop();
		mEffectTween.fforward();
	}
	
	/** Sets the animation to the beginning and stops it */
	public function beginning():Void
	{
		mEffectTween.stop();
		mEffectTween.rewind();
	}
	
	/**
	 * Do the tween
	 * @param pEnd The end value to tween the value to
	 */
	private function startTween(pEnd:Number):Void
	{
		var begin:Number = mObject[mProperty];
		mEffectTween.stop();
		mEffectTween = new Tween(mObject, mProperty, mEasingFunction, begin, pEnd, mAnimationDuration, true);
		mEffectTween.onMotionChanged = Delegate.create(this, updateEffect);	
	}
	
	/**
	 * Method triggered by the onMotionChanged event.
	 * @param pTween Reference to the tween object that triggered this event
	 */
	private function updateEffect(pTween:Tween):Void
	{
		setEffect(pTween.position);
	}
}