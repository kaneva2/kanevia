﻿import mx.utils.Delegate;

import flash.geom.Matrix;

import kaneva.events.SliderEvent;
import kaneva.omm.OMM;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.shapes.Rect;
import kaneva.omm.ui.shapes.RectRounded;
import kaneva.omm.ui.sliders.SliderThumb;

/**
 * Creates the slider control component which displays the current progress of media download and playback.  This
 * is used as an interface for users to control the position of media playback.
 * @author Scott
 */
class kaneva.omm.ui.sliders.SliderControl extends ComponentDisplay
{
	/** The position the media is currently at, in seconds */
	private var mPosition:Number = 0;
	
	/** The length of the media in seconds */
	private var mDuration:Number = 0;
	
	/** The outline clip of this component */
	private var mOutlineClip:MovieClip;
	
	/** The track clip of this component */
	public var mTrackClip:MovieClip;
	
	/** The shadow clip of this component...this is just a gradient rectangle over top of track and loader bar */
	private var mShadowClip:MovieClip;
	
	/** The loader bar which shows the download progress of the media */
	private var mLoadBarClip:MovieClip;
	
	/** The draggable doo dad that allows user to move the movie to a time in the media */
	public var mThumbClip:SliderThumb;
	
	/** The width and height of the slide control */
	private var mThumbWidth:Number = 7;
	private var mThumbHeight:Number = 13;
	
	/** The corner radius for the track */
	private var mCornerRadius:Number = 3;
	
	/** The right limit where the slide control can be dragged */
	private var mThumbBoundary:Number;
	
	/** The percent of the media that has loaded */
	private var mPercentLoaded:Number = 0;

	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function SliderControl ()
	{
		super ();
	}
	
	/** Create movie clips and other things */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create the track
		mTrackClip = this.createEmptyMovieClip ("mTrackClip", this.getNextHighestDepth ());
		
		// Set button events so when user clicks the track, it will move the slider control directly to that position
//		mTrackClip.onPress = Delegate.create (this, onTrackPress);
//		mTrackClip.onRelease = mTrackClip.onReleaseOutside = Delegate.create (this, onTrackRelease);
				
		// Create the load bar
		mLoadBarClip = this.createEmptyMovieClip ("mLoadBarClip", this.getNextHighestDepth ());
		
		// Set button events so when user clicks the track, it will move the slider control directly to that position
		// Made it so user can only click on green loader bar.  Hopefully this will help prevent people from trying to
		// click ahead
		mLoadBarClip.onPress = Delegate.create (this, onTrackPress);
		mLoadBarClip.onRelease = mLoadBarClip.onReleaseOutside = Delegate.create (this, onTrackRelease);
		
		// Draw the load bar                           
		mLoadBarClip.beginFill (0xCCFF32, 100);
		Rect.draw (mLoadBarClip, 1, mHeight);
		mLoadBarClip.endFill ();

		// Create and draw the shadow
		mShadowClip = this.createEmptyMovieClip ("mShadowClip", this.getNextHighestDepth ());
		var colors:Array = [0x000000, 0x000000];
		var alphas:Array = [10, 20];
		var ratios:Array = [0, 255];
		var matrix:Matrix = new Matrix();
		matrix.createGradientBox(mThumbWidth, mThumbHeight, Math.PI/2, 0, 0);
		mShadowClip.beginGradientFill("linear",colors, alphas, ratios, matrix);
		Rect.draw(mShadowClip, mThumbWidth, mThumbHeight);
		mShadowClip.endFill ();
		
		// Create the outline
		mOutlineClip = this.createEmptyMovieClip ("mOutlineClip", this.getNextHighestDepth ());
		
		// Create the slide control clip
		mThumbClip = SliderThumb(this.attachMovie("SliderThumb", "mThumbClip", this.getNextHighestDepth()));

		// Set the button events for the slider control
		mThumbClip.onPress = Delegate.create (this, onThumbPress);
		mThumbClip.onRelease = mThumbClip.onReleaseOutside = Delegate.create (this, onThumbRelease);
	}
	
	/** Draws pieces and stuff */
	private function draw ():Void
	{
		super.draw ();
		
		// Draw the track
		mTrackClip.clear ();
		mTrackClip.beginFill (OMM.getConfig().getAsNumber("slider.control.track.fill.color"), 100);
		RectRounded.draw (mTrackClip, mWidth, mHeight, mCornerRadius);
		mTrackClip.endFill ();
		
		// Draw the outline
		mOutlineClip.clear ();
		mOutlineClip.lineStyle(0, OMM.getConfig().getAsNumber("slider.control.track.outline.color"), 100, true);
		RectRounded.draw (mOutlineClip, mWidth, mHeight, mCornerRadius);
		
		// This resizes the loader bar so that the slider can be dragged properly
		setLoaderBar (mPercentLoaded);
	}
	
	/** Sizes pieces and stuff */
	private function size ():Void
	{
		// I'm lovin' it
		super.size (); // me

		// Size and position the shadow
		mShadowClip._width = mWidth;
		mShadowClip._height = mTrackClip._height - 3;
		mShadowClip._y = (mTrackClip._height - mShadowClip._height) / 2;
		
		// Size the scrub bar
		mThumbClip.setSize(7, 14);
		
		// Position the scrub bar
		mThumbClip._y = (mTrackClip._height - mThumbClip.height) / 2;
		
		// Set the slider to the proportional position
		setThumbPosition ();
	}
	
	/** Method called when the track is pressed */
	private function onTrackPress ():Void
	{
		// We want to send press event so that the media player pauses media
		var event:SliderEvent = new SliderEvent();
		event.type = SliderEvent.THUMB_PRESS;
		event.clickTarget = SliderEvent.TRACK;
		dispatchEvent(event);
		
		// Set the position of the slider....if it was clicked somewhere to the left of the  loader bar, then position as expected.....
		// Otherwise, position it at the end of the loader bar
//		mThumbClip._x = (this._xmouse < mThumbBoundary ? this._xmouse : mThumbBoundary - 1) - mThumbClip.width / 2;
		mThumbClip._x = this._xmouse;
		
		// Call drag method so media is updated
		onThumbDrag ();
	}
	
	/** Method called when the track is released */
	private function onTrackRelease ():Void
	{
		// Send release event so that media player knows how to deal with media
		var event:SliderEvent = new SliderEvent();
		event.type = SliderEvent.THUMB_RELEASE;
		event.clickTarget = SliderEvent.TRACK;
		dispatchEvent(event);
	}
	
	/** Method called when the slide control is pressed */
	private function onThumbPress ():Void
	{
		var thumbPos:Number = mThumbClip._y;
		
		// Hoping this offset will keep users from dragging past the loader
		var offset:Number = 5;
		mThumbClip.startDrag (false, mLoadBarClip._x, thumbPos, mThumbBoundary - mThumbClip.width - offset, thumbPos);
		mThumbClip.onMouseMove = Delegate.create (this, onThumbDrag);
		
		// Dispatch a press event
		var event:SliderEvent = new SliderEvent();
		event.type = SliderEvent.THUMB_PRESS;
		event.clickTarget = SliderEvent.THUMB;
		dispatchEvent(event);
	}
	
	/** Method called by the onMouseMove when the slide control is being dragged */
	private function onThumbDrag ():Void
	{
		var percent:Number = mThumbClip._x / (mWidth - mThumbWidth);
		setPosition (mDuration * percent);

		// Dispatch a drag event
		var event:SliderEvent = new SliderEvent();
		event.type = SliderEvent.THUMB_DRAG;
		event.clickTarget = SliderEvent.THUMB;
		dispatchEvent(event);
		
		// Make the motion smooth
		updateAfterEvent ();
	}
	
	/** Method called when the slide control is released */
	private function onThumbRelease ():Void
	{
		mThumbClip.stopDrag ();
		delete mThumbClip.onMouseMove;
		
		// Dispatch a release event
		var event:SliderEvent = new SliderEvent();
		event.type = SliderEvent.THUMB_RELEASE;
		event.clickTarget = SliderEvent.THUMB;
		dispatchEvent(event);
	}
	
	/** Gives the user feedback for the progress of the media download */
	private function setLoaderBar (pValue:Number):Void
	{
		// Set the percent loaded
		mPercentLoaded = pValue;
		
		// Resize the loader bar
		mLoadBarClip._width = mWidth * pValue;
		
		// Set the boundary for the slide control.....this limits the user from dragging it beyond what has loaded
		setThumbBoundary();
	}

	/** Method executed once the movie has reached the end or if the stop button is pressed */
	private function reset ():Void
	{
		mThumbClip._x = mThumbWidth / 2;
	}
	
	/** 
	 * Set the amount of time the media has in seconds 
	 * @param pValue The number of seconds for this piece of media
	 */
	public function setDuration (pValue:Number):Void
	{
		mDuration = pValue;
	}
	
	/** 
	 * Get the location the media is playing at
	 * @return The position in seconds
	 */
	public function getPosition ():Number
	{
		return mPosition;
	}
	
	/** 
	 * Set the location the media is playing at
	 * @param pValue The position in seconds
	 */
	public function setPosition (pValue:Number):Void
	{
		mPosition = pValue;
		setThumbPosition ();
		
		// Lets listeners know that the position of the slider has changed
		var event:SliderEvent = new SliderEvent();
		event.type = SliderEvent.CHANGE;
		event.value = pValue;
		dispatchEvent(event);
	}
	
	/** Set the position of the slide control */
	private function setThumbPosition ():Void
	{
		var percent:Number = getPosition () / mDuration;
		mThumbClip._x = (mWidth - mThumbWidth) * percent;
	}
	
	/** 
	 * Set the percent loaded
	 * @param pValue The percentage of the media that has downloaded
	 */
	public function setPercentLoaded (pValue:Number):Void
	{
//		trace("SliderControl.setPercentLoaded("+pValue+")");
		setLoaderBar (pValue);
	}

	/** set the boundry of the slide control*/
	private function setThumbBoundary():Void
	{
		mThumbBoundary = mLoadBarClip._width;
	}
	
	/**
	 * Set the enabled property for the thumb and track
	 * @param pValue Boolean, the value to set the enabled property to
	 */
	public function setEnabled(pValue:Boolean):Void
	{
		mThumbClip.enabled = pValue;
		mTrackClip.enabled = pValue;
	}
}
