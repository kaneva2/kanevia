import kaneva.omm.ui.buttons.AbstractButton;
import kaneva.omm.ui.effects.ColorTransformEffect;

/**
 * Represents the interface play/pause button used in the media player interface and the 
 * play list items.  
 * 
 * @author scott
 */
class kaneva.omm.ui.buttons.PlayPauseButton extends AbstractButton 
{
	/** The linkage id for the graphic of this button */
	private static var BUTTON_GRAPHIC_LINKAGE_ID:String = "PlayPauseButtonGraphic";
	
	/** Used by setIcon() to show that pressing the button will play the media  */
	public static var PLAY:String = "play";
	
	/** Used by setIcon() to show that pressing the button will pause the media */
	public static var PAUSE:String = "pause";
	
	/** The button movie clip with the play/pause graphic */
	private var mButton:MovieClip;
	
	/** The icon movie clip with the button clip that is within the play/pause graphic */
	private var mIcon:MovieClip;
	
	/** The top most value for the color transform objects offset properties */
	private var mTopOffset:Number = 35;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	public function PlayPauseButton() 
	{
		super();
	}
	
	/** Initialize stuff */
	private function init():Void
	{
		super.init();
		
		// Set reference to the button clip
		mButton = mGraphicClip.mButton;
		
		// Set reference to the icon clip
		mIcon = mButton.mIcon;
		
		// Create an instance of the color transform effect object
		createEffect(ColorTransformEffect);
		
		// Reset the target to the button rather than the entire graphic clip
		mEffect.setTarget(mButton);
	}
	
	/** Create assets stuff */
	private function createChildren():Void
	{
		super.createChildren();
		
		// Create the graphic
		mGraphicClip = this.attachMovie(BUTTON_GRAPHIC_LINKAGE_ID, "mGraphicClip", this.getNextHighestDepth());
	}
	
	/** Triggered when user presses button */
	private function onMousePress():Void
	{
		super.onMousePress();
		
		// Set the scale of the button to show it is being pressed
		mButton._xscale = mButton._yscale = 92;
		
		// Set color transform so it looks like it's being depressed
		mEffect.setEffect(-mTopOffset);
	}
	
	/** Triggered when user releases button */
	private function onClick():Void
	{
		super.onClick();
		
		// Set the scale of the button to show it is being released
		mButton._xscale = mButton._yscale = 100;
		
		// Set color transform so it looks like it's being depressed
		mEffect.setEffect(mTopOffset);
	}
	
	/** 
	 * Set the icon to either play or pause
	 * @param pFrame Which icon to show....set it to either PLAY or PAUSE (due to frame labels)
	 */
	public function setIcon(pFrame:String):Void
	{
		mIcon.gotoAndStop(pFrame);
	}
}