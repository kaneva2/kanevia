import kaneva.omm.ui.buttons.AbstractDetailsButton;
import kaneva.omm.ui.shapes.Rect;
import TextField.StyleSheet;

/**
 * These consist of the Report, Info, and Help buttons that are in the video base
 * @author scott
 */
class kaneva.omm.ui.buttons.ServiceButton extends AbstractDetailsButton 
{
	/** The icon for the button */
	private var mIcon:MovieClip;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function ServiceButton()
	{
		super();
	}
	
	/** Create assets stuff */
	private function createChildren():Void
	{
		super.createChildren();
		
		// Set style for the label
		mCSS.setStyle(".label", {color:"#787876", fontFamily:"Verdana", fontSize:"10"});
		
		// Create the place holder for the icon
		mIcon = this.createEmptyMovieClip("mIcon", this.getNextHighestDepth());
	}
	
	/** Sizes pieces and stuff */
	private function size():Void
	{
		super.size ();
		
		// Position icon
		mIcon._x = (mWidth - mIcon._width)/2;
		
		// Position the label
		mLabelTextField._x = (mWidth - mLabelTextField._width)/2;
		mLabelTextField._y = mIcon._y + mIcon._height + 1;
		
	}
	
	/**
	 * Set the icon
	 * @param pIconLinkage The linkage id of the icon to give this button
	 */
	public function setIcon(pIconLinkage:String):Void
	{
		mIcon.attachMovie(pIconLinkage, pIconLinkage, 0);
		
		// Got rid of caching because it seemed the culprit for icons not appearing.  My guess is that when video was resizing
		// it hid the icons and that's the way the images get cached
//		mIcon.cacheAsBitmap = true;
		
		// Resize so it fits the content.....label and icon
		setSize(mLabelTextField._width, mLabelTextField._height + mIcon._height);
		
		// If there is no icon, then we want the text to be white
		if(!pIconLinkage)
		{
			var css:Object = mCSS.getStyle(".label");
			css.color = "#FFFFFF";
			mCSS.setStyle(".label", css);
		}
	}
}