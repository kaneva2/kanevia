import mx.utils.Delegate;

import kaneva.events.MouseEvent;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.effects.ColorTransformEffect;
import kaneva.omm.ui.effects.Effect;

/**
 * This is the base class for all the buttons.  This will contain all the common
 * assets, methods, and properties that will be used throughout all the button
 * classes in omm.
 * @author scott
 */
class kaneva.omm.ui.buttons.AbstractButton extends ComponentDisplay 
{
	/** The linkage id for the graphic of this button */
	private static var BUTTON_GRAPHIC_LINKAGE_ID:String;
		
	/** The graphic clip for this button */
	private var mGraphicClip:MovieClip;
	
	/** The effect object used for the animation of the button events */
	private var mEffect:Effect;

	/** Constructor.....abstract class should not be instantiated, only extended */
	private function AbstractButton() 
	{
		super();
	}
	
	/** Initialize stuff */
	private function init():Void
	{
		super.init();
		
		// Set the button events for this button
		this.onPress = Delegate.create (this, onMousePress);
		this.onRelease = Delegate.create (this, onClick);
		this.onRollOver = this.onDragOver = Delegate.create (this, onMouseOver);
		this.onRollOut = this.onDragOut = this.onReleaseOutside = Delegate.create (this, onMouseOut);
		
		// Create an instance of the color transform effect object.  This will be the default effect
		// object used for most buttons. To use a different effect, the child class just has
		// to override this by calling setEffect in the init method and setting a different effect object.
		createEffect(ColorTransformEffect);
	}
	
	/**
	 * Creates the appropriate effect object that will be used for animating button events for this button
	 * @param pClass The class name of the effect class to create
	 */
	public function createEffect(pClass:Function):Void
	{
		mEffect = new pClass();
		
		// Set the target graphic that will be updated when the effects animates.  This can be overriden
		// by calling setTarget in the init method of the child class and specifying a different graphic clip
		// after the createEffect call
		mEffect.setTarget(mGraphicClip);
	}
	
	/** Triggered when user presses button */
	private function onMousePress():Void
	{
		mEffect.end();
	}
	
	/** Dispatches a click event when user clicks this button */
	private function onClick():Void
	{
		mEffect.end();
		
		var mouseEvent:MouseEvent = new MouseEvent();
		mouseEvent.type = MouseEvent.CLICK;
		dispatchEvent(mouseEvent);
	}
	
	/** Triggered when user mouses over the button. This will be used for rollover effects */
	private function onMouseOver():Void
	{
		mEffect.forward();
	}
	
	/** Triggered when user mouses out of the button. This will be used for rollout effects */
	private function onMouseOut():Void
	{
		mEffect.backward();
	}
	
	/**
	 * Set if the component is enabled.  Fade out the alpha just a bit so it is visibly disabled
	 * @param pEnabled True if enabled, false otherwise
	 */
	public function setEnabled(pEnabled:Boolean):Void
	{
		super.setEnabled(pEnabled);
		
		this._alpha = pEnabled ? 100 : 50;
	}
}