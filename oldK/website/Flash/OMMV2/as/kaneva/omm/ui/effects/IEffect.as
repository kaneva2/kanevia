/**
 * Interface for all effects classes.  This specifies methods that are necessary 
 * for all effects to update properly.
 * @author scott
 */
interface kaneva.omm.ui.effects.IEffect 
{
	/**
	 * Sets the filter or transform to the graphic clip
	 * @param pValue The property value to set
	 */
	public function setEffect(pValue:Number):Void;
}