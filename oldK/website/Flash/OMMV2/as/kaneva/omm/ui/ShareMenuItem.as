﻿import mx.transitions.easing.Regular;
import mx.transitions.Tween;
import mx.utils.Delegate;

import kaneva.events.MouseEvent;
import kaneva.omm.CSS;
import kaneva.omm.OMM;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.shapes.Rect;
import kaneva.omm.ui.ShareMenu;

/**
 * Creates the share menu item
 * @author Scott
 */
class kaneva.omm.ui.ShareMenuItem extends ComponentDisplay
{
	/** The share menu that contains this item */
	private var owner:ShareMenu;
	
	/** The base for the clip */
	private var mBaseClip:MovieClip;
	
	/** Tween used to fade in the base */
	private var mBaseTween:Tween;
	
	/** stylesheet for the text*/
	private var mCSS:TextField.StyleSheet;
	
	/** Shows text for the item */
	private var mTextField:TextField;
	
	/** Clip the icon is loaded into */
	private var mIconClip:MovieClip;
	
	/** Icon to show for this menu item.*/
	private var mIconLinkage:String = null ;
	
	/** Text to display for this menu item. */
	private var mText:String = null ;
	
	/** URL to which the user should be sent on interaction, if applicable.*/
	private var mLink:String = null ;

	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function ShareMenuItem ()
	{
		super ();
	}
	
	/** Initializes stuff */
	private function init ():Void
	{
		super.init ();
		
		// Set the owner for this item
		owner = ShareMenu (this._parent._parent);
	}
	
	/** Creates movie clips and other objects */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create the base clip
		mBaseClip = this.createEmptyMovieClip ("mBaseClip", this.getNextHighestDepth ());
		mBaseClip._alpha = 0;
		
		// Set the button events
		mBaseClip.onRelease = Delegate.create (this, onClick);
		mBaseClip.onRollOver = mBaseClip.onDragOver = Delegate.create (this, onMenuOver);
		mBaseClip.onRollOut = mBaseClip.onDragOut = Delegate.create (this, onMenuOut);
		
		// Create the icon clip
		mIconClip = this.createEmptyMovieClip ("mIconClip", this.getNextHighestDepth ());
		
		// set the stylesheet for the textfield
		mCSS = CSS.getShareMenuStyle();
		
		// Create the text field
		mTextField = this.createTextField ("mTextField", this.getNextHighestDepth (), 0, 0, 0, 0);
		
		// Set properties for the text field
		mTextField.selectable = false;
		mTextField.html = true;
		mTextField.styleSheet = mCSS;
	}
	
	/** Draws stuff */
	private function draw ():Void
	{
		super.draw ();
		
		// Draw the base
		drawBase ();
		
	}
	
	/** Sizes and positions stuff */
	private function size ():Void
	{
		super.size ();
		
		mIconClip._x = 10;
		mIconClip._y = (mHeight - mIconClip._height) / 2;
		
		mTextField._width = mWidth - (mIconClip._x + mIconClip._width);
		mTextField._height = mTextField.getTextFormat().getTextExtent(" ").textFieldHeight;
		if(mIconClip._x + mIconClip._width < 50)
			mTextField._x = 50;
		else
			mTextField._x = mIconClip._x + mIconClip._width + 5;
		mTextField._y = (mHeight - mTextField._height) / 2;
	}
	
	/** Draws the base clip */
	private function drawBase ():Void
	{	
		mBaseClip.clear ();
		mBaseClip.beginFill (OMM.getConfig().getAsNumber("share.menu.item.highlight.color"), 100);
		Rect.draw (mBaseClip, mWidth, mHeight);
		mBaseClip.endFill ();
	}
	
	/** Triggered when this item is clicked */
	private function onClick ():Void
	{	
		// Make the share menu that contains this item dispatch a click event
		var mouseEvent:MouseEvent = new MouseEvent();
		mouseEvent.type = MouseEvent.CLICK;
		owner.dispatchEvent(mouseEvent);
		
//		owner.dispatchEvent ({type:"click", target:this, link:mLink});
	}
	
	/** Rollover event method */
	private function onMenuOver ():Void
	{	
		mBaseTween = new Tween (mBaseClip, "_alpha", Regular.easeOut, mBaseClip._alpha, 100, 0.25, true);
	}
	
	/** Rollout event method */
	private function onMenuOut ():Void
	{	
		mBaseTween = new Tween (mBaseClip, "_alpha", Regular.easeOut, mBaseClip._alpha, 0, 0.25, true);
	}
	
	public function getLink():String { return this.mLink ; }
	public function setLink(pLink:String):Void { this.mLink = pLink ; }
	
	public function getIcon():String { return mIconLinkage ; }
	public function setIcon(pIcon:String):Void 
	{
		mIconLinkage = pIcon;
		var loadedIcon:MovieClip = mIconClip.attachMovie (mIconLinkage, mIconLinkage, 0);
		loadedIcon.cacheAsBitmap = true;
		size ();
	}

	public function setText(pText:String):Void {mTextField.htmlText = "<p class = 'label'>" + pText + "</p>"; mText = pText;}
	public function getText():String{return mText;}

}
