﻿import mx.events.EventDispatcher;

import kaneva.events.Event;
import kaneva.omm.IEventDispatcher;

/** 
 * Gives a movieclip that extends this class the ability to listen for and dispatch events.
 * This is a common way for components to deal with events and all custom components
 * should extend this class.
 * @author Scott
 * @see http://livedocs.macromedia.com/flash/8/main/00003471.html
 */
class kaneva.omm.ui.MovieClipDispatcher extends MovieClip implements IEventDispatcher
{
	/** Private constructor which should only be extended */
	private function MovieClipDispatcher ()
	{
		// Give this object the ability to add and remove listeners, and dispatch events.
		EventDispatcher.initialize (this);
	}
	
	/** 
	 * Implement the EventDispatcher methods
	 * @see mx.events.EventDispatcher
	 */
	public function addEventListener(pEvent:String, pListener:Function):Void {}
	public function dispatchEvent(pEventObj:Event):Void {}
	public function removeEventListener(pEvent:String, pListener:Function):Void {}
}
