import kaneva.omm.ui.buttons.AbstractButton;
import kaneva.omm.ui.effects.GlowEffect;

/**
 * @author scott
 */
class kaneva.omm.ui.buttons.KanevaLogoButton extends AbstractButton 
{
	/** The linkage id for the graphic of this button */
	private static var BUTTON_GRAPHIC_LINKAGE_ID:String = "KanevaLogoButtonGraphic";
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function KanevaLogoButton() 
	{
		super();
	}
	
	/** Initialize stuff */
	private function init():Void
	{
		super.init();
		
		// Create an instance of the glow effect object
		createEffect(GlowEffect);
	}
	
	/** Create assets stuff */
	private function createChildren():Void
	{
		super.createChildren();
		
		// Create the graphic
		mGraphicClip = this.attachMovie(BUTTON_GRAPHIC_LINKAGE_ID, "mGraphicClip", this.getNextHighestDepth());
	}
	
}