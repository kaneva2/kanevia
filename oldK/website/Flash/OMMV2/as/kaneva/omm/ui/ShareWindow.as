﻿import mx.utils.Delegate;

import flash.geom.Matrix;

import kaneva.events.ItemClickEvent;
import kaneva.events.MouseEvent;
import kaneva.omm.Constants;
import kaneva.omm.CSS;
import kaneva.omm.Logger;
import kaneva.omm.OMM;
import kaneva.omm.ui.buttons.MediaNavButton;
import kaneva.omm.ui.buttons.PopUpButton;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.shapes.RectRounded;
import kaneva.util.JavaScript;
import kaneva.util.Strings;

/**
 * Creates a pop up window that displays html code which people can copy and paste to stick in their websites
 * @author Scott
 */
class kaneva.omm.ui.ShareWindow extends ComponentDisplay
{
	/** The white base of the window */
	private var mWhiteBase:MovieClip;
	
	/** The white base of the window */
	private var mGrayBase:MovieClip;

	/** the gap separating the two bases*/
	private var mGap:Number = 10;
	
	/** The ok button */
	private var mOkButton:PopUpButton;

	/** Opens a link to a specific website */
	private var mHelpButton:MediaNavButton;	
	
	/** Format object for the text fields */
	private var mFormat:TextFormat;
	
	/** Description text fields that label the input text boxes which will display the text to be copied */
	private var mShareTextField:TextField;
	private var mShareTextField2:TextField;
	private var mEmbeddedTextField:TextField;
	private var mEmbeddedTextField2:TextField;
	
	/** Where the information to be copied is displayed */
	private var mShareTextInput:TextField;
	private var mEmbeddedTextArea:TextField;
	
	/** 
	 * Used to execute a sendAndLoad method.  Properties are appended to this object and sent to the server.
	 * The response from the server are sent to the xml object specified in the second parameter of the sendAndLoad method
	 */
	private var mSendingLoadVars:LoadVars;
	private var mReceivingXML:XML;	
	
	/** URL to which to upload variables */
	private var mLink:String;
	
	/** Constructor, private because this class is the AS 2.0 class for a clip */
	private function ShareWindow ()
	{
		super ();
		
	}
	
	/** Create clips and text fields and stuff */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create the base clip
		mGrayBase = this.createEmptyMovieClip("mGrayBase", this.getNextHighestDepth ());
		mWhiteBase = this.createEmptyMovieClip("mWhiteBase", this.getNextHighestDepth ());
		
		// Stick dummy button event so that nothing below it can be clicked
		mGrayBase.onRelease = function (){};
		mGrayBase.useHandCursor = false;
		
		// Create the format object
		mFormat = new TextFormat ();
		
		mFormat.font = CSS.getShareWindowStyle().getStyle(".label").fontFamily;
		
		// Create array of text field names
		var txtArray:Array = ["mShareTextField", "mShareTextField2", "mEmbeddedTextField", "mEmbeddedTextField2", "mShareTextInput", "mEmbeddedTextArea"];
		var max:Number = txtArray.length;
		
		// Create the text fields
		for (var i:Number = 0; i < max; i++)
		{
			var name:String = txtArray[i];
			var txtField:TextField = this[name] = this.createTextField (name, this.getNextHighestDepth (), 0, 0, 0, 0);
			txtField.autoSize = true;
			txtField.selectable = false;
			txtField.html = true;
			txtField.styleSheet = CSS.getShareWindowStyle();
		}	
		
		
		// Set the text for the fields
		mShareTextField.htmlText = "<p class = 'text'>URL for this media:</p>";
		mShareTextField2.htmlText = "<p class = 'text'>Use when you want a hyperlink. Good for email and when you can’t embed the video player.</p>";
		mShareTextField.wordWrap = mShareTextField2.wordWrap = mEmbeddedTextField.wordWrap = true;
		mShareTextField.multiline = mShareTextField2.multiline = mEmbeddedTextField.multiline = true;
		mEmbeddedTextField.htmlText = "<p class = 'text'>Video Player:</p>";
		mEmbeddedTextField2.htmlText = "<p class = 'text'>Add this media to your Kaneva channel, web site, or blog.</p>";
		mShareTextInput.htmlText = "<p class = 'text'>Share input text</p>";
		mEmbeddedTextArea.htmlText = "<p class = 'text'>Embedded input text</p>";
		
		// Set properties for the input and area text fields
		mShareTextInput.textColor = mEmbeddedTextArea.textColor = 0x666666;
		mShareTextInput.autoSize = mEmbeddedTextArea.autoSize = false;
		mShareTextInput.border = mEmbeddedTextArea.border = true;
		mShareTextInput.borderColor = mEmbeddedTextArea.borderColor = 0xA7A7A7;
		mShareTextInput.background = mEmbeddedTextArea.background = true;
		mShareTextInput.backgroundColor = mEmbeddedTextArea.backgroundColor = 0xF1F1F1;
		mEmbeddedTextArea.multiline = mEmbeddedTextField2.multiline = true;
		mShareTextInput.selectable = mEmbeddedTextArea.selectable = true;
		mEmbeddedTextArea.wordWrap = mEmbeddedTextField2.wordWrap = true;
		
		// Create button for the help icon. 
		mHelpButton = MediaNavButton (this.attachMovie ("MediaNavButton", "mHelpButton", this.getNextHighestDepth ()));
		mHelpButton.setLabel ("Help", true);
		mHelpButton.setIcon ("Help");
		mHelpButton.setCSS(CSS.getShareWindowStyle().getStyle('.help'));
		mHelpButton.setOut(CSS.getShareWindowStyle().getStyle('.out'));
		mHelpButton.setOver(CSS.getShareWindowStyle().getStyle('.over'));
		mHelpButton.addEventListener (MouseEvent.CLICK, Delegate.create(this, onHelpClick));		
		
		// Create the ok button
		mOkButton = PopUpButton (this.attachMovie("PopUpButton", "mOkButton", this.getNextHighestDepth ()));
		mOkButton.label = "OK";
		mOkButton.addEventListener (MouseEvent.CLICK, Delegate.create (this, onClick));
	}
	
	/** Draw all the elements */
	private function draw ():Void
	{
		super.draw ();
		
		drawBackground();
	}
	
	/** Arrange all the elements */
	private function size ():Void
	{
		super.size ();
		
		var leftColumnWidth:Number = 80;
		
		mWhiteBase._x = mWhiteBase._y = mGrayBase._x + mGap;
		
		// Position the button
		mOkButton._x = mGrayBase._width - (mOkButton._width + mGap);
		mOkButton._y = mGrayBase._height - (mOkButton._height + mGap);	
		
		// Position the share text field
		mShareTextField._x = mWhiteBase._x + mGap;
		mShareTextField._y = mWhiteBase._y + mGap;
		mShareTextField._width = leftColumnWidth;
		
		// Position and size the share input field
		mShareTextInput._x = mShareTextField._x + mShareTextField._width + mGap;
		mShareTextInput._y = mShareTextField._y;
		mShareTextInput._width = mWhiteBase._width - mShareTextInput._x - mGap;
		mShareTextInput._height = mFormat.getTextExtent(" ").textFieldHeight;		
		
		// Position the share 2 txt field
		mShareTextField2._x = mShareTextInput._x;
		mShareTextField2._y = mShareTextInput._y + mShareTextInput._height;
		mShareTextField2._width = mWhiteBase._width - mShareTextField2._x - mGap;
		mShareTextField2._height = mFormat.getTextExtent(mShareTextField2.text, mShareTextField2._width).textFieldHeight;		
		
		
		// Position the embed text field
		mEmbeddedTextField._x = mWhiteBase._x + mGap;
		mEmbeddedTextField._y = mShareTextField2._y + mShareTextField2._height + mGap;
		mEmbeddedTextField._width = leftColumnWidth;
		
		// Position and size the share input field
		mEmbeddedTextArea._x = mEmbeddedTextField._x + mEmbeddedTextField._width + mGap;
		mEmbeddedTextArea._y = mEmbeddedTextField._y;
		mEmbeddedTextArea._width = mWhiteBase._width - mEmbeddedTextArea._x - mGap;		
		
		// Position the second embed text
		mEmbeddedTextField2._x = mEmbeddedTextArea._x;
		mEmbeddedTextField2._width = mWhiteBase._width - mEmbeddedTextField2._x - 2*mGap;
		mEmbeddedTextArea._height = mWhiteBase._height - mEmbeddedTextArea._y - mEmbeddedTextField2._height;
		mEmbeddedTextField2._y = mEmbeddedTextArea._y + mEmbeddedTextArea._height;
		
		// Position the Help icon and link
		mHelpButton._x = mWhiteBase._x;
		mHelpButton._y = mOkButton._y;
	}
	
	/** Draws the background clip */
	private function drawBackground():Void
	{
		mGrayBase.clear ();
		mGrayBase.lineStyle (0, 0, 100, true);
		mGrayBase.beginFill (0xE6E6E6, 100);
		RectRounded.draw (mGrayBase, mWidth, mHeight, Constants.RADIUS);
		mGrayBase.endFill ();
		
		// Dimensions of the white base
		var mw:Number = mWidth - 2*mGap;
		var mh:Number = mHeight - 3*mGap - mOkButton._height;
		
		// Set up matrix		
		var matrix:Matrix = new Matrix ();
		matrix.createGradientBox (mw, mh, Math.PI/2, 0, 0);
		
		mWhiteBase.clear ();
		mWhiteBase.lineStyle (0, 0xCCCCCC, 100, true);
		//mWhiteBase.beginFill (0xFDFDFD, 100);
		mWhiteBase.beginGradientFill ("linear", [0xFFFFFF, 0xFFFFFF, 0xEBEDEC, 0xFFFFFF], [100, 100, 100, 100], [0, 200, 220, 255], matrix);
		RectRounded.draw (mWhiteBase, mw, mh, Constants.RADIUS);
		mWhiteBase.endFill ();
	}
	
	/** Click event for the ok button */
	public function onClick ():Void
	{
		// Dispatch an event so listeners can react to the user clicking the ok button
		var itemClickEvent:ItemClickEvent = new ItemClickEvent();
		itemClickEvent.type = ItemClickEvent.ITEM_CLICK;
		itemClickEvent.label = "cancel";
		dispatchEvent(itemClickEvent);
		
//		dispatchEvent ({type:"cancel", target:this});
		
		// Remove this window
		this.removeMovieClip();
	}
	
	private function onHelpClick():Void
	{
		var helpUri:String = OMM.getConfig().getAsString("uri.link.embedhelp");
		Logger.logMsg( "setHelp url: " + helpUri );
		JavaScript.openWindow(helpUri);
	}
	
	/** 
	 * Invoked by Flash Player when an XML document is received from the server
	 * @param pSuccess A Boolean value that evaluates to true if the XML object is successfully loaded with a XML.load() or XML.sendAndLoad() operation; otherwise, it is false
	 */
	private function onLoadXML (success:Boolean):Void
	{
		if (success)
		{
			// <omm-resp><confirm><share-data><details-url/><embed-info/></share-data>...
			
			// The rootNode will always be <omm-resp>
			var rootNode:XMLNode = mReceivingXML.firstChild;
			
			// If the root node has children...
			if (rootNode.hasChildNodes())
			{
				// Confirm node should be the first child
				var confirmNode:XMLNode = rootNode.firstChild;
				
				// If confirm node has children...
				if (confirmNode.hasChildNodes())
				{
					var shareNode:XMLNode = confirmNode.firstChild;
					
					// Get the children of the root node and it's length
					var rootChildren:Array = shareNode.childNodes;
					var numberOfRootChildren:Number = rootChildren.length;
					
					// Loop thru the children of root node
					for (var a:Number = 0; a < numberOfRootChildren; a++)
					{
						// The specified node
						var childNode:XMLNode = rootChildren[a];

						// Set the share data depending on the name of the node
						switch (childNode.nodeName)
						{
							case "details-url":
								// check to make sure node is a text node
								mShareTextInput.htmlText = "<p class = 'text'>" + (childNode.nodeType == 3 ? childNode.nodeValue : childNode.firstChild.nodeValue) + "</p>";
								break;
								
							case "embed-info":
								mEmbeddedTextArea.htmlText = "<p class = 'text'>" + Strings.HtmlEncode((childNode.nodeType == 3 ? childNode.nodeValue : childNode.firstChild.nodeValue)) + "</p>";
								break;
								
						}
					}
				}
			}
		}
	}
	
	/** Invoked when the movie clip is instantiated and appears in the timeline */
	private function onLoad ():Void
	{
		super.onLoad();
		
		// Initialize the xml object used to load xml response from the server
		mReceivingXML = new XML();
		mReceivingXML.ignoreWhite = true;
		mReceivingXML.onLoad = Delegate.create (this, onLoadXML); 
		
		// LoadVars object that sends
		mSendingLoadVars = new LoadVars();  
		mSendingLoadVars.sendAndLoad (mLink, mReceivingXML, "POST");  
	}
	
	/**
	 * Set the link
	 * @param pValue URL to which to upload variables
	 */
	public function setLink (pValue:String):Void
	{
		mLink = pValue;
	}
}