﻿/**
 * Class used to draw a triangle in a movie clip.
 * @author Scott
 */
class kaneva.omm.ui.shapes.Triangle
{
	/**
	 * Draw a basic (isosceles) triangle in a movie clip
	 * @param c The movie clip to draw the shape in
	 * @param w The width of the shape
	 * @param h The height of the shape
	 * @param d The direction of the shape (left, right, up, or down)
	 */
	public static function draw (c:MovieClip, w:Number, h:Number, d:String):Void
	{
		switch (d)
		{
			case "left" :
				c.moveTo (w, 0);
				c.lineTo (w, h);
				c.lineTo (0, h/2);
				c.lineTo (w, 0);
				break;
			case "up" :
				c.moveTo (w/2, 0);
				c.lineTo (w, h);
				c.lineTo (0, h);
				c.lineTo (w/2, 0);
				break;
			case "down" :
				c.lineTo (w, 0);
				c.lineTo (w/2, h);
				c.lineTo (0, 0);
				break;	
			case "right" :
			default : 
				c.lineTo (w, h/2);
				c.lineTo (0, h);
				c.lineTo (0, 0);
				break;
		}
	}

	/**
	 * Private constructor so it doesn't get instantiated
	 */
	private function Triangle () {}
}
