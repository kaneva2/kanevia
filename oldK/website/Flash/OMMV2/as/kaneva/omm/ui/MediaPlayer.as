﻿import mx.utils.Delegate;

import flash.geom.Matrix;

import kaneva.events.EnabledEvent;
import kaneva.events.Event;
import kaneva.events.LoadEvent;
import kaneva.events.MediaEvent;
import kaneva.events.MetaDataEvent;
import kaneva.info.MetaDataInfo;
import kaneva.events.MouseEvent;
import kaneva.events.ProgressEvent;
import kaneva.events.ResizeVideoEvent;
import kaneva.events.SliderEvent;
import kaneva.events.SoundEvent;
import kaneva.events.StatusEvent;
import kaneva.events.VideoEvent;
import kaneva.FullscreenSharedObject;
import kaneva.media.MediaAsset;
import kaneva.media.MediaType;
import kaneva.omm.CSS;
import kaneva.omm.OMM;
import kaneva.omm.ui.buttons.FullScreenButton;
import kaneva.omm.ui.buttons.PlayPauseButton;
import kaneva.omm.ui.buttons.ResizeVideoButton;
import kaneva.omm.ui.buttons.ShareButton;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.MediaObject;
import kaneva.omm.ui.MediaVideoBase;
import kaneva.omm.ui.shapes.Rect;
import kaneva.omm.ui.sliders.SliderControl;
import kaneva.omm.ui.sliders.VolumeControl;
import kaneva.util.JavaScript;
import kaneva.util.Strings;

/**
 * Creates the media player.  This brings together all the pieces and components for the media
 * player itself.  Deals with positioning of the parts and handling of component events which
 * allow this component to play and control media
 * @author Scott
 */
class kaneva.omm.ui.MediaPlayer extends ComponentDisplay
{
	/** The status values for the media player */
	public static var STOPPED:String = "STOPPED";
	public static var PLAYING:String = "PLAYING";
	public static var PAUSED:String = "PAUSED";
	
	/**count for in progress HBX send **/
	private static var count:Number = 0;
	
	/** The amount of space between elements */
	private static var GAP:Number = 5;
	
	/** The amount of space from the bottom */
	private static var BOTTOM:Number = 2;
	
	/** Status of the mediaplayer ... playing, stopped, or paused*/
	public var status:String = STOPPED;
	
	/** The height of the slider */
	public var sliderControlHeight:Number = 10;
	
	/** The height of the base pieces....the video base will be dynamically sized */
//	public var titleBaseHeight:Number = 30;
	public var mButtonBaseHeight:Number = 37;
	
	/** hides anything that is not in the viewing area*/
	private var mMask:MovieClip;
	
	/** The component used to give users ability to drag a slider to control the position of a piece of media */
	private var mSliderControl:SliderControl;
	
	/** The component used to control the volume of the sound */
	private var mVolumeControl:VolumeControl;
	
	/** Interval used to update the position of the slide control */
	private var mSliderInterval:Number;
	
	/** Keeps track of whether the slider is being dragged */
	private var mIsDragged:Boolean = false;
//	
//	/** The top base of the media player */
//	private var mBaseClip:MediaBase;
//	
	/** The base which contains the video objects. */
	private var mVideoBase:MediaVideoBase;
	
	/** The retangle base where the slider control, play button, volume control, etc. sits */
	private var mButtonBase:MovieClip;
	
	/** The base of the slider and volume */
	private var mSlideVolumeBase:MovieClip;
	
	/** The play/pause button */
	private var mPlayPauseButton:PlayPauseButton;
	
	/** The full screen button */
	private var mFullScreenButton:FullScreenButton;
	
	/** The resize video button */
	private var mResizeVideoButton:ResizeVideoButton;
	
	/** The share button */
	private var mShareButton:ShareButton;
	
	/** The text field that displays the time */
	private var mTimeTextField:TextField;
	
	/** The text field that displays the buffer */
	private var mBufferTextField:TextField;
	
	/** stylesheet for the class*/
	private var mCSS:TextField.StyleSheet;
			
	/** 
	 * Keeps track of whether the movie is paused or not before the user clicked the slider control.  This is so 
	 * after the user releases the slider control, the play will know if to continue playing, or stay paused
	 */
	private var mWasPaused:Boolean = false;
	
	/** Reference to the media object in the video base */
	private var mMediaObj:MediaObject;
	
	/** contains the data to load a piece of media into the player */
	private var mMediaAsset:MediaAsset;
	
	/** The type of media currently being played.*/
	private var mMediaType:String;
	
	/** This tells media whether or not to start playing once loaded */
	private var mAutoStart:Boolean = false;
	
	/** The volume level for the player.  This will keep track of what the volume should be reguardless of the media */
	private var mVolume:Number = 100;
	
	/** the xml that is returned from the server after the mediaplayer has told it that the current media has been viewed */
	private var mXml:XML;
	
	/** is the movie currently playing*/
	private var mViewingMedia:Boolean;
	
	/** does the server know that the movie has been viewed?*/ 
	private var mViewSentToServer:Boolean;
	
	/** The length of the media in seconds */
	private var mDuration:Number = 0;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function MediaPlayer ()
	{
		super ();
	}

	/** Initialize things */
	private function init ():Void
	{
		super.init ();
//		
//		// Listen for the progress event....this updates the progress bar in the slidercontrol component
//		mMediaObj.addEventListener (ProgressEvent.PROGRESS, Delegate.create (this, onLoadProgress));
//		
//		// Listen for the load event which is triggered once the media is completely loaded
//		mMediaObj.addEventListener ("load", Delegate.create (this, onLoadComplete));
//		
		
		// Listen for the progress event....this updates the progress bar in the slidercontrol component
		mMediaObj.addEventListener (VideoEvent.PROGRESS, Delegate.create (this, onLoadProgress));
		mMediaObj.addEventListener (SoundEvent.PROGRESS, Delegate.create (this, onLoadProgress));
		
		// Listen for the load event which is triggered once the media is completely loaded
		mMediaObj.addEventListener (SoundEvent.COMPLETE, Delegate.create (this, onSoundComplete));
		mMediaObj.addEventListener (VideoEvent.COMPLETE, Delegate.create (this, onVideoComplete));
		
		// Listen for when the image has loaded
		mMediaObj.addEventListener (LoadEvent.LOAD, Delegate.create (this, onImageLoad));
//		mMediaObj.addEventListener ("imageLoad", Delegate.create (this, onImageLoad));
		// Once the media is done playing, this event will trigger
		mMediaObj.addEventListener (MediaEvent.MEDIA_COMPLETE, Delegate.create (this, onPlayComplete));
		
		// Listen for the video resize event from the media video base
		mVideoBase.addEventListener(ResizeVideoEvent.RESIZE, Delegate.create(this, onResizeVideoEvent));
		
		// Listen for when the media is resizable or not
		mVideoBase.addEventListener(EnabledEvent.ENABLED, Delegate.create(this, onMediaResizable));
		
		// Set the listener for the resize video button
		mResizeVideoButton.addEventListener(MouseEvent.CLICK, Delegate.create(this, onResizeVideoButtonClick));
		
		// Set the listener for the fullscreen button
		mFullScreenButton.addEventListener(MouseEvent.CLICK, Delegate.create(this, onFullScreenButtonClick));
		
		// Set the listener for the share button
		mShareButton.addEventListener(MouseEvent.CLICK, Delegate.create(this, onShareButtonClick));
		
		// Set this class as a listener for the slider control component events
		mSliderControl.addEventListener (SliderEvent.THUMB_PRESS, Delegate.create (this, onSlideControlPress));
		mSliderControl.addEventListener (SliderEvent.THUMB_RELEASE, Delegate.create (this, onSlideControlRelease));
		mSliderControl.addEventListener (SliderEvent.THUMB_DRAG, Delegate.create (this, onSlideControlDrag));
		mSliderControl.addEventListener (SliderEvent.CHANGE, Delegate.create (this, setTimeElapsed));
		
		// Set volume control event
		mVolumeControl.addEventListener (SliderEvent.THUMB_DRAG, Delegate.create (this, onVolumeDrag));
		
//		// Set this class as a listener for the button base component events
//		mButtonBase.addEventListener ("play", Delegate.create (this, onPlayPauseClick));
//		mButtonBase.addEventListener ("stop", Delegate.create (this, stop));
//		mButtonBase.addEventListener ("volume", Delegate.create (this, onVolume));
//		mButtonBase.addEventListener ("press", Delegate.create (this, onVolumePress));
//		
		// Set button events
		mPlayPauseButton.addEventListener(MouseEvent.CLICK, Delegate.create(this, onPlayPauseClick));
		
		// Watch these properties
		this.watch ("status", onChangeStatus);
		
		// initialize xml
		mXml = new XML();
		// make sure to ignore any white spacing within the xml or reading it can be a pain.
		mXml.ignoreWhite = true;
	}
	
	/** Create movie clips and other things */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create the video object
		mVideoBase = MediaVideoBase (this.attachMovie ("MediaVideoBase", "mVideoBase", this.getNextHighestDepth ()));
		mVideoBase.addEventListener (MetaDataEvent.META_DATA, Delegate.create (this, onMetaData));
		
		// Get reference to the media object
		mMediaObj = mVideoBase.getMediaObject();
//		
//		// Create the top part of the base
//		mBaseClip = MediaBase (this.attachMovie ("MediaBase", "mBaseClip", this.getNextHighestDepth ()));
//		mBaseClip.showLogo (true);
//		
		// Create the bottom part of the base that has the interface buttons
		mButtonBase =  this.createEmptyMovieClip("mButtonBase", this.getNextHighestDepth ());
		
		// Create the slider volume base
		mSlideVolumeBase = this.createEmptyMovieClip("mSlideVolumeBase", this.getNextHighestDepth());
		
		// Create the button buttons
		mPlayPauseButton = PlayPauseButton(this.attachMovie("PlayPauseButton", "mPlayPauseButton", this.getNextHighestDepth()));
		mFullScreenButton = FullScreenButton(this.attachMovie("FullScreenButton", "mFullScreenButton", this.getNextHighestDepth()));
		mResizeVideoButton = ResizeVideoButton(this.attachMovie("ResizeVideoButton", "mResizeVideoButton", this.getNextHighestDepth()));
		mShareButton = ShareButton(this.attachMovie("ShareButton", "mShareButton", this.getNextHighestDepth()));
				
		// Create slider
		mSliderControl = SliderControl (this.attachMovie ("SliderControl", "mSliderControl", this.getNextHighestDepth ()));
		
		// Create the volume control
		mVolumeControl = VolumeControl(this.attachMovie("VolumeControl", "mVolumeControl", this.getNextHighestDepth()));
//		mVolumeControl.setPosition(0.75);
		
		// Create the text format object
		mCSS = CSS.getSliderStyle();
		
		// Create the text field that displays the time
		mTimeTextField = this.createTextField ("mTimeTextField", this.getNextHighestDepth (), 0, 0, 0, 0);
		mTimeTextField.autoSize = true;
		mTimeTextField.html = true;
		mTimeTextField.styleSheet = mCSS;
		mTimeTextField.htmlText = "<p class = 'text'>00:00 / 00:00</p>";
		mTimeTextField.selectable = false;

		// Create the text field that displays the time
		mBufferTextField = this.createTextField ("mBufferTextField", this.getNextHighestDepth (), 0, 0, 100, 30);
		mBufferTextField.autoSize = true;
		mBufferTextField.html = true;
		mBufferTextField.styleSheet = mCSS;
		mBufferTextField.selectable = false;

		mMask = this.createEmptyMovieClip("mMask", this.getNextHighestDepth());
		mVideoBase.setMask(mMask);
	}
	
	/** Draws stuff */
	private function draw():Void
	{
		super.draw();
		
		// Draw the button base
		var colors:Array = [0xFCFCFC, 0xC8C8C8];
		var alphas:Array = [100, 100];
		var ratios:Array = [0, 255];
		var matrix:Matrix = new Matrix();
		matrix.createGradientBox(mWidth, mButtonBaseHeight, Math.PI/2, 0, 0);
		mButtonBase.clear();
		mButtonBase.lineStyle(1, 0xBDBDBD, 100, false, "none", "none");
		mButtonBase.beginGradientFill("linear",colors, alphas, ratios, matrix);
		Rect.draw(mButtonBase, mWidth, mButtonBaseHeight);
		mButtonBase.endFill ();
	}
	
	/** Sizes pieces and stuff */
	private function size ():Void
	{
		super.size ();
		
		// Position and size the video
		mVideoBase.setSize (mWidth, mHeight - mButtonBaseHeight);
		
		// Position the button base
		mButtonBase._y = mVideoBase._x + mVideoBase.height;
		
		// Position the play button
		mPlayPauseButton._x = mButtonBase._x + GAP;
		mPlayPauseButton._y = mButtonBase._y + (mButtonBase._height - mPlayPauseButton.height)/2;
		
		// Position the share button
		mShareButton._x = mWidth - mShareButton.width - GAP;
		mShareButton._y = mHeight - mShareButton.height - BOTTOM;
		
		// Position the resize video button
		mResizeVideoButton._x = mWidth - mResizeVideoButton.width - GAP;
		mResizeVideoButton._y = mShareButton._y - mShareButton.height - GAP;
		
		// Position the full screen button
		mFullScreenButton._x = mShareButton._x;
		mFullScreenButton._y = mResizeVideoButton._y;
		
		// Position the slider volume base
		mSlideVolumeBase._x = mPlayPauseButton._x + mPlayPauseButton.width + GAP;
		mSlideVolumeBase._y = mButtonBase._y + mButtonBase._height - mSlideVolumeBase._height;
		
		// Draw the slide volume base...needs to be drawn here because we want it to fill in the space
		// between the play button and the other buttons (fullscreen, share, resize).  We have to position
		// them first before we know how big to make this 
		drawSlideVolumeBase(mShareButton._x - mSlideVolumeBase._x - GAP, mButtonBaseHeight*5/6, 5);
		
		// Position the volume control clip
		mVolumeControl._x = mSlideVolumeBase._x + mSlideVolumeBase._width - mVolumeControl._width - GAP * 2;
		mVolumeControl._y = mButtonBase._y + mButtonBase._height - mVolumeControl._height - BOTTOM;
		
		// Position and size the slider
		mSliderControl.setSize (mVolumeControl._x - mSliderControl._x - GAP, sliderControlHeight);
		mSliderControl._x = mSlideVolumeBase._x + GAP;
		mSliderControl._y = mButtonBase._y + mButtonBase._height - mSliderControl._height;
		
		// Position and size the text field that displays the time
		mTimeTextField._x = mSliderControl._x;
		mTimeTextField._y = mSliderControl._y - mTimeTextField._height - BOTTOM;
		
		// Position the buffer text field
		mBufferTextField._x = mTimeTextField._x + mTimeTextField._width + GAP;
		mBufferTextField._y = mTimeTextField._y;
				
		// Draw the mask to hide anything not in the viewable area
		mMask.clear ();
		mMask.beginFill (0x333333, 100);
		Rect.draw (mMask, mVideoBase.width, mVideoBase.height);
		mMask.endFill ();
		
		// Position mask to line up with the video
		mMask._x = mVideoBase._x;
		mMask._y = mVideoBase._y;
	}
	
	/** 
	 * Method invoked whenever the status property is changed
	 * @param pProp The watched property
	 * @param pOld The old value
	 * @param pNew The new value
	 * @return The value to set the property to
	 */
	private function onChangeStatus (pProp:String, pOld:String, pNew:String):String
	{
		// Set the play pause button icon 
		var frame:String = pNew == PLAYING ? PlayPauseButton.PAUSE : PlayPauseButton.PLAY;
		mPlayPauseButton.setIcon(frame);
		
		// Dispatch a status event
		var statusEvent:StatusEvent = new StatusEvent();
		statusEvent.type = StatusEvent.STATUS;
		statusEvent.code = pNew;
		dispatchEvent(statusEvent);
//		dispatchEvent ({type:"status", target:this, status:pNew});
		return pNew;
	}
	
	/** 
	 * Once media is done playing, stop (reset) the media and dispatch a complete event 
	 * @param pEvent The event object passed by the dispatcher
	 */
	private function onPlayComplete (pEvent:Event):Void
	{
		// If the slider is not being dragged.....
		if (!mIsDragged)
		{
			// Dispatch a complete event
			dispatchEvent(pEvent);
//			dispatchEvent ({type:"complete", target:this});
		}
	}
	
	/** 
	 * Invoked when the video base dipatches a meta event
	 * @param pEvent An object containing one property for each metadata item
	 */
	private function onMetaData (pEvent:MetaDataEvent):Void
	{
		var info:MetaDataInfo = pEvent.info;
		
		// Set the duration on the slider only if it is defined by the metadata event....otherwise use the length from the xml
		if (info.duration)
			setDuration (info.duration);
	}
	
	
	/** 
	 * Method called by a progress event, this updates the slider control of the current download progress
	 * which allows the slider control to update its progress bar.
	 * @param pEvent The event object sent to this method from the dispatching object being listened to
	 */
	private function onLoadProgress (pEvent:ProgressEvent):Void
	{
		var percent:Number = pEvent.bytesLoaded / pEvent.bytesTotal;
		var bufferPercent:Number = pEvent.bufferPercent;
		var isBuffering:Boolean = pEvent.isBuffering;
		
		// Set the percent loaded to slider control
		mSliderControl.setPercentLoaded (percent);
		
		// If buffering....
		if (isBuffering)
		{
			// Show the buffer percent
//			mSliderControl.setBuffer ("BUFFERING " + Math.floor(bufferPercent * 100).toString() + "%");
			var bufferString:String = "Buffering " + Math.floor(bufferPercent * 100).toString() + "%";
			mBufferTextField.htmlText = "<p class = 'text'>" + bufferString + "</p>";
			
		}
		else
		{
			// Clear out the buffer percent
//			mSliderControl.setBuffer ("");
			mBufferTextField.htmlText = "";
			
			// If autostart is true...then play
			checkAutoStart ();
		}
	}
//
//	/** 
//	 * Called by the load event
//	 * @param pEvent The event object sent to this method from the dispatching object being listened to
//	 */
//	private function onLoadComplete (pEvent:ProgressEvent):Void
//	{
//		if(mMediaType == MediaType.MP3)
//			var event:SoundEvent = SoundEvent(pEvent);
//			
//		// Set slider load bar percent to 1 so that it can fade out
//		mSliderControl.setPercentLoaded (1);
//		
//		// Set the volume for the media
//		var soundObj:Sound = pEvent.soundObject;
//		soundObj.setVolume (mVolume);
//		
//		// Set the duration if it's a sound object
//		if(mMediaType == MediaType.MP3)
//		{
//			var duration:Number = soundObj.duration / 1000;
//			setDuration(duration);
//		}
//		
//		// If autostart is true...then play
//		checkAutoStart ();
//		
//		// Clear out the buffer percent
////		mSliderControl.setBuffer ("");
//	}
//	
	/** 
	 * Called by the sound complete event
	 * @param pEvent The event object sent to this method from the dispatching object being listened to
	 */
	private function onSoundComplete (pEvent:SoundEvent):Void
	{
		// Set slider load bar percent to 1 so that it can fade out
		mSliderControl.setPercentLoaded (1);
		
		// Set the volume for the media
		var soundObj:Sound = pEvent.soundObject;
		soundObj.setVolume (mVolume);
		
		var duration:Number = soundObj.duration / 1000;
		setDuration(duration);
		
		// If autostart is true...then play
		checkAutoStart ();
		
		// Clear out the buffer percent
//		mSliderControl.setBuffer ("");
	}
	
	/** 
	 * Called by the video complete event
	 * @param pEvent The event object sent to this method from the dispatching object being listened to
	 */
	private function onVideoComplete (pEvent:VideoEvent):Void
	{
		// Set slider load bar percent to 1 so that it can fade out
		mSliderControl.setPercentLoaded (1);
		
		// Set the volume for the media
		var soundObj:Sound = pEvent.soundObject;
		soundObj.setVolume (mVolume);
		
		// If autostart is true...then play
		checkAutoStart ();
		
		// Clear out the buffer percent
//		mSliderControl.setBuffer ("");
	}
	
	/** Checks to see if the auto start property was set to true.  If so, then play the media and reset it to false */
	private function checkAutoStart ():Void
	{
		if (mAutoStart)
		{
			// Start playing the media
			play ();
			
			// Then reset the auto start property so it doesn't do this again
			mAutoStart = false;
		}
	}
	
	/** Method called by an interval to set the position of the slider control */
	private function setSlider ():Void
	{
		// Set the slider position
		mSliderControl.setPosition (mMediaObj.getTime());
		
		//only send every 30 seconds
		if(count >= 3600)
		{
			sendHBXData("playp");
			count = 0;
		}
		else
		{
			count++;
		}
	}
	
	/** 
	 * Event listener method which is executed when the slider control dispatches a press event
	 * @param pEvent The event object passed in by the dispatcher 
	 */
	private function onSlideControlPress(pEvent:SliderEvent):Void
	{
		// Keep track of whether the movie was playing or not before the user starts dragging
		mWasPaused = status == PLAYING ? false : true;
		
		// Pause the media
		pause ();
		
		// Set the override argument to true so that the interval is cleared no matter the value of isPaused
		setSliderInterval (true);
		
		// Sets if the slider is being dragged
		mIsDragged = true;
	}
	
	/** 
	 * Event listener method which is executed when the slider control dispatches a drag event 
	 * @param pEvent The event object passed in by the dispatcher
	 */
	private function onSlideControlDrag(pEvent:SliderEvent):Void
	{
//		var value:Number = pEvent.value;
//		mMediaObj.setTime (value);
		
		mMediaObj.setTime (mSliderControl.getPosition());
	}
	
	/** 
	 * Event listener method which is executed when the slider control dispatches a release event
	 * @param pEvent The event object passed in by the dispatcher
	 */
	private function onSlideControlRelease(pEvent:SliderEvent):Void
	{
		if(mWasPaused)
			pause ();
		else
			play ();
		
		setSliderInterval ();
		mIsDragged = false;
	}
	
	/** 
	 * Starts or stops the interval which updates the position of the slider
	 * @param pOverride Boolean that prevents the interval from being set reguardless of the is paused property
	 */
	private function setSliderInterval (pOverride:Boolean):Void
	{
		// Clear the slider interval
		clearInterval (mSliderInterval);
		
		// If the movie was playing and the override argument is not specified, then start the slider interval
		if (status == PLAYING && !pOverride)
			mSliderInterval = setInterval (this, "setSlider", 30);
	}
	
	/** Method executed when the play/pause interface button is clicked */
	public function onPlayPauseClick ():Void
	{
		// if no actual media is loaded, call load 
		if(!mVideoBase.isMediaLoaded)
			mVideoBase.load ( mMediaAsset, true, true );
		
		// If playing, then pause....otherwise play
		if(status == PLAYING)
			pause ();
		else
			play ();
	}
	
	/** Stops the media */
	public function stop ():Void
	{
		// Sets buttons, text fields, etc. so it relays to users that media is stopped
		showStopped ();
		
		// Set media object to stop
		mMediaObj.stop ();
		
		sendHBXData("stop");
		
		// Reset the slider to the beginning
		mSliderControl.setPosition (0);
	}
	
	/** Sets everything to relay that the media is currently stopped */
	private function showStopped ():Void
	{
		status = STOPPED;
		
		// This will clear the interval for the slider
		setSliderInterval ();
		
//		// Set icon on the play button to the play icon
//		isPlaying (false);
		
		// Show the status in the slider
//		mSliderControl.setStatus (status);
		
		// Show the play button on the video base
		mVideoBase.togglePlayButtonVisible (true);
	}
	
	/** Plays the media */
	public function play ():Void
	{
		// Sets buttons, text fields, etc. so it relays to users that media is playing
		showPlaying ();
		
		// Set media object to play
		mMediaObj.play ();
		
		sendHBXData("play");
		
		if(mViewingMedia && !mViewSentToServer)
			sendViewToServer();

	}
	
	/** Sets everything to relay that the media is currently playing */
	private function showPlaying ():Void
	{
		// Sets buttons, text fields, etc. so it relays to users that media is playing
		status = PLAYING;
		
		// Set the interval
		setSliderInterval ();
		
//		// Set icon on the play button to the pause icon
//		isPlaying (true);
//		
		// Show the status in the slider
//		mSliderControl.setStatus (status);
		
		// Hide the play button on the video base
		mVideoBase.togglePlayButtonVisible (false);
		
		// Resize the media if need be
		mVideoBase.toggleMediaSize ();
	}
	
	/** Pauses the media */
	public function pause ():Void
	{
		// Sets buttons, text fields, etc. so it relays to users that media is paused
		showPaused ();
		
		// Set media object to pause
		mMediaObj.pause ();
		
		//sends HBX data
		sendHBXData("pause");
	}
	
	/** Sets everything to relay that the media is currently paused */
	private function showPaused ():Void
	{
		status = PAUSED;
		
		// This will clear the interval for the slider
		setSliderInterval ();
		
//		// Set icon on the play button to the play icon
//		isPlaying (false);
		
		// Show the status in the slider
//		mSliderControl.setStatus (status);
		
		// Show the play button on the video base
		mVideoBase.togglePlayButtonVisible (false);
	}
//	
//	/** 
//	 * Triggered when the sound button is pressed.  This will set the position of the volume slider to the volume of the media player 
//	 * @param pEvent Object passed by the event dispatcher
//	 */
//	public function onVolumePress (pEvent:Object):Void
//	{
//		var vc:VolumeControl = VolumeControl(pEvent.target);
//		vc.setPosition (mVolume / 100);
//	}
//	
//	/**
//	 * Sets the volume for the media when the volume control is dragged
//	 * @param pEvent Object passed by the event dispatcher
//	 */
//	public function onVolume (pEvent:Object):Void
//	{
//		var percent:Number = pEvent.percent;
//		mVolume = percent * 100;
//		mMediaObj.setVolume (mVolume);
//	}
//	
	/**
	 * Loads the media
	 * @param pMedia The media asset that contains all info about a piece of media to be played
	 * @param pPlay Boolean that tells the media whether to automatically start playing or not
	 * @param pDetailMode size of the media, maximized or minimized
	 * @param pEnableBase - determines whether the buttons on the base should be able to be interacted with
	 * @param pViewing - tells the mediaplayer if it should tell the server that this piece of media is being viewed
	 */
	public function load (pMedia:MediaAsset, pPlay:Boolean, pDetailMode:Boolean, pEnableBase:Boolean, pViewing:Boolean):Void
	{
		// media has just loaded and is not playing yet, therefore, the server doesn't think the media has been viewed yet
		mViewSentToServer = false;
		
		mMediaAsset = pMedia;
		
		// Set the media type
		mMediaType = pMedia.type;
		
		// Set the duration of the media on the slide control bar
		mSliderControl.setDuration (convertLengthToSeconds (pMedia.length));
		
		// Load the media file
		mVideoBase.load ( pMedia, pPlay, pDetailMode, pEnableBase );
		
		// tell the server that this media is being viewed
		mViewingMedia = pViewing;
		
		// Set auto start...if true, then once enough of the media has loaded, it will begin to play the media
		// See the onLoadProgress
		mAutoStart = pPlay;
		
	}
	
	/** 
	 * Triggered by the imageLoad event from the media object when an image had fully loaded
	 * @param pEvent The event object passed by the dispatcher
	 */
	private function onImageLoad(pEvent:LoadEvent):Void
	{
		// If the media type is that of an image (mp3's have thumbnail images....don't want this to happen for mp3 thumbs)
		if (mMediaType == MediaType.IMAGE)
		{
			// Set the duration
			setDuration (OMM.getConfig().getAsNumber("image.time"));
			
			// Need to set slider load bar to full so we can drag the slider
			mSliderControl.setPercentLoaded (1);
			
			// Start playing slider interval
			checkAutoStart ();
		}
	}
	
	/** Convert the length property to seconds */
	private function convertLengthToSeconds (pValue:String):Number
	{
		// Split the string by the : since the value is going to be in HH:MM:SS or MM:SS format
		var timeArray:Array = pValue.split(":");
		var arrLength:Number = timeArray.length;
		
		var hours:Number;
		var minutes:Number;
		var seconds:Number;
		
		// If length is format HH:MM:SS
		if (arrLength == 3)
		{
			hours = Number (timeArray[0]);
			minutes = Number (timeArray[1]);
			seconds = Number (timeArray[2]);
			
			return hours * 3600 + minutes * 60 + seconds;
		}
		// Else length format is MM:SS
		else
		{
			minutes = Number (timeArray[0]);
			seconds = Number (timeArray[1]);
			
			return minutes * 60 + seconds;
		}
	}
//	
//	/** 
//	 * Set the title of the media
//	 * @param pValue The title of the media
//	 */
//	public function setTitle (pValue:String):Void
//	{
//		mBaseClip.setTitle (pValue);
//	}
//	
	/**
	 * Set the content text field with associated meta data passed in via the media asset parameter
	 * @param pMedia MediaAsset object containing media meta data
	 */
	public function setTextField(pMedia:MediaAsset):Void
	{
		mVideoBase.setTextField(pMedia);
	}
	
	/** sets the rating icon displayed for the current media*/
	public function setRatings(rating:Boolean):Void
	{
		// load rating graphic
		mVideoBase.setRatings(rating);
	}
	
	/** tells the server that this piece of media has been viewed*/
	private function sendViewToServer():Void
	{
		var uri:String;
		// replacing the {0} 
		uri = Strings.replace(OMM.getConfig().getAsString("uri.link.viewItem"), "{0}", String(OMM.getMedia().id));
		
		mXml.load(uri);
		mXml.onLoad = Delegate.create(this, onLoadViewFromServer);
	}
	
	// TODO - i have no idea what this method should do since all that is being returned by the server is " <omm-resp id="0" /> "
	private function onLoadViewFromServer(pSuccess:Boolean):Void
	{
		// if something is returned from the server, we'll assume that it is the <omm-resp> meaning success
		if(pSuccess)
			mViewSentToServer = true;
	}
	
	/**
	 * Draws the base for the slider and volume controls
	 * @param pW The width of the base
	 * @param pH The height of the base
	 * @param pC The corner radius
	 */
	private function drawSlideVolumeBase(pW:Number, pH:Number, pC:Number):Void
	{
		// Set up the gradient fill
		var colors:Array = [0xD6D6D6, 0xC7C7C7];
		var alphas:Array = [100, 100];
		var ratios:Array = [0, 255];
		var matrix:Matrix = new Matrix();
		matrix.createGradientBox(mWidth, mButtonBaseHeight, Math.PI/2, 0, 0);
		mSlideVolumeBase.clear();
		mSlideVolumeBase.lineStyle(1, 0xBDBDBD, 100, true, "none", "none");
		mSlideVolumeBase.beginGradientFill("linear",colors, alphas, ratios, matrix);
		
		// Draw the slider volume base
		mSlideVolumeBase.moveTo(pC, 0);
		mSlideVolumeBase.lineTo(pW - pC, 0);
		mSlideVolumeBase.curveTo(pW, 0, pW, pC);
		mSlideVolumeBase.lineTo(pW, pH);
		mSlideVolumeBase.lineTo(0, pH);
		mSlideVolumeBase.lineTo(0, pC);
		mSlideVolumeBase.curveTo(0, 0, pC, 0);
		
		// Fill is done
		mSlideVolumeBase.endFill ();
	}
	
	/**  
	 * Sets the time elapsed for the movie.....this will get called 
	 * in the method that sets the position of the slide control
	 * @param pEvent The event object passed in by the dispatcher
	 */
	public function setTimeElapsed (pEvent:SliderEvent):Void
	{
		mTimeTextField.htmlText = "<p class = 'text'>" + toMinutesSeconds (Math.floor (mSliderControl.getPosition())) + " / " + toMinutesSeconds (Math.floor (mDuration)) + "</p>";
				
//		var value:Number = pEvent.value;
//		mTimeTextField.htmlText = "<p class = 'text'>" + toMinutesSeconds (Math.floor (value)) + " / " + toMinutesSeconds (Math.floor (mDuration)) + "</p>";
	}
	
	/**
	 * Takes a number of seconds and returns a string in the format MM:SS.
	 * @param pValue The number of seconds to be converted.
	 * @return The value converted to MM:SS format.
	 */
	private function toMinutesSeconds (pValue:Number):String
	{
		// Get the minutes
		var minutes:Number = Math.floor (pValue / 60);
		minutes = isNaN (minutes) ? 0 : minutes;
		var minutesFormatted:String = minutes < 10 ? "0" + minutes : String (minutes);
		
		// Get the seconds
		var seconds:Number = pValue % 60;
		seconds = isNaN (seconds) ? 0 : seconds;
		var secondsFormatted:String = seconds < 10 ? "0" + seconds : String (seconds);
		
		// Formatted time
		return minutesFormatted + ":" + secondsFormatted;
	}
	
	/**
	 * Set the duration on the slidecontrol component and also for this class which is used to 
	 * set duration on the text field.
	 * @param pSeconds The number of seconds to set to the duration to
	 */
	private function setDuration(pSeconds:Number):Void
	{
		mDuration = pSeconds;
		mSliderControl.setDuration(pSeconds);
		setTimeElapsed();
	}
	
	/** 
	 * Method called when the volume control is dragged
	 * @param pEvent The slider event object
	 */
	private function onVolumeDrag(pEvent:SliderEvent):Void
	{
		var value:Number = pEvent.value;
		mVolume = value * 100;
		mMediaObj.setVolume (mVolume);
	}
	
	/**
	 * Triggered when the resize video button is clicked
	 */
	private function onResizeVideoButtonClick():Void
	{
		// Resize the media
		mVideoBase.resizeMedia();
	}
	
	/**
	 * Triggered when the video is resized by user clicking on the media video base
	 * @param pEvent The event object passed by the dispatcher
	 */
	private function onResizeVideoEvent(pEvent:ResizeVideoEvent):Void
	{
		// Update the icon on the resize video button
		mResizeVideoButton.setIcon(pEvent.size);
	}
	
	/**
	 * Triggered when the fullscreen button is clicked
	 */
	private function onFullScreenButtonClick():Void
	{
		// Set the media asset for the current media on the shared object so that the fullscreen
		// OMM.swf is able to easily retrieve it
		FullscreenSharedObject.setMediaAsset(mMediaAsset);
		
		// Stop the media
		stop();
		
		// Open pop up to show the full screen version
		JavaScript.fullScreen();
	}
	
	/**
	 * Triggered when the share button is clicked
	 */
	private function onShareButtonClick():Void
	{
		var shareUri:String = OMM.getConfig().getAsString("uri.link.media.share");
		var assetID:String = String(OMM.getMedia().id);
		shareUri = Strings.replace(shareUri,"{0}", assetID);
		shareUri = Strings.replace(shareUri,"{1}", assetID);
		JavaScript.openWindow(shareUri, "_blank");

		//trace("1,Share Button Click has not been implemented yet!!");
	}
	
	/**
	 * Event triggered when the media's resizability is changed
	 * @param
	 */
	private function onMediaResizable(pEvent:EnabledEvent):Void
	{
		mResizeVideoButton.setEnabled(pEvent.value);
	}
	
	/** @return The video base reference */
	public function getMediaVideoBase():MediaVideoBase
	{
		return mVideoBase;
	}
	
	/** @return The slider control reference */
	public function getSliderControl():SliderControl
	{
		return mSliderControl;
	}
	
	/** @return The full screen button */
	public function getFullScreenButton():FullScreenButton
	{
		return mFullScreenButton;
	}
	
	private function sendHBXData(movieStatus:String):Void
	{
		//set values
		var movieName:String 			= "javascript:_hbSet('m.f','" + mMediaAsset.title + "');";
		var playerName:String 			= "javascript:_hbSet('m.cl', 'OMM');";
		var flashVersion:String 		= "javascript:_hbSet('m.cv', '" + getVersion() + "');";
		var movieStat:String 			= "javascript:_hbSet('m.s', '" + movieStatus + "');";
		var movieCurrentPosition:String = "javascript:_hbSet('m.cp','" + toMinutesSeconds(mMediaObj.getTime()) + "');";
		var movieEndPosition:String 	= "javascript:_hbSet('m.ep','" + toMinutesSeconds(mMediaObj.getDuration()) + "');";
		var HCSend:String 				= "javascript:_hbSend()";

		//getURL (movieName + playerName + flashVersion + movieStat + movieCurrentPosition + movieEndPosition );
		//getURL (HCSend);
				
		//send HBX information
		//getURL (movieName + playerName + flashVersion + movieStat + movieCurrentPosition + movieEndPosition + HCSend);
	}
}

