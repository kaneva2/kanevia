﻿import mx.utils.Delegate;

import flash.geom.Matrix;

import kaneva.events.ScrollEvent;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.shapes.Rect;
import kaneva.omm.ui.shapes.Triangle;

/**
 * Custom ScrollBar component class
 * @author Scott
 */
class kaneva.omm.ui.ScrollBar extends ComponentDisplay
{
	/** 
	 * Property that gets or sets the current position of the scroll bar. 
	 * This is a percent position of the scroll bar to the scrollable area (between 0 and 1) 
	 */
	public var scrollPosition:Number = 0;
	
	/** The base for the scroll bar */
	private var mBaseClip:MovieClip;
	
	/** The scroll bar clip */
	private var mScrollBarClip:MovieClip;
	
	/** The scroll up button clip */
	private var mScrollUpButton:MovieClip;
	
	/** The scroll down button clip */
	private var mScrollDownButton:MovieClip;
	
	/** The top and bottom boundaries for the scroll bar. The scroll bar doesn't get dragged beyond these points */
	private var mScrollTop:Number;
	private var mScrollBottom:Number;
	
	/** The percent of the height of the scrollable area (between 0 and 1) */
	private var mScrollHeightPercentage:Number = 0.25;
	
	/** Interval called by the pressing of the up and down buttons */
	private var mScrollPositionInterval:Number;
	
	/** The corner radius for the buttons and scroll bar */
	private var mRadius:Number = 5;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function ScrollBar ()
	{
		super ();
	}
	
	/** Create movie clips and other things */
	private function init ():Void
	{
		super.init ();
		
		// Watch these properties
		this.watch ("scrollPosition", onScrollPosition);
	}
	
	/** Create movie clips and other things */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create the base clip
		mBaseClip = this.createEmptyMovieClip("mBaseClip", this.getNextHighestDepth ());
		mBaseClip.onRelease = Delegate.create (this, onBaseRelease);
		
		// Create the scrollbar clip
		mScrollBarClip = this.createEmptyMovieClip("mScrollBarClip", this.getNextHighestDepth ());
		mScrollBarClip.onPress = Delegate.create (this, onScrollBarPress);
		mScrollBarClip.onRelease = mScrollBarClip.onReleaseOutside = Delegate.create (this, onScrollBarRelease);
		
		// Create the scroll up button
		mScrollUpButton = this.createEmptyMovieClip("mScrollUpButton", this.getNextHighestDepth ());
		mScrollUpButton.onPress = Delegate.create (this, onScrollUpPress);
		mScrollUpButton.onRelease = mScrollUpButton.onReleaseOutside = Delegate.create (this, onScrollUpRelease);
		
		// Create the scroll down button
		mScrollDownButton = this.createEmptyMovieClip("mScrollDownButton", this.getNextHighestDepth ());
		mScrollDownButton.onPress = Delegate.create (this, onScrollDownPress);
		mScrollDownButton.onRelease = mScrollDownButton.onReleaseOutside = Delegate.create (this, onScrollDownRelease);
	}
	
	/** Draws pieces and stuff */
	private function draw ():Void
	{
		super.draw ();
		
		drawBase ();
		drawScrollUpButton ();
		drawScrollDownButton ();
		drawScrollBar ();
	}
	
	/** Sizes pieces and stuff */
	private function size ():Void
	{
		super.size ();
		
		mScrollDownButton._y = mHeight - mScrollDownButton._height;
		
		// Set the boundaries for the scroll bar
		mScrollTop = mScrollUpButton._y + mScrollUpButton._height;
		mScrollBottom = mScrollDownButton._y - mScrollBarClip._height;
		
		// When resized, keep the scroll bar in the position which keeps the same percentage value
		scrollPosition = scrollPosition;
	}
	
	/** Draw the scroll up button grahic */
	private function drawScrollUpButton ():Void
	{
		// Set up matrix		
		var matrix:Matrix = new Matrix ();
		matrix.createGradientBox (mWidth, mWidth, Math.PI, 0, 0);
		
		mScrollUpButton.clear ();
		mScrollUpButton.lineStyle (1, 0xCCCCCC, 100, true);
		mScrollUpButton.beginGradientFill ("linear", [0xEAEAEA, 0xFFFFFF], [100, 100], [30, 150], matrix);
		mScrollUpButton.lineTo(mWidth, 0);
		mScrollUpButton.lineTo(mWidth, mWidth - mRadius);
		mScrollUpButton.curveTo(mWidth, mWidth, mWidth - mRadius, mWidth); 
		mScrollUpButton.lineTo(mRadius, mWidth);
		mScrollUpButton.curveTo(0, mWidth, 0, mWidth - mRadius);
		mScrollUpButton.lineTo(0, 0);
		mScrollUpButton.endFill ();
		
		// Create the arrow....if it doesn't exist already
		var arrow:MovieClip = mScrollUpButton.arrow;
		if (!arrow)
			arrow = mScrollUpButton.createEmptyMovieClip("arrow", 0);
			
		// Draw the arrow
		arrow.beginFill(0, 100);
		Triangle.draw(arrow, mWidth/2, mWidth/3, "up");
		arrow.endFill();
		
		// Position arrow
		arrow._x = (mWidth - arrow._width) / 2;
		arrow._y = (mWidth - arrow._height) / 2;
	}
	
	/** Draw the scroll down button grahic */
	private function drawScrollDownButton ():Void
	{
		// Set up matrix		
		var matrix:Matrix = new Matrix ();
		matrix.createGradientBox (mWidth, mWidth, Math.PI, 0, 0);
		
		mScrollDownButton.clear ();
		mScrollDownButton.lineStyle (1, 0xCCCCCC, 100, true);
		mScrollDownButton.beginGradientFill ("linear", [0xEAEAEA, 0xFFFFFF], [100, 100], [30, 150], matrix);
		mScrollDownButton.moveTo(mRadius, 0);
		mScrollDownButton.lineTo(mWidth - mRadius, 0);
		mScrollDownButton.curveTo(mWidth, 0, mWidth, mRadius); 
		mScrollDownButton.lineTo(mWidth, mWidth);
		mScrollDownButton.lineTo(0, mWidth);
		mScrollDownButton.lineTo(0, mRadius);
		mScrollDownButton.curveTo(0, 0, mRadius, 0);
		mScrollDownButton.endFill ();
		
		// Create the arrow....if it doesn't exist already
		var arrow:MovieClip = mScrollDownButton.arrow;
		if (!arrow)
			arrow = mScrollDownButton.createEmptyMovieClip("arrow", 0);
			
		// Draw the arrow
		arrow.beginFill(0, 100);
		Triangle.draw(arrow, mWidth/2, mWidth/3, "down");
		arrow.endFill();
		
		// Position arrow
		arrow._x = (mWidth - arrow._width) / 2;
		arrow._y = (mWidth - arrow._height) / 2;
	}
	
	/** Draw the base graphic */
	private function drawBase ():Void
	{
		// Set up matrix		
		var matrix:Matrix = new Matrix ();
		matrix.createGradientBox (mWidth, mHeight, 0, 0, 0);
		
		mBaseClip.clear ();
		mBaseClip.lineStyle (1, 0xCCCCCC, 100, true);
		mBaseClip.beginGradientFill ("linear", [0xEAEAEA, 0xFFFFFF], [100, 100], [30, 150], matrix);
		Rect.draw (mBaseClip, mWidth, mHeight);
		mBaseClip.endFill ();
	}
	
	/** Draw the scroll bar graphic */
	private function drawScrollBar ():Void
	{
		// Get the height of the scroll bar
		var scrollBarHeight:Number = mScrollHeightPercentage * (mHeight - (mScrollDownButton._height + mScrollUpButton._height));
		var scrollBarWidth:Number = mWidth - 1;
		
		// If the scroll bar is going to be set to something less than the corner radius can be drawn....
		if (scrollBarHeight < mRadius * 2)
			return;
		
		// Set up matrix		
		var matrix:Matrix = new Matrix ();
		matrix.createGradientBox (scrollBarWidth, scrollBarHeight, 0, 0, 0);
		
		mScrollBarClip.clear ();
		mScrollBarClip.lineStyle (1, 0xCCCCCC, 100, true);
		mScrollBarClip.beginGradientFill ("linear", [0xEAEAEA, 0xFFFFFF], [100, 100], [30, 150], matrix);
		mScrollBarClip.lineTo(scrollBarWidth - mRadius, 0);
		mScrollBarClip.curveTo(scrollBarWidth, 0, scrollBarWidth, mRadius); 
		mScrollBarClip.lineTo(scrollBarWidth, scrollBarHeight - mRadius);
		mScrollBarClip.curveTo(scrollBarWidth, scrollBarHeight, scrollBarWidth - mRadius, scrollBarHeight); 
		mScrollBarClip.lineTo(0, scrollBarHeight);
		mScrollBarClip.lineTo(0, 0);
		mScrollBarClip.endFill ();
		
		// Get reference to the grip clip
		var grip:MovieClip = mScrollBarClip.grip;
		
		// Create grip lines clip if it doesn't exist
		if (!grip)
			grip = mScrollBarClip.createEmptyMovieClip("grip", 0);
		else if (mScrollBarClip._height < 16) // If the height of the scroll bar is less than what the grip will be...
		{
			// Get rid of it and return out of the method
			grip.removeMovieClip();
			return;
		}
		
		// Grip width
		var gw:Number = scrollBarWidth / 2;
		
		// Vertical space between each grip line
		var gs:Number = 3;
		
		// Draw grip
		grip.lineStyle (1, 0xAAB3B3, 100);
		for (var i:Number = 0; i < 4; i++)
		{
			grip.moveTo(0, gs * i);
			grip.lineTo(gw, gs * i);
		}
		
		// Position grip
		grip._x = (mScrollBarClip._width - grip._width) / 2;
		grip._y = (mScrollBarClip._height - grip._height) / 2;
	}
	
	/** Release event for the base clip */
	private function onBaseRelease ():Void
	{
		var sbPos:Number = this._ymouse - mScrollBarClip._height / 2;
		scrollPosition = (sbPos - mScrollUpButton._height) / (mScrollBottom - mScrollTop);
		onScrollBarDrag ();
	}
	
	/** Press event for the scroll bar */
	private function onScrollBarPress ():Void
	{
		mScrollBarClip.startDrag (false, 0, mScrollTop, 0, mScrollBottom);
		mScrollBarClip.onMouseMove = Delegate.create (this, onScrollBarDrag);
	}
	
	/** Release event for the scroll bar */
	private function onScrollBarRelease ():Void
	{
		mScrollBarClip.stopDrag ();
		calculatePercentage ();
		delete mScrollBarClip.onMouseMove;
	}
	
	/** Press event for the scroll up button */
	private function onScrollUpPress ():Void
	{
		clearInterval (mScrollPositionInterval);
		mScrollPositionInterval = setInterval (this, "setScrollPositionInterval", 30, -2/100);
	}
	
	/** Release event for the scroll up button */
	private function onScrollUpRelease ():Void
	{
		clearInterval (mScrollPositionInterval);
	}
	
	/** Press event for the scroll down button */
	private function onScrollDownPress ():Void
	{
		clearInterval (mScrollPositionInterval);
		mScrollPositionInterval = setInterval (this, "setScrollPositionInterval", 30, 2/100);
	}
	
	/** Release event for the scroll down button */
	private function onScrollDownRelease ():Void
	{
		clearInterval (mScrollPositionInterval);
	}
	
	/**
	 * Method called by a setInterval which is created by the press event of the up and down buttons
	 * @param pValue The amound to adjust the scroll position by.  This is a percent value from -1 to 1...depending on the direction you want to move
	 */
	public function setScrollPositionInterval (pValue:Number):Void
	{
		scrollPosition += pValue;
	}
	
	/** 
	 * Called on mouse move when the scroll bar is dragged.  This makes dragging smooth.  This also dispatches a drag
	 * event so that listeners know when and how to update things accordingly
	 */
	private function onScrollBarDrag ():Void
	{
		calculatePercentage ();
		updateAfterEvent ();
	}
	
	/** Calculate the percentage the scroll bar is down the draggable area */
	private function calculatePercentage ():Void
	{
		// Get the position of the scroll bar (take into account top button by subtracting it's height), and divide by the total height of the scrollable area
		scrollPosition = (mScrollBarClip._y - mScrollUpButton._height) / (mScrollBottom - mScrollTop);
	}
	
	/** 
	 * Method invoked whenever the scrollPosition property is changed
	 * @param pProp The watched property
	 * @param pOld The old value
	 * @param pNew The new value
	 * @return The value to set the property to
	 */
	private function onScrollPosition (pProp:String, pOld:Number, pNew:Number):Number
	{
		// If try to set value outside of range...
		if (pNew < 0)
			pNew = 0;
		if (pNew > 1)
			pNew = 1;
		
		// Set the position of the scroll bar
		mScrollBarClip._y = pNew * (mScrollBottom - mScrollTop) + mScrollUpButton._height;
		var event:ScrollEvent = new ScrollEvent();
		event.type = ScrollEvent.SCROLL;
		event.position = pNew;
		dispatchEvent(event);
		
		return pNew;
	}
	
	/** 
	 * Set the scroll bar height based on a percentage of the scrollable area
	 * @param pValue The value to set the scroll bar height percentage (between 0 and 1).  If it's out of range, then current value of mScrollBarHeightPercent is used
	 */
	public function setScrollSize (pValue:Number):Void
	{
		// If the parameter is a valid value.....set the height of the scroll bar
		if (pValue != undefined)
			mScrollHeightPercentage = pValue;
		
		draw ();
		size ();
	}

}