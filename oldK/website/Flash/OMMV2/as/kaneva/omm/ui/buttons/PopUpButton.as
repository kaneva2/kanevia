import kaneva.omm.CSS;
import kaneva.omm.ui.buttons.AbstractButton;
import kaneva.omm.ui.effects.ColorTransformEffect;

/**
 * Used in the popup windows within the media player.  Report window is an example
 * @author scott
 */
class kaneva.omm.ui.buttons.PopUpButton extends AbstractButton 
{
	/** The linkage id for the graphic of this button */
	private static var BUTTON_GRAPHIC_LINKAGE_ID:String = "PopUpButtonGraphic";
	
	/** The text field the label is displayed in */
	private var mLabelTextField:TextField;
	
	/** stylesheet for the class*/
	private var mCSS:TextField.StyleSheet;
	
	/** The label for the button */
	public var label:String;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function PopUpButton() 
	{
		super();
	}
	
	/** Initialize stuff */
	private function init():Void
	{
		super.init();
		
		// Create an instance of the glow effect object
		createEffect(ColorTransformEffect);
		
		// Reset the target to the entire button rather so that text is also used in the effect
		mEffect.setTarget(this);
		
		// Watch these properties
		this.watch ("label", onLabel);
	}
	
	/** Create assets stuff */
	private function createChildren():Void
	{
		super.createChildren();
		
		// Create the graphic
		mGraphicClip = this.attachMovie(BUTTON_GRAPHIC_LINKAGE_ID, "mGraphicClip", this.getNextHighestDepth());
		
		// set the stylesheet for the class
		mCSS = CSS.getButtonStyle();
		
		// Create the label text field
		mLabelTextField = this.createTextField ("mLabelTextField", this.getNextHighestDepth (), 0, 0, 0, 0);
		mLabelTextField.autoSize = true;
		mLabelTextField.selectable = false;
		mLabelTextField.html = true;
		mLabelTextField.styleSheet = mCSS;
		
	}
	
	/** Sizes and positions stuff */
	private function size ():Void
	{
		super.size ();
		
		// Position and size the text field 
		mLabelTextField._x = (mWidth - mLabelTextField._width) / 2;
		mLabelTextField._y = (mHeight - mLabelTextField._height) / 2;
	}
	
	/** 
	 * Method invoked whenever the dataProvider property is changed
	 * @param pProp The watched property
	 * @param pOld The old value
	 * @param pNew The new value
	 * @return The value to set the property to
	 */
	private function onLabel (pProp:String, pOld:String, pNew:String):String
	{
		mLabelTextField.htmlText = "<p class = 'label'>" + pNew + "</p>";
		size ();
		return pNew;
	}
}