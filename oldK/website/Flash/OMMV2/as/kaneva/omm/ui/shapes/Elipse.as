﻿/**
 * Class used to draw an elipse in a movie clip.
 * @author Scott
 */
class kaneva.omm.ui.shapes.Elipse
{
	/**
	 * Draw a basic elipse in a movie clip
	 * @param c The movie clip to draw the shape in
	 * @param w The width of the shape
	 * @param h The height of the shape
	 */
	public static function draw (c:MovieClip, w:Number, h:Number):Void
	{
		var d2r:Number = Math.PI / 180; // Degrees to Radians conversion constant
		var a:Number = Math.tan (22.5 * d2r); // Constant for the anchor (control point)
		var xr:Number = w / 2; // The radius in the x direction
		var yr:Number = h / 2; // The radius in the y direction

		c.moveTo (w, yr);

		for (var angle:Number = 45; angle <= 360; angle += 45)
		{
			var radian:Number = angle * d2r; // Calculate the radian value

			// Calculate the end point x and y values
			var endx:Number = xr * Math.cos (radian) + xr;
			var endy:Number = yr * Math.sin (radian) + yr;

			// Calculate the control point x and y values (radian - Constants.HALF_PI is used to give the correct sign)
			var cx:Number = endx + xr * a * Math.cos (radian - Math.PI/2);
			var cy:Number = endy + yr * a * Math.sin (radian - Math.PI/2);

			c.curveTo (cx, cy, endx, endy); // Draw the curve
		}
	}

	/**
	 * Private constructor so it doesn't get instantiated
	 */
	private function Elipse () {}
}
