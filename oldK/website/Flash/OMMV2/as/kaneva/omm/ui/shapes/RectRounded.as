﻿/**
 * Class used to draw a rounded rectangle in a movie clip.
 * @author Scott
 */
class kaneva.omm.ui.shapes.RectRounded
{
	/**
	 * Draw a rectangle with rounded corners in a movie clip
	 * @param c The movie clip to draw the shape in
	 * @param w The width of the shape
	 * @param h The height of the shape
	 * @param r The radius of the corners
	 */
	public static function draw (c:MovieClip, w:Number, h:Number, r:Number):Void
	{
		c.moveTo (r, 0);
		c.lineTo (w - r, 0);
		c.curveTo (w, 0, w, r);
		c.lineTo (w, h - r);
		c.curveTo (w, h, w - r, h);
		c.lineTo (r, h);
		c.curveTo (0, h, 0, h - r);
		c.lineTo (0, r);
		c.curveTo (0, 0, r, 0);
	}

	/**
	 * Private constructor so it doesn't get instantiated
	 */
	private function RectRounded () {}
}
