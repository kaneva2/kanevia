﻿import mx.transitions.easing.Regular;
import mx.transitions.Tween;
import mx.utils.Delegate;

import kaneva.events.LoadEvent;
import kaneva.events.MediaEvent;
import kaneva.events.MetaDataEvent;
import kaneva.info.MetaDataInfo;
import kaneva.events.NetStatusEvent;
import kaneva.events.SoundEvent;
import kaneva.events.VideoEvent;
import kaneva.media.MediaAsset;
import kaneva.media.MediaType;
import kaneva.omm.OMM;
import kaneva.omm.ui.AudioObject;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.ImageObject;
import kaneva.omm.ui.MediaVideoBase;
import kaneva.omm.ui.shapes.Rect;
import kaneva.omm.ui.VideoObject;

/**
 * Object which contains the all the possible media components needed to display media (AudioObject, VideoObject, ImageObject)
 * This will make it easier to deal with the different media types in the MediaVideoBase.  Rather than it having to figure out
 * which type of media to display, this class will handle all that stuff.  Plus this will make it easier to transition
 * from one piece of media to the next
 * @author Scott
 */
class kaneva.omm.ui.MediaObject extends ComponentDisplay
{
	/** The media video base that houses this component */
	public var owner:MediaVideoBase;
	
	/** Clip the contains the border for the video */
	private var mBorderClip:MovieClip;
	
	/** The video objects used to play loaded video media (.flv) */
	private var mVideoObj:VideoObject;
	
	/** The audio object used to load and play audio media (.mp3) */
	private var mAudioObj:AudioObject;
	
	/** Object used to load in images file types (.jpg, .gif, .png) */
	private var mImageObj:ImageObject;
	
	/** Used to keep track of which media type was last loaded into.  This way we can properly fade out */
	private var mPrevMediaType:String;
	
	/** The type of media currently being played.*/
	private var mMediaType:String;
	
	/** Media asset object for the media to load */
	private var mMediaAsset:MediaAsset;
	
	/** The length of the media in seconds */
	private var mDuration:Number = 0;
	
	/** Tween object used to alpha fade in and out the media */
	private var mFadeTween:Tween;
	
	/** Length of time the tween takes*/
	private var mTime:Number = 0.5;
	
	/** Easing function used for the tween */
	private var mFunc:Function = Regular.easeOut;
	
	/** Tell the on status if this video is to be a thumbnail preview of the video */
	private var isThumb:Boolean = false;
	
	/** The original dimensions of the media (video or image only).  This is needed for resizing proportionally */
	public var origWidth:Number;
	public var origHeight:Number;
	
	/** 
	 * Number of milliseconds since midnight January 1, 1970 used to keep track of when the image first shows up.  This is set
	 * using a date object.  We can use this value when the image first loads and get the difference of this value and that of
	 * the current time value to see how much time has elapsed.
	 */
	private var mImageStartTime:Number;
	
	/** 
	 * Keeps track of the time elapsed (in milliseconds).  This is needed when the user pauses the media.  We need to calculate how much time 
	 * has passed so when they start again, we can reset the start time accordingly.
	 */
	private var mImageElapsedTime:Number;
	
	/** Interval that constantly updates the time of the image */
	private var mImageInterval:Number;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function MediaObject ()
	{
		super ();
	}
	
	/** Initialize things */
	private function init ():Void
	{
		super.init ();
		
		// Set reference to the media video base which this component resides in 
		owner = MediaVideoBase (this._parent);
	}
	
	/** Create movie clips and other things */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create the video object used to play video files
		mVideoObj = VideoObject (this.attachMovie ("VideoObject", "mVideoObj", this.getNextHighestDepth ()));
		mVideoObj._alpha = 0;
		
		// Create the audio object used to play audio files
		mAudioObj = AudioObject (this.attachMovie ("AudioObject", "mAudioObj", this.getNextHighestDepth ()));		

		// Create the image object used to load images
		mImageObj = ImageObject (this.attachMovie ("ImageObject", "mImageObj", this.getNextHighestDepth ()));
		mImageObj._alpha = 0;
		
		// Create a border clip
		mBorderClip = this.createEmptyMovieClip ("mBorderClip", this.getNextHighestDepth ());	

		// Listen for the progress event....this updates the progress bar in the slidercontrol component
		mVideoObj.addEventListener (VideoEvent.PROGRESS, Delegate.create (this, onVideoProgress));
		mAudioObj.addEventListener (SoundEvent.PROGRESS, Delegate.create (this, onSoundProgress));
		
		// Listen for the load event which is triggered once the media is completely loaded
		mAudioObj.addEventListener (SoundEvent.COMPLETE, Delegate.create (this, onSoundComplete));
		mVideoObj.addEventListener (VideoEvent.COMPLETE, Delegate.create (this, onVideoComplete));
		
		// Listen for the complete event for the image object.  This is different because only video and audio will update the preloader bar in
		// the slider, but image objects have their own preloader bar.  So this event will do something different
		mImageObj.addEventListener (LoadEvent.LOAD, Delegate.create (this, onImageLoadComplete));
		
		// Listen for the meta data and status events of the video object
		mVideoObj.addEventListener (MetaDataEvent.META_DATA, Delegate.create (this, onMetaData));
		mVideoObj.addEventListener (NetStatusEvent.NET_STATUS, Delegate.create (this, onStatus));
		
		// Once the audio is done playing, this event will trigger
		mAudioObj.addEventListener (SoundEvent.SOUND_COMPLETE, Delegate.create (this, onPlayComplete));
	}
	
	/** Draw stuff */
	private function draw ():Void
	{
		// Draw a border around the video object
		mBorderClip.clear ();
//		mBorderClip.lineStyle (1, 0x797372, 100, false, "none", "none");
		mBorderClip.lineStyle (1, 0xFFFFFF, 100, false, "none", "none");
		Rect.draw (mBorderClip, mWidth, mHeight);
		mBorderClip.endFill ();
	}
	
	/** Sizes pieces and stuff */
	private function size ():Void
	{
		super.size ();
		
		mVideoObj.setSize (mWidth, mHeight);
		mImageObj.setSize (mWidth, mHeight);
	}
	
	/** 
	 * Method called by a sound progress event, this updates the slider control of the current download progress
	 * which allows the slider control to update its progress bar.
	 * @param pEvent The event object sent to this method from the dispatching object being listened to
	 */
	private function onSoundProgress(pEvent:SoundEvent):Void
	{
		dispatchEvent(pEvent);
//		dispatchEvent ({type:"progress", target:this, percent:pEvent.percent, buffer:pEvent.buffer, isBuffering:pEvent.isBuffering});
	}
	
	/** 
	 * Method called by a video progress event, this updates the slider control of the current download progress
	 * which allows the slider control to update its progress bar.
	 * @param pEvent The event object sent to this method from the dispatching object being listened to
	 */
	private function onVideoProgress(pEvent:VideoEvent):Void
	{
		dispatchEvent(pEvent);
//		dispatchEvent ({type:"progress", target:this, percent:pEvent.percent, buffer:pEvent.buffer, isBuffering:pEvent.isBuffering});
	}
	
	/** 
	 * Called by the sound complete event
	 * @param pEvent The event object sent to this method from the dispatching object being listened to
	 */
	private function onSoundComplete(pEvent:SoundEvent):Void
	{
		dispatchEvent(pEvent);
//		dispatchEvent ({type:"load", target:pEvent.target, sound:pEvent.sound, percent:1});
	}
	
	/** 
	 * Called by the video complete event
	 * @param pEvent The event object sent to this method from the dispatching object being listened to
	 */
	private function onVideoComplete(pEvent:VideoEvent):Void
	{
		dispatchEvent(pEvent);
//		dispatchEvent ({type:"load", target:pEvent.target, sound:pEvent.sound, percent:1});
	}
	
	/**
	 * Invoked when the Flash Player receives descriptive information embedded in the FLV file being played
	 * @param pEvent An object containing one property for each metadata item
	 */
	private function onMetaData (pEvent:MetaDataEvent):Void
	{
		var info:MetaDataInfo = pEvent.info;
		
		// If the duration does exist, then set it
		if(info.duration)
			setDuration(info.duration);
		
		// Set original width and height of the video
		origWidth = info.width;
		origHeight = info.height;
		
		// Fade the video back in....we wait for the meta data so we have the correct aspect ratio.  This also makes sure 
		// that the first frame of the video will be visible so we have a nice fade in effect
		mFadeTween = new Tween (mVideoObj, "_alpha", mFunc, mVideoObj._alpha, 100, mTime, true);
//		
//		var metaDataEvent:MetaDataEvent = new MetaDataEvent();
//		metaDataEvent.type = MetaDataEvent.META_DATA;
//		metaDataEvent.info = info;
//		dispatchEvent (metaDataEvent);

//		dispatchEvent ({type:"meta", info:info});
		
		// Dispatch the object that was heard by the listener...sort of a relay race. This class gets
		// it, uses it, and passes it along so someone else can use it.
		dispatchEvent(pEvent);
	}
	
	/** 
	 * Invoked every time a status change or error is posted for the NetStream object
	 * @param pEvent A parameter defined according to the status message or error message
	 */
	private function onStatus(pEvent:NetStatusEvent):Void
	{
		var code:String = pEvent.info.code;
//		var netStatusEvent:NetStatusEvent = new NetStatusEvent();
//		netStatusEvent.type = NetStatusEvent.NET_STATUS;
//		netStatusEvent.info = pEvent.info;
//		dispatchEvent (netStatusEvent);
		
		dispatchEvent (pEvent);
		
		// If the video is at the end....
		if (code == "NetStream.Play.Stop")
			onPlayComplete ();
	}
	
	/** Invoked once video or audio is done playing */
	private function onPlayComplete ():Void
	{
		var event:MediaEvent = new MediaEvent();
		event.type = MediaEvent.MEDIA_COMPLETE;
		dispatchEvent(event);
		
//		dispatchEvent ({type:"complete"});
	}
	
	/** Method triggered when the image object finished loading an image */
	private function onImageLoadComplete(pEvent:LoadEvent):Void
	{
		// Set original dimensions for this media object
		origWidth = mImageObj.origWidth;
		origHeight = mImageObj.origHeight;
		
		// Dispatch event that lets video base know that image is loaded
		dispatchEvent(pEvent);
		
//		dispatchEvent ({type:"imageLoad", target:mImageObj});
		
		// Fade the image object back in
		mFadeTween = new Tween (mImageObj, "_alpha", mFunc, 0, 100, mTime, true);
	}
	
	/** Starts the image interval */
	private function startImageInterval ():Void
	{
		// Set the duration
		setDuration (OMM.getConfig().getAsNumber("image.time"));
		
		// Get the current time
		var currentDate:Date = new Date();
		var currentTime:Number = currentDate.getTime();
		
		// If there is no start time defined....set it to the current time.
		// Otherwise, the user paused the interval and we need to reset the start
		// time so it takes into account the dead time the user had this paused
		if (!mImageStartTime)
			mImageStartTime = currentTime;
		else			
			mImageStartTime = currentTime - mImageElapsedTime;
		
		// Start the interval
		clearInterval (mImageInterval);
		mImageInterval = setInterval (this, "updateImageTime", 30);
	}
	
	/** Pauses the image interval */
	private function pauseImageInterval ():Void
	{
		clearInterval (mImageInterval);
		var currentDate:Date = new Date();
		mImageElapsedTime = currentDate.getTime() - mImageStartTime;
	}
	
	/** Stops the image interval */
	private function stopImageInterval ():Void
	{
		clearInterval (mImageInterval);
		
		// Clear out these variables to make way for any other images that will get loaded up
		mImageStartTime = undefined;
		mImageElapsedTime = undefined;
	}
	
	/**
	 * Sets the elapsed time that an image has been shown
	 * @param pValue Number of milliseconds to set the elapsed time to
	 */
	private function setImageElapsedTime (pValue:Number):Void
	{
		// Get the current time
		var currentDate:Date = new Date();
		var currentTime:Number = currentDate.getTime();
		
		// Set the new elapsed time
		mImageElapsedTime = pValue;
		
		// Need to move the start time up so it corresponds to the elapsed time
		mImageStartTime = currentTime - mImageElapsedTime;
	}
	
	/** Method called by the image interval */
	private function updateImageTime ():Void
	{
		// Get the current time
		var currentDate:Date = new Date();
		var currentTime:Number = currentDate.getTime();
		
		// Set the elapsed time
		mImageElapsedTime = currentTime - mImageStartTime;
		
		// If the elapsed time is greater than or equal to the time alloted for the image to be shown....
		if (mImageElapsedTime >= mDuration * 1000)
		{
			// Stop the interval
			stopImageInterval ();
			
			// Dispatch a play complete event
			onPlayComplete ();
		}
	}
	
	/** 
	 * Method triggerd at the end of the fade out tween
	 * @param pTween Reference to the tween object that called the event
	 */
	private function onFadeOut (pTween:Tween):Void
	{
// Commented out for now because initial media still shows stop button even though media continues to play
		// Now we want to stop the media so the play button comes up.  We need to call stop on the media player to do so
//		var mp:MediaPlayer = owner.owner;
//		mp.stop ();

		// Stop the download of media from either the video or audio objects.  
	 	// We don't want video loading while we are trying to check out some audio or vice versa 
		mVideoObj.close ();
		mAudioObj.close ();
		
		// Clear out the image
		mImageObj.close ();
		
		switch (mMediaType)
		{
			case MediaType.FLV :
				
				// If is thumb is true, then only use thumbnail image in the media object....otherwise load the flv file
				if (isThumb)
					loadThumb (mMediaAsset.thumb);
				else
					mVideoObj.load (mMediaAsset.url);
				
				// Reset the is thumb variable
				isThumb = false;
				
				break;
				
			case MediaType.MP3 :
			
				// Need to reset the position of the sound
				mAudioObj.stop (); 
				
				// Don't load audio unless specified to do so
				if (!isThumb)
					mAudioObj.load (mMediaAsset.url);
					
				// Load the thumbnail image associated with the audio
				loadThumb (mMediaAsset.thumb);
				
				break;
			
			case MediaType.IMAGE :
				loadThumb (mMediaAsset.url);
				break;
		}
	}
	
	/**
	 * Loads an image into the image object
	 * @param pUrl The image to load
	 */
	private function loadThumb (pUrl:String):Void
	{
		// Load the specified image into the image object
		mImageObj.load (pUrl); 
		
		// So that preload bar is visible
		mImageObj._alpha = 100;
		
		// Set the previous type to get the fade action.....this is used for special situation where initial media is set to not load.  
		// This makes sure there is nice smooth transition
		mPrevMediaType = MediaType.IMAGE;
		
		// Reset the elapsed time
		mImageElapsedTime = 0;
	}
	
	/**
	 * Loads the specified media into the appropriate object
	 * @param pMedia Object that contains all info about media to load
	 * @param pPlay False prevents download of media initially, otherwise it will start to load
	 */
	public function load (pMedia:MediaAsset, pPlay:Boolean):Void
	{
		// If media is set to not play initially....set var that tells on status that this video is a thumb and don't load entire video
		isThumb = pPlay == false;
		
		// Pause the media while it's fading out.  We pause it (instead of stop) so it doesn't jump back to the beginning
		pause ();
		
		// Reset the original dimensions
		origWidth = 100;
		origHeight = 100;
		
		// Set the media asset
		mMediaAsset = pMedia;
		
		// Set the media type
		mMediaType = mMediaAsset.type;
		
		// If the previous media type is different than current one and previous media type is defined.....then
		// we will use the previous one to define the object to fade out....otherwise use the current one
		var mediaType:String = mMediaType != mPrevMediaType && mPrevMediaType ? mPrevMediaType : mMediaType;
		
		// Fade out the specified media object
		var mediaObj = mediaType == MediaType.FLV ? mVideoObj : mImageObj;
		mFadeTween = new Tween (mediaObj, "_alpha", mFunc, mediaObj._alpha, 0, mTime, true);
		mFadeTween.onMotionFinished = Delegate.create(this, onFadeOut);
		
		// Set the previous media type
		mPrevMediaType = mMediaType;
	}
	
	/** Stops the media */
	public function stop ():Void
	{
		switch (mMediaType)
		{
			case MediaType.MP3 :
				mAudioObj.stop ();
				mAudioObj.setPosition (0);
				break;
				
			case MediaType.FLV :
				mVideoObj.getNetStream().pause (true);
				mVideoObj.getNetStream().seek (0);
				break;
				
			case MediaType.IMAGE :
				stopImageInterval ();
				break;
		}
	}
	
	/** Plays the media */
	public function play ():Void
	{
		switch (mMediaType)
		{
			case MediaType.MP3 :
				mAudioObj.play ();
				break;
				
			case MediaType.FLV :
				mVideoObj.getNetStream().pause (false);
				break;
				
			case MediaType.IMAGE :
				startImageInterval ();
				break;
		}
	}
	
	/** Pauses the media */
	public function pause ():Void
	{
		switch (mMediaType)
		{
			case MediaType.MP3 :
				mAudioObj.pause ();
//				mAudioObj.setPosition (this.getTime());
				break;
				
			case MediaType.FLV :
				mVideoObj.getNetStream().pause (true);
				break;
				
			case MediaType.IMAGE :
				pauseImageInterval ();
				break;
		}
	}
	
	/** 
	 * Sets the volume 
	 * @param pValue The volume level (0 - 100)
	 */
	public function setVolume (pValue:Number):Void
	{
		switch (mMediaType)
		{
			case MediaType.MP3 :
				mAudioObj.setVolume (pValue);
				break;
				
			case MediaType.FLV :
				mVideoObj.setVolume (pValue);
				break;
		}
	}
	
	/** 
	 * Gets the volume 
	 * @return The volume level (0 - 100)
	 */
	public function getVolume ():Number
	{
		switch (mMediaType)
		{
			case MediaType.MP3 :
				return mAudioObj.getVolume ();
		
			case MediaType.FLV :
				return mVideoObj.getVolume ();
		}
	}
	
	/** @return The amount of time (in seconds) the play head or slider is at */
	public function getTime ():Number
	{
		switch (mMediaType)
		{
			case MediaType.MP3 :
				return mAudioObj.getPosition();
				
			case MediaType.FLV :
				return mVideoObj.getNetStream().time;
				
			case MediaType.IMAGE :
				return mImageElapsedTime / 1000;
		} 
	}
	
	/** 
	 * Set the position of the playhead for the currently loaded media
	 * @param pValue The approximate time value, in seconds, to move to in a piece of media
	 */
	public function setTime (pValue:Number):Void
	{
		switch (mMediaType)
		{
			case MediaType.MP3 :
				mAudioObj.setPosition(pValue);
				break;
				
			case MediaType.FLV :
				mVideoObj.getNetStream().seek(pValue);
				break;
				
			case MediaType.IMAGE :
				setImageElapsedTime (pValue * 1000);
				break;
		} 
	}
	
	/** 
	 * Get the amount of time the media has in seconds 
	 * @return The amount of seconds for this piece of media
	 */
	public function getDuration ():Number
	{
		return mDuration;
	}
	
	/** 
	 * Set the amount of time the media has in seconds 
	 * @param pValue Amount of time in the movie
	 */
	public function setDuration (pValue:Number):Void
	{
		mDuration = pValue;
	}
	
}