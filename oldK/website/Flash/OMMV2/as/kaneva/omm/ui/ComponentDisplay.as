﻿import kaneva.events.ResizeEvent;
import kaneva.omm.ui.MovieClipDispatcher;
import kaneva.events.LoadEvent;
/**
 * This class has the main pieces needed to create a component/compiled clip.  There are
 * six functions in the component class that must be defined:
 * constructor (), init (), createChildren (), draw (), size (), setSize ()
 * @author Scott
 * @see http://livedocs.macromedia.com/flash/8/main/00003016.html
 * @see http://livedocs.macromedia.com/flash/8/main/00003042.html
 */
class kaneva.omm.ui.ComponentDisplay extends MovieClipDispatcher
{
	/** Clip used for real time resizing.  This is the only clip that is on the timeline of component at author time. */
	private var mBoundingBox:MovieClip;

	/** Used to store the height of this component. */
	private var mHeight:Number;

	/** Used to store the width of this component. */
	private var mWidth:Number;
	
	/**
	 * Constructor function is private (pseudo-abstract).
	 * The class needs to be subclassed....not instantiated. (pseudo-abstract)
	 * @see http://livedocs.macromedia.com/flash/8/main/wwhelp/wwhimpl/common/html/wwhelp.htm?context=LiveDocs_Parts&file=00003045.html
	 */
	private function ComponentDisplay ()
	{
		super ();

		// Initialize.
		init ();
	}

	/**
	 * Initialization method.  Called once by the constructor.  This does the things
	 * necessary for creating compiled clip (component) in Flash.
	 * @see http://livedocs.macromedia.com/flash/8/main/wwhelp/wwhimpl/common/html/wwhelp.htm?context=LiveDocs_Parts&file=00003043.html
	 */
	private function init ():Void
	{
		// Set the width and height of the component to the place holder width and height.
		mWidth = _width;
		mHeight = _height;

		// Set scale back to 100 so there is no distortion
		_xscale = 100;
		_yscale = 100;

		// Hide the bounding box.
		mBoundingBox._visible = false;
		mBoundingBox._width = mBoundingBox._height = 0;

		// Create all the component pieces.
		createChildren ();

		// Draw the component pieces.
		draw ();

		// Resize the pieces.
		size ();
	}

	/**
	 * Create any component pieces.
	 * This only gets called once by the constructor.
	 * Components should implement this method to
	 * create their contents.
	 * @see http://livedocs.macromedia.com/flash/8/main/wwhelp/wwhimpl/common/html/wwhelp.htm?context=LiveDocs_Parts&file=00003044.html
	 */
	private function createChildren ():Void 
	{
		
	}

	/**
	 * Draw any component pieces. Called by setSize().
	 * Components should implement this method to
	 * draw their contents.
	 * @see http://livedocs.macromedia.com/flash/8/main/wwhelp/wwhimpl/common/html/wwhelp.htm?context=LiveDocs_Parts&file=00003046.html
	 */
	private function draw ():Void 
	{
		
	}

	/**
	 * Size the object.  Called by setSize().
	 * Components should implement this method
	 * to layout their contents.
	 * @see http://livedocs.macromedia.com/flash/8/main/wwhelp/wwhimpl/common/html/wwhelp.htm?context=LiveDocs_Parts&file=00003047.html
	 */
	private function size ():Void 
	{
		
	}

	/**
	 * Resize the component.  This method is used to size the component
	 * rather than _width or _height properties.  This allows the component to
	 * resize properly without stretching and distortion. 
	 * @param pW The width to size the component to.
	 * @param pH The height to size the component to.
	 * @param noEvent Whether or not to dispatch a resize event
	 */
	public function setSize (pW:Number, pH:Number, noEvent:Boolean):Void
	{
		var oldWidth:Number = mWidth;
	  	var oldHeight:Number = mHeight;
		
		// Set scale back to 100%
		_xscale = 100;
		_yscale = 100;
		
		// Make sure parameters are not null......then set properties.  The double underscore width and height
		// are like value place holders.  Using single underscore width and height will not work because
		// it's values change as the dimensions change.
		if (pW != null)
			mWidth = pW;

		if (pH != null)
			mHeight = pH;

		// Draw base.
		draw ();

		// Size the pieces on stage
		size ();
		
		// If noEvent is not true, then dispatch the event
		if (noEvent != true)
		{
			var event:ResizeEvent = new ResizeEvent();
			event.type = ResizeEvent.RESIZE;
			event.oldHeight = oldHeight;
			event.oldWidth = oldWidth;
			dispatchEvent(event);
		}
	}
	
	/** 
	 * This is for a component that will unintentionally have content overlapping outside of the bounding box
	 * This happens for example, when a text field overlaps because it can't be set to the same height as the component.
	 * The overlapping gives the wrong (or unwanted) dimension of the _height for example.  Calling the height property will
	 * give us the mHeight or the _height of the bounding box.  This is also consistent with how Macromedia does things
	 * with their components
	 * @return The width or height of the bounding box
	 * @see http://livedocs.macromedia.com/flash/8/main/00001874.html
	 * @see http://livedocs.macromedia.com/flash/8/main/00004216.html
	 */
	public function get width ():Number
	{
		return mWidth;
	}
	public function get height ():Number
	{
		return mHeight;
	}
	
	/**
	 * I know I am not supposed to use set and get....but this is one time I kind of need it.  I need to be able to tween the
	 * size of components, but I can't tween the _width or _height properties without distortion.  And I can't use setSize in with
	 * tween objects without creating some ugly, extraneous variable to tween, and then set that value within the setSize method for the object.
	 * That seems like more work than necessary and having setters for the width and height is easier and more intuitive....sorry
	 * @param pValue The size to set the component to
	 * @see http://livedocs.macromedia.com/flash/8/main/00001884.html
	 */
	public function set width (pValue:Number):Void
	{
		setSize (pValue, null);
	}
	public function set height (pValue:Number):Void
	{
		setSize (null, pValue);
	}
	
	/** Invoked when the movie clip is instantiated and appears in the timeline */
	private function onLoad():Void
	{
		var event:LoadEvent = new LoadEvent();
		event.type = LoadEvent.ONLOAD;
		dispatchEvent(event);
	}
	
	/**
	 * Set if the component is enabled.  This gives us chance to set visible change to component when it is disabled
	 * Override this method in subclasses
	 * @param pEnabled True if enabled, false otherwise
	 */
	public function setEnabled(pEnabled:Boolean):Void
	{
		this.enabled = pEnabled;
	}
}