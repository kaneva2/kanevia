﻿import mx.transitions.easing.Regular;
import mx.transitions.Tween;
import mx.utils.Delegate;

import flash.geom.Matrix;

import kaneva.events.MouseEvent;
import kaneva.omm.CSS;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.shapes.RectRounded;
import kaneva.util.Strings;

/**
 * Creates the button
 * @author Scott
 */
class kaneva.omm.ui.buttons.Button extends ComponentDisplay
{
	/** The label for the button */
	public var label:String;
	
	/** Colors for the button base */
	public var outlineColor:Number = 0xDBDBDB;
	public var highlight:Number = 0xFFFFFF;
	public var shadow:Number = 0xEBEAE5;
	
	/** Properties for the label */
	public var over:Number ;
	public var out:Number ;
	
	/** The base for the button */
	private var mBaseClip:MovieClip;
	
	/** The clip whose alpha is tweened to create a rollover effect for the button */
	private var mRollClip:MovieClip;
	
	/** The tween object used for rollover events */
	private var mTween:Tween;
	
	/** Function used for tween */
	private var mFunc:Function = Regular.easeOut;
	
	/** Time it takes for tween to happen */
	private var mTime:Number = 0.35;
	
	/** The text field the label is displayed in */
	private var mLabelTextField:TextField;
	
	/** stylesheet for the class*/
	private var mCSS:TextField.StyleSheet;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function Button ()
	{
		super ();
	}
	
	/** Initialize stuff */
	private function init ():Void
	{
		super.init ();
		
		// Watch these properties
		this.watch ("label", onLabel);
	}
	
	/** Creates movie clips and other objects */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create the base clip
		mBaseClip = this.createEmptyMovieClip ("mBaseClip", this.getNextHighestDepth ());
		mBaseClip.onRollOver = mBaseClip.onDragOver = Delegate.create(this, onButtonRollOver);
		mBaseClip.onRollOut = mBaseClip.onDragOut = mBaseClip.onReleaseOutside = Delegate.create(this, onButtonRollOut);
		mBaseClip.onRelease = Delegate.create(this, onClick);
		mBaseClip.onPress = Delegate.create(this, onButtonPress);
		
		// Create the roll clip
		mRollClip = this.createEmptyMovieClip ("mRollClip", this.getNextHighestDepth ());
		mRollClip._alpha = 0;
		
		// set the stylesheet for the class
		mCSS = CSS.getButtonStyle();
		
		// Create the label text field
		mLabelTextField = this.createTextField ("mLabelTextField", this.getNextHighestDepth (), 0, 0, 0, 0);
		mLabelTextField.selectable = false;
		mLabelTextField.html = true;
		mLabelTextField.styleSheet = mCSS;
		
		// assign the colors for when the mouse is out or over a button
		out = Number(Strings.replace(mCSS.getStyle(".out").color, "#", "0x"));
		over = Number(Strings.replace(mCSS.getStyle(".over").color, "#", "0x"));
	}
	
	/** Draws stuff */
	private function draw ():Void
	{
		super.draw ();
		drawBase ();
		drawRoll ();
	}
	
	/** Sizes and positions stuff */
	private function size ():Void
	{
		super.size ();
		
		// Position and size the text field 
		mLabelTextField._width = mWidth;
		mLabelTextField._height = mLabelTextField.getTextFormat().getTextExtent(" ").textFieldHeight;
		mLabelTextField._y = (mHeight - mLabelTextField._height) / 2;
	}
	
	/** Draws the base clip */
	private function drawBase ():Void
	{
		// Set up matrix		
		var matrix:Matrix = new Matrix ();
		matrix.createGradientBox (mWidth, mHeight, Math.PI/2, 0, 0);
		
		mBaseClip.clear ();
		mBaseClip.lineStyle (2, outlineColor, 100, true);
		mBaseClip.beginGradientFill ("linear", [highlight, highlight, shadow, shadow, highlight, highlight], [100, 100, 100, 100, 100, 100], [0, 20, 70, 120, 200, 255], matrix);
		RectRounded.draw (mBaseClip, mWidth, mHeight, 5);
		mBaseClip.endFill ();
	}
	
	/** Draws the roll clip */
	private function drawRoll ():Void
	{
		mRollClip.clear ();
		mRollClip.beginFill (0xCCCCCC, 100);
		RectRounded.draw (mRollClip, mWidth, mHeight, 5);
		mRollClip.endFill ();
	}
	
	/** Roll over event for this button */
	private function onButtonRollOver ():Void
	{
		var end:Number = 70;
		mTween = new Tween (mRollClip, "_alpha", mFunc, mRollClip._alpha, end, mTime, true);
		// textColor overrides the stylesheet color setting		
		mLabelTextField.textColor = over;
	}
	
	/** Roll out event for this button */
	private function onButtonRollOut ():Void
	{
		var end:Number = 0;
		mTween = new Tween (mRollClip, "_alpha", mFunc, mRollClip._alpha, end, mTime, true);
		// textColor overrides the stylesheet color setting		
		mLabelTextField.textColor = out;
	}
	
	/** Click event for this button */
	private function onClick ():Void
	{
		var mouseEvent:MouseEvent = new MouseEvent();
		mouseEvent.type = MouseEvent.CLICK;
		dispatchEvent(mouseEvent);
		
		// textColor overrides the stylesheet color setting		
		mLabelTextField.textColor = over;
	}
	
	/** Click event for this button */
	private function onButtonPress ():Void
	{
		// textColor overrides the stylesheet color setting
		mLabelTextField.textColor = out;
	}
	
	/** 
	 * Method invoked whenever the dataProvider property is changed
	 * @param pProp The watched property
	 * @param pOld The old value
	 * @param pNew The new value
	 * @return The value to set the property to
	 */
	private function onLabel (pProp:String, pOld:String, pNew:String):String
	{
		mLabelTextField.htmlText = "<p class = 'label'>" + pNew + "</p>";
		size ();
		return pNew;
	}
}
