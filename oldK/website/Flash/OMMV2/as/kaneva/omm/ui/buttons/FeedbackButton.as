import kaneva.omm.ui.buttons.AbstractDetailsButton;
import kaneva.omm.ui.shapes.Rect;

/**
 * These consist of the Raves, Comments, and Views buttons that are in the video base
 * @author scott
 */
class kaneva.omm.ui.buttons.FeedbackButton extends AbstractDetailsButton 
{
	/** The text field that shows the count */
	private var mCountTextField:TextField;
	
	/** The count */
	private var mCount:Number = 0;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function FeedbackButton()
	{
		super();
		
		// TODO - Delete the stuff eventually....for now, I leave it for reference
		// Quick little hack fix.  The initial rollover effect seems to make text jump. Doing this
		// gets rid of that
		// ****UPDATE**** 
		// This seems to be the thing that was making the text in the buttons not show up when 
		// testing in Flash test mode.  This only happened when the player was set to initially go to 
		// full screen mode.  When set to details mode, this did not affect it.
//		this.onRollOut();
	}
	
	/** Create assets stuff */
	private function createChildren():Void
	{
		super.createChildren();
		
		// Set style for the label
		mCSS.setStyle(".label", {color:"#FFFFCD", fontFamily:"Verdana", fontSize:"10"});
		
		// Set style for the count
		mCSS.setStyle(".count", {color:"#FFFFFF", fontFamily:"Verdana", fontSize:"10"});
		
		// Create the count text field
		mCountTextField = this.createTextField("mCountTextField", this.getNextHighestDepth(), 0, 0, 0, 0);
		mCountTextField.autoSize = true;
		mCountTextField.selectable = false;
		mCountTextField.html = true;
		mCountTextField.styleSheet = mCSS;
	}
	
	/** Sizes pieces and stuff */
	private function size():Void
	{
		super.size ();

		// Position the label
		mLabelTextField._x = (mWidth - mLabelTextField._width)/2;
		
		// Position the count
		mCountTextField._x = (mWidth - mCountTextField._width)/2;
		mCountTextField._y = mLabelTextField._y + mLabelTextField._height;
	}
	
	/**
	 * Get the count of the button
	 * @return mCount The count
	 */
	public function getCount():Number
	{
		return mCount;
	}
	
	/**
	 * Set the count
	 * @param pCount The count to set
	 */
	public function setCount(pCount:Number):Void
	{
		mCount = pCount;
		mCountTextField.htmlText = "<span class='count'>" + mCount + "</span>";
		size();
	}
}