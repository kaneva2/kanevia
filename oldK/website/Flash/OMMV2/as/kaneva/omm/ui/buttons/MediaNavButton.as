﻿import mx.transitions.easing.Strong;
import mx.transitions.Tween;
import mx.utils.Delegate;

import kaneva.events.MouseEvent;
import kaneva.omm.ui.buttons.AbstractButton;
import kaneva.omm.ui.shapes.Rect;

/**
 * Creates button used in the media video base (Report Media and Get Details)
 * @author Scott
 */
 class kaneva.omm.ui.buttons.MediaNavButton extends AbstractButton
 {
	/** The icon clip that is attached to the button */
	private var mIcon:MovieClip;
	
	/** Linkage id of the icon from the library to use*/
	private var mIconLinkage:String = "Rave";
	
	/** The label of the button */
	private var mLabel:String = null;
	
	/** The text field that displays the label of the button */
	private var mLabelTextField:TextField;
	
	/** stylesheet for the textfield*/
	private var mCSS:TextField.StyleSheet;
	
	/** Used for roll over effect */
	private var mColor:Color;
	private var mTransform:Object;
	private var mTransformProp:Number = 0;
	private var mTween:Tween;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function MediaNavButton ()
	{
		super ();
	}
	
	/** Create movie clips and other things */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create the stylesheet
		mCSS = new TextField.StyleSheet();//CSS.getMediaNavButtonStyle();
		
		// Create icon clip
		mIcon = this.createEmptyMovieClip("mIcon", this.getNextHighestDepth());
		
		// Create the text field that displays the label
		mLabelTextField = this.createTextField ("mLabelTextField", this.getNextHighestDepth(), 0, 0, 0, 0);
		mLabelTextField.autoSize = true;
		mLabelTextField.selectable = false;
		mLabelTextField.html = true;
		mLabelTextField.styleSheet = mCSS;
		
		// Set up the color object which is used for the rollover effect
		mColor = new Color (this);
		mTransform = {rb:mTransformProp, gb:mTransformProp, bb:mTransformProp};
	}
	
	/** Sizes pieces and stuff */
	private function size ():Void
	{
		super.size ();
		
		// Create a hit area
		this.clear ();
		this.beginFill (0, 0);
		Rect.draw (this, mWidth, mHeight);
		this.endFill ();
		
		// Position the elements
		mLabelTextField._x = mIcon._width + 5;
		mLabelTextField._y = (mHeight - mLabelTextField.getTextFormat().getTextExtent(" ").textFieldHeight) / 2;
	}
	
	/** Method executed on rollover of this button */
	private function onMouseOver ():Void
	{
		super.onMouseOver();
		
		var top:Number = 100;
		mTween = new Tween (this, "mTransformProp", Strong.easeOut, mTransformProp, top, 0.5, true);
		mTween.onMotionChanged = Delegate.create (this, onPropChange);
		setLabel (mLabel, false);
	}
	
	/** Method executed on rollout of this button */
	private function onMouseOut ():Void
	{
		super.onMouseOut();
		
		var bottom:Number = 0;
		mTween = new Tween (this, "mTransformProp", Strong.easeOut, mTransformProp, bottom, 0.5, true);
		mTween.onMotionChanged = Delegate.create (this, onPropChange);
		setLabel (mLabel, true);
	}
	
	/** Called on the motion changed event of the tween object */
	private function onPropChange ():Void
	{
		mTransform.rb = mTransformProp;
		mTransform.gb = mTransformProp;
		mTransform.bb = mTransformProp;
		mColor.setTransform (mTransform);
	}
	
	/** 
	 * Set the linkage id of the icon for the button
	 * @param pValue The linkage id
	 */
	public function setIcon (pValue:String):Void
	{
		mIconLinkage = pValue;
		mIcon.attachMovie(mIconLinkage, "icon", 0);
		mIcon.cacheAsBitmap = true;
		mIcon._y = (mHeight - mIcon._height)/2;
		size ();
	}
	
	/** 
	 * Set the label of the button
	 * @param pValue The label
	 * @param pUnderline if true underline, else text is not underlined 
	 */
	public function setLabel (pValue:String, pUnderline:Boolean):Void
	{
		// This removes the text field in the case we pass an empty string to the button.  This is so the text
		// field (whose autoSize=true) does not take up any space and cause the button to have unpredicted dimensions
		if (pValue == "")
		{
			mLabelTextField.removeTextField();
			setSize (mIcon._width, mIcon._height);
			return;
		}
		
		mLabel = pValue;
		var underline:String = pUnderline ? "out" : "over";
		mLabelTextField.htmlText = "<p class = 'label'><p class = '" + underline + "'>" + mLabel + "</p></p>";
	}
	
	/** 
	 * set the .label style to whatever style is passed within pCSS.  currently used by the helpButton within the 
	 * shareWindow, since it is also a MediaNavButton, but may have a different style
	 */
	public function setCSS(pCSS:Object):Void
	{
		mCSS.setStyle('.label', pCSS);
	}
	
	public function setOut(pCSS:Object):Void
	{
		mCSS.setStyle('.out', pCSS);
	}

	public function setOver(pCSS:Object):Void
	{
		mCSS.setStyle('.over', pCSS);
	}

	/** a precautionary function to make sure the icons are loaded*/
	private function onLoad():Void
	{
		setIcon(mIconLinkage);
	}	
 }