﻿import mx.transitions.easing.Regular;
import mx.transitions.easing.Strong;
import mx.transitions.Tween;
import mx.utils.Delegate;

import kaneva.events.EnabledEvent;
import kaneva.events.ItemClickEvent;
import kaneva.events.LoadEvent;
import kaneva.events.MetaDataEvent;
import kaneva.events.MouseEvent;
import kaneva.events.ResizeVideoEvent;
import kaneva.events.StatusEvent;
import kaneva.info.MetaDataInfo;
import kaneva.media.MediaAsset;
import kaneva.media.MediaType;
import kaneva.omm.CSS;
import kaneva.omm.Logger;
import kaneva.omm.OMM;
import kaneva.omm.Ticker;
import kaneva.omm.ui.buttons.FeedbackButton;
import kaneva.omm.ui.buttons.FeedbackButtonService;
import kaneva.omm.ui.buttons.KanevaLogoButton;
import kaneva.omm.ui.buttons.PlayButton;
import kaneva.omm.ui.buttons.ServiceButton;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.ImageObject;
import kaneva.omm.ui.MediaObject;
import kaneva.omm.ui.MediaPlayer;
import kaneva.omm.ui.ReportProblem;
import kaneva.omm.ui.shapes.Rect;
import kaneva.omm.ui.ShareWindow;
import kaneva.util.JavaScript;
import kaneva.util.Strings;

/**
 * The video area of the media player.  This deals with the positioning and sizing of video 
 * objects, descriptive text content, and animating the video object.
 * @author Scott
 */
class kaneva.omm.ui.MediaVideoBase extends ComponentDisplay
{
	/** The media player that houses this component */
	public var owner:MediaPlayer;
	
	/** The amount of space between each service and feedback button */
	private var mButtonGap:Number = 7;
	
	/** Media asset object for the media to load */
	private var mMediaAsset:MediaAsset;
	
	/** The base of the component */
	private var mBase:MovieClip;
	
	/** Secondary base used to transition in the text and bg image.  Also hide them when the video is big */
	private var mBase2:MovieClip;
//	
//	/** Base behind the video when it is small */
//	private var mSmallBase:MovieClip;
//	
	/** The offset amount for the small base */
	private var mSmallOffset:Number = 5;
	
	/** Object that contains all media will be loaded into.  This bad boy can handle audio, video, and image files */
	private var mMediaObj:MediaObject;
	
	/** Image loaded behind the background */
	private var mBackgroundImageObj:ImageObject;
	
	/** The image object that displays the author's profile pic */
	private var mAuthorImageObj:ImageObject;
	
	/** Used to see if the video object is resized or not */
	private var mIsMediaBig:Boolean = false;
	
	/** The percent of the width and height the video object is tweened down to when the video is tweened to the smaller size */
	private var mSmallVideoScale:Number = 0.4;
	
	/** 
	 * Factor used to keep track of the size of the video object.  This value will be used so that tweening and 
	 * run-time resizing will work together. This variable will be between 0 and 1.  Making it zero will give dimensions
	 * of 0x0, so the smallest we want it is mSmallVideoScale which is 0.4 or 40% of the max size.
	 */
	private var mVideoScale:Number;
	
	/** Tween objects used to tween the video object */
	private var mTweenVideoSize:Tween;
	
	/** Function used for tweens */
	private var mFunc:Function = Strong.easeOut;
	
	/** Time for the resizing of the video tweens */
	private var mTime:Number = 0.40;
	
	/** The text field that displays the content for the media */
	private var mContentTextField:TextField;
	
	/** The text field that displays the profile info for the author */
	private var mProfileTextField:TextField;
	
	/** The ratings icon for this particular piece of media*/
	private var mRatingMovie:MovieClip;
	
	/** The info button */
	private var mInfoButton:ServiceButton;
	
	/** Displays a Report Problem window */
	private var mReportButton:ServiceButton;
	
	/** The add button */
	private var mAddButton:ServiceButton;
	
	/** The help button */
	private var mHelpButton:ServiceButton;
	
	/** The rave it button */
	private var mRaveItButton:ServiceButton;
	
	/** displatys the number of views for the media */
	private var mViewsButton:FeedbackButton;
	
	/** displatys the number of comments for the media and allows user to comment on the media*/
	private var mCommentsButton:FeedbackButton;
	
	/** displatys the number of raves for the media and allows the user to rave on the media */
	private var mRavesButton:FeedbackButton;
	
	/** the 'page' that is displayed when mReportButton is clicked*/
	private var mReportProblem:ReportProblem;
	
	/** The depth at which we want to create the report problem component */
	private var mReportProblemDepth:Number;
	
	/** Style sheet object for the content text field */
	private var mCSS:TextField.StyleSheet;
	
	/** Used to tween the position of the video object  */
	private var mPositionFactor:Number = 0;
	
	/** Animates the video object to the correct position */
	private var mTweenPosition:Tween;
	
	/** Tween for the secondary base */
	private var mTweenAlpha:Tween;
//	
//	/** Menu that animates in when the share button is pressed */
//	private var mShareMenu:ShareMenu;
//	
//	/** Mask for the share menu */
//	private var mShareMenuMask:MovieClip;
//	
//	/** True if menu is up, false if it is down */
//	private var mIsShareMenuToggled:Boolean = false;
//	
//	/** Used to tween the share menu into position */
//	private var mShareMenuTween:Tween;
//	
	/** Tween object used to alpha fade in and out the background image */
	private var mFadeBGTween:Tween;
//	
//	/** The positions of the share menu when it is toggled and when it is not */
//	private var mShareMenuDown:Number;
//	private var mShareMenuUp:Number;
//	
	/** The pop up window that shows the share data */
	private var mShareWindow:ShareWindow;
	
	/** The depth at which to create the share window clip */
	private var mShareWindowDepth:Number;
	
	/** get details link */
	private var mDetailsLink:String = "";
//
//	/** Reference to the share button */
//	private var mShareButton:AdditionalButton;
//	
	/** The play button that shows up when the video is not playing */
	private var mPlayButton:PlayButton;
	
	/** used to determine if the initial piece of media has been loaded into the player*/
	public var isMediaLoaded:Boolean = false;
	
	/** The logo movie clip */
	private var mKanevaLogoButton:KanevaLogoButton;
	
	/** The background clip for the media and details when the media is in details mode (media is small) */
	private var mMediaDetailsBG:MovieClip;
	
	/** The background clip for the buttons on the left side */
	private var mLeftButtonsBG:MovieClip;
	
	/** The background clip for the buttons on the right side */
	private var mRightButtonsBG:MovieClip;
	
	/** The background clip for the logo clip at the bottom */
	private var mLogoBG:MovieClip;

	/** Fades in the author image */
	private var mFadeAuthorTween:Tween;
	
	/** The amount of offset from the sides of the media base that the media object has when in details mode */
	private var mOffset:Number = 10;
	
	/**
	 * Used to keep track of whether the video is being toggled from full size to details mode or vice versa.
	 * This makes sure that mVideoScale is being set by the proper method.  When video scale is being tweened,
	 * we don't want the size() method to have any affect on it.  When this variable is false, then the size 
	 * method will reset it's value so it conforms to the media details background clip
	 */
	private var mIsVideoSizeTweening:Boolean = false;
	
	/** The clip that the ticker is loaded into*/
	private var mTickerPH:MovieClip;
	
	/** The text ticker */
	private var mTicker:Ticker;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function MediaVideoBase ()
	{
		super ();
		mVideoScale = mSmallVideoScale;
	}

	/** Initialize things */
	private function init ():Void
	{
		super.init ();
		
		// Set reference to the media player which this component resides in 
		owner = MediaPlayer (this._parent);
		
		// Listen out for the status of the owner....this will get triggered when the status changes
		// to play or pause or stop.
		owner.addEventListener(StatusEvent.STATUS, Delegate.create(this, onMediaPlayerStatus));
	}
	
	/** Create movie clips and other things */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create the base, it's first so it is on bottom
		mBase = this.createEmptyMovieClip ("mBase", this.getNextHighestDepth ());
		mBase.onRelease = Delegate.create (this, resizeMedia);
		
		// Create the background video object, it is on top of the base but below the content text field
		mBackgroundImageObj = ImageObject (this.attachMovie ("ImageObject", "mBackgroundImageObj", this.getNextHighestDepth ()));
		mBackgroundImageObj.addEventListener (LoadEvent.LOAD, Delegate.create(this, onLoadBGImage));
//		mBackgroundImageObj._alpha = 30;
		
		// Create the CSS for the content text
		mCSS = CSS.getMediaVideoBaseStyle();
		
		// Create background clips
		mMediaDetailsBG = this.createEmptyMovieClip ("mMediaDetailsBG", this.getNextHighestDepth ());
		mLeftButtonsBG = this.createEmptyMovieClip ("mLeftButtonsBG", this.getNextHighestDepth ());
		mRightButtonsBG = this.createEmptyMovieClip ("mRightButtonsBG", this.getNextHighestDepth ());
		mLogoBG = this.createEmptyMovieClip ("mLogoBG", this.getNextHighestDepth ());
		
		// The alpha of the bg clips 
		var bgAlpha:Number = 40;
		
		// Draw the media details bg
		mMediaDetailsBG.clear();
		mMediaDetailsBG.beginFill(0, bgAlpha);
		Rect.draw(mMediaDetailsBG, 1, 1);
		mMediaDetailsBG.endFill();
		
		// Draw the left buttons bg
		mLeftButtonsBG.clear();
		mLeftButtonsBG.beginFill(0, bgAlpha);
		Rect.draw(mLeftButtonsBG, 1, 1);
		mLeftButtonsBG.endFill();
		
		// Draw the right buttons bg
		mRightButtonsBG.clear();
		mRightButtonsBG.beginFill(0, bgAlpha);
		Rect.draw(mRightButtonsBG, 1, 1);
		mRightButtonsBG.endFill();
		
		// Draw the logo bg
		mLogoBG.clear();
		mLogoBG.beginFill(0, bgAlpha);
		Rect.draw(mLogoBG, 1, 1);
		mLogoBG.endFill();
		
		// Create the ticker holder
		mTickerPH = this.createEmptyMovieClip("mTickerPH", this.getNextHighestDepth());
//		mTicker = Ticker(this.attachMovie("Ticker", "mTicker", this.getNextHighestDepth()));
		
		// Set a reference to the ticker on the OMM
//		OMM.setTicker(mTicker);
		
		// Create the text field that displays the content
		mContentTextField = this.createTextField ("mContentTextField", this.getNextHighestDepth (), 0, 0, 0, 0);
		
		//set the attributes of the content
//		mContentTextField.border = true;
//		mContentTextField.borderColor = 0xFFFFFF;
		mContentTextField.html = true;
		mContentTextField.multiline = true;
		mContentTextField.selectable = false;
		mContentTextField.styleSheet = mCSS;
		mContentTextField.wordWrap = true;
		
		// Create the content text field where the info about the media is displayed in the video base
		mContentTextField.htmlText = "";
		
		// Create the text field that displays the profile info for the author
		mProfileTextField = this.createTextField ("mProfileTextField", this.getNextHighestDepth (), 0, 0, 0, 0);
		
		//set the attributes of the profile
//		mProfileTextField.border = true;
//		mProfileTextField.borderColor = 0xFFFFFF;
		mProfileTextField.html = true;
		mProfileTextField.multiline = true;
		mProfileTextField.selectable = false;
		mProfileTextField.styleSheet = mCSS;
		mProfileTextField.wordWrap = true;
//		mProfileTextField.border = true;
//		mProfileTextField.borderColor = 0xFFFFFF;
		
		// Create author image object 
		mAuthorImageObj = ImageObject(this.attachMovie ("ImageObject", "mAuthorImageObj", this.getNextHighestDepth ()));
		mAuthorImageObj.addEventListener (LoadEvent.LOAD, Delegate.create(this, onLoadAuthorImage));
		mAuthorImageObj.setShowOutline(true);
		
//		// Small base behind the video when it is small.  Behind base 2 so that it is not visible when the video is big
//		mSmallBase = this.createEmptyMovieClip ("mSmallBase", this.getNextHighestDepth ());
//		
//		// Create button for the report media
		mReportButton = ServiceButton(this.attachMovie("ServiceButton", "mReportButton", this.getNextHighestDepth ()));
		mReportButton.setLabel ("Report");
		mReportButton.setIcon ("");
		mReportButton.addEventListener (MouseEvent.CLICK, Delegate.create(this, onReportClick));
			
		// Create button for the info
		mAddButton = ServiceButton(this.attachMovie("ServiceButton", "mAddButton", this.getNextHighestDepth ()));
		mAddButton.setLabel("Add");
		mAddButton.setIcon ("Add");
		mAddButton.addEventListener (MouseEvent.CLICK, Delegate.create(this, onAddClick));
		
		// Create button for the info
		mInfoButton = ServiceButton(this.attachMovie("ServiceButton", "mInfoButton", this.getNextHighestDepth ()));
		mInfoButton.setLabel("Info");
		mInfoButton.setIcon ("Info");
		mInfoButton.addEventListener (MouseEvent.CLICK, Delegate.create(this, onInfoClick));
//		mInfoButton.setCSS(CSS.getMediaNavButtonStyle().getStyle('.label'));
//		mInfoButton.setOut(CSS.getMediaNavButtonStyle().getStyle('.out'));
//		mInfoButton.setOver(CSS.getMediaNavButtonStyle().getStyle('.over'));		
		
		// Create help button
		mHelpButton = ServiceButton(this.attachMovie("ServiceButton", "mHelpButton", this.getNextHighestDepth ()));
		mHelpButton.setIcon ("Help");
		mHelpButton.setLabel("Help");
		mHelpButton.addEventListener (MouseEvent.CLICK, Delegate.create(this, onHelpClick));
		
		// Create help button
		mRaveItButton = ServiceButton(this.attachMovie("ServiceButton", "mRaveItButton", this.getNextHighestDepth ()));
		mRaveItButton.setIcon ("RaveIt");
		mRaveItButton.setLabel("Rave It");
		mRaveItButton.addEventListener (MouseEvent.CLICK, Delegate.create(this, onRaveItClick));
		
		// Create button for raves
		mRavesButton = FeedbackButton(this.attachMovie("FeedbackButton", "mRavesButton", this.getNextHighestDepth ()));
		mRavesButton.setLabel ("Raves");
//		mRavesButton.onRelease = Delegate.create(this, onRavesRelease);
		
		// Create button for comments
		mCommentsButton = FeedbackButton(this.attachMovie("FeedbackButton", "mCommentsButton", this.getNextHighestDepth ()));
		mCommentsButton.setLabel ("Comments");
		mCommentsButton.onRelease = Delegate.create(this, onCommentsRelease);
		
		// Create button for views
		mViewsButton = FeedbackButton(this.attachMovie("FeedbackButton", "mViewsButton", this.getNextHighestDepth ()));
		mViewsButton.setLabel ("Views");
//		mViewsButton.enabled = false;
		
		// clip that the rating graphic is loaded into
		mRatingMovie = this.createEmptyMovieClip ("mRatingMovie", this.getNextHighestDepth ());
		mRatingMovie.cacheAsBitmap = true;
		
		// Base covers everything except the top video.....this base will tween it's alpha for transition effect
		mBase2 = this.createEmptyMovieClip ("mBase2", this.getNextHighestDepth ());

		// Create the media object
		mMediaObj = MediaObject (this.attachMovie ("MediaObject", "mMediaObj", this.getNextHighestDepth ()));
		mMediaObj.addEventListener (MetaDataEvent.META_DATA, Delegate.create (this, onMetaData));
		mMediaObj.addEventListener (LoadEvent.LOAD, Delegate.create (this, onImageLoad));
//		mMediaObj.addEventListener ("imageLoad", Delegate.create (this, onImageLoad));
		
		// Create the play button that shows up over the video and image objects.  This is used to play the video or audio
		mPlayButton = PlayButton(this.attachMovie ("PlayButton", "mPlayButton", this.getNextHighestDepth ()));
		mPlayButton.addEventListener (MouseEvent.CLICK, Delegate.create (this, onPlayButtonClick));
		
		// Create the kaneva logo that superimposes media
		mKanevaLogoButton = KanevaLogoButton(this.attachMovie("KanevaLogoButton", "mKanevaLogoButton", this.getNextHighestDepth()));
		mKanevaLogoButton._alpha = 60;
		
		// If the media is big, then we want to hide the logo
//		setLogoVisibility(mIsMediaBig);
		
		// Set the release event on the logo so it links to something
		mKanevaLogoButton.addEventListener(MouseEvent.CLICK, Delegate.create(this, onKanevaLogoClick));
//		
//		// Create the share menu
//		mShareMenu = ShareMenu (this.attachMovie ("ShareMenu", "mShareMenu", this.getNextHighestDepth ()));
//		setShareLinks();
//		
//		// Set the listener for the share menu
//		mShareMenu.addEventListener (MouseEvent.CLICK, Delegate.create (this, onShareMenuItemClick));
//		
//		// Create and set the mask for the share menu
//		mShareMenuMask = this.createEmptyMovieClip ("mShareMenuMask", this.getNextHighestDepth ());
//		mShareMenu.setMask (mShareMenuMask);
//		
		// Set the depth for the share window and report problem window
		mShareWindowDepth = mReportProblemDepth = this.getNextHighestDepth ();
		
	}
	
	/** Draws pieces and stuff */
	private function draw ():Void
	{
		super.draw ();
		
		// Draw the base
		mBase.clear ();
		mBase.beginFill (0x333333, 100);
		Rect.draw (mBase, mWidth, mHeight);
		mBase.endFill ();
		
		// Draw the secondary base
		mBase2.clear ();
		mBase2.beginFill (0x333333, 100);
		Rect.draw (mBase2, mWidth, mHeight);
		mBase2.endFill ();
//		
//		// Draw the mask for the share menu
//		mShareMenuMask.clear ();
//		mShareMenuMask.beginFill (0, 10);
//		Rect.draw (mShareMenuMask, mWidth, mHeight);
//		mShareMenuMask.endFill ();
	}
	
	/** Sizes pieces and stuff */
	private function size ():Void
	{
		super.size ();
		
		// Position the logo
		mKanevaLogoButton._x = mWidth - mKanevaLogoButton.width - 5;
		mKanevaLogoButton._y = mHeight - mKanevaLogoButton.height - 5;
		
		// Set the size of the media object
		setProportionalSize ();
		
		// Size the background video object
		mBackgroundImageObj.setSize (mWidth, mHeight);		
		
		// Set the position of the media object
		setMediaPosition();
		
		// Set play button position and size
//		var dimension:Number = Math.min(mMediaObj.width, mMediaObj.height) / 2;
//		mPlayButton.setSize (dimension, dimension);
		mPlayButton._x = mMediaObj._x + (mMediaObj.width - mPlayButton.width) / 2;
		mPlayButton._y = mMediaObj._y + (mMediaObj.height - mPlayButton.height) / 2;
//		
//		// Set the share menu positions
//		mShareMenuDown = mHeight + 1;
//		mShareMenuUp = mHeight - mShareMenu.height;
//		
//		// Position the share menu
//		mShareMenu._x = 80;
//		mShareMenu._y = mIsShareMenuToggled ? mShareMenuUp : mShareMenuDown;
//		
		
		// Position clips if in detail mode
		if(!mIsMediaBig)
		{
			setPiecesAndParts();			
		}
		
		// If report problem exists...
		if (mReportProblem)
		{
			// Position and size the report problem window
			mReportProblem.setSize (mWidth - 2*mSmallOffset, mHeight - 2*mSmallOffset);
			mReportProblem._x = mReportProblem._y = mSmallOffset;
		}
		
		// If the share window exists....
		if (mShareWindow)
		{
			// Position and size the share window
			mShareWindow.setSize (mWidth - 2*mSmallOffset, mHeight - 2*mSmallOffset);
			mShareWindow._x = mShareWindow._y = mSmallOffset;
		}
	}
	
	/** 
	 * Invoked when the media object dipatches a meta event
	 * @param pEvent An object containing one property for each metadata item
	 */
	private function onMetaData (pEvent:MetaDataEvent):Void
	{
		var info:MetaDataInfo = pEvent.info;
//		
//		var metaDataEvent:MetaDataEvent = new MetaDataEvent();
//		metaDataEvent.type = MetaDataEvent.META_DATA;
//		metaDataEvent.info = info;
//		dispatchEvent (metaDataEvent);
			
//		dispatchEvent ({type:"meta", info:info});
		
		// Dispatch the object that was heard by the listener...sort of a relay race. This class gets
		// it, uses it, and passes it along so someone else can use it.
		dispatchEvent(pEvent);
		
		// Resize the video object so it conforms to it's appropriate aspect ratio
		size ();
	}
	
	/**
	 * Set the visibility of the logo
	 * @param pVisible True if the logo is to be visible
	 */
	private function setLogoVisibility(pVisible:Boolean):Void
	{
		mKanevaLogoButton._visible = pVisible;
	}
	
	/** Resizes the media when the button is pressed */
	public function resizeMedia():Void
	{
		// Set this to true so that mVideoScale does not get reset when the size method is called.  We want 
		// the tween to have control of this variable so it sets it smoothly.
		mIsVideoSizeTweening = true;
		
		// If the video is big property is true....
		if (mIsMediaBig)
		{
			// Set the video is big property to false, resize the video/image, and fade the details in
			mIsMediaBig = false;
			animateMediaSizeAndPosition ();
			mTweenPosition.onMotionFinished = Delegate.create (this, fadeDetails);
		}
		else
		{
			// Otherwise, set the video is big property to true, fade the details out, and resize the video/image
			mIsMediaBig = true;
			fadeDetails ();
			mTweenAlpha.onMotionFinished = Delegate.create (this, animateMediaSizeAndPosition);
		}
		
		// Dispatch a video resize event
		var resizeVideoEvent:ResizeVideoEvent = new ResizeVideoEvent();
		resizeVideoEvent.type = ResizeVideoEvent.RESIZE;
		resizeVideoEvent.size = mIsMediaBig ? ResizeVideoEvent.DETAILS : ResizeVideoEvent.VIDEO;
		dispatchEvent(resizeVideoEvent);
		
		// If the media is not big or it is big but not playing, then we want to show the logo.....otherwise it will be hidden
		setLogoVisibility(!mIsMediaBig || (mIsMediaBig && owner.status != MediaPlayer.PLAYING));
	}
	
	/** 
	 * Initially sets the size of the media (video/image) to be big.  This allows us to instantly size the media
	 * rather than having to tween it to size.
	 * @param pValue True if we want it big, false to make small
	 */
	public function setInitMediaSize (pValue:Boolean):Void
	{
		mIsMediaBig = pValue;
		mVideoScale = pValue ? 1 : mSmallVideoScale;
		mPositionFactor = pValue ? 1 : 0;
		
		// If the media is big, then we want to hide the logo
		setLogoVisibility(mIsMediaBig);
		
		size ();
	}
	
	/** 
	 * Resizes and positions the video object.  This also resizes the image object by using the motion changed 
	 * event to trigger the size method.  The size method is what resizes both the image object and video object
	 */
	private function animateMediaSizeAndPosition ():Void
	{
		setPiecesAndParts();
		
		// Reset the small video scale
		mSmallVideoScale = (mMediaDetailsBG._height - 2 * mOffset) / mHeight;
		
		// Video size
		var vidSize:Number = mIsMediaBig ? 1 : mSmallVideoScale;
		
		// Position factor
		var posFac:Number = mIsMediaBig ? 1 : 0;
		
		// Tween that sizes the video object
		mTweenVideoSize = new Tween (this, "mVideoScale", mFunc, mVideoScale, vidSize, mTime, true);
		mTweenVideoSize.onMotionChanged = Delegate.create (this, size);
		mTweenVideoSize.onMotionFinished = Delegate.create(this, onVideoSizeFinished);
		
		// Tween that positions the video object
		mTweenPosition = new Tween (this, "mPositionFactor", mFunc, mPositionFactor, posFac, mTime, true);
		mTweenPosition.onMotionChanged = Delegate.create (this, size);
	}
	
	/** Tweens the base2 clip to hide the details */
	public function fadeDetails ():Void
	{
		var alpha:Number = mIsMediaBig ? 100 : 0;
		mTweenAlpha = new Tween (mBase2, "_alpha", mFunc, mBase2._alpha, alpha, mTime, true);
		
		// If media is big, set base 2 release to that of regular base.  This is so buttons below are not clickable
		// but it still resizes media.  Otherwise, delete the release event so details buttons can be clicked
		if(mIsMediaBig)
			mBase2.onRelease = mBase.onRelease;
		else
			delete mBase2.onRelease;
	}
	
	/** Load the background image */
	public function loadBackgroundImage ():Void
	{
		// Fade the background image object out
		fadeOutBG ();
		mFadeBGTween.onMotionFinished = Delegate.create(this, onFadeOut);
	}
	
	/**
	 * Load the user thumbnail image
	 */
	private function loadUserThumbImage():Void
	{
		var image:String = mMediaAsset.userThumb;
		var url:String = mMediaAsset.userUrl;
		mAuthorImageObj.load(image);
		
		// Set release event to open up specified url if one is set for the user
		if(url)
			mAuthorImageObj.onRelease = Delegate.create(this, setAuthorThumbLink);
		else
			delete mAuthorImageObj.onRelease;
	}
	
	/**
	 * Open link to the author url
	 */
	private function setAuthorThumbLink():Void
	{
		JavaScript.openWindow(mMediaAsset.userUrl);
	}
	
	/** Fades out the back ground image...this is used when we don't want to load a background image.....like mp3's and images */
	private function fadeOutBG ():Void
	{
		mFadeBGTween = new Tween (mBackgroundImageObj, "_alpha", Regular.easeOut, mBackgroundImageObj._alpha, 0, 0.75, true);
	}
	
	/** Method triggerd at the end of the fade out tween */
	private function onFadeOut ():Void
	{
		delete mFadeBGTween.onMotionFinished;
		mBackgroundImageObj.load (mMediaAsset.thumb);
	}
	
	/** Fade the background image object in once it has loaded */
	private function onLoadBGImage ():Void
	{ 
		mFadeBGTween = new Tween (mBackgroundImageObj, "_alpha", Regular.easeOut, mBackgroundImageObj._alpha, 30, 0.25, true);
	}
	
	/** Fade the user/author image object in once it has loaded */
	private function onLoadAuthorImage ():Void
	{ 
		mFadeAuthorTween = new Tween (mAuthorImageObj, "_alpha", Regular.easeOut, mAuthorImageObj._alpha, 100, 0.25, true);
	}
	
	/**
	 * Loads the media.  If the media is video (.flv), then this method loads the media in both the controlable video object 
	 * and the background video object.  If it's audio (.mp3), then it gets loaded into the audio object
	 * @param pMedia Object that contains all info about media to load
	 * @param pPlay False prevents download of media initially, otherwise it will start to load
	 * @param pDetailMode param set by a config parameter sent to the omm
	 */
	public function load (pMedia:MediaAsset, pPlay:Boolean, pDetailMode:Boolean, pEnableBase:Boolean):Void
	{
		// Set the media asset
		mMediaAsset = pMedia;
		
		// if any popups are open close them before loading a movie
		removeReportOrSharePopUps();
		
		// Load up the user thumbnail image
		loadUserThumbImage();
		
		// If not specified to initially load, then don't load the media.  When it's video, this loads a thumbnail preview of the video
		mMediaObj.load (mMediaAsset, pPlay);
		isMediaLoaded = pPlay;
		
		switch (mMediaAsset.type)
		{
			// If it's video.....
			case MediaType.FLV :

				// Set secondary base back to opaque so it can transition the content info back in for the next video
//				mBase2._alpha = 100;

				loadBackgroundImage ();
				
				// We need to be able to resize the video
				enableResize(pEnableBase);
				
				break;
				
			// If it's audio.....
			case MediaType.MP3 :
			
				// Don't want to show the bg image
				fadeOutBG ();
				
				// We don't want to be able to resize the image object when an mp3 is loaded
				enableResize(false);
				
				break;
			
			// If it's an image.....
			case MediaType.IMAGE :
			
				// Don't want to show the bg image
				fadeOutBG ();
				
				// Don't want the play button to show up
				togglePlayButtonVisible (false);
				
				break;
		}
		
		// If detail mode is false, then we want the video to be big initially.  This should only happen once when the media 
		// player is first loaded up.  This bypasses the animation and goes straight to big size
		if (pDetailMode == false)
			setInitMediaSize(true);
		else
			fadeDetails ();
//		
//		// Sets the share links for a particular piece of media
//		setShareLinks();
	}
	
	/** 
	 * Method triggered when the image object finished loading an image 
	 * @param pEvent The event object passed by the dispatcher
	 */
	private function onImageLoad (pEvent:LoadEvent):Void
	{
		// Needs to be sized proportionally
		size ();
		
		// If the image is big and it's associated with an mp3....then make it small
		if (mIsMediaBig && mMediaAsset.type == MediaType.MP3)
			resizeMedia ();
/*		else
		{
			// Otherwise, fade in the details
//			mBase2._alpha = 100;
			fadeDetails ();
		}
*/
	}
	
	/**
	 * Sets the content text field with the info passed in on the media asset object
	 * @param pMediaAsset Object that contains all info about the media being displayed
	 */
	public function setTextField(pMediaAsset:MediaAsset):Void
	{
		// Set the user name (and link if one is specified)
		var user:String = pMediaAsset.userUrl ? "<a href=\"" + pMediaAsset.userUrl + "\">" + pMediaAsset.user + "</a>" : pMediaAsset.user;
		
		// Set the content text field
		mContentTextField.htmlText = "<textformat leading='1'>" + 
			"<p class='body'><p class='content'>" +
			"<p class='title'>" + pMediaAsset.title + "</p>" +
//			"by " + "<p class='user'>" + user + "</p>" + 
//			"<b>Runtime:</b> " + pMediaAsset.length +
//			"<br><br>" + 
			"<p>" + pMediaAsset.descriptionValue + "</p>" + 
			"</p>" + 
			"</textformat>";
			
		// Set the profile text
		var imageTag:String = "<img src='RedArrow' align='right' hspace='3'/>";
		mProfileTextField.htmlText = "<textformat leading='5'>" + 
		"<p class='body'><p class='profile'>" +
		"<span class='yellow'>Shared by:</span> <p class='user'>" + user + "</p><br>" +
		imageTag + "<a href=\"" + pMediaAsset.userUrl + "\">Visit my profile page</a><br>" +
/*		imageTag + "<a href=\"#\">Meet me 3D</a><br>" +
		imageTag + "<a href=\"#\">View my virtual apartment</a><br>" + */
		"</p></p>" + 
		"</textformat>";
		
//		setViewText(pMediaAsset.views);
	}
	
	/** 
	 * Sets the rating icon displayed for the current media
	 * @param pRating The linkage id of the rating graphic to load
	 */
	public function setRatings(pMature:Boolean):Void
	{
		// load rating graphic
		if(pMature == true)
		{
			var rating:MovieClip = mRatingMovie.attachMovie ("Restricted", "rating", 0);
			mRatingMovie._visible = true;
		}
		else
			mRatingMovie._visible = false;
	}
	
	/** Displays or hides the Report a Problem frame*/
	private function onReportClick():Void
	{
		// set mbase to false to turn off all mouse events behind the created popup
		enableResize(false);
		createReportProblem ();
	}
//	
//	/** 
//	 * Method called when the submit button is pressed in the report problem clip 
//	 * @param pEvent Contains info about the report.  Object called 'info' contains 3 properties (email, error, notes)
//	 */
//	private function onSubmit (pEvent:Object):Void
//	{
//		
//	}
//
//	/** Method called when the cancel or submit button is pressed in the report problem clip  */
//	private function onCancel ():Void
//	{
//		// set mbase to true to register all mouse events on the base now that the popup is gone
//		enableResize(true);
//	}
//	
	/** 
	 * Method called when the cancel or submit button is pressed in the report problem clip
	 * @param pEvent Object passed by dispatcher
	 */
	private function onItemClick (pEvent:ItemClickEvent):Void
	{
		var label:String = pEvent.label;
		
		// If cancel button was pressed, set mbase to true to register all mouse events on the base now that the popup is gone
		if(label == "cancel")
			enableResize(true);
	}
	
	/** Creates the report problem clip */
	private function createReportProblem ():Void
	{
		mReportProblem = ReportProblem(this.attachMovie ("ReportProblem", "mReportProblem", mReportProblemDepth));
		mReportProblem.addEventListener (ItemClickEvent.ITEM_CLICK, Delegate.create (this, onItemClick));
//		mReportProblem.addEventListener ("submit", Delegate.create (this, onSubmit));
//		mReportProblem.addEventListener ("cancel", Delegate.create (this, onCancel));
		size ();
	}
//	
//	/** On click of the share button */
//	private function onShareButtonClick (pEvent:Object):Void
//	{
//		// if either the report problem, or the share/embed popup are open, do not open the share menu
//		// we check the depth of these movieclips because that is the only way to determine if they are loaded
//		if(!mReportProblem.getDepth () && !mShareWindow.getDepth ())
//		{
//			// Close the menu if it is toggled open, otherwise open it
//			if(mIsShareMenuToggled)
//				closeShareMenu();
//			else
//				openShareMenu();
//			
//			// Set the mouse up event to close the menu only if the menu is currently toggled.  This makes the menu close if
//			// the user clicks outside of the menu
//			mShareMenu.onMouseUp = mIsShareMenuToggled ? Delegate.create(this, closeShareMenu) : null;
//		}
//	}
//
//	/** Animates the opening of the share menu */
//	private function openShareMenu ():Void
//	{
//		mShareMenuTween = new Tween (mShareMenu, "_y", mFunc, mShareMenu._y, mShareMenuUp, 0.5, true);
//		mIsShareMenuToggled = true;
//		mShareButton.enabled = false;
//	}
//	
//	/** Animates the closing of the share menu */
//	private function closeShareMenu ():Void
//	{
//		mShareMenuTween = new Tween (mShareMenu, "_y", mFunc, mShareMenu._y, mShareMenuDown, 0.5, true);
//		mIsShareMenuToggled = false;
//		mShareButton.enabled = true;
//		mShareButton.onRollOut ();
//	}
//	
//	/**
//	 * Triggered once a share menu item is clicked
//	 * @param pEvent The event object passed by the dispatcher
//	 */
//	private function onShareMenuItemClick (pEvent:Object):Void
//	{
//		var link:String = pEvent.link;
//
//		// Get rid of the mouse up event 
//		mShareMenu.onMouseUp = null;
//		
//		// If the link is specified...then get the url in a new window
//		if (link)
//		{
//			// If the button pressed was blog it.....
//			if(pEvent.target.getText().toLowerCase() == "embed it")
//			{
//				createShareBlogWindow(link);
//			}
//			else
//			{
//				Logger.logMsg( "share link: " + link );
//				JavaScript.openWindow(link, "_blank");
//			}
//		}
//			
//		// Close the share menu
//		closeShareMenu();
//	}
//	
	/** 
	 * Method that opens the share window and displays the share data 
	 * @param pLink The link to send the request to in order to get back need info to show in the share window
	 */
	private function createShareBlogWindow (pLink:String):Void
	{
		// disable the mouse events on mBase while the popup is within view
		enableResize(false);
						
		mShareWindow = ShareWindow (this.attachMovie ("ShareWindow", "mShareWindow", mShareWindowDepth));
		mShareWindow.setLink (pLink);
		mShareWindow.addEventListener(ItemClickEvent.ITEM_CLICK, Delegate.create (this, onItemClick));
		
		size();
	}
	
	/** Method triggered by the get add button */
	private function onAddClick ():Void
	{
		// Disable the button so it shows it's been raved
		/*mRaveItButton.setIcon("RaveItDisabled");
		mRaveItButton.setEnabled(false);
		mRaveItButton.onRollOut();*/
		
		var fbs:FeedbackButtonService = new FeedbackButtonService();
		fbs.setButton(mAddButton);
		fbs.clicked("uri.button.add");
		
		
		/*var addUri:String = OMM.getConfig().getAsString("uri.button.add");
		var assetID:String = String(OMM.getMedia().id);
		addUri = Strings.replace(addUri,"{0}", assetID);
		JavaScript.openWindow(addUri, "_self");*/
	}
	
	/** Method triggered by the get details button */
	private function onInfoClick ():Void
	{
		var detailUri:String = OMM.getConfig().getAsString("uri.link.details");
		mDetailsLink = Strings.replace(detailUri,"{0}", String(OMM.getMedia().id));
		Logger.logMsg( "setDetails url: " + mDetailsLink );
		JavaScript.openWindow(mDetailsLink);
	}
	
	/** Method triggered when the help icon is clicked */
	private function onHelpClick ():Void
	{
		var helpUri:String = OMM.getConfig().getAsString("uri.link.help");
		Logger.logMsg( "setHelp url: " + helpUri );
		JavaScript.openWindow(helpUri, "_blank");		
	}
	
	/**
	 * Method triggered when the rave it button is clicked
	 */
	private function onRaveItClick():Void 
	{
		// Disable the button so it shows it's been raved
		mRaveItButton.setIcon("RaveItDisabled");
		mRaveItButton.setEnabled(false);
		mRaveItButton.onRollOut();
		
//		var raveItUri:String = OMM.getConfig().getAsString("uri.button.raveIt");
//		Logger.logMsg( "setRaveIt url: " + raveItUri );
//		JavaScript.openWindow(raveItUri, "_blank");
		
		var fbs:FeedbackButtonService = new FeedbackButtonService();
		fbs.setButton(getRaveItButton());
		fbs.clicked("uri.button.raveIt");
	}
	
//	
//	/** sets the links within the shareMenu*/
//	private function setShareLinks():Void
//	{
//		var diggUri:String = OMM.getConfig().getAsString("uri.link.digg");
//		var deliciousUri:String = OMM.getConfig().getAsString("uri.link.delicious");
//		var blogUri:String = OMM.getConfig().getAsString("uri.link.blog");
//		var emailUri:String = OMM.getConfig().getAsString("uri.link.email");
//
//		// jmw added next two lines since otherwise mDetailsLink empty string
//		var detailUri:String = OMM.getConfig().getAsString("uri.link.details");
//		mDetailsLink = Strings.replace(detailUri,"{0}", String(OMM.getMedia().id));
//		
////		Logger.logMsg( ">>>>>>> " + diggUri + "   " +  mDetailsLink );
//		mShareMenu.setDataProvider ([{text:"Embed it", icon:"BlogIt", link:Strings.replace(blogUri, "{0}", String(OMM.getMedia().id))},
//									{text:"Email it", icon:"Email", link:Strings.replace(emailUri, "{0}", String(OMM.getMedia().id))},
//									{text:"Digg it", icon:"Digg", link:Strings.replace(diggUri, "{0}", mDetailsLink)},
//									{text:"Del.icio.us it", icon:"Delicious", link:Strings.replace(deliciousUri, "{0}", mDetailsLink)}]);
//	}
//	
	/** Sizes the specified object so it retains it's original aspect ratio (no distortion) */
	private function setProportionalSize ():Void
	{
		// Figure out the proportion factor for each dimension
		var propWidth:Number = mMediaObj.origWidth / mWidth;
		var propHeight:Number = mMediaObj.origHeight / mHeight;
		
		// See which dimension proportion is greater and adjust the other dimension accordingly so 
		// it stays proportional with the original dimensions of the video
		if (propWidth > propHeight)
		{
			var newHeight:Number = mWidth * mMediaObj.origHeight / mMediaObj.origWidth;
			mMediaObj.setSize (mWidth * mVideoScale, newHeight * mVideoScale);
		}
		else if (propWidth < propHeight)
		{
			var newWidth:Number = mHeight * mMediaObj.origWidth / mMediaObj.origHeight;
			mMediaObj.setSize (newWidth * mVideoScale, mHeight * mVideoScale);
		}
		else
		{
			mMediaObj.setSize (mWidth * mVideoScale, mHeight * mVideoScale);
		}
	}
	
	/** set the position of media within the SmallBase*/
	private function setMediaPosition():Void
	{
		// The position factor is the variable which gets tweened. We have to use this in order for media resizing
		// and Flash player resizing to work together properly
		var factor:Number = 1 - mPositionFactor;
		
		// Position of the media
		var xPos:Number = mOffset * factor;
		var yPos:Number = mOffset * factor;
//		var borderOffset:Number = 2; // Border on the object...this makes video line up with the small base horizontally
//		var xPos:Number = (((mSmallBase._x + mSmallBase._width - mMediaObj.width) / 2) + borderOffset) * factor;
//		var yPos:Number = (mSmallBase._y + mSmallOffset) * factor;
		
		// Figure out the proportion factor for each dimension
		var propWidth:Number = mMediaObj.origWidth / mWidth;
		var propHeight:Number = mMediaObj.origHeight / mHeight;
		
		// If proportional width is bigger, then the left and right sides touch the edge, so we have to figure out where to position the y so it is centered
		// Otherwise, the proportional height is bigger and the top and bottom sides touch the edge, so we have to figure out where to position the x so it is centered
		if (propWidth > propHeight)
		{
			var newY:Number = (mHeight - mMediaObj.height) / 2;
			mMediaObj._x = xPos;
			mMediaObj._y = newY * mPositionFactor + yPos;
		}
		else
		{
			var newX:Number = (mWidth - mMediaObj.width) / 2;
			mMediaObj._x = newX * mPositionFactor + xPos;
			mMediaObj._y = yPos;
		}
	}
	
	/** 
	 * Set the positions of everything within the videobase, based on the x,y coordinates passed
	 * @param pAlternateWidth If specified, use this value rather than the pObj.width
	 */
	private function setFieldPositions (pAlternateWidth:Number):Void
	{
//		// Size and position the content text field
//		mContentTextField._x = mSmallBase._x + mSmallBase._width;
//		mContentTextField._y = mSmallBase._y;
//		mContentTextField._width = mWidth - mContentTextField._x;
//		mContentTextField._height = mHeight - mSmallBase._y - 1;
//		
//		// Position the flag button
//		mViewsButton._x = mSmallBase._x + mSmallOffset;
//		mViewsButton._y = mMediaObj.height + mSmallBase._y + 2*mSmallOffset;
//		
//		// Position the details button
//		mInfoButton._x = mViewsButton._x;
//		mInfoButton._y = mViewsButton._y + mViewsButton.height + mSmallOffset;
//		
//		// Position the views button
//		mReportButton._x = mInfoButton._x;
//		mReportButton._y = mInfoButton._y + mInfoButton.height + mSmallOffset;
//		
		// If report problem exists...
		if (mReportProblem)
		{
			// Position and size the report problem window
			//mReportProblem._x = mContentTextField._x + 2*mSmallOffset;
			//mReportProblem._y = mSmallBase._y;
			//mReportProblem.setSize(mContentTextField._width - 18 , mContentTextField._height); // When offsetting the width of the problem clip, take into consideration it's position is offset, hence the -18			
			mReportProblem.setSize (mWidth - 2*mSmallOffset, mHeight - 2*mSmallOffset);
			mReportProblem._x = mReportProblem._y = mSmallOffset;
		}
		
		// If the share window exists....
		if (mShareWindow)
		{
			// Position and size the share window
			mShareWindow.setSize (mWidth - 2*mSmallOffset, mHeight - 2*mSmallOffset);
			mShareWindow._x = mShareWindow._y = mSmallOffset;
		}
		
//		// Size and position the rating graphic
//		var ratingWidth:Number = pAlternateWidth ? pAlternateWidth : mMediaObj.width;
//		setRatingPosition (mMediaObj._x, ratingWidth);		
		
	}
	
	/** Method triggered when the play button over the video/image object is clicked */
	private function onPlayButtonClick ():Void
	{
		// Start playing the media
//		owner.play ();
		owner.onPlayPauseClick();
	}
	
	/** Resize the video object depending on the situation */
	public function toggleMediaSize ():Void
	{
		// If the video is small and its a video that's playing....resize it
		if (!mIsMediaBig && mMediaAsset.type == MediaType.FLV)
			resizeMedia ();
	}
	
	/** 
	 * Sets the visibility of the play button that is over the video / image
	 * @param pValue True shows the button, false hides it
	 */
	public function togglePlayButtonVisible (pValue:Boolean):Void
	{
		mPlayButton._visible = pValue;
	}
//	
//	/** Invoked when the movie clip is instantiated and appears in the timeline */
//	private function onLoad ():Void
//	{
//		// Get a reference to the share button
//		var buttonBase:MediaButtonBase = owner.buttonBase;
////		mShareButton = buttonBase.shareButton;
//		mShareButton.addEventListener ("click", Delegate.create (this, onShareButtonClick));
//	}

	/** checks to see if the report problem or the share popups are visible (have a depth value).  is so, remove the clips*/
	private function removeReportOrSharePopUps():Void
	{
		if(mReportProblem.getDepth())
			mReportProblem.removeMovieClip ();
		if(mShareWindow.getDepth())
			mShareWindow.removeMovieClip ();
			
		// set the base back to true inorder to begin receiving mouse triggers again
		enableResize(true);
	}
	
	/** @returns The media object */
	public function getMediaObject ():MediaObject
	{
		return mMediaObj;
	}
	
	/** Triggered by clicking on the kaneva logo.  Opens a link to kaneva.com */
	private function onKanevaLogoClick():Void
	{
		var logoUri:String = OMM.getConfig().getAsString("uri.link.media.base.logo");
		JavaScript.openWindow(logoUri, "_blank");
	}
	
	/**
	 * Sets the enabled property for the base....which is what resizes the media.  Then it
	 * dispatches an enabled event so listeners can know the enabled status of the media
	 * @param pResizable Boolean, whether or not media is resizable
	 */
	private function enableResize(pResizable:Boolean):Void
	{
		mBase.enabled = pResizable;
		
		var event:EnabledEvent = new EnabledEvent();
		event.type = EnabledEvent.ENABLED;
		event.value = pResizable;
		dispatchEvent(event);
	}
	
	/**
	 * The method triggerd on release of the raves feedback button
	 */
	private function onRavesRelease():Void
	{
		var raveUri:String = OMM.getConfig().getAsString("uri.link.digg");
		var assetID:String = String(OMM.getMedia().id);
		raveUri = Strings.replace(raveUri,"{0}", assetID);
		JavaScript.openWindow(raveUri, "_self");
		
		/*var fbs:FeedbackButtonService = new FeedbackButtonService();
		fbs.setButton(getRaveButton());
		fbs.clicked("uri.button.raveIt");*/
	}
	
	/**
	 * The method triggerd on release of the comments feedback button
	 */
	private function onCommentsRelease():Void
	{
//		var fbs:FeedbackButtonService = new FeedbackButtonService();
//		fbs.setButton(getCommentsButton());
//		fbs.clicked("uri.button.comment");
	}
	
	/** @return The rave button reference */
	public function getRaveButton():FeedbackButton
	{
		return mRavesButton;
	}
	
	/** @return The raveit button reference */
	public function getRaveItButton():ServiceButton
	{
		return mRaveItButton;
	}
	
	/** @return The views button reference */
	public function getViewsButton():FeedbackButton
	{
		return mViewsButton;
	}
	
	/** @return The comments button reference */
	public function getCommentsButton():FeedbackButton
	{
		return mCommentsButton;
	}
	
	/**
	 * Triggered when the media player dispatches a status event.  This happens when media status changes
	 * to a different status like playing, paused, or stopped
	 * @param pEvent Status event passed by the dispatching object
	 */
	private function onMediaPlayerStatus(pEvent:StatusEvent):Void 
	{
		var code:String = pEvent.code;
		var logoVisibility:Boolean = code == MediaPlayer.PLAYING ? false : true;
		setLogoVisibility(logoVisibility);
	}
	
	/**
	 * Set all the positioning and sizing for the stuff that is shown in the details mode
	 */
	private function setPiecesAndParts():Void 
	{
		// The space between the background clips
		var gap:Number = 2;
		
		// Size and position the logo background at the bottom
		mLogoBG._width = mWidth;
		mLogoBG._height = mKanevaLogoButton.height + 10;
		mLogoBG._y = mHeight - mLogoBG._height;
		
		// Position the report button
		mReportButton._x = gap;
		mReportButton._y = mLogoBG._y - mReportButton.height;
		
		// Position the ticker place holder
		mTickerPH._y = mLogoBG._y;
		
		// Size the ticker
		mTicker.setSize(mKanevaLogoButton._x - mTicker._x - 5, mLogoBG._height);
		
		// Gap between the author image object and the sides
		var space:Number = 5;
		
		// The largest height the author image object will be
		var height:Number = 75;
		
		// The fraction of the stage height to check for.
		var fraction:Number = 1/7;
		
		// If the height is greater than the fraction of the stage height.......the reset it to the fraction of it
		height = height > mHeight * fraction ? mHeight * fraction : height;
		
		// Set the width 
		var width:Number = height * 4/3;
		
		// Size and position the media object that shows the author's pic
		mAuthorImageObj.setSize(width, height);
		mAuthorImageObj._x = mWidth - mAuthorImageObj.width - space;
		mAuthorImageObj._y = mLogoBG._y - mAuthorImageObj.height - space; // mRightButtonsBG._y + mRightButtonsBG._height + space/2;
		
		// Size and position the profile text field
		mProfileTextField._x = 0;
		mProfileTextField._y = mAuthorImageObj._y;
		mProfileTextField._width = mAuthorImageObj._x;
		mProfileTextField._height = mAuthorImageObj.height;
		
		// Size and position the background clip for the left buttons
		mLeftButtonsBG._y = mAuthorImageObj._y - mLeftButtonsBG._height - space; // mMediaDetailsBG._height + gap;
		mLeftButtonsBG._width = mInfoButton.width + mAddButton.width + mHelpButton.width + mRaveItButton.width + 5*mButtonGap;
		mLeftButtonsBG._height = mInfoButton.height + mButtonGap;
		
		// Size and position the background clip for the right buttons
		mRightButtonsBG._x = mLeftButtonsBG._x + mLeftButtonsBG._width + gap;
		mRightButtonsBG._y = mLeftButtonsBG._y;
		mRightButtonsBG._width = mWidth - (mLeftButtonsBG._x + mLeftButtonsBG._width + gap);
		mRightButtonsBG._height = mLeftButtonsBG._height;
		
		// Size and position the background clip for the media object and the description
		mMediaDetailsBG._width = mWidth;
		mMediaDetailsBG._height = mLeftButtonsBG._y - gap; // mMediaObj._y + mMediaObj.height + 10;
		
		// Size and position the rating clip
		mRatingMovie._x = mWidth - mRatingMovie._width - 10;
		mRatingMovie._y = 10;
		
		// Size and position the content text field
		mContentTextField._x = mMediaObj._x + mMediaObj.width;
		mContentTextField._y = mMediaObj._y;
		mContentTextField._width = mWidth - mContentTextField._x;
		mContentTextField._height = mMediaDetailsBG._height - mMediaObj._y; // mMediaObj.height;
		
		// Size and position the add button
		mAddButton._x = mButtonGap;
		mAddButton._y = mLeftButtonsBG._y + (mLeftButtonsBG._height - mAddButton.height)/2;
		
		// Size and position the info button
		mInfoButton._x = mAddButton._x + mAddButton.width + mButtonGap;
		mInfoButton._y = mAddButton._y;
		
		// Size and position the help button
		mHelpButton._x = mInfoButton._x + mInfoButton.width + mButtonGap;
		mHelpButton._y = mAddButton._y;
		
		// Size and position the rave it button
		mRaveItButton._x = mHelpButton._x + mHelpButton.width + mButtonGap;
		mRaveItButton._y = mAddButton._y;
		
		// Size and position the raves button
		mRavesButton._x = mRightButtonsBG._x + mButtonGap;
		mRavesButton._y = mAddButton._y;
		
		// Size and position the views button
		mViewsButton._x = mRavesButton._x + mRavesButton.width + mButtonGap;
		mViewsButton._y = mRavesButton._y;
		
		// Size and position the comments button
		mCommentsButton._x = mViewsButton._x + mViewsButton.width + mButtonGap;
		mCommentsButton._y = mRavesButton._y;
		
		// if the comments is being chopped, then hide it
		mCommentsButton._visible = mCommentsButton._x + mCommentsButton.width < mWidth;
				
		// Don't want to set this property while it's being tweened....otherwise it will screw up.  Just reset
		// the mSmallVideoScale before starting to tween it
		if(!mIsVideoSizeTweening)
		{
			mVideoScale = (mMediaDetailsBG._height - 2 * mOffset) / mHeight;
		}
	}
	
	/**
	 * Once the video is done tweening, reset the is video size tweening back to false
	 */
	private function onVideoSizeFinished():Void 
	{
		mIsVideoSizeTweening = false;
	}
	
	/**
	 * Get reference to the ticker placeholder
	 * @return The place holder to load the ticker into
	 */
	public function getTickerPH():MovieClip
	{
		return mTickerPH;
	}
	
	/**
	 * Set reference to the ticker
	 * @param pTicker The ticker
	 */
	public function setTicker(pTicker:Ticker):Void
	{
		mTicker = pTicker;
	}
}
