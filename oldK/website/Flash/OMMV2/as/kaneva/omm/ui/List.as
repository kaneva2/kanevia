﻿import mx.transitions.easing.Strong;
import mx.transitions.Tween;
import mx.utils.Delegate;

import kaneva.events.ScrollEvent;
import kaneva.events.TweenEvent;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.PlayList;
import kaneva.omm.ui.PlayListItem;
import kaneva.omm.ui.shapes.Rect;

/**
 * Custom List component class for the PlayList
 * @author Scott
 */
class kaneva.omm.ui.List extends ComponentDisplay
{
	/** Array containing objects used to populate the list */
	public var dataProvider:Array;
	
	/** The number of rows in the list */
	public var length:Number;
	
	/** The row height, this is the standard size for all rows except for the currently selected row */
	public var rowHeight:Number = 100;
	
	/** The index of the last selected row */
	public var selectedIndex:Number;
	
	/** The object for the currently selected row in the list */
	public var selectedItem:Object;
	
	/** Array containing the colors to use for the play list item backgrounds */
	public var mAlternatingRowColors:Array;
	
	/** Array that contains references to all row objects in the content clip */
	public var mPlayListItemArray:Array;
	
/** The border 
	private var mBorderClip:MovieClip;
*/	
	/** The mask clip */
	private var mMaskClip:MovieClip;
	
	/** Contains the row objects */
	private var mContentClip:MovieClip;
	
	/** The distance that the content clip can be scrolled */
	private var mScrollableDistance:Number = 0;
	
	/** The percent ratio between the content height and the component height which is used to size a scroll bar */
	private var mScrollablePercent:Number = 0;
	
	/** Animates the sizing of the rows */
//	private var mOldTween:Tween;
//	private var mNewTween:Tween;
	
	/** Easing function used for tween objects */
	private var mEasingFunc:Function = Strong.easeOut;
	
	/** Animates the play list to show selected item */
	private var mRepositionPlayListTween:Tween;
	
	/** Animates the scroll bar to show selected item */
	private var mRepositionScrollBarTween:Tween;
	
	/** the parent that houses the Playlist*/
	private var owner:PlayList;

	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function List ()
	{
		super ();
	}
	
	/** Create movie clips and other things */
	private function init ():Void
	{
		super.init ();
		
		// Create array that contains references to rows created
		mPlayListItemArray = new Array ();
		
		// Set the playlist component reference
		owner = PlayList(this._parent);
		
		// Watch these properties
		this.watch ("dataProvider", onDataProvider);
		this.watch ("selectedIndex", onSelectedIndex);
	}
	
	/** Create movie clips and other things */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create the content clip which will hold the rows
		mContentClip = this.createEmptyMovieClip("mContentClip", this.getNextHighestDepth ());
		
		// Create the border clip which contains the border graphic
//		mBorderClip = this.createEmptyMovieClip("mBorderClip", this.getNextHighestDepth ());
		
		// Create the mask clip 
		mMaskClip = this.createEmptyMovieClip("mMaskClip", this.getNextHighestDepth ());
		
	}
	
	/** Draws pieces and stuff */
	private function draw ():Void
	{
		super.draw ();
/*		
		// Draw the border
		mBorderClip.clear ();
		mBorderClip.lineStyle (0, 0xFF0000, 100);
		Rect.draw (mBorderClip, mWidth, mHeight);
*/		
		// Draw the mask
		mMaskClip.clear ();
		mMaskClip.beginFill (0, 25);
		Rect.draw (mMaskClip, mWidth, mHeight);
	}
	
	/** Sizes pieces and stuff */
	private function size ():Void
	{
		super.size ();
		
		calculateScrollableDistance ();
		calculateScrollablePercent ();
		
		// Check to see if the list gets stretched beyond the bottom of the content clip.  If so, we want the list
		// to scroll all the way to the bottom
		if (mScrollableDistance < Math.abs(mContentClip._y))
			scroll (1);
	}
	
	/** Creates the rows for the list */
	private function createPlayListItems (pArray:Array):Void
	{
		// Overwrite the current content clip so all previous rows are done away with
		var contentDepth:Number = mContentClip.getDepth();
		mContentClip = this.createEmptyMovieClip("mContentClip", contentDepth); 
		
		// Set the mask
		mContentClip.setMask (mMaskClip);
		
		// Loop thru the row count and create the rows
		var max:Number = pArray.length;
		for (var i:Number = 0; i < max; i++)
		{
			var rowClip:PlayListItem = PlayListItem (mContentClip.attachMovie ("PlayListItem", "mPlayListItem" + i, i));
			rowClip.setSize (null, rowHeight);
			mPlayListItemArray.push (rowClip);
			rowClip._y = rowHeight * i;
			rowClip.setMediaAsset (pArray[i], i+1, max);
			rowClip.index = i;
		}
	}
	
	/** 
	 * Method invoked whenever the selectedIndex property is changed
	 * @param pProp The watched property
	 * @param pOld The old value
	 * @param pNew The new value
	 * @return The value to set the property to
	 */
	private function onSelectedIndex (pProp:String, pOld:Number, pNew:Number):Number
	{
		// Set the selected item property to the corresponding media asset object in the data provider array
		selectedItem = dataProvider[pNew];
		
		// Animate previously selected row back to the rowHeight
		var oldPlayListItem:PlayListItem = mPlayListItemArray[pOld];
		oldPlayListItem.onSelect (false);
		var begin:Number = oldPlayListItem.height;
		var end:Number = rowHeight;
		
//		mOldTween = new Tween (oldPlayListItem, "height", mEasingFunc, begin, end, 0.5, true);
//		mOldTween.onMotionChanged = Delegate.create (this, onTweenChanged);
		
		// Animate the currently selected row to the expanded height
		var newPlayListItem:PlayListItem = mPlayListItemArray[pNew];
		newPlayListItem.onSelect (true);
		begin = newPlayListItem.height;
		end = newPlayListItem.getSelectedHeight();
		
//		mNewTween = new Tween (newPlayListItem, "height", mEasingFunc, begin, end, 0.5, true);
//		mNewTween.onMotionChanged = Delegate.create (this, onTweenChanged);
//		mNewTween.onMotionFinished = Delegate.create(this, onTweenFinished);
		
		// For some reason, if I try to call this method right away, it doesn't work.  If I wait a millisecond, it works
		var item:PlayListItem = newPlayListItem; //? newPlayListItem : oldPlayListItem;
		_global.setTimeout(this, "repositionOutOfFrameCell", 1, item);
		
		return pNew;
	}
	
	/** 
	 * The motion changed method for the row that is being animated
	 * @param pTween Reference to the tween object that called this event
	 */
	private function onTweenChanged (pTween:Tween):Void
	{
		// Get the reference to the row object that is being tweened
		var rowObj:PlayListItem = PlayListItem(pTween.obj);
		
		// Get the row objects index
		var rowIndex:Number = rowObj.index;
		
		// Get the number of rows
		var max:Number = dataProvider.length;
		
		// Go from the row after the currently tweened row, to the end of the data provider and set the position of the 
		// rows below it
		for (var i:Number = rowIndex + 1; i < max; i++)
		{
			var nextPlayListItem:PlayListItem = mPlayListItemArray[i];
			var prevPlayListItem:PlayListItem = mPlayListItemArray[i - 1];
			nextPlayListItem._y = prevPlayListItem._y + prevPlayListItem.height;
		}
		
//		dispatchEvent ({type:"change", event:true});
		
		size ();
	}
	
	/** Triggered once the tween is done */
	private function onTweenFinished(pTween:Tween):Void
	{
		//dispatchEvent ({type:"finish", event:true});
		var event:TweenEvent = new TweenEvent();
		event.type = TweenEvent.TWEEN_END;
		dispatchEvent(event);
//		dispatchEvent ({type:"finish"});
	}
	
	/** Calculate the distance the content clip can be scrolled */
	private function calculateScrollableDistance ():Void
	{
		// It will be the greater of the two values...the difference between visible area height and content height, or 0 which is 0%
		mScrollableDistance = Math.max(mContentClip._height - mHeight, 0);//trace ("mScrollableDistance : " + mScrollableDistance);
	}
	
	/** This calculates the ratio of content to visible area.  This is used to size the scroll bar */
	private function calculateScrollablePercent ():Void
	{
		// It will be the lesser of the two values...the ratio of visible area height to content height, or 1 which is 100%
		mScrollablePercent = Math.min (mHeight / mContentClip._height, 1);//trace ("mScrollablePercent : " + mScrollablePercent);
	}
	
	/** 
	 * Method invoked whenever the dataProvider property is changed
	 * @param pProp The watched property
	 * @param pOld The old value
	 * @param pNew The new value
	 * @return The value to set the property to
	 */
	private function onDataProvider (pProp:String, pOld:Array, pNew:Array):Array
	{
		this.length = pNew.length;
		createPlayListItems (pNew);
		size ();
		return pNew;
	}
	
	/** Invoked when the movie clip is instantiated and appears in the timeline */
	private function onLoad ():Void
	{
		super.onLoad();
		
		// Need to wait for the list to instantiate before we can properly calculate this
		calculateScrollablePercent ();
	}
	
	/**
	 * Moves the content clip to the appropriate scroll position
	 * @param pValue The scroll position to move content to
	 */
	public function scroll (pValue:Number):Void
	{
		if (pValue != undefined)
		{
			mContentClip._y = -pValue * mScrollableDistance;
			var scrollEvent:ScrollEvent = new ScrollEvent();
			scrollEvent.type = ScrollEvent.SCROLL;
			dispatchEvent(scrollEvent);
//			dispatchEvent ({type:"scroll", target:this});
			setVisibleItems ();
		}
	}
	
	/** 
	 * Used to set if the list has rows that have alternating background colors
	 * @param pValue Array with colors for elements
	 */
	public function setRowColors (pValue:Array):Void
	{
		mAlternatingRowColors = pValue;
		
		// Loop thru the row count
		var max:Number = mPlayListItemArray.length;
		for (var i:Number = 0; i < max; i++)
		{
			var rowClip:PlayListItem = mPlayListItemArray[i];
			var element:Number = i % mAlternatingRowColors.length;
			var bgColor:Number = mAlternatingRowColors[element];
			rowClip.setBackgroundColor (bgColor);
		}
	}
	
	/** 
	 * Reposition the playlist when a selected cell is partially extended outside the viewable area 
	 * @param pSelectedCell The play list item that was selected
	 */
	private function repositionOutOfFrameCell (pSelectedCell:PlayListItem):Void
	{
		// If selected cell is undefined.....get out now
		if (pSelectedCell == undefined)
		{
			//mRepositionScrollBarTween = new Tween (owner.mScrollBar, "scrollPosition", mEasingFunc, owner.mScrollBar.scrollPosition, owner.mScrollBar.scrollPosition, 0.5, true); 
			return;
		}
		
		var percentage:Number; // percent of the area selected within the contentClip
		var newPositionOfContentClip:Number;
		
		// A reference to the selected play list item
		var item:PlayListItem = pSelectedCell;
//		trace (item);
		
		// Calculate the y position (top of clip) of the selected item once the animation is done....this is y relative to content clip
		var selectedItemYPos:Number = selectedIndex * rowHeight;
		
		// Calculate the y position plus it's height (bottom of clip of the selected item once the animation is done....this is y relative to content clip
		var selectedItemYPosPlusHeight:Number = selectedIndex * rowHeight + item.getSelectedHeight();
		
		// Calculate the y position of the top edge of the selected item once the animation is done....this is y relative to list (this class)
		var topEdge:Number = mContentClip._y + selectedItemYPos;
		
//		trace ("topEdge : " + topEdge);
//		trace ("item.getSelectedHeight() : " + item.getSelectedHeight());
		
		// Calculate the y position of the bottom edge of the selected item once the animation is done....this is y relative to list (this class)
		var bottomEdge:Number = mContentClip._y + selectedItemYPosPlusHeight;
		
//		trace ("bottomEdge : " + bottomEdge);
//		trace ("");
//		trace ("in view : " + (topEdge >= 0 && bottomEdge <= mHeight).toString());
//		trace ("");
		
		// If the cell is within the viewable area, then get out now.
		if (topEdge >= 0 && bottomEdge <= mHeight)
			return;
		
		// If the top of the selected play list item is above the viewable area.....
 		if (topEdge < 0)
		{
//			trace ("top is above 0");
			// Get new end values for the play list and scroll bar
			newPositionOfContentClip = mContentClip._y - topEdge;
			percentage = -newPositionOfContentClip / (mContentClip._height - mHeight);
			
		}
		// Else if the bottom of the selected play list item is below the viewable area....
		else if (bottomEdge > mHeight)
		{
//			trace ("bottom is below mHeight");
			// Get new end values for the play list and scroll bar
			newPositionOfContentClip = mContentClip._y - (bottomEdge - mHeight);
			percentage = -newPositionOfContentClip / (mContentClip._height - mHeight);
		}
		
		// Tween the play list and scroll bar into position
//		mRepositionPlayListTween = new Tween (mContentClip, "_y", mEasingFunc, mContentClip._y, newPositionOfContentClip, 0.5, true);
		mRepositionScrollBarTween = new Tween (owner.mScrollBar, "scrollPosition", mEasingFunc, owner.mScrollBar.scrollPosition, percentage, 0.5, true); 
//		trace ("\r");
	}
	
	/** 
	 * Set the visibility of the play list items depending on whether the are in the visible area of the list.  This keeps Flash 
	 * from having to redraw all of the items in the list when they aren't being used.  This also triggers the download of the thumbnail
	 * images only when the item is being shown.  This prevents the list from having to download all the thumbnail images all at once.
	 */
	private function setVisibleItems ():Void
	{
		var max:Number = mPlayListItemArray.length;
		
		// Loop thru all items
		for (var i:Number = 0; i < max; i++)
		{
			// Reference to the item
			var item:PlayListItem = mPlayListItemArray[i];
			
			// Calculate if the item is visible
			var visible:Boolean = item._y + item.height + mContentClip._y > 0 && item._y + mContentClip._y < mHeight;
			
			// Set visibility of the item
//			item._visible = visible;
			
			// Only try to load the thumb if this item is visible
			if (visible)
				item.loadThumbnail ();
		}
	}
	
	/** 
	 * Get the scrollable percent
	 * @returns A number between 0 and 1
	 */
	public function getScrollablePercent ():Number
	{
		return mScrollablePercent;
	}
}