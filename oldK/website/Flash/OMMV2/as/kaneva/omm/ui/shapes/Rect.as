﻿/**
 * Class used to draw a Rectangle in a movie clip.
 * @author Scott
 */
class kaneva.omm.ui.shapes.Rect
{
	/**
	 * Draw a basic rectangle in a movie clip
	 * @param c The movie clip to draw the shape in
	 * @param w The width of the shape
	 * @param h The height of the shape
	 */
	public static function draw (c:MovieClip, w:Number, h:Number):Void
	{
		c.lineTo (w, 0);
		c.lineTo (w, h);
		c.lineTo (0, h);
		c.lineTo (0, 0);
	}

	/**
	 * Private constructor so it doesn't get instantiated
	 */
	private function Rect () {}
}
