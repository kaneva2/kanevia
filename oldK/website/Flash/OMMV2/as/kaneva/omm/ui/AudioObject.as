﻿import mx.utils.Delegate;

import kaneva.events.SoundEvent;
import kaneva.omm.ui.MovieClipDispatcher;

/**
 * Object which contains the sound object used to playback audio content.  This object doesn't have any visual
 * assets, so isn't necessary to use the ComponentDisplay framework as the other components.  So this class
 * only needs to extend MovieClipDispatcher so it is capable of dispatching events.  But this class does represent a
 * movie clip from the library.  The parameter for the Sound object needs a movie clip as a target.  
 * Don't want to use an absolute reference (_root).
 * @author Scott
 */
class kaneva.omm.ui.AudioObject extends MovieClipDispatcher
{
	/** Sound object used to load mp3 files */
	private var mSoundObj:Sound;
	
	/** Interval that runs while an external audio file (.mp3) is loading */
	private var mLoadInterval:Number;
	
	/** Property used to keep track of where the audio position is once it gets paused */
	private var mPosition:Number = 0;
	
	/** Used to tell if the audio is playing or not....this is used to tell if it's buffering */
	private var mPrevPosition:Number = 0;
	
	/** Constructor */
	public function AudioObject ()
	{
		super ();
		createSoundObject ();
	}
	
	/** Create the sound object and set up method */
	private function createSoundObject ():Void
	{
		// Create a sound object
		mSoundObj = new Sound (this);
		
		// Set up methods
		mSoundObj.onLoad = Delegate.create (this, onSoundLoad);
		mSoundObj.onSoundComplete = Delegate.create (this, onSoundDonePlaying);
	}
	
	/** 
	 * Invoked automatically when a sound loads
	 * @param pSuccess A Boolean value of true if the sound has been loaded successfully, false otherwise
	 */
	private function onSoundLoad (pSuccess:Boolean):Void
	{
		// Let listeners know that the sound has completely loaded
		var soundEvent:SoundEvent = new SoundEvent();
		soundEvent.type = SoundEvent.COMPLETE;
		soundEvent.soundObject = mSoundObj;
		soundEvent.bytesLoaded = mSoundObj.getBytesLoaded();
		soundEvent.bytesTotal = mSoundObj.getBytesTotal();
		dispatchEvent(soundEvent);
		
//		dispatchEvent ({type:"load", target:this, sound:mSoundObj, percent:1});
		
		// Reset the previous position property
		mPrevPosition = 0;
		
		// Clear out any load interval
		clearInterval (mLoadInterval);
	}
	
	/** 
	 * Invoked automatically when a sound finishes playing. You can use this handler to trigger events 
	 * when a sound finishes playing. 
	 */
	private function onSoundDonePlaying ():Void
	{
		var soundEvent:SoundEvent = new SoundEvent();
		soundEvent.type = SoundEvent.SOUND_COMPLETE;
		dispatchEvent(soundEvent);
//		dispatchEvent ({type:"complete", target:this, sound:mSoundObj});
	}
	
	/**
	 * Get what percent of the sound is loaded
	 * @return The percent of the mp3 that has loaded
	 */
	private function getPercentLoaded ():Number
	{
		// Calculate the percent loaded
		var loaded:Number = mSoundObj.getBytesLoaded ();
		var total:Number = mSoundObj.getBytesTotal ();
		return loaded / total;
	}
	
	/** 
	 * Called by the load interval.  This dispatches a progress event letting any listeners know about
	 * the sound's current load status.  The sound object itself is passed as a property of the event object
	 * so that the listening method can access info about the sound download
	 */
	private function onLoadProgress():Void
	{
		// Calculate the difference between where the position of the media and the duration.....divide by 1000 since
		// duration and position are in milliseconds
		var diff:Number = (mSoundObj.duration - mSoundObj.position) / 1000;
		
		// Calculate how close the buffer is to being full
		var bufferPercent:Number = diff / _global._soundbuftime;
		
		// If the previous position equals the current position and buffer percent is 
		// less than 1...then the audio is probably not playing and therefore is buffering
		var isBuffering:Boolean = mPrevPosition == mSoundObj.position && bufferPercent < 1;
		
		// Set the progress
		var soundEvent:SoundEvent = new SoundEvent();
		soundEvent.type = SoundEvent.PROGRESS;
		soundEvent.soundObject = mSoundObj;
		soundEvent.bufferPercent = bufferPercent;
		soundEvent.isBuffering = isBuffering;
		soundEvent.bytesLoaded = mSoundObj.getBytesLoaded();
		soundEvent.bytesTotal = mSoundObj.getBytesTotal();
		dispatchEvent(soundEvent);
		
//		dispatchEvent ({type:"progress", target:this, sound:mSoundObj, percent:getPercentLoaded(), buffer:bufferPercent, isBuffering:isBuffering});
		
		// Set the previous position so that we have a reference to the position the last time
		// this method was called by the interval. If the current position is the same as this 
		// variable, then we can assume that the audio is not playing and therefore, it
		// is probably buffering
		mPrevPosition = mSoundObj.position;
	}
	
	/**
	 * Loads an MP3 file into a Sound object
	 * @param pUrl The location on a server of an MP3 sound file
	 */
	public function load (pUrl:String):Void
	{
		// Clear out any previous download of sound
		close ();
		
		// Establishes the number of seconds of streaming sound to buffer
		_global._soundbuftime = 10;
		
		// Load the sound
		mSoundObj.loadSound (pUrl, true);
		
		// Start the interval which checks download progress and dispatches event for 
		// listeners to do something with
		clearInterval (mLoadInterval);
		mLoadInterval = setInterval (this, "onLoadProgress", 250);
	}
	
	/** 
	 * Method used to pause the sound.  There is no pause method for sound objects.  
	 * Only stop and start.  Once a sound is stopped and then started again, it 
	 * starts back at position 0....unless otherwise specified in the first parameter 
	 * of the start method.  That's where the mPosition property comes in
	 */
	public function pause ():Void
	{
		mSoundObj.stop ();
		
		//TODO - This seems to not be needed.....get rid of once sure
//		setPosition (mPosition);

	}
	
	/** Plays the sound based on the last known position */
	public function play ():Void
	{
		mSoundObj.start (mPosition);

	}
	
	/** Stops the sound and resets the position property */
	public function stop ():Void
	{
		mSoundObj.stop ();
		setPosition (0);
	}
	
	/** 
	 * Sets the position of the sound 
	 * @param pValue The number of seconds 
	 */
	public function setPosition (pValue:Number):Void
	{
		// Make sure that the position can't be set past the duration
		if (pValue > mSoundObj.duration / 1000)
			mPosition = mSoundObj.duration / 1000;
		else
			mPosition = pValue;
	}
	
	/** 
	 * The current position of the sound
	 * @return Position in seconds
	 */
	public function getPosition ():Number
	{
		return mSoundObj.position / 1000;
	}
	
	/** 
	 * Sets the volume 
	 * @param pValue The volume level (0 - 100)
	 */
	public function setVolume (pValue:Number):Void
	{
		mSoundObj.setVolume (pValue);
	}
	
	/** 
	 * Gets the volume 
	 * @return The volume level (0 - 100)
	 */
	public function getVolume ():Number
	{
		return mSoundObj.getVolume ();
	}
	
	/** Stops the download progress of whatever sound is being downloaded */
	public function close ():Void
	{
		// Clear out any load interval
		clearInterval (mLoadInterval);
		
		// Delete the sound object
		delete mSoundObj;
		
		// Create a new sound object
		createSoundObject ();
	}
}
