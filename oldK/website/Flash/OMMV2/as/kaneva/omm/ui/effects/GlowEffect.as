import mx.transitions.easing.None;

import flash.filters.GlowFilter;

import kaneva.omm.ui.effects.Effect;
import kaneva.omm.ui.effects.IEffect;

/**
 * @author scott
 */
class kaneva.omm.ui.effects.GlowEffect extends Effect implements IEffect
{
	/** The glow filter object */
	private var mGlowFilter:GlowFilter;
	
	/** Constructor function */
	public function GlowEffect() 
	{
		super();
		
		// Create the glow filter object
		mGlowFilter = new GlowFilter(0xFFFFFF, 0, 4, 4, 5, 1);
		
		// Set up the tween stuff
		mObject = mGlowFilter;
		mProperty = "alpha";
		mEasingFunction = None.easeOut;
		mBeginValue = 0;
		mEndValue = 0.75;
		mAnimationDuration = 0.25;
	}

	/** 
	 * Called by the onMotionChanged event of the tween roll effect.
	 * @param pValue The property value to set
	 */
	public function setEffect(pValue:Number):Void
	{
		super.setEffect(pValue);
		
		// Set the alpha to the filter
		mGlowFilter.alpha = pValue;
		
		// Set the filter to the target graphic
		mEffectsTarget.filters = [mGlowFilter];
	}
}