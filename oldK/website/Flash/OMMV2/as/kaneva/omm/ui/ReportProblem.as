﻿import mx.utils.Delegate;

import flash.geom.Matrix;

import kaneva.events.ItemClickEvent;
import kaneva.events.MouseEvent;
import kaneva.omm.Constants;
import kaneva.omm.CSS;
import kaneva.omm.io.XmlParser;
import kaneva.omm.Logger;
import kaneva.omm.OMM;
import kaneva.omm.ui.buttons.PopUpButton;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.shapes.RectRounded;
import kaneva.omm.widgets.UrlRedirectWidget;
import kaneva.omm.widgets.WidgetData;
import kaneva.util.Strings;

/**
 *  A window that displays 4 radio buttons and a comment box.
 * @author Kevin
 */
class kaneva.omm.ui.ReportProblem extends ComponentDisplay
{
	/** Values set by user interaction of the radio buttons */
	public static var REPORT_COPYRIGHT_VIOLATION:String = "c";
	public static var REPORT_ILLEGAL:String = "i";
	public static var REPORT_MATURE_VIOLATION:String = "m";
	public static var REPORT_OTHER:String = "o";
	
	/** The white base of the window */
	private var mWhiteBase:MovieClip;
	
	/** The white base of the window */
	private var mGrayBase:MovieClip;
	
	/** Text format object for the text fields */
	private var mTextFormat:TextFormat;
	
	/** stylesheet for the textfields*/
	private var mCSS:TextField.StyleSheet;
	
	/** The text fields needed to create this report problem box */
	private var mTitle:TextField;
	private var mEmailText:TextField;
	private var mEmailBox:TextField;
	private var mComments:TextField;
	private var mRadio1Txt:TextField;
	private var mRadio2Txt:TextField;
	private var mRadio3Txt:TextField;
	private var mRadio4Txt:TextField;
	private var mNoteText:TextField;
	private var mNoteBox:TextField;
	
	/** Array that will hold reference to all the text fields */
	private var mTxtArray:Array;
	
	/** The radio button clips */
	private var mRadioBtn1:MovieClip;
	private var mRadioBtn2:MovieClip;
	private var mRadioBtn3:MovieClip;
	private var mRadioBtn4:MovieClip;
	
	/** Reference to the last selected radio button */
	private var mSelectedRadio:MovieClip;
	
	/** The submit button clip */
	private var mSubmitButton:PopUpButton;
	
	/** The cancel button clip */
	private var mCancelButton:PopUpButton;
	
	/** the gap separating the two bases*/
	private var mGap:Number = 10;
	
	/** 
	 * Used to execute a sendAndLoad method.  Properties are appended to this object and sent to the server.
	 * The response from the server are sent to the xml object specified in the second parameter of the sendAndLoad method
	 */
	private var mSendingLoadVars:LoadVars;
	private var mReceivingXML:XML;	
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function ReportProblem ()
	{
		super ();
	}
	
	/** Initialize stuff */
	private function init ():Void
	{
		super.init ();
		
		mReceivingXML = new XML ();  
		mSendingLoadVars = new LoadVars ();  
  		 
		// Set xml stuff up  
		mReceivingXML.ignoreWhite = true;  
		mReceivingXML.onLoad = Delegate.create (this, onLoadXML); 
		
		// Set your properties to send to the server....curently nothing is really being sent
		mSendingLoadVars["prop1"] = "prop1 value";
		mSendingLoadVars["prop2"] = "prop2 value";
		
		// For testing
		//setUri (mTempUrl);
	}	
	
	/** Create movie clips and other things */
	private function createChildren ():Void
	{
		super.createChildren ();		
		
		// Create the CSS for the content text
		mCSS = CSS.getReportWindowStyle();
		mTextFormat = new TextFormat(); 
		mTextFormat.font = mCSS.getStyle(".label").fontFamily;
		mTextFormat.color = Strings.getColor (mCSS.getStyle(".label").color);
		mTextFormat.size = parseInt(mCSS.getStyle(".label").fontSize);
		
		// Create the base clips
		mGrayBase = this.createEmptyMovieClip("mGrayBase", this.getNextHighestDepth ());
		mWhiteBase = this.createEmptyMovieClip("mWhiteBase", this.getNextHighestDepth ());
		
		// Stick dummy button event so that nothing below it can be clicked
		mGrayBase.onRelease = function (){};	
		
		// Get the height necessary for the specified format
		var txtHeight:Number = mTextFormat.getTextExtent("Test").textFieldHeight;
		
		// Stick text fields in the array
		mTxtArray = ["mTitle", "mEmailText", "mEmailBox", "mComments", "mRadio1Txt", "mRadio2Txt", "mRadio3Txt", "mRadio4Txt", "mNoteText", "mNoteBox"];
		
		var txtObj:TextField;
		
		// Loop thru array and set props that are universal to all text fields in this component
		var max:Number = mTxtArray.length;
		for (var i:Number = 0; i < max; i++)
		{
			txtObj = this[mTxtArray[i]] = this.createTextField (mTxtArray[i], this.getNextHighestDepth (), 0, 0, 0, 0);
			txtObj.autoSize = true;
			txtObj.multiline = true;
			txtObj.wordWrap = true;
			txtObj.html = true;
			txtObj.styleSheet = mCSS;
		}
		
		mTitle.multiline = mEmailText.multiline = false;
		mTitle.wordWrap = mEmailText.wordWrap = false;
		
		// Set the text for the text fields
		mTitle.htmlText = "<p class = 'label'>Report a Problem</p>";
		mEmailText.htmlText = "<p class = 'label'>Email:</p>";
		mComments.htmlText = "<p class = 'label'>Your comments will make our site better for everyone.</p>";		
		mRadio1Txt.htmlText = "<p class = 'label'>Inappropriate for under the age of 18</p>";		
		mRadio2Txt.htmlText = "<p class = 'label'>Copyright infringement</p>";
		mRadio3Txt.htmlText = "<p class = 'label'>Criminal activity</p>";
		mRadio4Txt.htmlText = "<p class = 'label'>Other</p>";		
		mNoteText.htmlText = "<p class = 'label'>Comment:</p>";
		
		mTitle.selectable = mEmailText.selectable = mComments.selectable = mRadio1Txt.selectable = mRadio2Txt.selectable = mRadio3Txt.selectable = mRadio4Txt.selectable = mNoteText.selectable = false;
		
		// Set email input text box properties
		mEmailBox.autoSize = mNoteBox.autoSize = false;
		mEmailBox.border = mNoteBox.border = true;
		mEmailBox.borderColor = mNoteBox.borderColor = 0xA7A7A7;
		mEmailBox.background = mNoteBox.background = true;
		mEmailBox.backgroundColor = mNoteBox.backgroundColor = 0xF1F1F1;
		mEmailBox._height = mNoteBox._height = txtHeight;
		mEmailBox.textColor = mNoteBox.textColor = 0x666666;
		
		// styleSheet must be set to undefined or else the textfield will be read-only thus the user will be unable to type into the fields
		mEmailBox.styleSheet = undefined;
		txtObj.styleSheet = undefined;

		mEmailBox.setNewTextFormat(mTextFormat);
		txtObj.setNewTextFormat(mTextFormat);

		mEmailBox.type = "input";
		// Set notes input text box properites
		mNoteBox.type = "input";
		
		// Create buttons
		mSubmitButton = PopUpButton (this.attachMovie ("PopUpButton", "mSubmitButton", this.getNextHighestDepth ()));
		mCancelButton = PopUpButton (this.attachMovie ("PopUpButton", "mCancelButton", this.getNextHighestDepth ()));
		mSubmitButton.label = "Submit";
		mCancelButton.label = "Cancel";
		mSubmitButton.addEventListener (MouseEvent.CLICK, Delegate.create (this, onSubmit));
		mCancelButton.addEventListener (MouseEvent.CLICK, Delegate.create (this, onCancel));
		
		// Create the radio buttons
		mRadioBtn1 = this.attachMovie ("RadioButton", "mRadioBtn1", this.getNextHighestDepth (), {code:REPORT_MATURE_VIOLATION});
		mRadioBtn2 = this.attachMovie ("RadioButton", "mRadioBtn2", this.getNextHighestDepth (), {code:REPORT_COPYRIGHT_VIOLATION});
		mRadioBtn3 = this.attachMovie ("RadioButton", "mRadioBtn3", this.getNextHighestDepth (), {code:REPORT_ILLEGAL});
		mRadioBtn4 = this.attachMovie ("RadioButton", "mRadioBtn4", this.getNextHighestDepth (), {code:REPORT_OTHER});
		
		// Don't want to delegate these events.....this is so the scope of the on click method is that of the radio button that called it
		mRadioBtn1.onRelease = mRadioBtn2.onRelease = mRadioBtn3.onRelease = mRadioBtn4.onRelease = onRadioClick;
	}
	
	/** Draws pieces and stuff */
	private function draw ():Void
	{
		super.draw ();

		drawBackground();

	}
	
	/** Sizes pieces and stuff */
	private function size ():Void
	{
		super.size ();
		
		// Gap between text fields, and also from the edge of the base
		var gap:Number = 5;

		// Position and size the title text
		mTitle._x = mGrayBase._x + mGap;
		mTitle._y = mGrayBase._y + mGap;
		
		mWhiteBase._x = mGrayBase._x + mGap;
		mWhiteBase._y = mTitle._y + mTitle._height + gap;
		
		// Position cancel and submit buttons
		mCancelButton._x = mGrayBase._width - mCancelButton._width - mGap;
		mCancelButton._y = mGrayBase._height - mCancelButton._height - mGap;
		mSubmitButton._x = mCancelButton._x - mSubmitButton._width - mGap;
		mSubmitButton._y = mCancelButton._y;			
		
		// Position and size the email text
		mEmailText._x = mWhiteBase._x + gap;
		mEmailText._y = mWhiteBase._y + gap;
		
		// Position and size the email input text
		mEmailBox._x = mEmailText._x + mEmailText._width + gap;
		mEmailBox._y = mEmailText._y ;
		mEmailBox._width = mWhiteBase._x + mWhiteBase._width - mEmailBox._x - gap;
		
		// Position and size the comments text
		mComments._x = mWhiteBase._x + gap;
		mComments._y = mEmailBox._y + mEmailBox._height + gap;
		mComments._width = mWhiteBase._width - 2*gap;
		
		// Set x position of radio buttons
		mRadio1Txt._x = mRadio2Txt._x = mRadio3Txt._x = mRadio4Txt._x = mRadioBtn1._x + mRadioBtn1._width;
		
		// Set widths of the radio text fields
		mRadio1Txt._width = mRadio2Txt._width = mRadio3Txt._width = mRadio4Txt._width = mWhiteBase._width - gap;

		// Position the radio buttons
		mRadioBtn1._x = mRadioBtn2._x = mRadioBtn3._x = mRadioBtn4._x = mWhiteBase._x + gap;
		mRadioBtn1._y = mComments._y + mComments._height + gap;
		mRadioBtn2._y = mRadioBtn1._y + mRadioBtn1._height + gap;
		mRadioBtn3._y = mRadioBtn2._y + mRadioBtn2._height + gap ;
		mRadioBtn4._y = mRadioBtn3._y + mRadioBtn3._height + gap ;		
		
		// Set y position of the radio buttons
		mRadio1Txt._y = mRadioBtn1._y + mRadioBtn1._height/2 - mRadio1Txt._height/2;
		mRadio2Txt._y = mRadioBtn2._y + mRadioBtn2._height/2 - mRadio2Txt._height/2;
		mRadio3Txt._y = mRadioBtn3._y + mRadioBtn3._height/2 - mRadio3Txt._height/2;
		mRadio4Txt._y = mRadioBtn4._y + mRadioBtn4._height/2 - mRadio4Txt._height/2;
		
		// Set position of the notes text
		mNoteText._x = mWhiteBase._x + gap;
		mNoteText._y = mRadio4Txt._y + mRadio4Txt._height + gap;
		mNoteText._width = mWhiteBase._width - gap;
		
		// Set position and size of the notes input text field
		mNoteBox._x = mNoteText._x;
		mNoteBox._y = mNoteText._y + mNoteText._height;
		mNoteBox._width = mWhiteBase._width - (gap * 2);
		mNoteBox._height = mWhiteBase._y + mWhiteBase._height - mNoteBox._y - gap;
		

	}
	
	/** Draws the background clip */
	private function drawBackground():Void
	{
		mGrayBase.clear ();
		mGrayBase.lineStyle (0, 0, 100, true);
		mGrayBase.beginFill (0xE6E6E6, 100);
		RectRounded.draw (mGrayBase, mWidth, mHeight, Constants.RADIUS);
		mGrayBase.endFill ();
		
		// Dimensions of the white base
		var mw:Number = mWidth - 2*mGap;
		var mh:Number = mHeight - 2*mGap - mCancelButton._height - 5*2/*gap from the draw function*/ - mTitle._height ;
		
		// Set up matrix		
		var matrix:Matrix = new Matrix ();
		matrix.createGradientBox (mw, mh, Math.PI/2, 0, 0);
		
		mWhiteBase.clear ();
		mWhiteBase.lineStyle (0, 0xCCCCCC, 100, true);
		//mWhiteBase.beginFill (0xFDFDFD, 100);
		mWhiteBase.beginGradientFill ("linear", [0xFFFFFF, 0xFFFFFF, 0xEBEDEC, 0xFFFFFF], [100, 100, 100, 100], [0, 200, 220, 255], matrix);
		RectRounded.draw (mWhiteBase, mw, mh, Constants.RADIUS);
		mWhiteBase.endFill ();
	}
	
	/** Method called when the submit button is pressed */
	private function onSubmit ():Void
	{
		// Get the values set by the user.....if not defined, then set to empty string
		var email:String = mEmailBox.text == undefined ? "" : mEmailBox.text;
		var error:String = mSelectedRadio.code == undefined ? "" : mSelectedRadio.code;
		var notes:String = mNoteBox.text == undefined ? "" : mNoteBox.text;
		
		Logger.logMsg( "onSubmit " + notes + error + email );
		
		var map:Object = OMM.getConfig().mConfigMapping;
		var reportProblem:String = map["uri.link.report"];
		var temp = Strings.replace(reportProblem, "{0}", String(OMM.getMedia().id));
		reportProblem = Strings.replace(temp, "{1}", error);
		temp = Strings.replace(reportProblem, "{2}", notes);
		reportProblem = Strings.replace(temp, "{3}", email);

		Logger.logMsg( "MediaVideoBase.onSubmit " + reportProblem + " " + mReceivingXML );
		
		mSendingLoadVars.sendAndLoad (reportProblem, mReceivingXML, "POST");  
		
		// Dispatch a submit event so the listeners can do something when then submit button is pressed
		var itemClickEvent:ItemClickEvent = new ItemClickEvent();
		itemClickEvent.type = ItemClickEvent.ITEM_CLICK;
		itemClickEvent.label = "submit";
		dispatchEvent(itemClickEvent);
		
		// Quick fix....need window to still be there so xml can finish it's thing.  Then we can call onCancel
		// which will remove the window.
		// TODO - Extract xml out of window so window can be removed immediately which xml can do it's thing
		this._visible = false;
		
		// Close this window
//		onCancel ();
	}
	
	/** Method called when the cancel button is pressed */
	private function onCancel ():Void
	{
		var itemClickEvent:ItemClickEvent = new ItemClickEvent();
		itemClickEvent.type = ItemClickEvent.ITEM_CLICK;
		itemClickEvent.label = "cancel";
		dispatchEvent(itemClickEvent);
		
//		dispatchEvent ({type:"cancel", target:this});
		this.removeMovieClip ();
	}
	
	/** Method called when a radio button is pressed */
	private function onRadioClick ():Void
	{
		// Since we aren't delegating this method, the scope is that of the radio button that called this method
		// This is our reference to the report problem class
		var owner:ReportProblem = ReportProblem (this._parent);
		
		// The radio button that called this method
		var radio:MovieClip = this;
		
		// Deselect previous radio button and then set the new one
		owner.mSelectedRadio.gotoAndStop(1);
		owner.mSelectedRadio = radio;
		owner.mSelectedRadio.gotoAndStop(2);
	}
	
	/** 
	 * Invoked by Flash Player when an XML document is received from the server
	 * @param pSuccess A Boolean value that evaluates to true if the XML object is successfully loaded with a XML.load() or XML.sendAndLoad() operation; otherwise, it is false
	 */
	private function onLoadXML (pSuccess:Boolean):Void  
	{  
		Logger.logMsg( "ReportProblem.onLoadXML " + pSuccess );
		
		// If xml loaded successfully....
		if (pSuccess)  
		{
			// Get the root node
			var rootNode:XMLNode = mReceivingXML.firstChild;
			
			// See if the root node has children
			if (rootNode.hasChildNodes())
			{
				// Get the array of children in root node and its length
				var rootChildren:Array = rootNode.childNodes;
				var numberOfRootChildren:Number = rootChildren.length;
				
				// Loop thru the children of root node
				for (var a:Number = 0; a < numberOfRootChildren; a++)
				{
					// The specified child
					var childNode:XMLNode = rootChildren[a];

					Logger.logMsg( "ReportProblem.onLoadXML got node: " + childNode.nodeName );
					
					// Do different things depending on the name of the specified node
					switch(childNode.nodeName)
					{
						case "confirm":
							//if widget is sent back - registerWidget should be called
							if(childNode.hasChildNodes())
							{
								var parser:XmlParser = new XmlParser();
								var widget:WidgetData = parser.readWidgetXML(childNode.childNodes[0]);
								var omm:OMM = OMM.getInstance();
								omm.registerWidget(widget);
							}
							break;

						case "error":
							// nothing in an error message
							break; 

						case "widgets":
							//if widget is sent back - registerWidget should be called
							if(childNode.hasChildNodes())
							{
								var parser:XmlParser = new XmlParser();
								var widget:WidgetData = parser.readWidgetXML(childNode.childNodes[0]);
								var omm:OMM = OMM.getInstance();
								omm.registerWidget(widget);
							}
							break;
							
						default:
							var redirect:UrlRedirectWidget = new UrlRedirectWidget();
							redirect.url = childNode.attributes.uri;
							redirect.run();
					}
				}
			}
		}
		// close the window once the xml has been returned or if it failed.
//		onCancel();
	}
}
