﻿import mx.utils.Delegate;

import kaneva.events.ItemClickEvent;
import kaneva.events.ResizeEvent;
import kaneva.events.ScrollEvent;
import kaneva.events.TweenEvent;
import kaneva.omm.Constants;
import kaneva.omm.OMM;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.List;
import kaneva.omm.ui.ScrollBar;
import kaneva.omm.ui.shapes.RectRounded;


/**
 * Represents the play list component which shows a list of media that can be played
 * @author Scott
 */
class kaneva.omm.ui.PlayList extends ComponentDisplay
{
	/** singleton - only one instance is allowed.  used to communicate with the omm*/
	private static var mOMM:OMM;
	
	/** The background for the playlist */
	private var mBGClip:MovieClip;
	
	/** Contains the rows */
	public var mList:List;
	
	/** The scroll bar clip */
	public var mScrollBar:ScrollBar;
	
//	/** The top base of the play list */
//	private var mBaseClip:MediaBase;
	
//	/** The height of the base pieces */
//	private var mBaseClipHeight:Number = 25;
	
	/** the index of the media currently 'playing'.  assumes when playlist is loaded, 1st item '0' is set to start playing*/
	public var mCurrentlyPlayingIndex:Number = 0;
	
	/** Indicates whether the scroll bar is visible or not */
	private var isScrollBar:Boolean = false;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function PlayList ()
	{
		super ();
		mOMM = OMM.getInstance();
	}
	
	/** Initialize stuff */
	private function init ():Void
	{
		super.init ();
		
		mScrollBar.addEventListener (ScrollEvent.SCROLL, Delegate.create (this, onScroll));
		mList.addEventListener (ResizeEvent.RESIZE, Delegate.create (this, onResizeScrollBar));
		mList.addEventListener (ItemClickEvent.ITEM_CLICK, Delegate.create (this, onListClick));
		mList.addEventListener (TweenEvent.TWEEN_END, Delegate.create (this, size));
//		mList.addEventListener ("finish", Delegate.create (this, onChange));
//		mList.addEventListener ("change", Delegate.create (this, onChange));
		
		Mouse.addListener (this);
	}
	
	/** Create movie clips and other things */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create the background clip
		mBGClip = this.createEmptyMovieClip ("mBGClip", this.getNextHighestDepth ());
		
//		// Create the top part of the base
//		mBaseClip = MediaBase (this.attachMovie ("MediaBase", "mBaseClip", this.getNextHighestDepth ()));
//		mBaseClip.setTitle (OMM.getConfig().getAsString("playlist.title"));
		
		// Create the list component
		mList = List (this.attachMovie ("List", "mList", this.getNextHighestDepth ()));
		mList.rowHeight = 100;

		// Create the scroll bar
		mScrollBar = ScrollBar (this.attachMovie ("ScrollBar", "mScrollBar", this.getNextHighestDepth ()));
	}
	
	/** Draw stuff */
	private function draw ():Void
	{
		super.draw ();
		
		// Draw the background
		mBGClip.beginFill(OMM.getConfig().getAsNumber("playlist.background.color"), 100);
//		RectRounded.draw(mBGClip, mWidth, mHeight - mBaseClipHeight, Constants.RADIUS);
		RectRounded.draw(mBGClip, mWidth, mHeight, Constants.RADIUS);
		mBGClip.endFill();
	}
	
	/** Sizes pieces and stuff */
	private function size ():Void
	{
		super.size ();
//		onChange ();
		
		// Set if the scroll bar should be visible or not
		isScrollBar = mList.getScrollablePercent() < 1;
		
//		// Size the base top component
//		mBaseClip.setSize (mWidth, mBaseClipHeight);
		
//		// Position the base
//		mBGClip._y = mBaseClipHeight;
		
		// Position and size the list and scrollbar
//		mList._y = mBaseClipHeight + Constants.RADIUS;
		mList._y = Constants.RADIUS;
		
//		mList.setSize (mWidth - (isScrollBar ? mScrollBar.width : 0), mHeight - mBaseClipHeight - 2*Constants.RADIUS);
		mList.setSize (mWidth - (isScrollBar ? mScrollBar.width : 0), mHeight - 2*Constants.RADIUS);
		mScrollBar.setSize (null, mList.height);
		mScrollBar._x = mList._x + mList.width;
		mScrollBar._y = mList._y;

	}
	
	/**
	 * Exectued when the scroll bar dispatches a scroll event
	 * @param pEvent Object passed to method from dispatcher
	 */
	private function onScroll (pEvent:ScrollEvent):Void
	{
		mList.scroll (pEvent.position);
	}
	
	/** Exectued when the list dispatches a resize event */
	private function onResizeScrollBar ():Void
	{
		var percent:Number = mList.getScrollablePercent();
		
		// Set whether the scroll bar should be visible or not
		isScrollBar = percent < 1;
		
		// If the scroll bar is visible, then set it's size
		if (isScrollBar)
			mScrollBar.setScrollSize (percent);
	}
	
	/**
	 * Notified when the user rolls the mouse wheel
	 * @param delta A number indicating how many lines should be scrolled for each notch the user rolls the mouse wheel
	 * @param target A parameter that indicates the topmost movie clip instance under the mouse pointer when the mouse wheel is rolled
	 */
	private function onMouseWheel (delta:Number, target:Object):Void
	{
		// Check to see if the mouse is over the list or the scroll bar....this is one rare occasion where I will use _root
		// I only want the absolute position of the mouse in order to find if the mouse is over this component or not.  Using
		// _root will always work for this situation
		if (mList.hitTest (_root._xmouse, _root._ymouse) || mScrollBar.hitTest (_root._xmouse, _root._ymouse))
			mScrollBar.scrollPosition -= delta / 100;
	}
	
	/**
	 * Get reference to the list component so outside classes can get and set info to and from the list
	 * @return The list component
	 */
	public function getList ():List
	{
		return mList;
	}
	
	/** sends the onListClick event off to the OMM to do what must be done */
	public function onListClick (pEvent:ItemClickEvent):Void
	{
		mOMM.receiveListClick(pEvent);
		//if another movie was playing, set it's play icon back to play
		if(mCurrentlyPlayingIndex != pEvent.index)
			mList.mPlayListItemArray[mCurrentlyPlayingIndex].resetPlayIcon();
		mCurrentlyPlayingIndex = pEvent.index;
	}

/** Triggered by an event from the list component which is triggered by the tweening of the row animations 
	private function onChange (pEvent:Object):Void
	{
		// Whether or not to dispatch a resize event
		var noEvent:Boolean = pEvent.event;
		
		// Set if the scroll bar should be visible or not
		isScrollBar = mList.getScrollablePercent() < 1;

//		// Size the base top component
//		mBaseClip.setSize (mWidth, mBaseClipHeight);
		
//		// Position the base
//		mBGClip._y = mBaseClipHeight;
		
		// Position and size the list and scrollbar
//		mList._y = mBaseClipHeight + Constants.RADIUS;
		mList._y = Constants.RADIUS;
		
//		mList.setSize (mWidth - (isScrollBar ? mScrollBar.width : 0), mHeight - mBaseClipHeight - 2*Constants.RADIUS, noEvent);
		mList.setSize (mWidth - (isScrollBar ? mScrollBar.width : 0), mHeight - 2*Constants.RADIUS, noEvent);
		mScrollBar.setSize (null, mList.height);
		mScrollBar._x = mList._x + mList.width;
		mScrollBar._y = mList._y;
		trace("PlayList.onChange(pEvent)");
	}
*/	
	/** the omm calls this function. it tells the cell within the list that either play or pause has been clicked*/
	public function playOrPauseMedia(pStatus:String):Void
	{
		if(mCurrentlyPlayingIndex != undefined)
		{
			mList.mPlayListItemArray[mCurrentlyPlayingIndex].onPlayOrPauseClick(pStatus);
		}
	}
}
