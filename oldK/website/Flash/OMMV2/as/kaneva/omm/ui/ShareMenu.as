﻿import kaneva.omm.OMM;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.ShareMenuItem;

/**
 * Creates the share menu
 * @author Scott
 */
 //TODO : add a function to the ShareMenuItem which retrieves the width of each graphic in a selection
 //		use that to determine where to set the placement of the text within the 'slide-up'
 //		create another function that sets the text to that new position, returning the width
 //		add that width and the width of the largest pic together to get the maxWidth of the contentClip
 //		... see createShareMenuItems at bottom of this file
class kaneva.omm.ui.ShareMenu extends ComponentDisplay
{
	/** The base for the clip */
	private var mBaseClip:MovieClip;
	
	/** The clip which the items are created in */
	private var mContentClip:MovieClip;
	
	/** Contains info necessary for each share menu item */
	private var mDataProvider:Array;
	
	/** The radius for the corners of the base */
	private var mCornerRadius:Number = 15;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function ShareMenu ()
	{
		super ();
	}
	
	/** Initializes stuff */
	private function init ():Void
	{
		super.init ();
	}
	
	/** Creates movie clips and other objects */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create the base clip
		mBaseClip = this.createEmptyMovieClip ("mBaseClip", this.getNextHighestDepth ());
		
		// Create the content clip
		mContentClip = this.createEmptyMovieClip ("mContentClip", this.getNextHighestDepth ());
	}
	
	/** Draws stuff */
	private function draw ():Void
	{
		super.draw ();
		
		// Draw the base
		drawBase ();
		
	}
	
	/** Sizes and positions stuff */
	private function size ():Void
	{
		super.size ();
	}
	
	/** Draws the base clip */
	private function drawBase ():Void
	{	
		mBaseClip.clear ();
		mBaseClip.lineStyle(0, OMM.getConfig().getAsNumber("share.menu.outline.color"), 100, true);
		mBaseClip.beginFill (OMM.getConfig().getAsNumber("share.menu.background.color"), 100);
		mBaseClip.moveTo(mCornerRadius, 0);
		mBaseClip.lineTo(mWidth - mCornerRadius, 0);
		mBaseClip.curveTo(mWidth, 0, mWidth, mCornerRadius);
		mBaseClip.lineTo(mWidth, mHeight);
		mBaseClip.lineTo(0, mHeight);
		mBaseClip.lineTo(0, mCornerRadius);
		mBaseClip.curveTo(0, 0, mCornerRadius, 0);
		mBaseClip.endFill ();
	}
	
	/** 
	 * Sets the info needed to create the menu items 
	 * @param pValue The array to set as the data provider
	 */
	public function setDataProvider (pValue:Array):Void
	{	
		mDataProvider = pValue;
		
		// Re-create the content clip so any previous items are discared.  We reuse the same depth
		var depth:Number = mContentClip.getDepth ();
		mContentClip = this.createEmptyMovieClip ("mContentClip", depth);
		
		// Create the share menu items
		createShareMenuItems ();
	}
	
	/** Creates the share menu items in the content clip */
	private function createShareMenuItems ():Void
	{
		var max:Number = mDataProvider.length;
		var maxWidth:Number = 0;
		for (var i:Number = 0; i < max; i++)
		{
			var item:ShareMenuItem = ShareMenuItem (mContentClip.attachMovie ("ShareMenuItem", "item" + i, i));
			item._y = i * item.height;
			
			// get the width of the largest cell within the menu and set the width of the content clip to that
			if(maxWidth < item._width)
				maxWidth = item._width;
			
			item.setSize (mWidth, null);
			
			var data:Object = mDataProvider[i];
			item.setLink (data.link);
			item.setIcon (data.icon);
			item.setText (data.text);

		}
		
		// Reposition the content clip so there is a little room at the top where the rounded edges are
		mContentClip._y = mCornerRadius;
		
		// Resize the share menu to accomodate for the items
		setSize (maxWidth, mContentClip._y + mContentClip._height);
	}
}
