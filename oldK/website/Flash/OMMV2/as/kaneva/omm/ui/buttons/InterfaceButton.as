﻿// TODO - This is being done away with.  We no longer have need for this

import mx.transitions.easing.Strong;
import mx.transitions.Tween;
import mx.utils.Delegate;

import flash.geom.Matrix;

import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.shapes.Elipse;
//import flash.filters.DropShadowFilter;

/**
 * Creates the interface buttons for the media player
 * @author Scott
 */
class kaneva.omm.ui.buttons.InterfaceButton extends ComponentDisplay
{
	
	/** Colors for the button base */
	public var outlineColor:Number = 0xDBDBDB;
	public var highlight:Number = 0xFFFFFF;
	public var shadow:Number = 0xEBEAE5;
	
	/** Button base graphic */
	private var mBaseClip:MovieClip;
	
	/** The icon clip for the button */
	private var mIcon:MovieClip;
	
	/** Linkage id of the icon from the library to use*/
	private var mIconLinkage:String = null;
	
	/** Used to change the color of the button on rollover/rollout */
	private var mColor:Color;

	/** Passed to the setTransform method of the Color object */
	private var mTransform:Object;

	/** The initial transform property for the transform object.  This is what the tween object will change */
	private var mTransformProp:Number = 0;

	/** Animates the rollover effect */
	private var mTween:Tween;
	
	/** Drop shadow filter */
	//private var mDropShadow:DropShadowFilter;
	
	/** 
	 * Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class
	 */
	private function InterfaceButton ()
	{
		super ();
	}
	
	/** Create movie clips and other things */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create drop shadow
		//mDropShadow = new DropShadowFilter (3, 20, 0, 0.35, 3, 3);
		
		// Create the base clip
		mBaseClip = this.createEmptyMovieClip("mBaseClip", this.getNextHighestDepth());
		
		// Create the icon clip
		mIcon = this.createEmptyMovieClip("mIcon", this.getNextHighestDepth());
		
		// Set default icon to the gray arrow...this is for when this button is used for the play list items and 
		// when used in the video base.
		setIcon ("PlayGray");
		
		// Set the button events for this button
		this.onRelease = Delegate.create (this, onClick);
		this.onRollOver = this.onDragOver = Delegate.create (this, onRollOverEvent);
		this.onRollOut = this.onDragOut = onReleaseOutside = Delegate.create (this, onRollOutEvent);
		
		// Set up the color object and transform object for the rollover effects
		mColor = new Color (this);
		mTransform = {rb:mTransformProp, gb:mTransformProp, bb:mTransformProp};
	}
	
	/** Draws stuff */
	private function draw ():Void
	{
		super.draw ();
		drawBase ();
		
	}
	
	/** Sizes pieces and stuff */
	private function size ():Void
	{
		super.size ();
		
		// Size the icon if it's the default icon
		if (mIconLinkage == "PlayGray")
		{
			mIcon._width = mWidth / 3;
			mIcon._yscale = mIcon._xscale;
		}
		
		// Position the icon
		mIcon._x = (mWidth - mIcon._width) / 2;
		mIcon._y = (mHeight - mIcon._height) / 2;
	}
	
	/** Draws the base clip */
	private function drawBase ():Void
	{
		// Set up matrix		
		var matrix:Matrix = new Matrix ();
		matrix.createGradientBox (mWidth, mHeight, Math.PI / 3, 0, 0);
		
		mBaseClip.clear ();
		mBaseClip.lineStyle (2, outlineColor, 100, true);
		mBaseClip.beginGradientFill ("linear", [highlight, highlight, shadow, shadow, highlight, highlight], [100, 100, 100, 100, 100, 100], [0, 20, 70, 140, 230, 255], matrix);
		Elipse.draw (mBaseClip, mWidth, mHeight);
		mBaseClip.endFill ();
		
		// Set the drop shadow on the base clip
		//mBaseClip.filters = [mDropShadow];
	}
	
	/** Dispatches a click event when user releases this button */
	private function onClick ():Void
	{
		dispatchEvent ({type:"click", target:this});
	}
	
	/** Method executed on rollover of this button */
	private function onRollOverEvent ():Void
	{
		var top:Number = -25;
		mTween = new Tween (this, "mTransformProp", Strong.easeOut, mTransformProp, top, 0.5, true);
		mTween.onMotionChanged = Delegate.create (this, onPropChange);
	}
	
	/** Method executed on rollout of this button */
	private function onRollOutEvent ():Void
	{
		var bottom:Number = 0;
		mTween = new Tween (this, "mTransformProp", Strong.easeOut, mTransformProp, bottom, 0.5, true);
		mTween.onMotionChanged = Delegate.create (this, onPropChange);
	}
	
	/** Called on the motion changed event of the tween object */
	private function onPropChange ():Void
	{
		mTransform.rb = mTransformProp;
		mTransform.gb = mTransformProp;
		mTransform.bb = mTransformProp;
		mColor.setTransform (mTransform);
	}
	
	/** 
	 * Set the linkage id of the icon for the button
	 * @param pValue The linkage id
	 */
	public function setIcon (pValue:String):Void
	{
		mIconLinkage = pValue;
		mIcon.attachMovie (mIconLinkage, "icon", 0);
		size ();
	}
}