﻿import mx.utils.Delegate;

import kaneva.events.MetaDataEvent;
import kaneva.info.MetaDataInfo;
import kaneva.events.NetStatusEvent;
import kaneva.events.VideoEvent;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.MediaObject;
import kaneva.omm.ui.MediaPlayer;
import kaneva.info.NetStatusInfo;

/**
 * Object which contains the video object used to playback video content.  Video objects in Flash cannot 
 * be created dynamically (unfortunately).  So this clip has the video object on it's timeline.  This class
 * sets up the necessary classes needed to load and control Flash Video files (*.flv).
 * @author Scott
 */
class kaneva.omm.ui.VideoObject extends ComponentDisplay
{
	/** The media video base that houses this component */
	public var owner:MediaObject;
	
	/** 
	 * The video object that plays the external flv.  This object can't be dynamically generated.  
	 * This resides on the stage of the clip which this class extends. 
	 */
	private var mVideo:Video;
	
	/** Object that provides the means to play back streaming FLV files from a local drive or HTTP address */
	private var mNetConnection:NetConnection;
	
	/** Object that provides methods and properties for playing Flash Video (FLV) files from the local file system or an HTTP address */
	private var mNetStream:NetStream;
	
	/** Interval that runs while an external video file (.flv) is loading */
	private var mLoadInterval:Number;
	
	/** Movie clip used for the target of the Sound object and used to control the video sound */
	private var mSoundControlClip:MovieClip;
	
	/** Sound object used to control video sound */
	private var mSoundObj:Sound;
	
	/** Used to tell if the audio is playing or not....this is used to tell if it's buffering */
	private var mPrevPosition:Number = 0;
	
	/** 
	 * Interval used to check to see if the video has screwed up.  Sometimes, the NetStream.Play.Stop event doesn't get called
	 * and the video sticks.  We want this interval to check and see if the video is supposed to be playing but the time property
	 * of the netstream object isn't changing over time.  If this is the case, it is stuck and we need to move on by dispatching
	 * the NetStream.Play.Stop event.  This interval won't get called until the NetStream.Buffer.Flush event is called because at that
	 * point, we know that the video is very close to the end.
	 */
	private var mErrorInterval:Number;
	 
	/** 
	 * Variable used to hold a value of the time property of the netstream object.  
	 * This is what we will compare the current value of the time property with 
	 */
	private var mPrevTime:Number;
	 
	/** Interval used to check for the width and height of the video object.  This is sort of work around for video that has no metadata */
//	private var mDimensionInterval:Number;
	
	/**
	 * This is used to keep track of the total bytes loaded for a piece of video media.  This is used to make sure we actually
	 * have the correct value for the total bytes.  There seems to be an issue with NetStream.bytesTotal when the download/stream
	 * gets disrupted.  It will come back as the last value for NetStream.bytesLoaded.  This is no good because checking to see
	 * if the media has finished downloading will be wrong.  Since bytesLoaded and bytesTotal will be the same, it will appear as
	 * though downloading is done
	 */
	private var mTotalBytes:Number = 0;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function VideoObject ()
	{
		super ();
	}

	/** Initialize things */
	private function init ():Void
	{
		super.init ();
		
		// Set reference to the media object which this component resides in 
		owner = MediaObject (this._parent);
		
		// Create a NetConnection object
		mNetConnection = new NetConnection ();
		
		// Create a local streaming connection
		mNetConnection.connect (null);
		
		// Create a NetStream object
		mNetStream = new NetStream (mNetConnection);
		
		// Attach the NetStream video feed to the Video object which is on the stage
		mVideo.attachVideo (mNetStream);
	
		// Set meta data event to dispatch an event
		mNetStream.onMetaData = Delegate.create(this, onMetaData);
		
		// Set status event to dispatch an event
		mNetStream.onStatus = Delegate.create(this, onStatus);
	}
	
	/** Create movie clips and other things */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create the sound control mc
		mSoundControlClip = this.createEmptyMovieClip ("mSoundControlClip", this.getNextHighestDepth ());
	}
	
	/** Sizes pieces and stuff */
	private function size ():Void
	{
		super.size ();
		
// TODO - MTASC has errors for some reason.  It says that Video does not have _width or _height properties
// This is a hack ass way around this issue....need to figure out how to fix this later
//		
// 		Size the video
//		mVideo._width = mWidth;
//		mVideo._height = mHeight;
		this["mVideo"]._width = mWidth;
		this["mVideo"]._height = mHeight;
	}
	
	/**
	 * Set the total bytes for the netstream.  This checks to make sure that the value is bigger than any previous value and
	 * not smaller.  This helps ensure it doesn't get reset to the last loaded value when the internet connection gets cut 
	 * off prematurely.
	 * @param pTotalBytes The total bytes
	 */
	private function setTotalBytes(pTotalBytes:Number):Void
	{
		// If the total bytes to set is greater than the current value......then set it
		if(pTotalBytes > mTotalBytes)
			mTotalBytes = pTotalBytes;
	}
	
	/**
	 * Resets the value of the total bytes property
	 */
	private function resetTotalBytes():Void
	{
		mTotalBytes = 0;
	}
	
	/** 
	 * Called by the load interval.  This dispatches a progress event letting any listeners know about
	 * the video's current load status.
	 */
	private function onVideoProgress ():Void
	{
		var loaded:Number = mNetStream.bytesLoaded;
		
//		var total:Number = mNetStream.bytesTotal;
		
		// Set the total bytes into a class property.  This makes sure it doesn't get reset to the loaded value if the 
		// user's internet connection cuts out
		setTotalBytes(mNetStream.bytesTotal);
		
		var percentDownloaded:Number = loaded / mTotalBytes;
		
//		trace("VideoObject.onVideoProgress("+loaded + " / " + mTotalBytes+")");
		
		// Calculate how close the buffer is to being full
		var bufferPercent:Number = mNetStream.bufferLength / mNetStream.bufferTime;
		
		// If the previous position equals the current position and buffer percent is less than 1...then the video is probably not playing and therefore is buffering
		var isBuffering:Boolean = mPrevPosition == mNetStream.time && bufferPercent < 1;
		
		// Create the video event
		var videoEvent:VideoEvent = new VideoEvent();
		videoEvent.soundObject = mSoundObj;
		videoEvent.netStreamObject = mNetStream;
		
		// If the video is done loading......clear the interval, reset the previous position property, 
		// and return out of this method so that the progress event is not dispatched
		if (loaded > 0 && mTotalBytes > 0 && percentDownloaded >= 1)
		{
			clearInterval (mLoadInterval);
			
			// Dispatch a video complete event
			videoEvent.type = VideoEvent.COMPLETE;
			dispatchEvent(videoEvent);
			
			mPrevPosition = 0;
			return;
		}
		
		// Dispatch a progress event
		videoEvent.type = VideoEvent.PROGRESS;
		videoEvent.bufferPercent = bufferPercent;
		videoEvent.isBuffering = isBuffering;
		videoEvent.bytesLoaded = loaded;
		videoEvent.bytesTotal = mTotalBytes;
		dispatchEvent(videoEvent);
		
		// Set the previous position so that we have a reference to the position the last time this method was called by the interval.
		// If the current position is the same as this variable, then we can assume that the video is not playing and therefore, it
		// is probably buffering
		mPrevPosition = mNetStream.time;
	}
	
	/**
	 * Invoked when the Flash Player receives descriptive information embedded in the FLV file being played
	 * @param pInfo An object containing one property for each metadata item
	 */
	private function onMetaData(pInfo:MetaDataInfo):Void
	{
		var metaDataEvent:MetaDataEvent = new MetaDataEvent();
		metaDataEvent.type = MetaDataEvent.META_DATA;
		metaDataEvent.info = pInfo;
		dispatchEvent(metaDataEvent);
	}
	
	/** 
	 * Invoked every time a status change or error is posted for the NetStream object
	 * @param pInfo A parameter defined according to the status message or error message
	 */
	private function onStatus (pInfo:NetStatusInfo):Void
	{
//		trace("VideoObject.onStatus(" + pInfo.code + ")");
		
		// Set the error interval to start checking if the video sticks or not
		if (pInfo.code == "NetStream.Buffer.Flush")
		{
//			trace("Error Hack normally happens here");
			
			clearInterval (mErrorInterval);
			mErrorInterval = setInterval (this, "checkVideoError", 500);
			mPrevTime = mNetStream.time;
		}
			
		// If stop status code is sent, clear the interval
		if (pInfo.code == "NetStream.Buffer.Stop")
			clearInterval (mErrorInterval);
		
		var netStatusEvent:NetStatusEvent = new NetStatusEvent();
		netStatusEvent.type = NetStatusEvent.NET_STATUS;
		netStatusEvent.info = pInfo;
		dispatchEvent(netStatusEvent);
	}
	
	/** Method called by the mErrorInterval which checks if the video gets stuck */
	private function checkVideoError ():Void
	{
		// Get reference to the media player
		var mp:MediaPlayer = owner.owner.owner;
		
		// Need to see if the video is supposed to be playing and if the time property of the netstream obj
		// is changing over time.
		if (mp.status == MediaPlayer.PLAYING && mPrevTime == mNetStream.time)
		{
			// Make sure we clear the interval
			clearInterval (mErrorInterval);
			
			// Dispatch a status event....the info object dispatches a NetStream.Play.Stop event so we can fake that it's done
			var netStatusEvent:NetStatusEvent = new NetStatusEvent();
			var netStatusInfo:NetStatusInfo = new NetStatusInfo();
			netStatusInfo.code = "NetStream.Play.Stop";
			netStatusEvent.type = NetStatusEvent.NET_STATUS;
//			netStatusEvent.info = {code:"NetStream.Play.Stop"};
			netStatusEvent.info = netStatusInfo;
			dispatchEvent(netStatusEvent);
		}
		// Need to clear the ErrorInterval if the mediaplayer is stopped.
		else if (mp.status == MediaPlayer.STOPPED)
		{
			clearInterval (mErrorInterval);
		}
		
		// Set the previous time
		mPrevTime = mNetStream.time;
	}
	
	/** 
	 * Public accessor for the netstream object that controls the video.  This is done so outside classes can control the video
	 * @return The netstream object that controls the video
	 */
	public function getNetStream ():NetStream
	{
		return mNetStream;
	}
	
	/**
	 * Loads a flv video file into the nestream object
	 * @param pUrl The location on a server of an flv video file
	 * @param pThumb True if we only want to load enough video to generate a thumbnail preview, otherwise load entire video
	 */
	public function load (pUrl:String/*, pThumb:Boolean*/):Void
	{
		// Make sure to reset the total bytes property before downloading a new video
		resetTotalBytes();
		
		// Make sure we clear the interval
		clearInterval (mErrorInterval);
		
		// Close out any previous downloads
		close ();
		
		// Attach the NetStream video feed to the Video object which is on the stage
		mVideo.attachVideo (mNetStream);
		
		// Specifies how long to buffer messages before starting to display the stream. 
		// For example, if you want to make sure that the first 5 seconds of the stream play 
		// without interruption, set numberOfSeconds to 5; Flash begins playing the stream 
		// only after 5 seconds of data are buffered
		mNetStream.setBufferTime(1);
		
		// Load the video
		mNetStream.play (pUrl);
		
		// Attach the audio from the FLV file to the control movie clip
		mSoundControlClip.attachAudio (mNetStream);
		
		// Create the sound object used to control the video sound
		mSoundObj = new Sound (mSoundControlClip);

		// Start the interval which checks download progress and dispatches event for listeners to do something with
		clearInterval (mLoadInterval);
		mLoadInterval = setInterval (this, "onVideoProgress", 250);
		
//		// Start the interval which checks for the width and height properties of the video object
//		clearInterval (mDimensionInterval);
//		mDimensionInterval = setInterval (this, "checkDimensions", 100);
	}
	
	/** Check to see if the width and height of the video object exist yet */
//	private function checkDimensions ():Void
//	{
//		// If the width and height of the video object are available....
//		if (mVideo.width > 0 && mVideo.height > 0)
//		{
//			// We want to dispatch meta data event and send the aspect ratio dimensions
//			var metaDataEvent:MetaDataEvent = new MetaDataEvent();
//			metaDataEvent.type = MetaDataEvent.META_DATA;
//			
//			var mdi:MetaDataInfo = new MetaDataInfo();
//			mdi.width = mVideo.width;
//			mdi.height = mVideo.height;
//			metaDataEvent.info = mdi;
//			dispatchEvent (metaDataEvent);
//			
//			// Clear the interval
//			clearInterval (mDimensionInterval);
//		}
//	}
	
	/** 
	 * Sets the volume 
	 * @param pValue The volume level (0 - 100)
	 */
	public function setVolume (pValue:Number):Void
	{
		mSoundObj.setVolume (pValue);
	}
	
	/** 
	 * Gets the volume 
	 * @return The volume level (0 - 100)
	 */
	public function getVolume ():Number
	{
		return mSoundObj.getVolume ();
	}
	
	/** Stops the download progress of whatever video is being downloaded */
	public function close ():Void
	{
		// Double check to make sure the interval isn't still running
		clearInterval (mLoadInterval);
		
		// Clear out any previous video download 
		mNetStream.close();
		
		// Clear out previous video
		mVideo.clear();
	}
	
}
