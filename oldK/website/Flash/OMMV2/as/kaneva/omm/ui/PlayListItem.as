﻿import mx.transitions.easing.Regular;
import mx.transitions.Tween;
import mx.utils.Delegate;

import kaneva.events.ItemClickEvent;
import kaneva.events.LoadEvent;
import kaneva.events.MouseEvent;
import kaneva.events.ResizeEvent;
import kaneva.media.MediaAsset;
import kaneva.omm.CSS;
import kaneva.omm.OMM;
import kaneva.omm.ui.buttons.PlayPauseButton;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.ImageObject;
import kaneva.omm.ui.List;
import kaneva.omm.ui.shapes.Rect;
import kaneva.util.Strings;

/**
 * Represents a piece of media in the play list component.  This shows a thumbnail image, show text info about the media, and
 * should dispatch information about the media to be played.
 * @author Scott
 */
class kaneva.omm.ui.PlayListItem extends ComponentDisplay
{
	/** The index value for this row */
	public var index:Number;
	
	/** 
	 * The text colors when the text is selected and not selected.  These have to be strings with # symbol as a prefix
	 * because they are used in style sheet objects
	 */
	public var textSelectedColor:String;
	public var textColor:String;
	
	/** The list that this row is in */
	public var owner:List;
	
	/** If this item is selected or not */
	public var selected:Boolean = false;
	
	/** Lets us know if the image has been loaded yet */
	public var hasImageLoaded:Boolean = false;

	/** an image of the rating of the selected clip*/
	private var mRatingMovie:MovieClip;
	
	/** The clip that is the background for this item....it can be set to different colors */
	private var mBackgroundClip:MovieClip;
	
	/** The color of the background for this item */
	private var mBackgroundColor:Number = 0xFFFFFF;
	
	/** The selection clip....this acts as the hitarea and is animated to show selection */
	private var mSelectionClip:MovieClip;
	
	/** An html Text field that shows the title and user */
	private var mTitleTextField:TextField;
	
	/** An html Text field that shows the description */
	private var mDescTextField:TextField;
	
	/** the position of this cell, relative to the other cells.  1,2,3,....*/
	private var mClipNumber:Number;
	
	/** the total number of cells in the playList*/
	private var mClipTotal:Number;
	
	/** an html text field that shows "clip x of y" */
	//private var mClipOfTotalTextField:TextField;
	
	/** Style sheet object for the title and field text fields */
	private var mCSS:TextField.StyleSheet;
	
	/** The media asset object that contains the info needed for this movie */
	private var mMediaAsset:MediaAsset;
	
	/** The play button component that loads the movie */
	private var mPlayPauseButton:PlayPauseButton;
	
	/** set the rating of the item that was loaded into this play list item.  We keep track of this so we only load the thumb when necessary*/
	private var mRating:String;
	
	/** Used to animate the selection base */
	private var mSelectionTween:Tween;
	
	/** Used to animate the image fade in */
	private var mImageTween:Tween;
	
	/** The alpha value of the selection clip when the item is selectd */
	private var mSelectionAlpha:Number = 100;
	
	/** The alpha value of the selection clip when the item is rolled over */
	private var mRollAlpha:Number = 50;
	
	/** Clip that the border is drawn in */
	private var mBorderClip:MovieClip;

	/** Object used to load in thumbnail images for items in a playlist */
	private var mImageObj:ImageObject;
	
	/** The image to load for the thumbnail.  We hold reference to it so we only load it when it is needed */
	private var mThumbnailLocation:String;
	
	/** The clip which contains the outline for the thumb nail image */
	private var mImageClip:MovieClip;
	
	/** Distance the image is away from the top and sides */
	private var mImageGap:Number = 18;
	
	/** Dimensions of the image clip */
	private var mImageWidth:Number = 83;
	private var mImageHeight:Number = 65;
	
	/** The height the description text field needs to be in order to show all it's contents */
	private var mDescriptionHeight:Number;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function PlayListItem ()
	{
		super ();
	}
	
	private function init():Void
	{
		super.init();
		
		// Set the list component reference
		owner = List (this._parent._parent);
		
		// Listen for the owners resize event
		owner.addEventListener (ResizeEvent.RESIZE, Delegate.create (this, onResize));
		
		// Listen for the scroll event from the owner
//		owner.addEventListener ("scroll", Delegate.create (this, onScroll));
		
	}
	
	/** Create movie clips and other things */
	private function createChildren ():Void
	{
		super.createChildren ();
		
		// Create the background clip
		mBackgroundClip = this.createEmptyMovieClip ("mBackgroundClip", this.getNextHighestDepth ());
		
		// Create the selected item clip and set events
		mSelectionClip = this.createEmptyMovieClip ("mSelectionClip", this.getNextHighestDepth ());
		mSelectionClip.onRelease = Delegate.create (this, onItemRelease);
		mSelectionClip._alpha = 0;
		setItemEvents ();
		
		// Create the CSS for the content text
		mCSS = CSS.getPlayListStyle();
		textSelectedColor = mCSS.getStyle(".selected").color;
		textColor = mCSS.getStyle(".unselected").color;
		
		// Create the text field that displays the title
		mTitleTextField = this.createTextField ("mTitleTextField", this.getNextHighestDepth (), 0, 0, 0, 0);
		
		// Create the text field that displays the description
		mDescTextField = this.createTextField ("mDescTextField", this.getNextHighestDepth (), 0, 0, 0, 0);
		
		//mClipOfTotalTextField = this.createTextField ("mClipOfTotalTextField", this.getNextHighestDepth (), 0, 0, 0, 0);
		//mClipOfTotalTextField.autoSize = true;
		
		// Set properties for the text fields
		mTitleTextField.html = mDescTextField.html = /*mClipOfTotalTextField.html =*/ true;
		mTitleTextField.multiline = mDescTextField.multiline = true;
		mTitleTextField.selectable = mDescTextField.selectable = /*mClipOfTotalTextField.selectable =*/ false;
		mTitleTextField.styleSheet = mDescTextField.styleSheet = /*mClipOfTotalTextField.styleSheet =*/ mCSS;
		mTitleTextField.wordWrap = mDescTextField.wordWrap = true;
		
		//mTitleTextField.border = mDescTextField.border = true;
		//mTitleTextField.borderColor = mDescTextField.borderColor = 0xFFFFFF;
		
		// Create the clip which the image will be loaded
		mImageClip = this.createEmptyMovieClip ("mImageClip", this.getNextHighestDepth ());
		
		// Create the image object used to load thumbnail images
		mImageObj = ImageObject (this.attachMovie ("ImageObject", "mImageObj", this.getNextHighestDepth ()));
		mImageObj.setSize (mImageWidth, mImageHeight);
		
		// Listen for when the image object has fully loaded the image
		mImageObj.addEventListener (LoadEvent.LOAD, Delegate.create(this, setImageSize));
		
		// Draw the thick white border for the thumbnail
		mImageClip.lineStyle (8, 0xFFFFFF, 100, false, "none", "none", "round");
		mImageClip.beginFill (0xFFFFFF, 100);
		Rect.draw (mImageClip, mImageWidth, mImageHeight);
		mImageClip.endFill ();
		
		// Create the play button
		mPlayPauseButton = PlayPauseButton(this.attachMovie ("PlayPauseButton", "mPlayPauseButton", this.getNextHighestDepth ()));
		mPlayPauseButton.addEventListener(MouseEvent.CLICK, Delegate.create (this, onPlayPauseButtonClick));
		
		// clip that the rating graphic is loaded into
		mRatingMovie = this.createEmptyMovieClip ("mRatingMovie", this.getNextHighestDepth ());
		
		// Clip the dashed line is drawn in 
		mBorderClip = this.createEmptyMovieClip ("mBorderClip", this.getNextHighestDepth ());
	}
	
	/** Draw stuff */
	private function draw ():Void
	{
		// Draw the background
		drawBackground ();
		
		// Draw the selection 
		drawSelection ();
		
		// Draw a border line
		drawBorder ();
	}
	
	/** Sizes pieces and stuff */
	private function size ():Void
	{
		super.size ();
		
		mImageClip._x = mImageClip._y = mImageGap;
		
		mPlayPauseButton._x = mWidth - mPlayPauseButton._width - 10;
		mPlayPauseButton._y = mImageClip._y;
		
		//mClipOfTotalTextField._x = mPlayPauseButton._x + mPlayPauseButton._width - mClipOfTotalTextField._width;
		//mClipOfTotalTextField._y = mPlayPauseButton._y + mPlayPauseButton._height + 5;
		
		mTitleTextField._x = mImageClip._x + mImageClip._width;
		mTitleTextField._y = mImageClip._y - 5;
		
		mRatingMovie._x = mPlayPauseButton._x + (mPlayPauseButton._width/2 - mRatingMovie._width/2); 
		mRatingMovie._y = mPlayPauseButton._y + mPlayPauseButton._height + 10;
		
		mTitleTextField._width = mPlayPauseButton._x - mTitleTextField._x - 5;
		mTitleTextField._height = owner.rowHeight - mTitleTextField._y;
		
		mDescTextField._x = mTitleTextField._x;
		mDescTextField._y = mTitleTextField._y + mTitleTextField._height;
		mDescTextField._width = mWidth - mDescTextField._x;
		mDescTextField._height = mHeight - mDescTextField._y;
//		setSelectedHeight (mDescTextField.getNewTextFormat().getTextExtent(mDescTextField.text, mDescTextField._width).textFieldHeight);
		
		// Position the line
		//mBorderClip._x = mImageClip._x;
		mBorderClip._y = mHeight - mBorderClip._height;
		
		// Position the image clip
		positionImage();
	}
	
	/** Draws the background clip for the item */
	private function drawBackground ():Void
	{	
		mBackgroundClip.clear ();
		mBackgroundClip.beginFill (mBackgroundColor, 100);
		Rect.draw (mBackgroundClip, mWidth, mHeight);
		mBackgroundClip.endFill ();
	}
	
	/** Draws the selection clip for the item */
	private function drawSelection ():Void
	{
		mSelectionClip.clear ();
		mSelectionClip.beginFill (OMM.getConfig().getAsNumber("playlist.item.selected.color"), 100);
		Rect.draw (mSelectionClip, mWidth, mHeight);
		mSelectionClip.endFill ();
	}
	
	/** 
	 * Draws a border which is used to separate each play list item
	 * @param pClip The movieclip timeline on which to draw the line
	 * @param pWidth The width the line needs to be
	 */
	private function drawBorder ():Void
	{
		mBorderClip.clear ();
		mBorderClip.lineStyle (1, 0xD8D8D8, 100);
		mBorderClip.lineTo (mWidth/* - mImageClip._x*/, 0);
		mBorderClip.moveTo (0, 0);
		mBorderClip.lineStyle (1, 0xF2F2F2, 100);
		mBorderClip.moveTo (0, 1);
		mBorderClip.lineTo (mWidth/* - mImageClip._x*/, 1);		
	}
	
	/** 
	 * The listener method for the play button 
	 * @param pEvent Object sent by dispatcher
	 */
	private function onPlayPauseButtonClick (pEvent:MouseEvent):Void
	{
		// If this dude is not selected....then set the list index to this one's index
		if (!selected)
			owner.selectedIndex = this.index;
			
		// Make the list component dispatch a click event
//		owner.dispatchEvent({type:"click", target:owner, media:mMediaAsset, currentlyPlaying:this.index});
		
		// Make the list component dispatch an item click event
		var event:ItemClickEvent = new ItemClickEvent();
		event.type = ItemClickEvent.ITEM_CLICK;
		event.index = this.index;
		event.item = mMediaAsset;
		event.relatedObject = owner;
		owner.dispatchEvent(event);
	}
	
	/** called by playList.  tells the cell whether play or pause has been clicked*/
	public function onPlayOrPauseClick(pStatus:String):Void
	{
		if(pStatus == "PLAYING")
			mPlayPauseButton.setIcon (PlayPauseButton.PAUSE);
		else
			mPlayPauseButton.setIcon (PlayPauseButton.PLAY);
	}
	
	/** resets the play button back to an 'unplayed' state*/
	public function resetPlayIcon():Void
	{
		mPlayPauseButton.setIcon("play");
	}
	
	/** The rollover event for this item */
	private function onItemRollOver ():Void
	{
		var alpha:Number = selected ? mSelectionAlpha : mRollAlpha;
		mSelectionTween = new Tween (mSelectionClip, "_alpha", Regular.easeOut, mSelectionClip._alpha, alpha, 0.25, true);
	}
	
	/** The rollout event for this item */
	private function onItemRollOut ():Void
	{
		mSelectionTween = new Tween (mSelectionClip, "_alpha", Regular.easeOut, mSelectionClip._alpha, 0, 0.25, true);
	}
	
	/** The release event for this item, this sets the selection to this item */
	private function onItemRelease ():Void
	{
		owner.selectedIndex = selected ? undefined : this.index;
	}
	
	/** Executed when a resize event is dispatched */
	private function onResize ():Void
	{
		// Make the width of this item the same as it's owner (List)
		this.width = owner.width;
	}
	
	/** 
	 * Triggered when this item is selected...this is called in the list class
	 * @param pSelected True is selected, false otherwise
	 */
	public function onSelect (pSelected:Boolean):Void
	{
		selected = pSelected;
		setItemEvents ();
		
		// Set the height of the description text field depending on whether this item is selected or not.  This gives the 
		// item a real representation of the _height property when not selected so that the List content clip can form
		// to the actual size and there is no gap at the bottom due to content extending below what is visible
//		mDescTextField._height = pSelected ? mDescriptionHeight : 0;
		
		// Call roll event
		if(pSelected)
			onItemRollOver();
		else
			onItemRollOut();

		mDescTextField.textColor = mTitleTextField.textColor = Strings.getColor (pSelected ? textSelectedColor : textColor);
	}
	
	/** Sets the rollover and rollout events to the selection clip */
	private function setItemEvents ():Void
	{
		// If this item is selected...
		if (selected)
		{
			// Then delete the rollover and rollout events so that the item stays highlighted
			delete mSelectionClip.onRollOut;
			delete mSelectionClip.onRollOver;
			delete mSelectionClip.onDragOut;
			delete mSelectionClip.onDragOver;
		}
		else
		{
			// Set the event back so that they react normally
			mSelectionClip.onRollOut = mSelectionClip.onDragOut = Delegate.create (this, onItemRollOut);
			mSelectionClip.onRollOver = mSelectionClip.onDragOver = Delegate.create (this, onItemRollOver);
		}
	}
	
	/** 
	 * Sets the media asset for this item....also set the media asset data in the text fields
	 * @param pValue The media asset object
	 */
	public function setMediaAsset (pValue:MediaAsset, pClipNumber:Number, pClipTotal:Number):Void
	{
		// Set the media asset for this item
		mMediaAsset = pValue;
		mClipNumber = pClipNumber;
		mClipTotal = pClipTotal;
		
		// Set the reference to the image location
		mThumbnailLocation = mMediaAsset.thumb;

		setTitleTextField(mMediaAsset);
		
		// Set the description text field
		mDescTextField.htmlText = "<span class='body'>" +
//		"<span class='subtitle'>Desc:</span> " + mMediaAsset.descriptionValue + 
//		"<br><br>" + 
//		"<span class='subtitle'>Tags:</span> " + mMediaAsset.tags + 
		"</span>";
		
		// set the clip count
		//mClipOfTotalTextField.htmlText = "<p class='body'>" +
		//"Clip " + mClipNumber + " of " + mClipTotal + "</p>";
		
		// Get the height of the description text field once it's content has been loaded
//		setSelectedHeight (mDescTextField.getNewTextFormat().getTextExtent(mDescTextField.text, mDescTextField._width).textFieldHeight);
		
		// load rating graphic
		if(mMediaAsset.mature)
			mRatingMovie.attachMovie("Restricted", "rating", 0);
	}
	
	/** Loads the image into the image object */
	public function loadThumbnail ():Void
	{
		// If the thumbnail hasn't been loaded yet....
		if (hasImageLoaded == false)
		{
			// Load the thumb nail image
			mImageObj.load (mThumbnailLocation);
			
			// Set the property to true so we don't reload the image again
			hasImageLoaded = true;
		}
	}
	
	/**
	 * Sets the color for the background of this item
	 * @param pValue The color of the background
	 */
	public function setBackgroundColor (pValue:Number):Void
	{
		mBackgroundColor = pValue;
		drawBackground ();
	}
	
	/** sets the size and location of MP3 thumbnail or Image*/ 
	private function setImageSize():Void
	{
		// Fade in the image
		mImageTween = new Tween (mImageObj, "_alpha", Regular.easeOut, 0, 100, 0.5, true);
		
		// Figure out the proportion factor for each dimension
		var wProportion:Number = mImageObj.origWidth ;// mImageWidth; // 83
		var hProportion:Number = mImageObj.origHeight ;// mImageHeight; // 65
		
		// See which dimension proportion is greater and adjust the other dimension accordingly so 
		// it stays proportional with the original dimensions of the video
		if (wProportion > hProportion)
		{
			var newHeight:Number = mImageWidth * mImageObj.origHeight / mImageObj.origWidth;
			mImageObj.setSize (mImageWidth , newHeight );
		}
		else if (wProportion < hProportion)
		{
			var newWidth:Number = mImageHeight * mImageObj.origWidth / mImageObj.origHeight;
			mImageObj.setSize (newWidth , mImageHeight );
			
		}
		else
		{
			mImageObj.setSize (mImageWidth , mImageHeight );
		}
		
		positionImage();
	}
	
	/** set the position of the image */
	private function positionImage():Void
	{
		mImageObj._x = mImageGap + (mImageWidth - mImageObj.width) / 2;
		mImageObj._y = mImageGap + (mImageHeight - mImageObj.height) / 2 ;		
	}
	
	/**
	 * Set the height of the play list item when it is selected 
	 * @param pValue The height the item will be when it is selected
	 */
//	private function setSelectedHeight (pValue:Number):Void
//	{
//		mDescriptionHeight = pValue;
//	}
	
	/**
	 * Get the height of the play list item when it is selected
	 * @return Should be rowHeight plus the height of the description text field
	 */
	public function getSelectedHeight ():Number
	{
		return owner.rowHeight; // + mDescriptionHeight;
	}
	
	
	/** 
	 * sets the textfield data displaying the Title, User, Runtime, Views, and Raves
	 * @param pMediaAsset object that contains all of the information that will be used to display to the user
	 */
	public function setTitleTextField(pMediaAsset:MediaAsset):Void
	{
		// Set the user name (and link if specified)
		var user:String = pMediaAsset.userUrl ? "<a href=\"" + pMediaAsset.userUrl + "\">" + pMediaAsset.user + "</a>" : pMediaAsset.user;
		
		// Set the title text field
		mTitleTextField.htmlText = "<p class='body'>" + 
		"<textformat leading='2'>" +
		"<p class='title'>" + pMediaAsset.title + "</p>" +
		"by " + user + 
		"</textformat>" + 
		"<br><br>" + 
		"<textformat leading='-2'>" + 
		"<span class='subtitle'>Runtime:</span> " + pMediaAsset.length + 
		"<br><span class='subtitle'>Views:</span> " + pMediaAsset.views + 
		"<br><span class='subtitle'>Raves:</span> " + pMediaAsset.thumbsup + 
		"</textformat>" + 
		"</p>";		

		// used to keep the text color the appropriate color once a change has been made
		if(selected)
			onSelect(true);
	}
}
