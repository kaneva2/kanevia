﻿import kaneva.events.LoadEvent;
import kaneva.omm.ui.ComponentDisplay;
import kaneva.omm.ui.shapes.Rect;

/**
 * Object which contains the image object used to display image content.  
 * @author Kevin
 */
class kaneva.omm.ui.ImageObject extends ComponentDisplay
{
	/** loads an image that is displayed in a row/cell*/
	private var mClipLoader:MovieClipLoader;
	
	/** Clip contained within the image clip....this is clip which thumb nail is loaded into */
	private var mImagePH:MovieClip;
	
	/** Clip which contains the loader bar graphic indicating the progress of the thumbnail download */
	private var mLoaderBar:MovieClip;	
	
	/** Set the total width of the loader bar */
	private var mLoaderWidth:Number;
	
	/** Clip the contains the border for the video */
	private var mBorderClip:MovieClip;	
	
	/** Original width and height of the image which is used to keep aspect ratio when resizing the image */
	public var origWidth:Number;
	public var origHeight:Number;

	/** True if need to show an outline */
	private var mShowOutline : Boolean;
	
	/** Constructor.....linked to a movieclip in the library so we don't want anyone to instantiate this class */
	private function ImageObject()
	{
		super();
	}
	
	/** Initialize things */
	private function init():Void
	{
		super.init();
		
		// Create the movie clip loader used to load the image and set this class as the listener
		mClipLoader = new MovieClipLoader();
		mClipLoader.addListener(this);		
	}
	
	/** Create movie clips and other things */
	private function createChildren ():Void
	{
		super.createChildren();
		
		createImagePlaceHolder ();
		
		// Create the clip which houses the preloader bar and position it so it's centered with the image area
		mLoaderBar = this.createEmptyMovieClip ("mLoaderBar", this.getNextHighestDepth ());
		
		// Create a border clip
		mBorderClip = this.createEmptyMovieClip ("mBorderClip", this.getNextHighestDepth ());		
	}
	
	/** Create the place holder clip for the image to load */
	private function createImagePlaceHolder ():Void
	{
		// Create the clip which the image will be loaded
		mImagePH = this.createEmptyMovieClip ("mImagePH", 0);
	}
	
	/** Draw stuff */
	private function draw ():Void
	{
		super.draw ();
			
		// Draw the outline border
		if(mShowOutline)
			drawOutline();
	}
	
	/** Sizes pieces and stuff */
	private function size ():Void
	{
		super.size ();	
		
		// Only resize the placeholder if the original width and height properties have been defined
		if (origWidth && origHeight)
			sizePlaceHolder ();
		
		// Set the loader bar width
		mLoaderWidth = mWidth * 3/4;
		
		// Set position of the loader bar
		mLoaderBar._x = (mWidth - mLoaderWidth) / 2;
		mLoaderBar._y = mHeight / 2;
	}
	
	/**
	 * Invoked when a call to MovieClipLoader.loadClip() has begun to download a file
	 * @param pTarget A movie clip loaded by the MovieClipLoader.loadClip() method
	 */
	private function onLoadStart (pTarget:MovieClip):Void
	{
		// Get rid of any previous values
		origWidth = 100;
		origHeight = 100;
		
		// Show the loader bar clip
		mLoaderBar._visible = true;
	}
	
	/**
	 * Invoked every time the loading content is written to the hard disk during the loading process 
	 * @param pTarget A movie clip loaded by the MovieClipLoader.loadClip() method
	 * @param pLoaded The number of bytes that had been loaded when the listener was invoked
	 * @param pTotal The total number of bytes in the file being loaded
	 */
	private function onLoadProgress (pTarget:MovieClip, pLoaded:Number, pTotal:Number):Void 
	{
    	// Draw the progress bar
		var percent:Number = pLoaded / pTotal;
		mLoaderBar.clear ();
		mLoaderBar.lineStyle(2, 0x990000, 100);
		mLoaderBar.lineTo (percent * mLoaderWidth, 0);
	}

	/**
	 * Invoked when a file that was loaded with MovieClipLoader.loadClip() is completely downloaded 
	 * When this listener has been invoked, you can set properties, use methods, and otherwise interact with the loaded movie
	 * @param pTarget A movie clip loaded by the MovieClipLoader.loadClip() method
	 * @param pHttpStatus The HTTP status code returned by the server. For example, a status code of 404 indicates that the server has not found anything matching the requested URI. For more information about HTTP status codes, see sections 10.4 and 10.5 of the HTTP specification at ftp://ftp.isi.edu/in-notes/rfc2616.txt
	 */
	private function onLoadComplete (pTarget:MovieClip, pHttpStatus:Number):Void
	{
		// For some reason, when images are big, they would have a jumping effect.  They would show up, then alpha in.  Not sure why, but this event
		// gets called before the onLoadInit method so setting alpha here seems to do the trick
		this._alpha = 0;
		origWidth = 100;
		origHeight = 100;

		// Hide the loader bar clip
		mLoaderBar._visible = false;
	}
	
	/**
	 * Invoked when the actions on the first frame of the loaded clip have been executed. 
	 * When this listener has been invoked, you can set properties, use methods, and otherwise interact with the loaded movie
	 * @param pTarget A movie clip loaded by the MovieClipLoader.loadClip() method
	 */
	private function onLoadInit (pTarget:MovieClip):Void
	{
		setDimensions();
		
		// Set the size of the image to proportionally fit this object
		size();
		
		// Dispatch a load event letting any listeners know that the image has fully loaded
		var loadEvent:LoadEvent = new LoadEvent();
		loadEvent.type = LoadEvent.LOAD;
		dispatchEvent(loadEvent);
	}
	
	/**
	 * Invoked when a file loaded with MovieClipLoader.loadClip() has failed to load. 
	 * This listener can be invoked for various reasons; for example, if the server is down, 
	 * the file is not found, or a security violation occurs
	 * @param pTarget A movie clip loaded by the MovieClipLoader.loadClip() method
	 * @param pErrorCode A string that explains the reason for the failure, either "URLNotFound" or "LoadNeverCompleted".
	 * @param pHttpStatus The HTTP status code returned by the server. For example, a status code of 404 indicates that the server has not found anything that matches the requested URI. For more information about HTTP status codes, see sections 10.4 and 10.5 of the HTTP specification at ftp://ftp.isi.edu/in-notes/rfc2616.txt
	 */
	private function onLoadError (pTarget:MovieClip, pErrorCode:String, pHttpStatus:Number):Void
	{
//		Logger.logMsg("Error loading image: " + pErrorCode + " HttpStatus: " + pHttpStatus);
//		trace ("!!!!!!!! ImageObject onLoadError () !!!!!!!!!!!!!");
	}
	
	/** 
	 * load an image
	 * @param pImage the image to display
	 */
	public function load (pImage:String):Void
	{
		// Clear out any previous image by recreating the place holder clip
		close ();
		
		// Load the thumb nail image
		mClipLoader.loadClip (pImage, mImagePH);
	}
	
	/** Resizes the image place holder */
	private function sizePlaceHolder():Void
	{
		mImagePH._width = mWidth;
		mImagePH._height = mHeight;
	}
	
	/** Saves the original dimensinos of the image for reference in order to resize the image proportially */
	private function setDimensions():Void
	{
		origWidth = mImagePH._width;
		origHeight = mImagePH._height;
	}
	
	/** Stops the download progress of whatever image is being downloaded and clear out whatever image is in there */
	public function close ():Void
	{
		// Recreate the image place holder to reset it's dimension values
		createImagePlaceHolder ();
	}
	
	/**
	 * Draw outline for the image if need be
	 */
	private function drawOutline():Void 
	{
		// Draw a border around the video object
		mBorderClip.clear ();
		mBorderClip.lineStyle (1, 0x797372, 100, false, "none", "none");
		Rect.draw (mBorderClip, mWidth, mHeight);
	}
	
	
	/**
	 * Whether to have an outline or not
	 * @param pShowOutline True if need an outline
	 */
	public function setShowOutline(pShowOutline:Boolean):Void
	{
		mShowOutline = pShowOutline;
	}
	
}