﻿import kaneva.omm.widgets.WidgetData;
// TODO - Comments
/**
 *
 * @author Kevin
 */
class kaneva.omm.widgets.AbstractWidget 
{
	/** path of the class */
	private var ClassPath:String = "kaneva.omm.widgets.";
	
	/** name of the class */
	private var ClassName:String = "AbstractWidget";
	
	/** Unique identifier for this Widget instance.  Assigned by the server. */
	public var id:Number ;
	
	/** Used to hold path of FLV to be loaded */
	public var widgetData:WidgetData ;
	
	/** type of media the advertisement is...FLV, MP3, IMAGE*/
	public var type:Number;
	
	/** Constructor */
	public function AbstractWidget()
	{
		trace("AbstractWidget() constructor");
	}

	/** Every child of this class must use this method inorder to do stuff */
	public function run():Void
	{
		trace("AbstractWidget.run()");
	}
	
	/** @return A string representation of the path and name of the class */
	public function toString():String
	{
		return "[object " + ClassPath + ClassName + "]";
	}
	
	/** 
	 * all child/subclasses *can* overwrite this function inorder to always make the appropriate 
	 * function run, if given the wrong name
	 * @param pFunctionName - name of the function that was called
	 * @see http://livedocs.macromedia.com/flash/8/main/00002588.html
	 */
	public function __resolve(pFunctionName:String):Void
	{
		trace("WARNING! FUNCTION DOES NOT EXIST! -" + pFunctionName + "()");
	}
	
}