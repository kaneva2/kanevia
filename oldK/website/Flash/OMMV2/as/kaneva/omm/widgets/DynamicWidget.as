﻿import mx.utils.Delegate;

import kaneva.media.MediaAsset;
import kaneva.omm.io.XmlParser;
import kaneva.omm.OMM;
import kaneva.omm.widgets.AbstractWidget;


/**
 * will request a flv from a jsp page when run() is invoked
 */
class kaneva.omm.widgets.DynamicWidget extends AbstractWidget
{
	/** name of the class */
	private var ClassName:String = "DynamicWidget";
	
	/** xml read in from a jsp page*/
	private var mXml:XML;
	
	/** parser for xml */
	private var mXmlParser:XmlParser;		
	
	/** Constructor */
	public function DynamicWidget()
	{
		trace("DynamicWidget() constructor");
		mXml = new XML();
		
		// make to ignore any white spacing with in the xml or reading it can be a pain.
		mXml.ignoreWhite = true;
		mXmlParser = new XmlParser();
	}
	
	/** loads a jsp page which should contina an flv to load*/
	public function run():Void
	{
		trace("DynamicWidget.run()");
		
		// load this jsp inorder to receive the xml containing which flv to load
		mXml.load("http://192.168.2.100/temp/dynamicWidget.jsp");
		
		// once the xml is loaded, call onLoadWidget
		mXml.onLoad = Delegate.create(this, onLoadWidget);

	}
	
	/** tells the OMM to run the flv that was sent from the server*/
	private function onLoadWidget(pSuccess:Boolean):Void
	{
		// seems like pSuccess is always true?!
		if(pSuccess)
		{
			
			// assuming some xml has been returned 
			if(mXml.toString().length > 0)
			{
				trace("XML with FLV to load received");
				
				// rootNode will always be <omm-resp>
				var rootNode:XMLNode = mXml.firstChild;
				
				// read in the confirm node  -- since this is only a test, i'm not concerned if the confirm tag is not infact read in here
				var confirmNode:XMLNode = rootNode.firstChild;
				
				var media:MediaAsset = mXmlParser.readMediaAssetXML(confirmNode.firstChild);
				
				// Play the advertisment
				OMM.getInstance().playMedia(true, media, false);
			}
			// no xml was sent, so tell the OMM that run() is complete
			else
			{
				trace("no XML received");
				OMM.getInstance().onComplete();
			}
			
		}
	}
	
}