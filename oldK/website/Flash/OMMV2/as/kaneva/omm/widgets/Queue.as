﻿/**
 * Contains all of the Queue information passed from an OMM XML document using
 * QueueParser 
 * 
 * @author kfrankic
 */
class kaneva.omm.widgets.Queue {
	
	/* Should this widget be executed before each media asset?  Default false. */
	public var pre:Boolean;
	
	/* Should this widget be executed after each media asset?  Default false. */
	public var post:Boolean;
	
	/* Index order by which this widget should be executed.  Zero values indicate execute immediately. */
	public var index:Number;
	
	/* Delay between executions.  Format of HH:MM:SS.  If not included, no schedule is used. */
	public var schedule:Object;
	
	public function Queue(){
		pre = false;
		post = false;
	
	}	

}