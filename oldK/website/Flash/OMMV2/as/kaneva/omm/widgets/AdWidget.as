﻿import kaneva.media.MediaAsset;
import kaneva.media.MediaType;
import kaneva.omm.OMM;
import kaneva.omm.widgets.AbstractWidget;

/**
 * used to display ads within the OMM.  when ads are run, the buttons can be disabled
 * if the arguement was set to 'true' within the XML used to create this widget
 */
class kaneva.omm.widgets.AdWidget extends AbstractWidget
{
	/** name of the class */
	private var ClassName:String = "AdWidget";
	
	/** Constructor */
	public function AdWidget()
	{
		trace("AdWidget() constructor");
	}
	
	/** Every child of this class must use this method inorder to do stuff */
	public function run():Void
	{
		trace("AdWidget.run()");
		
		// widgetData is a member of the AbstractWidget.  It is used here to get the url and length of the advertisement to play

		var media:MediaAsset = new MediaAsset();
		
		// mediaAsset created inorder to have something to play in the OMM
		media.url = String(widgetData.methods[0].args[0]);
		media.length = String(widgetData.methods[0].args[1]);
		media.type = MediaType.FLV;

		// if the arguement to disable the RAVE, ADD, SHARE, COMMENTS button was set in the XML, disable the buttons.
		// the arguments to disable the buttons should always be the third argument of the method
		if(String(widgetData.methods[0].args[2]) == "true")
			OMM.getInstance().disableAllButtonClicks(true);
			
		// set all of the text fields to display info about the media to be loaded, (not the ad)
		// (including buttons) not be blank, as they would be since this 
		// mediaAsset does not have all of it's members set to something.
		// this way, if a user minimizes the ad, they will see infomation about the media, and not the empty textfields
		OMM.getInstance().setMediaText();
		
		// Play the advertisment
		OMM.getInstance().playMedia(true, media, false);
	}
	
	/** 
	 * ### currently this function does nothing ###
	 * since there is an unknown amount of arguements, we dont place them in the parameters area, instead, within the function,
	 * we use the 'arguments' variable inorder to have access to these elements
	 */
	public function play():Void
	{
		trace("PLAY(" + arguments + ")");
	}
	

}