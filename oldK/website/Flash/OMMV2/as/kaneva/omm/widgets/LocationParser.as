﻿import kaneva.omm.widgets.Location;
 
/**
 * Parser class for Location objects.
 */
class kaneva.omm.widgets.LocationParser {
	
	/**
	 * @param pLocationNode  XML node to parse to create a new Location object.
	 * @return New Location object, initialized using the XML data.
	 */
	public function fromXml(pLocationNode:XMLNode):Location
	{
		var location:Location = new Location() ;
		location.id = pLocationNode.attributes.id;
		location.width = pLocationNode.attributes.width;
		location.height = pLocationNode.attributes.height;
		location.left = pLocationNode.attributes.left;
		location.top = pLocationNode.attributes.top;
		location.align = pLocationNode.attributes.align;
		location.valign = pLocationNode.attributes.valign;
		return location ;
	}


	/**
	 * @param pLocation  Location object used to output XML.
	 * @return Node of XML containing Location data.
	 */
	public function toXml(pLocation:Location):XMLNode
	{
		var doc:XML = new XML();
		
		var locationString:String = "location ";
		
		locationString += "id=\"" + pLocation.id + "\" ";
		locationString += "width=\"" + pLocation.width + "\" ";
		locationString += "height=\"" + pLocation.height + "\" ";
		locationString += "left=\"" + pLocation.left + "\" ";
		locationString += "top=\"" + pLocation.top + "\" ";
		locationString += "align=\"" + pLocation.align + "\" ";
		locationString += "valign=\"" + pLocation.valign + "\" ";
		
		return doc.createElement(locationString); // adds the '< />' to the element when created
	}


	

}