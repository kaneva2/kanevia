﻿import kaneva.omm.widgets.Location;
import kaneva.omm.widgets.Queue;

/**
 * Encapsulates widget directives received from the server.
 */
class kaneva.omm.widgets.WidgetData
{
   /** Unique key of the widget. */
   public var id:Number = null ;
   /** Location of the widget code. */
   public var uri:String = null ;

   /** Array of kaneva.lang.reflect.Field objects to set on the widget. */
   public var fields:Array ;
   
   /** 
    * Play schedule position within which the widget should be played, if applicable.  If not
	* included, the widget should be executed immediately.
	*/
   public var queue:Queue;
   
   /** Location instructions for the display of the widget, if applicable.*/
   public var location:Location;
   
   /** Array of kaneva.lang.reflect.Method objects to invoke on the widget. */
   public var methods:Array ;
   
   public function WidgetData()
   {
	   fields = new Array();
	   methods = new Array();
   }
     
}