﻿import kaneva.omm.widgets.Queue;

/**
 * Parser class for Queue objects.
 */
class kaneva.omm.widgets.QueueParser 
{
	/**
	 * @param pQueueNode  XML node to parse to create a new Queue object.
	 * @return New Queue object, initialized using the XML data.
	 */
	public function fromXml(pQueueNode:XMLNode):Queue
	{
		var queue:Queue = new Queue() ;

		if(pQueueNode.attributes.pre == "true")
			queue.pre = true;

		if(pQueueNode.attributes.post == "true")
			queue.post = true;
		queue.index = pQueueNode.attributes.index;
		queue.schedule = pQueueNode.attributes.schedule;
		return queue ;
	}
	
	/**
	 * @param pQueue  Queue object used to output XML.
	 * @return Node of XML containing Queue data.
	 */
	public function toXml(pQueue:Queue):XMLNode
	{
		var doc:XML = new XML();
		
		var queueString:String = "queue ";
		
		queueString += "pre=\"" + pQueue.pre + "\" ";
		queueString += "post=\"" + pQueue.post + "\" ";
		queueString += "index=\"" + pQueue.index + "\" ";
		queueString += "schedule=\"" + pQueue.schedule + "\" ";
		
		return doc.createElement(queueString);// adds the '< />' to the element when created
	}
}