﻿/**
 * Contains all of the Location information passed from an OMM XML document using
 * LocationParser 
 *
 * @author kfrankic
 */
class kaneva.omm.widgets.Location {
	
	/* If zero, or null, the module itself is assumed. */
	public var id:Number;
	
	/* Width of the widget, if appliable.  Number only. */
	public var width:Number;
	
	/* Height of the widget, if appliable.  Number only. */
	public var height:Number;
	
	/* Left position of the widget, if applicable.  Number only. */
	public var left:Number;
	
	/* Top position of the widget, if applicable.  Number only. */
	public var top:Number;
	
	/* Horizontal alignment of the widget.  Values left, right, center. */
	public var align:String;
	
	/* Vertical alignment of the widget.  Values top, bottom, middle. */
	public var valign:String;
	
}