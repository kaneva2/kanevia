﻿import kaneva.util.JavaScript;

/**
 * Used to redirect the web browser to a given URL.
 */
class kaneva.omm.widgets.UrlRedirectWidget extends kaneva.omm.widgets.AbstractWidget
{
	/** URL to which the browser should be redirected. */
	public var url:String = null ;
	
	/** Target browser for the URL redirection.  Defaults to a new ("blank") browser instance. */
	public var target:String = "_blank" ;
	
	/**
	 * Redirects the browser to the given URL and target.
	 */
	public function run():Void
	{
		JavaScript.openWindow(this.url, this.target) ;
	}
}