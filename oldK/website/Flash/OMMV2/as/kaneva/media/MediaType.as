/**
 * Media types that are used within the kaneva media player.  They will be one of the
 * following file types: FLV, MP3, IMAGE
 * 
 * @author scott
 */
class kaneva.media.MediaType 
{
	/** The media type value for a Flash Video file (*.flv) */
	public static var FLV:String = "2";
	
	/** The media type value for a MPEG Audio Layer III file (*.mp3) */
	public static var MP3:String = "4";
	
	/** The media type value for an image.  This could be a .jpg, .gif, or .png */
	public static var IMAGE:String = "5";
	
}