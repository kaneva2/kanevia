﻿/**
 * Describes a media asset within the Kaneva framework. 
 */
class kaneva.media.MediaAsset
{
	/** Primary key unique to this media asset.  Number.  Required.. */
	public var id:Number = -1;
	
	/** 
	 * The type of media
	 * @see kaneva.media.MediaType 
	 */
	public var type:String = "";
	
	/** Owner of the media.  Username on Kaneva.*/
	public var user:String = "";
	
	/** Thumbnail of the user.*/
	public var userThumb:String = "";
	
	/** Date the media was uploaded.  Format is MMDDYYYY.  Required.*/
	public var released:String = "";
	
	/** Thumbnail of the media.*/
	public var thumb:String = "";
	
	/** URL of the media */
	public var url:String = "";
	
	/** Size of the media, in kilobytes.  Required.*/
	public var size:Number = -1;
	
	/** Length in playtime of the media, video and audio only.*/
	public var length:String = "";
	
	/** Title of the asset.  Required.*/
	public var title:String = "";
	
	/** Creator of the asset.*/
	public var author:String = "";
	
	/** Album of asset, audio only. */
	public var album:String = "";
	
	/** Description of the media asset.*/
	public var descriptionValue:String = "";
	
	/** Number of times this media has been viewed.*/
	public var views:Number = 0;
	
	/** Number of "Thumbs Up" for this media. aka: Raves*/
	public var thumbsup:Number = 0;

	/** Number of times someone has added this to a channel*/
	public var channelCount:Number = 0;

	/** Number of shares for this media.*/
	public var shares:Number = 0;

	/** Cost to purchase this media.*/
	public var cost:String = "";
	
	/** Text tags associated with this media asset.*/
	public var tags:Array ;
	
	/** Number of comments for this media.*/
	public var total:Number = 0;
	
	/** Rating for the media.  ex: M for mature*/
	public var rating:String = "";
	
	/** Mature media?*/
	public var mature:Boolean = false;
	
	/** a URL to link to this user*/
	public var userUrl:String = "";
	
	/** true if already raved*/
	public var raved:Boolean = false;
	
	/** true if already in their library*/
	public var inlib:Boolean = false;
	
	/** Constructor */
	public function MediaAsset()
	{
		// Initialize the tags array
		tags = new Array();
	}
}
