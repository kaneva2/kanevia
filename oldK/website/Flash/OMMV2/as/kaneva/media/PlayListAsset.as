﻿/**
 * Describes a playlist within the Kaneva framework.
 */
class kaneva.media.PlayListAsset
{
	/** The id of the asset */
	public var id:Number;
	
	/** The author for an asset */
	public var author:String;
	
	/** The title of the asset */
	public var title:String;
	
	/** An array of assets */
	public var mediaAssets:Array;
	
	/** Constructor function */
	public function PlayListAsset()
	{
		mediaAssets = new Array();
	}
}