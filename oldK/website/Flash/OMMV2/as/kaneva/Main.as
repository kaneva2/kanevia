﻿import kaneva.omm.Config;
import kaneva.omm.Logger;
import kaneva.omm.OMM;
import kaneva.util.JavaScript;

/**
 * This class will replace the include file that was being used before.  This is sort of an
 * entry point to the .fla file.  Instantiating this class on the main timeline (_root) and 
 * passing a reference to the main timeline gives us a relatively nice and neat way of 
 * accessing the main timeline without having to reference it using the "_root" keyword.
 * 
 * @author scott
 */
class kaneva.Main 
{
	/** 
	 * Used to connect to S.O.S which is used for tracing and debugging 
	 * @see http://sos.powerflasher.com/
	 */
	private static var SOS_SOCKET:XMLSocket;
	
	/** 
	 * The ip address to use for the xml socket for sos.  If running locally, then you have
	 * to change this to your machine's ip.
	 */
	private static var SOS_IP:String = "192.168.2.124";
	
	/** 
	 * Array of colors to use for the sos background, they go in order of importance.  
	 * First is default or debug messages, second is warning messages, third are 
	 * error messages, and forth are fatal.
	 */
	private static var SOS_COLOR_ARRAY:Array = [0xDDDDDD, 0xFFFF00, 0xFF9900, 0xFF0000];
	
	/** A reference to the main timeline */
	public static var ROOT:MovieClip;
	
	/** 
	 * The xml file used to initialize what all the visual aspects of the player.  It tells OMM which
	 * widgets to show, which media to play and have in the playlist, what styles to use, what links
	 * go to, etc.
	 */
	private var startMe:String;
	
	/** Determines if the media starts right away or if it waits for user interaction before playing */
	private var autoStart:String;
	
	/** Determines if media should start off in detail mode or not */
	private var detailMode:String;
	
	/** Tell the OMM.swf if it is to be a fullscreen player or not */
	private var fullScreen:String;
	
	/** 
	 * The ContextMenu class provides runtime control over the items in the Flash Player 
	 * context menu, which appears when a user right-clicks (Windows) or Control-clicks (Macintosh)
	 * on Flash Player. In this case, we are using it to hide as much of the context menu as
	 * possible.
	 */
	private var mContextMenu:ContextMenu;

	/** Instance of OMM */
	private var mOMM:OMM;

	/** Instance of Config */
	private var mConfig:Config;
	
	/**
	 * Constructor function
	 * Wanted to get rid of the include file and have stuff in a class
	 * @param pRoot The reference to the main timeline
	 */
	public function Main(pRoot:MovieClip)
	{
		// Set reference to the _root timeline
		ROOT = pRoot;
		
		// Get references to the variables passed in from the html file that contains this swf file.
		// These will be passed in via a query string or through the FlashVars parameter of the embed
		// object
		startMe = ROOT.startMe;
		autoStart = ROOT.autoStart;
		detailMode = ROOT.detailMode;
		fullScreen = ROOT.fullScreen;
		
		// Initialize
		init();
	}
	
	/** Initialize stuff */
	private function init():Void 
	{
		// Connect to the sos
		/*SOS_SOCKET = new XMLSocket();
		SOS_SOCKET.connect(SOS_IP, 4445);
		SOS_SOCKET.send("<clear/>");*/
		
		// Initialize the OMM and pass it the reference to the main timeline
		mOMM = OMM.getInstance(ROOT);
		
		// Initialize the config
		mConfig = OMM.getConfig();

		// Set default values for the startMe, autoStart, and detailMode variables when
		// the isDebugger is set to true
//		if(System.capabilities.isDebugger)
//		{
//			if (startMe == undefined)
//				startMe = "startMe.xml";
//				
//			if (autoStart == undefined)
//				autoStart = "false";
//				
//			if (detailMode == undefined)
//				detailMode = "false"; // true == minimized
//		}
		
		// Check to see if autoStart was passed in from the html
		if(autoStart)
			mConfig.setValue("autoStart", autoStart);
		else
			mConfig.setValue("autoStart", "false");
		
		// Check to see if detailMode was passed in from the html
		if(detailMode)
			mConfig.setValue("detailMode", detailMode);
		else
			mConfig.setValue("detailMode", "false");
		
		// Load a xml into the omm
		Logger.logMsg("Starting OMM");
		mOMM.loadXml(startMe);
		
		// Hides as much of the contextMenu (menu displayed when right-clicking OMM) as possible
		mContextMenu = new ContextMenu();
		mContextMenu.hideBuiltInItems();
		
		// Show version number
		var menuItem_cmi:ContextMenuItem = new ContextMenuItem("Version", onContextMenuClick);
		mContextMenu.customItems.push(menuItem_cmi);
		
		// Hide the show redraw regions
		 _global.showRedrawRegions = false;
		 
		// Set this on the root timeline
		ROOT.menu = mContextMenu;
	}
	
	/** 
	 * This is our custom trace function used by the S.O.S. console which is used for debugging outside
	 * of the Flash IDE.  You use trace actions like normal throughout your classes, but this will 
	 * output them to the S.O.S so that you don't need to be viewing it in the Flash IDE in order 
	 * to see the output of the traces.  Place a number 1 thru 3 infront of the message and separating
	 * the number from the message with a comma will give the message a different background color which 
	 * helps distinguish it from other messages as being more important. For example:
	 * 
	 * trace("Default message is shown");
	 * trace("1,Warning message is shown");
	 * trace("2,Error message is shown");
	 * trace("3,Fatal message is shown");
	 * 
	 * Note: In order for this to work with sos, you have to compile with MTASC and specify the myTrace
	 * method for the -trace argument:
	 * 
	 * -trace "kaneva.Main.myTrace"
	 * 
	 * Compiling from the Flash IDE will not work.  You will get trace actions as usual that are only
	 * visible from the Flash IDE output window.
	 * 
	 * @param pObj The actual message being traced
	 * @param pFullClass The fully qualified class path that the trace statement came from
	 * @param pFile The name of the file the trace statement came from
	 * @param pLine The line number the trace statement came from
	 * @see http://sos.powerflasher.com/
	 * @see http://www.mtasc.org/english.html
	 */
	public static function myTrace(pObj:String, pFullClass:String, pFile:String, pLine:Number):Void
	{
		// The message to show in the sos
		var message:String = pObj;
		
		// Color for the background of the message in the sos.  Initially this is set to the default color
		var color:Number = SOS_COLOR_ARRAY[0];
		
		// Determine if there is a comman in the message
		var isComma:Boolean = pObj.indexOf(",") > -1;
		
		// If there is a comma...
		if(isComma)
		{
			// Get the number value off the front and get the  message off the back
			var arr:Array = pObj.split(",");
			var num:Number = Number(arr[0]);
			message = arr[1];
			
			// Set the new color
			color = SOS_COLOR_ARRAY[num];
		}
		
		// Set the color
		SOS_SOCKET.send("<setKey><name>Key</name><color>" + color + "</color></setKey>\n");
		
		// Send the message......this formats it in SOS so that extra info is hidden in a fold out menu.  This way it is nice 
		// and neat and only shown when necessary.
		SOS_SOCKET.send("<showFoldMessage key=\"Key\"><title>" + message +"</title><message>\t" + pFullClass + "\n\t" + pFile + "\n\t" + pLine + "</message></showFoldMessage>");
		
		// Show the sos window
		SOS_SOCKET.send("<show/>");
	}
	
	/**
	 * Method triggered by the context menu item.  Does nothing, but I have to define it for it to work
	 */
	private function onContextMenuClick():Void 
	{
		// Update this date value to current date...this will show the date I last made changes to the project.  Users can
		// right click to see this value and know if they have an older version or not.  I have to manually 
		// update this though so there is potential error with this, but this is a quick way of doing it.
		var lastUpdated:Date = new Date(2007, 1, 2);
		JavaScript.alert(lastUpdated.toString());
	}
}