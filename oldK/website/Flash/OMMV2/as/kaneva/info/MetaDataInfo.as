/**
 * The info object passed by the meta data event of the netstream class. The Flash 
 * Video Exporter utility (version 1.1 or greater) embeds a video's duration, 
 * creation date, data rates, and other information into the video file itself. 
 * Different video encoders embed different sets of metadata.
 * 
 * @author scott
 * @see NetStream.onMetaData
 * @see http://livedocs.macromedia.com/flash/8/main/00003559.html
 */
class kaneva.info.MetaDataInfo 
{
	/** 
	 * A Boolean value that is true if the FLV file is encoded with a keyframe on 
	 * the last frame that allows seeking to the end of a progressive download 
	 * movie clip. It is false if the FLV file is not encoded with a keyframe 
	 * on the last frame 
	 */
	public var canSeekToEnd:Boolean = null;
	
	/** 
	 * An array of objects, one for each cue point embedded in the FLV file. 
	 * Value is undefined if the FLV file does not contain any cue points 
	 */
	public var cuePoints:Array = null;
	
	/** A number that indicates the audio codec (code/decode technique) that was used */
	public var audiocodecid:Number = null;
	
	/** 
	 * A number that indicates what time in the FLV file "time 0" of the original FLV 
	 * file exists. The video content needs to be delayed by a small amount to 
	 * properly synchronize the audio
	 */
	public var audiodelay:Number = null;
	
	/** A number that is the kilobytes per second of audio */
	public var audiodatarate:Number = null;
	
	/** A number that is the codec version that was used to encode the video */
	public var videocodecid:Number = null;
	
	/** A number that is the frame rate of the FLV file */
	public var framerate:Number = null;
	
	/** A number that is the video data rate of the FLV file */
	public var videodatarate:Number = null;
	
	/** The height which the video was encoded at....the original height of the video */
	public var height:Number = null;
	
	/** The width which the video was encoded at....the original width of the video */
	public var width:Number = null;
	
	/** A number that specifies the duration of the FLV file in seconds  */
	public var duration:Number = null;
}