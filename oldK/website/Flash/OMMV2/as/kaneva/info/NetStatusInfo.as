/**
 * A parameter object of the onStatus event of a NetStream object, defined according to the status message or error message.
 * The information object has a code property containing a string that describes the result of the onStatus 
 * handler, and a level property containing a string that is either status or error.
 * 
 * @author scott
 * @see http://livedocs.macromedia.com/flash/8/main/00002563.html
 */
class kaneva.info.NetStatusInfo 
{
	/** Describes the result of the onStatus handler */
	public var code:String = null;
	
	/** Will be either 'status' or 'error' */
	public var level:String = null;
	
	/** Contains a time code that indicates the last valid position to which the user can seek. */
	public var details:Number;
}