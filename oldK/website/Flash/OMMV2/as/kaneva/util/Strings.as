﻿/**
 * Utility methods dealing with String objects.
 */
class kaneva.util.Strings 
{
	
	/**
	 * Replaces all occurencies of the passed-in string {@code what} with the passed-in
	 * string {@code to} in the passed-in string {@code string}.
	 * 
	 * @param string the string to replace the content of
	 * @param what the string to search and replace in the passed-in {@code string}
	 * @param to the string to insert instead of the passed-in string {@code what}
	 * @return the result in which all occurences of the {@code what} string are replaced
	 * by the {@code to} string
	 */
	public static function replace(pString:String, what:String, to:String):String 
	{
		return pString.split(what).join(to);
	}
	
	/**
	 * checks the originalString to see if the endingSubString exists within it
	 * @param pOriginalString - the string that is being searched through
	 * @param pEndingSubString - the substring that is being looked for within pOriginalString
	 * @return true if the substring ends the originalstring
	 */
	public static function endsWith(pOriginalString:String, pEndingSubString:String):Boolean
	{
		var indexOfSubString:Number = pOriginalString.lastIndexOf(pEndingSubString);
		var lengthOfSubString:Number = pEndingSubString.length;
		var lengthOfOrigString:Number = pOriginalString.length;
		
		if((lengthOfOrigString - indexOfSubString) == lengthOfSubString)
			return true;
		else
			return false;
	}

	/** 
	 * Decode html tags
	 * @param pString 
	 * @return 
	 */
	public static function HtmlDecode( pString:String ):String
	{
		var ret:String = replace( pString, "&amp;", "&" );
		ret = replace( ret, "&apos;", "'" );
		ret = replace( ret, "&quot;", "\"" );
		ret = replace( ret, "&lt;", "<" );
		ret = replace( ret, "&gt;", ">" );
		
		return ret;
	}
	
	/** 
	 * encode HTML encoded tags
	 * @param pString 
	 * @return encoded string
	 */
	public static function HtmlEncode( pString:String ):String
	{
		var ret:String = replace( pString, "&", "&amp;", "&" );
		ret = Strings.replace( ret, "'", "&apos;" );
		ret = Strings.replace( ret, "\"", "&quot;" );
		ret = Strings.replace( ret, "<", "&lt;" );
		ret = Strings.replace( ret, ">" , "&gt;");

		return ret;
	}	
	
	/** 
	 * If a string is too long for a text field, shorten it and stick some dots on the end
	 * @param pStr The string to check
	 * @param pTextField The text field which the string will be placed in 
	 * @return The new string
	 */
	public static function truncate (pStr:String, pTextField:TextField):String
	{
		// If the needed width for the specified text is greater than the text field width.....
		if (pTextField.getTextFormat().getTextExtent(pStr).textFieldWidth > pTextField._width)
		{
			var temp:String;
			
			// Iterate through the string backwards
			for (var i:Number = pStr.length - 1; i > 0; i--)
			{
				// Take off the last character of the string and save to the temp string
				temp = pStr.substr (0, i);
				
				// If the temp string fits...then break out of the loop
				if (pTextField.getTextFormat().getTextExtent(temp).textFieldWidth < pTextField._width)
					break;
			}
			
			// Take off the last three characters so we can stick the dots on the end and return the new string
			return temp.substr(0, temp.length - 3) + "...";
		}
		
		// Return the original string
		return pStr;
	}
	
	/**
	 * Takes a string version of a color value and converts it to a hexidecimal number value
	 * @param pValue The string to convert
	 * @return The hexidecimal number
	 */
	public static function getColor (pValue:String):Number
	{
		return Number(replace(pValue, "#", "0x"));
	}
	
	/**
	 * Takes a number of seconds (string type) and converts it to HH:MM:SS format
	 * @param pValue The number of seconds (string) to convert to the proper format
	 * @return The string formatted in HH:MM:SS
	 */
	public static function convertSecondsToTime (pValue:String):String
	{
		// Calculate numeric values for hours, minutes, and seconds
		var duration:Number = Number(pValue);
		var hours:Number = Math.floor(duration / 3600);
		duration -= hours * 3600;
		var minutes:Number = Math.floor(duration / 60);
		duration -= minutes * 60;
		var seconds:Number = Math.floor(duration);
		
		// Stick preceeding zeros where necessary
		var h:String = hours < 10 ? "0" + hours : hours.toString();
		var m:String = minutes < 10 ? "0" + minutes : minutes.toString();
		var s:String = seconds < 10 ? "0" + seconds : seconds.toString();
		
		// Return the formatted value......either MM:SS if there are no hours, or HH:MM:SS if there are hours
		
		return (hours ? h + ":" : "") +  m + ":" + s;
	}
}
