﻿import kaneva.omm.OMM;
/**
 * Methods used for web browser interface and control.
 * @author scott
 */
class kaneva.util.JavaScript
{
	/**
	 * Private constructor to keep from instantiating this class.  All methods of 
	 * this class are static.
	 */
	private function JavaScript()
	{
		
	}
	
	/**
	 * Invokes the window blur method on the current movie window. 
	 */
	public static function blur():Void
	{
		getURL("javascript:void window.blur();") ;
	} 
	
	/**
	 * Invokes the window focus method on the current movie window. 
	 */
	public static function focus():Void
	{
		getURL("javascript:void window.focus();") ;
	}
	
	/**
	 * Create a pop up window.
	 * @param pUrl URL to load in the new pop up.
	 * @param pName The name of the pop up
	 * @param pArguments Contains properties to set for the new window.
	 */
	public static function openPopup(pUrl:String, pName:String, pArguments:String):Void
	{
		getURL("javascript:void window.open('" + pUrl + "', '" + pName + "', '" + pArguments + "');");
	}
	
	/**
	 * Pop up the full screen player
	 */
	public static function fullScreen():Void
	{
		var fsUri:String = OMM.getConfig().getAsString("uri.link.fullscreen");
		getURL("javascript:var win=window.open('" + fsUri + "', 'fullScreen', 'menubar=no,personalbar=no,resizeable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no');win.moveTo(0, 0);win.resizeTo(window.screen.availWidth, window.screen.availHeight);void(0);");
	}
	
	/**
	 * Open a new window with the specified url.
	 * @param pUrl URL to load in the new pop up.
	 * @param pTarget The target of the new window........will be one of 4(_blank, _parent, _top, _self).......default is _blank
	 */
	public static function openWindow(pUrl:String, pTarget:String):Void
	{
//		pTarget = !pTarget ? "_blank" : pTarget;
		getURL(pUrl, pTarget);
	}
	
	/**
	 * Resize a window.
	 * @param pWidth The width to resize the window to.
	 * @param pHeight The height to resize the window to.
	 */
	public static function resizeTo(pWidth:Number, pHeight:Number):Void
	{
		getURL("javascript:void window.resizeTo(" + pWidth + "," + pHeight + ");");
	}
	
	/**
	 * Create an alert message
	 * @param pMessage The message to show in the alert box
	 */
	public static function alert(pMessage:String):Void
	{
		getURL("javascript:alert('" + pMessage + "')");
	} 
}