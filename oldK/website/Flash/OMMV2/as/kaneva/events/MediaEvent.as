import kaneva.events.Event;

/**
 * Represents events that are dispatched when media is done loading or playing
 * @author scott
 */
class kaneva.events.MediaEvent extends Event 
{
	/** Defines the value of the type property of a complete event object */
	public static var COMPLETE:String = "complete";
	
	/** Defines the value of the type property of a media complete event object. This happens when a piece of media is done playing */
	public static var MEDIA_COMPLETE:String = "mediaComplete";
}