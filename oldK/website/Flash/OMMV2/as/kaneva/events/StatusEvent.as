import kaneva.events.Event;

/**
 * Dispatched when an object reports its status
 * @author scott
 */
class kaneva.events.StatusEvent extends Event 
{
	/** Defines the value of the type property of a status event object */
	public static var STATUS:String = "status";
	
	/** A description of the object's status. */
	public var code:String = null;
}