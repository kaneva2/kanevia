import kaneva.events.Event;
import kaneva.omm.ui.ComponentDisplay;

/**
 * Represents events that are dispatched when a navigation item on a navigator bar, 
 * such as playlist component has been clicked.  Also used for the pop up window 
 * buttons like submit and cancel.
 * @author scott
 */
class kaneva.events.ItemClickEvent extends Event 
{
	/** Defines the value of the type property of the event object for an itemClick event */
	public static var ITEM_CLICK:String = "itemClick";
	
	/** The index of the associated navigation item */
	public var index:Number = 0;
	
	/** The item in the data provider of the associated navigation item */
	public var item:Object = null;
	
	/** The label of the associated navigation item */
	public var label:String = null;
	
	/** 
	 * The child object that generated the event; for example, 
	 * the List component that contains the PlayListItem component in 
	 * which the clicked button resides
	 */
	public var relatedObject:ComponentDisplay;
}