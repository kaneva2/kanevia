import kaneva.events.Event;
import kaneva.info.MetaDataInfo;

/**
 * Dispatched when a NetStream object reports its meta data
 * @author scott
 */
class kaneva.events.MetaDataEvent extends Event 
{
	/** An object with properties that describe the object's meta data */
	public static var META_DATA:String = "metaData";
	
	/** An object containing one property for each metadata item...main thing we want is duration */
	public var info:MetaDataInfo = null;
}