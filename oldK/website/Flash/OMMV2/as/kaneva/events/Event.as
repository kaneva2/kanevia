/**
 * An event object is passed to a listener as a parameter. The event object 
 * is an ActionScript object that has properties that contain information about
 * the event that occurred. You can use the event object inside the listener 
 * callback function to access the name of the event that was broadcast, or the
 * instance name of the component that broadcast the event.
 * 
 * The Event class is used as the base class for the creation of Event objects, 
 * which are passed as parameters to event listeners when an event occurs.
 * The properties of the Event class carry basic information about an event, 
 * such as the event's type. For many events, such as the events represented 
 * by the Event class constants, this basic information is sufficient. 
 * 
 * @author Scott
 * @see http://livedocs.macromedia.com/flash/8/main/00003472.html
 */
class kaneva.events.Event 
{
	/** The type of event */
	public var type:String = null;
	
	/** The event target, the object that dispatched this event */
	public var target:Object = null;
}