import kaneva.events.Event;

/**
 * Represents a change in a movieclips enabled property
 * @author scott
 */
class kaneva.events.EnabledEvent extends Event 
{
	/** The enabled event type */
	public static var ENABLED:String = "enabled";
	
	/** The value of enabled, either true or false */
	public var value:Boolean;
}