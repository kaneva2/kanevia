import kaneva.events.Event;

/**
 * Represents event objects that are dispatched when the size of a component changes
 * @author scott
 */
class kaneva.events.ResizeEvent extends Event 
{
	/** Defines the value of the type property of the event object for a resize event */
	public static var RESIZE:String = "resize";
	
	/** The previous height of the object, in pixels */
	public var oldHeight:Number = 0;
	
	/** The previous width of the object, in pixels */
	public var oldWidth:Number = 0;
}