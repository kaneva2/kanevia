import kaneva.events.Event;

/**
 * Represents events that are dispatched when video is resized
 * @author scott
 */
class kaneva.events.ResizeVideoEvent extends Event 
{
	/** 
	 * Defines the value of the type property of a resize event object...specifically, 
	 * when video is resized to show details or full size 
	 */
	public static var RESIZE:String = "videoResize";
	
	/** Show the details of media */
	public static var DETAILS:String = "details";
	
	/** Show only the video of media */
	public static var VIDEO:String = "video";
	
	/** This will be either DETAILS or VIDEO depending on which size the video is sized to */
	public var size:String = null;
}