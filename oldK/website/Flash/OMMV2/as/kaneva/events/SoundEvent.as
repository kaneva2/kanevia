import kaneva.events.ProgressEvent;

/**
 * Represents events that are dispatched when a sound is
 * @author scott
 */
class kaneva.events.SoundEvent extends ProgressEvent 
{
	/** Defines the value of the type property of a load complete event object */
	public static var COMPLETE:String = "soundLoadComplete";
	
	/** Defines the value of the type property of a soundComplete event object */
	public static var SOUND_COMPLETE:String = "soundComplete";
	
	/** Reference to the sound object */
	public var soundObject:Sound = null;
}