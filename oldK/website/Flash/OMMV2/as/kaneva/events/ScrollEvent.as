import kaneva.events.Event;

/**
 * @author scott
 */
class kaneva.events.ScrollEvent extends Event 
{
	/** Defines the value of the type property of a scroll event object */
	public static var SCROLL:String = "scroll";
	
	/** The new scroll position */
	public var position:Number = 0;
}