import kaneva.events.Event;
import kaneva.info.NetStatusInfo;

/**
 * Dispatched when a NetStream object reports its status
 * @author scott
 */
class kaneva.events.NetStatusEvent extends Event 
{
	/** An object with properties that describe the object's status or error condition */
	public static var NET_STATUS:String = "netStatus";
	
	/** An object with properties that describe the object's status or error condition */
	public var info:NetStatusInfo = null;
}