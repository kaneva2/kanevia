import kaneva.events.Event;

/**
 * Event that is used when a load operation has begun. These events are usually 
 * generated when SWF files, images or data are loaded into Flash Playerl
 * 
 * @author scott
 */
class kaneva.events.ProgressEvent extends Event 
{
	/** Defines the value of the type property of the event object for a progress event */
	public static var PROGRESS:String = "progress";
	
	/** The percent of buffered media */
	public var bufferPercent:Number = 0;
	
	/** Whether or not the media is buffering */
	public var isBuffering:Boolean = false;
	
	/** The number of items or bytes loaded when the listener processes the event */
	public var bytesLoaded:Number = 0;
	
	/** The total number of items or bytes that will be loaded if the loading process succeeds. */
	public var bytesTotal:Number = 0;
}