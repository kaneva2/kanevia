import kaneva.events.Event;

/**
 * The SliderEvent class represents the event object passed to the event listener 
 * for the change, thumbDrag, thumbPress, and thumbRelease events of the 
 * SliderControl and VolumeControl classes.
 * 
 * @author Scott
 */
class kaneva.events.SliderEvent extends Event 
{
	/** Defines the value of the type property of the event object for a change event */
	public static var CHANGE:String = "change";
	
	/** Defines the value of the type property of the event object for a thumbDrag event */
	public static var THUMB_DRAG:String = "thumbDrag";
	
	/** Defines the value of the type property of the event object for a thumbPress event */
	public static var THUMB_PRESS:String = "thumbPress";
	
	/** Defines the value of the type property of the event object for a thumbRelease event */
	public static var THUMB_RELEASE:String = "thumbRelease";
	
	/** Specifies that the Slider's thumb was clicked */
	public static var THUMB:String = "thumb";
	
	/** Specifies that the Slider's track was clicked */
	public static var TRACK:String = "track";
	
	/** 
	 * Specifies whether the slider track or a slider thumb was pressed. This property 
	 * can have one of two values: THUMB or TRACK. 
	 */
	public var clickTarget:String = null;
	
	/** The new value of the slider */
	public var value:Number = 0;
}