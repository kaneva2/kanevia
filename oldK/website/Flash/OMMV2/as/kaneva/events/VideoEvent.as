import kaneva.events.ProgressEvent;

/**
 * The VideoEvent class represents the event object passed to the event 
 * listener for events dispatched by the NetStream obeject.
 * @author Scott
 */
class kaneva.events.VideoEvent extends ProgressEvent 
{
	/** Defines the value of the type property of a load complete event object */
	public static var COMPLETE:String = "videoLoadComplete";
	
	/** Referecne to the netstream object that is used to control the video */
	public var netStreamObject:NetStream = null;
	
	/** Referecne to the sound object that is used to control the volume of the video sound */
	public var soundObject:Sound = null;
}