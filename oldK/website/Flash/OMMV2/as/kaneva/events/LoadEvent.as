import kaneva.events.Event;

/**
 * Dispatched when an image has loaded sucessfully
 * @author scott
 */
class kaneva.events.LoadEvent extends Event 
{
	/** The load event type */
	public static var LOAD:String = "load";
	
	/** The onload event type.  This is dispatched when the onLoad event for a movieclip triggers */
	public static var ONLOAD:String = "onload";
}