import kaneva.events.Event;

/**
 * Represent the different types of tween events that occur during the life of a
 * tween animation.
 * @author Scott
 */
class kaneva.events.TweenEvent extends Event 
{
	/** Defines the value of the event object's type property for a tweenEnd event */
	public static var TWEEN_END:String = "tweenEnd";
	
	/** Defines the value of the event object's type property for a tweenStart event */
	public static var TWEEN_START:String = "tweenStart";
	
	/** Defines the value of the event object's type property for a tweenUpdate event */
	public static var TWEEN_UPDATE:String = "tweenUpdate";
}