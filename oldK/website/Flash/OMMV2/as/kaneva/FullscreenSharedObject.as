import kaneva.media.MediaAsset;

/**
 * Saves a copy of the current media asset that the user is viewing in the media player.  This
 * Allows use to easily pass a MediaAsset object to the fullscreen OMM.swf from the embedded
 * OMM.swf without having to reparse any xml.  Once the fullscreen OMM opens up, it can read the 
 * media asset property from the shared object
 * @author scott
 */
class kaneva.FullscreenSharedObject 
{
	/** The name of the local object */
	private static var SO_NAME:String = "fullScreenSharedObject";
	
	/** The name of the property that will be on the data property of the shared object */
	private static var MEDIA_ASSET:String = "mediaAsset";
	
	/** Shared object used to store a media asset */
	private static var SHARED_OBJECT:SharedObject;
	
	/**
	 * Sets the media asset to the shared object
	 * @param pMediaAsset The media asset to save
	 */
	public static function setMediaAsset(pMediaAsset:MediaAsset):Void
	{
		// Create the shared object if it doesn't exist....get reference to it if it does
		SHARED_OBJECT = SharedObject.getLocal(SO_NAME);
		
		// Stick the media asset on the data property of the shared object
		SHARED_OBJECT.data[MEDIA_ASSET] = pMediaAsset;
		
		// Immediately writes a locally persistent shared object to a local file
		SHARED_OBJECT.flush();
	}
	
	/**
	 * @return The media asset object for the current media playing in the embedded player
	 */
	public static function getMediaAsset():MediaAsset
	{
		// Create the shared object if it doesn't exist....get reference to it if it does
		SHARED_OBJECT = SharedObject.getLocal(SO_NAME);
		
		// Return the media asset on the data property of the shared object
		return SHARED_OBJECT.data[MEDIA_ASSET]; 
	}
}