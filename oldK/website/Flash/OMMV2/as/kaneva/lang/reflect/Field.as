﻿/**
 * Encapsulates information regarding the setting of a field on an object.
 */
class kaneva.lang.reflect.Field 
{
	/* Name of the parameter.  Required. */
	public var name:String;
	
	/* Value of the parameter.  Required. */
	public var text:String;		
		
}