﻿/**
 * Encapsulates information used in the invocation of a method.
 */
class kaneva.lang.reflect.Method
{
	/** Name of the method. */
	public var name:String;
	
	/** Array of String arguments to pass to the method.*/
	public var args:Array ;
	
}