<?xml version="1.0" encoding="UTF-8"?>
<!--
   ELEMENT - omm-resp
   
   ATTRIBUTES
               id      - Unique key for this response.  Same as the id sent during the request, if applicable.
   -->
<!ELEMENT omm-resp ((confirm? | error?), widgets?, playlist?)>
<!ATTLIST omm-resp id CDATA #IMPLIED>

<!--
   ELEMENT - confirm 
               Confirmation message contained within the omm-resp body.  Mutually exclusive with error element.
               May contain zero or one media element.
   -->
<!ELEMENT confirm (media?)>

<!--
   ELEMENT - error
               Error message contained within the omm-resp body.  Mutually exclusive with confirm element.
   -->
<!ELEMENT error EMPTY>

<!--
   ELEMENT - widgets
               Encapsualtes one or more 'widget' elements within the omm-resp body.
   -->
<!ELEMENT widgets (widget+)>

<!--
   ELEMENT - widget
               Represents a unique instance of a widget for the OMM to download and instantiate.
               Child of 'widgets' element.
   
   ATTRIBUTES
               id       - Unique key assigned to this widget instance by the server.  References to this widget 
                              will utilize this key. (Required.)
               uri      - Codebase from which the widget code should be downloaded.  (Required.)
   -->
<!ELEMENT widget (queue?,location?,param*,method*)>
<!ATTLIST widget 
   id  CDATA   #REQUIRED
	uri CDATA   #REQUIRED
>

<!-- 
   ELEMENT - queue         Play schedule position within which the widget should be played, if applicable.  If not
                 included, the widget should be executed immediately.
                 XPath example: /omm-resp/widgets/widget[x]/queue
   
   ATTRIBUTES
              pre      - Should this widget be executed before each media asset?  Default false.
              post     - Should this widget be executed after each media asset?  Default false.
              index    - Index order by which this widget should be executed.  Zero values indicate execute immediately.                       
              schedule - Delay between executions.  Format of HH:MM:SS.  If not included, no schedule is used.                 
    -->
<!ELEMENT queue EMPTY>
<!ATTLIST queue
	index     CDATA           #IMPLIED
	post      (true|false)    "false"
	pre       (true|false)    "false"
	schedule  CDATA           #IMPLIED
>

<!--
   ELEMENT - location    - Location instructions for the display of the widget, if applicable.
                 XPath example: /omm-resp/widgets/widget[x]/location
   
   ATTRIBUTES
               id     - Unique key of the widget within which this widget should be displayed.
                        If zero, or null, the OMM itself is assumed.
               width  - Width of the widget, if appliable.  Number only.
               height - Height of the widget, if appliable.  Number only.
               left   - Left position of the widget, if applicable.  Number only.
               top    - Top position of the widget, if applicable.  Number only.
               align  - Horizontal alignment of the widget.  Values left, right, center.
               valign - Vertical alignment of the widget.  Values top, bottom, middle.                
   -->
<!ELEMENT location EMPTY>
<!ATTLIST location id     CDATA        #IMPLIED>
<!ATTLIST location width  CDATA        #IMPLIED>
<!ATTLIST location height CDATA        #IMPLIED>
<!ATTLIST location left   CDATA        #IMPLIED>
<!ATTLIST location top    CDATA        #IMPLIED>
<!ATTLIST location align  (left | right | center) #IMPLIED>
<!ATTLIST location valign (top | bottom | middle) #IMPLIED>

<!-- 
   ELEMENT - method
                Used to invoke a method on a widget.  Contains zero or more 'arg' (argument) elements.
                Child node of 'widget' element.
                XPath example: /omm-resp/widgets/widget[x]/method[x]
   
   ATTRIBUTES
               name  - Name of the method to invoke.
   -->
<!ELEMENT method (arg*)>
<!ATTLIST method name CDATA #REQUIRED>

<!-- 
   ELEMENT - arg
               Argument passed to a method.  Zero or more are contained within the 'method' element.
               XPath example: /omm-resp/widgets/widget[x]/method[x]/arg[x]

   CONTENT
       (text)  Data value to pass.  Data type is inconsequential, as Flash loosely-types variables.
   -->
<!ELEMENT arg (#PCDATA)>

<!-- 
   ELEMENT - param          - Parameter passed to the widget, set during initialization.
                    XPath example: /omm-resp/widgets/widget[x]/param[x]
   
   ATTRIBUTES
      name   - Name of the parameter.
   
   CONTENT
      (text)   Value of the parameter.
   -->
<!ELEMENT param (#PCDATA)>
<!ATTLIST param name CDATA #REQUIRED>

<!--
   ELEMENT - playlist
                     Contains one or more media elements to add to the playlist.
   
   ATTRIBUTES
      id           - Unique identifer for this playlist.
      author       - User author of the playlist, if applicable.
      title        - Title of the playlist, if applicable.
-->
<!ELEMENT playlist (media+)>
<!ATTLIST playlist
	id       CDATA        #IMPLIED
	author   CDATA        #IMPLIED
   title    CDATA        #IMPLIED
>
<!--
   ELEMENT - media  
                    Media element to play within the OMM.
                    Contains zero or one 'desc' (description) element, zero or one 'stats' (statistics) element,
                    zero or one 'tags' element, and zero or one 'comments' element.                    
   
   ATTRIBUTES
      id        - Unique key for the media.  (Required.)
      length    - Length of play time for the media, if applicable.
      released  - Date the media was uploaded to Kaneva. (Required.)
      size      - Size of the media. (Required.)
      thumb     - Location of the thumbnail image for this media.
      type      - Type of media file: video '2', audio '4', or image '5'.  (Required.)
      user      - User who uploaded the media. (Required.)
      url	- url of media (Required)
   -->
<!ELEMENT media (desc?,stats?,tags?,comments?)>
<!ATTLIST media
	id       CDATA        #REQUIRED
	length   CDATA        #IMPLIED
	released CDATA        #REQUIRED
	size     CDATA        #REQUIRED
	thumb    CDATA        #IMPLIED
	type     (2 | 4 | 5)  #REQUIRED 
	user     CDATA        #REQUIRED
	url	 CDATA	      #REQUIRED
>

<!--
   ELEMENT - desc 
               Description information.
   
   ATTRIBUTES
      album    - Album to which this song belongs.
      author   - Band or author of the audio.
      title    - Title of the media.

   CONTENT   
      (text)   - Short description of the media itself.
  -->
<!ELEMENT desc (#PCDATA)>
<!ATTLIST desc
	album  CDATA #IMPLIED
	author CDATA #IMPLIED
	title  CDATA #REQUIRED
>

<!-- 
   ELEMENT - stats 
                  Dynamic media statistics that may change over time.
   
   ATTRIBUTES
      cost     - Cost to purchase access to the asset.
      shares   - Number of times this media has been shared.
      thumbsup - Number of thumbs-up given by users.
      views    - Number of times this media has been viewed.
   -->
<!ELEMENT stats EMPTY>
<!ATTLIST stats
	cost     CDATA #IMPLIED
	shares   CDATA #REQUIRED
	thumbsup CDATA #REQUIRED
	views    CDATA #REQUIRED
>

<!-- 
   ELEMENT - tags 
                  Tags describing the contents of this media asset.
   
   CONTENT
      (text) - Comma separated list of tags assocaited with this media asset.
   -->
<!ELEMENT tags (#PCDATA)>

<!--
   ELEMENT - comments       - Information regarding the comments associated with a media asset.
   
   ATTRIBUTES
     total    - Number of comments associated with this asset.
   -->
<!ELEMENT comments EMPTY>
<!ATTLIST comments total CDATA #REQUIRED>

<!-- PLAYLISTS -->
<!ELEMENT playlists (playlistname*)>

<!ELEMENT playlistname EMPTY>
<!ATTLIST playlistname id NMTOKEN #REQUIRED>
<!ATTLIST playlistname name CDATA #REQUIRED>

