///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using Xunit;
using System.Reflection;

namespace Kaneva.BusinessLayer.FacadeTests
{
    public class SubscriptionFacadeTest
    {
        private SubscriptionFacade subscriptionFacade;

        public SubscriptionFacadeTest()
        {
            subscriptionFacade = new SubscriptionFacade();
        }

        /// <summary>
        ///A test for GetSubscription (int)
        ///</summary>
        [Fact]
        public void GetSubscriptionTest()
        {
           uint SubscriptionId = 1; // TODO: Initialize to an appropriate value

            Subscription actual;

            actual = subscriptionFacade.GetSubscription(SubscriptionId);

            Assert.True (actual.SubscriptionId > 0);
        }

        /// <summary>
        ///A test for GetSubscriptionEntitlements (int, string)
        ///</summary>
        [Fact]
        public void GetSubscriptionEntitlementsTest()
        {
            int subscriptionId = 1; // TODO: Initialize to an appropriate value

            string gender = null; // TODO: Initialize to an appropriate value
            System.Collections.Generic.List<Kaneva.BusinessLayer.BusinessObjects.SubscriptionEntitlement> actual;

            actual = subscriptionFacade.GetSubscriptionEntitlements(subscriptionId, gender);
            
            Assert.True (actual.Count > 0);
        }

        /// <summary>
        ///A test for GetUserSubscription (int)
        ///</summary>
        [Fact]
        public void GetUserSubscriptionTest()
        {
            uint userSubscriptionId = 1; // TODO: Initialize to an appropriate value
            UserSubscription actual;

            actual = subscriptionFacade.GetUserSubscription (userSubscriptionId);

            Assert.True(actual.SubscriptionId > 0);
        }

        /// <summary>
        ///A test for GetUserSubscriptions (int)
        ///</summary>
        [Fact]
        public void GetUserSubscriptionsTest()
        {
            int userId = 4; // TODO: Initialize to an appropriate value
            System.Collections.Generic.List<Kaneva.BusinessLayer.BusinessObjects.UserSubscription> actual;

            actual = subscriptionFacade.GetUserSubscriptions(userId);

            Assert.True (actual.Count > 0);
        }

        ///// <summary>
        /////A test for InsertUserSubscription (UserSubscription)
        /////</summary>
        //[Fact]
        //public void InsertUserSubscriptionTest()
        //{
        //    SubscriptionFacade target = new SubscriptionFacade();
        //    CybersourceAdaptor cybersourceAdaptor = new CybersourceAdaptor();

        //    UserSubscription userSubscription = new UserSubscription(0, 1, 0, 4,
        //        9.99, true, DateTime.Now, Subscription.SubscriptionStatus.Active, 0,
        //        "Fsknx83lscos", false, DateTime.Now, Subscription.SubscriptionTerm.BiWeekly, Subscription.SubscriptionTerm.Monthly,
        //        cybersourceAdaptor.GetTrialLengthFromKanevaConstants (Subscription.SubscriptionTerm.None), DateTime.Now.AddDays(14), "", 0, 0, false); ; // TODO: Initialize to an appropriate value

        //    uint actual;

        //    actual = target.InsertUserSubscription(userSubscription);

        //    Assert.IsTrue(actual > 0, "Didn't return any results");
        //}

        /// <summary>
        ///A test for UpdateUserSubscription (UserSubscription)
        ///</summary>
        [Fact]
        public void UpdateUserSubscriptionTest()
        {
            SubscriptionFacade target = new SubscriptionFacade();

            UserSubscription userSubscription = new UserSubscription(31, 1, 0, 4,
                9.99, true, DateTime.Now, Subscription.SubscriptionStatus.Active, 5,
                "543jklxid23221", false, DateTime.Now.AddDays(-30), Subscription.SubscriptionTerm.BiWeekly, Subscription.SubscriptionTerm.Monthly,
                DateTime.Now.AddDays(28), DateTime.Now.AddDays(14), "", 0, 0, false); // TODO: Initialize to an appropriate value


            target.UpdateUserSubscription(userSubscription);

            // Nothing to verify?
            // Assert.Inconclusive("A method that does not return a value cannot be verified.");
            Assert.True (true);
        }

        ///// <summary>
        /////A test for CreateSubscriptionTest
        /////</summary>
        //[Fact]
        //public void CreateSubscriptionTest()
        //{
        //    SubscriptionFacade target = new SubscriptionFacade();

        //    int orderId = StoreUtility.CreateOrder(4, "201.23.23.23", (int)Constants.eORDER_STATUS.CHECKOUT);

        //    // Set the purchase type
        //    StoreUtility.SetPurchaseType(orderId, Constants.ePURCHASE_TYPE.SUBSCRIPTION);

        //    // Record the transaction in the database, marked as checkout
        //    int transactionId = StoreUtility.PurchasePoints(4, (int)Constants.eTRANSACTION_STATUS.CHECKOUT, "Access Pass Subscription", 0, (int)Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE,
        //        14.99, 0, 0, 0, "201.23.23.23");

        //    // Set the order id on the point purchase transaction
        //    StoreUtility.UpdatePointTransaction(transactionId, orderId);
        //    StoreUtility.UpdateOrderPointTranasactionId(orderId, transactionId);

        //    int addressId = UsersUtility.InsertAddress(4, "Jason Test", "123 Pine St",
        //        "", "Marietta", "GA",
        //        "30064", "222332111", "US");

        //    // Save the address to the order
        //    StoreUtility.UpdatePointTransactionAddressId(transactionId, addressId);

        //    // Billing Info
        //    int billingInfoId = UsersUtility.InsertBillingInfo(4,
        //        Constants.eBILLING_INFO_TYPE.PROFILE, "",
        //        "Jason Boduch", KanevaGlobals.Encrypt("1111"), "Visa", "12", "", "2011", 0);

        //    // Update purchase transaction
        //    StoreUtility.UpdatePointTransactionBillingInfoId(transactionId, billingInfoId);

        //    // Create cybersource subscription
        //    DataRow drOrder = StoreUtility.GetOrder(orderId);
        //    DataRow drPpt = StoreUtility.GetPurchasePointTransaction(transactionId);

        //    string ccNumEncrypt = KanevaGlobals.Encrypt("4111111111111111");
        //    string ccSecCodeEncrypt = KanevaGlobals.Encrypt("411");

        //    int orderBillingInformationId = StoreUtility.CopyBillingInfoToOrder(0, billingInfoId, addressId);

        //    // Update the purchase_transaction order_billing_id
        //    StoreUtility.UpdatePointTransactionOrderBillingInfoId(transactionId, orderBillingInformationId);

        //    // Get it again to update the orderBillingInformationId to be used in the next call
        //    drPpt = StoreUtility.GetPurchasePointTransaction(transactionId);

        //    string subscriptionIdentifier = "";
        //    string userErrorMessage = "";
        //    uint userSubscriptionId = 0;

        //    // CyberSource

        //    bool bSystemDown = false;
        //    CybersourceAdaptor cybersourceAdaptor = new CybersourceAdaptor();
            
        //    // Everything ok insert the new subscription
        //    UserSubscription userSubscription = new UserSubscription(0, 1, 0, 4,
        //        9.99, true, DateTime.Now, Subscription.SubscriptionStatus.Pending, 0,
        //        "", false, DateTime.Now, Subscription.SubscriptionTerm.BiWeekly, Subscription.SubscriptionTerm.Monthly,
        //        DateTime.Now.AddDays(14), DateTime.Now.AddDays(14),"",0,0, false); ; // TODO: Initialize to an appropriate value

        //    userSubscriptionId = target.InsertUserSubscription(userSubscription);

        //    if (!cybersourceAdaptor.CreateSubscription(drOrder, drPpt, ccNumEncrypt, ccSecCodeEncrypt, "201.23.23.23", ref bSystemDown,
        //        ref userErrorMessage, ref subscriptionIdentifier, 1, userSubscriptionId,
        //        Subscription.SubscriptionTerm.BiWeekly, Subscription.SubscriptionTerm.Monthly))
        //    {
        //        // Failure
        //        // Alonzo show user error messages here!!!!
        //        // ShowMessage("Please correct the following errors:<br><br>" + userErrorMessage);
        //        Assert.IsTrue(false, "Failed cybersource " + userErrorMessage);
        //    }
        //    else
        //    {
        //        // Udpate with sub ident returned from Cybersource
        //        userSubscription.UserSubscriptionId = userSubscriptionId;
        //        userSubscription.SubscriptionIdentifier = subscriptionIdentifier;
        //        userSubscription.StatusId = Subscription.SubscriptionStatus.Active;
        //        target.UpdateUserSubscription(userSubscription);
        //    }



        //    Assert.IsTrue(userSubscriptionId > 0, "Didn't return any results");
        //}

        ///// <summary>
        /////A test for CancelSubscriptionTest 
        /////</summary>
        //[Fact]
        //public void CancelSubscriptionTest()
        //{
        //    SubscriptionFacade target = new SubscriptionFacade();

        //    bool bSystemDown = false;
        //    string userErrorMessage = "";

        //    UserSubscription userSubscription = target.GetUserSubscription(81);

        //    CybersourceAdaptor cybersourceAdaptor = new CybersourceAdaptor();
        //    cybersourceAdaptor.CancelSubscription(ref bSystemDown,ref userErrorMessage, userSubscription);

        //    Assert.Inconclusive("A method that does not return a value cannot be verified.");
        //}

    }
}
