///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using Xunit;

namespace Kaneva.BusinessLayer.BusinessObjectTests
{
    public class ScriptGameItemFacadeTests
    {
        private ScriptGameItemFacade sgiFacade;

        private const int TEST_ZONE_INSTANCE_ID = 1;
        private const int TEST_ZONE_TYPE = 6;
        private const int TEST_TEMPLATE_GLID = 1;
        private const int TEST_USER_ID = 1;

        public ScriptGameItemFacadeTests()
        {
            sgiFacade = new ScriptGameItemFacade();
        }

        [Fact]
        public void AddWorldScriptGameItemsToCustomDeed_AlwaysMakesNewSnapshotsOfQuests()
        {
            // Test zone is instantiated with a set of items of each type. Make sure they all have GIGLIDs.
            PagedList<ScriptGameItem> worldGameItems = sgiFacade.GetWorldScriptGameItems(TEST_ZONE_INSTANCE_ID, TEST_ZONE_TYPE, string.Empty, 1, Int32.MaxValue);
            sgiFacade.SnapshotWorldScriptGameitems(TEST_USER_ID, TEST_ZONE_INSTANCE_ID, TEST_ZONE_TYPE);

            // Store original quest GiGLIDs.
            Dictionary<int, int> originalValues = new Dictionary<int, int>();
            foreach(ScriptGameItem item in worldGameItems.Where(g => g.ItemType.Equals("quest")))
            {
                originalValues[item.GIId] = item.GIGlid;
            }
            Assert.NotEmpty(originalValues);

            // Add game items to the template deed.
            bool results = sgiFacade.AddWorldScriptGameItemsToCustomDeed(TEST_ZONE_INSTANCE_ID, TEST_ZONE_TYPE, TEST_TEMPLATE_GLID, TEST_USER_ID);
            Assert.Equal(true, results);

            PagedList<ScriptGameItem> deedItems = sgiFacade.GetDeedScriptGameItems(TEST_TEMPLATE_GLID, string.Empty, new string[0], string.Empty, 1, Int32.MaxValue);
            foreach(ScriptGameItem deedItem in deedItems.Where(g => g.ItemType.Equals("quest")))
            {
                Assert.NotEqual(originalValues[deedItem.GIId], deedItem.GIGlid);
            }
        }

        [Fact]
        public void ExtractBundledGlids_ReturnsEmptyIfNotInventoryCompatible()
        {
            ScriptGameItem testItem = new ScriptGameItem
            {
                GIGlid = 1,
                Glid = 2,
                Name = "Test Item",
                InventoryCompatible = false
            };

            Assert.Empty(sgiFacade.ExtractBundledGlids(testItem));
        }

        [Fact]
        public void ExtractBundledGlids_PullsFromGlidField()
        {
            ScriptGameItem testItem = new ScriptGameItem
            {
                GIGlid = 1,
                Glid = 2,
                Name = "Test Item",
                InventoryCompatible = true
            };

            Assert.NotEmpty(sgiFacade.ExtractBundledGlids(testItem));
        }

        [Fact]
        public void ExtractBundledGlids_PullsFromTopLevelProperties()
        {
            ScriptGameItem testItem = new ScriptGameItem
            {
                GIGlid = 1,
                Name = "Test Item",
                InventoryCompatible = true
            };
            testItem.Properties.Add("soundGLID", "1");

            Assert.NotEmpty(sgiFacade.ExtractBundledGlids(testItem));
        }

        [Fact]
        public void ExtractBundledGlids_PullsFromBehaviorParams()
        {
            ScriptGameItem testItem = new ScriptGameItem
            {
                GIGlid = 1,
                Name = "Test Item",
                InventoryCompatible = true
            };
            testItem.Properties.Add("behaviorParams", "{animations:[2]}");

            Assert.NotEmpty(sgiFacade.ExtractBundledGlids(testItem));
        }

        [Fact]
        public void MapBundledGlidsToGIGLIDs()
        {
            List<ScriptGameItem> testItems = new List<ScriptGameItem>();
            testItems.Add(new ScriptGameItem { GIGlid = 1 });
            testItems.Add(new ScriptGameItem { GIGlid = 2 });
            testItems.Add(new ScriptGameItem { GIGlid = 3 });

            testItems[0].BundledGlids.Add(1);
            testItems[0].BundledGlids.Add(3);
            testItems[1].BundledGlids.Add(2);
            testItems[2].BundledGlids.Add(3);

            Dictionary<int, List<int>> mapping = sgiFacade.MapBundledGlidsToGIGLIDs(testItems);
            Assert.Contains(1, mapping[1]);
            Assert.Contains(1, mapping[3]);
            Assert.Contains(2, mapping[2]);
            Assert.Contains(3, mapping[3]);
        }

        [Fact]
        public void MapBundledGlidsToGIIds()
        {
            List<ScriptGameItem> testItems = new List<ScriptGameItem>();
            testItems.Add(new ScriptGameItem { GIId = 1 });
            testItems.Add(new ScriptGameItem { GIId = 2 });
            testItems.Add(new ScriptGameItem { GIId = 3 });

            testItems[0].BundledGlids.Add(1);
            testItems[0].BundledGlids.Add(3);
            testItems[1].BundledGlids.Add(2);
            testItems[2].BundledGlids.Add(3);

            Dictionary<int, List<int>> mapping = sgiFacade.MapBundledGlidsToGIIds(testItems);
            Assert.Contains(1, mapping[1]);
            Assert.Contains(1, mapping[3]);
            Assert.Contains(2, mapping[2]);
            Assert.Contains(3, mapping[3]);
        }
    }
}
