///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using Xunit;
using System.Reflection;

namespace Kaneva.BusinessLayer.BusinessObjectTests
{
    public class UserFacadeTests
    {
        private UserFacade userFacade;

        private const int TEST_USER_ID = 123;
        private const int TEST_FACEBOOK_USER_ID = 999;
        private const int TEST_COMMUNITY_ID = 987654;
        private const string TEST_USERNAME = "Test_User";

        public UserFacadeTests()
        {
            userFacade = new UserFacade();
        }

        [Theory]
        [InlineData(25)]
        [InlineData(18)]
        public void UserIsAdult(int age)
        {
            UserFacade uf = new UserFacade();
            User user = new User();
            user.Age = age;
            Assert.True(uf.IsUserAdult(user));
        }

        [Fact]
        public void UserIsNotAdult()
        {
            UserFacade uf = new UserFacade();
            User user = new User();
            user.Age = 17;
            Assert.False(uf.IsUserAdult(user));
        }

        [Fact]
        public void UserIsAutomatedTestAccount()
        {
            UserFacade uf = new UserFacade();
            User user = new User();
            user.Role = (int)SiteRole.KanevaRoles.Automated_Test_Account;
            Assert.True(uf.IsUserAutomatedTestAccount(user));
        }

        [Fact]
        public void UserIsNotAutomatedTestAccount()
        {
            UserFacade uf = new UserFacade();
            User user = new User();
            user.Role = (int)SiteRole.KanevaRoles.Site_Member;
            Assert.False(uf.IsUserAutomatedTestAccount(user));
        }

        // Can't run this test until we find solution for testing RedeemPacket in FameFacade
        //        [Fact]
        //        public void ConnectUserToFacebook_AddsUserFacebookSettings()
        //        {
        //            int result = userFacade.ConnectUserToFacebook(TEST_USER_ID, TEST_FACEBOOK_USER_ID, false, "abcd1234");
        //            Assert.Equal(1, result);
        //        }

        [Fact]
        public void UpdateUserPreferences_AddNewPreference()
        {
            Preferences preferences = new Preferences();
            preferences.UserId = TEST_USER_ID;
            preferences.AlwaysPurchaseWithCurrencyType = Currency.CurrencyType.REWARDS;
            preferences.FilterByCountry = false;

            int result = userFacade.UpdateUserPreferences(preferences);
            Assert.Equal(1, result);
        }

        [Fact]
        public void GetUserPreferences_InsertAndRetrieve()
        {
            Preferences preferences = new Preferences();
            preferences.UserId = TEST_USER_ID;
            preferences.AlwaysPurchaseWithCurrencyType = Currency.CurrencyType.REWARDS;
            preferences.FilterByCountry = false;
            userFacade.UpdateUserPreferences(preferences);

            Preferences prefs = userFacade.GetUserPreferences(TEST_USER_ID);
            Assert.True(preferences.Equals(prefs));
        }

        [Fact]
        public void GetUserNotificationPreferences()
        {
            NotificationPreferences np = new NotificationPreferences();
            np.UserId = TEST_USER_ID;
            np.NotifyBlastComments = true;
            np.NotifyEventInvites = true;
            np.NotifyFriendBirthdays = true;
            np.NotifyMediaComments = false;
            np.NotifyShopCommentsPurchases = false;
            np.NotifyWorldBlasts = true;
            np.NotifyWorldRequests = true;
            userFacade.UpdateUserNotificationPreferences(np);

            NotificationPreferences np2 = userFacade.GetUserNotificationPreferences(TEST_USER_ID);
            Assert.True(np.UserId == np2.UserId && np.NotifyBlastComments == np2.NotifyBlastComments && np.NotifyMediaComments == np2.NotifyMediaComments);
        }

        [Fact]
        public void UpdateUserHomeCommunityId()
        {
            int communityId = TEST_COMMUNITY_ID;

            // Insert record to update
            int result = userFacade.UpdateUserHomeCommunityId(TEST_USER_ID, communityId);

            if (result == 1)
            {
                // Update the record now
                communityId = 223344;
                result = userFacade.UpdateUserHomeCommunityId(TEST_USER_ID, communityId);
            }

            Assert.Equal(1, result);
        }

        [Fact]
        public void InsertFriendGroup ()
        {
            FriendGroup fg = new FriendGroup();
            fg.OwnerId = TEST_USER_ID;
            fg.Name = "Test Friend Group";

            // Insert record to update
            int result = userFacade.InsertFriendGroup(fg);

            Assert.Equal(1, result);
        }

        // AdjustUserBalances calls SP and need this to add userbalance data
        //[Fact]
        //public void GetUserBalances()
        //{
        //    UserBalances ub = new UserBalances();
        //    preferences.UserId = TEST_USER_ID;
        //    preferences.AlwaysPurchaseWithCurrencyType = Currency.CurrencyType.REWARDS;
        //    preferences.FilterByCountry = false;
        //    userFacade.UpdateUserPreferences(preferences);

        //    Preferences prefs = userFacade.GetUserPreferences(TEST_USER_ID);
        //    Assert.True(preferences.Equals(prefs));
        //}

    }
}
