///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kaneva.BusinessLayer.BusinessObjects;
using Xunit;

namespace Kaneva.BusinessLayer.BusinessObjectTests
{
    public class MetaGameItemTests
    {
        [Fact]
        public void CannotConvertInvalidCurrency()
        {
            MetaGameItem mGameItem = new MetaGameItem();
            Assert.False(mGameItem.CanConvertToCurrency("BOGUS_CURRENCY"));
        }

        [Fact]
        public void CanConvertToRewards()
        {
            MetaGameItem mGameItem = new MetaGameItem("Test Item", "TestType", 10, 0, 100, 0);
            Assert.True(mGameItem.CanConvertToCurrency(Currency.CurrencyType.REWARDS));
            Assert.Equal(mGameItem.ConvertToCurrency(Currency.CurrencyType.REWARDS, 5, 0), 50);
        }

        [Fact]
        public void CanConvertToCredits()
        {
            MetaGameItem mGameItem = new MetaGameItem("Test Item", "TestType", 0, 10, 0, 100);
            Assert.True(mGameItem.CanConvertToCurrency(Currency.CurrencyType.CREDITS));
            Assert.Equal(mGameItem.ConvertToCurrency(Currency.CurrencyType.CREDITS, 5, 0), 50);
        }

        [Fact]
        public void CannotExceedConversionLimit()
        {
            MetaGameItem mGameItem = new MetaGameItem("Test Item", "TestType", 0, 10, 0, 100);
            Assert.Equal(mGameItem.ConvertToCurrency(Currency.CurrencyType.CREDITS, 5, 100), 0);
        }
    }
}
