///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kaneva.BusinessLayer.BusinessObjects;
using Xunit;

namespace Kaneva.BusinessLayer.BusinessObjectTests
{
    public class WOKItemTests
    {
        [Theory]
        [InlineData((int)WOKItem.USE_TYPE_CUSTOM_DEED, 50, 0, 0, 50)]
        [InlineData((int)WOKItem.USE_TYPE_SCRIPT_GAME_ITEM, 55, 0, 0, 55)]
        [InlineData((int)WOKItem.USE_TYPE_CLOTHING, 0, 65, 0, 65)]
        [InlineData((int)WOKItem.USE_TYPE_CLOTHING, 0, 50, 1, 55)]
        public void WebPrice(int useType, int mktCost, int webPrice, uint itemCreatorId, int expectedPrice)
        {
            WOKItem wGameItem = new WOKItem();
            wGameItem.UseType = useType;
            wGameItem.MarketCost = mktCost;
            wGameItem.WebPrice = webPrice;
            wGameItem.ItemCreatorId = itemCreatorId;
            Assert.Equal(wGameItem.WebPrice, expectedPrice);
        }
        
        [Fact]
        public void IsKanevaOwned()
        {
            WOKItem wGameItem = new WOKItem();
            wGameItem.ItemCreatorId = 4230696;
            Assert.True(wGameItem.IsKanevaOwned);
        }

        [Fact]
        public void IsNotKanevaOwned()
        {
            WOKItem wGameItem = new WOKItem();
            wGameItem.ItemCreatorId = 1;
            Assert.False(wGameItem.IsKanevaOwned);
        }
    }
}
