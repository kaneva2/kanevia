///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;
using log4net;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject (true)]
    public class SitemapFacade
    {
        private ISitemapDao sitemapDao = DataAccess.SitemapDao;
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        /// <summary>
        /// GetCommunities - returns list of communities
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<string> GetCommunities (int communityTypeId, string filter, string orderBy, int pageNumber, int pageSize)
        {
            return sitemapDao.GetCommunities (communityTypeId, filter, orderBy, pageNumber, pageSize);
        }

        /// <summary>
        /// GetUsers - returns list of users
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<string> GetUsers (string filter, string orderby, int pageNumber, int pageSize)
        {
            return sitemapDao.GetUsers (filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// GetAssets - returns list of assets
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<Int32> GetAssets (string filter, string orderby, int pageNumber, int pageSize)
        {
            return sitemapDao.GetAssets (filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// GetShopItems - returns list of items in shop
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<Int32> GetShopItems (string filter, string orderby, int pageNumber, int pageSize)
        {
            return sitemapDao.GetShopItems (filter, orderby, pageNumber, pageSize);
        }
    }
}
