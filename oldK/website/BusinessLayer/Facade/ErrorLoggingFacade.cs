///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class ErrorLoggingFacade
    {
        private IErrorLoggingDao errorDao = DataAccess.ErrorLoggingDao;

        /// <summary>
        /// Insert Disconnect Error Logg
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertDisconnectError(int errorType, string description, DateTime errorTime, int userId, int errorCode)
        {
            return errorDao.InsertErrorLog(errorType, description, errorTime, userId, errorCode); ;
        }
    }
}
