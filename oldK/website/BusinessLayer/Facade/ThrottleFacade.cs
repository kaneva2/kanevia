///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;
using log4net;

namespace Kaneva.BusinessLayer.Facade
{
    public class ThrottleFacade
    {

        /// <summary>
        /// CheckThrottle
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public ThrottleResults CheckThrottle(int userId, int appId, ThrottleConfigType throttleConfigType)
        {
            string cacheKey = Kaneva.DataLayer.DataObjects.CentralCache.keyThrottleStore + userId + "a" + appId + "t" + (int)throttleConfigType;

            // Get throttle config for this type
            ThrottleConfig throttleConfig = Configuration.GetThrottleConfiguration(throttleConfigType);

            m_logger.Debug("ThrottleFacade.CheckThrottle:: ThrottleConfigType=" + throttleConfig.ThrottleConfigType.ToString() +
                " ThrottleType=" + throttleConfig.ThrottleType.ToString() + " MinuteFrequency=" + throttleConfig.MinuteFrequency.ToString() +
                " Limit=" + throttleConfig.Limit.ToString());

            // Check for cache entry
            Object throttleCount = (Object)Kaneva.DataLayer.DataObjects.CentralCache.Get(cacheKey);

            m_logger.Debug("ThrottleFacade.CheckThrottle:: ThrottleCount=" + (throttleCount ?? 1).ToString());

            // If null insert a new one
            if (throttleCount == null)
            {
                throttleCount = 1;

                // Determine duration of throttle. If MinuteFrequency is a multiple of days, subtract the time needed to reach the next full day.
                // This allows us to create throttles such as "allow X times per day" without forcing a full 24-hour time difference.
                uint minutesInaDay = 1440;
                uint throttleDuration = throttleConfig.MinuteFrequency;

                if (throttleDuration % minutesInaDay == 0)
                {
                    DateTime currentDateTime = DateTime.Now;
                    DateTime tomorrow = currentDateTime.AddDays(1).Date;
                    uint minutesTillTomorrow = (uint)tomorrow.Subtract(currentDateTime).TotalMinutes;
                    throttleDuration -= minutesTillTomorrow;
                }

                m_logger.Debug("ThrottleFacade.CheckThrottle:: ThrottleDuration=" + throttleDuration);

                Kaneva.DataLayer.DataObjects.CentralCache.Store(cacheKey, Convert.ToInt32(throttleCount), TimeSpan.FromMinutes(throttleDuration));
            }
            // else if overlimit deny
            else if (Convert.ToUInt32(throttleCount) >= throttleConfig.Limit)
            {
                m_logger.Debug("ThrottleFacade.CheckThrottle:: Over Limit");
                return ThrottleResults.OverLimit;
            }
            // else increment counter
            else
            {
                Kaneva.DataLayer.DataObjects.CentralCache.Replace (cacheKey, Convert.ToInt32(throttleCount) + 1);
            }

            m_logger.Debug("ThrottleFacade.CheckThrottle:: OK");
            return ThrottleResults.Ok;
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}
