///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.BusinessLayer.Facade
{
    public interface ISocial
    {
        FacebookUser GetCurrentUser (string accessToken, out bool isError, out string errMsg);
        FacebookFriendList GetCurrentUsersFriends (string accessToken, out bool isError, out string errMsg);
        void PostToCurrentUsersWall (string accessToken, string msg, out bool isError, out string errMsg);
    }
}
