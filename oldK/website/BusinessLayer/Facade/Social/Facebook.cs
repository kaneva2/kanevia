///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Facebook;
using System.Web;
using System.Xml.Serialization;
using System.Web.Script.Serialization;
using Kaneva.BusinessLayer.BusinessObjects;


namespace Kaneva.BusinessLayer.Facade
{
    public class Facebook : ISocial
    {
        public FacebookUser GetCurrentUser (string accessToken, out bool isError, out string errMsg)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string> ();
            parameters.Add ("token", accessToken);
            parameters.Add ("path", "me");
            parameters.Add ("action", "get");
            return Invoke<FacebookUser> (parameters, new { fields = "name,id,email,first_name,last_name,birthday,gender,locale" }, out isError, out errMsg);
        }

        public FacebookFriendList GetCurrentUsersFriends (string accessToken, out bool isError, out string errMsg)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string> ();
            parameters.Add ("token", accessToken);
            parameters.Add ("action", "get");

            // As of August 7, 2016, the Facebool Query Language will no longer be active. We are forced to move to the GraphAPI to
            // retrieve a users friends list. As of v2.6, the API has the following limitations which we did not have with FQL:
            //  1. The API does not allow for any type of ordering the returned list
            //  2. Only friends who installed this app are returned in API v2.0 and higher. total_count in summary represents 
            //     the total number of friends, including those who haven't installed the app

            // parameters.Add ("path", "fql");
            // var query = "SELECT uid, name, pic_square, locale FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me()) ORDER BY last_name";
            // return Invoke<FacebookFriendList> (parameters, new { q = query }, out isError, out errMsg);

            // To use the GraphAPI, then use these params.  Just can't set order with api, must use FQL
            parameters.Add ("path", "/me/friends");
            return Invoke<FacebookFriendList> (parameters, new { fields = "name,id,picture,locale" }, out isError, out errMsg);
        }

        public void PostToCurrentUsersWall (string accessToken, string msg, out bool isError, out string errMsg)
        {
            var args = new Dictionary<string, object> ();
            args["message"] = msg;
            args["caption"] = "This is caption!";
            args["description"] = "This is description!";
            args["name"] = "This is name!";
            //    args["picture"] = "[your image URL]";
            //    args["link"] = "[your link URL]";

            Dictionary<string, string> parameters = new Dictionary<string, string> ();
            parameters.Add ("token", accessToken);
            parameters.Add ("path", "/me/feed");
            parameters.Add ("action", "post");

            Invoke<dynamic> (parameters, args, out isError, out errMsg);
        }

        private T Invoke<T> (IDictionary<String, String> parameters, Object fbParams, out bool isError, out string errMsg)
        {
            try
            {
                var JSONresult = "";
                isError = false;
                errMsg = "";

                FacebookClient client = new FacebookClient (parameters["token"]);


                if (parameters["action"] == "get")
                {
                    JSONresult = client.Get (parameters["path"], fbParams).ToString ();
                }
                else //if (parameters["action"] == "post")
                {
                    JSONresult = client.Post (parameters["path"], fbParams).ToString ();
                }

                JavaScriptSerializer ser = new JavaScriptSerializer ();
                return ser.Deserialize<T> (JSONresult);
            }
            catch (FacebookOAuthException ex)
            {
                // oauth exception occurred
                errMsg = ex.Message;
            }
            catch (FacebookApiLimitException ex)
            {
                // api limit exception occurred.
                errMsg = ex.Message;
            }
            catch (FacebookApiException ex)
            {
                // other general facebook api exception
                errMsg = ex.Message;
            }
            catch (Exception ex)
            {
                // non-facebook exception such as no internet connection.
                errMsg = ex.Message;
            }

            isError = true;
            return default(T);
        }

        private FacebookClient GetClient (string accessToken)
        {
            return new FacebookClient (accessToken);
        }
    }
}
