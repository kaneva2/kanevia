///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;
using System.Data;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;
using System.Collections.Specialized; 

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class SiteMgmtFacade
    {
        private ISiteManagementDao siteManagementDao = DataAccess.SiteManagementDao;
        public const int ADMIN_SITEROLEID = 1; //this method should be revisted maybe moved to webconfig
        public const int ADMIN_ROLEID = 58; //this method should be revisted maybe moved to webconfig
        public const string ADMIN_FILTER = "" + ROLE_ID + "='58'";
        public const string AUTHORIZED_FILTER = "" + ROLE_ID + ">2";
        public const string ROLE_ID = "role";

        /// <summary>
        /// Gets the available websites that the tool manages
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<WebSite> GetAvailableWebsites()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.GetAvailableWebsites();
        }

        /// <summary>
        ///Adds a new top menu item
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddTopMenuItem(SM_MenuItem menuItem)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.AddMainMenuItem(menuItem);
        }

        /// <summary>
        ///Adds a new top menu item
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddSubMenuItem(SM_MenuItem subMenuItem)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.AddSubMenuItem(subMenuItem);
        }

        /// <summary>
        /// Updates Top menu Item
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateTopMenuItem(SM_MenuItem menuItem)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.UpdateMainMenuItem(menuItem);
        }

        /// <summary>
        /// Updates Top menu Item
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateSubMenuItem(SM_MenuItem subMenuItem)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.UpdateSubMenuItem(subMenuItem);
        }

        /// <summary>
        /// Deletes indicated top menu item
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteTopMenuItem(int menuItemId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.DeleteMainMenuItem(menuItemId);
        }

        /// <summary>
        /// Deletes indicated top menu item
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteSubMenuItem(int subMenuItemId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.DeleteSubMenuItem(subMenuItemId);
        }

        /// <summary>
        /// Adds a new available website
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddAvailableWebsite(WebSite website)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.AddAvailableWebsite(website);
        }

        /// <summary>
        /// Updates available website
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateAvailableWebsite(WebSite website)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.UpdateAvailableWebsite(website);
        }

        /// <summary>
        /// Deletes available website
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteAvailableWebsite(int websiteId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.DeleteAvailableWebsite(websiteId);
        }

        /// <summary>
        /// Adds a new available website
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddUserControls(KanevaUserControl userControl)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.AddUserControl(userControl);
        }

        /// <summary>
        /// Updates available website
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateUserControl(KanevaUserControl userControl)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.UpdateUserControl(userControl);
        }

        /// <summary>
        /// Deletes user control
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteUserControl(int userControlId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.DeleteUserControl(userControlId);
        }

        /// <summary>
        /// Gets the available report types
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetUserControlTypes()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.GetUserControlTypes();
        }

        /// <summary>
        /// Gets the available main menu items for the selected website
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<SM_MenuItem> GetMainMenu(int siteId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.GetMainMenu(siteId);
        }

        /// <summary>
        /// Gets the available submenus of the provided main navigation
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<SM_MenuItem> GetSubMenu(int mainNavId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.GetSubMenu(mainNavId);
        }

        /// <summary>
        /// Gets the available main menu items for the selected website
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<SM_MenuItem> GetMainMenuFiltered(int siteId, string privilegeList)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.GetMainMenuFiltered(siteId, privilegeList);
        }

        /// <summary>
        /// Gets the available submenus of the provided main navigation
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<SM_MenuItem> GetSubMenuFiltered(int mainNavId, string privilegeList)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.GetSubMenuFiltered(mainNavId, privilegeList);
        }


        /// <summary>
        /// Gets the available submenus of the provided main navigation
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public KanevaUserControl GetUserControlByID(int userControlId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.GetUserControlByID(userControlId);
        }

        /// <summary>
        /// Gets the available submenus of the provided main navigation
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public KanevaUserControl GetUserControlByName(string name)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.GetUserControlByName(name);
        }

        /// <summary>
        /// Gets the available submenus of the provided main navigation
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<KanevaUserControl> GetAllUserControls()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            //zero will return them all
            return siteManagementDao.GetUserControls(0,null);
        }

        /// <summary>
        /// Gets the available submenus of the provided main navigation
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public string GetUserControlPath(int userControlId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteManagementDao.GetUserControlPath(userControlId);
        }

        /// <summary>
        /// InsertCSRLog
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertCSRLog(int userId, string activity, int assetId, int updatedUserId)
        {
            return siteManagementDao.InsertCSRLog(userId, activity, assetId, updatedUserId);    
        }

        /// <summary>
        /// GetNumberChannelShares
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
         public int GetNumberChannelShares(DateTime dtStartDate, DateTime dtEndDate)
        {
            return siteManagementDao.GetNumberChannelShares(dtStartDate, dtEndDate);
        }

        /// <summary>
        /// GetNumberChannelShares
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetNumberPublicChannelShares(DateTime dtStartDate, DateTime dtEndDate)
        {
            return siteManagementDao.GetNumberPublicChannelShares(dtStartDate, dtEndDate);
        }

        /// <summary>
        /// GetNumberChannelShares
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetNumberPersonalChannelShares(DateTime dtStartDate, DateTime dtEndDate)
        {
            return siteManagementDao.GetNumberPersonalChannelShares(dtStartDate, dtEndDate);
        }

        /// <summary>
        /// Gets a paged list of users who are site administrators
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<User> GetAuthorizedUsersList(string username, string email, string firstName, string lastName, int userStatus, string filter, string orderby, int pageNumber, int pageSize)
        {
            //get new user dao
            IUserDao userDao = DataAccess.UserDao;

            return (PagedList<User>) userDao.GetUsersList(username, email, firstName, lastName, userStatus, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Gets a paged list of users who are site administrators
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<User> GetSiteAdminsList(int companyId)
        {
            //get new user dao
            IUserDao userDao = DataAccess.UserDao;

            // TODO: add access security here..
            // TODO: add argument validation here..
            return (List<User>)(userDao.GetUsersList(ADMIN_FILTER, "", 1, int.MaxValue)).List;
        }

        /// <summary>
        /// gather the privileges for the user buy the kaneva role id and stores in a collection
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public NameValueCollection GetUsersRoleByRoleId(User user)
        {
            NameValueCollection collection = new NameValueCollection();
            
            try
            {
                //get new site security facade
                ISiteSecurityDao siteSecurDao = DataAccess.SiteSecurityDao;

                //get the privileges
                DataTable privileges = siteSecurDao.GetPrivilegesByRoleId(user.Role, user.SM_CompanyID);
                foreach (DataRow row in privileges.Rows)
                {
                    //if for some reason the Site role id has not been set set it once for future reference
                    if (user.SM_SiteRoleID <= 0)
                    {
                        user.SM_SiteRoleID = Convert.ToInt32(row["site_role_id"]);
                    }

                    //add privilege to collection
                    collection.Add(row["privilege_id"].ToString(), row["access_level_id"].ToString());
                }
            }
            catch (Exception)
            { }

            return collection;
        }

        /// <summary>
        /// gather the privileges for the user by the site management role id and stores in a collection
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public NameValueCollection GetUsersPrivileges(User user)
        {
            NameValueCollection collection = new NameValueCollection();

            try
            {
                //get new site security facade
                ISiteSecurityDao siteSecurDao = DataAccess.SiteSecurityDao;

                //get the privileges
                DataTable privileges = siteSecurDao.GetRolePrivileges(user.SM_SiteRoleID, user.SM_CompanyID);
                foreach (DataRow row in privileges.Rows)
                {
                    collection.Add(row["privilege_id"].ToString(), row["access_level_id"].ToString());
                }
            }
            catch (Exception)
            { }

            return collection;
        }

        /// <summary>
        /// GetNumberAssetShares
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetNumberAssetShares(DateTime dtStartDate, DateTime dtEndDate)
        {
            return siteManagementDao.GetNumberAssetShares(dtStartDate, dtEndDate);
        }

        /// <summary>
        /// Gets a paged list of users who are site administrators
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<WOK3DPlace> Search3DPlaceByName(string name, int page, int itemsPerPage)
        {
            ICommunityDao commDao = DataAccess.CommunityDao;

            return commDao.Search3DPlaceByName(name, page, itemsPerPage);
        }

        [DataObjectMethod(DataObjectMethodType.Update)]
        public int Set3DPlaceTiedStatus(int channelZoneId, int serverId, string tiedToServer)
        {
            ICommunityDao commDao = DataAccess.CommunityDao;

            return commDao.Set3DPlaceTiedStatus(channelZoneId, serverId, tiedToServer);
        }

        [DataObjectMethod(DataObjectMethodType.Update)]
        public int Set3DPlaceSpinDownDelayMinutes(int zoneInstanceId, int zoneType, int spinDownDelayMinutes)
        {
            ICommunityDao commDao = DataAccess.CommunityDao;

            return commDao.Set3DPlaceSpinDownDelayMinutes(zoneInstanceId, zoneType, spinDownDelayMinutes);
        }
    }
}
