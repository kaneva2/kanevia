///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;
using System.Data;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;
using System.Collections.Specialized;
//using Kaneva.Framework.Transactions;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class GameDeveloperFacade
    {
        private IGameDeveloperDao gameDeveloperDao = DataAccess.GameDeveloperDao;

        /// <summary>
        /// Get a specific game developer.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public GameDeveloper GetGameDeveloperByUserId(int gameDevId)
        {
            //get the game developer information first 
            GameDeveloper gameDev = gameDeveloperDao.GetGameDeveloperByUserId(gameDevId);

            //now populate the game developers user informtation
            gameDev.UserInfo = (new UserFacade()).GetUser(gameDevId);

            //now populate the security privileges
            gameDev.UserPrivileges = GetDeveloperPrivileges(gameDev);

            return gameDev;
        }

        /// <summary>
        /// Get a specific game developer.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public GameDeveloper GetGameDeveloperByEmail(string email)
        {
            //first get the user by the userName
            User userInfo = (new UserFacade()).GetUserByEmail(email);

            //get the developer information
            GameDeveloper gameDev = gameDeveloperDao.GetGameDeveloperNoUserInfo(userInfo.UserId);

            //assign the user information to the developer
            gameDev.UserInfo = userInfo;

            //now populate the security privileges
            gameDev.UserPrivileges = GetDeveloperPrivileges(gameDev);
            
            return gameDev;
        }

        /// <summary>
        /// Get a specific game developer.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public GameDeveloper GetGameDeveloperNoUserInfo(int userId, int companyId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            GameDeveloper gameDev = gameDeveloperDao.GetGameDeveloperNoUserInfo(userId, companyId);

            //now populate the security privileges
            gameDev.UserPrivileges = GetDeveloperPrivileges(gameDev);
            
            return gameDev;
        }

        /// <summary>
        /// Gets a paged list of developers
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<GameDeveloper> GetGameDevelopersList(int companyId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameDeveloperDao.GetGameDevelopersList(companyId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Gets a paged list of developers
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<GameDeveloper> GetCompanyAdminsList(int companyId)
        {
            //get new site security facade
            ISiteSecurityDao siteSecurDao = DataAccess.SiteSecurityDao;

            //get admin roles
            string filter = "company_id = " + companyId.ToString() + " AND " + GameDeveloper.ADMIN_FILTER;
            List<SiteRole> adminRole = (List<SiteRole>)(siteSecurDao.GetAllRolesList(filter, "", 1, int.MaxValue)).List;

            //get site role id
            int siteRoleId = adminRole[0].SiteRoleId;

            //create new filter for game developer
            filter = "role_id = " + siteRoleId.ToString();

            // TODO: add access security here..
            // TODO: add argument validation here..
            return (List<GameDeveloper>)(gameDeveloperDao.GetGameDevelopersList(companyId, filter, "", 1, int.MaxValue)).List;
        }

        /// <summary>
        /// Gets a list of users
        /// </summary>
        /*[DataObjectMethod(DataObjectMethodType.Select)]
        public PagedDataTable GetGameDevelopers(int companyId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameDeveloperDao.GetGameDevelopers(companyId, filter, orderby, pageNumber, pageSize);
        }*/

        /// <summary>
        /// Insert Game Developer
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertGameDeveloper(GameDeveloper gameDev)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameDeveloperDao.InsertGameDeveloper(gameDev);
        }

        /// <summary>
        /// Insert Game Developer
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int InsertGameDeveloperEmail (string email)
        {
            return gameDeveloperDao.InsertGameDeveloperEmail (email);
        }

        /// <summary>
        /// Insert Game Developer
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int UpdateGameDeveloper(GameDeveloper gameDev)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameDeveloperDao.UpdateGameDeveloper(gameDev);
        }

                /// <summary>
        /// Insert Game Developer
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteGameDeveloper(int gameDevId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameDeveloperDao.DeleteGameDeveloper(gameDevId);
        }

        /// <summary>
        /// Get an Invitation by invite id and company id.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DeveloperInvitation GetCompanyInvitation(int inviteId, int companyId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameDeveloperDao.GetCompanyInvitation(inviteId, companyId);
        }

        /// <summary>
        /// Get an Invitation by validation key and company id.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DeveloperInvitation GetCompanyInvitation(string validationKey)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameDeveloperDao.GetCompanyInvitation(validationKey);
        }

        /// <summary>
        /// Gets a paged list of invitations
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<DeveloperInvitation> GetCompanyInvitationList(int companyId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameDeveloperDao.GetCompanyInvitationsList(companyId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Insert Invitation
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertInvitation(DeveloperInvitation invite)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameDeveloperDao.InsertInvitation(invite);
        }

        /// <summary>
        /// Delete Invitation
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteInvite(DeveloperInvitation invite)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameDeveloperDao.DeleteInvitation(invite);
        }

        /// <summary>
        /// Delete Invitation
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int TrackDownLoads(User user, string itemDownLoaded)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameDeveloperDao.TrackDownLoads(user, itemDownLoaded);
        }

        /// <summary>
        /// gather the privileges for the game developer and stores in a collection
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        private NameValueCollection GetDeveloperPrivileges(GameDeveloper gameDev)
        {
            NameValueCollection collection = new NameValueCollection();

            try
            {
                //get new site security facade
                ISiteSecurityDao siteSecurDao = DataAccess.SiteSecurityDao;

                //get the privileges
                DataTable privileges = siteSecurDao.GetRolePrivileges(gameDev.DeveloperRoleId, gameDev.CompanyId);
                foreach (DataRow row in privileges.Rows)
                {
                    collection.Add(row["privilege_id"].ToString(), row["access_level_id"].ToString());
                }
            }
            catch (Exception)
            { }

            return collection;
        }



    }
}
