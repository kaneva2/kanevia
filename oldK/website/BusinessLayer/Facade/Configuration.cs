///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.BusinessLayer.Facade
{
	/// <summary>
	/// Summary description for Configuration.
	/// </summary>
	public class Configuration
	{
        public Configuration()
		{
		}

        public static string KanevaSetupInstallURL(IEnumerable<string> userExperimentGroups)
        {
            string ret = System.Configuration.ConfigurationManager.AppSettings["KanevaSetupInstallURL"];

            // Check configs for a url matching experiment group
            userExperimentGroups = (userExperimentGroups ?? new List<string>());
            foreach(string group in userExperimentGroups)
            {
                string groupSetting = System.Configuration.ConfigurationManager.AppSettings["KanevaSetupInstallURL_" + group];
                if (!string.IsNullOrWhiteSpace(groupSetting))
                {
                    ret = groupSetting;
                    break;
                }
            }
                
            if (ret == null)
                ret = "http://alphapatcher.kaneva.com/PatchData/signup/KanevaSetup.exe";

            return ret;
        }

        public static bool UseAmazonS3Storage
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["UseAmazonS3Storage"];
                if (ret == null)
                    ret = "false";
                return Convert.ToBoolean(ret);
            }
        }

        public static string AmazonS3Bucket 
		{
			get
			{
                string ret = System.Configuration.ConfigurationManager.AppSettings["AmazonS3Bucket"];
				if ( ret == null )
					ret = "kaneva3";
				return ret;
			}
		}

        public static string AmazonS3AccessKeyId 
		{
			get
			{
                string ret = System.Configuration.ConfigurationManager.AppSettings["AmazonS3AccessKeyId"];
				if ( ret == null )
					ret = "AKIAI5N3U4K4YXHSXNMA";
				return ret;
			}
		}

        public static string AmazonS3SecretAccessKey 
		{
			get
			{
                string ret = System.Configuration.ConfigurationManager.AppSettings["AmazonS3SecretAccessKey"];
				if ( ret == null )
					ret = "07WCCV4t1a6FjrNc/zx0RGPQYEqmeNE1XjCxQ2iH";
				return ret;
			}
		}
        

        public static double TourRewardsLooter
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["TourRewardsLooter"];
                if (ret == null)
                    ret = "100";
                return Convert.ToDouble(ret);
            }
        }

        public static double TourRewardsWorldOwner
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["TourRewardsWorldOwner"];
                if (ret == null)
                    ret = "25000";
                return Convert.ToDouble(ret);
            }
        }

        public static int TourNumberOfWorlds
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["TourNumberOfWorlds"];
                if (ret == null)
                    ret = "6";
                return Convert.ToInt32(ret);
            }
        }

        public static bool AutoValidateRegistrationEmail
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["AutoValidateRegistrationEmail"];
                if (ret == null)
                    ret = "false";
                return Convert.ToBoolean(ret);
            }
        }

        public static bool AutomatedTestUserGenerationEnabled
        {
            get
            {
                string setting = (System.Configuration.ConfigurationManager.AppSettings["AutomatedTestUserGenerationEnabled"] ?? "false");
                return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["AutomatedTestUserGenerationEnabled"]);
            }
        }

        public static string AutomatedTestUserGenerationKey
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["AutomatedTestUserGenerationKey"] ?? string.Empty;
            }
        }

        public static int AutomatedTestUserDefaultId
        {
            get
            {
                return Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["AutomatedTestUserDefaultId"] ?? "4168104");
            }
        }

        public static bool MetricPageTimingEnabled
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["MetricPageTimingEnabled"] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["MetricPageTimingEnabled"]);
                }
            }
        }

        public static int MetricsPageThrottleInSeconds
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["MetricsPageThrottleInSeconds"];
                if (ret == null)
                    ret = "60";
                return Convert.ToInt32(ret);
            }
        }

        /// <summary>
        /// Get list of Pages to track Metrics on
        /// </summary>
        public static System.Collections.Hashtable MetricsPageTiming
        {
            get
            {
                return (System.Collections.Hashtable)System.Configuration.ConfigurationManager.GetSection("MetricsPageTiming");
            }
        }

        public static int KanevaUserId
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["KanevaUserId"];
                if (ret == null)
                    ret = "4230696";
                return Convert.ToInt32(ret);
            }
        }

        public static int BaseDeedPrice 
		{
			get
			{
                string ret = System.Configuration.ConfigurationManager.AppSettings["BaseDeedPrice"];
				if ( ret == null )
					ret = "2000";
				return Convert.ToInt32(ret);
			}
		}

        public static int BaseGameDeedPrice
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["BaseGameDeedPrice"];
                if (ret == null)
                    ret = "4000";
                return Convert.ToInt32(ret);
            }
        }

        public static int LevelIdToBlastCommunity 
		{
			get
			{
                string ret = System.Configuration.ConfigurationManager.AppSettings["LevelIdToBlastCommunity"];
                if (ret == null)
                {
                    return 20;
                }
				return Convert.ToInt32(ret);
			}
		}

        /// <summary>
        /// Retrieves the WOK Game id since it is different on dev/pview/rc/prod  prod is 3296
        /// </summary>
        public static int WokGameId
        {
            get
            {
                string s = System.Configuration.ConfigurationManager.AppSettings["WokGameId"];
                if (s == null)
                    s = "3296";
                return Convert.ToInt32(s);
            }
        }

        /// <summary>
        /// Retrieves the WOK Game name since it is different on dev/pview/rc/prod
        /// </summary>
        public static string WokGameName
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["WokGameName"] ?? "World of Kaneva";
            }
        }

        public static string WokPatcherUrl 
		{
			get
			{
				string ret = System.Configuration.ConfigurationManager.AppSettings ["wokPatcherUrl"];
				if ( ret == null )
					ret = "http://www.kaneva.com/patch/wok";
				return ret;
			}
		}

        /// <summary>
        /// get the Url of the Swf if an AP game is in a nonAP zone
        /// </summary>
        public static string NonAPSwfGameReplacement
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["NonAPSwfGameReplacement"];
                if (ret == null)
                    ret = "";
                return ret;
            }
        }

        /// <summary>
        /// get the Url of the Swf for a flash frame if an AP game is in a nonAP zone
        /// </summary>
        public static string NonAPSwfFrameReplacement
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["NonAPSwfFrameReplacement"];
                if (ret == null)
                    ret = "";
                return ret;
            }
        }

        /// <summary>
        /// get the Url of the image for an imageif an AP image is in a nonAP zone
        /// </summary>
        public static string NonAPImageReplacement
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["NonAPImageReplacement"];
                if (ret == null)
                    ret = "";
                return ret;
            }
        }

        /// <summary>
        /// get the assetId of the image for an imageif an AP image is in a nonAP zone
        /// </summary>
        public static Int32 NonAPImageReplacementId
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["NonAPImageReplacementId"];
                if (ret == null)
                    ret = "0";
                return Int32.Parse(ret);
            }
        }

        /// <summary>
		/// Retrieves the Announce URL
		/// </summary>
		public static string AnnounceURL
		{
			get 
			{
				return "http://" + System.Configuration.ConfigurationManager.AppSettings ["tracker_ip"] + ":" + System.Configuration.ConfigurationManager.AppSettings ["tracker_port"] + "/announce";;
			} 
		}

		/// <summary>
		/// Retrieves Blogs per page from Web.Config file.
		/// </summary>
		public static int BlogsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["BlogsPerPage"]);
			} 
		}

        /// <summary>
        /// Retrieves official Access(Mature) pass
        /// </summary>
        public static int AccessPassGroupID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["accessPassGroupId"] == null)
                {
                    return 1;
                }
                else
                {
                    return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["accessPassGroupId"]);
                }
            }
        }

        /// <summary>
        /// Retrieves official VIP pass
        /// </summary>
        public static int VipPassGroupID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["vipPassGroupId"] == null)
                {
                    return 2;
                }
                else
                {
                    return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["vipPassGroupId"]);
                }
            }
        }

        /// <summary>
		/// Retrieves Assets per page from Web.Config file.
		/// </summary>
		public static int AssetsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["AssetsPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves Servers per page from Web.Config file.
		/// </summary>
		public static int ServersPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["ServersPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves ServersShowDaysOld per page from Web.Config file.
		/// </summary>
		public static int ServersShowDaysOld
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["ServersShowDaysOld"] == null)
				{
					return 30;
				}
				else
				{
					return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["ServersShowDaysOld"]);
				}
			} 
		}

		/// <summary>
		/// Retrieves cutoff age of minor
		/// </summary>
		public static int MinorCutOffAge
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["AgeCutOff"] == null)
				{
					return 14;
				}
				else
				{
					return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["AgeCutOff"]);
				}
			} 
		}


		/// <summary>
		/// Retrieves Communities per page from Web.Config file.
		/// </summary>
		public static int CommunitiesPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["CommunitiesPerPage"]);
			} 
		}
		
		/// <summary>
		/// Retrieves World Objects per page from Web.Config file.
		/// </summary>
		public static int WorldObjectsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["WorldObjectsPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves Members per page from Web.Config file.
		/// </summary>
		public static int MembersPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MembersPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves Topics per page from Web.Config file.
		/// </summary>
		public static int TopicsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["TopicsPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves Threads per page from Web.Config file.
		/// </summary>
		public static int ThreadsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["ThreadsPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves TransactionsPerPage per page from Web.Config file.
		/// </summary>
		public static int TransactionsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["TransactionsPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves CreditTransactionsPerPage per page from Web.Config file.
		/// </summary>
		public static int CreditTransactionsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["CreditTransactionsPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves Friends per page from Web.Config file.
		/// </summary>
		public static int FriendsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["FriendsPerPage"]);
			} 
		}

		/// <summary>
		/// Retrieves SearchResultsPerPage per page from Web.Config file.
		/// </summary>
		public static int SearchResultsPerPage
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["SearchResultsPerPage"]);
			} 
		}

		/// <summary>
		/// Use Search Farm?
		/// </summary>
		public static bool UseSearchFarm 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["UseSearchFarm"] == null)
				{
					return false;
				}
				else
				{
					return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings ["UseSearchFarm"]);
				}
			} 
		}

		/// <summary>
		/// Max uploadable image size
		/// </summary>
		public static int MaxUploadedImageSize
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MaxUploadedImageSize"]);
			} 
		}

		/// <summary>
		/// Max uploadable Avatar size
		/// </summary>
		public static int MaxUploadedAvatarSize
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MaxUploadedAvatarSize"]);;
			} 
		}

		/// <summary>
		/// Max uploadable Avatar size
		/// </summary>
		public static int MaxUploadedBannerSize
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MaxUploadedBannerSize"]);;
			} 
		}

		/// <summary>
		/// Max uploadable Screenshot size
		/// </summary>
		public static int MaxUploadedScreenShotSize
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MaxUploadedScreenShotSize"]);
			} 
		}

		/// <summary>
		/// MaxNumberOfCommunitiesPerUser
		/// </summary>
		public static int MaxNumberOfCommunitiesPerUser
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MaxNumberOfCommunitiesPerUser"]);
			} 
		}

        /// <summary>
        /// MaxNumberOf3DAppsPerUser
        /// </summary>
        public static int MaxNumberOf3DAppsPerUser
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["MaxNumberOf3DAppsPerUser"] == null)
                {
                    return 10;
                }
                else
                {
                    return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxNumberOf3DAppsPerUser"]);
                }
            }
        }

		/// <summary>
		/// MaxNumberOfLogicalAccounts
		/// </summary>
		public static int MaxNumberOfLogicalAccounts
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MaxNumberOfLogicalAccounts"]);
			} 
		}

		/// <summary>
		/// MaxCommentLength
		/// </summary>
		public static int MaxCommentLength
		{
			get 
			{
				try
				{
					int len = Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MaxCommentLength"]);

					if (len > 0)
					{
						return len;
					}
					else
					{
						return 256;
					}
				}
				catch 
				{
					return 256;
				}
			} 
		}

		/// <summary>
		/// MaxBlastLength
		/// </summary>
		public static int MaxBlastLength
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["MaxBlastLength"] == null)
				{
					return 140;
				}
				else
				{
					return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["MaxBlastLength"]);
				}
			} 
		}

		/// <summary>
		/// NumberOfDaysDownloadIsAvailable
		/// </summary>
		public static int NumberOfDaysDownloadIsAvailable
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["NumberOfDaysDownloadIsAvailable"]);
			} 
		}

		// NumberOfDaysDownloadStopDate
		public static int NumberOfDaysDownloadStopDate
		{
			get 
			{
				return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["NumberOfDaysDownloadStopDate"]);
			} 
		}

		/// <summary>
		/// ShareCatchpaCount
		/// </summary>
		public static int ShareCatchpaCount
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["ShareCatchpaCount"] == null)
				{
					return 50;
				}
				else
				{
					return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings ["ShareCatchpaCount"]);
				}
			} 
		}

		/// <summary>
		/// Retrieves ImageServer string from Web.Config file.
		/// </summary>
		public static string ImageServer 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["ImageServer"] == null)
				{
					return "http://images.kaneva.com";
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["ImageServer"];
				}
			} 
		}

        /// <summary>
        /// Retrieves WOKImageServer string from Web.Config file.
        /// </summary>
        public static string WOKImageServer
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["WOKImageServer"] == null)
                {
                    return "http://images.kaneva.com";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["WOKImageServer"];
                }
            }
        }

        /// <summary>
        /// Retrieves TextureServer string from Web.Config file.
        /// </summary>
        public static string TextureServer
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["TextureServer"] == null)
                {
                    return "http://textures.kaneva.com";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["TextureServer"];
                }
            }
        }

 
		/// <summary>
		/// Retrieves SMTPServer string from Web.Config file.
		/// </summary>
		public static string SMTPServer 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["SMTPServer"] == null)
				{
					return "";
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["SMTPServer"];
				}
			} 
		}

		/// <summary>
		/// Retrieves From string from Web.Config file.
		/// </summary>
		public static string FromEmail
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["FromEmail"] == null)
				{
					return "kaneva@kaneva.com";
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["FromEmail"];
				}
			} 
		}

		/// <summary>
		/// Retrieves From string from Web.Config file.
		/// </summary>
		public static string WebSideStoryAccount
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["WebSideStoryAccount"] == null)
				{
					// Default to dev
					return "DM5701157NDM";
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["WebSideStoryAccount"].ToString ();
				}
			} 
		}

        /// <summary>
        /// Retrieves From string from Web.Config file.
        /// </summary>
        public static string OmnitureReportId
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["OmnitureReportId"] == null)
                {
                    // Default to dev
                    return "kanevadev1";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["OmnitureReportId"].ToString();
                }
            }
        }



		/// <summary>
		/// Retrieves MinimumCreditCardTransactionAmount string from Web.Config file.
		/// </summary>
		public static double MinimumCreditCardTransactionAmount 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["MinimumCreditCardTransactionAmount"] == null)
				{
					return 5.00;
				}
				else
				{
					return Convert.ToDouble (System.Configuration.ConfigurationManager.AppSettings ["MinimumCreditCardTransactionAmount"]);
				}
			} 
		}

		/// <summary>
		/// Retrieves EnableCheckout string from Web.Config file.
		/// </summary>
		public static bool EnableCheckout 
		{
			get 
			{
				return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings ["EnableCheckout"]);
			} 
		}

        /// <summary>
        /// Retrieves EnablePaypal string from Web.Config file.
        /// </summary>
        public static bool EnablePaypal
        {
            get
            {
                try
                {
                    return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["EnablePaypal"]);
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
		/// Retrieves PayPalURL string from Web.Config file.
		/// </summary>
		public static string PayPalURL 
		{
			get 
			{
				return System.Configuration.ConfigurationManager.AppSettings ["PayPalURL"];
			} 
		}

		/// <summary>
		/// Retrieves PayPalBusiness string from Web.Config file.
		/// </summary>
		public static string PayPalBusiness 
		{
			get 
			{
				return System.Configuration.ConfigurationManager.AppSettings ["PayPalBusiness"];
			} 
		}

		/// <summary>
		/// Retrieves PayPalPDTToken string from Web.Config file.
		/// </summary>
		public static string PayPalPDTToken 
		{
			get 
			{
				return System.Configuration.ConfigurationManager.AppSettings ["PayPalPDTToken"];
			} 
		}

		/// <summary>
		/// Retrieves InCommURL string from Web.Config file.
		/// </summary>
		public static string InCommURL 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["InCommURL"] != null)
				{
					return System.Configuration.ConfigurationManager.AppSettings ["InCommURL"];
				}
				else
				{
					return "http://66.147.172.198:8080/transferedvalue/kaneva";
				}
			} 
		}

		/// <summary>
		/// Retrieves InCommAccountNumber string from Web.Config file.
		/// </summary>
		public static string InCommAccountNumber 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["InCommAccountNumber"] != null)
				{
					return System.Configuration.ConfigurationManager.AppSettings ["InCommAccountNumber"];
				}
				else
				{
					return "Kaneva";
				}
			} 
		}


        /// <summary>
        /// Retrieves SSLLogin string from Web.Config file.
        /// </summary>
        public static bool SSLLogin
        {
            get
            {
                return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["SSLLogin"]);
            }
        }

        /// <summary>
        /// Retrieves SSLLogin string from Web.Config file.
        /// </summary>
        public static string SuperRewardsKey
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["SuperRewardsKey"];
            }
        }

        /// <summary>
        /// Retrieves Super rewards URL string from Web.Config file.
        /// </summary>
        public static string SuperRewardsURL
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["SuperRewardsURL"];
            }
        }

        /// <summary>
		/// Retrieves FileStoreHack string from Web.Config file.
		/// </summary>
        public static int Credit2DollarRatio 
		{
			get 
			{
                if (System.Configuration.ConfigurationManager.AppSettings["Credit2Dollar"] != null)
				{
                    return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Credit2Dollar"]);
				}
				else
				{
					return 200;
				}
			} 
		}

        /// <summary>
		/// Retrieves LogFile string from Web.Config file.
		/// </summary>
		public static string LogFile 
		{
			get 
			{
				return System.Configuration.ConfigurationManager.AppSettings ["LogFile"];
			} 
		}

        /// <summary>
        /// Retrieves site name of the current string from Web.Config file.
        /// </summary>
        public static string CurrentSiteName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(System.Configuration.ConfigurationManager.AppSettings["CurrentSiteName"]))
                    return System.Configuration.ConfigurationManager.AppSettings["SiteName"];

                return System.Configuration.ConfigurationManager.AppSettings["CurrentSiteName"];
            }
        }

		/// <summary>
		/// Retrieves SiteName string from Web.Config file.
		/// </summary>
        /// <remarks>
        /// Almost every site and service in the system uses this property to mean "Kaneva.com Site Name".
        /// Because of this, "CurrentSiteName" should be used when wishing to reference the hostname of the 
        /// current site (shop.kaneva.com, kwas.kaneva.com, etc.).
        /// </remarks>
		public static string SiteName             
		{
			get 
			{
				return System.Configuration.ConfigurationManager.AppSettings ["SiteName"];
			} 
		}

		/// <summary>
        /// Retrieves ShoppingSiteName string from Web.Config file.
		/// </summary>
		public static string ShoppingSiteName             
		{
			get 
			{
                return System.Configuration.ConfigurationManager.AppSettings["ShoppingSiteName"];
			} 
		}

        /// <summary>
        /// Retrieves Join Location string from Web.Config file.
        /// </summary>
        public static string JoinLocation
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["JoinLocation"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["JoinLocation"];
                }
                else
                {
                    return "~/home.aspx";
                }


            }
        }


        /// <summary>
        /// Retrieves ContentServerPath string from Web.Config file.
        /// </summary>
        public static string ContentServerPath
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["ContentServerPath"];
            }
        }

        /// <summary>
        /// Retrieves ContentServerPath string from Web.Config file.
        /// </summary>
        public static string WOKImagesServerPath
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["WOKImagesPath"];
            }
        }

        /// <summary>
		/// Retrieves FileStoreHack string from Web.Config file.
		/// </summary>
		public static string FileStoreHack 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["FileStoreHack"] != null)
				{
					return System.Configuration.ConfigurationManager.AppSettings ["FileStoreHack"];
				}
				else
				{
					return "";
				}
			} 
		}

        /// <summary>
        /// Retrieves WOKFileStoreHack string from Web.Config file.
		/// </summary>
		public static string WOKFileStoreHack 
		{
			get 
			{
                if (System.Configuration.ConfigurationManager.AppSettings["WOKFileStoreHack"] != null)
				{
                    return System.Configuration.ConfigurationManager.AppSettings["WOKFileStoreHack"];
				}
				else
				{
					return "";
				}
			} 
		}

        
		/// <summary>
		/// Retrieves UploadRepository string from Web.Config file.
		/// </summary>
		public static string UploadRepository 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["uploadRepository"] == null)
				{
					throw new Exception ("UploadRepository is not found in config file");
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["uploadRepository"];
				}
			} 
		}

		/// <summary>
		/// Retrieves StreamingServer string from Web.Config file.
		/// </summary>
		public static string StreamingServer 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["StreamingServer"] == null)
				{
					return "http://KanevaStaging";
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["StreamingServer"];
				}
			} 
		}

		/// <summary>
		/// Retrieves StreamingFile string from Web.Config file.
		/// </summary>
		public static string StreamingFile 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["StreamingFile"] == null)
				{
					return "stream.php";
				}
				else
				{
					return System.Configuration.ConfigurationManager.AppSettings ["StreamingFile"];
				}
			} 
		}

        /// <summary>
        /// Retrieves StreamingFile string from Web.Config file.
        /// </summary>
        public static bool StreamIIS
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["StreamIIS"] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["StreamIIS"]);
                }
            }
        }

		/// <summary>
		/// Retrieves MaxFileSizeToStream string from Web.Config file.
		/// </summary>
		public static Int64 MaxFileSizeToStream 
		{
			get 
			{
				if (System.Configuration.ConfigurationManager.AppSettings ["MaxFileSizeToStream"] == null)
				{
					return 20972000;
				}
				else
				{
					return Convert.ToInt64 (System.Configuration.ConfigurationManager.AppSettings ["MaxFileSizeToStream"]);
				}
			} 
		}

        /// <summary>
        /// Retrieves ParatureAPIURL string from Web.Config file.
        /// </summary>
        public static string ParatureAPIURL
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ParatureAPIURL"] == null)
                {
                    return "https://s3.parature.com/api/v1/5412/5433/";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["ParatureAPIURL"];
                }
            }
        }

        /// <summary>
        /// Retrieves ParatureAPIToken string from Web.Config file.
        /// </summary>
        public static string ParatureAPIToken
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ParatureAPIToken"] == null)
                {
                    return "_token_=Qo7i4q4n/12mqKPajUzVpbc05cIrUpVYxad0g9m2@dkyTjsTZ4aSHS8Op6LAYUiIfENnUzvvMybExxYhCR19NQ==";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["ParatureAPIToken"];
                }
            }
        }

         /// <summary>
        /// Retrieves ParaturePassThroughPortalURL string from Web.Config file.
        /// </summary>
        public static string ParaturePassThroughPortalURL
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ParaturePassThroughPortalURL"] == null)
                {
                    return "http://s3-sandbox.parature.com/ics/support/security2.asp";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["ParaturePassThroughPortalURL"];
                }
            }
        }


         /// <summary>
        /// Retrieves ParatureDropOffURL string from Web.Config file.
        /// </summary>
        public static string ParatureDropOffURL
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ParatureDropOffURL"] == null)
                {
                    return "https://sso-mutual-auth.parature.com/ext/ref/dropoff";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["ParatureDropOffURL"];
                }
            }
        }

        /// <summary>
        /// Retrieves ParaturePassThroughCert string from Web.Config file.
        /// </summary>
        public static string ParaturePassThroughCert
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ParaturePassThroughCert"] == null)
                {
                    return "support_kaneva_com.cer";
                }
                else
                {
                    return System.Configuration.ConfigurationManager.AppSettings["ParaturePassThroughCert"];
                }
            }
        }
        


        /// <summary>
        /// Auto purchase uploaded UGC item
        /// </summary>
        public static bool UGC_AutoPurchaseUploadedItem
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;

                string strValue = GetCachedWebOption("UGC", "AutoPurchaseUploadedItem", role).String;
                if (strValue != null)
                {
                    return strValue == "Y";
                }

                // default value
                return true;
            }
        }

        /// <summary>
        /// Auto purchase uploaded derivative item. Default same as UGC_AutoPurchaseUploadedItem
        /// </summary>
        public static bool UGC_AutoPurchaseUploadedDerivativeItem
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;

                string strValue = GetCachedWebOption("UGC", "AutoPurchaseUploadedDerivativeItem", role).String;
                if (strValue != null)
                {
                    return strValue == "Y";
                }

                // default value
                return UGC_AutoPurchaseUploadedItem;
            }
        }


        /// <summary>
        /// Inventory type of auto purchased item
        /// </summary>
        public static OptionValueMap UGC_AutoPurchasedItemInvType
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;

                OptionValueMap opt = GetCachedWebOption("UGC", "AutoPurchasedItemInvType", role);
                return opt != null ? opt : Default_UGC_AutoPurchasedItemInvType;      // default value: -1 - determine by currency
            }
        }


        /// <summary>
        /// Inventory type of auto-purchased derivative item
        /// </summary>
        public static OptionValueMap UGC_AutoPurchasedDerivativeItemInvType
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;

                OptionValueMap opt = GetCachedWebOption("UGC", "AutoPurchasedDerivativeItemInvType", role);
                return opt != null ? opt: Default_UGC_AutoPurchasedDerivativeItemInvType;  // default value: -1 - determine by currency
            }
        }


        /// <summary>
        /// Currency to use when upload fee is assessed for creating a new item. Default is KPOINT.
        /// </summary>
        public static OptionValueMap UGC_UploadFeeCurrency
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;

                OptionValueMap opt = GetCachedWebOption("UGC", "UploadFeeCurrency", role);
                return opt != null ? opt : Default_UGC_UploadFeeCurrency;                // default value: KPOINT
            }
        }


        /// <summary>
        /// Currency to use when upload fee is assessed for creating a derivative item. Default same as UGC_UploadFeeCurrency.
        /// </summary>
        public static OptionValueMap UGC_DerivativeUploadFeeCurrency
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;

                OptionValueMap opt = GetCachedWebOption("UGC", "DerivativeUploadFeeCurrency", role);
                if (opt != null && opt.String != "")
                    return opt;

                // default value
                return UGC_UploadFeeCurrency;
            }
        }


        /// <summary>
        /// UGC Upload fee formula: coefficient on base price
        /// </summary>
        public static float UGC_UploadFee_BasePriceFactor
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;

                string strValue = GetCachedWebOption("UGC", "UploadFee_BasePriceFactor", role).String;
                if (strValue!=null)
                {
                    try
                    {
                        return (float)Convert.ToDouble(strValue);
                    }
                    catch (System.Exception) { }
                }

                // default value
                return 1;
            }
        }

        /// <summary>
        /// UGC Upload fee formula: coefficient on base commission
        /// </summary>
        public static float UGC_UploadFee_BaseCommissionFactor
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;

                string strValue = GetCachedWebOption("UGC", "UploadFee_BaseCommissionFactor", role).String;
                if (strValue != null)
                {
                    try
                    {
                        return (float)Convert.ToDouble(strValue);
                    }
                    catch (System.Exception) { }
                }

                // default value
                return 1;
            }
        }

        /// <summary>
        /// UGC Upload fee formula: fixed base charge
        /// </summary>
        public static int UGC_UploadFeeBase
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;

                string strValue = GetCachedWebOption("UGC", "UploadFeeBase", role).String;
                if (strValue != null)
                {
                    try
                    {
                        return Convert.ToInt32(strValue);
                    }
                    catch (System.Exception) { }
                }

                // default value
                return 0;
            }
        }

        /// <summary>
        /// UGC Upload fee formula: final discount, range: [0,1]
        /// </summary>
        public static float UGC_UploadFeeFactor
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;

                string strValue = GetCachedWebOption("UGC", "UploadFeeFactor", role).String;
                if (strValue != null)
                {
                    try
                    {
                        return (float)Convert.ToDouble(strValue);
                    }
                    catch (System.Exception) { }
                }

                // default value
                return 1;
            }
        }

        /// <summary>
        /// UGC Upload fee formula: minimal upload fee
        /// </summary>
        public static OptionValueMap UGC_UploadFeeMin
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;

                OptionValueMap opt = GetCachedWebOption("UGC", "UploadFeeMin", role);
                return opt != null ? opt : Default_UGC_UploadFeeMin;    // Default: 0
            }
        }

        /// <summary>
        /// UGC Upload fee formula: maximal upload fee
        /// </summary>
        public static OptionValueMap UGC_UploadFeeMax
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;
                OptionValueMap opt = GetCachedWebOption("UGC", "UploadFeeMax", role);
                return opt != null ? opt : Default_UGC_UploadFeeMax;  // Default: INF
            }
        }

        /// <summary>
        /// UGC Upload fee formula: maximal upload fee
        /// </summary>
        public static float CatalogChargeFactor
        {
            get
            {
                return 0.1f;
            }
        }

        /// <summary>
        /// UGC item selling allowed currency (256, 512, 768)
        /// </summary>
        public static int UGC_SellingCurrency
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;

                string strValue = GetCachedWebOption("UGC", "SellingCurrency", role).String;
                if (strValue != null)
                {
                    try
                    {
                        return Convert.ToInt32(strValue);
                    }
                    catch (System.Exception) { }
                }

                // default value credits
                return 256;
            }
        }

        /// <summary>
        /// UGC item selling price forumla: coefficient on base price
        /// </summary>
        public static float UGC_SellingPriceFactor
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;

                string strValue = GetCachedWebOption("UGC", "SellingPriceFactor", role).String;
                if (strValue != null)
                {
                    try
                    {
                        return (float)Convert.ToDouble(strValue);
                    }
                    catch (System.Exception) { }
                }

                // default value
                return 0;
            }
        }

        /// <summary>
        /// UGC fashion fame level to allow credit only
        /// </summary>
        public static int UGC_CreditOnlyFashionFameLevel
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;

                string strValue = GetCachedWebOption("UGC", "CreditOnlyFashionFameLevel", role).String;
                if (strValue != null)
                {
                    try
                    {
                        return (int)Convert.ToInt32(strValue);
                    }
                    catch (System.Exception) { }
                }

                // default value
                return 25;
            }
        }

        public static OptionValueMap UGC_UploadQuantity
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;
                OptionValueMap opt = GetCachedWebOption("UGC", "UploadQuantity", role);
                return opt != null ? opt : Default_UGC_UploadQuantity;  // Default: INF
            }
        }

        private static OptionValueMap Default_UGC_AutoPurchasedItemInvType = new OptionValueMap("-1");
        private static OptionValueMap Default_UGC_AutoPurchasedDerivativeItemInvType = new OptionValueMap("-1");
        private static OptionValueMap Default_UGC_UploadFeeCurrency = new OptionValueMap("KPOINT");
        private static OptionValueMap Default_UGC_UploadFeeMin = new OptionValueMap("0");
        private static OptionValueMap Default_UGC_UploadFeeMax = new OptionValueMap(Int32.MaxValue.ToString());
        private static OptionValueMap Default_UGC_UploadQuantity = new OptionValueMap("1");

        /* CONTESTS */
        /// <summary>
        /// Retrieves Minimum Number of Contest Entries from Web.Config file.
        /// </summary>
        public static int ContestMinumumEntries
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["Contest_Minimum_Entries"] == null)
                {
                    return 3;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["Contest_Minimum_Entries"]);
                }
            }
        }

        /// <summary>
        /// Retrieves Max Number of Days a Contest can run from Web.Config file.
        /// </summary>
        public static int ContestMaxDays
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["Contest_Max_Days"] == null)
                {
                    return 90;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["Contest_Max_Days"]);
                }
            }
        }

        /// <summary>
        /// Retrieves Min Fame Level a user must be to create a Contest from Web.Config file.
        /// </summary>
        public static int ContestMinimumFameLevelToCreate
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["Contest_Minimum_Fame_Level_To_Create"] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["Contest_Minimum_Fame_Level_To_Create"]);
                }
            }
        }

        /// <summary>
        /// Retrieves Min Fame Level a user must be to enter a Contest from Web.Config file.
        /// </summary>
        public static int ContestMinimumFameLevelToEnter
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["Contest_Minimum_Fame_Level_To_Enter"] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["Contest_Minimum_Fame_Level_To_Enter"]);
                }
            }
        }

        /// <summary>
        /// Retrieves Number of Days Owner has to pick Contest winner once the contest has ended from Web.Config file.
        /// </summary>
        public static int ContestNumDaysToPickWinner
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["Contest_Num_Days_To_Pick_Winner"] == null)
                {
                    return 3;
                }
                else
                {
                    return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["Contest_Num_Days_To_Pick_Winner"]);
                }
            }
        }

        /// <summary>
        /// Retrieves Number of Days Owner has to pick Contest winner once the contest has ended from Web.Config file.
        /// </summary>
        public static bool ContestCompareCurrentUserIP
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["Contest_Compare_Current_User_IP"] == null)
                {
                    return true;
                }
                else
                {
                    return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["Contest_Compare_Current_User_IP"]);
                }
            }
        }


          /// <summary>
        /// Retrieves MaxObjectsAllowedInZone from Web.Config file.
        /// </summary>
        public static int MaxObjectsAllowedInZone
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["MaxObjectsAllowedInZone"] == null)
                {
                    return 1000;
                }
                else
                {
                    return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxObjectsAllowedInZone"]);
                }
            }
        }

        /// <summary>
        /// IM server name
        /// </summary>
        public static String KIM_Server
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;

                OptionValueMap value = GetCachedWebOption("KIM", "Server", role);
                if (value != null)
                {
                    string strValue = value.String;
                    if (strValue != null)
                    {
                        return strValue;
                    }
                }

                // default value
                return "";  // "" -> use client default value
            }
        }

        /// <summary>
        /// IM server port number
        /// </summary>
        public static int KIM_Port
        {
            get
            {
                int role = 0;   // 0: get non-user specific web option
                User user = (User)HttpContext.Current.Items["User"];
                if (user != null)
                    role = user.Role;

                OptionValueMap value = GetCachedWebOption("KIM", "Port", role);
                if (value != null)
                {
                    string strValue = value.String;
                    if (strValue != null)
                    {
                        try
                        {
                            return Convert.ToInt32(strValue);
                        }
                        catch (System.Exception) { }
                    }
                }

                // default value
                return 0;  // 0 -> use client default value
            }
        }

        public static string FacebookGraphUrl
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["FacebookGraphUrl"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["FacebookGraphUrl"];
                }
                else
                {
                    return "http://graph.facebook.com/";
                }
            }
        }

        public static string FacebookProfileImageQueryString
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["FacebookProfileImageQueryString"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["FacebookProfileImageQueryString"];
                }
                else
                {
                    return "/picture?type=square";
                }
            }
        }

        public static string GetFacebookProfileImageQueryStringBySize(string size)
        {
            switch (size)
            {
                case "sm":
                    return "/picture?type=small";
                case "me":
                    return "/picture?type=normal";
                case "la":
                case "xl":
                    return "/picture?type=large";
                case "sq":
                    return "/picture?type=square";
                default:
                    return "/picture?type=square";
            }
        }

        public static double GetEventRewardAmount
        {
             get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["EventRewardAmount"] != null)
                {
                    return Convert.ToDouble (System.Configuration.ConfigurationManager.AppSettings["EventRewardAmount"]);
                }
                else
                {
                    return 1000.00;
                }
            }
        }

        public static DateTime NewUserFunnelStart
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["NewUserFunnelStart"];
                if (ret == null)
                    ret = "12-09-2014 12:00PM";
                return DateTime.Parse(ret);
            }
        }

        #region ThrottleConfig
        public static ThrottleConfig GetThrottleConfiguration(ThrottleConfigType throttleConfigType)
        {
            // Get the appropriate config if it exists
            ThrottleConfigSection throttleConfigSection = System.Configuration.ConfigurationManager.GetSection("ThrottleConfiguration") as ThrottleConfigSection;

            if (throttleConfigSection != null)
            {
                ThrottleConfig throttleConfig = throttleConfigSection.ThrottleConfigs[throttleConfigType];
                if (throttleConfig != null)
                {
                    return throttleConfig;
                }
            }

            // If config not found, return a default config object for this throttle type
            return new ThrottleConfig(throttleConfigType);
        }
        #endregion ThrottleConfig

        #region Web Options From DB

        public static string MakeWebOptionComboKey( string group, string key )
        {
            return group + "." + key;
        }
        
        /// <summary>
        /// RetrieveCachedWebOptions
        /// </summary>
        protected static Dictionary<string, Dictionary<int, OptionValueMap>> RetrieveCachedWebOptions(bool refreshFromDB)
        {
            const string cacheKey = "WebOptions";
            Dictionary<string, Dictionary<int, OptionValueMap>> webOptions = null;

            Monitor.Enter(sLock);

            try
            {
                if (!refreshFromDB)
                    webOptions = (Dictionary<string, Dictionary<int, OptionValueMap>>)HttpRuntime.Cache[cacheKey];

                if (webOptions == null)
                {
                    // cache not initialize or expired, load it from DB now
                    webOptions = new Dictionary<string, Dictionary<int, OptionValueMap>>();
                    IList<WebOption> list = GetWebOptions();
                    foreach (WebOption opt in list)
                    {
                        string comboKey = MakeWebOptionComboKey(opt.Group, opt.Key);
                        Dictionary<int, OptionValueMap> dictRoles = null;
                        if( !webOptions.ContainsKey(comboKey) )
                        {
                            dictRoles = new Dictionary<int, OptionValueMap>();
                            webOptions.Add(comboKey, dictRoles);
                        }
                        else dictRoles = webOptions[comboKey];

                        if (dictRoles.ContainsKey(opt.role))
                            dictRoles[opt.role] = new OptionValueMap(opt.Value);
                        else
                            dictRoles.Add(opt.role, new OptionValueMap(opt.Value));
                    }

                    // insert it into cache with 5 minutes expiration time
                    HttpRuntime.Cache.Insert(cacheKey, webOptions, null, DateTime.Now.AddMinutes(5), System.Web.Caching.Cache.NoSlidingExpiration);
                }
            }
            catch (System.Exception)
            {
            }

            Monitor.Exit(sLock);
            return webOptions;
        }

		/// <summary>
		/// Return values of web options by option group (to be used by Configuration class)
		/// </summary>
		public static Dictionary<string, OptionValueMap> GetCachedWebOptionsByGroup(string group, int role)
		{
			return GetCachedWebOptionsByGroup(group, role, false);
		}

		public static Dictionary<string, OptionValueMap> GetCachedWebOptionsByGroup(string group, int role, bool suppressGroupNamePrefix)
		{
			Dictionary<string, OptionValueMap> result = new Dictionary<string, OptionValueMap>();

			if (group != null)
			{
				Dictionary<string, Dictionary<int, OptionValueMap>> webOptions = RetrieveCachedWebOptions(false);
				if (webOptions != null)
				{
					string groupPrefix = MakeWebOptionComboKey(group, "");
					foreach (KeyValuePair<string, Dictionary<int, OptionValueMap>> pair in webOptions)
					{
						if( pair.Key.Substring(0, groupPrefix.Length)==groupPrefix )
						{
							// group matched
							if( pair.Value.ContainsKey(role) )
							{
								// role matched
								string key = pair.Key;
								if (suppressGroupNamePrefix)
									key = key.Substring(groupPrefix.Length);

								result.Add(key, pair.Value[role]);
							}
							else if (pair.Value.ContainsKey(0))	// use rules from role 0 if not overriden
							{
								// role matched
								string key = pair.Key;
								if (suppressGroupNamePrefix)
									key = key.Substring(groupPrefix.Length);

								result.Add(key, pair.Value[0]);
							}
						}
					}
				}
			}

			return result;
		}

        /// <summary>
        /// Return value of one web option (to be used by Configuration class)
        /// </summary>
        public static OptionValueMap GetCachedWebOption(string group, string key, int role)
        {
            if (group != null && key != null)
            {
                Dictionary<string, Dictionary<int, OptionValueMap>> webOptions = RetrieveCachedWebOptions(false);
                if (webOptions != null)
                {
                    string comboKey = MakeWebOptionComboKey(group, key);
                    if (webOptions.ContainsKey(comboKey))
                    {
                        Dictionary<int, OptionValueMap> values = webOptions[comboKey];
                        if (values.ContainsKey(role))
                            return values[role];
                        if (values.ContainsKey(0))  // If cannot find an exact match, try catch-all settings
                            return values[0];
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Return value of one web option from DB (to be used by Admin UI)
        /// </summary>
        public static WebOption GetWebOption(string group, string key, int role)
        {
            if (group != null && key != null)
            {
                return DataAccess.ConfigurationDao.GetWebOption(group, key, role);
            }

            return null;
        }

        /// <summary>
        /// Return all defined web options from DB (to be used by admin UI)
        /// </summary>
        public static IList<WebOption> GetWebOptions()
        {
            return GetWebOptions(null, 1, Int32.MaxValue);
        }

        /// <summary>
        /// Return all defined web options from DB (to be used by admin UI)
        /// </summary>
        public static IList<WebOption> GetWebOptions(string group)
        {
            return GetWebOptions(group, 1, Int32.MaxValue);
        }

        /// <summary>
        /// Return web options from DB in paged datatable (to be used by admin UI)
        /// </summary>
        public static IList<WebOption> GetWebOptions(int pageNumber, int pageSize)
        {
            return GetWebOptions(null, pageNumber, pageSize);
        }

        /// <summary>
        /// Return web options from DB in paged datatable (to be used by admin UI)
        /// </summary>
        public static IList<WebOption> GetWebOptions(string group, int pageNumber, int pageSize)
        {
            return DataAccess.ConfigurationDao.GetWebOptions(group, pageNumber, pageSize, false);
        }

        /// <summary>
        /// Return all available web options from DB (to be used by admin UI)
        /// </summary>
        public static IList<WebOptionTemplate> GetAvailableWebOptions()
        {
            return GetAvailableWebOptions(null);
        }

        /// <summary>
        /// Return all available web options from DB (to be used by admin UI)
        /// </summary>
        public static IList<WebOptionTemplate> GetAvailableWebOptions(string group)
        {
            return DataAccess.ConfigurationDao.GetAvailableWebOptions(group);
        }

        /// <summary>
        /// Return all available web option groups from DB (to be used by admin UI)
        /// </summary>
        public static IList<string> GetAvailableWebOptionGroups()
        {
            return DataAccess.ConfigurationDao.GetAvailableWebOptionGroups();
        }

        /// <summary>
        /// Set value for one web option in DB (to be used by admin UI)
        /// </summary>
        public static void SetWebOption(string group, string key, int role, string value)
        {
            SetWebOption(group, key, role, value, -1);
        }

        public static void SetWebOption(string group, string key, int role, string value, int origRole)
        {
            if (group != null && key != null)
            {
                DataAccess.ConfigurationDao.SetWebOption(group, key, role, value, origRole);
            }
        }

        /// <summary>
        /// Delete one web option in DB (to be used by admin UI)
        /// </summary>
        public static void DeleteWebOptions(string group, string key)
        {
            if (group != null && key != null)
            {
                DataAccess.ConfigurationDao.DeleteWebOptions(group, key);
            }
        }

        /// <summary>
        /// Delete one web option in DB (to be used by admin UI)
        /// </summary>
        public static void DeleteWebOption(string group, string key, int role)
        {
            if (group != null && key != null)
            {
                DataAccess.ConfigurationDao.DeleteWebOption(group, key, role);
            }
        }

        private static string sLock = "Lock";
 
        // Image URLs
        /// <summary>
        /// Return the achievement image URL
        /// </summary>
        public static string GetAchievementImageURL (string imagePath, string defaultSize)
        {
            if (imagePath.Length.Equals (0))
            {
                return ImageServer + "/defaultworld_" + defaultSize + ".jpg";
            }
            else
            {
                if ((defaultSize == null) || (defaultSize == ""))
                {
                    defaultSize = "me";
                }
                return ImageServer + "/" + System.IO.Path.GetDirectoryName (imagePath).Replace ("\\", "/") + "/" + 
                    System.IO.Path.GetFileNameWithoutExtension (imagePath) + "_" + defaultSize + 
                    System.IO.Path.GetExtension (imagePath);
            }
        }

        /// <summary>
        /// GetItemImageURL
        /// </summary>
        /// <param name="imagePath">The relative path to the image on the NAS. Example - 4/22/sports.gif</param>
        /// <param name="defaultSize">The default size of the image if the user has not uploaded a photo. Example - 'sm', 'me', 'la', 'xl'</param>
        /// <returns></returns>
        public static string GetItemImageURL (string imagePath, string defaultSize, string type)
        {
            if (imagePath.Length.Equals (0))
            {
                if (type == "Sound")
                {
                    return TextureServer + "/KanevaIconAudio.gif";
                }
                else
                {
                    return TextureServer + "/KanevaIconWOKItem_" + defaultSize + ".jpg";
                }
            }
            else
            {
                return TextureServer + "/" + imagePath;
            }
        }

        #endregion

        #region RabbitMQ

        public static string RabbitMQHost
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["RabbitMQHost"];
                if (string.IsNullOrEmpty(ret))
                    ret = "";
                return ret;
            }
        }

        public static int RabbitMQPort
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["RabbitMQPort"];
                if (ret == null)
                    ret = "5672";
                return Int32.Parse(ret);
            }
        }

        public static ushort RabbitMQHeartbeat
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["RabbitMQHeartbeat"];
                if (ret == null)
                    ret = "50";
                return ushort.Parse(ret);
            }
        }

        public static string RabbitMQUsername
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["RabbitMQUsername"];
                if (string.IsNullOrEmpty(ret))
                    ret = "";
                return ret;
            }
        }

        public static string RabbitMQPassword
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["RabbitMQPassword"];
                if (string.IsNullOrEmpty(ret))
                    ret = "";
                return ret;
            }
        }

        public static string RabbitMQVHost
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["RabbitMQVHost"];
                if (string.IsNullOrEmpty(ret))
                    ret = "\\";
                return ret;
            }
        }

        public static bool RabbitMQAutoRecoveryEnabled
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["RabbitMQAutoRecoveryEnabled"];
                if (string.IsNullOrEmpty(ret))
                    ret = "true";

                return Boolean.Parse(ret);
            }
        }

        public static TimeSpan RabbitMQReconnectInterval
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["RabbitMQReconnectInterval"];
                if (string.IsNullOrEmpty(ret))
                    ret = "10";

                return TimeSpan.FromSeconds(Double.Parse(ret));
            }
        }

        #endregion

        #region JIRA

        public static string JIRAUrl
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["JIRAUrl"];
                if (string.IsNullOrEmpty(ret))
                    ret = "https://kaneva.atlassian.net";
                return ret;
            }
        }
        public static string JIRAUsername
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["JIRAUsername"];
                if (string.IsNullOrEmpty(ret))
                    ret = "Jenkins";
                return ret;
            }
        }

        public static string JIRAPassword
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["JIRAPassword"];
                if (string.IsNullOrEmpty(ret))
                    ret = "13R1e$c~:N";
                return ret;
            }
        }

        public static bool JIRAConnectionEnabled
        {
            get
            {
                string ret = System.Configuration.ConfigurationManager.AppSettings["JIRAConnectionEnabled"];
                if (string.IsNullOrEmpty(ret))
                    ret = "false";
                return bool.Parse(ret);
            }
        }

        #endregion

        #region Leaderboards

        public static int WorldLeaderboardRavePointValue
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["WorldLeaderboardRavePointValue"] != null)
                {
                    return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["WorldLeaderboardRavePointValue"]);
                }
                else
                {
                    return 100;
                }
            }
        }

        public static int WorldLeaderboardVisitPointValue
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["WorldLeaderboardVisitPointValue"] != null)
                {
                    return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["WorldLeaderboardVisitPointValue"]);
                }
                else
                {
                    return 10;
                }
            }
        }

        #endregion
    }

    public class OptionValueMap
    {
        private string valueString = "";
        private Dictionary<string, string> values = new Dictionary<string, string>();
        private bool bDirty = true;

        private static char[] mainDelimiters = { ',' };
        private static char[] equalDelimiters = { '=' };

        public OptionValueMap(string valStr)
        {
            valueString = valStr;
            bDirty = true;
        }

        public string String
        {
            get { return valueString; }
            set {
                if( valueString.CompareTo(value)!=0 )
                {
                    valueString = value;
                    bDirty = true;
                }
            }
        }

        public string this[int key]
        {
            get {
                string sKey = key.ToString();
                return this[sKey];
            }
        }

        public string this[string key]
        {
            get {
                if (bDirty)
                    Parse();

                if (values.ContainsKey(key))
                    return values[key];
                else
                    return values[""];
            }
        }

        public string GetValueStr(int key, string defaultValue)
        {
            string res = this[key];
            if (res != null)
                return res;

            return defaultValue;
        }

        public string GetValueStr(string key, string defaultValue)
        {
            string res = this[key];
            if (res != null)
                return res;

            return defaultValue;
        }

        public int GetValueInt(int key, int defaultValue)
        {
            string sVal = GetValueStr(key, null);
            if( sVal!=null )
            {
                try
                {
                    return Convert.ToInt32(sVal);
                }
                catch (System.Exception)
                {
                }
            }

            return defaultValue;
        }

        public int GetValueInt(string key, int defaultValue)
        {
            string sVal = GetValueStr(key, null);
            if (sVal != null)
            {
                try
                {
                    return Convert.ToInt32(sVal);
                }
                catch (System.Exception)
                {
                }
            }

            return defaultValue;
        }

        private void Parse()
        {
            values.Clear();

            if (valueString != null && valueString.Length > 0)
            {
                string[] pairs = valueString.Split(mainDelimiters);
                foreach (string pairStr in pairs)
                {
                    string[] keyValuePair = pairStr.Trim().Split(equalDelimiters);
                    string key = null, strValue = null;
                    if (keyValuePair.Length == 2)
                    {
                        key = keyValuePair[0].Trim();
                        strValue = keyValuePair[1].Trim();
                    }
                    else if (keyValuePair.Length == 1)
                    {
                        key = "";
                        strValue = keyValuePair[0].Trim();
                    }

                    if (key != null && strValue != null)
                    {
                        values[key] = strValue;
                    }
                }
            }

            bDirty = false;
        }
    }
}
