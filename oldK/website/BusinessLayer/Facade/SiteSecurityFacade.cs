///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;
using System.Data;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class SiteSecurityFacade
    {
        private ISiteSecurityDao siteSecurityDao = DataAccess.SiteSecurityDao;

        //added here a centralized place for maintaining this since there is no access level object class
        public int GetDefaultAccessLevel()
        {
            return 1;
        }

        /// <summary>
        /// Get all access levels for privileges.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetAllAccessLevels()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteSecurityDao.GetAllAccessLevels();
        }

        /// <summary>
        /// Get all company roles as list
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<SiteRole> GetCompanyRolesList(int companyId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteSecurityDao.GetCompanyRolesList(companyId);
        }

        /// <summary>
        /// Get all company roles 
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetCompanyRoles(int companyId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteSecurityDao.GetCompanyRoles(companyId);
        }

        /// <summary>
        /// Get all company roles 
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public SiteRole GetCompanyRole(int companyId, int roleId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteSecurityDao.GetCompanyRole(companyId,roleId);
        }

        /// <summary>
        /// Get all privleges for the role of the company provided 
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetRolePrivileges(int siteRoleId, int companyId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteSecurityDao.GetRolePrivileges(siteRoleId, companyId);
        }


        /// <summary>
        /// Get all administrative privileges list
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<SitePrivilege> GetAdminSitePrivileges()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteSecurityDao.GetAdminSitePrivileges();
        }


        /// <summary>
        /// Get all user privileges list
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<SitePrivilege> GetAvailableUserPrivileges()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteSecurityDao.GetAvailableUserPrivileges();
        }

        /// <summary>
        /// Get all privileges
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<SitePrivilege> GetAllPrivileges()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteSecurityDao.GetAllPrivilegesList("");
        }

        /// <summary>
        /// Remove a user role
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int RemoveUserRole(int siteRoleId, int companyId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteSecurityDao.DeleteRole(siteRoleId, companyId);
        }

        /// <summary>
        /// Add a user role
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddUserRole(SiteRole role)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteSecurityDao.AddNewRole(role);
        }

        /// <summary>
        /// Update a user role
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int UpdateUserRole(SiteRole role)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteSecurityDao.UpdateRole(role);
        }

        /// <summary>
        /// Add a privilege to role
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddNewPrivilege2Role(int siteRoleId, int companyId, int privilegeId, int accessLevelId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteSecurityDao.AddNewPrivilege2Role(siteRoleId, companyId, privilegeId, accessLevelId);
        }

        /// <summary>
        /// Remove a user role
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteAllPrivilege2Role(int siteRoleId, int companyId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return siteSecurityDao.DeleteAllPrivilege2Role(siteRoleId,companyId);
        }     
  

    }
}
