///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;
using System.Data;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;

using log4net;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class InterestsFacade
    {
        private IInterestsDao interestsDao = DataAccess.InterestsDao;

        /// <summary>
        /// Get a list of the available interest categories.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<InterestCategory> GetInterestCategories()
        {
            return interestsDao.GetInterestCategories();
        }

        /// <summary>
        /// Get a list of the interest common between two users.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetCommonInterests(int userId, int userId2)
        {
            return interestsDao.GetCommonInterests(userId, userId2);
        }

        /// <summary>
        /// Get a list of a user's interest.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetInterests(int userId)
        {
            return interestsDao.GetInterests(userId);
        }

        /// <summary>
        /// Get a list of a user's interest.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetUserInterests(int userId)
        {
            return GetUserInterests(userId, "", "");
        }

        /// <summary>
        /// Get a list of a user's interest.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetUserInterests(int userId, string filter, string orderBy)
        {
            return interestsDao.GetUserInterests(userId, filter, orderBy);
        }

        /// <summary>
        /// Get a list of a interest by category.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetInterestByCategory(int userId, int icId)
        {
            return interestsDao.GetInterestByCategory( userId, icId);
        }

        /// <summary>
        /// Get a list of common interest between two users by category.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetCommonInterestsByCategory(int userId, int userId2, int icId)
        {
            return interestsDao.GetCommonInterestsByCategory(userId, userId2, icId);
        }

        /// <summary>
        /// Get a list of the user's interest by category.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetUserInterestsByCategory(int userId, int icId)
        {
            return interestsDao.GetUserInterestsByCategory(userId, icId);
        }

        /// <summary>
        /// Get a list of schools shared by two users.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetCommonSchools(int userId, int userId2)
        {
            return interestsDao.GetCommonSchools(userId, userId2);
        }

        /// <summary>
        /// Get a list of the user's schools.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetUserSchools(int userId)
        {
            return GetUserSchools(userId,"","");
        }

        /// <summary>
        /// checks if the provide school already exists.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int SchoolExists(string name)
        {
            return interestsDao.SchoolExists(name);
        }

        /// <summary>
        /// checks if the provide school already exists.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddSchool (string name, string city, string state)
        {
            return interestsDao.AddSchool(name, city, state);
        }

        /// <summary>
        /// Get a list of the user's schools with ordering and filtering.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetUserSchools(int userId, string filter, string orderBy)
        {
            return interestsDao.GetUserSchools(userId, filter, orderBy);
        }

        /// <summary>
        /// remove the interest from all users.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int RemoveInterestFromAllUsers(int interestId)
        {
            return interestsDao.RemoveInterestFromAllUsers(interestId);
        }

        /// <summary>
        /// remove the interest from the user.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int RemoveInterestFromUser(int userId, int interestId)
        {
            int rc = interestsDao.RemoveInterestFromUser(userId, interestId);

            if (rc == 1)
            {
                // update the users searchable interests 
                interestsDao.UpdateUsersSearchableInterests(userId);

                // increment interest count
                interestsDao.RemoveFromInterestCount(interestId);
            }

            return rc;
        }

        /// <summary>
        /// assign interest to users.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AssignInterestToUser(int userId, int interestId, int icId)
        {
            int rc = interestsDao.AssignInterestToUser(userId, interestId, icId);

            if (rc == 0)
            {
                // update the users searchable interests 
                interestsDao.UpdateUsersSearchableInterests(userId);

                // increment interest count
                interestsDao.AddInterestsToInterestCount(interestId);
            }

            return rc;

        }

        /// <summary>
        /// assign interest to users.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AssociateSchoolToUser(int userId, int schoolId)
        {
            int rc = interestsDao.AssociateSchoolToUser(userId, schoolId);

            if (rc == 0)
            {
                // update the users searchable interests 
                interestsDao.UpdateUsersSearchableInterests(userId);

                // increment interest count
                interestsDao.AddToSchoolCount(schoolId);
            }

            return rc;

        }

        /// <summary>
        /// add new iinterest
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddInterest(int icId, string interest)
        {
            return interestsDao.AddInterest(icId, interest);
        }

        /// <summary>
        /// add new iinterest
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InterestExists(int icId, string interest)
        {
            return interestsDao.InterestExists(icId, interest);
        }


        /// <summary>
        /// assign interest to users.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int RemoveSchoolFromUser(int userId, int schoolId)
        {
            int rc = interestsDao.RemoveSchoolFromUser(userId, schoolId);

            if (rc == 1)
            {
                // increment interest count
                interestsDao.RemoveFromSchoolCount(schoolId);
            }

            return rc;

        }

    }
}
