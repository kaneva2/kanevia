///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;
using System.Data;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class ViolationsFacade
    {
        private IViolationsDao violationsDao = DataAccess.ViolationsDao;

        /// <summary>
        /// Gets the available violation reports
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<ViolationReport> GetAllViolationReports(string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return violationsDao.GetAllViolationReports(filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Gets the available violation reports with user name and reporter name joined in
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<ViolationReport> GetAllViolationReportsJoined(string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return violationsDao.GetAllViolationReportsJoined(filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Gets the available violation reports grouped by item id ordered by Date descing
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<ViolationReport> GetAllViolationReportsGrouped(string filter, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return violationsDao.GetAllViolationReportsGrouped(filter, pageNumber, pageSize);
        }

        /// <summary>
        /// Gets the available violation reports grouped by item id ordered by Date descing
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetViolationCounts(uint assetId, uint wokItemId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return violationsDao.GetViolationCounts(assetId, wokItemId);
        }


        /// <summary>
        /// Inserts a new violation report - mostly used for interenal processing. Should use 
        /// ReportAViolation to report violation in most cases
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int NewViolationReport(ViolationReport report)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return violationsDao.AddNewViolationReport(report);
        }

        /// <summary>
        /// Gets the available violation action types
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<ViolationActionType> GetAllViolationActionTypes()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return violationsDao.GetAllViolationActionTypes();
        }

        /// <summary>
        /// Gets the available violation actions
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<ViolationAction> GetAllViolationActions()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return violationsDao.GetAllViolationActions();
        }

        /// <summary>
        /// Gets the available violation types
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<ViolationType> GetAllViolationTypes()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return violationsDao.GetAllViolationTypes();
        }

        /// <summary>
        /// Gets a violation by violation type id
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public ViolationAction GetViolationAction(int violationTypeId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return violationsDao.GetViolationAction(violationTypeId);
        }

        /// <summary>
        /// Gets a violation by violation type id
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateViolationReport(ViolationReport report)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return violationsDao.UpdateViolationReport(report);
        }

        /// <summary>
        /// Gets a violation by violation type id
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int ProcessViolationImmediately(ViolationReport report)
        {
            int success = 0;
            //process based on if wok item or asset
            if (report.AssetID > 0)
            {
                success = violationsDao.ProcessAssetViolation(report);
            }
            else if (report.WokItemId > 0)
            {
                success = violationsDao.ProcessWokItemViolation(report);
            }
            return success;
        }

        /// <summary>
        /// Reports a violation checking to see if the user has already reported the item
        /// this is the function that should be used normally for entering new violation reports
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int ReportAViolation(ViolationReport newReport)
        {
            //result of zero means failure to report
            int result = 0;
            //see if this user had already report this item for this same reason
            ViolationReport report = violationsDao.GetViolationReportByTypeANDReporter(newReport);
            ViolationReport report2 = violationsDao.GetViolationReportByViolationType(newReport);

            if ((report == null) || (report.OwnerId <= 0) || report2.ViolationActionTypeId.Equals((uint)ViolationActionType.eVIOLATION_ACTIONS.SET_AS_GOOD))
            {
                result = violationsDao.AddNewViolationReport(newReport);
            }
            else
            {
                result = -1; 
            }

            return result;
        }


    }
}
