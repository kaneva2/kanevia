///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;
using System.Text.RegularExpressions;
using System.Linq;
using System.Net;
using log4net;
using Newtonsoft.Json;

namespace Kaneva.BusinessLayer.Facade
{
	/// <summary>
    /// Summary description for Common.
	/// </summary>
    public class Common
	{
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private IGameDao gameDao = DataAccess.GameDao;

        public Common ()
		{
		}

		/// <summary>
		/// IsPottyMouth
		/// </summary>
		public static bool IsPottyMouth (string strText)
		{
			try
			{
				strText = strText.ToUpper ();

				if 
					(
                    (strText.IndexOf("SHIT") > -1) || (strText.IndexOf("FUCK") > -1)
                    || (strText.IndexOf("PUSSY") > -1) || strText.Equals("STFU")
                    || strText.Equals("COCK") || strText.Equals("COCKSUCKER")
                    || strText.Equals("TITS") || strText.Equals("WHORE") || strText.Equals("PENIS")
                    || strText.Equals("FAG") || strText.Equals("HOMO") || strText.Equals("LESBO")
                    || strText.Equals("QUEER") || strText.Equals("NIGGER") || strText.Equals("NIGGA")
                    || strText.Equals("WTF") || strText.Equals("ANAL")
                    || strText.Equals("BOOB") || strText.Equals("BOOBS")
                    || strText.Equals("ASSHOLE")
                    || strText.Equals("CUNT") || strText.Equals("VAGINA")
                    || strText.Equals("PRICK")
                    )
				{
					return true;
				}

//				#if KSERVICE
//				{
//					return false;
//				}
//				#else
//				{
//					// Else look up the database table
//					DataTable dtPottyMouth = WebCache.GetPottyWords ();
//					return (dtPottyMouth.Select ("bad_word = '" + CleanText (strText) + "'").Length > 0);
//				}
//				#endif
			}
			catch (Exception) {}
			return false;
		}

		/// <summary>
		/// Clean text for db insertion
		/// </summary>
		/// <param name="strText"></param>
		/// <returns></returns>
		public static string CleanText (string strText)
		{
			strText = strText.Replace ("'", "''");
			strText = strText.Replace ("\\", "\\\\");
			return strText.Replace ("\"", "\"\"");
		}

		/// <summary>
		/// Clean text for db insertion
		/// </summary>
		/// <param name="strText"></param>
		/// <returns></returns>
		public static string CleanHtmlEditorText (string strText)
		{
			// This is the only cleaning we need done with the HTML editor
			// It already does ".
			return strText.Replace ("'", "''");
		}

		/// <summary>
		/// Clean text for javascript
		/// </summary>
		/// <param name="strText"></param>
		/// <returns></returns>
		public static string CleanJavascript (string strText)
		{
			return strText.Replace ("'", "\\'");
		}

		/// <summary>
		/// Clean text for javascript
		/// </summary>
		/// <param name="strText"></param>
		/// <returns></returns>
		public static string CleanJavascriptFull (string strText)
		{
			return (strText.Replace ("'", "\\'")).Replace("\r\n","\\r\\n");
		}

        /// <summary>
        /// Strips excess formatting from a JSON-encoded string.
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static string StripExcessJSONFormatting(string json)
        {
            // Remove buffer spaces
            json = json.Trim();

            // Remove excess whitespace
            json = Regex.Replace(json, "[\r\n\t]+", "");
            json = Regex.Replace(json, "[ ]*[{}\"][ ]*", r => r.Value.Trim());
            json = Regex.Replace(json, "\":[ ]+", r => r.Value.TrimEnd());

            return json;
        }

		/// <summary>
		/// This method shortens a string to a certain length and
		/// adds ... when the string is longer than a specified
		/// maximum length.
		/// </summary>
		/// <param name="text"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static string TruncateWithEllipsis(string text, int length) 
		{
			if (text.Length > length)
				text = text.Substring(0,length) + "...";
			return text;
		}

		/// <summary>
		/// This method chops off the end of a string when the
		/// string is longer than a maximum length.
		/// </summary>
		/// <param name="text"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static string Truncate(string text, int length) 
		{
			if (text.Length > length)
				text = text.Substring(0,length);
			return text;
		}

		/// <summary>
		/// Represents the path of the current application.
		/// </summary>
		public static string AppPath 
		{
			get 
			{
				if (HttpContext.Current.Request.ApplicationPath == "/")
					return String.Empty;
				return HttpContext.Current.Request.ApplicationPath;
			}
		}

		public static string ResolveAbsoluteUrl (string Url) 
		{
			if (HttpContext.Current.Request.IsSecureConnection)
				return "https://" + Url;
			else
				return "http://" + Url;        
		}

		/// <summary>
		/// Removes everything from a request except for the page name.
		/// </summary>
		/// <param name="requestPath"></param>
		/// <returns></returns>
		public static string GetPageName(string requestPath)
		{
			// Remove query string
			if ( requestPath.IndexOf( '?' ) != -1 )
				requestPath = requestPath.Substring( 0, requestPath.IndexOf( '?' ) );       

			// Remove base path
			return requestPath.Remove( 0, requestPath.LastIndexOf( "/" ) );
		}

        public static long ConvertToUnixTimestamp(DateTime dateTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return (long)(dateTime.ToUniversalTime() - epoch).TotalSeconds;
        }

		/// <summary>
		/// Format the date time into a readable formate
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public static string FormatDateTime (DateTime dtDate)
		{
			System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo ("en-US", true);
			return dtDate.ToString ("MMM-d-yy hh:mm tt", culture);
		}

		/// <summary>
		/// Format the date into a readable formate
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public static string FormatDate (DateTime dtDate)
		{
			System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo ("en-US", true);
			return dtDate.ToString ("MMM-d-yy", culture);
		}

		/// <summary>
		/// Format the date into a readable formate
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public static string FormatDateNumbersOnly (DateTime dtDate)
		{
			System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo ("en-US", true);
			return dtDate.ToString ("MM-dd-yy", culture);
		}

		/// <summary>
		/// Format the date into a time span
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public static string FormatDateTimeSpan(Object dtDate)
		{
			return FormatDateTimeSpan(dtDate, DateTime.Now);	
		}
		public static string FormatDateTimeSpan(Object dtDate, Object dtDateNow)
		{
			string timespan = string.Empty;

			if (dtDate.Equals (DBNull.Value))
				return "";

			TimeSpan span = Convert.ToDateTime(dtDateNow) - Convert.ToDateTime(dtDate);
			
			if (span.Days > 29)
				timespan = (span.Days/30).ToString() + " month" + ((span.Days/30) > 1 ? "s" : "") + " ago";
			else if (span.Days > 0)
				timespan = span.Days.ToString() + " day" + (span.Days > 1 ? "s" : "") + " ago";
			else if (span.Hours > 0)
				timespan = span.Hours.ToString() + " hour" + (span.Hours > 1 ? "s" : "") + " ago";
			else if (span.Minutes > 0)
				timespan = span.Minutes.ToString() + " minute" + (span.Minutes > 1 ? "s" : "") + " ago";
			else if (span.Seconds > 0)
				timespan = span.Seconds.ToString() + " second" + (span.Seconds > 1 ? "s" : "") + " ago";
			else
				timespan = "1 second ago";
			
			return timespan;
		}
		/// <summary>
		/// Format the date into a readable formate
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public static DateTime UnFormatDate (string strDate)
		{
			try
			{
				System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo ("en-US", true);
				return DateTime.Parse (strDate, culture, System.Globalization.DateTimeStyles.None);
			}
			catch (Exception)
			{
				return new DateTime ();
			}
		}

		/// <summary>
		/// Format the date into a readable formate
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public static string FormatImageSize (long imageSize)
		{
			Double newImageSize;

			// Less than 1Kb show b
			if (imageSize < 1024)	// 1K = 1024 b
			{
				return imageSize.ToString ("#,##0.## b");
			}
			// Less than a MB show Kb
			else if (imageSize < 1048600)		// 1048600 b = 1MB = 1024 KB
			{
				newImageSize = (Double) imageSize / 1024;
				return newImageSize.ToString ("#,##0.## Kb");
			}
			// Show in MB
			else // if (imageSize < 1073700000) // 1073700000 b = 1 Gb, 1099500000000 b = 1 TB 
			{
				newImageSize = (Double) imageSize / 1048600;
				return newImageSize.ToString ("#,##0.## Mb");
			}
		}

		/// <summary>
		/// Format the date into a readable formate
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		public static string FormatImageSize (double imageSize)
		{
			Double newImageSize;

			if (imageSize < 1000)
			{
				return imageSize.ToString ("#,##0.## b");
			}
			else if (imageSize < 1000000)
			{
				newImageSize = (Double) imageSize / 1000;
				return newImageSize.ToString ("#,##0.## Kb");
			}
			else
			{
				newImageSize = (Double) imageSize / 1000000;
				return newImageSize.ToString ("#,##0.## Mb");
			}
		}

		/// <summary>
		/// Format currency
		/// </summary>
		/// <param name="dblCurrency"></param>
		/// <returns></returns>
		public static string FormatCurrency (Object dblCurrency)
		{
			if (dblCurrency == null)
				return ("$0.00");

			return ((Double) dblCurrency).ToString ("$#,##0.00#");
		}

		/// <summary>
		/// Format currency
		/// </summary>
		/// <param name="dblCurrency"></param>
		/// <returns></returns>
		public static string FormatCurrencyForTextBox (Object dblCurrency)
		{
			if (dblCurrency == null)
				return ("0.00");

			// No partial pennies here.
			return ((Double) dblCurrency).ToString ("#0.00");
		}

		/// <summary>
		/// Format KPoints with other options
		/// </summary>
		/// <param name="dblKPoints"></param>
		/// <param name="bIsHTML"></param>
		/// <param name="bShowFreeText"></param>
		/// <returns></returns>
		public static string FormatKPoints (Double dblKPoints, bool bIsHTML, bool bShowFreeText)
		{
			string formatString = "#,##0 credits";

			if (dblKPoints.Equals (null))
				return ("0");

			if (bShowFreeText && dblKPoints.Equals (0.0))
			{
				return "FREE - 0 kp";
			}

			if (dblKPoints < 0)
			{
				return (Math.Abs (dblKPoints)).ToString ("(" + formatString + ")");
			}
			else
			{
				return (dblKPoints).ToString (formatString);
			}
		}

		/// <summary>
		/// Format KPoints
		/// </summary>
		/// <param name="dblKPoints"></param>
		/// <returns></returns>
		public static string FormatKPoints (Double dblKPoints)
		{
			return FormatKPoints (dblKPoints, true, true);
		}

		/// <summary>
		/// Is the number numeric?
		/// </summary>
		/// <param name="candidate"></param>
		/// <returns></returns>
		public static bool IsNumeric (string candidate) 
		{ 

			char [] ca = candidate.ToCharArray(); 
			for (int i = 0; i < ca.Length;i++) 
			{ 
				if (ca[i] > 57 || ca[i] < 48) 
					return false; 
			} 
			return true; 
		} 

		/// <summary>
		/// Format seconds as time hh:mm:ss
		/// </summary>
		/// <param name="runTime"></param>
		/// <returns></returns>
		public static string FormatSecondsAsTime(int runTime)
		{
			return String.Format( "{0:D}:{1:D2}:{2:D2}", runTime / 3600, runTime / 60 % 60, runTime % 60 );
		}
		/// <summary>
		/// Get the results text
		/// </summary>
		/// <returns></returns>
		public static string GetResultsText (int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
		{
			return GetResultsText (TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
		}

		/// <summary>
		/// Get the results text
		/// </summary>
		/// <returns></returns>
		public static string GetResultsText (int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
		{
			
			return GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts,
			"Results %START%-%END% of %TOTAL%");
		}

		/// <summary>
		/// Get the results text
		/// </summary>
		/// <returns></returns>
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="TotalCount"></param>
		/// <param name="pageNumber"></param>
		/// <param name="resultsPerPage"></param>
		/// <param name="currentPageCount"></param>
		/// <param name="bGetCounts"></param>
		/// <param name="template">%START%, %END%, %TOTAL% will be replaced by starting number, ending number and total
		/// number of items showing</param>
		/// <returns></returns>
		public static string GetResultsText (int TotalCount, int pageNumber, int resultsPerPage, 
			int currentPageCount, bool bGetCounts, string template)
		{
			if (TotalCount > 0)
			{
				if (bGetCounts)
				{
					int start = ((pageNumber - 1) * resultsPerPage)+1;
					int end = (pageNumber - 1) * resultsPerPage+currentPageCount;
					return template.Replace("%START%", start.ToString()).Replace(
						"%END%", end.ToString()).Replace("%TOTAL%", TotalCount.ToString());
				}
				else
				{
					return "";
				}
			}
			else
			{
				return "<B>No Results Found</B>";
			}
		}

		/// <summary>
		/// Encrypt
		/// </summary>
		/// <param name="plainText"></param>
		/// <returns></returns>
		public static string Encrypt (string plainText)
		{
			TripleDESCryptoServiceProvider tds = new TripleDESCryptoServiceProvider ();
			byte[] plainByte = ASCIIEncoding.ASCII.GetBytes (plainText);

			string key = "jb7431%o$#=@tp+&";
			PasswordDeriveBytes pdbkey = new PasswordDeriveBytes (key, ASCIIEncoding.ASCII.GetBytes (String.Empty));
			byte[] keyByte = pdbkey.GetBytes (key.Length);

			// Set private key
			tds.Key = keyByte;
			tds.IV = new byte[] {0xf, 0x6f, 0x13, 0x2e, 0x35, 0xc2, 0xcd, 0xf9};
				
			// Encryptor object
			ICryptoTransform cryptoTransform = tds.CreateEncryptor ();
   
			// Memory stream object
			MemoryStream ms = new MemoryStream ();

			// Crpto stream object
			CryptoStream cs = new CryptoStream (ms, cryptoTransform, CryptoStreamMode.Write);

			// Write encrypted byte to memory stream
			cs.Write (plainByte, 0, plainByte.Length);
			cs.FlushFinalBlock ();

			// Get the encrypted byte length
			byte[] cryptoByte = ms.ToArray ();

			ms.Close ();
			cs.Close ();

			// Convert into base 64 to enable result to be used in Config
			return Convert.ToBase64String (cryptoByte, 0, cryptoByte.GetLength(0));
		}

		/// <summary>
		/// Decrypt
		/// </summary>
		/// <param name="encryptedText"></param>
		/// <returns></returns>
		public static string Decrypt (string encryptedText)
		{
			TripleDESCryptoServiceProvider tds = new TripleDESCryptoServiceProvider ();
			string result = "";

			string key = "jb7431%o$#=@tp+&";
			PasswordDeriveBytes pdbkey = new PasswordDeriveBytes (key, ASCIIEncoding.ASCII.GetBytes (String.Empty));
			byte[] keyByte = pdbkey.GetBytes (key.Length);

			// Decrypt it
			// Convert from base 64 string to bytes
			byte[] cryptoByte = Convert.FromBase64String (encryptedText);

			// Set private key
			tds.Key = keyByte;
			tds.IV = new byte[] {0xf, 0x6f, 0x13, 0x2e, 0x35, 0xc2, 0xcd, 0xf9};			

			// Decryptor object
			ICryptoTransform cryptoTransform = tds.CreateDecryptor ();
			MemoryStream msd = null;
			CryptoStream csd = null;
			StreamReader sr = null;

			try
			{
				// Memory stream object
				msd = new MemoryStream (cryptoByte, 0, cryptoByte.Length);

				// Crypto stream object
				csd = new CryptoStream (msd, cryptoTransform, CryptoStreamMode.Read);

				// Get the result from the Crypto stream
				sr = new StreamReader (csd);
				result = sr.ReadToEnd ();
			}
			catch (Exception)
			{
				//m_logger.Error ("Decryption Error", exc);
			}
			finally
			{
				cryptoTransform.Dispose ();

				if (msd != null)
				{
					msd.Close ();
				}
				if (csd != null)
				{
					csd.Close ();
				}
				if (sr != null)
				{
					sr.Close ();
				}
			}

			return result;
		}
        public static string AESEncrypt(string content)
        {
            byte[] keyBytes = { 0xEE, 0x98, 0x6F, 0x11, 0x95, 0xB0, 0xB9, 0xFF, 
                                0x43, 0x2F, 0xFB, 0xC0, 0xF0, 0xBF, 0x39, 0x3A };
            return AESEncrypt(content, keyBytes, 128 );
        }

        public static string AESSign(string plainText, string signature)
        {
            byte[] hash = derivePasswordHash(signature);
            return AESEncrypt(plainText, hash, hash.Length * 8);
        }

        private static byte[] derivePasswordHash(string signature)
        {
            int iterationcount = 10000;  // todo: move to config. On the assumption that the wokwebsession token
            // is somewhat volatile, we start with a relatively low iteration count.
            // Generally speaking, less volatility means a higher iteration count.
            Rfc2898DeriveBytes deriver
                = new Rfc2898DeriveBytes(signature
                                            , new byte[] {  0x0C, 0xCA, 0x54, 0x05, 0xB2, 0xAE, 0xDE, 0xE6, 
                                                            0xC8, 0x68, 0x5F, 0x4D, 0x40, 0xFE, 0x43, 0x77 }
                                            , iterationcount);// we can use a low iteration count as 

            byte[] hash = deriver.GetBytes(16); // why 20?
            return hash;
        }

        /// <summary>
        /// Encrypt a string using the AES encryption algorithm.
        /// </summary>
        public static string AESEncrypt(string plainText, byte[] keyBytes, int keySizeBits )
        {
            string encryptedString = string.Empty;

            byte[] ivBytes = { 0x0C, 0xCA, 0x54, 0x05, 0xB2, 0xAE, 0xDE, 0xE6, 
            0xC8, 0x68, 0x5F, 0x4D, 0x40, 0xFE, 0x43, 0x77 };

            // AES provider object
            using (RijndaelManaged aes = new RijndaelManaged())
            {
                // Setup AES
                aes.KeySize = 128;
                aes.Key = keyBytes;
                aes.IV = ivBytes;
                aes.Mode = CipherMode.CFB;
                aes.FeedbackSize = 8;
                aes.Padding = PaddingMode.None;

                // Encryptor object
                using (ICryptoTransform cryptoTransform = aes.CreateEncryptor(aes.Key, aes.IV))
                {
                    // Memory stream object
                    using (MemoryStream ms = new MemoryStream())
                    {
                        // Crpto stream object
                        using (CryptoStream cs = new CryptoStream(ms, cryptoTransform, CryptoStreamMode.Write))
                        {
                            // Stream writer object
                            using (StreamWriter sw = new StreamWriter(cs))
                            {
                                sw.Write(plainText);
                            }

                            // Get the encrypted bytes and transform with salt
                            byte[] cryptoByte = ms.ToArray();
                            string cryptoBytestr = BitConverter.ToString(cryptoByte);
                            encryptedString = cryptoBytestr.Replace("-", "");
                        }
                    }
                }
            }

            return encryptedString;
        }

        public static string AESUnsign(string hexEncryptedText /* s/b base64 */, string signature)
        {
            byte[] hash = derivePasswordHash(signature);
            return AESDecrypt(hexEncryptedText, hash, hash.Length * 8);
        }

        public static string AESDecrypt( String hexEncryptedText)  {
            byte[] keyBytes = { 0xEE, 0x98, 0x6F, 0x11, 0x95, 0xB0, 0xB9, 0xFF, 
                                0x43, 0x2F, 0xFB, 0xC0, 0xF0, 0xBF, 0x39, 0x3A };
            return AESDecrypt(hexEncryptedText, keyBytes, 128);
        }

        /// <summary>
        /// Decrypt a string that was encrypted using the AES encryption algorithm.
        /// </summary>
        public static string AESDecrypt(string hexEncryptedText, byte[] keyBytes, int keySizeBits )
        {
            string plainText = string.Empty;

            byte[] ivBytes = {  0x0C, 0xCA, 0x54, 0x05, 0xB2, 0xAE, 0xDE, 0xE6, 
                                0xC8, 0x68, 0x5F, 0x4D, 0x40, 0xFE, 0x43, 0x77 };

            // Split encrypted text into bytes
            int NumberChars = hexEncryptedText.Length / 2;
            byte[] encryptedBytes = new byte[NumberChars];
            using (var sr = new StringReader(hexEncryptedText))
            {
                for (int i = 0; i < NumberChars; i++)
                    encryptedBytes[i] = Convert.ToByte(new string(new char[2] { (char)sr.Read(), (char)sr.Read() }), 16);
            }

            // AES provider object
            using (RijndaelManaged aes = new RijndaelManaged())
            {
                // Setup AES
                aes.KeySize = keySizeBits;
                aes.Key = keyBytes;
                aes.IV = ivBytes;
                aes.Mode = CipherMode.CFB;
                aes.FeedbackSize = 8;
                aes.Padding = PaddingMode.None;

                // Encryptor object
                using (ICryptoTransform cryptoTransform = aes.CreateDecryptor(aes.Key, aes.IV))
                {
                    // Memory stream object
                    using (MemoryStream ms = new MemoryStream(encryptedBytes))
                    {
                        // Crpto stream object
                        using (CryptoStream cs = new CryptoStream(ms, cryptoTransform, CryptoStreamMode.Read))
                        {
                            // Stream reader object
                            using (StreamReader sr = new StreamReader(cs))
                            {
                                plainText = sr.ReadToEnd();
                            }
                        }
                    }
                }
            }

            return plainText;
        }

		/// <summary>
		/// Generate a token
		/// </summary>
		/// <returns></returns>
		public static string GenerateUniqueString (int maxLength)
		{
			try
			{
				// Now, this is real randomization.
                Random random = new Random(GetRandomSeed());

				char [] token = new char [maxLength];

				for (int i = 0; i < token.Length; i++)
				{
					token [i] = TOKEN_CHARACTERS [random.Next (0, TOKEN_CHARACTERS.Length - 1)];
				}

				return (new string (token)).Substring (0, maxLength);
			}
			catch (Exception){}

			return ("Kaneva");
		}

        public static int GetRandomSeed()
        {
            // Generate 4 random bytes for the seed
            byte[] randomBytes = new byte[4];
            new RNGCryptoServiceProvider().GetBytes(randomBytes);

            // Convert 4 bytes into a 32-bit integer value.
            int seed = (randomBytes[0] & 0x7f) << 24 |
                randomBytes[1] << 16 |
                randomBytes[2] << 8 |
                randomBytes[3];

            return seed;
        }

        /// <summary>
        /// Return the personal channel link
        /// </summary>
        /// <param name="channelId"></param>
        /// <returns></returns>
        public static string GetChannelUrl (string nameNoSpaces, bool isChannelUrl)
        {
            if (isChannelUrl)
            {
                return GetBroadcastChannelUrl (nameNoSpaces);
            }
            else
            {
                return GetPersonalChannelUrl (nameNoSpaces);
            }
        }

        /// <summary>
		/// Return the personal channel link
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static string GetPersonalChannelUrl (string nameNoSpaces)
		{
			return "http://" + Configuration.SiteName + "/channel/" + nameNoSpaces + ".people";
		}

		/// <summary>
		/// Return the broadcast channel link
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static string GetBroadcastChannelUrl (string nameNoSpaces)
		{
            return "http://" + Configuration.SiteName + "/channel/" + nameNoSpaces + ".channel";
		}

		/// <summary>
		/// Return the blog link
		/// </summary>
		/// <param name="blogId"></param>
		/// <returns></returns>
		public static string GeBlogUrl (int blogId)
		{
            return "http://" + Configuration.SiteName + "/blog/" + blogId.ToString() + ".blog";
		}

		/// <summary>
		/// Get Asset Details Link
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public static string GetAssetDetailsLink (int assetId)
		{
            return "http://" + Configuration.SiteName + "/asset/" + assetId.ToString() + ".media";
		}

		/// <summary>
		/// Get WOK Item Details Link
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public static string GetWOKDetailsLink (int itemId)
		{
            return "http://" + Configuration.SiteName + "/asset/" + itemId.ToString() + ".vworld";
		}

		/// <summary>
		/// Get UGC Item Details Link
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
        public static string GetUGCItemDetailsURL (int globalId)
		{
            return "http://" + Configuration.ShoppingSiteName + "/ItemDetails.aspx?gId=" + globalId.ToString ();
		}

        /// <summary>
        /// Return the user image URL
        /// </summary>
        ///
        public static string GetProfileImageURL(string imagePath)
        {
            return GetProfileImageURL(imagePath, "sm", "M");
        }

        /// <summary>
        /// GetProfileImageURL
        /// </summary>
        /// <param name="imagePath">The relative path to the image on the NAS. Example - 4/user.gif</param>
        /// <param name="defaultSize">The default size of the image if the user has not uploaded a photo. Example - 'sm', 'me', 'la', 'xl'</param>
        /// <param name="gender">The gender of the user, used to display the correct default picture. Example - 'M', 'F'</param>
        /// <returns></returns>
        public static string GetProfileImageURL(string imagePath, string defaultSize, string gender)
        {
            return GetProfileImageURL(imagePath, defaultSize, gender, true);
        }

        /// <summary>
        /// GetProfileImageURL
        /// </summary>
        /// <param name="imagePath">The relative path to the image on the NAS. Example - 4/user.gif</param>
        /// <param name="defaultSize">The default size of the image if the user has not uploaded a photo. Example - 'sm', 'me', 'la', 'xl'</param>
        /// <param name="gender">The gender of the user, used to display the correct default picture. Example - 'M', 'F'</param>
        /// <param name="useFBProfile">Whether or not the user wishes to use their Facebook profile pic.</param>
        /// <param name="fbUserId">The user's Facebook userId.</param>
        /// <returns></returns>
        public static string GetProfileImageURL(string imagePath, string defaultSize, string gender, bool useFBProfile, ulong fbUserId)
        {
            return GetProfileImageURL(imagePath, defaultSize, gender, useFBProfile, fbUserId, true);
        }

        /// <summary>
        /// GetProfileImageURL
        /// </summary>
        /// <param name="imagePath">The relative path to the image on the NAS. Example - 4/user.gif</param>
        /// <param name="defaultSize">The default size of the image if the user has not uploaded a photo. Example - 'sm', 'me', 'la', 'xl'</param>
        /// <param name="gender">The gender of the user, used to display the correct default picture. Example - 'M', 'F'</param>
        /// <param name="useFBProfile">Whether or not the user wishes to use their Facebook profile pic.</param>
        /// <param name="fbUserId">The user's Facebook userId.</param>
        /// <param name="fullURL">Whether or not to return the fully-qualified URL or just the relative path. Useful for instances
        /// in which we don't need the fully-qualified URL such as saving DB records but still want to select the correct gender default.</param>
        /// <returns></returns>
        public static string GetProfileImageURL(string imagePath, string defaultSize, string gender, bool useFBProfile, ulong fbUserId, bool fullURL)
        {
            if (useFBProfile && fbUserId > 0)
            {
                // Note: fullURL makes no difference in profile imageURL, as these must always be fully qualified.
                // We also need to do a little translation here.  Our "small" profile pics translate nicely to Facebook's 
                // "square" size, at least on myKaneva. This may change, but let's roll with it for now.
                defaultSize = (defaultSize == "sm" ? "sq" : defaultSize);
                return GetFacebookProfileImageUrl(fbUserId, defaultSize);
            }

            return GetProfileImageURL(imagePath, defaultSize, gender, fullURL);
        }

        /// <summary>
        /// GetProfileImageURL
        /// </summary>
        /// <param name="imagePath">The relative path to the image on the NAS. Example - 4/user.gif</param>
        /// <param name="defaultSize">The default size of the image if the user has not uploaded a photo. Example - 'sm', 'me', 'la', 'xl'</param>
        /// <param name="gender">The gender of the user, used to display the correct default picture. Example - 'M', 'F'</param>
        /// <param name="fullURL">Whether or not to return the fully-qualified URL or just the relative path. Useful for instances
        /// in which we don't need the fully-qualified URL such as saving DB records but still want to select the correct gender default.</param>
        /// <returns></returns>
        public static string GetProfileImageURL(string imagePath, string defaultSize, string gender, bool fullURL)
        {
            string returnURL = (fullURL ? Configuration.ImageServer : "");

            if (imagePath.Length.Equals(0))
            {
                if (gender.ToUpper().Equals("M"))
                {
                    returnURL += "/KanevaIconMale_" + defaultSize + ".gif";
                }
                else
                {
                    returnURL += "/KanevaIconFemale_" + defaultSize + ".gif";
                }
            }
            else
            {
                returnURL += "/" + imagePath;
            }

            return returnURL;
        }

        /// <summary>
        /// GetFacebookProfileImageUrl
        /// </summary>
        /// <param name="fbUserId">Facebook User ID.</param>
        /// <param name="defaultSize">Desired size of the profile image.</param>
        /// <returns></returns>
        public static string GetFacebookProfileImageUrl(UInt64 fbUserId, string defaultSize)
        {
            return Configuration.FacebookGraphUrl + fbUserId.ToString() + Configuration.GetFacebookProfileImageQueryStringBySize(defaultSize);
        }

        /// <summary>
        /// GetBroadcastChannelImageURL
        /// </summary>
        /// <param name="imagePath">The relative path to the image on the NAS. Example - 4/howdown.gif</param>
        /// <param name="defaultSize">The default size of the image if the user has not uploaded a photo. Example - 'sm', 'me', 'la'</param>
        /// <returns></returns>
        public static string GetBroadcastChannelImageURL(string imagePath, string defaultSize)
        {
            if (imagePath.Length.Equals(0))
            {
                return Configuration.ImageServer + "/defaultworld_" + defaultSize + ".jpg";
            }
            else
            {
                return Configuration.ImageServer + "/" + imagePath;
            }
        }

        /// <summary>
        /// Downloads a file from the specified url to a byte array.
        /// </summary>
        /// <param name="url">Url to download.</param>
        /// <returns></returns>
        public static byte[] DownloadFile(string url)
        {
            byte[] downloadedData = new byte[0];

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        if (responseStream == null)
                            return downloadedData;

                        int bufferSize = 1024;
                        byte[] buffer = new byte[bufferSize];
                        int bytesRead = 0;

                        using (MemoryStream memStream = new MemoryStream())
                        {
                            while ((bytesRead = responseStream.Read(buffer, 0, bufferSize)) != 0)
                            {
                                memStream.Write(buffer, 0, bytesRead);
                            }

                            downloadedData = memStream.ToArray();
                        }
                    }
                }
            }
            catch (Exception ex) 
            {
                m_logger.Error(string.Format("Common.DownloadFile(): Failed to download item at url {0}. {1}", url, ex.Message));
            }

            return downloadedData;
        }

        /// <summary>
        /// replace separators(commas, spaces, semicolons) with spaces
        /// </summary>
        /// <param name="tags"></param>
        /// <returns></returns>
        public static string GetNormalizedTags(string tags)
        {
            return tags.Replace(",", " ").Replace(";", " ").Replace("  ", " ");
        }

		/// <summary>
		/// Get the version
		/// </summary>
		/// <returns></returns>
		public static string Version ()
		{
			if (version == null)
			{
				// Get the current version
				System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly ();
				System.Reflection.AssemblyName an = a.GetName();
				version = "version " + an.Version.Major + "." + an.Version.Minor + "." + an.Version.Build + "." + an.Version.Revision;
			}

			return version;
		}

        /// <summary>
        /// GetHelpURL
        /// </summary>
        public static string GetHelpURL (string fName, string lName, string email)
        {
            ////string sessEmail = "systemuser@kaneva.com"; // 1mzrfj5mcglw2un4

            if (email.Length > 0)
            {
                // Old way deprecated April 18 2015
                //string sessID = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(email + "1mzrfj5mcglw2un4", "MD5").ToLower();
                //return "http://support.kaneva.com/ics/support/security.asp?deptID=5433&sessEmail=" + sessEmail + "&sessID=" + sessID + "&cFname=" + fName + "&cLname=" + lName + "&cEmail=" + email + "&cSlaName=" + System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile("Member", "MD5").ToLower();

                // Pass through 2.0
                return "http://" + Configuration.SiteName + "/ParaturePassThrough.aspx";
            }
            else
            {
                return "http://support.kaneva.com/ics/support/default.asp?deptID=5433";
            }

            // Parature is getting rid of Passthrough, Quick fix until we implement Passthrough 2.0
           // return "http://" + Configuration.SiteName + "/Suggestions.aspx";
        }

		/// <summary>
		/// Version number
		/// </summary>
		private static string version = null;

		// Possible token strings
		private static char [] TOKEN_CHARACTERS = "abcdefgijkmnopqrstwxyzABCDEFGHJKLMNPQRSTWXYZ23456789".ToCharArray ();

        /// <summary>
        /// AddToHash
        /// </summary>
        public static void AddToHash (char delimiter, Hashtable keywords, string keywordTags)
        {
            char[] splitter = { delimiter }; //= {' '};
            string[] values = keywordTags.Split (splitter);
            string theValue;

            for (int i = 0; i < values.Length; i++)
            {
                theValue = values[i].ToString ().Trim ();

                // You can use a tag once per item, and Tags must be at least 2 characters.
                if (!keywords.ContainsKey (theValue) && theValue.Length > 1)
                {
                    // Make sure it is not a potty word
                    if (!IsPottyMouth (theValue))
                    {
                        keywords.Add (theValue, values[i].ToString ());
                    }
                }
            }
        }

        public static void AddToHash (Hashtable keywords, string keywordTags)
        {
            AddToHash (' ', keywords, keywordTags);
        }

        public static decimal GetCashValue (int amt, string currencyType)
        {
            try
            {
                Currency currency = new Currency();
                ITransactionDao transactionDao = DataAccess.TransactionDao;

                switch (currencyType)
                {
                    case Currency.CurrencyType.KPOINT:
                        currency = transactionDao.GetConversionRateToDollar (Currency.CurrencyType.KPOINT);
                        break;
                    case Currency.CurrencyType.GPOINT:
                        currency = transactionDao.GetConversionRateToDollar (Currency.CurrencyType.GPOINT);
                        break;
                    case Currency.CurrencyType.MPOINT:
                        currency = transactionDao.GetConversionRateToDollar (Currency.CurrencyType.MPOINT);
                        break;
                    case Currency.CurrencyType.DOLLAR:
                        currency = transactionDao.GetConversionRateToDollar (Currency.CurrencyType.DOLLAR);
                        break;
                }

                return Convert.ToDecimal ((Convert.ToDecimal (1) / currency.ConversionRateToDollar) * amt);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static string GetDaySuffix (int day)
        {
            switch (day)
            {
                case 1:
                case 21:
                case 31:
                    return day + "st";
                case 2:
                case 22:
                    return day + "nd";
                case 3:
                case 23:
                    return day + "rd";
                default:
                    return day + "th";
            }
        }

        /// <summary>
        /// Retrieves the IP Address of the client accessing the site.
        /// </summary>
        /// <returns>The user's IP address, or an empty string if unable to retrieve.</returns>
        public static string GetVisitorIPAddress()
        {
            string visitorIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (String.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            if (string.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.UserHostAddress;

            if (string.IsNullOrEmpty(visitorIPAddress) || visitorIPAddress.Trim() == "::1" || visitorIPAddress.Trim() == "localhost")
            {
                //This is for Local(LAN) Connected ID Address
                string stringHostName = Dns.GetHostName();
                //Get Ip Host Entry
                IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
                //Get Ip Address From The Ip Host Entry Address List
                IPAddress[] arrIpAddress = ipHostEntries.AddressList;

                try
                {
                    visitorIPAddress = arrIpAddress[arrIpAddress.Length - 2].ToString();
                }
                catch
                {
                    try
                    {
                        visitorIPAddress = arrIpAddress[0].ToString();
                    }
                    catch
                    {
                        try
                        {
                            arrIpAddress = Dns.GetHostAddresses(stringHostName);
                            visitorIPAddress = arrIpAddress[0].ToString();
                        }
                        catch
                        {
                            visitorIPAddress = string.Empty;
                        }
                    }
                }
            }

            return visitorIPAddress;
        }


        /// <summary>
        /// Methods to remove HTML from strings.
        /// </summary>
        public static class HtmlRemoval
        {
            /// <summary>
            /// Remove HTML from string with Regex.
            /// </summary>
            public static string StripTagsRegex (string source)
            {
                return Regex.Replace (source, "<.*?>", string.Empty);
            }

            /// <summary>
            /// Compiled regular expression for performance.
            /// </summary>
            static Regex _htmlRegex = new Regex ("<.*?>", RegexOptions.Compiled);

            /// <summary>
            /// Remove HTML from string with compiled Regex.
            /// </summary>
            public static string StripTagsRegexCompiled (string source)
            {
                return _htmlRegex.Replace (source, string.Empty);
            }

            /// <summary>
            /// Remove HTML tags from string using char array.
            /// </summary>
            public static string StripTagsCharArray (string source)
            {
                char[] array = new char[source.Length];
                int arrayIndex = 0;
                bool inside = false;

                for (int i = 0; i < source.Length; i++)
                {
                    char let = source[i];
                    if (let == '<')
                    {
                        inside = true;
                        continue;
                    }
                    if (let == '>')
                    {
                        inside = false;
                        continue;
                    }
                    if (!inside)
                    {
                        array[arrayIndex] = let;
                        arrayIndex++;
                    }
                }
                return new string (array, 0, arrayIndex);
            }
        }

        public bool SetAsyncTaskCompleteKey(string key, string status, string message, int timeoutSeconds=120)
        {
            status = status ?? string.Empty;
            message = message ?? string.Empty;
            var taskStatus = new AsyncTaskStatus { TaskKey = key, Status = status, Message = message };

            // Be sure to log the status in the database in case memcache update fails.
            var recordsChanged = gameDao.UpsertAsyncTaskStatusLog(taskStatus);

            // Cache the key to save on database hits.
            try
            {
                CentralCache.Add(CentralCache.keyAsyncTaskCompletion(key), taskStatus, new TimeSpan(0, 0, timeoutSeconds));
            }
            catch (Exception) { }

            return recordsChanged > 0;
        }

        public AsyncTaskStatus CheckAsyncTaskCompleteKey(string key)
        {
            // Try cache first to save a few db hits.
            AsyncTaskStatus results = new AsyncTaskStatus();
            try
            {
                var cacheResults = CentralCache.Get(CentralCache.keyAsyncTaskCompletion(key));
                if (cacheResults != null)
                {
                    CentralCache.Remove(CentralCache.keyAsyncTaskCompletion(key));
                    results = (AsyncTaskStatus)cacheResults;
                }
            }
            catch (Exception e)
            {
                m_logger.Error("Unable to check completion status of task key: " + key + "Exception: " + e.Message);
            }

            // If not found in cache, fall back to the database.
            if (string.IsNullOrWhiteSpace(results.TaskKey))
            {
                results = gameDao.GetAsyncTaskStatusLog(key);
            }
            
            return results;
        }
    }
}


