///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade.ProxyObjects
{
    /// <summary>
    /// A proxy object representing Stats. Implements lazy loading (also called just-in-time loading).  Therefore 
    /// the Stats is loaded only when it is absolutely necessary.  This class
    /// is a proxy to a limited resource (the list coming from the database). 
    /// 
    /// GoF Design Patterns: Proxy.
    /// Enterprise Design Patterns: LazyLoad.
    /// </summary>

    [Serializable]
    public class ProxyForPreferences : Preferences
    {
        private IUserDao userDao = DataAccess.UserDao;
        Preferences _preferences;
        private User _user;

        /// <summary>
        /// Constructor for ProxyForStats.
        /// </summary>
        public ProxyForPreferences (User user)
        {
            _user = user;
        }

        private void LoadPreferences ()
        {
            if (_preferences == null)
            {
                _preferences = (Preferences)userDao.GetUserPreferences(_user.UserId);
            }
        }

        /// <summary>
        /// KeiPointIdPurchaseType
        /// </summary>
        public override string AlwaysPurchaseWithCurrencyType
        {
            get { LoadPreferences(); return _preferences.AlwaysPurchaseWithCurrencyType; }
            set { _preferences.AlwaysPurchaseWithCurrencyType = value; }
        }

        public override bool FilterByCountry
        {
            get { LoadPreferences(); return _preferences.FilterByCountry; }
            set { _preferences.FilterByCountry = value; }
        }

        
    }
}

