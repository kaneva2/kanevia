///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade.ProxyObjects
{
    [Serializable]
    public class ProxyForUsersPrivileges : UsersPrivileges
    {
        private ISiteManagementDao siteManagementDao = DataAccess.SiteManagementDao;
        UsersPrivileges _usersPrivileges;
        private User _user;

        /// <summary>
        /// Constructor for ProxyForStats.
        /// </summary>
        public ProxyForUsersPrivileges(User user)
        {
            _user = user;
        }

        private void LoadUserPrivileges()
        {
            if (_usersPrivileges == null)
            {
                _usersPrivileges = new UsersPrivileges();

                //get the site privileges first since the get by role id also popultates the site role field
                _usersPrivileges.UserPrivileges = new SiteMgmtFacade().GetUsersRoleByRoleId(_user);

            }
        }

        public override int SiteRoleId
        {
            get { LoadUserPrivileges(); return _usersPrivileges.SiteRoleId; }
            set { _usersPrivileges.SiteRoleId = value; }
        }

        public override NameValueCollection UserPrivileges
        {
            get { LoadUserPrivileges(); return _usersPrivileges.UserPrivileges; }
            set { _usersPrivileges.UserPrivileges = value; }
        }
    }
}
