///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade.ProxyObjects
{
    [Serializable]
    public class ProxyForContestEntries<ContestEntry> : ProxyList<ContestEntry>
    {
        private IContestDao contestDao = DataAccess.ContestDao;
        private ulong _contestId = 0;
        private string _orderBy = "";
        
        /// <summary>
        /// Constructor for ProxyForFriends.
        /// </summary>
        /// <param name="user">User.</param>
        public ProxyForContestEntries (Contest contest, string orderBy)
        {
            _contestId = contest.ContestId;
            _orderBy = orderBy;
        }

        /// <summary>
        /// Loads the entries associated with a contest.
        /// </summary>
        /// <returns>Number of items loaded.</returns>
        protected override int LoadList ()
        {
            if (base.List == null)
            {
                base.List = (IList<ContestEntry>) contestDao.GetContestEntries (_contestId, _orderBy, "entry_status=" + (int)eCONTEST_ENTRY_STATUS.Active, 1, 999);
            }
            return Count;
        }
    }
}
