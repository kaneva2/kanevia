///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade.ProxyObjects
{
    [Serializable]
    public class ProxyForCommunityPreferences : CommunityPreferences
    {
        #region Declarations

        private ICommunityDao communityDao = DataAccess.CommunityDao;
        CommunityPreferences communityPreferences;
        private Community community;

        #endregion

        /// <summary>
        /// Constructor for ProxyForCommunityStats.
        /// </summary>
        public ProxyForCommunityPreferences(Community community)
        {
            this.community = community;
        }

        private void LoadCommunityPreferences()
        {
            if (communityPreferences == null)
            {
                communityPreferences = (CommunityPreferences)communityDao.GetCommunityPreferences(community.CommunityId);            
            }
        }


        #region Attributes

        public override int CommunityPreferenceId
        {
            get { LoadCommunityPreferences(); return communityPreferences.CommunityPreferenceId; }
            set { communityPreferences.CommunityPreferenceId = value; }
        }

        public override int CommunityId
        {
            get { LoadCommunityPreferences(); return communityPreferences.CommunityId; }
            set { communityPreferences.CommunityId = value; }
        }

        public override bool ShowGender
        {
            get { LoadCommunityPreferences(); return communityPreferences.ShowGender; }
            set { communityPreferences.ShowGender = value; }
        }

        public override bool ShowLocation
        {
            get { LoadCommunityPreferences(); return communityPreferences.ShowLocation; }
            set { communityPreferences.ShowLocation = value; }
        }

        public override bool ShowAge
        {
            get { LoadCommunityPreferences(); return communityPreferences.ShowAge; }
            set { communityPreferences.ShowAge = value; }
        }

        #endregion

    }
}
