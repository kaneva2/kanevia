///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade.ProxyObjects
{
    /// <summary>
    /// A proxy object representing Stats. Implements lazy loading (also called just-in-time loading).  Therefore 
    /// the Stats is loaded only when it is absolutely necessary.  This class
    /// is a proxy to a limited resource (the list coming from the database). 
    /// 
    /// GoF Design Patterns: Proxy.
    /// Enterprise Design Patterns: LazyLoad.
    /// </summary>

    [Serializable]
    public class ProxyForStats : UserStats
    {
        private IUserDao userDao = DataAccess.UserDao;
        UserStats _userStats;
        private User _user;

        /// <summary>
        /// Constructor for ProxyForStats.
        /// </summary>
        public ProxyForStats(User user)
        {
            _user = user;
        }

        private void LoadStats()
        {
            if (_userStats == null)
            {
                _userStats = (UserStats)userDao.GetUserStats(_user.UserId);            
            }
        }

        /// <summary>
        /// NumberOfLogins
        /// </summary>
        public override int NumberOfLogins
        {
            get { LoadStats(); return _userStats.NumberOfLogins; }
            set { _userStats.NumberOfLogins = value; }
        }

        public override int NumberOfInboxMessages
        {
            get { LoadStats();return _userStats.NumberOfInboxMessages; }
            set { _userStats.NumberOfInboxMessages = value; }
        }

        public override int NumberOutboxMessages
        {
            get { LoadStats();return _userStats.NumberOutboxMessages; }
            set { _userStats.NumberOutboxMessages = value; }
        }

        public override int NumberForumPosts
        {
            get { LoadStats();return _userStats.NumberForumPosts; }
            set { _userStats.NumberForumPosts = value; }
        }

        public override int NumberOfFriends
        {
            get { LoadStats();return _userStats.NumberOfFriends; }
            set { _userStats.NumberOfFriends = value; }
        }

        public override int NumberOfPending
        {
            get { LoadStats();return _userStats.NumberOfPending; }
            set { _userStats.NumberOfPending = value; }
        }

        public override int NumberOfRequests
        {
            get { LoadStats();return _userStats.NumberOfRequests; }
            set { _userStats.NumberOfRequests = value; }
        }

        public override int NumberOfNewMessages
        {
            get { LoadStats();return _userStats.NumberOfNewMessages; }
            set { _userStats.NumberOfNewMessages = value; }
        }

        public override int Number3DAppRequests
        {
            get { LoadStats (); return _userStats.Number3DAppRequests; }
            set { _userStats.Number3DAppRequests = value; }
        }

        public override int NumberCommunityInvites
        {
            get { LoadStats (); return _userStats.NumberCommunityInvites; }
            set { _userStats.NumberCommunityInvites = value; }
        }

        public override int NumberOfBlogs
        {
            get { LoadStats();return _userStats.NumberOfBlogs; }
            set { _userStats.NumberOfBlogs = value; }
        }

        public override int NumberOfComments
        {
            get { LoadStats();return _userStats.NumberOfComments; }
            set { _userStats.NumberOfComments = value; }
        }

        public override int NumberOfAssetDiggs
        {
            get { LoadStats();return _userStats.NumberOfAssetDiggs; }
            set { _userStats.NumberOfAssetDiggs = value; }
        }

        public override int CommunitiesIOwn
        {
            get { LoadStats(); return _userStats.CommunitiesIOwn; }
            set { _userStats.CommunitiesIOwn = value; }
        }

        public override int CommunitiesIModerate
        {
            get { LoadStats(); return _userStats.CommunitiesIModerate; }
            set { _userStats.CommunitiesIModerate = value; }
        }

        public override int CommunitiesIBelongTo
        {
            get { LoadStats(); return _userStats.CommunitiesIBelongTo; }
            set { _userStats.CommunitiesIBelongTo = value; }
        }

        public override UInt32 InterestCount
        {
            get { LoadStats();return _userStats.InterestCount; }
            set { _userStats.InterestCount = value; }
        }

        public override UInt32 NumberAssetShares
        {
            get { LoadStats();return _userStats.NumberAssetShares; }
            set { _userStats.NumberAssetShares = value; }
        }

        public override UInt32 NumberCommunityShares
        {
            get { LoadStats();return _userStats.NumberCommunityShares; }
            set { _userStats.NumberCommunityShares = value; }
        }

        public override UInt32 NumberInvitesSent
        {
            get { LoadStats();return _userStats.NumberInvitesSent; }
            set { _userStats.NumberInvitesSent = value; }
        }

        public override UInt32 NumberInvitesAccepted
        {
            get { LoadStats();return _userStats.NumberInvitesAccepted; }
            set { _userStats.NumberInvitesAccepted = value; }
        }

        public override UInt32 NumberInvitesInWorld
        {
            get { LoadStats(); return _userStats.NumberInvitesInWorld; }
            set { _userStats.NumberInvitesInWorld = value; }
        }

        public override Double NumberFriendInviteRewards
        {
            get { LoadStats (); return _userStats.NumberFriendInviteRewards; }
            set { _userStats.NumberFriendInviteRewards = value; }
        }
    }
}

