///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade.ProxyObjects
{
    /// <summary>
    /// A proxy object representing Stats. Implements lazy loading (also called just-in-time loading).  Therefore 
    /// the Stats is loaded only when it is absolutely necessary.  This class
    /// is a proxy to a limited resource (the list coming from the database). 
    /// 
    /// GoF Design Patterns: Proxy.
    /// Enterprise Design Patterns: LazyLoad.
    /// </summary>
    
    [Serializable]
    public class ProxyForFacebookSettings : FacebookSettings
    {
 		private IUserDao userDao = DataAccess.UserDao;
		FacebookSettings _facebookSettings;
		private User _user;

		/// <summary>
		/// Constructor for ProxyForNotificationPreferences.
		/// </summary>
        public ProxyForFacebookSettings (User user)
		{
			_user = user;
		}

		private void LoadFacebookSettings()
		{
            if (_facebookSettings == null)
			{
                _facebookSettings = (FacebookSettings) userDao.GetUserFacebookSettings (_user.UserId);
			}
		}

        public override int UserId
		{
            get { LoadFacebookSettings (); return _facebookSettings.UserId; }
			set { _facebookSettings.UserId = value; }
		}
        public override UInt64 FacebookUserId
        {
            get { LoadFacebookSettings (); return _facebookSettings.FacebookUserId; }
            set { _facebookSettings.FacebookUserId = value; }
        }
        public override string AccessToken
        {
            get { LoadFacebookSettings (); return _facebookSettings.AccessToken; }
            set { _facebookSettings.AccessToken = value; }
        }
        public override bool UseFacebookProfilePicture
        {
            get { LoadFacebookSettings (); return _facebookSettings.UseFacebookProfilePicture; }
            set { _facebookSettings.UseFacebookProfilePicture = value; }
        }
    }
}
