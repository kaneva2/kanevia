///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade.ProxyObjects
{
    [Serializable]
    public class ProxyForUserBalances : UserBalances
    {
        private IUserDao userDao = DataAccess.UserDao;
        UserBalances userBalances;
        private User user;

        /// <summary>
        /// Constructor for ProxyForStats.
        /// </summary>
        public ProxyForUserBalances (User user)
        {
            this.user = user;
        }

        private void LoadUserBalances()
        {
            if (this.userBalances == null)
            {
                this.userBalances = (UserBalances) userDao.GetUserBalances (this.user.UserId);
            }
        }

        public override double GPoint
        {
            get { LoadUserBalances(); return this.userBalances.GPoint; }
            set { this.userBalances.GPoint = value; }
        }

        public override double KPoint
        {
            get { LoadUserBalances (); return this.userBalances.KPoint; }
            set { this.userBalances.KPoint = value; }
        }

        public override double MPoint
        {
            get { LoadUserBalances (); return this.userBalances.MPoint; }
            set { this.userBalances.MPoint = value; }
        }

        public override double Dollar
        {
            get { LoadUserBalances (); return this.userBalances.Dollar; }
            set { this.userBalances.Dollar = value; }
        }

        public override double Rewards
        {
            get { LoadUserBalances (); return this.userBalances.Rewards; }
        }

        public override double Credits
        {
            get { LoadUserBalances (); return this.userBalances.Credits; }
        }
    }
}
