///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade.ProxyObjects
{
    [Serializable]
    public class ProxyForChecklistPackets<ChecklistPacket> : ProxyList<ChecklistPacket>
    {
        private IFameDao fameDao = DataAccess.FameDao;
        private int _checklistId = 0;
        private int _userId = 0;

        /// <summary>
        /// Constructor for ProxyForChecklistPacket.
        /// </summary>
        /// <param name="user">Checklist.</param>
        public ProxyForChecklistPackets (Checklist checklist, int userId)
        {
            _checklistId = checklist.ChecklistId;
            _userId = userId;
        }

        /// <summary>
        /// Loads the entries associated with a contest.
        /// </summary>
        /// <returns>Number of items loaded.</returns>
        protected override int LoadList ()
        {
            if (base.List == null)
            {
                base.List = (IList<ChecklistPacket>) fameDao.GetChecklistPackets (_userId, _checklistId, "sort_order");
            }
            return Count;
        }
    }
}
