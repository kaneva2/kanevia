///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade.ProxyObjects
{
	/// <summary>
	/// A proxy object representing Stats. Implements lazy loading (also called just-in-time loading).  Therefore 
	/// the Stats is loaded only when it is absolutely necessary.  This class
	/// is a proxy to a limited resource (the list coming from the database). 
	/// 
	/// GoF Design Patterns: Proxy.
	/// Enterprise Design Patterns: LazyLoad.
	/// </summary>

	[Serializable]
	public class ProxyForNotificationPreferences : NotificationPreferences
	{
		private IUserDao userDao = DataAccess.UserDao;
		NotificationPreferences _notificationPreferences;
		private User _user;

		/// <summary>
		/// Constructor for ProxyForNotificationPreferences.
		/// </summary>
		public ProxyForNotificationPreferences(User user)
		{
			_user = user;
		}

		private void LoadNotificationPreferences()
		{
			if (_notificationPreferences == null)
			{
				_notificationPreferences = (NotificationPreferences)userDao.GetUserNotificationPreferences(_user.UserId);
			}
		}

		public override bool NotifyBlastComments
		{
			get { LoadNotificationPreferences(); return _notificationPreferences.NotifyBlastComments; }
			set { _notificationPreferences.NotifyBlastComments = value; }
		}

		public override bool NotifyMediaComments
		{
			get { LoadNotificationPreferences(); return _notificationPreferences.NotifyMediaComments; }
			set { _notificationPreferences.NotifyMediaComments = value; }
		}

		public override bool NotifyShopCommentsPurchases
		{
			get { LoadNotificationPreferences(); return _notificationPreferences.NotifyShopCommentsPurchases; }
			set { _notificationPreferences.NotifyShopCommentsPurchases = value; }
		}

        public override bool NotifyEventInvites
        {
            get { LoadNotificationPreferences (); return _notificationPreferences.NotifyEventInvites; }
            set { _notificationPreferences.NotifyEventInvites = value; }
        }

        public override bool NotifyFriendBirthdays
        {
            get { LoadNotificationPreferences (); return _notificationPreferences.NotifyFriendBirthdays; }
            set { _notificationPreferences.NotifyFriendBirthdays = value; }
        }

        public override bool NotifyWorldBlasts
        {
            get { LoadNotificationPreferences (); return _notificationPreferences.NotifyWorldBlasts; }
            set { _notificationPreferences.NotifyWorldBlasts = value; }
        }

        public override bool NotifyWorldRequests
        {
            get { LoadNotificationPreferences(); return _notificationPreferences.NotifyWorldRequests; }
            set { _notificationPreferences.NotifyWorldRequests = value; }
        }
	}
}

