///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade.ProxyObjects
{
    [Serializable]
    public class ProxyForUsersSubscriptions : UsersSubscriptions
    {

        private ISubscriptionDao subscriptionDao = DataAccess.SubscriptionDao;
        UsersSubscriptions _usersSubscription;
        private User _user;

        /// <summary>
        /// Constructor for ProxyForStats.
        /// </summary>
        public ProxyForUsersSubscriptions(User user)
        {
            _user = user;
        }

        private void LoadUserSubscriptions()
        {
            if (_usersSubscription == null)
            {
                _usersSubscription = new UsersSubscriptions();

                _usersSubscription.UserOwnedSubscriptions = subscriptionDao.GetActiveUserSubscriptions(_user.UserId);
                _usersSubscription.HasAccessPass = subscriptionDao.HasValidSubscriptionByPassType((uint)_user.UserId, (uint)Configuration.AccessPassGroupID);
                _usersSubscription.HasVIPPass = subscriptionDao.HasValidSubscriptionByPassType((uint)_user.UserId, (uint)Configuration.VipPassGroupID);

            }
        }

        public override bool HasAccessPass
        {
            get { LoadUserSubscriptions(); return _usersSubscription.HasAccessPass; }
            set { _usersSubscription.HasAccessPass = value; }
        }

        public override bool HasVIPPass
        {
            get { LoadUserSubscriptions(); return _usersSubscription.HasVIPPass; }
            set { _usersSubscription.HasVIPPass = value; }
        }

        public override List<UserSubscription> UserOwnedSubscriptions
        {
            get { LoadUserSubscriptions(); return _usersSubscription.UserOwnedSubscriptions; }
            set { _usersSubscription.UserOwnedSubscriptions = value; }
        }

    }
}
