///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade.ProxyObjects
{
    [Serializable]
    class ProxyForCommunityWOK3DApp : WOK3DApp
    {
        private int communityId = 0;
        private ICommunityDao communityDao = DataAccess.CommunityDao;
        WOK3DApp wok3DApp;

        public ProxyForCommunityWOK3DApp (int communityId)
        {
            this.communityId = communityId;       
        }

        private void LoadWOK3DApp ()
        {
            if (wok3DApp == null)
            {
                wok3DApp = (WOK3DApp) communityDao.GetWOK3DApp (this.communityId);
            }
        }

        public override int GameId
        {
            get { LoadWOK3DApp (); return wok3DApp.GameId; }
            set { wok3DApp.GameId = value; }
        }

        public override int GameStatusId
        {
            get { LoadWOK3DApp (); return wok3DApp.GameStatusId; }
            set { wok3DApp.GameStatusId = value; }
        }
    }
}
