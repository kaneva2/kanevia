///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade.ProxyObjects
{
    [Serializable]
    public class ProxyForCommunityStats : CommunityStats
    {
        #region Declarations

        private ICommunityDao communityDao = DataAccess.CommunityDao;
        CommunityStats communityStats;
        private Community community;

        #endregion

        /// <summary>
        /// Constructor for ProxyForCommunityStats.
        /// </summary>
        public ProxyForCommunityStats(Community community)
        {
            this.community = community;
        }

        private void LoadCommunityStats()
        {
            if (communityStats == null)
            {
                communityStats = (CommunityStats)communityDao.GetCommunityStats(community.CommunityId);            
            }
        }


        #region Attributes

        public override int ChannelId
        {
            get { LoadCommunityStats(); return communityStats.ChannelId; }
            set { communityStats.ChannelId = value; }
        }

        public override int NumberOfViews
        {
            get { LoadCommunityStats(); return communityStats.NumberOfViews; }
            set { communityStats.NumberOfViews = value; }
        }

        public override int NumberOfMembers
        {
            get { LoadCommunityStats(); return communityStats.NumberOfMembers; }
            set { communityStats.NumberOfMembers = value; }
        }

        public override int NumberOfDiggs
        {
            get { LoadCommunityStats(); return communityStats.NumberOfDiggs; }
            set { communityStats.NumberOfDiggs = value; }
        }

        public override int NumberTimesShared
        {
            get { LoadCommunityStats(); return communityStats.NumberTimesShared; }
            set { communityStats.NumberTimesShared = value; }
        }

        public override int NumberOfPendingMembers
        {
            get { LoadCommunityStats(); return communityStats.NumberOfPendingMembers; }
            set { communityStats.NumberOfPendingMembers = value; }
        }

        public override UInt32 NumberPosts7Days
        {
            get { LoadCommunityStats(); return communityStats.NumberPosts7Days; }
            set { communityStats.NumberPosts7Days = value; }
        }

        public override uint GameCount
        {
            get { LoadCommunityStats(); return communityStats.GameCount; }
            set { communityStats.GameCount = value; }
        }

        public override uint VideoCount
        {
            get { LoadCommunityStats(); return communityStats.VideoCount; }
            set { communityStats.VideoCount = value; }
        }

        public override uint MusicCount
        {
            get { LoadCommunityStats(); return communityStats.MusicCount; }
            set { communityStats.MusicCount = value; }
        }

        public override uint PhotoCount
        {
            get { LoadCommunityStats(); return communityStats.PhotoCount; }
            set { communityStats.PhotoCount = value; }
        }

        public override uint PatternCount
        {
            get { LoadCommunityStats(); return communityStats.PatternCount; }
            set { communityStats.PatternCount = value; }
        }

        public override uint TvCount
        {
            get { LoadCommunityStats(); return communityStats.TvCount; }
            set { communityStats.TvCount = value; }
        }

        public override uint WidgetCount
        {
            get { LoadCommunityStats(); return communityStats.WidgetCount; }
            set { communityStats.WidgetCount = value; }
        }

        public override uint BlogCount
        {
            get { LoadCommunityStats(); return communityStats.BlogCount; }
            set { communityStats.BlogCount = value; }
        }

        #endregion

    }
}
