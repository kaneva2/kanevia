///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class ScriptGameItemFacade
    {
        private IScriptGameItemDao scriptGameItemDao = DataAccess.ScriptGameItemDao;

        #region Game Item Global Configs

        /// <summary>
        /// GetSGIGlobalConfigValue
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public string GetSGIGlobalConfigValue(string propertyName)
        {
            return scriptGameItemDao.GetSGIGlobalConfigValue(propertyName);
        }

        /// <summary>
        /// GetSGIGlobalConfigValues
        /// </summary>
        public Dictionary<string, string> GetSGIGlobalConfigValues()
        {
            return scriptGameItemDao.GetSGIGlobalConfigValues();
        }

        /// <summary>
        /// UpdateSGIGlobalConfig
        /// </summary>
        public bool UpdateSGIGlobalConfig(Dictionary<string, string> config)
        {
            return scriptGameItemDao.UpdateSGIGlobalConfig(config);
        }

        /// <summary>
        /// DeleteSGIGlobalConfig
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public bool DeleteSGIGlobalConfig(string propName)
        {
            return scriptGameItemDao.DeleteSGIGlobalConfig(propName);
        }

        #endregion

        #region Game Item Snapshots

        /// <summary>
        /// GetSnapshotScriptGameItem
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public ScriptGameItem GetSnapshotScriptGameItem(int gIGlid, bool includeProperties = false, bool includeBundledGlids = false)
        {
            return scriptGameItemDao.GetSnapshotScriptGameItem(gIGlid, includeProperties, includeBundledGlids);
        }

        /// <summary>
        /// InsertSnapshotScriptGameItems
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public bool InsertSnapshotScriptGameItems(ref List<ScriptGameItem> sgItems, int userId, bool addToInventory = false, bool itemsAreDefault = false)
        {
            return scriptGameItemDao.InsertSnapshotScriptGameItems(ref sgItems, userId, addToInventory, itemsAreDefault);
        }

        /// <summary>
        /// AddSnapshotScriptGameItemsToTemplate
        /// </summary>
        /// <returns>False if an error occurred in processing, True otherwise.</returns>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public bool AddWorldScriptGameItemsToCustomDeed(int zoneInstanceId, int zoneType, int templateGlobalId, int userId)
        {
            // Don't pull game items if the framework is not enabled.
            SGFrameworkSettings settings = (new GameFacade()).GetWorldSGFrameworkSettings(zoneInstanceId, zoneType);
            if (settings == null || !settings.FrameworkEnabled)
            {
                return true;
            }

            PagedList<ScriptGameItem> sgItems = GetWorldScriptGameItems(zoneInstanceId, zoneType, string.Empty, 1, Int32.MaxValue, true, true);
            if (sgItems == null || sgItems.Count == 0)
            {
                return true;
            }

            // ALWAYS take a new snapshot of quests!!! They rely on dynamic object placements, so must have their own copy.
            List<ScriptGameItem> quests = sgItems.Where(sg => sg.ItemType == "quest").ToList();
            if (quests != null && quests.Count > 0)
            {
                bool questsOk = InsertSnapshotScriptGameItems(ref quests, userId);

                if (!questsOk)
                {
                    return false;
                }
            }

            return scriptGameItemDao.AddSnapshotScriptGameItemsToCustomDeed(sgItems, templateGlobalId);
        }

        /// <summary>
        /// DeleteAllSGItemsInCustomDeed
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public bool DeleteAllSGItemsInCustomDeed(int templateGlobalId)
        {
            return scriptGameItemDao.DeleteAllSGItemsInCustomDeed(templateGlobalId);
        }

        /// <summary>
        /// UpdateSnapshotScriptGameItem
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public bool UpdateSnapshotScriptGameItem(ScriptGameItem sgItem, int userId)
        {
            return scriptGameItemDao.UpdateSnapshotScriptGameItem(sgItem, userId);
        }

        /// <summary>
        /// Called to extract bundled glids from a game item's other properties
        /// </summary>
        public List<int> ExtractBundledGlids(ScriptGameItem gameItem)
        {
            string strProp;
            string strBehaviorParams;
            JToken jToken;

            List<int> bundledGlids = new List<int>();

            // Only inventory compatible items have bundles
            if (gameItem.InventoryCompatible)
            {
                // Add "glid"
                bundledGlids.Add(gameItem.Glid);

                // Process top-level game item properties
                foreach (string property in Enum.GetNames(typeof(ScriptGameItem.ParamGlidFields)))
                {
                    if (gameItem.Properties.TryGetValue(property, out strProp))
                    {
                        jToken = JsonConvert.DeserializeObject<JToken>(strProp);
                        if (jToken.Type == JTokenType.Array)
                        {
                            bundledGlids.AddRange(jToken.Values<int>());
                        }
                        else if (jToken.Type == JTokenType.Object)
                        {
                            bundledGlids.AddRange(((JObject)jToken).Properties().Values<int>());
                        }
                        else
                        {
                            bundledGlids.Add(jToken.Value<int>());
                        }
                    }
                }

                // Process behavior params
                if (gameItem.Properties.TryGetValue("behaviorParams", out strBehaviorParams))
                {
                    JObject behaviorParams = JsonConvert.DeserializeObject<JObject>(strBehaviorParams);
                    Dictionary<string, JToken> behaviorParamLookup = behaviorParams.Properties().ToDictionary(p => p.Name, p => p.Value);

                    foreach (string property in Enum.GetNames(typeof(ScriptGameItem.BehaviorParamGlidFields)))
                    {
                        if (behaviorParamLookup.TryGetValue(property, out jToken))
                        {
                            if (jToken.Type == JTokenType.Array)
                            {
                                bundledGlids.AddRange(jToken.Values<int>());
                            }
                            else if (jToken.Type == JTokenType.Object)
                            {
                                bundledGlids.AddRange(((JObject)jToken).Properties().Values<int>());
                            }
                            else
                            {
                                bundledGlids.Add(jToken.Value<int>());
                            }
                        }
                    }
                }
            }

            // Remove duplicates and invalid glids
            return bundledGlids.Distinct().Where(i => i > 0).ToList();
        }

        /// <summary>
        /// Takes a snapshot of all the game items in a world.
        /// </summary>
        /// <returns>False if an error was encountered during the snapshot, True otherwise.</returns>
        public bool SnapshotWorldScriptGameitems(int userId, int zoneInstanceId, int zoneType)
        {
            bool snapshotSucceeded = true;

            List<ScriptGameItem> itemsToSnapshot = GetWorldScriptGameItems(zoneInstanceId, zoneType, "sgi.game_item_glid IS NULL AND sgi.item_type <> 'quest' ", string.Empty, 1, Int32.MaxValue, true).ToList();

            if (itemsToSnapshot.Count > 0)
            {
                foreach (ScriptGameItem item in itemsToSnapshot)
                {
                    item.BundledGlids = ExtractBundledGlids(item);
                }

                // Make sure each game item in the world has a valid snapshot
                snapshotSucceeded = InsertSnapshotScriptGameItems(ref itemsToSnapshot, userId);

                if (snapshotSucceeded)
                {
                    // Update the world's giglids
                    snapshotSucceeded = UpdateWorldGIGLIDs(zoneInstanceId, zoneType, itemsToSnapshot);
                }
            }

            return snapshotSucceeded;
        }

        #endregion

        #region Default Script Game Items

        /// <summary>
        /// GetSnapshotScriptGameItem
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<ScriptGameItem> GetDefaultScriptGameItems(string searchString, string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false)
        {
            return scriptGameItemDao.GetDefaultScriptGameItems(searchString, orderBy, pageNumber, pageSize, includeProperties, includeBundledGlids);
        }

        /// <summary>
        /// GetDeedScriptGameItems
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<ScriptGameItem> GetDeedScriptGameItems(int templateGlid, string searchString, string[] itemTypes,
            string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false)
        {
            return scriptGameItemDao.GetDeedScriptGameItems(templateGlid, searchString, itemTypes, orderBy, pageNumber, pageSize, includeProperties, includeBundledGlids);
        }

        /// <summary>
        /// GetTemplateScriptGameItems
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<ScriptGameItem> GetTemplateScriptGameItems(int templateId, string searchString, string[] itemTypes,
            string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false)
        {
            return scriptGameItemDao.GetTemplateScriptGameItems(templateId, searchString, itemTypes, orderBy, pageNumber, pageSize, includeProperties, includeBundledGlids);
        }

        /// <summary>
        /// Helper method used to parse vendor trade items from the JSON "trades" parameter.
        /// </summary>
        private void ParseVendorTrades(Dictionary<int, ScriptGameItem> gameItemLookup, ScriptGameItem vendor)
        {
            string strTrades;
            if (vendor.Properties.TryGetValue("trades", out strTrades))
            {
                // Trades are always stored as object arrays, unless they're empty.
                JToken jToken = JsonConvert.DeserializeObject<JToken>(strTrades);
                if (jToken.Type == JTokenType.Array)
                {
                    foreach (JToken trade in jToken)
                    {
                        int input;
                        int output;
                        int outputCount;
                        int cost;
                        int time;

                        Int32.TryParse((trade["input"] ?? "0").ToString(), out input);
                        Int32.TryParse((trade["output"] ?? "0").ToString(), out output);
                        Int32.TryParse((trade["outputCount"] ?? "1").ToString(), out outputCount);
                        Int32.TryParse((trade["cost"] ?? "1").ToString(), out cost);
                        Int32.TryParse((trade["time"] ?? "0").ToString(), out time);

                        // Get input item. If not found in list of world game items, it may be another kind of value like Credits.
                        ScriptGameItem inputGameItem;
                        if (!gameItemLookup.TryGetValue(input, out inputGameItem))
                        {
                            inputGameItem = new ScriptGameItem();

                            if (trade["input"] != null)
                            {
                                inputGameItem.Name = trade["input"].ToString();
                            }
                        }

                        // Get output item. If not found in list of world game items, it may be another kind of value like Credits.
                        ScriptGameItem outputGameItem;
                        if (!gameItemLookup.TryGetValue(output, out outputGameItem))
                        {
                            outputGameItem = new ScriptGameItem();

                            if (trade["output"] != null)
                            {
                                outputGameItem.Name = trade["output"].ToString();
                            }
                        }

                        vendor.VendorTrades.Add(new SGIVendorItem(inputGameItem, outputGameItem, outputCount, cost, time));
                    }
                }
            }
        }

        /// <summary>
        /// Helper method used to parse quest giver quests from the JSON "quests" parameter.
        /// </summary>
        private void ParseItemQuests(Dictionary<int, ScriptGameItem> gameItemLookup, ScriptGameItem questGiver)
        {
            ScriptGameItem sgiOut;
            string strOut;
            if (questGiver.Properties.TryGetValue("behaviorParams", out strOut))
            {
                // Quests are stored as an array of integer ids.
                JToken jToken = JsonConvert.DeserializeObject<JToken>(strOut);
                if (jToken.Type == JTokenType.Object && jToken["quests"] != null && jToken["quests"].Type == JTokenType.Array)
                {
                    foreach (JToken questToken in jToken["quests"])
                    {
                        int questId = questToken.Value<int>();

                        // Get input item. If not found in list of world game items, it may be another kind of value like Credits.
                        ScriptGameItem questItem;
                        if (gameItemLookup.TryGetValue(questId, out questItem))
                        {
                            SGIQuest quest = new SGIQuest();
                            quest.QuestGameItem = questItem;
                            quest.Repeatable = (questItem.Properties.TryGetValue("repeatable", out strOut) ? strOut : "false") == "true";
                            quest.RepeatTime = Int32.Parse(questItem.Properties.TryGetValue("repeatTime", out strOut) ? strOut : "0");
                            
                            // Parse required item
                            JToken requiredItemToken = (questItem.Properties.TryGetValue("requiredItem", out strOut) ? JsonConvert.DeserializeObject<JToken>(strOut) : null);
                            if (requiredItemToken != null)
                            {
                                int requiredUnid = requiredItemToken["requiredUNID"].Value<int>();
                                int requiredCount = requiredItemToken["requiredCount"].Value<int>();

                                if (gameItemLookup.TryGetValue(requiredUnid, out sgiOut))
                                {
                                    quest.RequiredItem = sgiOut;
                                    quest.RequiredItemQuantity = requiredCount;
                                }
                            }

                            // Parse reward item
                            JToken rewardItemToken = (questItem.Properties.TryGetValue("rewardItem", out strOut) ? JsonConvert.DeserializeObject<JToken>(strOut) : null);
                            if (rewardItemToken != null)
                            {
                                int rewardUnid = rewardItemToken["rewardUNID"].Value<int>();
                                int rewardCount = rewardItemToken["rewardCount"].Value<int>();

                                if (gameItemLookup.TryGetValue(rewardUnid, out sgiOut))
                                {
                                    quest.RewardItem = sgiOut;
                                    quest.RewardItemQuantity = rewardCount;
                                }
                            }

                            // Parse prerequisite quest
                            JToken prereqQuestToken = (questItem.Properties.TryGetValue("prerequisiteQuest", out strOut) ? JsonConvert.DeserializeObject<JToken>(strOut) : null);
                            if (prereqQuestToken != null)
                            {
                                int prereqQuestId = prereqQuestToken.Value<int>();
                                quest.PrerequisiteQuest = (gameItemLookup.TryGetValue(prereqQuestId, out sgiOut) ? sgiOut : null);
                            }

                            questGiver.Quests.Add(quest);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Helper method to parse a single loot item token.
        /// </summary>
        private void ParseLootSinglesItem(Dictionary<int, ScriptGameItem> gameItemLookup, ScriptGameItem gameItem, JToken single)
        {
            int unid;
            int quantity;
            double dropChance;

            Int32.TryParse((single["UNID"] ?? "0").ToString(), out unid);
            Int32.TryParse((single["quantity"] ?? "1").ToString(), out quantity);
            Double.TryParse((single["drop"] ?? "100").ToString(), out dropChance);

            // Get output item. If not found in list of world game items, it may be another kind of value like Credits.
            ScriptGameItem outputGameItem;
            if (!gameItemLookup.TryGetValue(unid, out outputGameItem))
            {
                outputGameItem = new ScriptGameItem();

                if (single["UNID"] != null)
                {
                    outputGameItem.Name = single["UNID"].ToString();
                }
            }

            gameItem.LootItems.Add(new SGILootItem(outputGameItem, quantity.ToString(), dropChance));
        }

        /// <summary>
        /// Helper method to parse specified loot items dropped by a game item.
        /// </summary>
        private void ParseLootSingles(Dictionary<int, ScriptGameItem> gameItemLookup, ScriptGameItem gameItem)
        {
            string strLoot;
            if (gameItem.Properties.TryGetValue("loot", out strLoot))
            {
                JToken loot = JsonConvert.DeserializeObject<JToken>(strLoot);
                if (loot["singles"] != null)
                {
                    JToken jToken = loot["singles"];
                    if (jToken.Type == JTokenType.Array)
                    {
                        foreach (JToken single in jToken)
                        {
                            ParseLootSinglesItem(gameItemLookup, gameItem, single);   
                        }
                    }
                    else if(jToken.Type == JTokenType.Object)
                    {
                        ParseLootSinglesItem(gameItemLookup, gameItem, jToken);  
                    }
                }
            }
        }

        /// <summary>
        /// Helper method to parse specified loot item token.
        /// </summary>
        private void ParseLootLimitItem(Dictionary<int, ScriptGameItem> gameItemLookup, ScriptGameItem gameItem, JToken single)
        {
            int unid;
            int limit = 1;

            Int32.TryParse((single["UNID"] ?? "0").ToString(), out unid);
            Int32.TryParse((single["limit"] ?? "0").ToString(), out limit);

            // Get output item. If not found in list of world game items, it may be another kind of value like Credits.
            ScriptGameItem outputGameItem;
            if (!gameItemLookup.TryGetValue(unid, out outputGameItem))
            {
                outputGameItem = new ScriptGameItem();

                if (single["UNID"] != null)
                {
                    outputGameItem.Name = single["UNID"].ToString();
                }
            }

            gameItem.LootItems.Add(new SGILootItem(outputGameItem, "1 - " + limit.ToString()));
        }

        /// <summary>
        /// Helper method to parse specified loot items dropped by a game item.
        /// </summary>
        private void ParseLootLimitItems(Dictionary<int, ScriptGameItem> gameItemLookup, ScriptGameItem gameItem)
        {
            string strLoot;
            if (gameItem.Properties.TryGetValue("loot", out strLoot))
            {
                JToken loot = JsonConvert.DeserializeObject<JToken>(strLoot);
                if (loot["limitItems"] != null)
                {
                    JToken jToken = loot["limitItems"];
                    if (jToken.Type == JTokenType.Array)
                    {
                        foreach (JToken single in jToken)
                        {
                            ParseLootLimitItem(gameItemLookup, gameItem, single);
                        }
                    }
                    else if (jToken.Type == JTokenType.Object)
                    {
                        ParseLootLimitItem(gameItemLookup, gameItem, jToken);
                    }
                }
            }
        }

        /// <summary>
        /// Helper method to parse random loot settings for a game item.
        /// </summary>
        private void ParseRandomLootSettings(Dictionary<int, ScriptGameItem> gameItemLookup, ScriptGameItem gameItem)
        {
            string strLoot;
            if (gameItem.Properties.TryGetValue("loot", out strLoot))
            {
                JToken loot = JsonConvert.DeserializeObject<JToken>(strLoot);

                gameItem.RandomLootSettings.CanSpawnGeneric = (loot["generic"] != null && loot["generic"].Value<bool>());
                gameItem.RandomLootSettings.CanSpawnArmor = (loot["armor"] != null && loot["armor"].Value<bool>());
                gameItem.RandomLootSettings.CanSpawnAmmo = (loot["ammo"] != null && loot["ammo"].Value<bool>());
                gameItem.RandomLootSettings.CanSpawnConsumable = (loot["consumable"] != null && loot["consumable"].Value<bool>());
                gameItem.RandomLootSettings.CanSpawnWeapon = (loot["weapon"] != null && loot["weapon"].Value<bool>());
            }
        }

        /// <summary>
        /// Helper method used to parse loot from the JSON "loot" parameter.
        /// </summary>
        private void ParseGameItemLoot(Dictionary<int, ScriptGameItem> gameItemLookup, ScriptGameItem gameItem)
        {
            ParseLootSingles(gameItemLookup, gameItem);
            ParseLootLimitItems(gameItemLookup, gameItem);
            ParseRandomLootSettings(gameItemLookup, gameItem);
        }

        /// <summary>
        /// Helper method to parse specified loot items dropped by a game item.
        /// </summary>
        public List<SGICraftingItem> GetTemplateCraftingItems(int templateId)
        {
            ScriptGameItem sgiOut;
            string strOut;

            Dictionary<int, ScriptGameItem> gameItemLookup = GetTemplateScriptGameItems(templateId, string.Empty, new string[0], string.Empty, 1, Int32.MaxValue, true).ToDictionary(gi => gi.GIId);
            
            IEnumerable<ScriptGameItem> craftingItems = gameItemLookup
                .Where(gi => (gi.Value.ItemType.Equals("recipe") || gi.Value.ItemType.Equals("blueprint")))
                .Select(gi => gi.Value);

            List<SGICraftingItem> returnItems = new List<SGICraftingItem>();
            foreach(ScriptGameItem craftingItem in craftingItems)
            {
                SGICraftingItem sgiCraftingItem = new SGICraftingItem();
                sgiCraftingItem.CraftingItem = craftingItem;

                JToken inputs = (craftingItem.Properties.TryGetValue("inputs", out strOut) ? JsonConvert.DeserializeObject<JToken>(strOut) : null);
                if (inputs != null && inputs.Type == JTokenType.Array)
                {
                    foreach (JToken input in inputs)
                    {
                        int requiredUnid = input["UNID"].Value<int>();
                        int requiredCount = input["count"].Value<int>();

                        if (gameItemLookup.TryGetValue(requiredUnid, out sgiOut))
                        {
                            sgiCraftingItem.Inputs.Add(new SGICraftingInput { Item = sgiOut, Count = requiredCount });
                        }
                    }
                }

                int outputUnid = Int32.Parse(craftingItem.Properties["output"]);
                if (gameItemLookup.TryGetValue(outputUnid, out sgiOut))
                {
                    sgiCraftingItem.Output = sgiOut;
                }

                returnItems.Add(sgiCraftingItem);
            }

            return returnItems;
        }

        /// <summary>
        /// Returns a list of vendor game items that have been placed within a template.
        /// Since vendors are spawned items, it returns only a list vendors for which a spawner has been placed.
        /// </summary>
        public List<ScriptGameItem> GetTemplateScriptGameItemVendors(int templateId)
        {
            // Grab template game items, vendors as a sub list.
            Dictionary<int, ScriptGameItem> templateGameItems = GetTemplateScriptGameItems(templateId, string.Empty, new string[0], string.Empty, 1, Int32.MaxValue, true).ToDictionary( gi => gi.GIId);
            List<ScriptGameItem> spawners = GetSGISpawnersBySpawnedItem(templateGameItems, "character", "vendor");

            foreach(ScriptGameItem spawner in spawners)
            {
                foreach(SGISpawnedItem vendor in spawner.SpawnedItems)
                {
                    vendor.SpawnedGameItem.NumPlacedInTemplate = spawner.NumPlacedInTemplate;
                }
            }

            List<ScriptGameItem> vendors = spawners.SelectMany(vs => vs.SpawnedItems)
                .Select(vs => vs.SpawnedGameItem)
                .Distinct()
                .ToList();
            
            // Populate "Trades" collection for each vendor.
            foreach(ScriptGameItem vendor in vendors)
            {
                ParseVendorTrades(templateGameItems, vendor);
            }

            return vendors.Where(v => v.VendorTrades.Count > 0).ToList();
        }

        /// <summary>
        /// Returns a list of spawner game items that spawn items of a given criteria.
        /// </summary>
        private List<ScriptGameItem> GetSGISpawnersBySpawnedItem(Dictionary<int, ScriptGameItem> gameItemLookup, string spawnedItemType, string spawnedItemBehavior)
        {
            string strOut;
            ScriptGameItem sgiOut;

            // Grab a lookup of game items that match the spawned item description.
            Dictionary<int, ScriptGameItem> spawnedItemOptions = gameItemLookup
                .Where(gi => gi.Value.ItemType.Equals(spawnedItemType) 
                    && gi.Value.Properties.TryGetValue("behavior", out strOut) 
                    && strOut.Equals(spawnedItemBehavior))
                .ToDictionary(gi => gi.Key, gi => gi.Value);

            if (spawnedItemOptions.Count == 0)
            {
                return new List<ScriptGameItem>();
            }

            // Grab a list of spawner possibilities
            IEnumerable<ScriptGameItem> spawners = gameItemLookup
                .Where(gi => gi.Value.ItemType.Equals("spawner") 
                    && gi.Value.Properties.TryGetValue("behaviorParams", out strOut)
                    && strOut.Contains("spawnUNID")
                    && gi.Value.NumPlacedInTemplate > 0)
                .Select(gi => gi.Value);

            // Process each spawner checking to see if it spawns an item in the lookup.
            JsonSerializerSettings settings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
            HashSet<int> processedIds = new HashSet<int>();
            foreach(ScriptGameItem spawner in spawners)
            {
                SGISpawnedItem spawnedItem = JsonConvert.DeserializeObject<SGISpawnedItem>(spawner.Properties["behaviorParams"], settings);             
                if (spawner.Properties.TryGetValue("respawnTime", out strOut))
                {
                    spawnedItem.RespawnTime = Int32.Parse(strOut);
                }

                // If the item being spawned is one of our options, add this item to the spawner's list.
                if (spawnedItemOptions.TryGetValue(spawnedItem.SpawnUNID, out sgiOut))
                {
                    spawnedItem.SpawnedGameItem = sgiOut;
                    spawner.SpawnedItems.Add(spawnedItem);

                    // Get loot info
                    if (!processedIds.Contains(spawnedItem.SpawnedGameItem.GIId))
                    {
                        ParseGameItemLoot(gameItemLookup, spawnedItem.SpawnedGameItem);
                        ParseItemQuests(gameItemLookup, spawnedItem.SpawnedGameItem);
                    }

                    processedIds.Add(spawnedItem.SpawnedGameItem.GIId);
                }
            }

            return spawners.Where(gi => gi.SpawnedItems.Count > 0).ToList();
        }

        private List<ScriptGameItem> GetSGILootSpawners(Dictionary<int, ScriptGameItem> gameItemLookup)
        {
            string strOut;

            // Grab a list of spawner possibilities
            IEnumerable<ScriptGameItem> spawners = gameItemLookup
                .Where(gi => gi.Value.ItemType.Equals("spawner")
                    && gi.Value.Properties.TryGetValue("behavior", out strOut)
                    && strOut.Contains("spawner_loot")
                    && gi.Value.NumPlacedInTemplate > 0)
                .Select(gi => gi.Value);

            // Process each spawner checking to see if it spawns an item in the lookup.
            JsonSerializerSettings settings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
            HashSet<int> processedIds = new HashSet<int>();
            foreach (ScriptGameItem spawner in spawners)
            {
                SGISpawnedItem spawnedItem = new SGISpawnedItem();
                if (spawner.Properties.TryGetValue("respawnTime", out strOut))
                {
                    spawnedItem.RespawnTime = Int32.Parse(strOut);
                }

                spawnedItem.SpawnedGameItem = spawner;
                spawner.SpawnedItems.Add(spawnedItem);

                // Unlike other items, loot spawners store their loot in behavior params. Store it in "loot" as well, 
                // to make it easier to parse.
                if (!spawnedItem.SpawnedGameItem.Properties.ContainsKey("loot") && spawnedItem.SpawnedGameItem.Properties.ContainsKey("behaviorParams"))
                {
                    JToken behaviorParams = JsonConvert.DeserializeObject<JToken>(spawnedItem.SpawnedGameItem.Properties["behaviorParams"]);
                    spawnedItem.SpawnedGameItem.Properties.Add("loot", behaviorParams["loot"].ToString());
                }

                // Get loot info
                if (!processedIds.Contains(spawnedItem.SpawnedGameItem.GIId))
                {
                    ParseGameItemLoot(gameItemLookup, spawnedItem.SpawnedGameItem);
                }

                processedIds.Add(spawnedItem.SpawnedGameItem.GIId);
            }

            return spawners.Where(gi => gi.SpawnedItems.Count > 0).ToList();
        }

        public List<ScriptGameItem> GetTemplateQuestGivers(int templateId)
        {
            Dictionary<int, ScriptGameItem> templateGameItems = GetTemplateScriptGameItems(templateId, string.Empty, new string[0], string.Empty, 1, Int32.MaxValue, true).ToDictionary(gi => gi.GIId);

            // Get spawned quest givers
            List<ScriptGameItem> templateQuestGivers = GetSGISpawnersBySpawnedItem(templateGameItems, "character", "quest_giver");

            // Get placeable quest givers
            string strOut;
            IEnumerable<ScriptGameItem> placeableQuestGivers = templateGameItems
                .Where(gi => gi.Value.Properties.TryGetValue("behaviorParams", out strOut)
                    && strOut.Contains("quests")
                    && gi.Value.NumPlacedInTemplate > 0)
                .Select(gi => gi.Value);

            foreach(ScriptGameItem questGiver in placeableQuestGivers)
            {
                ParseItemQuests(templateGameItems, questGiver);
                templateQuestGivers.Add(questGiver);
            }

            return templateQuestGivers;
        }

        /// <summary>
        /// Returns a list of item spawners that have been placed within the template.
        /// </summary>
        public List<ScriptGameItem> GetTemplateSGISpawners(int templateId, string spawnedItemType, string spawnedItemBehavior)
        {
            Dictionary<int, ScriptGameItem> templateGameItems = GetTemplateScriptGameItems(templateId, string.Empty, new string[0], string.Empty, 1, Int32.MaxValue, true).ToDictionary(gi => gi.GIId);

            if (spawnedItemType.ToLower().Equals("loot"))
            {
                return GetSGILootSpawners(templateGameItems);
            }

            return GetSGISpawnersBySpawnedItem(templateGameItems, spawnedItemType, spawnedItemBehavior);
        }

        /// <summary>
        /// DeleteStartingScriptGameItem
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public bool DeleteStartingScriptGameItem(ScriptGameItem sgItem, int templateId = 0)
        {
            return scriptGameItemDao.DeleteStartingScriptGameItem(sgItem, templateId);
        }

        /// <summary>
        /// GetSuggestedGIGLID
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetSuggestedGIGLID()
        {
            return scriptGameItemDao.GetSuggestedGIGLID();
        }

        /// <summary>
        /// GetSuggestedBundleGlid
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetSuggestedBundleGlid()
        {
            return scriptGameItemDao.GetSuggestedBundleGlid();
        }

        /// <summary>
        /// GetSuggestedGIID
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetSuggestedGIID()
        {
            return scriptGameItemDao.GetSuggestedGIID();
        }

        #endregion

        #region World Game Items

        /// <summary>
        /// WorldHasGameItems
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool WorldHasScriptGameItems(int zoneIndex, int zoneInstanceId)
        {
            return scriptGameItemDao.WorldHasScriptGameItems(zoneIndex, zoneInstanceId);
        }

        public PagedList<ScriptGameItem> GetWorldScriptGameItems(int zoneInstanceId, int zoneType, string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false)
        {
            return GetWorldScriptGameItems(zoneInstanceId, zoneType, string.Empty, orderBy, pageNumber, pageSize, includeProperties, includeBundledGlids);
        }

        public PagedList<ScriptGameItem> GetWorldScriptGameItems(int zoneInstanceId, int zoneType, string filter, string orderBy, int pageNumber, int pageSize, bool includeProperties = false, bool includeBundledGlids = false)
        {
            PagedList<ScriptGameItem> sgItems = scriptGameItemDao.GetWorldScriptGameItems(zoneInstanceId, zoneType, filter, orderBy, pageNumber, pageSize, includeProperties);

            if (includeBundledGlids)
            {
                foreach (ScriptGameItem sgItem in sgItems)
                {
                    sgItem.BundledGlids = ExtractBundledGlids(sgItem);
                }
            }

            return sgItems;
        }
        
        [DataObjectMethod(DataObjectMethodType.Update)]
        public bool UpdateWorldGIGLIDs(int zoneInstanceId, int zoneType, List<ScriptGameItem> sgItems)
        {
            return scriptGameItemDao.UpdateWorldGIGLIDs(zoneInstanceId, zoneType, sgItems);
        }

        /// <summary>
        /// Creates an inverse mapping of bundled glids to the game items in a collection that contains them.
        /// </summary>
        /// <param name="sgItems"></param>
        /// <returns></returns>
        public Dictionary<int, List<int>> MapBundledGlidsToGIGLIDs(IEnumerable<ScriptGameItem> sgItems)
        {
            HashSet<int> processedGIGlids = new HashSet<int>();
            Dictionary<int, List<int>> bundleItems = new Dictionary<int, List<int>>();
            foreach (ScriptGameItem sgItem in sgItems)
            {
                // Protect against duplicates
                if (!processedGIGlids.Contains(sgItem.GIGlid))
                {
                    processedGIGlids.Add(sgItem.GIGlid);

                    foreach (int bundledGlid in sgItem.BundledGlids)
                    {
                        List<int> lOut;
                        if (bundleItems.TryGetValue(bundledGlid, out lOut))
                        {
                            lOut.Add(sgItem.GIGlid);
                        }
                        else
                        {
                            bundleItems[bundledGlid] = new List<int> { sgItem.GIGlid };
                        }
                    }
                }
            }

            return bundleItems;
        }

        /// <summary>
        /// Creates an inverse mapping of bundled glids to the game items in a collection that contains them.
        /// </summary>
        /// <param name="sgItems"></param>
        /// <returns></returns>
        public Dictionary<int, List<int>> MapBundledGlidsToGIIds(IEnumerable<ScriptGameItem> sgItems)
        {
            HashSet<int> processedGIIds = new HashSet<int>();
            Dictionary<int, List<int>> bundleItems = new Dictionary<int, List<int>>();
            foreach (ScriptGameItem sgItem in sgItems)
            {
                // Protect against duplicates
                if (!processedGIIds.Contains(sgItem.GIId))
                {
                    processedGIIds.Add(sgItem.GIId);

                    foreach (int bundledGlid in sgItem.BundledGlids)
                    {
                        List<int> lOut;
                        if (bundleItems.TryGetValue(bundledGlid, out lOut))
                        {
                            lOut.Add(sgItem.GIId);
                        }
                        else
                        {
                            bundleItems[bundledGlid] = new List<int> { sgItem.GIId };
                        }
                    }
                }
            }

            return bundleItems;
        }

        #endregion

        #region Inventory Game Items

        /// <summary>
        /// IsTypeInventoryCompatible
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool IsTypeInventoryCompatible(string itemType)
        {
            return scriptGameItemDao.IsTypeInventoryCompatible(itemType);
        }

        /// <summary>
        /// GetInventoryScriptGameItems
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<ScriptGameItem> GetInventoryScriptGameItems(int playerId, string inventoryType, string searchString, IList<ScriptGameItemCategory> itemTypes,
            string orderBy, int pageNumber, int pageSize, bool includeProperties = false)
        {
            return scriptGameItemDao.GetInventoryScriptGameItems(playerId, inventoryType, searchString, itemTypes, orderBy, pageNumber, pageSize, includeProperties);
        }

        /// <summary>
        /// GetInventoryNameUsage
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public string GetSuggestedGINameSuffixNum(int playerId, string inventoryType, string name, IList<ScriptGameItemCategory> itemTypes)
        {
            return scriptGameItemDao.GetSuggestedNameSuffixNum(playerId, inventoryType, name, itemTypes);
        }

        #endregion
    }
}
