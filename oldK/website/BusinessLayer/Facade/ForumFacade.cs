///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class ForumFacade
    {
        private IForumDao forumDao = DataAccess.ForumDao;


        /// <summary>
        /// Get a specific forum.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Forum GetForum(int forumId)
        {
            return forumDao.GetForum(forumId);
        }

        /// <summary>
        /// Get a specific forum.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<Forum> GetForums(int communityId)
        {

            return forumDao.GetForums(communityId);
        }

        /// <summary>
        /// Get a Paged list of latest forum topics.
        /// </summary>
        public PagedList<ForumTopic> GetLatestForumTopics(int communityId, int pageNumber, int pageSize)
        {
            return forumDao.GetLatestForumTopics(communityId, pageNumber, pageSize);
        }

        /// <summary>
        /// UpdateCommunity - takes community object as parameter
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertForumCategory(int communityId, string name, string description)
        {
            return forumDao.InsertForumCategory(communityId, name, description);
        }

        /// <summary>
        /// InsertForum - creates a new forum for a community
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertForum(int communityId, string forumName, string description, int forumCategoryId)
        {
            return forumDao.InsertForum(communityId, forumName, description, forumCategoryId);
        }

        /// <summary>
        /// GetForumCount - Get the number of forums for a given community
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetForumCount(int communityId)
        {
            return forumDao.GetForumCount(communityId);
        }

    }
}
