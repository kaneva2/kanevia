///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Xml;
using System.Net;
using log4net;

namespace Kaneva.BusinessLayer.Facade
{
    /// <summary>
    /// helper class for creating and sending Events to the game's 
    /// dispatcher server (Central Dispatcher)
    /// </summary>
    public class RemoteEventSender
    {
        // chat types from C++
        const int ctPrivate = 3;
        const int ctSystem = 7;
        const int ctS2S = 15; // server to server

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// Constructor gets server and port from web.config
        /// </summary>
        public RemoteEventSender()
        {
            m_server = System.Configuration.ConfigurationManager.AppSettings["KEPDispatcher.server"];
            if (m_server == null)
                m_server = "";
            try
            {
                m_serverPort = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["KEPDispatcher.port"]);
            }
            catch (Exception)
            {
                // not there or NaN, let send fail
            }
        }


        /// <summary>
        /// Send a test XML packet, building it by hand.  This is only a test
        /// </summary>
        public void SendTest(string msg)
        {
            string xml;
            xml = "<remote_event>" +
                    "	<event>" +
                    "		<name>TextEvent</name>" +
                    "		<version> " + 0x01000000.ToString() + "</version>" + // defaults to this
                    "		<filter>0</filter>" +               // defaults to this
                    "		<object_id>0</object_id>" +         // defaults to this
                    "		<values>" +
                    "			<value type=\"i\">0</value>" +
                    "			<value type=\"z\">test message from other side: " + msg + " </value>" +
                    "			<value type=\"i\">15</value>" + // ctS2S
                    "			<value type=\"z\"></value>" +   // no from distr list
                    "			<value type=\"z\"></value>" +   // no to distr list
                    "			<value type=\"i\">0</value>" +  // ctTalk original type 
                    "		</values>" +
                    "	</event>" +
                    "</remote_event>";

            sendRequest(xml, false, m_serverPort);
        }

        /// <summary>
        /// Broadcast a text event to all servers
        /// </summary>
        public void BroadcastMessageToAll(string msg)
        {
            BroadcastMessageToServer(msg, 0);
        }

        /// <summary>
        /// Broadcast a text event to a specific server
        /// </summary>
        public void BroadcastMessageToServer(string msg, int serverId)
        {
            if (msg.Length > 0)
            {
                XmlEventCreator xml = new XmlEventCreator("TextEvent");
                xml.AddValue(0);
                xml.AddValue(msg);
                xml.AddValue(ctS2S);
                xml.AddValue(""); // no distro list
                xml.AddValue(""); // no distro list
                xml.AddValue(0); // ctTalk is original msg type
                if (serverId != 0)
                    xml.AddServer(serverId);

                sendRequest(xml.GetXml(), false, m_serverPort);
            }
        }

        /// <summary>
        /// Broadcast a text event to a specific player
        /// </summary>
        /// <param name="msg">message to send</param>
        /// <param name="playerId">player_id from wok.players</param>
        public void BroadcastMessageToPlayer(string msg, int playerId)
        {
            if (msg.Length > 0 && playerId > 0)
            {
                XmlEventCreator xml = new XmlEventCreator("RenderTextEvent");
                xml.AddValue(msg);
                xml.AddValue(ctSystem);
                xml.AddPlayer(playerId);

                sendRequest(xml.GetXml(), false, m_serverPort);
            }
        }

        /// <summary>
        /// Broadcast a text event to a specific player from a specific player
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="fromId"></param>
        /// <param name="toName"></param>
        public void TellMessageToPlayer(string msg, int fromId, string toName)
        {
            if (msg.Length > 0 && fromId > 0 && toName.Length > 0)
            {
                XmlEventCreator xml = new XmlEventCreator("RenderTextEvent", 0, fromId);
                xml.AddValue(msg);
                xml.AddValue(ctPrivate);
                xml.AddPlayer(toName);

                sendRequest(xml.GetXml(), false, m_serverPort);
            }
        }

        public void BroadcastEventToServer(string xml)
        {
            sendRequest(xml, false, m_serverPort);
        }

        /// <summary>
        /// For the object tickler, this is the type of change made on the object
        /// </summary>
        public enum ChangeType { BadChangeType, AddObject, UpdateObject, DeleteObject };

        /// <summary>
        /// For the object tickler, this is the object that was changed
        /// </summary>
        public enum ObjectType { BadObjectType, ItemPassList, Item, NPC, PlayerInventory, BankInventory, ZonePassList, Store, BankZone, Quest, PlayerPassList, MediaPlaylist };

        /// <summary>
        /// send out an item change event to all servers
        /// </summary>
        /// <param name="ot">type object that changes</param>
        /// <param name="ct">how it changed</param>
        /// <param name="id">the object's unique id</param>
        /// <param name="sentXml">xml that was sent as the tickler event</param>
        public void BroadcastItemChangeEvent(ObjectType ot, ChangeType ct, Int32 id, out string sentXml)
        {
            broadcastItemChangeEvent(ot, ct, id, null, out sentXml);
        }

        /// <summary>
        /// send out an item change event to all servers
        /// </summary>
        /// <param name="ot">type object that changes</param>
        /// <param name="ct">how it changed</param>
        /// <param name="id">the object's unique id</param>
        public void BroadcastItemChangeEvent(ObjectType ot, ChangeType ct, Int32 id)
        {
            string sentXml;
            broadcastItemChangeEvent(ot, ct, id, null, out sentXml);
        }

        /// <summary>
        /// send out an item change event to all servers
        /// </summary>
        /// <param name="ot">type object that changes</param>
        /// <param name="ct">how it changed</param>
        /// <param name="id">the object's unique id</param>
        public void BroadcastItemChangeEvent(ObjectType ot, ChangeType ct, String id)
        {
            string sentXml;
            broadcastItemChangeEvent(ot, ct, 0, id, out sentXml);
        }

        /// <summary>
        /// private method that does the actual building and sending of the item changed event
        /// </summary>
        /// <param name="ot"></param>
        /// <param name="ct"></param>
        /// <param name="intId"></param>
        /// <param name="strId"></param>
        private void broadcastItemChangeEvent(ObjectType ot, ChangeType ct, Int32 intId, String strId, out string sentXml)
        {
            XmlEventCreator xml = new XmlEventCreator("ObjectTicklerEvent", (int)ct, (int)ot);
            if (strId != null)
                xml.AddValue(strId);
            else
                xml.AddValue(intId);
            // no server or player added, so it's sent to all servers

            sendRequest(xml.GetXml(), false, m_serverPort);
            sentXml = xml.GetXml();
        }

        //** NEW CODE HERE -- TESTING **//
        /// <summary>
        /// private method that does the actual building and sending of the item changed event
        /// </summary>
        /// <param name="ot"></param>
        /// <param name="ct"></param>
        /// <param name="intId"></param>
        /// <param name="strId"></param>
        public void broadcastItemChangeEventToServer(ObjectType ot, ChangeType ct, Int32 intId, Int32 serverId)
        {
            XmlEventCreator xml = new XmlEventCreator("ObjectTicklerEvent", (int)ct, (int)ot);
            xml.AddValue(intId);
            xml.AddServer(serverId);

            sendRequest(xml.GetXml(), false, m_serverPort);
        }


        /// <summary>
        /// copied from KEPAdmin's KEPServer.cs to send a buffer over the network 
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="getReply"></param>
        /// <param name="port"></param>
        /// <returns></returns>
        private string sendRequest(string xml, bool getReply, int port)
        {
            TcpClient conn = null;
            NetworkStream stream = null;
            string ret = "";

            try
            {
                m_logger.Debug(m_server + ", " + port);
                conn = new TcpClient(m_server, port);

                stream = conn.GetStream();

                System.Text.Encoding e = System.Text.Encoding.GetEncoding("utf-8");

                Int32 len = IPAddress.HostToNetworkOrder(xml.Length);

                byte[] buffer = BitConverter.GetBytes(len);
                stream.Write(buffer, 0, buffer.Length);

                buffer = e.GetBytes(xml);
                stream.Write(buffer, 0, xml.Length);

                if (getReply)
                {
                    buffer = new byte[BUFF_SIZE];

                    conn.ReceiveTimeout = RECV_TIMEOUT;
                    len = stream.Read(buffer, 0, 4);
                    if (len != 4)
                        throw new Exception("Bad length from KEPServer");

                    int expectedLen = BitConverter.ToInt32(buffer, 0);
                    expectedLen = IPAddress.NetworkToHostOrder(expectedLen);

                    int total = 0;
                    while (total < expectedLen && (len = stream.Read(buffer, 0, BUFF_SIZE)) > 0)
                    {
                        ret += e.GetString(buffer, 0, len);
                        total += len;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                try
                {
                    if (stream != null)
                        stream.Close();
                    if (conn != null)
                        conn.Close();
                }
                catch (Exception)
                {
                }
            }
            return ret;
        }

        static private int BUFF_SIZE = 1024;

#if DEBUG
        static private int RECV_TIMEOUT = 500000;
#else
		static private int RECV_TIMEOUT = 5000;
#endif


        private string m_server = "";
        private Int32 m_serverPort;
    }
}
