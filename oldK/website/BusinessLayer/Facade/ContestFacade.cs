///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject (true)]
    public class ContestFacade
    {
        private IContestDao contestDao = DataAccess.ContestDao;
        private IUserDao userDao = DataAccess.UserDao;

        /// <summary>
        /// GetContests 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<Contest> GetContests (int userId, string orderBy, string filter, int pageNumber, int pageSize)
        {
            return contestDao.GetContests (userId, orderBy, filter, pageNumber, pageSize);
        }

        /// <summary>
        /// SearchContests 
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Contest> SearchContests(int userId, string searchString, string orderBy, int pageNumber, int pageSize)
        {
            return contestDao.SearchContests(userId, searchString, orderBy, pageNumber, pageSize);
        }

        /// <summary>
        /// InsertContest 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int InsertContest (Contest contest)
        {
            return contestDao.InsertContest (contest);
        }
    
        /// <summary>
        /// GetContest 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public Contest GetContest (UInt64 contestId)
        {
            Contest contest = contestDao.GetContest (contestId);

            contest.ContestEntries = new ProxyForContestEntries<ContestEntry> (contest, "contest_entry_id DESC");
            
            return contest;
        }

        public int DeleteContest (UInt64 contestId, int userId)
        {
            int rc = 0;
            rc = contestDao.ArchiveContest (contestId);
            contestDao.UpdateContestStatus (contestId, (int) eCONTEST_STATUS.Deleted, userId);
            return rc;
        }

        /// <summary>
        /// InsertContestEntry 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int InsertContestEntry (ContestEntry entry)
        {
            return contestDao.InsertContestEntry (entry);
        }

        /// <summary>
        /// GetContestEntries 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public IList<ContestEntry> GetContestEntries (UInt64 contestId)
        {
            return contestDao.GetContestEntries (contestId, "created_date DESC", "status = 1", 1, 999);
        }

        /// <summary>
        /// GetContestEntries 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<ContestEntry> GetContestEntries (UInt64 contestId, string orderBy, string filter, int pageNumber, int pageSize)
        {
            return contestDao.GetContestEntries (contestId, orderBy, filter, pageNumber, pageSize);
        }

        /// <summary>
        /// GetContestsUserWon 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<Contest> GetContestsUserWon (int userId, string orderBy, string filter, int pageNumber, int pageSize)
        {
            return contestDao.GetContestsUserWon (userId, orderBy, filter, pageNumber, pageSize);
        }

        /// <summary>
        /// GetContestsNeedWinner 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<Contest> GetContestsNeedWinner (int userId, string orderBy, string filter, int pageNumber, int pageSize)
        {
            return contestDao.GetContestsNeedWinner (userId, orderBy, filter, pageNumber, pageSize);
        }

        /// <summary>
        /// MakeContestPayment
        /// </summary>
        /// <returns></returns>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int MakeContestPayment (int userId, UInt64 contestId, string keiPointId, Double amount, UInt16 transType, UInt16 contestTransType)
        {
            int wok_transaction_log_id = 0;     

            userDao.AdjustUserBalance (userId, keiPointId, amount, transType, ref wok_transaction_log_id);

            if (wok_transaction_log_id > 0)
            {
                // Update contest transaction details
                InsertContestTransactionDetails (wok_transaction_log_id, contestId, contestTransType);

                // Notify WOK
                new UserFacade ().SendBalanceUpdateToPlayer (userId, transType, keiPointId, amount);
            }

            return wok_transaction_log_id;
        }

        /// <summary>
        /// InsertContestTransactionDetails
        /// </summary>
        /// <returns></returns>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int InsertContestTransactionDetails (int wokTransLogId, UInt64 contestId, UInt16 contestTransType)
        {
            return contestDao.InsertContestTransactionDetails (wokTransLogId, contestId, contestTransType);  
        }

        /// <summary>
        /// CreateContest
        /// </summary>
        /// <returns>ContestId of newle created contest</returns>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public UInt64 CreateContest (Contest contest, int createFee, UInt16 transactionType, UInt16 contestTransactionType, string keiPointId, ref int wokTransactionLogId)
        {
            return contestDao.CreateContest (contest, createFee, transactionType, contestTransactionType, keiPointId, ref wokTransactionLogId);   
        }

        /// <summary>
        /// UpdateContestStatus
        /// </summary>
        /// <returns></returns>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int UpdateContestStatus (UInt64 contestId, int status)
        {
            return UpdateContestStatus (contestId, status, 0);
        }

        /// <summary>
        /// UpdateContestStatus
        /// </summary>
        /// <returns></returns>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int UpdateContestStatus (UInt64 contestId, int status, int userId)
        {
            return contestDao.UpdateContestStatus (contestId, status, userId);
        }

        /// <summary>
        /// ArchiveContest
        /// </summary>
        /// <returns></returns>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int ArchiveContest (UInt64 contestId)
        {                                                
            return contestDao.ArchiveContest (contestId);
        }

        /// <summary>
        /// UpdateContestWinner 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int UpdateContestWinner (UInt64 contestId, UInt64 popularWinnerEntryId, UInt64 ownerVoteWinnerEntryId)
        {
            return contestDao.UpdateContestWinner (contestId, popularWinnerEntryId, ownerVoteWinnerEntryId);
        }

        /// <summary>
        /// UpdateContestPopularWinner 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int UpdateContestPopularWinner (UInt64 contestId, UInt64 popularWinnerEntryId)
        {
            return UpdateContestWinner (contestId, popularWinnerEntryId, 0);
        }

        /// <summary>
        /// UpdateContestOwnerVoteWinner 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int UpdateContestOwnerVoteWinner (UInt64 contestId, UInt64 ownerVoteWinnerEntryId)
        {
            return UpdateContestWinner (contestId, 0, ownerVoteWinnerEntryId);
        }

        /// <summary>
        /// SetPopularWinnerAsContestWinner 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int SetPopularWinnerAsContestWinner (UInt64 contestId)
        {
            return contestDao.SetPopularWinnerAsContestWinner (contestId);
        }

        /// <summary>
        /// SetPopularWinner 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int SetPopularWinner (UInt64 contestId)
        {
            return contestDao.SetPopularWinner (contestId);
        }

        /// <summary>
        /// GetContestEntry 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public ContestEntry GetContestEntry (UInt64 contestEntryId)
        {
            return contestDao.GetContestEntry (contestEntryId);
        }
      
        /// <summary>
        /// GetContestEntryVoters 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public IList<ContestUser> GetContestEntryVoters (UInt64 contestEntryId, string orderBy, string filter)
        {
            return contestDao.GetContestEntryVoters (contestEntryId, orderBy, filter);
        }

        /// <summary>
        /// GetContestFollowers 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public IList<ContestUser> GetContestFollowers (UInt64 contestId, string orderBy, string filter)
        {
            return contestDao.GetContestFollowers (contestId, orderBy, filter);
        }

        /// <summary>
        /// InsertComment 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int InsertComment (UInt64 contestId, UInt64 contestEntryId, int userId, string comment)
        {
            return contestDao.InsertComment (contestId, contestEntryId, userId, comment);
        }

        /// <summary>
        /// DeleteComment 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int DeleteComment (UInt64 contestId, UInt64 contestEntryId, UInt64 commentId)
        {
            return contestDao.DeleteComment (contestId, contestEntryId, commentId);
        }

        /// <summary>
        /// RemoveContestEntry 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int DeleteContestEntry (ContestEntry ce, int userId)
        {
            int rc = 0;

            // if entry has already been removed, don't do again.
            if (ce.Status != (int) eCONTEST_ENTRY_STATUS.Active) { return rc; }
            
            // Set status of entry
            rc = UpdateContestEntryStatus (ce.ContestEntryId, (int)eCONTEST_ENTRY_STATUS.Disabled, userId);

            if (rc == 1)
            {
                // update contest total vote count
                UpdateVoteCount (ce.ContestId, ce.NumberOfVotes * (-1));
                // update entry count
                UpdateEntryCount (ce.ContestId, -1);
                // remove users from vote table
                DeleteEntryVoters (ce.ContestEntryId);
            }
            return rc;
        }

        /// <summary>
        /// UpdateContestEntryStatus 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        private int UpdateContestEntryStatus (UInt64 contestEntryId, int status, int userId)
        {
            return contestDao.UpdateContestEntryStatus (contestEntryId, status, userId);
        }

        /// <summary>
        /// UpdateEntryCount 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        private int UpdateEntryCount (UInt64 contestId, int amount)
        {
            return contestDao.UpdateEntryCount (contestId, amount);
        }

        /// <summary>
        /// UpdateVoteCount 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        private int UpdateVoteCount (UInt64 contestId, int amount)
        {
            return contestDao.UpdateVoteCount (contestId, amount);
        }

        /// <summary>
        /// DeteleEntryVoters 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        private int DeleteEntryVoters (UInt64 contestEntryId)
        {
            return contestDao.DeleteEntryVoters (contestEntryId);
        }



        /// <summary>
        /// GetContestComments 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<ContestComment> GetContestComments (UInt64 contestId, UInt64 contestEntryId, int pageNumber, int pageSize, string sortBy)
        {
            return contestDao.GetContestComments (contestId, contestEntryId, pageNumber, pageSize, sortBy);
        }
   
        /// <summary>
        /// GetContestsEntered 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<Contest> GetContestsEntered (int userId, string orderBy, string filter, int pageNumber, int pageSize)
        {
            return contestDao.GetContestsEntered (userId, orderBy, filter, pageNumber, pageSize);
        }

        /// <summary>
        /// GetContestsFollowing 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<Contest> GetContestsFollowing (int userId, string orderBy, string filter, int pageNumber, int pageSize)
        {
            return contestDao.GetContestsFollowing (userId, orderBy, filter, pageNumber, pageSize);
        }

        /// <summary>
        /// GetContestsVotedOn 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<Contest> GetContestsVotedOn (int userId, string orderBy, string filter, int pageNumber, int pageSize)
        {
            return contestDao.GetContestsVotedOn (userId, orderBy, filter, pageNumber, pageSize);
        }

        /// <summary>
        /// InsertVote 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int InsertVote (int userId, string ipAddress, UInt64 contestId, UInt64 contestEntryId)
        {
            return contestDao.InsertVote (userId, ipAddress, contestId, contestEntryId);
        }

        /// <summary>
        /// FollowContest 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int FollowContest (UInt64 contestId, int userId)
        {
            return contestDao.FollowContest (contestId, userId);
        }

        /// <summary>
        /// UnFollowContest 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Delete)]
        public int UnFollowContest (UInt64 contestId, int userId)
        {
            return contestDao.UnFollowContest (contestId, userId);
        }
    
        /// <summary>
        /// HasUserVotedOnContest 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public bool HasUserVotedOnContest (UInt64 contestId, int userId)
        {
            return contestDao.HasUserVotedOnContest (contestId, userId);
        }

        /// <summary>
        /// HasUserEnteredContest 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public bool HasUserEnteredContest (UInt64 contestId, int userId)
        {
            return contestDao.HasUserEnteredContest (contestId, userId);
        }
    
        /// <summary>
        /// IsUserFollowingContest 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public bool IsUserFollowingContest (UInt64 contestId, int userId)
        {
            return contestDao.IsUserFollowingContest (contestId, userId);
        }
    
        /// <summary>
        /// IsUserIPSameAsContestOwner 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public bool IsUserIPSameAsContestOwner (UInt64 contestId, string ipAddress)
        {
            return contestDao.IsUserIPSameAsContestOwner (contestId, ipAddress);
        }

        /// <summary>
        /// GetContestMessageDetails 
        /// </summary>
        public bool GetContestMessageDetails (eCONTEST_MSG_TYPES type, Contest contest, string siteName, ref string subj, ref string msg)
        {
            switch (type)
            {
                case eCONTEST_MSG_TYPES.Notify_Owner_Contest_Ended:
                    subj = "Your Contest has ended.  Please pick a winner.";
                    msg = "Your Contest has ended.<br/><br/>" +
                        "<a href=\"http://" + siteName + "/mykaneva/contests.aspx?contestId=" + contest.ContestId.ToString () + "\">" + contest.Title + "</a><br/><br/>" +
                        "You have 3 days to vote for a winner.  If you do not pick a winner, the entry with the most votes will be declared the winner.";
                    break;

                case eCONTEST_MSG_TYPES.You_Won:
                    subj = "Congratulations!  You just won a Contest!";
                    msg = "Congratulations!  You just won a Contest!<br/><br/>" +
                        "<a href=\"http://" + siteName + "/mykaneva/contests.aspx?contestId=" + contest.ContestId.ToString () + "\">" + contest.Title + "</a><br/>" +
                        contest.PrizeAmount.ToString () + " Credits have been added to your account!<br/><br/>" +
                        "Enter another <a href=\"http://" + siteName + "/mykaneva/contests.aspx\">Contest</a> for a chance to win more Credits.";
                    break;

                case eCONTEST_MSG_TYPES.Voted_On_Winner:
                    subj = "You picked the Winner in a Contest!";
                    msg = "A Contest has ended and you picked the winner!<br/><br/>" +
                        "<a href=\"http://" + siteName + "/mykaneva/contests.aspx?contestId=" + contest.ContestId.ToString () + "\">" + contest.Title + "</a><br/><br/>" +
                        "Thanks for your vote and check out more <a href=\"http://" + siteName + "/mykaneva/contests.aspx\">Contests</a>.  Can you pick another winner?";
                    break;

                case eCONTEST_MSG_TYPES.Entered_Did_Not_Win:
                    subj = "A Contest you entered has ended.";
                    msg = "A Contest you entered has ended and a winner has been chosen." +
                        "Go to <a href=\"http://" + siteName + "/mykaneva/contests.aspx\">Contests</a> and enter more!";
                    break;
                
                case eCONTEST_MSG_TYPES.Ended_No_Winner:
                    subj = contest.Title + " contest is over!";
                    msg = contest.Owner.Username + ",<br/><br/>" +
                        "Your contest is now over. There were no entries, so your credits have been refunded.<br/><br/>" +
                        "Thank you for putting together the contest.";
                    break;

                case eCONTEST_MSG_TYPES.Notify_Owner_Contest_Ended_Less_Than_Min_Entries:
                    subj = "Your contest has ended.";
                    msg = "Your Contest has ended.<br/><br/>" +
                        "<a href=\"http://" + siteName + "/mykaneva/contests.aspx?contestId=" + contest.ContestId.ToString () + "\">" + contest.Title + "</a><br/><br/>" +
                        "You have 3 days to vote for a winner, but there were not enough entries.  If you do not pick a winner, your credits will be refunded.";
                    break;
                
                case eCONTEST_MSG_TYPES.Following_Ended_Winner:
                    subj = "A Contest you are following has ended.";
                    msg = "A Contest you followed has ended and a winner has been chosen.<br/><br/>" +
                        "Go to <a href=\"http://" + siteName + "/mykaneva/contests.aspx\">Contests</a> and follow more!";
                    break;

                default:
                    return false;
            }

            return true;
        }

        /// <summary>
        /// GetEntriesUserHasVotedOn 
        /// </summary>
        public IList<UInt64> GetEntriesUserHasVotedOn (UInt64 contestId, int userId)
        {
            return contestDao.GetEntriesUserHasVotedOn (contestId, userId);
        }

        /// <summary>
        /// HasUserVotedOnContestEntry 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public bool HasUserVotedOnContestEntry (UInt64 contestEntryId, int userId)
        {
            return contestDao.HasUserVotedOnContestEntry (contestEntryId, userId);
        }
    }
}
