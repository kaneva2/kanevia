///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;
//using Kaneva.Framework.Transactions;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class CommentFacade
    {
        private ICommentsDao commentDao = DataAccess.CommentsDao;

        /// <summary>
        /// Adds a new comment
        /// </summary>
        /// <param name="comment">New Comment.</param>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertComment(Comment comment)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            return commentDao.InsertComment(comment);
        }

        /// <summary>
        /// Adds a new review to developer DB
        /// </summary>
        /// <param name="comment">New Comment.</param>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertGameReview(Comment comment)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            return commentDao.InsertGameReview(comment);
        }

        /// <summary>
        /// Adds a new wok item review
        /// </summary>
        /// <param name="comment">New Comment.</param>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertReview(Comment review)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            return commentDao.InsertReview(review);
        }

        /// <summary>
        /// Delete WOK Item Review
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public void DeleteReview(Comment review)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            commentDao.DeleteReview(review);
        }

        /// <summary>
        /// Delete developer Game Review
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public void DeleteGameReview(Comment review)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            commentDao.DeleteGameReview(review);
        }

        /// <summary>
        /// Delete comment
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public void DeleteComment(Comment comment)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            commentDao.DeleteComment(comment);
        }

        /// <summary>
        /// Update WOK item review
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateReview(Comment review)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            return commentDao.UpdateReview(review);
        }

        /// <summary>
        /// Update Developer Game review
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateGameReview(Comment review)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            return commentDao.UpdateGameReview(review);
        }

        /// <summary>
        /// Update comment
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateComment(Comment comment)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            return commentDao.UpdateComment(comment);
        }

        /// <summary>
        /// GetWOKComment
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Comment GetReview(int reviewId)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            return commentDao.GetReview(reviewId);
        }

        /// <summary>
        /// Get Game Review
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Comment GetGameReview(int reviewId)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            return commentDao.GetGameReview(reviewId);
        }

        /// <summary>
        /// GetComment
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Comment GetComment(int commentId)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            return commentDao.GetComment(commentId);
        }

        /// <summary>
        /// GetChannelComments
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Comment> GetChannelComments(int channelId, int pageNumber, int pageSize)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            return commentDao.GetChannelComments(channelId, pageNumber, pageSize);
        }

        /// <summary>
        /// GetAssetComments
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<Comment> GetAssetComments(int assetId, int pageNumber, int pageSize)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            return commentDao.GetAssetComments(assetId, pageNumber, pageSize);
        }

        /// <summary>
        /// GetWOKItemComments
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<Comment> GetWOKItemReviews(int itemId, int pageNumber, int pageSize)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            return commentDao.GetWOKItemReviews(itemId, pageNumber, pageSize);
        }

        /// <summary>
        /// GetWOKItemComments
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<Comment> GetGameReviews(int gameId, int pageNumber, int pageSize)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            return commentDao.GetGameReviews(gameId, pageNumber, pageSize);
        }

        /// <summary>
        /// GetNumPeopleForChannelComments
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetNumPeopleForChannelComments(int channelId)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            return commentDao.GetNumPeopleForChannelComments(channelId);
        }

        /// <summary>
        /// GetNumPeopleForAssetComments
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetNumPeopleForAssetComments(int assetId)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            return commentDao.GetNumPeopleForAssetComments(assetId);
        }

        /// <summary>
        /// CommentsBOTCheck
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool CommentsBOTCheck(int userId)
        {
            // TODO: add security here..
            // TODO: add argument validation here..

            return commentDao.CommentsBOTCheck(userId);
        }

    }
}
