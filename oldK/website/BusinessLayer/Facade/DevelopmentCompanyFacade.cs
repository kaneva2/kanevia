///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;
using System.Data;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class DevelopmentCompanyFacade
    {
        private IDevelopmentCompanyDao developmentCompanyDao = DataAccess.DevelopmentCompanyDao;

        /// <summary>
        /// CreateDefaultCompanyForUser - creates a company for a kaneva user that doesn't have one using their
        /// user information as starting company info. 
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int CreateDefaultCompanyForUser(int userId)
        {
            string city = "";
            string state = "";

            //get the user object to populate company info with user info
            User userInfo = (new UserFacade()).GetUser(userId);


            //parse the city and state from the user.location field
            if (userInfo.Location.Length > 0)
            {
                string[] usAddress = userInfo.Location.Split(',');
                switch (usAddress.Length)
                {
                    case 1:
                        city = usAddress[0];
                        break;
                    case 2:
                        city = usAddress[0];
                        state = usAddress[1];
                        break;
                    default:
                        break;
                }
            }

            //create company object
            DevelopmentCompany company = new DevelopmentCompany();
            company.AnnualRevenue = 1;
            company.Budget = 1;
            company.City = city;
            company.ClientPlatformId = 1;
            company.CompanyAddressI = "";
            company.CompanyAddressII = "";
            company.CompanyName = userInfo.DisplayName + " LLC";
            company.ContactsEmail = userInfo.Email;
            company.ContactsFirstName = userInfo.FirstName;
            company.ContactsLastName = userInfo.LastName;
            company.ContactsPhone = "";
            company.CountryId = userInfo.Country;
            company.CurrentTeamSize = 1;
            company.DateSubmitted = DateTime.Now;
            company.ProjectDescription = "n/a";
            company.ProjectedTeamSize = 1;
            company.ProjectName = "n/a";
            company.ProjectStartdateId = 1;
            company.ProjectStatusId = 1;
            company.Province = "n/a";
            company.StateCode = state;
            company.VworldInvolvmentId = 1;
            company.WebsiteURL = "n/a";
            company.ZipCode = userInfo.ZipCode;
            company.CompanyId = -1;
            company.AuthorizedTester = true;
            company.Approved = true;

            //attempt insert and return company id
            return InsertDevelopmentCompany(company);;
        }


        /// <summary>
        /// Get a specific game developer.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DevelopmentCompany GetDevelopmentCompany(int companyId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            DevelopmentCompany company = developmentCompanyDao.GetDevelopmentCompany(companyId);

            return company;
        }

        /// <summary>
        /// Get a development company by a game developers id.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DevelopmentCompany GetCompanyByDeveloperId(int gameDeveloperId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            DevelopmentCompany company = developmentCompanyDao.GetCompanyByDeveloperId(gameDeveloperId);

            return company;
        }

        /// <summary>
        /// Gets a list of users
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<DevelopmentCompany> GetDevelopmentCompanies(int companyId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return developmentCompanyDao.GetDevelopmentCompanies(companyId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Insert Game Developer
        /// </summary>
        /// <returns>company Id (int)</returns>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertDevelopmentCompany(DevelopmentCompany company)
        {
            // TODO: add access security here..
            // TODO: add argument validation here.
            //create new company
            int devCompanyId = developmentCompanyDao.InsertDevelopmentCompany(company);

            //create default company roles
            CreateDefaultCompanyRoles(devCompanyId);

            return devCompanyId;
        }

        /// <summary>
        /// Insert Game Developer
        /// </summary>
        /// <returns>company Id (int)</returns>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public void CreateDefaultCompanyRoles(int companyId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here.
            //create new company
            developmentCompanyDao.CreateDefaultCompanyRoles(companyId);
        }
        
        /// <summary>
        /// Gets virtual world involvment levels
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetVWorldInvolvment()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return developmentCompanyDao.GetVWorldInvolvment();
        }

        /// <summary>
        /// Gets project start date options
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetProjectStartDates()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return developmentCompanyDao.GetProjectStartDates();
        }


        /// <summary>
        /// Gets client platform options
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetClientPlatforms()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return developmentCompanyDao.GetClientPlatforms();
        }

        /// <summary>
        /// Gets project status options
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetProjectStatus()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return developmentCompanyDao.GetProjectStatus();
        }

        /// <summary>
        /// Gets all roles for the supplied company status options
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<SiteRole> GetCompanyRoles(int companyId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return developmentCompanyDao.GetCompaniesRoles(companyId);
        }


    }
}
