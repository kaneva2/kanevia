///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using log4net;
using Facebook;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject (true)]
    public class SocialFacade
    {
        private ISocial socialFB = new Facebook ();

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        /// <summary>
        /// Get the current users facebook profile.
        /// </summary>
        public FacebookUser GetCurrentUser (string accessToken, out bool isError, out string errMsg)
        {
            return socialFB.GetCurrentUser (accessToken, out isError, out errMsg);   
        }

        public FacebookFriendList GetCurrentUsersFriends (string accessToken, out bool isError, out string errMsg)
        {
            return socialFB.GetCurrentUsersFriends (accessToken, out isError, out errMsg);
        }

        public void PostToCurrentUsersWall (string accessToken, string msg, out bool isError, out string errMsg)
        {
            socialFB.PostToCurrentUsersWall (accessToken, msg, out isError, out errMsg);
        }
    }
}
