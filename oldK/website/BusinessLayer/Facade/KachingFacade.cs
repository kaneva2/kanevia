///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data;
using System.ComponentModel;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class KachingFacade
    {
        private IKachingDao kachingDao = DataAccess.KachingDao;

        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetLeaderboardDay(string sortOrder, int numResults)
        {
            return kachingDao.GetLeaderboardDay(sortOrder, numResults);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetLeaderboardWeek(string sortOrder, int numResults)
        {
            return kachingDao.GetLeaderboardWeek(sortOrder, numResults);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetLeaderboardAllTime(string sortOrder, int numResults)
        {
            return kachingDao.GetLeaderboardAllTime(sortOrder, numResults);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetUserStats(DateTime dtStartDate, DateTime dtEndDate, int userId)
        {
            return kachingDao.GetUserStats(dtStartDate, dtEndDate, userId);
        }


    }
}
