///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data;
using System.ComponentModel;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;



namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class MarketingFacade
    {
        private IMarketingDao marketingDao = DataAccess.MarketingDao;

        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetShareLeaderboard(string sortOrder, int numResults)
        {
            return marketingDao.GetShareLeaderboard(sortOrder, numResults);
        }

        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateShareView(int userId)
        {
            return marketingDao.UpdateShareView(userId);
        }

        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateShareClick(int userId)
        {
            return marketingDao.UpdateShareClick(userId);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddReferral(int referrerUserId, int referredUserId)
        {
            return marketingDao.AddReferral(referrerUserId, referredUserId);
        }

        [DataObjectMethod(DataObjectMethodType.Update)]
        public int CompleteReferral(int referredUserId)
        {
            // find out if it looks like they are potentially cheating the system.
            if (countIps(referredUserId) < 10)
            {
                // Reward the Referrer.
                return marketingDao.CompleteReferral(referredUserId);
            }
            else
            {
                return 0;
            }            
        }


        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetReferrer(int referredUserId)
        {
            return marketingDao.GetReferrer(referredUserId);
        }


        [DataObjectMethod(DataObjectMethodType.Select)]
        public int countIps(int referredUserId)
        {
            return marketingDao.countIps(referredUserId);
        }
        
    }
}