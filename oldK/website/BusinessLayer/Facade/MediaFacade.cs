///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;
//using Kaneva.Framework.Transactions;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class MediaFacade
    {
        private IMediaDao mediaDao = DataAccess.MediaDao;

        /// <summary>
        /// Gets the indicated asset
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Asset GetAsset(int assetId)
        {
            return GetAsset(assetId, 0);
        }

        /// <summary>
        /// Gets the indicated asset owned the the provide user
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Asset GetAsset(int assetId, int userId)
        {
            return mediaDao.GetAsset(assetId, userId);
        }

        /// <summary>
        /// Gets the indicated asset from the master db
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Asset GetNewAssetInformation(int assetId)
        {
            return mediaDao.GetNewAssetInformation(assetId);
        }

        /// <summary>
        /// Gets the indicated categoryid
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetAssetCategoryIdByName(int assetTypeId, string catName)
        {
            return mediaDao.GetAssetCategoryIdByName(assetTypeId, catName);
        }

        /// <summary>
        /// Updates the restriction level on an asset
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateAssetRestriction(int assetId, int assetRatingId, bool isMature)
        {
            //change assets restriction
            mediaDao.UpdateAssetRestriction(assetId, assetRatingId, isMature);

            //update search Boxes
            mediaDao.UpdateAssetInSearch(assetId);
        }

        /// <summary>
        /// "Deletes" an asset
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteAsset(int assetId, int userId, bool bPerminent)
        {
            //find asset
            Asset asset = GetAsset(assetId);

            // Was the asset found?
            if ((asset == null) || (asset.OwnerId <= 0))
            {
                return 1;
            }
            else
            {
                // Make sure they are an admin or moderator of the community to delete the asset
                // NO, now rule is only asset owner may delete it!!!! Moderators may only remove it.
                if (! (new UserFacade()).HasAssetDeleteRights(asset.OwnerId, userId))
                {
                    return 2;
                }
            }

            //delete the asset
            if (bPerminent)
            {
                mediaDao.DeleteAsset(asset);
            }
            else
            {
                mediaDao.UpdateAssetStatus(asset, (int)Asset.eASSET_STATUS.DELETED);
            }

            //delete asset from all channels
            mediaDao.DeleteAssetFromChannel(asset);

            //delete asset from search
            mediaDao.DeleteAssetFromSearch(asset);

            return 1;

        }

        /// <summary>
        /// UpdateMediaViews
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateMediaViews(int assetId, string userIpAddress, int userId)
        {
            return mediaDao.UpdateMediaViews(assetId, userIpAddress, userId);
        }

        /// <summary>
        /// GetUserViewHistory
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<AssetView> GetUserViewHistory(int userId, string orderBy, int pageNumber, int pageSize)
        {
            return mediaDao.GetUserViewHistory(userId, orderBy, pageNumber, pageSize);
        }

        /// <summary>
        /// ShareAsset
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public void ShareAsset(int assetId, int fromUserId, string fromIp, string toEmail, int toUserId)
        {
            mediaDao.ShareAsset(assetId, fromUserId, fromIp, toEmail, toUserId);
        }

        /// <summary>
        /// SharedAssetClicked
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void SharedAssetClicked(string keyValue)
        {
            mediaDao.SharedAssetClicked(keyValue);
        }

        /// <summary>
        /// SearchMedia
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Asset> SearchMedia(bool bGetMature, int assetTypeId, bool bGetPrivate, bool bThumbnailRequired, string searchString,
            string categories, int pastDays, string orderBy, int pageNumber, int pageSize, bool onlyAccessPass, List<int> iAssetList)
        {
            return mediaDao.SearchMedia(bGetMature, assetTypeId, bGetPrivate, bThumbnailRequired, searchString,
            categories, pastDays, orderBy, pageNumber, pageSize, onlyAccessPass, iAssetList);
        }

        /// <summary>
        /// SearchMedia
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Asset> SearchMedia(bool bGetMature, List<int> assetTypeId, bool bGetPrivate, bool bThumbnailRequired, string searchString,
            string categories, int pastDays, string orderBy, int pageNumber, int pageSize, bool onlyAccessPass, List<int> iAssetList)
        {
            return mediaDao.SearchMedia(bGetMature, assetTypeId, bGetPrivate, bThumbnailRequired, searchString,
            categories, pastDays, orderBy, pageNumber, pageSize, onlyAccessPass, iAssetList);
        }

        /// <summary>
        /// SearchMedia
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Asset> BrowseMedia(string sphinxIndex, bool bGetMature, int assetTypeId, bool bGetPrivate, bool bThumbnailRequired,
            string categories, int pastDays, string orderBy, int pageNumber, int pageSize, bool onlyAccessPass)
        {
            return mediaDao.BrowseMedia(sphinxIndex, bGetMature, assetTypeId, bGetPrivate, bThumbnailRequired,
            categories, pastDays, orderBy, pageNumber, pageSize, onlyAccessPass);
        }
    
        /// <summary>
        /// GetAccessibleAssetsInCommunity
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<Asset> GetAccessibleAssetsInCommunity (int communityId, int assetGroupId, bool bGetMature,
            int assetTypeId, int userId, string filter, string orderBy, int pageNumber, int pageSize)
        {
            return mediaDao.GetAccessibleAssetsInCommunity (communityId, assetGroupId, bGetMature,
                assetTypeId, userId, filter, orderBy, pageNumber, pageSize);
        }

        /// <summary>
        /// GetAssetGroups
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="filter"></param>
        /// <param name="orderby"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public PagedList<AssetGroup> GetAssetGroups(int channelId, string filter, string orderby, int pageNumber, int pageSize)
        {
            return mediaDao.GetAssetGroups(channelId, filter, orderby, pageNumber, pageSize);
        }

      
        /// <summary>
        /// GetAssetGroups
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="assetGroupIds"></param>
        /// <returns></returns>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<AssetGroup> GetAssetGroups(int channelId, string assetGroupIds)
        {
            return mediaDao.GetAssetGroups(channelId, assetGroupIds);
        }
  
        /// <summary>
        /// IsAssetInGroup
        /// </summary>
        /// <param name="assetGroupId"></param>
        /// <param name="assetId"></param>
        /// <returns></returns>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public bool IsAssetInGroup(int assetGroupId, int assetId)
        {
            return mediaDao.IsAssetInGroup(assetGroupId, assetId);
        }

        /// <summary>
        /// InsertAssetGroup
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="groupName"></param>
        /// <param name="groupDescription"></param>
        /// <returns></returns>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int InsertAssetGroup(int channelId, string groupName, string groupDescription, int shuffle)
        {
            return mediaDao.InsertAssetGroup(channelId, groupName, groupDescription, shuffle);
        }

        /// <summary>
        /// DeleteAssetGroup
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="assetGroupId"></param>
        /// <returns></returns>
        [DataObjectMethod (DataObjectMethodType.Delete)]
        public int DeleteAssetGroup(int channelId, int assetGroupId)
        {
            return mediaDao.DeleteAssetGroup(channelId, assetGroupId);

        }
 
        /// <summary>
        /// DeleteAllAssetsFromGroup
        /// </summary>
        /// <param name="assetGroupId"></param>
        /// <returns></returns>
        [DataObjectMethod (DataObjectMethodType.Delete)]
        public int DeleteAllAssetsFromGroup(int assetGroupId)
        {
            return mediaDao.DeleteAllAssetsFromGroup(assetGroupId);
        }

        /// <summary>
        /// GetAssetGroup
        /// </summary>
        /// <param name="assetGroupId"></param>
        /// <param name="channelId"></param>
        /// <returns></returns>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public AssetGroup GetAssetGroup(int assetGroupId, int channelId)
        {
            return mediaDao.GetAssetGroup(assetGroupId, channelId);

        }

        /// <summary>
        /// GetGroupCategoryAssetGroups
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<AssetGroup> GetGroupCategoryAssetGroups(int groupCategoryId, int page, int pageSize)
        {
            return mediaDao.GetGroupCategoryAssetGroups(groupCategoryId, page, pageSize);
        }
 
        /// <summary>
        /// InsertAssetInGroup
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="assetGroupId"></param>
        /// <param name="assetId"></param>
        /// <returns></returns>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int InsertAssetInGroup(int channelId, int assetGroupId, int assetId)
        {
            return mediaDao.InsertAssetInGroup(channelId, assetGroupId, assetId);
        }

        /// <summary>
        /// UpdateAssetGroupAssetSortOrder
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateAssetGroupAssetSortOrder(int assetId, int sortOrder, int assetGroupId)
        {
            return mediaDao.UpdateAssetGroupAssetSortOrder(assetId, sortOrder, assetGroupId);
        }

        /// <summary>
        /// MoveAssetToTopOfGroup
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int MoveAssetToTopOfGroup(int assetGroupId, int assetId)
        {
            return mediaDao.MoveAssetToTopOfGroup(assetGroupId, assetId);
        }

        /// <summary>
        /// MoveAssetToTopOfGroup
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int MoveAssetToBottomOfGroup(int assetGroupId, int assetId)
        {
            return mediaDao.MoveAssetToBottomOfGroup(assetGroupId, assetId);
        }
        
        /// <summary>
        /// RemoveAssetFromGroup
        /// </summary>
        /// <param name="assetGroupId"></param>
        /// <param name="assetId"></param>
        /// <returns></returns>
        [DataObjectMethod (DataObjectMethodType.Delete)]
        public int RemoveAssetFromGroup(int channelId, int assetGroupId, int assetId)
        {
           // Make sure it is ok
            AssetGroup assetGroup = mediaDao.GetAssetGroup(assetGroupId, channelId);

            if (!assetGroup.ChannelId.Equals(channelId))
            {
                return -1;
            } 

            return mediaDao.RemoveAssetFromGroup(assetGroupId, assetId);
        }

        /// <summary>
        /// RemoveAssetFromAllGroups
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        [DataObjectMethod (DataObjectMethodType.Delete)]
        public int RemoveAssetFromAllGroups(int assetId)
        {
            List<int> assetGroupIds = mediaDao.GetGroupIdsWithAsset(assetId);
            int count = mediaDao.RemoveAssetFromAllGroups(assetId);
            if (count > 0 && assetGroupIds.Count > 0)
            {
                foreach(int assetGroupId in assetGroupIds)
                {
                    (new RemoteEventSender()).BroadcastItemChangeEvent(RemoteEventSender.ObjectType.MediaPlaylist, RemoteEventSender.ChangeType.UpdateObject, assetGroupId);
                }
            }
            return count;
        }
  
        /// <summary>
        /// RemoveAssetFromGroupsByChannel
        /// </summary>
        /// <param name="channelId"></param>
        /// <param name="assetId"></param>
        /// <returns></returns>
        [DataObjectMethod (DataObjectMethodType.Delete)]
        public int RemoveAssetFromGroupsByChannel(int channelId, int assetId)
        {
            List<int> assetGroupIds = mediaDao.GetGroupIdsWithAssetByChannel(assetId, channelId);
            int count = mediaDao.RemoveAssetFromGroupsByChannel(channelId, assetId);
            if (count > 0 && assetGroupIds.Count > 0)
            {
                foreach(int assetGroupId in assetGroupIds)
                {
                    (new RemoteEventSender()).BroadcastItemChangeEvent(RemoteEventSender.ObjectType.MediaPlaylist, RemoteEventSender.ChangeType.UpdateObject, assetGroupId);
                }
            }
            return count;
        }

        /// <summary>
        /// GetWokPlaylist
        /// </summary>
        /// <param name="assetGroupId"></param>
        /// <returns></returns>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<AssetGroupAsset> GetWokPlaylist(int channelId, int assetGroupId)
        {
            AssetGroup assetGroup = mediaDao.GetAssetGroup(assetGroupId, channelId);

            if (!assetGroup.ChannelId.Equals(channelId))
            {
                return new PagedList<AssetGroupAsset>();
            }

            return mediaDao.GetWokPlaylist(assetGroupId);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<int> GetUserUploadedAssetIds(int userId)
        {
            return mediaDao.GetUserUploadedAssetIds(userId);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Asset> GetUserUploadedAssets(int userId, List<int> assetTypeIds, string orderBy, int pageNum, int itemsPerPage)
        {
            return mediaDao.GetUserUploadedAssets(userId, assetTypeIds, orderBy, pageNum, itemsPerPage);
        }
    }
}
