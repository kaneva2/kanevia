///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects.API;
using System.Data;
//using Kaneva.Framework.Transactions;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class CommunityFacade
    {
        private ICommunityDao communityDao = DataAccess.CommunityDao;
        private IWorldTemplateDao worldTemplateDao = DataAccess.WorldTemplateDao;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Community GetCommunity(int communityId)
        {
            return GetCommunity (communityId, false);
        }
        /// <summary>
        /// GetCommunity
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Community GetCommunity (int communityId, bool includeTemplateId)
        {
            //get the community
            Community community = communityDao.GetCommunity(communityId);

            //get community stats
            community.Stats = (CommunityStats)new ProxyForCommunityStats(community);
            
            //get community preferences
            community.Preferences = (CommunityPreferences)new ProxyForCommunityPreferences(community);

            //get community 3D App
            community.WOK3App = (WOK3DApp) new ProxyForCommunityWOK3DApp (community.CommunityId);

            // This was added so we can use to determine if game world
            if (includeTemplateId)
            {
                community.TemplateId = (uint)worldTemplateDao.GetWorldTemplateId(community.CommunityId);
            }

            return community;
        }

        /// <summary>
        /// GetWorldTemplateId
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetWorldTemplateId(int communityId)
        {
            return worldTemplateDao.GetWorldTemplateId(communityId);
        }

        /// <summary>
        /// SearchCommunities
        /// </summary>
        /// <returns></returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Community> SearchCommunities(bool onlyAccessPass, bool bGetMature, string searchString,
             bool bOnlyWithPhotos, string country, int pastDays, int[] communityTypeIds,
             string orderBy, int pageNumber, int pageSize, int iPlaceTypeId)
        {
            return communityDao.SearchCommunities(onlyAccessPass, bGetMature, searchString, bOnlyWithPhotos, country, pastDays,
                communityTypeIds, orderBy, pageNumber, pageSize, iPlaceTypeId);
        }

        /// <summary>
        /// GetUserCommunities - gets users communities - defaults to general community
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Community> GetUserCommunities(int userId, string orderBy, string filter, int pageNumber, int pageSize)
        {
            int[] communityTypeIds = { Convert.ToInt32 (CommunityType.APP_3D), Convert.ToInt32 (CommunityType.COMMUNITY), Convert.ToInt32 (CommunityType.HOME) };
            return GetUserCommunities (userId, communityTypeIds, orderBy, filter, pageNumber, pageSize);
        }

		/// <summary>
		/// GetUserCommunities - gets users communities allowing you to filter by community type
		/// </summary>
		[DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Community> GetUserCommunities (int userId, int[] communityTypeIds, string orderBy, string filter, int pageNumber, int pageSize)
		{
            PagedList<Community> communities = communityDao.GetCommunities (userId, communityTypeIds, orderBy, filter, pageNumber, pageSize);

			foreach (Community community in communities)
			{
				//get community preferences
				community.Preferences = (CommunityPreferences)new ProxyForCommunityPreferences(community);
			}

			return communities;
		}

        /// <summary>
        /// GetUserCommunities - gets users communities allowing you to filter by community type, and with 3d Appp info
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Community> GetUserCommunities (int userId, int[] communityTypeIds, string orderBy, string filter, int pageNumber, int pageSize, bool with3dAppInfo)
        {
            return GetUserCommunities (userId, communityTypeIds, orderBy, filter, pageNumber, pageSize, with3dAppInfo, "");
        }
				
		/// <summary>
        /// GetUserCommunities - gets users communities allowing you to filter by community type, and with 3d Appp info
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Community> GetUserCommunities (int userId, int[] communityTypeIds, string orderBy, string filter, int pageNumber, int pageSize, bool with3dAppInfo, string nameFilter)
        {
            PagedList<Community> communities = communityDao.GetCommunities (userId, communityTypeIds, orderBy, filter, pageNumber, pageSize, with3dAppInfo, nameFilter);

            foreach (Community community in communities)
            {
                //get community preferences
                community.Preferences = (CommunityPreferences)new ProxyForCommunityPreferences(community);
            }

            return communities;
        }

        /// <summary>
        /// GetApartmentCommunity
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Community GetApartmentCommunity(int zoneInstanceId)
        {
            return communityDao.GetApartmentCommunity(zoneInstanceId);
        }

        /// <summary>
        /// GetCommunityMember
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public CommunityMember GetCommunityMember(int communityId, int userId)
        {
            return communityDao.GetCommunityMember(communityId, userId);
        }

        /// <summary>
        /// GetCommunityMembers
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<CommunityMember> GetCommunityMembers(int communityId, UInt32 communityMemberStatusId, bool bIncludeSuspended,
            bool bIncludePending, bool bIncludeDeleted, bool bIncludeRejected, string filter, string orderby,
            int pageNumber, int pageSize, bool bShowMature, string userNameFilter)
        {
            return GetCommunityMembers (communityId, communityMemberStatusId, bIncludeSuspended,
            bIncludePending, bIncludeDeleted, bIncludeRejected, filter, orderby, pageNumber, pageSize, bShowMature, userNameFilter, false);
        }

        /// <summary>
        /// GetCommunityMembers
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<CommunityMember> GetCommunityMembers (int communityId, UInt32 communityMemberStatusId, bool bIncludeSuspended,
            bool bIncludePending, bool bIncludeDeleted, bool bIncludeRejected, string filter, string orderby,
            int pageNumber, int pageSize, bool bShowMature, string userNameFilter, bool includeNotificationPref)
        {
            return communityDao.GetCommunityMembers (communityId, communityMemberStatusId, bIncludeSuspended,
            bIncludePending, bIncludeDeleted, bIncludeRejected, filter, orderby, pageNumber, pageSize, bShowMature, userNameFilter, includeNotificationPref);
        }

        /// <summary>
        /// GetNumberOfCommunties - gets the number of communities a user has of the indicated status
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetNumberOfCommunities(int creatorId, int statusId)
        {
            return communityDao.GetNumberOfCommunities(creatorId, statusId, new int[0]);
        }

        /// <summary>
        /// GetNumberOfCommunties - gets the number of communities a user has of the indicated status
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetNumberOfCommunities(int creatorId, int statusId, int[] communityTypeIds)
        {
            return communityDao.GetNumberOfCommunities(creatorId, statusId, communityTypeIds);
        }

        /// <summary>
        /// GetNumberOf3DApps - gets the number of 3DApps a user has of the indicated status
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetNumberOf3DApps(int creatorId, int statusId)
        {
            return communityDao.GetNumberOf3DApps(creatorId, statusId);
        }

        /// <summary>
        /// updates just the URL - used for older functions in older pages
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateCommunityURL(int communityId, string url)
        {
            int result = 1; //indicates name is taken already (default)

            //check to see if URL is already taken
            if (!IsCommunityURLTaken(url, communityId))
            {
                communityDao.UpdateCommunityURL(communityId, url);
                result = 0;
            }

            return result;
        }

        /// <summary>
        /// Is this community URL already taken
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool IsCommunityURLTaken(string url, int communityId)
        {
            return (communityDao.IsCommunityURLTaken(url, communityId) > 0);
        }

        /// <summary>
        /// Is this community name already taken
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool IsCommunityNameTaken(bool isPersonal, string communityName, int communityId)
        {
            return (communityDao.IsCommunityNameTaken(isPersonal, communityName, communityId) > 0);
        }

        /// <summary>
        /// Suggests a new community name if the desired name is already taken.
        /// </summary>
        /// <param name="originalName"></param>
        /// <returns></returns>
        public string GetSuggestedName(string originalName)
        {
            int nameAddition = 1;

            while (IsCommunityNameTaken(false, originalName + nameAddition.ToString().Trim().Replace(" ", ""), 0))
            {
                nameAddition += 1;
            }

            return originalName + nameAddition.ToString();
        }

        /// <summary>
        /// Is the user a communtiy moderator
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool IsCommunityModerator(int communityId, int userId)
        {
            return IsCommunityModerator(communityId, userId, true);
        }

        /// <summary>
        /// Is the user a communtiy moderator
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool IsCommunityModerator(int communityId, int userId, bool bAlwaysTrueIfAdmin)
        {
            UserFacade userFacade = new UserFacade();

            // Administrator always has rights to access
            if (bAlwaysTrueIfAdmin && userFacade.IsUserAdministrator())
            {
                return true;
            }

            CommunityMember communtiyMember = GetCommunityMember(communityId, userId);

            // Owner is also always a moderator
            if (communtiyMember.StatusId.Equals((UInt32)CommunityMember.CommunityMemberStatus.ACTIVE) && (communtiyMember.AccountTypeId.Equals((int)CommunityMember.CommunityMemberAccountType.MODERATOR) || communtiyMember.AccountTypeId.Equals((int)CommunityMember.CommunityMemberAccountType.OWNER)))
            {
                return true;
            }

            return (false);
        }

        /// <summary>
        /// Is the community valid / does it exist
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        bool IsCommunityValid(int communityId)
        {
            return communityDao.IsCommunityValid(communityId);
        }

        /// <summary>
        /// IsCommunityOwner - checks to see if the user is te owner of the community
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool IsCommunityOwner(int communityId, int userId)
        {
            bool isCommunityOwner = false;

            CommunityMember communityMember = GetCommunityMember(communityId, userId);

            if (!communityMember.UserId.Equals(0))
            {
                int accountTypeID = communityMember.AccountTypeId;
                uint statusId = communityMember.StatusId;

                if (statusId.Equals((UInt32)CommunityMember.CommunityMemberStatus.ACTIVE) && accountTypeID.Equals((int)CommunityMember.CommunityMemberAccountType.OWNER))
                {
                    isCommunityOwner = true;
                }
            }

            return isCommunityOwner;
        }

        /// <summary>
        /// GetCommunityCategories
        /// </summary>
        /// <returns></returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<CommunityCategory> GetCommunityCategories()
        {
            return communityDao.GetCommunityCategories();
        }

        /// <summary>
        /// GetCommunityCategories
        /// </summary>
        /// <returns></returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<CommunityCategory> GetCommunityCategories(int communityId)
        {
            return communityDao.GetCommunityCategories(communityId);
        }

        public int InsertCommunityCategory(int communityId, int categoryId)
        {
            return communityDao.InsertCommunityCategory(communityId, categoryId);
        }

        public int DeleteCommunityCategories(int communityId)
        {
            return communityDao.DeleteCommunityCategories(communityId);
        }

        ///// <summary>
        ///// GetCommunityTabs
        ///// </summary>
        ///// <returns></returns>
        //[DataObjectMethod(DataObjectMethodType.Select)]
        //public IList<TabDisplay> GetCommunityTabsData(int communityId)
        //{
        //    return communityDao.GetTabsData(communityId, false);
        //}

        ///// <summary>
        ///// GetCommunityTabs
        ///// </summary>
        ///// <returns></returns>
        //[DataObjectMethod(DataObjectMethodType.Select)]
        //public IList<TabDisplay> GetProfileTabsData(int communityId)
        //{
        //    return communityDao.GetTabsData(communityId, true);
        //}

        /// <summary>
        /// GetCommunityPlaceTypes
        /// </summary>
        /// <returns></returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<CommunityPlaceType> GetCommunityPlaceTypes()
        {
            return communityDao.GetCommunityPlaceTypes();
        }

        /// <summary>
        /// Get the tabs to display on community page
        /// </summary>
        public List<CommunityTab> GetCommunityTabs (int communityId, int communityTypeId)
        {
            return communityDao.GetCommunityTabs (communityId, communityTypeId);
        }

        /// <summary>
        /// Get community tabs available for a community type
        /// </summary>
        public List<CommunityTab> GetCommunityTabsByCommunityType (int communityTypeId)
        {
            return communityDao.GetCommunityTabsByCommunityType (communityTypeId);
        }

        /// <summary>
        /// Updates community tab settings for a community
        /// </summary>
        public int UpdateCommunityTabs (int tabId, int communityId, int communityTypeId, bool isSelected, bool isDefault)
        {
            return communityDao.UpdateCommunityTabs (tabId, communityId, communityTypeId, isSelected, isDefault);
        }

        /// <summary>
        /// Gets that about text/html for a community
        /// </summary>
        public string GetCommunityAboutText (int communityId)
        {
            return communityDao.GetCommunityAboutText (communityId);
        }

        /// <summary>
        /// Updates community about text/html
        /// </summary>
        public int UpdateCommunityAboutText (int communityId, string aboutText)
        {
            return communityDao.UpdateCommunityAboutText (communityId, aboutText);
        }

        /// <summary>
        /// Get a 3D home or hangout
        /// </summary>
        public WOK3DPlace Get3DPlace (int zoneInstanceId, int zoneType)
        {
            return communityDao.Get3DPlace (zoneInstanceId, zoneType);
        }

        public WOK3DPlace Get3DPlace(int channelZoneId)
        {
            return communityDao.Get3DPlace(channelZoneId);
        }

        public WOK3DPlace Get3DPlace(string worldName)
        {
            return communityDao.Get3DPlace(worldName);
        }
        
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool IsUserWOK3DPlaceOwner(int userId, int zoneIndex, int zoneInstanceId)
        {
            return communityDao.IsUserWOK3DPlaceOwner(userId, zoneIndex, zoneInstanceId);
        }

        /// <summary>
        /// Get community Passes
        /// </summary>
        /// <returns></returns>
        public DataTable GetCommunityPasses(int communityId, int communityType)
        {
            return communityDao.GetCommunityPasses(communityId, communityType);
        }

        /// <summary>
        /// Gets the passgroups for a world given the full set of world ids
        /// </summary>
        public DataTable GetWorldPasses(int wokGameId, int gameId, int zoneInstanceId, int zoneType)
        {
            return communityDao.GetWorldPasses(wokGameId, gameId, zoneInstanceId, zoneType);
        }

        /// <summary>
        /// Add community Pass
        /// </summary>
        /// <returns></returns>
        public bool AddCommunityPass(int userId, bool isAdmin, int communityId, int communityType, int passId)
        {
            return communityDao.AddCommunityPass(userId, isAdmin, communityId, communityType, passId);
        }

        /// <summary>
        /// Delete community Pass
        /// </summary>
        /// <returns></returns>
        public bool DeleteCommunityPass(int userId, bool isAdmin, int communityId, int communityType, int passId)
        {
            return communityDao.DeleteCommunityPass(userId, isAdmin, communityId, communityType, passId);
        }

        public Community GetPersonalCommunityFromPlayerId (int playerId)
        {
            return communityDao.GetPersonalCommunityFromPlayerId (playerId);
        }

        /// <summary>
        /// ShareCommunity
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public void ShareCommunity(int communityId, int userId, int sentUserId, string email)
        {
            communityDao.ShareCommunity(communityId, userId, sentUserId, email);
        }

        /// <summary>
        /// UpdateCommunity - takes community object as parameter
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertCommunity(Community community)
        {
            //add the community
            int result = communityDao.InsertCommunity(community);

            //update the community id
            community.CommunityId = result;

            //add the community preference 
            community.Preferences = new CommunityPreferences();
            community.Preferences.CommunityId = result;
            communityDao.InsertCommunityPreferences(community);

            return result;
        }

        /// <summary>
        /// InsertCommunityMember - inserts a user into the indicated community
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertCommunityMember(CommunityMember communityMember)
        {
            return communityDao.InsertCommunityMember(communityMember);
        }

        /// <summary>
        /// UpdateChannelViews
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateChannelViews(int userId, int communityId, bool browseAnon, string ipAddress)
        {
            communityDao.UpdateChannelViews(userId, communityId, browseAnon, ipAddress);
        }

        /// <summary>
        /// UpdateCommunity 
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateCommunity(int communityId, int ownerId, bool IsPersonal, string thumbnailPath, string thumbnailType, bool hasThumbnail)
        {
            return communityDao.UpdateCommunity(communityId, ownerId, IsPersonal, thumbnailPath, thumbnailType, hasThumbnail);
        }

        /// <summary>
        /// UpdateCommunityThumb 
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateCommunityThumb(int communityId, string thumbnailPath, string dbColumn)
        {
            return communityDao.UpdateCommunityThumb(communityId, thumbnailPath, dbColumn);
        }

        /// <summary>
        /// UpdateCommunity - takes community object as parameter
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateCommunity(Community community)
        {
            //update the community
            int result = communityDao.UpdateCommunity(community); 

            //update the community preferences
            communityDao.UpdateCommunityPreferences(community);

            return result;
        }

        /// <summary>
        /// DeleteAllCommunityMembers - deletes all members from a community
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteAllCommunityMembers(int communityId)
        {
            return communityDao.DeleteAllCommunityMembers(communityId);
        }

        /// <summary>
        /// DeleteAllCommunityMembers - deletes all members from a community
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteAllCommunityAssets(int communityId)
        {
            return communityDao.DeleteAllCommunityAssets(communityId);
        }

        /// <summary>
        /// DeleteCommunityRecoverable - maintains all members, assets, etc.
        /// in a community. Meant to be used in conjunction with "deleting" 
        /// a user profile/homeworld.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int DeleteCommunityRecoverable(int communityId)
        {
            return communityDao.DeleteCommunityRecoverable(communityId);
        }

        /// <summary>
        /// RestoreDeletedCommunity - restores a community that was marked
        /// "deleted" as a result of a call to "DeleteCommunityRecoverable".
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int RestoreDeletedCommunity(int communityId)
        {
            return communityDao.RestoreDeletedCommunity(communityId);
        }

        /// <summary>
        /// DeleteCommunityThumbs - Delete All thumbnail url/paths
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteCommunityThumbs(int communityId)
        {
            return communityDao.DeleteCommunityThumbs(communityId);
        }

        /// <summary>
        /// IsActiveCommunityMember - Is user a member of community
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Delete)]
        public bool IsActiveCommunityMember (int communityId, int userId)
        {
            return communityDao.IsActiveCommunityMember (communityId, userId);
        }

        /// <summary>
        /// GetCommunityGameId
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetCommunityGameId(int communityId)
        {
            return communityDao.GetCommunityGameId(communityId);
        }

        /// <summary>
        /// GetCommunityIdFromGameId
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public int GetCommunityIdFromGameId (int gameId)
        {
            return communityDao.GetCommunityIdFromGameId (gameId);
        }

        /// <summary>
        /// GetCommunityIdFromName
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public int GetCommunityIdFromName(string communityName, bool isPersonal)
        {
            return communityDao.GetCommunityIdFromName(communityName, isPersonal);
        }

        /// <summary>
        /// GetCommunityIdFromZoneIndex
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public uint GetCommunityIdFromFaux3DAppZoneIndex (int zoneIndex)
        {
            return communityDao.GetCommunityIdFromFaux3DAppZoneIndex (zoneIndex);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetWorldOwnerFromWorldIds(int wokGameId, int gameId, int zoneIndex, int zoneInstanceId, int zoneType)
        {
            return communityDao.GetWorldOwnerFromWorldIds(wokGameId, gameId, zoneIndex, zoneInstanceId, zoneType);
        }

        /// <summary>
        /// GetCommunityIdFromWorldIds
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetCommunityIdFromWorldIds(int wokGameId, int gameId, int zoneIndex, int zoneInstanceId, int zoneType)
        {
            return communityDao.GetCommunityIdFromWorldIds(wokGameId, gameId, zoneIndex, zoneInstanceId, zoneType);
        }

        /// <summary>
        /// UpdateCommunityMember status.
        /// Replaces 'UpdateCommunityMember(int communityId, int userId, UInt32 statusId)'
        /// function in /KanevaFramework/communities/CommunityUtility.cs.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateCommunityMember(int communityId, int userId, UInt32 statusId)
        {
            int result = communityDao.UpdateCommunityMember(communityId, userId, statusId);

            // Handle case if they are moderator and are being deleted or locked
            if (statusId.Equals((UInt32)CommunityMember.CommunityMemberStatus.DELETED) || statusId.Equals((UInt32)CommunityMember.CommunityMemberStatus.LOCKED))
            {
                UpdateCommunityMember(communityId, userId, CommunityMember.CommunityMemberAccountType.SUBSCRIBER);
            }

            // Blast it, if necessary
            if (statusId.Equals((UInt32)CommunityMember.CommunityMemberStatus.ACTIVE))
            {
                RaveFacade raveFacade = new RaveFacade();

                if (raveFacade.GetCommunityRaveCountByUser(userId, communityId) == 0)
                {
                    RaveType raveType = raveFacade.GetRaveType(Convert.ToUInt32(RaveType.eRAVE_TYPE.SINGLE));
                    raveFacade.RaveCommunity(userId, communityId, raveType, string.Empty);
                }
            }

            return result;
        }

        /// <summary>
        /// UpdateCommunityMember status.
        /// Replaces 'UpdateCommunityMember(int communityId, int userId, int accountTypeId, string newsletter,
        ///     string allowAssetUploads, string allowForumUse)'
        /// function in /KanevaFramework/communities/CommunityUtility.cs.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateCommunityMember(int communityId, int userId, int accountTypeId, string newsletter,
            string allowAssetUploads, string allowForumUse)
        {
            return communityDao.UpdateCommunityMember(communityId, userId, accountTypeId, newsletter,
                allowAssetUploads, allowForumUse);
        }

        /// <summary>
        /// UpdateCommunityMember status.
        /// Replaces 'UpdateCommunityMember(int communityId, int userId, CommunityMember.CommunityMemberAccountType accountType)'
        /// function in /KanevaFramework/communities/CommunityUtility.cs. 
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateCommunityMember(int communityId, int userId, CommunityMember.CommunityMemberAccountType accountType)
        {
            return communityDao.UpdateCommunityMember(communityId, userId, accountType);
        }

        /// <summary>
        /// Add the given user as an active member of the given community. 
        /// Replaces functionality previously spread across /Kaneva/community/commJoin.aspx.cs 
        /// and /KanevaFramework/communities/CommunityUtility.cs.
        /// </summary>
        public void JoinCommunity(int communityId, int userId)
        {
            Community comm = GetCommunity(communityId);
            CommunityMember communityMember = GetCommunityMember(communityId, userId);
            bool redeemPacket = false;
            bool isAdmin = (new UserFacade().IsUserAdministrator());

            // Only update if valid community was found.
            if (comm.CommunityId > 0)
            {
                if (comm.IsPublic == "Y" || isAdmin)
                {
                    // They are trying to join a public
                    switch (communityMember.StatusId)
                    {
                        case 0:
                            {
                                communityMember.CommunityId = communityId;
                                communityMember.UserId = userId;
                                communityMember.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.SUBSCRIBER;
                                communityMember.Newsletter = "N";
                                communityMember.AllowAssetUploads = "N";
                                communityMember.AllowForumUse = "Y";
                                communityMember.StatusId = (UInt32)CommunityMember.CommunityMemberStatus.ACTIVE;

                                InsertCommunityMember(communityMember);


                                // Was it newly created?  Will return empty drComm for status new.
                                if (comm.CreatorId > 0 && comm.CreatorId != userId)
                                {
                                    redeemPacket = true;
                                }

                                break;
                            }
                        case ((UInt32)CommunityMember.CommunityMemberStatus.ACTIVE):
                        case ((UInt32)CommunityMember.CommunityMemberStatus.LOCKED):
                            {
                                // Don't do anything if active or locked
                                break;
                            }
                        default:
                            {
                                UpdateCommunityMember(communityId, userId, (int)CommunityMember.CommunityMemberAccountType.SUBSCRIBER, "N", "N", "Y");
                                UpdateCommunityMember(communityId, userId, (UInt32)CommunityMember.CommunityMemberStatus.ACTIVE);
                                break;
                            }
                    }
                }
                else
                {
                    // They are trying to join a private
                    switch (communityMember.StatusId)
                    {
                        case 0:
                            {
                                communityMember.CommunityId = communityId;
                                communityMember.UserId = userId;
                                communityMember.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.SUBSCRIBER;
                                communityMember.Newsletter = "N";
                                communityMember.AllowAssetUploads = "N";
                                communityMember.AllowForumUse = "Y";
                                communityMember.StatusId = (UInt32)CommunityMember.CommunityMemberStatus.PENDING;

                                InsertCommunityMember(communityMember);

                                redeemPacket = true;
                                break;
                            }
                        case ((UInt32)CommunityMember.CommunityMemberStatus.PENDING):
                        case ((UInt32)CommunityMember.CommunityMemberStatus.ACTIVE):
                        case ((UInt32)CommunityMember.CommunityMemberStatus.LOCKED):
                            {
                                // Don't do anything if Pending, Active, or Locked
                                break;
                            }
                        case ((UInt32)CommunityMember.CommunityMemberStatus.DELETED):
                        case ((UInt32)CommunityMember.CommunityMemberStatus.REJECTED):
                            {
                                UpdateCommunityMember(communityId, userId, (UInt32)CommunityMember.CommunityMemberStatus.PENDING);
                                break;
                            }
                    }
                }

                // Redeem fame packet
                if (redeemPacket)
                {
                    try
                    {
                        // Don't award fame when user is joined to his own community by the system.
                        // All users are made members of their personal channel by default
                        FameFacade fameFacade = new FameFacade();
                        fameFacade.RedeemPacketCommunityRelated(communityId, (int)PacketId.WEB_COMM_JOIN, (int)FameTypes.World);
                    }
                    catch (Exception ex)
                    {
                        m_logger.Error("Error awarding Packet WEB_COMM_JOIN, ownerId=" + comm.CreatorId.ToString() + ", communityId=" + communityId.ToString(), ex);
                    }
                }
            }
        }

        /// <summary>
        /// RemoveMemberFromAllOwnersWorlds.
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int RemoveMemberFromAllOwnersWorlds (int ownerId, int memberId)
        {
            return communityDao.RemoveMemberFromAllOwnersWorlds (ownerId, memberId);
        }

        /// <summary>
        /// InsertAPIAuth
        /// </summary>
        /// <param name="gameId"></param>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public void InsertAPIAuth(int communityId)
        {
            Community community = GetCommunity(communityId);

            string consumerKey = community.NameNoSpaces;
            string consumerSecret = Guid.NewGuid().ToString().Replace("-", "K");

            InsertAPIAuth(communityId, consumerKey, consumerSecret);
        }

        /// <summary>
        /// InsertAPIAuth
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="consumerKey"></param>
        /// <param name="consumerSecret"></param>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public void InsertAPIAuth(int communityId, string consumerKey, string consumerSecret)
        {
            communityDao.InsertAPIAuth(communityId, consumerKey, consumerSecret);
        }

        public APIAuthentication GetAPIAuth(int communityId)
        {
            return communityDao.GetAPIAuth(communityId);
        }


        /// <summary>
        /// GetAPIAuth
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public APIAuthentication GetAPIAuth(string consumerKey, string consumerSecret)
        {
            return communityDao.GetAPIAuth(consumerKey, consumerSecret);
        }

        /// <summary>
        /// GetAPIAuth
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public APIAuthentication GetAPIAuth(string consumerKey)
        {
            return communityDao.GetAPIAuth(consumerKey);
        }

        /// <summary>
        /// CanUserAccessCommunity
        /// </summary>
        public bool CanUserAccessCommunity(Community community, int userId, bool userHasAccessPass, bool userOver21, out int accessReason, out string accessMessage)
        {
            bool userHasAccess = false;
            bool isUserCommunityOwner = (userId == community.CreatorId);

            // blocking is universal, check it first
            if ((new UserFacade()).IsUserBlocked(community.CreatorId, userId))
            {
                userHasAccess = false;
                accessReason = (int)CommunityAccessReason.USER_BLOCKED;
                accessMessage = "This World is not available.";
            }
            else
            {
                if (community.IsPersonal == 1)
                {
                    //see if they are friends or not
                    bool isFriend = (new UserFacade()).AreFriends(community.CreatorId, userId);

                    //check for access pass requirement
                    if (community.IsAdult.Equals("Y") && !isUserCommunityOwner && !userOver21)
                    {
                        userHasAccess = false;
                        accessReason = (int)CommunityAccessReason.OVER_21_REQUIRED;
                        accessMessage = "You must be age 21 or older to view this profile.";
                    }
                    else if (community.IsPublic.Equals("N") && !isUserCommunityOwner && !isFriend)
                    {
                        userHasAccess = false;
                        accessReason = (int)CommunityAccessReason.FRIENDSHIP_REQUIRED;
                        accessMessage = "You must be friends with this user in order to view their profile.";
                    }
                    //check to see if community is private
                    else if (community.IsPublic.Equals("I") && !isUserCommunityOwner)
                    {
                        userHasAccess = false;
                        accessReason = (int)CommunityAccessReason.OWNER_ACCESS_ONLY;
                        accessMessage = "This profile is only accessible by the Owner.";
                    }
                    //everything is a go
                    else
                    {
                        userHasAccess = true;
                        accessReason = (int)CommunityAccessReason.ACCESS_ALLOWED;
                        accessMessage = "Access allowed.";
                    }
                }
                else
                {
                    // Check to see if the user is a member of the community
                    CommunityMember communityMember = GetCommunityMember(community.CommunityId, userId);
                    bool isMember = (communityMember.StatusId == (UInt32)CommunityMember.CommunityMemberStatus.ACTIVE);
                    bool isModerator = (communityMember.AccountTypeId == (int)CommunityMember.CommunityMemberAccountType.MODERATOR);

                    if (community.IsAdult.Equals("Y") && !isUserCommunityOwner && !userHasAccessPass)
                    {
                        userHasAccess = false;
                        accessReason = (int)CommunityAccessReason.ACCESS_PASS_REQUIRED;
                        accessMessage = "Access Pass allows you to explore content and features only " +
                            "available to an exclusive 18 and over community.";
                    }
                    //check for over 21
                    //else if (community.Over21Required && !isUserCommunityOwner && !userOver21)
                    //{
                    //    userHasAccess = false;
                    //    accessReason = (int)CommunityAccessReason.OVER_21_REQUIRED;
                    //    accessMessage = "You must be age 21 or older to view this world.";
                    //}
                    //check for user as member 
                    else if (community.IsPublic.Equals("N") && !isUserCommunityOwner && !isMember)
                    {
                        userHasAccess = false;
                        accessReason = (int)CommunityAccessReason.MEMBERSHIP_REQUIRED;
                        accessMessage = community.Name + " is members only. You must request access.";
                    }
                    //check to see if community is owner or moderator only
                    else if (community.IsPublic.Equals("I") && !isUserCommunityOwner && !isModerator)
                    {
                        userHasAccess = false;
                        accessReason = (int)CommunityAccessReason.OWNER_ACCESS_ONLY;
                        accessMessage = community.Name + " is only accessible by the Owner. Try back later.";
                    }
                    //everything is a go
                    else
                    {
                        userHasAccess = true;
                        accessReason = (int)CommunityAccessReason.ACCESS_ALLOWED;
                        accessMessage = "Access allowed.";
                    }
                }
            }

            return userHasAccess;
        }

        /// <summary>
        /// GetUserTopWorlds        
        /// </summary>
        public PagedList<Community> GetUserTopWorlds (int userId, int[] communityTypeIds, string orderBy, string filter, int pageNumber, int pageSize)
        {
            return communityDao.GetUserTopWorlds (userId, communityTypeIds, orderBy, filter, pageNumber, pageSize, false, 0);
        }

        public PagedList<Community> GetUserTopWorlds (int userId, int[] communityTypeIds, string orderBy, string filter, int pageNumber, int pageSize, bool includeCreditBalance, int premiumItemDaysPendingBeforeRedeem)
        {
            return communityDao.GetUserTopWorlds (userId, communityTypeIds, orderBy, filter, pageNumber, pageSize, includeCreditBalance, premiumItemDaysPendingBeforeRedeem);
        }

        public int UpdateCommunityParentId(int communityId, int parentId)
        {
            return communityDao.UpdateCommunityParentId (communityId, parentId);
        }

        public int InsertHomeCommunityIdAsChildWorld(int communityId)
        {
            return communityDao.InsertHomeCommunityIdAsChildWorld(communityId);
        }

        public int RemoveHomeCommunityIdAsChildWorld(int communityId)
        {
            return communityDao.RemoveHomeCommunityIdAsChildWorld(communityId);
        }
    }
}
