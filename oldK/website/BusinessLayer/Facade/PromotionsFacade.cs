///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class PromotionsFacade
    {
        private IPromotionsDao promotionsDao = DataAccess.PromotionsDao;

        // ***********************************************
        // Game Object Related Functions
        // ***********************************************
        #region Game Items

        /// <summary>
        /// GetActivePromotions
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Promotion> GetActivePromotions(string promoOfferTypeIDList)
        {
            return promotionsDao.GetActivePromotions(promoOfferTypeIDList);
        }

        /// <summary>
        /// GetActivePromotions
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Promotion> GetActivePromotions(string promoOfferTypeIDList, string keiPointType)
        {
            return promotionsDao.GetActivePromotions(promoOfferTypeIDList, keiPointType);
        }

        /// <summary>
        /// GetActivePromotions
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Promotion> GetActivePromotions(string promoOfferTypeIDList, string keiPointType, Double minimumPointAmount)
        {
            return promotionsDao.GetActivePromotions(promoOfferTypeIDList, keiPointType, minimumPointAmount);
        }

        /// <summary>
        /// GetActivePromotions
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Promotion> GetActivePromotions(string promoOfferTypeIDList, string keiPointType, Double minimumPointAmount, DateTime dateNOW)
        {
            return promotionsDao.GetActivePromotions(promoOfferTypeIDList, keiPointType, minimumPointAmount, dateNOW);
        }

        /// <summary>
        /// GetPromotionById
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Promotion GetPromotionsByPromoId(int promotionalID)
        {
            return promotionsDao.GetPromotionsByPromoId(promotionalID);
        }

        /// <summary>
        /// GetOtherPromotionsWithSamePassGroups - returns all other active promotions that share the same
        /// pass group(s) as the one provided
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Promotion> GetOtherPromotionsWithSamePassGroups(uint promotionID)
        {
            //first get all the pass groups associated with this promotion
            string passGroupList = GetPassGroupsAssociatedWithPromotionAsString(promotionID);

            //now get any other promotions that have these passes associated with them
            return GetAllActiveSubscriptionBasedPromotionsInclude(passGroupList);
        }

        /// <summary>
        /// GetPassGroupsAssociatedWithPromotion - returns a data table just in case this is needed in future
        /// by a page for data population
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public string GetPassGroupsAssociatedWithPromotionAsString(uint promotionID)
        {
            //get the pass group result set by promo id
            DataTable passGroups = GetPassGroupsAssociatedWithPromotion(promotionID);

            //create pass group id list as string
            string pass_groups = "";
            foreach (DataRow row in passGroups.Rows)
            {
                pass_groups += row["pass_group_id"] + ",";
            }
            //remove the trialing comma
            if((pass_groups != null) && (pass_groups.Length > 1))
            {
                pass_groups = pass_groups.Substring(0, pass_groups.Length - 1);
            }

            //return the result
            return pass_groups ;
        }

        /// <summary>
        /// GetPassGroupsAssociatedWithPromotion - returns a data table just in case this is needed in future
        /// by a page for data population
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetPassGroupsAssociatedWithPromotion(uint promotionID)
        {
            //create new result DataTable
            return promotionsDao.GetPassGroupsAssociatedWPromotion(promotionID);
        }

        /// <summary>
        /// GetTransactionCountExclusive
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetUserTransactionCountExclusive(int userId, string promotionTypeFilter)
        {
            return promotionsDao.GetUserTransactionCount(userId, "NOT IN", promotionTypeFilter);
        }

        /// <summary>
        /// GetTransactionCountInclusive
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetUserTransactionCountInclusive(int userId, string promotionTypeFilter)
        {
            return promotionsDao.GetUserTransactionCount(userId, "IN", promotionTypeFilter);
        }

        /// <summary>
        /// GetUserPurchasesByType
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetUserPurchasesByType(int userId, int promotionType)
        {
            return promotionsDao.GetUserPurchasesByType(userId, promotionType);
        }

        /// <summary>
        /// GetPromotionalOfferItemsByGender
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetPromotionalOfferItemsByGender(int promoId, string genderList)
        {
            return promotionsDao.GetPromotionalOfferItemsByGender(promoId, genderList);
        }

        /// <summary>
        /// GetBestSellingPromotion
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetBestSellingPromotion(string promoOfferTypeIDList)
        {
            return promotionsDao.GetBestSellingPromotion(promoOfferTypeIDList);
        }

        /// <summary>
        /// Checks to see if there is already one subscription of the provided promotiona type
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool DoesSubscriptionOfPromoTypeExist(int promotionTypeID)
        {
            //get list of all matching subscriptions
            List<Promotion> matchingPromos = promotionsDao.GetPromotionsWithSubscriptionByPromoType(promotionTypeID);

            //return bool indicating if any where found
            return (matchingPromos.Count > 0);
        }

        /// <summary>
        /// Checks to see if there is already one subscription of the provided promotiona type
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Promotion> GetPromotionsWithSubscriptionByPromoType(int promotionTypeID)
        {
            //get list of all matching subscriptions
            return promotionsDao.GetPromotionsWithSubscriptionByPromoType(promotionTypeID);
        }

        /// <summary>
        /// Gets all of the active subscription based promotions
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Promotion> GetAllActiveSubscriptionBasedPromotions()
        {
            //get list of all matching subscriptions
            return promotionsDao.GetAllActiveSubscriptionBasedPromotions("", "");
        }

        /// <summary>
        /// Gets all of the active subscription based promotions
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Promotion> GetAllActiveSubscriptionBasedPromotionsExclude(string pass2Exclued)
        {
            //get list of all matching subscriptions
            return promotionsDao.GetAllActiveSubscriptionBasedPromotions(pass2Exclued, "");
        }

        /// <summary>
        /// Gets all of the active subscription based promotions
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Promotion> GetAllActiveSubscriptionBasedPromotionsInclude(string pass2Inclued)
        {
            //get list of all matching subscriptions
            return promotionsDao.GetAllActiveSubscriptionBasedPromotions("", pass2Inclued);
        }


        /// <summary>
        /// Gets all of the active subscription based promotions
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Promotion> GetAllActiveUnpurchasedSubscriptionBasedPromotions(int userId, string pass2Exclued)
        {
            //get list of all matching subscriptions
            return promotionsDao.GetAllActiveUnpurchasedSubscriptionBasedPromotions(userId,pass2Exclued);
        }


        /// <summary>
        /// Checks to see if there is already one subscription of the provided promotion and pass type
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Promotion> GetPromotionsByPromotionAndPassType(int promotionTypeID, int passTypeID)
        {
            //get list of all matching subscriptions
            return promotionsDao.GetPromotionsByPromotionAndPassType(promotionTypeID, passTypeID);
        }

        #endregion

    }
}
