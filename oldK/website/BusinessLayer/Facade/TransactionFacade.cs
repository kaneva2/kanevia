///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject (true)]
    public class TransactionFacade
    {
        private ITransactionDao transDao = DataAccess.TransactionDao;
        private ITransactionDao transDao_DP2 = DataAccess.TransactionDao_DP2;

        /// <summary>
        /// Get fame transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<FameTransaction> GetFameTransactionsByUser (int userId, string gender, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao.GetFameTransactionsByUser (userId, gender, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Get reward transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<RewardTransaction> GetRewardTransactionsByUser (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao.GetRewardTransactionsByUser (userId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Get purchase transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<PurchaseTransaction> GetPurchasesTransactionsByUser (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao.GetPurchasesTransactionsByUser (userId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Get purchase rave transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<PurchaseTransaction> GetPurchasesRaveTransactionsByUser (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao.GetPurchasesRaveTransactionsByUser (userId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Get trade transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<TradeTransaction> GetTradeTransactionsByUser (int userId, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao.GetTradeTransactionsByUser (userId, orderby, pageNumber, pageSize);
        }
        
        /// <summary>
        /// Get gift transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<GiftTransaction> GetGiftTransactionsByUser (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao.GetGiftTransactionsByUser (userId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Get gift transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<PurchaseTransactionPremiumItem> GetPurchaseTransactionsByGame (int communityId, string filter, string orderby, int pageNumber, int pageSize)
        {
            return transDao.GetPurchaseTransactionsByGame (communityId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Get asset rave transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<RaveTransaction> GetAssetRaveTransactionsByUser (int userId, bool isUserTheRaveGiver, string filter, string orderby, int pageNumber, int pageSize)
        {
            return transDao.GetAssetRaveTransactionsByUser (userId, isUserTheRaveGiver, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Get Profile and Community rave transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<RaveTransaction> GetCommunityRaveTransactionsByUser (int userId, bool isUserTheRaveGiver, string filter1, string filter2, string orderby, int pageNumber, int pageSize)
        {
            return transDao.GetCommunityRaveTransactionsByUser(userId, isUserTheRaveGiver, filter1, filter2, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Get 3D Home and Hangout rave transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<RaveTransaction> Get3DHomeRaveTransactionsByUser (int userId, bool isUserTheRaveGiver, string filter, string orderby, int pageNumber, int pageSize)
        {
            return transDao.Get3DHomeRaveTransactionsByUser (userId, isUserTheRaveGiver, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Get UGC rave transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<RaveTransaction> GetUGCRaveTransactionsByUser (int userId, bool isUserTheRaveGiver, string filter, string orderby, int pageNumber, int pageSize)
        {
            return transDao.GetUGCRaveTransactionsByUser (userId, isUserTheRaveGiver, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Get rave transactions for a community.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<RaveTransaction> GetCommunityRaveTransactions(int communityId, string filter1, string filter2, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao.GetCommunityRaveTransactions(communityId, filter1, filter2, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Get rave transactions for a community.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedDataTable GetSalesOrderTransactions(DateTime dtStartDate, DateTime dtEndDate, string filter, string orderby, int pageNumber, int pageSize)
        {
            return transDao.GetSalesOrderTransactions(dtStartDate, dtEndDate, filter, orderby, pageNumber, pageSize);
        }



        /// <summary>
        /// Get gift transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetKanevaEventsByTransactionType(int transactionTypeId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao.GetKanevaEventsByTransactionType(transactionTypeId);
        }

        /// <summary>
        /// Get gift transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetKanevaEvents()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao.GetKanevaEvents();
        }

        /// <summary>
        /// Get gift transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetTransactionTypes(string filter, string orderBy)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao.GetTransactionTypes(filter, orderBy);
        }

        /// <summary>
        /// Get gift transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddKanevaRewardEvent(string eventName, int transactionTypeId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao.AddKanevaRewardEvent(eventName, transactionTypeId);
        }

        /// <summary>
        /// Get gift transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateKanevaRewardEvent(int eventId, string eventName, int transactionTypeId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao.UpdateKanevaRewardEvent(eventId, eventName, transactionTypeId);
        }

        /// <summary>
        /// Get gift transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int RewardCredits(double amount, string keiPointId, int rewardEventId, int transTypeId, DateTime startDate, DateTime endDate)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao.RewardCredits(amount, keiPointId, rewardEventId, transTypeId, startDate, endDate);
        }

        /// <summary>
        /// Get gift transactions for a user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int RewardCreditsToMultipleUsers(string userIdList, string userEmailList, double amount, int transTypeId, string keiPointId, int rewardEventId, ref string failedUserIdList)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao.RewardCreditsToMultipleUsers(userIdList, userEmailList, amount, transTypeId, keiPointId, rewardEventId, ref failedUserIdList);
        }

        /// <summary>
        /// Logs All attempted Super Rewards Transactions.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int LogSuperRewardsTransaction(SuperRewardsTransaction swTransaction)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao.LogSuperRewardsTransaction(swTransaction);
        }

        /// <summary>
        /// Logs All attempted Super Rewards Transactions to SQL Server.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int LogSuperRewardsTransaction_SQLSrv(SuperRewardsTransaction swTransaction)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao_DP2.LogSuperRewardsTransaction(swTransaction);
        }

        /// <summary>
        /// Logs All attempted Super Rewards Transactions.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SuperRewardsCreditAlreadyGiven(uint srTransactionId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao.SuperRewardsCreditAlreadyGiven(srTransactionId);
        }

        /// <summary>
        /// Logs All attempted Super Rewards Transactions into SQL Server.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SuperRewardsCreditAlreadyGiven_SQLSrv(uint srTransactionId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return transDao_DP2.SuperRewardsCreditAlreadyGiven(srTransactionId);
        }
    
        /// <summary>
        /// Gets total credit amount of all transactions for a game.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int GetTotalTransactionAmountByGame (int gameId)
        {
            return transDao.GetTotalTransactionAmountByGame (gameId);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public PremiumItemRedeemAmounts GetTotalTransactionCreditAmountsByGame (int communityId, int daysPendingInterval)
        {
            return transDao.GetTotalTransactionCreditAmountsByGame (communityId, daysPendingInterval);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public PremiumItemRedeemAmounts GetTotalTransactionCreditAmountsByUser(int userId, int daysPendingInterval)
        {
            return transDao.GetTotalTransactionCreditAmountsByUser(userId, daysPendingInterval);
        }

        [DataObjectMethod (DataObjectMethodType.Update)]
        public int RedeemPremiumItemCredits (int communityId, int daysPendingBeforeRedeem)
        {
            return transDao.RedeemPremiumItemCredits (communityId, daysPendingBeforeRedeem);
        }
    }
}


