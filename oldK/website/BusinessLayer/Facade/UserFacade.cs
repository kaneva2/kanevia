///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Net;
using System.IO;
using System.Web;
using System.Data;
using System.Collections.Specialized;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;
//using Kaneva.Framework.Transactions;

using System.Web.Security;
using System.Threading;
using log4net;

namespace Kaneva.BusinessLayer.Facade
{
    /// <summary>
    /// Facade (also called Service Layer) that controls all access to Customer, 
    /// Order, and Order Details related activities.
    /// 
    /// Gof Design Patterns: Facade, Decorator, Proxy
    /// Enterprise Design Patterns: Remote Facade, LazyLoad, Service Layer, Transaction Script.
    /// </summary>
    /// <remarks>
    /// This Facade is not secure. In most applications each method in the 
    /// Facade should check for access security. This can be accomplished  
    /// either programmatically or declaratively (using Attributes). In addition,
    /// validation of arguments should also be added in each procedure.
    /// 
    /// The DataObject and DataObjectMethod Attributes in this class tell the 
    /// Visual Studio 2005 Wizards which classes and methods are 
    /// appropriate for building the ObjectDataSource Web controls.
    /// 
    /// The Proxy Pattern is used in accessing User and User Stats.
    /// 
    /// The Decorator Pattern is used in bracketing database transactions with
    /// a decorated Transaction scope class. 
    /// 
    /// The Remote Facade Design Pattern provides a course-grained facade on a 
    /// fine-grained objects. Indeed the methods are course-grained as they 
    /// deal with complete entities and/or entity lists rather than their individual 
    /// attributes.
    /// 
    /// The Service Layer Design Pattern is the same as the Facade Design Pattern 
    /// except that Service Layer is more specific to Enterprise Business Applications 
    /// in that the layer sits on top of the Domain Model (which is the case here).
    /// 
    /// The Transaction Script Design Pattern organizes business logic by procedures
    /// where each procedure handles a single request from the presentation.  This is 
    /// exactly how the Facade API has been modeled. They entirely handle individual
    /// requests (either from Web Application or Web Service).
    /// </remarks>
    [DataObject(true)]
    public class UserFacade
    {
        private IUserDao userDao = DataAccess.UserDao;
        private IUserDao userDao_DP2 = DataAccess.UserDao_DP2;
        private ICommunityDao communityDao = DataAccess.CommunityDao;

        public enum eAdjustUserBalanceReturnCode
        {
            Success = 0,
            InsufficientFunds = 1,
            Error = -1
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Get a specific user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public User GetUser(int userId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            User user = userDao.GetUser(userId);

            //Gets user's Site Privileges
            user.UserPrivileges = (UsersPrivileges)new ProxyForUsersPrivileges(user);

            // Assign a Proxy for Friends, Stats, Community. Only when they are absolutely needed will they be retrieved from the database.
            //user.Friends = new ProxyForFriends<User>(user);
            //user.Community = new ProxyForCommunity(user);
            user.Stats = (UserStats)new ProxyForStats(user);
            user.Preference = (Preferences)new ProxyForPreferences(user);
            user.NotificationPreference = (NotificationPreferences)new ProxyForNotificationPreferences(user);

            //gets user's subscriptions proxy object
            user.UsersSubscriptions = (UsersSubscriptions)new ProxyForUsersSubscriptions(user);

            user.Balances = (UserBalances)new ProxyForUserBalances(user);

            // Get user facebook settings
            user.FacebookSettings = (FacebookSettings)new ProxyForFacebookSettings(user);

            return user;
        }

        /// <summary>
        /// Returns a paged list of users who share similar display_names.
        /// </summary>
        /// <param name="displayNameToSearch">The display name to search for.</param>
        /// <param name="userNameToExclude">Optional username of user to exclude. Pass string.Empty to ignore.</param>
        /// <param name="exact">Are we looking for exact matches only?</param>
        /// <param name="pageNumber">The current page number.</param>
        /// <param name="pageSize">The size of the page.</param>
        /// <returns>A paged list of users with similar display_names.</returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<User> GetOthersWithSimilarName(string displayNameToSearch, string userNameToExclude, bool exact, int pageNumber, int pageSize)
        {
            string searchString = string.Empty;
            string filter = string.Empty;

            // Make sure there's no white space weirdness
            displayNameToSearch = displayNameToSearch.Trim();
            displayNameToSearch = System.Text.RegularExpressions.Regex.Replace(displayNameToSearch, @"\s+", " ");

            if (exact)
            {
                // Phrase match on the display name
                filter = "@display_name \"^" + displayNameToSearch + "$\"";
            }
            else
            {
                // Keyword match on display name parts, phrase exclusion of display name whole
                searchString = displayNameToSearch;
                filter = "@display_name !\"" + displayNameToSearch + "\"";
            }

            // Optionally exclude the username
            filter += (userNameToExclude.Length > 0 ? " & @username !" + userNameToExclude : "");

            return userDao.SearchUsers(
                false, false, searchString,
                true, true, true, 18, 0,
                string.Empty, string.Empty, 0,
                0, 0, 0, -1, 0, -1, string.Empty,
                string.Empty, string.Empty, string.Empty,
                string.Empty, string.Empty, string.Empty,
                string.Empty, false, string.Empty, filter,
                string.Empty, pageNumber, pageSize);
        }

        public PagedList<User> GetUsersList(string username, string email, string firstName, string lastName, int userStatus, string filter, string orderby, int pageNumber, int pageSize)
        {
            return userDao.GetUsersList(username, email, firstName, lastName, userStatus, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Get a specific user from their Facebook User Id.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public User GetUserByFacebookUserId(UInt64 fbUserId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            User user = userDao.GetUserByFacebookUserId(fbUserId);

            //Gets user's Site Privileges
            user.UserPrivileges = (UsersPrivileges)new ProxyForUsersPrivileges(user);

            // Assign a Proxy for Friends, Stats, Community. Only when they are absolutely needed will they be retrieved from the database.
            //user.Friends = new ProxyForFriends<User>(user);
            //user.Community = new ProxyForCommunity(user);
            user.Stats = (UserStats)new ProxyForStats(user);
            user.Preference = (Preferences)new ProxyForPreferences(user);
            user.NotificationPreference = (NotificationPreferences)new ProxyForNotificationPreferences(user);

            //gets user's subscriptions proxy object
            user.UsersSubscriptions = (UsersSubscriptions)new ProxyForUsersSubscriptions(user);

            user.Balances = (UserBalances)new ProxyForUserBalances(user);

            // Get user facebook settings
            user.FacebookSettings = (FacebookSettings)new ProxyForFacebookSettings(user);

            return user;
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<User> GetUsersByFacebookUserId(int userId, string idList, string orderby, int pageNumber, int pageSize)
        {
            return userDao.GetUsersByFacebookUserId(userId, idList, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Set the Facebook access token.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int SetFacebookAccessToken(UInt64 fbUserId, string token)
        {
            return userDao.SetFacebookAccessToken(fbUserId, token);
        }

        /// <summary>
        /// Set users Facebook values.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int ConnectUserToFacebook(int userId, UInt64 fbUserId, string accessToken)
        {
            return ConnectUserToFacebook(userId, fbUserId, false, accessToken);
        }

        /// <summary>
        /// Set users Facebook values.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int ConnectUserToFacebook(int userId, UInt64 fbUserId, bool useFBProfilePic, string accessToken)
        {
            FameFacade fameFacade = new FameFacade();
            fameFacade.RedeemPacket(userId, (int)PacketId.CONNECT_TO_FACEBOOK, (int)FameTypes.World);

            return userDao.ConnectUserToFacebook(userId, fbUserId, useFBProfilePic, accessToken);
        }

        /// <summary>
        /// Disconnects User from facebook.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DisconnectUserFromFacebook(int userId)
        {
            return userDao.DisconnectUserFromFacebook(userId);
        }

        /// <summary>
        /// Check if facebook user is already connected to Kaneva User Account.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool IsFacebookUserAlreadyConnected(UInt64 fbUserId)
        {
            return userDao.IsFacebookUserAlreadyConnected(fbUserId);
        }

        /// <summary>
        /// Update user setting to use/not user their facebook profile picture
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int SetUseFacebookProfilePic(int userId, bool useProfilePic)
        {
            return userDao.SetUseFacebookProfilePic(userId, useProfilePic);
        }

        /// <summary>
        /// Gets list of kaneva user ids associated with facebook user ids
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<UInt64> GetFacebookUserIdForConnectedUsers(string fbUserIdList)
        {
            return userDao.GetFacebookUserIdForConnectedUsers(fbUserIdList);
        }

        /// <summary>
        /// Gets list of kaneva user ids associated with facebook user ids
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void InvalidateKanevaUserFacebookCache(int userId)
        {
            userDao.InvalidateKanevaUserFacebookCache(userId);
        }

        /// <summary>
        /// Gets a paged list of users 
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<User> GetUsers(string filter, string orderby, int pageNumber, int pageSize)
        {
            return userDao.GetUsersList(filter, orderby, pageNumber, pageSize);
        }



        /// <summary>
        /// GetUserObsoleteForWok
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataRow GetUserObsoleteForWok(int userId)
        {
            return userDao.GetUserObsoleteForWok(userId);

        }

        /// <summary>
        /// Get a specific user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public User GetUser(string userName)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            User user = userDao.GetUser(userName);

            //Gets user's Site Privileges
            user.UserPrivileges = (UsersPrivileges)new ProxyForUsersPrivileges(user);

            // Assign a Proxy for Friends, Stats, Community. Only when they are absolutely needed will they be retrieved from the database.
            //user.Friends = new ProxyForFriends<User>(user);
            //user.Community = new ProxyForCommunity(user);
            user.Stats = (UserStats)new ProxyForStats(user);
            user.Preference = (Preferences)new ProxyForPreferences(user);

            //gets user's subscriptions proxy object
            user.UsersSubscriptions = (UsersSubscriptions)new ProxyForUsersSubscriptions(user);

            user.Balances = (UserBalances)new ProxyForUserBalances(user);

            return user;
        }

        /// <summary>
        /// Authorize a user
        /// </summary>
        public int Authorize(string email, string password, int gameId, ref int roleMembership, string ipAddress)
        {
            int ret = 0;

            // Look up this user
            User user = userDao.GetUserByEmail(email);

            if (user.UserId.Equals(0))
            {
                return (int)eLOGIN_RESULTS.USER_NOT_FOUND;
            }
            else
            {
                ret = Authorize(user.Username, password, gameId, user.Password, user.Salt, user.StatusId, false);

                if (ret == (int)eLOGIN_RESULTS.INVALID_PASSWORD)
                {
                    roleMembership = 0;
                }
                else
                {
                    roleMembership = user.Role;
                }
            }

            return ret;
        }


        public int Authorize(string userName, string password, int gameId, string hashPassword, string salt, int userStatus, bool allowGame)
        {
            // Get the hashed password
            string calcHashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(MakeHash(password + userName.ToLower()) + salt, "MD5");
            return Authorize(userName, calcHashPassword, gameId, hashPassword, userStatus, allowGame);
        }

        /// <summary>
        /// Authorize
        /// </summary>
        public int Authorize(string userName, string calcHashPassword, int gameId, string hashPassword, int userStatus, bool allowGame)
        {
            eLOGIN_RESULTS ret = eLOGIN_RESULTS.ACCOUNT_LOCKED;
            int userId = 0;

            // Was the correct password supplied?
            if (calcHashPassword == hashPassword)
            {
                // if they are trying to authenticate against a game, check they have a subscription or bought it
                //if (gameId > 0 && !getDAO ().getGameAuthorization (userId, gameId))

                if (gameId > 0)
                {
                    // TODO: for STARs may need to do something for validating access, we now pass in gameId
                    //    return (int) Constants.eLOGIN_RESULTS.NO_GAME_ACCESS;
                }

                // Has this account been deleted?
                if (userStatus == (int)User.eUSER_STATUS.DELETED || userStatus == (int)User.eUSER_STATUS.DELETEDBYUS || userStatus == (int)User.eUSER_STATUS.DELETEDVALIDATED)
                {
                    return (int)eLOGIN_RESULTS.ACCOUNT_DELETED;
                }

                // check to see if the user has a record in the suspension table.

                // Has this account been locked?
                if (userStatus == (int)User.eUSER_STATUS.LOCKED || userStatus == (int)User.eUSER_STATUS.LOCKEDVALIDATED)
                {
                    // check to see if the user has a record in the suspension table.
                    userId = GetUserIdFromUsername(userName);

                    IList<UserSuspension> dtBans = GetUserSuspensions(userId);
                    DateTime maxEndDate = DateTime.Now.AddDays(-1);

                    foreach (UserSuspension da in dtBans)
                    {
                        // do they have a record with a null end date?
                        // if so they are perma banned.
                        if (da.BanEndDate == null)
                        {
                            return (int)eLOGIN_RESULTS.ACCOUNT_LOCKED;
                        }

                        // keep track of the maximum end date so that we can use it later.
                        if (Convert.ToDateTime(da.BanEndDate) > maxEndDate)
                        {
                            maxEndDate = Convert.ToDateTime(da.BanEndDate);
                        }
                    }

                    // pessimistic
                    ret = eLOGIN_RESULTS.ACCOUNT_LOCKED;

                    // Only do the following if the user has an entry in the suspension table
                    // This way everyone who was locked/banned before this system was implemented
                    // will fall through correctly.
                    if (dtBans != null && dtBans.Count > 0)
                    {
                        if (maxEndDate < DateTime.Now)
                        {
                            // The user has had a suspension, but not perma banned.
                            // The suspension has expired. Update their status and return success.
                            UserFacade userFacade = new UserFacade();
                            if (userStatus == 6 || userStatus == 7 || userStatus == 1)
                            {
                                // Set validated 
                                userFacade.UpdateUserStatus(userId, (int)User.eUSER_STATUS.REGVALIDATED);
                            }
                            else
                            {
                                // set to not validated
                                userFacade.UpdateUserStatus(userId, (int)User.eUSER_STATUS.REGNOTVALIDATED);
                            }

                            ret = eLOGIN_RESULTS.SUCCESS;
                        }
                    }
                }
                else
                    ret = eLOGIN_RESULTS.SUCCESS;
            }
            else
            {
                ret = eLOGIN_RESULTS.INVALID_PASSWORD;
            }

            // if they are allowing this game access, update the db to indicate
            // they shouldn't be prompted again
            if (ret == eLOGIN_RESULTS.SUCCESS && gameId > 0 && allowGame)
            {
                if (userId == 0)
                    userId = GetUserIdFromUsername(userName);

                GameFacade gf = new GameFacade();
                if (!gf.UserAllowedGame(gameId, userId))
                {
                    m_logger.Error("Failed to insert record into user_game_allowed table userName: " + userName + " userId: " + userId + " gameId: " + gameId);
                }
            }
            return (int)ret;
        }

        #region Suspensions

        /// <summary>
        /// Retrieves all suspensions associated with the specified user id, optionally returning only
        /// currently active suspensions.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<UserSuspension> GetUserSuspensions(int userId, bool activeOnly = false)
        {
            return userDao.GetUserSuspensions(userId, activeOnly);
        }

        /// <summary>
        /// Retrieves all suspensions associated with the specified user ids, optionally returning only
        /// currently active suspensions.
        /// </summary>
        public Dictionary<int, List<UserSuspension>> GetUserSuspensions(IEnumerable<int> userIds, bool activeOnly = false)
        {
            return userDao.GetUserSuspensions(userIds, activeOnly);
        }

        /// <summary>
        /// Retrieves a mapping of user ids to suspensions associated with the specified user system id, optionally returning only
        /// currently active suspensions.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Dictionary<int, List<UserSuspension>> GetUserSuspensionsBySystemBanId(int systemBanId, bool activeOnly = false)
        {
            return userDao.GetUserSuspensionsBySystemBanId(systemBanId, activeOnly);
        }

        /// <summary>
        /// Save changes to a UserSuspension, banning the user as appropriate.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SaveUserSuspension(int userCurrentStatus, UserSuspension suspension, int banningUserId, string banNote)
        {
            return userDao.SaveUserSuspension(userCurrentStatus, suspension, banningUserId, banNote);
        }

        /// <summary>
        /// Deletes a user suspension from the database. 
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteUserSuspension(int banId, int userId)
        {
            return userDao.DeleteUserSuspension(banId, userId);
        }

        /// <summary>
        /// Retrieves a list of user system suspensions for a given system, optionally limited
        /// to active suspensions only.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<UserSystemSuspension> GetUserSystemSuspensions(string systemId, bool activeOnly = false)
        {
            return userDao.GetUserSystemSuspensions(systemId, activeOnly);
        }

        /// <summary>
        /// Saves a user system suspension and any associated user suspensions.
        /// If the suspension is active, also inserts suspensions for all system users who have not
        /// already been suspended.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SaveUserSystemSuspension(UserSystemSuspension systemSuspension, int banningUserId)
        {
            return userDao.SaveUserSystemSuspension(systemSuspension, banningUserId);
        }

        public int DeleteUserSystemSuspension(int banId)
        {
            return userDao.DeleteUserSystemSuspension(banId);
        }

        #endregion

        /// <summary>
        /// MakeHash
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private string MakeHash(string s)
        {
            // This is one implementation of the abstract class MD5.
            System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();

            System.Text.Encoder encoder = System.Text.ASCIIEncoding.UTF8.GetEncoder();
            int byteCount = encoder.GetByteCount(s.ToCharArray(), 0, s.Length, true);
            byte[] bytes = new byte[byteCount];
            encoder.GetBytes(s.ToCharArray(), 0, s.Length, bytes, 0, true);
            byte[] result = md5.ComputeHash(bytes);

            string ret = "";
            for (int i = 0; i < result.Length; i++)
            {
                ret = ret + result[i].ToString("x2");
            }
            return ret;
        }

        /// SearchUsers
        /// </summary>
        public PagedList<User> SearchUsers(bool onlyAccessPass, bool showMature, string searchString, bool male, bool female, bool bOnlyPhoto,
            int ageFrom, int ageTo,
            string country, string zipCode, int zipMiles, int newWithinDays, int updatedWithinDays,
            int heightFromFeet, int heightFromInches, int heightToFeet, int heightToInches,
            string ethnicity, string religion,
            string relationship, string orientation, string education,
            string drink, string smoke,
            string interest, bool bOnlineNow, string hometown,
            string filter, string orderBy, int pageNumber, int pageSize)
        {
            return userDao.SearchUsers(
                onlyAccessPass, showMature, searchString, male, female, bOnlyPhoto,
                ageFrom, ageTo,
                country, zipCode, zipMiles, newWithinDays, updatedWithinDays,
                heightFromFeet, heightFromInches, heightToFeet, heightToInches,
                ethnicity, religion,
                relationship, orientation, education,
                drink, smoke,
                interest, bOnlineNow, hometown,
                filter, orderBy, pageNumber, pageSize
            );
        }

        /// <summary>
        /// AdjustUserBalance
        /// </summary>
        /// <returns></returns>
        public int AdjustUserBalance(int userId, string keiPointId, Double amount, UInt16 transType)
        {
            int wok_transaction_log_id = 0;

            int result = userDao.AdjustUserBalance(userId, keiPointId, amount, transType, ref wok_transaction_log_id);

            // Notify WOK
            SendBalanceUpdateToPlayer(userId, transType, keiPointId, amount);

            //Redeem Packet
            if (transType == (int)TransactionType.eTRANSACTION_TYPES.CASH_TT_BOUGHT_CREDITS)
            {
                FameFacade fameFacade = new FameFacade();
                fameFacade.RedeemPacket(userId, (int)PacketId.BUY_CREDITS, (int)FameTypes.World);
            }

            return result;
        }

        /// <summary>
        /// AdjustUserBalance
        /// </summary>
        /// <returns></returns>
        public int AdjustUserBalance(int userId, string keiPointId, Double amount, UInt16 transType, ref int wok_transaction_log_id)
        {
            //AdjustUserBalance was changed so that it returns 0 as success
            int result = userDao.AdjustUserBalance(userId, keiPointId, amount, transType, ref wok_transaction_log_id);

            // Notify WOK
            SendBalanceUpdateToPlayer(userId, transType, keiPointId, amount);

            return result;
        }

        /// <summary>
        /// AdjustUserBalanceWithDetails
        /// </summary>
        /// <returns></returns>
        public int AdjustUserBalanceWithDetails(int userId, string keiPointId, Double amount, UInt16 transType, int globalId, int quantity)
        {
            int wok_transaction_log_id = 0;
            int result = userDao.AdjustUserBalanceWithDetails(userId, keiPointId, amount, transType, globalId, quantity, ref wok_transaction_log_id);

            // Notify WOK
            SendBalanceUpdateToPlayer(userId, transType, keiPointId, amount);

            return result;
        }

        /// <summary>
        /// AdjustUserBalanceWithDetails
        /// </summary>
        /// <returns></returns>
        public int AdjustUserBalanceWithDetails(int userId, string keiPointId, Double amount, UInt16 transType, int globalId, int quantity, ref int wok_transaction_log_id)
        {
            //AdjustUserBalance was changed so that it returns 0 as success
            int result = userDao.AdjustUserBalanceWithDetails(userId, keiPointId, amount, transType, globalId, quantity, ref wok_transaction_log_id);

            // Notify WOK
            SendBalanceUpdateToPlayer(userId, transType, keiPointId, amount);

            return result;
        }

        /// <summary>
        /// SendBalanceUpdateToPlayer
        /// </summary>
        /// <param name="receiverUserId"></param>
        public void SendBalanceUpdateToPlayer(int receiverUserId, UInt16 transType, string keiPointId, Double amount)
        {
            // Get this one time, used several times below
            UserFacade userFacade = new UserFacade();
            XmlEventCreator xml = new XmlEventCreator("ClientTickler");

            User receiver = null;

            receiver = userFacade.GetUser(receiverUserId);

            // If user has never logged onto wok, no need to send event
            if (receiver.WokPlayerId > 0)
            {
                UserBalances ub = userDao.GetUserBalances(receiverUserId);

                xml.AddValue((int)NotificationsProfile.NotificationTypes.BalanceChange);
                xml.AddValue(receiver.Username);
                xml.AddValue(receiver.UserId);
                xml.AddValue(ub.Credits);
                xml.AddValue(ub.Rewards);
                xml.AddValue(keiPointId);
                xml.AddValue(amount);
                xml.AddValue(transType);

                xml.AddPlayer(receiver.WokPlayerId);

                try
                {
                    RemoteEventSender res = new RemoteEventSender();
                    res.BroadcastEventToServer(xml.GetXml()); //send event to all servers
                }
                catch (Exception e)
                {
                    m_logger.Error("Error Broadcasting ClientTickler to WoK Server ", e);
                }
            }
        }

        /// <summary>
        /// Called to setup single sign-on variables in session.
        /// </summary>
        public void SetupSingleSignOn(int userId, string username, string password)
        {
            // Prevent invalid token generation.
            if (!string.IsNullOrWhiteSpace(username) && !string.IsNullOrWhiteSpace(password))
            {
                string newToken = "User=" + Common.AESEncrypt(username) + "&Id=" + Common.AESEncrypt(password);

                string cacheKey = Kaneva.DataLayer.DataObjects.CentralCache.keyUserSingleSignOnToken(userId);
                Object token = Kaneva.DataLayer.DataObjects.CentralCache.Get(cacheKey);

                if (token == null)
                {
                    Kaneva.DataLayer.DataObjects.CentralCache.Store(cacheKey, newToken);
                }
                else
                {
                    Kaneva.DataLayer.DataObjects.CentralCache.Replace(cacheKey, newToken);
                }
            }
        }

        /// <summary>
        /// Called to cleanup single sign-on variables from session for security reasons.
        /// </summary>
        public void CleanupSingleSignOn(int userId)
        {
            string cacheKey = Kaneva.DataLayer.DataObjects.CentralCache.keyUserSingleSignOnToken(userId);
            Object token = Kaneva.DataLayer.DataObjects.CentralCache.Get(cacheKey);

            if (token != null)
            {
                Kaneva.DataLayer.DataObjects.CentralCache.Remove(cacheKey);
            }
        }

        /// <summary>
        /// Called to get the single sign-on token for a logged-in user.
        /// </summary>
        /// <returns>An empty string if no token is found.</returns>
        public string GetSingleSignOnToken(int userId)
        {
            string cacheKey = Kaneva.DataLayer.DataObjects.CentralCache.keyUserSingleSignOnToken(userId);
            return (Kaneva.DataLayer.DataObjects.CentralCache.Get(cacheKey) ?? string.Empty).ToString();
        }

        /// <summary>
        /// Get user Id
        /// </summary>
        /// <returns></returns>
        public int GetUserIdFromUsername(string username)
        {
            return userDao.GetUserIdFromUsername(username);
        }

        /// <summary>
        /// Get user home community id
        /// </summary>
        /// <returns></returns>
        public int GetUserHomeCommunityIdByUserId(int userId)
        {
            return userDao.GetUserHomeCommunityIdByUserId(userId);
        }

        /// <summary>
        /// Get user home community id
        /// </summary>
        /// <returns></returns>
        public int GetUserHomeCommunityIdByPlayerId(int playerId)
        {
            return userDao.GetUserHomeCommunityIdByPlayerId(playerId);
        }

        /// <summary>
        /// Get user name
        /// </summary>
        /// <returns></returns>
        public string GetUserName(int userId)
        {
            return userDao.GetUserName(userId);
        }

        /// <summary>
        /// Get users security role
        /// </summary>
        /// <returns></returns>
        public int UpdateUserSecurity(User user)
        {
            return userDao.UpdateUserSecurity(user);
        }

        /// <summary>
        /// delete user to clean up registration issue
        /// </summary>
        /// <returns></returns>
        public int CleanUpRegistrationIssue(string username)
        {
            return userDao.CleanUpRegistrationIssue(username);
        }

        /// <summary>
        /// Get a specific user.
        /// Uses the Proxy Design Pattern.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public User GetUserByEmail(string email)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            User user = userDao.GetUserByEmail(email);

            //Gets user's Site Privileges
            user.UserPrivileges = (UsersPrivileges)new ProxyForUsersPrivileges(user);

            // Assign a Proxy for Friends, Stats, Community. Only when they are absolutely needed will they be retrieved from the database.
            //user.Friends = new ProxyForFriends<User>(user);
            //user.Community = new ProxyForCommunity(user);
            user.Stats = (UserStats)new ProxyForStats(user);
            user.Preference = (Preferences)new ProxyForPreferences(user);

            //gets user's subscriptions proxy object
            user.UsersSubscriptions = (UsersSubscriptions)new ProxyForUsersSubscriptions(user);

            user.Balances = (UserBalances)new ProxyForUserBalances(user);

            // Get user facebook settings
            user.FacebookSettings = (FacebookSettings)new ProxyForFacebookSettings(user);

            return user;
        }

        /// <summary>
        /// gather the privileges for the user and stores in a collection
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        private NameValueCollection GetUserSitePrivileges(User _user)
        {
            NameValueCollection collection = new NameValueCollection();

            try
            {
                //get new site security facade
                ISiteSecurityDao siteSecurDao = DataAccess.SiteSecurityDao;

                //get the privileges
                DataTable privileges = siteSecurDao.GetPrivilegesByRoleId(_user.Role, 0); //company id hardcoded for hosting company - change as necessary - hosting company is always 0
                foreach (DataRow row in privileges.Rows)
                {
                    collection.Add(row["privilege_id"].ToString(), row["access_level_id"].ToString());
                }
            }
            catch (Exception)
            { }

            return collection;
        }

        /// <summary>
        /// Update a friend
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int AcceptFriend(int userId, int friendId)
        {
            int result = userDao.AcceptFriend(userId, friendId);

            if (result > 0)
            {
                // Blast out the results
                BlastFacade blastFacade = new BlastFacade();
                User user = userDao.GetUser(userId);
                User friend = userDao.GetUser(friendId);

                // Both Users completed fame packet
                try
                {
                    FameFacade fameFacade = new FameFacade();
                    //Old Packets
                    //fameFacade.RedeemPacket(userId, (int)PacketId.BOTH_OT_ADD_5_FRIENDS, (int)FameTypes.World);
                    //fameFacade.RedeemPacket(friendId, (int)PacketId.BOTH_OT_ADD_5_FRIENDS, (int)FameTypes.World);

                    //New Packets
                    fameFacade.RedeemPacket(userId, (int)PacketId.ADD_FIRST_FRIEND, (int)FameTypes.World);
                    fameFacade.RedeemPacket(userId, (int)PacketId.ADD_3_FRIENDS, (int)FameTypes.World);
                    fameFacade.RedeemPacket(userId, (int)PacketId.ADD_10_FRIENDS, (int)FameTypes.World);
                    fameFacade.RedeemPacket(userId, (int)PacketId.ADD_20_FRIENDS, (int)FameTypes.World);
                    fameFacade.RedeemPacket(friendId, (int)PacketId.ADD_FIRST_FRIEND, (int)FameTypes.World);
                    fameFacade.RedeemPacket(friendId, (int)PacketId.ADD_3_FRIENDS, (int)FameTypes.World);
                    fameFacade.RedeemPacket(friendId, (int)PacketId.ADD_10_FRIENDS, (int)FameTypes.World);
                    fameFacade.RedeemPacket(friendId, (int)PacketId.ADD_20_FRIENDS, (int)FameTypes.World);
                }
                catch (Exception ex)
                {
                    m_logger.Error("Error awarding Packet BOTH_OT_ADD_5_FRIENDS, userid=" + userId.ToString() + ", friendId=" + friendId.ToString(), ex);
                }
            }

            return result;
        }

        /// <summary>
        /// Update a friend request
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateFriendRequestSentDate(int userId, int friendId)
        {
            return userDao.UpdateFriendRequestSentDate(userId, friendId);
        }

        /// <summary>
        /// Delete a friend
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteFriend(int userId, int friendId)
        {
            return userDao.DeleteFriend(userId, friendId);
        }

        /// <summary>
        /// Checks to see if two people are friends
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool AreFriends(int userId, int userId2)
        {
            // Make sure they are not already friends
            Friend result = userDao.GetFriend(userId, userId2);
            return (result.UserId > 0);
        }

        /// <summary>
        /// Gets a list of users
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Friend> GetFriends(int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return GetFriends(userId, 0, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Gets a list of users
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Friend> GetFriends(int userId, int friendId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return GetFriends(userId, friendId, filter, orderby, pageNumber, pageSize, true, "");
        }

        /// <summary>
        /// Gets a list of users
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Friend> GetFriends(int userId, int friendId, string filter, string orderby, int pageNumber, int pageSize, bool bShowMature, string userNameFilter)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return userDao.GetFriends(userId, friendId, filter, orderby, pageNumber, pageSize, bShowMature, userNameFilter);
        }


        /// <summary>
        /// Gets a list of users
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Friend> GetFriendsFast(int userId, int friendId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return userDao.GetFriendsFast(userId, friendId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Gets a list of users in a friend group
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Friend> GetFriendsInGroup(int friendGroupId, int ownerId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return userDao.GetFriendsInGroup(friendGroupId, ownerId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Get a specific FriendGroup.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public FriendGroup GetFriendGroup(int friendGroupId, int ownerId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return userDao.GetFriendGroup(friendGroupId, ownerId);
        }

        /// <summary>
        /// Search a users friends list
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Friend> SearchFriends(int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return userDao.SearchFriends(userId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Search a users GetFriendsRecommended list
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Friend> GetFriendsRecommended(string strCountry, string strZipCode, int age, string orderby, int pageNumber, int pageSize)
        {
            // Only use zip for US, only country otherwise
            if (strCountry.ToUpper().Equals("US"))
            {
                strCountry = "";
            }
            else
            {
                strZipCode = "";
            }

            return userDao.GetFriendsRecommended(strCountry, strZipCode, age, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Insert Friend Request
        /// </summary>
        [System.Security.SecuritySafeCritical]
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertFriendRequest(int userId, int friendId, string sourcePage = null, RabbitMQ.Client.IModel channel = null)
        {
            int ret = userDao.InsertFriendRequest(userId, friendId);
            if (ret == 1)
            {
                if (!string.IsNullOrEmpty(sourcePage) && channel != null) 
                {
                    Dictionary<string, string> metricData = new Dictionary<string,string> 
                    {
                        {"userId", userId.ToString()},
                        {"source", sourcePage},
                        {"targetUserId", friendId.ToString()}
                    };
                    (new MetricsFacade()).InsertGenericMetricsLog("friend_request_log", metricData, channel);
                }
                SendFriendRequestEventToPlayer(userId, friendId);
            }
            return ret;
        }

        public void SendFriendRequestEventToPlayer(int senderUserId, int receiverUserId)
        {
            // Get this one time, used several times below
            UserFacade userFacade = new UserFacade();
            XmlEventCreator xml = new XmlEventCreator("ClientTickler");

            User sender = null;
            User receiver = null;

            sender = userFacade.GetUser(senderUserId);
            receiver = userFacade.GetUser(receiverUserId);

            // If user has never logged onto wok, no need to send event
            if (receiver.WokPlayerId > 0)
            {
                xml.AddValue((int)NotificationsProfile.NotificationTypes.FriendRequest);
                xml.AddValue(sender.Username);
                xml.AddValue(sender.UserId);
                xml.AddValue(sender.ThumbnailPath);

                xml.AddPlayer(receiver.WokPlayerId);


                try
                {
                    NotificationsProfile np = new NotificationsProfile(0, receiverUserId, (int) NotificationsProfile.NotificationTypes.FriendRequest, xml.GetXml(), "");
                    userFacade.InsertUserNotificationProfile(np);
                }
                catch (Exception e)
                {
                    m_logger.Error("Error inserting notification profile", e);
                }

                try
                {
                    RemoteEventSender res = new RemoteEventSender();
                    res.BroadcastEventToServer(xml.GetXml()); //send event to all servers
                }
                catch (Exception e)
                {
                    m_logger.Error("Error Broadcasting ClientTickler to WoK Server ", e);
                }
            }
        }

        /// <summary>
        /// GetFriendRequest
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataRow GetFriendRequest(int userId, int friendId)
        {
            return userDao.GetFriendRequest(userId, friendId);
        }

        /// <summary>
        /// DeleteFriendRequest
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int DeleteFriendRequest(int userId, int friendId)
        {
            return userDao.DeleteFriendRequest(userId, friendId);
        }

        /// <summary>
        /// DenyFriend
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int DenyFriend(int userId, int friendId)
        {
            return userDao.DenyFriend(userId, friendId);
        }

        /// <summary>
        /// Insert Friend Group
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertFriendGroup(FriendGroup group)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return userDao.InsertFriendGroup(group);
        }

        /// <summary>
        /// GetFriendBirthdays
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<FriendDates> GetFriendBirthdays(int userId, string filter, string orderBy, int pageSize, int pageNumber)
        {
            return userDao.GetFriendBirthdays(userId, filter, orderBy, pageSize, pageNumber);
        }

        /// <summary>
        /// GetFriendAnniversaries
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<FriendDates> GetFriendAnniversaries(int userId, string filter, string orderBy, int pageSize, int pageNumber)
        {
            return userDao.GetFriendAnniversaries(userId, filter, orderBy, pageSize, pageNumber);
        }

        /// <summary>
        /// GetUsersBirthdaysWithFriendsList
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<FriendDates> GetUsersBirthdaysWithFriendsList(string timeSpanOccuringInNextNumDays, string timeSpanOffsetLastLoggedIn, string orderBy, int pageSize, int pageNumber)
        {
            return userDao.GetUsersBirthdaysWithFriendsList(timeSpanOccuringInNextNumDays, timeSpanOffsetLastLoggedIn, orderBy, pageSize, pageNumber);
        }

        /// <summary>
        /// Insert Message
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertMessage(Message message)
        {
            int ret = 0;
            if (IsUserBlocked(message.ToId, message.FromId))
            {
                ret = -99;
            }
            else
            {
                ret = userDao.InsertMessage(message);
                if (ret == 0)
                {
                    SendNewMessageEventToPlayer(message);
                }
            }
            return ret;
        }

        public void SendNewMessageEventToPlayer(Message message)
        {
            // Get this one time, used several times below
            UserFacade userFacade = new UserFacade();
            XmlEventCreator xml = new XmlEventCreator("ClientTickler");

            User sender = null;
            User receiver = null;

            sender = userFacade.GetUser(message.FromId);
            receiver = userFacade.GetUser(message.ToId);

            int messageType = (int)NotificationsProfile.NotificationTypes.NewMessage;
            if (message.Type == (int)eMESSAGE_TYPE.COMMUNITY_INVITES)
            {
                messageType = (int)NotificationsProfile.NotificationTypes.WebWorldInvite;
            }

            // If user has never logged onto wok, no need to send event
            if (receiver.WokPlayerId > 0)
            {
                xml.AddValue(messageType);
                xml.AddValue(sender.Username);
                xml.AddValue(sender.UserId);
                xml.AddValue(sender.ThumbnailPath);

                xml.AddPlayer(receiver.WokPlayerId);

                try
                {
                    NotificationsProfile np = new NotificationsProfile(0, message.ToId, messageType, xml.GetXml(), "");
                    userFacade.InsertUserNotificationProfile(np);
                }
                catch (Exception e)
                {
                    m_logger.Error("Error inserting notification profile", e);
                }

                try
                {
                    RemoteEventSender res = new RemoteEventSender();
                    res.BroadcastEventToServer(xml.GetXml()); //send event to all servers
                }
                catch (Exception e)
                {
                    m_logger.Error("Error Broadcasting ClientTickler to WoK Server ", e);
                }
            }
        }

        public int SendInWorldInviteToPlayer(string fromUsername, string toUsername, string message, string url, string trackingMetric)
        {
            UserFacade userFacade = new UserFacade();
            User sender = userFacade.GetUser(fromUsername);
            User receiver = userFacade.GetUser(toUsername);
            if (sender != null && receiver != null)
            {
                if (receiver.WokPlayerId > 0)// && (receiver.Ustate == User.ONLINE_USTATE_INWORLD || receiver.Ustate == User.ONLINE_USTATE_ONINWORLD))
                {
                    XmlEventCreator xml = new XmlEventCreator("ClientTickler");
                    xml.AddValue((int)NotificationsProfile.NotificationTypes.InWorldInvite);
                    xml.AddValue(sender.Username);
                    xml.AddValue(sender.UserId);
                    xml.AddValue(sender.ThumbnailPath);
                    xml.AddValue(message);
                    xml.AddValue(url);
                    xml.AddValue(trackingMetric);

                    xml.AddPlayer(receiver.WokPlayerId);

                    try
                    {
                        NotificationsProfile np = new NotificationsProfile(0, receiver.UserId, (int) NotificationsProfile.NotificationTypes.GoToURL, xml.GetXml(), "");
                        userFacade.InsertUserNotificationProfile(np);
                    }
                    catch (Exception e)
                    {
                        m_logger.Error("Error inserting notification profile", e);
                    }

                    try
                    {
                        RemoteEventSender res = new RemoteEventSender();
                        res.BroadcastEventToServer(xml.GetXml()); //send event to all servers
                        return 0;
                    }
                    catch (Exception e)
                    {
                        m_logger.Error("Error Broadcasting ClientTickler to WoK Server ", e);
                    }
                }
            }
            return -1;
        }

        public int SendInWorldGiftInviteToPlayer(string fromUsername, string toUsername, string message, string url, string trackingMetric)
        {
            UserFacade userFacade = new UserFacade();
            User sender = userFacade.GetUser(fromUsername);
            User receiver = userFacade.GetUser(toUsername);
            if (sender != null && receiver != null)
            {
                if (receiver.WokPlayerId > 0)// && (receiver.Ustate == User.ONLINE_USTATE_INWORLD || receiver.Ustate == User.ONLINE_USTATE_ONINWORLD))
                {
                    XmlEventCreator xml = new XmlEventCreator("ClientTickler");
                    xml.AddValue((int)NotificationsProfile.NotificationTypes.InWorldGiftInvite);
                    xml.AddValue(sender.Username);
                    xml.AddValue(sender.UserId);
                    xml.AddValue(sender.ThumbnailPath);
                    xml.AddValue(message);
                    xml.AddValue(url);
                    xml.AddValue(trackingMetric);

                    xml.AddPlayer(receiver.WokPlayerId);

                    try
                    {
                        NotificationsProfile np = new NotificationsProfile(0, receiver.UserId, (int) NotificationsProfile.NotificationTypes.GiftInvite, xml.GetXml(), "");
                        userFacade.InsertUserNotificationProfile(np);
                    }
                    catch (Exception e)
                    {
                        m_logger.Error("Error inserting notification profile", e);
                    }

                    try
                    {
                        RemoteEventSender res = new RemoteEventSender();
                        res.BroadcastEventToServer(xml.GetXml()); //send event to all servers
                        m_logger.Debug("Sent InWorldGiftInvite to Player=" + toUsername);
                        return 0;
                    }
                    catch (Exception e)
                    {
                        m_logger.Error("Error Broadcasting ClientTickler to WoK Server ", e);
                    }
                }
            }
            return -1;
        }

        public bool SendKIMInviteToPlayer(string fromUsername, string toUsername, string message, string url)
        {
            bool ret = false;

            if (!string.IsNullOrEmpty(fromUsername) && !string.IsNullOrEmpty(toUsername))
            {
                string siteurl = string.Empty;
                string postData = string.Empty;

                try
                {
                    string server = System.Configuration.ConfigurationManager.AppSettings["KIM.server"];
                    string port = System.Configuration.ConfigurationManager.AppSettings["KIM.port"];
                    string adminSite = System.Configuration.ConfigurationManager.AppSettings["KIM.admin"];
                    string adminUser = System.Configuration.ConfigurationManager.AppSettings["KIM.admin_user"];
                    string adminPass = System.Configuration.ConfigurationManager.AppSettings["KIM.admin_pass"];

                    siteurl = server;
                    siteurl += ":" + port;
                    siteurl += adminSite;
                    siteurl += fromUsername;

                    WebRequest request = WebRequest.Create(siteurl);
                    request.Method = "POST";

                    message += url;
                    message = HttpUtility.UrlEncode(message);
                    postData = @"sendimfrom_arg_recipient=" + toUsername;
                    postData += @"&sendimfrom_arg_message=";
                    postData += message;
                    postData += "&sendimfrom_arg_ishtml=true&sendimfrom=Send";

                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                    request.ContentType = "application/x-www-form-urlencoded";

                    request.ContentLength = byteArray.Length;
                    request.Credentials = new NetworkCredential(adminUser, adminPass);

                    Stream dataStream = request.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    dataStream.Close();
                    WebResponse response = request.GetResponse();

                    dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);

                    string responseFromServer = reader.ReadToEnd();
                    m_logger.Debug( "KIM Responded " + responseFromServer );
                    reader.Close();
                    dataStream.Close();
                    response.Close();
                    ret = true;
                }
                catch (Exception e)
                {
                    m_logger.Error("Error sending invite to KIM server. URL:" + siteurl + " POST data:" + postData, e);
                    ret = false;
                }
            }

            return ret;
        }

        public int SendEventReminderToPlayer(string fromUsername, string toUsername, string message, string eventName, string url, string trackingMetric)
        {
            UserFacade userFacade = new UserFacade();
            User sender = userFacade.GetUser(fromUsername);
            User receiver = userFacade.GetUser(toUsername);
            if (sender != null && receiver != null)
            {
                if (receiver.WokPlayerId > 0)// && (receiver.Ustate == User.ONLINE_USTATE_INWORLD || receiver.Ustate == User.ONLINE_USTATE_ONINWORLD))
                {
                    XmlEventCreator xml = new XmlEventCreator("ClientTickler");
                    xml.AddValue((int)NotificationsProfile.NotificationTypes.EventReminder);
                    xml.AddValue(sender.Username);
                    xml.AddValue(sender.UserId);
                    xml.AddValue(sender.ThumbnailPath);
                    xml.AddValue(message);
                    xml.AddValue(eventName);
                    xml.AddValue(url);
                    xml.AddValue(trackingMetric);

                    xml.AddPlayer(receiver.WokPlayerId);

                    try
                    {
                        NotificationsProfile np = new NotificationsProfile(0, receiver.UserId, (int) NotificationsProfile.NotificationTypes.EventReminder, xml.GetXml(), "");
                        userFacade.InsertUserNotificationProfile(np);
                    }
                    catch (Exception e)
                    {
                        m_logger.Error("Error inserting notification profile", e);
                    }

                    try
                    {
                        RemoteEventSender res = new RemoteEventSender();
                        res.BroadcastEventToServer(xml.GetXml()); //send event to all servers
                        return 0;
                    }
                    catch (Exception e)
                    {
                        m_logger.Error("Error Broadcasting ClientTickler to WoK Server ", e);
                    }
                }
            }
            return -1;
        }

        public int SendCreditGiftNotificationToPlayer(int fromUserId, int toUserId, double creditAmount)
        {
            User sender = GetUser(fromUserId);
            User receiver = GetUser(toUserId);
            if (sender != null && receiver != null)
            {
                if (receiver.WokPlayerId > 0)
                {
                    XmlEventCreator xml = new XmlEventCreator("ClientTickler");
                    xml.AddValue((int)NotificationsProfile.NotificationTypes.ReceivedCreditGift);
                    xml.AddValue(sender.Username);
                    xml.AddValue(sender.UserId);
                    xml.AddValue(sender.ThumbnailPath);
                    xml.AddValue(string.Format("{0} gave you {1} {2}.", sender.Username, creditAmount, (creditAmount == 1 ? "credit" : "credits")));

                    xml.AddPlayer(receiver.WokPlayerId);

                    try
                    {
                        NotificationsProfile np = new NotificationsProfile(0, receiver.UserId, (int)NotificationsProfile.NotificationTypes.ReceivedCreditGift, xml.GetXml(), "");
                        InsertUserNotificationProfile(np);
                    }
                    catch (Exception e)
                    {
                        m_logger.Error("Error inserting notification profile", e);
                    }

                    try
                    {
                        RemoteEventSender res = new RemoteEventSender();
                        res.BroadcastEventToServer(xml.GetXml()); //send event to all servers
                        m_logger.Debug("Sent ReceivedCreditGift to Player=" + receiver.Username);
                        return 0;
                    }
                    catch (Exception e)
                    {
                        m_logger.Error("Error Broadcasting ClientTickler to WoK Server ", e);
                    }
                }
            }
            return -1;
        }

        /// <summary>
        /// Is User Blocked
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool IsUserBlocked(int userId, int blockedUserId)
        {   //**BLOCK -- remove after test
            //            if (IsUserAdministrator () || IsUserCSR ())
            //            {
            //                return false;
            //            }
            return userDao.IsUserBlocked(userId, blockedUserId);
        }

        /// <summary>
        /// Is User Blocked from community or game
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool IsUserBlocked(int userId, int gameId, int communityId)
        {   //**BLOCK -- remove after test
            //            if (IsUserAdministrator () || IsUserCSR ())
            //           {
            //              return false;
            //            }
            return userDao.IsUserBlocked(userId, gameId, communityId);
        }

        /// <summary>
        /// Unblocks user
        /// </summary>
        public int UnBlockUser(int userId, int blockedUserId, int blockSourceId = 0)
        {   //**BLOCK -- remove after test
            int result = 0;

            if (IsUserBlocked(userId, blockedUserId))
            {
                result = userDao.DeleteBlockedUser(userId, blockedUserId);

                // send update to kim
                if (result > 0 && IsUserOnlineInKIM(userId))
                {
                    string username = GetUserName(userId);
                    string blockedUsername = GetUserName(blockedUserId);
                    SendKIMInviteToPlayer(username, username, blockedUsername + " has been unblocked and removed from your blocked list.", string.Empty);
                }
            }

            return result;
        }

        /// <summary>
        /// blocks user
        /// </summary>
        public int BlockUser(int userId, int blockedUserId, int blockSourceId)
        {   //**BLOCK -- remove after test
            int result = 0;

            // Can't block self
            if (userId != blockedUserId && !IsUserGM(blockedUserId))
            {
                if (!IsUserBlocked(userId, blockedUserId))
                {
                    result = userDao.InsertUserToBlock(userId, blockedUserId, blockSourceId);

                    if (result > 0)
                    {
                        // remove user as friend
                        userDao.DeleteFriend(userId, blockedUserId);

                        // remove blocked user from all of blockers worlds 
                        communityDao.RemoveMemberFromAllOwnersWorlds(userId, blockedUserId);

                        // send update to kim
                        if (IsUserOnlineInKIM(userId))
                        {
                            string username = GetUserName(userId);
                            string blockedUsername = GetUserName(blockedUserId);
                            SendKIMInviteToPlayer(username, username, blockedUsername + " has been blocked and added to your blocked list.", string.Empty);
                        }
                    }
                }
            }

            return result;
        }

        public PagedDataTable GetBlockedUsers(int userId, string orderBy, int pageNumber, int pageSize)
        {
            return userDao.GetBlockedUsers(userId, orderBy, pageNumber, pageSize);
        }

        public bool IsUserOnlineInKIM(int userId)
        {
            return userDao.IsUserOnlineInKIM(userId);
        }

        /// <summary>
        /// GetCrew
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Crew GetCrew(int userId)
        {
            return userDao.GetCrew(userId);
        }

        /// <summary>
        /// AddCrewPeep
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddCrewPeep(int crewOwnerId, int peepUserId)
        {
            // See if we need to insert a crew first
            Crew crew = userDao.GetCrew(crewOwnerId);

            if (crew.CrewId.Equals(0))
            {
                crew.UserId = crewOwnerId;
                crew.CrewId = userDao.AddCrew(crew);
            }

            CrewPeep crewPeep = new CrewPeep(crew.CrewId, peepUserId, DateTime.Now);
            return userDao.AddCrewPeep(crewPeep);
        }

        /// <summary>
        /// AddItemToPendingInventory
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public void AddItemToPendingInventory(int userId, string inventory_type, int global_id, int quantity)
        {
            AddItemToPendingInventory(userId, inventory_type, global_id, quantity, 256);
        }

        /// <summary>
        /// AddItemToPendingInventory
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public void AddItemToPendingInventory(int userId, string inventory_type, int global_id, int quantity, int inventorySubType)
        {
            userDao.AddItemToPendingInventory(userId, inventory_type, global_id, quantity, inventorySubType);
        }

        /// <summary>
        /// Get a game inventories sync record
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataRow GetGameInventoriesSync(int gameId, int userId, string inventory_type, int global_id, int inventorySubType)
        {
            return userDao.GetGameInventoriesSync(gameId, userId, inventory_type, global_id, inventorySubType);
        }

        /// <summary>
        /// AddItemToGameInventoriesSync
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public void AddItemToGameInventoriesSync(int gameId, int userId, string inventory_type, int global_id, int quantity, int inventorySubType)
        {
            userDao.AddItemToGameInventoriesSync(gameId, userId, inventory_type, global_id, quantity, inventorySubType);
        }

        /// <summary>
        /// AddItemToPendingInventory
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public void InsertRewardLog(int userId, double amount, string keiPointId, int transTypeId, int rewardEventId, int wokTransLogId)
        {
            userDao.InsertRewardLog(userId, amount, keiPointId, transTypeId, rewardEventId, wokTransLogId);
        }

        /// <summary>
        /// IsUserAdministrator
        /// </summary>
        public bool IsUserAdministrator()
        {
            return HttpContext.Current.User.IsInRole(User.ROLE_ADMIN);
        }

        /// <summary>
        /// Is the current user adult?
        /// </summary>
        /// <returns></returns>
        public bool IsUserAdult(User user)
        {
            // If they are logged in and over 18, we can show the adult communities
            return (user.Age >= 18);
        }


        /// <summary>
        /// IsUserCSR
        /// </summary>
        public bool IsUserCSR()
        {
            return HttpContext.Current.User.IsInRole(User.ROLE_CSR);
        }

        /// <summary>
        /// IsUserCSR
        /// </summary>
        public bool IsUserGM(int userId)
        {
            bool isAdmin = false;
            bool isGm = false;
            DateTime secondTolastLogin;
            userDao.GetPlayerAdmin(userId, out isAdmin, out isGm, out secondTolastLogin);
            return isGm;
        }

        /// <summary>
        /// Checks to see if a user is an automated test account.
        /// </summary>
        public bool IsUserAutomatedTestAccount(User user)
        {
            return user.Role == (int)SiteRole.KanevaRoles.Automated_Test_Account;
        }

        /// <summary>
        /// Gets a list of States
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<State> GetStates()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return userDao.GetStates();
        }

        /// <summary>
        /// Gets a list of Countries
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<Country> GetCountries()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return userDao.GetCountries();
        }

        /// <summary>
        /// UpdatePlayerTitle
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertPlayerTitle(int playerId, string title, int sortOrder)
        {
            return userDao.InsertPlayerTitle(playerId, title, sortOrder);
        }

        /// <summary>
        /// UpdatePlayerTitle
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public void ChangePlayerNameColor(int userId, int color)
        {
            userDao.ChangePlayerNameColor(userId, color);
        }


        /// <summary>
        /// InsertNewUser
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertUser(string username, string password, string salt, int role, int accountType,
            string first_name, string last_name, string display_name, string gender, string homepage, string email, DateTime birthdate,
            string key_value, string country, string zipCode, string registrationKey, string ipAddress, int validationState, int joinSourceId)
        {
            // Make sure the username does not already exist
            if (UsernameExists(username))
            {
                return -1;
            }

            // Make sure the email does not already exist?
            if (EmailExists(email))
            {
                return -2;
            }

            return userDao.InsertUser(username, password, salt, role, accountType, first_name, last_name, display_name, gender, homepage, email, birthdate, key_value, country,
                zipCode, registrationKey, ipAddress, validationState, joinSourceId);
        }

        /// <summary>
        /// Creates the default home page for a newly registered user. Places a set of default
        /// widgets on the page.
        /// </summary>
        /// <returns>bool - true on success</returns>
        public bool CreateDefaultUserHomePage(string username, int userId)
        {
            if (userId.Equals(0))
            {
                return false;
            }

            try
            {
                CommunityFacade communityFacade = new CommunityFacade();

                //create a personal channel
                Community profile = new Community();
                profile.PlaceTypeId = 0;
                profile.Name = username;
                profile.NameNoSpaces = username.Replace(" ", "");
                profile.Description = "NEW";
                profile.CreatorId = userId;
                profile.CreatorUsername = username;
                profile.IsPublic = "Y";
                profile.IsAdult = "N";
                profile.IsPersonal = 1; //true
                profile.CommunityTypeId = (int)CommunityType.USER;
                profile.StatusId = (int)CommunityStatus.ACTIVE;

                int profileId = communityFacade.InsertCommunity(profile);

                //if profile successfully created add user as member
                if (profileId > 0)
                {
                    CommunityMember member = new CommunityMember();
                    member.CommunityId = profileId;
                    member.UserId = userId;
                    member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.OWNER;
                    member.Newsletter = "Y";
                    member.InvitedByUserId = 0;
                    member.AllowAssetUploads = "Y";
                    member.AllowForumUse = "Y";
                    member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

                    communityFacade.InsertCommunityMember(member);
                }

                // Give each user three friends groups: 
                FriendGroup friendGroup = new FriendGroup();
                friendGroup.OwnerId = userId;

                //create Family
                friendGroup.Name = "Family";
                InsertFriendGroup(friendGroup);

                //create Coworkers
                friendGroup.Name = "Coworkers";
                InsertFriendGroup(friendGroup);

                //create Inner Circle
                friendGroup.Name = "Inner Circle";
                InsertFriendGroup(friendGroup);

                return true;
            }
            catch (Exception e)
            {
                m_logger.Error(e.ToString());
            }

            return false;
        }

        /// <summary>
        /// Creates the default home world for a newly registered user. 
        /// </summary>
        /// <returns>bool - true on success</returns>
        public bool CreateDefaultUserHomeWorld(string username, int userId)
        {
            if (userId.Equals(0))
            {
                return false;
            }

            try
            {
                CommunityFacade communityFacade = new CommunityFacade();

                string worldName = username + " World";
                int worldNameIndex = 0;
                bool invalidNameFound = communityFacade.IsCommunityNameTaken(false, worldName, 0);

                while (invalidNameFound)
                {
                    worldNameIndex++;
                    invalidNameFound = communityFacade.IsCommunityNameTaken(false, worldName + " " + worldNameIndex, 0);
                }

                //create a personal channel
                Community homeWorld = new Community();
                homeWorld.PlaceTypeId = 0;
                homeWorld.Name = worldName;
                homeWorld.NameNoSpaces = worldName.Replace(" ", "");
                homeWorld.Description = username + " Home World";
                homeWorld.CreatorId = userId;
                homeWorld.CreatorUsername = username;
                homeWorld.IsPublic = "Y";
                homeWorld.IsAdult = "N";
                homeWorld.IsPersonal = 0; //false
                homeWorld.CommunityTypeId = (int)CommunityType.HOME;
                homeWorld.StatusId = (int)CommunityStatus.ACTIVE;

                int communityId = communityFacade.InsertCommunity(homeWorld);
                bool success = (communityId > 0);

                //if profile successfully created add user as member and set as home community
                if (success)
                {
                    CommunityMember member = new CommunityMember();
                    member.CommunityId = communityId;
                    member.UserId = userId;
                    member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.OWNER;
                    member.Newsletter = "Y";
                    member.InvitedByUserId = 0;
                    member.AllowAssetUploads = "Y";
                    member.AllowForumUse = "Y";
                    member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

                    communityFacade.InsertCommunityMember(member);

                    UpdateUserHomeCommunityId(userId, communityId);
                }

                return success;
            }
            catch (Exception e)
            {
                m_logger.Error(e.ToString());
            }

            return false;
        }

        /// <summary>
        /// UpdateUserAccount
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateUserAccount(int userId, string firstName, string lastName, string email)
        {
            userDao.UpdateUserAccount(userId, firstName, lastName, email);
        }

        /// <summary>
        /// UpdateUserAccount
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateUserAccount(int userId, string displayName, string email, string country, string zipCode)
        {
            userDao.UpdateUserAccount(userId, displayName, email, country, zipCode);
        }

        /// <summary>
        /// GetUserPreferences
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Preferences GetUserPreferences(int userId)
        {
            return userDao.GetUserPreferences(userId);
        }

        /// <summary>
        /// UpdateUserPreferences
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateUserPreferences(Preferences preferences)
        {
            return userDao.UpdateUserPreferences(preferences);
        }

        /// <summary>
        /// GetUserNotificationPreferences
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public NotificationPreferences GetUserNotificationPreferences(int userId)
        {
            return userDao.GetUserNotificationPreferences(userId);
        }

        /// <summary>
        /// UpdateUserNotificationPreferences
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateUserNotificationPreferences(NotificationPreferences notificationPreferences)
        {
            return userDao.UpdateUserNotificationPreferences(notificationPreferences);
        }

        /// <summary>
        /// GetUserBalances
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public UserBalances GetUserBalances(int userId)
        {
            return userDao.GetUserBalances(userId);
        }

        /// <summary>
        /// GetUserBalances
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public UserBalances GetUserBalancesCached(int userId)
        {
            return userDao.GetUserBalancesCached(userId);
        }




        /// <summary>
        /// UpdateUserName
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateUserName(int userId, string oldName, string newName)
        {
            return userDao.UpdateUserName(userId, oldName, newName);
        }

        /// <summary>
        /// UpdatePlayerTitle
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdatePlayerTitle(int playerId, string title)
        {
            userDao.UpdatePlayerTitle(playerId, title);
        }

        /// <summary>
        /// UpdateUserHomeCommunityId
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateUserHomeCommunityId(int userId, int communityId)
        {
            return userDao.UpdateUserHomeCommunityId(userId, communityId);
        }

        /// <summary>
        /// GetPlayerTitleByFameType
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public string GetPlayerTitleByFameType(int userId, int fameTypeId)
        {
            return userDao.GetPlayerTitleByFameType(userId, fameTypeId);
        }

        /// <summary>
        /// InsertUserSourceAcquisitionUrl
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertUserSourceAcquisitionUrl(int userId, string fullUrl, string hostName, int sourceTypeId)
        {
            return userDao.InsertUserSourceAcquisitionUrl(userId, fullUrl, hostName, sourceTypeId);
        }

        /// <summary>
        /// UsernameExists
        /// </summary>
        /// <param name="strUsername"></param>
        /// <returns></returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool UsernameExists(string strUsername)
        {
            return userDao.UsernameExists(strUsername);
        }

        /// <summary>
        /// EmailExists
        /// </summary>
        /// <param name="strEmail"></param>
        /// <returns></returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool EmailExists(string strEmail)
        {
            return userDao.EmailExists(strEmail);
        }

        /// <summary>
        /// EmailExists
        /// </summary>
        /// <param name="strEmail"></param>
        /// <returns></returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool HasAssetDeleteRights(int assetOwnerId, int userId)
        {
            // New rule is only asset owner may delete it!!!!
            if (IsUserAdministrator() || IsUserCSR())
            {
                return true;
            }
            else
            {
                return ((assetOwnerId == userId) ? true : false);
            }
        }

        /// <summary>
        /// LogSearch
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int LogSearch(int userId, string searchString, int searchType)
        {
            return userDao.LogSearch(userId, searchString, searchType);
        }

        /// <summary>
        /// SearchConvert
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int SearchConvert (int logId)
        {
            return userDao.SearchConvert(logId);
        }

        /// <summary>
        /// GetLoginAttempts
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<UserLoginIssue> GetLoginAttempts(DateTime dtStartDate, DateTime dtEndDate, string orderBy, int pageNumber, int pageSize)
        {
            return userDao.GetLoginAttempts(dtStartDate, dtEndDate, orderBy, pageNumber, pageSize);
        }

        /// <summary>
        /// InsertUserLoginIssue
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertUserLoginIssue(int userId, string ipAddress, string description, string errorType, string serverType)
        {
            return userDao.InsertUserLoginIssue(userId, ipAddress, description, errorType, serverType);
        }

        /// <summary>
        /// InsertUserLoginIssue
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertKIMLoginLog(int userId, string servername, string ipAddress)
        {
            return userDao.InsertKIMLoginLog(userId, servername, ipAddress);
        }


        /// <summary>
        /// get a dataset of player data to return to a server
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataSet GetPlayerData(int kanevaUserId, int flags, int gameId)
        {
            return userDao.GetPlayerData(kanevaUserId, flags, gameId);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataSet GetPlayListData(int zoneIndex, int zoneInstanceId, int playListId, int playerId)
        {
            return userDao.GetPlayListData(zoneIndex, zoneInstanceId, playListId, playerId);
        }

        /// <summary>
        /// details about the player
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public void GetPlayerAdmin(int kanevaUserId, out bool isAdmin, out bool isGm, out DateTime secondTolastLogin)
        {
            userDao.GetPlayerAdmin(kanevaUserId, out isAdmin, out isGm, out secondTolastLogin);
        }

        /// <summary>
        /// details about the player
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetPlayerLoginCount(int userId)
        {
            return userDao.GetPlayerLoginCount(userId);
        }

        /// <summary>
        /// details about the player
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataRow GetUserLastLoginByGame(int userId, int gameId)
        {
            return userDao.GetUserLastLoginByGame(userId, gameId);
        }

        /// <summary>
        /// GetUserFirstWokLogin
        /// </summary>
        /// <returns>The date the user first logged into WoK, or DateTime.MaxValue if user has never logged in</returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DateTime GetUserFirstWokLogin(int userId)
        {
            return userDao.GetUserFirstWokLogin(userId);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public Dictionary<int, bool> AreUsersOnlineInWoK(int[] userIds)
        {
            return userDao.AreUsersOnlineInWoK(userIds);
        }

        /// <summary>
        /// GetCommunityOwnerByCommunityId
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public User GetCommunityOwnerByCommunityId(int communityId)
        {
            return userDao.GetCommunityOwnerByCommunityId(communityId);
        }


        /// <summary>
        /// GetUserMessages
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<UserMessage> GetUserMessages(int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            return userDao.GetUserMessages(userId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// GetSentMessages
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<UserMessage> GetSentMessages(int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            return userDao.GetSentMessages(userId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// GetUserMessage
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public UserMessage GetUserMessage(int userId, int messageId)
        {
            return userDao.GetUserMessage(userId, messageId);
        }

        // **********************************************************************************************
        // Update User Functions
        // **********************************************************************************************
        #region UpdateUser Functions

        /// <summary>
        /// Gets a list of users
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<KanevaColor> GetAvailableNameColors()
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return userDao.GetAvailableNameColors();
        }

        /// <summary>
        /// Gets a list of users
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetUsersNameColor(int wokPlayerId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return userDao.GetUsersNameColor(wokPlayerId);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataRow GetUsersNameColorRGB(int userId)
        {
            return userDao.GetUsersNameColorRGB(userId);
        }

        /// <summary>
        /// Update UpdateUserEmailSecurity
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateUsersNameColor(int userId, int nameColorId)
        {
            return userDao.UpdateUsersNameColor(userId, nameColorId);
        }

        /// <summary>
        /// Update UpdateUserEmailSecurity
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateUserEmailSecurity(int userId, string showEmail)
        {
            userDao.UpdateUserEmailSecurity(userId, showEmail);
        }

        /// <summary>
        /// Update user
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateUser(int userId, int showMature, int browseAnon, int showOnline, string newsletter,
            int notifyBlogComments, int notifyProfileComments, int notifyFriendMessages, int notifyAnyoneMessages,
            int notifyFriendRequests, int notifyNewFriends, int friendsCanComment, int everyoneCanComment, int matureProfile, int receiveUpdates)
        {
            userDao.UpdateUser(userId, showMature, browseAnon, showOnline, newsletter,
             notifyBlogComments, notifyProfileComments, notifyFriendMessages, notifyAnyoneMessages,
             notifyFriendRequests, notifyNewFriends, friendsCanComment, everyoneCanComment, matureProfile, receiveUpdates);
        }

        //**        public void UpdateUser (int userId, int showMature, int browseAnon, int showOnline, 
        //**            int friendsCanComment, int everyoneCanComment, int matureProfile)
        //**        {
        //**            userDao.UpdateUser (userId, showMature, browseAnon, showOnline, 
        //**             friendsCanComment, everyoneCanComment, matureProfile, receiveUpdates);
        //**        }


        /// <summary>
        /// Update user
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateUser(int userId, int showMature)
        {
            userDao.UpdateUser(userId, showMature);
        }


        /// <summary>
        /// Update user unsubscribe
        /// </summary>   
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateUserNoCommunication(int userId, string keyvalue)
        {
            userDao.UpdateUserNoCommunication(userId, keyvalue);
        }

        /// <summary>
        /// Update blast preferences
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateBlastPreferences(int userId, int blastPreferences, int blastShowPreferences)
        {
            userDao.UpdateBlastPreferences(userId, blastPreferences, blastShowPreferences);
        }

        /// <summary>
        /// Add Blast Block
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddBlastBlock(int userId, int blockedUserId, int blockedCommunityId)
        {
            return userDao.AddBlastBlock(userId, blockedUserId, blockedCommunityId);
        }

        /// <summary>
        /// Delete Blast Block
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteBlastBlock(int userId, int blockedUserId, int blockedCommunityId)
        {
            return userDao.DeleteBlastBlock(userId, blockedUserId, blockedCommunityId);
        }

        /// <summary>
        /// Get Blast Blocks
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<BlastBlock> GetBlastBlocks(int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            return userDao.GetBlastBlocks(userId, filter, orderby, pageNumber, pageSize);
        }


        /// <summary>
        /// Is Blast Blocked
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool IsBlastBlocked(int recipientId, int blockedUserId, int blockedCommunityId)
        {
            return userDao.IsBlastBlocked(recipientId, blockedUserId, blockedCommunityId);
        }


        /// <summary>
        /// Update user identity
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateUserIdentityCSR(int userId, string firstName, string lastName, string description, string gender,
            string homepage, string email, bool bShowEmail, bool bShowMature, string country, string zipCode, DateTime birthDate, string displayName)
        {
            userDao.UpdateUserIdentityCSR(userId, firstName, lastName, description, gender,
              homepage, email, bShowEmail, bShowMature, country, zipCode, birthDate, displayName);
        }

        /// <summary>
        /// Update a user status
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateUserStatus(int userId, int userStatus)
        {
            userDao.UpdateUserStatus(userId, userStatus);
        }

        /// <summary>
        /// Marks a user account as deleted, along with the user's home world and profile.
        /// This is intended to be recoverable, so nothing is actually deleted, and no
        /// assets, dynamic objects, members, friends, etc. associated with the account
        /// are removed.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void MarkUserDeletedRecoverable(int userId, int userStatus)
        {
            CommunityFacade communityFacade = new CommunityFacade();

            userDao.UpdateUserStatus(userId, userStatus);

            int profileId = GetPersonalChannelId(userId);
            if (profileId > 0)
            {
                communityFacade.DeleteCommunityRecoverable(profileId);
            }

            int homeWorldId = GetUserHomeCommunityIdByUserId(userId);
            if (homeWorldId > 0)
            {
                communityFacade.DeleteCommunityRecoverable(homeWorldId);
            }
        }

        /// <summary>
        /// Recover a user account that has been marked deleted.  Restores both the 
        /// profile page and home world of the user.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void RecoverDeletedUser(int userId, int userStatus)
        {
            CommunityFacade communityFacade = new CommunityFacade();

            userDao.UpdateUserStatus(userId, userStatus);

            int profileId = GetPersonalChannelId(userId);
            if (profileId > 0)
            {
                communityFacade.RestoreDeletedCommunity(profileId);
            }

            int homeWorldId = GetUserHomeCommunityIdByUserId(userId);
            if (homeWorldId > 0)
            {
                communityFacade.RestoreDeletedCommunity(homeWorldId);
            }
        }

        /// <summary>
        /// Update a users password
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdatePassword(int userId, string password, string salt)
        {
            return userDao.UpdatePassword(userId, password, salt);
        }

        /// <summary>
        /// InvalidateKanevaUserCache
        /// </summary>
        /// <param name="userId"></param>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void InvalidateKanevaUserCache(int userId)
        {
            userDao.InvalidateKanevaUserCache(userId);
        }

        /// <summary>
        /// Update last login and number of logins
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateLastLogin(int userId, string ipAddress, string serverName)
        {
            FameFacade fameFacade = new FameFacade();
            fameFacade.RedeemPacket(userId, (int)PacketId.WEB_DAILY_SIGNIN, (int)FameTypes.World);
            return userDao.UpdateLastLogin(userId, ipAddress, serverName);
        }

        /// <summary>
        /// Update online status
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateOnlineStatus(int userId, int online, string uState, int loginId)
        {
            userDao.UpdateOnlineStatus(userId, online, uState, loginId);
        }

        /// <summary>
        /// Update last ip of the user
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateLastIP(int userId, string ipAddress)
        {
            userDao.UpdateLastIP(userId, ipAddress);
        }

        /// <summary>
        /// Update number of failed logins
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateFailedLogins(string email)
        {
            User user = userDao.GetUserByEmail(email);
            if (user.UserId.Equals(0))
            {
                return;
            }

            userDao.UpdateFailedLogins(user.UserId);
        }

        /// <summary>
        /// Update users restriction setting
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateUserRestriction(int userId, int matureProfile)
        {
            userDao.UpdateUserRestriction(userId, matureProfile);
        }

        /// <summary>
        /// UpdateUserEmailStatus
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateUserEmailStatus(int userId, int emailStatus)
        {
            userDao.UpdateUserEmailStatus(userId, emailStatus);
        }

        /// <summary>
        /// Update user
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateUser(int userId, string description)
        {
            userDao.UpdateUser(userId, description);
        }

        /// <summary>
        /// GetFraudDetect
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public FraudDetect GetFraudDetect(int userId)
        {
            return userDao.GetFraudDetect(userId);
        }

        /// <summary>
        /// SaveFraudDetect
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void SaveFraudDetect(int userId, FraudDetect fraudDetect)
        {
            userDao.SaveFraudDetect(userId, fraudDetect);
        }

        #endregion

        #region User Profile/Personal Info

        /// <summary>
        /// Get a user info (stats)
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataRow GetUserPersonalInfo(int userId)
        {
            return userDao.GetUserPersonalInfo(userId);
        }

        /// <summary>
        /// Get a user profile for personal info page pulls non user object data
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataRow GetUserProfile(int userId)
        {
            return userDao.GetUserProfile(userId);
        }

        /// <summary>
        /// Get a user login records
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedDataTable GetUserLoginHistory(int userId, string orderby, int pageNumber, int pageSize)
        {
            return userDao.GetUserLoginHistory(userId, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// updates a users profile
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateUserProfile(int userId, string relationship, string orientation, string religion, string ethnicity, string children, string education,
            string income, int heightFeet, int heightInches, string smoke, string drink, string hometown)
        {

            //see if profile exists
            if (!userDao.DoesUserProfileExist(userId))
            {
                userDao.AddNewUserProfile(userId);
            }


            return userDao.UpdateUserProfile(userId, relationship, orientation, religion, ethnicity, children, education, income, heightFeet, heightInches, smoke, drink, hometown);
        }

        /// <summary>
        /// Get user Id, player id for game if it exists otherwise 0
        /// </summary>
        /// <returns></returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataRow GetUserIdPlayerIdFromUsername(string username)
        {
            return userDao.GetUserIdPlayerIdFromUsername(username);
        }

        /// <summary>
        /// returns the personal channel id for a given user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetPersonalChannelId(int userId)
        {
            return userDao.GetPersonalChannelId(userId);
        }

        #endregion


        // Tracking


        /// <summary>
        /// InsertTrackingRequest
        /// </summary>
        /// <returns>The request ID</returns>
        public string InsertTrackingRequest(int requestTypeId, int userIdSent, int gameId, int communityId)
        {
            // UserId <= 11 chars, Guid = 36 chars
            string requestId = (userIdSent.ToString() + Guid.NewGuid().ToString());

            // Insert into DB
            int result = userDao.InsertTrackingRequest(requestId, requestTypeId, userIdSent, gameId, communityId);

            if (result < 1)
            {
                return "ERROR";
            }

            return requestId;
        }

        /// <summary>
        /// InsertTrackingTypeSent
        /// </summary>
        /// <returns></returns>
        public string InsertTrackingTypeSent(string requestId, int messageTypeId, int userId)
        {
            // UserId <= 11 chars, Guid = 36 chars
            string requestTypesSentId = (userId.ToString() + Guid.NewGuid().ToString());

            int result = userDao.InsertTrackingTypeSent(requestTypesSentId, requestId, messageTypeId, userId);

            if (result < 1)
            {
                return "ERROR";
            }

            return requestTypesSentId;
        }

        /// <summary>
        /// AcceptTrackingRequest
        /// </summary>
        public int AcceptTrackingRequest(string requestTypeSentId, int userId, int acceptTypeId = 0, char isIE = 'N', string userAgent = "")
        {
            // Make sure RTSID is valid
            if (GetTrackingRequestType(requestTypeSentId) > 0)
            {
                userDao.AcceptTrackingRequest(requestTypeSentId, userId, acceptTypeId, isIE, userAgent);
            }
            return 0;
        }


        /// <summary>
        /// Convenience function used when generating requests. If requestId is null or empty, inserts a new request
        /// and returns. If requestId is not null, returns. Useful for simple checking to make sure multiple requests
        /// aren't inserted without resorting to database calls to check.
        /// </summary>
        /// <returns></returns>
        public string GetTrackingRequestId(string requestId, int requestTypeId, int userIdSent, int gameId, int communityId)
        {
            if (string.IsNullOrEmpty(requestId))
            {
                requestId = InsertTrackingRequest(requestTypeId, userIdSent, gameId, communityId);
            }

            return requestId;
        }

        /// <summary>
        /// GetTrackingRequestType
        /// </summary>
        public int GetTrackingRequestType(string requestTypeSentId)
        {
            return userDao.GetTrackingRequestType(requestTypeSentId);
        }

        // System Configuration
        public Graph<int, User> GetUserAltsGraph(int userId)
        {
            return userDao.GetUserAltsGraph(userId);
        }

        public bool InsertUsersSystemIds(int userId, string systemId)
        {
            return userDao.InsertUsersSystemIds(userId, systemId);
        }

        /// <summary>
        /// Retrieves a list of users that are associated with a given system.
        /// </summary>
        public List<User> GetSystemUsers(string systemId)
        {
            return userDao.GetSystemUsers(systemId);
        }

        public List<UserSystem> GetUserSystems(int userId)
        {
            return userDao.GetUserSystems(userId);
        }

        public IList<UserSystemConfiguration> GetUserSystemConfiguationsBySystemName(string systemName)
        {
            return userDao.GetUserSystemConfiguationsBySystemName(systemName);
        }

        public UserSystemConfiguration GetUserSystemConfiguation(string systemConfigurationId)
        {
            return userDao.GetUserSystemConfiguation(systemConfigurationId);
        }

        public UserSystemConfiguration GetMostRecentConfigForSystem(string systemId)
        {
            return userDao.GetMostRecentConfigForSystem(systemId);
        }

        public UserSystemConfiguration InsertUserSystemConfiguration(string systemId, uint? cpus, uint? cpuMhz, ulong? sysMem, ulong? diskSize, uint? gpuX, uint? gpuY,
            uint? gpuHz, uint? gpuFmt, string dxVer, string cpuVer, string sysVer, string sysName, string net, string netMAC, string netType, string netHash, string gpu, 
            uint? cpuPhysCore, string cpuVendor, string cpuBrand, uint? cpuFamily, uint? cpuModel, uint? cpuStepping,
            uint? cpuSSE3, uint? cpuSSSE3, uint? cpuSSE4_1, uint? cpuSSE4_2, uint? cpuSSE4_A, uint? cpuSSE5, uint? cpuAVX, uint? cpuAVX2, uint? cpuFMA3, uint? cpuFMA4,
            string cpuCodeName)
        {
            return userDao.InsertUserSystemConfiguration(systemId, cpus, cpuMhz, sysMem, diskSize, gpuX, gpuY,
                gpuHz, gpuFmt, dxVer, cpuVer, sysVer, sysName, net, netMAC, netType, netHash, gpu, 
                cpuPhysCore, cpuVendor, cpuBrand, cpuFamily, cpuModel, cpuStepping,
                cpuSSE3, cpuSSSE3, cpuSSE4_1, cpuSSE4_2, cpuSSE4_A, cpuSSE5, cpuAVX, cpuAVX2, cpuFMA3, cpuFMA4, cpuCodeName);
        }

        public int UpdateSystemConfigurationLastUserId(string runtimeId)
        {
            return userDao.UpdateSystemConfigurationLastUserId(runtimeId);
        }

        public int InsertUserAgentData(int userId, string os, string browser, string useragent)
        {
            return userDao.InsertUserAgentData(userId, os, browser, useragent);
        }

        public DataRow GetUserLoggedInWorld(int userId)
        {
            return userDao.GetUserLoggedInWorld(userId);
        }

        public bool HasUserCompletedAvatarSelect(int wokPlayerId)
        {
            return userDao.HasUserCompletedAvatarSelect(wokPlayerId);
        }

        public PagedList<User> GetUsersByEmail(List<string> emails)
        {
            return userDao.GetUsersByEmail(emails);
        }

        public DataRow GetInvitePointAwards(int invitePointId)
        {
            return userDao.GetInvitePointAwards(invitePointId);
        }

        public UserMetaGameItemBalances GetUserMetaGameItemBalances(int userId)
        {
            return userDao.GetUserMetaGameItemBalances(userId);
        }

        public bool CreditUserGameItemBalances(int userId, IEnumerable<KeyValuePair<string, int>> itemAmounts)
        {
            UserMetaGameItemBalances balances = GetUserMetaGameItemBalances(userId);

            try
            {
                foreach (KeyValuePair<string, int> itemAmount in itemAmounts)
                {
                    balances.CreditMetaGameItemBalance(itemAmount.Key, itemAmount.Value);
                }
                return userDao.SaveUserMetaGameItemBalances(balances);
            }
            catch (Exception e)
            {
                m_logger.Error(e);
                return false;
            }
        }

        public int GetUserDailyTotalMGIConversions(int userId, string itemType, string currencyType = null)
        {
            DateTime start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 3, 0, 0);
            DateTime endDate = start.AddDays(1);
            DateTime end = new DateTime(endDate.Year, endDate.Month, endDate.Day, 2, 59, 59);

            return userDao.GetUserTotalMGIConversions(userId, itemType, start, endDate, currencyType);
        }

        public int GetUserDailyTotalMGIConversionAmount(int userId, string itemType, string currencyType = null)
        {
            DateTime start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 3, 0, 0);
            DateTime endDate = start.AddDays(1);
            DateTime end = new DateTime(endDate.Year, endDate.Month, endDate.Day, 2, 59, 59);

            return userDao.GetUserTotalMGIConversionAmount(userId, itemType, start, endDate, currencyType);
        }

        /// <summary>
        /// Converts specified MetaGameItem amounts into the desired currency.
        /// </summary>
        public bool ConvertMetaGameItemsToCurrency(int userId, string itemType, string currencyType, IList<KeyValuePair<string, int>> itemAmounts, out int dailyTotal, out int amtAvailableToConvert)
        {
            GameFacade gameFacade = new GameFacade();
            int totalNumToConvert = 0;
            int totalConversionAmount = 0;
            amtAvailableToConvert = 0;

            // Validate params.
            if (userId <= 0)
                throw new ArgumentException("Invalid userId.", "userId");

            if (string.IsNullOrWhiteSpace(itemType))
                throw new ArgumentException("Invalid itemType.", "itemType");

            if (!currencyType.Equals(Currency.CurrencyType.REWARDS) && !currencyType.Equals(Currency.CurrencyType.CREDITS))
                throw new ArgumentException("Invalid currencyType. Must be either GPOINT (Rewards) or KPOINT (Credits).", "userId");

            if (itemAmounts == null || itemAmounts.Count == 0)
                throw new ArgumentException("itemAmounts must not be null or empty.", "itemAmounts");

            // Grab the total amount (in the specified currency) that the user has converted for this item type today.
            dailyTotal = GetUserDailyTotalMGIConversionAmount(userId, itemType, currencyType);

            // Get the user's current meta game item balances.
            UserMetaGameItemBalances metaGameItemBalances = GetUserMetaGameItemBalances(userId);

            // Validate and try to convert each MetaGameItem.
            foreach (KeyValuePair<string, int> itemAmount in itemAmounts)
            {
                MetaGameItem item = gameFacade.GetMetaGameItem(itemAmount.Key);

                if (string.IsNullOrWhiteSpace(item.ItemName))
                    throw new ArgumentException("Invalid MetaGameItem name = " + (item.ItemName ?? "null"), "itemAmounts");

                if (!item.ItemType.Equals(itemType))
                    throw new ArgumentException("Invalid MetaGameItem type for item " + item.ItemName + " = " + item.ItemType + " should be " + itemType, "itemAmounts");

                if (itemAmount.Value <= 0)
                    throw new ArgumentException("Invalid amount for item " + item.ItemName + " = " + itemAmount.Value, "itemAmounts");

                // Coins are stored on a per-world basis, and are not stored in the normal balances table.
                // As such, Web does not need to manage the user's Coin balances.
                // For everything else, we need to adjust the user's balance for the item.
                string debitErrorMsg;
                if (!itemType.Equals("Coin") && !metaGameItemBalances.TryDebitMetaGameItemBalance(item.ItemName, itemAmount.Value, out debitErrorMsg))
                    throw new ArgumentException("Invalid amount for item " + item.ItemName + ". " + debitErrorMsg);

                // Make the conversion and update totals.
                int currencyValue = item.ConvertToCurrency(currencyType, itemAmount.Value, dailyTotal);
                if (currencyValue == 0)
                {
                    if (item.GetAmountAvailableToConvert(currencyType, totalConversionAmount) < item.GetConversionLimit(currencyType))
                        throw new ArgumentException("Unable to convert items. Total conversion amount exceeds daily conversion limit.");

                    throw new ArgumentException("Unable to convert item " + item.ItemName);
                }

                dailyTotal += currencyValue;
                totalNumToConvert += itemAmount.Value;
                totalConversionAmount += currencyValue;
                amtAvailableToConvert = item.GetAmountAvailableToConvert(currencyType, dailyTotal);
            }

            // Make the database conversion.
            return userDao.ConvertMetaGameItemToCurrency(userId, itemType, currencyType, totalNumToConvert, totalConversionAmount, metaGameItemBalances);
        }

        public bool ConvertMetaGameItemToCurrency(int userId, MetaGameItem item, string currencyType, int numToConvert, out int dailyTotal, out int amtAvailableToConvert)
        {
            List<KeyValuePair<string, int>> itemAmounts = new List<KeyValuePair<string, int>>();
            itemAmounts.Add(new KeyValuePair<string, int>(item.ItemName, numToConvert));

            return ConvertMetaGameItemsToCurrency(userId, item.ItemType, currencyType, itemAmounts, out dailyTotal, out amtAvailableToConvert);
        }

        /// <summary>
        /// Spends the specified MetaGameItem amounts and gives percentage of the reward value to the world owner.
        /// </summary>
        public bool SpendMetaGameItems(int userId, int zoneInstanceId, int zoneType, string itemType, IList<KeyValuePair<string, int>> itemAmounts,
            string gameItemName, string gameItemType)
        {
            GameFacade gameFacade = new GameFacade();
            uint totalItemsSpent = 0;
            uint totalRewardsValue = 0;

            // Validate params.
            if (userId <= 0)
                throw new ArgumentException("Invalid userId.", "userId");

            if (zoneInstanceId <= 0)
                throw new ArgumentException("Invalid zoneInstanceId.", "zoneInstanceId");

            if (zoneType <= 0)
                throw new ArgumentException("Invalid zoneType.", "zoneType");

            if (string.IsNullOrWhiteSpace(itemType))
                throw new ArgumentException("Invalid itemType.", "itemType");

            if (itemAmounts == null || itemAmounts.Count == 0)
                throw new ArgumentException("itemAmounts must not be null or empty.", "itemAmounts");

            int worldOwnerId = (new CommunityFacade()).GetWorldOwnerFromWorldIds(0, 0, 0, zoneInstanceId, zoneType);
            if (worldOwnerId <= 0)
                throw new ArgumentException("Unable to find world owner.");

            // Get the user's current meta game item balances.
            UserMetaGameItemBalances metaGameItemBalances = GetUserMetaGameItemBalances(userId);

            // Validate and try to convert each MetaGameItem.
            foreach (KeyValuePair<string, int> itemAmount in itemAmounts)
            {
                MetaGameItem item = gameFacade.GetMetaGameItem(itemAmount.Key);

                if (string.IsNullOrWhiteSpace(item.ItemName))
                    throw new ArgumentException("Invalid MetaGameItem name = " + (item.ItemName ?? "null"), "itemAmounts");

                if (!item.ItemType.Equals(itemType))
                    throw new ArgumentException("Invalid MetaGameItem type for item " + item.ItemName + " = " + item.ItemType + " should be " + itemType, "itemAmounts");

                if (itemAmount.Value <= 0)
                    throw new ArgumentException("Invalid amount for item " + item.ItemName + " = " + itemAmount.Value, "itemAmounts");

                // Coins are stored on a per-world basis, and are not stored in the normal balances table.
                // As such, Web does not need to manage the user's Coin balances.
                // For everything else, we need to adjust the user's balance for the item.
                string debitErrorMsg;
                if (!itemType.Equals("Coin") && !metaGameItemBalances.TryDebitMetaGameItemBalance(item.ItemName, itemAmount.Value, out debitErrorMsg))
                    throw new ArgumentException("Invalid amount for item " + item.ItemName + ". " + debitErrorMsg);

                // Make the conversion and update totals.
                int currencyValue = item.GetCurrencyValue(Currency.CurrencyType.REWARDS, itemAmount.Value);
                if (currencyValue == 0)
                    throw new ArgumentException("Unable to convert item " + item.ItemName);

                totalItemsSpent += (uint)itemAmount.Value;
                totalRewardsValue += (uint)currencyValue;
            }

            // Make the database transaction.
            return userDao.SpendMetaGameItems(userId, itemType, totalItemsSpent, totalRewardsValue, 
                zoneInstanceId, zoneType, gameItemName, gameItemType, worldOwnerId, metaGameItemBalances);
        }

        public int InsertUserNotificationProfile(NotificationsProfile notifcationsProfile)
        {
            return userDao.InsertUserNotificationProfile (notifcationsProfile);
        }

        /// <summary>
        /// Gets all Profile Notifications
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<NotificationsProfile> GetNotificationsProfile (int userId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return userDao.GetNotificationsProfile(userId);
        }

        /// <summary>
        /// Dismiss a profile notification
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int DismissNotficationProfile(int userId, uint notificationId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return userDao.DismissNotficationProfile(userId, notificationId);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public void InsertUserNote(int userId, int createdUserId, string note)
        {
            userDao.InsertUserNote(userId, createdUserId, note);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public void InsertUserNote(int userId, int createdUserId, string note, int wokTransLogId)
        {
            userDao.InsertUserNote(userId, createdUserId, note, wokTransLogId);
        }

        public bool SetSmokeTestUserLoginKey(string key, User user)
        {
            return CentralCache.Add(CentralCache.keySmokeTestUserLogin(key), user, new TimeSpan(0, 5, 0));
        }

        public User CheckSmokeTestUserLoginKey(string key)
        {
            User user = (User)(CentralCache.Get(CentralCache.keySmokeTestUserLogin(key)) ?? new User());
            if (user.UserId > 0)
            {
                CentralCache.Remove(CentralCache.keySmokeTestUserLogin(key));
            }
            return user;
        }

        /// <summary>
        /// Insert into developer authorization dropbox for developer.dat downloads
        /// </summary>
        /// [DataObjectMethod(DataObjectMethodType.Insert)]
        public string InsertDeveloperAuthorizationDropBox(int userId, string systemName, byte[] content, uint secondsToLive)
        {
            return userDao.InsertDeveloperAuthorizationDropBox(userId, systemName, content, secondsToLive);
        }

        /// <summary>
        /// Retrieve data for developer.dat downloads
        /// </summary>
        /// [DataObjectMethod(DataObjectMethodType.Select)]
        public byte[] GetDeveloperAuthorizationFromDropBox(int userId, string systemName, string uuid)
        {
            return userDao.GetDeveloperAuthorizationFromDropBox(userId, systemName, uuid);
        }

        /// <summary>
        /// Delete expired developer authorization dropbox entries
        /// </summary>
        /// [DataObjectMethod(DataObjectMethodType.Delete)]
        public void CleanupDeveloperAuthorizationDropBox()
        {
            userDao.CleanupDeveloperAuthorizationDropBox();
        }
    }
}
