///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;
using log4net;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject (true)]
    public class RaveFacade
    {
        private IRaveDao raveDao = DataAccess.RaveDao;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);


        #region Community Raves

        /// <summary>
        /// Rave a community or profile
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int RaveCommunity (int userId, int communityId, RaveType raveType, string currency)
        {
            CommunityFacade communityFacade = new CommunityFacade ();
            RaveType.eRAVE_TYPE eRT = raveType.RaveTypeId == (int) RaveType.eRAVE_TYPE.SINGLE ? RaveType.eRAVE_TYPE.SINGLE : RaveType.eRAVE_TYPE.MEGA;
            
            int successfulPayment = 0;
            int megaRaveHistoryId = 0;
            bool isRaveFree = IsCommunityRaveFree (userId, communityId, eRT);
            bool alreadyRaved = !isRaveFree; // Check this separately from isRaveFree. Let's admins rave over and over without blasting/joining over and over.
            bool isMegaRave = (RaveType.eRAVE_TYPE.MEGA == eRT);

            // If Admin, no charge for raving
            if (new UserFacade ().IsUserAdministrator ()) { isRaveFree = true; }

            // If not a free rave, then they must purchase
            if (!isRaveFree)
            {
                successfulPayment = PurchaseRave (userId, eRT, currency);
            }

            if (successfulPayment == 0)
            {
                 // Get the community
                Community community = communityFacade.GetCommunity (communityId);

                // Check to see if the community owner has blocked the user
                bool isUserBlocked = (new UserFacade()).IsUserBlocked(community.CreatorId, userId);

                if (isMegaRave)
                {
                    // insert/update raves for the community/person
                    int result = RaveCommunityAndAdjustStats(userId, community.CommunityId, raveType.MegaRaveValue);          

                    // Update MagaRave history
                    if (result > 0)
                    {
                        megaRaveHistoryId = InsertMegaRaveHistory(raveType.RaveTypeId, community.CommunityId, 0, 0, 0, raveType.MegaRaveValue, userId, raveType.PriceMegaRave, raveType.PriceMegaRave * (raveType.PercentCommission / 100));
                    }
                
                    // If user has been in world, send them the megarave animation if not blocked
                    if (!isUserBlocked)
                    {
                        User user = new UserFacade().GetUser(community.CreatorId);
                        if (user.WokPlayerId > 0 && community.IsPersonal.Equals(1))
                        {                                                             //**RAVE -- need to get the global_id somehow
                            new UserFacade().AddItemToPendingInventory(community.CreatorId, "P", 4942, 1);
                        }
                    }
                }
                else  // Normal Rave
                {
                    RaveCommunityAndAdjustStats(userId, community.CommunityId, 1);
                }


                // Will need to use this method if/when the RAVED - Profile packet #24 is enabled in the db.
                // This method will determine how many times the redeem packet will need
                // to occur based on the number of raves received from a mega rave.  
                // For Example:  Asset has 4 raves, gets a mega rave, now has 29 raves.  The
                // packet for asset rave 1 every 10x should get redeemed 2 times since the
                // mega rave amount pushed the rave count pass the 10 and 20 total rave counts.

                // fameFacade.RedeemPacketAssetRelatedAfterMegaRave()


                // If there is a commission, then award the Receiver 
                // of the Rave with the commission commission
                if (raveType.PercentCommission > 0 && currency.Equals (Currency.CurrencyType.CREDITS))
                {
                    AwardRaveCommission (community.CreatorId, raveType, (isMegaRave ? raveType.PriceMegaRave : raveType.PriceSingleRave), megaRaveHistoryId);
                }


                // Fame Packet -- RAVED profile, community
                FameFacade fameFacade = new FameFacade();         
                int packetTypeId = 0;
                string packetTypeDesc = "";
                try
                {
                    if (community.CommunityId > 0)
                    {
                        if (Convert.ToBoolean(community.IsPersonal) && !isUserBlocked)   // personal profile
                        {
                            //packetTypeId = (int) PacketId.BOTH_RAVED_PROFILE;
                            //packetTypeDesc = PacketId.WEB_RAVED_COMMUNITY.ToString ();

                            int result = fameFacade.RedeemPacket(userId, (int)PacketId.RAVE_SOMEONE, (int)FameTypes.World);
                            
                            // Send event to the user
                            SendRavedEventToPlayer (userId, community.CreatorId, eRT);
                        }
                        else // community
                        {
                            packetTypeId = (int) PacketId.WEB_RAVED_COMMUNITY;
                            packetTypeDesc = PacketId.WEB_RAVED_COMMUNITY.ToString ();
                            fameFacade.RedeemPacketCommunityRelated(community.CommunityId, packetTypeId, (int)FameTypes.World);
                            fameFacade.RedeemPacket(userId, (int)PacketId.RAVE_PLACE_1, (int)FameTypes.World);

                            if (community.CommunityTypeId == (int)CommunityType.HOME && community.CreatorId > 0)
                            {
                                fameFacade.RedeemPacket(community.CreatorId, (int)PacketId.HOME_RAVED_1, (int)FameTypes.World);
                                fameFacade.RedeemPacket(community.CreatorId, (int)PacketId.HOME_RAVED_10, (int)FameTypes.World);
                                fameFacade.RedeemPacket(community.CreatorId, (int)PacketId.HOME_RAVED_20, (int)FameTypes.World);
                                fameFacade.RedeemPacket(community.CreatorId, (int)PacketId.HOME_RAVED_50, (int)FameTypes.World);
                                fameFacade.RedeemPacket(community.CreatorId, (int)PacketId.HOME_RAVED_100, (int)FameTypes.World);
                                fameFacade.RedeemPacket(community.CreatorId, (int)PacketId.HOME_RAVED_250, (int)FameTypes.World);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    m_logger.Error ("Error awarding Packet " + packetTypeDesc + " for Rave, userid=" + userId.ToString () + ", communityId=" + communityId.ToString () + ". ", ex);
                }

                // Redeem Fame Packet -- MEGA RAVE 
                if (RaveType.eRAVE_TYPE.MEGA == eRT)
                {
                    try
                    {
                        packetTypeId = 0;
                        packetTypeDesc = "";

                        if (community.CommunityId > 0)                          
                        {
                            // Award Fame packet to Rave Receiver
                            fameFacade.RedeemPacket (community.CreatorId, (int) PacketId.MEGA_RAVE_RECEIVER, (int) FameTypes.World);

                            if (!isUserBlocked)
                            {
                                // Award Fame packet to Rave Giver
                                fameFacade.RedeemPacket(userId, (int)PacketId.MEGA_RAVE_GIVER, (int)FameTypes.World);
                            }
                        }
                    }
                    catch (Exception ex)  
                    {
                        m_logger.Error ("Error awarding Packet " + packetTypeDesc + " for MEGA RAVE, userid=" + userId.ToString () + ", communityId=" + communityId.ToString (), ex);
                    }
                }


                // Send the Rave Blast.  NOTE:  Not sending blasts for AP items because we currently do not have any
                // way to manage who gets blast.  Meaning, don't want underage or non-AP holders to get rave
                // blasts of items that are AP
                if (!community.IsAdult.Equals("Y") && !isUserBlocked)
                {
                    User user = new UserFacade ().GetUser (userId);

                    if (Convert.ToBoolean (community.IsPersonal))
                    {
                        new BlastFacade ().SendRaveProfileBlast (user, community, isMegaRave);
                    }
                    else if (!alreadyRaved)
                    {
                        new BlastFacade().SendRave3DAppBlast(user, community);
                    }
                }

                // Join the community if it is a world per 'Rave World Joins and Faves' project. 
                if (!alreadyRaved && !Convert.ToBoolean(community.IsPersonal) && !isUserBlocked)
                {
                    communityFacade.JoinCommunity(communityId, userId);
                }
            }

            return successfulPayment;
        }

        /// <summary>
        /// The the number of times a user has raved a community or profile
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public int GetCommunityRaveCountByUser (int userId, int communityId)
        {
            return raveDao.GetCommunityRaveCountByUser (userId, communityId);
        }

        /// <summary>
        /// Is the next community rave free for the user
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public bool IsCommunityRaveFree (int userId, int communityId, RaveType.eRAVE_TYPE raveType)
        {
            if (raveDao.GetRaveType (Convert.ToUInt32 (raveType)).NumberAllowedFree > raveDao.GetCommunityRaveCountByUser (userId, communityId))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// inserts community rave(s) and updates community stats
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int RaveCommunityAndAdjustStats(int ravingUserId, int communityId, int numRaves)
        {
            return raveDao.RaveCommunityAndAdjustStats(ravingUserId, communityId, numRaves);
        }

        /// <summary>
        /// Adjusts the total number of raves for a community
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        private void AdjustCommunityRaveTotal(int communityId, int numRaves)
        {
            raveDao.AdjustCommunityRaveTotal(communityId, numRaves);
        }

        /// <summary>
        /// Add a rave to a community
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        private int InsertCommunityRave (int userId, int communityId, int numRaves)
        {
            return raveDao.InsertCommunityRave(userId, communityId, numRaves);
        }

        
        #endregion Community Raves


        #region Asset Raves


        /// <summary>
        /// Puchase a rave for an asset
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int PurchaseAssetRave (int userId, int assetId, RaveType.eRAVE_TYPE eRT)
        {
            RaveType rt;

            if (RaveType.eRAVE_TYPE.MEGA == eRT)
            {
                rt = GetRaveType (Convert.ToUInt32 (RaveType.eRAVE_TYPE.MEGA));
                
                // update the blast amount of raves
                AdjustAssetRaveTotal (assetId, rt.MegaRaveValue);

                // Update MagaRave history
                InsertMegaRaveHistory (rt.RaveTypeId, 0, 0, 0, assetId, rt.MegaRaveValue, userId, rt.PriceMegaRave, rt.PriceMegaRave * (rt.PercentCommission/100));
            }
            else  // Normal Rave
            {
                InsertAssetRave (userId, assetId);
            }

            return 0;
        }

        /// <summary>
        /// Raves an asset with either a normal rave or a mega rave
        /// </summary>
        public int RaveAsset (int userId, int assetId, RaveType.eRAVE_TYPE eRT, string currency)
        {
            // Return codes
            //  0 = Success
            //  1 = Not enough Rewards
            //  2 = Not enough Credits
            // -1 = Error

            int successfulPayment = 0;
            int megaRaveHistoryId = 0;
            bool isRaveFree = IsAssetRaveFree (userId, assetId, eRT);
            bool isMegaRave = (RaveType.eRAVE_TYPE.MEGA == eRT);
            RaveType raveType = GetRaveType (Convert.ToUInt32 (eRT));

            // If Admin, no charge for raving
            if (new UserFacade ().IsUserAdministrator ()) { isRaveFree = true; }

            // If not a free rave, then they must purchase
            if (!isRaveFree)
            {
                successfulPayment = PurchaseRave (userId, eRT, currency);
            }

            if (successfulPayment == 0)
            {
                if (isMegaRave)
                {
                    // update the blast amount of raves
                    AdjustAssetRaveTotal (assetId, raveType.MegaRaveValue);

                    // Update MagaRave history
                    megaRaveHistoryId = InsertMegaRaveHistory (raveType.RaveTypeId, 0, 0, 0, assetId, raveType.MegaRaveValue, userId, raveType.PriceMegaRave, raveType.PriceMegaRave * (raveType.PercentCommission / 100));
                }
                else  // Normal Rave
                {
                    InsertAssetRave (userId, assetId);

                    // update the blast amount of raves
                    AdjustAssetRaveTotal (assetId, 1);
                }

                // Get the asset
                Asset asset = new MediaFacade ().GetAsset (assetId);
                
                // If there is a commission, then award the Receiver 
                // of the Rave with the commission commission
                if (raveType.PercentCommission > 0 && currency.Equals(Currency.CurrencyType.CREDITS))
                {
                    AwardRaveCommission (asset.OwnerId, raveType, (isMegaRave ? raveType.PriceMegaRave : raveType.PriceSingleRave), megaRaveHistoryId);
                }


                // Will need to use this method if/when we expand mega raves to assets.
                // This method will determine how many times the redeem packet will need
                // to occur based on the number of raves received from a mega rave.  
                // For Example:  Asset has 4 raves, gets a mega rave, now has 29 raves.  The
                // packet for asset rave 1 every 10x should get redeemed 2 times since the
                // mega rave amount pushed the rave count pass the 10 and 20 total rave counts.
                
                // fameFacade.RedeemPacketAssetRelatedAfterMegaRave()


                // Fame Packet -- RAVED pattern, photo, video                         
                int packetTypeId = 0;
                string packetTypeDesc = "";
                string assetTypeDesc = "";
                try
                {
                    if (asset.AssetId > 0)
                    {
                        switch (asset.AssetTypeId)
                        {
                            case (int) Asset.eASSET_TYPES.PATTERN:
                                packetTypeId = (int) PacketId.WEB_RAVED_PATTERN;
                                packetTypeDesc = PacketId.WEB_RAVED_PATTERN.ToString ();
                                assetTypeDesc = "pattern";
                                break;
                            case (int) Asset.eASSET_TYPES.PICTURE:
                                packetTypeId = (int) PacketId.WEB_RAVED_PHOTO;
                                packetTypeDesc = PacketId.WEB_RAVED_PHOTO.ToString ();
                                assetTypeDesc = "photo";
                                break;
                            case (int) Asset.eASSET_TYPES.VIDEO:
                                packetTypeId = (int) PacketId.WEB_RAVED_VIDEO;
                                packetTypeDesc = PacketId.WEB_RAVED_VIDEO.ToString ();
                                assetTypeDesc = "video";
                                break;
                            case (int) Asset.eASSET_TYPES.GAME:
                                assetTypeDesc = "game";
                                break;
                            case (int) Asset.eASSET_TYPES.MUSIC:
                                assetTypeDesc = "music";
                                break;
                            case (int) Asset.eASSET_TYPES.TV:
                                assetTypeDesc = "tv channel";
                                break;
                            case (int) Asset.eASSET_TYPES.WIDGET:
                                assetTypeDesc = "flash widget";
                                break;
                        }
                    }

                    if (packetTypeId > 0)
                    {
                        FameFacade fameFacade = new FameFacade ();
                        fameFacade.RedeemPacketAssetRelated (asset.AssetId, packetTypeId, (int) FameTypes.World);
                    }
                }
                catch (Exception ex)      
                {
                    m_logger.Error ("Error awarding Packet " + packetTypeDesc + " for Rave, userid=" + userId.ToString () + ", assetId=" + assetId.ToString (), ex);
                }


                // ----------- Start Comment of Mega Rave Code -----------
                // NOTE:  MegaRave for assets is not currently suppored.  When that feature is added, then this section
                // of code will need to be uncommented and tested.
                // Fame Packet -- MEGA RAVE 
                //if (RaveType.eRAVE_TYPE.MEGARAVE == eRT)
                //{
                //    try
                //    {
                //        packetTypeId = 0;
                //        packetTypeDesc = "";

                //        if (asset.AssetId > 0)                          
                //        {
                //            // Award Fame packet to Rave Receiver
                //            packetTypeId = (int) PacketId.MEGA_RAVE_RECEIVER;
                //            // do we need a new redeem packet method?
                //            if (packetTypeId > 0)                       
                //            {
                //                FameFacade fameFacade = new FameFacade ();
                //                fameFacade.RedeemPacket (asset.OwnerId, packetTypeId, (int) FameTypes.World);
                //            }

                //            // Award Fame packet to Rave Giver
                //            packetTypeId = (int) PacketId.MEGA_RAVE_GIVER;
                //            // do we need a new redeem packet method?
                //            if (packetTypeId > 0)
                //            {
                //                FameFacade fameFacade = new FameFacade ();
                //                fameFacade.RedeemPacket (userId, packetTypeId, (int) FameTypes.World);
                //            }
                //        }
                //    }
                //    catch (Exception ex)  
                //    {
                //        m_logger.Error ("Error awarding Packet " + packetTypeDesc + " for Mega Rave, userid=" + userId.ToString () + ", assetId=" + assetId.ToString (), ex);
                //    }
                //}
                // ----------- End Comment of Mega Rave Code -----------


                // Send the Rave Blast.  NOTE:  Not sending blasts for AP items because we currently do not have any
                // way to manage who gets blast.  Meaning, don't want underage or non-AP holders to get rave
                // blasts of items that are AP
                if (!asset.Mature)
                {
                    User user = new UserFacade ().GetUser (userId);
                    new BlastFacade ().SendRaveAssetBlast (user, asset, assetTypeDesc, isMegaRave);
                }
            }
            
            return successfulPayment;
        }

        /// <summary>
        /// The the number of times a user has raved an asset
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public int GetAssetRaveCountByUser (int userId, int assetId)
        {
            return raveDao.GetAssetRaveCountByUser (userId, assetId);
        }

        /// <summary>
        /// Is the next asset rave free for the user
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public bool IsAssetRaveFree (int userId, int assetId, RaveType.eRAVE_TYPE raveType)
        {
            if (raveDao.GetRaveType (Convert.ToUInt32(raveType)).NumberAllowedFree > raveDao.GetAssetRaveCountByUser (userId, assetId))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Add a rave to an asset
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        private int InsertAssetRave (int userId, int assetId)
        {
            return raveDao.InsertAssetRave (userId, assetId);
        }

        /// <summary>
        /// Adjusts the total number of raves for an asset
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        private void AdjustAssetRaveTotal (int assetId, int numRaves)
        {
            raveDao.AdjustAssetRaveTotal (assetId, numRaves);
        }


        #endregion Asset Raves


        #region UGC Raves

        /// <summary>
        /// Adjusts the total number of raves for a ugc item
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public void AdjustUGCRaveTotal (int globalId, int numRaves)
        {
            raveDao.AdjustUGCRaveTotal (globalId, numRaves);
        }

        /// <summary>
        /// Get the rave count for an ugc item
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int InsertUGCRave (int userId, int globalId)
        {
            return raveDao.InsertUGCRave (userId, globalId);
        }
            
        /// <summary>
        /// The the number of times a user has raved an ugc item
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public int GetUGCRaveCountByUser (int userId, int globalId)
        {
            return raveDao.GetUGCRaveCountByUser (userId, globalId);
        }

        /// <summary>
        /// Get the rave count for an ugc item
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public int GetUGCRaveCount (int globalId)
        {
            return raveDao.GetUGCRaveCount (globalId);
        }

        /// <summary>
        /// Is the next UGC Item rave free for the user
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public bool IsUGCRaveFree (int userId, int globalId, RaveType.eRAVE_TYPE raveType)
        {
            if (raveDao.GetRaveType (Convert.ToUInt32 (raveType)).NumberAllowedFree > raveDao.GetUGCRaveCountByUser (userId, globalId))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Raves an asset with either a normal rave or a mega rave
        /// </summary>
        public int RaveUGCItem (int userId, int globalId, RaveType.eRAVE_TYPE eRT, string currency)
        {
            // Return codes
            //  0 = Success
            //  1 = Not enough Rewards
            //  2 = Not enough Credits
            // -1 = Error

            int successfulPayment = 0;
            int megaRaveHistoryId = 0;
            bool isRaveFree = IsUGCRaveFree (userId, globalId, eRT);
            bool isMegaRave = (RaveType.eRAVE_TYPE.MEGA == eRT);
            RaveType raveType = GetRaveType (Convert.ToUInt32 (eRT));

            // If Admin, no charge for raving
            if (new UserFacade ().IsUserAdministrator ()) { isRaveFree = true; }

            // If not a free rave, then they must purchase
            if (!isRaveFree)
            {
                successfulPayment = PurchaseRave (userId, eRT, currency);
            }

            if (successfulPayment == 0)
            {
                if (isMegaRave)   //** Need to updated this section if add ability to megarave UGC item
                {
                   //  update the blast amount of raves
                    // AdjustUGCRaveTotal (globalId, raveType.MegaRaveValue);

                    //** Need to update this to pass in globalId when add megarave to ugc items
                    // Update MagaRave history  
                    //megaRaveHistoryId = InsertMegaRaveHistory (raveType.RaveTypeId, 0, 0, 0, assetId, raveType.MegaRaveValue, userId, raveType.PriceMegaRave, raveType.PriceMegaRave * (raveType.PercentCommission / 100));
                }
                else  // Normal Rave
                {
                    InsertUGCRave (userId, globalId);

                    // Update the amount of raves
                    AdjustUGCRaveTotal (globalId, 1);
                }

                // Get the raved item
                WOKItem item = new ShoppingFacade ().GetItem (globalId);

                // If there is a commission, then award the Receiver 
                // of the Rave with the commission commission
                if (raveType.PercentCommission > 0 && currency.Equals(Currency.CurrencyType.CREDITS))
                {
                    AwardRaveCommission (Convert.ToInt32(item.ItemCreatorId), raveType, (isMegaRave ? raveType.PriceMegaRave : raveType.PriceSingleRave), megaRaveHistoryId);
                }

                try
                {
                    FameFacade fameFacade = new FameFacade();
                    // Fashion Fame
                    fameFacade.RedeemPacket(Convert.ToInt32(item.ItemCreatorId), (int)PacketIdFashion.RECEIVE_RAVE, (int)FameTypes.Fashion);
                    //World Fame
                    if (item.UseType == WOKItem.USE_TYPE_CLOTHING)
                    {
                        fameFacade.RedeemPacket((int)userId, (int)PacketId.RAVE_CLOTHING_1, (int)FameTypes.World);
                    }
					//}
                }
                catch (Exception ex)
                {
                    m_logger.Error ("Error awarding Packet " + PacketIdFashion.RECEIVE_RAVE.ToString() + " for Rave, userid=" + userId.ToString () + ", globalId=" + globalId.ToString (), ex);
                }

                // Send the Rave Blast.  NOTE:  Not sending blasts for AP items because we currently do not have any
                // way to manage who gets blast.  Meaning, don't want underage or non-AP holders to get rave
                // blasts of items that are AP
                if (!item.PassTypeId.Equals(1))  //(int)Constants.ePASS_TYPE.ACCESS))
                {
                    User user = new UserFacade ().GetUser (userId);

                    // Send Rave Blast
                    new BlastFacade ().SendRaveUGCBlast (user, item, isMegaRave);
                }
            }

            return successfulPayment;
        }
        
        #endregion UGC Raves


        /// <summary>
        /// Puchase a rave
        /// </summary>
        private int PurchaseRave (int userId, RaveType.eRAVE_TYPE eRT, string currency)
        {
            // Return codes
            //  0 = Success
            //  1 = Not enough Rewards
            //  2 = Not enough Credits
            // -1 = Error

            UserFacade userFacade = new UserFacade ();
            int ravePrice = 0;
            ushort transType = 0;
            int successfulPayment = 0;

            // What is the cost of this rave?
            RaveType raveType = GetRaveType (Convert.ToUInt32 (eRT));

            if (eRT == RaveType.eRAVE_TYPE.MEGA)
            {
                ravePrice = raveType.PriceMegaRave;
                transType = Convert.ToUInt16 (TransactionType.eTRANSACTION_TYPES.CASH_TT_MEGARAVE);
            }
            else  // Normal rave
            {
                ravePrice = raveType.PriceSingleRave;
                transType = Convert.ToUInt16 (TransactionType.eTRANSACTION_TYPES.CASH_TT_RAVE);
            }

            UserBalances ub = userFacade.GetUserBalances (userId);

            if (!userFacade.IsUserAdministrator ())
            {
                try
                {
                    // Verify user has enough rewards/credits to pay for this
                    if (Currency.CurrencyType.REWARDS == currency)
                    {
                        if (ub.Rewards < ravePrice)
                        {
                            return 1;
                        }

                        successfulPayment = userFacade.AdjustUserBalance (userId, Currency.CurrencyType.REWARDS, -Convert.ToDouble (ravePrice), transType);
                    }
                    else  // Pay with Credits (KPOINTS)
                    {
                        if (ub.Credits < ravePrice)
                        {
                            return 2;
                        }

                        successfulPayment = userFacade.AdjustUserBalance (userId, Currency.CurrencyType.CREDITS, -Convert.ToDouble (ravePrice), transType);
                    }
                }
                catch(Exception ex)
                {
                    switch (ex.Message)
                    {
                        case "1":
                            successfulPayment = 1;
                            break;
                        case "2":
                            successfulPayment = 2;
                            break;
                        case "-1":
                        default:
                            successfulPayment = -1;
                            break;
                    }
                }
            }

            return successfulPayment;
        }

        /// <summary>
        /// Award a rave commission to user
        /// </summary>
        private int AwardRaveCommission (int raveReceiverUserId, RaveType eRT, int ravePrice, int megaRaveHistoryId)
        {
            int success = 0;
            double commissionAmt = 0;
            try
            {
                if (eRT != null)
                {
                    if (eRT.PercentCommission > 0)
                    {
                        // Reciever gets %commission * rave cost in rewards
                        commissionAmt = Math.Round((Convert.ToDouble (eRT.PercentCommission) / Convert.ToDouble (100)) * ravePrice);

                        if (commissionAmt > 0)          
                        {
                            success = new UserFacade ().AdjustUserBalance (raveReceiverUserId, Currency.CurrencyType.REWARDS, commissionAmt, Convert.ToUInt16 (TransactionType.eTRANSACTION_TYPES.CASH_TT_COMMISSION));
                        }

                        if (success.Equals(0) && megaRaveHistoryId > 0)
                        {
                            // Update the Mega Rave History record with commission amount awarded
                            UpdateMegaRaveHistoryCommission (megaRaveHistoryId, Convert.ToInt32(commissionAmt));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string message = "Error";
                switch (ex.Message)
                {
                    case "1":
                        success = 1;
                        message = "Not enough Rewards";
                        break;
                    case "2":
                        success = 2;
                        message = "Not enough Credits";
                        break;
                    case "-1":
                    default:
                        success = -1;
                        break;
                }
                m_logger.Error("Error awarding Rave Commission to userid=" + raveReceiverUserId.ToString() + ". Commission amount=" + commissionAmt.ToString() + ". " +
                    "Rave type=" + eRT.Name + ". " + message, ex);
            }

            return success;
        }

        public int UserHasFundsToBuyRave (RaveType raveType, string currency, UserBalances ub)
        {
            // Return Codes
            // 0  - User has enough rewards/credits to purchase rave/megarave
            // 1  - Not enough rewards to purchase single rave
            // 2  - Not enough rewards to purchase megarave
            // 3  - Not enough credits to purchase single rave
            // 4  - Not enough credits to purachse megarave
            // 99 - Unsupported currency typ

            if (Currency.CurrencyType.REWARDS == currency) // Rewards
            {
                if (Convert.ToUInt32 (RaveType.eRAVE_TYPE.SINGLE) == raveType.RaveTypeId)  // Single Rave
                {
                    if (ub.Rewards < raveType.PriceSingleRave)
                    {
                        return 1;
                    }
                    return 0;
                }
                else // MegaRave
                {
                    if (ub.Rewards < raveType.PriceMegaRave)
                    {
                        return 2;
                    }
                    return 0;
                }
            }
            else if (Currency.CurrencyType.CREDITS == currency) // Credits
            {
                if (Convert.ToUInt32 (RaveType.eRAVE_TYPE.SINGLE) == raveType.RaveTypeId)  // Single Rave
                {
                    if (ub.Credits < raveType.PriceSingleRave)
                    {
                        return 3;
                    }
                    return 0;
                }
                else // MegaRave
                {
                    if (ub.Credits < raveType.PriceMegaRave)
                    {
                        return 4;
                    }
                    return 0;
                }
            }
            else
            {
                // unsupported currency type
                return 99;
            }
        }

        public void SendRavedEventToPlayer (int raverUserId, int ravedUserId, RaveType.eRAVE_TYPE eRT)
        {
            // Get this one time, used several times below
            UserFacade userFacade = new UserFacade ();
            XmlEventCreator xml = new XmlEventCreator ("ClientTickler");
            User raverUser = null;
            User ravedUser = null;

            if (raverUserId == ravedUserId || ravedUserId == 0) //player raved themselves
            {
                raverUser = userFacade.GetUser (raverUserId);
                ravedUser = raverUser; //same user as above
            }
            else
            {
                raverUser = userFacade.GetUser (raverUserId);
                ravedUser = userFacade.GetUser (ravedUserId);
            }

            // If user has never logged onto wok, no need to send event
            if (ravedUser.WokPlayerId > 0)
            {
                xml.AddValue((int)NotificationsProfile.NotificationTypes.PlayerRaved);
                xml.AddValue (eRT.ToString ().ToLower ());
                xml.AddValue (raverUser.Username);
                xml.AddValue (raverUser.UserId);
                xml.AddValue (raverUser.ThumbnailPath);
                xml.AddValue (ravedUser.Username);
                xml.AddValue (ravedUser.UserId);
                xml.AddValue (ravedUser.ThumbnailPath);

				//already raved or not
				//						 GetCommunityRave(where the usser raved, has raved this communityId ie uer)				
				DataRow drDigg = raveDao.GetCommunityRave(ravedUser.UserId,	raverUser.CommunityId);
				int alreadyRaved = 0;
				if (drDigg != null)
				{
					alreadyRaved = 1;
				}
				xml.AddValue(alreadyRaved);



                //xml.AddPlayer(raverUser.WokPlayerId); dont send to raver on to raved
                //if (raverUser.WokPlayerId != ravedUser.WokPlayerId)
                //{
                    xml.AddPlayer (ravedUser.WokPlayerId);
                //}

                try
                {
                    RemoteEventSender sender = new RemoteEventSender ();
                    sender.BroadcastEventToServer (xml.GetXml ()); //send event to all servers
                    m_logger.Debug("RaveFacade:SendRavedEventToPlayer: Sent ClientTickler.PlayerRaved event. RaverUserID=" + raverUser.UserId + " RavedUserID=" + ravedUser.UserId);
                    //sender.BroadcastMessageToPlayer("Test", user.WokPlayerId);
                }
                catch (Exception e)
                {
                    m_logger.Error ("Error Broadcasting Raved Event to WoK Server ", e);
                }
            }
        }
                                        
        /// <summary>
        /// GetRaveTypes
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public IList<RaveType> GetRaveTypes ()
        {
            return raveDao.GetRaveTypes ();
        }

        /// <summary>
        /// GetRaveType
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public RaveType GetRaveType (UInt32 raveTypeId)
        {
            return raveDao.GetRaveType (raveTypeId);
        }

        /// <summary>
        /// Get the price for a particular rave type
        /// </summary>
        public int GetRavePrice (RaveType raveType, RaveType.eRAVE_TYPE eRT)
        {
            switch (eRT)
            {
                case RaveType.eRAVE_TYPE.SINGLE:
                    return raveType.PriceSingleRave;

                case RaveType.eRAVE_TYPE.MEGA:
                    return raveType.PriceMegaRave;

                default:
                    return raveType.PriceSingleRave;
            }
        }

        /// <summary>
        /// Insert history record for a Mega Rave
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        private int InsertMegaRaveHistory (UInt32 raveTypeId, int channelId, int zoneInstanceId, int zoneType, int assetId,
            int raveValue, int userIdRaver, int pricePaid, int commissionReceived)
        {
            return raveDao.InsertMegaRaveHistory (raveTypeId, channelId, zoneInstanceId, zoneType, assetId,
                raveValue, userIdRaver, pricePaid, commissionReceived);
        }

        /// <summary>
        /// Update history record for a Mega Rave
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        private int UpdateMegaRaveHistoryCommission (int megaRaveHistoryId, int commissionReceived)
        {
            return raveDao.UpdateMegaRaveHistoryCommission (megaRaveHistoryId, commissionReceived);
        }


        /// <summary>
        /// Get Mege Rave Count
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public int GetUserMegaRaveCount (int personalChannelId)
        {
            return raveDao.GetUserMegaRaveCount (personalChannelId);
        }

    }
}
