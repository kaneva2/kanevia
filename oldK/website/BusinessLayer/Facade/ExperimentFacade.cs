///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Net;
using System.IO;
using System.Web;
using System.Data;
using System.Collections.Specialized;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;
using System.Linq;

using System.Web.Security;
using System.Threading;
using log4net;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class ExperimentFacade
    {
        private IExperimentDao experimentDao = DataAccess.ExperimentDao;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Create / Update


        public UserExperimentGroup GetUserExperimentGroupCommon (int userId, string experimentId)
        {
            UserExperimentGroup userExperimentGroup = null;
            Experiment experiment = GetExperiment(experimentId);

            // Don't bother processing unless experiment is active.
            if (experiment != null && (experiment.Status == EXPERIMENT_STATUS_TYPE.STARTED || experiment.Status == EXPERIMENT_STATUS_TYPE.ENDED))
            {
                userExperimentGroup = GetUserExperimentGroupByExperimentId(userId, experimentId);

                // Make sure the user is assigned to a group.
                if (userExperimentGroup == null)
                {
                    string assignedGroup = AssignParticipant(experiment);
                    userExperimentGroup = new UserExperimentGroup(string.Empty, userId, assignedGroup, DateTime.Now, null, false, string.Empty);
                }

                // Mark that the user has been exposed to the test subject.
                if (userExperimentGroup.ConversionDate == null)
                {
                    userExperimentGroup.ConversionDate = DateTime.Now;
                    SaveUserExperimentGroup(userExperimentGroup);
                }
            }

            return userExperimentGroup;
        }



        /// <summary>
        /// Will select a random group if the experiment is in a started state or if experiment is
        /// ended but no winning group id has been set
        /// </summary>
        public string AssignParticipant(Experiment experiment, User user = null)
        {
            Random randomGenerator = new Random(Common.GetRandomSeed());
            int randValue = randomGenerator.Next(0, 100);
            int cumulative = 0;
            string assignedGroupId = null;

            if (experiment.Status == EXPERIMENT_STATUS_TYPE.ENDED && !string.IsNullOrWhiteSpace(experiment.WinningGroupId))
            {
                assignedGroupId = experiment.WinningGroupId;
            }
            else if (experiment.Status == EXPERIMENT_STATUS_TYPE.STARTED)
            {
                if (experiment.ExperimentGroups.Count > 0)
                {
                    UserFacade userFacade = new UserFacade();
                    if (user != null && userFacade.IsUserAutomatedTestAccount(user))
                    {
                        // Automated test accounts should always be set to the control variant.
                        // By convention, this is group 'A'. If there is no group labeled 'A', the group will be empty.
                        try
                        {
                            ExperimentGroup controlGroup = experiment.ExperimentGroups.SingleOrDefault(g => g.Label == "A");
                            assignedGroupId = (controlGroup ?? new ExperimentGroup()).GroupId;
                        }
                        catch(InvalidOperationException)
                        {
                            m_logger.Error("Unable to determine control group of experiment " + experiment.Name + ". There are multiple groups labeled 'A'.");
                        }
                    }
                    else
                    {
                        // Select a group using the cumulative probability of each element.
                        List<ExperimentGroup> sortedGroups = experiment.ExperimentGroups
                            .OrderBy(g => g.AssignmentPercentage)
                            .ThenBy(g => g.GroupId).ToList();

                        foreach (ExperimentGroup group in sortedGroups)
                        {
                            cumulative += group.AssignmentPercentage;
                            if (randValue < cumulative)
                            {
                                assignedGroupId = group.GroupId;
                                break;
                            }
                        }
                    }
                }
            }

            return assignedGroupId;
        }

        /// <summary>
        /// SaveExperiment - will save experiment to the database or update existing experiment
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public bool SaveExperiment(Experiment experiment)
        {
            if (!string.IsNullOrWhiteSpace(experiment.WinningGroupId) && experiment.ExperimentGroups.Count == 0)
            {
                throw new ArgumentException("Winning group is invalid.");
            }

            return experimentDao.SaveExperiment(experiment);
        }

        /// <summary>
        /// SaveExperimentGroups - will save experiment to the database or update existing experiment
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public bool SaveExperimentGroups(Experiment experiment, bool resetAssignmentPercentages)
        {
            if (experiment.ExperimentGroups.Count == 0)
            {
                throw new ArgumentException("Group data must not be null.");
            }

            if (resetAssignmentPercentages)
            {
                ResetExperimentGroupAssignmentPercentages(experiment);
            }

            IEnumerable<int> groupPercentages = experiment.ExperimentGroups.Select(g => g.AssignmentPercentage);

            if (groupPercentages.Sum() != 100)
            {
                throw new ArgumentException("Group assignment percentages do not add up to 100.");
            }

            if (groupPercentages.Where(i => i < 0 || i > 100).Count() > 0)
            {
                throw new ArgumentException("Group assignment percentages must be between 0 and 100.");
            }

            return experimentDao.SaveExperimentGroups(experiment);
        }

        /// <summary>
        /// SaveExperimentCategory - will save experiment category to the database or update existing experiment
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public bool SaveExperimentCategory(ExperimentCategory category)
        {
            return experimentDao.SaveExperimentCategory(category);
        }

        /// <summary>
        /// SaveExperimentEffect - will save experiment effect to the database or update existing experiment
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public bool SaveExperimentEffect(ExperimentEffect effect)
        {
            return experimentDao.SaveExperimentEffect(effect);
        }

        /// <summary>
        /// SaveExperimentGroupEffect - will save experiment group to the database or update existing experiment group
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public bool SaveExperimentGroupEffect(ExperimentGroup group, ExperimentEffect effect)
        {
            return experimentDao.SaveExperimentGroupEffect(group, effect);
        }

        /// <summary>
        /// SaveUserExperimentGroup - will save experiment participant to the database or update existing experiment participant
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public bool SaveUserExperimentGroup(UserExperimentGroup participant)
        {
            return experimentDao.SaveUserExperimentGroup(participant);
        }

        /// <summary>
        /// SaveWorldExperimentGroup - will save experiment participant to the database or update existing experiment participant
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public bool SaveWorldExperimentGroup(WorldExperimentGroup participant)
        {
            return experimentDao.SaveWorldExperimentGroup(participant);
        }

        /// <summary>
        /// SaveWorldTemplateExperimentGroup - will save experiment participant to the database or update existing experiment participant
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public bool SaveWorldTemplateExperimentGroup(WorldTemplateExperimentGroup participant)
        {
            return experimentDao.SaveWorldTemplateExperimentGroup(participant);
        }

        /// <summary>
        /// Reset experiment group assignment percentages to equal values per group.
        /// </summary>
        private void ResetExperimentGroupAssignmentPercentages(Experiment experiment)
        {
            if (experiment != null && experiment.ExperimentGroups.Count > 0)
            {
                int groupCount = experiment.ExperimentGroups.Count;
                int groupPercentage = 100 / groupCount;
                int leftOver = 100 - groupCount * groupPercentage;

                foreach (ExperimentGroup group in experiment.ExperimentGroups)
                {
                    group.AssignmentPercentage = groupPercentage;
                }

                // Make sure we always add up to 100% in the case of decimal values.
                experiment.ExperimentGroups[0].AssignmentPercentage = groupPercentage + leftOver;
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes an Experiment object from the database.
        /// </summary>
        /// <remarks>
        /// Deletion assumes proper referential integrity constraints will cascade the change appropriately.
        /// If this is NOT the case, this method must be adjusted to make the associated changes manually.
        /// </remarks>
        /// <returns>True if the Experiment was successfully deleted.</returns>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public bool DeleteExperiment(Experiment experiment)
        {
            return experimentDao.DeleteExperiment(experiment);
        }

        /// <summary>
        /// Deletes an ExperimentCategory object from the database.
        /// </summary>
        /// <remarks>
        /// Deletion assumes proper referential integrity constraints will cascade the change appropriately.
        /// If this is NOT the case, this method must be adjusted to make the associated changes manually.
        /// </remarks>
        /// <returns>True if the ExperimentCategory was successfully deleted.</returns>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public bool DeleteExperimentCategory(ExperimentCategory category)
        {
            return experimentDao.DeleteExperimentCategory(category);
        }

        /// <summary>
        /// Deletes an ExperimentEffect object from the database.
        /// </summary>
        /// <remarks>
        /// Deletion assumes proper referential integrity constraints will cascade the change appropriately.
        /// If this is NOT the case, this method must be adjusted to make the associated changes manually.
        /// </remarks>
        /// <returns>True if the ExperimentEffect was successfully deleted.</returns>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public bool DeleteExperimentEffect(ExperimentEffect effect)
        {
            return experimentDao.DeleteExperimentEffect(effect);
        }

        /// <summary>
        /// Deletes an ExperimentGroup object from the database.
        /// </summary>
        /// <remarks>
        /// Deletion assumes proper referential integrity constraints will cascade the change appropriately.
        /// If this is NOT the case, this method must be adjusted to make the associated changes manually.
        /// </remarks>
        /// <returns>True if the ExperimentGroup was successfully deleted.</returns>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public bool DeleteExperimentGroup(ExperimentGroup group)
        {
            bool deleteOk = false;

            Experiment experiment = group.Experiment;

            if (experiment != null && experimentDao.DeleteExperimentGroup(group))
            {
                // Update associated expreriment object.
                experiment.ExperimentGroups.Remove(group);

                if (!string.IsNullOrWhiteSpace(experiment.WinningGroupId) && experiment.WinningGroupId.Equals(group.GroupId))
                {
                    experiment.WinningGroupId = string.Empty;
                }

                // Recalculate percentages if necessary.
                if (experiment.ExperimentGroups.Count > 0)
                {
                    deleteOk = SaveExperimentGroups(experiment, true);
                }
                else
                {
                    deleteOk = true;
                }
            }

            return deleteOk;
        }

        /// <summary>
        /// Deletes an ExperimentEffect object from the database for a specific group.
        /// </summary>
        /// <remarks>
        /// Deletion assumes proper referential integrity constraints will cascade the change appropriately.
        /// If this is NOT the case, this method must be adjusted to make the associated changes manually.
        /// </remarks>
        /// <returns>True if the ExperimentEffect was successfully deleted.</returns>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public bool DeleteExperimentGroupEffect(ExperimentGroup group, ExperimentEffect effect)
        {
            return experimentDao.DeleteExperimentGroupEffect(group, effect);
        }

        /// <summary>
        /// Deletes an ExperimentGroup object from the database.
        /// </summary>
        /// <remarks>
        /// Deletion assumes proper referential integrity constraints will cascade the change appropriately.
        /// If this is NOT the case, this method must be adjusted to make the associated changes manually.
        /// </remarks>
        /// <returns>True if the ExperimentGroup was successfully deleted.</returns>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public bool DeleteUserExperimentGroup(UserExperimentGroup participant)
        {
            return experimentDao.DeleteUserExperimentGroup(participant);
        }

        /// <summary>
        /// Deletes an WorldExperimentGroup object from the database.
        /// </summary>
        /// <remarks>
        /// Deletion assumes proper referential integrity constraints will cascade the change appropriately.
        /// If this is NOT the case, this method must be adjusted to make the associated changes manually.
        /// </remarks>
        /// <returns>True if the WorldExperimentGroup was successfully deleted.</returns>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public bool DeleteWorldExperimentGroup(WorldExperimentGroup participant)
        {
            return experimentDao.DeleteWorldExperimentGroup(participant);
        }

        /// <summary>
        /// Deletes an WorldTemplateExperimentGroup object from the database.
        /// </summary>
        /// <remarks>
        /// Deletion assumes proper referential integrity constraints will cascade the change appropriately.
        /// If this is NOT the case, this method must be adjusted to make the associated changes manually.
        /// </remarks>
        /// <returns>True if the WorldTemplateExperimentGroup was successfully deleted.</returns>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public bool DeleteWorldTemplateExperimentGroup(WorldTemplateExperimentGroup participant)
        {
            return experimentDao.DeleteWorldTemplateExperimentGroup(participant);
        }

        #endregion

        #region Read

        /// <summary>
        /// GetExperiment
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Experiment GetExperiment(string experimentId, bool includeExperimentGroups = true)
        {
            return experimentDao.GetExperiment(experimentId, includeExperimentGroups);
        }

        /// <summary>
        /// SearchExperiments
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Experiment> SearchExperiments(string name, bool exactMatch, string orderBy, int pageNumber, int pageSize, bool includeExperimentGroups = true)
        {
            return experimentDao.SearchExperiments(name, exactMatch, orderBy, pageNumber, pageSize, includeExperimentGroups);
        }

        /// <summary>
        /// SearchExperiments
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Experiment> SearchExperiments(string name, bool exactMatch, string category, string status, string orderBy, int pageNumber, int pageSize, bool includeExperimentGroups = true)
        {
            return experimentDao.SearchExperiments(name, exactMatch, category, status, orderBy, pageNumber, pageSize, includeExperimentGroups);
        }

        /// <summary>
        /// GetAvailableExperimentsByUserContext - returns info from all records in db corresponding to active experiments the user is not taking part in
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<Experiment> GetAvailableExperimentsByUserContext(int userId, string context = "Request", bool includeExperimentGroups = true)
        {
            return experimentDao.GetAvailableExperimentsByUserContext(userId, context, includeExperimentGroups);
        }

        /// <summary>
        /// GetExperimentCategory
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public ExperimentCategory GetExperimentCategory(string name)
        {
            return experimentDao.GetExperimentCategory(name);
        }

        /// <summary>
        /// SearchExperimentCategories
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<ExperimentCategory> SearchExperimentCategories(string name, string sortBy, int pageNumber, int pageSize)
        {
            return experimentDao.SearchExperimentCategories(name, sortBy, pageNumber, pageSize);
        }

        /// <summary>
        /// GetExperimentEffects
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<ExperimentEffect> GetExperimentEffects(string sortBy, int pageNumber, int pageSize)
        {
            return experimentDao.GetExperimentEffects(sortBy, pageNumber, pageSize);
        }

        /// <summary>
        /// GetExperimentGroup
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public ExperimentGroup GetExperimentGroup(string experimentGroupId)
        {
            return experimentDao.GetExperimentGroup(experimentGroupId);
        }

        /// <summary>
        /// SearchUserExperimentGroups
        /// </summary>
        public PagedList<UserExperimentGroup> SearchUserExperimentGroups(string username, bool bExactMatch, string experimentId, string experimentStatus, string orderBy, int pageNum, int pageSize, bool includeExperimentGroups = true)
        {
            return experimentDao.SearchUserExperimentGroups(username, bExactMatch, experimentId, experimentStatus, orderBy, pageNum, pageSize, includeExperimentGroups);
        }

        /// <summary>
        /// GetActiveUserExperimentGroups - returns info from all records in db corresponding to active experiments the user is taking part in
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<UserExperimentGroup> GetActiveUserExperimentGroups(int userId, bool includeExperimentGroups = true)
        {
            return experimentDao.GetActiveUserExperimentGroups(userId, includeExperimentGroups);
        }

        /// <summary>
        /// GetUserExperimentGroup - returns info from one record in db corresponding to experiment and user
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public UserExperimentGroup GetUserExperimentGroup(string userGroupId, bool includeExperimentGroups = true)
        {
            return experimentDao.GetUserExperimentGroup(userGroupId, includeExperimentGroups);
        }

        /// <summary>
        /// GetUserExperimentGroupByExperimentId - returns info from one record in db corresponding to experiment and user
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public UserExperimentGroup GetUserExperimentGroupByExperimentId(int userId, string experimentId, bool includeExperimentGroups = true)
        {
            return experimentDao.GetUserExperimentGroupByExperimentId(userId, experimentId, includeExperimentGroups);
        }

        /// <summary>
        /// Returns a WorldExperimentGroup.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public WorldExperimentGroup GetWorldExperimentGroup(int zoneInstanceId, int zoneType, string groupId, bool includeExperimentGroups = true)
        {
            return experimentDao.GetWorldExperimentGroup(zoneInstanceId, zoneType, groupId, includeExperimentGroups);
        }

        /// <summary>
        /// Returns a WorldExperimentGroup.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public WorldExperimentGroup GetWorldExperimentGroupByExperimentId(int zoneInstanceId, int zoneType, string experimentId, bool includeExperimentGroups = true)
        {
            return experimentDao.GetWorldExperimentGroupByExperimentId(zoneInstanceId, zoneType, experimentId, includeExperimentGroups);
        }

        /// <summary>
        /// Pulls a list of Experiments that are available for the specified world to join.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<Experiment> GetAvailableExperimentsByWorldContext(int zoneInstanceId, int zoneType, string context = "Request", bool includeExperimentGroups = true)
        {
            return experimentDao.GetAvailableExperimentsByWorldContext(zoneInstanceId, zoneType, context, includeExperimentGroups);
        }

        /// <summary>
        /// Returns info from all records in db corresponding to active experiments the world is taking part in
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<WorldExperimentGroup> GetActiveWorldExperimentGroups(int zoneInstanceId, int zoneType, bool includeExperimentGroups = true)
        {
            return experimentDao.GetActiveWorldExperimentGroups(zoneInstanceId, zoneType, includeExperimentGroups);
        }

        #endregion
    }
}
