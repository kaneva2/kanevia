///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Text.RegularExpressions;
using Kaneva.BusinessLayer.BusinessObjects.API;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;
//using Kaneva.Framework.Transactions;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class BlastFacade
    {
        private IBlastDao blastDao = DataAccess.BlastDao;

        /// <summary>
        /// FormatBlast
        /// </summary>
        public string FormatBlast(int blastType, string subject, string strURL)
        {
            if (blastType.Equals((int)BlastTypes.BASIC) || blastType.Equals((int)BlastTypes.VIDEO)
                || blastType.Equals((int)BlastTypes.PHOTO) || blastType.Equals((int)BlastTypes.LINK)
                || blastType.Equals((int)BlastTypes.EVENT) || blastType.Equals((int)BlastTypes.PLACE)
                || blastType.Equals((int)BlastTypes.ADMIN) || blastType.Equals((int) BlastTypes.SHOP_PURCHASE)
				|| blastType.Equals((int)BlastTypes.SHOP_UPLOAD) || blastType.Equals((int)BlastTypes.REQUEST_3DAPP))
            {
                return String.Format(subject, strURL);
            }
            else
            {
                return subject;
            }
        }

        /// <summary>
        /// FormatBlast
        /// </summary>
        public string FormatBlast (int blastType, string subject, string strURL, string diaryText)
        {
            if (blastType.Equals ((int) BlastTypes.BASIC) || blastType.Equals ((int) BlastTypes.VIDEO)
                || blastType.Equals ((int) BlastTypes.PHOTO) || blastType.Equals ((int) BlastTypes.LINK)
                || blastType.Equals ((int) BlastTypes.EVENT) || blastType.Equals ((int) BlastTypes.PLACE)
                || blastType.Equals ((int) BlastTypes.ADMIN))
            {
                return String.Format (subject, strURL, Common.TruncateWithEllipsis(StripHTML(diaryText, "Text Blast"), 80));
            }                                          
            else
            {
                return subject; 
            }
        }

        #region Admin Blasts
        /// <summary>
        /// SendAdminBlast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendAdminBlast (int userId, DateTime dtExpires, string username, string subject, string blastText, string cssHightLight, string thumbnailPath)
        {
            UInt64 entryId = blastDao.GetNewEntryId ();
            return SendAdminBlast (entryId, userId, dtExpires, subject, blastText, cssHightLight, thumbnailPath, true);
        }

        /// <summary>
        /// SendAdminBlast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendAdminBlast (int userId, DateTime dtExpires, string subject, string blastText, string cssHightLight, string thumbnailPath, bool sticky)
        {
            UInt64 entryId = blastDao.GetNewEntryId ();
            return SendAdminBlast (entryId, userId, dtExpires, subject, blastText, cssHightLight, thumbnailPath, sticky);
        }
        
        /// <summary>
        /// SendAdminBlast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendAdminBlast(UInt64 entryId, int userId, DateTime dtExpires, string username, string subject, string blastText, string cssHightLight, string thumbnailPath)
        {
            return SendAdminBlast(entryId, userId, dtExpires, subject, blastText, cssHightLight, thumbnailPath, true);
        }

        /// <summary>
        /// SendAdminBlast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendAdminBlast (UInt64 entryId, int userId, DateTime dtExpires, string subject, string blastText, string cssHightLight, string thumbnailPath, bool sticky)
        {
            // Increment number of blasts sent
            IncrementBlastsSent(userId, BlastTypes.ADMIN);

            string diaryText = StripFormatUnfriendlyCharacters(blastText);
            return blastDao.SendAdminBlast(entryId, userId, dtExpires, subject, blastText, cssHightLight, thumbnailPath, sticky);
        }

        /// <summary>
        /// SendAdminTextBlast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendAdminTextBlast(int userId, DateTime dtExpires, string username, string nameNoSpaces, string blastText, string cssHightLight, string thumbnailPath, bool isCommunityBlast)
        {
            string subject = "<a href=\"" + Common.GetChannelUrl (nameNoSpaces, isCommunityBlast) + "\" >" + username + "</a>" +
                " blasts <a href=\"{0}\">{1}</a>";

            string diaryText = StripFormatUnfriendlyCharacters (blastText);

            return SendAdminBlast(userId, dtExpires, username, subject, diaryText, cssHightLight, thumbnailPath);
        }

        /// <summary>
        /// SendAdminVideoBlast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendAdminVideoBlast (int userId, DateTime dtExpires, string username, string nameNoSpaces, string caption, string embed, string cssHightLight, string thumbnailPath, bool isCommunityBlast)
        {
            string subject = "<a href=\"" + Common.GetChannelUrl (nameNoSpaces, isCommunityBlast) + "\" >" + username + "</a>" +
                " blasts a <a href=\"{0}\">" + StripFormatUnfriendlyCharacters(Common.Truncate(StripHTML(caption, "Video"), 60)) + " [video]</a>";

            string diaryText = StripFormatUnfriendlyCharacters(embed + "<BR><BR>" + caption);

            return SendAdminBlast(userId, dtExpires, username, subject, diaryText, cssHightLight, thumbnailPath);
        }

        /// <summary>
        /// SendAdminPhotoBlast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendAdminPhotoBlast (int userId, DateTime dtExpires, string username, string nameNoSpaces, string url, string caption, string cssHightLight, string thumbnailPath, bool isCommunityBlast)
        {
            string subject = "<a href=\"" + Common.GetChannelUrl (nameNoSpaces, isCommunityBlast) + "\" >" + username + "</a>" +
                " blasts a <a href=\"{0}\">" + StripFormatUnfriendlyCharacters(Common.Truncate(StripHTML(caption, "Photo"), 60)) + " [photo]</a>";

            string diaryText = StripFormatUnfriendlyCharacters(url + "<BR><BR>" + caption);

            return SendAdminBlast(userId, dtExpires, username, subject, diaryText, cssHightLight, thumbnailPath);
        }

        /// <summary>
        /// SendAdminLinkBlast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendAdminLinkBlast (int userId, DateTime dtExpires, string username, string nameNoSpaces, string name, string URL, string Description, string cssHightLight, string thumbnailPath, bool isCommunityBlast)
        {
            string subject = "<a href=\"" + Common.GetChannelUrl (nameNoSpaces, isCommunityBlast) + "\" >" + username + "</a>" +
                " blasts a link <a href=\"{0}\">" + StripFormatUnfriendlyCharacters(Common.Truncate(StripHTML(name, "link"), 60)) + " [link]</a>";

            string diaryText = StripFormatUnfriendlyCharacters("<a href=\"" + URL + "\">" + name + "</a><BR><BR>" + Description);

            return SendAdminBlast(userId, dtExpires, username, subject, diaryText, cssHightLight, thumbnailPath);
        }

        /// <summary>
        /// SendAdminEventBlast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendAdminEventBlast (int userId, DateTime dtExpires, string username, string nameNoSpaces, string eventName, string location, string address, string description, string strDate, DateTime dtEventDate, string cssHightLight, string thumbnailPath, bool isCommunityBlast)
        {
            UInt64 entryId = blastDao.GetNewEntryId();
            int result = 0;

            string subject = "<a href=\"" + Common.GetChannelUrl (nameNoSpaces, isCommunityBlast) + "\" >" + username + "</a> " +
                " blasts <a href=\"{0}\">" + StripFormatUnfriendlyCharacters(eventName) + " [event]</a>";

            string diaryText = "<TABLE border=0 width=300px>" +
                "<TR><TD Width=100px align=left>Event Name:</TD><TD Width=200px align=left>" + eventName + "</TD></TR>" +
                "<TR><TD align=left>Date:</TD><TD align=left>" + strDate + "</TD></TR>" +
                "<TR><TD align=left>Location Name:</TD><TD align=left>" + location + "</TD></TR>";

            if (address.Length > 0)
            {
                diaryText += "<TR><TD align=left>Address:</TD><TD align=left>" + address + "</TD></TR>";
            }

            diaryText += "<TR><TD align=left valign=top>Description:</TD><TD align=left>" + description + "</TD></TR>" +
            "</TABLE>";

            diaryText = StripFormatUnfriendlyCharacters(diaryText);

            result = SendAdminBlast (entryId, userId, dtExpires, username, subject, diaryText, cssHightLight, thumbnailPath);

            if (result.Equals(1))
            {
                subject = "<a href=\"{0}\">" + eventName + "</a> / " + strDate + " / " + location + " / " +
                    " <a href=\"" + Common.GetChannelUrl (nameNoSpaces, isCommunityBlast) + "\" >" + username + "</a>";

                result = blastDao.InsertUserEvent (entryId, subject, diaryText, userId, 0, dtEventDate, cssHightLight, eventName, location);
            }
            return result;
        }

        /// <summary>
        /// SendAdminPlaceBlast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendAdminPlaceBlast (int userId, DateTime dtExpires, string username, string nameNoSpaces, string location, string address, string description, string cssHightLight, string thumbnailPath, bool isCommunityBlast)
        {
            string subject = "<a href=\"" + Common.GetChannelUrl (nameNoSpaces, isCommunityBlast) + "\" >" + username + "</a>" +
                " blasts <a href=\"{0}\">" + StripFormatUnfriendlyCharacters(Common.Truncate(location, 60)) + " [place]</a>";

            string diaryText = "<TABLE border=0 width=300px>" +
                "<TR><TD Width=100px align=left>Location Name:</TD><TD Width=200px align=left>" + location + "</TD></TR>";

            if (address.Length > 0)
            {
                diaryText += "<TR><TD align=left>Address:</TD><TD align=left>" + address + "</TD></TR>";
            }

            diaryText += "<TR><TD align=left valign=top>Description:</TD><TD align=left>" + description + "</TD></TR>" +
            "</TABLE>";

            diaryText = StripFormatUnfriendlyCharacters(diaryText);

            return SendAdminBlast (userId, dtExpires, username, subject, diaryText, cssHightLight, thumbnailPath);
        }
        #endregion

        #region Active Blasts
        /// <summary>
        /// SendTextBlast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendTextBlast (int userId, int communityId, string username, string nameNoSpaces, string blastText, string cssHightLight, string thumbnailPath)
        {
            return SendTextBlast (userId, communityId, "", false, username, nameNoSpaces, blastText, cssHightLight, thumbnailPath, false);
        }
        /// <summary>
        /// SendTextBlast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendTextBlast (int userId, int communityId, string communityName, bool isCommunityPersonal, string username, string nameNoSpaces, string blastText, string cssHightLight, string thumbnailPath, bool isCommunityBlast)
        {
            string subject = "";

            string diaryText = StripFormatUnfriendlyCharacters (blastText);

            return SendActiveBlast (BlastTypes.BASIC, userId, username, communityId, communityName, isCommunityPersonal, subject, diaryText, cssHightLight, thumbnailPath, 0);
        }

         /// <summary>
        /// SendVideoBlast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendVideoBlast (int userId, int communityId, string communityName, bool isCommunityPersonal, string username, string nameNoSpaces, string caption, string embed, string cssHightLight, string thumbnailPath)
        {
            return SendVideoBlast (userId, communityId, communityName, isCommunityPersonal, username, nameNoSpaces, caption, embed, cssHightLight, thumbnailPath, false);
        }
        /// <summary>
        /// SendVideoBlast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendVideoBlast (int userId, int communityId, string communityName, bool isCommunityPersonal, string username, string nameNoSpaces, string caption, string embed, string cssHightLight, string thumbnailPath, bool isCommunityBlast)
        {
            string diaryText = StripFormatUnfriendlyCharacters(embed);

            return SendActiveBlast (BlastTypes.VIDEO, userId, username, communityId, communityName, isCommunityPersonal, caption, diaryText, cssHightLight, thumbnailPath, 0);
        }

        /// <summary>
        /// SendPhotoBlast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendPhotoBlast (int userId, int communityId, string communityName, bool isCommunityPersonal, string username, string nameNoSpaces, string url, string caption, string cssHightLight, string thumbnailPath)
        {
            return SendPhotoBlast (userId, communityId, communityName, isCommunityPersonal, username, nameNoSpaces, url, caption, cssHightLight, thumbnailPath, false);
        }
        /// <summary>
        /// SendPhotoBlast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendPhotoBlast (int userId, int communityId, string communityName, bool isCommunityPersonal, string username, string nameNoSpaces, string url, string caption, string cssHightLight, string thumbnailPath, bool isCommunityBlast)
        {
            string subject = "";

            string diaryText = StripFormatUnfriendlyCharacters (url + "<div class=\"feedPhotoText\">" + caption + "</div>");

            return SendActiveBlast (BlastTypes.PHOTO, userId, username, communityId, communityName, isCommunityPersonal, subject, diaryText, cssHightLight, thumbnailPath, 0);
        }

        /// <summary>
        /// Send a Camera Screenshot
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendCameraScreenshotBlast(int senderId, int communityId, string screenshotURL, string caption, int assetId)
        {
            UserFacade userFacade = new UserFacade();
            User sender = userFacade.GetUser(senderId);

            CommunityFacade communityFacade = new CommunityFacade();
            Community community = communityFacade.GetCommunity(communityId);

            // Request Tracking
            string requestId = userFacade.InsertTrackingRequest(37, sender.UserId, 0, 0);
            string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent(requestId, 2, 0);

            string senderProfileURL = Common.GetPersonalChannelUrl(sender.NameNoSpaces) + "?RTSID=" + requestTypeSentIdBlast;
            string senderThumbnailURL = Common.GetProfileImageURL(sender.ThumbnailSmallPath, "sm", sender.Gender, sender.FacebookSettings.UseFacebookProfilePicture, sender.FacebookSettings.FacebookUserId, false);
            
            // Fix for extra /
            senderThumbnailURL.TrimStart('/');
            
            string worldProfileURL = Common.GetBroadcastChannelUrl(community.NameNoSpaces) + "?RTSID=" + requestTypeSentIdBlast;
            string photoURL = StripFormatUnfriendlyCharacters(screenshotURL);

            // Fix for extra /
            senderThumbnailURL.TrimStart('/');

            string communityType = community.Name;
            string commProfileLink = worldProfileURL;
            string commProfileName = community.Name;

            string assetDetailsPage = Common.GetAssetDetailsLink (assetId) + "?RTSID=" + requestTypeSentIdBlast;

            string passiveSubject =
                "<a href=\"" + senderProfileURL + "\" >" + sender.Username + "</a> shared a <a class=\"plainLink\" href=\"" + assetDetailsPage + "\" >photo</a> " + commProfileName;
            string activeSubject =
                "<a href=\"" + senderProfileURL + "\" >" + sender.Username + "</a> shared a <a class=\"plainLink\" href=\"" + assetDetailsPage + "\" >photo</a> from <a name=\"locale\" href=\"" + commProfileLink + "\">" + communityType + "</a>";

            string diaryText = "<div class=\"feedScreenShotText\">" + caption + "<p>Click image to visit</p></div>" + photoURL;
    
            // Send passive blast to sender for notification
            SendFriendPassiveBlast(BlastTypes.CAMERA_SCREENSHOT, sender.UserId, sender.Username, passiveSubject, "");

            // Send active blast to sender 
            return SendActiveBlast(BlastTypes.CAMERA_SCREENSHOT, sender.UserId, sender.Username, community.CommunityId, community.Name, false, activeSubject, diaryText, "", senderThumbnailURL, 0);
        }

        /// <summary>
        /// Send a Camera Screenshot
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendCameraScreenshotBlast (int senderId, string communityName, string screenshotURL, string caption, int assetId)
        {
            UserFacade userFacade = new UserFacade ();
            User sender = userFacade.GetUser (senderId);

            CommunityFacade communityFacade = new CommunityFacade ();
//            Community community = communityFacade.GetCommunity (communityId);

            // Request Tracking
            string requestId = userFacade.InsertTrackingRequest (37, sender.UserId, 0, 0);
            string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent (requestId, 2, 0);

            string senderProfileURL = Common.GetPersonalChannelUrl (sender.NameNoSpaces) + "?RTSID=" + requestTypeSentIdBlast;
            string senderThumbnailURL = Common.GetProfileImageURL (sender.ThumbnailSmallPath, "sm", sender.Gender, sender.FacebookSettings.UseFacebookProfilePicture, sender.FacebookSettings.FacebookUserId, false);
   //         string worldProfileURL = Common.GetBroadcastChannelUrl (community.NameNoSpaces) + "?RTSID=" + requestTypeSentIdBlast;
            string photoURL = StripFormatUnfriendlyCharacters (screenshotURL);

            // Fix for extra /
            senderThumbnailURL.TrimStart('/');

   //         string communityType = "";
   //         string commProfileLink = "";
   //         string commProfileName = "";

   //         communityType = community.Name;
   //         commProfileLink = worldProfileURL;
   //         commProfileName = community.Name;

            string assetDetailsPage = Common.GetAssetDetailsLink (assetId) + "?RTSID=" + requestTypeSentIdBlast;

            string passiveSubject =
                "<a href=\"" + senderProfileURL + "\" >" + sender.Username + "</a> shared a <a class=\"plainLink\" href=\"" + assetDetailsPage + "\" >photo</a> " + communityName;
            string activeSubject =
                "<a href=\"" + senderProfileURL + "\" >" + sender.Username + "</a> shared a <a class=\"plainLink\" href=\"" + assetDetailsPage + "\" >photo</a> from <a name=\"locale\" href=\"javascript:void(0);\">" + communityName + "</a>";

            string diaryText = "<div class=\"feedScreenShotText\">" + caption + "<p>Click image to visit</p></div>" + photoURL;

            // Send passive blast to sender for notification
            SendFriendPassiveBlast (BlastTypes.CAMERA_SCREENSHOT, sender.UserId, sender.Username, passiveSubject, "");

            // Send active blast to sender 
            return SendActiveBlast (BlastTypes.CAMERA_SCREENSHOT, sender.UserId, sender.Username, 0, communityName, false, activeSubject, diaryText, "", senderThumbnailURL, 0);
        }


		/// <summary>
		/// SendShopPurchaseItemBlast
		/// </summary>
		[DataObjectMethod(DataObjectMethodType.Insert)]
		public int SendShopPurchaseItemBlast(int userId, string username, string nameNoSpaces, string itemName, string itemDesc,  
			string url,string itemThumbnail,  string cssHightLight, string thumbnailPath)
		{
			string subject = "";
			string imgLink = "<a href=\"" + url +"\" target=\"_blank\"><img src=\"" + itemThumbnail + "\"/></a>";
			string nameLink = username + " got <a href=\"" + url + "\"  target=\"_blank\">" + itemName + "</a>";

			string diaryText = StripFormatUnfriendlyCharacters(imgLink + "<div class=\"feedPhotoText\">" + nameLink + "<br />" + itemDesc + "</div>");

			return SendActiveBlast(BlastTypes.SHOP_PURCHASE, userId, username, 0, "", false, subject, diaryText, cssHightLight, thumbnailPath, 0);
		}

		/// <summary>
		/// SendShopUploadItemBlast
		/// </summary>
		[DataObjectMethod(DataObjectMethodType.Insert)]
		public int SendShopUploadItemBlast(int userId, string username, string nameNoSpaces, string itemName, string itemDesc,
			string url, string itemThumbnail, string cssHightLight, string thumbnailPath)
		{
			string subject = "";
			string imgLink = "<a href=\"" + url + "\"   target=\"_blank\"><img src=\"" + itemThumbnail + "\"/ ></a>";
			string nameLink = username + " added <a href=\"" + url + "\"   target=\"_blank\">" + itemName + "</a>";

			string diaryText = StripFormatUnfriendlyCharacters(imgLink + "<div class=\"feedPhotoText\">" + nameLink + "<br />" + itemDesc + "</div>");

			return SendActiveBlast(BlastTypes.SHOP_UPLOAD, userId, username, 0, "", false, subject, diaryText, cssHightLight, thumbnailPath, 0);
		}

        /// <summary>
        /// SendLinkBlast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendLinkBlast (int userId, int communityId, string username, string nameNoSpaces, string name, string URL, string Description, string cssHightLight, string thumbnailPath)
        {
            return SendLinkBlast (userId, communityId, "", false, username, nameNoSpaces, name, URL, Description, cssHightLight, thumbnailPath, false);
        }
        /// <summary>
        /// SendLinkBlast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendLinkBlast (int userId, int communityId, string communityName, bool isCommunityPersonal, string username, string nameNoSpaces, string name, string URL, string Description, string cssHightLight, string thumbnailPath)
        {
            return SendLinkBlast (userId, communityId, communityName, isCommunityPersonal, username, nameNoSpaces, name, URL, Description, cssHightLight, thumbnailPath, false);
        }
        /// <summary>
        /// SendLinkBlast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendLinkBlast (int userId, int communityId, string communityName, bool isCommunityPersonal, string username, string nameNoSpaces, string name, string URL, string Description, string cssHightLight, string thumbnailPath, bool isCommunityBlast)
        {
            string subject = "";

            string diaryText = "<div class=\"linkBlast\">" + StripFormatUnfriendlyCharacters("<a href=\"" + URL + "\" target=\"blank\">" + name + "</a><div>" + Description + "</div></div>");

            return SendActiveBlast (BlastTypes.LINK, userId, username, communityId, communityName, isCommunityPersonal, subject, diaryText, cssHightLight, thumbnailPath, 0);
        }

        /// <summary>
        /// SendEventBlast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendEventBlast (int userId, int communityId, string username, string nameNoSpaces, string eventName, string location, string address, string description, string strDate, DateTime dtEventDate, string cssHightLight, string thumbnailPath)
        {
            return SendEventBlast (userId, communityId, username, nameNoSpaces, eventName, location, address, description, strDate, dtEventDate, cssHightLight, thumbnailPath, false);
        }
        
		/// <summary>
        /// SendEventBlast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendEventBlast (int userId, int communityId, string username, string nameNoSpaces, string eventName, string location, string address, string description, string strDate, DateTime dtEventDate, string cssHightLight, string thumbnailPath, bool isCommunityBlast)
        {
            int result = 0;
            UInt64 strEntryId = blastDao.GetNewEntryId();

            eventName = StripFormatUnfriendlyCharacters(eventName);
            location = StripFormatUnfriendlyCharacters(location);

            string subject = "";

            string diaryText = "<TABLE border=\"0\" cellpadding=\"3\" cellspacing=\"0\" width=\"98%\">" +
                "<TR><TD width=\"100px\" align=\"left\">Event Name:</TD><TD width=\"200px\" align=\"left\">" + eventName + "</TD></TR>" +
                "<TR><TD align=\"left\">Date:</TD><TD align=\"left\">" + strDate + "</TD></TR>" +
                "<TR><TD align=\"left\">Location Name:</TD><TD align=\"left\">" + location + "</TD></TR>";

            if (address.Length > 0)
            {
                diaryText += "<TR><TD align=\"left\">Address:</TD><TD align=left>" + address + "</TD></TR>";
            }

            diaryText += "<TR><TD align=\"left\" valign=\"top\">Description:</TD><TD align=\"left\">" + description + "</TD></TR>" +
            "</TABLE>";

            diaryText = StripFormatUnfriendlyCharacters(diaryText);

            result = SendEventBlast (strEntryId, BlastTypes.EVENT, userId, username, communityId, subject, diaryText, cssHightLight, thumbnailPath);

            if (result.Equals(1))
            {
                subject = "<a href=\"{0}\">" + eventName + "</a> / " + strDate + " / " + location + " / " +
                    " <a href=\"" + Common.GetChannelUrl (nameNoSpaces, isCommunityBlast) + "\" >" + username + "</a>";

                result = blastDao.InsertUserEvent(strEntryId, subject, diaryText, userId, communityId, dtEventDate, cssHightLight, eventName, location);
            }

            return result;
        }

        /// <summary>
        /// SendPlaceBlast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendPlaceBlast (int userId, int communityId, string communityName, bool isCommunityPersonal, string username, string nameNoSpaces, string location, string address, string description, string cssHightLight, string thumbnailPath)
        {
            return SendPlaceBlast (userId, communityId, communityName, isCommunityPersonal, username, nameNoSpaces, location, address, description, cssHightLight, thumbnailPath, false);
        }
        
		/// <summary>
        /// SendPlaceBlast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendPlaceBlast (int userId, int communityId, string communityName, bool isCommunityPersonal, string username, string nameNoSpaces, string location, string address, string description, string cssHightLight, string thumbnailPath, bool isCommunityBlast)
        {
            string subject = "";

            string diaryText = "<div class=\"linkPlace\">" +
                "<div><span class=\"label\">Location Name:</span><span>" + StripFormatUnfriendlyCharacters (location) + "</span></div>";
            
            if (address.Length > 0)
            {
                diaryText += "<div><span class=\"label\">Address:</span><span>" + StripFormatUnfriendlyCharacters (address) + "</span></div>";
            }

            diaryText += "<div><span class=\"label\">Description:</span><span>" + StripFormatUnfriendlyCharacters (description) + "</span></div>" +
                "</div>";

            return SendActiveBlast (BlastTypes.PLACE, userId, username, communityId, communityName, isCommunityPersonal, subject, diaryText, cssHightLight, thumbnailPath, 0);
        }

		/// <summary>
		/// SendAppRequest
		/// </summary>
		[DataObjectMethod(DataObjectMethodType.Insert)]
		public int SendAppRequest(int fromUserId, int toUserId, string requestDescriptionText, string requestActionLinkText,
			string appName, int gameId, string appUrl, string requestImage, string cssHightLight, string subject)
		{
			//appUrl format: http://www.kaneva.com/kgp/playwok.aspx?goto=U<gameid>&ILC=MM3D&link=private
			
			
			int gameCommunityId = new CommunityFacade().GetCommunityIdFromGameId(gameId);
			Community gameCommunity = new CommunityFacade().GetCommunity(gameCommunityId);

			User fromUser = new UserFacade().GetUser(fromUserId);
			User toUser = new UserFacade().GetUser(toUserId);

			// Check if the user has blocked this.
			// if so then return -1 
			if (new UserFacade().IsBlastBlocked(toUser.UserId, fromUser.UserId, gameCommunityId))
			{
				return -1;
			}

			string fromUserLink = "<a href=\"" + fromUser.URL + "\">" + fromUser.Username + "</a>";
			string appLink = "<a href=\"" + appUrl + "\">" + appName + "</a>";			
			string imgLink = "<a href=\"" + appUrl + "\" target=\"_blank\"><img src=\"" + requestImage + "\"/></a>";
            string actionLink = "<a id=\"aAccept\" name=\"aAccept\" href=\"" + appUrl + "\">" + requestActionLinkText + "</a>";

            string diaryText = requestDescriptionText;

            // Substitutions...  ^lineBreak^ ^senderName^ ^appLink^
            diaryText = diaryText.Replace("^lineBreak^", "<br/>");
            diaryText = diaryText.Replace("^senderName^", fromUserLink);
            diaryText = diaryText.Replace("^appLink^", actionLink);

            // New per Billy request
            diaryText = diaryText.Replace("^gamename^", gameCommunity.Name);
            diaryText = diaryText.Replace("^gameName^", gameCommunity.Name);
            diaryText = diaryText.Replace("^recipientName^", toUser.Username);
            diaryText = diaryText.Replace("^recepientName^", toUser.Username);
            diaryText = diaryText.Replace("^receipientName^", toUser.Username);
            diaryText = diaryText.Replace("^recipientAvatar^", "<img src=\"" + Common.GetProfileImageURL(fromUser.ThumbnailSmallPath, "sm", fromUser.Gender) + "\" border=\"0\"/>");

            diaryText = diaryText.Replace("^senderRealName^", fromUser.DisplayName);
            diaryText = diaryText.Replace("^recipientRealName^", toUser.DisplayName);

            diaryText = diaryText.Replace("^senderDisplayThenUserName^", (fromUser.DisplayName + (string.IsNullOrEmpty(fromUser.DisplayName) ? fromUser.Username : " (" + fromUser.Username + ")")));
            diaryText = diaryText.Replace("^senderUserThenDisplayName^", (fromUser.Username + (string.IsNullOrEmpty(fromUser.DisplayName) ? "" : " (" + fromUser.DisplayName + ")")));

            //string strBlockText = "<div class=\"time\"> " +
            //        "<a href=\"http://" + Configuration.SiteName + "/mykaneva/blast/blastBlockAction.aspx?a=b&u=0&c=" + gameCommunityId + "&d=prefs\">Block " + appName + "</a>" +
            //        " | " +
            //        "<a href=\"http://" + Configuration.SiteName +
            //        "/mykaneva/blast/blastBlockAction.aspx?a=b&u=" + fromUser.UserId + "&c=" + gameCommunityId + "&d=prefs\">Block requests from " + fromUser.Username + "</a> " +
            //        "</div>";

            diaryText = StripFormatUnfriendlyCharacters("<div class=\"feedPhotoText\">" + diaryText + "</div>");
			
			// send blast
			int blastRet = SendActiveBlast(BlastTypes.REQUEST_3DAPP, fromUser.UserId, fromUser.Username, gameCommunity.CommunityId,
				gameCommunity.Name, true, "", diaryText, cssHightLight, fromUser.ThumbnailSmallPath, toUser.UserId);
			
			//Send kmail 	
            string kmailSubject = subject;
            if (subject.Length.Equals (0))
            {
                // Default subject
                kmailSubject = fromUser.Username + " " + requestDescriptionText + " " + appName;
            }

            kmailSubject = kmailSubject.Replace("^gamename^", gameCommunity.Name);
            kmailSubject = kmailSubject.Replace("^recipientName^", toUser.Username);
            kmailSubject = kmailSubject.Replace("^senderRealName^", toUser.DisplayName);
            kmailSubject = kmailSubject.Replace("^recipientRealName^", fromUser.DisplayName);

            string kmailText = "<table><tr><td valign=top><img src=" + Common.GetBroadcastChannelImageURL(gameCommunity.ThumbnailSmallPath, "sm") + " border=\"0\"/></td><td valign=top><strong>" + diaryText + "</strong></td></tr></table>";

			UserFacade userFacade = new UserFacade();
			Message message = new Message(0, fromUserId, toUserId, kmailSubject,
				kmailText, new DateTime(), 0, 5, gameCommunity.CommunityId, "U", "S");

			userFacade.InsertMessage(message);


            // Do the request notification tracking
            GameFacade gameFacade = new GameFacade();
            gameFacade.InsertNotificationAppRequest(toUserId, gameId);

            // Used for Places menu 'NEW' box on 3D Apps
            gameFacade.InsertNotificationAppRequest(toUserId, 3296);
 

			return blastRet;
		}


        public int SendAchievementEarned (int userId, int gameCommunityId, uint achievementId, string linkText, string appUrl)
        {
            User toUser = new UserFacade ().GetUser (userId);
            Community gameCommunity = new CommunityFacade ().GetCommunity (gameCommunityId);
            Achievement achievement = new GameFacade ().GetAchievement (achievementId);

            string strLink = "";

            if (linkText.Length > 0)
            {
                strLink = "<div><a href=\"" + appUrl + "\">" + linkText + "</a></div>";
            }

            string diaryText = "<img src=\"" + Configuration.GetAchievementImageURL(achievement.ImageUrl, "me") + "\"/>" +
                "<div class=\"feedAchievementText\">" + 
                "<div class=\"bold\">" + achievement.Name + "</div>" +
                "<div>" + achievement.Description + "</div>" +
                "<div class=\"bold\">" + achievement.Points + " points</div>" +
                strLink +
                "</div>";

            if (linkText.Length > 0)
            {

            }

            diaryText = StripFormatUnfriendlyCharacters (diaryText);

            // send blast
            int blastRet = SendActiveBlast (BlastTypes.ACHIEVEMENT_3DAPP, toUser.UserId, toUser.Username, 0,
                gameCommunity.Name, true, "", diaryText, "", toUser.ThumbnailSmallPath, 0);

            return blastRet;
        }

        /* Contest Blasts */

        /// <summary>
        /// SendContestCreatedBlast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendContestCreatedBlast (int userId, string contestUrl, string contestTitle, int prizeAmt,  
            string username, string nameNoSpaces, string thumbnailPath)
        {   
            string subject = "";
            string nameLink = username + " added <a href=\"" + contestUrl + "\">" + contestTitle + "</a>";

            string diaryText = StripFormatUnfriendlyCharacters ("<img src=\"http://" + Configuration.SiteName + "/images/contests/contests_ribbon_44x63.png\"/ >" +
                "<div class=\"feedContestText\">" +
                    "<div>New Contest Started!</div>" +
                    "<div class=\"bold\"><a href=\"" + contestUrl + "\">" + contestTitle + "</a></div>" +
                    "<div><span class=\"bold\">Prize: </span> " + prizeAmt.ToString() + " Credits</div>" +
                    "<div><a href=\"" + contestUrl + "\" class=\"small\">Enter Now!</a></div>" +
                "</div>");

            return SendActiveBlast (BlastTypes.CONTEST, userId, username, 0, "", false, subject, diaryText, "", thumbnailPath, 0);
        }

        /// <summary>
        /// SendContestEnteredBlast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendContestEnteredBlast (int userId, string contestUrl, string contestTitle, int prizeAmt,
            string username, string nameNoSpaces, string thumbnailPath, string gender)
        {
            string subject = "";
            string nameLink = username + " added <a href=\"" + contestUrl + "\">" + contestTitle + "</a>";

            string diaryText = StripFormatUnfriendlyCharacters ("<img src=\"http://" + Configuration.SiteName + "/images/contests/contests_ribbon_44x63.png\"/ >" +
                "<div class=\"feedContestText\">" +
                    "<div>Entered a Contest!</div>" +
                    "<div class=\"bold\"><a href=\"" + contestUrl + "\">" + contestTitle + "</a></div>" +
                    "<div><span class=\"bold\">Prize: </span> " + prizeAmt.ToString () + " Credits</div>" +
                    "<div><a href=\"" + contestUrl + "\" class=\"small\">View " +
                    (gender.ToUpper() == "M" ? "his" : "her") + " entry!</a></div>" +
                "</div>");

            return SendActiveBlast (BlastTypes.CONTEST, userId, username, 0, "", false, subject, diaryText, "", thumbnailPath, 0);
        }

        /// <summary>
        /// SendContestVotedBlast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendContestVotedBlast (int userId, string contestUrl, string contestTitle, int prizeAmt,
            string username, string nameNoSpaces, string thumbnailPath)
        {
            string subject = "";
            string nameLink = username + " added <a href=\"" + contestUrl + "\">" + contestTitle + "</a>";

            string diaryText = StripFormatUnfriendlyCharacters ("<img src=\"http://" + Configuration.SiteName + "/images/contests/contests_ribbon_44x63.png\"/ >" +
                "<div class=\"feedContestText\">" +
                    "<div>Voted on a Contest!</div>" +
                    "<div class=\"bold\"><a href=\"" + contestUrl + "\">" + contestTitle + "</a></div>" +
                    "<div><span class=\"bold\">Prize: </span> " + prizeAmt.ToString () + " Credits</div>" +
                    "<div><a href=\"" + contestUrl + "\" class=\"small\">View Contest</a></div>" +
                "</div>");

            return SendActiveBlast (BlastTypes.CONTEST, userId, username, 0, "", false, subject, diaryText, "", thumbnailPath, 0);
        }
        
        /// <summary>
        /// SendContestFollowingBlast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendContestFollowingBlast (int userId, string contestUrl, string contestTitle, int prizeAmt,
            string username, string nameNoSpaces, string thumbnailPath)
        {
            string subject = "";
            string nameLink = username + " added <a href=\"" + contestUrl + "\">" + contestTitle + "</a>";

            string diaryText = StripFormatUnfriendlyCharacters ("<img src=\"http://" + Configuration.SiteName + "/images/contests/contests_ribbon_44x63.png\"/ >" +
                "<div class=\"feedContestText\">" +
                    "<div>Is following a Contest!</div>" +
                    "<div class=\"bold\"><a href=\"" + contestUrl + "\">" + contestTitle + "</a></div>" +
                    "<div><span class=\"bold\">Prize: </span> " + prizeAmt.ToString () + " Credits</div>" +
                    "<div><a href=\"" + contestUrl + "\" class=\"small\">Enter Now!</a></div>" +
                "</div>");

            return SendActiveBlast (BlastTypes.CONTEST, userId, username, 0, "", false, subject, diaryText, "", thumbnailPath, 0);
        }

        /// <summary>
        /// SendContestWinnerBlast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendContestWinnerBlast (int userId, string contestUrl, string contestTitle, int prizeAmt,
            string username, string nameNoSpaces, string thumbnailPath)
        {
            string subject = "";
            string nameLink = username + " added <a href=\"" + contestUrl + "\">" + contestTitle + "</a>";

            string diaryText = StripFormatUnfriendlyCharacters ("<img src=\"http://" + Configuration.SiteName + "/images/contests/contests_trophy_44x63.png\"/ >" +
                "<div class=\"feedContestText\">" +
                    "<div class=\"bold\">Won a Contest!</div>" +
                    "<div class=\"bold\"><a href=\"" + contestUrl + "\">" + contestTitle + "</a></div>" +
                    "<div><span class=\"bold\">Prize: </span> " + prizeAmt.ToString () + " Credits</div>" +
                    "<div><a href=\"" + contestUrl + "\" class=\"small\">View the Winner!</a></div>" +
                "</div>");

            return SendActiveBlast (BlastTypes.CONTEST, userId, username, 0, "", false, subject, diaryText, "", thumbnailPath, 0);
        }

		#endregion

        #region Passive Blasts
        /// <summary>
        /// Send a Profile Update Blast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendProfileUpdateBlast(int userId, string username, string nameNoSpaces)
        {
            string subject = "<a href=\"" + Common.GetPersonalChannelUrl(nameNoSpaces) + "\" >" + username + "</a>" +
                " updated the theme on their " +
                " <a href=\"" + Common.GetPersonalChannelUrl(nameNoSpaces) + "\">profile</a>";

            return SendFriendPassiveBlast(BlastTypes.PROFILE_UPDATE, userId, username, subject, "");
        }

        /// <summary>
        /// Send a Blog Blast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendBlogBlast(int userId, string username, string nameNoSpaces, int blogId, string strURL)
        {
            string subject = "<a href=\"" + Common.GetPersonalChannelUrl(nameNoSpaces) + "\" >" + username + "</a>" +
                " wrote on their " +
                " <a href=\"" + strURL + "\">blog</a>";

            return SendFriendPassiveBlast(BlastTypes.BLOG, userId, username, subject, "");
        }

        /// <summary>
        /// Send a Comment Blast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendCommentBlast(int userId, string username, string nameNoSpaces, int assetId, string assetName)
        {
            string subject = "<a href=\"" + Common.GetPersonalChannelUrl(nameNoSpaces) + "\" >" + username + "</a>" +
                " commented on " +
                " <a href=\"" + Common.GetAssetDetailsLink(assetId) + "\">" + assetName + "</a>";

            return SendFriendPassiveBlast(BlastTypes.MEDIA_COMMENT, userId, username, subject, "");
        }

        /// <summary>
        /// Send a Comment Blast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendReviewBlast(int userId, string username, string nameNoSpaces, int itemId, string itemName)
        {
            string subject = "<a href=\"" + Common.GetPersonalChannelUrl(nameNoSpaces) + "\" >" + username + "</a>" +
                " commented on " +
                " <a href=\"" + Common.GetWOKDetailsLink(itemId) + "\">" + itemName + "</a>";

            return SendFriendPassiveBlast(BlastTypes.MEDIA_COMMENT, userId, username, subject, "");
        }

        /// <summary>
        /// Send a Post Blast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendPostBlast(int userId, string username, string nameNoSpaces, int channelId, int topicId, string topicName)
        {
            string subject = "<a href=\"" + Common.GetPersonalChannelUrl(nameNoSpaces) + "\" >" + username + "</a>" +
                " posted in the " +
                " <a href=\"" + "http://" + Configuration.SiteName + "/forum/forumThreads.aspx?topicId=" + topicId + "&communityId=" + channelId + "#0" + "\">" + Common.Truncate(topicName, 20) + "</a>";

            return SendFriendPassiveBlast(BlastTypes.FORUM_POST, userId, username, subject, "");
        }

        /// <summary>
        /// Send a Friend Blast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendNewFriendBlast(int userId, string username, string nameNoSpaces, string usernameFriend, string nameNoSpacesFriend)
        {
            string subject = "<a href=\"" + Common.GetPersonalChannelUrl(nameNoSpaces) + "\" >" + username + "</a>" +
                " added a new friend " +
                " <a href=\"" + Common.GetPersonalChannelUrl(nameNoSpacesFriend) + "\" >" + usernameFriend + "</a>";

            return SendFriendPassiveBlast(BlastTypes.ADD_FRIEND, userId, username, subject, "");
        }

        /// <summary>
        /// Send a community join Blast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendCommunityJoinBlast(int userId, string username, string nameNoSpaces, string communityName, string communityNameNoSpaces)
        {
            string subject = "<a href=\"" + Common.GetPersonalChannelUrl(nameNoSpaces) + "\" >" + username + "</a>" +
                " joined the community " +
                " <a href=\"" + Common.GetBroadcastChannelUrl(communityNameNoSpaces) + "\">" + communityName + "</a>";

            return SendFriendPassiveBlast(BlastTypes.JOIN_COMMUNITY, userId, username, subject, "");
        }
       
        /// <summary>
        /// Send a Rave 3D Home Blast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendRave3DHomeBlast(User raver, Community community)
        {
            // Request Tracking
            UserFacade userFacade = new UserFacade();
            string requestId = userFacade.InsertTrackingRequest(23, raver.UserId, 0, 0);

            string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent(requestId, 2, 0);
            string subject = "<a href=\"" + Common.GetPersonalChannelUrl (raver.NameNoSpaces) + "?RTSID=" + requestTypeSentIdBlast + "\" >" + raver.Username + "</a>" +
                " raved the 3D home of " +
                " <a href=\"" + Common.GetPersonalChannelUrl (community.NameNoSpaces) + "?RTSID=" + requestTypeSentIdBlast + "\" >" + community.CreatorUsername + "</a>";

            // Send blast to raver
            SendFriendPassiveBlast (BlastTypes.RAVE_3D_HOME, raver.UserId, raver.Username, subject, "");

            // Send blast to raver
            SendFriendPassiveBlast (BlastTypes.RAVE_3D_HOME, community.CreatorId, community.CreatorUsername, subject, "");

            return 0;
        }

        /// <summary>
        /// Send a Rave 3D Hangout Blast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendRave3DHangoutBlast(User raver, Community community)
        {
            // Request Tracking
            UserFacade userFacade = new UserFacade();
            string requestId = userFacade.InsertTrackingRequest(24, raver.UserId, 0, 0);
            string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent(requestId, 2, 0);

            string subject = "<a href=\"" + Common.GetPersonalChannelUrl(raver.NameNoSpaces) + "?RTSID=" + requestTypeSentIdBlast + "\" >" + raver.Username + "</a>" +
                " raved the 3D hangout " +
                " <a href=\"" + Common.GetBroadcastChannelUrl(community.NameNoSpaces) + "?RTSID=" + requestTypeSentIdBlast + "\" >" + community.Name + "</a>";

            // Send blast to raver
            SendFriendPassiveBlast (BlastTypes.RAVE_3D_HANGOUT, raver.UserId, raver.Username, subject, "");

            // Send blast to ravee
            SendFriendPassiveBlast (BlastTypes.RAVE_3D_HANGOUT, community.CreatorId, community.CreatorUsername, subject, "");

            return 0;
        }

        /// <summary>
        /// Send a Rave 3D App Blast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendRave3DAppBlast(User raver, Community community)
        {
            // Request Tracking
            UserFacade userFacade = new UserFacade();
            string requestId = userFacade.InsertTrackingRequest(22, raver.UserId, 0, 0);
            string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent(requestId, 2, 0);

            string raverProfileURL = Common.GetPersonalChannelUrl(raver.NameNoSpaces) + "?RTSID=" + requestTypeSentIdBlast;
            string raverThumbnailURL = Common.GetProfileImageURL(raver.ThumbnailSquarePath, "sq", raver.Gender, raver.FacebookSettings.UseFacebookProfilePicture, raver.FacebookSettings.FacebookUserId, false) ;
            string worldProfileURL = Common.GetBroadcastChannelUrl(community.NameNoSpaces) + "?RTSID=" + requestTypeSentIdBlast;
            string worldThumbnailURL = Common.GetBroadcastChannelImageURL(community.ThumbnailSmallPath, "sm");

            string passiveSubject =
                "<a href=\"" + raverProfileURL + "\" >" + raver.Username + "</a> raved <a class=\"plainLink\" href=\"" + worldProfileURL + "\" >" + community.Name + "</a>";
            string activeSubject =
                "<a href=\"" + raverProfileURL + "\" >" + raver.Username + "</a> raved <a class=\"plainLink\" href=\"" + worldProfileURL + "\" >a world</a>";

            string diaryText =
                " <a href=\"" + worldProfileURL + "\" ><img src=\"" + worldThumbnailURL + "\"/></a>" +
                " <a class=\"worldLink\" href=\"" + worldProfileURL + "\" >" + community.Name + "</a>";

            // Send passive blast to raver for notification
            SendFriendPassiveBlast(BlastTypes.RAVE_3D_APP, raver.UserId, raver.Username, passiveSubject, "");

            // Send active blast to raver 
            SendActiveBlast(BlastTypes.RAVE_3D_APP, raver.UserId, raver.Username, community.CommunityId, community.Name, false, activeSubject, diaryText, "", raverThumbnailURL, 0);
            
            return 0;
        }

        /// <summary>
        /// Send a Rave UGC Blast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendRaveUGCBlast (User raver, WOKItem item, bool isMegaRave)
        {
            BlastTypes blastType = new BlastTypes ();
            string megaRaveDesc = "";

            if (isMegaRave)
            {
                blastType = BlastTypes.MEGARAVE;
                megaRaveDesc = " mega";
            }
            else
            {
                blastType = BlastTypes.RAVE;
            }

            string subject = "<a href=\"" + Common.GetPersonalChannelUrl (raver.NameNoSpaces) + "\" >" + raver.Username + "</a>" +
                megaRaveDesc + " raved the UGC item " +
                " <a href=\"" + Common.GetUGCItemDetailsURL (item.GlobalId) + "\" target=\"blank\" >" + item.DisplayName + "</a>";

            // Send blast to raver
            SendFriendPassiveBlast (blastType, raver.UserId, raver.Username, subject, "");
                                                
            // Send blast to owner of item
            if (!item.IsKanevaOwned)  // 0 is a Kaneva item
            {
                SendFriendPassiveBlast (blastType, (int) item.ItemCreatorId, new UserFacade ().GetUserName ((int) item.ItemCreatorId), subject, "");
            }

            return 0;
        }

        /// <summary>
        /// Send a Rave Asset Blast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendRaveAssetBlast (User raver, Asset asset, string assetTypeDesc, bool isMegaRave)
        {
            BlastTypes blastType = new BlastTypes ();
            string megaRaveDesc = "";

            if (isMegaRave)
            {
                blastType = BlastTypes.MEGARAVE;
                megaRaveDesc = " mega";
            }
            else
            {
                blastType = BlastTypes.RAVE;
            }

            string subject = "<a href=\"" + Common.GetPersonalChannelUrl (raver.NameNoSpaces) + "\" >" + raver.Username + "</a>" +
                megaRaveDesc + " raved the " + assetTypeDesc +
                " <a href=\"" + Common.GetAssetDetailsLink (asset.AssetId) + "\" >" + asset.Name + "</a>";

            // Send rave to raver
            SendFriendPassiveBlast (blastType, raver.UserId, raver.Username, subject, "");

            // Send rave to ravee
            SendFriendPassiveBlast (blastType, asset.OwnerId, asset.OwnerUsername, subject, "");

            return 0;
        }

        /// <summary>
        /// Send a Rave Profile Blast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendRaveProfileBlast (User raver, Community community, bool isMegaRave)
        {
            BlastTypes blastType = new BlastTypes ();
            string megaRaveDesc = "";

            if (isMegaRave)
            {
                blastType = BlastTypes.MEGARAVE;
                megaRaveDesc = " mega";
            }
            else
            {
                blastType = BlastTypes.RAVE;
            }

            string subject = "<a href=\"" + Common.GetPersonalChannelUrl (raver.NameNoSpaces) + "\" >" + raver.Username + "</a>" +
                megaRaveDesc + " raved member " +
                " <a href=\"" + Common.GetBroadcastChannelUrl (community.NameNoSpaces) + "\" >" + community.Name + "</a>";
            
            // Send blast to the ravee
            SendFriendPassiveBlast (blastType, raver.UserId, raver.Username, subject, "");

            // Send blast to the raver
            SendFriendPassiveBlast (blastType, community.CreatorId, community.CreatorUsername, subject, "");

            return 0;
        }

        /// <summary>
        /// Send a Rave Community Blast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int SendRaveCommunityBlast (User raver, Community community, bool isMegaRave)
        {
            BlastTypes blastType = new BlastTypes ();
            string megaRaveDesc = "";

            if (isMegaRave)
            {
                blastType = BlastTypes.MEGARAVE;
                megaRaveDesc = " mega";
            }
            else
            {
                blastType = BlastTypes.RAVE;
            }

            string subject = "<a href=\"" + Common.GetPersonalChannelUrl (raver.NameNoSpaces) + "\" >" + raver.Username + "</a>" +
                megaRaveDesc + " raved community " +
                " <a href=\"" + Common.GetBroadcastChannelUrl (community.NameNoSpaces) + "\" >" + community.Name + "</a>";

            // Send blast to raver
            SendFriendPassiveBlast (blastType, raver.UserId, raver.Username, subject, "");

            // Send blast to owner of community
            SendFriendPassiveBlast (blastType, community.CreatorId, community.CreatorUsername, subject, "");

            return 0;
        }

        /// <summary>
        /// Send a Gift Blast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SendGiftBlast(int userId, string username, string nameNoSpaces, string usernameRecipient, string nameNoSpacesRecipient)
        {
            string subject = "<a href=\"" + Common.GetPersonalChannelUrl(nameNoSpaces) + "\" >" + username + "</a>" +
                " sent a gift to " +
                " <a href=\"" + Common.GetPersonalChannelUrl(nameNoSpacesRecipient) + "\" >" + usernameRecipient + "</a>";

            return SendFriendPassiveBlast(BlastTypes.SEND_GIFT, userId, username, subject, "");
        }

        /// <summary>
        /// Send Friend Passive Blast
        /// </summary>
        public int SendFriendPassiveBlast (BlastTypes blastType, int userId, string username, string subject, string diaryText)
        {
            return blastDao.SendFriendPassiveBlast (blastType, userId, username, subject, diaryText);
        }

        /// <summary>
        /// Send Friend Passive Blast
        /// </summary>
        private int SendFriendPassiveBlast (UInt64 entryId, BlastTypes blastType, int userId, string username, string subject, string diaryText)
        {
            return blastDao.SendFriendPassiveBlast (entryId, blastType, userId, username, subject, diaryText);
        }

        /// <summary>
        /// Send Friend Passive Blast
        /// </summary>
        public int SendFameBlast (int userId, string username, string subject, string diaryText)
        {
            return blastDao.SendFriendPassiveBlast (BlastTypes.FAME, userId, username, subject, diaryText);
        }

        /// <summary>
        /// Send Friend Passive Blast
        /// </summary>
        private int SendFameBlast (UInt64 entryId, int userId, string username, string subject, string diaryText)
        {
            return blastDao.SendFriendPassiveBlast (entryId, BlastTypes.FAME, userId, username, subject, diaryText);
        }

        /// <summary>
        /// Send Fame Passive Blast
        /// </summary>
        public int SendFameLevelUpBlast (int userId, string username, string subject, string diaryText)
        {
            return blastDao.SendFriendPassiveBlast (BlastTypes.FAME_LEVELUP, userId, username, subject, diaryText);
        }

        #endregion

     

        /// <summary>
        /// SendActiveBlast
        /// </summary>
        private int SendActiveBlast(BlastTypes blastType, int userId, string username, int communityId, string communityName, bool isCommunityPersonal,
			string subject, string diaryText, string cssHightLight, string thumbnailPath, int recipientId)
        {
            int result = 0;
            if (communityId > 0)
            {
                // Increment number of blasts sent
                IncrementBlastsSent(userId, blastType);

                result = blastDao.SendChannelActiveBlast(blastType, userId, username, communityId, communityName, 
					isCommunityPersonal, subject, diaryText, cssHightLight, thumbnailPath, recipientId);
            }
            else if (blastType == BlastTypes.CAMERA_SCREENSHOT && communityId == 0)
            {
                // Increment number of blasts sent
                IncrementBlastsSent (userId, blastType);

                result = blastDao.SendChannelActiveBlast (blastType, userId, username, 0, communityName,
                    isCommunityPersonal, subject, diaryText, cssHightLight, thumbnailPath, recipientId);
            }
            else
            {
                result = SendFriendActiveBlast(blastType, userId, username, subject, diaryText, cssHightLight, thumbnailPath, recipientId);
            }
            if (result == 1)
            {
                FameFacade fameFacade = new FameFacade();
                fameFacade.RedeemPacket(userId, (int)PacketId.BLASTED_ON_SITE, (int)FameTypes.World);
            }
            return result;
        }

        /// <summary>
        /// Send a Blog Blast
        /// </summary>
        public int SendFriendActiveBlast(BlastTypes blastType, int userId, string username, string subject, string diaryText, string cssHightLight, string thumbnailPath, int recipientId)
        {
            // Increment number of blasts sent
            IncrementBlastsSent(userId, blastType);

            return blastDao.SendFriendActiveBlast(blastType, userId, username, subject, diaryText, cssHightLight, thumbnailPath, recipientId);
        }

        /// <summary>
        /// Send a Friend Blast
        /// </summary>
        private int SendFriendActiveBlast(UInt64 entryId, BlastTypes blastType, int userId, string username, string subject, string diaryText, string cssHightLight, string thumbnailPath, int recipientId)
        {
            // Increment number of blasts sent
            IncrementBlastsSent(userId, blastType);

            return blastDao.SendFriendActiveBlast(entryId, blastType, userId, username, subject, diaryText, cssHightLight, thumbnailPath, recipientId);
        }

        /// <summary>
        /// SendEventBlast
        /// </summary>
        private int SendEventBlast (UInt64 entryId, BlastTypes blastType, int userId, string userName, int communityId, string subject, string diaryText, string cssHightLight, string thumbnailPath)
        {
            if (communityId > 0)
            {
                // Increment number of blasts sent
                IncrementBlastsSent (userId, blastType);

                return blastDao.SendChannelActiveBlast (entryId, blastType, userId, userName, communityId, "", false, subject, diaryText, cssHightLight, thumbnailPath, 0);
            }
            else
            {
                return SendFriendActiveBlast (entryId, blastType, userId, userName, subject, diaryText, cssHightLight, thumbnailPath, 0);
            }
        }

        /// <summary>
        /// Send a Channel Blast
        /// </summary>
        private int SendChannelBlast(UInt64 entryId, BlastTypes blastType, int userId, string userName, int communityId, string subject, string diaryText, string cssHightLight, string thumbnailPath)
        {
            CommunityFacade communityFacade = new CommunityFacade();

            // Must be admin, owner, or moderator to do this one
            if (!communityFacade.IsCommunityModerator(communityId, userId, true))
            {
                return -99;
            }

            // Increment number of blasts sent
            IncrementBlastsSent(userId, blastType);

            return blastDao.SendChannelActiveBlast (entryId, blastType, userId, userName, communityId, "", false, subject, diaryText, cssHightLight, thumbnailPath, 0);
        }

        /// <summary>
        /// GetBlast
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Blast GetBlast(UInt64 entryId)
        {
            return blastDao.GetBlast(entryId);
        }
        /// <summary>
        /// GetBlasts
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<Blast> GetAdminBlasts()
        {
            return blastDao.GetAdminBlasts();
        }

        /// <summary>
        /// GetBlasts
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Blast GetAdminBlast(UInt64 entryId)
        {
            return blastDao.GetAdminBlast(entryId);
        }

        /// <summary>
        /// GetPassiveBlasts
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Blast> GetPassiveBlasts(int userId, int pageSize, string userVisibilityFilter, int pageNumber)
        {
            return blastDao.GetPassiveBlasts(userId, pageSize, userVisibilityFilter, pageNumber);
        }

        /// <summary>
        /// GetMyPassiveBlasts
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public List<Blast> GetMyPassiveBlasts (int userId, int pageSize, string userVisibilityFilter, int pageNumber)
        {
            return blastDao.GetMyPassiveBlasts (userId, pageSize, userVisibilityFilter, pageNumber);
        }

        /// <summary>
        /// GetActivityBlasts
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Blast> GetActiveBlasts(int userId, int currentUserId, int pageSize, int pageNumber)
        {
            return blastDao.GetActiveBlasts(userId, currentUserId, pageSize, pageNumber);
        }

        /// <summary>
        /// GetFriendActiveBlasts
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<Blast> GetFriendActiveBlasts (int userId, int pageSize, int pageNumber)
        {
            return blastDao.GetFriendActiveBlasts (userId, pageSize, pageNumber);
        }

        /// <summary>
        /// GetMyActiveBlasts
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Blast> GetMyActiveBlasts (int userId, int pageSize, int pageNumber)
        {
            return blastDao.GetMyActiveBlasts (userId, pageSize, pageNumber);
        }

         /// <summary>
        /// GetCommunityActiveBlasts
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Blast> GetCommunityActiveBlasts (int communityId, int senderId, bool isPersonalCommunity, int pageSize, int pageNumber)
        {
            return blastDao.GetCommunityActiveBlasts (communityId, senderId, isPersonalCommunity, pageSize, pageNumber);
        }

        /// <summary>
        /// GetEvents
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<Blast> GetUpcomingEvents(int userId, int pageSize)
        {
            return blastDao.GetUpcomingEvents(userId, pageSize);
        }

        /// <summary>
        /// GetAdminEvents
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public IList<Blast> GetAdminEvents ()
        {
            return blastDao.GetAdminEvents ();
        }

        /// <summary>
        /// InsertBlastReply
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int InsertBlastReply(UInt64 entryId, int userId, string message, string ipAddress)
        {
           return blastDao.InsertBlastReply (entryId, userId, message, ipAddress);
        }


        /// <summary>
        /// GetBlast Replies
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<BlastReply> GetBlastReplies(UInt64 entryId, int pageNumber, int pageSize, string sortBy)
        {
			return blastDao.GetBlastReplies(entryId, pageNumber, pageSize, sortBy);
        }

        /// <summary>
        /// DeleteAdminBlast
        /// </summary>
        /// <param name="id">the id of the blast</param>
        /// <returns></returns>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public void DeleteAdminBlast(UInt64 blastId)
        {
            blastDao.DeleteAdminBlast(blastId);
        }

        /// <summary>
        /// DeleteBlast
        /// </summary>
        /// <param name="id">the id of the blast</param>
        /// <returns></returns>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public void DeleteBlast(UInt64 blastId)
        {
            blastDao.DeleteBlast(blastId);
        }

        /// <summary>
        /// IncrementBlastsSent
        /// </summary>
        private void IncrementBlastsSent(int userId, BlastTypes blastType)
        {
            if (blastType.Equals (BlastTypes.BASIC) || blastType.Equals (BlastTypes.EVENT) || blastType.Equals (BlastTypes.ADMIN) ||
                blastType.Equals (BlastTypes.VIDEO) || blastType.Equals (BlastTypes.PHOTO) || blastType.Equals (BlastTypes.LINK) ||
                blastType.Equals (BlastTypes.PLACE))
            {
                blastDao.IncrementBlastsSent (userId);
            }
        }

        /// <summary>
        /// StripHTML
        /// </summary>
        public string StripHTML(string htmlString, string strDefault)
        {
            //This pattern Matches everything found inside html tags;
            //(.|\n) - > Look for any character or a new line
            // *?  -> 0 or more occurences, and make a non-greedy search meaning
            //That the match will stop at the first available '>' it sees, and not at the last one
            //(if it stopped at the last one we could have overlooked 
            //nested HTML tags inside a bigger HTML tag..)

            string pattern = @"<(.|\n)*?>";
            string strStripped = Regex.Replace(htmlString, pattern, string.Empty);

            if (strStripped.Length.Equals(0))
            {
                return StripFormatUnfriendlyCharacters(strDefault);
            }
            else
            {
                return StripFormatUnfriendlyCharacters(strStripped);
            }
        }

        /// <summary>
        /// StripHTML
        /// </summary>
        private string StripFormatUnfriendlyCharacters(string htmlString)
        {
            //this removes any characters we determine to case issues for 
            //the string format function

            htmlString = htmlString.Replace("{", "[");
            htmlString = htmlString.Replace("}", "]");

            return htmlString;
        }

        public string GetBlastImg(int blastType)
        {
            string imgName = "";

            switch (blastType)
            {
                case (int)BlastTypes.BLOG:
                    imgName = "blog";
                    break;

                case (int)BlastTypes.FORUM_POST:
                    imgName = "forum";
                    break;

                case (int)BlastTypes.JOIN_COMMUNITY:
                    imgName = "community";
                    break;

                case (int)BlastTypes.MEDIA_COMMENT:
                    imgName = "comment";
                    break;

                case (int)BlastTypes.PROFILE_UPDATE:
                    imgName = "profile";
                    break;

                case (int)BlastTypes.RAVE_3D_HOME:
                    imgName = "rave";
                    break;

                case (int)BlastTypes.RAVE_3D_HANGOUT:
                    imgName = "rave";
                    break;

                case (int)BlastTypes.RAVE_3D_APP:
                    imgName = "apps3d";
                    break;

                case (int)BlastTypes.CAMERA_SCREENSHOT:
                    imgName = "apps3d";
                    break;

                case (int)BlastTypes.SEND_GIFT:
                    imgName = "gift";
                    break;

                case (int)BlastTypes.FAME:
                    imgName = "fame";
                    break;

                case (int)BlastTypes.RAVE:
                    imgName = "rave";
                    break;

                case (int)BlastTypes.MEGARAVE:
                    imgName = "megarave";
                    break;
            }

            if (imgName != "")
                return "<img src=\"images/activity_" + imgName + ".gif\" alt=\"" + imgName + "\" width=\"16\" height=\"16\" border=\"0\">";
            else
                return "";
        }

        public string GetBlastText(int blastType)
        {
            switch (blastType)
            {
                case (int)BlastTypes.BLOG:
                    return "wrote";

                case (int)BlastTypes.FORUM_POST:
                    return "posted";

                case (int)BlastTypes.JOIN_COMMUNITY:
                    return "joined";

                case (int)BlastTypes.MEDIA_COMMENT:
                    return "commented";

                case (int)BlastTypes.PROFILE_UPDATE:
                    return "posted";

                case (int)BlastTypes.CAMERA_SCREENSHOT:
                    return "shared";

                case (int)BlastTypes.RAVE_3D_HOME:
                    return "raved";

                case (int)BlastTypes.RAVE_3D_HANGOUT:
                    return "raved";

                case (int)BlastTypes.RAVE_3D_APP:
                    return "raved";

                case (int)BlastTypes.SEND_GIFT:
                    return "gifted";

                case (int)BlastTypes.FAME:
                    return "earned fame";

                case (int)BlastTypes.RAVE:
                    return "raved";

                case (int)BlastTypes.MEGARAVE:
                    return "mega raved";

                default:
                    return "";
            }
        }

        public string GetBlastType(int blastType)
        {
            switch (blastType)
            {
                case (int)BlastTypes.ADD_FRIEND:
                    return "friends";

                case (int)BlastTypes.BLOG:
                    return "blogs";

                case (int)BlastTypes.FORUM_POST:
                    return "posts";

                case (int)BlastTypes.JOIN_COMMUNITY:
                    return "joins";

                case (int)BlastTypes.MEDIA_COMMENT:
                    return "comments";

                case (int)BlastTypes.PROFILE_UPDATE:
                    return "posts";

                case (int)BlastTypes.SEND_GIFT:
                    return "gifts";

                case (int)BlastTypes.FAME:
                    return "fame awards";

                default:
                    return "";
            }
        }

        public int SendReblast (Blast blast, User currentUser)
        {
            //** Does this count toward # of blasts sent????
            // Increment number of blasts sent
        //    IncrementBlastsSent (userId, blastType);

            return blastDao.SendReblast (blast.BlastType, currentUser.UserId, currentUser.Username,
                blast.Subj, blast.DiaryEntry, blast.HighlightCss, currentUser.ThumbnailSquarePath, 
                blast.OriginatorId > 0 ? blast.OriginatorId : blast.SenderId, 
                blast.OriginatorId > 0 ? blast.OriginatorName : blast.SenderName);
        }
 
    }
}

