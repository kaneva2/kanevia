///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;
using RabbitMQ.Client;
using log4net;
using Newtonsoft.Json;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class MetricsFacade
    {
        private IMetricsDao metricsDao = DataAccess.MetricsDao;
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Insert Disconnect Error Logg
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertMetricsCounterData_v0(    string counterName
                                                , double? a1
                                                , double? a2
                                                , double? b1
                                                , double? b2 )
        {
            IMetricsDao dao = metricsDao; 
            return dao.InsertMetricsCounterData_v0(counterName, a1, a2, b1, b2);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertMetricsCounterData_v1( string      counterName
                                                , string    k1
                                                , string    k2
                                                , string    k3
                                                , string    k4
                                                , double?   a1
                                                , double?   a2
                                                , double?   b1
                                                , double?   b2            )
        {
            IMetricsDao dao = metricsDao;
            return dao.InsertMetricsCounterData_v1 (counterName, k1, k2, k3, k4, a1, a2, b1, b2 );
        }

        public bool CanLogPageMetrics (string pageName)
        {
            string cacheKey = CentralCache.keyPageMetrics + pageName;

            // Have we logged in last X seconds?
            String loggedMetric = (String)CentralCache.Get(cacheKey);

            if (loggedMetric == null)
            {
                // Store it in cache
                CentralCache.Store(cacheKey, pageName, TimeSpan.FromSeconds(Configuration.MetricsPageThrottleInSeconds));

                return true;
            }
            else
            {
                return false;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertMetricsPageTiming (string name, double timeInMS)
        {
            IMetricsDao dao = metricsDao;
            return dao.InsertMetricsPageTiming(name, timeInMS);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertClientMtbfLog(int userId, double totalRuntime, uint totalRuns, uint totalCrashes, string appVersion, string runtimeId)
        {
            IMetricsDao dao = metricsDao;
            return dao.InsertClientMtbfLog(userId, totalRuntime, totalRuns, totalCrashes, appVersion, runtimeId);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public string GetClientRuntimeSystemId(string runtimeId)
        {
            IMetricsDao dao = metricsDao;
            return dao.GetClientRuntimeSystemId(runtimeId);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public string InsertClientRuntimePerformanceLog(string runtimeId, int userId, string systemConfigurationId, string appVersion, uint? testGroup, ulong? diskAvail, double? pingMs, uint? ripperDetected)
        {
            IMetricsDao dao = metricsDao;
            return dao.InsertClientRuntimePerformanceLog(runtimeId, userId, systemConfigurationId, appVersion, testGroup, diskAvail, pingMs, ripperDetected);
        }

        public int InsertClientRuntimeIP(string runtimeId, string ipAddress)
        {
            IMetricsDao dao = metricsDao;
            return dao.InsertClientRuntimeIP(runtimeId, ipAddress);
        }

        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateClientRuntimePerformanceLog(string runtimeId, int userId, double? appCpuTime, ulong? appMemNum, ulong? appMemAvg, ulong? appMemMin, ulong? appMemMax,
            ulong? texMemNum, ulong? texMemAvg, ulong? texMemMin, ulong? texMemMax, double? fps, string shutDownType, uint? ripperDetected, double? msgMoveEnabledTimeSec)
        {
            IMetricsDao dao = metricsDao;
            return dao.UpdateClientRuntimePerformanceLog(runtimeId, userId, appCpuTime, appMemNum, appMemAvg, appMemMin, appMemMax,
                texMemNum, texMemAvg, texMemMin, texMemMax, fps, shutDownType, ripperDetected, msgMoveEnabledTimeSec);
        }

        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateClientRuntimePerformanceLogUserId(string runtimeId, int userId)
        {
            IMetricsDao dao = metricsDao;
            return dao.UpdateClientRuntimePerformanceLogUserId(runtimeId, userId);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertClientWebCallPerformanceLog(string runtimeId, string webCallType, uint webCalls, double webCallsMs, double webCallsResponseMs, uint webCallsBytes, uint webCallsErrored)
        {
            IMetricsDao dao = metricsDao;
            return dao.InsertClientWebCallPerformanceLog(runtimeId, webCallType, webCalls, webCallsMs, webCallsResponseMs, webCallsBytes, webCallsErrored);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertClientZonePerformanceLog(string runtimeId, uint gameId, uint zoneIndex, uint zoneInstanceId, uint zoneType, double? pingMs, double? fps,
            double? zoneTimeMs, double? avgDo, double? minDo, double? maxDo, double? avgPoly, double? minPoly, double? maxPoly, uint mediaCount, uint texMem)
        {
            IMetricsDao dao = metricsDao;
            return dao.InsertClientZonePerformanceLog(runtimeId, gameId, zoneIndex, zoneInstanceId, zoneType, pingMs, fps,
                zoneTimeMs, avgDo, minDo, maxDo, avgPoly, minPoly, maxPoly, mediaCount, texMem);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertLauncherMtbfLog(double totalRuntime, ushort totalRuns, ushort totalCrashes, ulong runtimeId)
        {
            IMetricsDao dao = metricsDao;
            return dao.InsertLauncherMtbfLog(totalRuntime, totalRuns, totalCrashes, runtimeId);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public ulong SaveLauncherRuntimePerformanceLog(ulong runtimeId, string runtimeState, string systemConfigurationId, string appVersion, ulong? diskAvail, double? appCpuTime, uint? appMemNum,
            ulong? appMemAvg, ulong? appMemMin, ulong? appMemMax, ushort? patchRv)
        {
            IMetricsDao dao = metricsDao;
            return dao.SaveLauncherRuntimePerformanceLog(runtimeId, runtimeState, systemConfigurationId, appVersion, diskAvail, appCpuTime, appMemNum,
                appMemAvg, appMemMin, appMemMax, patchRv);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertLauncherRuntimeIP(ulong runtimeId, string ipAddress)
        {
            IMetricsDao dao = metricsDao;
            return dao.InsertLauncherRuntimeIP(runtimeId, ipAddress);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertLauncherPatchPerformanceLog(ulong runtimeId, string patchUrl, double? patchMs, ushort dl_NumOk, ushort dl_NumErr, double dl_Ms, uint dl_Bytes,
          ushort dl_Waits, ushort dl_Retries, ushort dl_RetriesErr,
          ushort zip_NumOk, ushort zip_NumErr, double zip_Ms, uint zip_Bytes,
          ushort pak_NumOk, ushort pak_NumErr, double pak_Ms, uint pak_Bytes)
        {
            IMetricsDao dao = metricsDao;
            return dao.InsertLauncherPatchPerformanceLog(runtimeId, patchUrl, patchMs, dl_NumOk, dl_NumErr, dl_Ms, dl_Bytes,
                dl_Waits, dl_Retries, dl_RetriesErr,
                zip_NumOk, zip_NumErr, zip_Ms, zip_Bytes,
                pak_NumOk, pak_NumErr, pak_Ms, pak_Bytes);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertLauncherWebCallPerformanceLog(ulong runtimeId, string webCallType, uint webCalls, double webCallsMs, double webCallsResponseMs, uint webCallsBytes, uint webCallsErrored)
        {
            IMetricsDao dao = metricsDao;
            return dao.InsertLauncherWebCallPerformanceLog(runtimeId, webCallType, webCalls, webCallsMs, webCallsResponseMs, webCallsBytes, webCallsErrored);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertClientVersionHistory(string clientVersion, DateTime releaseDate, DateTime deprecatedDate, string comment)
        {
            IMetricsDao dao = metricsDao;
            return dao.InsertClientVersionHistory(clientVersion, releaseDate, deprecatedDate, comment);
        }

        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateClientVersionHistory(string clientVersion, DateTime releaseDate, DateTime deprecatedDate, string comment)
        {
            IMetricsDao dao = metricsDao;
            return dao.UpdateClientVersionHistory(clientVersion, releaseDate, deprecatedDate, comment);
        }

        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteClientVersionHistory(string clientVersion)
        {
            IMetricsDao dao = metricsDao;
            return dao.DeleteClientVersionHistory(clientVersion);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedDataTable GetClientVersionHistory(int page, int itemsPerPage, string clientVersion = "")
        {
            IMetricsDao dao = metricsDao;
            return dao.GetClientVersionHistory(page, itemsPerPage, clientVersion);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertScriptGameItemPlacementLog(int zoneInstanceId, int zoneType, string username, string gameplayMode, int gameItemId, int gameItemGlid, string itemType, string behavior)
        {
            IMetricsDao dao = metricsDao;
            return dao.InsertScriptGameItemPlacementLog(zoneInstanceId, zoneType, username, gameplayMode, gameItemId, gameItemGlid, itemType, behavior);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertScriptGameItemLootingLog(int zoneInstanceId, int zoneType, string username, string lootSource, int gameItemId, int gameItemGlid, string itemType, string behavior)
        {
            IMetricsDao dao = metricsDao;
            return dao.InsertScriptGameItemLootingLog(zoneInstanceId, zoneType, username, lootSource, gameItemId, gameItemGlid, itemType, behavior);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertFrameworkActivationLog(int zoneInstanceId, int zoneType, string username, string activationAction)
        {
            IMetricsDao dao = metricsDao;
            return dao.InsertFrameworkActivationLog(zoneInstanceId, zoneType, username, activationAction);
        }

        [DataObjectMethod(DataObjectMethodType.Update)]
        public int RecordNewUserFunnelProgression(int userId, string funnelStep)
        {
            int ret = 0;
            User u = (new UserFacade()).GetUser(userId);

            if (u.SignupDate >= Configuration.NewUserFunnelStart)
            {
                IMetricsDao dao = metricsDao;
                ret = dao.RecordNewUserFunnelProgression(userId, funnelStep);
            }
            
            return ret;
        }

        [System.Security.SecuritySafeCritical]
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public void InsertGenericMetricsLog(string metricName, IDictionary<string, string> metricData, IModel channel)
        {
            string strOut;

            // Validate params.
            if (string.IsNullOrWhiteSpace(metricName))
                throw new ArgumentException("metricName must be a non-empty string.");
            if (metricData == null)
                throw new ArgumentNullException("metricData may not be null");
            if (metricData.Count < 1)
                throw new ArgumentException("metricData must contain at least one key-value pair.");
            if (metricData.TryGetValue("collection", out strOut))
                throw new ArgumentException("'collection' is a reserved key and cannot be used in metricData.");

            // Build the message in JSON.
            metricData.Add("collection", metricName.Replace(",", ""));
            string message = JsonConvert.SerializeObject(metricData);

            // Send message to RabbitMQ.
            try
            {
                channel.QueueDeclare("metrics", true, false, false, null);
                channel.BasicPublish("", "metrics", null, System.Text.Encoding.UTF8.GetBytes(message));
            }
            catch(Exception ex)
            {
                m_logger.Error("Unable to write metric to RabbitMQ.", ex);
            }
        }
    }
}
