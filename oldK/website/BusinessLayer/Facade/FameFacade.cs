///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;
using System.Data;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class FameFacade
    {
        private IFameDao fameDao = DataAccess.FameDao;
        IGameDao gameDao = DataAccess.GameDao;
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
               
        /// <summary>
        /// Gets user's fame
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public UserFame GetUserFame(int userId, int fameTypeId)
        {
            // Special Case for Dance fame until it gets moved to Fame Engine
            if (fameTypeId.Equals (2))
            {
                // Look up player ID
                IUserDao userDao = DataAccess.UserDao;
                User user = userDao.GetUser (userId);

                // Look up wok.player_skills
                UserFame danceFame = fameDao.GetDanceFame (userId, fameTypeId, user.Username, user.WokPlayerId);

                // Look up Level Information
                danceFame.CurrentLevel = fameDao.GetDanceLevel (danceFame.LevelId, fameTypeId);
                

                return danceFame;
            }

            // TODO: add access security here..
            // TODO: add argument validation here..
            UserFame userFame = fameDao.GetUserFame(userId, fameTypeId);
            userFame.CurrentLevel = fameDao.GetLevel(userFame.LevelId);
            

            return userFame;
        }
        
        /// <summary>
        /// Gets details of a fame system
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public FameType GetFameType(int fameTypeId)
        {
            FameType fameType = fameDao.GetFameType(fameTypeId);            
            return fameType;
        }

        /// <summary>
        /// Gets a list of users
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<UserFame> GetUserFame(int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return fameDao.GetUserFame(fameTypeId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Gets a Packet
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Packet GetPacket(int packetId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return fameDao.GetPacket(packetId);
        }

        /// <summary>
        /// Gets a list of packets
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Packet> GetPackets(int fameTypeId, string filter, string orderby, string groupby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return fameDao.GetPackets(fameTypeId, filter, orderby, groupby, pageNumber, pageSize);
        }

        /// <summary>
        /// Gets a Level
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Level GetLevel(int levelId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return fameDao.GetLevel(levelId);
        }

        /// <summary>
        /// Forces a player's fame up to a specific level.
        /// Intended to be used in automated testing.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int SetLevel(int userId, int fameTypeId, int level)
        {
            return fameDao.SetLevel(userId, fameTypeId, level);
        }

        /// <summary>
        /// Gets a Level Awards
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<LevelAwards> GetLevelAwards(int levelId, string gender)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return fameDao.GetLevelAwards(levelId, gender);
        }

        /// <summary>
        /// Gets all Level Awards
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<LevelAwards> GetAllLevelAwards(int fameTypeId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return fameDao.GetAllLevelAwards(fameTypeId);
        }
        

        /// <summary>
        /// Gets a list of levels
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Level> GetLevels(int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return fameDao.GetLevels(fameTypeId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Gets a list of users packet history
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<PacketHistory> GetPacketHistory(int userId, int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return fameDao.GetPacketHistory (userId, fameTypeId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Gets a list of users
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<LevelHistory> GetLevelHistory(int userId, int fameTypeId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return fameDao.GetLevelHistory(userId, fameTypeId, filter, orderby, pageNumber, pageSize);
        }


        // Place holer for now.  This will need to be completed when we add Mega Rave ability to assets
        public int RedeemPacketAssetRelatedAfterMegaRave (int assetId, int packetId, int fameTypeId, int numRavesToBeAwarded)
        {
            try
            {

            }
            catch { }
            return 0;
        }

        //
        public int RedeemPacketAfterMegaRave (int communityId, int packetId, int fameTypeId, int numRavesToBeAwarded)
        {
            try
            {

            }
            catch { }
            return 0;
        }




        /// <summary>
        /// Captures a Fame Packet
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int RedeemPacketAssetRelated (int assetId, int packetId, int fameTypeId)
        {
            try
            {
                MediaFacade mediaFacade = new MediaFacade();
                Asset asset = mediaFacade.GetAsset(assetId);

                // Make sure it is multiple of 10
                if (packetId == (int)PacketId.WEB_RAVED_PATTERN)
                {
                    if (asset.Stats.NumberOfDiggs == 0 || asset.Stats.NumberOfDiggs % 10 != 0)
                    {
                        // Not a multiple they get rewarded for
                        return -4;
                    }
                }
                else if (packetId == (int)PacketId.WEB_RAVED_PHOTO)
                {
                    if (asset.Stats.NumberOfDiggs == 0 || asset.Stats.NumberOfDiggs % 10 != 0)
                    {
                        // Not a multiple they get rewarded for
                        return -4;
                    }
                }
                else if (packetId == (int)PacketId.WEB_RAVED_VIDEO)
                {
                    if (asset.Stats.NumberOfDiggs == 0 || asset.Stats.NumberOfDiggs % 10 != 0)
                    {
                        // Not a multiple they get rewarded for
                        return -4;
                    }
                }
                else
                {
                    throw new Exception("Unhandled Asset Packet");
                }

                return RedeemPacket(asset.OwnerId, packetId, fameTypeId, false);
            }
            catch (Exception)
            {
                return -7;
            }
        }

        /// <summary>
        /// Captures a Fame Packet
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int RedeemPacketCommunityRelated (int communityId, int packetId, int fameTypeId)
        {
             try
            {
                CommunityFacade communityFacade = new CommunityFacade ();
                Community community = communityFacade.GetCommunity (communityId);

                // Make sure it is number 10 join
                if (packetId == (int)PacketId.WEB_COMM_JOIN)
                {
                    if (community.Stats.NumberOfMembers == 0 || community.Stats.NumberOfMembers % 5 != 0)
                    {
                        // Not a multiple they get rewarded for
                        return -4;
                    }
                }
                else if (packetId == (int)PacketId.WEB_RAVED_COMMUNITY)
                {
                    if (community.Stats.NumberOfDiggs == 0 || community.Stats.NumberOfDiggs % 10 != 0)
                    {
                        // Not a multiple they get rewarded for
                        return -4;
                    }
                }
                else
                {
                    throw new Exception("Unhandled Community Packet");
                }

                return RedeemPacket(community.CreatorId, packetId, fameTypeId, false);
            }
            catch (Exception)
            {
                return -7;
            }
        }

        /// <summary>
        /// Redeems and login related packets
        /// </summary>
        public int RedeemPacketLoginRelated (int userId, int fameTypeId, out int consecutiveDaysLogin)
        {
            m_logger.Debug ("START RedeemPacketLoginRelated() for UserId = " + userId);

            //Check daily login history to see if they earn Login 2 Days in  a row
            int retCode = 0;
            consecutiveDaysLogin = 0;

            string filter = " date_rewarded <= DATE(NOW()) AND date_rewarded > ADDDATE(DATE(NOW()), INTERVAL -3 DAY)";
            PagedList<DailyHistory> dhList = this.fameDao.GetDailyHistoryForUser (userId, (int) PacketId.WORLD_DAILY_SIGNIN, fameTypeId, filter, "date_rewarded DESC", 1, 3);

            // Get consecutive days user has logged in up to 3
            for (int j = 0; j < dhList.TotalCount; j++)
            {
                if (dhList[j].DateRewarded == DateTime.Now.Date.AddDays (j * -1))
                {
                    consecutiveDaysLogin++;
                }
            }

            switch (consecutiveDaysLogin)
            {
                case 2: //Logged in 2 days in a row
                    {
                        retCode = RedeemPacket (userId, (int) PacketId.LOGIN_WOK_2_DAYS_IN_A_ROW, fameTypeId, true);
                        m_logger.Debug ("RedeemPacketLoginRelated() -- Redeeming PacketId.LOGIN_WOK_2_DAYS_IN_A_ROW for UserId = " + userId);
                        break;
                    }
                case 3: //Logged in 3 days in a row
                    {
                        retCode = RedeemPacket (userId, (int) PacketId.LOGIN_WOK_3_DAYS_IN_A_ROW, fameTypeId, true);
                        m_logger.Debug ("RedeemPacketLoginRelated() -- Redeeming PacketId.LOGIN_WOK_3_DAYS_IN_A_ROW for UserId = " + userId);
                        break;
                    }
                default:
                    {
                        retCode = RedeemPacket (userId, (int) PacketId.LOGIN_WOK_1_DAY_IN_A_ROW, fameTypeId, true);
                        consecutiveDaysLogin = 1;
                        m_logger.Debug ("RedeemPacketLoginRelated() -- Redeeming PacketId.LOGIN_WOK_1_DAY_IN_A_ROW for UserId = " + userId);
                        break;
                    }
            }

            m_logger.Debug ("END RedeemPacketLoginRelated() for UserId = " + userId);
            return retCode;
        }
        
        public int CheckForABExperiment(int packetId, int userId)
        {
            // A/B Test for Fame 2.0
            string fameExperimentId = "064da00f-9ac1-11e6-84f2-a3cf223dc93b";
            //string pmGroupA = "1096cd27-9ac1-11e6-84f2-a3cf223dc93b";
            string fameGroupB = "1b7fcde2-9ac1-11e6-84f2-a3cf223dc93b";
            string fameGroupC = "22b0a78f-9ac1-11e6-84f2-a3cf223dc93b";

            ExperimentFacade experimentFacade = new ExperimentFacade();
            UserExperimentGroup userExperimentGroup = experimentFacade.GetUserExperimentGroupCommon(userId, fameExperimentId);
            if (userExperimentGroup != null && (userExperimentGroup.GroupId == fameGroupB || userExperimentGroup.GroupId == fameGroupC))
            {
                // Both Group B gets these new packets
                switch (packetId)
                {
                    case (int)PacketId.KILLED_1_MONSTER:            //592
                    case (int)PacketId.CRAFTED_1_OBJECT:            //596
                    case (int)PacketId.PLACE_1_OBJECT:              //600
                    case (int)PacketId.HARVESTED_1_OBJECT:          //612
                        return packetId;
                }

                // Group C also gets these packets in addition to the above 4
                if (userExperimentGroup.GroupId == fameGroupC)
                {
                    switch(packetId)
                    {
                        case (int)PacketId.BUY_AN_ITEM:             //484
                            return 704;

                        case (int)PacketId.BLASTED_ON_SITE:         //440
                            return 705;

                        case (int)PacketId.UPLOAD_MEDIA_EVERY_10:   //478
                            return 706;

                        case (int)PacketId.VIEW_SOMEONE_PROFILE:    //425
                            return 707;
                    }
                }
            }
            return packetId;
        }

        /// <summary>
        /// Captures a Fame Packet
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int RedeemPacket (int userId, int packetId, int fameTypeId)
        {
            return RedeemPacket (userId, packetId, fameTypeId, true);
        }

        private int RedeemPacket (int userId, int packetId, int fameTypeId, bool UseMultiples)
        {
            m_logger.Debug ("START RedeemPacket() for PacketId = " + packetId + ", UserId = " + userId);

            if (!IsAllowedToUseFame(userId))
            {
                return -6;
            }


            // Return values
            //  0 - Success
            // -2 - Already redeemed
            // -3 - No packet found
            // -4 - No Action taken, did not meet packet requirements
            // -5 - Packet Id not redeemable
            // -6 - User not able to use fame
            // -7 - Error
            // -8 - User is at max redemption
            // -9 - Packet is not active

            // Get the users current level
            UserFame userFameCurrent = GetUserFame(userId, fameTypeId);

            Packet packet = GetPacket(packetId);

            // Get this one time, used several times below
            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);

            if (packet.Status == PacketStatus.InActive)
            {
                return -9;
            }

            // Logic for one time packets, make sure they have not redeemed already
            if (packet.TimeInterval == (int)PacketTimeInterval.OneTime)
            {
                if (IsOneTimePacketRedeemed(userId, packetId))
                {
                    return -2;
                }
            }

            // Logic for max per day  (Does not apply to one time packets)
            if (packet.MaxPerDay > 0 && packet.TimeInterval != (int)PacketTimeInterval.OneTime)
            {
                // See if this user has redeemed max
                if (IsUserAtMaxRedemptions(userId, packetId, fameTypeId, packet.MaxPerDay))
                {
                    return -8;
                }
            }

            if (UseMultiples && packet.RewardInterval > 0)
            {
                // New logic for reward intervals
                if (packet.TimeInterval == (int)PacketTimeInterval.OneTime)
                {
                    // OPTIMIZE - Reward interval for one time only applies if more that 1
                    if (packet.RewardInterval > 1)
                    {
                        int currentRewardInterval = fameDao.GetRewardInterval(userId, packetId);

                        // update interval
                        fameDao.UpdateRewardInterval(userId, packetId);
                        currentRewardInterval = currentRewardInterval + 1;

                        if (currentRewardInterval % packet.RewardInterval == 0)
                        {
                            // Ok to let them pass to redeem
                        }
                        else
                        {
                            //Send Packet Update Information to client
                            if (user.WokPlayerId > 0)
                            {
                                sendPacketRewardIntervalUpdateToPlayer(user, packet);
                            }
                            // Not a multiple they get rewarded for
                            return -4;
                        }
                    }

                    // Free to pass

                }
                if (packet.TimeInterval == (int)PacketTimeInterval.Unlimited)
                {
                    int currentRewardInterval = fameDao.GetRewardInterval(userId, packetId);

                    // update interval
                    fameDao.UpdateRewardInterval(userId, packetId);
                    currentRewardInterval = currentRewardInterval + 1;

                    if (currentRewardInterval % packet.RewardInterval == 0)
                    {
                        // Ok to let them pass to redeem
                    }
                    else
                    {
                        //Send Packet Update Information to client
                        if (user.WokPlayerId > 0)
                        {
                            sendPacketRewardIntervalUpdateToPlayer(user, packet);
                        }
                        // Not a multiple they get rewarded for
                        return -4;
                    }

                }
                else if (packet.TimeInterval == (int)PacketTimeInterval.Daily)
                {
                    int currentDailyRewardInterval = fameDao.GetRewardIntervalDaily(userId, packetId);

                    // update interval
                    currentDailyRewardInterval = currentDailyRewardInterval + 1;

                    if (currentDailyRewardInterval % packet.RewardInterval == 0)
                    {
                        // Ok to let them pass to redeem
                    }
                    else
                    {
                        // Not a multiple they get rewarded for
                        fameDao.UpdateRewardIntervalDaily(userId, packetId);
                        return -4;
                    }
                }

            }
            

            // NOTE: Pass in WEB_INVITE_JOIN_SITE for 
            // PacketId.WEB_OT_FRIENDS_INVITE_10, PacketId.WEB_OT_FRIENDS_INVITE_25, PacketId.WEB_OT_FRIENDS_INVITE_50

            // ***********************************************************
            // Logic here for special packets
            // ***********************************************************
            switch (packetId)
            {
                case ((int) PacketId.WEB_OT_FRIENDS_INVITE_10):
                {
                    // Handled by WEB_INVITE_JOIN_SITE
                    return -5;
                }
                case ((int) PacketId.WEB_OT_FRIENDS_INVITE_25):
                {
                    // Handled by WEB_INVITE_JOIN_SITE
                    return -5;
                }
                case ((int) PacketId.WEB_OT_FRIENDS_INVITE_50):
                {
                    // Handled by WEB_INVITE_JOIN_SITE
                    return -5;
                }

                case ((int) PacketId.WEB_INVITE_JOIN_SITE):
                {
                     // See how many invites were accepted
                    uint iNumberInvitesAccepted = user.Stats.NumberInvitesAccepted;

                    if (iNumberInvitesAccepted > 0 && iNumberInvitesAccepted % 10 == 0)
                    {
                        packetId = (int)PacketId.WEB_OT_FRIENDS_INVITE_10;
                    }
                    else if (iNumberInvitesAccepted > 0 && iNumberInvitesAccepted % 25 == 0)
                    {
                        packetId = (int)PacketId.WEB_OT_FRIENDS_INVITE_25;
                    }
                    else if (iNumberInvitesAccepted > 0 && iNumberInvitesAccepted % 50 == 0)
                    {
                        packetId = (int)PacketId.WEB_OT_FRIENDS_INVITE_50;
                    }
                    else if (iNumberInvitesAccepted > 0 && iNumberInvitesAccepted % 5 == 0)
                    {
                        packetId = (int)PacketId.WEB_INVITE_JOIN_SITE;
                    }
                    else
                    {
                        // Not a multiple they get rewarded for
                        return -4;
                    }
                    break;
                }

                case ((int)PacketId.WORLD_INVITE_GO_INWORLD):
                {
                    uint iNumberInvitesInWorld = user.Stats.NumberInvitesInWorld;

                    if (iNumberInvitesInWorld > 0 && iNumberInvitesInWorld % 5 != 0)
                    {
                        // Not a multiple they get rewarded for
                        return -4;
                    }
                    break;
                }

                case ((int)PacketId.BOTH_OT_ADD_5_FRIENDS):
                {
                    // Make sure it is thier 5th friend
                    if (user.Stats.NumberOfFriends % 5 != 0)
                    {
                        return -4;
                    }
                    break;
                }

                case ((int)PacketId.BOTH_RAVED_PROFILE):
                {
                    if (user.NumberOfDiggs % 10 != 0)
                    {
                        return -4;
                    }
                    break;
                }

                // Per Billy request, add user as a member of 'Builder Fame' community
                // if they redeem the Active Time packet.
                case ((int)PacketId.BUILDER_ACTIVE_TIME):
                {
                    CommunityFacade communityFacade = new CommunityFacade();
                    int builderFameCommunityId = communityFacade.GetCommunityIdFromName("Builder Fame", false);
                    if (builderFameCommunityId > 0)
                    {
                        communityFacade.JoinCommunity(builderFameCommunityId, userId);
                    }
                    break;
                }

                default:
                {
                    break;
                }
            }


            // ***********************************************************
            // End Special Logic
            // ***********************************************************


            // Redeem the packet
            int returnCode = 0;
            int levelId = 0;
            bool levelUp = false;

            fameDao.AwardFame(packetId, userId, ref returnCode, ref levelId);

            // Did they level up?   Give out awards (rewards given in SP), and send blast
            if (userFameCurrent.LevelId < levelId)
            {
                // Next level reached
                levelUp = true;
                Level nextLevel = GetLevel(levelId);

                // What awards did they get?
                string strPMAward = "";
                string strPMNewTitle = "";
                string strBlastAward = "Reaching level " + nextLevel.LevelNumber + " in fame awarded prizes:<br/><br/>";
                string strNewTitle = "";

                List<LevelAwards> levelAwards = fameDao.GetLevelAwards(levelId, user.Gender);
                foreach (LevelAwards levelAward in levelAwards)
                {
                    string thumb = "";
                    string label = "Item:";

                    if (levelAward.NewTitle.Length > 0 && strPMNewTitle.Length.Equals (0))
                    {
                        strNewTitle = levelAward.NewTitle;
                        strBlastAward += "Title '" + levelAward.NewTitle + "<br/>";
                        strPMNewTitle = "<div class=\"newtitle btmmargin\"><span>New Title:</span> " + levelAward.NewTitle + "</div>";
                    }

                    /*  TEST CODE */
                    strPMNewTitle = "<div class=\"newtitle btmmargin\"><span>New Title:</span> Up & Coming Kanevan</div>";
                    

                    if (levelAward.GlobalId > 0)
                    {
                        WOKItem item = new ShoppingFacade ().GetItem (levelAward.GlobalId);

                        if (item.ThumbnailLargePath.Length > 0)
                        {
                            thumb = Configuration.GetItemImageURL(item.ThumbnailLargePath, "la", ""); 
                        }
                    }
                    else if (levelAward.AnimationId > 0) // Emote
                    {
                        label = "Emote:";
                        thumb = Configuration.GetItemImageURL ("emote_90x90.jpg", "", "");
                    }
                    else                                // Functionality Unlock
                    {
                        label = "Unlock:";
                        thumb = Configuration.GetItemImageURL ("unlocked_90x90.jpg", "", "");
                    }

                    // KMail
                    thumb = "<div class=\"itemImg\">" +
                            "<img src=\"" + thumb + "\" border=\"0\" /></div>";

                    strPMAward += "<div class=\"itemContainer clear btmmargin\"><div class=\"floatL\">" + thumb + "</div>" +
                        "<div class=\"itemDesc floatL\"><span>" + label + "</span><br/>" + levelAward.Name + "</div></div>";
                    
                    // Blast
                    strBlastAward += levelAward.Quantity + " " + levelAward.Name + "<br/>";
                }

                strBlastAward += "<br/><br/>Earn fame to get your own titles and rewards, and get on the road to Fame and Fortune!<br/><br/>";

                if (strPMAward.Length > 0)
                {
                    strPMAward = "<div class=\"btmmargin\">You've also been awarded:</div>" + strPMAward;
                }

                FameType fameType = fameDao.GetFameType(fameTypeId);

                // Only send a PM or blast if not on the first level
                if (nextLevel.LevelNumber > 0)
                {
                    // Send PM
                    if (nextLevel.SendKmail)
                    {
                        string strPMRewards = nextLevel.Rewards > 0 ? "<div class=\"bonus btmmargin\"><span>Level Up Bonus:</span> " + nextLevel.Rewards + " reward points</div>" : "";

                        Message message = new Message (0, userId, userId, fameType.Name + " Level " + nextLevel.LevelNumber, 
                            "<div class=\"container\">" +         
                            "<div class=\"title\">You've reached " + fameType.Name.ToUpper() + " LEVEL " + nextLevel.LevelNumber + "!</div>" +
                            strPMRewards + 
                            strPMNewTitle + 
                            strPMAward +
                            "<div class=\"clear footnote\">Keep playing to earn more Rewards and additional bonuses!</div>" +
                            "</div>", 
                            new DateTime(), 0, 0, 0, "U", "S");
                        //userFacade.InsertMessage(message);
                        
                        BlastFacade blastFacade = new BlastFacade ();
                        blastFacade.SendFameLevelUpBlast (userId, user.Username, message.Subject, message.MessageText);
                    }

                    //// Send Blast
                    if (strNewTitle.Length > 0)
                    {
                        BlastFacade blastFacade = new BlastFacade();
                        blastFacade.SendFameBlast(userId, user.Username, user.Username + " achieved fame title '" + strNewTitle + "'", strBlastAward);
                    }
                }
                
                // Give level up awards here? check SP
                AddLevelAwards (levelId, user);
            }

            // If it is one time packet, see if they completed a checklist
            if (packet.TimeInterval == (int)PacketTimeInterval.OneTime && packet.PacketId != (int) PacketId.BOTH_OT_NEWBIE_TASKS)
            {
                // Did they complete a checklist?
                // See which checklist this packet is in
                int checkListId = fameDao.GetChecklistIdByPacketId (packetId);

                // See if they completed them all
                if (checkListId > 0 && fameDao.IsChecklistCompleteLongWay (userId, checkListId))
                {
                    fameDao.InsertChecklistComplete(userId, checkListId);

                    // Be careful of recursion here.
                    RedeemPacket(userId, (int)PacketId.BOTH_OT_NEWBIE_TASKS, (int) FameTypes.World);

                    
                    // In addition to fame, update if it is a referral
                    MarketingFacade marketingFacade = new MarketingFacade();
                    if (marketingFacade.CompleteReferral(userId) != 0)
                    {
                        // Reward For Refferal
                        int referringUserID = marketingFacade.GetReferrer(userId);
                        RedeemPacket(referringUserID, (int)PacketId.REFERRAL_LIST_COMPLETED, (int)FameTypes.World);                        
                    };
                }
            }

            // Mark max per day records (No need to do this for one time?????)
            if (packet.MaxPerDay > 0)
            {
                fameDao.UpdateRewardIntervalDaily (userId, packetId);
            }

            int packetNotificationId = 0;
            int levelNotificationId = 0;
            int badgeNotificationId = 0;

            // Give out badges
            if (packet.BadgeId > 0)
            {
                GameFacade gameFacade = new GameFacade();

                gameDao.EarnAchievement(userId, (uint)packet.BadgeId);
                badgeNotificationId = gameDao.InsertNotifcation(userId, 0, -1, packet.BadgeId);

                gameFacade.AddBadgeAwards(userId, (uint)packet.BadgeId);

                // Send badge awarded notification to all clients in the same zone
                sendAchievementNotificationToPlayerZone(user, packet.BadgeId);
            }

            // Add level notification
            if (levelUp)
            {
                levelNotificationId = SendLevelUpNotification(user, fameTypeId, levelId);
            }

            // Add packet notification
            // Don't insert notification for daily consecutive login packets.  We do
            // not want to send this to user
            if (packetId != (int)PacketId.LOGIN_WOK_1_DAY_IN_A_ROW &&
                packetId != (int)PacketId.LOGIN_WOK_2_DAYS_IN_A_ROW &&
                packetId != (int)PacketId.LOGIN_WOK_3_DAYS_IN_A_ROW &&
                packet.FameTypeId != (int)FameTypes.CoreHelp)
            {
                // https://kaneva.atlassian.net/browse/ED-4695
                // Winner of test was to not show In World 15 Minutes packet
                if (packetId != (int)PacketId.IN_WORLD_15_MINUTES)
                {
                    packetNotificationId = gameDao.InsertNotifcation(userId, packet.PacketId, -1, 0);
                }
            }

            if (user.WokPlayerId > 0)
            {
                // Temporary hack to exclude the daily consecutive login packets.  Will remove
                // when permanent fix is put in place.
                if (packetId != (int) PacketId.LOGIN_WOK_1_DAY_IN_A_ROW &&
                    packetId != (int) PacketId.LOGIN_WOK_2_DAYS_IN_A_ROW &&
                    packetId != (int) PacketId.LOGIN_WOK_3_DAYS_IN_A_ROW)
                {
                    // https://kaneva.atlassian.net/browse/ED-4695
                    // Winner of test was to not show In World 15 Minutes packet
                    if (packetId != (int)PacketId.IN_WORLD_15_MINUTES)
                    {
                        SendFameEarnedEventToPlayer(user, fameTypeId, packet, levelUp, levelId, packetNotificationId, levelNotificationId, packet.BadgeId, badgeNotificationId);
                    }
                }
            }

            m_logger.Debug ("END RedeemPacket() for PacketId = " + packetId + ", UserId = " + userId);

            return returnCode;
        }

        public int SendLevelUpNotification(User user, int fameTypeId, int levelId)
        {
            int levelNotificationId = gameDao.InsertNotifcation(user.UserId, 0, levelId, 0);
            // Send level up notification to all clients in the same zone
            sendLevelNotificationToPlayerZone(user, fameTypeId, levelId);
            return levelNotificationId;
        }
		
        public int GetRewardInterval (int userId, int packetId)
        {
            return fameDao.GetRewardInterval(userId, packetId);
        }

        public int GetRewardIntervalDaily(int userId, int packetId)
        {
            return fameDao.GetRewardIntervalDaily(userId, packetId);
        }

        public void UpdateDailyHistory(int userId, int packetId, int fameTypeId)
        {
            if (packetId == (int)PacketId.WORLD_DAILY_SIGNIN)
            {
                // Add this packet to Daily History table.  It will be used to calculate the
                // number of consecutive days a user has logged into WoK
                fameDao.UpdateDailyHistory(userId, packetId, (int)FameTypes.World);
            }
        }

        public int GetRewardProgress(Packet packet, int userId)
        {
            int currentRewardInterval = 0;
            if (packet.TimeInterval == (int)PacketTimeInterval.OneTime ||
                packet.TimeInterval == (int)PacketTimeInterval.Unlimited)
            {
                if (packet.RewardInterval > 0)
                {
                    currentRewardInterval = this.GetRewardInterval (userId, packet.PacketId);
                }
                else
                {
                    if (packet.TimeInterval == (int)PacketTimeInterval.OneTime)
                    {
                        if (this.IsOneTimePacketRedeemed(userId, packet.PacketId))
                        {
                            currentRewardInterval = 1;
                        }
                        else
                        {
                            currentRewardInterval = 0;
                        }
                    }
                }
            }
            else if (packet.TimeInterval == (int)PacketTimeInterval.Daily)
            {
                if (packet.RewardInterval > 0)
                {
                    currentRewardInterval = GetRewardIntervalDaily (userId, packet.PacketId);
                }
            }
            return currentRewardInterval;
        }

        public int IsPacketAvailable(Packet packet, int userId)
        {
            int available = 0;
             if (packet.TimeInterval == (int)PacketTimeInterval.OneTime)
            {
                bool bRedeemed = this.IsOneTimePacketRedeemed(userId, packet.PacketId);

                if (bRedeemed)
                {
                    available = 0;
                }
                else
                {
                    available = 1;
                }
            }
            else if (packet.TimeInterval == (int)PacketTimeInterval.Daily)
            {
                if (this.IsUserAtMaxRedemptions(userId, packet.PacketId, packet.FameTypeId, packet.MaxPerDay))
                {
                    available = 0;
                }
                else
                {
                    available = 1;
                }
            }
            else if (packet.TimeInterval == (int)PacketTimeInterval.Unlimited)
            {
                available = 1;
            }
            return available;
        }

        private void sendPacketRewardIntervalUpdateToPlayer(User user, Packet packet)
        {
            int available = IsPacketAvailable(packet, user.UserId);
            int progress = GetRewardProgress(packet, user.UserId);

            // Save a query here, use it from above and pass in
            // This event happens almost 8 million times per day on 6/21....
            int numberOfDailyRedeptions = GetRewardIntervalDaily (user.UserId, packet.PacketId);  

            int remainingPerDay = packet.MaxPerDay - numberOfDailyRedeptions;

            XmlEventCreator xml = new XmlEventCreator("ClientTickler");
            //first encode packet information
            xml.AddValue((int)NotificationsProfile.NotificationTypes.Fame_PacketProgress);
            xml.AddValue(packet.FameTypeId);
            xml.AddValue(fameDao.GetFameType(packet.FameTypeId).Name);
            xml.AddValue(packet.PacketId);
            xml.AddValue(packet.Name);
            xml.AddValue(available);
            xml.AddValue(progress);
            xml.AddValue(remainingPerDay);
            xml.AddPlayer(user.WokPlayerId);

            try
            {
                UserFacade userFacade = new UserFacade();
                NotificationsProfile np = new NotificationsProfile(0, user.UserId, (int)NotificationsProfile.NotificationTypes.Fame_PacketProgress, xml.GetXml(), "");
                userFacade.InsertUserNotificationProfile(np);
            }
            catch (Exception e)
            {
                m_logger.Error("Error inserting notification profile", e);
            }


            try
            {
                RemoteEventSender sender = new RemoteEventSender();
                sender.BroadcastEventToServer(xml.GetXml()); //send event to all servers
            }
            catch (Exception e)
            {
                m_logger.Error("Error Broadcasting Fame Event to WoK Server ", e);
            }
        }


        public void SendFameEarnedEventToPlayer(User user, int fameTypeId, Packet packet, bool levelUp, int levelId, int packetNotificationId, int levelNotificationId, int badgeId, int badgeNotificationId)
        {
            //send an event to the server that they were awarded fame, the server will send to client

            //earned from this packet
            string title = "";
            string award = "";
            int awardQty = 0;
            int rewards = 0;
            //nextLevelAwards
            string nextTitle = "";
            string nextAward = "";
            int nextAwardQty = 0;
            int available = IsPacketAvailable(packet, user.UserId);
            int progress = GetRewardProgress(packet, user.UserId);
            int numberOfDailyRedeptions = GetRewardIntervalDaily(user.UserId, packet.PacketId);
            int remainingPerDay = packet.MaxPerDay - numberOfDailyRedeptions;

            //we need both current Level fame and next Level Awards
            UserFame currentFame = fameDao.GetUserFame(user.UserId, fameTypeId );
            Level currentLevel = fameDao.GetLevel(levelId);
            List<LevelAwards> nextLevelAwards = fameDao.GetLevelAwards(currentLevel.NextLevelId, user.Gender);
            if (levelUp)
            {
                List<LevelAwards> levelAwards = fameDao.GetLevelAwards(levelId, user.Gender);
                foreach (LevelAwards levelAward in levelAwards)
                {
                    if (levelAward.NewTitle.Length > 0)
                    {
                        title = levelAward.NewTitle;
                    }

                    if (levelAward.GlobalId > 0)
                    {
                        awardQty = levelAward.Quantity;
                        award = levelAward.Name;
                    }
                }
            }
            rewards = currentLevel.Rewards;

            foreach (LevelAwards levelAward in nextLevelAwards)
            {
                if (levelAward.NewTitle.Length > 0)
                {
                    nextTitle = levelAward.NewTitle;
                }

                if (levelAward.GlobalId > 0)
                {
                    nextAwardQty = levelAward.Quantity;
                    nextAward = levelAward.Name;
                }
            }          

            XmlEventCreator xml = new XmlEventCreator("FameEarnedEvent");
            //first encode packet information
            xml.AddValue(packet.FameTypeId);
            xml.AddValue(fameDao.GetFameType(packet.FameTypeId).Name);
            xml.AddValue(packet.PacketId);
            xml.AddValue(packet.Name);
            xml.AddValue(packet.PointsEarned);
            xml.AddValue(packet.Rewards);
            //encode whether they leveled up due to this packet
            xml.AddValue(Convert.ToInt16(levelUp));
            //encode their current level information
            xml.AddValue(levelId);
            xml.AddValue(currentLevel.LevelNumber);
            xml.AddValue(currentFame.Points);
            xml.AddValue(currentLevel.StartPoints);
            xml.AddValue(currentLevel.EndPoints);
            xml.AddValue(rewards);
            xml.AddValue(title);
            xml.AddValue(award);
            xml.AddValue(awardQty);
             //encode their next level awards
            xml.AddValue(currentLevel.NextLevelId);
            xml.AddValue(nextTitle);
            xml.AddValue(nextAward);
            xml.AddValue(nextAwardQty);
            xml.AddValue(available);
            xml.AddValue(progress);
            xml.AddValue(remainingPerDay);
            xml.AddPlayer(user.WokPlayerId);

            // Notfications
            xml.AddValue(packetNotificationId);
            xml.AddValue(levelNotificationId);

            // Earned a badge?
            xml.AddValue(badgeId);
            xml.AddValue(badgeNotificationId);

            try
            {
                UserFacade userFacade = new UserFacade();
                NotificationsProfile np = new NotificationsProfile(0, user.UserId, (int) NotificationsProfile.NotificationTypes.FameXPEarned, xml.GetXml(), "");
                userFacade.InsertUserNotificationProfile(np);
            }
            catch (Exception e)
            {
                m_logger.Error("Error inserting notification profile", e);
            }

            try
            {
                m_logger.Debug("BEGIN SEND FameEarnedEvent: Username= " + user.Username + ", UserId = " + user.UserId);
                m_logger.Debug("FameEarnedEvent XML=" + xml.GetXml());
                RemoteEventSender sender = new RemoteEventSender();
                sender.BroadcastEventToServer(xml.GetXml()); //send event to all servers
                //sender.BroadcastMessageToPlayer("Test", user.WokPlayerId);
                m_logger.Debug("END SEND FameEarnedEvent: Username= " + user.Username + ", UserId = " + user.UserId);
            }
            catch (Exception e) 
            {
                m_logger.Error("Error Broadcasting Fame Event to WoK Server ", e);
            }
        }

		public void sendLevelNotificationToPlayerZone(User user, int fameTypeId, int levelId)
		{
			//send an event to the server that they were awarded fame, the server will send to client
			Level currentLevel = fameDao.GetLevel(levelId);

			XmlEventCreator xml = new XmlEventCreator("FameLevelUpEvent");
			//encode their current level information
			xml.AddValue(user.Username);
			xml.AddValue(fameTypeId);
			xml.AddValue(currentLevel.LevelNumber);
			xml.AddZoneByPlayerIds(user.WokPlayerId);

            try
            {
                UserFacade userFacade = new UserFacade ();
                NotificationsProfile np = new NotificationsProfile(0, user.UserId, (int) NotificationsProfile.NotificationTypes.FameLevelUp, xml.GetXml(), "");
                userFacade.InsertUserNotificationProfile(np);
	        }
			catch (Exception e)
			{
				m_logger.Error("Error inserting notification profile", e);
			}

			try
			{
				RemoteEventSender sender = new RemoteEventSender();
				sender.BroadcastEventToServer(xml.GetXml()); //send event to all servers
			}
			catch (Exception e)
			{
				m_logger.Error("Error Broadcasting Fame Event to WoK Server ", e);
			}
		}


        public void sendAchievementNotificationToPlayerZone(User user, int achievementId)
        {
            XmlEventCreator xml = new XmlEventCreator("FameBadgeAwardedEvent");
			//encode badge awarded event
			xml.AddValue(user.Username);
			xml.AddValue(achievementId);
			xml.AddZoneByPlayerIds(user.WokPlayerId);

            try
            {
                GameFacade gameFacade = new GameFacade();
                List<AchievementAward> achievementAwards = gameFacade.GetAchievementAwards((uint) achievementId, user.Gender);
                if (achievementAwards.Count > 0)
                {
                    foreach (AchievementAward aa in achievementAwards)
                    {
                        //XmlElement badgeAwardNode = doc.CreateElement("BadgeAward");
                        //xml.AppendChild(badgeAwardNode);

                        // Add the award node
                        //badgeAwardNode.SetAttribute("BadgeAwardId", aa.AchievementAwardId.ToString());
                        //badgeAwardNode.SetAttribute("Name", aa.Name);
                        //badgeAwardNode.SetAttribute("GlobalId", aa.GlobalId.ToString());
                        //badgeAwardNode.SetAttribute("AnimationId", aa.AnimationId.ToString());
                        //badgeAwardNode.SetAttribute("Gender", aa.Gender);
                        //badgeAwardNode.SetAttribute("Quantity", aa.Quantity.ToString());
                        xml.AddValue(aa.Rewards.ToString());
                        xml.AddValue(aa.NewTitleName);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error inserting achievement awards", exc);
            }

            try
            {
                UserFacade userFacade = new UserFacade();
                NotificationsProfile np = new NotificationsProfile(0, user.UserId, (int) NotificationsProfile.NotificationTypes.FameBadgeEarned, xml.GetXml(), "");
                userFacade.InsertUserNotificationProfile(np);
            }
            catch (Exception e)
            {
                m_logger.Error("Error inserting notification profile", e);
            }

			try
			{
                m_logger.Debug("**** BroadcastEventToServer XML = " + xml.GetXml());
                RemoteEventSender sender = new RemoteEventSender();
				sender.BroadcastEventToServer(xml.GetXml()); //send event to all servers
			}
			catch (Exception e)
			{
				m_logger.Error("Error Broadcasting Fame Event to WoK Server ", e);
			}
		}

        /// <summary>
        /// AddLevelAwards
        /// </summary>
        private void AddLevelAwards (int levelId, User user)
		{
            UserFacade userFacade = new UserFacade();
            string WOK_INVENTORY_TYPE_PERSONAL = "P";

            try
            {
                List<LevelAwards> levelAwards = fameDao.GetLevelAwards(levelId, user.Gender);

                foreach (LevelAwards levelAward in levelAwards)
                {
                    if (levelAward.NewTitle.Length > 0)
                    { 
                        // Award the new title here
                        // They have to be in WOK
                        if (user.WokPlayerId > 0)
                        {
                            // Add it to wok.player_titles
                            userFacade.InsertPlayerTitle(user.WokPlayerId, levelAward.NewTitle, 0);
                            


                            // Set it on thier player record
                            userFacade.UpdatePlayerTitle (user.WokPlayerId, levelAward.NewTitle);
                        }
                    }


                    if (levelAward.GlobalId > 0)
                    {
                        userFacade.AddItemToPendingInventory (user.UserId, WOK_INVENTORY_TYPE_PERSONAL, levelAward.GlobalId, levelAward.Quantity, 512);
                    }
                }
            }
            catch (Exception) { }
		}

        /// <summary>
        /// IsPacketRedeemed
        /// </summary>
        public bool IsOneTimePacketRedeemed (int userId, int packetId)
        {
            OnetimeHistory onetimeHistory = fameDao.GetOneTimeHistory(userId, packetId);
            return (onetimeHistory != null);
        }

        /// <summary>
        /// IsUserAtMaxRedemptions
        /// </summary>
        public bool IsUserAtMaxRedemptions(int userId, int packetId, int fameTypeId, int maxPerDay)
        {
            int packetCount = fameDao.GetRewardIntervalDaily (userId, packetId);
            return (packetCount >= maxPerDay);
        }

        /// <summary>
        /// Gets a list of nudges belong to a checklist
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<Nudge> GetChecklist (int userId, int checklistId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return fameDao.GetChecklist (userId, checklistId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Gets a list of nudges user has not completed
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<Nudge> GetNudges (int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return fameDao.GetNudges (userId, filter, orderby, pageNumber, pageSize);
        }

         /// <summary>
        /// Gets a list of nudges user has not completed
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<Nudge> GetNudgesForHelp(int userId, int checklistId)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return fameDao.GetChecklistNudges(userId, checklistId);
        }

        

        /// <summary>
        /// IsPacketRedeemed
        /// </summary>
        public bool IsChecklistComplete (int userId, int checklistId)
        {
            return fameDao.IsChecklistComplete (userId, checklistId);
        }


        /*  Methods for Beta Testing */
        
        /// <summary>
        /// IsAllowedToUseFame
        /// </summary>
        public bool IsAllowedToUseFame (int userId)
        {
            return true;
        }
        
        /// <summary>
        /// IsAllowedToViewFame
        /// </summary>
        public bool IsAllowedToViewFame (int userId)
        {
            return true;
        }

        /// <summary>
        /// GetYesterdayWebLogins
        /// </summary>
        public List<User> GetYesterdayWebLogins()
        {
            return fameDao.GetYesterdayWebLogins();
        }

        /// <summary>
        /// GetYesterdayWOKLogins
        /// </summary>
        public List<User> GetYesterdayWOKLogins()
        {
            return fameDao.GetYesterdayWOKLogins();
        }

        /// <summary>
        /// GetYesterday10MinsInWorld
        /// </summary>
        /// <returns></returns>
        public List<User> GetYesterday10MinsInWorld()
        {
            return fameDao.GetYesterday10MinsInWorld();
        }

        /// <summary>
        /// GetYesterday3HoursInWorld
        /// </summary>
        /// <returns></returns>
        public List<User> GetYesterday3HoursInWorld()
        {
            return fameDao.GetYesterday3HoursInWorld();
        }

        /// <summary>
        /// GetYesterdayDanceLevel10
        /// </summary>
        /// <returns></returns>
        public List<User> GetYesterdayDanceLevel10()
        {
            return fameDao.GetYesterdayDanceLevel10();
        }

        /// <summary>
        /// GetYesterdayVisitForum
        /// </summary>
        /// <returns></returns>
        public List<User> GetYesterdayVisitForum()
        {
            return fameDao.GetYesterdayVisitForum();
        }

        /// <summary>
        /// GetYesterdayRead3Posts
        /// </summary>
        /// <returns></returns>
        public List<User> GetYesterdayRead3Posts()
        {
            return fameDao.GetYesterdayRead3Posts();
        }

        /// <summary>
        /// GetYesterdayView3Media
        /// </summary>
        /// <returns></returns>
        public List<User> GetYesterdayView3Media()
        {
            return fameDao.GetYesterdayView3Media();
        }

        /// <summary>
        /// InsertDanceToLevel10
        /// </summary>
        public int InsertDanceToLevel10(int userId)
        {
            return fameDao.InsertDanceToLevel10(userId);
        }

        /// <summary>
        /// GetFamePointsFromInvitingFriends
        /// </summary>
        public DataRow GetFamePointsFromInvitingFriends (int userId)
        {
            return fameDao.GetFamePointsFromInvitingFriends (userId);
        }

        /// <summary>
        /// GetUserLastDailyLoginPacketRedemption
        /// </summary>
        public DataRow GetUserLastDailyLoginPacketRedemption (int userId)
        {
            return fameDao.GetUserLastDailyLoginPacketRedemption (userId);
        }

        public Checklist GetChecklist (int userId, int checklistId)
        {
            Checklist cl = fameDao.GetChecklist (checklistId, userId);

            //gets checklist packets proxy object
            cl.ChecklistPackets = new ProxyForChecklistPackets<ChecklistPacket> (cl, userId);

            return cl;
        }

        public void CompleteAccountCompleteChecklist (int userId, int checklistId)
        {
            // Check to see if already in completed checklists table.  If so, don't add again.
            if (!fameDao.IsChecklistComplete(userId, checklistId))
            {
                fameDao.InsertChecklistComplete (userId, checklistId);
            }
        }
    }
}
