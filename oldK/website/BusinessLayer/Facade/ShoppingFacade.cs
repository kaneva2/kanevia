///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;
using System.Data;
using System.Linq;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class ShoppingFacade
    {
        private IShoppingDao shoppingDao = DataAccess.ShoppingDao;

        // ***********************************************
        // Game Object Related Functions
        // ***********************************************
        #region Game Items

         /// <summary>
        /// AddCustomItem
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int AddCustomItem (WOKItem item)
        {
            return AddCustomItem (item, 0);
        }
        
        /// <summary>
        /// AddCustomItem
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddCustomItem(WOKItem item, UInt32 animTargetActorGLID)
        {
            if (item.DesignerPrice < 0)
            {
                item.DesignerPrice = 0;
            }

            return shoppingDao.AddCustomItem(item, animTargetActorGLID);
        }

        public bool IsItemAccessPass(int globalId)
        {
            return shoppingDao.IsItemAccessPass(globalId);
        }

        /// <summary>
        /// UpdateCustomItemTexture
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateCustomItemTexture(WOKItem item)
        {
            return shoppingDao.UpdateCustomItemTexture(item);
        }

         /// <summary>
        /// UpdateCustomItemTextureEncryption
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateCustomItemTextureEncryption(WOKItem item, string path)
        {
            return shoppingDao.UpdateCustomItemTextureEncryption(item, path);
        }

        public SGFrameworkSettings GetDeedSGFrameworkSettings(int templateGlid)
        {
            ISGFrameworkSettingsDao frameworkDao = DataAccess.SGFrameworkSettingsDao;
            return frameworkDao.GetDeedSGFrameworkSettings(templateGlid);
        }

        /// <summary>
        /// Helper function used by UpdateShopItem() to update the bundles associated with the item being edited.
        /// </summary>
        /// <param name="modifiedItem"></param>
        /// <param name="originalItem"></param>
        /// <returns></returns>
        private List<BundleItemChange> UpdateAssociatedBundles(WOKItem modifiedItem, WOKItem originalItem)
        {
            List<BundleItemChange> bundleChanges = new List<BundleItemChange>();

            if (originalItem.UseType != (WOKItem.USE_TYPE_BUNDLE) &&
                originalItem.UseType != (WOKItem.USE_TYPE_CUSTOM_DEED) &&
                (modifiedItem.DesignerPrice != originalItem.DesignerPrice ||    // price
                 modifiedItem.ItemActive != originalItem.ItemActive ||          // deleted, public, private
                 modifiedItem.PassTypeId != originalItem.PassTypeId ||          // AP
                 modifiedItem.InventoryType != originalItem.InventoryType ||    // rewards and/or credits
                 (originalItem.DerivationLevel == 0 && originalItem.BaseGlobalId != 0 && modifiedItem.TemplateDesignerPrice != originalItem.TemplateDesignerPrice))
                )
            {
                List<WOKItem> bundles = GetBundlesItemBelongsTo(modifiedItem.GlobalId, "", false);

                foreach (WOKItem modifiedBundle in bundles)
                {
                    bool bundleUpdated = false;
                    
                    // Item marked private/deleted removes bundle from shop
                    if (modifiedItem.ItemActive == (int)WOKItem.ItemActiveStates.Private ||
                        modifiedItem.ItemActive == (int)WOKItem.ItemActiveStates.Deleted)
                    {
                        if (modifiedBundle.ItemActive == (int)WOKItem.ItemActiveStates.Public)
                        {
                            modifiedBundle.ItemActive = (int)WOKItem.ItemActiveStates.Private;
                            bundleUpdated = true;
                        }
                    }

                    // Item changed to AP, bundle gets set to AP if it is not already
                    if (modifiedItem.PassTypeId != originalItem.PassTypeId && modifiedItem.PassTypeId == 1) // 1 is ePASS_TYPE.ACCESS
                    {
                        // If bundle is not already AP, update bundle
                        if (modifiedBundle.PassTypeId != 1) // 1 is ePASS_TYPE.ACCESS
                        {
                            modifiedBundle.PassTypeId = modifiedItem.PassTypeId;
                            bundleUpdated = true;
                        }
                    }

                    // Item price changed
                    if (modifiedItem.DesignerPrice != originalItem.DesignerPrice)
                    {
                        // We store WebPrice as market cost for bundles so the price does not have to be
                        // calculated on the fly for search.
                        modifiedBundle.BundleItems = (modifiedBundle.BundleItems == null ? GetBundleItems(modifiedBundle.GlobalId) : modifiedBundle.BundleItems);
                        modifiedBundle.MarketCost = modifiedBundle.WebPrice;
                        bundleUpdated = true;
                    }

                    // Item payment type changed, bundle type will change
                    if (modifiedItem.InventoryType != originalItem.InventoryType)
                    {
                        // Only need to update if bundle inv type is not already set
                        // to the item's new inventory type
                        if (modifiedItem.InventoryType != modifiedBundle.InventoryType)
                        {
                            // If changed to credits only, then change bundle to credits only
                            if (modifiedItem.InventoryType == (int)WOKItem.InventoryTypes.Credits)
                            {
                                modifiedBundle.InventoryType = (int)WOKItem.InventoryTypes.Credits;
                                bundleUpdated = true;
                            }
                            else
                            {
                                // get all the items belonging to this bundle and see if any
                                // other items are credits only.  if not then update bundle.
                                bool updateInvType = true;

                                modifiedBundle.BundleItems = (modifiedBundle.BundleItems == null ? GetBundleItems(modifiedBundle.GlobalId) : modifiedBundle.BundleItems);
                                foreach (WOKItem bi in modifiedBundle.BundleItems)
                                {
                                    // if another item in bundle is already credits only the bundle
                                    // will stay credits only.  No need to update.
                                    if (bi.InventoryType == (int)WOKItem.InventoryTypes.Credits)
                                    {
                                        updateInvType = false;
                                        break;
                                    }
                                }

                                if (updateInvType)
                                {
                                    modifiedBundle.InventoryType = modifiedItem.InventoryType;
                                    bundleUpdated = true;
                                }
                            }
                        }
                    }

                    // Update the bundle if necessary
                    if (bundleUpdated)
                    {
                        WOKItem originalBundle = GetItem(modifiedBundle.GlobalId);
                        bundleChanges.Add(new BundleItemChange(originalItem, modifiedItem, originalBundle, modifiedBundle));
                        UpdateCustomItem(modifiedBundle);
                    }
                }

                // If price change then we have to check to see if any items are derived from this
                // item.  If so we need to check to see if the derived items are included in any
                // bundles, update those bundles and notify bundle owners that the price has changed
                // check item.TemplateDesignerPrice != UInt32.MaxValue since this value is not set for texture updates, defaults to MaxValue (See UpdateCustomItem that is where logic i copied from)
                if ((modifiedItem.TemplateDesignerPrice != originalItem.TemplateDesignerPrice) && (modifiedItem.TemplateDesignerPrice != UInt32.MaxValue))  //origItem.DerivationLevel != WOKItem.DRL_UGCDO_DERIVATIVE_L1 &&
                {
                    // Get derived items
                    List<WOKItem> derivatives = GetDerivedItemsIncludedInBundles((int)originalItem.BaseGlobalId);
                    foreach (WOKItem derivedItem in derivatives)
                    {
                        // Get bundles these items belong to
                        List<WOKItem> diBundles = GetBundlesItemBelongsTo(derivedItem.GlobalId, "", false);
                        foreach (WOKItem diBundle in diBundles)
                        {
                            // Sore original bundle price to avoid having to get the item a second time
                            int? originalBundlePrice = diBundle.MarketCost;

                            // Get all items in bundles so we can recalc the new price for the bundle
                            diBundle.BundleItems = GetBundleItems(diBundle.GlobalId);
                            diBundle.MarketCost = diBundle.WebPrice;
                            
                            // Save the new price for search
                            UpdateCustomItem(diBundle);
                            bundleChanges.Add(new BundleItemChange(originalItem, derivedItem, diBundle, diBundle, originalBundlePrice));
                        }
                    }
                }
            }

            return bundleChanges;
        }

        private List<DeedItemChange> UpdateAssociatedDeed(WOKItem modifiedItem, WOKItem originalItem)
        {
            List<DeedItemChange> deedChanges = new List<DeedItemChange>();

            if (originalItem.UseType != (WOKItem.USE_TYPE_CUSTOM_DEED) &&
                originalItem.UseType != (WOKItem.USE_TYPE_BUNDLE) &&
                (modifiedItem.DesignerPrice != originalItem.DesignerPrice ||    // price
                 modifiedItem.ItemActive != originalItem.ItemActive ||          // deleted, public, private
                 modifiedItem.PassTypeId != originalItem.PassTypeId ||          // AP
                 modifiedItem.InventoryType != originalItem.InventoryType ||    // rewards and/or credits
                 (originalItem.DerivationLevel == 0 && originalItem.BaseGlobalId != 0 && modifiedItem.TemplateDesignerPrice != originalItem.TemplateDesignerPrice))
                )
            {
                List<WOKItem> deeds = GetDeedsItemBelongsTo(modifiedItem.GlobalId, "", false);
                foreach (WOKItem modifiedDeed in deeds)
                {
                    bool deedUpdated = false;

                    // Item marked private/deleted removes deed from shop
                    // Per discussion with CK, do not remove associated deeds from shop when a component item is deactivated
                    //if (modifiedItem.ItemActive == (int)WOKItem.ItemActiveStates.Private ||
                    //    modifiedItem.ItemActive == (int)WOKItem.ItemActiveStates.Deleted)
                    //{
                    //    if (modifiedDeed.ItemActive == (int)WOKItem.ItemActiveStates.Public)
                    //    {
                    //        // update deed
                    //        modifiedDeed.ItemActive = (int)WOKItem.ItemActiveStates.Private;
                    //        deedUpdated = true;
                    //    }
                    //}

                    // Item changed to AP, deed gets set to AP if it is not already
                    if (modifiedItem.PassTypeId != originalItem.PassTypeId && modifiedItem.PassTypeId == 1) // 1 is ePASS_TYPE.ACCESS
                    {
                        // If deed is not already AP, update bundle
                        if (modifiedDeed.PassTypeId != 1) // 1 is ePASS_TYPE.ACCESS
                        {
                            // update deed
                            modifiedDeed.PassTypeId = modifiedItem.PassTypeId;
                            deedUpdated = true;
                        }
                    }

                    // Item price changed
                    int oldMarketCost = modifiedDeed.MarketCost;

                    List<DeedItem> deedItems = GetDeedSalesItemList(modifiedDeed.GlobalId, (int)modifiedDeed.ItemCreatorId);
                    SGFrameworkSettings fwSettings = GetDeedSGFrameworkSettings(modifiedDeed.GlobalId);
                    DeedPrice bp = CalculateInitialDeedPrice(deedItems, (int)modifiedDeed.DesignerPrice, modifiedDeed.ItemCreatorId, (fwSettings != null && fwSettings.FrameworkEnabled));
                    modifiedDeed.MarketCost = bp.TotalDeedWithCommission;

                    if (!modifiedDeed.MarketCost.Equals(oldMarketCost))
                    {
                        deedUpdated = true;
                    }

                    // Item payment type changed, bundle type will change
                    if (modifiedItem.InventoryType != originalItem.InventoryType)
                    {
                        // Only need to update if deed inv type is not already set
                        // to the item's new inventory type
                        if (modifiedItem.InventoryType != modifiedDeed.InventoryType)
                        {
                            // If changed to credits only, then change deed to credits only
                            if (modifiedItem.InventoryType == (int)WOKItem.InventoryTypes.Credits)
                            {
                                modifiedDeed.InventoryType = (int)WOKItem.InventoryTypes.Credits;
                                deedUpdated = true;
                            }
                            // Framework worlds are always credit only, don't bother checking.
                            else if (fwSettings == null || !fwSettings.FrameworkEnabled)
                            {
                                // get all the items belonging to this deed and see if any
                                // other items are credits only.  if not then update deed.
                                bool updateInvType = true;

                                foreach (DeedItem di in deedItems)
                                {
                                    // if another item in deed is already credits only the deed
                                    // will stay credits only.  No need to update.
                                    if (di.InventoryType == (int)WOKItem.InventoryTypes.Credits)
                                    {
                                        updateInvType = false;
                                        break;
                                    }
                                }

                                if (updateInvType)
                                {
                                    modifiedDeed.InventoryType = modifiedItem.InventoryType;
                                    deedUpdated = true;
                                }
                            }
                        }
                    }

                    // Update the bundle if necessary
                    if (deedUpdated)
                    {
                        WOKItem originalDeed = GetItem(modifiedDeed.GlobalId);
                        deedChanges.Add(new DeedItemChange(originalItem, modifiedItem, originalDeed, modifiedDeed));
                        UpdateCustomItem(modifiedDeed);
                    }
                }


                // If price change then we have to check to see if any items are derived from this
                // item.  If so we need to check to see if the derived items are included in any
                // deed, update those deed and notify deed owners that the price has changed
                // check item.TemplateDesignerPrice != UInt32.MaxValue since this value is not set for texture updates, defaults to MaxValue (See UpdateCustomItem that is where logic i copied from)
                if ((modifiedItem.TemplateDesignerPrice != originalItem.TemplateDesignerPrice) && (modifiedItem.TemplateDesignerPrice != UInt32.MaxValue))  //origItem.DerivationLevel != WOKItem.DRL_UGCDO_DERIVATIVE_L1 &&
                {
                    // Get derived items
                    List<WOKItem> derivatives = GetDerivedItemsIncludedInDeeds((int)originalItem.BaseGlobalId);
                    foreach (WOKItem derivedItem in derivatives)
                    {
                        // Get bundles these items belong to
                        List<WOKItem> diDeeds = GetDeedsItemBelongsTo(derivedItem.GlobalId, "", false);
                        foreach (WOKItem diDeed in diDeeds)
                        {
                            // Sore original bundle price to avoid having to get the item a second time
                            int? originalDeedPrice = diDeed.MarketCost;

                            // Get all items in bundles so we can recalc the new price for the bundle
                            List<DeedItem> deedItems = GetDeedSalesItemList(diDeed.GlobalId, (int)diDeed.ItemCreatorId);
                            SGFrameworkSettings fwSettings = GetDeedSGFrameworkSettings(diDeed.GlobalId);
                            DeedPrice bp = CalculateInitialDeedPrice(deedItems, (int)diDeed.DesignerPrice, diDeed.ItemCreatorId, (fwSettings != null && fwSettings.FrameworkEnabled));
                            diDeed.MarketCost = bp.TotalDeedWithCommission;

                            // Save the new price for search
                            UpdateCustomItem(diDeed);
                            deedChanges.Add(new DeedItemChange(originalItem, derivedItem, diDeed, diDeed, originalDeedPrice));
                        }
                    }
                }
            }

            return deedChanges;
        }

        /// <summary>
        /// Encapsulates business logic used in updating an item on shop.
        /// </summary>
        /// <param name="modifiedItem">The item to update. IMPORTANT - Should be fully fleshed-out (i.e. result of GetItem() call).</param>
        /// <param name="modifyingUserId"></param>
        /// <param name="adminEdit"></param>
        /// <param name="removeFromInventories"></param>
        /// <param name="bundleChanges"></param>
        /// <param name="deedChanges"></param>
        /// <returns>-1 if user is not authorized to update the item</returns>
        public int UpdateShopItem(WOKItem modifiedItem, int modifyingUserId, bool adminEdit, bool removeFromInventories, 
            out List<BundleItemChange> bundleChanges, out List<DeedItemChange> deedChanges)
        {
            // Get original item for comparison
            WOKItem originalItem = GetItem(modifiedItem.GlobalId);

            // Validate that user is able to edit
            if (!adminEdit && originalItem.ItemCreatorId != modifyingUserId)
            {
                bundleChanges = new List<BundleItemChange>();
                deedChanges = new List<DeedItemChange>();
                return -1;
            }

            // Fall back to original inventory type if modified value is invalid
            modifiedItem.InventoryType = (modifiedItem.InventoryType == 0 || 
                                          modifiedItem.InventoryType == 256 || 
                                          modifiedItem.InventoryType == 512 ||
                                          modifiedItem.InventoryType == 768 ? modifiedItem.InventoryType : originalItem.InventoryType);

            // Only admins can change item creator id
            modifiedItem.ItemCreatorId = (adminEdit ? modifiedItem.ItemCreatorId : originalItem.ItemCreatorId);

            // Anyone can make an item AP. Only admins can remove AP.
            modifiedItem.PassTypeId = (adminEdit || modifiedItem.PassTypeId > 0 ? modifiedItem.PassTypeId : originalItem.PassTypeId);

            // Enforce designer price boundaries
            modifiedItem.DesignerPrice = (modifiedItem.DesignerPrice > 5000000 ? 5000000 : modifiedItem.DesignerPrice);
            modifiedItem.DesignerPrice = (modifiedItem.DesignerPrice < 0 ? 0 : modifiedItem.DesignerPrice);

            // Set designer price of item and base
            // This case covers "original" UGC DOs. All UGC DOs have base items, even if they're not derived from anything.
            // If a UGC DO is original, non-derived, it will have a base item generated for it on upload.
            // In this case, non-derived UGC DOs set the designer price of their base item that is generated for them upon upload
            // instead of the designer price of the item itself.
            if (modifiedItem.DerivationLevel == 0 && modifiedItem.BaseGlobalId != 0)
            {
                modifiedItem.TemplateDesignerPrice = modifiedItem.DesignerPrice;     // Set new designer price for template item
                modifiedItem.DesignerPrice = originalItem.DesignerPrice;                 // Keep original designer price for current item
            }

            // Special logic by type
            if (modifiedItem.UseType == WOKItem.USE_TYPE_BUNDLE)
            {
                // AP can only be removed if all items in the bundle are not AP
                if (modifiedItem.PassTypeId != originalItem.PassTypeId && modifiedItem.PassTypeId == 0) // GENERAL pass
                {
                    foreach (WOKItem item in modifiedItem.BundleItems)
                    {
                        if (item.PassTypeId > 0)
                        {
                            modifiedItem.PassTypeId = originalItem.PassTypeId;
                            break;
                        }
                    }
                }

                // We store WebPrice as market cost for bundles so the price does not have to be
                // calculated on the fly for search.
                modifiedItem.BundleItems = (modifiedItem.BundleItems == null ? GetBundleItems(modifiedItem.GlobalId) : modifiedItem.BundleItems);
                modifiedItem.MarketCost = modifiedItem.WebPrice;
            }
            else if (modifiedItem.UseType == WOKItem.USE_TYPE_CUSTOM_DEED)
            {
                List<DeedItem> deedItems = GetDeedSalesItemList(modifiedItem.GlobalId, (int)modifiedItem.ItemCreatorId);

                // AP can only be removed if all items in the deed are not AP
                if (modifiedItem.PassTypeId != originalItem.PassTypeId && modifiedItem.PassTypeId == 0) // GENERAL pass
                {
                    foreach (DeedItem item in deedItems)
                    {
                        if (item.PassTypeId > 0)
                        {
                            modifiedItem.PassTypeId = originalItem.PassTypeId;
                            break;
                        }
                    }
                }

                // We store fully calculated price for deeds so the price does not have to be
                // calculated on the fly for search.
                SGFrameworkSettings fwSettings = GetDeedSGFrameworkSettings(modifiedItem.GlobalId);
                DeedPrice bp = CalculateInitialDeedPrice(deedItems, (int)modifiedItem.DesignerPrice, modifiedItem.ItemCreatorId, (fwSettings != null && fwSettings.FrameworkEnabled));
                modifiedItem.MarketCost = bp.TotalDeedWithCommission;
            }

            // Update the item itself
            shoppingDao.UpdateShopItem(modifiedItem, originalItem, modifyingUserId);

            // Update any associated bundles
            bundleChanges = UpdateAssociatedBundles(modifiedItem, originalItem);

            // Update any associated deeds
            deedChanges = UpdateAssociatedDeed(modifiedItem, originalItem);

            // Update associated wok stores
            if (modifiedItem.ItemActive != originalItem.ItemActive 
                && modifiedItem.ItemActive != (int)WOKItem.ItemActiveStates.Public)
            {
                DeleteFromStoreInventories(modifiedItem.GlobalId);
            }

            // Update Inventories
            if (modifiedItem.ItemActive == (int)WOKItem.ItemActiveStates.Deleted
                && removeFromInventories)
            {
                shoppingDao.DeleteItemFromPendingAdds(modifiedItem.GlobalId);
                shoppingDao.DeleteItemFromDynamicObjects(modifiedItem.GlobalId);
                shoppingDao.DeleteItemFromAllInventory(modifiedItem.GlobalId);
            }

            return 0;
        }

        /// <summary>
        /// UpdateCustomItem
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateCustomItem(WOKItem item)
        {
            if (item.DesignerPrice < 0)
            {
                item.DesignerPrice = 0;
            }

            return shoppingDao.UpdateCustomItem(item);
        }

        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int InsertIntoStoreInventories(int storeId, int itemId)
        {
            return shoppingDao.InsertIntoStoreInventories(storeId, itemId);
        }

        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteFromStoreInventories(int storeId, int itemId)
        {
            return shoppingDao.DeleteFromStoreInventories(storeId, itemId);
        }

        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteFromStoreInventories(int itemId)
        {
            return shoppingDao.DeleteFromStoreInventories(itemId);
        }

        /// <summary>
        /// DeleteCustomItemThumbnail
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int DeleteCustomItemThumbnail(int globalId)
        {
            return shoppingDao.DeleteCustomItemThumbnail(globalId);
        }

        /// <summary>
        /// GetItem
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public WOKItem GetItem(int globalId)
        {
            return GetItem(globalId, false);
        }

        /// <summary>
        /// GetItems
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<WOKItem> GetItems(IEnumerable<int> globalIds, bool bAllowUGCTemplate, string orderBy)
        {
            return shoppingDao.GetItems(globalIds, bAllowUGCTemplate, orderBy);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public WOKItem GetItem(int globalId, bool bAllowUGCTemplate, bool onlyPublic = true)
        {
            WOKItem item = shoppingDao.GetItem(globalId, bAllowUGCTemplate);

            // If Bundle, get all items in that bundle
            if (item != null && item.UseType == (int)WOKItem.USE_TYPE_BUNDLE)
            {
                item.BundleItems = shoppingDao.GetBundleItems (globalId, "", onlyPublic);
            }
            
            return item;
        }

        /// <summary>
        /// GetItemAnimations
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public List<WOKItem> GetItemAnimations (int globalId)
        {
            return  GetItemAnimations (globalId, "", false);
        }

        /// <summary>
        /// GetItemAnimations
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public List<WOKItem> GetItemAnimations (int globalId, string filter, bool showPrivate)
        {
            return shoppingDao.GetItemAnimations (globalId, filter, showPrivate);
        }
        
        /// <summary>
        /// GetAnimationActor
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public WOKItem GetAnimationActor (int globalId)
        {
            return shoppingDao.GetAnimationActor (globalId);
        }

        /// <summary>
        /// GetOwnerItems
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<WOKItem> GetOwnerItems(string searchString, int ownerId, UInt32 itemCategoryId, int pageSize, int pageNumber, string orderBy)
        {
            return shoppingDao.GetOwnerItems(searchString, ownerId, itemCategoryId, pageSize, pageNumber, orderBy);
        }

        /// <summary>
        /// GetOwnerItems
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<WOKItem> GetOwnerItems(string searchString, int ownerId, UInt32 itemCategoryId, int pageSize, int pageNumber, string orderBy, bool bIncludeUGCTemplate)
        {
            return shoppingDao.GetOwnerItems(searchString, ownerId, itemCategoryId, pageSize, pageNumber, orderBy, bIncludeUGCTemplate);
        }

        /// <summary>
        /// SearchItems
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<WOKItem> SearchItems (string searchString, int ownerId, uint itemCategoryId, int pageSize, int pageNumber, string orderBy, bool showRestricted, bool onlyAccessPass, int showPrivate)
        {
            return SearchItems (searchString, ownerId, new uint[] {itemCategoryId}, pageSize, pageNumber, orderBy, showRestricted, onlyAccessPass, showPrivate, false);
        }

        /// <summary>
        /// SearchItems
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<WOKItem> SearchItems(string searchString, int ownerId, UInt32[] itemCategoryIds, int pageSize, int pageNumber, string orderBy, bool showRestricted, bool onlyAccessPass, int showPrivate)
        {
            return SearchItems(searchString, ownerId, itemCategoryIds, pageSize, pageNumber, orderBy, showRestricted, onlyAccessPass, showPrivate, false);
        }

        /// <summary>
        /// SearchItems
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<WOKItem> SearchItems (string searchString, int ownerId, UInt32[] itemCategoryIds, int pageSize, int pageNumber, string orderBy, bool showRestricted, bool onlyAccessPass, int showPrivate, bool onlyAnimated)
        {
            return shoppingDao.SearchItems (searchString, ownerId, itemCategoryIds, pageSize, pageNumber, orderBy, showRestricted, onlyAccessPass, showPrivate, onlyAnimated);
        }

        /// <summary>
        /// SearchItems
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<WOKItem> GetPremiumItems (int gameId, int pageSize, int pageNumber, string orderBy, bool showPrivate)
        {
            return this.GetPremiumItems (gameId, pageSize, pageNumber, orderBy, showPrivate, false);
        }

        /// <summary>
        /// SearchItems
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<WOKItem> GetPremiumItems (int gameId, int pageSize, int pageNumber, string orderBy, bool showPrivate, bool showNonApproved)
        {
            return shoppingDao.GetPremiumItems (gameId, pageSize, pageNumber, orderBy, showPrivate, showNonApproved);
        }

        
        
        /// <summary>
        /// GetBundlesItemBelongsTo
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public List<WOKItem> GetBundlesItemBelongsTo (int itemId, string filter = "", bool onlyPublic = true)
        {
            return shoppingDao.GetBundlesItemBelongsTo (itemId, filter, onlyPublic);
        }

        /// <summary>
        /// GetBundleItems
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public List<WOKItem> GetBundleItems (int bundleId, string filter = "", bool onlyPublic = true)
        {
            return shoppingDao.GetBundleItems (bundleId, filter, onlyPublic);
        }

        
        /// <summary>
        /// AddItemToBundle
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddItemToBundle (int bundleGlobalId, int itemGlobalId, int quantity)
        {
            return shoppingDao.AddItemToBundle (bundleGlobalId, itemGlobalId, quantity);
        }

        /// <summary>
        /// RemoveItemsFromBundle
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int RemoveItemsFromBundle(int bundleGlobalId, List<int> itemGlobalIds)
        {
            return shoppingDao.RemoveItemsFromBundle(bundleGlobalId, itemGlobalIds);
        }

        /// <summary>
        /// CalculateBundlePrice
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public BundlePrice CalculateBundlePrice(IList<WOKItem> items, int commission, int bundleOwnerId)
        {
            int fullPrice = 0;
            int basePrice = 0;

            foreach (WOKItem item in items)
            {
                if (item.ItemCreatorId == bundleOwnerId)
                {
                    basePrice += item.MarketCost;
                }
                else
                {
                    fullPrice += item.WebPrice;
                }
            }

            BundlePrice bp = new BundlePrice();
            bp.TotalItemsNotOwned = fullPrice;
            bp.TotalItemsOwned = basePrice;
            bp.DesignCommission = commission;
            return bp;
        }

        /// <summary>
        /// GetDerivedItemsIncludedInBundles
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public List<WOKItem> GetDerivedItemsIncludedInBundles (int globalId, string filter = "")
        {
            return shoppingDao.GetDerivedItemsIncludedInBundles (globalId, filter);
        }

        /// <summary>
        /// GetBundleStoredWebPrice
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public int GetBundleStoredWebPrice (int globalId)
        {
            return shoppingDao.GetBundleStoredWebPrice (globalId);
        }
            
        /// <summary>
        /// GetCategoriesThatHaveItems - temporary
        /// works on assumption that production has no bottom level categories that don't have items associated
        /// with it
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<ItemCategory> GetCategoriesThatHaveItems(string filter)
        {
            return shoppingDao.GetCategoriesThatHaveItems(filter);
        }

		/// <summary>
		/// GetItemCategoryByName
		/// </summary>
		[DataObjectMethod(DataObjectMethodType.Select)]
		public ItemCategory GetItemCategory(string name)
		{
			return shoppingDao.GetItemCategoryByName(name);
		}

        /// <summary>
        /// GetItemCategoryByTree
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public ItemCategory GetItemCategory(string parentCategory, string subCategory)
        {
            return shoppingDao.GetItemCategoryByTree(parentCategory, subCategory);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<ItemCategory> GetItemCategories(IEnumerable<KeyValuePair<string, string>> categoryPairs)
        {
            List<ItemCategory> categories = new List<ItemCategory>();
            foreach(KeyValuePair<string, string> pair in categoryPairs)
            {
                ItemCategory category = shoppingDao.GetItemCategoryByTree(pair.Key, pair.Value);
                if (category != null && category.ItemCategoryId > 0)
                {
                    categories.Add(category);
                }
            }
            return categories;
        }

		/// <summary>
		/// GetItemCategoryParentByUploadType
		/// </summary>
		[DataObjectMethod(DataObjectMethodType.Select)]
		public ItemCategory GetItemCategoryParentByUploadType(int uploadType)
		{
			return shoppingDao.GetItemCategoryParentByUploadType(uploadType);
		}

		/// <summary>
		/// GetUploadTypeByItemCategoryId
		/// </summary>
		[DataObjectMethod(DataObjectMethodType.Select)]
		public int GetUploadTypeByItemCategoryId(int categoryId, int retry)
		{
			return shoppingDao.GetUploadTypeByItemCategoryId(categoryId, retry);
		}

        /// <summary>
        /// GetItemCategory
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public ItemCategory GetItemCategory(UInt32 categoryId)
        {
            return shoppingDao.GetItemCategory(categoryId);
        }


        /// <summary>
        /// Gets top level categories
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<ItemCategory> GetTopLevelCategories()
        {
            //top level is designated with a parent category id of 0
            return GetItemCategoriesByParentCategoryId (new UInt32[] { 0, 9999 });
        }

        /// <summary>
        /// Get Item Categories By Parent Category Id - gets all categories with the provided parent category
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<ItemCategory> GetItemCategoriesByParentCategoryId(UInt32 parentCategoryId)
        {
            return GetItemCategoriesByParentCategoryId (new UInt32[] { parentCategoryId });
        }

        public List<ItemCategory> GetItemCategoriesByParentCategoryId (UInt32[] parentCategoryIds)
        {
            return shoppingDao.GetItemCategoriesByParentCategoryId (parentCategoryIds);
        }


		/// <summary>
		/// Get Item Categories By UploadType
		/// </summary>
		[DataObjectMethod(DataObjectMethodType.Select)]
		public DataTable GetItemCatsByUploadType(uint uploadType)
		{
			return shoppingDao.GetItemCatsByUploadType(uploadType);
		}

		/// <summary>
		/// Get Item Categories By Use Type and Actor Group
		/// </summary>
		[DataObjectMethod(DataObjectMethodType.Select)]
		public List<ItemCategory> GetItemCatByUploadTypeActorGroup(uint uploadType, int actorGroup)
		{
			return shoppingDao.GetItemCatByUploadTypeActorGroup(uploadType, actorGroup);
		}

        /// <summary>
        /// GetItemCategories
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<ItemCategory> GetItemCategories(UInt32 itemCategoryId, string filter, int pageSize, int pageNumber, string orderBy)
        {
            return shoppingDao.GetItemCategories(itemCategoryId, filter, pageSize, pageNumber, orderBy);
        }

        /// <summary>
        /// Get Item Categories By ItemId - gets all categories an item is in
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<ItemCategory> GetItemCategoriesByItemId(int globalId)
        {
            return shoppingDao.GetItemCategoriesByItemId(globalId);
        }

        /// <summary>
        /// GetItemCategoriesByFilter
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<ItemCategory> GetItemCategoriesByFilter(string filter)
        {
            return shoppingDao.GetItemCategoriesByFilter(filter);
        }

        /// <summary>
        /// GetItemPromotions
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<WOKItem> GetItemPromotions(UInt32 itemCategoryId, int pageSize)
        {
            return shoppingDao.GetItemPromotions(itemCategoryId, pageSize);
        }

        /// <summary>
        /// 
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<ItemReview> GetItemReviews(int globalId, int pageSize, int pageNumber)
        {
            return shoppingDao.GetItemReviews(globalId, pageSize, pageNumber);
        }

        /// <summary>
        /// AddItemReview
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddItemReview(ItemReview itemReview)
        {
            return shoppingDao.AddItemReview(itemReview);
        }

        /// <summary>
        /// DeleteItemReview
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int DeleteItemReview(uint reviewId)
        {
            return shoppingDao.DeleteItemReview(reviewId);
        }

        /// <summary>
        /// GetRelatedItems
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<WOKItem> GetRelatedItems(WOKItem item, UInt32 itemCategoryId, int pageSize, int pageNumber, bool showRestricted)
        {
            return shoppingDao.GetRelatedItems(item, itemCategoryId, pageSize, pageNumber, showRestricted);
        }

        /// <summary>
        /// AddItemPurchase
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddItemPurchase(int globalId, int txnType, int userId, UInt32 itemOwnerId, int purchasePrice, UInt32 ownerPrice, string ipAddress, string keiPointId, int quantity, int parentPurchaseId)
        {
            return shoppingDao.AddItemPurchase(globalId, txnType, userId, itemOwnerId, purchasePrice, ownerPrice, ipAddress, keiPointId, quantity, parentPurchaseId);
        }

        /// <summary>
        /// GetWeeklySales
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetWeeklySales(int userId, ref int credits, ref int numberOfSales)
        {
            return shoppingDao.GetWeeklySales(userId, ref credits, ref numberOfSales);
        }

        /// <summary>
        /// GetItemPurchases
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<ItemPurchase> GetItemPurchases(int userId, int pageSize, int pageNumber)
        {
            return shoppingDao.GetItemPurchases(userId, pageSize, pageNumber);
        }

        /// <summary>
        /// InsertItemDigg
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertItemDigg(ItemDigg itemDigg)
        {
            return shoppingDao.InsertItemDigg(itemDigg);
        }

        /// <summary>
        /// DeleteCustomItemTexture
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteCustomItemTexture(int globalId, int userId, bool bRemoveFromAllInventory)
        {
            int result = 0;
            UserFacade userFacade = new UserFacade();

            // Make sure they are owner or admin
            WOKItem item = GetItem(globalId);

            if (!item.ItemCreatorId.Equals(Convert.ToUInt32 (userId)))
            {
                if (!userFacade.IsUserAdministrator())
                {
                    return -1;
                }
            }

            // Remove it from the website shopping site
            result = shoppingDao.UpdateItemTextureActiveState(globalId, (int) WOKItem.ItemActiveStates.Deleted);

            //try
            //{
            //    // Run within the context of a database transaction.
            //    using (TransactionDecorator transaction = new TransactionDecorator(TransactionScopeOption.Required))
            //    {
                    // Remove it from existing places completely?
                    if (bRemoveFromAllInventory)
                    {
                        // Per Jim this order and in a transaction
                        shoppingDao.DeleteItemFromPendingAdds(globalId);
                        shoppingDao.DeleteItemFromDynamicObjects(globalId);
                        shoppingDao.DeleteItemFromAllInventory(globalId);
                    }

            //        transaction.Complete();
            //    }
            //}
            //catch (TransactionAbortedException tae)
            //{
                
            //}
            //catch (Exception exc)
            //{

            //}


            
            return result;
        }

        public int CreateItemTemplate(WOKItem item)
        {
            if (item.DesignerPrice < 0)
            {
                item.DesignerPrice = 0;
            }

            return shoppingDao.CreateItemTemplate(item);
        }

        /// <summary>
        /// GetOriginalItem - Get original item which a derived item is based on
        /// </summary>
        public int GetBaseItemGlobalId(int globalId)
        {
            return shoppingDao.GetBaseItemGlobalId(globalId);
        }

		/// <summary>
		/// AddItemPath - store relative path of item data file into wok.item_paths table
		/// </summary>
		public void AddItemPath(ItemPath itemPath)
		{
			shoppingDao.AddItemPath(itemPath);
		}

		/// <summary>
		/// GetItemPaths - retrieve a list of relative paths from wok.item_paths table
		/// </summary>
		public List<ItemPath> GetItemPaths(int globalId)
		{
			return shoppingDao.GetItemPaths(globalId);
		}

		/// <summary>
        /// AddItemPathTexture - store texture metadata in wok.item_path_textures table
		/// </summary>
		public void AddItemPathTexture(ItemPathTexture itemPathTex)
		{
            shoppingDao.AddItemPathTexture(itemPathTex);
		}

        /// <summary>
        /// AddItemPathFileStore - add record for per-item file store override for unique asset stores
        /// </summary>
        public void AddItemPathFileStore(int globalId, ItemPathType pathTypeInfo, string filestore)
        {
            shoppingDao.AddItemPathFileStore(globalId, pathTypeInfo, filestore);
        }

        /// <summary>
        /// GetItemPathFileStore - get record for per-item file store override for unique asset stores
        /// </summary>
        public string GetItemPathFileStore(int globalId, ItemPathType pathTypeInfo)
        {
            return shoppingDao.GetItemPathFileStore(globalId, pathTypeInfo);
        }

        /// <summary>
        /// GetItemPathType - select record in item_path_type table
        /// </summary>
        public ItemPathType GetItemPathType(string pathType)
        {
            return shoppingDao.GetItemPathType(pathType);
        }

        /// <summary>
        /// GetExistingUniqueAssetIdBySHA256 - return existing asset ID for matching hash. Return 0 if not found.
        /// </summary>
        public uint GetExistingUniqueAssetIdBySHA256(ItemPathType pathTypeInfo, byte[] hashSHA256)
        {
            return shoppingDao.GetExistingUniqueAssetIdBySHA256(pathTypeInfo, hashSHA256);
        }

        /// <summary>
        /// AllocUniqueAssetIdBySHA256 - allocate unique asset ID if hash is new, otherwise return existing asset ID
        /// </summary>
        public uint AllocUniqueAssetIdBySHA256(ItemPathType pathTypeInfo, byte[] hashSHA256, int size, int compressedSize, out bool isNew)
        {
            return shoppingDao.AllocUniqueAssetIdBySHA256(pathTypeInfo, hashSHA256, size, compressedSize, out isNew);
        }

        /// <summary>
		/// GetItemParameter - select record in item parameter table
		/// </summary>
		public ItemParameter GetItemParameter(int globalId, int paramTypeId)
		{
			return shoppingDao.GetItemParameter(globalId, paramTypeId);
		}

		/// <summary>
		/// UpdateItemParameter - add or update record in item parameter table
		/// </summary>
		public void UpdateItemParameter( int globalId, int paramTypeId, string value )
		{
			shoppingDao.UpdateItemParameter(globalId, paramTypeId, value);
		}

        /// <summary>
        /// AddItemToGame - associates an item to a game
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public void AddItemToGame (int globalId, int gameId, int approvalStatus)
        {
            shoppingDao.AddItemToGame(globalId, gameId, approvalStatus);
        }

        /// <summary>
        /// InsertPremiumItemCreditRedemption 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int InsertPremiumItemCreditRedemption (int purchaseId, int communityId)
        {
            return shoppingDao.InsertPremiumItemCreditRedemption (purchaseId, communityId);
        }

        /// <summary>
        /// DeletePremiumItem 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Delete)]
        public int DeletePremiumItem (int globalId)
        {
            return shoppingDao.DeletePremiumItem (globalId);
        }

        /// <summary>
        /// UpdatePremiumItem 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int UpdatePremiumItem (WOKItem item)
        {
            int rc = shoppingDao.UpdateCustomItem (item);
            if (rc > 0)
            {
                // Now update the approval status change
                rc = shoppingDao.UpdatePremiumItemApprovalStatus (item);
            }

            return rc;
        }

        /// <summary>
        /// UpdatePremiumItemApprovalStatus 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int UpdatePremiumItemApprovalStatus (WOKItem item)
        {
            return shoppingDao.UpdatePremiumItemApprovalStatus (item);
        }

        /// <summary>
        /// UpdateItemDeniedReason 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int UpdateItemDeniedReason (int globalId, string desc)
        {
            return shoppingDao.UpdateItemDeniedReason (globalId, desc);
        }
        
        /// <summary>
        /// GetItemDeniedReason 
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public string GetItemDeniedReason (int globalId)
        {
            return shoppingDao.GetItemDeniedReason (globalId);
        }

        /// <summary>
        /// GetItemAnimations
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public DataTable GetItemAnimations (int globalId, bool showPrivate)
        {
            return shoppingDao.GetItemAnimations (globalId, showPrivate);
        }

        /// <summary>
        /// GetAnimations
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public DataTable GetAnimations (string globalIds)
        {
            return shoppingDao.GetAnimations (globalIds);
        }

        /// <summary>
        /// GetPasses
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetPasses(string globalIds)
        {
            return shoppingDao.GetPasses(globalIds);
        }

        /// <summary>
        /// GetExclusionGroupIds
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetExclusionGroupIds(string globalIds)
        {
            return shoppingDao.GetExclusionGroupIds(globalIds);
        }

        /// <summary>
        /// GetSpaceDynamicObjectParams
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public DataTable GetSpaceDynamicObjectParams (string ids)
        {
            return shoppingDao.GetSpaceDynamicObjectParams (ids);
        }

        /// <summary>
        /// CreateCustomZoneTempate
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public bool CreateCustomZoneTemplate(int zoneIndex, int zoneInstanceId, int templateGlobalId, int userId)
        {
            bool success = false;

            ZoneIndex zIndexObj = new ZoneIndex(zoneIndex);
            int zoneType = zIndexObj.ZoneType;

            ScriptGameItemFacade sgFacade = new ScriptGameItemFacade();
            GameFacade gFacade = new GameFacade();

            // Step 1: Make sure all game items in the deed have a snapshot.
            bool sgSnapshotOk = sgFacade.SnapshotWorldScriptGameitems(userId, zoneInstanceId, zoneType);

            if (sgSnapshotOk)
            {
                // Step 2: Process framework settings.
                bool sgFrameworkSettingsOk = gFacade.AddSGFrameworkSettingsToCustomDeed(zoneInstanceId, zoneType, templateGlobalId);

                if (sgFrameworkSettingsOk)
                {
                    // Step 3: Process custom data.
                    bool sgCustomDataOk = gFacade.AddSGCustomDataToCustomDeed(zoneInstanceId, zoneType, templateGlobalId);

                    if (sgCustomDataOk)
                    {
                        // Step 4: Add all the game items to the template.
                        bool sgItemsOk = sgFacade.AddWorldScriptGameItemsToCustomDeed(zoneInstanceId, zoneType, templateGlobalId, userId);

                        if (sgItemsOk)
                        {
                            // Step 5: Process dynamic objects, parameters, and playlists.
                            bool worldDOsOk = shoppingDao.AddWorldDOsToCustomDeed(zoneIndex, zoneInstanceId, templateGlobalId);

                            if (worldDOsOk)
                            {
                                // Step 6: Process world object player settings.
                                success = shoppingDao.AddWOPSettingsToCustomDeed(zoneIndex, zoneInstanceId, templateGlobalId);
                            }
                        }
                    }
                }
            }

            if (!success)
            {
                // Cleanup any inserted records if we encounter failure.
                shoppingDao.DeleteAllWOPSettingsFromCustomDeed(templateGlobalId);
                shoppingDao.DeleteAllDOsFromCustomDeed(templateGlobalId);
                sgFacade.DeleteAllSGItemsInCustomDeed(templateGlobalId);
                gFacade.DeleteSGCustomDataFromCustomDeed(templateGlobalId);
                gFacade.DeleteSGFrameworkSettingsFromCustomDeed(templateGlobalId);
            }

            return success;
        }

        /// <summary>
        /// ChangeZoneMap
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int ChangeZoneMap (int oldZoneIndex, int zoneInstanceId, int newZoneIndex, string playerName, int templateId,
            string gameNameNoSpaces, ref int result, ref string url)
        {
            return shoppingDao.ChangeZoneMap(oldZoneIndex, zoneInstanceId, newZoneIndex, playerName, templateId,
                gameNameNoSpaces, ref result, ref url);
        }

        /// <summary>
        /// ImportZone
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int ImportZone (int playerId, int oldZoneIndex, int oldZoneInstanceId, int baseZoneIndexPlain,
            int srcZoneIndex, int srcZoneInstanceId, bool maintainScripts, ref int result, ref int newZoneIndex)
        {
            return shoppingDao.ImportZone (playerId, oldZoneIndex, oldZoneInstanceId, baseZoneIndexPlain,
                srcZoneIndex, srcZoneInstanceId, maintainScripts, ref result, ref newZoneIndex);
        }

        /// <summary>
        /// Returns the zoneindex of a wok zone. Replaces WOKStoreUtility.MakeZoneIndex().
        /// </summary>
        /// <returns></returns>
        public int MakeZoneIndex(int zoneIndexPlain, int zoneType)
        {
            return ((zoneType << 28) | zoneIndexPlain);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertChannelZone(int userId, int zoneIndex, int instanceId, string name, int serverId, string tiedToServer, string country, int age, int scriptServerId)
        {
            int communityId = 0;
            ZoneIndex zIndexObj = new ZoneIndex(zoneIndex);
            int zoneType = zIndexObj.ZoneType;

            if (zoneType == (int)WOK3DPlace.eZoneType.HANGOUT)
            {
                communityId = instanceId;
            }
            else if (zoneType == (int)WOK3DPlace.eZoneType.HOUSING)
            {
                communityId = (new UserFacade()).GetUserHomeCommunityIdByUserId(userId);
            }

            return shoppingDao.InsertChannelZone(userId, communityId, zoneIndex, instanceId, zoneType, name, serverId, tiedToServer, country, age, scriptServerId);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataRow GetItemByPlacementId(int objPlacementId, int zoneInstanceId, int playerId)
        {
            return shoppingDao.GetItemByPlacementId(objPlacementId, zoneInstanceId, playerId);
        }

        #endregion

        #region Custom Deeds and Deed Items

        /// <summary>
        /// GetCustomDeedUsage
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataRow GetCustomDeedUsage(int deedId)
        {
            return shoppingDao.GetCustomDeedUsage(deedId);
        }

        /// <summary>
        /// IsOriginalUGCZone
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool IsOriginalUGCZone(int zoneIndex, int zoneInstanceId)
        {
            return shoppingDao.IsOriginalUGCZone(zoneIndex, zoneInstanceId);
        }

        /// <summary>
        /// GetDeedTryOnZone
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public WOK3DPlace GetDeedTryOnZone(int deedTemplateId)
        {
            return shoppingDao.GetDeedTryOnZone(deedTemplateId);
        }

        /// <summary>
        /// GetCommunityIdFromDeedTemplateId
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetCommunityIdFromDeedTemplateId(int deedTemplateId)
        {
            return shoppingDao.GetCommunityIdFromDeedTemplateId(deedTemplateId);
        }

        /// <summary>
        /// RemoveItemsFromDeed
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int RemoveItemsFromDeed(int deedTemplateId, List<int> itemGlobalIds)
        {
            return shoppingDao.RemoveItemsFromDeed(deedTemplateId, itemGlobalIds);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<DeedItem> GetDeedSGIDeedItems(int deedOwnerId, int deedTemplateId)
        {
            // Helper mapping of game item glids to their associated WOKItems.
            Dictionary<int, WOKItem> giGlidsToWOKItems = new Dictionary<int, WOKItem>();

            // Grab all of the deed's game items.
            ScriptGameItemFacade sgFacade = new ScriptGameItemFacade();
            PagedList<ScriptGameItem> sgItems = sgFacade.GetDeedScriptGameItems(deedTemplateId, string.Empty, null, string.Empty, 1, Int32.MaxValue, true, true);

            // If no game items were found, we're done.
            if (sgItems.TotalCount == 0)
            {
                return new List<DeedItem>();
            }

            // Grab WOKItems for the game items.
            List<WOKItem> snapshotSGItems = GetItems(sgItems.Select(gi => gi.GIGlid), false, string.Empty);
            foreach (WOKItem wokItem in snapshotSGItems)
            {
                giGlidsToWOKItems.Add(wokItem.GlobalId, wokItem);
            }

            // Grab WOKItems for each of the bundled items.
            Dictionary<int, List<int>> bundleGlidMap = sgFacade.MapBundledGlidsToGIGLIDs(sgItems);
            List<WOKItem> bundleItems = GetItems(bundleGlidMap.Keys, false, string.Empty);

            foreach (WOKItem bundleItem in bundleItems)
            {
                List<int> gIGlids = bundleGlidMap[bundleItem.GlobalId];
                foreach (int gIGlid in gIGlids)
                {
                    if (giGlidsToWOKItems[gIGlid].BundleItems == null)
                    {
                        giGlidsToWOKItems[gIGlid].BundleItems = new List<WOKItem>();
                    }

                    giGlidsToWOKItems[gIGlid].BundleItems.Add(bundleItem);
                }
            }

            return giGlidsToWOKItems.Values.Select(wi => new DeedItem(deedOwnerId, wi)).ToList();
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<DeedItem> GetWorldSGIDeedItems(int worldOwnerId, int zoneIndex, int zoneInstanceId)
        {
            // Helper mapping of game item ids to their associated WOKItems.
            Dictionary<int, WOKItem> gIIdsToWOKItems = new Dictionary<int,WOKItem>();

            ZoneIndex zIndexObj = new ZoneIndex(zoneIndex);
            int zoneType = zIndexObj.ZoneType;

            // Don't pull game items if the framework is not enabled.
            SGFrameworkSettings settings = (new GameFacade()).GetWorldSGFrameworkSettings(zoneInstanceId, zoneType);
            if (settings == null || !settings.FrameworkEnabled)
            {
                return new List<DeedItem>();
            }

            // Grab all of the world's game items.
            ScriptGameItemFacade sgFacade = new ScriptGameItemFacade();
            PagedList<ScriptGameItem> sgItems = sgFacade.GetWorldScriptGameItems(zoneInstanceId, zoneType, string.Empty, 1, Int32.MaxValue, true, true);

            // If no game items were found, we're done.
            if (sgItems.TotalCount == 0)
            {
                return new List<DeedItem>();
            }

            // Grab WOKItems for the game items that already have a snapshot.
            if (sgItems.Where(sgi => sgi.GIGlid != 0).Count() > 0)
            {
                ILookup<int, ScriptGameItem> giGlidsToGIIds = sgItems.Where(sg => sg.GIGlid != 0).ToLookup(sg => sg.GIGlid);

                List<WOKItem> snapshotSGItems = GetItems(giGlidsToGIIds.Select(l => l.Key), false, string.Empty);
                foreach (WOKItem wokItem in snapshotSGItems)
                {
                    foreach(ScriptGameItem gameItem in giGlidsToGIIds[wokItem.GlobalId])
                    {
                        gIIdsToWOKItems.Add(gameItem.GIId, wokItem);
                    }
                }
            }

            // Add placeholder WOKItems for game items that do not have a snapshot.
            foreach (ScriptGameItem sgItem in sgItems)
            {
                if (sgItem.GIGlid == 0)
                {
                    string sOut;
                    WOKItem wokItem = new WOKItem
                    {
                        GlobalId = sgItem.GIGlid,
                        Name = sgItem.Name,
                        Description = (sgItem.Properties.TryGetValue("description", out sOut) ? sOut : ""),
                        UseType = WOKItem.USE_TYPE_SCRIPT_GAME_ITEM,
                        ItemCreatorId = (uint)worldOwnerId,
                        IsDerivable = 0,
                        DerivationLevel = -1,
                        PassTypeId = 0,
                        InventoryType = BundleItem.CURRENCY_TYPE_BOTH_R_C,
                        ItemActive = (int)WOKItem.ItemActiveStates.Public
                    };

                    gIIdsToWOKItems.Add(sgItem.GIId, wokItem);
                }
            }
            
            // Grab WOKItems for each of the bundled items.
            Dictionary<int, List<int>> bundleGlidMap = sgFacade.MapBundledGlidsToGIIds(sgItems);
            List<WOKItem> bundleItems = GetItems(bundleGlidMap.Keys, false, string.Empty);

            foreach (WOKItem bundleItem in bundleItems)
            {
                List<int> gIIds = bundleGlidMap[bundleItem.GlobalId];
                foreach (int gIId in gIIds)
                {
                    if (gIIdsToWOKItems[gIId].BundleItems == null)
                    {
                        gIIdsToWOKItems[gIId].BundleItems = new List<WOKItem>();
                    }

                    gIIdsToWOKItems[gIId].BundleItems.Add(bundleItem);
                }
            }

            return gIIdsToWOKItems.Values.Select(wi => new DeedItem(worldOwnerId, wi)).ToList();
        }

        /// <summary>
        /// Returns the list of all DeedItems to be included when a deed is made from a World.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<DeedItem> GetInitialDeedItemList(int worldOwnerId, int zoneIndex, int zoneInstanceId)
        {
            // Grab game items, dynamic objects, parameters, and animations.
            List<DeedItem> deedItems = new List<DeedItem>();
            deedItems.AddRange(shoppingDao.GetWorldDODeedItems(zoneIndex, zoneInstanceId, worldOwnerId));
            deedItems.AddRange(shoppingDao.GetWorldDOPDeedItems(zoneIndex, zoneInstanceId, worldOwnerId));
            deedItems.AddRange(GetWorldSGIDeedItems(worldOwnerId, zoneIndex, zoneInstanceId));         

            return deedItems;
        }

        /// <summary>
        /// Returns the list of all DeedItems included within a deed.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<DeedItem> GetDeedSalesItemList(int deedTemplateId, int deedOwnerId)
        {
            List<DeedItem> deedItems = new List<DeedItem>();
            deedItems.AddRange(shoppingDao.GetDODeedItems(deedTemplateId, deedOwnerId));
            deedItems.AddRange(shoppingDao.GetDOPDeedItems(deedTemplateId, deedOwnerId));
            deedItems.AddRange(GetDeedSGIDeedItems(deedOwnerId, deedTemplateId));

            return deedItems;
        }

        /// <summary>
        /// GetDeedsItemBelongsTo
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<WOKItem> GetDeedsItemBelongsTo(int itemId, string filter, bool onlyPublic = true)
        {
            return shoppingDao.GetDeedsItemBelongsTo(itemId, filter, onlyPublic);
        }

        /// <summary>
        /// GetDerivedItemsIncludedInDeeds
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<WOKItem> GetDerivedItemsIncludedInDeeds(int globalId)
        {
            return shoppingDao.GetDerivedItemsIncludedInDeeds(globalId);
        }

        /// <summary>
        /// CalculateInitialDeedPrice
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DeedPrice CalculateInitialDeedPrice(List<DeedItem> deedItems, int commissionPrice, UInt32 deedOwnerId, bool frameworkEnabled)
        {
            int fullPrice = 0;
            int basePrice = 0;
            HashSet<int> usedGlids = new HashSet<int>();

            foreach (DeedItem deedItem in deedItems)
            {
                // We only want to process each item once. Game items are a bit different, as they may not have a GlobalId at
                // the time of initial calculation.
                if (!usedGlids.Contains(deedItem.GlobalId) || deedItem.UseType == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM)
                {
                    usedGlids.Add(deedItem.GlobalId);
                    fullPrice += (int)deedItem.DeedItemPrice;

                    // Game items may have bundled glids that we need to process.
                    if (deedItem.UseType == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM)
                    {
                        foreach (DeedItem bundledItem in deedItem.BundleItems)
                        {
                            if (!usedGlids.Contains(bundledItem.GlobalId))
                            {
                                usedGlids.Add(bundledItem.GlobalId);
                                fullPrice += (int)bundledItem.DeedItemPrice;
                            }
                        }
                    }
                }
            }

            // Per billy base price is fixed
            if (frameworkEnabled)
            {
                basePrice = Configuration.BaseGameDeedPrice;
            }
            else
            {
                basePrice = Configuration.BaseDeedPrice;
            }

            DeedPrice bp = new DeedPrice();
            bp.TotalItemsNotOwned = fullPrice;
            bp.TotalItemsOwned = basePrice;
            bp.DesignCommission = commissionPrice;
            return bp;
        }

        #endregion

        #region Category Management

        /// <summary>
        /// AddCategoryToItem
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<WOKItem> GetWOKItemsList()
        {
            return shoppingDao.GetWOKItemsList();
        }

        /// <summary>
        /// InsertWOKCategory
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertWOKCategory(string category, int parentCategoryId, string description, int userId, string marketingPath)
        {
            return shoppingDao.InsertWOKCategory( category, parentCategoryId, description, userId, marketingPath);
        }

        /// <summary>
        /// AddCategoryToItem
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateWOKCategory(int categoryId, string category, int parentCategoryId, string description, int userId, string marketingPath)
        {
            return shoppingDao.UpdateWOKCategory(categoryId, category, parentCategoryId, description, userId, marketingPath);
        }

        /// <summary>
        /// AddCategoryToItem
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddCategoryToItem(int globalId, uint ItemCategoryId, int categoryIndex)
        {
            return shoppingDao.AddCategoryToItem(globalId, ItemCategoryId, categoryIndex);
        }

        /// <summary>
        /// AddCategoryToItem
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateCategorysParent(int categoryId, int parentCategoryId)
        {
            return shoppingDao.UpdateCategorysParent(categoryId, parentCategoryId);
        }

        /// <summary>
        /// DeleteCategoryFromItem
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteCategoryFromItem(int globalId, int categoryIndex)
        {
            return shoppingDao.DeleteCategoryFromItem(globalId, categoryIndex);
        }

        /// <summary>
        /// DeleteCategory
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteCategory(int categoryId)
        {
            return shoppingDao.DeleteCategory(categoryId);
        }

        /// <summary>
        /// DeleteItemategory
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteItemCategory(int categoryId, int parentCategoryId)
        {
            //find all occurances of this category
            //update all categories having this deleted one as their parent to have the delete items parent as their new parent
            UpdateCategorysParent(categoryId, parentCategoryId);

            //move all items that might be under that category to the parent category
            //if category being removed is a toplevel remove all the items under it.
            if (parentCategoryId == 0)
            {
                shoppingDao.DeleteWOKCategoryItems(categoryId);
            }
            else
            {
                //pull back all items and attempt update individually to avoid group failure due to duplicate keys
                DataTable categoryItems = GetWOKCategoryItemsByCategoryId(categoryId);
                foreach (DataRow row in categoryItems.Rows)
                {
                    //catch exception - you want it to keep processing regardless of failures
                    try
                    {
                        shoppingDao.MoveWOKCategoryItem(categoryId, parentCategoryId, Convert.ToInt32(row["global_id"]));
                    }
                    catch (Exception) { }
                }
            }
            //once complete delete from item_categories table
           return DeleteCategory(categoryId);
        }

        //for now these will be datatables since they do not return an actual object
        //will look into another option for these later.
        public DataTable GetWOKCategoryItemsByCategoryId(int categoryId)
        {
            return shoppingDao.GetWOKCategoryItemsByCategoryId(categoryId);
        }

        //for now these will be datatables since they do not return an actual object
        //will look into another option for these later.
        public DataTable GetWOKCategoryItems(int itemID, int categoryId)
        {
            return shoppingDao.GetWOKCategoryItems(itemID,categoryId);
        }

        /// <summary>
        /// RecursiveDeleteParentWOKCategory
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public void RecursiveDeleteParentWOKCategory(int categoryId, int parentCatId)
        {
            //find all children of this category
            List<ItemCategory> childCategories = GetItemCategoriesByParentCategoryId((uint)categoryId);

            //process the removal of this category
            DeleteItemCategory(categoryId, parentCatId);

            //recursively process child categories now parent categories
            foreach (ItemCategory item in childCategories)
            {
                RecursiveDeleteParentWOKCategory((int)item.ItemCategoryId, 0);
            }
        }

        /// <summary>
        /// GetItemTemplates
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<ItemTemplate> GetItemTemplates()
        {
            return shoppingDao.GetItemTemplates();
        }

        /// <summary>
        /// GetItems for sync, if only globalId non-zero it gets only that item
        /// otherwise it gets all items greater than each id
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public DataTable GetItems(int globalId, int ugcId, int templateId, int count )
        {
            return shoppingDao.GetItems(globalId, ugcId, templateId, count );
        }


        /// <summary>
        /// get space based on the playerId, zoneIndex, instanceId
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public DataTable GetSpaceDynamicObjects(int playerId, int zoneIndex, int instanceId)
        {
            return shoppingDao.GetSpaceDynamicObjects( playerId, zoneIndex, instanceId);
        }

        /// <summary>
        /// get space world object settings based on the playerId, zoneIndex, instanceId
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public DataTable GetSpaceWorldObjectSettings(int playerId, int zoneIndex, int instanceId)
        {
            return shoppingDao.GetSpaceWorldObjectSettings(playerId, zoneIndex, instanceId);
        }
    
       #endregion

        [DataObjectMethod(DataObjectMethodType.Select)]
        public IEnumerable<KeyValuePair<int, int>> CopyGlids(IEnumerable<int> glidsToCopy)
        {
            List<KeyValuePair<int, int>> glidPairs = new List<KeyValuePair<int, int>>();

            foreach(int originalGlid in glidsToCopy)
            {
                int newGlid = shoppingDao.CopyGlid(originalGlid);
                glidPairs.Add(new KeyValuePair<int, int>(originalGlid, newGlid));
            }

            return glidPairs;
        }

        /// <summary>
        /// Get UGC template info
        /// </summary>
        /// <returns>true if succeeded. false if no template.</returns>
        public bool GetUGCTemplateInfo(WOKItem item, out UInt32 templateCreatorId, out UInt32 commission)
        {
            templateCreatorId = 0;
            commission = 0;

            // obtain template info (if any)
            WOKItem baseItem = null;
            if (item.BaseGlobalId != 0)
                baseItem = GetItem(Convert.ToInt32(item.BaseGlobalId), true);

            if (baseItem != null && !baseItem.IsKanevaOwned)    // has template and not designed by Kaneva
            {
                templateCreatorId = baseItem.ItemCreatorId;
                commission = baseItem.DesignerPrice;
                return true;
            }

            return false;
        }

        #region Item Preloads

        /// <summary>
        /// CreateItemPreloadList - create a new record in item_preloads table and return list ID. Return value is -1 if creation failed.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int CreateItemPreloadList(string name)
        {
            return shoppingDao.CreateItemPreloadList(name);
        }

        /// <summary>
        /// UpdateItemPreloadList
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public bool UpdateItemPreloadList(int listId, string newName)
        {
            return shoppingDao.UpdateItemPreloadList(listId, newName);
        }

        /// <summary>
        /// PublishItemPreloadList - update a existing item_preloads record and assign it with a valid version number. Return value is -1 if assignment failed.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int PublishItemPreloadList(int listId)
        {
            return shoppingDao.PublishItemPreloadList(listId);
        }

        /// <summary>
        /// GetAllItemPreloadLists()
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetAllItemPreloadLists()
        {
            return shoppingDao.GetAllItemPreloadLists();
        }

        /// <summary>
        /// GetItemPreloadList
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataRow GetItemPreloadList(int listId)
        {
            return shoppingDao.GetItemPreloadList(listId);
        }

        /// <summary>
        /// GetLatestItemPreloadListVersion
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetLatestItemPreloadListVersion()
        {
            return shoppingDao.GetLatestItemPreloadListVersion();
        }

        /// <summary>
        /// AddItemPreloadListItem - add a new global ID to a preload list. Throws one of three possible exceptions: listId is invalid, globalId already exists in the list or globalId is invalid.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public void AddItemPreloadListItem(int listId, int globalId)
        {
            shoppingDao.AddItemPreloadListItem(listId, globalId);
        }

        /// <summary>
        /// RemoveItemPreloadListItem - remove a new global ID from a preload list. Throws one of three possible exceptions: listId is invalid, globalId does not exists in the list or globalId is invalid.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public void RemoveItemPreloadListItem(int listId, int globalId)
        {
            shoppingDao.RemoveItemPreloadListItem(listId, globalId);
        }

        /// <summary>
        /// MergeItemPreloadListItems - merge all items from another list
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int MergeItemPreloadListItems(int dstListId, int srcListId)
        {
            return shoppingDao.MergeItemPreloadListItems(dstListId, srcListId);
        }

        /// <summary>
        /// GetItemPreloadListItems()
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetItemPreloadListItems(int listId)
        {
            return shoppingDao.GetItemPreloadListItems(listId);
        }

        #endregion
    }
}
