///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Net;
using System.Xml;
using System.Linq;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;

using log4net;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace Kaneva.BusinessLayer.Facade
{

    [DataObject(true)]
    public class GameFacade
    {
        private IGameDao gameDao = DataAccess.GameDao;
        private IGameServerDao gameServerDao = DataAccess.GameServerDao;
        private IGameLicenseDao gameLicenseDao = DataAccess.GameLicenseDao;
        private IWorldTemplateDao worldTemplateDao = DataAccess.WorldTemplateDao;
        private IShoppingDao shoppingDao = DataAccess.ShoppingDao;
        private IUserDao userDao = DataAccess.UserDao;
        private ISGCustomDataDao sgCustomDataDao = DataAccess.SGCustomDataDao;
        private ISGFrameworkSettingsDao sgFrameworkSettingsDao = DataAccess.SGFrameworkSettingsDao;
        private IExperimentDao experimentDao = DataAccess.ExperimentDao;
        private IEventDao eventDao = DataAccess.EventDao;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // ***********************************************
        // Game Creation functions
        // ***********************************************
        #region Game Functions
        /// <summary>
        /// Gets all Games by an owner
        /// </summary>
        /// <param name="ownerId"></param>
        /// <returns>IList</returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<Game> GetGamesByOwner(int ownerId, string filter, string orderby)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameDao.GetGamesByOwner(ownerId, filter, orderby);
        }

        /// <summary>
        /// Gets all Games by an owner
        /// </summary>
        /// <param name="ownerId"></param>
        /// <returns>PagedDataTable</returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedDataTable GetGamesByOwner(int ownerId, string filter, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameDao.GetGamesByOwner(ownerId, filter, orderby, pageNumber, pageSize);
        }


        /// <summary>
        /// Gets all Games matching certain search criteria
        /// </summary>
        /// <param name="ownerId"></param>
        /// <returns>PagedDataTable</returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedDataTable SearchForGames(string searchString, string filter, string categories, bool bGetMature, bool bThumbnailRequired, int pastDays, int ownerId, string orderby, int pageNumber, int pageSize)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameDao.SearchForGames(searchString, filter, categories, bGetMature, bThumbnailRequired, pastDays, ownerId, orderby, pageNumber, pageSize);
        }

        public PagedList<Community> SearchWorldPrepopulation(string worldStartsWith, string orderby, int pageNumber, int pageSize)
        {
            return gameDao.SearchWorldPrepopulation(worldStartsWith, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Gets all apartments matching certain search criteria
        /// </summary>
        /// <returns>PagedDataTable</returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable SearchApartments(bool bHasAPSubscription, bool bShowOnlyAP, Int32 pageNumber, Int32 pageSize, String searchString, String orderBy, ref int totalCount)
        {
            return gameDao.SearchApartments(bHasAPSubscription, bShowOnlyAP, pageNumber, pageSize, searchString, orderBy, ref totalCount);
        }

        /// <summary>
        /// Gets all hangouts matching certain search criteria
        /// </summary>
        /// <returns>PagedDataTable</returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable SearchHangouts(bool bHasAPSubscription, bool bShowOnlyAP, bool bOnlyPopulated, bool bOnlyEmptyPopulation, Int32 pageNumber, Int32 pageSize, String searchString, int placeType, bool myCommunitiesOnly, int userId, String orderBy, int wokGameId, string country, int minAge, int maxAge, ref int totalCount)
        {
            return gameDao.SearchHangouts(bHasAPSubscription, bShowOnlyAP, bOnlyPopulated, bOnlyEmptyPopulation, pageNumber, pageSize, searchString, placeType, myCommunitiesOnly, userId, orderBy, wokGameId, country, minAge, maxAge, ref totalCount);
        }

        /// <summary>
        /// Gets all 3dapps matching certain search criteria
        /// </summary>
        /// <returns>PagedDataTable</returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable Search3DApps(bool onlyAccessPass, bool bGetMature, string searchString,
            bool bOnlyWithPhotos, string country, int[] communityTypeIds,
            string orderBy, int pageNumber, int pageSize, int iPlaceTypeId, bool myCommunitiesOnly, bool onlyOwned, int userId, int wokGameId, ref int totalNumRecords)
        {
            return gameDao.Search3DApps(onlyAccessPass, bGetMature, searchString,
            bOnlyWithPhotos, country, communityTypeIds,
            orderBy, pageNumber, pageSize, iPlaceTypeId, myCommunitiesOnly, onlyOwned, userId, wokGameId, ref totalNumRecords);
        }

        public DataTable GetUserHomeWorldForSearch(int userId, int wokGameId, ref int totalNumRecords)
        {
            return gameDao.GetUserHomeWorldForSearch(userId, wokGameId, ref totalNumRecords);
        }

        public DataTable GetMostVisited3DApps(bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId)
        {
            return gameDao.GetMostVisited3DApps(ShowOnlyAP, HideAP, ref totalNumRecords, pageNumber, pageSize, wokGameId);
        }

        public DataTable GetMostPopulated3DApps(bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId)
        {
            return gameDao.GetMostPopulated3DApps(ShowOnlyAP, HideAP, ref totalNumRecords, pageNumber, pageSize, wokGameId);
        }

        public DataTable GetMostRaved3DApps(bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId)
        {
            return gameDao.GetMostRaved3DApps(ShowOnlyAP, HideAP, ref totalNumRecords, pageNumber, pageSize, wokGameId);
        }

        public DataTable Browse3DAppsByRequestB(bool ShowOnlyAP, int userId, ref int totalNumRecords, int pageNumber, int PageSize, int wokGameId)
        {
            return gameDao.Browse3DAppsByRequestB(ShowOnlyAP, userId, ref totalNumRecords, pageNumber, PageSize, wokGameId);
        }

        public DataTable Browse3DAppsByRequest(bool ShowOnlyAP, bool HideAP, int userId, ref int totalNumRecords, int pageNumber, int PageSize, int wokGameId)
        {
            return gameDao.Browse3DAppsByRequest(ShowOnlyAP, HideAP, userId, ref totalNumRecords, pageNumber, PageSize, wokGameId);
        }

        public DataTable MostPopulatedCombined(bool ShowOnlyAP, bool HideAP, int userId, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId, string countryFilterBy, string countrySortBy, int ageFilterBy, int ageSortBy, int offset)
        {
            return gameDao.MostPopulatedCombined(ShowOnlyAP, HideAP, userId, ref totalNumRecords, pageNumber, pageSize, wokGameId, countryFilterBy, countrySortBy, ageFilterBy, ageSortBy, offset);
        }

        public DataTable GetMostRequested(bool ShowOnlyAP, bool HideAP, int userId, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId)
        {
            return gameDao.GetMostRequested(ShowOnlyAP, HideAP, userId, ref totalNumRecords, pageNumber, pageSize, wokGameId);
        }

        /// <summary>
        /// Gets GameOwner by game id
        /// this function can receive format exceptions if the Dao
        /// comes back empty they are caught here and return null to 
        /// indcate failure
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetGameOwner(int gameId)
        {
            //default vaule indicating not found
            int owner_id = 0;

            //get game by the id provided
            Game game = GetGameByGameId(gameId);

            if (game != null)
            {
                owner_id = game.OwnerId;
            }

            return owner_id;
        }


        /// <summary>
        /// when user "allows" access to a game, this marks it as allowed
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="userId"></param>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public bool UserAllowedGame(int gameId, int userId)
        {
            return gameDao.UserAllowedGame(gameId, userId);
        }

        /// <summary>
        /// IsUserAllowedGame
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public bool IsUserAllowedGame(int gameId, int userId)
        {
            return gameDao.IsUserAllowedGame(gameId, userId);
        }

        /// <summary>
        /// Gets Game by game id
        /// this function can receive format exceptions if the Dao
        /// comes back empty they are caught here and return null to 
        /// indcate failure
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Game GetGameByGameId(int gameId)
        {
            Game game = null;

            try
            {
                game = gameDao.GetGame(gameId);
            }
            catch (Exception ex)
            {
                m_logger.Error("Error in Game facade when loading games by id", ex);
            }

            return game;
        }

        /// <summary>
        /// Gets Game by game id
        /// this function can receive format exceptions if the Dao
        /// comes back empty they are caught here and return null to 
        /// indcate failure
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Game GetGameByGameName(string gameName)
        {
            Game game = null;

            try
            {
                game = gameDao.GetGame(gameName);
            }
            catch (Exception ex)
            {
                m_logger.Error("Error in Game facade when loading games by id", ex);
            }

            return game;
        }

        /// <summary>
        /// Gets Game by game id
        /// this function can receive format exceptions if the Dao
        /// comes back empty they are caught here and return null to 
        /// indcate failure
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Game GetGameByServerId(int serverId)
        {
            Game game = null;

            try
            {
                game = gameDao.GetGameByServerId(serverId);
            }
            catch (Exception ex)
            {
                m_logger.Error("Error in Game facade when loading games by server id", ex);
            }

            return game;
        }

        /// <summary>
        /// Gets game id with provided community Id
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetGameIdByCommunityId(int communityId)
        {
            return gameDao.GetGameIdByCommunityId(communityId); ;
        }

        /// <summary>
        /// get all the currently available games by owner id EXCLUDING the current game
        /// </summary>
        /// <param name="categoryID"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedDataTable GetGamesByOwnerIdExclusive(bool showMature, int ownerId, int gameId, int pageNumber, int pageSize)
        {
            string filter = "g.game_id != " + gameId;
            return SearchForGames(null, filter, null, showMature, false, 0, ownerId, null, pageNumber, pageSize);
        }

        /// <summary>
        /// get all the currently available games by category names
        /// </summary>
        /// <param name="categoryID"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetWorldsByCategory(bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId, string category)
        {
            return gameDao.GetWorldsByCategory(ShowOnlyAP, HideAP, ref totalNumRecords, pageNumber, pageSize, wokGameId, category);
        }

        /// <summary>
        /// get all the top worlds for tour
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable TopWorldsTour(int userId, bool ShowOnlyAP, bool HideAP, ref int totalNumRecords, int pageNumber, int pageSize, int wokGameId)
        {
            return gameDao.TopWorldsTour(userId, ShowOnlyAP, HideAP, ref totalNumRecords, pageNumber, pageSize, wokGameId);
        }

        /// <summary>
        /// ReedemTopWorldReward
        /// </summary>
        public int ReedemTopWorldReward(int userId, int zoneInstanceId, int zoneType, double rewardAmount)
        {
            int result = gameDao.ReedemTopWorldReward(userId, zoneInstanceId, zoneType);

            if (result > 0)
            {
                UserFacade userFacade = new UserFacade();

                int wok_transaction_log_id = 0;
                // Issue Reward
                int rc = userFacade.AdjustUserBalance(userId, Currency.CurrencyType.REWARDS, rewardAmount, (ushort)TransactionType.eTRANSACTION_TYPES.CASH_TT_TOUR_PARTICIPATION, ref wok_transaction_log_id);

                if (wok_transaction_log_id > 0)
                {   // Update reward_log table so reward will show in users transactions page
                    userFacade.InsertRewardLog(userId, rewardAmount, Currency.CurrencyType.REWARDS, (int)TransactionType.eTRANSACTION_TYPES.CASH_TT_TOUR_PARTICIPATION, 15, wok_transaction_log_id);
                }
            }

            return result;
        }

        public bool HasTopWorldRewardBeenRedeemed(int userId, int zoneInstanceId, int zoneType)
        {
            return gameDao.HasTopWorldRewardBeenRedeemed(userId, zoneInstanceId, zoneType);
        }

        /// <summary>
        ///Saves a new game (game, license, and 3d community) in a transaction
        /// returns the games community id if seuccesful (last create done)
        /// </summary>
        /// <param name="game"></param>
        /// <param name="license"></param>
        /// <param name="community"></param>
        /// <returns>int</returns>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int TransactionalGameCreation(Game game, GameLicense license, Community community, int daysLeft)
        {
            return gameDao.TransactionalGameCreation(game, license, community, daysLeft);
        }

        /// <summary>
        ///Saves a new game
        /// returns the games game_id
        /// </summary>
        /// <param name="game"></param>
        /// <returns>int</returns>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SaveGame(Game game)
        {
            int result = 0;
            if (game.GameId < 1)
            {
                result = SaveNewGame(game);
            }
            else
            {
                result = UpdateGame(game);
            }
            return result;
        }

        /// <summary>
        ///Saves a new game
        /// returns the games game_id
        /// </summary>
        /// <param name="game"></param>
        /// <returns>int</returns>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int SaveNewGame(Game game)
        {
            return gameDao.AddNewGame(game);
        }

        /// <summary>
        ///Updates an existing game
        /// returns the number of rows affected
        /// </summary>
        /// <param name="game"></param>
        /// <returns>int</returns>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateGame(Game game)
        {
            return gameDao.UpdateGame(game);
        }

        /// <summary>
        ///Deletes an existing game
        /// returns the number of rows affected
        /// </summary>
        /// <param name="game"></param>
        /// <returns>int</returns>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteGame(int gameId, int userId)
        {
            return gameDao.DeleteGame(gameId, userId);
        }

        /// <summary>
        ///Updates a games thumbnail path
        /// </summary>
        /// <param name="game"></param>
        /// <returns>int</returns>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateGameThumbnail(int gameId, string itemImagePath)
        {
            return gameDao.UpdateGameThumbnail(gameId, itemImagePath);
        }

        /// <summary>
        ///raves a game
        /// </summary>
        /// <param name="game"></param>
        /// <returns>int</returns>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertGameRave(int userId, int gameId)
        {
            return gameDao.InsertGameRave(userId, gameId);
        }

        /// <summary>
        /// Updates a games google analytics account id
        /// </summary>
        /// <param name="game"></param>
        /// <returns>int</returns>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateGameGAAccountID(int gameId, string gaAccountID)
        {
            return gameDao.UpdateGameGAAccountID(gameId, gaAccountID);
        }

        #endregion

        // ***********************************************
        // Game option Functions
        // ***********************************************
        #region game option functions

        /// <summary>
        /// Gets Game status settings
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetGameStatusOptions()
        {
            return gameDao.GetGameStatus();
        }

        /// <summary>
        /// Gets Game access levels
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetGameAccessLevels()
        {
            return gameDao.GetGameAccess();
        }

        /// <summary>
        /// Gets Game rating settings
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetGameRatingOptionsLevels()
        {
            return gameDao.GetGameRatings();
        }

        /// <summary>
        /// Gets Game Passes
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetGamePasses(int gameId)
        {
            return gameDao.GetGamePasses(gameId);
        }

        /// <summary>
        /// Deletes a game pass
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public bool DeleteGamePass(int userId, bool isAdmin, int gameId, int passId)
        {
            return gameDao.DeleteGamePass(userId, isAdmin, gameId, passId);
        }

        /// <summary>
        /// Adds a game pass
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public bool AddGamePass(int userId, bool isAdmin, int gameId, int passId)
        {
            return gameDao.AddGamePass(userId, isAdmin, gameId, passId);
        }

        #endregion

        // ***********************************************
        // Category Functions
        // ***********************************************
        #region Categories Functions

        /// <summary>
        /// Gets Game category options
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        [Obsolete("Kept temporarily to allow function while porting game categories over to communities. Needs to be moved to CommunityFacade.", false)]
        public DataTable GetGameCategories()
        {
            return gameDao.GetGameCategories();
        }

        /// <summary>
        /// Generate Category Id List from a game id
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        [Obsolete("Kept temporarily to allow function while porting game categories over to communities. Needs to be moved to CommunityFacade.", false)]
        public string GenerateCategoryIdList(int gameId)
        {
            string inClause = "";
            //get all categories of the game
            DataTable dtCategories = GetGameCategories(gameId);
            try
            {
                if (dtCategories.Rows.Count > 0)
                {
                    //create the list of category ids
                    foreach (DataRow row in dtCategories.Rows)
                    {
                        inClause += (row["category_id"].ToString() + ",");
                    }

                    inClause = inClause.Substring(0, (inClause.Length - 1));
                }
            }
            catch (Exception)
            {
            }

            return inClause;
        }

        /// <summary>
        /// Generate Category Id List from a game id
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        [Obsolete("Kept temporarily to allow function while porting game categories over to communities. Needs to be moved to CommunityFacade.", false)]
        public string GenerateCategoryNameList(int gameId)
        {
            string inClause = "";
            DataTable dtCategories = GetGameCategories(gameId);
            try
            {
                if (dtCategories.Rows.Count > 0)
                {
                    foreach (DataRow row in dtCategories.Rows)
                    {
                        inClause += (row["description"].ToString() + " ");
                    }
                }
            }
            catch (Exception)
            {
            }

            return inClause;
        }

        /// <summary>
        /// Gets The game's indicated categories
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        [Obsolete("Kept temporarily to allow function while porting game categories over to communities. Needs to be moved to CommunityFacade.", false)]
        public DataTable GetGameCategories(int gameId)
        {
            return gameDao.GetGameCategories(gameId);
        }

        /// <summary>
        /// Gets the category id for a given game category name.
        /// </summary>
        [Obsolete("Kept temporarily to allow function while porting game categories over to communities. Needs to be moved to CommunityFacade.", false)]
        public int GetGameCategoryIdByName(string categoryName)
        {
            return gameDao.GetGameCategoryIdByName(categoryName);
        }

        /// <summary>
        /// delete's all a game's categories
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        [Obsolete("Kept temporarily to allow function while porting game categories over to communities. Needs to be moved to CommunityFacade.", false)]
        public int DeleteGameCategories(int gameId)
        {
            return gameDao.DeleteGameCategories(gameId);
        }

        /// <summary>
        /// adds a new category to a game
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        [Obsolete("Kept temporarily to allow function while porting game categories over to communities. Needs to be moved to CommunityFacade.", false)]
        public int AddGameCategory(int gameId, int categoryId)
        {
            return gameDao.InsertGameCategories(gameId, categoryId);
        }

        #endregion

        // ***********************************************
        // License Object Functions
        // ***********************************************
        #region License Functions

        /// <summary>
        /// Gets Game license types
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public GameLicenseSubscription GetGameLicenseType(int license_subscription_id)
        {
            return gameLicenseDao.GetGameLicenseType(license_subscription_id);
        }

        /// <summary>
        /// Gets Game license types
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<GameLicenseSubscription> GetGameLicenseTypes()
        {
            return gameLicenseDao.GetGameLicenseTypes();
        }

        /// <summary>
        /// Gets Game license for game
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public GameLicense GetGameLicenseByGameId(int gameId)
        {
            GameLicense license = null;

            license = gameLicenseDao.GetGameLicenseByGameId(gameId);

            return license;
        }

        /// <summary>
        /// Gets Game license for game
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public GameLicense GetGameLicenseByGameLicenseId(int gameLicenseId)
        {
            return gameLicenseDao.GetGameLicense(gameLicenseId);
        }

        /// <summary>
        /// Adds a game license
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddNewGameLicense(GameLicense license, int daysLeft)
        {
            return gameLicenseDao.AddNewGameLicense(license, daysLeft);
        }

        /// <summary>
        /// Updates a game license
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateGameLicense(GameLicense license)
        {
            return gameLicenseDao.UpdateGameLicense(license);
        }

        /// <summary>
        /// gets a game license contact
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public GameLicenseContact GetGameLicenseContact(int gameLicenseId)
        {
            return gameLicenseDao.GetGameLicenseContact(gameLicenseId);
        }

        /// <summary>
        /// Adds a game license contact
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertGameLicenseContact(GameLicenseContact gameLicenseContact)
        {
            return gameLicenseDao.InsertGameLicenseContact(gameLicenseContact);
        }

        #endregion

        // ***********************************************
        // Game Server Object Functions
        // ***********************************************
        #region Game Server Functions
        /// <summary>
        /// Gets Game server visibility options 
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetServerVisibilityOptions()
        {
            return gameServerDao.GetGameServerVisibility();
        }

        /// <summary>
        /// Gets Game server status options 
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetServerStatusOptions()
        {
            return gameServerDao.GetGameServerStatus();
        }

        /// <summary>
        /// Gets Game server's config settings as a string 
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public string GetServerConfig(int serverType, string serverActualIp)
        {
            return gameServerDao.GetServerConfig(serverType, serverActualIp);
        }

        /// <summary>
        /// Gets Game server type options
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetServerTypeOptions()
        {
            return gameServerDao.GetGameServerType();
        }

        /// <summary>
        /// Returns the version of a given server.
        /// </summary>
        /// <returns></returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public string GetServerVersion(int serverId)
        {
            return gameServerDao.GetServerVersion(serverId);
        }

        /// <summary>
        /// Returns the version of a given server.
        /// </summary>
        /// <returns></returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public string GetServerVersion(int gameId, string serverName, int port, int serverTypeId)
        {
            return gameServerDao.GetServerVersion(gameId, serverName, port, serverTypeId);
        }

        /// <summary>
        /// Gets a list of game servers for that game
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList<GameServer> GetServerListByGameId(int gameId, string filter, string orderby)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameServerDao.GetServerListByGameId(gameId, filter, orderby);
        }

        /// <summary>
        /// Gets a list of game servers for that game
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetServerByGameId(int gameId, string filter, string orderby)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameServerDao.GetServerByGameId(gameId, filter, orderby);
        }

        /// <summary>
        /// Adds a game server
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddNewGameServer(GameServer server)
        {
            return gameServerDao.AddNewServer(server);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int AddNewServerAndPatch(int userId, string licenseKey, int gameId, string hostName, int port, string patchUrl, string alternatePatchUrl, string patchType, string clientPath, ref int webAssignedPort, out int serverId)
        {
            return gameServerDao.AddNewServerAndPatch(userId, licenseKey, gameId, hostName, port, patchUrl, alternatePatchUrl, patchType, clientPath, ref webAssignedPort, out serverId);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedDataTable GetGames(int pageNumber, int pageSize, string orderBy)
        {
            return gameServerDao.GetGames(pageNumber, pageSize, orderBy);
        }

        /// <summary>
        /// Updates a game server
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateGameServer(GameServer server)
        {
            return gameServerDao.UpdateServer(server);
        }

        /// <summary>
        /// Updates a game server at pinger initialization
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public DataRow UpdateGameServerStart(int serverId, int maxCount, int statusId, string ipAddress, int port, string actualIp, int type, uint protocolVersion, int schemaVersion, string serverVersion, uint assetVersion, int parentGameId)
        {
            return gameServerDao.UpdateGameServerStart(serverId, maxCount, statusId, ipAddress, port, actualIp, type, protocolVersion, schemaVersion, serverVersion, assetVersion, parentGameId);
        }

        /// <summary>
        /// Updates a game server by pinging
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public DataRow UpdateGameServerPingCheck(int serverId, string actualIp, int numberOfPlayers, int maxNumberOfPlayers, int statusId, bool adminOnly)
        {
            return gameServerDao.UpdateGameServerPingCheck(serverId, actualIp, numberOfPlayers, maxNumberOfPlayers, statusId, adminOnly);
        }

        /// <summary>
        /// Deletes a game server
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteGameServer(int serverId)
        {
            return gameServerDao.DeleteServer(serverId);
        }

        public void BroadcastUpdateScriptZoneSpinDownDelayEvent(int channelZoneId)
        {
            // Reload zone-info from DB
            var zoneInfo = (new CommunityFacade()).Get3DPlace(channelZoneId);
            if (zoneInfo.ChannelZoneId == 0)
            {
                // Query failed
                throw new Exception("Error loading zone info from DB with channelZoneId " + channelZoneId.ToString());
            }

            XmlEventCreator xml = new XmlEventCreator("UpdateScriptZoneSpinDownDelayEvent");
            // Encode order see UpdateScriptZoneSpinDownDelayEvent.h
            xml.AddValue((uint)zoneInfo.ZoneIndexPlain);
            xml.AddValue((uint)zoneInfo.ZoneInstanceId);
            xml.AddValue((uint)zoneInfo.ZoneType);
            xml.AddValue(zoneInfo.SpinDownDelayMinutes);
            uint tiedServerId = zoneInfo.TiedToServer == "NOT_TIED" ? 0 : (uint)zoneInfo.ServerId;
            xml.AddValue(tiedServerId);

            try
            {
                m_logger.Info("Broadcast UpdateScriptZoneSpinDownDelayEvent: " +
                    "channelZoneId = " + channelZoneId.ToString() + ", " +
                    "spin-down delay = " + zoneInfo.SpinDownDelayMinutes.ToString() + ", " +
                    "tied server ID = " + tiedServerId.ToString());

                RemoteEventSender sender = new RemoteEventSender();
                sender.BroadcastEventToServer(xml.GetXml()); //send event to all servers
            }
            catch (Exception e)
            {
                m_logger.Error("Error broadcasting UpdateScriptZoneSpinDownDelayEvent to WoK servers", e);
            }
        }

        #endregion

        // ***********************************************
        // World Template Object Functions
        // ***********************************************
        #region Game Template Functions
        /// <summary>
        /// Called to get a paged list of WorldTemplates.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<WorldTemplate> GetWorldTemplates(int status, int[] passGroupId, int pageNumber, int pageSize)
        {
            int[] status_array = { status };
            return GetWorldTemplates(status_array, passGroupId, 0, null, pageNumber, pageSize);
        }

        /// <summary>
        /// Called to get a paged list of WorldTemplates.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<WorldTemplate> GetWorldTemplates(int[] status, int[] passGroupId, int userId, DateTime? userSignupDate, int pageNumber, int pageSize, string orderBy = "sort_order ASC")
        {
            PagedList<WorldTemplate> templates = worldTemplateDao.GetWorldTemplates(status, passGroupId, userSignupDate, false, pageNumber, pageSize, orderBy);
            return templates;
        }

        /// <summary>
        /// Called to search a paged list of WorldTemplates.
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<WorldTemplate> SearchWorldTemplates(string searchString, int pageNumber, int pageSize, string orderBy = "sort_order ASC")
        {
            return worldTemplateDao.SearchWorldTemplates(searchString, pageNumber, pageSize, orderBy);
        }

        /// <summary>
        /// Called to get a WorldTemplate
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public WorldTemplate GetWorldTemplate(int templateId, bool includeExperimentGroups = false)
        {
            WorldTemplate template = worldTemplateDao.GetWorldTemplate(templateId);

            if (template.TemplateId > 0 && includeExperimentGroups)
            {
                template.ExperimentGroups = experimentDao.GetWorldTemplateExperimentGroups(template, string.Empty, 1, Int32.MaxValue).ToList();

                foreach (WorldTemplateExperimentGroup group in template.ExperimentGroups)
                {
                    group.WorldTemplate = template;
                }
            }

            return template;
        }

        /// <summary>
        /// Called to get the default world template to be applied to newly created home worlds
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public WorldTemplate GetDefaultHomeWorldTemplate(int userId)
        {
            WorldTemplate template = worldTemplateDao.GetDefaultHomeWorldTemplate();

            ExperimentFacade experimentFacade = new ExperimentFacade();
            IList<UserExperimentGroup> participantData = experimentFacade.GetActiveUserExperimentGroups(userId, false);

            // If we have a valid A/B test template to use, use it instead
            if (participantData.Count > 0)
            {
                PagedList<WorldTemplate> templates = worldTemplateDao.GetWorldTemplatesByExperimentGroups(participantData.Select(pd => pd.GroupId).ToList(), ExperimentEffect.EFFECT_USE_AS_HOME_TEMPLATE);

                if (templates.Count == 1)
                {
                    template = templates.SingleOrDefault();

                    // Load experiment groups with the template
                    template = GetWorldTemplate(template.TemplateId, true);
                }
            }

            return template;
        }

        /// <summary>
        /// Called to insert a WorldTemplate
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertWorldTemplate(WorldTemplate template)
        {
            int returnVal = worldTemplateDao.InsertWorldTemplate(template);

            if (returnVal > 0)
            {
                // Add experiment groups
                experimentDao.ClearWorldTemplateExperimentGroups(template);
                foreach (WorldTemplateExperimentGroup group in template.ExperimentGroups)
                {
                    experimentDao.SaveWorldTemplateExperimentGroup(group, true);
                }
            }

            return returnVal;
        }

        /// <summary>
        /// Called to update a WorldTemplate
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateWorldTemplate(WorldTemplate template)
        {
            int returnVal = worldTemplateDao.UpdateWorldTemplate(template);

            // Add experiment groups
            experimentDao.ClearWorldTemplateExperimentGroups(template);
            foreach (WorldTemplateExperimentGroup group in template.ExperimentGroups)
            {
                experimentDao.SaveWorldTemplateExperimentGroup(group, true);
            }

            return returnVal;
        }

        /// <summary>
        /// Called to delete a WorldTemplate
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteWorldTemplate(WorldTemplate template)
        {
            return worldTemplateDao.DeleteWorldTemplate(template);
        }

        /// <summary>
        /// Gets default category for the game template
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetWorldTemplateDefaultCategory(int templateId)
        {
            WorldTemplate template = GetWorldTemplate(templateId);
            return template.DefaultCategoryId;
        }

        public void AddWorldTemplateDefaultItemsToInventory(int templateId, int userId)
        {
            // Get default inventory items associated with the template
            DataTable dtIds = worldTemplateDao.GetWorldTemplateDefaultItemIds(templateId);

            foreach (DataRow row in dtIds.Rows)
            {
                try
                {
                    // if item is a bundle, get bundle and add all items from bundle
                    int bundleId = Convert.ToInt32(row["bundle_id"]);
                    if (bundleId > 0)
                    {
                        // Get the bundle
                        List<WOKItem> bundleItems = shoppingDao.GetBundleItems(bundleId, "");
                        // add items to users inventory
                        foreach (WOKItem item in bundleItems)
                        {
                            if (item.GlobalId > 0)
                            {
                                userDao.AddItemToPendingInventory(userId, "B", item.GlobalId, 1, 256);
                            }
                        }
                    }
                    else
                    {
                        // Get the item and add to users inventory
                        WOKItem item = shoppingDao.GetItem(Convert.ToInt32(row["global_id"]), true);

                        if (item.GlobalId > 0)
                        {
                            userDao.AddItemToPendingInventory(userId, "B", item.GlobalId, 1, 256);
                        }
                    }
                }
                catch { }
            }
        }

        public PagedList<WOKItem> GetWorldTemplateDefaultItems(int templateId, int pageNumber, int pageSize)
        {
            return worldTemplateDao.GetWorldTemplateDefaultItems(templateId, pageNumber, pageSize);
        }

        public int InsertWorldTemplateDefaultItemIds(int templateId, int[] globalIds)
        {
            return worldTemplateDao.InsertWorldTemplateDefaultItemIds(templateId, globalIds);
        }

        public int DeleteWorldTemplateDefaultItemIds(int templateId, int[] globalIds)
        {
            return worldTemplateDao.DeleteWorldTemplateDefaultItemIds(templateId, globalIds);
        }

        /// <summary>
        /// AddWorldTemplateDefaultIcons
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="templateId"></param>
        public void AddWorldTemplateDefaultIcons(int communityId, int templateId)
        {
            CommunityFacade communityFacade = new CommunityFacade();

            WorldTemplate worldTemplate = worldTemplateDao.GetWorldTemplate(templateId);

            if (worldTemplate.DefaultThumbnail.Length > 0)
            {
                string fileType = System.IO.Path.GetExtension(worldTemplate.DefaultThumbnail);
                string fileName = worldTemplate.DefaultThumbnail.Replace(fileType, "");

                communityFacade.UpdateCommunity(communityId, 0, false, worldTemplate.DefaultThumbnail, fileType, true);

                communityFacade.UpdateCommunityThumb(communityId, fileName + "_sm" + fileType, "thumbnail_small_path");
                communityFacade.UpdateCommunityThumb(communityId, fileName + "_me" + fileType, "thumbnail_medium_path");
                communityFacade.UpdateCommunityThumb(communityId, fileName + "_la" + fileType, "thumbnail_large_path");
                communityFacade.UpdateCommunityThumb(communityId, fileName + "_xl" + fileType, "thumbnail_xlarge_path");

                // Update game thumb for 
                UpdateGameThumbnail(GetGameIdByCommunityId(communityId), fileName + "_sm" + fileType);
            }
        }

        /// <summary>
        /// Called to insert a record linking game and templagte
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertWorldsToTemplate(int communityId, int templateId)
        {
            return worldTemplateDao.InsertWorldsToTemplate(communityId, templateId);
        }

        /// <summary>
        /// Called to insert a record linking game and templagte
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int DeleteWorldsToTemplate(int communityId, int templateId)
        {
            return worldTemplateDao.DeleteWorldsToTemplate(communityId, templateId);
        }


        #endregion

        // ***********************************************
        // Patch URL Functions
        // ***********************************************
        #region patchURL Functions

        /// <summary>
        /// Gets a list of game servers for that game
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetPatchURLsByGameId(int gameId, string filter, string orderby)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameServerDao.GetPatchURLByGameId(gameId, filter, orderby);
        }

        /// <summary>
        /// adds a new patch URL entry
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int AddNewPatchURLs(int gameId, string patchUrl)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameServerDao.AddNewPatchURL(gameId, patchUrl);
        }

        /// <summary>
        /// adds a new patch URL entry
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int DeletePatchURLs(int gameId, int patchUrlID)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameServerDao.DeletePatchURL(gameId, patchUrlID);
        }

        /// <summary>
        /// adds a new patch URL entry
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int UpdatePatchURLs(int gameId, int patchUrlID, string patchUrl)
        {
            // TODO: add access security here..
            // TODO: add argument validation here..
            return gameServerDao.UpdatePatchURL(gameId, patchUrlID, patchUrl);
        }

        #endregion

        // ***********************************************
        // Patch URL Functions
        // ***********************************************
        #region Miscellaneous Functions

        public void ClearZoneCustomizationsCache(int zoneIndex, int instanceId)
        {
            gameDao.ClearZoneCustomizationsCache(zoneIndex, instanceId);
        }

        public DataSet GetZoneCustomizations(int zoneIndex, int instanceId, float x, float y, float z)
        {
            return gameDao.GetZoneCustomizations(zoneIndex, instanceId, x, y, z);
        }

        public IDictionary<uint, uint[]> GetZoneDownloadSizes(int zoneIndex, int instanceId, uint meshPathTypeId, uint texPathTypeId)
        {
            return gameDao.GetZoneDownloadSizes(zoneIndex, instanceId, meshPathTypeId, texPathTypeId);
        }

        public bool IsFrameworkEnabled(int instanceId, int zoneType)
        {
            return gameDao.IsFrameworkEnabled(instanceId, zoneType);
        }

        public bool UserInGame(int userId, int gameId)
        {
            return gameDao.UserInGame(userId, gameId);
        }

        public DataTable GetGameUserIn(int userId)
        {
            return gameDao.GetGameUserIn(userId);
        }

        public eLOGIN_RESULTS LoginUser(int userId, string userIpAddress, int gameId, int serverId, string actualIp, ref int roleId)
        {
            return gameDao.LoginUser(userId, userIpAddress, gameId, serverId, actualIp, ref roleId);
        }

        public eLOGIN_RESULTS LogoutUser(int userId, int reasonCode, int serverId, string actualIp)
        {
            return gameDao.LogoutUser(userId, reasonCode, serverId, actualIp);
        }

        public DataSet GetChildGameWebCallValues()
        {
            return gameDao.GetChildGameWebCallValues();
        }

        public DataSet GetGameItems(int userId, int gameId, int globalId, int quantity)
        {
            return gameDao.GetGameItems(userId, gameId, globalId, quantity);
        }

        public CommunityMember.CommunityMemberAccountType GetGamePrivileges(int userId, int gameId)
        {
            return gameDao.GetGamePrivileges(userId, gameId);
        }

        public int GetGameAccessByServerId(int serverId)
        {
            return gameDao.GetGameAccessByServerId(serverId);
        }

        public string CheckGameServers(string overrideIpAddress, int gameId, int serverId, out bool internalError)
        {
            GameServer gs = gameServerDao.GetServer(serverId);
            DataTable patchUrls = gameServerDao.GetPatchURLByGameId(gameId, "", "");

            if (gs.GameId != 0 && patchUrls != null)
            {
                return gs.Validate(overrideIpAddress, ref patchUrls, out internalError);
            }
            else
            {
                internalError = true;
                return "No data found for serverId of " + serverId.ToString();
            }
        }

        public GameServer GetServer(int serverId)
        {
            return gameServerDao.GetServer(serverId);
        }

        public static int ANY_GAME = 0;

        #endregion

        // ***********************************************
        // Game License Subscriptions
        // ***********************************************
        #region Game License Subscriptions Functions

        /// <summary>
        /// GetGameSubscription
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public GameSubscription GetGameSubscription(int SubscriptionId)
        {
            return gameLicenseDao.GetGameSubscription(SubscriptionId);
        }

        /// <summary>
        /// GetGameSubscriptionByOrderId
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public GameSubscription GetGameSubscriptionByOrderId(int orderId)
        {
            return gameLicenseDao.GetGameSubscriptionByOrderId(orderId);
        }

        /// <summary>
        /// GetGameSubscriptions
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<GameSubscription> GetGameSubscriptions(int GameId, string orderby, int pageNumber, int pageSize)
        {
            return gameLicenseDao.GetGameSubscriptions(GameId, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// InsertGameSubscription
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertGameSubscription(GameSubscription gameSubscription)
        {
            return gameLicenseDao.InsertGameSubscription(gameSubscription);
        }

        /// <summary>
        /// UpdateGameSubscription
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateGameSubscription(GameSubscription gameSubscription)
        {
            gameLicenseDao.UpdateGameSubscription(gameSubscription);
        }

        /// <summary>
        /// InsertGameSubscriptionOrder
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertGameSubscriptionOrder(int subscriptionId, int orderId)
        {
            return gameLicenseDao.InsertGameSubscriptionOrder(subscriptionId, orderId);
        }

        #endregion

        /// <summary>
        /// InsertGameSubscriptionOrder
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int AdjustGameBalance(int gameId, string keiPointId, Double amount)
        {
            return gameDao.AdjustGameBalance(gameId, keiPointId, amount);
        }

        // ***********************************************
        // API Related
        // ***********************************************
        #region API Related

        /// <summary>
        /// InsertGiftedItem
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int InsertGiftedItem(int gameId, int userId, int globalId)
        {
            return gameDao.InsertGiftedItem(gameId, userId, globalId);
        }

        /// <summary>
        /// GetGiftedItemsCount
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int GetGiftedItemsCount(int gameId, int userId)
        {
            return gameDao.GetGiftedItemsCount(gameId, userId);
        }

        /// <summary>
        /// LeaderBoard
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Leaderboard GetLeaderBoard(int communityId)
        {
            return gameDao.GetLeaderBoard(communityId);
        }

        /// <summary>
        /// LeaderBoard
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<LeaderboardValue> GetLeaderBoardValues(int communityId, int userId, bool onlyFriends, int pageNumber, int pageSize)
        {
            if (pageNumber.Equals(0))
            {
                // Find user in list mode
                PagedList<LeaderboardValue> plLeaderboard = gameDao.GetLeaderBoardValues(communityId, userId, onlyFriends, 1, 1000);
                int rank = 1;

                foreach (LeaderboardValue lbv in plLeaderboard)
                {
                    if (lbv.UserId.Equals(userId))
                    {
                        if (lbv.Rank < pageSize)
                        {
                            pageNumber = 1;
                            break;
                        }

                        pageNumber = (int)System.Math.Ceiling(((double)rank - (double)pageSize) / 2);
                        break;
                    }
                    rank++;
                }

            }

            // User was not found, start at top
            if (pageNumber.Equals(0))
            {
                pageNumber = 1;
            }

            return gameDao.GetLeaderBoardValues(communityId, userId, onlyFriends, pageNumber, pageSize);
        }


        /// <summary>
        /// LeaderBoard
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<UserPlayerData> GetLeaderBoardValuesWithAllData(int communityId, int userId, bool onlyFriends, int pageNumber, int pageSize)
        {
            PagedList<LeaderboardValue> plLeaderboard = GetLeaderBoardValues(communityId, userId, onlyFriends, pageNumber, pageSize);
            PagedList<UserPlayerData> plUPD = new PagedList<UserPlayerData>();

            foreach (LeaderboardValue lbv in plLeaderboard)
            {
                LevelValue lv = gameDao.GetLevelValue(communityId, lbv.UserId);

                plUPD.Add(new UserPlayerData(lbv.CommunityId, lbv.UserId, lbv.CurrentValue,
                    lbv.ChangedDate, lbv.Name, lbv.ColumnName, lbv.Username,
                    lbv.DisplayName, lbv.ThumbnailSmallPath, lbv.ThumbnailMediumPath,
                    lbv.MatureProfile, lbv.Ustate, lbv.Rank, lbv.Gender,
                    lv.LevelNumber, lv.PercentComplete, lv.Title)
                    );
            }

            plUPD.TotalCount = plLeaderboard.TotalCount;
            return plUPD;
        }


        /// <summary>
        /// EarnLeaderBoard
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int EarnLeaderBoard(int communityId, int userId, int newValue)
        {
            gameDao.EarnLeaderBoard(communityId, userId, newValue);
            return newValue;
        }

        /// <summary>
        /// SaveLeaderBoard
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int SaveLeaderBoard(Leaderboard leaderboard)
        {
            return gameDao.SaveLeaderBoard(leaderboard);
        }

        /// <summary>
        /// ResetLeaderBoard
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int ResetLeaderBoard(int communityId)
        {
            return gameDao.ResetLeaderBoard(communityId);
        }

        /// <summary>
        /// EarnPlayerData
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void EarnPlayerData(int communityId, int userId, int newLeaderboardValue, UInt32 levelNumber, UInt32 percentComplete, string title)
        {
            gameDao.EarnLeaderBoard(communityId, userId, newLeaderboardValue);
            gameDao.EarnLevel(userId, communityId, levelNumber, percentComplete, title);


        }

        /// <summary>
        /// EarnLevel
        /// </summary>  
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void EarnLevel(int communityId, int userId, uint levelNumber, uint percentComplete, string title, bool sendBlast, string appUrl, string linkText)
        {
            gameDao.EarnLevel(communityId, userId, levelNumber, percentComplete, title);

            if (sendBlast)
            {
                //xxx
            }
        }

        /// <summary>
        /// EarnTitle
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void EarnTitle(int communityId, int userId, string title)
        {
            gameDao.EarnTitle(communityId, userId, title);
        }

        /// <summary>
        /// GetTitle
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public string GetTitle(int communityId, int userId)
        {
            return gameDao.GetTitle(communityId, userId);
        }

        /// <summary>
        /// GetLevelValue
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public LevelValue GetLevelValue(int communityId, int userId)
        {
            return gameDao.GetLevelValue(communityId, userId);
        }

        /// <summary>
        /// RemovePlayerFromLeaderBoard
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int RemovePlayerFromLeaderBoard(int communityId, int userId)
        {
            return gameDao.RemovePlayerFromLeaderBoard(communityId, userId);
        }


        /// <summary>
        /// EarnAchievement
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void EarnAchievement(int communityId, int userId, uint achievementId, bool sendBlast, string linkText, string appUrl)
        {
            // Look up achievement make sure community id is valid
            Achievement achievement = gameDao.GetAchievement(achievementId);

            if (!achievement.CommunityId.Equals(communityId))
            {
                return;
            }


            gameDao.EarnAchievement(userId, achievementId);

            // Awards
            AddBadgeAwards(userId, achievementId);

            if (sendBlast)
            {
                // Send the achievement blast
                try
                {
                    new BlastFacade().SendAchievementEarned(userId, communityId, achievementId, linkText, appUrl);
                }
                catch (Exception ex)
                {
                    m_logger.Error("Error sending Achievement Blast", ex);
                }
            }
        }

        /// <summary>
        /// AddBadgeAwards
        /// </summary>
        public void AddBadgeAwards(int userId, uint achievementId)
        {
            string WOK_INVENTORY_TYPE_PERSONAL = "P";

            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);

            try
            {
                List<AchievementAward> achievementAwards = GetAchievementAwards(achievementId, user.Gender);

                foreach (AchievementAward achievementAward in achievementAwards)
                {
                    if (achievementAward.NewTitleName.Length > 0)
                    {
                        // Award the new title here
                        // They have to be in WOK
                        if (user.WokPlayerId > 0)
                        {
                            // Add it to wok.player_titles
                            userFacade.InsertPlayerTitle(user.WokPlayerId, achievementAward.NewTitleName, 0);

                            // Set it on thier player record
                            userFacade.UpdatePlayerTitle(user.WokPlayerId, achievementAward.NewTitleName);
                        }
                    }

                    // Items
                    if (achievementAward.GlobalId > 0)
                    {
                        userFacade.AddItemToPendingInventory(user.UserId, WOK_INVENTORY_TYPE_PERSONAL, achievementAward.GlobalId, achievementAward.Quantity, 512);
                    }

                    // Only C group gets rewards...
                    const string experimentId = "9bb4ed18-6623-11e5-96ff-002219919c2a";
                    //const string variantBGroupId = "16de969b-6624-11e5-96ff-002219919c2a";  // B
                    const string variantCGroupId = "3312c53b-6624-11e5-96ff-002219919c2a";  // C

                    ExperimentFacade experimentFacade = new ExperimentFacade();

                    try
                    {
                        UserExperimentGroup userExperimentGroup = experimentFacade.GetUserExperimentGroupCommon(userId, experimentId);

                        if (userExperimentGroup != null)
                        {
                            //// Variant B
                            //if (userExperimentGroup.GroupId.Equals(variantBGroupId))
                            //{
                            //   // Nada
                            //}
                            // Variant C
                            if (userExperimentGroup.GroupId.Equals(variantCGroupId))
                            {
                                // Rewards
                                if (achievementAward.Rewards > 0)
                                {
                                    userFacade.AdjustUserBalance(userId, Currency.CurrencyType.REWARDS, achievementAward.Rewards, (ushort)TransactionType.eTRANSACTION_TYPES.CASH_TT_WORLD_FAME_ACHIEVEMENT);
                                }
                            }

                        }
                        else
                        {
                            // Rewards
                            if (achievementAward.Rewards > 0)
                            {
                                userFacade.AdjustUserBalance(userId, Currency.CurrencyType.REWARDS, achievementAward.Rewards, (ushort)TransactionType.eTRANSACTION_TYPES.CASH_TT_WORLD_FAME_ACHIEVEMENT);
                            }
                        }
                    }
                    catch
                    {
                        // Rewards
                        if (achievementAward.Rewards > 0)
                        {
                            userFacade.AdjustUserBalance(userId, Currency.CurrencyType.REWARDS, achievementAward.Rewards, (ushort)TransactionType.eTRANSACTION_TYPES.CASH_TT_WORLD_FAME_ACHIEVEMENT);
                        }
                    }


                }
            }
            catch (Exception) { }
        }

        public List<AchievementAward> GetAchievementAwards(uint achievementId, string gender)
        {
            return gameDao.GetAchievementAwards(achievementId, gender);
        }

        /// <summary>
        /// AchievementForUser
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<AchievementEarned> AchievementForUser(int userId, int communityId, string sortBy, bool showAll, bool onlyFriends, int pageNumber, int pageSize)
        {
            return gameDao.AchievementForUser(userId, communityId, sortBy, showAll, onlyFriends, pageNumber, pageSize);
        }

        /// <summary>
        /// AchievementForApp
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Achievement> AchievementForApp(int communityId, string sortBy, int pageNumber, int pageSize)
        {
            return gameDao.AchievementForApp(communityId, sortBy, pageNumber, pageSize);
        }

        /// <summary>
        /// GetUserAchievementsForApp
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<UserAchievementData> GetUserAchievementsForApp(int communityId, int userId, string orderBy, int pageNumber, int pageSize)
        {
            return gameDao.GetUserAchievementsForApp(communityId, userId, orderBy, pageNumber, pageSize);
        }

        /// <summary>
        /// GetAchievement
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Achievement GetAchievement(uint achievementId)
        {
            return gameDao.GetAchievement(achievementId);
        }

        /// <summary>
        /// SaveAchievement
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public uint SaveAchievement(Achievement achievement)
        {
            return gameDao.SaveAchievement(achievement);
        }

        /// <summary>
        /// UpdateAchievementThumbnail
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateAchievementThumbnail(uint achievementId, string imageUrl)
        {
            return gameDao.UpdateAchievementThumbnail(achievementId, imageUrl);
        }

        /// <summary>
        /// GetTotalAchievementPoints
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int GetTotalAchievementPoints(int communityId, string filter)
        {
            return gameDao.GetTotalAchievementPoints(communityId, filter);
        }

        /// <summary>
        /// DeleteAchievement
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteAchievement(uint achievementId)
        {
            return gameDao.DeleteAchievement(achievementId);
        }

        /// <summary>
        /// ResetAchievements
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int ResetAchievements(int communityId)
        {
            return gameDao.ResetAchievements(communityId);
        }

        /// <summary>
        /// GetGamesInWhichUserHasFame
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Community> GetGamesInWhichUserHasFame(int userId, int pageNumber, int pageSize, string orderBy)
        {
            PagedList<Community> plc = gameDao.GetGamesInWhichUserHasFame(userId, pageNumber, pageSize, orderBy);

            if (pageNumber == 1)
            {
                Community c = new Community();

                // We need to add World Fame to top of list if user does not have any world fame points yet
                UserFame userWorldFame = new FameFacade().GetUserFame(userId, (int)FameTypes.World);

                if (userWorldFame.Points == 0)
                {
                    bool addWorldFame = true;

                    foreach (Community community in plc)
                    {
                        if (community.CommunityId == (int)FameTypes.World)
                        {
                            addWorldFame = false;
                            break;
                        }
                    }

                    if (addWorldFame)
                    {
                        // World Fame Community Id is the same as the Fame Type
                        c = new CommunityFacade().GetCommunity((int)FameTypes.World);
                        plc.Insert(0, c);
                        plc.TotalCount = plc.TotalCount + 1;
                    }
                }
                // World Fame is already in the list and at the top via query, so we don't need to do anything with list


                // Designer Fame -- only show if they have participated.  Points > 0.
                UserFame userFame = new FameFacade().GetUserFame(userId, (int)FameTypes.Fashion);
                if (userFame.Points > 0)
                {
                    c = new Community();
                    // Since Designer Fame does not have a community, set the id to the fame type so
                    // we can get the user fame this fame type
                    c.CommunityId = (int)FameTypes.Fashion;
                    c.Name = "Designer Fame";
                    c.ThumbnailSmallPath = "DesignerFameIcon_sm.jpg";
                    c.ThumbnailMediumPath = "DesignerFameIcon_me.jpg";
                    c.IsAdult = "N";
                    c.NameNoSpaces = "KanevaDesignerFame";
                    plc.Insert(1, c);
                    plc.TotalCount = plc.TotalCount + 1;
                }
            }

            return plc;
        }

        /// <summary>
        /// GetAchievementsSummaryForApp
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Dictionary<string, int> GetAchievementsSummaryForApp(int communityId, int userId)
        {
            return gameDao.GetAchievementsSummaryForApp(communityId, userId);
        }

        /// <summary>
        /// GetAchievementsSummaryForAllApps
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Dictionary<string, int> GetAchievementsSummaryForAllApps(int userId)
        {
            return gameDao.GetAchievementsSummaryForAllApps(userId);
        }

        /// <summary>
        /// InsertAPIAuth
        /// </summary>
        /// <param name="gameId"></param>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        [Obsolete("Use version in CommunityFacade instead.", false)]
        public void InsertAPIAuth(int gameId)
        {
            Game game = GetGameByGameId(gameId);

            string consumerKey = game.GameName.Replace(" ", "");
            string consumerSecret = Guid.NewGuid().ToString().Replace("-", "K");

            InsertAPIAuth(gameId, consumerKey, consumerSecret);
        }

        /// <summary>
        /// InsertAPIAuth
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="consumerKey"></param>
        /// <param name="consumerSecret"></param>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        [Obsolete("Use version in CommunityFacade instead.", false)]
        public void InsertAPIAuth(int gameId, string consumerKey, string consumerSecret)
        {
            gameDao.InsertAPIAuth(gameId, consumerKey, consumerSecret);
        }

        public APIAuthentication GetAPIAuth(int gameId)
        {
            return gameDao.GetAPIAuth(gameId);
        }


        /// <summary>
        /// GetAPIAuth
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        [Obsolete("Use version in CommunityFacade instead.", false)]
        public APIAuthentication GetAPIAuth(string consumerKey, string consumerSecret)
        {
            return gameDao.GetAPIAuth(consumerKey, consumerSecret);
        }

        /// <summary>
        /// GetAPIAuth
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        [Obsolete("Use version in CommunityFacade instead.", false)]
        public APIAuthentication GetAPIAuth(string consumerKey)
        {
            return gameDao.GetAPIAuth(consumerKey);
        }

        #endregion

        public DataTable GetMostVisited(int userId, bool HideAP, int pageNumber, int pageSize, int constrainCount, int wokGameId)
        {
            return GetMostVisited(userId, HideAP, pageNumber, pageSize, constrainCount, wokGameId, false);
        }

        public DataTable GetMostVisited(int userId, bool HideAP, int pageNumber, int pageSize, int constrainCount, int wokGameId, bool includePopulation)
        {
            return gameDao.GetMostVisited(userId, HideAP, pageNumber, pageSize, constrainCount, wokGameId, includePopulation);
        }

        public DataTable Top3Dapps(bool onlyAccessPass, bool HideAP, int pageNumber, int pageSize, ref int totalCount, int wokGameId, bool hideOwnerOnly = false)
        {
            return gameDao.Top3Dapps(onlyAccessPass, HideAP, pageNumber, pageSize, ref totalCount, wokGameId, hideOwnerOnly);
        }

        public DataTable GetUnique3DAppVisits(int pageNumber, int pageSize)
        {
            return gameDao.GetUnique3DAppVisits(pageNumber, pageSize);
        }

        /// <summary>
        /// CreateApp
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string CreateApp(string name, string description, int userId, int templateId)
        {
            string url = AppHostURL() + "CreateApp?name=" + name + "&desc=" + description + "&owner=" + userId.ToString() + "&templateid=" + templateId.ToString();

            WebRequest request = HttpWebRequest.Create(url);
            request.Timeout = 30000;
            WebResponse response = request.GetResponse();
            XmlDocument doc = new XmlDocument();
            doc.Load(response.GetResponseStream());

            return doc.InnerXml;
        }

        /// <summary>
        /// IsTaskCompleted
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public string IsTaskCompleted(Int64 taskId)
        {
            string url = AppHostURL() + "IsTaskCompleted?taskid=" + taskId.ToString();

            WebRequest request = HttpWebRequest.Create(url);
            WebResponse response = request.GetResponse();
            XmlDocument doc = new XmlDocument();
            doc.Load(response.GetResponseStream());

            return doc.InnerXml;
        }

        /// <summary>
        /// GoToApp
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GoToApp(int id)
        {
            string url = AppHostURL() + "GoToApp?appid=" + id.ToString();

            WebRequest request = HttpWebRequest.Create(url);
            WebResponse response = request.GetResponse();
            XmlDocument doc = new XmlDocument();
            doc.Load(response.GetResponseStream());

            return doc.InnerXml;
        }

        private string AppHostURL()
        {
            string url = "http://dev-appman1.kaneva.com:8080/";

            if (System.Configuration.ConfigurationManager.AppSettings["AppHostURL"] != null)
            {
                url = System.Configuration.ConfigurationManager.AppSettings["AppHostURL"];
            }

            return url;
        }


        public int InsertNotificationAppRequest(int userId, int gameId)
        {
            return gameDao.InsertNotificationAppRequest(userId, gameId);
        }

        public int ClearNotificationAppRequest(int userId, int gameId)
        {
            return gameDao.ClearNotificationAppRequest(userId, gameId);
        }

        public int GetNumberOfNotificationAppRequest(int userId, int gameId)
        {
            return gameDao.GetNumberOfNotificationAppRequest(userId, gameId);
        }

        public int GetTotalNotificationAppRequests(int userId)
        {
            return gameDao.GetTotalNotificationAppRequests(userId);
        }

        public int GetNumberOfVisitorsForGamesByOwner(int userId, DateTime startDate)
        {
            return gameDao.GetNumberOfVisitorsForGamesByOwner(userId, startDate);
        }

        public bool InsertAppMessageQueueImport3DApp(int gameId, int zoneIndex, int instanceId)
        {
            return InsertAppMessageQueue(gameId, "Import", "getSpaceForImport.aspx?game=T&amp;zoneIndex=" + zoneIndex + "&amp;instanceId=" + instanceId);
        }

        public bool InsertAppMessageQueue(int gameId, string msgType, string msgSource)
        {
            return gameDao.InsertAppMessageQueue(gameId, msgType, msgSource);
        }

        public DataTable GetAppMessageQueue(int gameId)
        {
            return gameDao.GetAppMessageQueue(gameId);
        }

        public bool UpdateAppMessageQueue(string id, string clientResponseId, string clientResponseCode, DateTime clientProcessedDate)
        {
            return gameDao.UpdateAppMessageQueue(id, clientResponseId, clientResponseCode, clientProcessedDate);
        }

        public DataRow GetGameFaux3DApp(int gameId)
        {
            return gameDao.GetGameFaux3DApp(gameId);
        }

        public string GetZoneNameFromZoneIndex(int zoneIndex)
        {
            return gameDao.GetZoneNameFromZoneIndex(zoneIndex);
        }

        // ***********************************************
        // World Creation from Template
        // ***********************************************
        #region World Creation from Template

        /// <summary>
        /// Creates a world using the asynchronous World Creation Service.
        /// </summary>
        /// <returns>A token that can be used to check the progress of the world creation.</returns>
        [System.Security.SecuritySafeCritical]
        public string CreateWorldFromTemplateAsync(int userId, string title, string description, int templateId, int parentId, IModel channel)
        {
            string guid = Guid.NewGuid().ToString();

            CreateWorldMessage message = new CreateWorldMessage
            {
                UserId = userId,
                WorldName = title,
                WorldDescription = description,
                TemplateId = templateId,
                ParentId = parentId
            };
            string messageJSON = JsonConvert.SerializeObject(message);

            // Send message to RabbitMQ.
            try
            {
                IBasicProperties props = channel.CreateBasicProperties();
                props.CorrelationId = guid;
                channel.QueueDeclare("world_creation", true, false, false, null);
                channel.BasicPublish("", "world_creation", props, System.Text.Encoding.UTF8.GetBytes(messageJSON));
            }
            catch (Exception ex)
            {
                m_logger.Error("Unable to write metric to RabbitMQ.", ex);
                guid = null;
            }

            return guid;
        }

        /// <summary>
        /// Called to create a new world from a world template.
        /// </summary>
        /// <returns>The CommunityId of the newly created world.</returns>
        public int CreateWorldFromTemplate(WorldTemplate template, string title, string urlSafeTitle, string description, string urlSafeDescription, User currentUser, bool isUserAdmin, bool isUserBetaTester, string wokGameName, out int errorCode, out string errorMsg)
        {
            int communityId = 0;

            if (ValidateWorldCreationRules(title, currentUser, isUserAdmin, isUserBetaTester, out errorCode, out errorMsg))
            {
                if (template.IncubatorHosted)
                {
                    communityId = Create3DAppFromTemplate(template, title, urlSafeTitle, description, urlSafeDescription, currentUser, out errorCode, out errorMsg);
                }
                else
                {
                    communityId = CreateCommunityFromTemplate(template, title, description, currentUser, wokGameName, out errorCode, out errorMsg);
                }

                if (communityId > 0)
                {
                    // App was succesfully created, so give the owner credit.
                    FameFacade fameFacade = new FameFacade();
                    fameFacade.RedeemPacket(currentUser.UserId, (int)PacketId.MAKE_A_GAME, (int)FameTypes.World);

                    // Update the app's category to default
                    int defaultCategory = GetWorldTemplateDefaultCategory(template.TemplateId);
                    if (defaultCategory >= 1)
                    {
                        CommunityFacade communityFacade = new CommunityFacade();
                        communityFacade.InsertCommunityCategory(communityId, defaultCategory);
                    }

                    // Add any default template items to users inventory
                    AddWorldTemplateDefaultItemsToInventory(template.TemplateId, currentUser.UserId);

                    // Default thumbnails
                    AddWorldTemplateDefaultIcons(communityId, template.TemplateId);
                }
                else
                {
                    errorCode = -14;
                    errorMsg = "Error creating World.";
                }
            }

            return communityId;
        }

        /// <summary>
        /// Centralizes world creation business logic.
        /// </summary>
        /// <returns>True if the world is able to be created.</returns>
        private bool ValidateWorldCreationRules(string title, User currentUser, bool isUserAdmin, bool isUserBetaTester, out int errorCode, out string errorMsg)
        {
            errorCode = 0;
            errorMsg = string.Empty;
            int communityActiveStatus = 1;
            int userActiveStatus = 1;

            CommunityFacade communityFacade = new CommunityFacade();

            int numberOfCommunities = communityFacade.GetNumberOfCommunities(currentUser.UserId, communityActiveStatus, new int[] { (int)CommunityType.COMMUNITY, (int)CommunityType.APP_3D, (int)CommunityType.HOME });

            // Email must be validated to continue
            // Per AS Email in 1/24 you are allowed first 4 without validating
            if (numberOfCommunities >= 5)
            {
                if (!isUserAdmin && !currentUser.StatusId.Equals(userActiveStatus))
                {
                    errorMsg = "You must first validate your email to create a World";
                    errorCode = -2;
                }
            }

            // 3dapp limits
            if (!isUserAdmin && !isUserBetaTester && currentUser.UserId != Configuration.AutomatedTestUserDefaultId)
            {
                if (numberOfCommunities >= Configuration.MaxNumberOfCommunitiesPerUser)
                {
                    errorMsg = "Users are restricted to a maximum of " + Configuration.MaxNumberOfCommunitiesPerUser + " Worlds";
                    errorCode = -3;
                }
            }

            // Check for community already exists
            if (communityFacade.IsCommunityNameTaken(false, title.Replace(" ", ""), 0))
            {
                errorMsg = "World " + title + " already exists, please change the name. We recommend " + communityFacade.GetSuggestedName(title);
                errorCode = -5;
            }

            // Validate world name
            if (!Regex.IsMatch(title, "^[\\d\\s_\\-a-zA-Z0-9]{4,50}$"))
            {
                errorMsg = "Name should be at least four characters and can contain only letters, numbers, spaces, dashes or underscore.";
                errorCode = -7;
            }

            // Everything's good
            return (errorCode == 0);
        }

        /// <summary>
        /// Called to create a new community (hangout) based on the world template.
        /// </summary>
        /// <returns>CommunityId of the new community.</returns>
        private int CreateCommunityFromTemplate(WorldTemplate template, string title, string description, User currentUser, string wokGameName, out int errorCode, out string errorMsg)
        {
            int communityId = 0;
            errorMsg = string.Empty;
            errorCode = 0;

            ShoppingFacade shoppingFacade = new ShoppingFacade();

            // If this template is based on a custom deed, make sure we can find it.
            DataRow drCustomDeedUsage = shoppingFacade.GetCustomDeedUsage(template.GlobalId);
            if (template.GlobalId > 0 && drCustomDeedUsage == null)
            {
                errorMsg = "Unable to find World template.";
                errorCode = -21;
            }
            else
            {
                // Insert the community record for the new world
                CommunityFacade communityFacade = new CommunityFacade();
                Community community = new Community();
                community.PlaceTypeId = 0;
                community.Name = title;
                community.NameNoSpaces = community.Name.Replace(" ", "");
                community.Description = description;
                community.CreatorId = currentUser.UserId;
                community.CreatorUsername = currentUser.Username;
                community.IsPublic = (template.PrivateOnCreation ? "N" : "Y");
                community.IsAdult = "N";
                community.IsPersonal = 0;
                community.CommunityTypeId = (int)CommunityType.COMMUNITY;
                community.StatusId = (int)CommunityStatus.ACTIVE;

                communityId = communityFacade.InsertCommunity(community);

                if (communityId > 0)
                {
                    // Insert the community world owner as a community member
                    CommunityMember member = new CommunityMember();
                    member.CommunityId = communityId;
                    member.UserId = currentUser.UserId;
                    member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.OWNER;
                    member.Newsletter = "Y";
                    member.InvitedByUserId = 0;
                    member.AllowAssetUploads = "Y";
                    member.AllowForumUse = "Y";
                    member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

                    communityFacade.InsertCommunityMember(member);

                    // Insert "Worlds to Templates" record
                    InsertWorldsToTemplate(communityId, template.TemplateId);

                    // Insert A/B test associations for the new world
                    ExperimentFacade experimentFacade = new ExperimentFacade();
                    IList<Experiment> activeExperiments = experimentFacade.GetAvailableExperimentsByWorldContext(communityId, 6, Experiment.ASSIGN_ON_CREATION_ONLY, true);

                    foreach (Experiment experiment in activeExperiments)
                    {
                        string assignedGroup = experimentFacade.AssignParticipant(experiment);
                        if (!string.IsNullOrWhiteSpace(assignedGroup))
                        {
                            experimentFacade.SaveWorldExperimentGroup(new WorldExperimentGroup(string.Empty, communityId, 6, assignedGroup, DateTime.Now, null));
                        }
                    }

                    // If the template is associated with a custom deed, pre-create the channel_zone record and apply
                    if (template.GlobalId > 0)
                    {
                        int newZoneIndex = shoppingFacade.MakeZoneIndex(Convert.ToInt32(drCustomDeedUsage["zone_index_plain"]), 6);
                        int czRecordsInserted = shoppingFacade.InsertChannelZone(currentUser.UserId, newZoneIndex, communityId, community.Name,
                            0, "NOT_TIED", currentUser.Country, currentUser.Age, 0);

                        if (czRecordsInserted <= 0)
                        {
                            errorMsg = "Unable to create world.";
                            errorCode = -22;
                            communityId = 0;
                        }
                        else
                        {
                            int result = 0;
                            string urlResult = "";

                            // Call Move objects, populate the zone
                            shoppingFacade.ChangeZoneMap(newZoneIndex, communityId, newZoneIndex, currentUser.Username, template.GlobalId,
                                wokGameName, ref result, ref urlResult);
                        }
                    }
                }
                else
                {
                    errorMsg = "Unable to find world";
                    errorCode = -11;
                }
            }

            return communityId;
        }

        /// <summary>
        /// Called to create a 3dapp from a world template.
        /// </summary>
        /// <returns>The community id of the newly created 3dapp.</returns>
        private int Create3DAppFromTemplate(WorldTemplate template, string title, string urlSafeTitle, string description, string urlSafeDescription, User currentUser, out int errorCode, out string errorMsg)
        {
            Int64 taskId = 0;
            UInt32 errParsingFailed = 0xFFFFFFFF;
            errorCode = 0;
            errorMsg = string.Empty;

            try
            {
                string result = CreateApp(urlSafeTitle, urlSafeDescription, currentUser.UserId, template.TemplateId);

                //<result>TaskId</result>
                //
                XmlDocument doc = new XmlDocument();
                System.IO.StringReader sr = new System.IO.StringReader(result);
                doc.Load(sr);

                XmlNode root = doc.DocumentElement;

                if (root.Name.ToUpper().Equals("ERROR"))
                {
                    m_logger.Error("Error returned from App Create " + result);
                    errorCode = -15;
                    errorMsg = "Error creating World - " + root.InnerText;
                    return 0;
                }
                else
                {

                    taskId = Convert.ToInt64(root.InnerText);

                    int maxSeconds = 300;
                    UInt32 err = 0;

                    // Now wait for task complete
                    for (int i = 0; i < maxSeconds; i++)
                    {
                        result = IsTaskCompleted(taskId);
                        sr = new System.IO.StringReader(result);
                        doc.Load(sr);
                        root = doc.DocumentElement;

                        if (root.Name.ToUpper().Equals("ERROR"))
                        {
                            m_logger.Error("Error returned from IsTaskCompleted " + result);
                            errorCode = -13;
                            errorMsg = "Error creating World - " + root.InnerText;
                            return 0;
                        }

                        if (root.InnerText.Equals("1"))
                        {
                            // Look up the community id via the name (Per Yangfeng)
                            CommunityFacade communityFacade = new CommunityFacade();
                            int communityId = communityFacade.GetCommunityIdFromName(title, false);

                            if (communityId <= 0)
                            {
                                m_logger.Error("3Dapp could not be found");
                                errorCode = -11;
                                errorMsg = "World could not be found";
                            }

                            return communityId;
                        }
                        else if (!root.InnerText.Equals("0"))
                        {
                            //An error has occurred
                            try
                            {
                                err = Convert.ToUInt32(root.InnerText);
                            }
                            catch (Exception)
                            {
                                err = errParsingFailed;
                            }

                            break;
                        }

                        System.Threading.Thread.Sleep(1000);
                    }

                    if (err == 0)
                    {
                        // We timed out waiting for return
                        m_logger.Error("Unable to get response from 3dapp host server");
                        errorCode = -12;
                        errorMsg = "Unable to get response from World host server";
                        return 0;
                    }
                    else
                    {
                        // error occurred
                        m_logger.Error(String.Format("3dapp host server returned error: (0x{0:x8}).", err));
                        errorCode = -6;
                        errorMsg = "World host server busy, please try again later. (0x{0:x8})";
                        return 0;
                    }
                }
            }
            catch (System.Net.WebException exc)
            {
                m_logger.Error("Error connecting to 3DApp host server", exc);
                errorCode = -10;
                errorMsg = "Error connecting to World host server";
                return 0;
            }
            catch (Exception exc)
            {
                m_logger.Error("Bad response from 3dapp host server", exc);
                errorCode = -11;
                errorMsg = "Bad response from World host server";
                return 0;
            }
        }

        #endregion

        public PagedList<WOKItem> GetScriptAvailableItems(int pageNumber, int pageSize)
        {
            return gameDao.GetScriptAvailableItems(pageNumber, pageSize);
        }

        public int InsertScriptAvailableItemsIds(int[] globalIds)
        {
            return gameDao.InsertScriptAvailableItemsIds(globalIds);
        }

        public int DeleteScriptAvailableItemsIds(int[] globalIds)
        {
            return gameDao.DeleteScriptAvailableItemsIds(globalIds);
        }

        public DataSet GetPremiumItems(int premiumItemTypeId)
        {
            return gameDao.GetPremiumItems(premiumItemTypeId);
        }

        public bool ModifyDefaultSGCustomDataItem(SGCustomDataItem sgCustomDataItem, bool updateTemplates = false)
        {
            return sgCustomDataDao.ModifyDefaultSGCustomDataItem(sgCustomDataItem, updateTemplates);
        }

        /// <summary>
        /// Adds starting_script_game_custom_data to a deed. 
        /// </summary>
        /// <returns>False if an error occurred in processing, True otherwise.</returns>
        public bool AddSGCustomDataToCustomDeed(int zoneInstanceId, int zoneType, int templateGlobalId)
        {
            return sgCustomDataDao.AddSGCustomDataToCustomDeed(zoneInstanceId, zoneType, templateGlobalId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>False if an error occurred in processing, True otherwise.</returns>
        public bool ConfigureTryOnZoneSGCustomData(int zoneInstanceId, int zoneType)
        {
            PagedList<SGCustomDataItem> dtCustomData = sgCustomDataDao.GetWorldSGCustomData(zoneInstanceId, zoneType, " AND attribute = 'WorldSettings' ", string.Empty, 1, 1);

            // If there is no "WorldSettings" custom data, this is not a game world and doesn't need to be processed.
            if (dtCustomData == null || dtCustomData.Count == 0)
            {
                return true;
            }

            // Decompose WorldSettings JSON
            JObject jObject = JsonConvert.DeserializeObject<JObject>(dtCustomData[0].Value);
            jObject["playerBuild"] = false;

            bool creatorModeOk = sgCustomDataDao.AddSGCustomDataToWorld(zoneInstanceId, zoneType, "CreatorModeEnabled", "false");
            bool worldSettingsOk = sgCustomDataDao.AddSGCustomDataToWorld(zoneInstanceId, zoneType, "WorldSettings", Common.StripExcessJSONFormatting(jObject.ToString()));

            return creatorModeOk && worldSettingsOk;
        }

        public PagedList<SGCustomDataItem> GetDefaultSGCustomData(string orderBy, int pageNumber, int pageSize)
        {
            return sgCustomDataDao.GetDefaultSGCustomData(orderBy, pageNumber, pageSize);
        }

        public SGCustomDataItem GetDefaultSGCustomDataItem(string attribute)
        {
            return sgCustomDataDao.GetDefaultSGCustomDataItem(attribute);
        }

        public bool DeleteDefaultSGCustomDataItem(SGCustomDataItem sgCustomDataItem, bool removeFromTemplates = false)
        {
            return sgCustomDataDao.DeleteDefaultSGCustomDataItem(sgCustomDataItem, removeFromTemplates);
        }

        public bool DeleteSGCustomDataFromCustomDeed(int deedTemplateId)
        {
            return sgCustomDataDao.DeleteSGCustomDataFromCustomDeed(deedTemplateId);
        }

        public bool AddSGFrameworkSettingsToCustomDeed(int zoneInstanceId, int zoneType, int templateGlobalId)
        {
            return sgFrameworkSettingsDao.AddSGFrameworkSettingsToCustomDeed(zoneInstanceId, zoneType, templateGlobalId);
        }

        public SGFrameworkSettings GetWorldSGFrameworkSettings(int zoneInstanceId, int zoneType)
        {
            return sgFrameworkSettingsDao.GetWorldSGFrameworkSettings(zoneInstanceId, zoneType);
        }

        public bool DeleteSGFrameworkSettingsFromCustomDeed(int deedTemplateId)
        {
            return sgFrameworkSettingsDao.DeleteSGFrameworkSettingsFromCustomDeed(deedTemplateId);
        }

        public MetaGameItem GetMetaGameItem(string itemName)
        {
            return gameDao.GetMetaGameItem(itemName);
        }

        public List<MetaGameItem> GetMetaGameItems(string itemType)
        {
            return gameDao.GetMetaGameItems(itemType);
        }

        public bool SetPlayNowStartedKey(string playNowGuid)
        {
            return CentralCache.Add(CentralCache.keyPlayNowGuid(playNowGuid), true, new TimeSpan(0, 1, 0));
        }

        public bool CheckPlayNowStartedKey(string playNowGuid)
        {
            if (CentralCache.Get(CentralCache.keyPlayNowGuid(playNowGuid)) != null)
            {
                CentralCache.Remove(CentralCache.keyPlayNowGuid(playNowGuid));
                return true;
            }

            return false;
        }

        public List<TopWorld> GetTopWorlds(int userId, bool hideAP, string startTimeOffset, int wokGameId)
        {
            List<TopWorld> lstTopWorlds = new List<TopWorld>();

            // Get events occuring now that user has accepted invite to                 "-5 MINUTE"
            DataTable dtEvents = eventDao.GetEventsUserAcceptedByStartTimeOffset(userId, startTimeOffset);
            foreach (DataRow dr in dtEvents.Rows)
            {
                TopWorld tw = new TopWorld();
                tw.community_id = Convert.ToInt32(dr["community_id"]);
                tw.game_id = Convert.ToInt32(dr["game_id"]);
                tw.image_path = dr["thumbnail_small_path"].ToString();
                tw.location = dr["location"].ToString();
                tw.population = Convert.ToInt32(dr["population"]);

                lstTopWorlds.Add(tw);
            }

            // Get Most Visited worlds
            if (lstTopWorlds.Count < 5)
            {
                DataTable dtMostVisited = GetMostVisited(userId, hideAP, 1, 10, 10, wokGameId, true);
                if (dtMostVisited.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtMostVisited.Rows)
                    {
                        if (dr["population"] != null)
                        {
                            try
                            {
                                // Only user worlds that are currently populated (population > 0)
                                int pop = Convert.ToInt32(dr["population"]);
                                if (pop > 0)
                                {
                                    // Add world to list
                                    TopWorld tw = new TopWorld();
                                    tw.community_id = Convert.ToInt32(dr["community_id"]);
                                    tw.game_id = Convert.ToInt32(dr["game_id"]);
                                    tw.image_path = dr["image_path"].ToString();
                                    tw.location = dr["location"].ToString();
                                    tw.population = pop;

                                    if (!lstTopWorlds.Exists(x => x.community_id == tw.community_id))
                                    {
                                        lstTopWorlds.Add(tw);
                                    }

                                    // If list has 5 worlds, stop looking for more worlds to add
                                    if (lstTopWorlds.Count == 5)
                                    {
                                        break;
                                    }
                                }
                            }
                            catch { }
                        }
                    }
                }
            }

            if (lstTopWorlds.Count < 5)
            {
                // Get Most Populated Worlds to fill out the list if needed
                int totalNumRecords = 0;
                DataTable dtMostPopulated = gameDao.MostPopulatedCombined(false, hideAP, userId, ref totalNumRecords, 1, 5, wokGameId, "", "", 0, 0, -1);

                foreach (DataRow dr in dtMostPopulated.Rows)
                {
                    // Add world to list
                    TopWorld tw = new TopWorld();
                    tw.community_id = Convert.ToInt32(dr["community_id"]);
                    tw.game_id = Convert.ToInt32(dr["game_id"]);
                    tw.image_path = dr["thumbnail_small_path"].ToString();
                    tw.location = dr["name"].ToString();
                    tw.population = Convert.ToInt32(dr["population"]);

                    if (!lstTopWorlds.Exists(x => x.community_id == tw.community_id))
                    {
                        lstTopWorlds.Add(tw);
                    }

                    // If list has 5 worlds, stop looking for more worlds to add
                    if (lstTopWorlds.Count == 5)
                    {
                        break;
                    }
                }
            }

            return lstTopWorlds;
        }

        public class TopWorld
        {
            private Int32 _communityId = 0;
            private Int32 _gameId = 0;
            private Int32 _population = 0;
            private string _imagePath = "";
            private string _location = "";

            public TopWorld() { }

            public Int32 community_id
            {
                get { return _communityId; }
                set { _communityId = value; }
            }
            public Int32 game_id
            {
                get { return _gameId; }
                set { _gameId = value; }
            }
            public Int32 population
            {
                get { return _population; }
                set { _population = value; }
            }
            public string image_path
            {
                get { return _imagePath; }
                set { _imagePath = value; }
            }
            public string location
            {
                get { return _location; }
                set { _location = value; }
            }
        }

        // ***********************************************
        // World Template Object Functions
        // ***********************************************
        #region Game Leaderboard Functions

        public int InsertObjectUserInteraction(int userId, int zoneInstanceId, int zoneType, int objPlacementId, string name, string interactionType)
        {
            // If the user has not, then apply the rave
            if (CanUserInteractWithGameObject(userId, objPlacementId, interactionType))
            {
                // raveFacade call returns 0 if successful
                int retValue = gameDao.InsertObjectUserInteraction(userId, zoneInstanceId, zoneType, objPlacementId, name, interactionType);

                if (retValue == 1)
                {
                    // Success
                    return 0;
                }
                else
                {
                    // DB insert failed
                    return -2;
                }
            }
            else
            {
                // User has already raved this item
                return -1;
            }
        }

        public int UpdateLeaderboardObjectName(int objPlacementId, string name)
        {
            return gameDao.UpdateLeaderboardObjectName(objPlacementId, name);
        }

        private bool CanUserInteractWithGameObject(int userId, int objPlacementId, string interactionType)
        {
            if (interactionType == "VISIT")
            {
                return gameDao.HasUserVisitedGameObject(userId, objPlacementId);
            }
            else
            {
                return !gameDao.HasUserRavedGameObject(userId, objPlacementId);
            }
        }

        public int UpdateTeamMembers(List<int> userIds, int objPlacementId)
        {
            return gameDao.UpdateTeamMembers(userIds, objPlacementId);
        }

        public DataTable GetTeamMembers(IEnumerable<int> objPlacementIds)
        {
            return gameDao.GetTeamMembers(objPlacementIds);
        }

        public DataTable GetTeamMembers(int objPlacementId)
        {
            return gameDao.GetTeamMembers(objPlacementId);
        }

        public DataTable GetLeaderboard(int zoneInstanceId, int zoneType, int ravePointValue, int visitPointValue)
        {
            return gameDao.GetLeaderboard(zoneInstanceId, zoneType, ravePointValue, visitPointValue);
        }

        public DataTable GetFinalLeaderboard(int zoneInstanceId, int zoneType)
        {
            return gameDao.GetFinalLeaderboard(zoneInstanceId, zoneType);
        }

        public DataTable GetFinalLeaderboardTeamMembers(int zoneInstanceId, int zoneType)
        {
            return gameDao.GetFinalLeaderboardTeamMembers(zoneInstanceId, zoneType);
        }

        public DataTable GetUserLeaderboardAwards(int zoneInstanceId, int zoneType, int userId, string username)
        {
            List<int> skippedAwards = new List<int>();
            DataTable awards = gameDao.GetUserLeaderboardAwards(zoneInstanceId, zoneType, userId, username);

            if (awards != null && awards.Rows.Count > 0)
            {
                DateTime awardDate;
                DataRow prevRow;
                Dictionary<DateTime, DataRow> highestRows = new Dictionary<DateTime, DataRow>();

                // Keep only the highest ranking for the given award batch (determined by date awarded)
                awards.AcceptChanges();
                foreach (DataRow row in awards.Rows)
                {
                    awardDate = DateTime.Parse(row["date_awarded"].ToString());
                    awardDate = awardDate.Date;

                    if (highestRows.TryGetValue(awardDate, out prevRow))
                    {
                        if (Convert.ToInt32(row["ranking"]) < Convert.ToInt32(prevRow["ranking"]))
                        {
                            skippedAwards.Add(Convert.ToInt32(prevRow["world_leaderboard_id"]));
                            prevRow.Delete();
                            highestRows[awardDate] = row;
                        }
                        else
                        {
                            skippedAwards.Add(Convert.ToInt32(row["world_leaderboard_id"]));
                            row.Delete();
                        }
                    }
                    else
                    {
                        highestRows[awardDate] = row;
                    }
                }
                awards.AcceptChanges();
            }

            // Clean up any award ids from the DB if necessary
            if (skippedAwards.Count > 0)
            {
                gameDao.RedeemUserLeaderboardAward(userId, skippedAwards);
            }

            return awards;
        }

        public bool RedeemUserLeaderboardAward(int userId, int worldLeaderboardId)
        {
            return gameDao.RedeemUserLeaderboardAward(userId, worldLeaderboardId);
        }

        public DataTable CreateLeaderboardFinalResults(int ravePointValue, int visitPointValue)
        {
            return gameDao.CreateLeaderboardFinalResults(ravePointValue, visitPointValue);
        }

        public int InsertLeaderboardFinalResults(int objPlacementId, int zoneInstanceId, int zoneType, string name, int ranking, int points)
        {
            return gameDao.InsertLeaderboardFinalResults(objPlacementId, zoneInstanceId, zoneType, name, ranking, points);
        }

        public int InsertLeaderboardFinalTeamMembers(DataTable dtMembers, int worldLeaderboardId, int objPlacementId, int awardQuantity)
        {
            return gameDao.InsertLeaderboardFinalTeamMembers(dtMembers, worldLeaderboardId, objPlacementId, awardQuantity);
        }

        public int UpdateObjectInteractionsAsProcessed()
        {
            return gameDao.UpdateObjectInteractionsAsProcessed();
        }

        public int CleanupExpiredLeaderboards()
        {
            int recordsAffected = gameDao.CleanupTeamMembersHistory();
            recordsAffected += gameDao.CleanupWorldLeaderboards();
            recordsAffected += gameDao.CleanupLeaderboardVisits();

            return recordsAffected;
        }

        #endregion Game Leaderboard Functions

        public DataTable GetLinkedZones(int communityId, int zoneInstanceId, int zoneType, int wokGameId)
        {
            return gameDao.GetLinkedZones(communityId, zoneInstanceId, zoneType, wokGameId);
        }

        public bool IsChildZone(int zoneInstanceId, int zoneType, out int parentCommunityId)
        {
            return gameDao.IsChildZone(zoneInstanceId, zoneType, out parentCommunityId);
        }

        public bool IsParentZone(int zoneInstanceId, int zoneType)
        {
            int communityId = (new CommunityFacade()).GetCommunityIdFromWorldIds(0, 0, 0, zoneInstanceId, zoneType);
            DataTable dt = GetLinkedZones(communityId, zoneInstanceId, zoneType, 0);
            // GetLinkedZones returns the parent as well as the children. Needs to be > 1 to truly be a parent.
            return dt != null && dt.Rows.Count > 1;
        }

        public DataTable GetTutorial(string testGroupId)
        {
            return gameDao.GetTutorial(testGroupId);
        }

        public bool CanWorldDropGemChests(int userId, int communityId)
        {
            return gameDao.CanWorldDropGemChests(userId, communityId);
        }

        public int ArchiveInactiveWorld(int communityId, string archivedVia)
        {
            return gameDao.ArchiveInactiveWorld(communityId, archivedVia);
        }

        public int UnarchiveInactiveWorld(int communityId)
        {
            return gameDao.UnarchiveInactiveWorld(communityId);
        }

        public PagedList<ArchivedWorld> GetArchivedWorlds(string worldName, string creatorName, string orderBy,
            int pageNum, int itemsPerPage)
        {
            return gameDao.GetArchivedWorlds(worldName, creatorName, orderBy, pageNum, itemsPerPage);
        }
    }
}
