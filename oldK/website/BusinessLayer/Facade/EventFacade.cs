///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;
using System.Data;
using System.Linq;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;

namespace Kaneva.BusinessLayer.Facade
{
    [DataObject(true)]
    public class EventFacade
    {
        private CommunityFacade communityFacade = new CommunityFacade ();
        private UserFacade userFacade = new UserFacade ();
        private IEventDao eventDao = DataAccess.EventDao;
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Insert an event
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertEvent(int communityId, int userId, string title, string details, string location, int zoneIndex, int zoneInstanceId, DateTime startTime, DateTime endTime, int typeId, int timeZoneId, int premium, int privacy, bool isAPEvent, string trackingRequestGUID)
        {
            return eventDao.InsertEvent(communityId, userId, title, details, location, zoneIndex, zoneInstanceId, startTime, endTime, typeId, timeZoneId, premium, privacy, isAPEvent, trackingRequestGUID);
        }

        /// <summary>
        /// Insert an event
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int InsertEvent (Event evt, string trackingRequestGUID)
        {
            return eventDao.InsertEvent (evt, trackingRequestGUID);
        }

        /// <summary>
        /// Update an event
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int UpdateEvent (Event evt)
        {
            return eventDao.UpdateEvent (evt);
        }

        /// <summary>
        /// Update an event
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int UpdateEvent (int eventId, int communityId, string title, string details, string location,
            DateTime startTime, DateTime endTime, int typeId, int privacy, int premium, int timeZoneId, bool isAPEvent)
        {
            return eventDao.UpdateEvent (eventId, communityId, title, details, location, startTime, endTime, typeId, privacy, premium, timeZoneId, isAPEvent);
        }

        /// <summary>
        /// Gets location detail
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public LocationDetail GetLocationDetail(int zone_index, int zone_instance_id, int creator_id, int community_id)
        {
            return eventDao.GetLocationDetail (zone_index, zone_instance_id, creator_id, community_id);
        }

        [DataObjectMethod (DataObjectMethodType.Insert)]
        public Event GetEvent (int eventId)
        {
            return eventDao.GetEvent (eventId);
        }

        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<Event> GetWorldEvents (int communityId, int startTimeFilter, string orderBy, int pageNumber, int pageSize)
        {
            return GetWorldEvents (communityId, startTimeFilter, DateTime.MinValue, orderBy, pageNumber, pageSize);
        }

        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<Event> GetWorldEvents (int communityId, int startTimeFilter, DateTime startTime, string orderBy, int pageNumber, int pageSize)
        {    
            return eventDao.GetWorldEvents (communityId, startTimeFilter, startTime, orderBy, pageNumber, pageSize, 0);
        }
        public PagedList<Event> GetWorldEvents (int communityId, int startTimeFilter, DateTime startTime, string orderBy, int pageNumber, int pageSize, int recurringEventParentId)
        {
            return eventDao.GetWorldEvents (communityId, startTimeFilter, startTime, orderBy, pageNumber, pageSize, recurringEventParentId);
        }

        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<Event> GetEvents (string filter, string orderBy, int pageNumber, int pageSize)
        {
            return eventDao.GetEvents (filter, orderBy, pageNumber, pageSize);
        }

        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<Event> GetEventsByTimestampOffset(DateTime currentTime, int minuteOffset, string orderBy, int pageNumber, int pageSize)
        {
            return eventDao.GetEventsByTimestampOffset(currentTime, minuteOffset, orderBy, pageNumber, pageSize);
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<EventReminder> GetEventReminders(string orderBy, int pageNumber, int pageSize)
        {
            return eventDao.GetEventReminders(orderBy, pageNumber, pageSize);
        }

        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int LogEventReminderNotification(int eventId, int reminderId, DateTime sentDate)
        {
            return eventDao.LogEventReminderNotification(eventId, reminderId, sentDate);
        }

        /// <summary>
        /// ConcurrencyCheck
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public int ConcurrencyCheck(int eventId, int userId, int communityId, DateTime startDate, DateTime endDate, int zone_index, int zone_instance_id)
        {
            try
            {
                // Test for more than 1 event at the time specified in the location specified.               
                DataTable dtEv = eventDao.GetUserCreatedEvents(userId, 3, "", 1, 20);
                
                for (int i = 0; i < dtEv.Rows.Count; i++)
                {
                    // Don't check it against itself. If an update this will not be 0
                    if (!Convert.ToInt32(dtEv.Rows[i]["event_id"]).Equals(eventId))
                    {
                        DateTime dtStartTime = Convert.ToDateTime(dtEv.Rows[i]["start_time"]);
                        DateTime dtEndTime = Convert.ToDateTime(dtEv.Rows[i]["end_time"]);

                        m_logger.Debug("Proposed event: Start Time:" + startDate + " End Time:" + endDate);
                        m_logger.Debug("Existing event: Start Time:" + dtStartTime + " End Time:" + dtEndTime);

                        // Start time check          
                        int startFail = 0;
                        if (dtStartTime <= startDate && dtEndTime >= startDate)
                        {
                            m_logger.Debug("start time fail");
                            startFail = 1;
                        }

                        // End time check
                        int endFail = 0;
                        if (dtStartTime <= endDate && dtEndTime >= endDate)
                        {
                            m_logger.Debug("end time fail");
                            endFail = 1;
                        }

                        // Zone Check
                        int existingZIndex = Convert.ToInt32(dtEv.Rows[i]["zone_index"]);
                        int existingZInstanceId = Convert.ToInt32(dtEv.Rows[i]["zone_instance_id"]);
                        m_logger.Debug("Existing ZI:" + existingZIndex + " ZII:" + existingZInstanceId);


                        int zoneFail = 0;
                        if (existingZIndex == zone_index && existingZInstanceId == zone_instance_id)
                        {
                            m_logger.Debug("zone fail");
                            zoneFail = 1;
                        }

                        if ((startFail + endFail > 0) && zoneFail > 0)
                        {
                            m_logger.Debug("Failed.");
                            return 0;
                        }
                    }
                }
                return 1;
            }
            catch (Exception exc)
            {
                m_logger.Error("Event ConcurrencyCheck Error.", exc);
                return 2;
            }

        }

        public int ConcurrencyCheckRecurring(int eventId, int userId, int communityId, DateTime startDate, DateTime endDate, bool isRecurring)
        {
            DateTime conflictDate = new DateTime ();
            return ConcurrencyCheckRecurring (eventId, userId, communityId, startDate, endDate, isRecurring, 0, ref conflictDate);
        }

        public int ConcurrencyCheckRecurring(int eventId, int userId, int communityId, DateTime startDate, DateTime endDate, bool isRecurring, int recurringEventParentId)
        {
            DateTime conflictDate = new DateTime ();
            return ConcurrencyCheckRecurring (eventId, userId, communityId, startDate, endDate, isRecurring, recurringEventParentId, ref conflictDate);
        }

        public int ConcurrencyCheckRecurring (int eventId, int userId, int communityId, DateTime startDate, DateTime endDate, bool isRecurring, int recurringEventParentId, ref DateTime conflictDate)
        {
            Event.eSTART_TIME_FILTER startTimeFilter = Event.eSTART_TIME_FILTER.RECURRING_ALL;
            if (!isRecurring)
            {
                startTimeFilter = Event.eSTART_TIME_FILTER.RECURRING_PAST;
            }

            // If we are scheduling a recurring event via the event service, we pass in the recurringEventParentId so the parent 
            // event will be excluded from this events list.  If we did not exclude the parent then we would have a conflict
            // for the event time and this check would fail
            PagedList<Event> pdtEvents = GetWorldEvents (communityId, (int) startTimeFilter, startDate, "", 1, 20, recurringEventParentId);
            int communityOwnerId = new CommunityFacade().GetCommunity(communityId).CreatorId;

            // Counters
            int cntPriorRecurring = 0;
            int cntCurrDateNonRecurring = 0;
            int cntCurrDateRecurring = 0;
            int cntFutureRecurringPlus7 = 0;
            int cntFutureRecurringPlus14 = 0;
            int cntFutureNonRecurringPlus7 = 0;
            int cntFutureNonRecurringPlus14 = 0;

            // For existing events the user is editing, it does not have the settings that were changed by the user because 
            // those changes have not been saved yet. If that event is returned from the query then we will remove it.  Get the
            // Event from the db and then modify it to match the settings from the edit page, then put it back in the
            // data table so it can be checked for concurrency conflicts.
            if (eventId > 0)
            {
                Event e = GetEvent (eventId);                   
                int i = 0;
                bool match = false;
                foreach (Event ev in pdtEvents.List)
                {
                    if (ev.EventId == e.EventId)
                    {
                        match = true;
                        break;
                    }
                    i++;
                }

                if (match)
                {
                    pdtEvents.List.RemoveAt (i);
                }

                // Make modifications to match the edit page settings when user selected save
                e.StartTime = startDate;
                e.EndTime = endDate;
                e.RecurringStatusId = (int) Event.eRECURRING_STATUS_TYPE.NONE;
                if (isRecurring)
                {
                    e.RecurringStatusId = (int) Event.eRECURRING_STATUS_TYPE.INVITES_SENT;
                }
                // Add event back to the list with the current setting the user wants to use
                pdtEvents.Insert (0, e);
            }
            else
            {
                Event e = new Event ();
                // Make modifications to match the edit page settings when user selected save
                e.UserId = userId;
                e.StartTime = startDate;
                e.EndTime = startDate.AddHours(1);
                e.RecurringStatusId = (int) Event.eRECURRING_STATUS_TYPE.NONE;
                if (isRecurring)
                {
                    e.RecurringStatusId = (int) Event.eRECURRING_STATUS_TYPE.INVITES_SENT;
                }
                // Add event back to the list with the current setting the user wants to use
                pdtEvents.Insert (0, e);
            }

            Dictionary<Int32, SByte> dictCurrDayEvent = new Dictionary<Int32, SByte>{};

            foreach (Event e in pdtEvents)
            {
                // Start time check          
                int startFail = 0;
                int endFail = 0;
                conflictDate = DateTime.MaxValue;
                
                // Is this event a prior scheduled event that is recurring?
                if (e.StartTime.Date < startDate.Date && e.IsRecurring) 
                {
                    cntPriorRecurring++;
                }

                // Event occurs on same date as new/updated event
                if (e.StartTime.Date == startDate.Date)
                {
                    if (e.UserId.Equals(communityOwnerId))
                    {
                        // Is event the event that is being edited?
                        if (e.IsRecurring)
                        {
                            cntCurrDateRecurring++;
                        }
                        else
                        {
                            cntCurrDateNonRecurring++;
                        }
                    }
                    else
                    {
                        if (dictCurrDayEvent.ContainsKey(e.UserId))
                        {
                            dictCurrDayEvent[e.UserId] += 1;
                        }
                        else
                        {
                            dictCurrDayEvent.Add(e.UserId, 1);
                        }
                    }
                }

                // Is this event scheduled in the future?
                if (e.StartTime.Date > startDate.Date)
                {
                    if (e.IsRecurring)
                    {
                        if (e.StartTime.Date == startDate.Date.AddDays (7))
                        {
                            cntFutureRecurringPlus7++;
                        }
                        else
                        {
                            cntFutureRecurringPlus14++;
                        }
                    }
                    else
                    {
                        if (e.StartTime.Date == startDate.Date.AddDays (7))
                        {
                            cntFutureNonRecurringPlus7++;    
                        }
                        else
                        {
                            cntFutureNonRecurringPlus14++;
                        }
                    }
                }

                // Check the counters for conflicts
                if (cntPriorRecurring + cntCurrDateRecurring + cntCurrDateNonRecurring > 2 ||
                    cntPriorRecurring + cntCurrDateRecurring + cntFutureNonRecurringPlus7 > 2 ||
                    cntPriorRecurring + cntCurrDateRecurring + cntFutureNonRecurringPlus14 > 2 ||
                    cntPriorRecurring + cntCurrDateRecurring + cntFutureRecurringPlus7 + cntFutureNonRecurringPlus7 > 2 ||
                    cntPriorRecurring + cntCurrDateRecurring + cntFutureRecurringPlus7 + cntFutureRecurringPlus14 + cntFutureNonRecurringPlus14 > 2)
                {
                    if (isRecurring)
                    {
                        conflictDate = e.StartTime;
                    }
                    
                    return (int) Event.eCONCURRENCY_ERROR.EXCEED_MAX_EVENTS_PER_DAY;
                }

                // Non-owner can only have 2 events per world and 
                // the total number of non-owner events for a world can not exceed 8
                if (dictCurrDayEvent.ContainsKey(userId))
                {
                    if (dictCurrDayEvent[userId] > 2)
                    {
                        return (int)Event.eCONCURRENCY_ERROR.EXCEED_MAX_EVENTS_PER_DAY;
                    }
                    else if (dictCurrDayEvent.Sum(x => x.Value) > 8)
                    {
                        return (int)Event.eCONCURRENCY_ERROR.EXCEED_MAX_WORLD_EVENTS_PER_DAY;
                    }
                }

                // For updated event, don't check against itself
                if (eventId != e.EventId)
                {
                    // Check to see if times overlap
                    if ((startDate.TimeOfDay >= e.StartTime.TimeOfDay && startDate.TimeOfDay < e.EndTime.TimeOfDay) ||
                         startDate.TimeOfDay == e.StartTime.TimeOfDay)
                    {
                        m_logger.Debug ("start time fail");
                        startFail = 1;
                    }
                    if (endDate.TimeOfDay > e.StartTime.TimeOfDay && endDate.TimeOfDay < e.EndTime.TimeOfDay)
                    {
                        m_logger.Debug ("end time fail");
                        endFail = 1;
                    }

                    // Check if something failed
                    if (startFail + endFail > 0)
                    {
                        if (isRecurring)
                        {
                            conflictDate = e.StartTime;
                        }

                        m_logger.Debug ("Failed.");
                        return (int) Event.eCONCURRENCY_ERROR.TIME_OVERLAP;
                    }
                }
            }

            return (int) Event.eCONCURRENCY_ERROR.NONE;
        }

        /// <summary>
        /// ConcurrencyCheck
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public int ConcurrencyCheck (int eventId, int communityId, DateTime startDate, DateTime endDate)
        {
            try                                    
            {
                // Test for more than 1 event at the time specified in the location specified.               
                PagedList<Event> pdtEvents = GetWorldEvents (communityId, (int) Event.eSTART_TIME_FILTER.TODAY, "", 1, 20);
                int cntEventsOnDate = 0;

                foreach (Event e in pdtEvents)
                {
                    // Don't check it against itself. If an update this will not be 0                                             
                    if (!e.EventId.Equals (eventId))
                    {
                        // Start time check          
                        int startFail = 0;
                        int endFail = 0;
                        
                        if (startDate >= e.StartTime && startDate < e.EndTime)
                        {
                            m_logger.Debug ("end time fail");
                            startFail = 1;
                        }
                        if (endDate > e.StartTime && endDate < e.EndTime)
                        {
                            m_logger.Debug ("end time fail");
                            endFail = 1;
                        }

                        // Check if something failed
                        if (startFail + endFail > 0)
                        {
                            m_logger.Debug ("Failed.");
                            return (int) Event.eCONCURRENCY_ERROR.TIME_OVERLAP;
                        }

                        if (e.StartTime.Date == startDate.Date)
                        {
                            cntEventsOnDate++;
                        }
                        else { cntEventsOnDate = 0; }

                        // Can't have more that 2 events 
                        if (cntEventsOnDate == 2)
                        {
                            return (int)Event.eCONCURRENCY_ERROR.EXCEED_MAX_EVENTS_PER_DAY;   
                        }
                    }
                } 
                return 0;
            }
            catch (Exception exc)
            {
                m_logger.Error ("Event ConcurrencyCheck Error.", exc);
                return (int) Event.eCONCURRENCY_ERROR.GENERAL;
            }
        }

        /// <summary>
        /// InsertEventInvitees
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int InsertEventInvitees (int communityId, int userId, int eventId, int inviteStatus, bool includeFriends)
        {
            return eventDao.InsertEventInvitees (communityId, userId, eventId, inviteStatus, includeFriends);
        }

        /// <summary>
        /// GetEventInviteesByStatus
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<EventInvitee> GetEventInviteesByStatus (int eventId, int inviteStatusId, string orderBy, int pageNumber, int pageSize)
        {
            return GetEventInviteesByStatus (eventId, inviteStatusId, false, orderBy, pageNumber, pageSize);
        }

        /// <summary>
        /// GetEventInviteesByStatus
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<EventInvitee> GetEventInviteesByStatus (int eventId, int inviteStatusId, bool includeEventOwner, string orderBy, int pageNumber, int pageSize)
        {
            return eventDao.GetEventInviteesByStatus (eventId, inviteStatusId, includeEventOwner, orderBy, pageNumber, pageSize);
        }

        /// <summary>
        /// GetAutomatedEventInvitees
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<EventInvitee> GetAutomatedEventInvitees(string orderBy, int pageNumber, int pageSize)
        {
            return eventDao.GetAutomatedEventInvitees(orderBy, pageNumber, pageSize);
        }

        /// <summary>
        /// GetEventInviteesByStatus
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int ChargeForPremiumEvent (int userId, string currency)
        {
            // Premium Event?
            int successfulPayment = -1;

            // Do they have enough money?
            try
            {
                if (currency.Equals ("GPOINT") || currency.Equals ("KPOINT"))
                {
                    successfulPayment = (new UserFacade ()).AdjustUserBalance (userId, currency, -200.0, (ushort)TransactionType.eTRANSACTION_TYPES.CASH_TT_EVENT);
                }
            }
            catch (Exception)
            {
                // Not enough credits/rewards
                return -1;
            }

            if (successfulPayment == 0)//0 is success according to AdjustUserBalance notes
            {
                return 1;
            }
            else
            {
                //another error occured.
                return 0;
            }
        }

        /// <summary>
        /// UpdateEventStatus
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int UpdateEventStatus (int eventId, int statusId, int userId)
        {
            eventDao.InsertEventAudit (eventId, userId, "Event cancelled");

            return eventDao.UpdateEventStatus (eventId, statusId);   
        }
        
        /// <summary>
        /// GetEventInvitee
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public EventInvitee GetEventInvitee (int userId, int eventId)
        {
            return eventDao.GetEventInvitee (userId, eventId);
        }

        /// <summary>
        /// InsertEventInvitee
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int InsertEventInvitee (int eventId, int userId, int eventStatusId)
        {
            return eventDao.InsertEventInvitee (eventId, userId, eventStatusId);
        }

        /// <summary>
        /// InsertComment
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int InsertComment (int eventId, string eventBlastId, int userId, string comment)
        {
            return eventDao.InsertComment (eventId, eventBlastId, userId, comment);
        }

        /// <summary>
        /// InsertEventBlast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Insert)]
        public int InsertEventBlast (int eventId, int userId, string comment)
        {
            return eventDao.InsertEventBlast (eventId, userId, comment);
        }
        
        /// <summary>
        /// GetEventBlast
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public EventBlast GetEventBlast (string eventBlastId)
        {
            return eventDao.GetEventBlast (eventBlastId);
        }

        /// <summary>
        /// GetEventBlasts
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<EventBlast> GetEventBlasts (int eventId, int pageNumber, int pageSize, string sortBy)
        {
            return eventDao.GetEventBlasts (eventId, pageNumber, pageSize, sortBy);
        }

        /// <summary>
        /// GetEventComments
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public PagedList<EventComment> GetEventComments (int eventId, string eventBlastId, int pageNumber, int pageSize, string sortBy, string groupBy)
        {
            return eventDao.GetEventComments (eventId, eventBlastId, pageNumber, pageSize, sortBy, groupBy);
        }

        /// <summary>
        /// DeleteEventComment
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Delete)]
        public int DeleteEventComment (int eventId, string eventBlastId, string eventBlastCommentId)
        {
            return eventDao.DeleteEventComment (eventId, eventBlastId, eventBlastCommentId);
        }

        /// <summary>
        /// UpdateEventInviteStatus
        /// </summary>
        [DataObjectMethod (DataObjectMethodType.Update)]
        public int UpdateEventInviteStatus (int eventId, int inviteeUserId, int newInviteStatus, bool isPremiumEvent)
        {
            if (eventId == 0 || inviteeUserId == 0 || newInviteStatus == 0)
            {
                return 0;
            }

            EventInvitee invitee = GetEventInvitee (inviteeUserId, eventId);

            // Check to see if an invite exists for this event
            if (invitee.UserId > 0 && invitee.InviteStatus == EventInvitee.Invite_Status.INVITED)
            {
                // Update the status of the invite 
                return eventDao.UpdateEventInviteStatus (eventId, inviteeUserId, newInviteStatus);
            }
            else if (isPremiumEvent &&
                invitee.InviteStatus == EventInvitee.Invite_Status.NOT_INVITED &&
                (newInviteStatus == (int)EventInvitee.Invite_Status.ACCEPTED || 
                newInviteStatus == (int)EventInvitee.Invite_Status.DECLINED)
                )
            {
                return InsertEventInvitee (eventId, inviteeUserId, newInviteStatus);
            }

            return 0;
        }

        /// <summary>
        /// UpdateEventInviteeAttendance
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateEventInviteeAttendance(int eventId, int inviteeUserId)
        {
            if (eventId == 0 || inviteeUserId == 0)
            {
                return 0;
            }

            EventInvitee invitee = GetEventInvitee(inviteeUserId, eventId);
            return eventDao.UpdateEventInviteeAttendance(eventId, inviteeUserId, (int)invitee.InviteStatus);
        }

        public bool IsUserAttendingEvent (int userId, int eventId)
        {
            EventInvitee ei = eventDao.GetEventInvitee (userId, eventId);

            return ei.InviteStatus == EventInvitee.Invite_Status.ACCEPTED;
        }

        [DataObjectMethod(DataObjectMethodType.Select)]
        public Event.EventMessage GetEventMessageDetails(Event.eMESSAGE_TYPE emt, Event evt, string fromUsername, string eventDetailPageUrl, string blastComment, string trackingMetric)
        {
            return GetEventMessageDetails(emt, evt, fromUsername, eventDetailPageUrl, blastComment, trackingMetric, string.Empty);
        }

        [DataObjectMethod (DataObjectMethodType.Select)]
        public Event.EventMessage GetEventMessageDetails (Event.eMESSAGE_TYPE emt, Event evt, string fromUsername, string eventDetailPageUrl, string blastComment, string trackingMetric, string playWoKUrl)
        {
            Event.EventMessage em = new Event.EventMessage ();
            string ampm = evt.StartTime.ToString ("tt").ToLower ();
            string dateTime = evt.StartTime.ToString ("dddd, MMMM ") + evt.StartTime.Day + " at " +
                string.Format ("{0:h:mm}{1}", evt.StartTime, ampm) + " EST";
            string communityName = communityFacade.GetCommunity (evt.CommunityId).Name;
            string communityNameFormatted = "<span class=\"bold\">" + communityName + "</span>";
            string eventDetailsBaseUrl = eventDetailPageUrl + "?event=" + evt.EventId;
            string eventDetailsUrl = "<a href=\"" + eventDetailsBaseUrl + "\" class=\"bold\">" + evt.Title + "</a>";
            string eventDetailsUrlTracking = "<a href=\"" + eventDetailsBaseUrl + "&RTSID=" + trackingMetric + "\" class=\"bold\">" + evt.Title + "</a>";
            string eventAcceptUrl = "<a href=\"" + eventDetailsBaseUrl + "&action=attend&RTSID=" + trackingMetric + "\" class=\"btnxsmall grey\">Attend</a>";
            string eventDeclineUrl = "<a href=\"" + eventDetailsBaseUrl + "&action=decline" + "\" class=\"decline\">Decline</a>";
            string eventPlayWoKUrl = "<a href=\"" + playWoKUrl + "&RTSID=" + trackingMetric + "\" class=\"btnmedium blue\">Play Now</a>";
            string userProfileUrl = "<a href=\"" + Common.GetPersonalChannelUrl (fromUsername) + "\" class=\"bold\">" + fromUsername + "</a>";
            
            if (emt == Event.eMESSAGE_TYPE.EventInvite)
            {
                em.Subject = fromUsername + " invited you to an event at Kaneva";
                em.Message = "<p>" + userProfileUrl + " invited you to " + eventDetailsUrl + " at " + communityNameFormatted + ".</p><br/><br/>" +
                    "<p>" + eventDetailsUrl + "</p><br/>" +
                    "<p>" + Common.TruncateWithEllipsis (evt.Details, 50) + "</p><br/>" +
                    "<p>" + dateTime + "</p><br/>" +
                    "<p>" + communityName + "</p><br/><br/>" +
                    "<p>" + eventAcceptUrl + eventDeclineUrl + "</p><br/><br/>" +
                    "<p>Earn 1000 Rewards a day for going to an event.</p>";
            }
            else if (emt == Event.eMESSAGE_TYPE.EventChangeNotification)
            {
                em.Subject = fromUsername + "'s event at Kaneva has changed";
                em.Message = "<p>" + userProfileUrl + " changed details of " + eventDetailsUrlTracking + " at " + communityNameFormatted + ".</p><br/><br/>" +
                    "<p>" + eventDetailsUrlTracking + "</p><br/>" +
                    "<p>" + Common.TruncateWithEllipsis (evt.Details, 50) + "</p><br/>" +
                    "<p>" + dateTime + "</p><br/>" +
                    "<p>" + communityName + "</p><br/><br/>" +
                    "<p>Earn 1000 Rewards a day for going to an event.</p>";
            }
            else if (emt == Event.eMESSAGE_TYPE.EventChangeInvite)
            {
                em.Subject = fromUsername + " changed their event " + evt.Title;
                em.Message = "<p>" + eventDetailsUrl + "</p><br/>" +
                   "<p>" + Common.TruncateWithEllipsis (evt.Details, 50) + "</p><br/>" +
                   "<p>" + dateTime + "</p><br/>" +
                   "<p>" + communityName + "</p><br/><br/>" +
                   "<p>" + eventAcceptUrl + eventDeclineUrl + "</p><br/><br/>" +
                   "<p>Earn 1000 Rewards a day for going to an event.</p>";
            }
            else if (emt == Event.eMESSAGE_TYPE.EventCancellation)
            {
                em.Subject = fromUsername + " cancelled their event " + evt.Title;
                em.Message = "<p>" + userProfileUrl + " cancelled their event " + eventDetailsUrl + ".</p>";
            }
            else if (emt == Event.eMESSAGE_TYPE.EventOwnerBlastNotification)
            {
                em.Subject = fromUsername + " blasted about their event " + evt.Title;
                em.Message = "<p>" + userProfileUrl + " blasted about their event " + eventDetailsUrl + ".</p><br/><br/>" +
                    "<p>" + userProfileUrl + " wrote: " + blastComment + "</p><br/><br/>" +
                    "<p><a href=\"" + eventDetailsBaseUrl + "&RTSID=" + trackingMetric + "\"  class=\"btnsmall grey\">See Comment</a></p>";
            }
            else if (emt == Event.eMESSAGE_TYPE.EventAttendeeBlastNotification)
            {
                em.Subject = fromUsername + " blasted about your event " + evt.Title;
                em.Message = "<p>" + userProfileUrl + " blasted about your event " + eventDetailsUrl + ".</p><br/><br/>" +
                    "<p>" + userProfileUrl + " wrote: " + blastComment + "</p><br/><br/>" +
                    "<p><a href=\"" + eventDetailsBaseUrl + "&RTSID=" + trackingMetric + "\"  class=\"btnsmall grey\">See Comment</a></p>";
            }
            else if (emt == Event.eMESSAGE_TYPE.EventBlastCommentNotification)  
            {
                em.Subject = fromUsername + " also commented on " + evt.Title;
                em.Message = "<p>" + userProfileUrl + " also commented on the event " + eventDetailsUrl + " blast.</p><br/><br/>" +
                    "<p>" + userProfileUrl + " wrote: " + blastComment + "</p><br/><br/>" +
                    "<p><a href=\"" + eventDetailsBaseUrl + "&RTSID=" + trackingMetric + "\"  class=\"btnsmall grey\">See Comment</a></p>";
            }
            else if (emt == Event.eMESSAGE_TYPE.EventReminderToday)
            {
                em.Subject = fromUsername + "'s event at " + communityNameFormatted + " is happening today";
                em.Message = "<p>" + userProfileUrl + "'s event is happening today!</p><br/><br/>" +
                    "<p>" + eventDetailsUrl + "</p><br/>" +
                    "<p>" + Common.TruncateWithEllipsis(evt.Details, 50) + "</p><br/>" +
                    "<p>" + dateTime + "</p><br/>" +
                    "<p>" + communityName + "</p><br/><br/>" +
                    "<p>" + eventPlayWoKUrl + "</p><br/><br/>" +
                    "<p>Earn 1000 Rewards a day for going to an event.</p>";
            }
            else if (emt == Event.eMESSAGE_TYPE.EventReminderNow)
            {
                em.Subject = fromUsername + "'s event at " + communityNameFormatted + " is starting now";
                em.Message = "<p>" + userProfileUrl + "'s event is starting now!</p><br/><br/>" +
                    "<p>" + eventDetailsUrl + "</p><br/>" +
                    "<p>" + Common.TruncateWithEllipsis(evt.Details, 50) + "</p><br/>" +
                    "<p>" + dateTime + "</p><br/>" +
                    "<p>" + communityName + "</p><br/><br/>" +
                    "<p>" + eventPlayWoKUrl + "</p><br/><br/>" +
                    "<p>Earn 1000 Rewards a day for going to an event.</p>";
            }

            em.Message = "<span class=\"eventmsg\">" + em.Message + "</span>";

            return em;
        }

        /// <summary>
        /// RewardUserForEvent
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int RewardUserForEvent(int userId, int eventId)
        {
            double rewardAmount = Configuration.GetEventRewardAmount;

            if (eventDao.IsUserAlreadyRewardedToday(userId))
            {
                return -99;
            }

            // Record Reward
            eventDao.InsertEventReward(userId, eventId, rewardAmount);

            int wok_transaction_log_id = 0;
            // Issue Reward
            int rc = new UserFacade().AdjustUserBalance(userId, Currency.CurrencyType.REWARDS, rewardAmount, (ushort)TransactionType.eTRANSACTION_TYPES.CASH_TT_EVENT, ref wok_transaction_log_id);

            if (wok_transaction_log_id > 0)
            {   // Update reward_log table so reward will show in users transactions page
                userFacade.InsertRewardLog (userId, rewardAmount, Currency.CurrencyType.REWARDS, (int)TransactionType.eTRANSACTION_TYPES.CASH_TT_EVENT, 15, wok_transaction_log_id); 
            }

            return rc;
        }

        /// <summary>
        /// DoesGameHaveActiveEvent
        /// </summary>
        /// <param name="gameId"></param>
        /// <returns></returns>
        public bool DoesCommunityHaveActiveEvent(int communityId)
        {
            return eventDao.DoesCommunityHaveActiveEvent(communityId);
        }

        public bool IsEventHappeningNow (Event evt)
        {
            if (evt.StartTime < DateTime.Now && evt.EndTime > DateTime.Now)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// EventInviteCounts
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        [DataObjectMethod (DataObjectMethodType.Select)]
        public EventInviteCounts GetEventInviteCounts (int eventId)
        {
            return eventDao.GetEventInviteCounts (eventId);
        }
    
        /// <summary>
        /// GetEventsUserAcceptedByStartTimeOffset
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="startTimeOffset"></param>
        /// <returns></returns>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetEventsUserAcceptedByStartTimeOffset(int userId, string startTimeOffset)
        {
            return eventDao.GetEventsUserAcceptedByStartTimeOffset(userId, startTimeOffset);
        }


        public List<Event> GetWorldEventsWithEventFlags(int communityId)
        {
            return GetWorldEventsWithEventFlags(communityId, string.Empty);
        }

        public List<Event> GetWorldEventsWithEventFlags(int communityId, string orderBy)
        {
            return eventDao.GetWorldEventsWithEventFlags(communityId, orderBy);
        }

        public Event GetEventByEventFlagPlacementId (int objPlacementId)
        {
            return eventDao.GetEventByEventFlagPlacementId(objPlacementId);
        }

        public void SendNewEventCreatedUpdateToUsers(Event evt, PagedDataTable users)
        {
            XmlEventCreator xml = new XmlEventCreator("ClientTickler");
            //first encode packet information
            xml.AddValue((int)NotificationsProfile.NotificationTypes.EventCreated);
            xml.AddValue(evt.EventId);
            xml.AddValue(evt.Title);
            xml.AddValue(evt.Details);
            xml.AddValue(evt.StartTime.ToString());
            xml.AddValue(evt.EndTime.ToString());
            xml.AddValue(evt.CommunityId);
            xml.AddValue(evt.ObjPlacementId);
            
            RemoteEventSender sender = new RemoteEventSender();

            for (int i = 0; i < users.Rows.Count; i++ )
            {
                xml.AddPlayer(Convert.ToInt32(users.Rows[i]["player_id"]));
                sender.BroadcastEventToServer(xml.GetXml()); //send event to all servers
            }
        }

    }
}
