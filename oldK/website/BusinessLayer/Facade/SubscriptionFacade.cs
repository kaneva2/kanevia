///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;
using System.Data;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;

using log4net;

namespace Kaneva.BusinessLayer.Facade
{

    [DataObject(true)]
    public class SubscriptionFacade
    {
        private ISubscriptionDao subscriptionDao = DataAccess.SubscriptionDao;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        
        // ***********************************************
        // Subscriptions
        // ***********************************************
        #region Subscription Functions

        /// <summary>
        /// GetUnpurchasedSubscriptionsList
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Subscription> GetUnpurchasedSubscriptionsList(int userID)
        {
            return subscriptionDao.GetUnpurchasedSubscriptionsList(userID);
        }

        /// <summary>
        /// GetUnpurchasedSubscriptions
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetUnpurchasedSubscriptions(int userID, string passGroupFilter)
        {
            return subscriptionDao.GetUnpurchasedSubscriptions(userID, passGroupFilter);
        }

       

        /// <summary>
        /// GetUnpurchasedSubscriptionsList
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Subscription> GetUnpurchasedSubscriptionsList(int userID, string filter)
        {
            return subscriptionDao.GetUnpurchasedSubscriptionsList(userID, filter);
        }

        /// <summary>
        /// GetSubscription
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Subscription GetSubscription(uint SubscriptionId)
        {
            return subscriptionDao.GetSubscription(SubscriptionId);
        }

        /// <summary>
        /// GetAllSubscriptions
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Subscription> GetAllSubscriptionsList()
        {
            return subscriptionDao.GetAllSubscriptionsList();
        }

        /// <summary>
        /// GetAllSubscriptions
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetAllSubscriptions()
        {
            return subscriptionDao.GetAllSubscriptions();
        }

        /// <summary>
        /// Get the Subscription by the UserSubscriptionId
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Subscription GetSubscriptionByUserSubscriptionId(uint userSubscriptionId)
        {
            return subscriptionDao.GetSubscriptionByUserSubscriptionId(userSubscriptionId);
        }

 
        /// <summary>
        /// Get the Active Subscriptions by UserId
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Subscription> GetUserActiveSubscriptions (int userId)
        {
            return subscriptionDao.GetUserActiveSubscriptions (userId);
        }
     
     
        /// <summary>
        /// GetSubscriptionsByPassGroupId
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Subscription> GetSubscriptionsByPassGroupId(uint passGroupId)
        {
            return GetSubscriptionsByPassGroupIds(passGroupId.ToString());
        }

        /// <summary>
        /// GetSubscriptionsByPassGroupIds
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Subscription> GetSubscriptionsByPassGroupIds(string passGroupIds)
        {
            return subscriptionDao.GetSubscriptionsByPassGroupIds(passGroupIds);
        }


        /// <summary>
        /// GetSubscriptionByPromotionId
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Subscription GetSubscriptionByPromotionId(uint promotionId)
        {
            return subscriptionDao.GetSubscriptionByPromotionId(promotionId);
        }

        /// <summary>
        /// GetSubscriptionsByPromotionId
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Subscription> GetSubscriptionsByPromotionId(uint promotionId)
        {
            return subscriptionDao.GetSubscriptionsByPromotionId(promotionId);
        }

        /// <summary>
        /// GetAvailableSubscriptions
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetAvailableSubscriptions()
        {
            return subscriptionDao.GetAvailableSubscriptions();
        }

        /// <summary>
        /// GetSubscriptionTerms
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetSubscriptionTerms()
        {
            return subscriptionDao.GetSubscriptionTerms();
        }

        /// <summary>
        /// GetSubscriptionTerms
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetSubscriptionStatus()
        {
            return subscriptionDao.GetSubscriptionStatus();
        }

        /// <summary>
        /// DeleteAllSubscription2PromotionByPromoID
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteAllSubscription2PromotionByPromoID(int promotion_id)
        {
            return subscriptionDao.DeleteAllSubscription2PromotionByPromoID(promotion_id);
        }

        /// <summary>
        /// DeleteAllSubscription2PromotionBySubscptID
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteAllSubscription2PromotionBySubscptID(int subscription_id)
        {
            return subscriptionDao.DeleteAllSubscription2PromotionBySubscptID(subscription_id);
        }

        /// <summary>
        /// DeleteSubscription
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteSubscription(int subscriptionId)
        {
            return subscriptionDao.DeleteSubscription(subscriptionId);
        }

        /// <summary>
        /// InsertSubscription2Promotion(int promotion_id, int subscription_id)
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertSubscription2Promotion(int promotion_id, int subscription_id)
        {
            return subscriptionDao.InsertSubscription2Promotion(promotion_id, subscription_id);
        }

        /// <summary>
        /// DeleteAllSubscription2PassGroupByPassGroupID
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteAllSubscription2PassGroupBySubscptID(uint subscriptionId)
        {
            return subscriptionDao.DeleteAllSubscription2PassGroupBySubscptID(subscriptionId);
        }

        /// <summary>
        /// DeleteAllSubscription2PassGroupByPassGroupID
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public int DeleteAllSubscription2PassGroupByPassGroupID(uint passGroupId)
        {
            return subscriptionDao.DeleteAllSubscription2PassGroupByPassGroupID(passGroupId);
        }

        /// <summary>
        /// InsertSubscription2PassGroup
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertSubscription2PassGroup(uint passGroupId, uint subscriptionId)
        {
            return subscriptionDao.InsertSubscription2PassGroup(passGroupId, subscriptionId);
        }

        /// <summary>
        /// InsertSubscription
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertSubscription(Subscription subscription)
        {
            return subscriptionDao.InsertSubscription(subscription);
        }

        /// <summary>
        /// UpdateSubscription
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateSubscription(Subscription subscription)
        {
            subscriptionDao.UpdateSubscription(subscription);
        }

        /// <summary>
        /// InsertUserSubscription
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public uint InsertUserSubscription(UserSubscription userSubscription)
        {
            return subscriptionDao.InsertUserSubscription(userSubscription);
        }

        /// <summary>
        /// GetUserSubscription
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public UserSubscription GetUserSubscription(uint SubscriptionId, uint userID)
        {
            return subscriptionDao.GetUserSubscription(SubscriptionId, userID);
        }

        /// <summary>
        /// GetUserSubscriptionByPromotionId
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public UserSubscription GetUserSubscriptionByPromotionId(uint promotionID, uint userID)
        {
            return subscriptionDao.GetUserSubscriptionByPromotionId(promotionID, userID);
        }

        /// <summary>
        /// GetUserSubscriptionByPromotionId
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<UserSubscription> GetUserSubscriptionsByPromotionIds(string promotionIDS, uint userID)
        {
            return subscriptionDao.GetUserSubscriptionsByPromotionIds(promotionIDS, userID);
        }

        /// <summary>
        /// GetUserSubscriptionByPassGroupId
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public UserSubscription GetUserSubscriptionByPassGroupId(uint passGroupId, uint userID)
        {
            return subscriptionDao.GetUserSubscriptionByPassGroupId(passGroupId, userID);
        }

        /// <summary>
        /// GetUserSubscription
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public UserSubscription GetUserSubscription(uint userSubscriptionId)
        {
            return subscriptionDao.GetUserSubscription(userSubscriptionId);
        }

        /// <summary>
        /// GetUserSubscriptions
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<UserSubscription> GetUserSubscriptions(int userId)
        {
            return subscriptionDao.GetUserSubscriptions(userId);
        }

        /// <summary>
        /// GetActiveUserSubscriptions
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<UserSubscription> GetActiveUserSubscriptions(int userId)
        {
            return subscriptionDao.GetActiveUserSubscriptions(userId);
        }

        
        /// <summary>
        /// UpdateUserSubscription
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public void UpdateUserSubscription (UserSubscription userSubscription)
        {
            subscriptionDao.UpdateUserSubscription (userSubscription);
        }

        /// <summary>
        /// GetSubscriptionEntitlements
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<SubscriptionEntitlement> GetSubscriptionEntitlements(int subscriptionId, string gender)
        {
            return subscriptionDao.GetSubscriptionEntitlements (subscriptionId, gender);
        }

        /// <summary>
        /// HasValidSubscription
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool HasValidSubscription(int userId, int subscriptionId)
        {
            return subscriptionDao.HasValidSubscription(userId, subscriptionId);
        }

        /// <summary>
        /// DoesUserAlreadyHaveThisPass
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool DoesUserAlreadyHaveThisPass(uint passGroupID, uint userID)
        {
            //see if the user has a valid associated subscription with this promotion
            return subscriptionDao.HasValidSubscriptionByPassType(userID, passGroupID);
        }

        /// <summary>
        /// GetPassGroupsAssociatedWSubscription
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetPassGroupsAssociatedWSubscription(uint subscriptionId)
        {
            //see if the user has a valid associated subscription with this promotion
            return subscriptionDao.GetPassGroupsAssociatedWSubscription(subscriptionId);
        }

        /// <summary>
        /// GetPassGroupsToSubscription
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DataTable GetPassGroupsToSubscription()
        {
            //see if the user has a valid associated subscription with this promotion
            return subscriptionDao.GetPassGroupsToSubscription();
        }

        /// <summary>
        /// DoesUserCurrentlyHaveThisPromotion - checks to see if the user has a subscription based on the provided promotion
        /// that is still active
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool DoesUserCurrentlyHaveThisPromotion(uint promotionID, uint userID)
        {
            bool userHasActivePromotion = false;

            //get the user subscription associated with this promotion if any
            UserSubscription usersSubscription = GetUserSubscriptionByPromotionId(promotionID, userID);
            
            //if a subscription was found see if it is still active
            if ((usersSubscription != null) && (usersSubscription.UserSubscriptionId > 0))
            {
                userHasActivePromotion = usersSubscription.EndDate > DateTime.Now;
            }

            return userHasActivePromotion;
        }


        /// <summary>
        /// HasUserHadThisPassBefore
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool HasUserHadThisPromotionBefore(uint promotionID, uint userID)
        {
            bool userHasHad = false;

            UserSubscription usersSubscription = GetUserSubscriptionByPromotionId(promotionID,userID);
            if ((usersSubscription != null) && (usersSubscription.UserSubscriptionId > 0))
            {
                userHasHad = true;
            }

            return userHasHad;
        }

        /// <summary>
        /// InsertUserSubscriptionOrder
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertUserSubscriptionOrder(uint usersubscriptionId, int orderId)
        {
            return subscriptionDao.InsertUserSubscriptionOrder(usersubscriptionId, orderId);
        }

        /// <summary>
        /// GetNextEndDate
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public DateTime GetNextEndDate(DateTime purchaseDate, DateTime currentEndDate, Subscription.SubscriptionTerm subscriptionTerm)
        {
            return subscriptionDao.GetNextEndDate(purchaseDate, currentEndDate, subscriptionTerm);
        }

        #endregion
    }
}
