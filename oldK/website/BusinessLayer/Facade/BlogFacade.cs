///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Text;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;
//using Kaneva.Framework.Transactions;

namespace Kaneva.BusinessLayer.Facade
{
    public class BlogFacade
    {
        private IBlogDao blogDao = DataAccess.BlogDao;

        /// <summary>
        /// GetBlog
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public Blog GetBlog(int blogId)
        {
            return blogDao.GetBlog (blogId);
        }

        /// <summary>
        /// GetBlogs
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Blog> GetBlogs(int communityId, string filter, string orderby, int pageNumber, int pageSize)
        {
            return blogDao.GetBlogs(communityId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// GetAllUserBlogs
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<Blog> GetAllUserBlogs(int userId, string filter, string orderby, int pageNumber, int pageSize)
        {
            return blogDao.GetAllUserBlogs(userId, filter, orderby, pageNumber, pageSize);
        }

        /// <summary>
        /// Insert a new blog
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertBlog(int communityId, string subject, string bodyText, int userId, string createdUsername, string ipAddress, string keywords)
        {
            return blogDao.InsertBlog(communityId, subject, bodyText, userId, createdUsername, ipAddress, keywords);
        }

        /// <summary>
        /// Update a blog
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateBlog(int userId, int blogId, string subject, string bodyText, int statusId, string keywords)
        {
            return blogDao.UpdateBlog(userId, blogId, subject, bodyText, statusId, keywords);
        }

  

        /// <summary>
        /// DeleteBlog
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public void DeleteBlog(int blogId, int userId, bool bPermanent)
        {
            blogDao.DeleteBlog(blogId, userId, bPermanent);
        }

        /// <summary>
        /// DeleteBlog
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public void DeleteBlog(int blogId, int userId)
        {
            blogDao.DeleteBlog(blogId, userId);
        }

        /// <summary>
        /// IsBlogOwner
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public bool IsBlogOwner(int userId, int blogId)
        {
            return blogDao.IsBlogOwner(userId, blogId);
        }

        /// <summary>
        /// InsertBlogComment
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public int InsertBlogComment(int blogId, int userId, string bodyText, string ipAddress)
        {
            return blogDao.InsertBlogComment(blogId, userId, bodyText, ipAddress);
        }

        /// <summary>
        /// UpdateBlogComment
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update)]
        public int UpdateBlogComment(int blogCommentId, int userId, string bodyText, string ipAddress)
        {
            return blogDao.UpdateBlogComment(blogCommentId, userId, bodyText, ipAddress);
        }

        /// <summary>
        /// GetBlogComment
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public BlogComment GetBlogComment(int blogCommentId)
        {
            return blogDao.GetBlogComment(blogCommentId);
        }

        /// <summary>
        /// GetBlogComments
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<BlogComment> GetBlogComments(int blogId, int pageNumber, int pageSize)
            {
                return blogDao.GetBlogComments(blogId, pageNumber, pageSize);
        }

        /// <summary>
        /// GetBlogCommentsByCommunity
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public PagedList<BlogComment> GetBlogCommentsByCommunity(int communityId, DateTime sinceDate, int pageNumber, int pageSize)
        {
            return blogDao.GetBlogCommentsByCommunity(communityId, sinceDate, pageNumber, pageSize);
        }

        /// <summary>
        /// DeleteBlogComment
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Delete)]
        public void DeleteBlogComment(int blogCommentId, int userId)
        {
            blogDao.DeleteBlogComment(blogCommentId, userId);
        }

        /// <summary>
        /// Return a list of unique tags used in a channel blog
        /// </summary>
        [DataObjectMethod(DataObjectMethodType.Select)]
        public IList GetChannelBlogTags(int channelId)
        {
            ArrayList retVal = new ArrayList();
            DataTable dt = blogDao.GetBlogKeywords(channelId);

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string keywords = dr["keywords"] == DBNull.Value ? "" : dr["keywords"].ToString();
                    string[] split = Common.GetNormalizedTags(keywords).Split(' ');
                    foreach (string tag in split)
                    {
                        if (tag.Trim() != "" && !retVal.Contains(tag))
                        {
                            retVal.Add(tag);
                        }
                    }
                }
            }

            retVal.Sort();
            return retVal;
        }
    }
}
