///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Kaneva.BusinessLayer.Facade
{
    /// <summary>
    /// helper class for creating events for the RemoteDispatcher
    /// what you put in the event depends on the event.  You must
    /// match what the receiver expects for the specific event
    /// </summary>
    public class XmlEventCreator
    {
        /* format of XML is a version of the RemoteDispatcherEvent

        <remote_event>
            <servers>  (optional)
                <server>0</server>
            </servers>
            <players>  (optional)
                <player>0</player>
            </players>
            <event>
                <name>MOTDEvent</name>
                <version>0x1000000</version>
                <filter>0</filter>
                <objectId>0</objectId>
                <values> (optional)
                    <value type="z|i|l|d|f|s|u|t|b">value</value> z = string, s = short u = ulong, t = ushort
                    ...
                </values>
            </event>
        </remote_event>
        */

        /// <summary>
        /// test fn
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            XmlEventCreator p = new XmlEventCreator("TextEvent",23,324);

            p.AddServer(123);
            p.AddPlayer(456);
            p.AddValue("test");
            p.AddValue(1);
            p.AddValue(2.123);
            p.AddValue((Int16)1);
            p.AddValue((UInt16)123);
            p.AddValue((UInt32)2343123);
            p.AddValue((float)3.141);
            string s = p.GetXml();

            System.Console.WriteLine(s);
        }

        #region Constructors

        /// <summary>
        /// basic constructor
        /// </summary>
        /// <param name="eventName">name of the event, must match subscriber</param>
        public XmlEventCreator(string eventName) : this(eventName,0,0)
        {
        }

        /// <summary>
        /// constructor with filter
        /// </summary>
        /// <param name="eventName">name of the event, must match subscriber</param>
        /// <param name="filter">number subscribers can use for sub-type filtering</param>
        public XmlEventCreator(string eventName, int filter) : this(eventName,filter,0)
        {
        }

        /// <summary>
        /// constructor with filter and objectId
        /// </summary>
        /// <param name="eventName">name of the event, must match subscriber</param>
        /// <param name="filter">number subscribers can use for sub-type filtering</param>
        /// <param name="objectId">arbitrary identifier</param>
        public XmlEventCreator(string eventName, int filter, int objectId )
        {
            m_xml = new XmlDocument();

            m_xml.PreserveWhitespace = true;

            m_rootNode = m_xml.AppendChild(m_xml.CreateElement("remote_event"));
            m_eventNode = m_rootNode.AppendChild(m_xml.CreateElement("event"));
            XmlNode nameNode = m_xml.CreateElement("name");
            nameNode.AppendChild(m_xml.CreateTextNode(eventName));
            m_eventNode.AppendChild(nameNode);
            if (filter != 0)
            {
                XmlNode filterNode = m_xml.CreateElement("filter");
                filterNode.AppendChild(m_xml.CreateTextNode(filter.ToString()));
                m_eventNode.AppendChild(filterNode);
            }
            if (objectId != 0)
            {
                XmlNode objectNode = m_xml.CreateElement("object_id");
                objectNode.AppendChild(m_xml.CreateTextNode(objectId.ToString()));
                m_eventNode.AppendChild(objectNode);
            }

        }
        #endregion

        #region DestinationMethods Methods for setting the destination.  If none called goes to all servers

        /// <summary>
        /// if sending to a specific server, call this.  If this or AddPlayer is not called
        /// the event goes to all servers
        /// </summary>
        /// <param name="serverId">server_id from the developer.game_servers table</param>
        /// <returns>true if ok</returns>
        public bool AddServer(int serverId)
        {
            if (m_serversNode == null)
            {
                m_serversNode = m_xml.CreateElement("servers");
                m_rootNode.AppendChild(m_serversNode);
            }
            XmlNode serverNode = m_xml.CreateElement("server");
            serverNode.AppendChild(m_xml.CreateTextNode(serverId.ToString()));
            m_serversNode.AppendChild(serverNode);
            return true;
        }

        /// <summary>
        /// if sending to a specific player, call this.  If never called
        /// goes to all players on the destination servers.  Should only use one flavor of AddPlayer
        /// </summary>
        /// <param name="playerId">player_id from wok.players</param>
        /// <returns>true if ok</returns>
        public bool AddPlayer(int playerId)
        {
            if (m_playersNode == null)
            {
                m_playersNode = m_xml.CreateElement("players");
                m_rootNode.AppendChild(m_playersNode);
            }
            XmlNode playerNode = m_xml.CreateElement("player");
            playerNode.AppendChild(m_xml.CreateTextNode(playerId.ToString()));
            m_playersNode.AppendChild(playerNode);
            return true;
        }

        /// <summary>
        /// if sending to a specific player, call this.  If never called
        /// goes to all players on the destination servers.  Should only use one flavor of AddPlayer
        /// </summary>
        /// <param name="playerId">player_id from wok.players</param>
        /// <returns>true if ok</returns>
        public bool AddPlayer(string name)
        {
            if (m_playersNode == null)
            {
                m_playersNode = m_xml.CreateElement("playerNames");
                m_rootNode.AppendChild(m_playersNode);
            }
            XmlNode playerNode = m_xml.CreateElement("player");
            playerNode.AppendChild(m_xml.CreateTextNode(name));
            m_playersNode.AppendChild(playerNode);
            return true;
        }

		/// <summary>
		/// Sending to all players in the same zone as a specified player
		/// Should not be mixed with AddPlayer.
		/// </summary>
		/// <param name="playerId">player_id from wok.players</param>
		/// <returns>true if ok</returns>
		public bool AddZoneByPlayerIds(int playerId)
		{
			if (m_playerZonesNode == null)
			{
				m_playerZonesNode = m_xml.CreateElement("playerZones");
				m_rootNode.AppendChild(m_playerZonesNode);
			}
			XmlNode playerNode = m_xml.CreateElement("player");
			playerNode.AppendChild(m_xml.CreateTextNode(playerId.ToString()));
			m_playerZonesNode.AppendChild(playerNode);
			return true;
		}

        #endregion

        #region AddValue overrides for adding values to the event, the order of the calls to AddValue is important since the values are streamed out in order on the other end.  Check doc for specific event

        /// <summary>
        /// add a string to the buffer
        /// </summary>
        /// <param name="s">the string to add</param>
        /// <returns>true if ok</returns>
        public bool AddValue(string s)
        {
            return addValue(s, STRING_TYPE);
        }

        /// <summary>
        /// add an Int32 value to the buffer
        /// </summary>
        /// <param name="value">the number to encode</param>
        /// <returns>true if ok</returns>
        public bool AddValue(Int32 value)
        {
            return addValue(value.ToString(), INT_TYPE);
        }

        /// <summary>
        /// add an Int16 value to the buffer
        /// </summary>
        /// <param name="value">the number to encode</param>
        /// <returns>true if ok</returns>
        public bool AddValue(Int16 value)
        {
            return addValue(value.ToString(), SHORT_TYPE);
        }

        /// <summary>
        /// add a UInt32 value to the buffer
        /// </summary>
        /// <param name="value">the number to encode</param>
        /// <returns>true if ok</returns>
        public bool AddValue(UInt32 value)
        {
            return addValue(value.ToString(), ULONG_TYPE);
        }

        /// <summary>
        /// add a UInt16 value to the buffer
        /// </summary>
        /// <param name="value">the number to encode</param>
        /// <returns>true if ok</returns>
        public bool AddValue(UInt16 value)
        {
            return addValue(value.ToString(), USHORT_TYPE);
        }

        /// <summary>
        /// add a double value to the buffer
        /// </summary>
        /// <param name="value">the number to encode</param>
        /// <returns>true if ok</returns>
        public bool AddValue(double value)
        {
            return addValue(value.ToString(), DOUBLE_TYPE);
        }

        /// <summary>
        /// add a float value to the buffer
        /// </summary>
        /// <param name="value">the number to encode</param>
        /// <returns>true if ok</returns>
        public bool AddValue(float value)
        {
            return addValue(value.ToString(), FLOAT_TYPE);
        }

        #endregion

        /// <summary>
        /// private method called by all the above methods to do the actual encoding
        /// </summary>
        /// <param name="strValue">the value as a string</param>
        /// <param name="type">type of value</param>
        /// <returns>true if ok</returns>
        private bool addValue(string strValue, string type)
        {
            if (m_valuesNode== null)
            {
                m_valuesNode = m_xml.CreateElement("values");
                m_eventNode.AppendChild(m_valuesNode);
            }
            XmlElement node = m_xml.CreateElement("value");
            node.AppendChild(m_xml.CreateTextNode(strValue.ToString()));
            node.SetAttributeNode(m_xml.CreateAttribute("type")).Value = type;
            m_valuesNode.AppendChild(node);

            return true;
        }

        /// <summary>
        /// return the XML for passing to the RemoteDispatcher
        /// </summary>
        /// <returns>a string of XML</returns>
        public string GetXml()
        {
            return m_xml.OuterXml;
        }

        /// <summary>
        /// return the XML for passing to the RemoteDispatcher
        /// </summary>
        /// <returns>a string of XML</returns>
        public override string ToString()
        {
            return GetXml();
        }

        private XmlNode m_eventNode = null;
        private XmlNode m_serversNode = null;
        private XmlNode m_playersNode = null;
		private XmlNode m_playerZonesNode = null;
        private XmlNode m_valuesNode = null;
        private XmlNode m_rootNode = null;
        private XmlDocument m_xml = null;

        const String STRING_TYPE = "z";
        const String INT_TYPE = "i";
        const String LONG_TYPE = "l";
        const String DOUBLE_TYPE = "d";
        const String FLOAT_TYPE = "f";
        const String SHORT_TYPE = "s";
        const String ULONG_TYPE = "u";
        const String USHORT_TYPE = "t";
        const String BOOL_TYPE = "b";

    }
}
