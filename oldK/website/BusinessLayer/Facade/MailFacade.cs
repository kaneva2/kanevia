///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;
using System.Data;
using System.Collections.Specialized;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade.ProxyObjects;
using Kaneva.DataLayer.DataObjects;
using System.Threading;


namespace Kaneva.BusinessLayer.Facade
{
	[DataObject(true)]
	public class MailFacade
	{
		private IMailDao mailDao = DataAccess.MailDao;

		/// <summary>
		/// Gets all enabled promotions by type
		/// </summary>
		[DataObjectMethod(DataObjectMethodType.Select)]
		public IList<EmailTemplate> GetTemplatesByTypeId(int emailTypeId, int enabled)
		{
			//get list of all matching subscriptions
			return mailDao.GetTemplatesByTypeId( emailTypeId, enabled);
		}
	}
}
