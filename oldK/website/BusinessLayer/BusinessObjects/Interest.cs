///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class Interest
    {
        private UInt32 _InterestId;
        private UInt32 _IcId;
        private string _Interest;
        private UInt32 _UserCount;

        public Interest (UInt32 interestId, UInt32 icId, string interest, UInt32 userCount)
        {
            this._InterestId = interestId;
            this._IcId = icId;
            this._Interest = interest;
            this._UserCount = userCount;
        }

        public UInt32 InterestId
        {
           get {return _InterestId;}
           set {_InterestId = value;}
        }

        public UInt32 IcId
        {
           get {return _IcId;}
           set {_IcId = value;}
        }

        public string Name
        {
           get {return _Interest;}
           set {_Interest = value;}
        }

        public UInt32 UserCount
        {
            get { return _UserCount; }
            set { _UserCount = value; }
        }
    }
}
