///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public enum eMESSAGE_TYPE
    {
        PRIVATE_MESSAGE = 0,
        ALERT = 1,
        FRIEND_REQUEST = 2,
        MEMBER_REQUEST = 3,
        GIFT = 4,
        APP_REQUEST = 5,
        COMMUNITY_INVITES = 6
    }

    [Serializable]
    public class Message
    {
        private int _MessageId = 0;
        private int _FromId;
        private int _ToId;
        private string _Subject;
        private string _Message;
        private DateTime _MessageDate;
        private int _Replied;
        private int _Type;
        private int _ChannelId;
        private string _ToViewable;
        private string _FromViewable;

        public Message () { }

        public Message(int messageId, int fromId, int toId, 
            string subject, string message, DateTime messageDate,
            int replied, int type, int channelId, string toViewable, string fromViewable)
        {
            this._MessageId = messageId;
            this._FromId = fromId;
            this._ToId = toId;
            this._Subject = subject;
            this._Message = message;
            this._MessageDate = messageDate;
            this._Replied = replied;
            this._Type = type;
            this._ChannelId = channelId;
            this._ToViewable = toViewable;
            this._FromViewable = fromViewable;
        }

        public int MessageId
        {
           get {return _MessageId;}
           set {_MessageId = value;}
        }

        public int FromId
        {
           get {return _FromId;}
           set {_FromId = value;}
        }

        public int ToId
        {
           get {return _ToId;}
           set {_ToId = value;}
        }

        public string Subject
        {
           get {return _Subject;}
           set {_Subject = value;}
        }

        public string MessageText
        {
           get {return _Message;}
           set {_Message = value;}
        }

        public DateTime MessageDate
        {
           get {return _MessageDate;}
           set {_MessageDate = value;}
        }

        public int Replied
        {
           get {return _Replied;}
           set {_Replied = value;}
        }

        public int Type
        {
           get {return _Type;}
           set {_Type = value;}
        }

        public int ChannelId
        {
           get {return _ChannelId;}
           set {_ChannelId = value;}
        }

        public string ToViewable
        {
           get {return _ToViewable;}
           set {_ToViewable = value;}
        }

        public string FromViewable
        {
           get {return _FromViewable;}
           set {_FromViewable = value;}
        }
    }
}
