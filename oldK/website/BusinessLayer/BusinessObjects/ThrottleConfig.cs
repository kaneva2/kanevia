///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public enum ThrottleConfigType
        {
            Unknown = 0,
            AppRequest = 1,
            InsertBlast = 2,
            GetProfile = 3,
            GetFriends = 4,
            GetBlasts = 5,
            InsertBlastComment = 6,
            EarnAchievement = 7,
            AchievementForUser = 8,
            AchievementEarnedForUser = 9,
            AchievementForApp = 10,
            EarnPlayerData = 11,
            EarnLeaderBoard = 12,
            LeaderBoard = 13,
            LeaderBoardWithAllData = 14,
            EarnLevel = 15,
            LevelForUser = 16,
            EarnTitle = 17,
            GetTitle = 18,
            AppRequestEmail = 19,
            AppRequestHourlyGiftNotification = 20,
            AppRequestDailyGiftNotification = 21
        }

        public enum ThrottleType
        {
            PerUserPerApp = 1,
            PerApp = 2
        }

        public enum ThrottleResults
        {
            Ok = 1,
            OverLimit = 2
        }

        public class ThrottleConfig : ConfigurationElement
        {
            public ThrottleConfig() { }

            public ThrottleConfig(ThrottleConfigType throttleConfigType)
            {
                this.ThrottleConfigType = throttleConfigType;
            }

            public ThrottleConfig(ThrottleConfigType throttleConfigType, ThrottleType throttleType, UInt32 minuteFrequency, UInt32 limit)
            {
                this.ThrottleConfigType = throttleConfigType;
                this.ThrottleType = throttleType;
                this.MinuteFrequency = minuteFrequency;
                this.Limit = limit;
            }

            [ConfigurationProperty("throttleconfigtype", DefaultValue = ThrottleConfigType.Unknown, IsRequired = true, IsKey = true)]
            public ThrottleConfigType ThrottleConfigType
            {
                get { return (ThrottleConfigType)this["throttleconfigtype"]; }
                set { this["throttleconfigtype"] = value; }
            }

            [ConfigurationProperty("throttletype", DefaultValue = ThrottleType.PerUserPerApp, IsRequired = true)]
            public ThrottleType ThrottleType
            {
                get { return (ThrottleType)this["throttletype"]; }
                set { this["throttletype"] = value; }
            }

            [ConfigurationProperty("minutefrequency", DefaultValue = (UInt32)1, IsRequired = true)]
            public UInt32 MinuteFrequency
            {
                get { return (UInt32)this["minutefrequency"]; }
                set { this["minutefrequency"] = value; }
            }

            [ConfigurationProperty("limit", DefaultValue = (UInt32)1, IsRequired = true)]
            public UInt32 Limit
            {
                get { return (UInt32)this["limit"]; }
                set { this["limit"] = value; }
            }
        }

        #region ThrottleConfigCollection
        /// <summary>
        /// Defines the Configuration Collection for ThrottleConfigs
        /// </summary>
        public class ThrottleConfigCollection : ConfigurationElementCollection
        {
            public ThrottleConfig this[int index]
            {
                get { return base.BaseGet(index) as ThrottleConfig; }
                set
                {
                    if (base.BaseGet(index) != null)
                    {
                        base.BaseRemoveAt(index);
                    }
                    base.BaseAdd(index, value);
                }
            }

            public ThrottleConfig this[ThrottleConfigType throttleConfigType]
            {
                get { return base.BaseGet(throttleConfigType) as ThrottleConfig; }
                set
                {
                    int index = -1;
                    if (base.BaseGet(throttleConfigType) != null)
                    {
                        index = base.BaseIndexOf(base.BaseGet(throttleConfigType));
                        base.BaseRemove(throttleConfigType);
                    }

                    if (index == -1)
                    {
                        this.BaseAdd(value);
                    }
                    else
                    {
                        this.BaseAdd(index, value);
                    }
                }
            }

            protected override ConfigurationElement CreateNewElement()
            {
                return new ThrottleConfig();
            }

            protected override object GetElementKey(ConfigurationElement element)
            {
                return ((ThrottleConfig)element).ThrottleConfigType;
            }
        }
        #endregion ThrottleConfigCollection

        #region ThrottleConfigSection
        /// <summary>
        /// Defines the Configuration Section element for throttle configs.
        /// </summary>
        public class ThrottleConfigSection : ConfigurationSection
        {
            [ConfigurationProperty("throttleconfigs", IsRequired = true, IsDefaultCollection = false)]
            public ThrottleConfigCollection ThrottleConfigs
            {
                get
                {
                    ThrottleConfigCollection throttleConfigCollection = (ThrottleConfigCollection)base["throttleconfigs"];
                    return throttleConfigCollection;
                }
            }
        }
        #endregion ThrottleConfigSection
}
