///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class Currency
    {
        public class CurrencyType
        {
            public const string KPOINT = "KPOINT";
            public const string MPOINT = "MPOINT";
            public const string DOLLAR = "DOLLAR";
            public const string GPOINT = "GPOINT";
            public const string CREDITS = KPOINT;
            public const string REWARDS = GPOINT;
        }

        private string keiPointId = "";
        private decimal conversionRateToDollar = 0;
        private string description = "";
        private string currencySymbol = "";
        private bool userPurchaseable = false;
        private string displayFormat = "";

        public Currency () {}

        public Currency (string keiPointId, decimal conversionRateToDollar, string description,
            string currencySymbol, bool userPurchaseable, string displayFormat)
        {
            this.keiPointId = keiPointId;
            this.conversionRateToDollar = conversionRateToDollar;
            this.description = description;
            this.currencySymbol = currencySymbol;
            this.userPurchaseable = userPurchaseable;
            this.displayFormat = displayFormat;
        }

        public string KeiPointId
        {
            get { return this.keiPointId; }
            set { this.keiPointId = value; }
        }

        public decimal ConversionRateToDollar
        {
            get { return this.conversionRateToDollar; }
            set { this.conversionRateToDollar = value; }
        }

        public string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        public string CurrencySymbol
        {
            get { return this.currencySymbol; }
            set { this.currencySymbol = value; }
        }

        public bool UserPurchaseable
        {
            get { return this.userPurchaseable; }
            set { this.userPurchaseable = value; }
        }

        public string DisplayFormat
        {
            get { return this.displayFormat; }
            set { this.displayFormat = value; }
        }
    }
}
