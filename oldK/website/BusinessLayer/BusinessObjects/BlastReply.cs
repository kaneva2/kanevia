///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class BlastReply
    {
        private UInt64 _DiaryReplyId;
        private UInt64 _ReplyToId;
        private DateTime _DateCreated;
        private string _Message;
        private int _UserId;

        // Extra fields
        private string _name_no_spaces;
        private string _thumbnail_small_path;
        private string _gender;
        private DateTime _ctime;

        private FacebookSettings _facebookSettings = new FacebookSettings ();

        public BlastReply(UInt64 diaryReplyId, UInt64 replyToId, DateTime dateCreated, string message, int userId,
            string nameNoSpaces, string thumbnailSmallPath, DateTime currentTime)
        {
            this._DiaryReplyId = diaryReplyId;
            this._ReplyToId = replyToId;
            this._DateCreated = dateCreated;
            this._Message = message;
            this._UserId = userId;

            // Extras
            this._name_no_spaces = nameNoSpaces;
            this._thumbnail_small_path = thumbnailSmallPath;
            this._ctime = currentTime;
        }

        public UInt64 DiaryReplyId
        {
           get {return _DiaryReplyId;}
           set {_DiaryReplyId = value;}
        }

        public UInt64 ReplyToId
        {
           get {return _ReplyToId;}
           set {_ReplyToId = value;}
        }

        public DateTime DateCreated
        {
           get {return _DateCreated;}
           set {_DateCreated = value;}
        }

        public string Message
        {
           get {return _Message;}
           set {_Message = value;}
        }

        public int UserId
        {
           get {return _UserId;}
           set {_UserId = value;}
        }

        // Extra fields
        public string NameNoSpaces
        {
            get { return _name_no_spaces; }
            set { _name_no_spaces = value; }
        }

        public string ThumbnailSmallPath
        {
            get { return _thumbnail_small_path; }
            set { _thumbnail_small_path = value; }
        }

        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        public DateTime CurrentTime
        {
            get { return _ctime; }
            set { _ctime = value; }
        }

        public FacebookSettings OwnerFacebookSettings
        {
            get { return _facebookSettings; }
            set { _facebookSettings = value; }
        }
    }
}
