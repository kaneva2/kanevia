///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;

namespace Kaneva.BusinessLayer.BusinessObjects.Metrics
{
    public class PageTiming
    {
        string m_Name;
        string m_URL;
        string m_Action = "";

        public PageTiming(string name, string URL) : this(name, URL, "")
        {
        }


        public PageTiming (string name, string URL, string action)
		{
            m_Name = name;
            m_URL = URL;
            m_Action = action;
		}

        public string Name
        {
            get
            {
                return m_Name;
            }
        }

        public string URL
        {
            get
            {
                return m_URL;
            }
        }

        public string Action
        {
            get
            {
                return m_Action;
            }
        }

    }
}
