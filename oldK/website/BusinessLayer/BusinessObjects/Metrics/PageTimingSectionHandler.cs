///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Configuration;
using System.Xml;

namespace Kaneva.BusinessLayer.BusinessObjects.Metrics
{
    /// <summary>
    /// Summary description for ContentServerSectionHandler.
    /// </summary>
    public class PageTimingSectionHandler : IConfigurationSectionHandler
    {
        public PageTimingSectionHandler()
        {
        }

        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            PageTiming aPageTiming;
            XmlNodeList pageTimingSettings;
            System.Collections.Hashtable htPageTimings = new System.Collections.Hashtable();

            pageTimingSettings = section.SelectNodes("PageTimings//PageTiming");

            foreach (XmlNode nodeTracker in pageTimingSettings)
            {
                try
                {
                    if (nodeTracker.Attributes.GetNamedItem("action") == null)
                    {
                        aPageTiming = new PageTiming(nodeTracker.Attributes.GetNamedItem("pageName").Value, nodeTracker.Attributes.GetNamedItem("url").Value.ToUpper ());
                    }
                    else
                    {
                        aPageTiming = new PageTiming(nodeTracker.Attributes.GetNamedItem("pageName").Value, nodeTracker.Attributes.GetNamedItem("url").Value.ToUpper(), nodeTracker.Attributes.GetNamedItem("action").Value.ToUpper());
                    }

                    htPageTimings.Add(nodeTracker.Attributes.GetNamedItem("url").Value.ToUpper(), aPageTiming);
                }
                catch (Exception)
                {
                    //m_logger.Error("Error reading Metrics configuration", exc);
                }

            }

            return htPageTimings;
        }

    }

}
