///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class Comment
    {
        private int _CommentId;
        private int _UserId;
        private int _AssetId;
        private int _ChannelId;
        private int _ParentCommentId;
        private int _ReplyToCommentId;
        private string _Comment;
        private int _CommentType;
        private string _IpAddress;
        private DateTime _LastUpdatedDate;
        private DateTime _CreatedDate;
        private int _LastUpdatedUserId;
        private int _StatusId;

        private string _UpdatedUsername = "";
        private DateTime _CurrentTime = new DateTime ();

        private User _owner = null;

        /// <summary>
        /// Default constructor for Comment class.
        /// </summary>
        public Comment() { }

        public Comment (int commentId, int userId, int assetId,
            int channelId, int parentCommentId, int replyToCommentId, string comment, 
            int commentType, string ipAddress, DateTime lastUpdatedDate, DateTime createdDate, 
            int lastUpdatedUserId, int statusId)
        {
            this._CommentId = commentId;
            this._UserId = userId;
            this._AssetId = assetId;
            this._ChannelId = channelId;
            this._ParentCommentId = parentCommentId;
            this._ReplyToCommentId = replyToCommentId;
            this._Comment = comment;
            this._CommentType = commentType;
            this._IpAddress = ipAddress;
            this._LastUpdatedDate = lastUpdatedDate;
            this._CreatedDate = createdDate;
            this._LastUpdatedUserId = lastUpdatedUserId;
            this._StatusId = statusId;
        }

        public int CommentId
        {
           get {return _CommentId;}
           set {_CommentId = value;}
        }

        public int UserId
        {
           get {return _UserId;}
           set {_UserId = value;}
        }

        public int AssetId
        {
           get {return _AssetId;}
           set {_AssetId = value;}
        }

        public int ChannelId
        {
           get {return _ChannelId;}
           set {_ChannelId = value;}
        }

        public int ParentCommentId
        {
           get {return _ParentCommentId;}
           set {_ParentCommentId = value;}
        }

        public int ReplyToCommentId
        {
           get {return _ReplyToCommentId;}
           set {_ReplyToCommentId = value;}
        }

        public string CommentText
        {
           get {return _Comment;}
           set {_Comment = value;}
        }

        public int CommentType
        {
           get {return _CommentType;}
           set {_CommentType = value;}
        }

        public string IpAddress
        {
           get {return _IpAddress;}
           set {_IpAddress = value;}
        }

        public DateTime LastUpdatedDate
        {
           get {return _LastUpdatedDate;}
           set {_LastUpdatedDate = value;}
        }

        public DateTime CreatedDate
        {
           get {return _CreatedDate;}
           set {_CreatedDate = value;}
        }

        public int LastUpdatedUserId
        {
           get {return _LastUpdatedUserId;}
           set {_LastUpdatedUserId = value;}
        }

        public int StatusId
        {
           get {return _StatusId;}
           set {_StatusId = value;}
        }

        public string UpdatedUsername
        {
            get { return _UpdatedUsername; }
            set { _UpdatedUsername = value; }
        }

        public DateTime CurrentTime
        {
            get { return _CurrentTime; }
            set { _CurrentTime = value; }
        }

        public User Owner
        {
            get { return _owner; }
            set { _owner = value; }
        }

    }
}
