///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class FacebookUser
    {
        private string _Name = "";
        private UInt64 _Id = 0;
        private string _Picture = "";
        private string _Email = "";
        private string _AccessToken = "";
        private string _FirstName = "";
        private string _LastName = "";
        private string _Birthday = "";
        private string _Gender = "";
        private string _Locale = "";

        public string Name 
        {
            get { return _Name; } 
            set { _Name = value; } 
        }
        public UInt64 Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
        public string Picture
        {
            get { return _Picture; }
            set { _Picture = value; }
        }
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        public string AccessToken
        {
            get { return _AccessToken; }
            set { _AccessToken = value; }
        }
        public string First_Name
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        public string Last_Name
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        public string Birthday
        {
            get { return _Birthday; }
            set { _Birthday = value; }
        }
        public string Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }
        public string Locale
        {
            get { return _Locale; }
            set { _Locale = value; }
        }
        public string Pic_Square
        {
            set { _Picture = value; }
        }
        public UInt64 UId
        {
            set { _Id = value; }
        }
    }
}
