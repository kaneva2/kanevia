///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class SiteRole
    {
        private int _SiteRoleId = 0;
        private int _RoleId = 0;
        private int _CompanyId = 0;
        private string _RolesName = "";
        public const string ROLE_ID = "RoleId";
        public const string SITE_ROLE_ID = "SiteRoleId";
        public const string ROLE_NAME = "RolesName";

        public enum KanevaRoles
        {
            System = 1,
            Site_Member = 2,
            Beta_Tester = 4,
            Automated_Test_Account = 6,
            Advisor = 10,
            Customer_Service_Rep = 18,
            Corporate_Admin = 26,
            Site_Admin = 58,
            Super_Admin = 62
        }

        public SiteRole()
        {
        }

        /// <summary>
        /// Overloaded constructor for the User class.
        /// </summary>
        public SiteRole(int SiteRoleId, int RoleId, int CompanyId, string RolesName)
        {
            this._SiteRoleId = SiteRoleId;
            this._RoleId = RoleId;
            this._CompanyId = CompanyId;
            this._RolesName = RolesName;
        }

        /// <summary>
        /// Gets or sets unique user identifier.
        /// The Identity Field Design Pattern. 
        /// </summary>
        /// 

        public int SiteRoleId
        {
            get { return _SiteRoleId; }
            set { _SiteRoleId = value; }
        }

        public int CompanyId
        {
            get { return _CompanyId; }
            set { _CompanyId = value; }
        }

        public int RoleId
        {
            get { return _RoleId; }
            set { _RoleId = value; }
        }

        public string RolesName
        {
            get { return _RolesName; }
            set { _RolesName = value; }
        }

    }


}
