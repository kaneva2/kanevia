///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class GameSubscription
    {

        public enum SubscriptionStatus
        {
            Pending = 1,
            Active = 2,
            Refunded = 3,
            Cancelled = 4,
            Expired = 5,
            NoPayment = 6
        }

        private UInt32 _SubscriptionId = 0;
        private int _GameLicenseId = 0;
        private int _GameId = 0;
        private Double _GrossPointAmount = 0;
        private bool _Renewable = false;
        private DateTime _PurchaseDate;
        private UInt32 _GsStatusId = (int)SubscriptionStatus.Active;
        private int _BillingType = 0;
        private string _SubscriptionIdentifier = "";
        private bool _UserCancelled = false;

        public GameSubscription (){}

        public GameSubscription (UInt32 subscriptionId, int gameLicenseId, int gameId, Double grossPointAmount, bool renewable, DateTime purchaseDate, UInt32 gsStatusId, int billingType, string subscriptionIdentifier, bool userCancelled    )
        {
            this._SubscriptionId = subscriptionId;
            this._GameLicenseId = gameLicenseId;
            this._GameId = gameId;
            this._GrossPointAmount = grossPointAmount;
            this._Renewable = renewable;
            this._PurchaseDate = purchaseDate;
            this._GsStatusId = gsStatusId;
            this._BillingType = billingType;
            this._SubscriptionIdentifier = subscriptionIdentifier;
            this._UserCancelled = userCancelled;
        }

        public UInt32 SubscriptionId
        {
            get {return _SubscriptionId;}
            set {_SubscriptionId = value;}
        }

        public int GameLicenseId
        {
            get {return _GameLicenseId;}
            set {_GameLicenseId = value;}
        }

        public int GameId
        {
            get {return _GameId;}
            set {_GameId = value;}
        }

        public Double GrossPointAmount
        {
            get {return _GrossPointAmount;}
            set {_GrossPointAmount = value;}
        }

        public bool Renewable
        {
            get {return _Renewable;}
            set {_Renewable = value;}
        }

        public DateTime PurchaseDate
        {
            get {return _PurchaseDate;}
            set {_PurchaseDate = value;}
        }

        public UInt32 GsStatusId
        {
            get {return _GsStatusId;}
            set {_GsStatusId = value;}
        }

        public int BillingType
        {
            get {return _BillingType;}
            set {_BillingType = value;}
        }

        public string SubscriptionIdentifier
        {
            get {return _SubscriptionIdentifier;}
            set {_SubscriptionIdentifier = value;}
        }

        public bool UserCancelled
        {
            get {return _UserCancelled;}
            set {_UserCancelled = value;}
        }

    }
}
