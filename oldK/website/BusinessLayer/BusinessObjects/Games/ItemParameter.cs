///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{

    [Serializable]
    public class ItemParameter
    {
		// item parameter constants (should be in sync with wok.parameters table)
		public static int PARAM_TYPE_HEALING_AMT		= WOKItem.USE_TYPE_HEALING;
		public static int PARAM_TYPE_AMMO				= WOKItem.USE_TYPE_AMMO;
		public static int PARAM_TYPE_SKILL				= WOKItem.USE_TYPE_SKILL;
		public static int PARAM_TYPE_MENU_ID			= WOKItem.USE_TYPE_MENU;
		public static int PARAM_TYPE_COMBINE			= WOKItem.USE_TYPE_COMBINE;
		public static int PARAM_TYPE_HOUSING_ID			= WOKItem.USE_TYPE_PLACE_HOUSE;
		public static int PARAM_TYPE_INSTALL_HOUSE_ID	= WOKItem.USE_TYPE_INSTALL_HOUSE;
		public static int PARAM_TYPE_SOMMON_AI_CFG_ID	= WOKItem.USE_TYPE_SUMMON_CREATURE;
		public static int PARAM_TYPE_OLD_SCRIPT_PARAM	= WOKItem.USE_TYPE_OLD_SCRIPT;
		public static int PARAM_TYPE_DYN_OBJ_ID			= WOKItem.USE_TYPE_ADD_DYN_OBJ;
		public static int PARAM_TYPE_ATTACH_DYN_OBJ_ID	= WOKItem.USE_TYPE_ADD_ATTACH_OBJ;
		public static int PARAM_TYPE_MOUNT_AI_CFG_ID	= WOKItem.USE_TYPE_MOUNT;
		public static int PARAM_TYPE_EQUIP_ID			= WOKItem.USE_TYPE_EQUIP;

		public static int PARAM_TYPE_TEXTURE_ASSET_ID	= 14;
		public static int PARAM_TYPE_TEXTURE_URL		= 15;
		public static int PARAM_TYPE_ASSET_GROUP_ID		= 16;
		public static int PARAM_TYPE_SWF_NAME			= 17;
		public static int PARAM_TYPE_SWF_PARAMETER		= 18;
		public static int PARAM_TYPE_SWF_ASSET_ID		= 19;
		public static int PARAM_TYPE_FRIEND_ID			= 20;
		public static int PARAM_TYPE_SCRIPT_FILE_PATH	= 29;
		public static int PARAM_TYPE_SCRIPT_CREATED_DATE= 30;
		public static int PARAM_TYPE_EXCLUSION_GROUPS	= 31;
        public static int PARAM_TYPE_GAME_ITEM_ID       = 33;
        public static int PARAM_TYPE_SCRIPT_GAME_ITEM_BUNDLE_GLID = 34;
        public static int PARAM_TYPE_PREMIUM_ITEM_CREDIT_DONATION = 35;
        public static int PARAM_TYPE_EFFECT_DURATION    = 36; // Duration for animation, sound and particle

		public static int PARAM_TYPE_ANIMATION			= WOKItem.USE_TYPE_ANIMATION;
		public static int PARAM_TYPE_P2P_ANIMATION		= WOKItem.USE_TYPE_P2P_ANIMATION;
		public static int PARAM_TYPE_NPC				= WOKItem.USE_TYPE_NPC;
		public static int PARAM_TYPE_DEED				= WOKItem.USE_TYPE_DEED;
		public static int PARAM_TYPE_QUEST				= WOKItem.USE_TYPE_QUEST;
        public static int PARAM_TYPE_CUSTOM_DEED        = WOKItem.USE_TYPE_CUSTOM_DEED;

        private int _globalId = 0;
        private int _paramTypeId = 0;
        private string _paramValue = "";

        /// <summary>
        /// Default constructor for ItemParameter class.
        /// </summary>
        public ItemParameter () { }

        public ItemParameter (int globalId, int paramTypeId, string strValue)
        {
            _globalId = globalId;
            _paramTypeId = paramTypeId;
            _paramValue = strValue;
        }

        public int GlobalId
        {
            get { return _globalId; }
            set { _globalId = value; }
        }

        public int ParamTypeId
        {
            get { return _paramTypeId; }
            set { _paramTypeId = value; }
        }

        public string ParamValue
        {
            get { return _paramValue; }
            set { _paramValue = value; }
        }
    }
}
