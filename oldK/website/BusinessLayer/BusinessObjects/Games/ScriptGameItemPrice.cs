///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class ScriptGameItemPrice
    {
        private int _marketCost;
        private int _designerPrice;
        private int _bundleWebPrice;
        private int _bundleWebPriceVIP;

        public const double CATALOG_FEE = 1.1;
        public const double VIP_DISCOUNT = .9;
        public const int BASE_GAME_ITEM_PRICE = 1000;

        /// <summary>
        /// Constructs a new ScriptGameItemPrice instance.
        /// </summary>
        public ScriptGameItemPrice(WOKItem gameItem, WOKItem bundle) 
        {
            if (gameItem == null)
                throw new ArgumentNullException("scriptGameItem");

            _marketCost = gameItem.MarketCost;
            _designerPrice = (int)gameItem.DesignerPrice;

            bundle = (bundle ?? new WOKItem());
            bundle.BundleItems = (bundle.BundleItems ?? new List<WOKItem>());
            _bundleWebPrice = bundle.BundleWebPrice;
            _bundleWebPriceVIP = bundle.BundleWebPriceVIP;
        }

        /// <summary>
        /// Applies the catalog charge to the provided price, rounding up to the nearest whole number.
        /// </summary>
        private int ApplyCatalogCharge(int price)
        {
            if (price - Math.Floor(price * CATALOG_FEE) > 0)
            {
                return (int)Math.Ceiling(price * CATALOG_FEE);
            }
            return (int)(price * CATALOG_FEE);
        }

        /// <summary>
        /// Removes the catalog charge from the provided price, rounding up to the nearest whole number.
        /// </summary>
        private int RemoveCatalogCharge(int price)
        {
            if (price - Math.Floor(price / CATALOG_FEE) > 0)
            {
                return (int)Math.Ceiling(price / CATALOG_FEE);
            }
            return (int)(price / CATALOG_FEE);
        }

        /// <summary>
        /// The base price of the game item, without commissions, fees, or bundled items.
        /// </summary>
        public int BasePrice
        {
            get
            {
                int basePrice = _marketCost - ApplyCatalogCharge(_designerPrice) - _bundleWebPrice;

                return RemoveCatalogCharge(basePrice >= 0 ? basePrice : 0);
            }
            set
            {
                RecalculateWebPrice(value);
            }
        }

        /// <summary>
        /// The designer commission for the game item.
        /// </summary>
        public int DesignerPrice
        {
            get 
            { 
                return _designerPrice; 
            }
            set 
            {
                _designerPrice = value;
                RecalculateWebPrice(BasePrice);
            }
        }

        /// <summary>
        /// The total price paid by users when they purchase this game item from shop.
        /// </summary>
        public int WebPrice
        {
            get 
            {
                // The total price of the game item is stored as "MarketCost" to allow searching by price.
                return _marketCost;
            }
        }

        /// <summary>
        /// The total price paid by VIP users when they purchase this game item from shop.
        /// </summary>
        public int WebPriceVIP
        {
            get
            {
                return (int)(_designerPrice + BasePrice * VIP_DISCOUNT) + _bundleWebPriceVIP;
            }
        }

        /// <summary>
        /// Recalculates the total web price of the game item and stores as MarketCost.
        /// </summary>
        private void RecalculateWebPrice(int basePrice)
        {
            _marketCost = ApplyCatalogCharge(basePrice + _designerPrice) + _bundleWebPrice;
        }
    }
}