///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public enum eLOGIN_RESULTS
    {
        FAILED = 0,				// 0 not authenticated
        SUCCESS = 1,			// 1 successuful authentication
        USER_NOT_FOUND = 2,		// 2 user not retrieved from database
        INVALID_PASSWORD = 3,	// 3 invalid password
        NO_GAME_ACCESS = 4,		// 4 user was authenticated, but not to game supplied
        NOT_VALIDATED = 5,		// 5 user has not validated the account
        ACCOUNT_DELETED = 6,	// 6 user account was deleted
        ACCOUNT_LOCKED = 7,		// 7 user account was locked
        ALREADY_LOGGED_IN = 8,  // 8 user alreay logged in, performed by DS web service
        GAME_FULL = 9,		    // 9 Max user limit hit for the game
        NOT_AUTHORIZED = 10,    // 10 mature, age, access pass, etc.
        WRONG_SERVER = 11      // 11 mismatched server info serverId doesn't match serverId for actualIp
    }

    public enum eGAME_ACCESS
    {
        NOT_FOUND = 0, // never in db
        PUBLIC = 1,    // only following values in db
        PRIVATE = 2,   // Owner and Moderators only
        BANNED = 3,
        INACCESSIBLE = 4, // was public, but can't reach it
        DELETED = 5,
        PURGED = 6, 
        MEMBERS = 7 // Only allow world members access
    };

    public enum eGAME_STATUS
    {
        NOT_FOUND = 0, // never in db
        ACTIVE = 1,    // only following values in db
        INACTIVE = 2
    };

    [Serializable]
    public class Game
    {
        //public const string ROLE_FUTURE_USE = "Not Used Yet";

        private int _GameId = 0;
        private int _OwnerId = 0;
        private int _GameRatingId = 0;
        private int _GameStatusId = 0;
        private int _GameAccessId = 0;
        private int _GameModifiersId = 0;
        private int _NumberOfComments = 0;
        private int _NumberOfRaves = 0;
        private int _NumberOfViews = 0;
        private int _NumberOfPlayers = 0;
        private int _ParentGameId = 0;

        private string _GameName = "";
        private string _GameDescription = "";
        private string _GameSynopsis = "";
        private string _GameImagePath = null;
        private string _GameKeyWords = "";
        private string _GameEncryptionKey = "";

        private bool _IsAdult = false;
        private bool _Over21Required = false;

        private DateTime _GameCreationDate;
        private DateTime _LastUpdated;

        private string _GAAccountID = "";
        private bool _IsIncubatorHosted = false;

        /// <summary>
        /// Default constructor for Game class.
        /// </summary>
        public Game() { }

        /// <summary>
        /// Overloaded constructor for the User class.
        /// </summary>
        public Game(int gameId, int ownerId, int gameRatingId, int gameStatusId, int gameAccessId,
           int modifiersId, int numberComments, int numberRaves, int numberViews, int numberPlayers, string gameName, string gameDescription, 
            string gameSynopsis, string gameImagePath, string gameKeywords, DateTime gameCreationDate, DateTime lastUpdated, string gameEncryptionKey,
            int parentGameId, bool isAdult, bool over21Required, string gaAccountID, bool isIncubatorHosted)
        {
            this._GameId = gameId;
            this._OwnerId = ownerId;
            this._GameRatingId = gameRatingId;
            this._GameStatusId = gameStatusId;
            this._GameAccessId = gameAccessId;
            this._GameModifiersId = modifiersId;
            this._NumberOfComments = numberComments;
            this._NumberOfRaves = numberRaves;
            this._NumberOfViews = numberViews;
            this._NumberOfPlayers = numberPlayers;
            this._GameName = gameName;
            this._GameDescription = gameDescription;
            this._GameSynopsis = gameSynopsis;
            this._GameImagePath = gameImagePath;
            this._GameKeyWords = gameKeywords;
            this._GameCreationDate = gameCreationDate;
            this._LastUpdated = lastUpdated;
            this._GameEncryptionKey = gameEncryptionKey;
            this._ParentGameId = parentGameId;
            this._IsAdult = isAdult;
            this._Over21Required = over21Required;
            this._GAAccountID = gaAccountID;
            this._IsIncubatorHosted = isIncubatorHosted;
       }

        /// <summary>
        /// Gets or sets unique user identifier.
        /// The Identity Field Design Pattern. 
        /// </summary>
        public int GameId
        {
            get { return _GameId; }
            set { _GameId = value; }
        }

        public int OwnerId
        {
            get { return _OwnerId; }
            set { _OwnerId = value; }
        }

        public int GameRatingId
        {
            get { return _GameRatingId; }
            set { _GameRatingId = value; }
        }

        public int GameStatusId
        {
            get { return _GameStatusId; }
            set { _GameStatusId = value; }
        }

        public int GameAccessId // Game.eGAME_ACCESS are valid values
        {
            get { return _GameAccessId; }
            set { _GameAccessId = value; }
        }

        public int GameModifiersId
        {
            get { return _GameModifiersId; }
            set { _GameModifiersId = value; }
        }

        public int NumberOfComments
        {
            get { return _NumberOfComments; }
            set { _NumberOfComments = value; }
        }

        public int NumberOfRaves
        {
            get { return _NumberOfRaves; }
            set { _NumberOfRaves = value; }
        }

        public int NumberOfViews
        {
            get { return _NumberOfViews; }
            set { _NumberOfViews = value; }
        }

        public int NumberOfPlayers
        {
            get { return _NumberOfPlayers; }
            set { _NumberOfPlayers = value; }
        }
        
        public string GameEncryptionKey
        {
            get { return _GameEncryptionKey; }
            set { _GameEncryptionKey = value; }
        }

        public string GameName
        {
            get { return _GameName; }
            set { _GameName = value; }
        }

        public string GameDescription
        {
            get { return _GameDescription; }
            set { _GameDescription = value; }
        }

        public string GameSynopsis
        {
            get { return _GameSynopsis; }
            set { _GameSynopsis = value; }
        }

        public string GameImagePath
        {
            get { return _GameImagePath; }
            set { _GameImagePath = value; }
        }

        public string GameKeyWords
        {
            get { return _GameKeyWords; }
            set { _GameKeyWords = value; }
        }

        public DateTime GameCreationDate
        {
            get { return _GameCreationDate; }
            set { _GameCreationDate = value; }
        }

        public DateTime LastUpdated
        {
            get { return _LastUpdated; }
            set { _LastUpdated = value; }
        }

        public int ParentGameId
        {
            get { return _ParentGameId; }
            set { _ParentGameId = value; }
        }

        public bool IsAdult
        {
            get { return _IsAdult; }
            set { _IsAdult = value; }
        }

        public bool Over21Required
        {
            get { return _Over21Required; }
            set { _Over21Required = value; }
        }

        public string GAAccountID
        {
            get { return _GAAccountID; }
            set { _GAAccountID = value; }
        }

        public bool IsIncubatorHosted
        {
            get { return _IsIncubatorHosted; }
            set { _IsIncubatorHosted = value; }
        }
    }
}
