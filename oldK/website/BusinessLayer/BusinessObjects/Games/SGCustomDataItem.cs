///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class SGCustomDataItem
    {
        #region Declarations

        private string _Attribute;
        private string _Value;

        #endregion

        public SGCustomDataItem() { }

        public SGCustomDataItem(string attribute, string value) 
        {
            _Attribute = attribute;
            _Value = value;
        }

        #region Properties

        public string Attribute
        {
            get { return _Attribute; }
            set { _Attribute = value; }
        }

        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        #endregion
    }
}
