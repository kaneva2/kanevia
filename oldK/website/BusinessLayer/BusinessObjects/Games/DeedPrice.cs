///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class DeedPrice
    {
        private int _TotalItemsNotOwned = 0;
        private int _TotalItemsOwned = 0;
        private int _DesignCommission = 0;

        public DeedPrice() { }

        public int TotalItemsNotOwned
        {
            get { return _TotalItemsNotOwned; }
            set { _TotalItemsNotOwned = value; }
        }

        public int TotalItemsNotOwnedPlusCommission
        {
            get
            {
                if (_TotalItemsNotOwned - Math.Floor(_TotalItemsNotOwned * Commission) > 0)
                {   // not a whole number
                    return (int)Math.Ceiling(_TotalItemsNotOwned * Commission);
                }
                else
                {
                    return (int)(_TotalItemsNotOwned * Commission);
                }
            }
        }

        public int TotalItemsOwned
        {
            get { return _TotalItemsOwned; }
            set { _TotalItemsOwned = value; }
        }
        public int TotalItemsOwnedPlusCommission
        {
            get
            {
                if (_TotalItemsOwned - Math.Floor(_TotalItemsOwned * Commission) > 0)
                {   // not a whole number
                    return (int)Math.Ceiling(_TotalItemsOwned * Commission);
                }
                else
                {
                    return (int)(_TotalItemsOwned * Commission);
                }
            }
        }
        public int TotalItemsNotOwnedPlusDesignDeCommission
        {
            get
            {
                return _TotalItemsNotOwned + _DesignCommission;
            }
        }
        public int DesignCommission
        {
            get { return _DesignCommission; }
            set { _DesignCommission = value; }
        }
        public int DesignCommissionPlusCommission
        {
            get
            {
                if ((_DesignCommission * Commission) - Math.Floor(_DesignCommission * Commission) > 0)
                {   // not a whole number
                    return (int)Math.Ceiling(_DesignCommission * Commission);
                }
                else
                {
                    return (int)(_DesignCommission * Commission);
                }
            }
        }
        public int TotalDeed
        {
            get { return TotalItemsNotOwned + TotalItemsOwned + DesignCommission; }
        }

        public int TotalDeedWithCommission
        {
            get { return TotalItemsNotOwnedPlusCommission + TotalItemsOwnedPlusCommission + DesignCommissionPlusCommission; }
        }

        public double Commission
        {
            get { return 1.1; }
        }
    }
}