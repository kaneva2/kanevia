///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{

    [Serializable]
    public class WOKItem
    {
        private int _GlobalId = 0;
        private string _Name = "";
        private string _Description = "";
        private int _MarketCost = 0;
        private int _SellingPrice = 0;
        private int _RequiredSkill = -1;
        private int _RequiredSkillLevel = 0;
        private int _UseType = 0;
		private int _UseValue = 0;
        private string _DisplayName = "";
        private int _PassTypeId = 1;
        private DateTime _DateAdded;
        private UInt32 _ItemCreatorId = 0;
        private int _ItemActive = 0;
        private int _ArmAnywhere = 1;
        private int _Disarmable = 1;
        private int _Stackable = 1;
        private int _DestroyWhenUsed = 0;
        private int _InventoryType = 0;
        private UInt32 _BaseGlobalId = 0;
        private string _TemplatePath = "";
        private string _TexturePath = "";
        private string _ThumbnailPath = "";
        private string _ThumbnailSmallPath = "";
        private string _ThumbnailMediumPath = "";
        private string _ThumbnailLargePath = "";
        private string _ThumbnailAssetdetailsPath = "";
        private UInt32 _DesignerPrice = 0;
        private string _Keywords = "";
        private int _WebPrice = 0;
        private int _BundleWebPrice = 0;
        private int _ExpiredDuration = 5;
        private UInt32 _NumberSoldOnWeb = 0;
        private UInt32 _NumberOfRaves = 0;
        private UInt32 _Quantity = 1;

        private UInt32 _NumberOfViews = 0;
        private UInt32 _SalesTotal = 0;
        private UInt32 _SalesDesignerTotal = 0;

        private int _IsDerivable = 0;
        private int _DerivationLevel = 0;
        private UInt32 _TemplateDesignerPrice = UInt32.MaxValue;    // Initialized as "invalid"

        private int _ActorGroup = 0;

        private UInt32 _category1 = 0;
        private UInt32 _category2 = 0;
        private UInt32 _category3 = 0;

        private bool _IsAnimated = false;
        private IList<WOKItem> _BundleItems;

        public enum ItemActiveStates
        {
            Deleted = 0,
            Public = 1,
            Private = 2
        }

        public enum ItemApprovalStatus
        {
            Pending = 0,
            Requested = 1,
            Approved = 2,
            Denied = 3
        }

        public enum InventoryTypes
        {
            Credits = 256,
            Rewards = 512,
            BothCurrencies = 768 
        }

        // Premium Item related attributes
        public const ItemApprovalStatus DEFAULT_PREMIUM_ITEM_STATUS = ItemApprovalStatus.Approved;
        private int _gameId = 0;
        private ItemApprovalStatus _approvalStatus = DEFAULT_PREMIUM_ITEM_STATUS;

        public enum PremiumItemType
        {
            Donation = 1,
            Key = 2
        }

        // Derivation levels
        public const int DRL_UNKNOWN = -2;                  // Derivation level not provided
        public const int DRL_TEMPLATE = -1;                 // User templates or Kaneva templates
        public const int DRL_KANEVAITEM = -1;               // Regular kaneva items
        public const int DRL_UGCDO_ORIGINAL = 0;            // UGCDO original creations
        public const int DRL_UGCDO_DERIVATIVE_L1 = 1;       // UGCDO first level derivatives (currently the only type of derivatives)
        public const int DRL_ITEMRETEX = 1;                 // Retextured custom items from kaneva templates
        public const int DRL_UGCFURNITURE = DRL_ITEMRETEX;  // 1) UGC furniture
        public const int DRL_UGCCLOTHING = DRL_ITEMRETEX;   // 2) UGC clothing

        public const int UGC_BASE_GLID = 3000000;
        public const int UGC_BASE_TEMPLATE_GLID = 1000000000;

        public const int USE_TYPE_NONE = 0;
		public const int USE_TYPE_FIRST_TYPE = 1;
		public const int USE_TYPE_HEALING = 1;
        public const int USE_TYPE_AMMO = 2;
        public const int USE_TYPE_SKILL = 3;
        public const int USE_TYPE_MENU = 4;
        public const int USE_TYPE_COMBINE = 5;
        public const int USE_TYPE_PLACE_HOUSE = 6;
        public const int USE_TYPE_INSTALL_HOUSE = 7;
        public const int USE_TYPE_SUMMON_CREATURE = 8;
        public const int USE_TYPE_OLD_SCRIPT = 9; 
        public const int USE_TYPE_ADD_DYN_OBJ = 10;
        public const int USE_TYPE_ADD_ATTACH_OBJ = 11;
        public const int USE_TYPE_MOUNT = 12;
		public const int USE_TYPE_EQUIP = 13;

        public const int USE_TYPE_FIRST_EVENT = 200;

        // convenience helpers
        public const int USE_TYPE_CLOTHING = 0;
        public const int USE_TYPE_ANIMATION = USE_TYPE_FIRST_EVENT + 0;
        public const int USE_TYPE_P2P_ANIMATION = USE_TYPE_FIRST_EVENT + 1;
        public const int USE_TYPE_NPC = USE_TYPE_FIRST_EVENT + 2;
        public const int USE_TYPE_DEED = USE_TYPE_FIRST_EVENT + 3;
        public const int USE_TYPE_QUEST = USE_TYPE_FIRST_EVENT + 4;
        public const int USE_TYPE_PREMIUM = USE_TYPE_FIRST_EVENT + 5;
		public const int USE_TYPE_PARTICLE = USE_TYPE_FIRST_EVENT + 6;
		public const int USE_TYPE_SOUND = USE_TYPE_FIRST_EVENT + 7;
        public const int USE_TYPE_BUNDLE = USE_TYPE_FIRST_EVENT + 8;
        public const int USE_TYPE_CUSTOM_DEED = USE_TYPE_FIRST_EVENT + 9;
        public const int USE_TYPE_ACTION_ITEM = USE_TYPE_FIRST_EVENT + 10;
        public const int USE_TYPE_SCRIPT_GAME_ITEM = USE_TYPE_FIRST_EVENT + 11;

        public const uint CATEGORY_CLOTHING = 3;
        public const uint CATEGORY_ACCESSORIES = 200;
        public const uint CATEGORY_EMOTES = 73;
        public const uint CATEGORY_BUNDLES = 254;

        /// <summary>
        /// Default constructor for Item class.
        /// </summary>
        public WOKItem() { }

        public WOKItem(int globalId, string name, string description, int marketCost, int sellingPrice, int requiredSkill,
           int requiredSkillLevel, int useType, string displayName, int passTypeId,
           DateTime dateAdded, UInt32 itemCreatorId,
           int itemActive, int armAnywhere, int disarmable, int stackable, int destroyWhenUsed,
           int inventoryType, UInt32 baseGlobalId, string templatePath, string texturePath, string thumbnailPath,
           string thumbnailSmallPath, string thumbnailMediumPath, string thumbnailLargePath, string thumbnailAssetdetailsPath, UInt32 designerPrice, string keywords, int webPrice,
           int expiredDuration, UInt32 numberSoldOnWeb, UInt32 numberOfRaves, UInt32 numberOfViews, UInt32 salesTotal, UInt32 salesDesignerTotal,
           int isDerivable, int derivationLevel, UInt32 templateDesignerPrice, UInt32 category1, UInt32 category2, UInt32 category3, int actorGroup)
            :

            this(globalId, name, description, marketCost, sellingPrice, requiredSkill, requiredSkillLevel, useType, displayName, passTypeId,
                    dateAdded, itemCreatorId, itemActive, armAnywhere, disarmable, stackable, destroyWhenUsed, inventoryType, baseGlobalId, templatePath,
                    texturePath, thumbnailPath, thumbnailSmallPath, thumbnailMediumPath, thumbnailLargePath, thumbnailAssetdetailsPath, designerPrice,
                    keywords, webPrice, expiredDuration, numberSoldOnWeb, numberOfRaves, numberOfViews, salesTotal, salesDesignerTotal,
                    isDerivable, derivationLevel, templateDesignerPrice, category1, category2, category3, actorGroup, 0, ItemApprovalStatus.Pending) { }

        public WOKItem (int globalId, string name, string description, int marketCost, int sellingPrice, int requiredSkill,
           int requiredSkillLevel, int useType, string displayName, int passTypeId,
           DateTime dateAdded, UInt32 itemCreatorId,
           int itemActive, int armAnywhere, int disarmable, int stackable, int destroyWhenUsed,
           int inventoryType, UInt32 baseGlobalId, string templatePath, string texturePath, string thumbnailPath,
           string thumbnailSmallPath, string thumbnailMediumPath, string thumbnailLargePath, string thumbnailAssetdetailsPath, UInt32 designerPrice, string keywords, int webPrice,
           int expiredDuration, UInt32 numberSoldOnWeb, UInt32 numberOfRaves, UInt32 numberOfViews, UInt32 salesTotal, UInt32 salesDesignerTotal,
           int isDerivable, int derivationLevel, UInt32 templateDesignerPrice, UInt32 category1, UInt32 category2, UInt32 category3, int actorGroup, bool isAnimated)
            :
            this (globalId, name, description, marketCost, sellingPrice, requiredSkill, requiredSkillLevel, useType, displayName, passTypeId,
                    dateAdded, itemCreatorId, itemActive, armAnywhere, disarmable, stackable, destroyWhenUsed, inventoryType, baseGlobalId, templatePath,
                    texturePath, thumbnailPath, thumbnailSmallPath, thumbnailMediumPath, thumbnailLargePath, thumbnailAssetdetailsPath, designerPrice,
                    keywords, webPrice, expiredDuration, numberSoldOnWeb, numberOfRaves, numberOfViews, salesTotal, salesDesignerTotal,
                    isDerivable, derivationLevel, templateDesignerPrice, category1, category2, category3, actorGroup, 0, ItemApprovalStatus.Pending, isAnimated) { }

        public WOKItem (int globalId, string name, string description, int marketCost, int sellingPrice, int requiredSkill,
            int requiredSkillLevel, int useType, string displayName, int passTypeId,
            DateTime dateAdded, UInt32 itemCreatorId,
            int itemActive, int armAnywhere, int disarmable, int stackable, int destroyWhenUsed,
            int inventoryType, UInt32 baseGlobalId, string templatePath, string texturePath, string thumbnailPath,
            string thumbnailSmallPath, string thumbnailMediumPath, string thumbnailLargePath, string thumbnailAssetdetailsPath, UInt32 designerPrice, string keywords, int webPrice,
            int expiredDuration, UInt32 numberSoldOnWeb, UInt32 numberOfRaves, UInt32 numberOfViews, UInt32 salesTotal, UInt32 salesDesignerTotal,
            int isDerivable, int derivationLevel, UInt32 templateDesignerPrice, UInt32 category1, UInt32 category2, UInt32 category3, int actorGroup, int gameId, ItemApprovalStatus approvalStatus)
            :

            this (globalId, name, description, marketCost, sellingPrice, requiredSkill, requiredSkillLevel, useType, displayName, passTypeId,
                    dateAdded, itemCreatorId, itemActive, armAnywhere, disarmable, stackable, destroyWhenUsed, inventoryType, baseGlobalId, templatePath,
                    texturePath, thumbnailPath, thumbnailSmallPath, thumbnailMediumPath, thumbnailLargePath, thumbnailAssetdetailsPath, designerPrice,
                    keywords, webPrice, expiredDuration, numberSoldOnWeb, numberOfRaves, numberOfViews, salesTotal, salesDesignerTotal,
                    isDerivable, derivationLevel, templateDesignerPrice, category1, category2, category3, actorGroup, gameId, approvalStatus, false) { }

        public WOKItem(int globalId, string name, string description, int marketCost, int sellingPrice, int requiredSkill,
            int requiredSkillLevel, int useType, string displayName, int passTypeId, 
            DateTime dateAdded, UInt32 itemCreatorId,
            int itemActive, int armAnywhere, int disarmable, int stackable, int destroyWhenUsed,
            int inventoryType, UInt32 baseGlobalId, string templatePath, string texturePath, string thumbnailPath,
            string thumbnailSmallPath, string thumbnailMediumPath, string thumbnailLargePath, string thumbnailAssetdetailsPath, UInt32 designerPrice, string keywords, int webPrice,
            int expiredDuration, UInt32 numberSoldOnWeb, UInt32 numberOfRaves, UInt32 numberOfViews, UInt32 salesTotal, UInt32 salesDesignerTotal,
            int isDerivable, int derivationLevel, UInt32 templateDesignerPrice, UInt32 category1, UInt32 category2, UInt32 category3, 
            int actorGroup, int gameId, ItemApprovalStatus approvalStatus, bool isAnimated)
        {
            this._GlobalId = globalId;
            this._Name = name;
            this._Description = description;
            this._MarketCost = marketCost;
            this._SellingPrice = sellingPrice;
            this._RequiredSkill = requiredSkill;
            this._RequiredSkillLevel = requiredSkillLevel;
            this._UseType = useType;
            this._DisplayName = displayName;
            this._PassTypeId = passTypeId;
            this._DateAdded = dateAdded;
            this._ItemCreatorId = itemCreatorId;
            this._ItemActive = itemActive;
            this._ArmAnywhere = armAnywhere;
            this._Disarmable = disarmable;
            this._Stackable = stackable;
            this._DestroyWhenUsed = destroyWhenUsed;
            this._InventoryType = inventoryType;
            this._BaseGlobalId = baseGlobalId;
            this._TemplatePath = templatePath;
            this._TexturePath = texturePath;
            this._ThumbnailPath = thumbnailPath;
            this._ThumbnailSmallPath = thumbnailSmallPath;
            this._ThumbnailMediumPath = thumbnailMediumPath;
            this._ThumbnailLargePath = thumbnailLargePath;
            this._ThumbnailAssetdetailsPath = thumbnailAssetdetailsPath;
            this._DesignerPrice = designerPrice;
            this._Keywords = keywords;
            this._WebPrice = webPrice;
            this._ExpiredDuration = expiredDuration;
            this._NumberSoldOnWeb = numberSoldOnWeb;
            this._NumberOfRaves = numberOfRaves;

            this._NumberOfViews = numberOfViews;
            this._SalesTotal = salesTotal;
            this._SalesDesignerTotal = salesDesignerTotal;

            this._IsDerivable = isDerivable;
            this._DerivationLevel = derivationLevel;
            this._TemplateDesignerPrice = templateDesignerPrice;

            this._category1 = category1;
            this._category2 = category2;
            this._category3 = category3;
			this._ActorGroup = actorGroup;

            this._gameId = gameId;
            this._approvalStatus = approvalStatus;

            this._IsAnimated = isAnimated;
        }

        public int GlobalId
        {
           get {return _GlobalId;}
           set {_GlobalId = value;}
        }

        public virtual string Name
        {
           get {return _Name;}
           set {_Name = value;}
        }

        public string Description
        {
           get {return _Description;}
           set {_Description = value;}
        }

        public int MarketCost
        {
           get {return _MarketCost;}
           set {_MarketCost = value;}
        }

        public int SellingPrice
        {
           get {return _SellingPrice;}
           set {_SellingPrice = value;}
        }

        public int RequiredSkill
        {
           get {return _RequiredSkill;}
           set {_RequiredSkill = value;}
        }

        public int RequiredSkillLevel
        {
           get {return _RequiredSkillLevel;}
           set {_RequiredSkillLevel = value;}
        }

        public int UseType
        {
           get {return _UseType;}
           set {_UseType = value;}
        }

		// Adding this back so that we can use it when creating a new one.
		// and biggs and yc told me too.
		public int UseValue
		{
			get { return _UseValue; }
			set { _UseValue = value; }
		}

        public string DisplayName
        {
           get {return _DisplayName;}
           set {_DisplayName = value;}
        }

        public int PassTypeId
        {
           get {return _PassTypeId;}
           set {_PassTypeId = value;}
        }

        public DateTime DateAdded
        {
           get {return _DateAdded;}
           set {_DateAdded = value;}
        }

        public UInt32 ItemCreatorId
        {
           get {return _ItemCreatorId;}
           set {_ItemCreatorId = value;}
        }

        public int ItemActive
        {
           get {return _ItemActive;}
           set {_ItemActive = value;}
        }

        public int ArmAnywhere
        {
           get {return _ArmAnywhere;}
           set {_ArmAnywhere = value;}
        }

        public int Disarmable
        {
           get {return _Disarmable;}
           set {_Disarmable = value;}
        }

        public int Stackable
        {
           get {return _Stackable;}
           set {_Stackable = value;}
        }

        public int DestroyWhenUsed
        {
           get {return _DestroyWhenUsed;}
           set {_DestroyWhenUsed = value;}
        }

        public int InventoryType
        {
            get
            {   // All UGC items will be credits only, all Kaneva items are rewards only unless they are specifically marked credits only in db.
                if (IsKanevaOwned)
                {
                    if (_InventoryType != (int)InventoryTypes.Credits)
                    {
                        return (int)InventoryTypes.Rewards;
                    }

                    return _InventoryType;
                }
                else
                {
                    return (int)InventoryTypes.Credits;
                }
            }
            set {_InventoryType = value;}
        }

        public UInt32 BaseGlobalId
        {
           get {return _BaseGlobalId;}
           set {_BaseGlobalId = value;}
        }

        public string TemplatePath
        {
           get {return _TemplatePath;}
           set {_TemplatePath = value;}
        }

        public string TexturePath
        {
           get {return _TexturePath;}
           set {_TexturePath = value;}
        }

        public string ThumbnailPath
        {
           get {return _ThumbnailPath;}
           set {_ThumbnailPath = value;}
        }

        public string ThumbnailSmallPath
        {
           get {return _ThumbnailSmallPath;}
           set {_ThumbnailSmallPath = value;}
        }

        public string ThumbnailMediumPath
        {
            get { return _ThumbnailMediumPath; }
            set { _ThumbnailMediumPath = value; }
        }

        public string ThumbnailLargePath
        {
            get { return _ThumbnailLargePath; }
            set { _ThumbnailLargePath = value; }
        }

        public string ThumbnailAssetdetailsPath
        {
            get { return _ThumbnailAssetdetailsPath; }
            set { _ThumbnailAssetdetailsPath = value; }
        }

        public UInt32 DesignerPrice
        {
           get {return _DesignerPrice;}
           set {_DesignerPrice = value;}
        }

        public string Keywords
        {
           get {return _Keywords;}
           set {_Keywords = value;}
        }

        public int WebPrice
        {
            get 
            {
                // Calculate price for the bundle
                if (_UseType == (int) USE_TYPE_BUNDLE)
                {
                    return this.BundleWebPrice;
                }
                if (_UseType == (int)USE_TYPE_CUSTOM_DEED || _UseType == (int)USE_TYPE_SCRIPT_GAME_ITEM)
                {
                    return (int)(_MarketCost);  // We store fully-calculated cost for deeds
                }
                else  // Get price for single item
                {
                    // Adding 10% catalog charge for UGC items
                    if (!IsKanevaOwned)
                    {
                        return (int) (_WebPrice * 1.1);
                    }
                    //else ** Uncomment this code to increase reward price for Kaneva items
                    //{
                        // If item in a Kaneva item & is clothing or accessories
                        // Then the reward price will be 10x
                        //    return (int)(_WebPrice * 10);
                    //}
                }
                return _WebPrice; 
            }
            set { _WebPrice = value; }
        }

        public int WebPriceVIP
        {
            get
            {
                // https://kaneva.atlassian.net/wiki/display/PM/VIP+-+Reduce+Pricing+Discounts
                // Change all avatar categories 'base price discounts' from 100% to 10%.
                // Change all objects 'base price discounts' from 100% to 90%      

                // https://kaneva.atlassian.net/browse/ED-3368
                // Billy updated PRD, now 10% discount on objects instead of old prd 90% change

                //// Is it Avatar Category
                //if (IsAvatarCategory(_category1) || IsAvatarCategory(_category2) || IsAvatarCategory(_category3))
                //{
                    return (int)(_DesignerPrice + _TemplateDesignerPrice + _MarketCost * .90);
                //}
                //else
                //{
                //    return (int)(_DesignerPrice + _TemplateDesignerPrice + _MarketCost * .10);
                //}
            }
        }

        private bool IsAvatarCategory(uint cateogortyId)
        {
            return (cateogortyId.Equals(CATEGORY_CLOTHING) || cateogortyId.Equals(CATEGORY_ACCESSORIES) ||
                cateogortyId.Equals(CATEGORY_EMOTES) || cateogortyId.Equals(CATEGORY_BUNDLES));

        }

        public int BundleWebPrice
        {
            get 
            {
                if (_BundleWebPrice == 0)
                {
                    int ownerItemsPrice = 0;
                    int nonOwnerItemsPrice = 0;
                    if (this._BundleItems != null)
                    {
                        foreach (WOKItem item in this._BundleItems)
                        {
                            if (item.ItemCreatorId == this.ItemCreatorId)
                            {
                                ownerItemsPrice += item.MarketCost;
                            }
                            else
                            {
                                nonOwnerItemsPrice += item.WebPrice;
                            }
                        }

                        if (Math.Round((ownerItemsPrice + this.DesignerPrice) * 1.1, 1) - Math.Floor((ownerItemsPrice + this.DesignerPrice) * 1.1) > 0)
                        {
                            // not a whole number, round up
                            return (int)Math.Ceiling((ownerItemsPrice + this.DesignerPrice) * 1.1) + nonOwnerItemsPrice;
                        }
                        else
                        {
                            return (int)(((ownerItemsPrice + this.DesignerPrice) * 1.1) + nonOwnerItemsPrice);
                        }
                    }
                }

                return _BundleWebPrice;
            }
            set { _BundleWebPrice = value; }
        }

        public int BundleWebPriceVIP
        {
            get
            {
                int nonOwnerItemsPrice = 0;
                if (this._BundleItems != null)
                {
                    foreach (WOKItem item in this._BundleItems)
                    {
                        if (item.ItemCreatorId != this.ItemCreatorId)
                        {
                            nonOwnerItemsPrice += item.WebPriceVIP;
                        }
                    }
                }

                return (int) (this.DesignerPrice + nonOwnerItemsPrice);
            }
        }
        
        public int ExpiredDuration
        {
            get { return _ExpiredDuration; }
            set { _ExpiredDuration = value; }
        }

        public UInt32 NumberSoldOnWeb
        {
            get { return _NumberSoldOnWeb; }
            set { _NumberSoldOnWeb = value; }
        }

        public UInt32 NumberOfRaves
        {
            get { return _NumberOfRaves; }
            set { _NumberOfRaves = value; }
        }

        public UInt32 NumberOfViews
        {
            get { return _NumberOfViews; }
            set { _NumberOfViews = value; }
        }

        public UInt32 SalesTotal
        {
            get { return _SalesTotal; }
            set { _SalesTotal = value; }
        }

        public UInt32 SalesDesignerTotal
        {
            get { return _SalesDesignerTotal; }
            set { _SalesDesignerTotal = value; }
        }

        public int IsDerivable
        {
            get { return _IsDerivable; }
            set { _IsDerivable = value; }
        }

        public int DerivationLevel
        {
            get { return _DerivationLevel; }
            set { _DerivationLevel = value; }
        }

        public UInt32 TemplateDesignerPrice
        {
            get { return _TemplateDesignerPrice; }
            set { _TemplateDesignerPrice = value; }
        }

        public int ActorGroup
        {
            get { return _ActorGroup; }
            set { _ActorGroup = value; }
        }

        public UInt32 Category1
        {
            get { return _category1; }
            set { _category1 = value; }
        }

        public UInt32 Category2
        {
            get { return _category2; }
            set { _category2 = value; }
        }

        public UInt32 Category3
        {
            get { return _category3; }
            set { _category3 = value; }
        }

        public int GameId
        {
            get { return _gameId; }
            set { _gameId = value; }
        }

        public ItemApprovalStatus ApprovalStatus
        {
            get { return _approvalStatus; }
            set { _approvalStatus = value; }
        }

        public bool isAnimated
        {
            get { return _IsAnimated; }
            set { _IsAnimated = value; }
        }

        public UInt32 Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public IList<WOKItem> BundleItems
        {
            get { return _BundleItems; }
            set { _BundleItems = value; }
        }

        public string GlobalIdName
        {
            get { return string.Format ("{0} - {1}", GlobalId, Name); }
        }

        public bool IsKanevaOwned
        {
            get
            {
                return (_ItemCreatorId == 0 || _ItemCreatorId == 4230696);
            }
        }

        private bool IsPriceAdjustItem
        {
            get
            {
                return (Category1.Equals(CATEGORY_CLOTHING) || Category1.Equals(CATEGORY_ACCESSORIES) ||
                        Category2.Equals(CATEGORY_CLOTHING) || Category2.Equals(CATEGORY_ACCESSORIES) ||
                        Category3.Equals(CATEGORY_CLOTHING) || Category3.Equals(CATEGORY_ACCESSORIES)
                        );
                //|| cateogortyId.Equals(CATEGORY_EMOTES) || cateogortyId.Equals(CATEGORY_BUNDLES));
            }
        }
    }
}
