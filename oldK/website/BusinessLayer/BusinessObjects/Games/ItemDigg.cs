///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{

    [Serializable]
    public class ItemDigg
    {
        private UInt32 _DiggId = 0;
        private int _UserId = 0;
        private int _GlobalId = 0;
        private DateTime _CreatedDate;

        public ItemDigg() { }

        public ItemDigg (UInt32 diggId, int userId, int globalId, DateTime createdDate)
        {
            this._DiggId = diggId;
            this._UserId = userId;
            this._GlobalId = globalId;
            this._CreatedDate = createdDate;
        }

        public UInt32 DiggId
        {
           get {return _DiggId;}
           set {_DiggId = value;}
        }

        public int UserId
        {
           get {return _UserId;}
           set {_UserId = value;}
        }

        public int GlobalId
        {
            get { return _GlobalId; }
            set { _GlobalId = value; }
        }

        public DateTime CreatedDate
        {
           get {return _CreatedDate;}
           set {_CreatedDate = value;}
        }


    }
}
