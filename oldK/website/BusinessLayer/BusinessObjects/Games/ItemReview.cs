///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{

    [Serializable]
    public class ItemReview
    {
        public enum ItemReviewStatus
        {
            Active = 1,
            Deleted = 2
        }

       private UInt32 _ReviewId;
        private int _GlobalId;
        private int _UserId;
        private string _Comment;
        private string _IpAddress;
        private DateTime _LastUpdatedDate;
        private DateTime _CreatedDate;
        private int _LastUpdatedUserId;
        private int _StatusId;

        private string _username = "";
        private string _thumbnailSmallpath = "";

        public ItemReview(UInt32 reviewId, int globalId, int userId, string comment, string ipAddress, DateTime lastUpdatedDate, DateTime createdDate, int lastUpdatedUserId, int statusId, string username, string thumbnailSmallpath)
        {
            this._ReviewId = reviewId;
            this._GlobalId = globalId;
            this._UserId = userId;
            this._Comment = comment;
            this._IpAddress = ipAddress;
            this._LastUpdatedDate = lastUpdatedDate;
            this._CreatedDate = createdDate;
            this._LastUpdatedUserId = lastUpdatedUserId;
            this._StatusId = statusId;

            this._username = username;
            this._thumbnailSmallpath = thumbnailSmallpath;
        }

        public UInt32 ReviewId
        {
           get {return _ReviewId;}
           set {_ReviewId = value;}
        }

        public int GlobalId
        {
           get {return _GlobalId;}
           set {_GlobalId = value;}
        }

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public string Comment
        {
           get {return _Comment;}
           set {_Comment = value;}
        }

        public string IpAddress
        {
           get {return _IpAddress;}
           set {_IpAddress = value;}
        }

        public DateTime LastUpdatedDate
        {
           get {return _LastUpdatedDate;}
           set {_LastUpdatedDate = value;}
        }

        public DateTime CreatedDate
        {
           get {return _CreatedDate;}
           set {_CreatedDate = value;}
        }

        public int LastUpdatedUserId
        {
           get {return _LastUpdatedUserId;}
           set {_LastUpdatedUserId = value;}
        }

        public int StatusId
        {
           get {return _StatusId;}
           set {_StatusId = value;}
        }

        public string Username
        {
           get {return _username;}
           set {_username = value;}
        }

        public string ThumbnailSmallpath
        {
           get {return _thumbnailSmallpath;}
           set {_thumbnailSmallpath = value;}
        }
    }
}
