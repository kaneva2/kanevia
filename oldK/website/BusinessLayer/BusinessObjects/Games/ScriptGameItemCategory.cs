///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class ScriptGameItemCategory
    {
        #region Declarations

        private string _itemType;
        private string _behavior;

        #endregion

        /// <summary>
        /// Default constructor for Item class.
        /// </summary>
        public ScriptGameItemCategory() { }

        public ScriptGameItemCategory(string itemType, string behavior)
        {
            _itemType = itemType;
            _behavior = behavior;
        }

        #region Properties

        public string ItemType
        {
            get { return _itemType; }
            set { _itemType = value; }
        }

        public string Behavior
        {
            get { return _behavior; }
            set { _behavior = value; }
        }

        #endregion
    }
}
