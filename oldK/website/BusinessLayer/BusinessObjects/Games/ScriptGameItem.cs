///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class ScriptGameItem
    {
        #region Declarations

        private int _ZoneInstanceId;
        private int _ZoneType;
        private int _GIId;
        private int _GIGlid;
        private int _Glid;
        private int _Level = 1;
        private int _BundleGlid;
        private int _NumPlacedInTemplate = 0;
        private int? _AutomaticInsertionOrder = null;
        private List<int> _BundledGlids;
        private bool _InventoryCompatible;
        private bool _ExistsOnShop = false;
        private string _Name;
        private string _ItemType;
        private string _Rarity = "Common";
        private DateTime? _AutomaticInsertionStart = null;
        private Dictionary<string, string> _Properties;
        private List<SGIVendorItem> _VendorTrades;
        private List<SGISpawnedItem> _SpawnedItems;
        private List<SGILootItem> _LootItems;
        private SGIRandomLootSetting _RandomLootSettings;
        private List<SGIQuest> _Quests;

        public const int GIGLID_LOWER_LIMIT = 1000000;
        public const int GIGLID_UPPER_LIMIT = 1499999;

        public const int GI_BUNDLE_GLID_LOWER_LIMIT = 1500000;
        public const int GI_BUNDLE_GLID_UPPER_LIMIT = 2999999;

        public enum ParamGlidFields
        {
            addGLIDS, 
            femaleGLIDS, 
            maleGLIDS, 
            soundGLID
        };

        public enum BehaviorParamGlidFields
        {
            animations, 
            spawnGLID, 
            closeAnim, 
            openAnim, 
            openedAnim, 
            idleParticle, 
            chargeParticle, 
            attackSound
        };

        #endregion

        /// <summary>
        /// Default constructor for Item class.
        /// </summary>
        public ScriptGameItem() { }

        #region Properties

        public int ZoneInstanceId
        {
            get { return _ZoneInstanceId; }
            set { _ZoneInstanceId = value; }
        }
        public int ZoneType
        {
            get { return _ZoneType; }
            set { _ZoneType = value; }
        }
        public int GIId
        {
            get { return _GIId; }
            set { _GIId = value; }
        }
        public int GIGlid
        {
            get { return _GIGlid; }
            set { _GIGlid = value; }
        }
        public int Glid
        {
            get { return _Glid; }
            set { _Glid = value; }
        }
        public int Level
        {
            get { return _Level; }
            set { _Level = value; }
        }
        public int BundleGlid
        {
            get { return _BundleGlid; }
            set { _BundleGlid = value; }
        }
        public int NumPlacedInTemplate
        {
            get { return _NumPlacedInTemplate; }
            set { _NumPlacedInTemplate = value; }
        }
        public int? AutomaticInsertionOrder
        {
            get { return _AutomaticInsertionOrder; }
            set { _AutomaticInsertionOrder = value; }
        }
        public List<int> BundledGlids
        {
            get
            {
                if (_BundledGlids == null)
                    _BundledGlids = new List<int>();

                return _BundledGlids;
            }
            set { _BundledGlids = value; }
        }
        public bool InventoryCompatible
        {
            get { return _InventoryCompatible; }
            set { _InventoryCompatible = value; }
        }
        public bool ExistsOnShop
        {
            get { return _ExistsOnShop; }
            set { _ExistsOnShop = value; }
        }
        /// <summary>
        /// True, if AutomaticInsertionStart is a valid DateTime.
        /// </summary>
        public bool InsertAutomatically
        {
            get { return _AutomaticInsertionStart != null; }
        }
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public string ItemType
        {
            get { return _ItemType; }
            set { _ItemType = value; }
        }
        public string Rarity
        {
            get { return _Rarity; }
            set { _Rarity = value; }
        }
        public DateTime? AutomaticInsertionStart
        {
            get { return _AutomaticInsertionStart; }
            set { _AutomaticInsertionStart = value; }
        }
        public Dictionary<string, string> Properties
        {
            get 
            {
                if (_Properties == null)
                    _Properties = new Dictionary<string, string>();

                return _Properties; 
            }
            set { _Properties = value; }
        }

        public List<SGIVendorItem> VendorTrades
        {
            get
            {
                if (_VendorTrades == null)
                    _VendorTrades = new List<SGIVendorItem>();

                return _VendorTrades;
            }
            set { _VendorTrades = value; }
        }

        public List<SGISpawnedItem> SpawnedItems
        {
            get
            {
                if (_SpawnedItems == null)
                    _SpawnedItems = new List<SGISpawnedItem>();

                return _SpawnedItems;
            }
            set { _SpawnedItems = value; }
        }

        public List<SGILootItem> LootItems
        {
            get
            {
                if (_LootItems == null)
                    _LootItems = new List<SGILootItem>();

                return _LootItems;
            }
            set { _LootItems = value; }
        }

        public SGIRandomLootSetting RandomLootSettings
        {
            get
            {
                if (_RandomLootSettings == null)
                    _RandomLootSettings = new SGIRandomLootSetting();

                return _RandomLootSettings;
            }
            set { _RandomLootSettings = value; }
        }

        public List<SGIQuest> Quests
        {
            get
            {
                if (_Quests == null)
                    _Quests = new List<SGIQuest>();

                return _Quests;
            }
            set { _Quests = value; }
        }

        #endregion
    }
}
