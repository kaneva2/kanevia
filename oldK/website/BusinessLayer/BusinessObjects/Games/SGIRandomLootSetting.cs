///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class SGIRandomLootSetting
    {
        #region Declarations

        private ScriptGameItem _Item;
        private bool _CanSpawnGeneric;
        private bool _CanSpawnArmor;
        private bool _CanSpawnAmmo;
        private bool _CanSpawnConsumable;
        private bool _CanSpawnWeapon;

        #endregion

        /// <summary>
        /// Default constructor for Item class.
        /// </summary>
        public SGIRandomLootSetting() { }

        #region Properties

        public ScriptGameItem Item
        {
            get { return _Item; }
            set { _Item = value; }
        }

        public bool CanSpawnGeneric
        {
            get { return _CanSpawnGeneric; }
            set { _CanSpawnGeneric = value; }
        }
        public bool CanSpawnArmor
        {
            get { return _CanSpawnArmor; }
            set { _CanSpawnArmor = value; }
        }
        public bool CanSpawnAmmo
        {
            get { return _CanSpawnAmmo; }
            set { _CanSpawnAmmo = value; }
        }
        public bool CanSpawnConsumable
        {
            get { return _CanSpawnConsumable; }
            set { _CanSpawnConsumable = value; }
        }
        public bool CanSpawnWeapon
        {
            get { return _CanSpawnWeapon; }
            set { _CanSpawnWeapon = value; }
        }

        #endregion
    }
}
