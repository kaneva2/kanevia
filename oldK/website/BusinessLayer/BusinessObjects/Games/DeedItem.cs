///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class DeedItem
    {
        #region Declarations

        private List<DeedItem> _BundleItems;
        private int _DeedOwnerId = 0;
        private int _Quantity = 1;
        private WOKItem _WOKItem;    

        public const int CURRENCY_TYPE_CREDITS = 256;
        public const int CURRENCY_TYPE_REWARDS = 512;
        public const int CURRENCY_TYPE_BOTH_R_C = 768;

        #endregion Declarations

        public DeedItem(int deedOwnerId, WOKItem item)
        {
            this._DeedOwnerId = deedOwnerId;
            this._WOKItem = item;       
        }

        #region Properties

        public List<DeedItem> BundleItems
        {
            get
            {
                if (_BundleItems == null)
                {
                    if (_WOKItem.BundleItems == null || _WOKItem.BundleItems.Count == 0)
                    {
                        _WOKItem.BundleItems = new List<WOKItem>();
                    }

                    _BundleItems = _WOKItem.BundleItems.Select(bi => new DeedItem(_DeedOwnerId, bi)).ToList();
                }

                return _BundleItems;
            }
            set
            {
                if (value == null || value.Count == 0)
                {
                    _WOKItem.BundleItems.Clear();
                    _BundleItems = null;
                }
                else
                {
                    _WOKItem.BundleItems = value.Select(di => di._WOKItem).ToList();
                    _BundleItems = value;
                }
            }
        }

        public bool CreditsOnly
        {
            get 
            {
                bool bundleAllowsRewardPurchases = true;
                if (BundleItems.Count > 0)
                {
                    bundleAllowsRewardPurchases = BundleItems.Where(di => di.CreditsOnly).Count() == 0;
                }

                return (bundleAllowsRewardPurchases ? _WOKItem.InventoryType.Equals(CURRENCY_TYPE_CREDITS) : true);
            }
        }

        public int DeedOwnerId
        {
            get { return _DeedOwnerId; }
            set { _DeedOwnerId = value; }
        }

        public uint DeedItemPrice
        {
            get { return (IsKanevaOwned || ItemCreatorId == _DeedOwnerId) ? 0 : (uint)(_WOKItem.DesignerPrice + _WOKItem.TemplateDesignerPrice); }
        }

        public string DisplayName
        {
            get 
            { 
                return (string.IsNullOrWhiteSpace(_WOKItem.DisplayName) ? _WOKItem.Name : _WOKItem.DisplayName); 
            }
            set { _WOKItem.DisplayName = value; }
        }

        public int GlobalId
        {
            get { return _WOKItem.GlobalId; }
            set { _WOKItem.GlobalId = value; }
        }

        public bool IsAP
        {
            get
            {
                bool bundleIsGeneralPass = true;
                if (BundleItems.Count > 0)
                {
                    bundleIsGeneralPass = BundleItems.Where(di => di.IsAP).Count() == 0;
                }

                return (bundleIsGeneralPass ? (_WOKItem.PassTypeId == 1) : true);
            }
        }

        public int InventoryType
        {
            get { return _WOKItem.InventoryType; }
            set { _WOKItem.InventoryType = value; }
        }

        public int ItemActive
        {
            get
            {
                bool bundleOk = true;
                if (BundleItems.Count > 0)
                {
                    bundleOk = BundleItems.Where(di => di.ItemActive != (int)WOKItem.ItemActiveStates.Public).Count() == 0;
                }

                return (bundleOk ? _WOKItem.ItemActive : (int)WOKItem.ItemActiveStates.Private);
            }
        }

        public UInt32 ItemCreatorId
        {
            get { return _WOKItem.ItemCreatorId; }
            set { _WOKItem.ItemCreatorId = value; }
        }

        public bool IsKanevaOwned
        {
            get { return _WOKItem.IsKanevaOwned; }
        }

        public int PassTypeId
        {
            get { return _WOKItem.PassTypeId; }
            set { _WOKItem.PassTypeId = value; }
        }

        public int Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }        

        public string ThumbnailSmallPath
        {
            get { return _WOKItem.ThumbnailSmallPath; }
            set { _WOKItem.ThumbnailSmallPath = value; }
        }

        public int UseType
        {
            get { return _WOKItem.UseType; }
            set { _WOKItem.UseType = value; }
        }

        #endregion Properties
    }
}