///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class SGFrameworkSettings
    {
        #region Declarations

        private bool _FrameworkEnabled;

        #endregion

        public SGFrameworkSettings() { }

        public SGFrameworkSettings(bool frameworkEnabled) 
        {
            _FrameworkEnabled = frameworkEnabled;
        }

        #region Properties

        public bool FrameworkEnabled
        {
            get { return _FrameworkEnabled; }
            set { _FrameworkEnabled = value; }
        }

        #endregion
    }
}
