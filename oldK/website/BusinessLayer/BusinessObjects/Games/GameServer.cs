///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data;
using System.Net;
using System.IO;
using System.Diagnostics; // for Process

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class GameServer
    {
        //public const string ROLE_FUTURE_USE = "Not Used Yet";

        // From developer.game_server_visibility
        public enum Visibility { Unknown = 0, Public = 1, Private = 2 };

        private int _ServerId = 0;
        private int _GameId = 0;
        private int _VisibiltyId = 0;
        private int _Port = 25857; //default as indicated by WOK team
        private int _ServerStatusId = 0;
        private int _ServerTypeId = 0;
        private int _NumberOfPlayers = 0;
        private int _MaxPlayers = 0;
        private int _GameServerModifiersId = 0;


        private string _ServerName = "";
        private string _IPAddress = "";

        private DateTime _ServerStartedDate;
        private DateTime _LastPingDate;

        /// <summary>
        /// Default constructor for Game class.
        /// </summary>
        public GameServer() { }

        /// <summary>
        /// Overloaded constructor for the User class.
        /// </summary>
        public GameServer(int serverId, int gameId, int visibilityId, int port, int serverStatusId, int serverTypeId, int gameServerModifiersId, 
           int numberOfPlayers, int maxPlayers, string serverName, string ipAddress, DateTime serverStartDate, DateTime lastPingDate)
        {
            this._ServerId = serverId;
            this._GameId = gameId;
            this._VisibiltyId = visibilityId;
            this._Port = port;
            this._ServerStatusId = serverStatusId;
            this._ServerTypeId = serverTypeId;
            this._GameServerModifiersId = gameServerModifiersId;
            this._NumberOfPlayers = numberOfPlayers;
            this._MaxPlayers = maxPlayers;
            this._ServerName = serverName;
            this._IPAddress = ipAddress;
            this._ServerStartedDate = serverStartDate;
            this._LastPingDate = lastPingDate;
       }

        /// <summary>
        /// Gets or sets unique user identifier.
        /// The Identity Field Design Pattern. 
        /// </summary>
        public int ServerId
        {
            get { return _ServerId; }
            set { _ServerId = value; }
        }

        public int GameId
        {
            get { return _GameId; }
            set { _GameId = value; }
        }

        public int VisibiltyId
        {
            get { return _VisibiltyId; }
            set { _VisibiltyId = value; }
        }

        public int Port
        {
            get { return _Port; }
            set { _Port = value; }
        }

        public int ServerStatusId
        {
            get { return _ServerStatusId; }
            set { _ServerStatusId = value; }
        }

        public int ServerTypeId
        {
            get { return _ServerTypeId; }
            set { _ServerTypeId = value; }
        }

        public int GameServerModifiersId
        {
            get { return _GameServerModifiersId; }
            set { _GameServerModifiersId = value; }
        }

        public int NumberOfPlayers
        {
            get { return _NumberOfPlayers; }
            set { _NumberOfPlayers = value; }
        }

        public int MaxPlayers
        {
            get { return _MaxPlayers; }
            set { _MaxPlayers = value; }
        }

        public string ServerName
        {
            get { return _ServerName; }
            set { _ServerName = value; }
        }

        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }
        }

        public DateTime ServerStartedDate
        {
            get { return _ServerStartedDate; }
            set { _ServerStartedDate = value; }
        }

        public DateTime LastPingDate
        {
            get { return _LastPingDate; }
            set { _LastPingDate = value; }
        }

        public static string WokNetTestDir
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["WokNetTestDir"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["WokNetTestDir"];
                }
                else
                {
                    return "";
                }
            }
        }
        public static int WokProtocolVer
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["WokProtocolVer"] != null)
                {
                    return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["WokProtocolVer"]);
                }
                else
                {
                    return 34; // value as of 4/1
                }
            }
        }
        public static int CheckGameServerTimeout
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["CheckGameServerTimeout"] != null)
                {
                    return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CheckGameServerTimeout"]);
                }
                else
                {
                    return 5;
                }
            }
        }

        /// <summary>
        /// read the versioninfo from a url and check the first line
        /// </summary>
        /// <param name="_url">base url of patch</param>
        /// <param name="timeoutSec">number of seconds for timeout</param>
        /// <returns></returns>
        static public string CheckVersionInfo(string _url, int timeoutSec)
        {
            string errMsg = "";

            string url = _url;
            if (url.Substring(url.Length - 1, 1) != "/")
                url += "/";

            HttpWebRequest req = null;
            WebResponse resp = null;
            try
            {
                req = (HttpWebRequest)WebRequest.Create(url + "versioninfo.dat");
                req.ReadWriteTimeout = timeoutSec * 1000;
                resp = req.GetResponse();
                Stream receiveStream = resp.GetResponseStream();

                Encoding encode = Encoding.GetEncoding("utf-8");

                // Pipe the stream to a higher level stream reader with the required encoding format. 
                StreamReader readStream = new StreamReader(receiveStream, encode);
                Char[] read = new Char[256];

                // Read 256 charcters at a time.    
                string st = readStream.ReadLine();

                if (st != "[CURRENT]")
                    errMsg = "Unexpected content checking patch URL: " + url + " \"[CURRENT]\" != " + st;
                // else ok!
            }
            catch (Exception e)
            {
                errMsg = "Exception checking patch URL: " + url + " " + e.Message;
            }
            finally
            {
                if ( resp != null )
                    resp.Close();
            }

            return errMsg;
        }

        /// <summary>
        /// run woknettest.exe against a server 
        /// </summary>
        /// <param name="workingDir">dir where woknettest.exe and current versioninfo.dat lives</param>
        /// <param name="ipAddr">ip of game server</param>
        /// <param name="port">udb port of game server</param>
        /// <param name="internalError">if error returned, true if internal error</param>
        /// <returns>empty string of ok, otherwiser error string</returns>
        static public string CheckServerConnection(bool onInit, string workingDir, string ipAddr, int port, int currentVersion, int timeoutSecs, out bool internalError)
        {
            internalError = false;
            string errMsg = "";

            // Start the child process.
            Process p = new Process();

            // Redirect the output stream of the child process.
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.FileName = workingDir + "\\" + "woknettest.exe";
            p.StartInfo.Arguments = ipAddr + " " + port.ToString() + " woknettest woknettest " + currentVersion.ToString() + " " + timeoutSecs.ToString();
            p.StartInfo.WorkingDirectory = workingDir;

            string exeString = p.StartInfo.FileName + " " + p.StartInfo.Arguments;

            try
            {
                if (p.Start())
                {
                    // Do not wait for the child process to exit before
                    // reading to the end of its redirected stream.
                    // p.WaitForExit();
                    // Read the output stream first and then wait.
                    string output = p.StandardOutput.ReadToEnd();

                    // parse output 
                    string delim = "\t\r\n";
                    string[] codes = output.Split(delim.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                    p.WaitForExit();

                    // output is two or three tab-delimited strings.  
                    // The first is a decimal return code.  
                    // The second is a string representations of the number
                    // the optional third string is supporting details depending on the return code.
                    if (codes.Length < 2)
                    {
                        // error in wok net test
                        errMsg = "Error checking game server. No output from " + exeString;
                        internalError = true;
                    }
                    else if (codes[0] == "0x00000000" && codes.Length > 2 &&
                             (codes[2] == "LR_OK" || (onInit && codes[2] == "LR_NO_PING")))
                    {
                        // ok! do nothing
                    }
                    else if (codes.Length > 2)
                    {
                        errMsg = "Error checking game server. Cannot access: " + ipAddr + ":" + port.ToString() + " " + codes[0] + " " + codes[1] + " " + codes[2];
                    }
                    else
                    {
                        errMsg = "Error checking game server. Cannot access: " + ipAddr + ":" + port.ToString() + " " + codes[0] + " " + codes[1];
                    }
                }
                else
                {
                    errMsg = "Error checking game server running " + exeString;
                    internalError = true;
                }
            }
            catch (Exception e)
            {
                errMsg = "Exception checking game server running: " + p.StartInfo.FileName + " " + p.StartInfo.Arguments + " Exception: " + e.Message;
                internalError = true;
            }

            return errMsg;
        }

        /// <summary>
        /// test a 3dApps servers by checking the game server and patch url
        /// </summary>
        /// <param name="workingDir">dir where woknettest.exe and current versioninfo.dat lives</param>
        /// <param name="ipAddr">ip of game server</param>
        /// <param name="port">udb port of game server</param>
        /// <param name="currentVersion">current wok protocol version</param>
        /// <param name="patchUrl">base url of patch</param>
        /// <param name="timeoutsec">timeout for woknettest</param>
        /// <param name="internalError">if error returned, true if internal error</param>
        /// <returns>empty string of ok, otherwiser error string</returns>
        static public string Validate(bool onInit, string workingDir, string ipAddr, int port, int currentVersion, ref DataTable patchUrls, int timeoutSecs, out bool internalError)
        {
            string ret = CheckServerConnection(onInit, workingDir, ipAddr, port, currentVersion, timeoutSecs, out internalError);
            if (ret.Length == 0)
            {
                // check all the patch urls, looking for one good one
                for (int i = 0; i < patchUrls.Rows.Count; i++)
                {
                    ret = CheckVersionInfo(patchUrls.Rows[i]["patch_url"].ToString(), timeoutSecs);
                    if (ret.Length == 0) // good one!
                        break;
                }
            }
            return ret;
        }

        public string Validate(bool onInit, string ipAddr, ref DataTable patchUrls, out bool internalError)
        {
            return Validate(onInit, WokNetTestDir, ipAddr, Port, WokProtocolVer, ref patchUrls, CheckGameServerTimeout, out internalError);
        }

        public string Validate(string overrideIpAddress, ref DataTable patchUrls, out bool internalError)
        {
            if (patchUrls != null && patchUrls.Rows.Count != 0 )
            {
                return Validate(overrideIpAddress != null, overrideIpAddress != null ? overrideIpAddress : IPAddress, ref patchUrls, out internalError);
            }
            else
            {
                internalError = true;
                return "No data found";  // null check made earlier, never should be here
            }
        }
    }
}
