///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class BundleItemChange
    {
        private WOKItem _originalItem;
        private WOKItem _modifiedItem;
        private WOKItem _originalBundle;
        private WOKItem _modifiedBundle;
        private int? _originalBundlePrice;

        public BundleItemChange(WOKItem originalItem, WOKItem modifiedItem, WOKItem originalBundle, WOKItem modifiedBundle)
            : this(originalItem, modifiedItem, originalBundle, modifiedBundle, null) { }

        public BundleItemChange(WOKItem originalItem, WOKItem modifiedItem, WOKItem originalBundle, WOKItem modifiedBundle, int? originalBundlePrice)
        {
            this._originalItem = originalItem;
            this._modifiedItem = modifiedItem;
            this._originalBundle = originalBundle;
            this._modifiedBundle = modifiedBundle;
            this._originalBundlePrice = originalBundlePrice ?? originalBundle.MarketCost;
        }

        public WOKItem OriginalItem
        {
            get { return _originalItem; }
        }

        public WOKItem ModifiedItem
        {
            get { return _modifiedItem; }
        }

        public WOKItem OriginalBundle
        {
            get { return _originalBundle; }
        }

        public WOKItem ModifiedBundle
        {
            get { return _modifiedBundle; }
        }

        public int OriginalBundlePrice
        {
            get { return (int)_originalBundlePrice; }
        }
    }
}