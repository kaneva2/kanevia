///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Text;
using System;

namespace Kaneva.BusinessLayer.BusinessObjects
{
	public class ItemPathType
	{
        public ItemPathType()
        {
        }

        public ItemPathType(int typeId, string type, int version, string fileFormat, string pathRegex, string pathFormat, AssetStoreType storeType, int primaryTypeId, string sha256Table)
        {
            _typeId = typeId;
            _type = type;
            _assetVersion = version;
            _fileFormat = fileFormat;
            _pathRegex = pathRegex;
            _pathFormat = pathFormat;
            _storeType = storeType;
            _primaryTypeId = primaryTypeId;
            _SHA256Table = sha256Table;
        }

        public int TypeId
        {
            get { return _typeId; }
        }

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public int AssetVersion
        {
            get { return _assetVersion; }
        }

        public string FileFormat
        {
            get { return _fileFormat; }
        }

        public string PathRegex
        {
            get { return _pathRegex; }
        }

        public string PathFormat
        {
            get { return _pathFormat; }
        }

        public AssetStoreType StoreType
        {
            get { return _storeType; }
        }

        public int PrimaryTypeId
        {
            get { return _primaryTypeId; }
        }

        public string SHA256Table
        {
            get { return _SHA256Table; }
        }

        public bool isDynamicObjectMesh()
        {
            return _fileFormat.StartsWith("dob.");
        }

        private string getUniquePathPrefix(uint uniqueAssetId, string numberFormat, bool reverse, int [,] segmentDefs)
        {
            Debug.Assert(segmentDefs != null);
            Debug.Assert(segmentDefs.GetLength(1) == 2);

            string hexId = string.Format("{0:" + numberFormat + "}", uniqueAssetId);
            if (reverse)
            {
                var hexIdCharArr = hexId.ToCharArray();
                Array.Reverse(hexIdCharArr);
                hexId = new string(hexIdCharArr);
            }

            int numSegs = segmentDefs.GetLength(0);
            string[] segments = new string[numSegs];
            for (int i = 0; i < numSegs; i++)
            {
                segments[i] = hexId.Substring(segmentDefs[i, 0], segmentDefs[i, 1]);
                if (segments[i].Length == 0)
                {
                    segments[i] = "_";  // Potential bad argument. This is to prevent degenerated path
                }
            }

            return String.Join("/", segments);
        }

        public string getAssetPathUnique(uint uniqueAssetId)
        {
            string JPGQualityMacroPlaceHolder = MACRO_QUALITY.Replace("{", "<").Replace("}", ">");

            if (_pathFormat.IndexOf('{') == -1)
            {
                // No macros found
                throw new Exception("getAssetPathUnique: path type " + _type + " *NOT* properly defined for unique asset storage. Not asset ID macros found in path format: '" + _pathFormat + "'");
            }

            // Pre-process custom macros in _fileFormat
            StringBuilder format = new StringBuilder(_pathFormat);

            // {q} - preserve JPG quality macro
            format.Replace(MACRO_QUALITY, JPGQualityMacroPlaceHolder);

            // {d} - shortcut
            format.Replace("{d}", uniqueAssetId.ToString());

            // {dNNN..}, {drNNN..}, {xNNN..}, {xrNNN..}, {XNNN..}, {XrNNN..}
            Regex regex = new Regex(Regex.Escape("{") + "([Xxd])(r?)([0-9]?)([0-9]?)([0-9]?)" +Regex.Escape("}"));
            while (true)
            {
                var match = regex.Match(format.ToString());
                if (!match.Success)
                {
                    break;
                }
                string macro = match.Value;
                string numberFormat;
                switch (match.Groups[1].Value)
                {
                    case "X": numberFormat = "X8"; break;
                    case "x": numberFormat = "x8"; break;
                    case "d": numberFormat = "d10"; break;
                    default: Debug.Assert(false); numberFormat = ""; break;
                }

                bool reverse = match.Groups[2].Value == "r";

                int numSegs = match.Groups[5].Value != "" ? 3 : match.Groups[4].Value != "" ? 2 : match.Groups[3].Value != "" ? 1 : 0;

                int[,] segmentDefs = new int[numSegs, 2];
                for (int i = 0, curOfs = 0; i < numSegs; i++ )
                {
                    int segLen = Convert.ToInt32(match.Groups[i + 3].Value);
                    segmentDefs[i, 0] = curOfs;
                    segmentDefs[i, 1] = segLen;
                    curOfs += segLen;
                }

                format.Replace(macro, getUniquePathPrefix(uniqueAssetId, numberFormat, reverse, segmentDefs));
            }

            // Replace unrecognized macros with empty string (lazy match .*?)
            regex = new Regex(Regex.Escape("{") + ".*?" + Regex.Escape("}"));
            while (true)
            {
                var match = regex.Match(format.ToString());
                if (!match.Success)
                {
                    break;
                }

                format.Replace(match.Value, "");
            }

            // Restore JPG quality macro
            format.Replace(JPGQualityMacroPlaceHolder, MACRO_QUALITY);

            return format.ToString();
        }

        public string toXml(string rootTag)
        {
            XElement root = new XElement("ItemPathType",
                                new XElement("type_id", _typeId.ToString()),
                                new XElement("type", _type),
                                new XElement("asset_version", _assetVersion.ToString()),
                                new XElement("file_format", _fileFormat),
                                new XElement("path_regex", _pathRegex),
                                new XElement("path_format", _pathFormat),
                                new XElement("store_type", _storeType.ToString()),
                                new XElement("primary_type_id", _primaryTypeId.ToString())
                           );

            if (rootTag != null && rootTag != "")
            {
                root = new XElement(rootTag, root);
            }

            using (StringWriter sw = new StringWriter())
            {
                using (XmlTextWriter xw = new XmlTextWriter(sw))
                {
                    new XDocument(root).WriteTo(xw);
                    return sw.ToString();
                }
            }
        }

        private static bool IsPerItemJPGPathType(string pathType)
        {
            var parser = new Regex("tex_jpg_[0-9][0-9]");
            return parser.IsMatch(pathType);
        }

        private static string GetPerItemJPGPathType(int quality)
        {
            return "tex_jpg_" + string.Format("{0:00}", quality);
        }

        private static bool IsUniqueJPGPathType(string pathType)
        {
            var parser = new Regex("tex_j[0-9][0-9]");
            return parser.IsMatch(pathType);
        }

        private static string GetUniqueJPGPathType(int quality)
        {
            return "tex_j" + string.Format("{0:00}", quality);
        }

        public static bool IsJPGPathType(string pathType)
        {
            return IsPerItemJPGPathType(pathType) || IsUniqueJPGPathType(pathType);
        }

        public static string GetDDSPathTypeByJPGPathType(string jpgPathType)
        {
            if (IsPerItemJPGPathType(jpgPathType))
            {
                return TYPE_ITEM_TEX_DDS;
            }
            else if (IsUniqueJPGPathType(jpgPathType))
            {
                return TYPE_UNIQUE_TEX_DDS;
            }

            return null;
        }

        public static string GetJPGPathTypeByDDSPathType(string ddsPathType, int quality)
        {
            switch (ddsPathType)
            {
                case TYPE_ITEM_TEX_DDS:
                    return GetPerItemJPGPathType(quality);
                case TYPE_UNIQUE_TEX_DDS:
                    return GetUniqueJPGPathType(quality);
                default:
                    return null;
            }
        }
        public static string GetTemplatePathTypeByTexturePathType(string texPathType)
        {
            switch (texPathType)
            {
                case TYPE_ITEM_TEX_DDS:
                    return TYPE_ITEM_TEM_DDS;

                case TYPE_UNIQUE_TEX_DDS:
                    // New type for unique texture file store - template path no longer used
                    return null;

                default:
                    if (IsPerItemJPGPathType(texPathType))
                    {
                        return TYPE_ITEM_TEM_JPG;
                    }
                    else if (IsUniqueJPGPathType(texPathType))
                    {
                        // New type for unique texture file store - template path no longer used
                        return null;
                    }
                    else
                    {
                        // Not supported
                        Debug.Assert(false);
                        return null;
                    }
            }
        }

        public static bool IsMeshPathType(string pathType)
        {
            return pathType == TYPE_MESH || pathType == TYPE_UNIQUE_MESH;
        }

        // Default path type names (must match item_path_types table)
        public const string TYPE_MESH = "mesh";
        public const string TYPE_SOUND = "sound";
        public const string TYPE_ANIM = "anim";
        public const string TYPE_EQUIP = "equip";
        public const string TYPE_PARTICLE = "particle";
        public const string TYPE_SCRIPT = "script";
        public const string TYPE_ITEM_TEM_DDS = "tem_dds_gz";
        public const string TYPE_ITEM_TEM_JPG = "tem_jpg";
        public const string TYPE_ITEM_TEX_DDS = "tex_dds_gz";
        public const string TYPE_UNIQUE_TEX_DDS = "tex_dds";
        public const string TYPE_UNIQUE_MESH = "mesh_u";

        // Some delayed-expansion macros
        public const string MACRO_SUB_ID = "{subid}";
        public const string MACRO_QUALITY = "{q}";

        public enum AssetStoreType // item_path_types.store_type
        {
            PER_ITEM = 1,       // Asset stored on a per-item basis
            UNIQUE = 2,         // Asset stored in a common repository organized based on the content of the asset
            AUX = 3,            // Auxiliary asset - stored next to a "primary" asset it's associated with (e.g. JPG textures)
        }

        public const int INVALID_PATH_TYPE_ID = -1;

        private int _typeId = INVALID_PATH_TYPE_ID;
        private string _type = "";
        private int _assetVersion = 0;
        private string _fileFormat = "";
        private string _pathRegex = "";
        private string _pathFormat = "";
        private AssetStoreType _storeType = AssetStoreType.PER_ITEM;
        private int _primaryTypeId = INVALID_PATH_TYPE_ID;
        private string _SHA256Table = "";
    }
}
