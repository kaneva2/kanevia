///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class BundleItem
    {
        private int _GlobalId = 0;
        private int _WebPrice = 0;
        private int _MarketCost = 0;
        private int _PassTypeId = 0;
        private int _UseType = 0;
        private int _InventoryType = 0;
        private UInt32 _ItemCreatorId = 0;
        private string _DisplayName = "";

        public const int CURRENCY_TYPE_CREDITS = 256;
        public const int CURRENCY_TYPE_REWARDS = 512;
        public const int CURRENCY_TYPE_BOTH_R_C = 768;

        public BundleItem (WOKItem item)
        {
            this._GlobalId = item.GlobalId;
            this._WebPrice = item.WebPrice;
            this._MarketCost = item.MarketCost;
            this._PassTypeId = item.PassTypeId;
            this._UseType = item.UseType;
            this._InventoryType = item.InventoryType;
            this._ItemCreatorId = item.ItemCreatorId;
            this._DisplayName = item.DisplayName;
        }

        public int GlobalId
        {
            get { return _GlobalId; }
            set { _GlobalId = value; }
        }

        public string DisplayName
        {
            get { return _DisplayName; }
            set { _DisplayName = value; }
        }

        public int WebPrice
        {
            get { return _WebPrice; }
            set { _WebPrice = value; }
        }

        public int MarketCost
        {
            get { return _MarketCost; }
            set { _MarketCost = value; }
        }

        public int PassTypeId
        {
            get { return _PassTypeId; }
            set { PassTypeId = value; }
        }

        public int UseType
        {
            get { return _UseType; }
            set { _UseType = value; }
        }

        public UInt32 ItemCreatorId
        {
            get { return _ItemCreatorId; }
            set { _ItemCreatorId = value; }
        }

        public int InventoryType
        {
            get { return _InventoryType; }
            set { _InventoryType = value; }
        }

        public bool IsAP
        {
            get
            {
                return (_PassTypeId == 1);
            }
        }

        public bool CreditsOnly
        {
            get { return _InventoryType.Equals (CURRENCY_TYPE_CREDITS); }
        }
    }
}