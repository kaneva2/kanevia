///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class WorldTemplate
    {
        public enum WorldTemplateStatus
        {
            ARCHIVED = 0,
            ACTIVE = 1,
            ADMIN_ONLY = 2
        }

        private int _TemplateId = 0;
        private int _DefaultCategoryId = 0;
        private int _SortOrder = 0;
        private int _Status = 0;
        private int _KEPThumbnailId = 0;
        private int _DisplayPassGroupId = 0;
        private int _UpsellPassGroupId = 0;
        private int _GlobalId = 0;
        private int _ZoneIndexPlain = 0;
        private string _Name = "";
        private string _Description = "";
        private string _PreviewImagePathSmall = "";
        private string _PreviewImagePathMedium = "";
        private string _PreviewImagePathLarge = "";
        private string _DefaultThumbnail = "";
        private string _Features = "";
        private bool _IsDefaultHomeTemplate = false;
        private bool _IncubatorHosted = false;
        private bool _PrivateOnCreation = false;
        private DateTime? _GrandfatherDate = null;
        private DateTime _FirstPublicReleaseDate = DateTime.MaxValue;
        private List<WorldTemplateExperimentGroup> _ExperimentGroups;

        /// <summary>
        /// Default constructor for WorldTemplate class.
        /// </summary>
        public WorldTemplate() { }

        /// <summary>
        /// Overloaded constructor for the WorldTemplate class.
        /// </summary>
        public WorldTemplate(int templateId, string name, string description, string features, int defaultCategoryId, int sortOrder, int status, string previewImagePathSmall,
            string previewImagePathMedium, string previewImagePathLarge, int kepThumbnailId, bool isDefaultHomeTemplate, string defaultThumbnail, bool incubatorHosted, 
            int displayPassGroupId, int upsellPassGroupId, int globalId, int zoneIndexPlain, DateTime? grandfatherDate, DateTime firstPublicReleaseDate, bool privateOnCreation)
        {
            this._TemplateId = templateId;
            this._Name = name;
            this._Description = description;
            this._Features = features;
            this._DefaultCategoryId = defaultCategoryId;
            this._SortOrder = sortOrder;
            this._Status = status;
            this._PreviewImagePathSmall = previewImagePathSmall;
            this._PreviewImagePathMedium = previewImagePathMedium;
            this._PreviewImagePathLarge = previewImagePathLarge;
            this._KEPThumbnailId = kepThumbnailId;
            this._IsDefaultHomeTemplate = isDefaultHomeTemplate;
            this._DefaultThumbnail = defaultThumbnail;
            this._IncubatorHosted = incubatorHosted;
            this._DisplayPassGroupId = displayPassGroupId;
            this._UpsellPassGroupId = upsellPassGroupId;
            this._GlobalId = globalId;
            this._ZoneIndexPlain = zoneIndexPlain;
            this._GrandfatherDate = grandfatherDate;
            this._FirstPublicReleaseDate = firstPublicReleaseDate;
            this._PrivateOnCreation = privateOnCreation;
       }

        public int TemplateId
        {
            get { return _TemplateId; }
            set { _TemplateId = value; }
        }

        public int DefaultCategoryId
        {
            get { return _DefaultCategoryId; }
            set { _DefaultCategoryId = value; }
        }

        public int SortOrder
        {
            get { return _SortOrder; }
            set { _SortOrder = value; }
        }

        public int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        public int KEPThumbnailId
        {
            get { return _KEPThumbnailId; }
            set { _KEPThumbnailId = value; }
        }

        public int DisplayPassGroupId
        {
            get { return _DisplayPassGroupId; }
            set { _DisplayPassGroupId = value; }
        }

        public int UpsellPassGroupId
        {
            get { return _UpsellPassGroupId; }
            set { _UpsellPassGroupId = value; }
        }

        public int GlobalId
        {
            get { return _GlobalId; }
            set { _GlobalId = value; }
        }

        public int ZoneIndexPlain
        {
            get { return _ZoneIndexPlain; }
            set { _ZoneIndexPlain = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        [XmlIgnore]
        public string Features
        {
            get { return _Features; }
            set { _Features = value; }
        }

        public string PreviewImagePathSmall
        {
            get { return _PreviewImagePathSmall; }
            set { _PreviewImagePathSmall = value; }
        }

        public string PreviewImagePathMedium
        {
            get { return _PreviewImagePathMedium; }
            set { _PreviewImagePathMedium = value; }
        }

        public string PreviewImagePathLarge
        {
            get { return _PreviewImagePathLarge; }
            set { _PreviewImagePathLarge = value; }
        }

        public bool IsDefaultHomeTemplate
        {
            get { return _IsDefaultHomeTemplate; }
            set { _IsDefaultHomeTemplate = value; }
        }

        public bool IncubatorHosted
        {
            get { return _IncubatorHosted; }
            set { _IncubatorHosted = value; }
        }

        public bool PrivateOnCreation
        {
            get { return _PrivateOnCreation; }
            set { _PrivateOnCreation = value; }
        }

        public string DefaultThumbnail
        {
            get { return _DefaultThumbnail; }
            set { _DefaultThumbnail = value; }
        }

        public DateTime? GrandfatherDate
        {
            get { return _GrandfatherDate; }
            set { _GrandfatherDate = value; }
        }

        public DateTime FirstPublicReleaseDate
        {
            get { return _FirstPublicReleaseDate; }
            set { _FirstPublicReleaseDate = value; }
        }

        [XmlIgnore]
        public List<WorldTemplateExperimentGroup> ExperimentGroups
        {
            get 
            {
                if (_ExperimentGroups == null)
                {
                    _ExperimentGroups = new List<WorldTemplateExperimentGroup>();
                }
                return _ExperimentGroups; 
            }
            set { _ExperimentGroups = value; }
        }

        // Included here for ease of XML serialization.  Easier for WoK client to parse if
        // count of features is provided.
        public int FeatureCount
        {
            get
            {
                return FeatureList.Length;
            }
            set { /* Required for XML serialization */ }
        }

        [XmlArray("FeatureList")]
        [XmlArrayItem("Feature")]
        public string[] FeatureList
        {
            get
            {
                // Per discussion with D.Hicks, for the moment, it doesn't make sense to build out a 1 -> many db structure for
                // template features. The 'description' field is being used for this purpose.
                return _Features.Split('|');
            }
            set { /* Required for XML serialization */ }
        }
    }
}
