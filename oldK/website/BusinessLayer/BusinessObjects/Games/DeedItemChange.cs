///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class DeedItemChange
    {
        private WOKItem _originalItem;
        private WOKItem _modifiedItem;
        private WOKItem _originalDeed;
        private WOKItem _modifiedDeed;
        private int? _originalDeedPrice;

        public DeedItemChange(WOKItem originalItem, WOKItem modifiedItem, WOKItem originalDeed, WOKItem modifiedDeed)
            : this(originalItem, modifiedItem, originalDeed, modifiedDeed, null) { }

        public DeedItemChange(WOKItem originalItem, WOKItem modifiedItem, WOKItem originalDeed, WOKItem modifiedDeed, int? originalDeedPrice)
        {
            this._originalItem = originalItem;
            this._modifiedItem = modifiedItem;
            this._originalDeed = originalDeed;
            this._modifiedDeed = modifiedDeed;
            this._originalDeedPrice = originalDeedPrice ?? originalDeed.MarketCost;
        }

        public WOKItem OriginalItem
        {
            get { return _originalItem; }
        }

        public WOKItem ModifiedItem
        {
            get { return _modifiedItem; }
        }

        public WOKItem OriginalDeed
        {
            get { return _originalDeed; }
        }

        public WOKItem ModifiedDeed
        {
            get { return _modifiedDeed; }
        }

        public int OriginalDeedPrice
        {
            get { return (int)_originalDeedPrice; }
        }
    }
}