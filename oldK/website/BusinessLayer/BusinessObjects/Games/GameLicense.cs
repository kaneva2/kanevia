///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class GameLicense
    {
        public enum LicenseStatus
        {
            Active = 1,
            Deleted = 2,
            WaitingForITSetup = 3,					// Passed Credit Card Authorization
            FailedPaymentAuth = 4,
            Unsubscribed = 5,
            PendingAuth = 6,
            WaitForITTakedown = 7			// Next step is deleted
        }

        //public const string ROLE_FUTURE_USE = "Not Used Yet";

        private int _GameLicenseId = 0;
        private int _GameId = 0;
        private int _LicenseSubscriptionId = 0;
        private int _LicenseStatusId = 1;
        private int _ModifierId = 0;
        private int _maxGameUsers = 2000;

        private string _GameKey = "";

        private DateTime _StartDate;
        private DateTime _EndDate;
        private DateTime _PurchaseDate;

        /// <summary>
        /// Default constructor for Game class.
        /// </summary>
        public GameLicense() { }

        /// <summary>
        /// Overloaded constructor for the User class.
        /// </summary>
        public GameLicense(int gameLicenseId, int gameId, int licenseSubscriptionId, int licenseStatusId, int licenseModifiersId, string gameKey, DateTime startDate,
            DateTime endDate, DateTime purchaseDate, int maxGameUsers)
        {
            this._GameLicenseId = gameLicenseId;
            this._GameId = gameId;
            this._LicenseSubscriptionId = licenseSubscriptionId;
            this._LicenseStatusId = licenseStatusId;
            this._ModifierId = licenseModifiersId;
            this._GameKey = gameKey;
            this._StartDate = startDate;
            this._EndDate = endDate;
            this._PurchaseDate = purchaseDate;
            this._maxGameUsers = maxGameUsers;
        }

        /// <summary>
        /// Gets or sets unique user identifier.
        /// The Identity Field Design Pattern. 
        /// </summary>
        public int GameLicenseId
        {
            get { return _GameLicenseId; }
            set { _GameLicenseId = value; }
        }

        public int MaxGameUsers
        {
            get { return _maxGameUsers; }
            set { _maxGameUsers = value; }
        }

        public int GameId
        {
            get { return _GameId; }
            set { _GameId = value; }
        }

        public int LicenseSubscriptionId
        {
            get { return _LicenseSubscriptionId; }
            set { _LicenseSubscriptionId = value; }
        }

        public int LicenseStatusId
        {
            get { return _LicenseStatusId; }
            set { _LicenseStatusId = value; }
        }

        public int ModifierId
        {
            get { return _ModifierId; }
            set { _ModifierId = value; }
        }

        public string GameKey
        {
            get { return _GameKey; }
            set { _GameKey = value; }
        }

        public DateTime StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }

        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        public DateTime PurchaseDate
        {
            get { return _PurchaseDate; }
            set { _PurchaseDate = value; }
        }

    }
}
