///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class SGISpawnedItem
    {
        public ScriptGameItem SpawnedGameItem { get; set; }
        public int SpawnUNID { get; set; }
        public int MaxSpawns { get; set; }
        public int RespawnTime { get; set; }
        public bool DefaultSpawner { get; set; }
        public bool PersistentSpawn { get; set; }
    }
}
