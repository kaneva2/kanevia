///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{

    [Serializable]
    public class ItemPurchase
    {
        private UInt32 _PurchaseId;
        private int _TxnType;
        private int _GlobalId;
        private string _Name;
        private int _UserId;
        private UInt32 _ItemOwnerId;
        private int _PurchasePrice;
        private int _OwnerPrice;
        private string _IpAddress;
        private DateTime _PurchaseDate;
        private int _quantity;

        public enum eItemTxnType
        {
            PURCHASE = 0,
            COMMISSION = 1,
            FREE_FOR_GM = 2,
            VIP_PURCHASE = 3
        }

        public ItemPurchase(UInt32 purchaseId, int txnType, int globalId, string name, int userId, UInt32 itemOwnerId, int purchasePrice, int ownerPrice, string ipAddress, DateTime purchaseDate, int quantity)
        {
            this._PurchaseId = purchaseId;
            this._TxnType = txnType;
            this._GlobalId = globalId;
            this._Name = name;
            this._UserId = userId;
            this._ItemOwnerId = itemOwnerId;
            this._PurchasePrice = purchasePrice;
            this._OwnerPrice = ownerPrice;
            this._IpAddress = ipAddress;
            this._PurchaseDate = purchaseDate;
            this._quantity = quantity;
        }

        public UInt32 PurchaseId
        {
           get {return _PurchaseId;}
           set {_PurchaseId = value;}
        }

        public int TxnType
        {
            get { return _TxnType; }
            set { _TxnType = value;  }
        }

        public int GlobalId
        {
           get {return _GlobalId;}
           set {_GlobalId = value;}
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int UserId
        {
           get {return _UserId;}
           set {_UserId = value;}
        }

        public UInt32 ItemOwnerId
        {
           get {return _ItemOwnerId;}
           set {_ItemOwnerId = value;}
        }

        public int PurchasePrice
        {
           get {return _PurchasePrice;}
           set {_PurchasePrice = value;}
        }

        public int OwnerPrice
        {
            get { return _OwnerPrice; }
            set { _OwnerPrice = value; }
        }

        public string IpAddress
        {
           get {return _IpAddress;}
           set {_IpAddress = value;}
        }

        public DateTime PurchaseDate
        {
           get {return _PurchaseDate;}
           set {_PurchaseDate = value;}
        }

        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        
    }
}
