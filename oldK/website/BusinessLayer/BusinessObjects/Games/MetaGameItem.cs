///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class MetaGameItem
    {
        private string _ItemName;
        private string _ItemType;
        private int _ConversionValueRewards;
        private int _ConversionValueCredits;
        private int _RedemptionRewardsLimit;
        private int _RedemptionCreditsLimit;

        public MetaGameItem() { }

        public MetaGameItem(string itemName, string itemType, int conversionRewards, int conversionCredits,
            int redemptionRewardsLimit, int redemptionCreditsLimit)
        {
            _ItemName = itemName;
            _ItemType = itemType;
            _ConversionValueRewards = conversionRewards;
            _ConversionValueCredits = conversionCredits;
            _RedemptionRewardsLimit = redemptionRewardsLimit;
            _RedemptionCreditsLimit = redemptionCreditsLimit;
        }

        public string ItemName
        {
            get { return _ItemName; }
            set { _ItemName = value; }
        }

        public string ItemType
        {
            get { return _ItemType; }
            set { _ItemType = value; }
        }

        /// <summary>
        /// Returns true if the MetaGameItem can be converted to the specify currency.
        /// </summary>
        public bool CanConvertToCurrency(string currencyType)
        {
            return (currencyType.Equals(Currency.CurrencyType.REWARDS) && _ConversionValueRewards > 0 && _RedemptionRewardsLimit > 0) ||
                (currencyType.Equals(Currency.CurrencyType.CREDITS) && _ConversionValueCredits > 0 && _RedemptionCreditsLimit > 0);
        }

        /// <summary>
        /// Returns the remaining amount in the specified currency that can be converted from this MetaGameItem.
        /// If the conversion is not possible, 0 is returned.
        /// </summary>
        public int GetAmountAvailableToConvert(string currencyType, int amtAlreadyConverted)
        {
            int amtAvailable = 0;

            if (CanConvertToCurrency(currencyType))
            {
                amtAvailable = GetConversionLimit(currencyType) - amtAlreadyConverted;
            }

            return (amtAvailable < 0 ? 0 : amtAvailable);
        }

        /// <summary>
        /// Returns the value of the MetaGameItem in the specified currency.
        /// If the conversion is not possible, 0 is returned.
        /// </summary>
        public int ConvertToCurrency(string currencyType, int numToConvert, int amtAlreadyConverted)
        {
            int currencyValue = 0;

            if (CanConvertToCurrency(currencyType))
            {
                int conversionValue = GetConversionValue(currencyType);
                int amtAvailable = GetAmountAvailableToConvert(currencyType, amtAlreadyConverted);
                int conversionAmt = conversionValue * numToConvert;

                currencyValue = ((amtAvailable - conversionAmt) < 0 ? 0 : conversionAmt);
            }

            return currencyValue;
        }

        /// <summary>
        /// Returns the value of the MetaGameItem in the specified currency.
        /// If the conversion is not possible, 0 is returned.
        /// </summary>
        public int GetCurrencyValue(string currencyType, int numToConvert)
        {
            int currencyValue = 0;

            if (CanConvertToCurrency(currencyType))
            {
                int conversionValue = GetConversionValue(currencyType);
                currencyValue = conversionValue * numToConvert;
            }

            return currencyValue;
        }

        /// <summary>
        /// Returns the conversion value of the MetaGameItem in the specified currency.
        /// Returns 0 if currencyType is invalid.
        /// </summary>
        public int GetConversionValue(string currencyType)
        {
            return currencyType.Equals(Currency.CurrencyType.REWARDS) ? _ConversionValueRewards
                : currencyType.Equals(Currency.CurrencyType.CREDITS) ? _ConversionValueCredits
                : 0;
        }

        /// <summary>
        /// Returns the conversion limit for the MetaGameItem in the specified currency.
        /// Returns 0 if currencyType is invalid.
        /// </summary>
        public int GetConversionLimit(string currencyType)
        {
            return currencyType.Equals(Currency.CurrencyType.REWARDS) ? _RedemptionRewardsLimit
                : currencyType.Equals(Currency.CurrencyType.CREDITS) ? _RedemptionCreditsLimit
                : 0;
        }
    }
}