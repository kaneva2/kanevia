///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class SGIVendorItem
    {
        #region Declarations

        private ScriptGameItem _Input;
        private ScriptGameItem _Output;
        private int _OutputCount;
        private int _Cost;
        private int _Time;

        #endregion

        /// <summary>
        /// Default constructor for Item class.
        /// </summary>
        public SGIVendorItem(ScriptGameItem input, ScriptGameItem output, int outputCount, int cost, int time) 
        {
            _Input = input;
            _Output = output;
            _OutputCount = outputCount;
            _Cost = cost;
            _Time = time;
        }

        #region Properties

        public ScriptGameItem Input
        {
            get { return _Input; }
            set { _Input = value; }
        }

        public ScriptGameItem Output
        {
            get { return _Output; }
            set { _Output = value; }
        }

        public int OutputCount
        {
            get { return _OutputCount; }
            set { _OutputCount = value; }
        }

        public int Cost
        {
            get { return _Cost; }
            set { _Cost = value; }
        }

        public int Time
        {
            get { return _Time; }
            set { _Time = value; }
        }

        #endregion
    }
}
