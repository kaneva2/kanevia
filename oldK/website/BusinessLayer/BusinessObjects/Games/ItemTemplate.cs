///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{

    [Serializable]
    public class ItemTemplate
    {
        private int _GlobalId = 0;
        private string _Name = "";
        private string _ThumbnailSmallPath = "";
        private string _ThumbnailMediumPath = "";
        private string _ThumbnailLargePath = "";
        private int _Height = 0;
        private int _Width = 0;
       
        /// <summary>
        /// Default constructor for Item class.
        /// </summary>
        public ItemTemplate() { }

        public ItemTemplate(int globalId, string name,
            string thumbnailSmallPath, string thumbnailMediumPath, string thumbnailLargePath, int height, int width)
        {
            this._GlobalId = globalId;
            this._Name = name;
            this._ThumbnailSmallPath = thumbnailSmallPath;
            this._ThumbnailMediumPath = thumbnailMediumPath;
            this._ThumbnailLargePath = thumbnailLargePath;
            this._Height = height;
            this._Width = width;
        }

        public int GlobalId
        {
            get { return _GlobalId; }
            set { _GlobalId = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string ThumbnailSmallPath
        {
            get { return _ThumbnailSmallPath; }
            set { _ThumbnailSmallPath = value; }
        }

        public string ThumbnailMediumPath
        {
            get { return _ThumbnailMediumPath; }
            set { _ThumbnailMediumPath = value; }
        }

        public string ThumbnailLargePath
        {
            get { return _ThumbnailLargePath; }
            set { _ThumbnailLargePath = value; }
        }

        public int Height
        {
            get { return _Height; }
            set { _Height = value; }
        }

        public int Width
        {
            get { return _Width; }
            set { _Width = value; }
        }
    }
}
