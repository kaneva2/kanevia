///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class ItemPathTexture
    {
        public enum ImageOpacity { UNKNOWN, FULLY_OPAQUE, ONE_BIT_ALPHA, TRANSLUCENT };

        private int _globalId;
        private Int64 _ordinal;
        private UInt32 _width;
        private UInt32 _height;
        private ImageOpacity _opacity;
        private UInt32 _averageColor;

        public ItemPathTexture(int globalId, Int64 ordinal, UInt32 width, UInt32 height, ImageOpacity opacity, UInt32 averageColor)
		{
			_globalId = globalId;
			_ordinal = ordinal;
            _width = width;
            _height = height;
            _opacity = opacity;
            _averageColor = averageColor;
		}

        public int GlobalId { get { return _globalId; } }
        public Int64 Ordinal { get { return _ordinal; } }
        public UInt32 Width { get { return _width; } }
        public UInt32 Height { get { return _height; } }
        public ImageOpacity Opacity { get { return _opacity; } }
        public UInt32 AverageColor { get { return _averageColor; } }
    }
}
