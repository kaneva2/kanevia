///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{

    [Serializable]
    public class ItemCategory
    {
        private UInt32 _ItemCategoryId = 0;
        private string _Name = "Unknown";
        private string _Description = "Unknown";
        private UInt32 _ParentCategoryId = 0;
        private int _SortOrder = 0;
        private string _MarketingPath = "";
		private int _UploadType = 0;
		private int _ActorGroup = 0;

        public enum GameCategories
        {
            Armor = 255,
            Animals = 256,
            Pets = 257,
            Consumables = 258,
            Energy = 259,
            Generic_Loot = 260,
            Monsters = 261,
            Placeables = 262,
            Vendors = 263,
            NPC = 264,
            Weapons = 265
        }

        public ItemCategory() { }

        public ItemCategory(UInt32 itemCategoryId, string name, string description, UInt32 parentCategoryId, int sortOrder, string marketingPath, int uploadType, int actorGroup)
        {
            this._ItemCategoryId = itemCategoryId;
            this._Name = name;
            this._Description = description;
            this._ParentCategoryId = parentCategoryId;
            this._SortOrder = sortOrder;
            this._MarketingPath = marketingPath;
			this._UploadType = uploadType;
			this._ActorGroup = actorGroup;
        }

        public UInt32 ItemCategoryId
        {
           get {return _ItemCategoryId;}
           set {_ItemCategoryId = value;}
        }

        public string MarketingPath
        {
            get { return _MarketingPath; }
            set { _MarketingPath = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string Description
        {
           get {return _Description;}
           set {_Description = value;}
        }

        public UInt32 ParentCategoryId
        {
           get {return _ParentCategoryId;}
           set {_ParentCategoryId = value;}
        }

        public int SortOrder
        {
           get {return _SortOrder;}
           set {_SortOrder = value;}
        }


		public int UploadType
		{
			get { return _UploadType; }
			set { _UploadType = value; }
		}

		public int ActorGroup
		{
			get { return _ActorGroup; }
			set { _ActorGroup = value; }
		}

    }
}
