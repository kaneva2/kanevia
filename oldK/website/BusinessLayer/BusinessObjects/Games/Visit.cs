///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class Vist
    {
        private string _location = "";
        private string _STPURL = "";
        private int _gameId = 0;
        private string _imagePath = "";

        public Vist() { }

        public Vist(string location, string STPURL, int gameId, string imagePath)
        {
            _location = location;
            _STPURL = STPURL;
            _gameId = gameId;
            _imagePath = imagePath;
        }

        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public string STPURL
        {
            get { return _STPURL; }
            set { _STPURL = value; }
        }

        public int GameId
        {
            get { return _gameId; }
            set { _gameId = value; }
        }

        public string ImagePath
        {
            get { return _imagePath; }
            set { _imagePath = value; }
        }
    }
}
