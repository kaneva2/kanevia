///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class SGILootItem
    {
        #region Declarations

        private ScriptGameItem _Item;
        private string _QuantityRange;
        private double _DropChance;

        #endregion

        /// <summary>
        /// Default constructor for Item class.
        /// </summary>
        public SGILootItem(ScriptGameItem item, string quantityRange, double dropChance = 100.0)
        {
            _Item = item;
            _QuantityRange = quantityRange;
            _DropChance = dropChance;
        }

        #region Properties

        public ScriptGameItem Item
        {
            get { return _Item; }
            set { _Item = value; }
        }

        public string QuantityRange
        {
            get { return _QuantityRange; }
            set { _QuantityRange = value; }
        }

        public double DropChance
        {
            get { return _DropChance; }
            set { _DropChance = value; }
        }

        #endregion
    }
}
