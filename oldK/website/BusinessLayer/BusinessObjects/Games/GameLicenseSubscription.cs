///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class GameLicenseSubscription
    {
        public enum LicenseSubscriptionIdTypes
        {
            Consumer = 1,
            Independent = 2,
            Professional = 3,
            BetaTrial = 4,
            StandAlone = 5,
            Educational = 6
        }

        private int _LicenseSubscriptionId;
        private string _Name;
        private int _LengthOfSubscription;
        private Double _Amount;
        private string _AmountKeiPointId;
        private int _MaxServerUsers;
        private int _DiskQuota;

        public GameLicenseSubscription() { }

        public GameLicenseSubscription(int licenseSubscriptionId, string name, int lengthOfSubscription, Double amount, string amountKeiPointId, int maxServerUsers, int diskQuota)
        {
            this._LicenseSubscriptionId = licenseSubscriptionId;
            this._Name = name;
            this._LengthOfSubscription = lengthOfSubscription;
            this._Amount = amount;
            this._AmountKeiPointId = amountKeiPointId;
            this._MaxServerUsers = maxServerUsers;
            this._DiskQuota = diskQuota;
        }

        public int LicenseSubscriptionId
        {
            get { return _LicenseSubscriptionId; }
            set { _LicenseSubscriptionId = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int LengthOfSubscription
        {
            get { return _LengthOfSubscription; }
            set { _LengthOfSubscription = value; }
        }

        public Double Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public string AmountKeiPointId
        {
            get { return _AmountKeiPointId; }
            set { _AmountKeiPointId = value; }
        }

        public int MaxServerUsers
        {
            get { return _MaxServerUsers; }
            set { _MaxServerUsers = value; }
        }

        public int DiskQuota
        {
            get { return _DiskQuota; }
            set { _DiskQuota = value; }
        }

    }
}
