///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
	public class ItemPath
	{
		public ItemPath(int globalId, string pathType, Int64 ordinal, string path, int fileSize, string fileHash, int compressedSize, uint uniqueId)
		{
			GlobalId = globalId;
			PathType = pathType;
			Ordinal = ordinal;
			Path = path;
            FileSize = fileSize;
            FileHash = fileHash;
            CompressedSize = compressedSize;
            UniqueId = uniqueId;
		}

		public int GlobalId { get; set; }
        public string PathType { get; private set; }
        public Int64 Ordinal { get; private set; }
        public string Path { get; private set; }
        public int FileSize { get; private set; }
        public string FileHash { get; private set; }
        public int CompressedSize { get; private set; }
        public uint UniqueId { get; private set; }
    }
}
