///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class SGIQuest
    {
        #region Declarations

        private ScriptGameItem _QuestGameItem;
        private ScriptGameItem _RequiredItem;
        private int _RequiredItemQuantity;
        private ScriptGameItem _RewardItem;
        private int _RewardItemQuantity;
        private ScriptGameItem _PrerequisiteQuest;
        private bool _Repeatable;
        private int _RepeatTime;

        #endregion

        /// <summary>
        /// Default constructor for Item class.
        /// </summary>
        public SGIQuest() { }

        #region Properties

        public ScriptGameItem QuestGameItem
        {
            get { return _QuestGameItem; }
            set { _QuestGameItem = value; }
        }

        public ScriptGameItem RequiredItem
        {
            get { return _RequiredItem; }
            set { _RequiredItem = value; }
        }

        public int RequiredItemQuantity
        {
            get { return _RequiredItemQuantity; }
            set { _RequiredItemQuantity = value; }
        }

        public ScriptGameItem RewardItem
        {
            get { return _RewardItem; }
            set { _RewardItem = value; }
        }

        public int RewardItemQuantity
        {
            get { return _RewardItemQuantity; }
            set { _RewardItemQuantity = value; }
        }

        public ScriptGameItem PrerequisiteQuest
        {
            get { return _PrerequisiteQuest; }
            set { _PrerequisiteQuest = value; }
        }

        public bool Repeatable
        {
            get { return _Repeatable; }
            set { _Repeatable = value; }
        }

        public int RepeatTime
        {
            get { return _RepeatTime; }
            set { _RepeatTime = value; }
        }

        #endregion
    }
}
