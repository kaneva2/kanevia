///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class GameLicenseContact
    {
        private UInt32 _GameLicenseContactId;
        private string _Name;
        private string _OrgName;
        private string _PhoneNumber;
        private string _Email;
        private string _AboutGame;
        private int _GameLicenseId;

        public GameLicenseContact() { }

        public GameLicenseContact(UInt32 gameLicenseContactId, string name, string orgName, string phoneNumber, string email, string aboutGame, int gameLicenseId)
        {
            this._GameLicenseContactId = gameLicenseContactId;
            this._Name = name;
            this._OrgName = orgName;
            this._PhoneNumber = phoneNumber;
            this._Email = email;
            this._AboutGame = aboutGame;
            this._GameLicenseId = gameLicenseId;
        }

        public UInt32 GameLicenseContactId
        {
            get { return _GameLicenseContactId; }
            set { _GameLicenseContactId = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string OrgName
        {
            get { return _OrgName; }
            set { _OrgName = value; }
        }

        public string PhoneNumber
        {
            get { return _PhoneNumber; }
            set { _PhoneNumber = value; }
        }

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        public string AboutGame
        {
            get { return _AboutGame; }
            set { _AboutGame = value; }
        }

        public int GameLicenseId
        {
            get { return _GameLicenseId; }
            set { _GameLicenseId = value; }
        }

    }
}
