///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class CrewPeep
    {
        private int _CrewId = 0;
        private int _UserId = 0;
        private DateTime _CreatedDatetime;

        public CrewPeep() { }

        public CrewPeep (int crewId, int userId, DateTime createdDatetime)
        {
            this._CrewId = crewId;
            this._UserId = userId;
            this._CreatedDatetime = createdDatetime;
        }

        public int CrewId
        {
            get { return _CrewId; }
            set { _CrewId = value; }
        }

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public DateTime CreatedDatetime
        {
            get { return _CreatedDatetime; }
            set { _CreatedDatetime = value; }
        }

    }
}
