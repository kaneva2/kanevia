///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class UsersSubscriptions
    {
        private bool _HasVIPPass = false;
        private bool _HasAccessPass = false;
        private List<UserSubscription> _UserOwnedSubscriptions = new List<UserSubscription>();

        /// <summary>
        /// Default constructor for UserStats class.
        /// </summary>
        public UsersSubscriptions() { }

        /// <summary>
        /// Overloaded constructor for the UserStats class.
        /// </summary>
        public UsersSubscriptions(bool hasAccessPass, bool hasVIPPass, List<UserSubscription> userOwnedSubscriptions)
        {
            this._HasAccessPass = hasAccessPass;
            this._HasVIPPass = hasVIPPass;
            this._UserOwnedSubscriptions = userOwnedSubscriptions;
        }

        /// <summary>
        /// NumberOfLogins
        /// </summary>
        public virtual bool HasAccessPass
        {
            get { return _HasAccessPass; }
            set { _HasAccessPass = value; }
        }

        public virtual bool HasVIPPass
        {
            get { return _HasVIPPass; }
            set { _HasVIPPass = value; }
        }

        public virtual List<UserSubscription> UserOwnedSubscriptions
        {
            get { return _UserOwnedSubscriptions; }
            set { _UserOwnedSubscriptions = value; }
        }

    }
}