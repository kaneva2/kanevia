///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class UserLoginIssue
    {
        private string _UserName = "";
        private int _UserId = 0;
        private string _IPAddress = "";
        private string _Description = "";
        private DateTime _CreatedDatetime;

        public UserLoginIssue() { }

        public UserLoginIssue(string username, int userId, string ipAddress, string description, DateTime createdDatetime)
        {
            this._UserName = username;
            this._UserId = userId;
            this._IPAddress = ipAddress;
            this._Description = description;
            this._CreatedDatetime = createdDatetime;
        }

        public string Username
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }
        }

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public DateTime CreatedDatetime
        {
            get { return _CreatedDatetime; }
            set { _CreatedDatetime = value; }
        }
    }
}
