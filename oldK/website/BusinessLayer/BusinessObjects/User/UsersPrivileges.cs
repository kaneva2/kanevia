///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class UsersPrivileges
    {
        private int _SiteRoleId;
        private NameValueCollection _UserPrivileges;

        /// <summary>
        /// Default constructor for UserStats class.
        /// </summary>
        public UsersPrivileges() 
        {
            this._UserPrivileges = new NameValueCollection();
            this._SiteRoleId = 0;
        }

        /// <summary>
        /// Overloaded constructor for the UserStats class.
        /// </summary>
        public UsersPrivileges(NameValueCollection userPrivileges, int siteRoleId)
        {
            this._UserPrivileges = userPrivileges;
            this._SiteRoleId = siteRoleId;
        }

        public virtual int SiteRoleId
        {
            get { return _SiteRoleId; }
            set { _SiteRoleId = value; }
        }

        public virtual NameValueCollection UserPrivileges
        {
            get { return _UserPrivileges; }
            set { _UserPrivileges = value; }
        }
    }
}
