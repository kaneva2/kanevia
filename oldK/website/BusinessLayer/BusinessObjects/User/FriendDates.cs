///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class FriendDates
    {
        private int _UserId = 0;
        private string _Username = "";
        private string _DisplayName = "";
        private DateTime _BirthDate;
        private DateTime _SignupDate;
        private string _Gender = "M";
        private string _FriendThumbnailSmallPath = "";
        private string _FriendThumbnailMediumPath = "";
        private string _FriendThumbnailSquarePath = "";
        private int _FriendUserId = 0;
        private string _FriendUsername = "";
        private string _FriendEmail = "";
        private int _FriendStatusId = 0;
        private bool _FriendSendBirthdayEmail = false;

        public FriendDates() { }

        public FriendDates (int userId, string username, DateTime birthDate, DateTime signupDate)
        {
            this._UserId = userId;
            this._Username = username;
            this._BirthDate = birthDate;
            this._SignupDate = signupDate;
        }

        public FriendDates (int userId, string username, string displayName, DateTime birthDate, string gender,  
            string friendThumbnailSmallPath, string friendThumbnailMediumPath, string friendThumbnailSquarePath, int friendUserId, 
            string friendUsername, string friendEmail, int friendStatusId, bool friendSendBirthdayEmail)
        {
            this._UserId = userId;
            this._Username = username;
            this._DisplayName = displayName;
            this._BirthDate = birthDate;
            this._Gender = gender;
            this._FriendThumbnailSmallPath = friendThumbnailSmallPath;
            this._FriendThumbnailMediumPath = friendThumbnailMediumPath;
            this._FriendThumbnailSquarePath = friendThumbnailSquarePath;
            this._FriendUserId = friendUserId;
            this._FriendUsername = friendUsername;
            this._FriendEmail = friendEmail;
            this._FriendStatusId = friendStatusId;
            this._FriendSendBirthdayEmail = friendSendBirthdayEmail;
        }

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public string Username
        {
            get { return _Username; }
            set { _Username = value; }
        }

        public string DisplayName
        {
            get { return _DisplayName; }
            set { _DisplayName = value; }
        }

        public DateTime BirthDate
        {
            get { return _BirthDate; }
            set { _BirthDate = value; }
        }

        public DateTime SignupDate
        {
            get { return _SignupDate; }
            set { _SignupDate = value; }
        }

        public string Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }

        public string FriendThumbnailSmallPath
        {
            get { return _FriendThumbnailSmallPath; }
            set { _FriendThumbnailSmallPath = value; }
        }

        public string FriendThumbnailMediumPath
        {
            get { return _FriendThumbnailMediumPath; }
            set { _FriendThumbnailMediumPath = value; }
        }

        public string FriendThumbnailSquarePath
        {
            get { return _FriendThumbnailSquarePath; }
            set { _FriendThumbnailSquarePath = value; }
        }

        public int FriendUserId
        {
            get { return _FriendUserId; }
            set { _FriendUserId = value; }
        }

        public string FriendUsername
        {
            get { return _FriendUsername; }
            set { _FriendUsername = value; }
        }

        public string FriendEmail
        {
            get { return _FriendEmail; }
            set { _FriendEmail = value; }
        }

        public int FriendStatusId
        {
            get { return _FriendStatusId; }
            set { _FriendStatusId = value; }
        }

        public bool FriendEmailIsValidated
        {
            get { return _FriendStatusId.Equals (1); }
        }

        public bool FriendSendBirthdayEmail
        {
            get { return _FriendSendBirthdayEmail; }
            set { _FriendSendBirthdayEmail = value; }
        }
    }
}
