///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Linq;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class Preferences
    {
        private int _UserId = 0;
        private string _AlwaysPurchaseWithCurrencyType = "NOTSET";
        private bool _FilterByCountry = false;

        public Preferences (int userId, string alwaysPurchaseWithCurrencyType, bool filterByCountry)
        {
            this._UserId = userId;
            this._AlwaysPurchaseWithCurrencyType = alwaysPurchaseWithCurrencyType;
            this._FilterByCountry = filterByCountry;
        }

        /// <summary>
        /// Default constructor for UserStats class.
        /// </summary>
        public Preferences() { }

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public virtual string AlwaysPurchaseWithCurrencyType
        {
            get { return _AlwaysPurchaseWithCurrencyType; }
            set { _AlwaysPurchaseWithCurrencyType = value; }
        }

        public virtual bool FilterByCountry
        {
            get { return _FilterByCountry; }
            set { _FilterByCountry = value; }
        }

        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if (obj == null || GetType() != obj.GetType()) return false;
            
            Preferences p = (Preferences)obj;
            var type = GetType();
            var unequalProperties =
                from pi in
                    type.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                let selfValue = type.GetProperty(pi.Name).GetValue(this, null)
                let toValue = type.GetProperty(pi.Name).GetValue(obj, null)
                where selfValue != toValue &&
                (selfValue == null || !selfValue.Equals(toValue))
                select selfValue;
            return !unequalProperties.Any();
        }
    }
}
