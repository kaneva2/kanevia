///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class NotificationsProfile
    {
       
        public enum NotificationTypes
        {
            FriendRequest = 1,
            PlayerRaved = 2,
            NewMessage = 3,   //ViewInbox
            InWorldInvite = 4,  // GoToURL
            BalanceChange = 5,
            Fame_PacketProgress = 6,
            EventReminder = 7,
            InWorldGiftInvite = 8,
            WebLink = 9,                                                          
            GoToURL = 10,                                             
            FameXPEarned = 11,                                                                                            
            FameLevelUp = 12,                                                                       
            FameBadgeEarned = 13,                                                                     
            Nudge = 14,                                                                                                                                                                                                                    
            GeneralMessage = 15,                                                                        
            FameXPAndRewardsEarned = 16,                                                                                                        
            GiftInvite= 17,
            ReceivedCreditGift = 18,
            EventCreated = 19,
            WebWorldInvite = 20
        }

        public NotificationsProfile(uint notificationId, int userId, int notificationType, string XMLData, string notifText)
        {
            this.NotificationId = notificationId;
            this.NotificationType = notificationType;
            this.UserId = userId;
            this.XMLData = XMLData;
            this.NotifText = notifText;
        }

        /// <summary>
        /// Default constructor for NotificationsProfile class.
        /// </summary>
        public NotificationsProfile() { }

        public uint NotificationId { get; set; }
        public int NotificationType { get; set; }
        public int UserId { get; set; }
        public string XMLData { get; set; }
        public string NotifText { get; set; }
        public string Param1 { get; set; }
        public string Param2 { get; set; }
        public string Param3 { get; set; }
        public string Param4 { get; set; }
        public DateTime AddedDatetime { get; set; }

     
    }
}
