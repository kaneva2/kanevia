///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class Friend
    {
        private int _userId;
        private string _username;
        private string _displayName;
        private DateTime _GluedDate;
        private string _UState;
        private string _Location;
        private string _Gender;
        private string _NameNoSpaces;
        private string _ThumbnailSmallPath;
        private string _ThumbnailMediumPath;
        private string _ThumbnailLargePath;
        private string _ThumbnailSquarePath;
        private int _NumberOfDiggs;
        private int _NumberOfViews;
        private int _StatusId = 0;
        private int _Age = 0;
        private bool _IsMatureProfile;
        private bool _IsUnder18;
        private bool _ShowAge;
        private bool _ShowGender;
        private bool _ShowLocation;

        private FacebookSettings _facebookSettings = new FacebookSettings ();

        /// <summary>
        /// Default constructor for Friend class.
        /// </summary>
        public Friend() { }

        public Friend (int userId, string username, string displayName, string Gender, DateTime GluedDate, string UState, string Location,
            string NameNoSpaces, string ThumbnailSmallPath, string ThumbnailMediumPath, string ThumbnailLargePath,
            int NumberOfViews, int NumberOfDiggs, int statusId, int Age, bool isMatureProfile, bool isUnder18,
            bool showAge, bool showGender, bool showLocation)
            : this (userId, username, displayName, Gender, GluedDate, UState, Location, NameNoSpaces, ThumbnailSmallPath,
                ThumbnailMediumPath, ThumbnailLargePath, ThumbnailMediumPath, NumberOfViews, NumberOfDiggs, statusId, 
                Age, isMatureProfile, isUnder18, showAge, showGender, showLocation)
        {}

        /// <summary>
        /// Overloaded constructor for the Friend class.
        /// </summary>
        public Friend(int userId, string username, string displayName, string Gender, DateTime GluedDate, string UState, string Location,
            string NameNoSpaces, string ThumbnailSmallPath, string ThumbnailMediumPath, string ThumbnailLargePath, string ThumbnailSquarePath,
            int NumberOfViews, int NumberOfDiggs, int statusId, int Age, bool isMatureProfile, bool isUnder18, bool showAge, bool showGender, bool showLocation
            )
        {
            this._userId = userId;
            this._username = username;
            this._displayName = displayName;
            this._Gender = Gender;
            this._Location = Location;
            this._UState = UState;
            this._GluedDate = GluedDate;
            this._NameNoSpaces = NameNoSpaces;
            this._ThumbnailSmallPath = ThumbnailSmallPath;
            this._ThumbnailMediumPath = ThumbnailMediumPath;
            this._ThumbnailLargePath = ThumbnailLargePath;
            this._ThumbnailSquarePath = ThumbnailSquarePath;
            this._NumberOfViews = NumberOfViews;
            this._NumberOfDiggs = NumberOfDiggs;
            this._StatusId = statusId;
            this._Age = Age;
            this._IsMatureProfile = isMatureProfile;
            this._ShowAge = showAge;
            this._ShowGender = showGender;
            this._ShowLocation = showLocation;
            this._IsUnder18 = isUnder18;
        }

         /// <summary>
        /// Overloaded constructor for the Friend class.
        /// </summary>
        public Friend(int userId, string username, string displayName, string Gender, DateTime GluedDate, string UState, string Location,
            string NameNoSpaces, string ThumbnailSmallPath, string ThumbnailMediumPath, string ThumbnailLargePath,
            int NumberOfViews, int NumberOfDiggs, int statusId, int Age, bool isMatureProfile)
              : this (userId, username, displayName, Gender, GluedDate, UState, Location,
                    NameNoSpaces, ThumbnailSmallPath, ThumbnailMediumPath, ThumbnailLargePath,
                    ThumbnailMediumPath, NumberOfViews, NumberOfDiggs, statusId, Age, isMatureProfile)
        {}

        public Friend (int userId, string username, string displayName, string Gender, DateTime GluedDate, string UState, string Location,
            string NameNoSpaces, string ThumbnailSmallPath, string ThumbnailMediumPath, string ThumbnailLargePath,
            string ThumbnailSquarePath, int NumberOfViews, int NumberOfDiggs, int statusId, int Age, bool isMatureProfile)
        {
            this._userId = userId;
            this._username = username;
            this._displayName = displayName;
            this._Gender = Gender;
            this._Location = Location;
            this._UState = UState;
            this._GluedDate = GluedDate;
            this._NameNoSpaces = NameNoSpaces;
            this._ThumbnailSmallPath = ThumbnailSmallPath;
            this._ThumbnailMediumPath = ThumbnailMediumPath;
            this._ThumbnailLargePath = ThumbnailLargePath;
            this._ThumbnailSquarePath = ThumbnailSquarePath;
            this._NumberOfViews = NumberOfViews;
            this._NumberOfDiggs = NumberOfDiggs;
            this._StatusId = statusId;
            this._Age = Age;
            this._IsMatureProfile = isMatureProfile;
        }

        /// <summary>
        /// Gets or sets unique user identifier.
        /// The Identity Field Design Pattern. 
        /// </summary>
        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        /// <summary>
        /// Gets or sets username
        /// </summary>
        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

        public string Gender
        {
            get { return _Gender;}
            set { _Gender = value;}
        }

        public string UState
        {
            get { return _UState; }
            set { _UState = value; }
        }

        public string Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        public DateTime GluedDate
        {
            get { return _GluedDate; }
            set { _GluedDate = value; }
        }

        public string NameNoSpaces
        {
            get { return _NameNoSpaces;}
            set { _NameNoSpaces = value;}
        }

        public string ThumbnailSmallPath
        {
            get { return _ThumbnailSmallPath;}
            set { _ThumbnailSmallPath = value;}
        }

        public string ThumbnailMediumPath
        {
            get { return _ThumbnailMediumPath;}
            set { _ThumbnailMediumPath = value;}
        }

        public string ThumbnailLargePath
        {
            get { return _ThumbnailLargePath;}
            set { _ThumbnailLargePath = value;}
        }

        public string ThumbnailSquarePath
        {
            get { return _ThumbnailSquarePath; }
            set { _ThumbnailSquarePath = value; }
        }

        public int NumberOfDiggs
        {
            get { return _NumberOfDiggs;}
            set { _NumberOfDiggs = value;}
        }

        public int NumberOfViews
        {
            get { return _NumberOfViews;}
            set { _NumberOfViews = value;}
        }

        public int StatusId
        {
            get { return _StatusId; }
            set { _StatusId = value; }
        }

        public int Age
        {
            get { return _Age; }
            set { _Age = value; }
        }

        public bool IsMatureProfile
        {
            get { return _IsMatureProfile; }
            set { _IsMatureProfile = value; }
        }

        public bool IsUnder18
        {
            get { return _IsUnder18; }
            set { _IsUnder18 = value; }
        }

        public bool ShowAge
        {
            get { return _ShowAge; }
            set { _ShowAge = value; }
        }

        public bool ShowGender
        {
            get { return _ShowGender; }
            set { _ShowGender = value; }
        }

        public bool ShowLocation
        {
            get { return _ShowLocation; }
            set { _ShowLocation = value; }
        }

        public FacebookSettings FacebookSettings
        {
            get { return _facebookSettings; }
            set { _facebookSettings = value; }
        }
    }
}

