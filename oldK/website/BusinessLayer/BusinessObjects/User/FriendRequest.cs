///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class FriendRequest
    {
        private int _UserId;
        private string _Username;
        private string _DisplayName;
        private string _Location;
        private string _Gender;
        private string _NameNoSpaces;
        private string _ThumbnailSmallPath;
        private string _ThumbnailMediumPath;
        private string _ThumbnailLargePath;
        private DateTime _BirthDate;
        private string _ZipCode;
        private int _Age = 0;
        private DateTime _RequestDate = DateTime.MinValue;
        private DateTime _CurrentDate = DateTime.Now;
        
        /// <summary>
        /// Default constructor for FriendRequest class.
        /// </summary>
        public FriendRequest() { }

        public FriendRequest (int userId, string username, string displayName, string Gender, DateTime BirthDate, string Location,
            string NameNoSpaces, string ThumbnailSmallPath, string ThumbnailMediumPath, string ThumbnailLargePath,
            string ZipCode, int Age, DateTime RequestDate, DateTime CurrentDate 
            )
        {
            this._UserId = userId;
            this._Username = username;
            this._DisplayName = displayName;
            this._Gender = Gender;
            this._BirthDate = BirthDate;
            this._Location = Location;
            this._NameNoSpaces = NameNoSpaces;
            this._ThumbnailSmallPath = ThumbnailSmallPath;
            this._ThumbnailMediumPath = ThumbnailMediumPath;
            this._ThumbnailLargePath = ThumbnailLargePath;
            this._ZipCode = ZipCode;
            this._Age = Age;
            this._RequestDate = RequestDate;
            this._CurrentDate = CurrentDate;
        }

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public string Username
        {
            get { return _Username; }
            set { _Username = value; }
        }

        public string DisplayName
        {
            get { return _DisplayName; }
            set { _DisplayName = value; }
        }

        public string Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }

        public DateTime BirthDate
        {
            get { return _BirthDate; }
            set { _BirthDate = value; }
        }

        public string Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        public string NameNoSpaces
        {
            get { return _NameNoSpaces; }
            set { _NameNoSpaces = value; }
        }

        public string ThumbnailSmallPath
        {
            get { return _ThumbnailSmallPath; }
            set { _ThumbnailSmallPath = value; }
        }

        public string ThumbnailMediumPath
        {
            get { return _ThumbnailMediumPath; }
            set { _ThumbnailMediumPath = value; }
        }

        public string ThumbnailLargePath
        {
            get { return _ThumbnailLargePath; }
            set { _ThumbnailLargePath = value; }
        }

        public string ZipCode
        {
            get { return _ZipCode; }
            set { _ZipCode = value; }
        }

        public int Age
        {
            get { return _Age; }
            set { _Age = value; }
        }

        public DateTime RequestDate
        {
            get { return _RequestDate; }
            set { _RequestDate = value; }
        }

        public DateTime CurrentDate
        {
            get { return _CurrentDate; }
            set { _CurrentDate = value; }
        }
    }
}
