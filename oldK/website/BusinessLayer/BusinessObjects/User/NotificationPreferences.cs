///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
	[Serializable]
	public class NotificationPreferences
	{
		private int _UserId = 0;
		private bool _NotifyBlastComments = true;
		private bool _NotifyMediaComments = true;
		private bool _NotifyShopCommentsPurchases = true;
        private bool _NotifyEventInvites = true;
        private bool _NotifyFriendBirthdays = true;
        private bool _NotifyWorldBlasts = true;
        private bool _NotifyWorldRequests = true;
		

		public NotificationPreferences(int userId, bool notifyBlastComments, bool notifyMediaCommments,
            bool notifyShopCommentsPurchases, bool notifyEventInvites, bool notifyFriendBirthdays)
		{
			this._UserId = userId;
			this._NotifyBlastComments = notifyBlastComments;
			this._NotifyMediaComments = notifyMediaCommments;
			this._NotifyShopCommentsPurchases = notifyShopCommentsPurchases;
            this._NotifyEventInvites = notifyEventInvites;
		}

        public NotificationPreferences (int userId, bool notifyBlastComments, bool notifyMediaCommments,
            bool notifyShopCommentsPurchases, bool notifyEventInvites, bool notifyFriendBirthdays, bool notifyWorldBlasts, bool notifyWorldRequests)
        {
            this._UserId = userId;
            this._NotifyBlastComments = notifyBlastComments;
            this._NotifyMediaComments = notifyMediaCommments;
            this._NotifyShopCommentsPurchases = notifyShopCommentsPurchases;
            this._NotifyEventInvites = notifyEventInvites;
            this._NotifyFriendBirthdays = notifyFriendBirthdays;
            this._NotifyWorldBlasts = notifyWorldBlasts;
            this._NotifyWorldRequests = notifyWorldRequests;
        }


		/// <summary>
        /// Default constructor for NotificationPreferences class.
		/// </summary>
		public NotificationPreferences() { }

		public int UserId
		{
			get { return _UserId; }
			set { _UserId = value; }
		}

		public virtual bool NotifyBlastComments 
		{
			get { return _NotifyBlastComments; }
			set { _NotifyBlastComments = value; }
		}

		public virtual bool NotifyMediaComments
		{
			get { return _NotifyMediaComments; }
			set { _NotifyMediaComments = value; }
		}

		public virtual bool NotifyShopCommentsPurchases
		{
			get { return _NotifyShopCommentsPurchases; }
			set { _NotifyShopCommentsPurchases = value; }
		}

        public virtual bool NotifyEventInvites
		{
            get { return _NotifyEventInvites; }
            set { _NotifyEventInvites = value; }
		}
        
        public virtual bool NotifyFriendBirthdays
        {
            get { return _NotifyFriendBirthdays; }
            set { _NotifyFriendBirthdays = value; }
        }

        public virtual bool NotifyWorldBlasts
        {
            get { return _NotifyWorldBlasts; }
            set { _NotifyWorldBlasts = value; }
        }

        public virtual bool NotifyWorldRequests
        {
            get { return _NotifyWorldRequests; }
            set { _NotifyWorldRequests = value; }
        }
    }
}
