///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class UserStats
    {
        private int _NumberOfLogins;
        private int _NumberOutboxMessages;
        private int _Number3DAppRequests;
        private int _NumberCommunityInvites;

        private int _NumberForumPosts;
        private int _NumberOfFriends;
        private int _NumberOfPending;
        private int _NumberOfRequests;
        private int _NumberOfNewMessages;
        private int _NumberOfInboxMessages;

        private int _NumberOfBlogs;
        private int _NumberOfComments;
        private int _NumberOfAssetDiggs;
        private int _NumberBlastsSent;
        private int _IOwn;
        private int _IModerate;
        private int _IBelongTo;

        private UInt32 _InterestCount;
        private UInt32 _NumberAssetShares;
        private UInt32 _NumberCommunityShares;
        private UInt32 _NumberInvitesSent;
        private UInt32 _NumberInvitesAccepted;
        private UInt32 _NumberInvitesInWorld;

        private Double _NumberFriendInviteRewards;

        /// <summary>
        /// Default constructor for UserStats class.
        /// </summary>
        public UserStats() { }

        /// <summary>
        /// Overloaded constructor for the UserStats class.
        /// </summary>
        public UserStats(int numberOfLogins, int numberForumPosts, int numberOfFriends,
            int numberOfPending, int numberOfRequests, int numberOfInboxMessages, int numberOfNewMessages,
            int numberOfBlogs, int numberOfComments, int numberOfAssetDiggs, int numberBlastsSent, 
            int iOwn, int iModerate, int iBelongTo,
            UInt32 interestCount, UInt32 numberAssetShares, UInt32 numberCommunityShares,
            UInt32 numberInvitesSent, UInt32 numberInvitesAccepted, UInt32 numberInvitesInWorld, 
            Double numberFriendInviteRewards, int number3DAppRequests, int numberCommunityInvites)
        {
            this._NumberOfLogins = numberOfLogins;
            this._NumberForumPosts = numberForumPosts;
            this._NumberOfFriends = numberOfFriends;
            this._NumberOfPending = numberOfPending;
            this._NumberOfRequests = numberOfRequests;
            this._NumberOfInboxMessages = numberOfInboxMessages;
            this._NumberOfNewMessages = numberOfNewMessages;
            this._Number3DAppRequests = number3DAppRequests;
            this._NumberCommunityInvites = numberCommunityInvites;

            this._NumberOfBlogs = numberOfBlogs;
            this._NumberOfComments = numberOfComments;
            this._NumberOfAssetDiggs = numberOfAssetDiggs;
            this._NumberBlastsSent = numberBlastsSent;
            this._IOwn = iOwn;
            this._IModerate = iModerate;
            this._IBelongTo = iBelongTo;

            this._InterestCount = interestCount;
            this._NumberAssetShares = numberAssetShares;
            this._NumberCommunityShares = numberCommunityShares;
            this._NumberInvitesSent = numberInvitesSent;
            this._NumberInvitesAccepted = numberInvitesAccepted;
            this._NumberInvitesInWorld = numberInvitesInWorld;
            this._NumberFriendInviteRewards = numberFriendInviteRewards;
        }

        /// <summary>
        /// NumberOfLogins
        /// </summary>
        public virtual int NumberOfLogins
        {
            get { return _NumberOfLogins; }
            set { _NumberOfLogins = value; }
        }

        public virtual int NumberOfInboxMessages
        {
            get { return _NumberOfInboxMessages; }
            set { _NumberOfInboxMessages = value; }
        }

        public virtual int NumberOutboxMessages
        {
            get { return _NumberOutboxMessages; }
            set { _NumberOutboxMessages = value; }
        }

        public virtual int NumberForumPosts
	    {
		    get { return _NumberForumPosts;}
		    set { _NumberForumPosts = value;}
	    }

        public virtual int NumberOfFriends
	    {
		    get { return _NumberOfFriends;}
		    set { _NumberOfFriends = value;}
	    }

        public virtual int NumberOfPending
	    {
		    get { return _NumberOfPending;}
		    set { _NumberOfPending = value;}
	    }

        public virtual int NumberOfRequests
	    {
		    get { return _NumberOfRequests;}
		    set { _NumberOfRequests = value;}
	    }

        public virtual int NumberOfNewMessages
	    {
		    get { return _NumberOfNewMessages;}
		    set { _NumberOfNewMessages = value;}
	    }

        public virtual int NumberOfBlogs
	    {
		    get { return _NumberOfBlogs;}
		    set { _NumberOfBlogs = value;}
	    }

        public virtual int NumberOfComments
	    {
		    get { return _NumberOfComments;}
		    set { _NumberOfComments = value;}
	    }

        public virtual int NumberOfAssetDiggs
	    {
		    get { return _NumberOfAssetDiggs;}
		    set { _NumberOfAssetDiggs = value;}
	    }

        public virtual int NumberBlastsSent
        {
            get { return _NumberBlastsSent; }
            set { _NumberBlastsSent = value; }
        }

        public virtual int CommunitiesIOwn
	    {
		    get { return _IOwn;}
		    set { _IOwn = value;}
	    }

        public virtual int CommunitiesIModerate
	    {
		    get { return _IModerate;}
		    set { _IModerate = value;}
	    }

        public virtual int CommunitiesIBelongTo
	    {
		    get { return _IBelongTo;}
		    set { _IBelongTo = value;}
	    }

        public virtual int Number3DAppRequests
        {
            get { return _Number3DAppRequests; }
            set { _Number3DAppRequests = value; }
        }

        public virtual int NumberCommunityInvites
        {
            get { return _NumberCommunityInvites; }
            set { _NumberCommunityInvites = value; }
        }

        public virtual UInt32 InterestCount
	    {
		    get { return _InterestCount;}
		    set { _InterestCount = value;}
	    }

        public virtual UInt32 NumberAssetShares
	    {
		    get { return _NumberAssetShares;}
		    set { _NumberAssetShares = value;}
	    }

        public virtual UInt32 NumberCommunityShares
	    {
		    get { return _NumberCommunityShares;}
		    set { _NumberCommunityShares = value;}
	    }
        
        public virtual UInt32 NumberInvitesSent
	    {
		    get { return _NumberInvitesSent;}
		    set { _NumberInvitesSent = value;}
	    }
        
        public virtual UInt32 NumberInvitesAccepted
	    {
		    get { return _NumberInvitesAccepted;}
		    set { _NumberInvitesAccepted = value;}
	    }

        public virtual UInt32 NumberInvitesInWorld
	    {
            get { return _NumberInvitesInWorld; }
            set { _NumberInvitesInWorld = value; }
	    }

        public virtual Double NumberFriendInviteRewards
        {
            get { return _NumberFriendInviteRewards; }
            set { _NumberFriendInviteRewards = value; }
        }
    }
}