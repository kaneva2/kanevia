///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class UserSuspension
    {
        private int _BanId = 0;
        private int _UserId = 0;
        private DateTime _BanStartDate;
        private DateTime? _BanEndDate;
        private int? _SystemBanId;

        public UserSuspension(int banId, int userId, DateTime banStartDate, DateTime? banEndDate, int? systemBanId)
        {
            this._BanId = banId;
            this._UserId = userId;
            this._BanStartDate = banStartDate;
            this._BanEndDate = banEndDate;
            this._SystemBanId = systemBanId;
        }

        public int BanId
        {
            get { return _BanId; }
            set { _BanId = value; }
        }

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public DateTime BanStartDate
        {
            get { return _BanStartDate; }
            set { _BanStartDate = value; }
        }

        public DateTime? BanEndDate
        {
            get { return _BanEndDate; }
            set { _BanEndDate = value; }
        }

        public int? SystemBanId
        {
            get { return _SystemBanId; }
            set { _SystemBanId = value; }
        }

        public int BanLengthDays
        {
            get 
            {
                if (IsPermanent)
                    return Int32.MaxValue;

                return (int)((DateTime)BanEndDate).Subtract(BanStartDate).TotalDays;
            }
        }

        public bool IsPermanent
        {
            get { return _BanEndDate == null; }
        }
    }
}