///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class UserMetaGameItemRoyalties
    {
        public const decimal ROYALTY_PERCENTAGE = 0.1M;

        /// <summary>
        /// Overloaded constructor for the UserBalances class.
        /// </summary>
        public UserMetaGameItemRoyalties(int userId, string itemType, uint itemsSpentInOwnedWorlds, uint rewardsReceived, decimal unconvertedBalance)
        {
            UserId = userId;
            ItemType = itemType;
            ItemsSpentInOwnedWorlds = itemsSpentInOwnedWorlds;
            RewardsReceived = rewardsReceived;
            UnconvertedBalance = unconvertedBalance;
        }

        public int UserId { get; set; }
        public string ItemType { get; set; }
        public uint ItemsSpentInOwnedWorlds { get; private set; }
        public uint RewardsReceived { get; private set; }
        public decimal UnconvertedBalance { get; private set; }

        /// <summary>
        /// Calculates the royalty and returns the number of rewards to pay the user.
        /// </summary>
        public uint AddRoyalty(uint itemsSpent, uint rewardsValue)
        {
            if (itemsSpent == 0)
                throw new ArgumentOutOfRangeException("itemsSpent", "itemsSpent must be > 0.");
            if (rewardsValue == 0)
                throw new ArgumentOutOfRangeException("rewardsValue", "rewardsValue must be > 0.");

            ItemsSpentInOwnedWorlds += itemsSpent;
            UnconvertedBalance += rewardsValue * ROYALTY_PERCENTAGE;

            uint payout = (uint)Math.Floor(UnconvertedBalance);
            if (payout > 0)
            {
                RewardsReceived += payout;
                UnconvertedBalance -= payout; 
            }

            return payout;
        }
   }
}
