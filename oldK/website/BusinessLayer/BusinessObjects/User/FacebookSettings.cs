///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class FacebookSettings
    {
        private int _UserId = 0;
        private UInt64 _FacebookUserId = 0;
        private string _AccessToken = "";
        private bool _UserFBProfilePicture = false;

        public FacebookSettings () { }

        public FacebookSettings (int userId, UInt64 fbUserId, string accessToken, bool useFBProfilePic)
        {
            this._UserId = userId;
            this._FacebookUserId = fbUserId;
            this._AccessToken = accessToken;
            this._UserFBProfilePicture = useFBProfilePic;
        }

        public virtual int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }
        public virtual UInt64 FacebookUserId
        {
            get { return _FacebookUserId; }
            set { _FacebookUserId = value; }
        }
        public virtual string AccessToken
        {
            get { return _AccessToken; }
            set { _AccessToken = value; }
        }
        public virtual bool UseFacebookProfilePicture
        {
            get { return _UserFBProfilePicture; }
            set { _UserFBProfilePicture = value; }
        }
    }
}
