///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class UserMetaGameItemBalances : IEnumerable<KeyValuePair<string, int>>
    {
        private int _UserId;
        private Dictionary<string, int> _MetaGameItemBalances;
 
        /// <summary>
        /// Overloaded constructor for the UserBalances class.
        /// </summary>
        public UserMetaGameItemBalances(int userId)
        {
            this._UserId = userId;
            this._MetaGameItemBalances = new Dictionary<string,int>();
        }

        /// <summary>
        /// UserId of the user owning these balances.
        /// </summary>
        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        /// <summary>
        /// Returns the user's current balance for the specified game item.
        /// </summary>
        public int GetMetaGameItemBalance(string itemName)
        {
            int balance;
            
            if (!_MetaGameItemBalances.TryGetValue(itemName, out balance))
            {
                balance = 0;
            }

            return balance;
        }

        /// <summary>
        /// Sets the user's balance for the specified game item to the specified amount.
        /// </summary>
        public void SetMetaGameItemBalance(string itemName, int amt)
        {
            if (string.IsNullOrWhiteSpace(itemName))
            {
                throw new ArgumentException("itemName must not be null or empty.", "itemName");
            }

            _MetaGameItemBalances[itemName] = amt;
        }

        /// <summary>
        /// Increases the user's balance for the specified game item by the specified amount.
        /// </summary>
        public void CreditMetaGameItemBalance(string itemName, int amtToAdd)
        {
            if (string.IsNullOrWhiteSpace(itemName))
            {
                throw new ArgumentException("itemName must not be null or empty.", "itemName");
            }

            if (amtToAdd <= 0)
            {
                throw new ArgumentOutOfRangeException("amtToAdd", "amtToAdd must be a positive integer.");
            }

            int balance = GetMetaGameItemBalance(itemName);
            SetMetaGameItemBalance(itemName, balance + amtToAdd);
        }

        /// <summary>
        /// Decreases the user's balance for the specified game item by the specified amount.
        /// Returns true if the debit succeeded, false otherwise.
        /// </summary>
        public bool TryDebitMetaGameItemBalance(string itemName, int amtToSubtract, out string errorMsg)
        {
            int balance = GetMetaGameItemBalance(itemName);

            if (string.IsNullOrWhiteSpace(itemName))
            {
                errorMsg = "itemName must not be null or empty.";
                return false;
            }

            if (amtToSubtract <= 0)
            {
                errorMsg = "amtToSubtract must be a positive integer.";
                return false;
            }

            if (balance < amtToSubtract)
            {
                errorMsg = "amtToSubtract exceeds the balance for the specified MetaGameItem. Current balance = " + balance;
                return false;
            }

            SetMetaGameItemBalance(itemName, balance - amtToSubtract);
            
            errorMsg = string.Empty;
            return true;
        }

        /// <summary>
        /// System.Collections.Generic.IEnumerator implementation.
        /// </summary>
        public IEnumerator<KeyValuePair<string, int>> GetEnumerator()
        {
            return _MetaGameItemBalances.GetEnumerator();
        }

        /// <summary>
        /// System.Collections.IEnumerator implementation.
        /// </summary>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
   }
}
