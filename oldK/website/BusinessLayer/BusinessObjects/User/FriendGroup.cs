///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class FriendGroup
    {
        private int _FriendGroupId = 0;
        private int _OwnerId = 0;
        private string _Name = "";
        private DateTime _CreatedDatetime;
        private UInt32 _FriendCount = 0;

        /// <summary>
        /// Default constructor for Friend class.
        /// </summary>
        public FriendGroup() { }

        public FriendGroup (int friendGroupId, int ownerId, string name, DateTime createdDatetime, 
            UInt32 friendCount)
        {
            this._FriendGroupId = friendGroupId;
            this._OwnerId = ownerId;
            this._Name = name;
            this._CreatedDatetime = createdDatetime;
            this._FriendCount = friendCount;
        }

        public int FriendGroupId
        {
           get {return _FriendGroupId;}
           set {_FriendGroupId = value;}
        }

        public int OwnerId
        {
           get {return _OwnerId;}
           set {_OwnerId = value;}
        }

        public string Name
        {
           get {return _Name;}
           set {_Name = value;}
        }

        public DateTime CreatedDatetime
        {
           get {return _CreatedDatetime;}
           set {_CreatedDatetime = value;}
        }

        public UInt32 FriendCount
        {
           get {return _FriendCount;}
           set {_FriendCount = value;}
        }

    }
}
