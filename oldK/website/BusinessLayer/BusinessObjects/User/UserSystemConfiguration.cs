///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class UserSystemConfiguration
    {
        /// <summary>
        /// Default constructor for UserSystemConfiguration class.
        /// </summary>
        public UserSystemConfiguration() { }

        /// <summary>
        /// Overloaded constructor for the UserSystemConfiguration class.
        /// </summary>
        public UserSystemConfiguration(string systemConfigurationId, string systemId, uint? cpus, uint? cpuMhz, ulong? sysMem, ulong? diskSize, uint? gpuX, uint? gpuY, 
            uint? gpuHz, uint? gpuFmt, string dxVer, string cpuVer, string sysVer, string sysName, string net, string netMAC, string netType, string netHash, string gpu, 
            DateTime dateFirstUsed, DateTime dateLastUsed, uint? cpuPhysCore, string cpuVendor, string cpuBrand, uint? cpuFamily, uint? cpuModel, uint? cpuStepping,
            uint? cpuSSE3, uint? cpuSSSE3, uint? cpuSSE4_1, uint? cpuSSE4_2, uint? cpuSSE4_A, uint? cpuSSE5, uint? cpuAVX, uint? cpuAVX2, uint? cpuFMA3, uint? cpuFMA4,
            string cpuCodeName)
        {
            SystemConfigurationId = systemConfigurationId;
            SystemId = systemId;
            CpuCount = cpus;
            CpuMhz = cpuMhz;
            SystemMemory = sysMem;
            DiskSize = diskSize;
            GpuDisplayModeWidth = gpuX;
            GpuDisplayModeHeight = gpuY;
            GpuRefreshRate = gpuHz;
            GpuDisplayModeFormat = gpuFmt;
            DirectXVersion = dxVer;
            CpuVersion = cpuVer;
            SystemVersion = sysVer;
            SystemName = sysName;
            NetworkInterfaceName = net;
            NetworkInterfaceMAC = netMAC;
            NetworkInterfaceType = netType;
            NetworkInterfaceHash = netHash;
            GpuName = gpu;
            DateFirstUsed = dateFirstUsed;
            DateLastUsed = dateLastUsed;
            CpuPhysCore = cpuPhysCore;
            CpuVendor = cpuVendor;
            CpuBrand = cpuBrand;
            CpuFamily = cpuFamily;
            CpuModel = cpuModel;
            CpuStepping = cpuStepping;
            CpuSSE3 = cpuSSE3;
            CpuSSSE3 = cpuSSSE3;
            CpuSSE4_1 = cpuSSE4_1;
            CpuSSE4_2 = cpuSSE4_2;
            CpuSSE4_A = cpuSSE4_A;
            CpuSSE5 = cpuSSE5;
            CpuAVX = cpuAVX;
            CpuAVX2 = cpuAVX2;
            CpuFMA3 = cpuFMA3;
            CpuFMA4 = cpuFMA4;
            CpuCodeName = cpuCodeName;
        }

        public string SystemConfigurationId { get; set; }
        public string SystemId { get; set; }
        public uint? CpuCount { get; set; }
        public uint? CpuMhz { get; set; }
        public ulong? SystemMemory { get; set; }
        public ulong? DiskSize { get; set; }
        public uint? GpuDisplayModeWidth { get; set; }
        public uint? GpuDisplayModeHeight { get; set; }
        public uint? GpuRefreshRate { get; set; }
        public uint? GpuDisplayModeFormat { get; set; }
        public uint? CpuPhysCore { get; set; }
        public uint? CpuFamily { get; set; }
        public uint? CpuModel { get; set; }
        public uint? CpuStepping { get; set; }
        public uint? CpuSSE3 { get; set; }
        public uint? CpuSSSE3 { get; set; }
        public uint? CpuSSE4_1 { get; set; }
        public uint? CpuSSE4_2 { get; set; }
        public uint? CpuSSE4_A { get; set; }
        public uint? CpuSSE5 { get; set; }
        public uint? CpuAVX { get; set; }
        public uint? CpuAVX2 { get; set; }
        public uint? CpuFMA3 { get; set; }
        public uint? CpuFMA4 { get; set; }
        public string DirectXVersion { get; set; }
        public string CpuVersion { get; set; }
        public string SystemVersion { get; set; }
        public string SystemName { get; set; }
        public string NetworkInterfaceName { get; set; }
        public string NetworkInterfaceMAC { get; set; }
        public string NetworkInterfaceType { get; set; }
        public string NetworkInterfaceHash { get; set; }
        public string CpuVendor { get; set; }
        public string CpuBrand { get; set; }
        public string GpuName { get; set; }
        public string CpuCodeName { get; set; }
        public DateTime DateFirstUsed { get; set; }
        public DateTime DateLastUsed { get; set; }
        public bool IsRunning { get; set; }
    }
}

