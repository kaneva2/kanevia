///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class UserBalances
    {
        private double _Rewards = 0.0;
        private double _Credits = 0.0;
        private double _MPoints = 0.0;
        private double _Dollars = 0.0;
 
        /// <summary>
        /// Default constructor for UserBalances class.
        /// </summary>
        public UserBalances () { }

        /// <summary>
        /// Overloaded constructor for the UserBalances class.
        /// </summary>
        public UserBalances (double credits, double rewards, double dollars, double mpoints)
        {
            this._Credits = credits;
            this._Rewards = rewards;
            this._Dollars = dollars;
            this._MPoints = mpoints;
        }

        public virtual double GPoint
        {
            get { return _Rewards; }
            set { _Rewards = value; }
        }
        public virtual double KPoint
        {
            get { return _Credits; }
            set { _Credits = value; }
        }
        public virtual double MPoint
        {
            get { return _MPoints; }
            set { _MPoints = value; }
        }
        public virtual double Dollar
        {
            get { return _Dollars; }
            set { _Dollars = value; }
        }
        public virtual double Rewards
        {
            get { return _Rewards; }
        }
        public virtual double Credits
        {
            get { return _Credits + _MPoints; }
        }
   }
}
