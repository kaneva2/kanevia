///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class UserSystem
    {
        private string _systemId = null;
        private DateTime _dateFirstUsed = DateTime.Now;
        private DateTime _dateLastUsed = DateTime.Now;

        /// <summary>
        /// Default constructor for UserSystem class.
        /// </summary>
        public UserSystem() { }

        /// <summary>
        /// Overloaded constructor for the UserSystem class.
        /// </summary>
        public UserSystem(string systemId, DateTime dateFirstUsed, DateTime dateLastUsed)
        {
            this._systemId = systemId;
            this._dateFirstUsed = dateFirstUsed;
            this._dateLastUsed = dateLastUsed;
        }

        public string SystemId
        {
            get { return _systemId; }
            set { _systemId = value; }
        }

        public DateTime DateFirstUsed
        {
            get { return _dateFirstUsed; }
            set { _dateFirstUsed = value; }
        }

        public DateTime DateLastUsed
        {
            get { return _dateLastUsed; }
            set { _dateLastUsed = value; }
        }
    }
}

