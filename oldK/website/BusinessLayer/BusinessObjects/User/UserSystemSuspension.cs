///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class UserSystemSuspension
    {
        private int _SystemBanId;
        private string _SystemId;
        private DateTime _BanStartDate;
        private DateTime? _BanEndDate;

        public UserSystemSuspension(int systemBanId, string systemId, DateTime banStartDate, DateTime? banEndDate)
        {
            this._SystemBanId = systemBanId;
            this._SystemId = systemId;
            this._BanStartDate = banStartDate;
            this._BanEndDate = banEndDate;
        }

        public int SystemBanId
        {
            get { return _SystemBanId; }
            set { _SystemBanId = value; }
        }

        public string SystemId
        {
            get { return _SystemId; }
            set { _SystemId = value; }
        }

        public DateTime BanStartDate
        {
            get { return _BanStartDate; }
            set { _BanStartDate = value; }
        }

        public DateTime? BanEndDate
        {
            get { return _BanEndDate; }
            set { _BanEndDate = value; }
        }

        public int BanLengthDays
        {
            get 
            {
                if (IsPermanent)
                    return Int32.MaxValue;

                return (int)((DateTime)BanEndDate).Subtract(BanStartDate).TotalDays;
            }
        }

        public bool IsPermanent
        {
            get { return _BanEndDate == null; }
        }
    }
}