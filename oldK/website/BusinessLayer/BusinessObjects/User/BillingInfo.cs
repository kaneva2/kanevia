///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class BillingInfo
    {
        private string _FullName;
        private string _Address1;
        private string _Address2;
        private string _City;
        private string _State;
        private string _PostalCode;
        private string _Country;
        private string _PhoneNumber;
        private string _CardType;                       // VISA, MASTER CARD, AMERICAN EXPRESS, etc.
        private string _CybersourceCardTypeNumber;      // VISA = "001", MC = "002", AMEX = "003", etc.
        private string _CardNumber;                     // when returned from cybersource, only partial number 41111xxxxx1111
        private string _CardNumberEncrypt;
        private string _CardNumberLast4;
        private string _SecurityCodeEncrypt;
        private string _ExpMonth;
        private string _ExpYear;
        private string _SubscriptionId;

        public BillingInfo ()
        {
            this._FullName = "";
            this._Address1 = "";
            this._Address2 = "";
            this._City = "";
            this._State = "";
            this._PostalCode = "";
            this._Country = "";
            this._PhoneNumber = "";
            this._CardType = "";
            this._CybersourceCardTypeNumber = "";
            this._CardNumber = "";
            this._CardNumberEncrypt = "";
            this._CardNumberLast4 = "";
            this._SecurityCodeEncrypt = "";
            this._ExpMonth = "";
            this._ExpYear = "";
            this._SubscriptionId = "";
        }

        public string FullName
        {
            get { return _FullName; }
            set { _FullName = value; }
        }

        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }

        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }

        public string City
        {
            get { return _City; }
            set { _City = value; }
        }

        public string State
        {
            get { return _State; }
            set { _State = value; }
        }

        public string PostalCode
        {
            get { return _PostalCode; }
            set { _PostalCode = value; }
        }

        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        public string PhoneNumber
        {
            get { return _PhoneNumber; }
            set { _PhoneNumber = value; }
        }

        public string CardType
        {
            get { return _CardType; }
            set { _CardType = value; }
        }

        public string CybersourceCardTypeNumber
        {
            get { return _CybersourceCardTypeNumber; }
            set { _CybersourceCardTypeNumber = value; }
        }

        public string CardNumber
        {
            get { return _CardNumber; }
            set { _CardNumber = value; }
        }

        public string CardNumberEncrypt
        {
            get { return _CardNumberEncrypt; }
            set { _CardNumberEncrypt = value; }
        }

        public string CardNumberLast4
        {
            get 
            {
                if (_CardNumberLast4 == "")
                {
                    if (_CardNumber.Length > 3)
                    {
                        _CardNumberLast4 = _CardNumber.Substring (_CardNumber.Length - 4, 4);
                    }
                }
                return _CardNumberLast4;
            }
            set { _CardNumberLast4 = value; }
        }

        public string CardSecurityCodeEncrypt
        {
            get { return _SecurityCodeEncrypt; }
            set { _SecurityCodeEncrypt = value; }
        }

        public string CardExpirationMonth
        {
            get { return _ExpMonth; }
            set { _ExpMonth = value; }
        }

        public string CardExpirationYear
        {
            get { return _ExpYear; }
            set { _ExpYear = value; }
        }

        public string SubscriptionId
        {
            get { return _SubscriptionId; }
            set { _SubscriptionId = value; }
        }
    }
}
