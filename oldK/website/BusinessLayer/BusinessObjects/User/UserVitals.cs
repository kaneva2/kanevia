///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class UserVitals
    {
        //<user>
        //   <u>4507238</u>
        //   <r>2</r>
        //   <g>F</g>
        //   <b>1969-12-29</b>
        //   <m>0</m>
        //   <c>US</c>
        //   <p>1</p>
        //</user>
        //
        // Online status
        private int _UserId = 0;
        private string _Gender = "M";
        private string _BirthDate;
        private bool _ShowMature = false;
        private string _Country;
        private int _Role = 0;
        private int _priv = 0;


        /// <summary>
        /// Default constructor for User class.
        /// </summary>
        public UserVitals() { }

        public UserVitals(  int         userId
                            , string    gender
                            , string    birthdate
                            , bool      showMature
                            , string    country
                            , int       role
                            , int       privs ) 
        {
            this._UserId = userId;
            this._Role = role;
            this._Gender = gender;
            this._BirthDate = birthdate;
            this._Country = country;
            this._ShowMature = showMature;
       }

        ///// <summary>
        ///// Overloaded constructor for the User class.
        ///// </summary>
        //public User(int userId, string username, string _gender, DateTime _GluedDate,
        //    string _NameNoSpaces, string _URL,
        //    string _ThumbnailPath, string _ThumbnailSmallPath, string _ThumbnailMediumPath, string _ThumbnailLargePath, string _ThumbnailXlargePath,
        //    int _NumberOfDiggs, int _NumberOfViews, int _NumberTimesShared
        //    )
        //{
        //    this._userId = userId;
        //    this._username = username;
        //}

        /// <summary>
        /// Gets or sets unique user identifier.
        /// The Identity Field Design Pattern. 
        /// </summary>
        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public int Role
        {
            get { return _Role; }
            set { _Role = value; }
        }

        public string Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }
        public string BirthDate
        {
            get { return _BirthDate; }
            set { _BirthDate = value; }
        }

        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        public bool ShowMature
        {
            get { return _ShowMature; }
            set { _ShowMature = value; }
        }
    }
}
