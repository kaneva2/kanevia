///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
	[Serializable]
	public class BlastBlock
	{
		int _UserId = 0;
		int _BlockedUserId = 0;
		string _BlockedUserName = "";
		int _BlockedCommunityId = 0;
		string _BlockedCommunityName = "";

		public BlastBlock(int userId, int blockedUserId, string blockedUserName, int blockedCommunityId, string blockedCommunityName)
		{
			this._UserId = userId;
			this._BlockedUserId = blockedUserId;
			this._BlockedUserName = blockedUserName;
			this._BlockedCommunityId = blockedCommunityId;
			this._BlockedCommunityName = blockedCommunityName;
		}

		/// <summary>
		/// Default constructor for BlastBlock class.
		/// </summary>
		public BlastBlock() { }

		public int UserId
		{
			get { return _UserId; }
			set { _UserId = value; }
		}

		public int BlockedUserId
		{
			get { return _BlockedUserId; }
			set { _BlockedUserId = value; }
		}

		public string BlockedUserName
		{
			get { return _BlockedUserName; }
			set { _BlockedUserName = value; }
		}

		public int BlockedCommunityId
		{
			get { return _BlockedCommunityId; }
			set { _BlockedCommunityId = value; }
		}

		public string BlockedCommunityName
		{
			get { return _BlockedCommunityName; }
			set { _BlockedCommunityName = value; }
		}




		
	}
}
