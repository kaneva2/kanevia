///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class User
    {
        public const string ROLE_SYSTEM = "System";
        public const string ROLE_MEMBER = "Member";
        public const string ROLE_CERTIFIED_BETATESTER = "Certified Betatester";
        public const string ROLE_ADMIN = "Administrator User";
        public const string ROLE_CSR = "CSR";
        public const string ROLE_IT = "IT";
        public const string ROLE_FUTURE_USE = "Not Used Yet";

        // Online status
        public const string ONLINE_USTATE_ON = "On";
        public const string ONLINE_USTATE_OFF = "Off";
        public const string ONLINE_USTATE_INWORLD = "InWorld";
        public const string ONLINE_USTATE_ONINWORLD = "OnInWorld";

        private int _UserId = 0;
        private int _CompanyId = 0;
        private string _Username = "";
        private string _DisplayName = "";
        private int _Role = 0;
        private UsersPrivileges _UserPrivileges = new UsersPrivileges();
        private string _RegistrationKey;
        private int _AccountType;
        private string _FirstName = "";
        private string _LastName = "";
        private string _Description;
        private string _Gender = "M";
        private string _Homepage;
        private string _Email = "";
        private int _StatusId = 0;
        private int _EmailStatus = 0;
        private string _KeyValue = "";
        private DateTime _LastLogin;
        private DateTime _SignupDate;
        private string _BirthDate;
        private string _Newsletter;
        private bool _PublicProfile;
        private string _ZipCode;
        private string _Country;
        private string _IpAddress;
        private string _LastIpAddress;
        private bool _ShowMature = false;
        private bool _BrowseAnonymously;
        private bool _ShowOnline;
        private string _NotifyBlogComments;
        private string _NotifyProfileComments;
        private string _NotifyFriendMessages;
        private string _NotifyAnyoneMessages;
        private string _NotifyFriendRequests;
        private bool _NotifyNewFriends = false;
        private int _FriendsCanComment;
        private int _EveryoneCanComment;
        private bool _MatureProfile = false;        
        private int _Online;
        private DateTime _SecondToLastLogin;
        private int _Age = 0;
        private string _Location;
        private bool _OwnMod;
        private bool _Active = false;
        private UInt32 _BlastShowPermissions;
        private UInt32 _BlastPrivacyPermissions;
        private string _Ustate;
        private bool _Over21 = false;
        private int _WokPlayerId = 0;
        private int _JoinSourceCommunityId;
        private string _ReceiveUpdates;
        private UsersSubscriptions _usersSubscriptions = new UsersSubscriptions();

        // Community Props
        private int _CommunityId = 0;
        private int _TemplateId;
        private int _HomeCommunityId = 0;
        private string _NameNoSpaces;
        private string _Url;
        private string _ThumbnailPath = "";
        private string _ThumbnailSmallPath = "";
        private string _ThumbnailMediumPath = "";
        private string _ThumbnailLargePath = "";
        private string _ThumbnailXlargePath = "";
        private string _ThumbnailSquarePath = "";

        // Channel Stats
        private int _NumberOfDiggs;
        private int _NumberOfViews;
        private int _NumberTimesShared;

        private UserStats _stats;
        private Preferences _preferences;
		private NotificationPreferences _notificationPreferences;
        private UserBalances _balances = new UserBalances();
        private FacebookSettings _facebookSettings = new FacebookSettings ();

        // Blast settings
        private string _blastPrivacyPermissionsBase2 = "";
        private string _blastShowPermissionsBase2 = "";

        private bool _chkBlogcomments = true;
        private bool _chkProfiles = true;
        private bool _chkComment = true;
        private bool _chkForum = true;
        private bool _chkAddFriend = true;
        private bool _chkJoinCommunity = true;
        private bool _chkRave3DHome = true;
        private bool _chkRave3DHangout = true;
        private bool _chkSendGift = true;

        private bool _chkShowBlogcomments = true;
        private bool _chkShowProfiles = true;
        private bool _chkShowComment = true;
        private bool _chkShowForum = true;
        private bool _chkShowAddFriend = true;
        private bool _chkShowJoinCommunity = true;
        private bool _chkShowRave3DHome = true;
        private bool _chkShowRave3DHangout = true;
        private bool _chkShowSendGift = true;
        private bool _chkShowRave3DApp = true;
        private bool _chkShowCameraScreenshot = true;

        private string _salt = "";
        private string _password = "";

        /// <summary>
        /// Default constructor for User class.
        /// </summary>
        public User() { }

        public User(int userId, string username, string displayName, int role, string registrationKey,
           int accountType, string firstName, string lastName, string description,
           string gender, string homepage, string email, int statusId, int emailStatus,
           string keyValue, DateTime lastLogin, DateTime signupDate, string birthDate,
           string newsletter, bool publicProfile, string zipCode, string country, string ipAddress, string lastIpAddress,
           bool showMature, bool browseAnonymously,
           bool showOnline, string notifyBlogComments, string notifyProfileComments, string notifyFriendMessages,
           string notifyAnyoneMessages, string notifyFriendRequests, bool notifyNewFriends, int friendsCanComment, int everyoneCanComment,
           bool matureProfile, int online, DateTime secondToLastLogin, int age, string location, bool ownMod,
           bool active, UInt32 blastShowPermissions, UInt32 blastPrivacyPermissions,
           string ustate, bool over21, int wokPlayerId, int joinSourceCommunityId, string receiveUpdates,
           int communityId, int templateId,
           string nameNoSpaces, string url,
           string thumbnailPath, string thumbnailSmallPath, string thumbnailMediumPath, string thumbnailLargePath, string thumbnailXlargePath,
           string thumbnailSquarePath, int numberOfDiggs, int numberOfViews, int numberTimesShared, int homeCommunityId)
            : this(userId, username, displayName, role, registrationKey,
                   accountType, firstName, lastName, description,
                   gender, homepage, email, statusId, emailStatus,
                   keyValue, lastLogin, signupDate, birthDate,
                   newsletter, publicProfile, zipCode, country, ipAddress, lastIpAddress,
                   showMature, browseAnonymously,
                   showOnline, notifyBlogComments, notifyProfileComments, notifyFriendMessages,
                   notifyAnyoneMessages, notifyFriendRequests, notifyNewFriends, friendsCanComment, everyoneCanComment,
                   matureProfile, online, secondToLastLogin, age, location, ownMod,
                   active, blastShowPermissions, blastPrivacyPermissions,
                   ustate, over21, wokPlayerId, joinSourceCommunityId, receiveUpdates,
                   communityId, templateId,
                   nameNoSpaces, url,
                   thumbnailPath, thumbnailSmallPath, thumbnailMediumPath, thumbnailLargePath, thumbnailXlargePath,
                   thumbnailSquarePath, numberOfDiggs, numberOfViews, numberTimesShared, "", "", homeCommunityId)
        { }

        /// <summary>
        /// Overloaded constructor for the User class.
        /// </summary>
        public User (int userId, string username, string displayName, int role, string registrationKey, 
           int accountType, string firstName, string lastName, string description, 
           string gender, string homepage, string email, int statusId, int emailStatus,
           string keyValue, DateTime lastLogin, DateTime signupDate, string birthDate, 
           string newsletter, bool publicProfile, string zipCode, string country,  string ipAddress, string lastIpAddress,
           bool showMature, bool browseAnonymously, 
           bool showOnline, string notifyBlogComments, string notifyProfileComments, string notifyFriendMessages, 
           string notifyAnyoneMessages, string notifyFriendRequests, bool notifyNewFriends, int friendsCanComment, int everyoneCanComment, 
           bool matureProfile, int online, DateTime secondToLastLogin, int age, string location, bool ownMod, 
           bool active, UInt32 blastShowPermissions, UInt32 blastPrivacyPermissions,
           string ustate, bool over21, int wokPlayerId, int joinSourceCommunityId, string receiveUpdates,
           int communityId, int templateId,
           string nameNoSpaces, string url,
           string thumbnailPath, string thumbnailSmallPath, string thumbnailMediumPath, string thumbnailLargePath, string thumbnailXlargePath,
           string thumbnailSquarePath, int numberOfDiggs, int numberOfViews, int numberTimesShared, string salt, string password, int homeCommunityId
        )
        {
            this._UserId = userId;
            this._Username = username;
            this._DisplayName = displayName;
            this._Role = role;
            this._RegistrationKey = registrationKey;
            this._AccountType = accountType;
            this._FirstName = firstName;
            this._LastName = lastName;
            this._Description = description;
            this._Gender = gender;
            this._Homepage = homepage;
            this._Email = email;
            this._StatusId = statusId;
            this._EmailStatus = emailStatus;
            this._KeyValue = keyValue;
            this._LastLogin = lastLogin;
            this._SignupDate = signupDate;
            this._BirthDate = birthDate;
            this._Newsletter = newsletter;
            this._PublicProfile = publicProfile;
            this._ZipCode = zipCode;
            this._Country = country;
            this._IpAddress = ipAddress;
            this._LastIpAddress = lastIpAddress;
            this._ShowMature = showMature;
            this._BrowseAnonymously = browseAnonymously;
            this._ShowOnline = showOnline;
            this._NotifyBlogComments = notifyBlogComments;
            this._NotifyProfileComments = notifyProfileComments;
            this._NotifyFriendMessages = notifyFriendMessages;
            this._NotifyAnyoneMessages = notifyAnyoneMessages;
            this._NotifyFriendRequests = notifyFriendRequests;
            this._NotifyNewFriends = notifyNewFriends;
            this._FriendsCanComment = friendsCanComment;
            this._EveryoneCanComment = everyoneCanComment;
            this._MatureProfile = matureProfile;            
            this._Online = online;
            this._SecondToLastLogin = secondToLastLogin;
            this._Age = age;
            this._Location = location;
            this._OwnMod = ownMod;
            this._Active = active;
            this._BlastShowPermissions = blastShowPermissions;
            this._BlastPrivacyPermissions = blastPrivacyPermissions;
            this._Ustate = ustate;
            this._Over21 = over21;
            this._WokPlayerId = wokPlayerId;
            this._JoinSourceCommunityId = joinSourceCommunityId;
            this._ReceiveUpdates = receiveUpdates;
            this._HomeCommunityId = homeCommunityId;
			


            // convert blast privacy settings to base 2
            _blastPrivacyPermissionsBase2 = Convert.ToString(_BlastPrivacyPermissions, 2).PadLeft(9, '0');

            //populate the individual privacy settings
            _chkBlogcomments = _blastPrivacyPermissionsBase2[(int)eBLAST_PREFERENCES.BLOG_COMMENT].Equals('0') ? false : true;
            _chkProfiles = _blastPrivacyPermissionsBase2[(int)eBLAST_PREFERENCES.PROFILES].Equals('0') ? false : true;
            _chkComment = _blastPrivacyPermissionsBase2[(int)eBLAST_PREFERENCES.COMMENTS].Equals('0') ? false : true;
            _chkForum = _blastPrivacyPermissionsBase2[(int)eBLAST_PREFERENCES.FORUMS].Equals('0') ? false : true;
            _chkAddFriend = _blastPrivacyPermissionsBase2[(int)eBLAST_PREFERENCES.ADD_FRIEND].Equals('0') ? false : true;
            _chkJoinCommunity = _blastPrivacyPermissionsBase2[(int)eBLAST_PREFERENCES.JOIN_COMMUNITY].Equals('0') ? false : true;
            _chkRave3DHome = _blastPrivacyPermissionsBase2[(int)eBLAST_PREFERENCES.RAVE_HOME].Equals('0') ? false : true;
            _chkRave3DHangout = _blastPrivacyPermissionsBase2[(int)eBLAST_PREFERENCES.RAVE_HANGOUT].Equals('0') ? false : true;
            _chkSendGift = _blastPrivacyPermissionsBase2[(int)eBLAST_PREFERENCES.SEND_GIFT].Equals('0') ? false : true;
            //_chkRave3DApp = _blastPrivacyPermissionsBase2[(int)eBLAST_PREFERENCES.RAVE_3DAPP].Equals('0') ? false : true;

            // convert blast show settings to base 2
            _blastShowPermissionsBase2 = Convert.ToString(_BlastShowPermissions, 2).PadLeft(9, '0');

            //populate the individual show settings
            _chkShowBlogcomments = _blastShowPermissionsBase2[(int)eBLAST_PREFERENCES.BLOG_COMMENT].Equals('0') ? false : true;
            _chkShowProfiles = _blastShowPermissionsBase2[(int)eBLAST_PREFERENCES.PROFILES].Equals('0') ? false : true;
            _chkShowComment = _blastShowPermissionsBase2[(int)eBLAST_PREFERENCES.COMMENTS].Equals('0') ? false : true;
            _chkShowForum = _blastShowPermissionsBase2[(int)eBLAST_PREFERENCES.FORUMS].Equals('0') ? false : true;
            _chkShowAddFriend = _blastShowPermissionsBase2[(int)eBLAST_PREFERENCES.ADD_FRIEND].Equals('0') ? false : true;
            _chkShowJoinCommunity = _blastShowPermissionsBase2[(int)eBLAST_PREFERENCES.JOIN_COMMUNITY].Equals('0') ? false : true;
            _chkShowRave3DHome = _blastShowPermissionsBase2[(int)eBLAST_PREFERENCES.RAVE_HOME].Equals('0') ? false : true;
            _chkShowRave3DHangout = _blastShowPermissionsBase2[(int)eBLAST_PREFERENCES.RAVE_HANGOUT].Equals('0') ? false : true;
            _chkShowSendGift = _blastShowPermissionsBase2[(int)eBLAST_PREFERENCES.SEND_GIFT].Equals('0') ? false : true;
            //_chkShowRave3DApp = _blastShowPermissionsBase2[(int)eBLAST_PREFERENCES.RAVE_3DAPP].Equals('0') ? false : true;
            //_chkShowCameraScreenshot = _blastShowPermissionsBase2[(int)eBLAST_PREFERENCES.CAMERA_SCREENSHOT].Equals('0') ? false : true;

            // Community Props
            this._CommunityId = communityId;
            this._TemplateId = templateId;
            this._NameNoSpaces = nameNoSpaces;
            this._Url = url;
            this._ThumbnailPath = thumbnailPath;
            this._ThumbnailSmallPath = thumbnailSmallPath;
            this._ThumbnailMediumPath = thumbnailMediumPath;
            this._ThumbnailLargePath = thumbnailLargePath;
            this._ThumbnailXlargePath = thumbnailXlargePath;
            this._ThumbnailSquarePath = thumbnailSquarePath;

            // Channel Stats
            this._NumberOfViews = numberOfViews;
            this._NumberOfDiggs = numberOfDiggs;
            this._NumberTimesShared = numberTimesShared;

            // pw info
            this._salt = salt;
            this._password = password;
       }

        ///// <summary>
        ///// Overloaded constructor for the User class.
        ///// </summary>
        //public User(int userId, string username, string _gender, DateTime _GluedDate,
        //    string _NameNoSpaces, string _URL,
        //    string _ThumbnailPath, string _ThumbnailSmallPath, string _ThumbnailMediumPath, string _ThumbnailLargePath, string _ThumbnailXlargePath,
        //    int _NumberOfDiggs, int _NumberOfViews, int _NumberTimesShared
        //    )
        //{
        //    this._userId = userId;
        //    this._username = username;
        //}

        /// <summary>
        /// Gets or sets unique user identifier.
        /// The Identity Field Design Pattern. 
        /// </summary>
        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public UsersSubscriptions UsersSubscriptions
        {
            get { return _usersSubscriptions; }
            set { _usersSubscriptions = value; }
        }

        public UsersPrivileges UserPrivileges
        {
            get { return _UserPrivileges; }
            set { _UserPrivileges = value; }
        }

        public int SM_SiteRoleID
        {
            get { return _UserPrivileges.SiteRoleId; }
            set { _UserPrivileges.SiteRoleId = value; }
        }

        public int SM_CompanyID
        {
            get { return _CompanyId; }
            set { _CompanyId = value; }
        }

        public NameValueCollection SitePrivileges
        {
            get { return _UserPrivileges.UserPrivileges; }
            set { _UserPrivileges.UserPrivileges = value; }
        }

        public bool HasAccessPass
        {
            get { return _usersSubscriptions.HasAccessPass; }
            set { _usersSubscriptions.HasAccessPass = value; }
        }

        public bool HasVIPPass
        {
            get { return _usersSubscriptions.HasVIPPass; }
            set { _usersSubscriptions.HasVIPPass = value; }
        }

        public List<UserSubscription> UserOwnedSubscriptions
        {
            get { return _usersSubscriptions.UserOwnedSubscriptions; }
            set { _usersSubscriptions.UserOwnedSubscriptions = value; }
        }

        public string Username
        {
            get { return _Username; }
            set { _Username = value; }
        }

        public string DisplayName
        {
            get { return _DisplayName; }
            set { _DisplayName = value; }
        }

        public int Role
        {
            get { return _Role; }
            set { _Role = value; }
        }

        public string RegistrationKey
        {
            get { return _RegistrationKey; }
            set { _RegistrationKey = value; }
        }

        public int AccountType
        {
            get { return _AccountType; }
            set { _AccountType = value; }
        }

        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }

        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public string Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }

        public string Homepage
        {
            get { return _Homepage; }
            set { _Homepage = value; }
        }

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        public int StatusId
        {
            get { return _StatusId; }
            set { _StatusId = value; }
        }

        public int EmailStatus
        {
            get { return _EmailStatus; }
            set { _EmailStatus = value; }
        }

        public string KeyValue
        {
            get { return _KeyValue; }
            set { _KeyValue = value; }
        }

        public DateTime LastLogin
        {
            get { return _LastLogin; }
            set { _LastLogin = value; }
        }

        public DateTime SignupDate
        {
            get { return _SignupDate; }
            set { _SignupDate = value; }
        }

        public string BirthDate
        {
            get { return _BirthDate; }
            set { _BirthDate = value; }
        }

        public string Newsletter
        {
            get { return _Newsletter; }
            set { _Newsletter = value; }
        }

        public bool PublicProfile
        {
            get { return _PublicProfile; }
            set { _PublicProfile = value; }
        }

        public string ZipCode
        {
            get { return _ZipCode; }
            set { _ZipCode = value; }
        }

        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        public string IpAddress
        {
          get { return _IpAddress; }
          set { _IpAddress = value; }
        }

        public string LastIpAddress
        {
          get { return _LastIpAddress; }
          set { _LastIpAddress = value; }
        }

        public bool ShowMature
        {
            get { return _ShowMature; }
            set { _ShowMature = value; }
        }

        public bool BrowseAnonymously
        {
            get { return _BrowseAnonymously; }
            set { _BrowseAnonymously = value; }
        }

        public bool ShowOnline
        {
            get { return _ShowOnline; }
            set { _ShowOnline = value; }
        }

        public string NotifyBlogComments
        {
            get { return _NotifyBlogComments; }
            set { _NotifyBlogComments = value; }
        }

        public string NotifyProfileComments
        {
            get { return _NotifyProfileComments; }
            set { _NotifyProfileComments = value; }
        }

        public string NotifyFriendMessages
        {
            get { return _NotifyFriendMessages; }
            set { _NotifyFriendMessages = value; }
        }

        public string NotifyAnyoneMessages
        {
            get { return _NotifyAnyoneMessages; }
            set { _NotifyAnyoneMessages = value; }
        }

        public string NotifyFriendRequests
        {
            get { return _NotifyFriendRequests; }
            set { _NotifyFriendRequests = value; }
        }

        public bool NotifyNewFriends
        {
            get { return _NotifyNewFriends; }
            set { _NotifyNewFriends = value; }
        }

        public int FriendsCanComment
        {
            get { return _FriendsCanComment; }
            set { _FriendsCanComment = value; }
        }

        public int EveryoneCanComment
        {
            get { return _EveryoneCanComment; }
            set { _EveryoneCanComment = value; }
        }

        public bool MatureProfile
        {
            get { return _MatureProfile; }
            set { _MatureProfile = value; }
        }

        public string ReceiveUpdates
        {
            get { return _ReceiveUpdates; }
            set { _ReceiveUpdates = value; }
        }

        public int Online
        {
            get { return _Online; }
            set { _Online = value; }
        }

        public DateTime SecondToLastLogin
        {
            get { return _SecondToLastLogin; }
            set { _SecondToLastLogin = value; }
        }

        public int Age
        {
            get { return _Age; }
            set { _Age = value; }
        }

        public string Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        public bool OwnMod
        {
            get { return _OwnMod; }
            set { _OwnMod = value; }
        }

        public bool Active
        {
            get { return _Active; }
            set { _Active = value; }
        }

        public UInt32 BlastShowPermissions
        {
            get { return _BlastShowPermissions; }
            set { _BlastShowPermissions = value; }
        }

        public UInt32 BlastPrivacyPermissions
        {
            get { return _BlastPrivacyPermissions; }
            set { _BlastPrivacyPermissions = value; }
        }

        public string Ustate
        {
            get { return _Ustate; }
            set { _Ustate = value; }
        }

        public bool IsAdult
        {
            get { return (_Age >= 18); }
        }

        public bool Over21
        {
            get { return _Over21; }
            set { _Over21 = value; }
        }

        public int WokPlayerId
        {
            get { return _WokPlayerId; }
            set { _WokPlayerId = value; }
        }

        public bool HasWOKAccount
        {
            get { return (_WokPlayerId > 0); }
        }

        public int JoinSourceCommunityId
        {
            get { return _JoinSourceCommunityId; }
            set { _JoinSourceCommunityId = value; }
        }

        public int CommunityId
        {
            get { return _CommunityId; }
            set { _CommunityId = value; }
        }

        public int TemplateId
        {
            get { return _TemplateId; }
            set { _TemplateId = value; }
        }

        public int HomeCommunityId
        {
            get { return _HomeCommunityId; }
            set { _HomeCommunityId = value; }
        }

        public string NameNoSpaces
        {
            get { return _NameNoSpaces;}
            set { _NameNoSpaces = value;}
        }
        
        public string URL
        {
            get { return _Url; }
            set { _Url = value; }
        }

        public string ThumbnailPath
        {
            get { return _ThumbnailPath;}
            set { _ThumbnailPath = value;}
        }

        public string ThumbnailSmallPath
        {
            get { return _ThumbnailSmallPath;}
            set { _ThumbnailSmallPath = value;}
        }

        public string ThumbnailMediumPath
        {
            get { return _ThumbnailMediumPath;}
            set { _ThumbnailMediumPath = value;}
        }

        public string ThumbnailLargePath
        {
            get { return _ThumbnailLargePath;}
            set { _ThumbnailLargePath = value;}
        }

        public string ThumbnailXlargePath
        {
            get { return _ThumbnailXlargePath;}
            set { _ThumbnailXlargePath = value;}
        }

        public string ThumbnailSquarePath
        {
            get { return _ThumbnailSquarePath; }
            set { _ThumbnailSquarePath = value; }
        }

        public int NumberOfDiggs
        {
            get { return _NumberOfDiggs;}
            set { _NumberOfDiggs = value;}
        }

        public int NumberOfViews
        {
            get { return _NumberOfViews;}
            set { _NumberOfViews = value;}
        }

         public int NumberTimesShared
        {
            get { return _NumberTimesShared;}
            set { _NumberTimesShared = value;}
        }

        /// <summary>
        /// Stats
        /// </summary>
        public UserStats Stats
        {
            get { return _stats; }
            set { _stats = value; }
        }

        /// <summary>
        /// Preference
        /// </summary>
        public Preferences Preference
        {
            get { return _preferences; }
            set { _preferences = value; }
        }

		/// <summary>
		/// Preference
		/// </summary>
		public NotificationPreferences NotificationPreference
		{
			get { return _notificationPreferences; }
			set { _notificationPreferences = value; }
		}

        /// <summary>
        /// Currency Balances
        /// </summary>
        public UserBalances Balances
        {
            get { return _balances; }
            set { _balances = value; }
        }

        /// <summary>
        /// Facebook Settings
        /// </summary>
        public FacebookSettings FacebookSettings
        {
            get { return _facebookSettings; }
            set { _facebookSettings = value; }
        }

        // Blast 
        #region Blast

        public bool BlastPrivacyBlogs
        {
            get { return _chkBlogcomments; }
        }

        public bool BlastPrivacyProfiles
        {
            get { return _chkProfiles; }
        }

        public bool BlastPrivacyComments
        {
            get { return _chkComment; }
        }

        public bool BlastPrivacyForums
        {
            get { return _chkForum; }
        }

        public bool BlastPrivacyFriendsAdd
        {
            get { return _chkAddFriend; }
        }

        public bool BlastPrivacyJoinCommunity
        {
            get { return _chkJoinCommunity; }
        }

        public bool BlastPrivacyRave3dHome
        {
            get { return _chkRave3DHome; }
        }

        public bool BlastPrivacyRave3dHangout
        {
            get { return _chkRave3DHangout; }
        }

        public bool BlastPrivacySendGift
        {
            get { return _chkSendGift; }
        }

        public bool BlastShowBlogs
        {
            get { return _chkShowBlogcomments; }
        }

        public bool BlastShowProfiles
        {
            get { return _chkShowProfiles; }
        }

        public bool BlastShowComments
        {
            get { return _chkShowComment; }
        }

        public bool BlastShowForums
        {
            get { return _chkShowForum; }
        }

        public bool BlastShowFriendsAdd
        {
            get { return _chkShowAddFriend; }
        }

        public bool BlastShowJoinCommunity
        {
            get { return _chkShowJoinCommunity; }
        }

        public bool BlastShowRave3dHome
        {
            get { return _chkShowRave3DHome; }
        }

        public bool BlastShowRave3dHangout
        {
            get { return _chkShowRave3DHangout; }
        }

        public bool BlastShowSendGift
        {
            get { return _chkShowSendGift; }
        }

        public bool BlastShowRave3dApp
        {
            get { return _chkShowRave3DApp; }
        }

        public bool BlastShowCameraScreenshot
        {
            get { return _chkShowCameraScreenshot; }
        }

        public string BlastPrivacyPermissionsBase2
        {
            get { return _blastPrivacyPermissionsBase2; }
        }

        public string BlastShowPermissionsBase2
        {
            get { return _blastShowPermissionsBase2; }
        }

        #endregion

        /// <summary>
        /// Password salt
        /// </summary>
        public string Salt
        {
            get { return _salt; }
            set { _salt = value; }
        }

        /// <summary>
        /// Password hash
        /// </summary>
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public DateTime? FirstWoKLogin { get; set; }

        private enum eBLAST_PREFERENCES
        {

            BLOG_COMMENT = 0,
            PROFILES = 1,
            COMMENTS = 2,
            FORUMS = 3,
            ADD_FRIEND = 4,
            JOIN_COMMUNITY = 5,
            RAVE_HOME = 6,
            RAVE_HANGOUT = 7,
            SEND_GIFT = 8,
            RAVE_3DAPP = 9,
            CAMERA_SCREENSHOT = 10
        }

        public enum eUSER_STATUS
        {
            REGVALIDATED = 1,
            REGNOTVALIDATED = 2,
            DELETEDBYUS = 3,
            DELETED = 4,
            LOCKED = 5,
            LOCKEDVALIDATED = 6,
            DELETEDVALIDATED = 7
        }

    }
}
