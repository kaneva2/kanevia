///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class WorldExperimentGroup
    {
        private string _worldGroupId;
        private int _zoneInstanceId;
        private int _zoneType;
        private string _groupId;
        private DateTime _assignmentDate;
        private DateTime? _conversionDate;
        private ExperimentGroup _experimentGroup;

        public WorldExperimentGroup() { }

        public WorldExperimentGroup(string worldGroupId, int zoneInstanceId, int zoneType, string groupId, DateTime assignmentDate, 
            DateTime? conversionDate, ExperimentGroup experimentGroup = null)
        {
            _worldGroupId = worldGroupId;
            _zoneInstanceId = zoneInstanceId;
            _zoneType = zoneType;
            _groupId = groupId;
            _assignmentDate = assignmentDate;
            _conversionDate = conversionDate;
            _experimentGroup = experimentGroup;
        }

        public string WorldGroupId
        {
            get { return _worldGroupId; }
            set { _worldGroupId = value; }
        }

        public int ZoneInstanceId
        {
            get { return _zoneInstanceId; }
            set { _zoneInstanceId = value; }
        }

        public int ZoneType
        {
            get { return _zoneType; }
            set { _zoneType = value; }
        }

        public string GroupId
        {
            get { return _groupId; }
            set { _groupId = value; }
        }

        public DateTime AssignmentDate
        {
            get { return _assignmentDate; }
            set { _assignmentDate = value; }
        }

        public DateTime? ConversionDate
        {
            get { return _conversionDate; }
            set { _conversionDate = value; }
        }

        [XmlIgnore]
        public ExperimentGroup ExperimentGroup
        {
            get { return _experimentGroup; }
            set { _experimentGroup = value; }
        }
    }
}
