///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public static class EXPERIMENT_STATUS_TYPE
    {
        public const string CREATED  = "C";
        public const string READY    = "R";
        public const string STARTED  = "S";
        public const string ENDED    = "E";
        public const string ARCHIVED = "A";

        public static string ConvertFromFullStatusToDBStatus(string fullStatus)
        {
            switch (fullStatus.ToUpper())
            {
                case "CREATED":
                    return CREATED;
                case "READY":
                    return READY;
                case "STARTED":
                    return STARTED;
                case "ENDED":
                    return ENDED;
                case "ARCHIVED":
                    return ARCHIVED;
                default:
                    return "UNKNOWN";
            }
        }

        public static string ConvertFromDBStatusToFullStatus(string dbStatus)
        {
            switch (dbStatus.ToUpper())
            {
                case CREATED:
                    return "Created";
                case READY:
                    return "Ready";
                case STARTED:
                    return "Started";
                case ENDED:
                    return "Ended";
                case ARCHIVED:
                    return "Archived";
                default:
                    return "Unknown";
            }
        }
    }

    [Serializable]
    public class Experiment
    {
        private string _experimentId;
        private string _name;
        private string _description;
        private string _status;
        private string _winningGroupId;
        private DateTime _creationDate;
        private DateTime? _startDate;
        private DateTime? _endDate;
        private string _assignGroupsOn;
        private string _category;
        private int _creatorId;
        private string _assignmentTarget;
        private string _cleanupJIRA;
        private List<ExperimentGroup> _experimentGroups;

        public const string ASSIGN_ON_REQUEST = "Request";
        public const string ASSIGN_ON_CREATION_ONLY = "CreationOnly";

        public const string ASSIGNMENT_TARGET_USERS = "Users";
        public const string ASSIGNMENT_TARGET_WORLDS = "Worlds";

        public Experiment() { }

        public Experiment(string experimentId, string name, string description, string status, string winningGroupId, DateTime creationDate, 
            DateTime? startDate, DateTime? endDate, string assignGroupsOn, string category, int creatorId, string assignmentTarget, string cleanupJIRA,
            List<ExperimentGroup> experimentGroups = null)
        {
            _experimentId = experimentId;
            _name = name;
            _description = description;
            _status = status;
            _winningGroupId = winningGroupId;
            _creationDate = creationDate;
            _startDate = startDate;
            _endDate = endDate;
            _assignGroupsOn = assignGroupsOn;
            _category = category;
            _creatorId = creatorId;
            _experimentGroups = experimentGroups;
            _assignmentTarget = assignmentTarget;
            _cleanupJIRA = cleanupJIRA;
        }

        public string ExperimentId
        {
            get { return _experimentId; }
            set { _experimentId = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public string WinningGroupId
        {
            get { return _winningGroupId; }
            set { _winningGroupId = value; }
        }

        public DateTime CreationDate
        {
            get { return _creationDate; }
            set { _creationDate = value; }
        }

        public DateTime? StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        public DateTime? EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        public string AssignGroupsOn
        {
            get { return _assignGroupsOn; }
            set { _assignGroupsOn = value; }
        }

        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }

        public int CreatorId
        {
            get { return _creatorId; }
            set { _creatorId = value; }
        }

        public string AssignmentTarget
        {
            get { return _assignmentTarget; }
            set { _assignmentTarget = value; }
        }

        public string CleanupJIRA
        {
            get { return _cleanupJIRA; }
            set { _cleanupJIRA = value; }
        }

        public List<ExperimentGroup> ExperimentGroups
        {
            get
            {
                if (_experimentGroups == null)
                    _experimentGroups = new List<ExperimentGroup>();
                return _experimentGroups;
            }
            set { _experimentGroups = value; }
        }

        public ExperimentGroup WinningGroup
        {
            get 
            {
                return ExperimentGroups.SingleOrDefault(g => g.GroupId == _winningGroupId);
            }
        }
    }
}
