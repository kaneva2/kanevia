///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class ExperimentGroup
    {
        private string _groupId;
        private string _experimentId;
        private string _name;
        private string _description;
        private DateTime _creationDate;
        private string _label;
        private int _assignmentPercentage;
        private Experiment _experiment;
        private List<ExperimentEffect> _effects;

        public ExperimentGroup() { }

        public ExperimentGroup(string groupId, string experimentId, string name, string description, DateTime creationDate, string label, 
            int assignmentPercentage, Experiment experiment = null, List<ExperimentEffect> effects = null)
        {
            _groupId = groupId;
            _experimentId = experimentId;
            _name = name;
            _description = description;
            _creationDate = creationDate;
            _label = label;
            _assignmentPercentage = assignmentPercentage;
            _experiment = experiment;
            _effects = effects;
        }

        public string GroupId
        {
            get { return _groupId; }
            set { _groupId = value; }
        }

        public string ExperimentId
        {
            get { return _experimentId; }
            set { _experimentId = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public DateTime CreationDate
        {
            get { return _creationDate; }
            set { _creationDate = value; }
        }

        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }

        public int AssignmentPercentage
        {
            get { return _assignmentPercentage; }
            set { _assignmentPercentage = value; }
        }

        public Experiment Experiment
        {
            get { return _experiment; }
            set { _experiment = value; }
        }

        public List<ExperimentEffect> Effects
        {
            get 
            {
                if (_effects == null)
                    _effects = new List<ExperimentEffect>();
                return _effects;
            }
            set { _effects = value; }
        }
    }
}
