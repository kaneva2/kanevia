///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class ExperimentEffect
    {
        private string _effect;
        private string _description;

        public const string EFFECT_USE_AS_HOME_TEMPLATE = "Use As Home Template";

        /// <summary>
        /// Overloaded constructor for the UserStats class.
        /// </summary>
        public ExperimentEffect(string effect, string description)
        {
            this._effect = effect;
            this._description = description;
        }

        public string Effect
        {
            get { return _effect; }
            set { _effect = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
    }
}
