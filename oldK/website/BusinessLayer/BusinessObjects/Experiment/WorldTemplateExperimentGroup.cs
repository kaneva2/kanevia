///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class WorldTemplateExperimentGroup
    {
        private int _templateId;
        private string _groupId;
        private WorldTemplate _template;
        private ExperimentGroup _experimentGroup;

        public WorldTemplateExperimentGroup(int templateId, string groupId, WorldTemplate template = null, ExperimentGroup experimentGroup = null)
        {
            _templateId = templateId;
            _groupId = groupId;
            _template = template;
            _experimentGroup = experimentGroup;
        }

        public int TemplateId
        {
            get { return _templateId; }
            set { _templateId = value; }
        }

        public string GroupId
        {
            get { return _groupId; }
            set { _groupId = value; }
        }

        public WorldTemplate WorldTemplate
        {
            get { return _template; }
            set { _template = value; }
        }

        public ExperimentGroup ExperimentGroup
        {
            get { return _experimentGroup; }
            set { _experimentGroup = value; }
        }
    }
}
