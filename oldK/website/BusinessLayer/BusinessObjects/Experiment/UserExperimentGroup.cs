///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class UserExperimentGroup
    {
        private string _userGroupId;
        private int _userId;
        private string _groupId;
        private DateTime _assignmentDate;
        private DateTime? _conversionDate;
        private bool _assignedViaUrl;
        private string _username;
        private ExperimentGroup _experimentGroup;

        public UserExperimentGroup() { }

        public UserExperimentGroup(string userGroupId, int userId, string groupId, DateTime userAssignmentDate, DateTime? userConversionDate, 
            bool assignedViaUrl, string username, ExperimentGroup experimentGroup = null)
        {
            _userGroupId = userGroupId;
            _userId = userId;
            _groupId = groupId;
            _assignmentDate = userAssignmentDate;
            _conversionDate = userConversionDate;
            _assignedViaUrl = assignedViaUrl;
            _username = username;
            _experimentGroup = experimentGroup;
        }

        public string UserGroupId
        {
            get { return _userGroupId; }
            set { _userGroupId = value; }
        }

        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        public string GroupId
        {
            get { return _groupId; }
            set { _groupId = value; }
        }

        public DateTime AssignmentDate
        {
            get { return _assignmentDate; }
            set { _assignmentDate = value; }
        }

        public DateTime? ConversionDate
        {
            get { return _conversionDate; }
            set { _conversionDate = value; }
        }

        public bool AssignedViaUrl
        {
            get { return _assignedViaUrl; }
            set { _assignedViaUrl = value; }
        }

        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        [XmlIgnore]
        public ExperimentGroup ExperimentGroup
        {
            get { return _experimentGroup; }
            set { _experimentGroup = value; }
        }
    }
}
