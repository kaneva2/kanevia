///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class UserMessage : Message
    {
        private string thumbnailSquarePath;
        private string commNameNoSpaces;
        private string userName;
        private DateTime currentDate;

        private FacebookSettings _facebookSettings = new FacebookSettings ();

        public UserMessage () { }

        public UserMessage (int messageId, int fromId, int toId,
            string subject, string messageText, DateTime messageDate,
            int replied, int type, int channelId, string toViewable, string fromViewable,
            string thumbnailSquarePath, string communityNameNoSpaces, string userName, DateTime currentDate)
        {
            this.thumbnailSquarePath = thumbnailSquarePath;
            this.commNameNoSpaces = communityNameNoSpaces;
            this.userName = userName;
            this.currentDate = currentDate;

            this.MessageId = messageId;
            this.FromId = fromId;
            this.ToId = toId;
            this.Subject = subject;
            this.MessageText = messageText;
            this.MessageDate = messageDate;
            this.Replied = replied;
            this.Type = type;
            this.ChannelId = channelId;
            this.ToViewable = toViewable;
            this.FromViewable = fromViewable;
        }

        public string ThumbnailSquarePath
        {
            get { return thumbnailSquarePath; }
            set { thumbnailSquarePath = value; }
        }

        public string CommunityNameNoSpaces
        {
            get { return commNameNoSpaces; }
            set { commNameNoSpaces = value; }
        }

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public DateTime CurrentDate
        {
            get { return currentDate; }
            set { currentDate = value; }
        }

        public FacebookSettings OwnerFacebookSettings
        {
            get { return _facebookSettings; }
            set { _facebookSettings = value; }
        }
    }
}
