///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class UGCProperty
    {
        // ===========================
        // Start property IDs
        public const int PROP_MESH_CNT = 0x1000;
        public const int PROP_VERTEX_CNT = 0x1001;
        public const int PROP_TRIANGLE_CNT = 0x1002;
        public const int PROP_BOUNDBOX_SIZEX = 0x1003;
        public const int PROP_BOUNDBOX_SIZEY = 0x1004;
        public const int PROP_BOUNDBOX_SIZEZ = 0x1005;
        public const int PROP_MATERIAL_CNT = 0x1006;
        public const int PROP_TEXTURE_CNT = 0x1007;
        public const int PROP_DISTINCT_TEXTURE_CNT = 0x1008;
        public const int PROP_BOUNDBOX_VOLUME = 0x1009;
        public const int PROP_BOUNDBOX_MINX = 0x100A;
        public const int PROP_BOUNDBOX_MINY = 0x100B;
        public const int PROP_BOUNDBOX_MINZ = 0x100C;
        public const int PROP_BOUNDBOX_MAXX = 0x100D;
        public const int PROP_BOUNDBOX_MAXY = 0x100E;
        public const int PROP_BOUNDBOX_MAXZ = 0x100F;
        public const int PROP_BOUNDBOX_XYZSUM = 0x1010;
        public const int PROP_MEDIA_PLAYBACK = 0x1011;
        public const int PROP_COLLISION = 0x1012;

        public const int PROP_MESH_VERTEX_CNT = 0x2001;
        public const int PROP_MESH_TRIANGLE_CNT = 0x2002;
        public const int PROP_MESH_BOUNDBOX_SIZEX = 0x2003;
        public const int PROP_MESH_BOUNDBOX_SIZEY = 0x2004;
        public const int PROP_MESH_BOUNDBOX_SIZEZ = 0x2005;
        public const int PROP_MESH_MATERIAL_CNT = 0x2006;
        public const int PROP_MESH_TEXTURE_CNT = 0x2007;
        public const int PROP_MESH_DISTINCT_TEXTURE_CNT = 0x2008;
        public const int PROP_MESH_BOUNDBOX_VOLUME = 0x2009;
        public const int PROP_MESH_BOUNDBOX_MINX = 0x200A;
        public const int PROP_MESH_BOUNDBOX_MINY = 0x200B;
        public const int PROP_MESH_BOUNDBOX_MINZ = 0x200C;
        public const int PROP_MESH_BOUNDBOX_MAXX = 0x200D;
        public const int PROP_MESH_BOUNDBOX_MAXY = 0x200E;
        public const int PROP_MESH_BOUNDBOX_MAXZ = 0x200F;
        public const int PROP_MESH_BOUNDBOX_XYZSUM = 0x2010;

        public const int PROP_IMAGE_FILESIZE = 0x4000;
        public const int PROP_IMAGE_PIXEL_CNT = 0x4001;
        public const int PROP_IMAGE_WIDTH = 0x4002;
        public const int PROP_IMAGE_HEIGHT = 0x4003;
        public const int PROP_IMAGE_DEPTH = 0x4004;
        public const int PROP_IMAGE_OPACITY = 0x4005;
        public const int PROP_IMAGE_AVG_COLOR = 0x4006;
        public const int PROP_IMAGE_ID = 0x4007;

        public const int PROP_EFFECT_DURATION = 0x8000;
        public const int PROP_EFFECT_FRAME_COUNT = 0x8001;
        public const int PROP_SKELETON_BONE_COUNT = 0x8002;
        public const int PROP_TARGET_ACTOR_TYPE = 0x8003;
        public const int PROP_TARGET_ACTOR_GLID = 0x8004;
        public const int PROP_EXCLUSION_GROUPS = 0x8010;

        public const int PROP_ACTIONITEM_PARENTSCRIPTGLID = 0x8011;
        // End property IDs
    }
}
