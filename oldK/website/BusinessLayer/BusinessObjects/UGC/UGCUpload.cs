///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class UGCUpload
    {
        public enum eType
        {
            UNKNOWN = 0,       // Unknown upload type
            DOB_GZ = 1,        // User generated dynamic objects
            TEX_DDS_KRX_GZ_DEPRECATED = 2,// Textures for user generated dynamic objects (encrypted and gz-compressed DDS)
            CUSTOMTEX = 3,     // Custom textures
            ICON = 4,          // Icon
            EQP_GZ = 5,	    // Equippable objects
            TEX_DDS_GZ = 6,    // Textures for user generated dynamic objects (gz-compressed DDS)
            MEDIA = 10,
            PATTERN = 11,
            PICTURE = 12,
            ANM_GZ = 13,
            SND_OGG_KRX = 14,
            PARTICLE = 15,
            ACTIONITEM = 16,
            APPASSET = 17,
        }

        public enum eUsage
        {
            UNKNOWN = 0,
            KEP_DYNAMIC_OBJ = 1,
            KEP_ZONE = 3,
            KEP_ACTOR = 4,
            KEP_CLOTHING = 5,
            KEP_ARMABLE = 6,
            UGC_DYNAMIC_OBJ = 101,
            UGC_ZONE = 103,
            UGC_CLOTHING = 105,
            UGC_EQUIPPABLE = 106,
            UGC_CHARANIM = 110,
            UGC_SOUND = 120,
            UGC_PARTICLE_EFFECT = 130,
        }

        public enum eState
        {
            WAITING = 0,       // Record created and waiting for data upload
            UPLOADED = 1,      // Uploading completed.
            COMPLETED = 2,     // All necessary operations have been completed. Ready to archive.
            CANCELLED = 3,     // Uploading cancelled by user
            FAILED = 4,        // Uploading failed (misc reasons)
            REJECTED = 5,      // Uploading rejected because of invalid data (bad checksum, etc.)
            DERIVATION = 10,   // UGC derivation entry (no upload needed, sub uploads must match existing record)
            DONTCARE = -1,
        }

        public UGCUpload(int id, eType type, int userId, string name, DateTime time, string summary, int basePrice, bool isDerivation, UInt32 baseCommission, int baseGlid, int validationReqId, int bytesUploaded, string originalHash, int originalSize)
        {
            _Id = id;
            _type = type;
            _userId = userId;
            _name = name;
            _updateTime = time;
            _summary = summary;
            _basePrice = basePrice;
            _isDerivation = isDerivation;
            _baseCommission = baseCommission;
            _baseGlid = baseGlid;
            _validationReqId = validationReqId;
            _bytesUploaded = bytesUploaded;
            _originalHash = originalHash;
            _originalSize = originalSize;
        }

        public int ID
        {
            get { return _Id; }
        }

        public string Name
        {
            get { return _name; }
        }

        public eType Type
        {
            get { return _type; }
        }

        public int UserID
        {
            get { return _userId; }
        }

        public DateTime UpdateTime
        {
            get { return _updateTime; }
        }

        public string Summary
        {
            get { return _summary; }
        }

        public int BasePrice
        {
            get { return _basePrice; }
        }

        public bool IsDerivation
        {
            get { return _isDerivation; }
        }

        public UInt32 BaseCommission
        {
            get { return _baseCommission; }
        }

        public int BaseGlid
        {
            get { return _baseGlid; }
        }

        public int ValidationReqID
        {
            get { return _validationReqId; }
        }

        public int BytesUploaded
        {
            get { return _bytesUploaded; }
        }

        public string OriginalHash
        {
            get { return _originalHash; }
        }

        public int OriginalSize
        {
            get { return _originalSize; }
        }

        public bool IsDOAnimation
        {
            get { return Type == UGCUpload.eType.ANM_GZ && AnimTargetActorGLID > 0; }
        }

        public UInt32 AnimTargetActorGLID
        {
            get { return _actorGLID; }
            set { _actorGLID = value; }
        }

        public string ScriptBundleGLIDs
        {
            get { return _scriptBundleGLIDs; }
            set { _scriptBundleGLIDs = value; }
        }

        public string DefaultGlid
        {
            get { return _defaultGlid; }
            set { _defaultGlid = value; }
        }

        public UInt32 AssetType
        {
            get { return _assetType; }
            set { _assetType = value; }
        }

        private int _Id;
        private eType _type;
        private int _userId;
        private string _name;
        private DateTime _updateTime;
        private string _summary;
        private int _basePrice;
        private UInt32 _baseCommission;
        private bool _isDerivation;
        private int _baseGlid;
        private int _validationReqId;
        private int _bytesUploaded;
        private string _originalHash;
        private int _originalSize;
        private UInt32 _actorGLID = 0;
        private string _scriptBundleGLIDs = "";
        private string _defaultGlid = "";
        private UInt32 _assetType = 0;


        // Must match UGC_Globals.h
        public const string UGC_UPLOAD_DOB_GZ = "dou";          // dob.gz
        public const string UGC_UPLOAD_TEX_DDS_GZ = "dds.gz";   // dds.gz
        public const string UGC_UPLOAD_CUSTOM_TEX = "ctu";
        public const string UGC_UPLOAD_ICON = "itu";
        public const string UGC_UPLOAD_MEDIA = "mu";
        public const string UGC_UPLOAD_PATTERN = "patu";
        public const string UGC_UPLOAD_PICTURE = "picu";
        public const string UGC_UPLOAD_ANM_GZ = "cau";          // anm.gz
        public const string UGC_UPLOAD_SND_OGG_KRX = "snd";     // ogg.krx
        public const string UGC_UPLOAD_EQP_GZ = "equ";          // eqp.gz
        public const string UGC_UPLOAD_PARTICLE = "peu";        // par
        public const string UGC_UPLOAD_ACTION_ITEM = "act";
        public const string UGC_UPLOAD_APP_ASSET = "ass";

        #region Static Functions
        /// <summary>
        /// ParseUploadType - return numeric upload type based on upload type string
        /// </summary>
        public static eType ParseUploadType(string sUploadType)
        {
            eType type = eType.UNKNOWN;

            if (sUploadType == UGC_UPLOAD_DOB_GZ)
                type = eType.DOB_GZ;
            else if (sUploadType == UGC_UPLOAD_TEX_DDS_GZ)
                type = eType.TEX_DDS_GZ;
            else if (sUploadType == UGC_UPLOAD_CUSTOM_TEX)
                type = eType.CUSTOMTEX;
            else if (sUploadType == UGC_UPLOAD_ICON)
                type = eType.ICON;
            else if (sUploadType == UGC_UPLOAD_MEDIA)
                type = eType.MEDIA;
            else if (sUploadType == UGC_UPLOAD_PATTERN)
                type = eType.PATTERN;
            else if (sUploadType == UGC_UPLOAD_PICTURE)
                type = eType.PICTURE;
            else if (sUploadType == UGC_UPLOAD_ANM_GZ)
                type = eType.ANM_GZ;
            else if (sUploadType == UGC_UPLOAD_SND_OGG_KRX)
                type = eType.SND_OGG_KRX;
            else if (sUploadType == UGC_UPLOAD_EQP_GZ)
                type = eType.EQP_GZ;
            else if (sUploadType == UGC_UPLOAD_PARTICLE)
                type = eType.PARTICLE;
            else if (sUploadType == UGC_UPLOAD_ACTION_ITEM)
                type = eType.ACTIONITEM;
            else if (sUploadType == UGC_UPLOAD_APP_ASSET)
                type = eType.APPASSET;

            return type;
        }

        /// <summary>
        /// GetUploadTypeString - return upload type string based on numeric upload type
        /// </summary>
        public static string GetUploadTypeString(eType uploadType)
        {
            switch (uploadType)
            {
                case eType.DOB_GZ:
                    return UGC_UPLOAD_DOB_GZ;
                case eType.TEX_DDS_GZ:
                    return UGC_UPLOAD_TEX_DDS_GZ;
                case eType.CUSTOMTEX:
                    return UGC_UPLOAD_CUSTOM_TEX;
                case eType.ICON:
                    return UGC_UPLOAD_ICON;
                case eType.MEDIA:
                    return UGC_UPLOAD_MEDIA;
                case eType.PATTERN:
                    return UGC_UPLOAD_PATTERN;
                case eType.PICTURE:
                    return UGC_UPLOAD_PICTURE;
                case eType.ANM_GZ:
                    return UGC_UPLOAD_ANM_GZ;
                case eType.SND_OGG_KRX:
                    return UGC_UPLOAD_SND_OGG_KRX;
                case eType.EQP_GZ:
                    return UGC_UPLOAD_EQP_GZ;
                case eType.PARTICLE:
                    return UGC_UPLOAD_PARTICLE;
                case eType.ACTIONITEM:
                    return UGC_UPLOAD_ACTION_ITEM;
                case eType.APPASSET:
                    return UGC_UPLOAD_APP_ASSET;
            }

            return "";
        }

        /// <summary>
        /// GetUploadTypeFileFormat
        /// </summary>
        public static string GetUploadFileFormat(eType uploadType)
        {
            // Return a value matching item_path_types.file_format column
            switch (uploadType)
            {
                case eType.DOB_GZ:
                    return "dob.gz";
                case eType.SND_OGG_KRX:
                    return "ogg.krx";
                case eType.ANM_GZ:
                    return "anm.gz";
                case eType.EQP_GZ:
                    return "eqp.gz";
                case eType.PARTICLE:
                    return "par";
                case eType.TEX_DDS_GZ:
                    return "dds.gz";
                case eType.ICON:
                    return "jpg";
                case eType.CUSTOMTEX:
                case eType.PATTERN:
                case eType.PICTURE:
                case eType.ACTIONITEM:
                case eType.APPASSET:
                    return "";
                default:
                    throw new ArgumentException();
            }
        }

        /// <summary>
        /// GetUploadTypeDescription
        /// </summary>
        public static string GetUploadTypeDescription(eType uploadType)
        {
            switch (uploadType)
            {
                case eType.DOB_GZ:
                    return "Dynamic Object";

                case eType.TEX_DDS_GZ:
                    return "Texture";

                case eType.CUSTOMTEX:
                    return "Texture";

                case eType.ICON:
                    return "Preview Image";

                case eType.MEDIA:
                    return "Media";

                case eType.PATTERN:
                    return "Pattern";

                case eType.PICTURE:
                    return "Picture";

                case eType.ANM_GZ:
                    return "Animation";

                case eType.SND_OGG_KRX:
                    return "Sound";

                case eType.EQP_GZ:
                    return "Equippable Object";

                case eType.PARTICLE:
                    return "Particle Effect";
            }

            return "";
        }
        #endregion
    }

    #region wok.ugc_uploads
    /*
    CREATE TABLE wok.ugc_uploads (
	    upload_id			int(20)			primary key auto_increment,	-- upload ID with random start ID
	    upload_type			int(3)			not null,	-- upload Type Id
	    master_upload_id	int(20)			not null,	-- a link to parent item for bundled uploads (e.g. textures for dynamic object)
        sub_id              bigint          not null,	-- sub ID to identify multiple uploads per UGC
	    player_id			int(11)			not null,	-- player_id as in players table
	    bytes				int(11)			not null,	-- size of uploaded file
	    hash				varchar(300)	not null,	-- MD5 hash of uploaded file
	    origName			varchar(100)	,		-- name of the asset (dynamic object name, original texture file name, etc)
	    uploadName			varchar(300)	,		-- uploaded file name (file name of the temporary server copy)
	    summary				varchar(200)	,		-- automatically generated human-readable summaries
	    state				int(3)			not null,	-- current state
        global_id           int(11)         ,		-- global ID for created ugc item 
	    creationTime		datetime		not null,
	    updateTime			datetime		not null)
	    auto_increment = 9504;

    -- UGC items numbered from 1,000,000,000 ( this will leave 3m ~ 999.99m to customed items )
    CREATE TABLE shard_info.items_ugc (
        id int(10) unsigned PRIMARY KEY
    );

    INSERT INTO shard_info.items_ugc (id) VALUES (1000000000);

    -- procedure to auto increment UGC items GLID
    CREATE FUNCTION `shard_info`.`get_items_ugc_id`() RETURNS int(11)
    BEGIN
      UPDATE shard_info.items_ugc SET id=@_item_id:=id+1;
      return @_item_id;
    END;
    */
    #endregion
}
