///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public enum eINVITE_SOURCE_TYPE
    {
        None = 0,
        Registration = 1,                   
        Community_Creation = 2,             
        Invite_Friend = 3,
        AOL = 4,
        Gmail = 5,
        Yahoo = 6,
        Outlook = 7,
        Facebook = 8,
        Manual = 9
    }
    
    [Serializable]
    public class Invite
    {
        private int _InviteId;
        private int _UserId;
        private string _Email;
        private string _KeyValue;
        private int _InviteStatusId;
        private DateTime _InvitedDate;
        private string _PointsAwarded;
        private string _KeiPointIdAwarded;
        private DateTime _ReinviteDate;
        private UInt32 _ChannelId;
        private int _InvitedUserId;
        private UInt32 _Attempts;

        public Invite (int inviteId, int userId, string email, string keyValue, int inviteStatusId, 
            DateTime invitedDate, string pointsAwarded, string keiPointIdAwarded, 
            DateTime reinviteDate, UInt32 channelId, int invitedUserId, UInt32 attempts)
        {
            this._InviteId = inviteId;
            this._UserId = userId;
            this._Email = email;
            this._KeyValue = keyValue;
            this._InviteStatusId = inviteStatusId;
            this._InvitedDate = invitedDate;
            this._PointsAwarded = pointsAwarded;
            this._KeiPointIdAwarded = keiPointIdAwarded;
            this._ReinviteDate = reinviteDate;
            this._ChannelId = channelId;
            this._InvitedUserId = invitedUserId;
            this._Attempts = attempts;
        }

        public int InviteId
        {
           get {return _InviteId;}
           set {_InviteId = value;}
        }

        public int UserId
        {
           get {return _UserId;}
           set {_UserId = value;}
        }

        public string Email
        {
           get {return _Email;}
           set {_Email = value;}
        }

        public string KeyValue
        {
           get {return _KeyValue;}
           set {_KeyValue = value;}
        }

        public int InviteStatusId
        {
           get {return _InviteStatusId;}
           set {_InviteStatusId = value;}
        }

        public DateTime InvitedDate
        {
           get {return _InvitedDate;}
           set {_InvitedDate = value;}
        }

        public string PointsAwarded
        {
           get {return _PointsAwarded;}
           set {_PointsAwarded = value;}
        }

        public string KeiPointIdAwarded
        {
           get {return _KeiPointIdAwarded;}
           set {_KeiPointIdAwarded = value;}
        }

        public DateTime ReinviteDate
        {
           get {return _ReinviteDate;}
           set {_ReinviteDate = value;}
        }

        public UInt32 ChannelId
        {
           get {return _ChannelId;}
           set {_ChannelId = value;}
        }

        public int InvitedUserId
        {
           get {return _InvitedUserId;}
           set {_InvitedUserId = value;}
        }

        public UInt32 Attempts
        {
           get {return _Attempts;}
           set {_Attempts = value;}
        }
    }
}
