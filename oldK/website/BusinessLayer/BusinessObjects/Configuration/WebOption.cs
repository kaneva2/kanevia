///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class WebOption: WebOptionTemplate
    {
        public WebOption(string group, string key, int role, string val, string desc)
            : this(group, key, role, val, desc, null, null, null, null)
        {
        }

        public WebOption(string group, string key, int role, string val, string desc, string defaultValues, string helpExamples, string helpReferences, string helpSeeAlsos)
                : base( group, key, desc, true, false, defaultValues, helpExamples, helpReferences, helpSeeAlsos )
        {
            _role = role;
            _value = val;
        }

        public int role
        {
            get { return _role; }
            set { _role = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }

        private int _role;
        private string _value;
    }
}
