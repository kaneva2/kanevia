///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class WebOptionTemplate
    {
        public WebOptionTemplate(string group, string key, string desc, bool enabled, bool supportComplexValues,
            string defaultValues, string helpExamples, string helpReferences, string helpSeeAlsos)
        {
            _group = group;
            _key = key;
            _desc = desc;
            _enabled = enabled;
            _supportComplexValues = supportComplexValues;
            _defaultValues = defaultValues;
            _helpExamples = helpExamples;
            _helpReferences = helpReferences;
            _helpSeeAlsos = helpSeeAlsos;
        }

        public string Group
        {
            get { return _group; }
        }

        public string Key
        {
            get { return _key; }
        }

        public string Description
        {
            get { return _desc; }
        }

        public bool Enabled
        {
            get { return _enabled; }
        }

        public bool SupportComplexValues
        {
            get { return _supportComplexValues; }
        }

        public string DefaultValues
        {
            get { return _defaultValues==null?"":_defaultValues;  }
        }

        public string HelpExamples
        {
            get { return _helpExamples; }
        }

        public string HelpReferences
        {
            get { return _helpExamples; }
        }

        public string HelpSeeAlsos
        {
            get { return _helpSeeAlsos; }
        }

        private string _group;
        private string _key;
        private string _desc;
        private bool _enabled;
        private bool _supportComplexValues;
        private string _defaultValues;
        private string _helpExamples;
        private string _helpReferences;
        private string _helpSeeAlsos;
    }
}
