///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class WebSite
    {
        private int _WebsiteId = 0;
        private string _WebsiteName = "";
        private string _ConnectionString = "";
        private string _WebsiteURL = "";

        public WebSite()
        {
        }

        /// <summary>
        /// Overloaded constructor for the User class.
        /// </summary>
        public WebSite(int WebsiteId, string WebsiteName, string ConnectionString, string WebsiteURL)
        {
            this._WebsiteId = WebsiteId;
            this._WebsiteName = WebsiteName;
            this._ConnectionString = ConnectionString;
            this._WebsiteURL = WebsiteURL;
        }

        /// <summary>
        /// Gets or sets unique user identifier.
        /// The Identity Field Design Pattern. 
        /// </summary>
        /// 

        public int WebsiteId
        {
            get { return _WebsiteId; }
            set { _WebsiteId = value; }
        }

        public string WebsiteName
        {
            get { return _WebsiteName; }
            set { _WebsiteName = value; }
        }

        public string ConnectionString
        {
            get { return _ConnectionString; }
            set { _ConnectionString = value; }
        }

        public string WebsiteURL
        {
            get { return _WebsiteURL; }
            set { _WebsiteURL = value; }
        }

    }


}
