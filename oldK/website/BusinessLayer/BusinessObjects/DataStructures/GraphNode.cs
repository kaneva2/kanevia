///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class GraphNode<K, V> : Node<K, V>
    {
        private List<string> edgeLabels;
        private List<int> costs;

        public GraphNode(K key, V value) : base(key, value) { }
        public GraphNode(K key, V value, NodeList<K,V> neighbors) : base(key, value, neighbors) { }

        public List<string> EdgeLabels
        {
            get
            {
                if (edgeLabels == null)
                    edgeLabels = new List<string>();

                return edgeLabels;
            }
        }

        public List<int> Costs
        {
            get
            {
                if (costs == null)
                    costs = new List<int>();

                return costs;
            }
        }
    }
}
