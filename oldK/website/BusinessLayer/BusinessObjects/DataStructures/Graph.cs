///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class Graph<K,V> : IEnumerable<GraphNode<K,V>>
    {
        private Dictionary<K, GraphNode<K,V>> nodes;

        public Graph() : this(null) { }
        public Graph(Dictionary<K, GraphNode<K,V>> nodes)
        {
            if (nodes == null)
                this.nodes = new Dictionary<K, GraphNode<K,V>>();
            else
                this.nodes = nodes;
        }

        public void AddNode(GraphNode<K,V> node)
        {
            GraphNode<K,V> gnOut;

            if (node == null)
                throw new ArgumentNullException("Cannot add a null node.", "node");
            if (node.Key == null)
                throw new ArgumentException("Node key cannot be null.", "node");
            if (nodes.TryGetValue(node.Key, out gnOut))
                throw new ArgumentException("Node values must be unique.", "node");

            nodes.Add(node.Key, node);   
        }

        public void AddNode(K key, V value)
        {
            GraphNode<K,V> gnOut;

            if (key == null)
                throw new ArgumentNullException("Node key cannot be null.", "key");
            if (nodes.TryGetValue(key, out gnOut))
                throw new ArgumentException("Node keys must be unique.", "key");

            nodes.Add(key, new GraphNode<K,V>(key, value));  
        }

        public void AddDirectedEdge(GraphNode<K,V> from, GraphNode<K,V> to, string label = "", int cost = 0)
        {
            GraphNode<K,V> tOut;

            if (from == null)
                throw new ArgumentNullException("Node cannot be null.", "from");
            if (from.Key == null)
                throw new ArgumentException("Node key cannot be null.", "from");
            if (!nodes.TryGetValue(from.Key, out tOut))
                throw new ArgumentException(string.Format("Unable to find node with value {0}.", from), "from");
            if (to == null)
                throw new ArgumentNullException("Node cannot be null.", "to");
            if (to.Key == null)
                throw new ArgumentException("Node key cannot be null.", "to");
            if (!nodes.TryGetValue(from.Key, out tOut))
                throw new ArgumentException(string.Format("Unable to find node with value {0}.", to), "to");

            from.Neighbors.Add(to);
            from.EdgeLabels.Add(label);
            from.Costs.Add(cost);
        }

        public void AddDirectedEdge(K from, K to, string label = "", int cost = 0)
        {
            GraphNode<K,V> fromNode;
            GraphNode<K,V> toNode;

            if (from == null)
                throw new ArgumentNullException("Node key cannot be null.", "from");
            if (!nodes.TryGetValue(from, out fromNode))
                throw new ArgumentException(string.Format("Unable to find node with value {0}.", from), "from");
            if (to == null)
                throw new ArgumentNullException("Node key cannot be null.", "to");
            if (!nodes.TryGetValue(to, out toNode))
                throw new ArgumentException(string.Format("Unable to find node with value {0}.", to), "to");
            
            fromNode.Neighbors.Add(toNode);
            fromNode.EdgeLabels.Add(label);
            fromNode.Costs.Add(cost);             
        }

        public void AddUndirectedEdge(GraphNode<K,V> from, GraphNode<K,V> to, string label = "", int cost = 0)
        {
            GraphNode<K,V> tOut;

            if (from == null)
                throw new ArgumentNullException("Node cannot be null.", "from");
            if (from.Key == null)
                throw new ArgumentException("Node key cannot be null.", "from");
            if (!nodes.TryGetValue(from.Key, out tOut))
                throw new ArgumentException(string.Format("Unable to find node with value {0}.", from), "from");
            if (to == null)
                throw new ArgumentNullException("Node cannot be null.", "to");
            if (to.Key == null)
                throw new ArgumentException("Node key cannot be null.", "to");
            if (!nodes.TryGetValue(from.Key, out tOut))
                throw new ArgumentException(string.Format("Unable to find node with value {0}.", to), "to");

            from.Neighbors.Add(to);
            from.EdgeLabels.Add(label);
            from.Costs.Add(cost);

            to.Neighbors.Add(from);
            to.EdgeLabels.Add(label);
            to.Costs.Add(cost);
        }

        public void AddUndirectedEdge(K from, K to, string label = "", int cost = 0)
        {
            GraphNode<K,V> fromNode;
            GraphNode<K,V> toNode;

            if (from == null)
                throw new ArgumentNullException("Node key cannot be null.", "from");
            if (!nodes.TryGetValue(from, out fromNode))
                throw new ArgumentException(string.Format("Unable to find node with value {0}.", from), "from");
            if (to == null)
                throw new ArgumentNullException("Node key cannot be null.", "to");
            if (!nodes.TryGetValue(to, out toNode))
                throw new ArgumentException(string.Format("Unable to find node with value {0}.", to), "to");

            fromNode.Neighbors.Add(toNode);
            fromNode.EdgeLabels.Add(label);
            fromNode.Costs.Add(cost);

            toNode.Neighbors.Add(fromNode);
            toNode.EdgeLabels.Add(label);
            toNode.Costs.Add(cost);
        }

        public bool Contains(K key)
        {
            if (key == null)
                return false;

            GraphNode<K,V> gnOut;
            return nodes.TryGetValue(key, out gnOut);
        }

        public GraphNode<K,V> GetNode(K key)
        {
            if (key == null)
                return null;

            return nodes[key];
        }

        public string GetEdgeLabel(GraphNode<K,V> from, GraphNode<K,V> to)
        {
            if (from == null)
                throw new ArgumentNullException("Node cannot be null.", "from");
            if (to == null)
                throw new ArgumentNullException("Node cannot be null.", "to");

            int index = from.Neighbors.IndexOf(to);

            if (index < 0 || index >= from.EdgeLabels.Count)
                return null;

            return from.EdgeLabels[index];
        }

        public List<string> GetEdgeLabels()
        {
            HashSet<string> labelHash = new HashSet<string>();
            foreach (KeyValuePair<K, GraphNode<K,V>> nodePair in nodes)
            {
                foreach (string label in nodePair.Value.EdgeLabels)
                {
                    labelHash.Add(label);
                }
            }

            return labelHash.ToList();
        }

        public NodeList<K,V> GetNeighborsByEdgeLabel(GraphNode<K,V> from, string label)
        {
            if (from == null)
                throw new ArgumentNullException("Node cannot be null.", "from");
            
            List<int> indexes = Enumerable.Range(0, from.EdgeLabels.Count)
                                .Where(i => from.EdgeLabels[i] == label)
                                .ToList();

            if (indexes.Count == 0)
                return null;

            NodeList<K,V> neighbors = new NodeList<K,V>();
            foreach(int index in indexes)
            {
                neighbors.Add(from.Neighbors[index]);
            }

            return neighbors;
        }

        public bool Remove(K key)
        {
            if (key == null)
                return false;

            // first remove the node from the nodeset
            GraphNode<K,V> nodeToRemove;
            if (!nodes.TryGetValue(key, out nodeToRemove))
                // node wasn't found
                return false;

            // otherwise, the node was found
            nodes.Remove(key);

            // enumerate through each node in the nodeSeK, removing edges to this node
            foreach (KeyValuePair<K, GraphNode<K,V>> nodePair in nodes)
            {
                GraphNode<K,V> gnode = nodePair.Value;
                int index = gnode.Neighbors.IndexOf(nodeToRemove);
                if (index != -1)
                {
                    // remove the reference to the node and associated cost
                    gnode.Neighbors.RemoveAt(index);
                    gnode.EdgeLabels.RemoveAt(index);
                    gnode.Costs.RemoveAt(index);
                }
            }

            return true;
        }

        public int Count
        {
            get { return nodes.Count; }
        }
    
        public IEnumerator<GraphNode<K,V>> GetEnumerator()
        {
            return nodes.Values.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
 	        return this.GetEnumerator();
        }
    }
}
