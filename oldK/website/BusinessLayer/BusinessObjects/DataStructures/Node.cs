///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class Node<K,V>
    {
        private K key;
        private V data;
        private NodeList<K,V> neighbors = null;

        public Node(K key, V data) : this(key, data, null) {}
        public Node(K key, V data, NodeList<K,V> neighbors)
        {
            this.key = key;
            this.data = data;
            this.neighbors = neighbors;
        }

        public K Key
        {
            get { return key; }
        }

        public V Value
        {
            get { return data; }
            set { data = value; }
        }

        public NodeList<K,V> Neighbors
        {
            get
            {
                if (neighbors == null)
                    neighbors = new NodeList<K,V>();

                return neighbors;
            }
            set { neighbors = value; }
        }
    }
}
