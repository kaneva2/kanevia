///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

using System.Runtime.Serialization;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [DataContract]
    public class PagedList<T> : IList<T>
    {
        /// <summary>
        /// The number of items in the datasource
        /// </summary>
        [DataMember]
        public UInt32 TotalCount
        {
            get
            {
                return m_TotalCount;
            }
            set
            {
                m_TotalCount = value;
            }
        }

        /// <summary>
        /// Gets or sets the types list or types being proxied.
        /// </summary>
        [DataMember]
        public IList<T> List
        {
            get { return _list; }
            set
            {
                _list = value;
            }
        }

        /// <summary>
        /// Determines the index of a specific item in the list.
        /// </summary>
        /// <remarks>
        /// Before the operation starts it checks whether list is already loaded.
        /// If not, it first lazy-loads the list.
        /// </remarks>
        /// <param name="item">Item for which index is requested.</param>
        /// <returns>Index of the item in the list.</returns>
        public int IndexOf(T item)
        {
            return _list.IndexOf(item);
        }

        /// <summary>
        /// Inserts an item at the given index in the list.
        /// </summary>
        /// <remarks>
        /// Before the operation starts it checks whether list is loaded.
        /// If not, it first lazy-loads it.
        /// </remarks>
        /// <param name="index">Index at which item is inserted.</param>
        /// <param name="item">Item being inserted.</param>
        public void Insert(int index, T item)
        {
            _list.Insert(index, item);
        }

        /// <summary>
        /// Removes the list item at the specified index.
        /// </summary>
        /// <remarks>
        /// Before the operation starts it checks whether list is loaded.
        /// If not, it first lazy-loads it.
        /// </remarks>
        /// <param name="index">Index position.</param>
        public void RemoveAt(int index)
        {
            _list.RemoveAt(index);
        }

        /// <summary>
        /// Indexer into list. Get or set item at given index.
        /// </summary>
        /// <remarks>
        /// Before the operation starts it checks whether list is loaded.
        /// If not, it first lazy-loads it.
        /// </remarks>
        /// <param name="index">Index at which item is located.</param>
        /// <returns>Type at index location.</returns>
        public T this[int index]
        {
            get
            {
                return _list[index];
            }
            set
            {
                _list[index] = value;
            }
        }

        #region ICollection<T> Members

        /// <summary>
        /// Adds an item to the list.
        /// <remarks>
        /// Before the operation starts it checks whether list is loaded.
        /// If not, it first lazy-loads it.
        /// </remarks>
        /// </summary>
        /// <param name="item">Item to be added</param>
        public void Add(T item)
        {
            _list.Add(item);
        }

        /// <summary>
        /// Removes all items from the list.
        /// </summary>
        public void Clear()
        {
            _list.Clear();
        }

        /// <summary>
        /// Determines whether the list contains a specific item.
        /// </summary>
        /// <remarks>
        /// Before the operation starts it checks whether list is loaded.
        /// If not, it first lazy-loads it.
        /// </remarks>
        /// <param name="item">Item being looked for.</param>
        /// <returns>Value indicating whether item is present in list.</returns>
        public bool Contains(T item)
        {
            return _list.Contains(item);
        }

        /// <summary>
        /// Copies the element of list into an array starting at a given index.
        /// </summary>
        /// <remarks>
        /// Before the operation starts it checks whether list is loaded.
        /// If not, it first lazy-loads it.
        /// </remarks>
        /// <param name="array">Generic array being copied into.</param>
        /// <param name="arrayIndex">Start index from which to copy.</param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Gets the number of elements in the list.
        /// </summary>
        /// <remarks>
        /// Before the operation starts it checks whether list is loaded.
        /// If not, it first lazy-loads it.
        /// </remarks>
        [DataMember]
        public int Count
        {
            get
            {
                return _list.Count;
            }
            private set { }
        }

        /// <summary>
        /// Gets the value indicating whether the list is read-only.
        /// </summary>
        /// <remarks>
        /// Before the operation starts it checks whether list is loaded.
        /// If not, it first lazy-loads it.
        /// </remarks>
        public bool IsReadOnly
        {
            get
            {
                return _list.IsReadOnly;
            }
        }

        /// <summary>
        /// Removes the first occurrence of a specific item in the list.
        /// </summary>
        /// <remarks>
        /// Before the operation starts it checks whether list is loaded.
        /// If not, it first lazy-loads it.
        /// </remarks>
        /// <param name="item">Item being searched for.</param>
        /// <returns>Value indicating whether item was removed.</returns>
        public bool Remove(T item)
        {
            return _list.Remove(item);
        }

        #endregion

        #region IEnumerable<T> Members

        /// <summary>
        /// Returns a generic enumerator that iterates over the list.
        /// </summary>
        /// <remarks>
        /// Before the operation starts it checks whether list is loaded.
        /// If not, it first lazy-loads it.
        /// </remarks>
        /// <returns>Requested generic enumerator.</returns>
        public IEnumerator<T> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Returns an enumerator that iterates over the list.
        /// </summary>
        /// <remarks>
        /// Before the operation starts it checks whether list is loaded.
        /// If not, it first lazy-loads it.
        /// </remarks>
        /// <returns>Requested enumerator.</returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        #endregion

        private UInt32 m_TotalCount = 0;
        private IList<T> _list = new List<T>();
 
    }
}
