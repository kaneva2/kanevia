///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

/// this object is used for both top level navigation and sub navigation
/// 

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class SM_MenuItem
    {
        //used for both main and sub navigation
        private int _NavigationId = 0;
        private string _NavigationTitle = "";
        private int _PrivilegeId = 0;
        //used for main navigation only
        private int _WebsiteId = 0;
        //used for sub navigation only
        private int _UserControlId = 0;
        private int _ParentNavId = 0;

        public SM_MenuItem()
        {
        }

        /// <summary>
        /// Overloaded constructor for the User class.
        /// </summary>
        public SM_MenuItem(int NavigationId, string NavigationTitle, int PrivilegeId, int WebsiteId, int UserControlId, int ParentNavId)
        {
            this._NavigationId = NavigationId;
            this._NavigationTitle = NavigationTitle;
            this._PrivilegeId = PrivilegeId;
            this._WebsiteId = WebsiteId;
            this._UserControlId = UserControlId;
            this._ParentNavId = ParentNavId;
        }

        /// <summary>
        /// Gets or sets unique user identifier.
        /// The Identity Field Design Pattern. 
        /// </summary>
        /// 

        public int NavigationId
        {
            get { return _NavigationId; }
            set { _NavigationId = value; }
        }

        public string NavigationTitle
        {
            get { return _NavigationTitle; }
            set { _NavigationTitle = value; }
        }

        public int PrivilegeId
        {
            get { return _PrivilegeId; }
            set { _PrivilegeId = value; }
        }

        public int WebsiteId
        {
            get { return _WebsiteId; }
            set { _WebsiteId = value; }
        }

        public int UserControlId
        {
            get { return _UserControlId; }
            set { _UserControlId = value; }
        }

        public int ParentNavId
        {
            get { return _ParentNavId; }
            set { _ParentNavId = value; }
        }

    }


}
