///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class BlogComment
    {
        private int _BlogCommentId;
        private int _BlogId;
        private int _UserId;
        private string _Subject;
        private string _BodyText;
        private string _IpAddress;
        private DateTime _CreatedDate;
        private DateTime _LastUpdatedDate;
        private int _LastUpdatedUserId;
        private int _StatusId;

        private string _Username;
        private string _UpdatedUsername;

        public BlogComment() { }

        public BlogComment (int blogCommentId, int blogId, int userId, string subject, string bodyText, 
            string ipAddress, DateTime createdDate, DateTime lastUpdatedDate, int lastUpdatedUserId, int statusId,
            string username, string updatedUsername)
        {
            this._BlogCommentId = blogCommentId;
            this._BlogId = blogId;
            this._UserId = userId;
            this._Subject = subject;
            this._BodyText = bodyText;
            this._IpAddress = ipAddress;
            this._CreatedDate = createdDate;
            this._LastUpdatedDate = lastUpdatedDate;
            this._LastUpdatedUserId = lastUpdatedUserId;
            this._StatusId = statusId;

            this._Username = username;
            this._UpdatedUsername = updatedUsername;
        }
        
        public int BlogCommentId
        {
           get {return _BlogCommentId;}
           set {_BlogCommentId = value;}
        }

        public int BlogId
        {
           get {return _BlogId;}
           set {_BlogId = value;}
        }

        public int UserId
        {
           get {return _UserId;}
           set {_UserId = value;}
        }

        public string Subject
        {
           get {return _Subject;}
           set {_Subject = value;}
        }

        public string BodyText
        {
           get {return _BodyText;}
           set {_BodyText = value;}
        }

        public string IpAddress
        {
           get {return _IpAddress;}
           set {_IpAddress = value;}
        }

        public DateTime CreatedDate
        {
           get {return _CreatedDate;}
           set {_CreatedDate = value;}
        }

        public DateTime LastUpdatedDate
        {
           get {return _LastUpdatedDate;}
           set {_LastUpdatedDate = value;}
        }

        public int LastUpdatedUserId
        {
           get {return _LastUpdatedUserId;}
           set {_LastUpdatedUserId = value;}
        }

        public int StatusId
        {
           get {return _StatusId;}
           set {_StatusId = value;}
        }

        public string Username
        {
           get {return _Username;}
           set {_Username = value;}
        }

        public string UpdatedUsername
        {
           get {return _UpdatedUsername;}
           set {_UpdatedUsername = value;}
        }

    }
}
