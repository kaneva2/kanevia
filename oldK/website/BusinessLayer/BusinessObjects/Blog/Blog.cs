///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public enum BlogStatus
    {
        Active = 1,
        Deleted = 2,
        Locked = 3,
        New = 4
    }

    [Serializable]
    public class Blog
    {
        private int _BlogId = 0;
        private int _CommunityId = 0;
        private string _Subject = "";
        private string _BodyText = "";
        private int _CreatedUserId;
        private int _NumberOfComments = 0;
        private int _NumberOfViews = 0;
        private DateTime _CreatedDate;
        private DateTime _LastUpdatedDate;
        private int _LastUpdatedUserId;
        private string _IpAddress;
        private int _StatusId = (int) BlogStatus.New;
        private string _Keywords = "";

        private string _Username;
        private string _NameNoSpaces;

        public Blog() {}

        public Blog (int blogId, int communityId, string subject, string bodyText, int createdUserId, 
            int numberOfComments, int numberOfViews, DateTime createdDate, DateTime lastUpdatedDate, 
            int lastUpdatedUserId, string ipAddress, int statusId, string keywords, string username, string nameNoSpaces)
        {
            this._BlogId = blogId;
            this._CommunityId = communityId;
            this._Subject = subject;
            this._BodyText = bodyText;
            this._CreatedUserId = createdUserId;
            this._NumberOfComments = numberOfComments;
            this._NumberOfViews = numberOfViews;
            this._CreatedDate = createdDate;
            this._LastUpdatedDate = lastUpdatedDate;
            this._LastUpdatedUserId = lastUpdatedUserId;
            this._IpAddress = ipAddress;
            this._StatusId = statusId;
            this._Keywords = keywords;

            this._Username = username;
            this._NameNoSpaces = nameNoSpaces;
        }

        public int BlogId
        {
           get {return _BlogId;}
           set {_BlogId = value;}
        }

        public int CommunityId
        {
           get {return _CommunityId;}
           set {_CommunityId = value;}
        }

        public string Subject
        {
           get {return _Subject;}
           set {_Subject = value;}
        }

        public string BodyText
        {
           get {return _BodyText;}
           set {_BodyText = value;}
        }

        public int CreatedUserId
        {
           get {return _CreatedUserId;}
           set {_CreatedUserId = value;}
        }

        public int NumberOfComments
        {
           get {return _NumberOfComments;}
           set {_NumberOfComments = value;}
        }

        public int NumberOfViews
        {
           get {return _NumberOfViews;}
           set {_NumberOfViews = value;}
        }

        public DateTime CreatedDate
        {
           get {return _CreatedDate;}
           set {_CreatedDate = value;}
        }

        public DateTime LastUpdatedDate
        {
           get {return _LastUpdatedDate;}
           set {_LastUpdatedDate = value;}
        }

        public int LastUpdatedUserId
        {
           get {return _LastUpdatedUserId;}
           set {_LastUpdatedUserId = value;}
        }

        public string IpAddress
        {
           get {return _IpAddress;}
           set {_IpAddress = value;}
        }

        public int StatusId
        {
           get {return _StatusId;}
           set {_StatusId = value;}
        }

        public string Keywords
        {
           get {return _Keywords;}
           set {_Keywords = value;}
        }

        public string Username
        {
            get { return _Username; }
            set { _Username = value; }
        }

        public string NameNoSpaces
        {
            get { return _NameNoSpaces; }
            set { _NameNoSpaces = value; }
        }

    }
}
