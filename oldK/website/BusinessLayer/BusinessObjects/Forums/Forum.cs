///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public enum ForumStatus
    {
        ACTIVE = 1,
        DELETED = 2,
        LOCKED = 3,
        NEW = 4
    }

    [Serializable]
    public class Forum
    {

        private int forumId;
        private int forumCategoryId;
        private int communityId;
        private string forumName;
        private string description;
        private int numberOfTopics;
        private DateTime createdDate;
        private DateTime lastPostDate;
        private int lastTopicId;
        private int lastUserId;
        private int displayOrder;
        private int forumCategoryDisplayOrder;
        private int statusId;

        public Forum()
        {
            this.forumId = 0;
            this.forumCategoryId = 0;
            this.communityId = 0;
            this.forumName = "";
            this.description = "";
            this.numberOfTopics = 0;
            this.createdDate = DateTime.Now;
            this.lastPostDate = DateTime.Now;
            this.lastTopicId = 0;
            this.lastUserId = 0;
            this.displayOrder = 0;
            this.statusId = (int)ForumStatus.ACTIVE;
            this.forumCategoryDisplayOrder = 0;
        }

        public Forum(int forumId, int forumCategoryId, int communityId, string forumName, string description,
            int numberOfTopics, DateTime createdDate, DateTime lastPostDate,
            int lastTopicId, int lastUserId, int displayOrder, int statusId, int forumCategoryDisplayOrder)
        {
            this.forumId = forumId;
            this.forumCategoryId = forumCategoryId;
            this.communityId = communityId;
            this.forumName = forumName;
            this.description = description;
            this.numberOfTopics = numberOfTopics;
            this.createdDate = createdDate;
            this.lastPostDate = lastPostDate;
            this.lastTopicId = lastTopicId;
            this.lastUserId = lastUserId;
            this.displayOrder = displayOrder;
            this.statusId = statusId;
            this.forumCategoryDisplayOrder = forumCategoryDisplayOrder;
        }

        public int ForumId
        {
            get { return forumId; }
            set { forumId = value; }
        }

        public int ForumCategoryId
        {
            get { return forumCategoryId; }
            set { forumCategoryId = value; }
        }

        public int CommunityId
        {
            get { return communityId; }
            set { communityId = value; }
        }

        public string ForumName
        {
            get { return forumName; }
            set { forumName = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public int NumberOfTopics
        {
            get { return numberOfTopics; }
            set { numberOfTopics = value; }
        }

        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        public DateTime LastPostDate
        {
            get { return lastPostDate; }
            set { lastPostDate = value; }
        }

        public int LastTopicId
        {
            get { return lastTopicId; }
            set { lastTopicId = value; }
        }

        public int LastUserId
        {
            get { return lastUserId; }
            set { lastUserId = value; }
        }

        public int DisplayOrder
        {
            get { return displayOrder; }
            set { displayOrder = value; }
        }

        public int ForumCategoryDisplayOrder
        {
            get { return forumCategoryDisplayOrder; }
            set { forumCategoryDisplayOrder = value; }
        }

        public int StatusId
        {
            get { return statusId; }
            set { statusId = value; }
        }

    }
}
