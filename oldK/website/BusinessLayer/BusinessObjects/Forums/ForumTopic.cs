///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class ForumTopic
    {
        private string subject;
        private string createdUsername;
        private int topicId;
        private int forumId;
        private int communityId;
        private int createdUserId;
        private int numberOfReplies;
        private DateTime createdDate;
        private DateTime lastReplyDate;
        private DateTime lastUpdatedDate;
        private int lastUpdatedUserId;
        private int lastUserId;
        private string createdNameNoSpaces;
        private string createdThumbnailSmallPath;
        private string lastPostNameNoSpaces;
        private string lastUsername;
        private string lastThumbnailSmallPath;


        public ForumTopic()
        {
            this.subject = "";
            this.createdUsername = "";
            this.topicId = 0;
            this.forumId = 0;
            this.communityId = 0;
            this.createdUserId = 0;
            this.numberOfReplies = 0;
            this.createdDate = DateTime.Now;
            this.lastReplyDate = DateTime.Now;
            this.lastUpdatedDate = DateTime.Now;
            this.lastUpdatedUserId = 0;
            this.lastUserId = 0;
            this.createdNameNoSpaces = "";
            this.createdThumbnailSmallPath = "";
            this.lastPostNameNoSpaces = "";
            this.lastUsername = "";
            this.lastThumbnailSmallPath = "";
        }

        public ForumTopic(string subject, string createdUsername, int topicId, int forumId, int communityId,
         int createdUserId, int numberOfReplies, DateTime createdDate, DateTime lastReplyDate, DateTime lastUpdatedDate,
         int lastUpdatedUserId, int lastUserId, string createdNameNoSpaces, string createdThumbnailSmallPath, 
         string lastPostNameNoSpaces, string lastUsername, string lastThumbnailSmallPath)
        {
            this.subject = subject;
            this.createdUsername = createdUsername;
            this.topicId = topicId;
            this.forumId = forumId;
            this.communityId = communityId;
            this.createdUserId = createdUserId;
            this.numberOfReplies = numberOfReplies;
            this.createdDate = createdDate;
            this.lastReplyDate = lastReplyDate;
            this.lastUpdatedDate = lastUpdatedDate;
            this.lastUpdatedUserId = lastUpdatedUserId;
            this.createdUserId = createdUserId;
            this.lastUserId = lastUserId;
            this.createdNameNoSpaces = createdNameNoSpaces;
            this.createdThumbnailSmallPath = createdThumbnailSmallPath;
            this.lastPostNameNoSpaces = lastPostNameNoSpaces;
            this.createdNameNoSpaces = createdNameNoSpaces;
            this.lastUsername = lastUsername;
            this.lastThumbnailSmallPath = lastThumbnailSmallPath;
        }

        public string Subject
        {
            get { return subject; }
            set { subject = value; }
        }

        public string CreatedUsername
        {
            get { return createdUsername; }
            set { createdUsername = value; }
        }

        public int TopicId
        {
            get { return topicId; }
            set { topicId = value; }
        }

        public int ForumId
        {
            get { return forumId; }
            set { forumId = value; }
        }

        public int CommunityId
        {
            get { return communityId; }
            set { communityId = value; }
        }

        public int CreatedUserId
        {
            get { return createdUserId; }
            set { createdUserId = value; }
        }

        public int NumberOfReplies
        {
            get { return numberOfReplies; }
            set { numberOfReplies = value; }
        }

        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        public DateTime LastReplyDate
        {
            get { return lastReplyDate; }
            set { lastReplyDate = value; }
        }

        public DateTime LastUpdatedDate
        {
            get { return lastUpdatedDate; }
            set { lastUpdatedDate = value; }
        }

        public int LastUpdatedUserId
        {
            get { return lastUpdatedUserId; }
            set { lastUpdatedUserId = value; }
        }

        public int LastUserId
        {
            get { return lastUserId; }
            set { lastUserId = value; }
        }

        public string CreatedNameNoSpaces
        {
            get { return createdNameNoSpaces; }
            set { createdNameNoSpaces = value; }
        }

        public string CreatedThumbnailSmallPath
        {
            get { return createdThumbnailSmallPath; }
            set { createdThumbnailSmallPath = value; }
        }

        public string LastPostNameNoSpaces
        {
            get { return lastPostNameNoSpaces; }
            set { lastPostNameNoSpaces = value; }
        }

        public string LastUsername
        {
            get { return lastUsername; }
            set { lastUsername = value; }
        }

        public string LastThumbnailSmallPath
        {
            get { return lastThumbnailSmallPath; }
            set { lastThumbnailSmallPath = value; }
        }

    }
}
