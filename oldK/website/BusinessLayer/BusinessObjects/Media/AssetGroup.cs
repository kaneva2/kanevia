///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Runtime.Serialization;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class AssetGroup
    {
        private int _AssetGroupId = 0;
        private string _Name = "";
        private DateTime _CreatedDate;
        private string _LastUpdated;
        private int _ChannelId = 0;
        private UInt32 _AssetCount = 0;
        private string _Description = "";
        private int _GroupCategoryId = 0;
        private int _GroupCategorySortOrder = 0;
        private int _shuffle = 0;

        public AssetGroup() {}

        public AssetGroup(int assetGroupId, string name, DateTime createdDate, string lastUpdated, int channelId, UInt32 assetCount, string description)
            : this(assetGroupId, name, createdDate, lastUpdated, channelId, assetCount, description, 0, 0, 0)
        { }

        public AssetGroup (int assetGroupId, string name, DateTime createdDate, string lastUpdated, int channelId, UInt32 assetCount, string description, int groupCategoryId, int groupCategorySortOrder, int shuffle)
        {
            this._AssetGroupId = assetGroupId;
            this._Name = name;
            this._CreatedDate = createdDate;
            this._LastUpdated = lastUpdated;
            this._ChannelId = channelId;
            this._AssetCount = assetCount;
            this._Description = description;
            this._GroupCategoryId = groupCategoryId;
            this._GroupCategorySortOrder = groupCategorySortOrder;
            this._shuffle = shuffle;
        }

        [DataMember]
        public int AssetGroupId
        {
            get { return _AssetGroupId; }
            set { _AssetGroupId = value; }
        }

        [DataMember]
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        [DataMember]
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        [DataMember]
        public string LastUpdated
        {
            get { return _LastUpdated; }
            set { _LastUpdated = value; }
        }

        [DataMember]
        public int ChannelId
        {
            get { return _ChannelId; }
            set { _ChannelId = value; }
        }

        [DataMember]
        public UInt32 AssetCount
        {
            get { return _AssetCount; }
            set { _AssetCount = value; }
        }

        [DataMember]
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        [DataMember]
        public int GroupCategoryId
        {
            get { return _GroupCategoryId; }
            set { _GroupCategoryId = value; }
        }

        [DataMember]
        public int GroupCategorySortOrder
        {
            get { return _GroupCategorySortOrder; }
            set { _GroupCategorySortOrder = value; }
        }

        [DataMember]
        public int Shuffle
        {
            get { return _shuffle; }
            set { _shuffle = value; }
        }
    }

}
