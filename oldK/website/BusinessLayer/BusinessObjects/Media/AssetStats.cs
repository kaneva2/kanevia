///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class AssetStats
    {
        private int _AssetId = 0;
        private int _NumberOfComments = 0;
        private int _NumberOfDownloads = 0;
        private UInt32 _NumberOfShares = 0;
        private UInt32 _NumberOfChannels = 0;
        private UInt32 _NumberOfDiggs = 0;

        public AssetStats () { }

        public AssetStats (int assetId, int numberOfComments, int numberOfDownloads,
            UInt32 numberOfShares, UInt32 numberOfChannels, UInt32 numberOfDiggs)
        {
            this._AssetId = assetId;
            this._NumberOfComments = numberOfComments;
            this._NumberOfDownloads = numberOfDownloads;
            this._NumberOfShares = numberOfShares;
            this._NumberOfChannels = numberOfChannels;
            this._NumberOfDiggs = numberOfDiggs;
        }

        public int AssetId
        {
           get {return _AssetId;}
           set {_AssetId = value;}
        }

        public int NumberOfComments
        {
           get {return _NumberOfComments;}
           set {_NumberOfComments = value;}
        }

        public int NumberOfDownloads
        {
           get {return _NumberOfDownloads;}
           set {_NumberOfDownloads = value;}
        }

        public UInt32 NumberOfShares
        {
           get {return _NumberOfShares;}
           set {_NumberOfShares = value;}
        }

        public UInt32 NumberOfChannels
        {
           get {return _NumberOfChannels;}
           set {_NumberOfChannels = value;}
        }

        public UInt32 NumberOfDiggs
        {
           get {return _NumberOfDiggs;}
           set {_NumberOfDiggs = value;}
        }
    }
}
