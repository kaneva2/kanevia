///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Runtime.Serialization;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class AssetGroupAsset
    {
        [DataMember]
        public int AssetId {get; set;}
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string AssetOffsiteId { get; set; }
        [DataMember]
        public int AssetTypeId { get; set; }
        [DataMember]
        public int AssetSubTypeId {get; set;}
        [DataMember]
        public int RunTimeSeconds {get; set;}
        [DataMember]
        public string ThumbnailMediumPath {get; set;}
        [DataMember]
        public bool Mature {get; set;}
        [DataMember]
        public bool Public {get; set;}
        [DataMember]
        public bool Over21Required {get; set;}
        [DataMember]
        public int NumberOfDiggs {get; set;}
        [DataMember]
        public string MediaPath {get; set;}
        [DataMember]
        public int SortOrder { get; set; }

        public AssetGroupAsset(int assetId, string name, string assetOffSiteId, int assetTypeId, int assetSubtypeId,
            int runTimeSeconds, string thumbnailMediumPath, bool mature, bool pubic, bool over21Required, int numberOfDiggs, string mediaPath, int sortOrder)
        {
            AssetId = assetId;
            Name = name;
            AssetOffsiteId = assetOffSiteId;
            AssetTypeId = assetTypeId;
            AssetSubTypeId = assetSubtypeId;
            RunTimeSeconds = runTimeSeconds;
            ThumbnailMediumPath = thumbnailMediumPath;
            Mature = mature;
            Public = pubic;
            Over21Required = over21Required;
            NumberOfDiggs = numberOfDiggs;
            MediaPath = mediaPath;
            SortOrder = sortOrder;
        }

    }
}
