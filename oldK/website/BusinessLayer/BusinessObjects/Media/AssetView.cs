///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class AssetView
    {
        private UInt32 _ViewId = 0;
        private UInt32 _UserId = 0;
        private UInt32 _AssetId = 0;
        private DateTime _CreatedDate;
        private string _IpAddress;
        private string _Name = "";
        private int _AssetRatingId;
        private string _ThumbnailSmallPath = "";
        private int _ThumbnailGen = 0;
        private int _AssetTypeId = 0;

        public AssetView(UInt32 viewId, UInt32 userId, UInt32 assetId, DateTime createdDate, string ipAddress,
            string name, int assetRatingId, string thumbnailSmallPath, int thumbnailGen, int assetTypeId)
        {
            this._ViewId = viewId;
            this._UserId = userId;
            this._AssetId = assetId;
            this._CreatedDate = createdDate;
            this._IpAddress = ipAddress;
            this._Name = name;
            this._AssetRatingId = assetRatingId;
            this._ThumbnailSmallPath = thumbnailSmallPath;
            this._ThumbnailGen = thumbnailGen;
            this._AssetTypeId = assetTypeId;
        }

        public UInt32 ViewId
        {
            get { return _ViewId; }
            set { _ViewId = value; }
        }

        public UInt32 UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public UInt32 AssetId
        {
            get { return _AssetId; }
            set { _AssetId = value; }
        }

        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        public string IpAddress
        {
            get { return _IpAddress; }
            set { _IpAddress = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        
        public int AssetRatingId
        {
            get { return _AssetRatingId; }
            set { _AssetRatingId = value; }
        }


        public string ThumbnailSmallPath
        {
            get { return _ThumbnailSmallPath; }
            set { _ThumbnailSmallPath = value; }
        }

        
        public int ThumbnailGen
        {
            get { return _ThumbnailGen; }
            set { _ThumbnailGen = value; }
        }

        public int AssetTypeId
        {
            get { return _AssetTypeId; }
            set { _AssetTypeId = value; }
        }

    }
}