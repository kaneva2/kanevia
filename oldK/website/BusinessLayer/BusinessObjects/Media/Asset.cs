///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class Asset
    {
        public enum eASSET_TYPES
        {
            ALL = 0,
            GAME = 1,
            VIDEO = 2,
            ASSET = 3,
            MUSIC = 4,
            PICTURE = 5,
            PATTERN = 6,
            TV = 7,
            WIDGET = 8
        }

        public enum eASSET_STATUS
        {
            ACTIVE = 1,
            DELETED = 2,
            NEW = 3,
            MARKED_FOR_DELETION = 4
        }

        public enum eASSET_RATING
        {
            GENERAL = 1,
            TEEN = 2,
            MATURE = 3
        }
        
        private int _AssetId = 0;
        private int _AssetTypeId = (int)eASSET_TYPES.PICTURE;
        private string _IsKanevaGame = "N";
        private int _AssetSubTypeId = 0;
        private int _AssetRatingId = 0;
        private int _OwnerId = 0;
        private UInt64 _FileSize = 0;
        private int _RunTimeSeconds = 0;
        private int _Category1Id;
        private int _Category2Id;
        private int _Category3Id;
        private Double _Amount = 0;
        private string _KeiPointId;
        private string _ImagePath;
        private string _ImageFullPath;
        private string _ThumbnailAssetdetailsPath;
        private string _ThumbnailXlargePath;
        private string _ThumbnailLargePath;
        private string _ThumbnailMediumPath;
        private string _ThumbnailSmallPath;
        private int _ThumbnailGen;
        private string _ImageCaption;
        private int _StatusId;
        private DateTime _CreatedDate;
        private DateTime _LastUpdatedDate;
        private int _LastUpdatedUserId = 0;
        private string _LicenseCc;
        private string _LicenseURL;
        private string _LicenseType;
        private string _LicenseName;
        private string _LicenseAdditional;
        private int _PublishStatusId;
        private int _PercentComplete;
        private string _IpAddress;
        private string _GameEncryptionKey;
        private string _RequireLogin;
        private string _ContentExtension;
        private string _TargetDir;
        private string _MediaPath;
        private int _Permission = 0;
        private int _PermissionGroup = 0;
        private bool _Published = false;
        private bool _Mature = true;
        private bool _Public = false;
        private bool _Playlistable = false;
        private bool _Over21Required = false;
        private string _AssetOffsiteId = "";
        private UInt32 _SortOrder;
        private string _Name;
        private string _BodyText;
        private string _ShortDescription;
        private string _Teaser;
        private string _OwnerUsername = "";
        private string _Keywords;

        // Extras
        private string _Categories = "";
        private string _OwnerZipCode = "";
        private int _DiggId = 0;
        private AssetStats _assetStats;

        public Asset() { }

        public Asset(int assetId, int assetTypeId, string isKanevaGame, int assetSubTypeId,
            int assetRatingId, int ownerId, UInt64 fileSize, int runTimeSeconds,
            int category1Id, int category2Id, int category3Id, Double amount,
            string keiPointId, string imagePath, string imageFullPath, string thumbnailAssetdetailsPath,
            string thumbnailXlargePath, string thumbnailLargePath, string thumbnailMediumPath,
            string thumbnailSmallPath, int thumbnailGen, string imageCaption, int statusId, DateTime createdDate,
            DateTime lastUpdatedDate, int lastUpdatedUserId,
            string licenseCc, string licenseURL, string licenseType, string licenseName, string licenseAdditional,
            int publishStatusId, int percentComplete, string ipAddress, string gameEncryptionKey,
            string requireLogin,
            string contentExtension, string targetDir, string mediaPath, int permission, int permissionGroup,
            bool published, bool mature, bool isPublic, bool playlistable,
            bool over21Required, string assetOffsiteId, UInt32 sortOrder,
            string name, string bodyText, string shortDescription, string teaser, string ownerUsername,
            string keywords, string categories, string ownerZipCode, int diggId)
        {
            this._AssetId = assetId;
            this._AssetTypeId = assetTypeId;
            this._IsKanevaGame = isKanevaGame;
            this._AssetSubTypeId = assetSubTypeId;
            this._AssetRatingId = assetRatingId;
            this._OwnerId = ownerId;
            this._FileSize = fileSize;
            this._RunTimeSeconds = runTimeSeconds;
            this._Category1Id = category1Id;
            this._Category2Id = category2Id;
            this._Category3Id = category3Id;
            this._Amount = amount;
            this._KeiPointId = keiPointId;
            this._ImagePath = imagePath;
            this._ImageFullPath = imageFullPath;
            this._ThumbnailAssetdetailsPath = thumbnailAssetdetailsPath;
            this._ThumbnailXlargePath = thumbnailXlargePath;
            this._ThumbnailLargePath = thumbnailLargePath;
            this._ThumbnailMediumPath = thumbnailMediumPath;
            this._ThumbnailSmallPath = thumbnailSmallPath;
            this._ThumbnailGen = thumbnailGen;
            this._ImageCaption = imageCaption;
            this._StatusId = statusId;
            this._CreatedDate = createdDate;
            this._LastUpdatedDate = lastUpdatedDate;
            this._LastUpdatedUserId = lastUpdatedUserId;
            this._LicenseCc = licenseCc;
            this._LicenseURL = licenseURL;
            this._LicenseType = licenseType;
            this._LicenseName = licenseName;
            this._LicenseAdditional = licenseAdditional;
            this._PublishStatusId = publishStatusId;
            this._PercentComplete = percentComplete;
            this._IpAddress = ipAddress;
            this._GameEncryptionKey = gameEncryptionKey;
            this._RequireLogin = requireLogin;
            this._ContentExtension = contentExtension;
            this._TargetDir = targetDir;
            this._MediaPath = mediaPath;
            this._Permission = permission;
            this._PermissionGroup = permissionGroup;
            this._Published = published;
            this._Mature = mature;
            this._Public = isPublic;
            this._Playlistable = playlistable;
            this._Over21Required = over21Required;
            this._AssetOffsiteId = assetOffsiteId;
            this._SortOrder = sortOrder;
            this._Name = name;
            this._BodyText = bodyText;
            this._ShortDescription = shortDescription;
            this._Teaser = teaser;
            this._OwnerUsername = ownerUsername;
            this._Keywords = keywords;

            // Extra
            this._Categories = categories;
            this._OwnerZipCode = ownerZipCode;
            this._DiggId = diggId;
        }

        public int AssetId
        {
            get { return _AssetId; }
            set { _AssetId = value; }
        }

        public int AssetTypeId
        {
            get { return _AssetTypeId; }
            set { _AssetTypeId = value; }
        }

        public string IsKanevaGame
        {
            get { return _IsKanevaGame; }
            set { _IsKanevaGame = value; }
        }

        public int AssetSubTypeId
        {
            get { return _AssetSubTypeId; }
            set { _AssetSubTypeId = value; }
        }

        public int AssetRatingId
        {
            get { return _AssetRatingId; }
            set { _AssetRatingId = value; }
        }

        public int OwnerId
        {
            get { return _OwnerId; }
            set { _OwnerId = value; }
        }

        public UInt64 FileSize
        {
            get { return _FileSize; }
            set { _FileSize = value; }
        }

        public int RunTimeSeconds
        {
            get { return _RunTimeSeconds; }
            set { _RunTimeSeconds = value; }
        }

        public int Category1Id
        {
            get { return _Category1Id; }
            set { _Category1Id = value; }
        }

        public int Category2Id
        {
            get { return _Category2Id; }
            set { _Category2Id = value; }
        }

        public int Category3Id
        {
            get { return _Category3Id; }
            set { _Category3Id = value; }
        }

        public Double Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public string KeiPointId
        {
            get { return _KeiPointId; }
            set { _KeiPointId = value; }
        }

        public string ImagePath
        {
            get { return _ImagePath; }
            set { _ImagePath = value; }
        }

        public string ImageFullPath
        {
            get { return _ImageFullPath; }
            set { _ImageFullPath = value; }
        }

        public string ThumbnailAssetdetailsPath
        {
            get { return _ThumbnailAssetdetailsPath; }
            set { _ThumbnailAssetdetailsPath = value; }
        }

        public string ThumbnailXlargePath
        {
            get { return _ThumbnailXlargePath; }
            set { _ThumbnailXlargePath = value; }
        }

        public string ThumbnailLargePath
        {
            get { return _ThumbnailLargePath; }
            set { _ThumbnailLargePath = value; }
        }

        public string ThumbnailMediumPath
        {
            get { return _ThumbnailMediumPath; }
            set { _ThumbnailMediumPath = value; }
        }

        public string ThumbnailSmallPath
        {
            get { return _ThumbnailSmallPath; }
            set { _ThumbnailSmallPath = value; }
        }

        public int ThumbnailGen
        {
            get { return _ThumbnailGen; }
            set { _ThumbnailGen = value; }
        }

        public string ImageCaption
        {
            get { return _ImageCaption; }
            set { _ImageCaption = value; }
        }

        public int StatusId
        {
            get { return _StatusId; }
            set { _StatusId = value; }
        }

        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        public DateTime LastUpdatedDate
        {
            get { return _LastUpdatedDate; }
            set { _LastUpdatedDate = value; }
        }

        public int LastUpdatedUserId
        {
            get { return _LastUpdatedUserId; }
            set { _LastUpdatedUserId = value; }
        }

        public string LicenseCc
        {
            get { return _LicenseCc; }
            set { _LicenseCc = value; }
        }

        public string LicenseURL
        {
            get { return _LicenseURL; }
            set { _LicenseURL = value; }
        }

        public string LicenseType
        {
            get { return _LicenseType; }
            set { _LicenseType = value; }
        }

        public string LicenseName
        {
            get { return _LicenseName; }
            set { _LicenseName = value; }
        }

        public string LicenseAdditional
        {
            get { return _LicenseAdditional; }
            set { _LicenseAdditional = value; }
        }

        public int PublishStatusId
        {
            get { return _PublishStatusId; }
            set { _PublishStatusId = value; }
        }

        public int PercentComplete
        {
            get { return _PercentComplete; }
            set { _PercentComplete = value; }
        }

        public string IpAddress
        {
            get { return _IpAddress; }
            set { _IpAddress = value; }
        }

        public string GameEncryptionKey
        {
            get { return _GameEncryptionKey; }
            set { _GameEncryptionKey = value; }
        }

        public string RequireLogin
        {
            get { return _RequireLogin; }
            set { _RequireLogin = value; }
        }

        public string ContentExtension
        {
            get { return _ContentExtension; }
            set { _ContentExtension = value; }
        }

        public string TargetDir
        {
            get { return _TargetDir; }
            set { _TargetDir = value; }
        }

        public string MediaPath
        {
            get { return _MediaPath; }
            set { _MediaPath = value; }
        }

        public int Permission
        {
            get { return _Permission; }
            set { _Permission = value; }
        }

        public int PermissionGroup
        {
            get { return _PermissionGroup; }
            set { _PermissionGroup = value; }
        }

        public bool Published
        {
            get { return _Published; }
            set { _Published = value; }
        }

        public bool Mature
        {
            get { return _Mature; }
            set { _Mature = value; }
        }

        public bool Public
        {
            get { return _Public; }
            set { _Public = value; }
        }

        public bool Playlistable
        {
            get { return _Playlistable; }
            set { _Playlistable = value; }
        }

        public bool Over21Required
        {
            get { return _Over21Required; }
            set { _Over21Required = value; }
        }

        public string AssetOffsiteId
        {
            get { return _AssetOffsiteId; }
            set { _AssetOffsiteId = value; }
        }

        public UInt32 SortOrder
        {
            get { return _SortOrder; }
            set { _SortOrder = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string BodyText
        {
            get { return _BodyText; }
            set { _BodyText = value; }
        }

        public string ShortDescription
        {
            get { return _ShortDescription; }
            set { _ShortDescription = value; }
        }

        public string Teaser
        {
            get { return _Teaser; }
            set { _Teaser = value; }
        }

        public string OwnerUsername
        {
            get { return _OwnerUsername; }
            set { _OwnerUsername = value; }
        }

        public string Keywords
        {
            get { return _Keywords; }
            set { _Keywords = value; }
        }

        // Extra
        public string Categories
        {
            get { return _Categories; }
            set { _Categories = value; }
        }

        public string OwnerZipCode
        {
            get { return _OwnerZipCode; }
            set { _OwnerZipCode = value; }
        }

        public int DiggId
        {
            get { return _DiggId; }
            set { _DiggId = value; }
        }

        public AssetStats Stats
        {
            get { return _assetStats; }
            set { _assetStats = value; }
        }

    }
}
