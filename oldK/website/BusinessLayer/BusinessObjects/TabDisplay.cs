///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{

    [Serializable]
    public class TabDisplay
    {
        private int count;
        private int tabId;
        private string tabName;
        private string controlToLoad;
        private string ajaxDataPage;
        private string ajaxDataPageParams;
        private string containerToUse;
        private bool allowAlphaSorting;
		private bool allowDateSorting;
        private bool allowSorting;


        public TabDisplay()
        {
            this.count = 0;
            this.tabId = 0;
            this.tabName = "";
            this.controlToLoad = "";
            this.ajaxDataPage = "";
            this.ajaxDataPageParams = "";
            this.containerToUse = "";
            this.allowAlphaSorting = false;
			this.allowDateSorting = false;
            this.allowSorting = false;
        }

        /// <summary>
        /// Overloaded constructor for the TabDisplay class.
        /// </summary>
        public TabDisplay(int count, int tabId, string tabName, string controlToLoad, string containerToUse)
        {
            this.count = count;
            this.tabId = tabId;
            this.tabName = tabName;
            this.controlToLoad = controlToLoad;
            this.containerToUse = containerToUse;
            this.ajaxDataPage = "";
            this.ajaxDataPageParams = "";
            this.allowAlphaSorting = false;
			this.allowDateSorting = false;
            this.allowSorting = false;
        }

        public int Count
        {
            get { return count; }
            set { count = value; }
        }

        public int TabId
        {
            get { return tabId; }
            set { tabId = value; }
        }

        public string TabName
        {
            get { return tabName; }
            set { tabName = value; }
        }

        public string ControlToLoad
        {
            get { return controlToLoad; }
            set { controlToLoad = value; }
        }

        public string ContainerToUse
        {
            get { return containerToUse; }
            set { containerToUse = value; }
        }

        public string AjaxDataPage
        {
            get
            {
                return this.ajaxDataPage;
            }
            set
            {
                this.ajaxDataPage = value;
            }
        }

        public string AjaxDataPageParams
        {
            get
            {
                return this.ajaxDataPageParams;
            }
            set
            {
                this.ajaxDataPageParams = value;
            }
        }

        public bool AllowSorting
        {
            get
            {
                return this.allowSorting;
            }
            set
            {
                this.allowSorting = value;
            }
        }

        public bool AllowAlphaSorting
        {
            get
            {
                return this.allowAlphaSorting;
            }
            set
            {
                this.allowAlphaSorting = value;
            }
        }

		public bool AllowDateSorting
		{
			get
			{
				return this.allowDateSorting;
			}
			set
			{
				this.allowDateSorting = value;
			}
		}

    }
}
