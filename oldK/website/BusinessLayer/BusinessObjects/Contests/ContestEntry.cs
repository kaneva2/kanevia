///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public enum eCONTEST_ENTRY_STATUS
    {
        Inactive = 0,                   // 0 entry is not active
        Active = 1,                     // 1 entry is active
        Disabled = 2                    // 2 entry has been disabled
    }
    
    [Serializable]
    public class ContestEntry
    {
        private UInt64 _ContestEntryId = 0;
        private UInt64 _ContestId = 0;
        private ContestUser _Owner = new ContestUser ();
        private DateTime _CreatedDate = DateTime.MinValue;
        private DateTime _CurrentDate = DateTime.MinValue;
        private string _Description = "";
        private int _NumVotes = 0;
        private int _NumComments = 0;
        private int _Status = (int) eCONTEST_ENTRY_STATUS.Inactive;
        
        public ContestEntry () { }

        public ContestEntry (UInt64 contestEntryId, UInt64 contestId, DateTime createdDate, 
            string description, int numVotes, int numComments, int ownerId, string owner_username, string gender,
            string thumbnailSmallPath, string thumbnailMediumPath, string thumbnailLargePath, string thumbnailSquarePath,
            DateTime currentDate, int status)
        {
            this._ContestEntryId = contestEntryId;
            this._ContestId = contestId;
            this._CreatedDate = createdDate;
            this._Description = description;
            this._NumVotes = numVotes;
            this._NumComments = numComments;
            this._Owner.UserId = ownerId;
            this._Owner.Username = owner_username;
            this._Owner.Gender = gender;
            this._Owner.ThumbnailSmallPath = thumbnailSmallPath;
            this._Owner.ThumbnailMediumPath = thumbnailMediumPath;
            this._Owner.ThumbnailLargePath = thumbnailLargePath;
            this._Owner.ThumbnailSquarePath = thumbnailSquarePath;
            this._CurrentDate = currentDate;
            this._Status = status;
        }

        public UInt64 ContestEntryId
        {
            get { return _ContestEntryId; }
            set { _ContestEntryId = value; }
        }

        public UInt64 ContestId
        {
            get { return _ContestId; }
            set { _ContestId = value; }
        }

        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        public DateTime CurrentDate
        {
            get { return _CurrentDate; }
            set { _CurrentDate = value; }
        }

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public int NumberOfVotes
        {
            get { return _NumVotes; }
            set { _NumVotes = value; }
        }

        public int NumberOfComments
        {
            get { return _NumComments; }
            set { _NumComments = value; }
        }

        public ContestUser Owner
        {
            get { return _Owner; }
            set { _Owner = value; }
        }

        public int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

    }
}
