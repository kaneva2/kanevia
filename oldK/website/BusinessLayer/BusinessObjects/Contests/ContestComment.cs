///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class ContestComment
    {
        private UInt64 _CommentId = 0;
        private UInt64 _ContestEntryId = 0;
        private UInt64 _ContestId = 0;
        private int _ContestOwnerUserId = 0;
        private ContestUser _Creator = new ContestUser ();
        private DateTime _CreatedDate = DateTime.MinValue;
        private DateTime _CurrentDate = DateTime.MinValue;
        private string _Comment = "";
        private int _NumComments = 0;

        public ContestComment () { }

        public ContestComment (UInt64 commentId, UInt64 contestId, int contestOwnerUserId, UInt64 contestEntryId, DateTime createdDate, 
            string comment, int numComments, int ownerId, string owner_username, string gender,
            string thumbnailSmallPath, string thumbnailMediumPath, string thumbnailLargePath, 
            string thumbnailSquarePath, DateTime currentDate)
        {
            this._CommentId = commentId;
            this._ContestEntryId = contestEntryId;
            this._ContestId = contestId;
            this._ContestOwnerUserId = contestOwnerUserId;
            this._CreatedDate = createdDate;
            this._Comment = comment;
            this._NumComments = numComments;
            this._Creator.UserId = ownerId;
            this._Creator.Username = owner_username;
            this._Creator.Gender = gender;
            this._Creator.ThumbnailSmallPath = thumbnailSmallPath;
            this._Creator.ThumbnailMediumPath = thumbnailMediumPath;
            this._Creator.ThumbnailLargePath = thumbnailLargePath;
            this._Creator.ThumbnailSquarePath = thumbnailSquarePath;
            this._CurrentDate = currentDate;
        }

        public UInt64 CommentId
        {
            get { return _CommentId; }
            set { _CommentId = value; }
        }

        public UInt64 ContestEntryId
        {
            get { return _ContestEntryId; }
            set { _ContestEntryId = value; }
        }

        public UInt64 ContestId
        {
            get { return _ContestId; }
            set { _ContestId = value; }
        }

        public int ContestOwnerUserId
        {
            get { return _ContestOwnerUserId; }
            set { _ContestOwnerUserId = value; }
        }

        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        public DateTime CurrentDate
        {
            get { return _CurrentDate; }
            set { _CurrentDate = value; }
        }

        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        public int NumberOfComments
        {
            get { return _NumComments; }
            set { _NumComments = value; }
        }

        public ContestUser Creator
        {
            get { return _Creator; }
            set { _Creator = value; }
        }
    }
}
