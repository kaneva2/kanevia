///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class ContestUser
    {
        private int _UserId = 0;
        private string _Username = "";
        private string _ThumbnailSmallPath = "";
        private string _ThumbnailMediumPath = "";
        private string _ThumbnailLargePath = "";
        private string _ThumbnailSquarePath = "";
        private string _Gender = "M";

        private FacebookSettings _facebookSettings = new FacebookSettings ();

        public ContestUser () { }

        public ContestUser (int userId, string username, string gender,
            string thumbnailSmallPath, string thumbnailMediumPath, string thumbnailLargePath, string thumbnailSquarePath)
        {
            this._UserId = userId;
            this._Username = username;
            this._Gender = gender;
            this._ThumbnailSmallPath = thumbnailSmallPath;
            this._ThumbnailMediumPath = thumbnailMediumPath;
            this._ThumbnailLargePath = thumbnailLargePath;
            this._ThumbnailSquarePath = thumbnailSquarePath;
        }

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public string Username
        {
            get { return _Username; }
            set { _Username = value; }
        }

        public string Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }

        public string ThumbnailSmallPath
        {
            get { return _ThumbnailSmallPath; }
            set { _ThumbnailSmallPath = value; }
        }

        public string ThumbnailMediumPath
        {
            get { return _ThumbnailMediumPath; }
            set { _ThumbnailMediumPath = value; }
        }

        public string ThumbnailLargePath
        {
            get { return _ThumbnailLargePath; }
            set { _ThumbnailLargePath = value; }
        }

        public string ThumbnailSquarePath
        {
            get { return _ThumbnailSquarePath; }
            set { _ThumbnailSquarePath = value; }
        }

        public FacebookSettings FacebookSettings
        {
            get { return _facebookSettings; }
            set { _facebookSettings = value; }
        }
    }
}
