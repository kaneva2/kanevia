///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public enum eCONTEST_STATUS
    {
        Inactive = 0,                   // 0 contest is not active
        Active = 1,                     // 1 contest is active
        Ended_Waiting_For_Winner = 2,   // 2 contest ended, waiting on winner to be selected
        Ended_Winner_Selected = 3,      // 3 contest ended, winner has been selected 
        Ended_Winner_Paid = 4,          // 4 contest ended, winner paid
        Ended_Owner_Refunded = 5,       // 5 contest ended, no winner, owner refunded
        Deleted = 6                     // 6 contest had been deleted
    }   

    public enum eCONTEST_MSG_TYPES
    {
        Created = 1,
        Entered = 2,
        Notify_Owner_Contest_Ended = 3,
        Notify_Owner_Contest_Ended_Less_Than_Min_Entries = 4,
        Winner_Picked = 5,
        You_Won = 6,
        Voted_On_Winner = 7,
        Entered_Did_Not_Win = 8,
        Ended_No_Winner = 9,
        Following_Ended_Winner = 10
    }

    [Serializable]
    public class Contest
    {
        private UInt64 _ContestId = 0;
        private ContestUser _Owner = new ContestUser ();
        private DateTime _CreatedDate = DateTime.MinValue;
        private DateTime _EndDate = DateTime.MinValue;
        private DateTime _CurrentDate = DateTime.Now;
        private string _Title = "";
        private string _Description = "";
        private int _PrizeAmount = 0;
        private bool _IsContestActive = false;
        private int _Status = (int) eCONTEST_STATUS.Inactive;
        private int _NumEntries = 0;
        private int _NumVotes = 0;
        private int _NumComments = 0;
        private UInt64 _PopularVoteWinnerEntryId = 0;
        private UInt64 _OwnerVoteWinnerEntryId = 0;
        private IList<ContestEntry> _ContestEntries;

        public Contest () { }

        public Contest (UInt64 contestId, DateTime createdDate, DateTime endDate, string title, string description, 
            int prizeAmount, bool isActive, int status, int numEntries, int numVotes, int numComments,
            UInt64 popularVoteWinnerEntryId, UInt64 ownerVoteWinnerEntryId, DateTime currentDate)
        {
            this._ContestId = contestId;
            this._CreatedDate = createdDate;
            this._Description = description;
            this._EndDate = endDate;
            this._IsContestActive = isActive;
            this._Status = status;
            this._NumEntries = numEntries;
            this._NumVotes = numVotes;
            this._NumComments = numComments;
            this._OwnerVoteWinnerEntryId = ownerVoteWinnerEntryId;
            this._PopularVoteWinnerEntryId = popularVoteWinnerEntryId;
            this._PrizeAmount = prizeAmount;
            this._Title = title;
            this._CurrentDate = currentDate;
        }

        public Contest (UInt64 contestId, DateTime createdDate, DateTime endDate, string title, string description, int prizeAmount,
            bool isActive, int status, int numEntries, int numVotes, int numComments, int ownerId, string owner_username, string gender, string thumbnailSmallPath,
            string thumbnailMediumPath, string thumbnailLargePath, string thumbnailSquarePath, UInt64 popularVoteWinnerEntryId, UInt64 ownerVoteWinnerEntryId, DateTime currentDate)
        {
            this._ContestId = contestId;
            this._CreatedDate = createdDate;
            this._Description = description;
            this._EndDate = endDate;
            this._IsContestActive = isActive;
            this._Status = status;
            this._NumEntries = numEntries;
            this._NumVotes = numVotes;
            this._NumComments = numComments;
            this._OwnerVoteWinnerEntryId = ownerVoteWinnerEntryId;
            this._PopularVoteWinnerEntryId = popularVoteWinnerEntryId;
            this._PrizeAmount = prizeAmount;
            this._Title = title;
            this._Owner.UserId = ownerId;
            this._Owner.Username = owner_username;
            this._Owner.Gender = gender;
            this._Owner.ThumbnailSmallPath = thumbnailSmallPath;
            this._Owner.ThumbnailMediumPath = thumbnailMediumPath;
            this._Owner.ThumbnailLargePath = thumbnailLargePath;
            this._Owner.ThumbnailSquarePath = thumbnailSquarePath;
            this._CurrentDate = currentDate;
        }

        public UInt64 ContestId
        {
            get { return _ContestId; }
            set { _ContestId = value; }
        }

        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        public DateTime CurrentDate
        {
            get { return _CurrentDate; }
            set { _CurrentDate = value; }
        }

        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public int PrizeAmount
        {
            get { return _PrizeAmount; }
            set { _PrizeAmount = value; }
        }

        public bool IsContestActive
        {
            get { return _IsContestActive; }
            set { _IsContestActive = value; }
        }

        public int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        public int NumberOfEntries
        {
            get { return _NumEntries; }
            set { _NumEntries = value; }
        }

        public int NumberOfVotes
        {
            get { return _NumVotes; }
            set { _NumVotes = value; }
        }

        public int NumberOfComments
        {
            get { return _NumComments; }
            set { _NumComments = value; }
        }

        public UInt64 PopularVoteWinnerEntryId
        {
            get { return _PopularVoteWinnerEntryId; }
            set { _PopularVoteWinnerEntryId = value; }
        }

        public UInt64 OwnerVoteWinnerEntryId
        {
            get { return _OwnerVoteWinnerEntryId; }
            set { _OwnerVoteWinnerEntryId = value; }
        }

        public TimeSpan TimeSpanRemaining
        {
            get
            {
                return EndDate.Subtract (CurrentDate);
            }
        }

        public string TimeRemaining
        {
            get 
            {
                TimeSpan span = TimeSpanRemaining;

                if ((span.Days > 0 && span.Hours > 0) || 
                    (span.Days > 0 && span.Hours == 0 && span.Minutes > 0) ||
                    (span.Days > 0 && span.Hours == 0 && span.Minutes == 0 && span.Seconds > 0) ||
                    (span.Days == 0 && span.Hours > 12)
                    )
                {
                    return (span.Days+1).ToString () + " Day" + (span.Days+1 > 1 ? "s" : "");
                }
                if (span.Hours > 0)
                {
                    if (span.Minutes > 0)
                    {
                        return (span.Hours+1).ToString () + " Hour" + (span.Hours+1 > 1 ? "s" : "");
                    }

                    return span.Hours.ToString () + " Hour" + (span.Hours > 1 ? "s" : "");
                }
                if (span.Seconds < 1)
                {
                    return "ENDED";
                }

                return "1 Hour";
                //return span.Minutes.ToString () + "Minutes";
            }
        }

        public ContestUser Owner
        {
            get { return _Owner; }
            set { _Owner = value; }
        }

        public IList<ContestEntry> ContestEntries
        {
            get { return _ContestEntries; }
            set { _ContestEntries = value; }
        }
    }
}
