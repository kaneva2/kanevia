///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class KanevaColor
    {
        private uint colorType;
        private string colorName;
        private string colorDescription;
        private ushort colorR;
        private ushort colorG;
        private ushort colorB;

        public KanevaColor()
        {
            this.colorType = 0;
            this.colorName = "Black";
            this.colorDescription = "Default Color";
            this.colorR = 0;
            this.colorG = 0;
            this.colorB = 0;
        }

        public KanevaColor(uint colorType, string colorName, string colorDescription, ushort colorR, ushort colorG, ushort colorB)
        {
            this.colorType = colorType;
            this.colorName = colorName;
            this.colorDescription = colorDescription;
            this.colorR = colorR;
            this.colorG = colorG;
            this.colorB = colorB;
        }

        public uint ColorType
        {
            get { return this.colorType; }
            set { this.colorType = value; }
        }

        public string ColorName
        {
            get { return this.colorName; }
            set { this.colorName = value; }
        }

        public string ColorDescription
        {
            get { return this.colorDescription; }
            set { this.colorDescription = value; }
        }
        public ushort ColorR
        {
            get { return this.colorR; }
            set { this.colorR = value; }
        }

        public ushort ColorG
        {
            get { return this.colorG; }
            set { this.colorG = value; }
        }

        public ushort ColorB
        {
            get { return this.colorB; }
            set { this.colorB = value; }
        }
    }
}
