///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public enum BlastTypes
    {
        BASIC = 1,
        PROFILE_UPDATE = 2,
        BLOG = 3,
        MEDIA_COMMENT = 4,
        FORUM_POST = 5,
        ADD_FRIEND = 6,
        JOIN_COMMUNITY = 7,
        RAVE_3D_HOME = 8,
        RAVE_3D_HANGOUT = 9,
        EVENT = 10,
        SEND_GIFT = 11,
        ADMIN = 12,
        VIDEO = 13,
        PHOTO = 14,
        LINK = 15,
        PLACE = 16,
        AWARD = 17,
        FAME = 18,
        RAVE = 19,
        MEGARAVE = 20,
		SHOP_PURCHASE = 21,
		SHOP_UPLOAD = 22,
		REQUEST_3DAPP = 23,
		ACHIEVEMENT_3DAPP = 24,
        FAME_LEVELUP = 25,
        CONTEST = 26,
        RAVE_3D_APP = 27,
        CAMERA_SCREENSHOT = 28
    }

    [Serializable]
    public class Blast
    {
        private UInt64 entryId;
        private DateTime dateCreated;
        private UInt32 senderId;
        private string senderName;
        private string subject;
        private string diaryEntry;
        private string highlightCss;
        private int blastType;
        private UInt32 communityId;
        private string communityName = "";
        private bool isCommunityPersonal = false;
        private int numberOfReplies;
        private string displayThumbnail;
		private int recipientId = 0;
        private UInt32 originatorId = 0;
        private string originatorName = "";

        // Event specific
        private DateTime dateEvent;
        private string location = "";
        private string eventName = "";

        // Extra fields
        private string nameNoSpaces;
        private string thumbnailSmallPath;
        private DateTime currentTime;

        public Blast() {}

        public Blast (UInt64 entryId, DateTime dateCreated, UInt32 senderId, string senderName, string subj,
            string diaryEntry, string highlightCss, int blastType, UInt32 communityId, int numberOfReplies,
            string displayThumbnail, string nameNoSpaces, string thumbnailSmallPath, DateTime currentTime,
            string communityName, bool isCommunityPersonal, int recipientId)
             : this (entryId, dateCreated, senderId, senderName, subj,
               diaryEntry, highlightCss, blastType, communityId, numberOfReplies,
               displayThumbnail, nameNoSpaces, thumbnailSmallPath, currentTime,
               communityName, isCommunityPersonal, recipientId, 0, "")
        { }
        
        public Blast(UInt64 entryId, DateTime dateCreated, UInt32 senderId, string senderName, string subj, 
            string diaryEntry, string highlightCss, int blastType, UInt32 communityId, int numberOfReplies,
            string displayThumbnail, string nameNoSpaces, string thumbnailSmallPath, DateTime currentTime,
            string communityName, bool isCommunityPersonal, int recipientId, UInt32 originatorId, string originatorName)
        {
            this.entryId = entryId;
            this.dateCreated = dateCreated;
            this.senderId = senderId;
            this.senderName = senderName;
            this.subject = subj;
            this.diaryEntry = diaryEntry;
            this.highlightCss = highlightCss;
            this.blastType = blastType;
            this.communityId = communityId;
            this.communityName = communityName;                      
            this.isCommunityPersonal = isCommunityPersonal;                          
            this.numberOfReplies = numberOfReplies;
            this.displayThumbnail = displayThumbnail;
			this.recipientId = recipientId;
            this.originatorName = originatorName;
            this.originatorId = originatorId;

            // Extras
            this.nameNoSpaces = nameNoSpaces;
            this.thumbnailSmallPath = thumbnailSmallPath;
            this.currentTime = currentTime;
        }

        // Type Event
        public Blast(UInt64 entryId, DateTime dateCreated, UInt32 senderId, string senderName, string subj,
           string diaryEntry, string highlightCss, int blastType, UInt32 communityId, int numberOfReplies,
           string displayThumbnail, DateTime dateEvent, string location, string eventName,
           string nameNoSpaces, string thumbnailSmallPath, DateTime currentTime,
           string communityName, bool isCommunityPersonal)
        {
            this.entryId = entryId;
            this.dateCreated = dateCreated;
            this.senderId = senderId;
            this.senderName = senderName;
            this.subject = subj;
            this.diaryEntry = diaryEntry;
            this.highlightCss = highlightCss;
            this.blastType = blastType;
            this.communityId = communityId;
            this.communityName = communityName;                      
            this.isCommunityPersonal = isCommunityPersonal;                          
            this.numberOfReplies = numberOfReplies;
            this.displayThumbnail = displayThumbnail;

            this.dateEvent = dateEvent;
            this.location = location;
            this.eventName = eventName;

            // Extras
            this.nameNoSpaces = nameNoSpaces;
            this.thumbnailSmallPath = thumbnailSmallPath;
            this.currentTime = currentTime;
        }

        public UInt64 EntryId
        {
           get {return this.entryId;}
           set { this.entryId = value; }
        }

        public DateTime DateCreated
        {
           get {return this.dateCreated;}
           set {this.dateCreated = value;}
        }

        public UInt32 SenderId
        {
           get {return this.senderId;}
           set {this.senderId = value;}
        }

        public string SenderName
        {
            get { return this.senderName; }
            set { this.senderName = value; }
        }

        public string Subj
        {
           get {return this.subject;}
           set {this.subject = value;}
        }

        public string DiaryEntry
        {
           get {return this.diaryEntry;}
           set {this.diaryEntry = value;}
        }

        public string HighlightCss
        {
           get {return this.highlightCss;}
           set {this.highlightCss = value;}
        }

        public int BlastType
        {
           get {return this.blastType;}
           set {this.blastType = value;}
        }

        public UInt32 CommunityId
        {
           get {return this.communityId;}
           set {this.communityId = value;}
        }

        public string CommunityName
        {
            get { return this.communityName; }
            set { this.communityName = value; }
        }

        public bool IsCommunityPersonal
        {
            get { return this.isCommunityPersonal; }
            set { this.isCommunityPersonal = value; }
        }

        public int NumberOfReplies
        {
           get {return this.numberOfReplies;}
           set {this.numberOfReplies = value;}
        }

        public string DisplayThumbnail
        {
           get {return this.displayThumbnail;}
           set {this.displayThumbnail = value;}
        }

		public int RecipientId
		{
			get { return this.recipientId; }
			set { this.recipientId = value; }
		}

        // Events
        public DateTime DateEvent
        {
            get { return this.dateEvent; }
            set { this.dateEvent = value; }
        }

        public string Location
        {
            get { return this.location; }
            set { this.location = value; }
        }

        public string EventName
        {
            get { return this.eventName; }
            set { this.eventName = value; }
        }

        // Extra fields
        public string NameNoSpaces
        {
            get { return this.nameNoSpaces; }
            set { this.nameNoSpaces = value; }
        }

        public string ThumbnailSmallPath
        {
            get { return this.thumbnailSmallPath; }
            set { this.thumbnailSmallPath = value; }
        }

        public DateTime CurrentTime
        {
            get { return this.currentTime; }
            set { this.currentTime = value; }
        }

        public UInt32 OriginatorId
        {
            get { return this.originatorId; }
            set { this.originatorId = value; }
        }

        public string OriginatorName
        {
            get { return this.originatorName; }
            set { this.originatorName = value; }
        }

    }
}
