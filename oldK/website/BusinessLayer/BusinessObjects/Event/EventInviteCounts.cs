///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Runtime.Serialization;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class EventInviteCounts
    {
        private int _eventId = 0;
        private int _invitesAccepted = 0;       // invite_status = 2
        private int _invitesDeclined = 0;       // invite_status = 3
        private int _invitesOutstanding = 0;    // invite_status = 1

        public EventInviteCounts ()
        { }

        public EventInviteCounts (int eventId, int invitesOutstanding, int invitesAccepted, int invitesDeclined)
        {
            this._eventId = eventId;
            this._invitesOutstanding = invitesOutstanding;
            this._invitesAccepted = invitesAccepted;
            this._invitesDeclined = invitesDeclined;
        }

        public int EventId
        {
            get { return this._eventId; }
            set { this._eventId = value; }
        }

        public int InvitesSent
        {
            get { return this._invitesAccepted + this._invitesDeclined + this._invitesOutstanding; }
        }

        public int IntitesAccepted
        {
            get { return this._invitesAccepted; }
            set { this._invitesAccepted = value; }
        }

        public int InvitesDeclined
        {
            get { return this._invitesDeclined; }
            set { this._invitesDeclined = value; }
        }

        public int InvitesOutstanding
        {
            get { return this._invitesOutstanding; }
            set { this._invitesOutstanding = value; }
        }
    }
}
