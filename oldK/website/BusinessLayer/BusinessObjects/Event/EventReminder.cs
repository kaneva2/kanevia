///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class EventReminder
    {
        private int _ReminderId = 0;
        private int _ReminderTimeOffset = 0;
        private string _EmailType = string.Empty;

        public EventReminder()
        { }

        public EventReminder(int reminderId, int reminderTimeOffset, string emailType)
        {
            _ReminderId = reminderId;
            _ReminderTimeOffset = reminderTimeOffset;
            _EmailType = emailType;
        }

        public int ReminderId
        {
            get { return _ReminderId; }
            set { _ReminderId = value; }
        }

        public int ReminderTimeOffset
        {
            get { return _ReminderTimeOffset; }
            set { _ReminderTimeOffset = value; }
        }

        public string EmailType
        {
            get { return _EmailType; }
            set { _EmailType = value; }
        }
    }
}
