///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Runtime.Serialization;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class LocationDetail
    {
        private string _name = "";
        private int _communityId = 0;
        private int _zoneIndex = 0;
        private int _zoneInstance_Id = 0;

        public LocationDetail ()
        { }

        public LocationDetail(string name, Int32 communityId, Int32 zoneIndex, Int32 zoneInstanceId)
        {
            this._name = name;
            this._communityId = communityId;
            this._zoneIndex = zoneIndex;
            this._zoneInstance_Id = zoneInstanceId;
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public Int32 CommunityId
        {
            get { return _communityId; }
            set { _communityId = value; }
        }

        public Int32 ZoneIndex
        {
            get { return _zoneIndex; }
            set { _zoneIndex = value; }
        }

        public Int32 ZoneInstanceId
        {
            get { return _zoneInstance_Id; }
            set { _zoneInstance_Id = value; }
        }

    }
}
