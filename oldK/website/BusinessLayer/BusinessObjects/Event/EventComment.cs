///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class EventComment
    {
        private string _EventBlastCommentId = string.Empty;
        private string _EventBlastId = string.Empty;
        private int _EventId = 0;
        private int _EventOwnerUserId = 0;
        private EventInvitee _Creator = new EventInvitee ();
        private DateTime _CreatedDate = DateTime.MinValue;
        private DateTime _CurrentDate = DateTime.MinValue;
        private string _Comment = "";

        public EventComment () { }

        public EventComment (string eventBlastCommentId, int eventId, int eventOwnerUserId, string eventBlastId, DateTime createdDate, 
            string comment, int ownerId, string owner_username, string gender, 
            string thumbnailSmallPath, string thumbnailMediumPath, string thumbnailLargePath,
            string thumbnailSquarePath, DateTime currentDate)
        {
            this._EventBlastCommentId = eventBlastCommentId;
            this._EventBlastId = eventBlastId;
            this._EventId = eventId;
            this._EventOwnerUserId = eventOwnerUserId;
            this._CreatedDate = createdDate;
            this._Comment = comment;
            this._Creator.UserId = ownerId;
            this._Creator.Username = owner_username;
            this._Creator.Gender = gender;
            this._Creator.ThumbnailSmallPath = thumbnailSmallPath;
            this._Creator.ThumbnailMediumPath = thumbnailMediumPath;
            this._Creator.ThumbnailLargePath = thumbnailLargePath;
            this._Creator.ThumbnailSquarePath = thumbnailSquarePath;
            this._CurrentDate = currentDate;
        }

        public string EventBlastCommentId
        {
            get { return _EventBlastCommentId; }
            set { _EventBlastCommentId = value; }
        }

        public string EventBlastId
        {
            get { return _EventBlastId; }
            set { _EventBlastId = value; }
        }

        public int EventId
        {
            get { return _EventId; }
            set { _EventId = value; }
        }

        public int EventOwnerUserId
        {
            get { return _EventOwnerUserId; }
            set { _EventOwnerUserId = value; }
        }

        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        public DateTime CurrentDate
        {
            get { return _CurrentDate; }
            set { _CurrentDate = value; }
        }

        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        public EventInvitee Creator
        {
            get { return _Creator; }
            set { _Creator = value; }
        }
    }
}
