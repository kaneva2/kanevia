///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.Serialization;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class EventInvitee : ContestUser
    {
        public enum Invite_Status
        {
            NOT_INVITED = 0,
            INVITED = 1,
            ACCEPTED = 2,
            DECLINED = 3
        }

        private string _email = string.Empty;
        private int _userStatusId = 0;
        private bool _emailNotifications = false;
        private bool _attendedEvent = false;

        private Invite_Status _inviteStatus = Invite_Status.NOT_INVITED;

        public EventInvitee ()
        { }

        public EventInvitee (int userId, string username, string email, int userStatusId,
            Invite_Status inviteStatus, bool emailNotifications, string gender, string thumbnailSmallPath,
            string thumbnailMediumPath, string thumbnailLargePath, string thumbnailSquarePath, bool attendedEvent)
        {
            UserId = userId;
            Username = username;
            Gender = gender;
            ThumbnailSmallPath = thumbnailSmallPath;
            ThumbnailMediumPath = thumbnailMediumPath;
            ThumbnailLargePath = thumbnailLargePath;
            ThumbnailSquarePath = thumbnailSquarePath;
            this._email = email;
            this._userStatusId = userStatusId;
            this._inviteStatus = inviteStatus;
            this._emailNotifications = emailNotifications;
            this._attendedEvent = attendedEvent;
        }

        public string Email
        {
            get { return this._email; }
            set { this._email = value; }
        }

        public int UserStatusId
        {
            get { return _userStatusId; }
            set { _userStatusId = value; }
        }

        public bool EmailIsValidated
        {
            get { return _userStatusId.Equals (1); }
        }

        public Invite_Status InviteStatus
        {
            get { return this._inviteStatus; }
            set { this._inviteStatus = value; }
        }

        public bool EmailNotifications
        {
            get { return this._emailNotifications; }
            set { this._emailNotifications = value; }
        }

        public bool AttendedEvent
        {
            get { return this._attendedEvent; }
            set { this._attendedEvent = value; }
        }
      
    }
}
