///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Runtime.Serialization;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    [DataContract]
    public class Event : ICloneable
    {
        public enum eSTART_TIME_FILTER
        {
            NOW = 0,
            PAST = 1,
            UPCOMING = 2,
            ALL = 3,
            TODAY = 4,
            RECURRING_PAST = 5,
            RECURRING_ALL = 6
        }

        public enum ePRIORITY
        {
            NORMAL = 0,
            PREMIUM = 1,
            ADMINISTRATOR = 2
        }

        public enum eCONCURRENCY_ERROR
        {
            NONE = 0,
            TIME_OVERLAP = 1,
            EXCEED_MAX_EVENTS_PER_DAY = 2,
            GENERAL = 3,
            EXCEED_MAX_WORLD_EVENTS_PER_DAY = 4
        }

        public enum eMESSAGE_TYPE
        {
            EventInvite,
            EventChangeNotification,
            EventChangeInvite,
            EventCancellation,
            EventOwnerBlastNotification,
            EventAttendeeBlastNotification,
            EventBlastCommentNotification,
            EventReminderToday,
            EventReminderNow
        }

        public enum eSTATUS_TYPE
        {
            ACTIVE = 1,
            CANCELLED = 2        }

        public enum eRECURRING_STATUS_TYPE
        {
            NONE = 0,
            CREATED = 1,
            INVITES_SENT = 2,
            NEXT_OCCURANCE_SCHEDULED = 3
        }

        public class EventMessage
        {
            private string _subject = "";
            private string _message = "";

            public EventMessage () { }

            public string Subject
            {
                get { return _subject; }
                set { _subject = value; }
            }

            public string Message
            {
                get { return _message; }
                set { _message = value; }
            }
        }

        private int _EventId = 0;
        private int _CommunityId = 0;
        private int _UserId = 0;
        private string _Title = "";
        private string _Details = "";
        private string _Location = "";
        private DateTime _StartTime = DateTime.MinValue;
        private DateTime _EndTime = DateTime.MinValue;
        private int _TypeId = 0;
        private int _Priority = 0;
        private bool _IsPrivate = false;
        private DateTime _CreatedDate = DateTime.MinValue;
        private DateTime _LastUpdateDate = DateTime.MinValue;
        private bool _IsAP = false;
        private string _TrackingRequestGUID = "";
        private int _EventStatusId = (int)eSTATUS_TYPE.ACTIVE;
        private int _RecurringStatusId = (int) eRECURRING_STATUS_TYPE.NONE;
        private bool _InviteFriends = false;
        private int _RecurringEventParentId = 0;
        private int _ObjPlacementId = 0;


        public Event ()
        { }

        public Event (int eventId, int communityId, int userId, string title, string details, string location,
            DateTime startTime, DateTime endTime, int priority, bool isPrivate, DateTime createdDate, bool isAP, 
            string trackingRequestGUID)
              : this (eventId, communityId, userId, title, details, location,
                startTime, endTime, priority, isPrivate, createdDate, isAP, 
                trackingRequestGUID, (int)eSTATUS_TYPE.ACTIVE)
        { }

        public Event (int eventId, int communityId, int userId, string title, string details, string location,
            DateTime startTime, DateTime endTime, int priority, bool isPrivate, DateTime createdDate, bool isAP,
            string trackingRequestGUID, int statusId)
            : this (eventId, communityId, userId, title, details, location,
                startTime, endTime, priority, isPrivate, createdDate, isAP,
                trackingRequestGUID, statusId, (int) eRECURRING_STATUS_TYPE.NONE, false, 0, 0)
        { }

        public Event(int eventId, int communityId, int userId, string title, string details, string location,
            DateTime startTime, DateTime endTime, int priority, bool isPrivate, DateTime createdDate, bool isAP,
            string trackingRequestGUID, int statusId, int recurringStatusId, bool inviteFriends, int recurringEventParentId)
            : this(eventId, communityId, userId, title, details, location,
                startTime, endTime, priority, isPrivate, createdDate, isAP,
                trackingRequestGUID, statusId, recurringStatusId, false, recurringEventParentId, 0)
        { }

        public Event(int eventId, int communityId, int userId, string title, string details, string location,
            DateTime startTime, DateTime endTime, int priority, bool isPrivate, DateTime createdDate, bool isAP, 
            string trackingRequestGUID, int statusId, int recurringStatusId, bool inviteFriends, int recurringEventParentId, int objPlacementId)
        {
            this._EventId = eventId;
            this._CommunityId = communityId;
            this._UserId = userId;
            this._Title = title;
            this._Details = details;
            this._Location = location;
            this._StartTime = startTime;
            this._EndTime = endTime;
            this._Priority = priority;
            this._IsPrivate = isPrivate;
            this._CreatedDate = createdDate;
            this._IsAP = isAP;
            this._TrackingRequestGUID = trackingRequestGUID;
            this._EventStatusId = statusId;
            this._RecurringStatusId = recurringStatusId;
            this._InviteFriends = inviteFriends;
            this._RecurringEventParentId = recurringEventParentId;
            this._ObjPlacementId = objPlacementId;
        }

        public int EventId
        {
            get { return _EventId; }
            set { _EventId = value; }
        }

        public int CommunityId
        {
            get { return _CommunityId; }
            set { _CommunityId = value; }
        }

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        public string Details
        {
            get { return _Details; }
            set { _Details = value; }
        }

        public string Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        public DateTime StartTime
        {
            get { return _StartTime; }
            set { _StartTime = value; }
        }

        public DateTime EndTime
        {
            get { return _EndTime; }
            set { _EndTime = value; }
        }

        public int TypeId
        {
            get { return _TypeId; }
            set { _TypeId = value; }
        }

        public int Priority
        {
            get { return _Priority; }
            set { _Priority = value; }
        }

        public bool IsPrivate
        {
            get { return _IsPrivate; }
            set { _IsPrivate = value; }
        }

        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        public DateTime LastUpdateDate
        {
            get { return _LastUpdateDate; }
            set { _LastUpdateDate = value; }
        }

        public bool IsAP
        {
            get { return _IsAP; }
            set { _IsAP = value; }
        }

        public bool IsPremium
        {
            get { return _Priority == (int) ePRIORITY.PREMIUM; }
        }

        public bool IsAdmin
        {
            get { return _Priority == (int) ePRIORITY.ADMINISTRATOR; }
        }

        public string TrackingRequestGUID
        {
            get { return _TrackingRequestGUID; }
            set { _TrackingRequestGUID = value; }
        }

        public int StatusId
        {
            get { return _EventStatusId; }
            set { _EventStatusId = value; }
        }

        public bool IsActive
        {
            get { return _EventStatusId == (int) eSTATUS_TYPE.ACTIVE; }
        }

        public int RecurringStatusId
        {
            get { return _RecurringStatusId; }
            set { _RecurringStatusId = value; }
        }

        public bool IsRecurring
        {
            get { return _RecurringStatusId != (int) eRECURRING_STATUS_TYPE.NONE; }
        }

        public bool InviteFriends
        {
            get { return _InviteFriends; }
            set { _InviteFriends = value; }
        }

        public int RecurringEventParentId
        {
            get { return _RecurringEventParentId; }
            set { _RecurringEventParentId = value; }
        }

        public int ObjPlacementId
        {
            get { return _ObjPlacementId; }
            set { _ObjPlacementId = value; }
        }

        public object Clone ()
        {
            return this.MemberwiseClone ();
        }
    }
}
