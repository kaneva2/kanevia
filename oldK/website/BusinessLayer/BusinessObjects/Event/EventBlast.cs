///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class EventBlast
    {
        private string _EventBlastId = string.Empty;
        private int _EventId = 0;
        private int _EventOwnerUserId = 0;
        private ContestUser _Creator = new ContestUser ();
        private DateTime _CreatedDate = DateTime.MinValue;
        private DateTime _CurrentDate = DateTime.MinValue;
        private string _Comment = "";
        private int _NumComments = 0;

        public EventBlast () { }

        public EventBlast (int eventId, int eventOwnerUserId, string eventBlastId, DateTime createdDate, 
            string comment, int numComments, int ownerId, string owner_username, string gender, 
            string thumbnailSmallPath, string thumbnailMediumPath, string thumbnailLargePath, 
            string thumbnailSquarePath, DateTime currentDate)
        {
            this._EventBlastId = eventBlastId;
            this._EventId = eventId;
            this._EventOwnerUserId = eventOwnerUserId;
            this._CreatedDate = createdDate;
            this._Comment = comment;
            this._NumComments = numComments;
            this._Creator.UserId = ownerId;
            this._Creator.Username = owner_username;
            this._Creator.Gender = gender;
            this._Creator.ThumbnailSmallPath = thumbnailSmallPath;
            this._Creator.ThumbnailMediumPath = thumbnailMediumPath;
            this._Creator.ThumbnailLargePath = thumbnailLargePath;
            this._Creator.ThumbnailSquarePath = thumbnailSquarePath;
            this._CurrentDate = currentDate;
        }

        public string EventBlastId
        {
            get { return _EventBlastId; }
            set { _EventBlastId = value; }
        }

        public int EventId
        {
            get { return _EventId; }
            set { _EventId = value; }
        }

        public int EventOwnerUserId
        {
            get { return _EventOwnerUserId; }
            set { _EventOwnerUserId = value; }
        }

        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        public DateTime CurrentDate
        {
            get { return _CurrentDate; }
            set { _CurrentDate = value; }
        }

        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        public int NumberOfComments
        {
            get { return _NumComments; }
            set { _NumComments = value; }
        }

        public ContestUser Creator
        {
            get { return _Creator; }
            set { _Creator = value; }
        }
    }
}
