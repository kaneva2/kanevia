///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class FraudDetect
    {
       /// <summary>
        /// Default constructor for FraudDetect class.
        /// </summary>
        public FraudDetect() { 
            SuccessCount = 0;
            FailureCount = 0;
            TansactionList = new List<TransactionData>();
        }

        public int SuccessCount { get; set; } 
        public int FailureCount { get; set; } 
        public DateTime Expires { get; set; } 
        public List<TransactionData> TansactionList { get; set; } 
    }

    [Serializable]
    public class TransactionData
    {
        public TransactionData (){}

        public TransactionData (string iPAddress, Double amount, string last4Digits, DateTime transDate, bool success){
            IPAddress = iPAddress;
            Amount = amount;
            Last4Digits = last4Digits;
            TransDate = transDate;
            Success = success;
        }

        public string IPAddress { get; set; } 
        public Double Amount { get; set; } 
        public string Last4Digits { get; set; } 
        public DateTime TransDate { get; set; } 
        public bool Success { get; set; } 
    }
}
