///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class SubscriptionInfo
    {
        private int _SubscriptionId;
        private string _SubReferenceIdentifier;
        private string _Status;

        public SubscriptionInfo(int subscriptionId, string subReferenceIdentifier, string status)
        {
            this._SubscriptionId = subscriptionId;
            this._SubReferenceIdentifier = subReferenceIdentifier;
            this._Status = status;
        }

        public int SubscriptionId
        {
            get { return _SubscriptionId; }
            set { _SubscriptionId = value; }
        }

        public string SubReferenceIdentifier
        {
            get { return _SubReferenceIdentifier; }
            set { _SubReferenceIdentifier = value; }
        }

        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

    }
}
