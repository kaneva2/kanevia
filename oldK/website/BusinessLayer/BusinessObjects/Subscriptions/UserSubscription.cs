///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class UserSubscription
    {
        private UInt32 _UserSubscriptionId;
        private UInt32 _SubscriptionId;
        private UInt32 _PromotionId;
        private uint _DiscountPercent;
        private uint _MontlyAllowance;
        private int _UserId;
        private Double _Price;
        private bool _AutoRenew;
        private DateTime _PurchaseDate;
        private Subscription.SubscriptionStatus _StatusId;
        private int _BillingType;
        private string _SubscriptionIdentifier;
        private bool _UserCancelled;
        private DateTime _CancelledDate;
        private Subscription.SubscriptionTerm _InitalTerm;
        private Subscription.SubscriptionTerm _RenewalTerm;
        private DateTime _EndDate;
        private DateTime _FreeTrialEndDate;
        private string _UserCancellationReason;
        private bool _IsLegacy;

        public UserSubscription()
        {
            this._UserSubscriptionId = 0;
            this._SubscriptionId = 0;
            this._PromotionId = 0;
            this._DiscountPercent = 0;
            this._MontlyAllowance = 0;
            this._UserId = 0;
            this._Price = 0.0;
            this._AutoRenew = true;
            this._PurchaseDate = DateTime.Now;
            this._StatusId = Subscription.SubscriptionStatus.Active;
            this._BillingType = 0;
            this._SubscriptionIdentifier = "";
            this._UserCancelled = false;
            this._CancelledDate = DateTime.Now;
            this._InitalTerm = Subscription.SubscriptionTerm.BiWeekly;
            this._RenewalTerm = Subscription.SubscriptionTerm.BiWeekly;
            this._EndDate = DateTime.Now; ;
            this._FreeTrialEndDate = DateTime.Now;
            this._UserCancellationReason = "";
            this._IsLegacy = false;
        }


        public UserSubscription(UInt32 userSubscriptionId, UInt32 subscriptionId, UInt32 promotionId, int userId, Double price, bool autoRenew, DateTime purchaseDate, Subscription.SubscriptionStatus statusId, int billingType, string subscriptionIdentifier, bool userCancelled, DateTime cancelledDate, Subscription.SubscriptionTerm initalTerm, Subscription.SubscriptionTerm renewalTerm, DateTime endDate, DateTime freeTrialEndDate, string userCancellationReason, uint discountPercentage, uint monthlyAllowance, bool isLegacy)
        {
            this._UserSubscriptionId = userSubscriptionId;
            this._SubscriptionId = subscriptionId;
            this._PromotionId = promotionId;
            this._UserId = userId;
            this._Price = price;
            this._AutoRenew = autoRenew;
            this._PurchaseDate = purchaseDate;
            this._StatusId = statusId;
            this._BillingType = billingType;
            this._SubscriptionIdentifier = subscriptionIdentifier;
            this._UserCancelled = userCancelled;
            this._CancelledDate = cancelledDate;
            this._InitalTerm = initalTerm;
            this._RenewalTerm = renewalTerm;
            this._EndDate = endDate;
            this._FreeTrialEndDate = freeTrialEndDate;
            this._UserCancellationReason = userCancellationReason;
            this._DiscountPercent = discountPercentage;
            this._MontlyAllowance = monthlyAllowance;
            this._IsLegacy = isLegacy;
        }

        public uint DiscountPercent
        {
            get { return _DiscountPercent; }
            set { _DiscountPercent = value; }
        }

        public uint MontlyAllowance
        {
            get { return _MontlyAllowance; }
            set { _MontlyAllowance = value; }
        }

        public UInt32 PromotionId
        {
            get { return _PromotionId; }
            set { _PromotionId = value; }
        }

        public string UserCancellationReason
        {
            get { return _UserCancellationReason; }
            set { _UserCancellationReason = value; }
        }

        public UInt32 UserSubscriptionId
        {
            get { return _UserSubscriptionId; }
            set { _UserSubscriptionId = value; }
        }

        public UInt32 SubscriptionId
        {
            get {return _SubscriptionId;}
            set {_SubscriptionId = value;}
        }

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }
            

        public Double Price
        {
            get {return _Price;}
            set {_Price = value;}
        }

        public bool AutoRenew
        {
            get {return _AutoRenew;}
            set {_AutoRenew = value;}
        }

        public DateTime PurchaseDate
        {
            get {return _PurchaseDate;}
            set {_PurchaseDate = value;}
        }

        public Subscription.SubscriptionStatus StatusId
        {
            get {return _StatusId;}
            set {_StatusId = value;}
        }

        public int BillingType
        {
            get {return _BillingType;}
            set {_BillingType = value;}
        }

        public string SubscriptionIdentifier
        {
            get {return _SubscriptionIdentifier;}
            set {_SubscriptionIdentifier = value;}
        }

        public bool UserCancelled
        {
            get {return _UserCancelled;}
            set {_UserCancelled = value;}
        }

        public DateTime CancelledDate
        {
            get {return _CancelledDate;}
            set {_CancelledDate = value;}
        }

        public Subscription.SubscriptionTerm InitalTerm
        {
            get {return _InitalTerm;}
            set {_InitalTerm = value;}
        }

        public Subscription.SubscriptionTerm RenewalTerm
        {
            get {return _RenewalTerm;}
            set {_RenewalTerm = value;}
        }

        public DateTime EndDate
        {
            get {return _EndDate;}
            set {_EndDate = value;}
        }

        public DateTime FreeTrialEndDate
        {
            get {return _FreeTrialEndDate;}
            set {_FreeTrialEndDate = value;}
        }

        public bool IsLegacy
        {
            get { return _IsLegacy; }
        }
    }
}
