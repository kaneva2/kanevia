///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

//string sqlString = "INSERT INTO subscription_entitlements (" +
//    " entitlement_id, subscription_id, points, kei_point_id, global_id, quantity, " +
//    " keep_on_cancel" +
//    ") VALUES (" +
//    " @entitlement_id,  @subscription_id,  @points,  @kei_point_id,  @global_id,  @quantity, " +
//    "  @keep_on_cancel" +
//    ")";

//    List < IDbDataParameter > parameters = new List<IDbDataParameter>();
//    parameters.Add(new MySqlParameter("@entitlement_id", SubscriptionEntitlements.entitlement_id));
//    parameters.Add(new MySqlParameter("@subscription_id", SubscriptionEntitlements.subscription_id));
//    parameters.Add(new MySqlParameter("@points", SubscriptionEntitlements.points));
//    parameters.Add(new MySqlParameter("@kei_point_id", SubscriptionEntitlements.kei_point_id));
//    parameters.Add(new MySqlParameter("@global_id", SubscriptionEntitlements.global_id));
//    parameters.Add(new MySqlParameter("@quantity", SubscriptionEntitlements.quantity));
//    parameters.Add(new MySqlParameter("@keep_on_cancel", SubscriptionEntitlements.keep_on_cancel));
//    return Db.DB_NAME_HERE.ExecuteNonQuery(sqlString, parameters);

//string sqlString = "UPDATE subscription_entitlements SET"+
//    "entitlement_id=@entitlement_id, "+
//    "subscription_id=@subscription_id, "+
//    "points=@points, "+
//    "kei_point_id=@kei_point_id, "+
//    "global_id=@global_id, "+
//    "quantity=@quantity, "+
//    "keep_on_cancel=@keep_on_cancel"+
//    " WHERE entitlement_id = @EntitlementId"

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class SubscriptionEntitlement
    {
        private UInt32 _EntitlementId;
        private UInt32 _SubscriptionId;
        private Double _Points;
        private string _KeiPointId;
        private int _GlobalId;
        private UInt32 _Quantity;
        private bool _KeepOnCancel;

    public SubscriptionEntitlement (UInt32 entitlementId, UInt32 subscriptionId, Double points, string keiPointId, int globalId, UInt32 quantity, bool keepOnCancel    )
    {
        this._EntitlementId = entitlementId;
        this._SubscriptionId = subscriptionId;
        this._Points = points;
        this._KeiPointId = keiPointId;
        this._GlobalId = globalId;
        this._Quantity = quantity;
        this._KeepOnCancel = keepOnCancel;
    }

    public UInt32 EntitlementId
    {
        get {return _EntitlementId;}
        set {_EntitlementId = value;}
    }

    public UInt32 SubscriptionId
    {
        get {return _SubscriptionId;}
        set {_SubscriptionId = value;}
    }

    public Double Points
    {
        get {return _Points;}
        set {_Points = value;}
    }

    public string KeiPointId
    {
        get {return _KeiPointId;}
        set {_KeiPointId = value;}
    }

    public int GlobalId
    {
        get {return _GlobalId;}
        set {_GlobalId = value;}
    }

    public UInt32 Quantity
    {
        get {return _Quantity;}
        set {_Quantity = value;}
    }

    public bool KeepOnCancel
    {
        get {return _KeepOnCancel;}
        set {_KeepOnCancel = value;}
    }

}
}
