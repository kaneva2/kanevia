///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class Subscription
    {
        public enum SubscriptionStatus
        {
            Pending = 1,
            Active = 2,
            Refunded = 3,
            Cancelled = 4,
            Expired = 5,
            NoPayment = 6
        }

        public enum SubscriptionTerm
        {
            None = 0,
            Weekly = 1,
            BiWeekly = 2,
            Monthly = 3,
            BiMonthly = 4,
            Yearly = 5,
            Quarterly = 6,
            SemiAnnually = 7
        }

        private UInt32 _SubscriptionId;
        private UInt32 _DiscountPercent;
        private string _Name;
        private Double _Price;
        private Double _IntroductoryPrice;
        private DateTime _IntroductoryEndDate;
        private bool _GrandfatherPrice;
        private SubscriptionTerm _Term;
        private int _DaysFree;
        private string _UpsellImage;
        private string _LearnMoreImage;
        private string _LearnMoreContent;
        private string _Reasons2Keep;
        private uint _MontlyAllowance; 

        public Subscription()
        {
            this._SubscriptionId = 0;
            this._Name = "";
            this._Price = 0.0;
            this._IntroductoryPrice = 0.0;
            this._IntroductoryEndDate = DateTime.Now;
            this._GrandfatherPrice = false;
            this._Term = 0;
            this._DaysFree = 0;
            this._UpsellImage = "";
            this._LearnMoreImage = "";
            this._LearnMoreContent = "";
            this._DiscountPercent = 0;
            this._Reasons2Keep = "";
            this._MontlyAllowance = 0;
        }

        public Subscription(UInt32 subscriptionId, string name, Double price, Double introductoryPrice,
            DateTime introductoryEndDate, bool grandfatherPrice, SubscriptionTerm term, int daysFree,
            string upsellImage, string learnMoreImage, string learnMoreContent, 
            uint discountPercent, string reasons2Keep, uint monthlyAllowance)
        {
            this._SubscriptionId = subscriptionId;
            this._Name = name;
            this._Price = price;
            this._IntroductoryPrice = introductoryPrice;
            this._IntroductoryEndDate = introductoryEndDate;
            this._GrandfatherPrice = grandfatherPrice;
            this._Term = term;
            this._DaysFree = daysFree;
            this._UpsellImage = upsellImage;
            this._LearnMoreImage = learnMoreImage;
            this._LearnMoreContent = learnMoreContent;
            this._DiscountPercent = discountPercent;
            this._Reasons2Keep = reasons2Keep;
            this._MontlyAllowance = monthlyAllowance;
        }

        public uint MonthlyAllowance
        {
            get { return _MontlyAllowance; }
            set { _MontlyAllowance = value; }
        }

        public string Reasons2Keep
        {
            get { return _Reasons2Keep; }
            set { _Reasons2Keep = value; }
        }

        public string LearnMoreContent
        {
            get { return _LearnMoreContent; }
            set { _LearnMoreContent = value; }
        }

        public string UpsellImage
        {
            get { return _UpsellImage; }
            set { _UpsellImage = value; }
        }

        public string LearnMoreImage
        {
            get { return _LearnMoreImage; }
            set { _LearnMoreImage = value; }
        }

        public UInt32 SubscriptionId
        {
            get { return _SubscriptionId; }
            set { _SubscriptionId = value; }
        }

        public UInt32 DiscountPercent
        {
            get { return _DiscountPercent; }
            set { _DiscountPercent = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public Double Price
        {
            get { return _Price; }
            set { _Price = value; }
        }

        public Double IntroductoryPrice
        {
            get { return _IntroductoryPrice; }
            set { _IntroductoryPrice = value; }
        }

        public DateTime IntroductoryEndDate
        {
            get { return _IntroductoryEndDate; }
            set { _IntroductoryEndDate = value; }
        }

        public bool GrandfatherPrice
        {
            get { return _GrandfatherPrice; }
            set { _GrandfatherPrice = value; }
        }

        public SubscriptionTerm Term
        {
            get { return _Term; }
            set { _Term = value; }
        }

        public int DaysFree
        {
            get { return _DaysFree; }
            set { _DaysFree = value; }
        }

    }
}

