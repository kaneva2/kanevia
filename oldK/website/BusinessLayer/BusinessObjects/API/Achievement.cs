///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Runtime.Serialization;

namespace Kaneva.BusinessLayer.BusinessObjects.API
{
    [Serializable]
    [DataContract]
    public class Achievement
    {
        private UInt32 _AchievementsId = 0;
        private Int32 _CommunityId = 0;
        private string _Name = "";
        private string _Description = "";
        private UInt32 _Points = 0;
        private string _ImageUrl = "";

        public Achievement ()
        { }

        public Achievement (UInt32 achievementsId, Int32 communityId, string name, string description, UInt32 points, string imageUrl    )
        {
            this._AchievementsId = achievementsId;
            this._CommunityId = communityId;
            this._Name = name;
            this._Description = description;
            this._Points = points;
            this._ImageUrl = imageUrl;
        }

        [DataMember]
        public UInt32 AchievementsId
        {
            get {return _AchievementsId;}
            set {_AchievementsId = value;}
        }

        [DataMember]
        public Int32 CommunityId
        {
            get {return _CommunityId;}
            set {_CommunityId = value;}
        }

        [DataMember]
        public string Name
        {
            get {return _Name;}
            set {_Name = value;}
        }

        [DataMember]
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        [DataMember]
        public UInt32 Points
        {
            get { return _Points; }
            set { _Points = value; }
        }

        [DataMember]
        public string ImageUrl
        {
            get { return _ImageUrl; }
            set { _ImageUrl = value; }
        }

    }
}
