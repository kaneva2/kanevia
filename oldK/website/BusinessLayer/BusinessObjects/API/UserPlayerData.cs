///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects.API
{
    [Serializable]
    public class UserPlayerData : LeaderboardValue
    {
        private UInt32 _LevelNumber = 0;
        private UInt32 _PercentComplete = 0;
        private string _Title = "";

        public UserPlayerData (int communityId, int userId, string newValue, DateTime changedDate, string lbName, string columnNname,
            string username, string displayName, string thumbnailSquarePath, string thumbnailMediumPath,
            bool matureProfile, string uState, int rank, string gender, UInt32 levelNumber, UInt32 percentComplete, string title)

            : base(communityId, userId, newValue, changedDate, lbName, columnNname,
            username, displayName, thumbnailSquarePath, thumbnailMediumPath,
            matureProfile, uState, rank, gender) 
        {
            this._LevelNumber = levelNumber;
            this._PercentComplete = percentComplete;
            this._Title = title;
        }

        public UInt32 LevelNumber
        {
            get { return _LevelNumber; }
            set { _LevelNumber = value; }
        }

        public UInt32 PercentComplete
        {
            get { return _PercentComplete; }
            set { _PercentComplete = value; }
        }

        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }
    }
}
