///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Runtime.Serialization;

namespace Kaneva.BusinessLayer.BusinessObjects.API
{
    [Serializable]
    [DataContract]
    public class UserAchievementData : Achievement
    {
        private int _UserId = 0;
        private DateTime _CreatedDate = DateTime.MinValue;

        public UserAchievementData (UInt32 achievementsId, Int32 communityId, string name, 
            string description, UInt32 points, string imageUrl, int userId, DateTime createdDate)
        
            : base (achievementsId, communityId, name, description, points, imageUrl)
        {
            this._UserId = userId;
            this._CreatedDate = createdDate;
        }

        [DataMember]
        public int UserId
        {
            get { return this._UserId; }
            set { this._UserId = value; }
        }

        [DataMember]
        public DateTime CreatedDate
        {
            get { return this._CreatedDate; }
            set { this._CreatedDate = value; }
        }

        [DataMember]
        public bool AchievementEarned
        {
            get { return !(this._CreatedDate.Equals(DateTime.MinValue)); }
            set { }
        }
    }
}
