///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects.API
{
    [Serializable]
    public class LevelValue
    {
        private Int32 _CommunityId;
        private Int32 _UserId;
        private UInt32 _LevelNumber = 0;
        private UInt32 _PercentComplete = 0;
        private DateTime _ChangedDate;
        private string _Title = "";

        public LevelValue() { }

        public LevelValue(Int32 communityId, Int32 userId, UInt32 levelNumber, UInt32 percentComplete, DateTime changedDate, string title)
        {
            this._CommunityId = communityId;
            this._UserId = userId;
            this._LevelNumber = levelNumber;
            this._PercentComplete = percentComplete; 
            this._ChangedDate = changedDate;
            this._Title = title;
        }

        public Int32 CommunityId
        {
            get { return _CommunityId; }
            set { _CommunityId = value; }
        }

        public Int32 UserId
        {
            get {return _UserId;}
            set {_UserId = value;}
        }

        public UInt32 LevelNumber
        {
            get { return _LevelNumber; }
            set { _LevelNumber = value; }
        }

        public UInt32 PercentComplete
        {
            get { return _PercentComplete; }
            set { _PercentComplete = value; }
        }

        public DateTime ChangedDate
        {
            get {return _ChangedDate;}
            set {_ChangedDate = value;}
        }

        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }
    }
}
