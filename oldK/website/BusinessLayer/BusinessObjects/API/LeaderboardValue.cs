///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects.API
{
    [Serializable]
    public class LeaderboardValue
    {
        private int _CommunityId;
        private int _UserId;
        private string _Value;
        private DateTime _ChangedDate;
        private string _Name;
        private string _ColumnName;

        private string _Username = "";
        private string _DisplayName = "";
        private string _ThumbnailSmallPath = "";
        private string _ThumbnailMediumPath = "";
        private string _Gender = "M";
        private bool _MatureProfile = false;    
        private string _Ustate;
        private int _Rank;

        public LeaderboardValue(int communityId, int userId, string newValue, DateTime changedDate, string lbName, string columnNname,
            string username, string displayName, string thumbnailSmallPath, string thumbnailMediumPath,
            bool matureProfile, string uState, int rank, string gender)
        {
            this._CommunityId = communityId;
            this._UserId = userId;
            this._Value = newValue;
            this._ChangedDate = changedDate;
            this._Name = lbName;
            this._ColumnName = columnNname;

            this._Username = username;
            this._DisplayName = displayName;
            this._ThumbnailSmallPath = thumbnailSmallPath;
            this._ThumbnailMediumPath = thumbnailMediumPath;
            this._MatureProfile = matureProfile;
            this._Ustate = uState;
            this._Rank = rank;
            this._Gender = gender;
        }

        public int CommunityId
        {
            get { return _CommunityId; }
            set { _CommunityId = value; }
        }

        public int UserId
        {
            get {return _UserId;}
            set {_UserId = value;}
        }

        public string CurrentValue
        {
            get { return _Value; }
            set { _Value = value; }
        }

        public DateTime ChangedDate
        {
            get {return _ChangedDate;}
            set {_ChangedDate = value;}
        }


        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string ColumnName
        {
            get { return _ColumnName; }
            set { _ColumnName = value; }
        }

        public string Username
        {
            get { return _Username; }
            set { _Username = value; }
        }

        public string DisplayName
        {
            get { return _DisplayName; }
            set { _DisplayName = value; }
        }

        public string ThumbnailSmallPath
        {
            get { return _ThumbnailSmallPath; }
            set { _ThumbnailSmallPath = value; }
        }

        public string ThumbnailMediumPath
        {
            get { return _ThumbnailMediumPath; }
            set { _ThumbnailMediumPath = value; }
        }

        public bool MatureProfile
        {
            get { return _MatureProfile; }
            set { _MatureProfile = value; }
        }

        public string Ustate
        {
            get { return _Ustate; }
            set { _Ustate = value; }
        }

        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }

        public string Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }
    }

}
