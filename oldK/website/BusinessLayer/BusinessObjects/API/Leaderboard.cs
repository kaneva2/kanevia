///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects.API
{
    [Serializable]
    public class Leaderboard
    {
        private int _CommunityId = 0;
        private string _Name = "";
        private string _SortBy = SORTBY_DESC;
        private int  _Format = (int)FormatType.General;
        private string _ColumnName = "";

        // Order By 
        public const string SORTBY_ASC = "ASC";
        public const string SORTBY_DESC = "DESC";

        // Format Type
        public enum FormatType
        {
            General = 0,
            Currency = 1,
            Time = 2
        }

        public Leaderboard () { }

        public Leaderboard(int communityId, string name, string sortBy, int format, string columnName)
        {
            this._CommunityId = communityId;
            this._Name = name;
            this._SortBy = sortBy;
            this._Format = format;
            this._ColumnName = columnName;
        }

        public int CommunityId
        {
            get { return _CommunityId; }
            set { _CommunityId = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string SortBy
        {
            get { return _SortBy; }
            set { _SortBy = value; }
        }

        public int FormatForValue
        {
            get { return _Format; }
            set { _Format = value; }
        }

        public string ColumnName
        {
            get { return _ColumnName; }
            set { _ColumnName = value; }
        }
    }
}
