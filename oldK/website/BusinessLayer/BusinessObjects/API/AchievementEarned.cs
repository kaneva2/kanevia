///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Runtime.Serialization;

namespace Kaneva.BusinessLayer.BusinessObjects.API
{
    [Serializable]
    [DataContract]
    public class AchievementEarned
    {
        private UInt32 _AchievementsId;
        private UInt32 _UserId;
        private DateTime _CreatedDate;

        private string _Name;
        private string _Description;
        private string _ImageUrl;

        private string _Username = "";
        private string _DisplayName = "";
        private string _ThumbnailSmallPath = "";
        private string _ThumbnailMediumPath = "";
        private string _Gender = "M";
        private bool _MatureProfile = false;
        private string _Ustate;

        public AchievementEarned (UInt32 achievementsId, UInt32 userId, DateTime createdDate,
            string name, string description, string imageUrl ,
            string username, string displayName, string thumbnailSmallPath, string thumbnailMediumPath,
            bool matureProfile, string uState, string gender)
        {
            this._AchievementsId = achievementsId;
            this._UserId = userId;
            this._CreatedDate = createdDate;

            this._Name = name;
            this._Description = description;
            this._ImageUrl = imageUrl;

            this._Username = username;
            this._DisplayName = displayName;
            this._ThumbnailSmallPath = thumbnailSmallPath;
            this._ThumbnailMediumPath = thumbnailMediumPath;
            this._MatureProfile = matureProfile;
            this._Ustate = uState;
            this._Gender = gender;
        }

        [DataMember]
        public UInt32 AchievementsId
        {
            get {return _AchievementsId;}
            set {_AchievementsId = value;}
        }

        [DataMember]
        public UInt32 UserId
        {
            get {return _UserId;}
            set {_UserId = value;}
        }

        [DataMember]
        public DateTime CreatedDate
        {
            get {return _CreatedDate;}
            set {_CreatedDate = value;}
        }

        [DataMember]
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        [DataMember]
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        [DataMember]
        public string ImageUrl
        {
            get { return _ImageUrl; }
            set { _ImageUrl = value; }
        }

        [DataMember]
        public string Username
        {
            get { return _Username; }
            set { _Username = value; }
        }

        [DataMember]
        public string DisplayName
        {
            get { return _DisplayName; }
            set { _DisplayName = value; }
        }

        [DataMember]
        public string ThumbnailSmallPath
        {
            get { return _ThumbnailSmallPath; }
            set { _ThumbnailSmallPath = value; }
        }

        [DataMember]
        public string ThumbnailMediumPath
        {
            get { return _ThumbnailMediumPath; }
            set { _ThumbnailMediumPath = value; }
        }

        [DataMember]
        public bool MatureProfile
        {
            get { return _MatureProfile; }
            set { _MatureProfile = value; }
        }

        [DataMember]
        public string Ustate
        {
            get { return _Ustate; }
            set { _Ustate = value; }
        }

        [DataMember]
        public string Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }

    }
}
