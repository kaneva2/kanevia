///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects.API
{
    [Serializable]
    public class APIAuthentication
    {
        private int _CommunityId = 0;
        private string _ConsumerKey = "";
        private string _ConsumerSecret = "";
        private int _Game = 0;
        
        private bool _AdminOverrideBlast = false;
        private bool _SendBlast = false;

        public APIAuthentication() { }

        public APIAuthentication(int communityId, string consumerKey, string consumerSecret, int gameId, bool adminOverrideBlast, bool sendBlast)
        {
            this._CommunityId = communityId;
            this._ConsumerKey = consumerKey;
            this._ConsumerSecret = consumerSecret;
            this._Game = gameId;
            this._AdminOverrideBlast = adminOverrideBlast;
            this._SendBlast = sendBlast;
        }

        public int CommunityId
        {
            get {return _CommunityId;}
            set {_CommunityId = value;}
        }

        public string ConsumerKey
        {
            get {return _ConsumerKey;}
            set {_ConsumerKey = value;}
        }

        public string ConsumerSecret
        {
            get {return _ConsumerSecret;}
            set {_ConsumerSecret = value;}
        }

        public int GameId
        {
            get { return _Game; }
            set { _Game = value; }
        }

        public bool AdminOverrideBlast
        {
            get { return _AdminOverrideBlast; }
            set { _AdminOverrideBlast = value; }
        }

        public bool SendBlast
        {
            get { return _SendBlast; }
            set { _SendBlast = value; }
        }
    }

}
