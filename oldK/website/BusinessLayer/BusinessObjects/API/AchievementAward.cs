///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Runtime.Serialization;

namespace Kaneva.BusinessLayer.BusinessObjects.API
{
    [Serializable]
    [DataContract]
    public class AchievementAward
    {
       
        public AchievementAward ()
        { }

        public AchievementAward(UInt32 achievementAwardId, UInt32 achievement_id, string name, int globalId, 
            int animationId, string gender, int quantity, string newTitleName, double rewards)
        {
            this.AchievementAwardId = achievementAwardId;
            this.AchievementId = achievement_id;
            this.Name = name;
            this.GlobalId = globalId;
            this.AnimationId = animationId;
            this.Gender = gender;
            this.Quantity = quantity;
            this.NewTitleName = newTitleName;
            this.Rewards = rewards;
        }

        [DataMember]
        public UInt32 AchievementAwardId { get; set; }

        [DataMember]
        public UInt32 AchievementId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int GlobalId { get; set; }

        [DataMember]
        public int AnimationId { get; set; }

        [DataMember]
        public string Gender { get; set; }

        [DataMember]
        public int Quantity { get; set; }

        [DataMember]
        public string NewTitleName { get; set; }

        [DataMember]
        public double Rewards { get; set; }

    }
}
