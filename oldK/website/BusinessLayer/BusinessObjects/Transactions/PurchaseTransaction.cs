///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class PurchaseTransaction : WOKItem
    {
        private UInt32 _ItemPrice = 0;
        private UInt32 _Quantity = 0;
        private DateTime _PurchaseDate;
        private string _KeiPointId = "";
        private int _TransactionTypeId = 0;
        private int _ItemPurchaseTransactionType = 0;
        
        /// <summary>
        /// Default constructor for Item class.
        /// </summary>
        public PurchaseTransaction() { }

        public PurchaseTransaction (int globalId, string name, string description, string displayName, 
            UInt32 itemPrice, UInt32 quantity, DateTime purchaseDate, string keiPointId, int transactionType)
        {
            this._ItemPrice = itemPrice;
            this._Quantity = quantity;
            this._PurchaseDate = purchaseDate;
            this._KeiPointId = keiPointId;
            this._TransactionTypeId = transactionType;

            this.GlobalId = globalId;
            this.Name = name;
            this.Description = description;
            this.DisplayName = displayName;
        }

        public UInt32 ItemPrice
        {
            get { return _ItemPrice; }
            set { _ItemPrice = value; }
        }

        public new UInt32 Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public UInt32 TotalPrice
        {
            get { return _Quantity * _ItemPrice; }
        }

        public DateTime PurchaseDate
        {
            get { return _PurchaseDate; }
            set { _PurchaseDate = value; }
        }
        
        public string KeiPointId
        {
            get { return _KeiPointId; }
            set { _KeiPointId = value; }
        }
        
        public int TransactionTypeId
        {
            get { return _TransactionTypeId; }
            set { _TransactionTypeId = value; }
        }

        // This is used to track if purchase was a free purchase by GM. Used
        // for the transactions page
        public int ItemPurchaseTransactionType
        {
            get { return _ItemPurchaseTransactionType; }
            set { _ItemPurchaseTransactionType = value; }
        }
    }
}
