///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class TradeTransaction : GiftTransaction 
    {
        private int _TransactionAmount = 0;
        private string _KeiPointId = "";
        
        /// <summary>
        /// Default constructor for GiftTransaction class.
        /// </summary>
        public TradeTransaction() { }

        public TradeTransaction (DateTime transDate, int globalId, string itemName,
            int quantity, int toId, int fromId, int transAmount, string keiPointId)   
        {
            this.TransactionDate = transDate;
            this.GlobalId = globalId;
            this.ItemName = itemName;
            this.Quantity = quantity;
            this.ToId = toId;
            this.FromId = fromId;
            this._TransactionAmount = transAmount;
            this._KeiPointId = keiPointId;
        }

        public int TransactionAmount
        {
            get { return _TransactionAmount; }
            set { _TransactionAmount = value; }
        }

        public string KeiPointId
        {
            get { return _KeiPointId; }
            set { _KeiPointId = value; }
        }
    }
}
