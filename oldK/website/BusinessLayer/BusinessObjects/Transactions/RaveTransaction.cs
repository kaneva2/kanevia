///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class RaveTransaction
    {
        public enum eItemType
        {
            ASSET_GAME = 1,
            ASSET_VIDEO = 2,
            ASSET_GENERIC = 3,
            ASSET_MUSIC = 4,
            ASSET_PHOTO = 5,
            ASSET_PATTERN = 6,
            ASSET_TV_CHANNEL = 7,
            ASSET_WIDGET = 8,
            WEB_PROFILE = 9,
            WEB_COMMUNITY = 10,
            WOK_AVATAR = 11,
            WOK_HOME = 12,
            WOK_HANGOUT = 13,           
            UGC = 14
        }

        private DateTime _TransactionDate;
        private int _RaverId;
        private int _ItemId;
        private int _ItemOwnerId;
        private string _RaverName;
        private string _ItemName;
        private string _ItemOwnerName;
        private bool _IsMegaRave;
        private eItemType _ItemType;
        
        /// <summary>
        /// Default constructor for RaveTransaction class.
        /// </summary>
        public RaveTransaction() { }

        public RaveTransaction (int raverId, string raverName, int itemId, string itemName, int itemOwnerId,
            string itemOwnerName, eItemType itemType, bool isMegaRave, DateTime transactionDate) 
        {
            this._RaverId = raverId;
            this._RaverName = raverName;
            this._ItemId = itemId;
            this._ItemName = itemName;
            this._ItemOwnerId = itemOwnerId;
            this._ItemOwnerName = itemOwnerName;
            this._ItemType = itemType;
            this._IsMegaRave = isMegaRave;
            this._TransactionDate = transactionDate;
        }

        public RaveTransaction (int raverId, string raverName, int itemId, string itemName, int itemOwnerId,
           string itemOwnerName, int itemType, bool isMegaRave, DateTime transactionDate)
        {
            this._RaverId = raverId;
            this._RaverName = raverName;
            this._ItemId = itemId;
            this._ItemName = itemName;
            this._ItemOwnerId = itemOwnerId;
            this._ItemOwnerName = itemOwnerName;
            this._ItemType = (eItemType) itemType;
            this._IsMegaRave = isMegaRave;
            this._TransactionDate = transactionDate;
        }

        public DateTime TransactionDate
        {
            get { return _TransactionDate; }
            set { _TransactionDate = value; }
        }

        public int RaverId
        {
            get { return _RaverId; }
            set { _RaverId = value; }
        }

        public string RaverName
        {
            get { return _RaverName; }
            set { _RaverName = value; }
        }

        public int ItemId
        {
            get { return _ItemId; }
            set { _ItemId = value; }
        }

        public string ItemName
        {
            get { return _ItemName; }
            set { _ItemName = value; }
        }

        public int ItemOwnerId
        {
            get { return _ItemOwnerId; }
            set { _ItemOwnerId = value; }
        }

        public string ItemOwnerName
        {
            get { return _ItemOwnerName; }
            set { _ItemOwnerName = value; }
        }

        public eItemType ItemType
        {
            get { return _ItemType; }
            set { _ItemType = value; }
        }

        public bool IsMegaRave
        {
            get { return _IsMegaRave; }
            set { _IsMegaRave = value; }
        }

    }
}
