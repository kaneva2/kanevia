///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class PremiumItemRedeemAmounts
    {
        private int pending = 0;
        private int available = 0;
        private int communityId = 0;
        private int earnedPending = 0;
        private int earnedAvailable = 0;

        public PremiumItemRedeemAmounts () { }

        public PremiumItemRedeemAmounts (int communityId, int pending, int available)
        {
            this.CommunityId = communityId;
            this.Pending = pending;
            this.Available = available;
        }

        public int Pending
        {
            get { return pending; }
            set { pending = value; }
        }

        public int Available
        {
            get { return available; }
            set { available = value; }
        }

        public int Total
        {
            get { return pending + available; }
        }

        public int CommunityId
        {
            get { return communityId; }
            set { communityId = value; }
        }

        public int EarnedPending
        {
            get 
            { 
                if( earnedPending == 0)
                {
                    earnedPending = Convert.ToInt32 (Math.Ceiling (pending * PremiumItemUserRedemptionPercentage));            
                }

                return earnedPending;
            }
        }
        
        public int EarnedAvailable
        {
            get
            {
                if (earnedAvailable == 0)
                {
                    earnedAvailable = Convert.ToInt32 (Math.Ceiling (available * PremiumItemUserRedemptionPercentage));
                }

                return earnedAvailable;
            }
        }

        public int EarnedTotal
        {
            get { return EarnedPending + EarnedAvailable; }
        }

        public static double PremiumItemUserRedemptionPercentage
        {
            get
            {
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["PremiumItemUserRedemptionPercentage"] != null)
                    {
                        return Convert.ToDouble (System.Configuration.ConfigurationManager.AppSettings["PremiumItemUserRedemptionPercentage"]);
                    }
                    else
                    {
                        return .7;
                    }
                }
                catch
                {
                    return .7;
                }
            }
        }

    }
}
