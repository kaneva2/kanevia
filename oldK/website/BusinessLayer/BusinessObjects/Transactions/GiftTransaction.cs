///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class GiftTransaction
    {
        private DateTime _TransactionDate;
        private int _ToId = 0;
        private int _FromId = 0;
        private int _GlobalId = 0;
        private int _Quantity = 0;
        private string _ItemName = "";

        /// <summary>
        /// Default constructor for GiftTransaction class.
        /// </summary>
        public GiftTransaction() { }

        public GiftTransaction (DateTime transDate, int globalId, string itemName, 
            int quantity, int toId, int fromId)   
        {
            this._TransactionDate = transDate;
            this._GlobalId = globalId;
            this._ItemName = itemName;
            this._Quantity = quantity;
            this._ToId = toId;
            this._FromId = fromId;
        }

        public DateTime TransactionDate
        {
            get { return _TransactionDate; }
            set { _TransactionDate = value; }
        }

        public int GlobalId
        {
            get { return _GlobalId; }
            set { _GlobalId = value; }
        }

        public string ItemName
        {
            get { return _ItemName; }
            set { _ItemName = value; }
        }

        public int Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public int ToId
        {
            get { return _ToId; }
            set { _ToId = value; }
        }

        public int FromId
        {
            get { return _FromId; }
            set { _FromId = value; }
        }
    }
}
