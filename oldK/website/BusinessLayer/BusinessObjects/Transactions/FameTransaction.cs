///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class FameTransaction
    {
        private int _UserId = 0;
        private int _RewardsAwarded = 0;
        private int _FameTypeId = 0;
        private int _LevelNumber = 0;
        private int _LevelAwardQuantity = 0;
        private int _LevelAwardGlobalId = 0;

        private string _FameTypeName = "";
        private string _LevelAwardName = "";
        private string _NewTitle = "";
        
        private DateTime _LevelDate;
        private IList<LevelAwards> _LevelAward;

        /// <summary>
        /// Default constructor for FameTransaction class.
        /// </summary>
        public FameTransaction() { }

        public FameTransaction (int UserId, int fameTypeId, string fameTypeName,  
            DateTime levelDate, int levelNumber, int rewardsAwarded)   
        {
            this._FameTypeId = fameTypeId;
            this._FameTypeName = fameTypeName;
            this._LevelDate = levelDate;
            this._LevelNumber = levelNumber;
            this._RewardsAwarded = rewardsAwarded;
        }

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public DateTime LevelDate
        {
           get {return _LevelDate;}
           set {_LevelDate = value;}
        }

        public int RewardsAwarded
        {
           get {return _RewardsAwarded;}
           set {_RewardsAwarded = value;}
        }
 
        public int FameTypeId
        {
           get {return _FameTypeId;}
           set {_FameTypeId = value;}
        }

        public string FameTypeName
        {
           get {return _FameTypeName;}
           set {_FameTypeName = value;}
        }

        public int LevelNumber
        {
           get {return _LevelNumber;}
           set {_LevelNumber = value;}
        }

        public string LevelAwardName
        {
           get {return _LevelAwardName;}
           set {_LevelAwardName = value;}
        }

        public int LevelAwardGlobalId
        {
           get {return _LevelAwardGlobalId;}
           set {_LevelAwardGlobalId = value;}
        }

        public int LevelAwardQuantity
        {
           get {return _LevelAwardQuantity;}
           set {_LevelAwardQuantity = value;}
        }

        public string NewTitle
        {
           get {return _NewTitle;}
           set {_NewTitle = value;}
        }

        public IList <LevelAwards> LevelAwards
        {
            get { return _LevelAward; }
            set { _LevelAward = value; }
        }
    }
}
