///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class RewardTransaction
    {
        private DateTime _RewardDate;
        private int _RewardAmount = 0;
        private int _UserId = 0;
        private Int64 _TransTypeId = 0;
        private int _RewardEventId = 0;
        private int _WokTransLogId = 0;
        private string _TransDescription = "";
        private string _EventName = "";
        
        /// <summary>
        /// Default constructor for RewardTransaction class.
        /// </summary>
        public RewardTransaction() { }

        public RewardTransaction (int userId, string transDesc, int rewardAmount, DateTime rewardDate,
            Int64 transTypeId, int rewardEventId, string eventName, int wokTransLogId)
        {
            this._UserId = userId;
            this.TransactionDescription = transDesc;
            this.RewardAmount = rewardAmount;
            this.RewardDate = rewardDate;
            this._TransTypeId = transTypeId;
            this._RewardEventId = rewardEventId;
            this._EventName = eventName;
            this._WokTransLogId = wokTransLogId;
        }

        public DateTime RewardDate
        {
            get { return _RewardDate; }
            set { _RewardDate = value; }
        }

        public string TransactionDescription
        {
            get { return _TransDescription; }
            set { _TransDescription = value; }
        }

        public int RewardAmount
        {
            get { return _RewardAmount; }
            set { _RewardAmount = value; }
        }

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public Int64 TransactionTypeId
        {
            get { return _TransTypeId; }
            set { _TransTypeId = value; }
        }

        public int RewardEventId
        {
            get { return _RewardEventId; }
            set { _RewardEventId = value; }
        }

        public string EventName
        {
            get { return _EventName; }
            set { _EventName = value; }
        }

        public int WokTransactionLogId
        {
            get { return _WokTransLogId; }
            set { _WokTransLogId = value; }
        }
    }
}
