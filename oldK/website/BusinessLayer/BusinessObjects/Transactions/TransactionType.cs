///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class TransactionType
    {
        public enum eTRANSACTION_TYPES
        {
            CASH_TT_BOUGHT_CREDITS = 1,
            CASH_TT_TRADE = 2,
            CASH_TT_BOUGHT_ITEM = 3,
            CASH_TT_SOLD_ITEM = 4,
            CASH_TT_LOOT = 5,
            CASH_TT_MISC = 6,
            CASH_TT_REFUND = 7,
            CASH_TT_OPENING_BALANCE = 8,
            CASH_TT_BOUGHT_HOUSE_ITEM = 9,
            CASH_TT_QUEST = 10,
            CASH_TT_BANKING = 11,
            CASH_TT_HOUSE_BANKING = 12,
            CASH_TT_BOUGHT_GIFT = 13,
            CASH_TT_ROYALITY = 14,
            CASH_TT_SPECIAL = 15,
            CASH_TT_COVER_CHARGE = 16,
            CASH_TT_COMMISSION = 17,
            CASH_TT_ITEM_PICKUP = 18,
            CASH_TT_LEVEL_REWARD = 19,
            CASH_TT_NAME_CHANGE = 20,
            CASH_TT_BOUGHT_ITEM_ON_WEB = 21,
            CASH_TT_CONTEST = 22,
            CASH_TT_EVENT = 23,
            CASH_TT_SURVEY = 24,
            CASH_TT_INSTANT_BUY = 25,
            CASH_TT_SUPER_REWARDS = 26,
            CASH_TT_SUBSCRIPTIONS = 27,
            CASH_TT_RAVE = 28,
            CASH_TT_MEGARAVE = 29,
            CASH_TT_RECEIVED_GIFT = 30,
            CASH_TT_TOUR = 31,
            CASH_TT_WORLD_FAME_ACHIEVEMENT = 32,
            CASH_TT_EMP_AGENCY_LEVELUP = 33,
            CASH_TT_TOUR_PARTICIPATION = 34,
            CASH_TT_WORLD_VISITORS_BONUS = 35,
            CASH_TT_PREMIUM_ITEM_CREDIT_REDEEM = 36,
            CASH_TT_PREMIUM_ITEM = 37,
            CASH_TT_GAMING = 38,
            CASH_TT_GAME_ITEM = 39,
            CASH_TT_GAME_ITEM_COMMISION = 40,
            CASH_TT_LINKED_WORLD_SLOT = 41,
            TT_AUTO_TEST_USER_BALANCE_ADJUST = 42
        }

        public enum eCONTEST_TRANSACTION_TYPES
        {
            CONTEST_TT_CREATE = 1,
            CONTEST_TT_WINNER = 2,
            CONTEST_TT_REFUND = 3
        }
    }
}
