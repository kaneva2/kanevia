///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class PurchaseTransactionPremiumItem : PurchaseTransaction
    {
        public enum RedeemStatus
        {
            Pending = 0,
            Available = 1,
            Redeemed = 2
        }

        private int purchaseId = 0;
        private string buyerName = "";
        private int buyerId = 0;
        private DateTime redemptionDate = DateTime.MaxValue;

        /// <summary>
        /// Default constructor for Item class.
        /// </summary>
        public PurchaseTransactionPremiumItem () { }

        public PurchaseTransactionPremiumItem (int globalId, string name, string description, string displayName, 
            UInt32 itemPrice, UInt32 quantity, DateTime purchaseDate, string keiPointId, int transactionType,
            int purchaseId, int buyerId, string buyerName, DateTime redemptionDate)
        {
            this.purchaseId = purchaseId;
            this.buyerId = buyerId;
            this.buyerName = buyerName;
            
            this.ItemPrice = itemPrice;
            this.Quantity = quantity;
            this.PurchaseDate = purchaseDate;
            this.KeiPointId = keiPointId;
            this.TransactionTypeId = transactionType;

            this.GlobalId = globalId;
            this.Name = name;
            this.Description = description;
            this.DisplayName = displayName;
            this.RedemptionDate = redemptionDate;
        }

        public int PurchaseId
        {
            get { return purchaseId; }
            set { purchaseId = value; }
        }

        public string BuyerName
        {
            get { return buyerName; }
            set { buyerName = value; }
        }

        public int BuyerId
        {
            get { return buyerId; }
            set { buyerId = value; }
        }

        public DateTime RedemptionDate
        {
            get { return redemptionDate; }
            set { redemptionDate = value; }
        }

        public RedeemStatus RedemptionStatus
        {
            get
            {
                DateTime tempDate = DateTime.Now.AddDays (PremiumItemDaysPendingBeforeRedeem * -1);
                if (redemptionDate.Date < DateTime.MaxValue.Date)
                {
                    return RedeemStatus.Redeemed;
                }
                else if (this.PurchaseDate.Date >= tempDate.Date)
                {
                    return RedeemStatus.Pending;   
                }
               
                return RedeemStatus.Available;
            }
        }

        private int PremiumItemDaysPendingBeforeRedeem
        {
            get
            {
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["PremiumItemDaysPendingBeforeRedeem"] != null)
                    {
                        return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["PremiumItemDaysPendingBeforeRedeem"]);
                    }
                    else
                    {
                        return 7;
                    }
                }
                catch
                {
                    return 7;
                }
            }
        }

    }
}
