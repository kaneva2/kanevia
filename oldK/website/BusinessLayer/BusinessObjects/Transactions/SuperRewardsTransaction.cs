///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class SuperRewardsTransaction
    {
        public enum eSR_AWARD_STATUS
        {
            FAIL = 0,
            SUCCEED = 1,
            INVALID_PARAMETERS = 2,
            SECURITY_FAILURE = 3,
            ALREADY_REWARDED = 4
        }

        private uint _transactionId = 0;
        private uint _srTransactionId = 0;
        private uint _creditsAwarded = 0;
        private uint _creditsAllTime = 0;
        private uint _userId = 0;
        private uint _srOfferId = 0;
        private string _securityKey = "";
        private string _requestingIPAddress = "";
        private uint _wokTransactionLogId = 0;
        private short _awardStatus = 0;
        private DateTime _awardDate;

        public SuperRewardsTransaction()
        {
        }

        /// <summary>
        /// Overloaded constructor for the User class.
        /// </summary>
        public SuperRewardsTransaction(uint TransactionId, uint SrTransactionId, uint CreditsAwarded, uint CreditsAllTime, uint UserId, uint SrOfferId,
            string SecurityKey, short AwardStatus, DateTime AwardDate, string IPAddress, uint wokTransactionLogId)
        {
            _transactionId = TransactionId;
            _srTransactionId = SrTransactionId;
            _creditsAwarded = CreditsAwarded;
            _creditsAllTime = CreditsAllTime;
            _userId = UserId;
            _srOfferId = SrOfferId;
            _securityKey = SecurityKey;
            _awardStatus = AwardStatus;
            _awardDate = AwardDate;
            _requestingIPAddress = IPAddress;
            _wokTransactionLogId = wokTransactionLogId;
        }

        public uint WokTransactionLogID
        {
            get { return _wokTransactionLogId; }
            set { _wokTransactionLogId = value; }
        }

        public string RequestingIPAddress
        {
            get { return _requestingIPAddress; }
            set { _requestingIPAddress = value; }
        }

        public uint TransactionID
        {
            get { return _transactionId; }
            set { _transactionId = value; }
        }

        public uint SuperRewardsTransactionID
        {
            get { return _srTransactionId; }
            set { _srTransactionId = value; }
        }

        public uint CreditsAwarded
        {
            get { return _creditsAwarded; }
            set { _creditsAwarded = value; }
        }

        public uint CreditsAllTime
        {
            get { return _creditsAllTime; }
            set { _creditsAllTime = value; }
        }

        public uint UserID
        {
            get { return _userId; }
            set { _userId = value; }
        }

        public uint SuperRewardsOfferID
        {
            get { return _srOfferId; }
            set { _srOfferId = value; }
        }

        public string SecurityKey
        {
            get { return _securityKey; }
            set { _securityKey = value; }
        }

        public short AwardStatus
        {
            get { return _awardStatus; }
            set { _awardStatus = value; }
        }

        public DateTime AwardDate
        {
            get { return _awardDate; }
            set { _awardDate = value; }
        }


    }
}
