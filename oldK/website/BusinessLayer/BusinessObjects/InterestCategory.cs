///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class InterestCategory
    {
        private UInt32 _IcId;
        private string _CategoryText;

        public InterestCategory(UInt32 icId, string categoryText)
        {
            this._IcId = icId;
            this._CategoryText = categoryText;
        }

        public UInt32 IcId
        {
           get {return _IcId;}
           set {_IcId = value;}
        }

        public string CategoryText
        {
           get {return _CategoryText;}
           set {_CategoryText = value;}
        }
    }
}
