///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class SitePrivilege
    {
        private int _PrivilegeId = 0;
        private string _PrivilegeName = "";
        private string _PrivilegeDescription = "";
        private bool _AdminOnly = true;

        public enum eACCESS_LEVEL
        {
            ACCESS_NONE = 1,
            ACCESS_READ = 2,
            ACCESS_FULL = 3
        }

        public enum ePRIVILEGE
        {
            COMMUNITY_PROFILE_ADMIN = 1,
            USER_PROFILE_ADMIN = 2,
            USER_DATA_ADMIN = 3,
            KANEVA_PROPRIETARY = 4,
            SITE_ADMIN = 5,
            STAR_ADMIN = 6,
            MANAGE_USERS = 11,
            MANAGE_ACCOUNT = 12,
            MANAGE_INFO = 13,
            MANAGE_STARS = 14,
            MANAGE_SECURITY = 15
        }

        public SitePrivilege()
        {
        }

        /// <summary>
        /// Overloaded constructor for the User class.
        /// </summary>
        public SitePrivilege(int PrivilegeId, string PrivilegeName, string PrivilegeDesc, bool AdminOnly)
        {
            this._PrivilegeId = PrivilegeId;
            this._PrivilegeName = PrivilegeName;
            this._PrivilegeDescription = PrivilegeDesc;
            this._AdminOnly = AdminOnly;
        }

        /// <summary>
        /// Gets or sets unique user identifier.
        /// The Identity Field Design Pattern. 
        /// </summary>
        /// 

        public int PrivilegeId
        {
            get { return _PrivilegeId; }
            set { _PrivilegeId = value; }
        }

        public string PrivilegeName
        {
            get { return _PrivilegeName; }
            set { _PrivilegeName = value; }
        }

        public string PrivilegeDescription
        {
            get { return _PrivilegeDescription; }
            set { _PrivilegeDescription = value; }
        }

        public bool AdminOnly
        {
            get { return _AdminOnly; }
            set { _AdminOnly = value; }
        }
    }
}
