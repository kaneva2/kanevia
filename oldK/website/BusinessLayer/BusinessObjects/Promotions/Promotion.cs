///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class Promotion
    {
        public enum ePROMOTION_TYPE
        {
            CREDITS = 0,
            MATURE_SUBSCRIPTION_PASS = 1,
            SPECIAL = 2,
            SPECIAL_ONE_TIME = 3,
            SPECIAL_FIRST_TIME = 4,
            REFERRAL = 5,
            SUBSCRIPTION_PASS = 6
        }

        public const int CREDITS = 1;
        public const int SPECIALS = 2;

        public const string CURR_KPOINT = "KPOINT";
        public const string CURR_MPOINT = "MPOINT";
        public const string CURR_DOLLAR = "DOLLAR";
        public const string CURR_GPOINT = "GPOINT";

        private uint _promotionId = 0;
        private string _keiPointId = CURR_KPOINT;
        private decimal _dollarAmount = 0.0m;
        private decimal _originalPrice = 0.0m;
        private decimal _keiPointAmount = 0.0m;  
	    private decimal _freePointsAwardedAmount = 0.0m;
        private string _freeKeiPointID = CURR_KPOINT; 
	    private uint _isSpecial = 0;
	    private string _promotionDescription = ""; 
	    private uint _promotionalOffersTypeId = 0;
	    private uint _wokPassGroupId = 0; 
	    private decimal _valueOfCredits = 0.0m; 
	    private uint _modifiersId = 0;
	    private uint _sku = 0; 
	    private string _bundleTitle = ""; 
	    private string _bundleSubheading1 = "";  
	    private string _bundleSubheading2 = ""; 
	    private DateTime _promotionStart; 
	    private DateTime _promotionEnd; 
	    private string _promotionListHeading = ""; 
	    private string _highlightColor = ""; 
	    private string _specialBackgroundColor = "";
	    private string _specialBackgroundImage = ""; 
	    private string _specialStickerImage = ""; 
	    private string _promotionalPackageLabel = "";
        private string _specialFontColor = "";
        private string _adRotatorPath = "";
        private uint _useDisplayOptions = 0;

        public Promotion()
        {
        }

        /// <summary>
        /// Overloaded constructor for the User class.
        /// </summary>
        public Promotion(uint PromotionId, string KeiPointId, decimal DollarAmount, decimal KeiPointAmount, decimal FreePointsAwardedAmount, string FreeKeiPointID, uint IsSpecial, string PromotionDescription,
            uint PromotionalOffersTypeId, uint WokPassGroupId, decimal ValueOfCredits, uint ModifiersId, uint Sku, string BundleTitle, string BundleSubheading1, string BundleSubheading2,
            DateTime PromotionStart, DateTime PromotionEnd, string PromotionListHeading, string HighlightColor, string SpecialBackgroundColor, string SpecialBackgroundImage,
            string SpecialStickerImage, string PromotionalPackageLabel, string SpecialFontColor, uint UseDisplayOptions, decimal OriginalPrice, string AdRotatorPath)
        {
            _promotionId = PromotionId;
            _keiPointId = KeiPointId;
            _dollarAmount = DollarAmount;
            _keiPointAmount = KeiPointAmount;  
	        _freePointsAwardedAmount = FreePointsAwardedAmount;
            _freeKeiPointID = FreeKeiPointID; 
	        _isSpecial = IsSpecial;
	        _promotionDescription = PromotionDescription; 
	        _promotionalOffersTypeId = PromotionalOffersTypeId;
	        _wokPassGroupId = WokPassGroupId; 
	        _valueOfCredits = ValueOfCredits; 
	        _modifiersId = ModifiersId;
	        _sku = Sku;
            _bundleTitle = BundleTitle; 
	        _bundleSubheading1 = BundleSubheading1;  
	        _bundleSubheading2 = BundleSubheading2;
            _promotionStart = PromotionStart; 
	        _promotionEnd = PromotionEnd; 
	        _promotionListHeading = PromotionListHeading; 
	        _highlightColor = HighlightColor; 
	        _specialBackgroundColor = SpecialBackgroundColor;
	        _specialBackgroundImage = SpecialBackgroundImage;
            _specialStickerImage = SpecialStickerImage;
            _promotionalPackageLabel = PromotionalPackageLabel;
            _specialFontColor = SpecialFontColor;
            _useDisplayOptions = UseDisplayOptions;
            _originalPrice = OriginalPrice;
            _adRotatorPath = AdRotatorPath;
        }

        /// <summary>
        /// Gets or sets unique user identifier.
        /// The Identity Field Design Pattern. 
        /// </summary>
        /// 

        public uint UseDisplayOptions
        {
            get { return _useDisplayOptions; }
            set { _useDisplayOptions = value; }
        }

        public uint PromotionId
        {
            get { return _promotionId; }
            set { _promotionId = value; }
        }

        public string KeiPointId
        {
            get { return _keiPointId; }
            set { _keiPointId = value; }
        }

        public string AdRotatorPath
        {
            get { return _adRotatorPath; }
            set { _adRotatorPath = value; }
        }

        public decimal DollarAmount
        {
            get { return _dollarAmount; }
            set { _dollarAmount = value; }
        }

        public decimal OriginalPrice
        {
            get { return _originalPrice; }
            set { _originalPrice = value; }
        }

        public decimal KeiPointAmount
        {
            get { return _keiPointAmount; }
            set { _keiPointAmount = value; }
        }

        public decimal FreePointsAwardedAmount
        {
            get { return _freePointsAwardedAmount; }
            set { _freePointsAwardedAmount = value; }
        }

        public string FreeKeiPointID
        {
            get { return _freeKeiPointID; }
            set { _freeKeiPointID = value; }
        }

        public uint IsSpecial
        {
            get { return _isSpecial; }
            set { _isSpecial = value; }
        }

        public string PromotionDescription
        {
            get { return _promotionDescription; }
            set { _promotionDescription = value; }
        }

        public uint PromotionalOffersTypeId
        {
            get { return _promotionalOffersTypeId; }
            set { _promotionalOffersTypeId = value; }
        }

        public uint WokPassGroupId
        {
            get { return _wokPassGroupId; }
            set { _wokPassGroupId = value; }
        }

        public decimal ValueOfCredits
        {
            get { return _valueOfCredits; }
            set { _valueOfCredits = value; }
        }

        public uint ModifiersId
        {
            get { return _modifiersId; }
            set { _modifiersId = value; }
        }

        public uint Sku
        {
            get { return _sku; }
            set { _sku = value; }
        }

        public string BundleTitle
        {
            get { return _bundleTitle; }
            set { _bundleTitle = value; }
        }

        public string BundleSubheading1
        {
            get { return _bundleSubheading1; }
            set { _bundleSubheading1 = value; }
        }

        public string BundleSubheading2
        {
            get { return _bundleSubheading2; }
            set { _bundleSubheading2 = value; }
        }

        public DateTime PromotionStart
        {
            get { return _promotionStart; }
            set { _promotionStart = value; }
        }

        public DateTime PromotionEnd
        {
            get { return _promotionEnd; }
            set { _promotionEnd = value; }
        }

        public string PromotionListHeading
        {
            get { return _promotionListHeading; }
            set { _promotionListHeading = value; }
        }

        public string HighlightColor
        {
            get { return _highlightColor; }
            set { _highlightColor = value; }
        }

        public string SpecialBackgroundColor
        {
            get { return _specialBackgroundColor; }
            set { _specialBackgroundColor = value; }
        }

        public string SpecialBackgroundImage
        {
            get { return _specialBackgroundImage; }
            set { _specialBackgroundImage = value; }
        }

        public string SpecialStickerImage
        {
            get { return _specialStickerImage; }
            set { _specialStickerImage = value; }
        }

        public string PromotionalPackageLabel
        {
            get { return _promotionalPackageLabel; }
            set { _promotionalPackageLabel = value; }
        }

        public string SpecialFontColor
        {
            get { return _specialFontColor; }
            set { _specialFontColor = value; }
        }

    }
}
