///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#region Include

using System;

#endregion

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class CreateWorldMessage
    {
        public int UserId { get; set; }
        public int TemplateId { get; set; }
        public string WorldName { get; set; }
        public string WorldDescription { get; set; }
        public int ParentId { get; set; }
    }
}
