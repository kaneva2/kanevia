///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#region Include

using System;

#endregion

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class AsyncTaskStatus
    {
        public string TaskKey { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
    }
}
