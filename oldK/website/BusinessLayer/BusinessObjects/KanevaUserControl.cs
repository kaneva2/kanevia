///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class KanevaUserControl
    {
        private int _UserControlId = 0;
        private int _ControlTypeId = 1;
        private string _UserControlName = "";
        private string _ControlType = "";

        public KanevaUserControl()
        {
        }

        /// <summary>
        /// Overloaded constructor for the User class.
        /// </summary>
        public KanevaUserControl(int UserControlId, int ControlTypeId, string UserControlName, string ControlType)
        {
            this._UserControlId = UserControlId;
            this._ControlTypeId = ControlTypeId;
            this._UserControlName = UserControlName;
            this._ControlType = ControlType;
        }

        /// <summary>
        /// Gets or sets unique user identifier.
        /// The Identity Field Design Pattern. 
        /// </summary>
        /// 

        public int UserControlId
        {
            get { return _UserControlId; }
            set { _UserControlId = value; }
        }

        public int ControlTypeId
        {
            get { return _ControlTypeId; }
            set { _ControlTypeId = value; }
        }

        public string UserControlName
        {
            get { return _UserControlName; }
            set { _UserControlName = value; }
        }

        public string ControlType
        {
            get { return _ControlType; }
            set { _ControlType = value; }
        }

    }
}
