///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{

    [Serializable]
    public class ViolationType
    {
        public enum eVIOLATION_TYPES
        {
            OFFENSIVE = 1,
            PORNOGRAPHIC = 2,
            INNAPPROPRIATE = 3,
            MATURE = 4
        }

        private uint _violationTypeId = 0;
        private string _violationTypeDescription = "";

        public ViolationType()
        {
        }

        public ViolationType(uint violationTypeId, string violationTypeDescription)
        {
            _violationTypeId = violationTypeId;
            _violationTypeDescription = violationTypeDescription;
        }

        public uint ViolationTypeId
        {
            get { return _violationTypeId; }
            set { _violationTypeId = value; }
        }

        public string ViolationTypeDescription
        {
            get { return _violationTypeDescription; }
            set { _violationTypeDescription = value; }
        }

    }
}
