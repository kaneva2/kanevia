///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class ViolationReport
    {
        private uint _violationId = 0;
        private uint _assetId = 0;
        private uint _wokItemId = 0;
        private uint _ownerId = 0;
        private uint _reporterId = 0;
        private uint _violationTypeId = 0;
        private uint _violationActionTypeId = 0;
        private string _memberComments = "";
        private string _csrNotes = "";
        private DateTime _reportDate;
        private DateTime _lastUpdated;
        private uint _modifiersId = 0;


        public ViolationReport()
        {
        }

        public ViolationReport(uint violationID, uint assetID, uint wokItemID, uint ownerID, uint reporterID, uint violationTypeID, uint violationActionTypeID,
            string memberComments, string csrNotes, DateTime reportDate, DateTime lastUpdated, uint modifiersId)
        {
            _violationId = violationID;
            _assetId = assetID;
            _wokItemId = wokItemID;
            _ownerId = ownerID;
            _reporterId = reporterID;
            _violationTypeId = violationTypeID;
            _violationActionTypeId = violationActionTypeID;
            _memberComments = memberComments;
            _csrNotes = csrNotes;
            _reportDate = reportDate;
            _lastUpdated = lastUpdated;
            _modifiersId = modifiersId;
        }

        public uint ViolationID
        {
            get { return _violationId; }
            set { _violationId = value; }
        }

        public uint ModifiersID
        {
            get { return _modifiersId; }
            set { _modifiersId = value; }
        }

        public uint AssetID
        {
            get { return _assetId; }
            set { _assetId = value; }
        }

        public uint WokItemId
        {
            get { return _wokItemId; }
            set { _wokItemId = value; }
        }

        public uint OwnerId
        {
            get { return _ownerId; }
            set { _ownerId = value; }
        }

        public uint ReportingUserId
        {
            get { return _reporterId; }
            set { _reporterId = value; }
        }

        public uint ViolationTypeId
        {
            get { return _violationTypeId; }
            set { _violationTypeId = value; }
        }

        public uint ViolationActionTypeId
        {
            get { return _violationActionTypeId; }
            set { _violationActionTypeId = value; }
        }

        public string MemberComments
        {
            get { return _memberComments; }
            set { _memberComments = value; }
        }

        public string CSRNotes
        {
            get { return _csrNotes; }
            set { _csrNotes = value; }
        }

        public DateTime ReportDate
        {
            get { return _reportDate; }
            set { _reportDate = value; }
        }

        public DateTime LastUpdated
        {
            get { return _lastUpdated; }
            set { _lastUpdated = value; }
        }

    }
}
