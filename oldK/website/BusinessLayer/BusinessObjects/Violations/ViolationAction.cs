///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class ViolationAction
    {
        private uint _violationActionId = 0;
        private uint _violationTypeId = 0;
        private uint _violationActionTypeId = 0;
        private uint _violationTriggerCount = 0;

        public ViolationAction()
        {
        }

        public ViolationAction(uint violationActionID, uint violationTypeID, uint violationActionTypeID, uint violationTriggerCount)
        {
            _violationActionId = violationActionID;
            _violationTypeId = violationTypeID;
            _violationActionTypeId = violationActionTypeID;
            _violationTriggerCount = violationTriggerCount;
        }

        public uint ViolationID
        {
            get { return _violationActionId; }
            set { _violationActionId = value; }
        }

        public uint ViolationTypeId
        {
            get { return _violationTypeId; }
            set { _violationTypeId = value; }
        }

        public uint ViolationActionTypeId
        {
            get { return _violationActionTypeId; }
            set { _violationActionTypeId = value; }
        }

        public uint violationTriggerCount
        {
            get { return _violationTriggerCount; }
            set { _violationTriggerCount = value; }
        }
   }
}
