///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class ViolationActionType
    {
        public enum eVIOLATION_ACTIONS
        {
            MONITOR = 1,
            SET_AS_AP = 2,
            PENDING = 3,
            REMOVE = 4,
            SET_AS_GOOD = 5
        }

        private uint _violationActionTypeId = 0;
        private string _violationActionDescription = "";

        public ViolationActionType()
        {
        }

        public ViolationActionType(uint violationActionTypeId, string violationActionDescription)
        {
            _violationActionTypeId = violationActionTypeId;
            _violationActionDescription = violationActionDescription;
        }

        public uint ViolationActionTypeId
        {
            get { return _violationActionTypeId; }
            set { _violationActionTypeId = value; }
        }

        public string ViolationActionDescription
        {
            get { return _violationActionDescription; }
            set { _violationActionDescription = value; }
        }
    }
}
