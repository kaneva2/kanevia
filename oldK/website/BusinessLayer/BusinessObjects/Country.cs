///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class Country
    {
        private string _CountryId;
        private string _Country;

        public Country (string countryId, string country)
        {
            this._CountryId = countryId;
            this._Country = country;
        }

        public string CountryId
        {
           get {return _CountryId;}
           set {_CountryId = value;}
        }

        public string Name
        {
           get {return _Country;}
           set {_Country = value;}
        }
    }
}
