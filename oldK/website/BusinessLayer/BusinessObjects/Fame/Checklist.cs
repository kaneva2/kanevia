///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class Checklist
    {
        private int _ChecklistId = 0;
        private int _RewardPacketId = 0;
        private string _Name = "";
        private bool _Completed = false;
        private decimal _PercentComplete = 0;
        private IList<ChecklistPacket> _ChecklistPackets;

        public Checklist () { }

        public Checklist (int checklistId, string name, int rewardPackeId, bool completed) 
        {
            this._ChecklistId = checklistId;
            this._RewardPacketId = rewardPackeId;
            this._Name = name;
            this._Completed = completed;
        }

        public int ChecklistId
        {
            get { return _ChecklistId; }
            set { _ChecklistId = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public bool Completed
        {
            get { return _Completed; }
            set { _Completed = value; }
        }

        public int RewardPacketId
        {
            get { return _RewardPacketId; }
            set { _RewardPacketId = value; }
        }

        public decimal PercentComplete
        {
            get 
            {
                if (_PercentComplete == 0)
                {
                    foreach (ChecklistPacket cp in ChecklistPackets)
                    {
                        if (cp.Completed)
                        {
                            _PercentComplete += cp.Percent;   
                        }
                    }
                }
                return _PercentComplete;
            }
        }

        public IList<ChecklistPacket> ChecklistPackets
        {
            get { return _ChecklistPackets; }
            set { _ChecklistPackets = value; }
        }

    }
}
