///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class ChecklistPacket : Packet
    {
        private decimal _Percent = 0;
        private bool _Completed = false;

        public ChecklistPacket () { }

        public ChecklistPacket (int packetId, int fameTypeId, string name, int pointsEarned, int rewards, 
            int timeInterval, int rewardInterval, int maxPerDay, PacketStatus status, int badgeId, 
            bool isServerOnly, decimal percent, bool completed)
        {
            this._Percent = percent;
            this._Completed = completed;

            this.PacketId = packetId;
            this.FameTypeId = fameTypeId;
            this.Name = name;
            this.PointsEarned = pointsEarned;
            this.Rewards = rewards;
            this.TimeInterval = timeInterval;
            this.RewardInterval = rewardInterval;
            this.MaxPerDay = maxPerDay;
            this.Status = status;
            this.BadgeId = badgeId;
            this.IsServerOnly = isServerOnly;
        }

        public decimal Percent
        {
            get { return _Percent; }
            set { _Percent = value; }
        }

        public bool Completed
        {
            get { return _Completed; }
            set { _Completed = value; }
        }
    }
}
