///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public enum FameTypes
    {
        World = 1,
        Dance = 2,
        Kaching  = 3,
        Fashion = 4,
        Creator = 5,
        Builder = 6,
        CoreHelp = 7
    }

    [Serializable]
    public class FameType
    {
        private int _FameTypeId;
        private string _Name;
        private int _MaxLevel;

        public FameType (int fameTypeId, string name, int maxLevel)
        {
            this._FameTypeId = fameTypeId;
            this._Name = name;
            this._MaxLevel = maxLevel;
        }

        public int FameTypeId
        {
           get {return _FameTypeId;}
           set {_FameTypeId = value;}
        }

        public string Name
        {
           get {return _Name;}
           set {_Name = value;}
        }

        public int MaxLevel
        {
            get { return _MaxLevel; }
            set { _MaxLevel = value; }
        }
    }
}
