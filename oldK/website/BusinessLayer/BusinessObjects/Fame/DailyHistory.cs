///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class DailyHistory
    {
        private int _UserId;
        private int _PacketId;
        private DateTime _DateRewarded;
        private int _FameTypeId;
        private int _PacketCount;

        public DailyHistory() { }

        public DailyHistory(int userId, int packetId, DateTime dateRewarded, int fameTypeId, int packetCount)
        {
            this._UserId = userId;
            this._PacketId = packetId;
            this._DateRewarded = dateRewarded;
            this._FameTypeId = fameTypeId;
            this._PacketCount = packetCount;
        }

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public int PacketId
        {
            get { return _PacketId; }
            set { _PacketId = value; }
        }

        public DateTime DateRewarded
        {
            get { return _DateRewarded; }
            set { _DateRewarded = value; }
        }

        public int FameTypeId
        {
            get { return _FameTypeId; }
            set { _FameTypeId = value; }
        }

        public int PacketCount
        {
            get { return _PacketCount; }
            set { _PacketCount = value; }
        }

    }
}
