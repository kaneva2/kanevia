///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class PacketHistory
    {
        private Int64 _PacketHistoryId;
        private int _UserId;
        private int _PacketId;
        private DateTime _DateRewarded;
        private int _PointsAwarded;
        private int _RewardsAwarded;
        private int _FameTypeId;
        private string _PacketName;

        public PacketHistory() { }

        public PacketHistory(Int64 packetHistoryId, int userId, int packetId, DateTime dateRewarded, int pointsAwarded, int rewardsAwarded, int fameTypeId, string packetName)
        {
            this._PacketHistoryId = packetHistoryId;
            this._UserId = userId;
            this._PacketId = packetId;
            this._DateRewarded = dateRewarded;
            this._PointsAwarded = pointsAwarded;
            this._RewardsAwarded = rewardsAwarded;
            this._FameTypeId = fameTypeId;
            this._PacketName = packetName;
        }

        public Int64 PacketHistoryId
        {
           get {return _PacketHistoryId;}
           set {_PacketHistoryId = value;}
        }

        public int UserId
        {
           get {return _UserId;}
           set {_UserId = value;}
        }

        public int PacketId
        {
           get {return _PacketId;}
           set {_PacketId = value;}
        }

        public DateTime DateRewarded
        {
           get {return _DateRewarded;}
           set {_DateRewarded = value;}
        }

        public int PointsAwarded
        {
           get {return _PointsAwarded;}
           set {_PointsAwarded = value;}
        }

        public int RewardsAwarded
        {
           get {return _RewardsAwarded;}
           set {_RewardsAwarded = value;}
        }

        public int FameTypeId
        {
           get {return _FameTypeId;}
           set {_FameTypeId = value;}
        }

        public string PacketName
        {
           get {return _PacketName;}
           set {_PacketName = value;}
        }

    }
}
