///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class Level
    {
        private int _LevelId;
        private int _FameTypeId;
        private int _LevelNumber;
        private int _StartPoints;
        private int _EndPoints;
        private int _Rewards;
        private int _NextLevelId;
        private int _MaxLevelNumber;
        private bool _SendKmail = false;

        public Level() { }

        public Level (int levelId, int fameTypeId, int levelNumber, int startPoints, int endPoints, int rewards, int nextLevelId, int maxLevelNumber, bool sendKmail)
        {
            this._LevelId = levelId;
            this._FameTypeId = fameTypeId;
            this._LevelNumber = levelNumber;
            this._StartPoints = startPoints;
            this._EndPoints = endPoints;
            this._Rewards = rewards;
            this._NextLevelId = nextLevelId;
            this._MaxLevelNumber = maxLevelNumber;
            this._SendKmail = sendKmail;
        }

        public int LevelId
        {
           get {return _LevelId;}
           set {_LevelId = value;}
        }

        public int FameTypeId
        {
           get {return _FameTypeId;}
           set {_FameTypeId = value;}
        }

        public int LevelNumber
        {
           get {return _LevelNumber;}
           set {_LevelNumber = value;}
        }

        public int StartPoints
        {
           get {return _StartPoints;}
           set {_StartPoints = value;}
        }

        public int EndPoints
        {
           get {return _EndPoints;}
           set {_EndPoints = value;}
        }

        public int Rewards
        {
           get {return _Rewards;}
           set {_Rewards = value;}
        }

        public int NextLevelId
        {
            get { return _NextLevelId; }
            set { _NextLevelId = value; }
        }

        public int MaxLevel
        {
            get { return _MaxLevelNumber; }
            set { _MaxLevelNumber = value; }
        }

        public bool SendKmail
        {
            get { return _SendKmail; }
            set { _SendKmail = value; }
        }

    }
}
