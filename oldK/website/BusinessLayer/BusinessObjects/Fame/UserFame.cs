///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class UserFame
    {
        private int _UserId;
        private int _FameTypeId = 1;
        private int _LevelId = 0;
        private int _Points = 0;
        private string _Username = "";

        private Level _level;

        public UserFame() { }

        public UserFame(int userId, int fameTypeId, int levelId, int points, string username)
        {
            this._UserId = userId;
            this._FameTypeId = fameTypeId;
            this._LevelId = levelId;
            this._Points = points;
            this._Username = username;
        }

        public int UserId
        {
           get {return _UserId;}
           set {_UserId = value;}
        }

        public int FameTypeId
        {
           get {return _FameTypeId;}
           set {_FameTypeId = value;}
        }

        public int LevelId
        {
           get {return _LevelId;}
           set {_LevelId = value;}
        }

        public int Points
        {
           get {return _Points;}
           set {_Points = value;}
        }

        public string Username
        {
            get { return _Username; }
            set { _Username = value; }
        }

        /// <summary>
        /// Level
        /// </summary>
        public Level CurrentLevel
        {
            get { return _level; }
            set { _level = value; }
        }
    }
}
