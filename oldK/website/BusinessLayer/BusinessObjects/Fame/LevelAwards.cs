///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class LevelAwards
    {
        private int _LevelAwardId = 0;
        private int _FameTypeId = 0;
        private int _LevelId = 0;
        private string _Name = "";
        private int _GlobalId = 0;
        private int _AnimationId = 0;
        private string _Gender = "U";
        private int _Quantity = 0;
        private string _NewTitle = "";

        public LevelAwards() { }

        public LevelAwards (int levelAwardId, int levelId, int fameTypeId, string Name, int globalId, int animationId, string gender, int quantity, string newTitle)
        {
            this._LevelAwardId = levelAwardId;
            this._LevelId = levelId;
            this._FameTypeId = fameTypeId;
            this._Name = Name;
            this._GlobalId = globalId;
            this._AnimationId = animationId;
            this._Gender = gender;
            this._Quantity = quantity;
            this._NewTitle = newTitle;
        }

        public int LevelAwardId
        {
           get {return _LevelAwardId;}
           set {_LevelAwardId = value;}
        }

        public int LevelId
        {
           get {return _LevelId;}
           set {_LevelId = value;}
        }

        public int FameTypeId
        {
            get { return _FameTypeId; }
            set { _FameTypeId = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int GlobalId
        {
           get {return _GlobalId;}
           set {_GlobalId = value;}
        }

        public int AnimationId
        {
           get {return _AnimationId;}
           set {_AnimationId = value;}
        }

        public string Gender
        {
           get {return _Gender;}
           set {_Gender = value;}
        }

        public int Quantity
        {
           get {return _Quantity;}
           set {_Quantity = value;}
        }

        public string NewTitle
        {
           get {return _NewTitle;}
           set {_NewTitle = value;}
        }

    }
}
