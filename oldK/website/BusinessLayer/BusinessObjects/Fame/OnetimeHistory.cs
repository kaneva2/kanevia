///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class OnetimeHistory
    {
        private int _UserId;
        private int _FameTypeId;
        private int _PacketId;
        private DateTime _DateRewarded;

        public OnetimeHistory (int userId, int fameTypeId, int packetId, DateTime dateRewarded)
        {
            this._UserId = userId;
            this._FameTypeId = fameTypeId;
            this._PacketId = packetId;
            this._DateRewarded = dateRewarded;
        }

        public int UserId
        {
           get {return _UserId;}
           set {_UserId = value;}
        }

        public int FameTypeId
        {
           get {return _FameTypeId;}
           set {_FameTypeId = value;}
        }

        public int PacketId
        {
           get {return _PacketId;}
           set {_PacketId = value;}
        }

        public DateTime DateRewarded
        {
           get {return _DateRewarded;}
           set {_DateRewarded = value;}
        }

    }
}
