///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public enum NudgeStatus
    {
        InActive = 0,
        Active = 1
    }

    [Serializable]
    public class Nudge
    {
        private int _NudgeId;
        private int _PacketId;
        private int _Rewards = -1;
        private string _Name;
        private string _Title;
        private string _Desc;
        private string _UrlText;
        private string _Url;
        private string _ChecklistText = "";
        private bool _IsComplete = false;
        private int _Impressions = 0;
        private NudgeStatus _status;

        public Nudge() { }

        public Nudge (int nudgeId, string name, string title, string desc, string urlText, string url, 
            int packetId, bool isComplete, string checklistText, int rewards, int impressions, NudgeStatus nudgeStatus)
        {
            this._NudgeId = nudgeId;
            this._Name = name;
            this._Title = title;
            this._Desc = desc;
            this._UrlText = urlText;
            this._Url = url;
            this._ChecklistText = checklistText;
            this._PacketId = packetId;
            this._IsComplete = isComplete;
            this._Impressions = impressions;
            this._Rewards = rewards;
            this._status = nudgeStatus;
        }
        
        public int NudgeId
        {
            get { return _NudgeId; }
            set { _NudgeId = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        public string Desc
        {
            get { return _Desc; }
            set { _Desc = value; }
        }

        public string UrlText
        {
            get { return _UrlText; }
            set { _UrlText = value; }
        }

        public string URL
        {
            get { return _Url; }
            set { _Url = value; }
        }

        public string ChecklistText
        {
            get { return _ChecklistText; }
            set { _ChecklistText = value; }
        }

        public int PacketId
        {
            get { return _PacketId; }
            set { _PacketId = value; }
        }

        public int Rewards
        {
            get { return _Rewards; }
            set { _Rewards = value; }
        }

        public bool IsComplete
        {
            get { return _IsComplete; }
            set { _IsComplete = value; }
        }

        public int Impressions
        {
            get { return _Impressions; }
            set { _Impressions = value; }
        }

        public NudgeStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

    }
}
