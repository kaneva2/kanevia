///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class LevelHistory
    {
        private int _UserId;
        private int _FameTypeId;
        private int _LevelId;
        private DateTime _LevelDate;
        private Int64 _SecondsToLevel;
        private int _LevelNumber;

        public LevelHistory() { }

        public LevelHistory(int userId, int fameTypeId, int levelId, DateTime levelDate, Int64 secondsToLevel, int levelNumber)
        {
            this._UserId = userId;
            this._FameTypeId = fameTypeId;
            this._LevelId = levelId;
            this._LevelDate = levelDate;
            this._SecondsToLevel = secondsToLevel;
            this._LevelNumber = levelNumber;
        }

        public int UserId
        {
           get {return _UserId;}
           set {_UserId = value;}
        }

        public int FameTypeId
        {
           get {return _FameTypeId;}
           set {_FameTypeId = value;}
        }

        public int LevelId
        {
           get {return _LevelId;}
           set {_LevelId = value;}
        }

        public DateTime LevelDate
        {
           get {return _LevelDate;}
           set {_LevelDate = value;}
        }

        public Int64 SecondsToLevel
        {
           get {return _SecondsToLevel;}
           set {_SecondsToLevel = value;}
        }

        public int LevelNumber
        {
            get { return _LevelNumber; }
            set { _LevelNumber = value; }
        }
    }
}
