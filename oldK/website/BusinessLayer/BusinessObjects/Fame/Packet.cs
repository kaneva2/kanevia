///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public enum PacketTimeInterval
    {
        OneTime = 1,
        Unlimited = 2,
        Daily = 3
    }

    public enum PacketId
    {
        WEB_OT_ADD_PICTURE = 1,
        WORLD_OT__FIRST_LOGIN = 2,
        WORLD_OT_TUTORIAL = 3,
        WEB_OT_HELP = 4,
        WORLD_OT_PLACE_OBJ = 5,
        WORLD_OT_CHANGE_CLOTHES = 6,
        WORLD_OT_CHANGE_PATTERN = 7,
        WORLD_OT_CHANGE_TV_MEDIA = 8,
        WEB_OT_ADD_THEME_BACKGROUND = 9,
        WEB_OT_UPLOAD_YOUTUBE = 10,
        BOTH_OT_NEWBIE_TASKS = 11,
        WEB_OT_FRIENDS_INVITE_10 = 12,
        WEB_OT_FRIENDS_INVITE_25 = 13,
        WEB_OT_FRIENDS_INVITE_50 = 14,
        BOTH_OT_ADD_5_FRIENDS = 15,
        WORLD_OT_USE_HANGOUT_DEED = 16,
        DANCE_UNIQUE_VISIT_10 = 17,
        DANCE_UNIQUE_VISIT_50 = 18,
        WORLD_HOME_VISIT_10 = 19,
        WORLD_HOME_VISIT_50 = 20,
        WORLD_HANGOUT_VISIT_10 = 21,
        WORLD_HANGOUT_VISIT_50 = 22,
        WORLD_RAVED_HOME = 23,
        BOTH_RAVED_PROFILE = 24,
        WORLD_RAVED_HANGOUT = 25,
        WEB_RAVED_COMMUNITY = 26,
        WORLD_GIFT = 27,
        WEB_RAVED_PATTERN = 28,
        WEB_RAVED_PHOTO = 29,
        WEB_RAVED_VIDEO = 30,
        WEB_COMM_JOIN = 31,
        WEB_INVITE_JOIN_SITE = 32,
        WORLD_INVITE_GO_INWORLD = 33,
        WEB_INVITE_SPEND_CREDIT = 34,
        WORLD_DAILY_SIGNIN = 35,
        WORLD_DAILY_10_MINS = 36,
        WORLD_DAILY_3_HOURS = 37,
        WORLD_DAILY_DANCE_10 = 38,
        WEB_DAILY_SIGNIN = 39,
        WEB_DAILY_VISIT_FORUM = 40,
        WEB_DAILY_FORUM_POSTS_3 = 41,
        WEB_DAILY_MEDIA_VIEWS_3 = 42,
        WEB_DAILY_SLOT_MACHINE = 43,
        WEB_OT_EMAIL_VALIATION = 44,
        REFERRAL_LIST_COMPLETED = 45,
        FASHION_PURCHASE_WITH_CREDS = 54, //this is a duplicate packet that gives world fame
        MEGA_RAVE_RECEIVER = 55,
        MEGA_RAVE_GIVER = 56,
        RAVE_SOMEONE = 271,
        TOUR_1ST = 400,
        TOUR_2ND = 401,
        TOUR_3RD = 402,
        TOUR_4TH10TH = 403,
        ADD_FIRST_FRIEND = 406,
        ADD_3_FRIENDS = 407,
        FIRST_VISITOR_HOME = 428,
        FIRST_10_VISITORS_HOME = 429,
        BUY_SHOP_1_ITEM = 417,
        BUY_SHOP_3_ITEMS = 418,
        BUY_SHOP_5_ITEMS = 419,
        BUY_SHOP_10_ITEMS = 420,
        BUY_ANIMATION = 421,
        VIEW_SOMEONE_PROFILE = 425,
        RAVE_CLOTHING_1 = 438,
        RAVE_PLACE_1 = 439,
        BLASTED_ON_SITE = 440,
        HOME_RAVED_1 = 415,
        WELCOME_KANEVA = 442,
        LOGIN_WOK_2_DAYS_IN_A_ROW = 405,
        LOGIN_WOK_3_DAYS_IN_A_ROW = 443,
        LOGIN_WOK_5_DAYS_IN_A_ROW = 444,
        ADD_10_FRIENDS = 445,
        ADD_20_FRIENDS = 446,
        SEND_BLAST_FROM_APP = 447,
        BUY_CREDITS = 437,
        FIRST_20_VISITORS_HOME = 451,
        FIRST_50_VISITORS_HOME = 452,
        FIRST_100_VISITORS_HOME = 453,
        FIRST_250_VISITORS_HOME = 454,
        FIRST_500_VISITORS_HOME = 455,
        FIRST_1000_VISITORS_HOME = 456,
        HOME_RAVED_10 = 459,
        HOME_RAVED_20 = 460,
        HOME_RAVED_50 = 461,
        HOME_RAVED_100 = 462,
        HOME_RAVED_250 = 463,
        HOME_RAVED_500 = 464,
        HOME_RAVED_1000 = 465,
        BUY_SHOP_20_ITEMS = 472,
        BUY_SHOP_50_ITEMS = 473,
        BUY_SHOP_100_ITEMS = 474,
        BUY_SHOP_250_ITEMS = 475,
        BUY_SHOP_500_ITEMS = 476,
        BUY_SHOP_1000_ITEMS = 477,
        UPLOAD_MEDIA_EVERY_10 = 478,
        EVERY_10_VISITORS_COMMUNITY = 479,
        EVERY_35_VISITORS_COMMUNITY = 480,
        BUY_VIP_PASS = 482,
        BUY_AN_ITEM = 484,
        IN_WORLD_15_MINUTES = 485,
        MAKE_A_GAME = 489,
        LOGIN_WOK_1_DAY_IN_A_ROW = 490,
        CONNECT_TO_FACEBOOK = 491,
        KILLED_1_MONSTER = 592,
        CRAFTED_1_OBJECT = 596,
        PLACE_1_OBJECT = 600,
        HARVESTED_1_OBJECT = 612,
        DEVELOPER_TUTORIAL_READ = 5000,
        DEVELOPER_FIRST_MENU_EDITED = 5001,
        DEVELOPER_FIRST_SCRIPT_EDITED = 5002,
        DEVELOPER_FIRST_BADGE_ADDED = 5003,
        DEVELOPER_FIRST_PREMIUM_ADDED = 5004,
        DEVELOPER_FIRST_LEADERBOARD_MODDED = 5005,
        DEVELOPER_FIRST_MODEL_IMPORTED = 5006,
        DEVELOPER_FIRST_PROFILE_SETUP = 5007,
        DEVELOPER_FIRST_PUBLISH = 5008,
        DEVELOPER_FIRST_DEV_CHALLENGE = 5009,
        DEVELOPER_UNIQUE_VISITORS_1 = 5010,
        DEVELOPER_UNIQUE_VISITORS_100 = 5011,
        DEVELOPER_UNIQUE_VISITORS_500 = 5012,
        DEVELOPER_UNIQUE_VISITORS_1000 = 5013,
        DEVELOPER_UNIQUE_VISITORS_5000 = 5014,
        DEVELOPER_FIRST_CUSTOMIZED_ZONE = 5015,
        DEVELOPER_FIRST_PLACED_OBJECT = 5016,
        DEVELOPER_PREMIUM_ITEM_SALE = 5017,
        DEVELOPER_EVERY_10_VISITORS = 5018,
        DEVELOPER_UNIQUE_VISITORS_10000 = 5019,
        BUILDER_WELCOME							= 6000,
        BUILDER_PLACED_FIRST_OBJECT				= 6001,
        BUILDER_MOVED_AN_OBJECT					= 6002,
        BUILDER_ROTATED_AN_OBJECT				= 6003,
        BUILDER_MULTISELECTED_OBJECTS			= 6004,
        BUILDER_BOUGHT_A_ZONE					= 6005,
        BUILDER_SOLD_1ST_ZONE					= 6006,
        BUILDER_SOLD_10_ZONES_FOR_CREDITS		= 6007,
        BUILDER_SOLD_100_ZONES_FOR_CREDITS		= 6008,
        BUILDER_SOLD_500_ZONES_FOR_CREDITS		= 6009,
        BUILDER_SOLD_1000_ZONES_FOR_CREDITS		= 6010,
        BUILDER_SOLD_10000_ZONES_FOR_CREDITS	= 6011,
        BUILDER_SOLD_3_ZONES_FOR_REWARDS		= 6012,
        BUILDER_SOLD_10_ZONES_FOR_REWARDS		= 6013,
        BUILDER_SOLD_25_ZONES_FOR_REWARDS		= 6014,
        BUILDER_SOLD_100_ZONES_FOR_REWARDS		= 6015,
        BUILDER_PLACED_10_ITEMS					= 6016,
        BUILDER_PLACED_30_ITEMS					= 6017,
        BUILDER_PLACED_100_ITEMS				= 6018,
        BUILDER_PLACED_500_ITEMS				= 6019,
        BUILDER_PLACED_1000_ITEMS				= 6020,
        BUILDER_DAILY_BONUS						= 6021,
        BUILDER_ACTIVE_TIME						= 6022,
        BUILDER_PLACED_AN_OBJECT                = 6023
    }

    public enum PacketIdFashion
    {
        RECEIVE_RAVE = 281,
        PURCHASE_WITH_REWARDS = 291,
        PURCHASE_WITH_CREDS = 301,
        PURCHASED_ITEM = 311,
        TRY_ON = 321
    }

    public enum PacketStatus
    {
        InActive = 0,
        Active = 1
    }

    [Serializable]
    public class Packet
    {
        private int _PacketId;
        private int _FameTypeId;
        private string _Name;
        private int _PointsEarned;
        private int _Rewards;
        private int _TimeInterval;
        private int _RewardInterval;
        private int _MaxPerDay;
        private PacketStatus _status;
        private int _bagdeId;
        private bool _IsServerOnly;

        public Packet() { }

        public Packet(int packetId, int fameTypeId, string name, int pointsEarned, int rewards, int timeInterval, int rewardInterval, int maxPerDay, PacketStatus status, int badgeId, bool isServerOnly)
        {
            this._PacketId = packetId;
            this._FameTypeId = fameTypeId;
            this._Name = name;
            this._PointsEarned = pointsEarned;
            this._Rewards = rewards;
            this._TimeInterval = timeInterval;
            this._RewardInterval = rewardInterval;
            this._MaxPerDay = maxPerDay;
            this._status = status;
            this._bagdeId = badgeId;
            this._IsServerOnly = isServerOnly;
        }

        public int PacketId
        {
           get {return _PacketId;}
           set {_PacketId = value;}
        }

        public int FameTypeId
        {
           get {return _FameTypeId;}
           set {_FameTypeId = value;}
        }

        public string Name
        {
           get {return _Name;}
           set {_Name = value;}
        }

        public int PointsEarned
        {
           get {return _PointsEarned;}
           set {_PointsEarned = value;}
        }

        public int Rewards
        {
           get {return _Rewards;}
           set {_Rewards = value;}
        }

        public int TimeInterval
        {
           get {return _TimeInterval;}
           set {_TimeInterval = value;}
        }

        public int RewardInterval
        {
            //If reward interval is 0, should be treated like reward interval = 1
           get {return Math.Max(1, _RewardInterval);}
           set {_RewardInterval = value;}
        }

        public int MaxPerDay
        {
            get { return _MaxPerDay; }
            set { _MaxPerDay = value; }
        }

        public PacketStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public int BadgeId
        {
            get { return _bagdeId; }
            set { _bagdeId = value; }
        }

        public bool IsServerOnly
        {
            get { return _IsServerOnly; }
            set { _IsServerOnly = value; }
        }
        
        /// <summary>
        /// Factory function that returns a "safe" blank packet for use when one is required
        /// syntatically but may be blank.
        /// </summary>
        public static Packet GetDummyPacket(int fameTypeId)
        {
            Packet p = new Packet();
            p.Name = "Dummy Packet";
            p.FameTypeId = fameTypeId;
            return p;
        }
    }
}
