///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class CommunityStats
    {
        private int _ChannelId;
        private int _NumberOfViews;
        private int _NumberOfMembers;
        private int _NumberOfDiggs;
        private int _NumberTimesShared;
        private int _NumberOfPendingMembers;
        private UInt32 _NumberPosts7Days;
        private uint gameCount;
        private uint videoCount;
        private uint musicCount;
        private uint photoCount;
        private uint patternCount;
        private uint tvCount;
        private uint widgetCount;
        private uint blogCount;     

        public CommunityStats() { }

        public CommunityStats (int channelId, int numberOfViews, int numberOfMembers, int numberOfDiggs, int numberTimesShared, int numberOfPendingMembers, UInt32 numberPosts7Days,
            uint gameCount, uint videoCount, uint musicCount, uint photoCount, uint patternCount, uint tvCount, uint widgetCount, uint blogCount)
        {
            this._ChannelId = channelId;
            this._NumberOfViews = numberOfViews;
            this._NumberOfMembers = numberOfMembers;
            this._NumberOfDiggs = numberOfDiggs;
            this._NumberTimesShared = numberTimesShared;
            this._NumberOfPendingMembers = numberOfPendingMembers;
            this._NumberPosts7Days = numberPosts7Days;
            this.gameCount = gameCount;
            this.videoCount = videoCount;
            this.musicCount = musicCount;
            this.photoCount = photoCount;
            this.patternCount = patternCount;
            this.tvCount = tvCount;
            this.widgetCount = widgetCount;
            this.blogCount = blogCount;
        }

        public virtual int ChannelId
        {
           get {return _ChannelId;}
           set {_ChannelId = value;} 
        }

        public virtual int NumberOfViews
        {
           get {return _NumberOfViews;}
           set {_NumberOfViews = value;}
        }

        public virtual int NumberOfMembers
        {
           get {return _NumberOfMembers;}
           set {_NumberOfMembers = value;}
        }

        public virtual int NumberOfDiggs
        {
           get {return _NumberOfDiggs;}
           set {_NumberOfDiggs = value;}
        }

        public virtual int NumberTimesShared
        {
           get {return _NumberTimesShared;}
           set {_NumberTimesShared = value;}
        }

        public virtual int NumberOfPendingMembers
        {
           get {return _NumberOfPendingMembers;}
           set {_NumberOfPendingMembers = value;}
        }

        public virtual UInt32 NumberPosts7Days
        {
           get {return _NumberPosts7Days;}
           set {_NumberPosts7Days = value;}
        }

        public virtual uint GameCount
        {
            get { return gameCount; }
            set { gameCount = value; }
        }

        public virtual uint VideoCount
        {
            get { return videoCount; }
            set { videoCount = value; }
        }

        public virtual uint MusicCount
        {
            get { return musicCount; }
            set { musicCount = value; }
        }

        public virtual uint PhotoCount
        {
            get { return photoCount; }
            set { photoCount = value; }
        }

        public virtual uint PatternCount
        {
            get { return patternCount; }
            set { patternCount = value; }
        }

        public virtual uint TvCount
        {
            get { return tvCount; }
            set { tvCount = value; }
        }

        public virtual uint WidgetCount
        {
            get { return widgetCount; }
            set { widgetCount = value; }
        }

        public virtual uint BlogCount
        {
            get { return blogCount; }
            set { blogCount = value; }
        }

    }
}
