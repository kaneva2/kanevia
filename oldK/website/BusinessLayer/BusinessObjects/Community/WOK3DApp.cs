///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class WOK3DApp
    {
        private int gameId = 0;
        private int gameStatusId = 0;
		private int minutesFromLastPing = 0;
		private int serverStatusId = 0;
        private int gameAccessId = 0;
        private bool isIncubatorHosted = false;
        
        public WOK3DApp () { }

        public WOK3DApp (int gameId, int gameStatusId)
        {
            this.gameId = gameId;
            this.gameStatusId = gameStatusId;
        }

        public virtual int GameId
        {
            get { return gameId; }
            set { gameId = value; }
        }

        public virtual int GameStatusId
        {
            get { return gameStatusId; }
            set { gameStatusId = value; }
        }

		public virtual int MinutesFromLastPing
		{
			get { return minutesFromLastPing; }
			set { minutesFromLastPing = value; }
		}

		public virtual int ServerStatusId
		{
			get { return serverStatusId; }
			set { serverStatusId = value; }
		}

        public virtual int GameAccessId
        {
            get { return gameAccessId; }
            set { gameAccessId = value; }
        }

        public virtual bool IsIncubatorHosted
        {
            get { return isIncubatorHosted; }
            set { isIncubatorHosted = value; }
        }
    }
}
