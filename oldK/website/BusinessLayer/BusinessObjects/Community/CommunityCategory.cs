///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class CommunityCategory
    {
        private int _CategoryId = 0;
        private string _CategoryName = "";
        private string _Description = "";

        public CommunityCategory() { }

        public CommunityCategory(int categoryId, string categoryName, string description)
        {
            this._CategoryId = categoryId;
            this._CategoryName = categoryName;
            this._Description = description;
        }

        public int CategoryId
        {
            get {return _CategoryId;}
            set {_CategoryId = value;}
        }

        public string CategoryName
        {
            get {return _CategoryName;}
            set {_CategoryName = value;}
        }

        public string Description
        {
            get {return _Description;}
            set {_Description = value;}
        }
    }
}

