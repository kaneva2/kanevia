///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class CommunityMember
    {
        public enum CommunityMemberStatus
        {
            ACTIVE = 1,
            PENDING = 2,
            LOCKED = 3,
            DELETED = 4,
            REJECTED = 5,
            PENDINGPAYMENT = 6
        }

        public enum CommunityMemberAccountType
        {
            OWNER = 1,
            MODERATOR = 2,
            SUBSCRIBER = 3
        }

        private int _CommunityId = 0;
        private int _UserId = 0;
        private string _UserName;
        private string _DisplayName;
        private int _AccountTypeId = 0; //(int) CommunityMemberAccountType.SUBSCRIBER;
        private DateTime _AddedDate;
        private UInt32 _StatusId = 0; //(UInt32) CommunityMemberStatus.DELETED;
        private string _Newsletter;
        private int _InvitedByUserId = 0;
        private string _AllowAssetUploads;
        private string _AllowForumUse;
        private string _Keywords;
        private int _Notifications;
        private int _ItemNotify;
        private int _ItemReviewNotify;
        private int _BlogNotify;
        private int _BlogCommentNotify;
        private int _PostNotify;
        private int _ReplyNotify;
        private bool _HasEditRights = false;
        private bool _ActiveMember = false;
        private string _ThumbnailSquarePath = "";
        private string _ThumbnailSmallPath = "";
        private string _ThumbnailMediumPath = "";
        private string _Gender;
        private int _Online;
        private bool _IsMatureProfile = false;
        private string _Email = "";

        private FacebookSettings _facebookSettings = new FacebookSettings ();
        private NotificationPreferences _notificationPreferences = new NotificationPreferences();

        public CommunityMember() { }

        public CommunityMember(int communityId, int userId, string userName, string displayName, int accountTypeId, DateTime addedDate,
            UInt32 statusId, string newsletter, int invitedByUserId, string allowAssetUploads,
            string allowForumUse, string keywords, int notifications, int itemNotify,
            int itemReviewNotify, int blogNotify, int blogCommentNotify, int postNotify,
            int replyNotify, bool hasEditRights, bool activeMember,
            string thumbnailSmallPath, string thumbnailMediumPath, string thumbnailSquarePath, string gender, 
            int online, bool isMatureProfile, string email)
        {
            this._CommunityId = communityId;
            this._UserId = userId;
            this._UserName = userName;
            this._DisplayName = displayName;
            this._AccountTypeId = accountTypeId;
            this._AddedDate = addedDate;
            this._StatusId = statusId;
            this._Newsletter = newsletter;
            this._InvitedByUserId = invitedByUserId;
            this._AllowAssetUploads = allowAssetUploads;
            this._AllowForumUse = allowForumUse;
            this._Keywords = keywords;
            this._Notifications = notifications;
            this._ItemNotify = itemNotify;
            this._ItemReviewNotify = itemReviewNotify;
            this._BlogNotify = blogNotify;
            this._BlogCommentNotify = blogCommentNotify;
            this._PostNotify = postNotify;
            this._ReplyNotify = replyNotify;
            this._HasEditRights = hasEditRights;
            this._ActiveMember = activeMember;
            this._ThumbnailSmallPath = thumbnailSmallPath;
            this._ThumbnailMediumPath = thumbnailMediumPath;
            this._ThumbnailSquarePath = thumbnailSquarePath;
            this._Gender = gender;
            this._Online = online;
            this._IsMatureProfile = isMatureProfile;
            this._Email = email;
        }


        public int CommunityId
        {
            get { return _CommunityId; }
            set { _CommunityId = value; }
        }

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        public string DisplayName
        {
            get { return _DisplayName; }
            set { _DisplayName = value; }
        }

        public int AccountTypeId
        {
            get { return _AccountTypeId; }
            set { _AccountTypeId = value; }
        }

        public DateTime AddedDate
        {
            get { return _AddedDate; }
            set { _AddedDate = value; }
        }

        public UInt32 StatusId
        {
            get { return _StatusId; }
            set { _StatusId = value; }
        }

        public string Newsletter
        {
            get { return _Newsletter; }
            set { _Newsletter = value; }
        }

        public int InvitedByUserId
        {
            get { return _InvitedByUserId; }
            set { _InvitedByUserId = value; }
        }

        public string AllowAssetUploads
        {
            get { return _AllowAssetUploads; }
            set { _AllowAssetUploads = value; }
        }

        public string AllowForumUse
        {
            get { return _AllowForumUse; }
            set { _AllowForumUse = value; }
        }

        public string Keywords
        {
            get { return _Keywords; }
            set { _Keywords = value; }
        }

        public int Notifications
        {
            get { return _Notifications; }
            set { _Notifications = value; }
        }

        public int ItemNotify
        {
            get { return _ItemNotify; }
            set { _ItemNotify = value; }
        }

        public int ItemReviewNotify
        {
            get { return _ItemReviewNotify; }
            set { _ItemReviewNotify = value; }
        }

        public int BlogNotify
        {
            get { return _BlogNotify; }
            set { _BlogNotify = value; }
        }

        public int BlogCommentNotify
        {
            get { return _BlogCommentNotify; }
            set { _BlogCommentNotify = value; }
        }

        public int PostNotify
        {
            get { return _PostNotify; }
            set { _PostNotify = value; }
        }

        public int ReplyNotify
        {
            get { return _ReplyNotify; }
            set { _ReplyNotify = value; }
        }

        public bool HasEditRights
        {
            get { return _HasEditRights; }
            set { _HasEditRights = value; }
        }

        public bool ActiveMember
        {
            get { return _ActiveMember; }
            set { _ActiveMember = value; }
        }

        public string ThumbnailSquarePath
        {
            get { return _ThumbnailSquarePath; }
            set { _ThumbnailSquarePath = value; }
        }

        public string ThumbnailSmallPath
        {
            get { return _ThumbnailSmallPath; }
            set { _ThumbnailSmallPath = value; }
        }

        public string ThumbnailMediumPath
        {
            get { return _ThumbnailMediumPath; }
            set { _ThumbnailMediumPath = value; }
        }

        public string Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }
    
        public int Online
        {
            get { return _Online; }
            set { _Online = value; }
        }

        public bool IsMatureProfile
        {
            get { return _IsMatureProfile; }
            set { _IsMatureProfile = value; }
        }

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        public FacebookSettings FacebookSettings
        {
            get { return _facebookSettings; }
            set { _facebookSettings = value; }
        }

        /// <summary>
        /// Preference
        /// </summary>
        public NotificationPreferences NotificationPreference
        {
            get { return _notificationPreferences; }
            set { _notificationPreferences = value; }
        }

    }
}
