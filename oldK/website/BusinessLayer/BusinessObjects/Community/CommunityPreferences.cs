///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class CommunityPreferences
    {
        private int communityPreferenceId;
        private int communityId;
        private bool showGender;
        private bool showLocation;
        private bool showAge;

        public CommunityPreferences() 
        {
            this.communityPreferenceId = 0;
            this.communityId = 0;
            this.showGender = true;
            this.showLocation = true;
            this.showAge = true;
        }

        public CommunityPreferences (int communityPreferenceId, int communityId, bool showGender, bool showLocation, bool showAge)
        {
            this.communityPreferenceId = communityPreferenceId;
            this.communityId = communityId;
            this.showGender = showGender;
            this.showLocation = showLocation;
            this.showAge = showAge;
        }

        public virtual int CommunityPreferenceId
        {
           get {return communityPreferenceId;}
           set {communityPreferenceId = value;}
        }

        public virtual int CommunityId
        {
           get {return communityId;}
           set {communityId = value;}
        }

        public virtual bool ShowGender
        {
           get {return showGender;}
           set {showGender = value;}
        }

        public virtual bool ShowLocation
        {
           get {return showLocation;}
           set {showLocation = value;}
        }

        public virtual bool ShowAge
        {
           get {return showAge;}
           set {showAge = value;}
        }

    }
}
