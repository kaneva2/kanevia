///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public class ZoneIndex
    {
        public ZoneIndex(Int32 id)
        {
            m_id = id;
        }

        public Int32 ZoneType
        {
            get { return m_id >> 28; }
        }

        public Int32 ZoneIndexPlain
        {
            get { return m_id & 0xfff; }
        }

        public Int32 ZoneIdx
        {
            get { return m_id; }
        }

        public bool IsPermanent
        {
            get { return ZoneType == (int)WOK3DPlace.eZoneType.PERMANENT; }
        }

        public bool IsBroadband
        {
            get { return ZoneType == (int)WOK3DPlace.eZoneType.HANGOUT; }
        }

        public bool IsApartment
        {
            get { return ZoneType == (int)WOK3DPlace.eZoneType.HOUSING; }
        }

        public bool IsArena
        {
            get { return ZoneType == (int)WOK3DPlace.eZoneType.ARENA; }
        }

        public bool IsGroup
        {
            get { return ZoneType == (int)WOK3DPlace.eZoneType.PERMANENT; }
        }

        public bool IsGuild
        {
            get { return ZoneType == (int)WOK3DPlace.eZoneType.PERMANENT; }
        }

        private Int32 m_id;
    }
}
