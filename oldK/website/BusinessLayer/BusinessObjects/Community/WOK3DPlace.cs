///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class WOK3DPlace
    {
        private int _ChannelZoneId = 0;
        private int _zoneIndex = 0;
        private int _ZoneInstanceId = 0;
        private int _ZoneType = 0;
        private int _OwnerUserId = 0;
        private string _Name = "";
        private string _TiedToServer = "";
        private int _ServerId = 0;
        private int _Raves = 0;
        private int _SpinDownDelayMinutes = -1;
        private int _ScriptZoneServerId = 0;    // script_zones.server_id

        public enum eZoneType
        {
            HOUSING = 3,
            PERMANENT = 4,
            ARENA = 5,
            HANGOUT = 6
        }


        public WOK3DPlace () { }

        public WOK3DPlace(int channelZoneId, int zoneIndex, int zoneInstanceId, int zoneType, int ownerUserId, string name, int raves, int serverId, string tiedToServer, int spinDownDelayMinutes, int scriptZoneServerId)
        {
            this._ChannelZoneId = channelZoneId;
            this._zoneIndex = zoneIndex;
            this._ZoneInstanceId = zoneInstanceId;
            this._ZoneType = zoneType;
            this._OwnerUserId = ownerUserId;
            this._Name = name;
            this._Raves = raves;
            this._ServerId = serverId;
            this._TiedToServer = tiedToServer;
            this._SpinDownDelayMinutes = spinDownDelayMinutes;
            this._ScriptZoneServerId = scriptZoneServerId;
        }

        public int ChannelZoneId
        {
            get { return _ChannelZoneId; }
            set { _ChannelZoneId = value; }
        }

        public int ZoneIndex
        {
            get { return _zoneIndex; }
            set { _zoneIndex = value; }
        }

        public int ZoneIndexPlain
        {
            get { return _zoneIndex & 0xfff; }
        }

        public int ZoneInstanceId
        {
            get { return _ZoneInstanceId; }
            set { _ZoneInstanceId = value; }
        }

        public int ZoneType
        {
            get { return _ZoneType; }
            set { _ZoneType = value; }
        }

        public int OwnerUserId
        {
            get { return _OwnerUserId; }
            set { _OwnerUserId = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int ServerId
        {
            get { return _ServerId; }
            set { _ServerId = value; }
        }

        public string TiedToServer
        {
            get { return _TiedToServer; }
            set { _TiedToServer = value; }
        }

        public int Raves
        {
            get { return _Raves; }
            set { _Raves = value; }
        }

        public int SpinDownDelayMinutes
        {
            get { return _SpinDownDelayMinutes; }
        }

        public int ScriptZoneServerId
        {
            get { return _ScriptZoneServerId; }
        }
    }
}
