///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class ArchivedWorld
    {
        public int CommunityId { get; set; }
        public string Name { get; set; }
        public int CreatorId { get; set; }
        public string CreatorUsername { get; set; }
        public string ArchivedVia { get; set; }
        public DateTime ArchivedOn { get; set; }
    }
}
