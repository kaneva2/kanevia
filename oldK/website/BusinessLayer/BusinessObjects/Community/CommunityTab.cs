///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class CommunityTab
    {
        private int tab_id = 0;
        private int display_order = 0;
        private int community_id = 0;
        private int community_type_id = 0;
        private string tab_name = "";
        private bool is_configurable = false;
        private bool is_default = false;
        
        public CommunityTab () {}

        public CommunityTab (int tabId, string tabName, int displayOrder, int communityId, 
            int communityTypeId, bool isConfigurable)
             :
            
            this (tabId, tabName, displayOrder, communityId, communityTypeId, isConfigurable, false) {}


        public CommunityTab (int tabId, string tabName, int displayOrder, int communityId, 
            int communityTypeId, bool isConfigurable, bool isDefault)
        {
            this.tab_id = tabId;
            this.tab_name = tabName;
            this.display_order = displayOrder;
            this.community_type_id = communityTypeId;
            this.community_id = communityId;
            this.is_configurable = isConfigurable;
            this.is_default = isDefault;
        }

        public int TabId
        {
            get { return this.tab_id; }
            set { this.tab_id = value; }
        }

        public string TabName
        {
            get { return this.tab_name; }
            set { this.tab_name = value; }
        }

        public int DisplayOrder
        {
            get { return this.display_order; }
            set { this.display_order = value; }
        }

        public int CommunityId
        {
            get { return this.community_id; }
            set { this.community_id = value; }
        }
        
        public int CommunityTypeId
        {
            get { return this.community_type_id; }
            set { this.community_type_id = value; }
        }
        
        public bool IsConfigurable
        {
            get { return this.is_configurable; }
            set { this.is_configurable = value; }
        }
        
        public bool IsDefault
        {
            get { return this.is_default; }
            set { this.is_default = value; }
        }
    }
}
