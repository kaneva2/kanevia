///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    public enum CommunityStatus
    {
        ACTIVE = 1,
        LOCKED = 2,
        DELETED = 3,
        NEW = 4,
        IMPORT = 5
    }
    public enum CommunityType
    {
        USER = 1,
        COMMUNITY = 2,
        APP_3D = 3,
        FAME = 4,
        HOME = 5, 
        TRY_ON = 6
    }
    public enum CommunityAccessReason
    {
        ACCESS_ALLOWED = 0,
        OVER_21_REQUIRED = 1,
        ACCESS_PASS_REQUIRED = 2,
        VIP_PASS_REQUIRED = 3,
        MEMBERSHIP_REQUIRED = 4,
        FRIENDSHIP_REQUIRED = 5,
        OWNER_ACCESS_ONLY = 6,
        USER_BLOCKED = 7
    }


    [Serializable]
    public class Community
    {
        private int communityId = 0;
        private string nameNoSpaces = "";
        private string url = "";
        private int creatorId = 0;
        private DateTime createdDate;
        private DateTime lastEdit;
        private int statusId = (int) CommunityStatus.ACTIVE;
        private string email;
        private string isPublic = "N";
        private string isAdult = "Y";
        private string thumbnailPath;
        private string thumbnailSmallPath;
        private string thumbnailMediumPath;
        private string thumbnailLargePath;
        private string thumbnailXlargePath;
        private string thumbnailType;
        private int allowPublishing;
        private int allowMemberEvents;
        private int isPersonal = 0;
        private UInt32 templateId = 0;
        private bool over21Required = false;
        private bool hasThumbnail = false;
        private string name = "";
        private string description = "";
        private string backgroundRGB = "";
        private string backgroundImage = "";
        private string creatorUsername = "";
        private string creatorNameNoSpaces = "";
        private string creatorThumbnailSmallPath = "";
        private string keywords = "";
        private int placeTypeId;
        private int communityTypeId;

        // Extra
        private string ownerZipCode = "";
        private int ownerAge = 0;
        private string ownerGender = "";
        private string ownerCountry = "";
        private string ownerLocation = "";
        private bool ownerMatureProfile = false;
        private int ownerFriendCount = 0;

        private CommunityStats stats;
        private CommunityPreferences preferences;
        private IList<CommunityMember> memberList;
        private WOK3DApp wok3DApp;

        public Community() { }

        public Community(int communityId, string nameNoSpaces, string url, int creatorId,
            DateTime createdDate, DateTime lastEdit, int statusId, string email, string isPublic,
            string isAdult, string thumbnailPath, string thumbnailSmallPath, string thumbnailMediumPath,
            string thumbnailLargePath, string thumbnailXlargePath, string thumbnailType, int allowPublishing,
            int allowMemberEvents, int isPersonal, UInt32 templateId, bool over21Required, bool hasThumbnail, string name,
            string description, string backgroundRGB, string backgroundImage, string creatorUsername, string keywords,
            string ownerZipCode, int ownerAge, string ownerGender, string ownerCountry, string ownerLocation, bool ownerMatureProfile,
            int ownerFriendCount, int placeTypeId, int communityTypeId)
        {
            this.communityId = communityId;
            this.nameNoSpaces = nameNoSpaces;
            this.url = url;
            this.creatorId = creatorId;
            this.createdDate = createdDate;
            this.lastEdit = lastEdit;
            this.statusId = statusId;
            this.email = email;
            this.isPublic = isPublic;
            this.isAdult = isAdult;
            this.thumbnailPath = thumbnailPath;
            this.thumbnailSmallPath = thumbnailSmallPath;
            this.thumbnailMediumPath = thumbnailMediumPath;
            this.thumbnailLargePath = thumbnailLargePath;
            this.thumbnailXlargePath = thumbnailXlargePath;
            this.thumbnailType = thumbnailType;
            this.allowPublishing = allowPublishing;
            this.allowMemberEvents = allowMemberEvents;
            this.isPersonal = isPersonal;
            this.templateId = templateId;
            this.over21Required = over21Required;
            this.hasThumbnail = hasThumbnail;
            this.name = name;
            this.description = description;
            this.backgroundRGB = backgroundRGB;
            this.backgroundImage = backgroundImage;
            this.creatorUsername = creatorUsername;
            this.keywords = keywords;
            this.placeTypeId = placeTypeId;
            this.communityTypeId = communityTypeId;

            // Extra
            this.ownerZipCode = ownerZipCode;
            this.ownerAge = ownerAge;
            this.ownerGender = ownerGender;
            this.ownerCountry = ownerCountry;
            this.ownerLocation = ownerLocation;
            this.ownerMatureProfile = ownerMatureProfile;
            this.ownerFriendCount = ownerFriendCount;
        }

        public int CommunityId
        {
            get { return this.communityId; }
            set { this.communityId = value; }
        }

        public int PlaceTypeId
        {
            get { return this.placeTypeId; }
            set { this.placeTypeId = value; }
        }

        public int CommunityTypeId
        {
            get { return this.communityTypeId; }
            set { this.communityTypeId = value; }
        }

        public string NameNoSpaces
        {
            get { return this.nameNoSpaces; }
            set { this.nameNoSpaces = value; }
        }

        public string Url
        {
            get { return this.url; }
            set { this.url = value; }
        }

        public int CreatorId
        {
            get { return this.creatorId; }
            set { this.creatorId = value; }
        }

        public DateTime CreatedDate
        {
            get { return this.createdDate; }
            set { this.createdDate = value; }
        }

        public DateTime LastEdit
        {
            get { return this.lastEdit; }
            set { this.lastEdit = value; }
        }

        public int StatusId
        {
            get { return this.statusId; }
            set { this.statusId = value; }
        }

        public string Email
        {
            get { return this.email; }
            set { this.email = value; }
        }

        public string IsPublic
        {
            get { return this.isPublic; }
            set { this.isPublic = value; }
        }

        public string IsAdult
        {
            get { return this.isAdult; }
            set { this.isAdult = value; }
        }

        public string ThumbnailPath
        {
            get { return this.thumbnailPath; }
            set { this.thumbnailPath = value; }
        }

        public string ThumbnailSmallPath
        {
            get { return this.thumbnailSmallPath; }
            set { this.thumbnailSmallPath = value; }
        }

        public string ThumbnailMediumPath
        {
            get { return this.thumbnailMediumPath; }
            set { this.thumbnailMediumPath = value; }
        }

        public string ThumbnailLargePath
        {
            get { return this.thumbnailLargePath; }
            set { this.thumbnailLargePath = value; }
        }

        public string ThumbnailXlargePath
        {
            get { return this.thumbnailXlargePath; }
            set { this.thumbnailXlargePath = value; }
        }

        public string ThumbnailType
        {
            get { return this.thumbnailType; }
            set { this.thumbnailType = value; }
        }

        public int AllowPublishing
        {
            get { return this.allowPublishing; }
            set { this.allowPublishing = value; }
        }

        public int AllowMemberEvents
        {
            get { return this.allowMemberEvents; }
            set { this.allowMemberEvents = value; }
        }

        public int IsPersonal
        {
            get { return this.isPersonal; }
            set { this.isPersonal = value; }
        }

        public UInt32 TemplateId
        {
            get { return this.templateId; }
            set { this.templateId = value; }
        }

        public bool Over21Required
        {
            get { return this.over21Required; }
            set { this.over21Required = value; }
        }

        public bool HasThumbnail
        {
            get { return this.hasThumbnail; }
            set { this.hasThumbnail = value; }
        }

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        public string BackgroundRGB
        {
            get { return this.backgroundRGB; }
            set { this.backgroundRGB = value; }
        }

        public string BackgroundImage
        {
            get { return this.backgroundImage; }
            set { this.backgroundImage = value; }
        }

        public string CreatorUsername
        {
            get { return this.creatorUsername; }
            set { this.creatorUsername = value; }
        }

        public string CreatorNameNoSpaces
        {
            get { return this.creatorNameNoSpaces; }
            set { this.creatorNameNoSpaces = value; }
        }

        public string CreatorThumbnailSmallPath
        {
            get { return this.creatorThumbnailSmallPath; }
            set { this.creatorThumbnailSmallPath = value; }
        }

        public string Keywords
        {
            get { return this.keywords; }
            set { this.keywords = value; }
        }

        // Extra
        public string OwnerZipCode
        {
            get { return this.ownerZipCode; }
            set { this.ownerZipCode = value; }
        }

        public int OwnerAge
        {
            get { return this.ownerAge; }
            set { this.ownerAge = value; }
        }

        public string OwnerGender
        {
            get { return this.ownerGender; }
            set { this.ownerGender = value; }
        }

        public string OwnerCountry
        {
            get { return this.ownerCountry; }
            set { this.ownerCountry = value; }
        }

        public string OwnerLocation
        {
            get { return this.ownerLocation; }
            set { this.ownerLocation = value; }
        }

        public bool OwnerMatureProfile
        {
            get { return this.ownerMatureProfile; }
            set { this.ownerMatureProfile = value; }
        }

        public int OwnerFriendCount
        {
            get { return this.ownerFriendCount; }
            set { this.ownerFriendCount = value; }
        }

        public CommunityStats Stats
        {
            get { return this.stats; }
            set { this.stats = value; }
        }

        public CommunityPreferences Preferences
        {
            get { return this.preferences; }
            set { this.preferences = value; }
        }

        public IList<CommunityMember> MemberList
        {
            get { return this.memberList; }
            set { this.memberList = value; }
        }

        public WOK3DApp WOK3App
        {
            get { return this.wok3DApp; }
            set { this.wok3DApp = value; }
        }
    }
}
