///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class CommunityPlaceType
    {
        private int _PlaceTypeId = 0;
        private string _Name = "";
        private string _Description = "";

        public CommunityPlaceType() { }

        public CommunityPlaceType(int placeTypeId, string name, string description)
        {
            this._PlaceTypeId = placeTypeId;
            this._Name = name;
            this._Description = description;
        }

        public int PlaceTypeId
        {
            get {return _PlaceTypeId;}
            set {_PlaceTypeId = value;}
        }

        public string Name
        {
            get {return _Name;}
            set {_Name = value;}
        }

        public string Description
        {
            get {return _Description;}
            set {_Description = value;}
        }

    }

}
