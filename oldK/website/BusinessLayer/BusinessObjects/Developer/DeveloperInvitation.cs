///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class DeveloperInvitation
    {
        private int _InvitationId = 0;
        private int _CompanyId = 0;
        private int _RoleId = 0;
        private string _ValidationKey = "";
        private string _Email = "";
        private string _Message = "";
        private string _Name = "";
        private DateTime _InviteDate = DateTime.Now;

        public DeveloperInvitation()
        {
        }

        /// <summary>
        /// Overloaded constructor for the GameDeveloper class.
        /// </summary>
        public DeveloperInvitation(int InvitationId, int CompanyId, int RoleId, string ValidationKey, string Email, string Message, string Name, DateTime InviteDate)
        {
            this._InvitationId = InvitationId;
            this._CompanyId = CompanyId;
            this._RoleId = RoleId;
            this._ValidationKey = ValidationKey;
            this._Email = Email;
            this._Message = Message;
            this._Name = Name;
            this._InviteDate = InviteDate;
        }

        /// <summary>
        /// Gets or sets unique user identifier.
        /// The Identity Field Design Pattern. 
        /// </summary>
        /// 

        public int CompanyId
        {
            get { return _CompanyId; }
            set { _CompanyId = value; }
        }

        public int InvitationId
        {
            get { return _InvitationId; }
            set { _InvitationId = value; }
        }

        public int RoleId
        {
            get { return _RoleId; }
            set { _RoleId = value; }
        }

        public string ValidationKey
        {
            get { return _ValidationKey; }
            set { _ValidationKey = value; }
        }

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public DateTime InviteDate
        {
            get { return _InviteDate; }
            set { _InviteDate = value; }
        }

    }
}
