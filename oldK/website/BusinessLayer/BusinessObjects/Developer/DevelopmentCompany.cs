///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class DevelopmentCompany
    {
        private int _company_id = -1;
        private string _contacts_firstname = "";
        private string _contacts_lastname = "";
        private string _contacts_phone = "";
        private string _company_name = "";
        private string _contacts_email = "";
        private string _company_addressI = "";
        private string _company_addressII;
        private string _state_code = "GA";
        private string _city = "";
        private string _province = "";
        private string _country_id = "US";
        private string _zip_code;
        private string _website_URL;
        private int _annual_revenue;
        private int _vworld_involvment_id;
        private string _project_name;
        private string _project_description;
        private int _project_startdate_id = 0;
        private int _budget = 0;
        private int _client_platform_id = 0;
        private int _project_status_id = 0;
        private int _current_team_size =0;
        private int _projected_team_size = 0;
        private bool _approved = false;
        private bool _authorized_tester = false;
        private DateTime _date_submitted = DateTime.Now;

        public DevelopmentCompany()
        {
        }

        /// <summary>
        /// Overloaded constructor for the User class.
        /// </summary>
        public DevelopmentCompany(int CompanyId, string ContactsFirstName, string ContactsLastName, string ContactsPhone,
        string CompanyName, string ContactsEmail, string CompanyAddressI, string CompanyAddressII, string StateCode,
        string City, string Province, string CountryId, string ZipCode, string WebsiteURL, int AnnualRevenue, int VworldInvolvmentId, 
        string ProjectName, string ProjectDescription, int ProjectStartdateId, int Budget, int ClientPlatformId, int ProjectStatusId,
        int CurrentTeamSize, int ProjectedTeamSize, DateTime DateSubmitted, bool Approved, bool AuthorizedTester)
        {
            this._company_id = CompanyId;
            this._contacts_firstname = ContactsFirstName;
            this._contacts_lastname = ContactsLastName;
            this._contacts_phone = ContactsPhone;
            this._company_name = CompanyName;
            this._contacts_email = ContactsEmail;
            this._company_addressI = CompanyAddressI;
            this._company_addressII = CompanyAddressII;
            this._state_code = StateCode;
            this._city = City;
            this._province = Province;
            this._country_id = CountryId;
            this._zip_code = ZipCode;
            this._website_URL = WebsiteURL;
            this._annual_revenue = AnnualRevenue;
            this._vworld_involvment_id = VworldInvolvmentId;
            this._project_name = ProjectName;
            this._project_description = ProjectDescription;
            this._project_startdate_id = ProjectStartdateId;
            this._budget = Budget;
            this._client_platform_id = ClientPlatformId;
            this._project_status_id = ProjectStatusId;
            this._current_team_size = CurrentTeamSize;
            this._projected_team_size = ProjectedTeamSize;
            this._date_submitted = DateSubmitted;
            this._approved = Approved;
            this._authorized_tester = AuthorizedTester;
        }

        /// <summary>
        /// Gets or sets unique user identifier.
        /// The Identity Field Design Pattern. 
        /// </summary>
        /// 

        public bool AuthorizedTester
        {
            get { return _authorized_tester; }
            set { _authorized_tester = value; }
        }

        public bool Approved
        {
            get { return _approved; }
            set { _approved = value; }
        }

        public DateTime DateSubmitted
        {
            get { return _date_submitted; }
            set { _date_submitted = value; }
        }

        public int CurrentTeamSize
        {
            get { return _current_team_size; }
            set { _current_team_size = value; }
        }

        public int ProjectedTeamSize
        {
            get { return _projected_team_size; }
            set { _projected_team_size = value; }
        }

        public int ProjectStatusId
        {
            get { return _project_status_id; }
            set { _project_status_id = value; }
        }

        public int ClientPlatformId
        {
            get { return _client_platform_id; }
            set { _client_platform_id = value; }
        }
        public string ContactsFirstName
        {
            get { return _contacts_firstname; }
            set { _contacts_firstname = value; }
        }

        public string ContactsLastName
        {
            get { return _contacts_lastname; }
            set { _contacts_lastname = value; }
        }

        public string ContactsPhone
        {
            get { return _contacts_phone; }
            set { _contacts_phone = value; }
        }

        public string CompanyName
        {
            get { return _company_name; }
            set { _company_name = value; }
        }

        public string ContactsEmail
        {
            get { return _contacts_email; }
            set { _contacts_email = value; }
        }

        public string CompanyAddressI
        {
            get { return _company_addressI; }
            set { _company_addressI = value; }
        }

        public string CompanyAddressII
        {
            get { return _company_addressII; }
            set { _company_addressII = value; }
        }

        public int CompanyId
        {
            get { return _company_id; }
            set { _company_id = value; }
        }

        public string StateCode
        {
            get { return _state_code; }
            set { _state_code = value; }
        }

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        public string Province
        {
            get { return _province; }
            set { _province = value; }
        }

        public string ZipCode
        {
            get { return _zip_code; }
            set { _zip_code = value; }
        }

        public string WebsiteURL
        {
            get { return _website_URL; }
            set { _website_URL = value; }
        }

        public string ProjectDescription
        {
            get { return _project_description; }
            set { _project_description = value; }
        }

        public string ProjectName
        {
            get { return _project_name; }
            set { _project_name = value; }
        }

        public string CountryId
        {
            get { return _country_id; }
            set { _country_id = value; }
        }

        public int VworldInvolvmentId
        {
            get { return _vworld_involvment_id; }
            set { _vworld_involvment_id = value; }
        }

        public int ProjectStartdateId
        {
            get { return _project_startdate_id; }
            set { _project_startdate_id = value; }
        }

        public int Budget
        {
            get { return _budget; }
            set { _budget = value; }
        }

        public  int AnnualRevenue
        {
            get { return _annual_revenue; }
            set { _annual_revenue = value; }
        }

    }
}
