///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class GameDeveloper
    {
        private int _UserId = 0;
        private int _CompanyId = 0;
        private int _DeveloperRoleId = 0;
        private User _UserInfo = new User();
        private NameValueCollection _UserPrivileges = new NameValueCollection();
        public const int ADMIN_ROLEID = 1; //this method should be revisted maybe moved to webconfig
        public const string ADMIN_FILTER = "" + ROLE_ID + "=1" ;
        public const string ROLE_ID = "role_id";

        public const int NEWCOMP_NEWUSER = 0;
        public const int COMPEXISTS_NEWUSER = 1;
        public const int COMPEXISTS_NEWDEVELOPER = 2;
        public const int COMPEXISTS_DEVELOPEREXISTS = 3;
        public const int NEWCOMPANY_NEWDEVELOPER = 4;

        public GameDeveloper()
        {
        }

        /// <summary>
        /// Overloaded constructor for the GameDeveloper class.
        /// </summary>
        public GameDeveloper(int UserId, int CompanyId, int DeveloperRoleId, User UserInfo, NameValueCollection UserPrivileges)
        {
            this._UserId = UserId;
            this._CompanyId = CompanyId;
            this._DeveloperRoleId = DeveloperRoleId;
            this._UserInfo = UserInfo;
            this._UserPrivileges = UserPrivileges;
        }

        /// <summary>
        /// Gets or sets unique user identifier.
        /// The Identity Field Design Pattern. 
        /// </summary>
        /// 

        public int CompanyId
        {
            get { return _CompanyId; }
            set { _CompanyId = value; }
        }

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public int DeveloperRoleId
        {
            get { return _DeveloperRoleId; }
            set { _DeveloperRoleId = value; }
        }

        public User UserInfo
        {
            get { return _UserInfo; }
            set { _UserInfo = value; }
        }

        public NameValueCollection UserPrivileges
        {
            get { return _UserPrivileges; }
            set { _UserPrivileges = value; }
        }

    }
}
