///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class RaveType
    {
        public enum eRAVE_TYPE
        {
            SINGLE = 1,
            MEGA = 2
        }

        private uint _RaveTypeId;
        private string _Name;
        private string _Description;
        private int _MegaRaveValue;
        private int _PriceSingleRave;
        private int _PriceMegaRave;
        private int _NumberAllowedFree;
        private int _PercentCommission;

        public RaveType (uint raveTypeId, string name, string description, int megaRaveValue, int priceSingleRave,
            int priceMegaRave, int numberAllowedFree, int percentCommission)
        {
            this._RaveTypeId = raveTypeId;
            this._Name = name;
            this._Description = description;
            this._MegaRaveValue = megaRaveValue;
            this._PriceSingleRave = priceSingleRave;
            this._PriceMegaRave = priceMegaRave;
            this._NumberAllowedFree = numberAllowedFree;
            this._PercentCommission = percentCommission;
        }

        public uint RaveTypeId
        {
            get { return _RaveTypeId; }
            set { _RaveTypeId = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public int MegaRaveValue
        {
            get { return _MegaRaveValue; }
            set { _MegaRaveValue = value; }
        }

        public int PriceSingleRave
        {
            get { return _PriceSingleRave; }
            set { _PriceSingleRave = value; }
        }

        public int PriceMegaRave
        {
            get { return _PriceMegaRave; }
            set { _PriceMegaRave = value; }
        }

        public int NumberAllowedFree
        {
            get { return _NumberAllowedFree; }
            set { _NumberAllowedFree = value; }
        }

        public int PercentCommission
        {
            get { return _PercentCommission; }
            set { _PercentCommission = value; }
        }
    }
}
