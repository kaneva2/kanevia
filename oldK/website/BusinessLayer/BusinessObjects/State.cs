///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

namespace Kaneva.BusinessLayer.BusinessObjects
{
    [Serializable]
    public class State
    {
        private string _StateCode;
        private string _State;

        public State (string stateCode, string state)
        {
            this._StateCode = stateCode;
            this._State = state;
        }

        public string StateCode
        {
           get {return _StateCode;}
           set {_StateCode = value;}
        }

        public string Name
        {
           get {return _State;}
           set {_State = value;}
        }

    }
}
