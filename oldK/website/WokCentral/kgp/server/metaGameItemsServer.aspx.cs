///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Web;
using System.Web.UI;
using System.Data;
using log4net;
using System.Linq;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.WOKCentral
{
    public class metaGameItemsServer : Page
    {
        private string _action;
        private string _itemType;
        private string _username;
        private List<KeyValuePair<string, int>> _itemAmounts;

        protected void Page_Load (object sender, EventArgs e)
        {
            GetRequestParams();

            // Process based on action.
            switch (_action)
            {
                case "getBalances":
                    ProcessGetBalances();
                    break;
                case "creditItemBalances":
                    ProcessCreditItemBalances();
                    break;
                case "convertToRewards":
                    ProcessConvertToRewards();
                    break;
                default:
                    Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid action</ResultDescription>\r\n</Result>");
                    break;
            }
        }

        /// <summary>
        /// Returns the xml for the meta game item balances.
        /// </summary>
        private string BuildMetaGameItemBalanceXML(List<MetaGameItem> items, UserMetaGameItemBalances balances, int dailyTotal, int amtAvailableToConvert)
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("Result"));
            XmlNode root = doc.DocumentElement;
            
            XmlElement elem = doc.CreateElement("ReturnCode");
            elem.InnerText = "0";
            root.AppendChild(elem);

            elem = doc.CreateElement("ResultDescription");
            elem.InnerText = "success";
            root.AppendChild(elem);

            elem = doc.CreateElement("DailyConversionsTotal");
            elem.InnerText = dailyTotal.ToString();
            root.AppendChild(elem);

            elem = doc.CreateElement("DailyConversionsRemaining");
            elem.InnerText = amtAvailableToConvert.ToString();
            root.AppendChild(elem);

            elem = doc.CreateElement("ItemBalances");
            foreach (MetaGameItem item in items)
            {
                KeyValuePair<string, int> itemBalance = balances.SingleOrDefault(i => i.Key.Equals(item.ItemName));

                XmlElement itemElem = doc.CreateElement("Item");
                itemElem.InnerXml += string.Format("<Name>{0}</Name>", item.ItemName);
                itemElem.InnerXml += string.Format("<Balance>{0}</Balance>", itemBalance.Value);
                itemElem.InnerXml += string.Format("<ConversionValueRewards>{0}</ConversionValueRewards>", item.GetConversionValue(Currency.CurrencyType.REWARDS));
                elem.AppendChild(itemElem);
            }
            root.AppendChild(elem);

            return root.OuterXml;
        }

        /// <summary>
        /// Returns UserMetaGameItemBalances for the specified item type.
        /// </summary>
        private void ProcessGetBalances()
        {
            UserFacade userFacade = new UserFacade();
            GameFacade gameFacade = new GameFacade();

            if (string.IsNullOrWhiteSpace(_itemType))
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid itemType</ResultDescription>\r\n</Result>");
                return;
            }

            if (string.IsNullOrWhiteSpace(_username))
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>username not specified</ResultDescription>\r\n</Result>");
                return;
            }

            int userId = userFacade.GetUserIdFromUsername(_username);
            if (userId == 0)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid username</ResultDescription>\r\n</Result>");
                return;
            }

            List<MetaGameItem> items = gameFacade.GetMetaGameItems(_itemType);
            UserMetaGameItemBalances balances = userFacade.GetUserMetaGameItemBalances(userId);
            int dailyTotal = userFacade.GetUserDailyTotalMGIConversionAmount(userId, _itemType, Currency.CurrencyType.REWARDS);
            int amtAvailableToConvert = (items.Count > 0 ? items[0].GetAmountAvailableToConvert(Currency.CurrencyType.REWARDS, dailyTotal) : 0);

            Response.Write(BuildMetaGameItemBalanceXML(items, balances, dailyTotal, amtAvailableToConvert));
        }

        /// <summary>
        /// Increments UserMetaGameItemBalances by the specified amounts.
        /// Returns the new balances if the update was successful.
        /// </summary>
        private void ProcessCreditItemBalances()
        {
            UserFacade userFacade = new UserFacade();
            GameFacade gameFacade = new GameFacade();

            if (string.IsNullOrWhiteSpace(_itemType))
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid itemType</ResultDescription>\r\n</Result>");
                return;
            }

            if (_itemAmounts.Count == 0)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid itemAmounts</ResultDescription>\r\n</Result>");
                return;
            }

            if (string.IsNullOrWhiteSpace(_username))
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>username not specified</ResultDescription>\r\n</Result>");
                return;
            }

            int userId = userFacade.GetUserIdFromUsername(_username);
            if (userId == 0)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid username</ResultDescription>\r\n</Result>");
                return;
            }

            // If update succeeded, return new balance
            if (userFacade.CreditUserGameItemBalances(userId, _itemAmounts))
            {
                List<MetaGameItem> items = gameFacade.GetMetaGameItems(_itemType);
                UserMetaGameItemBalances balances = userFacade.GetUserMetaGameItemBalances(userId);
                int dailyTotal = userFacade.GetUserDailyTotalMGIConversionAmount(userId, _itemType, Currency.CurrencyType.REWARDS);
                int amtAvailableToConvert = (items.Count > 0 ? items[0].GetAmountAvailableToConvert(Currency.CurrencyType.REWARDS, dailyTotal) : 0);

                Response.Write(BuildMetaGameItemBalanceXML(items, balances, dailyTotal, amtAvailableToConvert));
            }
            else
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>unable to update meta game item balances</ResultDescription>\r\n</Result>");
            }
        }

        /// <summary>
        /// Converts the specified meta game items to rewards.
        /// Returns the new balances if the update was successful.
        /// </summary>
        private void ProcessConvertToRewards()
        {
            UserFacade userFacade = new UserFacade();
            GameFacade gameFacade = new GameFacade();

            int dailyTotal;
            int amtAvailableToConvert;

            if (string.IsNullOrWhiteSpace(_itemType))
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid itemType</ResultDescription>\r\n</Result>");
                return;
            }

            if (_itemAmounts.Count == 0)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid itemAmounts</ResultDescription>\r\n</Result>");
                return;
            }

            if (string.IsNullOrWhiteSpace(_username))
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>username not specified</ResultDescription>\r\n</Result>");
                return;
            }

            int userId = userFacade.GetUserIdFromUsername(_username);
            if (userId == 0)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid username</ResultDescription>\r\n</Result>");
                return;
            }

            try
            {
                bool conversionSucceeded = userFacade.ConvertMetaGameItemsToCurrency(userId, _itemType, Currency.CurrencyType.REWARDS, _itemAmounts, out dailyTotal, out amtAvailableToConvert);

                if (!conversionSucceeded)
                {
                    Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>unable to convert items to rewards.</ResultDescription>\r\n</Result>");
                    return;
                }
            }
            catch (ArgumentException e)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>unable to convert items to rewards. " + e.Message + "</ResultDescription>\r\n</Result>");
                return;
            }

            // Return user balances
            List<MetaGameItem> items = gameFacade.GetMetaGameItems(_itemType);
            UserMetaGameItemBalances balances = userFacade.GetUserMetaGameItemBalances(userId);

            Response.Write(BuildMetaGameItemBalanceXML(items, balances, dailyTotal, amtAvailableToConvert));
        }

        /// <summary>
        /// Reads all request parameters from query string and provides appropriate defaults.
        /// </summary>
        private void GetRequestParams()
        {
            int iOut;

            _username = Request.Params["username"];
            _action = Request.Params["action"];
            _itemType = Request.Params["itemType"];
            _itemAmounts = new List<KeyValuePair<string, int>>();

            // Item amounts should be specified as a list of pipe-delmited tuples.
            // Example: itemAmounts=World Coin,10|Emerald,15.
            if (!string.IsNullOrWhiteSpace(Request.Params["itemAmounts"]))
            {
                try
                {
                    string[] itemList = Request.Params["itemAmounts"].Split('|');
                    foreach (string item in itemList)
                    {
                        string[] itemValues = item.Split(',');

                        // Quick check to make sure that key and value aren't swapped
                        if (Int32.TryParse(itemValues[0], out iOut) || !Int32.TryParse(itemValues[1], out iOut))
                        {
                            throw new ArgumentException();
                        }

                        _itemAmounts.Add(new KeyValuePair<string, int>(itemValues[0], Int32.Parse(itemValues[1])));
                    }
                }
                catch (Exception) 
                {
                    _itemAmounts.Clear();
                }
            }
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
    }
}