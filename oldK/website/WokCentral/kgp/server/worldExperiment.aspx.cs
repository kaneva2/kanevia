///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Xml;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Linq;

namespace KlausEnt.KEP.Kaneva.WOKCentral
{
	/// <summary>
	/// worldExperiment inserts new experiment participant records and returns participant data for the specified world
	/// </summary>
    public class worldExperiment : Page
	{
        string _action;
        string _experimentId;
        string _groupId;
        bool _assignAutomatically;
        int _zoneInstanceId;
        int _zoneType;

		private void Page_Load(object sender, System.EventArgs e)
		{
            GetRequestParams();

            // Process based on action.
            switch (_action)
            {
                case "getActiveExperimentGroups":
                    ProcessGetActiveExperimentGroups();
                    break;
                case "recordConversion":
                    ProcessRecordConversion();
                    break;
                default:
                    Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid action</ResultDescription>\r\n</Result>");
                    break;
            }
		}

        /// <summary>
        /// Gets all active experiment participation data for a specified world.  If "assignAutomatically" is passed
        /// into the call as "Y", automatically assigns the world to active experiments first.
        /// </summary>
        private void ProcessGetActiveExperimentGroups()
        {
            ExperimentFacade experimentFacade = new ExperimentFacade();

            if (_zoneInstanceId == 0)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid zoneInstanceId</ResultDescription>\r\n</Result>");
                return;
            }

            if (_zoneType == 0)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid zoneType</ResultDescription>\r\n</Result>");
                return;
            }

            if (_assignAutomatically)
            {
                // assign a group to the world for every experiment that has started (or ended) but the world is not already participating in
                IList<Experiment> availableExperiments = experimentFacade.GetAvailableExperimentsByWorldContext(_zoneInstanceId, _zoneType, Experiment.ASSIGN_ON_REQUEST, true);

                foreach (Experiment experiment in availableExperiments)
                {
                    string assignedGroup = experimentFacade.AssignParticipant(experiment);
                    if (!string.IsNullOrWhiteSpace(assignedGroup))
                    {
                        experimentFacade.SaveWorldExperimentGroup(new WorldExperimentGroup(string.Empty, _zoneInstanceId, _zoneType, assignedGroup, DateTime.Now, null));
                    }
                }
            }

            // Return a list of experiments the world is participating in
            List<WorldExperimentGroup> activeExperiments = experimentFacade.GetActiveWorldExperimentGroups(_zoneInstanceId, _zoneType, false).ToList();
            if (activeExperiments != null)
            {
                XmlDocument doc = new XmlDocument();

                XmlNode root = doc.CreateElement("Result");
                doc.AppendChild(root);

                XmlElement elem = doc.CreateElement("ReturnCode");
                elem.InnerText = "0";
                root.AppendChild(elem);

                elem = doc.CreateElement("ReturnDescription");
                elem.InnerText = "success";
                root.AppendChild(elem);

                XmlDocument experimentsXml = WorldExperimentGroupsToXML(activeExperiments);
                XmlNode experimentsRoot = doc.ImportNode(experimentsXml.DocumentElement, true);
                root.AppendChild(experimentsRoot);

                Response.Write(doc.OuterXml);

                return;
            }
            else
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>error finding experiments for world</ResultDescription>\r\n</Result>";
                Response.Write(errorStr);
                return;
            }
        }

        /// <summary>
        /// Records "conversion" date for a world group. Conversion may indicate the first time
        /// users entered the world, the first time users encountered the feature under testing,
        /// etc.
        /// </summary>
        private void ProcessRecordConversion()
        {
            ExperimentFacade experimentFacade = new ExperimentFacade();

            if (_zoneInstanceId == 0)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid zoneInstanceId</ResultDescription>\r\n</Result>");
                return;
            }

            if (_zoneType == 0)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>invalid zoneType</ResultDescription>\r\n</Result>");
                return;
            }

            WorldExperimentGroup worldGroup = null;

            if (!string.IsNullOrWhiteSpace(_groupId))
            {
                worldGroup = experimentFacade.GetWorldExperimentGroup(_zoneInstanceId, _zoneType, _groupId, false);
            }
            else if (!string.IsNullOrWhiteSpace(_experimentId))
            {
                worldGroup = experimentFacade.GetWorldExperimentGroupByExperimentId(_zoneInstanceId, _zoneType, _experimentId, false);
            }

            if (worldGroup == null)
            {
                Response.Write("<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>unable to find group assignment for world</ResultDescription>\r\n</Result>");
                return;
            }

            worldGroup.ConversionDate = DateTime.Now;
            experimentFacade.SaveWorldExperimentGroup(worldGroup);

            Response.Write("<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>success</ResultDescription>\r\n</Result>");
        }

        /// <summary>
        /// Reads all request parameters from query string and provides appropriate defaults.
        /// </summary>
        private void GetRequestParams()
        {
            int iOut;

            _action = Request.Params["action"];
            _experimentId = Request.Params["experimentId"];
            _groupId = Request.Params["groupId"];
            _assignAutomatically = (Request.Params["assignAutomatically"] ?? "N").Equals("Y");
            _zoneInstanceId = Int32.TryParse(Request.Params["zoneInstanceId"], out iOut) ? iOut : 0;
            _zoneType = Int32.TryParse(Request.Params["zoneType"], out iOut) ? iOut : 0;
        }

        ///<summary>
        /// Returns an xml document that will be sent back to the caller containing all experiment participant data.
        /// </summary>
        private XmlDocument WorldExperimentGroupsToXML(List<WorldExperimentGroup> participants)
        {
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(participants.GetType());
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);

            x.Serialize(writer, participants);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sb.ToString());

            return doc;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
