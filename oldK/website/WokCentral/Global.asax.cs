///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

// Import log4net classes.
using log4net;
using log4net.Config;

using Facade = Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.DataLayer.DataObjects;

namespace KlausEnt.KEP.Kaneva.WOKCentral
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            // Change to log4net to be consistent with other modules
            XmlConfigurator.Configure(new System.IO.FileInfo(Server.MapPath("~") + "\\" + System.Configuration.ConfigurationManager.AppSettings["LogConfigFile"]));

            
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            if (Server.GetLastError() is HttpException)
            {
                m_logger.Warn("HttpException from " + Request.Url.ToString(), Server.GetLastError());
            }
            else
            {
                m_logger.Error("Unhandled application error in " + Request.Url.ToString(), Server.GetLastError());
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    }
}