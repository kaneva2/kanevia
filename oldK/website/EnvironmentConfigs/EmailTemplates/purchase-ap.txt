Subject: Kaneva Credit Order Confirmation
Message:
#header#  

Congratulations, you are now an Access Pass member!<br />
<br />
Your confirmation information is:<br />
<br />
Order Number: #orderid#<br />
Description:  #description#<br />
Amount: #bill# (Billed #billInterval#)<br />
Purchase Date:  #transaction_date#<br />
<br />
You can always review your order by signing into the Kaneva website and visiting:  <a href="http://www.kaneva.com/mykaneva/credithistory.aspx">http://www.kaneva.com/mykaneva/credithistory.aspx</a><br />
<br />
<br />
Start exploring exclusive Access Pass member content including dance clubs and communities; music, videos, photos.  Plus customize your Kaneva experience with Adult Only fashions and furniture.<br />
#footer#

