Subject: #username# Added a Comment to Your
Message:
#header#

Hi #username#,<br>
<br>
<a href="#userlink#">#username#</a> added a comment to your Kaneva Profile.<br>
<br>
<a href="#tolink#">Click here to see the comment</a>.<br>
<br>
<br>
#footer#

