<?php
$template_file = "sphinx_template.conf";

if($argc>1){
   $template_file = $argv[1];
}
echo "Using template file: $template_file\n";

$config["dev"] = array(
    "%SQL_HOST%" => "db.kanevia.com",
    "%SQL_HOST_AUDIT%" => "db.kanevia.com",
    "%SQL_USER%" => "TBD",
    "%SQL_PASS%" => "TBD",
    "%LISTEN%" => "db.kanevia.com",
    "%PORT%" => "3312",
);

if(file_exists($template_file)){
echo "loading file\n";
    $templatestring = file_get_contents($template_file);
}

foreach($config as $system => $configset){
    $outstring = $templatestring;
    foreach ($configset as $from => $to){
        echo "Processing $system::$from\n";
        $outstring = str_replace($from,$to,$outstring);
    }
    if(($fileptr=fopen("sphinx_$system.conf","wb"))!=null){
        fwrite($fileptr,$outstring);
        fclose($fileptr);    
    }
}
?>
