#!/usr/bin/php
<?
error_reporting(E_ALL);

// Disabled for now.
//include "email_config.php";
include "fileLogger.php";
include "os.php";

$quiet = false;


// fix up the execution path.  We do this because if this is launched via cron it will be launched from the cron
// users home directory, typically /root.  i.e. regardless of the location of sphinxmonitor, it is launched
// from ~, and then we don't find out configuration file.
//
/*
$pathparts = explode( '/', $argv[0] );
if ( $pathparts[0] == '.' ) {
    $fullpath = getcwd() . "/" . $pathparts[count($pathparts)-1];
    $pathparts = explode( "/", $fullpath );
}
$scriptname = $pathparts[count($pathparts)-1];
unset( $pathparts[count($pathparts)-1] );
$fullpath = implode( "/", $pathparts ) . "/";
*/
$fullpath = script_path();



//$logger->intro();
//$logger->logMessage( fileLogger::DEBUG, 'process', "**** launched [$fullpath$scriptname]" );

include "${fullpath}index_list.php";

$command = 'assert';



for($i = 1; $i< $argc; $i++){
//    $arg = $argv[$i];
	switch ( $argv[$i] ) {
	case '-q':		$quiet 		= true; 		break;
	case '-cmd':	$command 	= $argv[++$i]; 	break;
	}
//    if($arg == '-q'){
//        $i++;
//    }
}

if ( os::processDetected( 'sphinxloop.php' ) != false ) {
//	$logger->logMessage( fileLogger::DEBUG, 'process', "[$scriptname] is already running" );
	exit;
}

$logger = new fileLogger( $fullpath . 'sphinxmonitor.log', fileLogger::DEBUG );


$cmd = "${fullpath}sphinxloop.php -v -c ".$host_config['connect']." -s ".$host_config['pollintervalsecs'] . " -r " . $host_config['email'];
//            $cmd = "./sphinxloop.php -i ".$index["name"]." -s ".$index["sec"];

if(array_key_exists("dbhost",$host_config)&& array_key_exists("rmfiles",$host_config)){
	$cmd .= " -h ".$host_config["dbhost"]." -rm ".$host_config["rmfiles"];
}


$cmd .= "  > /dev/null&";
$logger->logMessage( fileLogger::DEBUG, 'process', "launching sphinxloop [$cmd]" );

$sysret = system($cmd, $sysret );
$logger->logMessage( fileLogger::DEBUG, 'process', "sphinx indexer loop started on $hostname [$sysret]" );
//$errlist = array();
//$errlist[] = array("Action", "$hostname restarted");
//  	if($quiet!=true) mail_error($errlist, $subject=$index["name"]." restarted");
//$logger->logMessage( fileLogger::DEBUG, 'process',  '**** endrun' );


?>
