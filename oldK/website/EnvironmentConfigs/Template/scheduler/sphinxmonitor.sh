#!/bin/bash
cd /usr/local/var/data
for x in $( ls | awk -F. '{ print $1 }' | uniq ); do
echo $x `du --block-size=1048576 -c $x.* | grep total | awk '{ print $1}'`
done
