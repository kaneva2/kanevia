#!/usr/bin/php
<?php
include( 'Logger.php' );




/**
 * This class is the precursor to a logging infrastructure for Kaneva PHP applications.
 * Currently it makes assumptions about 1) the output target and 2) application specific 
 * data (e.g. indexer_host, index name).  But there are some common pieces like logging level,
 * class and message we can leverage later on.
 * 
 * 1) add abstractions for application specific ancillary data (e.g. indexer_host, index).
 * 2) add abstractions for output target (e.g. file, db, email, remote call etc)
 *
 * NOTE: Investigate existing (e.g. PearLog) logging solutions before performing any of these 
 * abstractions
 */
class fileLogger extends Logger {

	private $logfile;	
	
	function __construct( $logfile, $level = fileLogger::INFO ) {
		parent::__construct($level);
		$this->logfile = $logfile;
	}

	function logInternal( $level, $class, $msg, $ancillary = null) {
		try {
			if ( !$this->wouldLog($level) ) return;
			if ( $ancillary == null ) $ancillary = array();

			$handle = fopen( $this->logfile, "a" );
			$leveltag = parent::levelTag($level);
   			fputs( $handle, date('Y-m-d m:i') . " [$leveltag][$class] $msg\n" );
   			fclose( $handle );
		}
		catch( Exception $e ) {
			echo $e->getMessage() . "\n";
		}
	}
};

$runtest 		= false;
$testfile 	    = null;

for ( $i = 1; $i < $argc; $i++ ) {
   if ( $argv[$i] == '-testunit' ) $runtest = true;
   if ( $argv[$i] == '-testfile' ) $testfile = $argv[++$i];
}


if ( $runtest == true ) {
	if ($testfile==null) throw new Exception( 'No log file specified' );
	$logger = new fileLogger( $testfile, Logger::WARN );

	$logger->logMessage( Logger::DEBUG, 'test', 'DEBUG' );
	$logger->logMessage( Logger::WARN, 'test', 'WARNING' );
	$logger->logMessage( Logger::ERROR, 'test', 'ERROR' );
	$logger->setLogLevel( Logger::DEBUG );
	$logger->logMessage( Logger::DEBUG, 'test', 'DEBUG - Hooray!' );
	$logger->logMessage( Logger::WARN, 'test', 'WARNING' );
	$logger->logMessage( Logger::ERROR, 'test', 'ERROR' );

}


?>
