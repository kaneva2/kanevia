#!/usr/bin/php
<?
error_reporting(E_ALL);
include "email_config.php";
include "index_list.php";

$date = date("YmdHis",time());
$logfile = "/tmp/sphinx-".$date.".log";

foreach($index_list as $index){
    $cmd = "ps -ef | grep -v grep | grep sphinxloop.php | grep ".$index["name"]." | awk '{ print $2 }'";
    $output = system($cmd);

    if(is_numeric($output)){
        $cmd = "kill -9 $output > /dev/null";
        system($cmd);
    }

    $cmd = "mv /tmp/".$index["name"]."_sphinx.log  /tmp/".$index["name"]."_sphinx.".$date.".log";
    system($cmd);
    $cmd = "cat /tmp/".$index["name"]."_sphinx.".$date.".log >> $logfile";
    system($cmd);
    $cmd = "/bin/rm -f /tmp/".$index["name"]."_sphinx.".$date.".log";
    system($cmd);
}

$cmd = "./sphinxmonitor.php -q";
system($cmd);

$report = array();
if(($fileptr=fopen($logfile,"r"))!==false){
   while(($line = fgetcsv($fileptr)) !== false){
      $index = $line[0];
      $start = $line[1];
      $mid = $line[2];
      $end = $line[3];
      $delta = $mid - $start;
      $merge = $end - $mid;

      if(!array_key_exists($index, $report)) $report[$index] = array("name"=>$index, "count"=>0, "mindelta"=>INF, "totaldelta"=>0, "maxdelta"=>-1, "minmerge"=>INF, "totalmerge"=>0,"maxmerge"=>-1);
      $report[$index]["count"]++;
      $report[$index]["totaldelta"] += $delta;
      if($report[$index]["mindelta"]>$delta) $report[$index]["mindelta"] = $delta;
      if($report[$index]["maxdelta"]<$delta) $report[$index]["maxdelta"] = $delta;
      $report[$index]["totalmerge"] += $merge;
      if($report[$index]["minmerge"]>$merge) $report[$index]["minmerge"] = $merge;
      if($report[$index]["maxmerge"]<$merge) $report[$index]["maxmerge"] = $merge;
   }
}

$reportstr="<table border=1><tr bgcolor=#DDDDFF><th>index</th><th>count</th><th>min</th><th>max</th><th>avg</th></tr>\n";
foreach($report as $index=>$values){
    $deltaavg = $values["totaldelta"]/$values["count"];
    $mergeavg = $values["totalmerge"]/$values["count"];

    $reportstr .= "<tr>";
    $reportstr .= sprintf("<td>$index</td>");
    $reportstr .= sprintf("<td>%d</td>",$values["count"]);
    $reportstr .= sprintf("<td>%0.4f</td>",$values["mindelta"]);
    $reportstr .= sprintf("<td>%0.4f</td>",$values["maxdelta"]);
    $reportstr .= sprintf("<td>%0.4f</td>",$deltaavg);
    $reportstr .= "</tr>\n";

/*
    $reportstr .= "<tr>";
    $reportstr .= sprintf("<td>$index merge</td>");
    $reportstr .= sprintf("<td>%d</td>",$values["count"]);
    $reportstr .= sprintf("<td>%0.4f</td>",$values["minmerge"]);
    $reportstr .= sprintf("<td>%0.4f</td>",$values["maxmerge"]);
    $reportstr .= sprintf("<td>%0.4f</td>",$mergeavg);
    $reportstr .= "</tr>\n";
*/

   
/* CSV style format 
    echo $index.",";
    echo $values["count"].",";
    echo $values["mindelta"].",";
    echo $values["maxdelta"].",";
    echo $values["totaldelta"].",";
    echo $values["minmerge"].",";
    echo $values["maxmerge"].",";
    echo $values["totalmerge"];
    echo "\n";
*/
}
$reportstr.="</table>";

mail("dbaalerts@kaneva.com",$logfile,$reportstr, "Content-Type: text/html; charset=iso-8859-1\nContent-Transfer-Encoding: 8bit\n");

?>
