<?php
include_once('Logger.php');
include_once('dbConnector.php');
include_once('dbHelper.php' );
/**
 * This class is the precursor to a logging infrastructure for Kaneva PHP applications.
 * Currently it makes assumptions about 1) the output target and 2) application specific 
 * data (e.g. indexer_host, index name).  But there are some common pieces like logging level,
 * class and message we can leverage later on.
 * 
 * 1) add abstractions for application specific ancillary data (e.g. indexer_host, index).
 * 2) add abstractions for output target (e.g. file, db, email, remote call etc)
 *
 * NOTE: Investigate existing (e.g. PearLog) logging solutions before performing any of these 
 * abstractions
 */
class dbLogger extends Logger {
	
	private $connection;
	
	function __construct( $connection, $level = dbLogger::LOG_LEVEL_INFO ) {
		parent::__construct($level);

		if ( is_string($connection) ) {
			$dbconnector 	= new dbConnector( $connection );
			$connection 	= $dbconnector->connect();
		}

		$this->connection = $connection;
	}

	function logInternal( $level, $class, $msg, $ancillary = null) {
		try {
//			if ( !$this->wouldLog($level) ) return;
			if ( $ancillary == null ) $ancillary = array();
			$ancillary['level'] = parent::levelTag($level); ;
			$ancillary['class'] = $class;
			$ancillary['message'] = $this->connection->real_escape_string($msg);
//			$ancillary['message'] = $msg;

			$ancillary['time'] = '#plain#now()';
			$sql = dbHelper::generateInsert( 'sphinx_indexer_log', $ancillary );
			$queryret = $this->connection->query( $sql  );
			if ( !$queryret )	{
				echo "Error " . $this->connection->errno . " : " . $this->connection->error;
				echo " while logging: [$level][$msg]\n";
			}
		}
		catch( Exception $e ) {
			echo $e->getMessage() . "\n";
		}
	}
};


$runtest 		= false;
$connectstring   = null;

for ( $i = 1; $i < $argc; $i++ ) {
   if ( $argv[$i] == '-testunit' ) 	$runtest 		= true;
   if ( $argv[$i] == '-c' ) 		$connectstring  = $argv[++$i];
}


if ( $runtest == true ) {
	if ($connectstring==null) throw new Exception( 'No log file specified' );
	$logger = new dbLogger( $connectstring, Logger::WARN );

	$logger->logMessage( Logger::DEBUG, 'test', 'DEBUG' );
	$logger->logMessage( Logger::WARN, 'test', 'WARNING' );
	$logger->logMessage( Logger::ERROR, 'test', 'ERROR' );
}


?>