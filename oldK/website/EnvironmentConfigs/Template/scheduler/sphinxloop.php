#!/usr/bin/php
<?php
error_reporting(E_ALL);

include_once( "dbConnector.php" );
include_once( "dbLogger.php" );
include_once( 'os.php' );
include_once( 'email_config.php');



try {
	if ( !os::isWindows() ) {
		echo "Installing sighandler\n";
		pcntl_signal( SIGTERM, 'sig_handler' );
	}
	else {
		echo "No pcntl support available.\n";
	}

	$connectString 	= null;
	$verbose 				= false;
	$loglevel 				= 'WARN';
	$recipients = 'bill.andrew@gmail.com';
	for ( 	$i = 1; $i < $argc; $i++ ) {
		$arg = $argv[$i];
		if ( $arg == '-c' ) $connectString 	= $argv[++$i];
		if ( $arg == '-v' )	$verbose 		= true;
		if ( $arg == '-l' ) $loglevel		= $argv[++$i];
		if ( $arg == '-r' ) $recipients     = $argv[++$i];
	}

	if ( $verbose ) {
		echo "sphinx indexer v1.0\n";
		echo "verbose\n";
		echo "recipients: $recipients\n";
	}

	if ( $connectString == null ) 		throw new Exception( "No database connect string supplied" );
	$dbconnector 	= new dbConnector( $connectString );

	if ( $verbose ) echo "Connector: " . $dbconnector->toString() . "\n";

	$dbconnection 	= $dbconnector->connect();
	if ( !$dbconnection ) throw new Exception( 'Failed to connect to database ' . $argv[1] );
	$logger		= new dbLogger( $dbconnection, Logger::levelVal($loglevel) );

	// Tell the logger to unconditional indicate a start to the logging session.
	//
	$logger->intro();

	$indexer 	= new SphinxIndexer( $dbconnection, $logger, $recipients /*, $intervalSecs*/, $verbose );
	$indexer->poll();
}
catch( Exception $exception ) {
	echo 'Exception raised ['. $exception->getMessage()."]";
}

function sig_handler($signo) {
	switch($signo) {
		case SIGTERM:  echo "break signalled]n"; break;
		default:   // intentional noop
	}
}











class SphinxIndexer {
	private $verbose;
	private $logger;
	private $connection;
	private $localhost;
	private $pollSql;
	private $hostInfo;
	private $email_alert_address	= 'bill.andrew@gmail.com';

	public function __construct( $connector, $logger, $recipients = 'bill.andrew@gmail.com'/*, $intervalSecs = 60*/, $verbose = false ) {
		$this->verbose 				= $verbose;
		$this->logger 				= $logger;
		$this->connection 			= $connector;
		$localhost 					= system('hostname');
		$this->localhost			= $localhost;
		$this->email_alert_address 	= $recipients;

		$this->loadHostInfo();
		$this->statusSql 		
			=	"select"
					. 	" *, hosts.active as host_active, control.active as index_active, hosts.sphinx_end_point "
				. "from "
					. 	"sphinx_indexer_hosts as hosts "
					. 		"join sphinx_indexer_control as control on hosts.host_id=control.host_id "
				. "where "
					.	"host = '$this->localhost' ";

		$this->pollSql = $this->statusSql . " and next_index < unix_timestamp()"; 
	}

	function getItemsIndexed( $sphinx_end_point ) {
		$this->logMessage( Logger::DEBUG, 'debug', "Connecting to sphinx endpoint [$sphinx_end_point]" );
		$connection = mysql_connect( $sphinx_end_point );
		if ( $connection == false ) {
			$this->logMessage(Logger::ERROR, 'sphinx-indexer', 'Error connecting to sphinx index - ' . mysql_errno() . ":" . mysql_error() );
			return -1;
		}

		$sql 	= "SELECT  COUNT(DISTINCT has_thumbnail) FROM people_srch_idx GROUP BY has_thumbnail";
		$rs 	= mysql_query( $sql, $connection );
		$sum 	= 0;

		while ( $tuple = mysql_fetch_array( $rs,MYSQL_ASSOC ) ) {
			$sum += $tuple['@count'];
		}
		mysql_close( $connection );
		return $sum;
	}

	function poll() {

		// Send out initial alerts.  That the polling loop is starting on this host and
		// an enumeration of what indexes are enabled/disabled
		//
		$initialstate  = $this->getState( $this->statusSql );

		if ( !isset($initialstate) ) {
			$this->logMessage( Logger::ERROR, 'process', "Failed to acquire initial state!" );
			exit;
		}

		$this->reportInitialState($initialstate);

		try {
			// Loop until we are told to exit.
			while ( true ) {
				$this->loadHostInfo();

				$newstate 		= $this->getState($this->statusSql);
				$state_changes 	= $this->compareState($initialstate, $newstate );
				$this->reportStateChanges($state_changes);
				$initialstate  	= $newstate;

				$pollData		= $this->getState($this->pollSql);

				$this->processPollData($pollData );
				$this->logMessage( Logger::DEBUG, 'process', 'Polling complete! Idling for ' . $this->hostInfo['poll_interval_secs'] );
				sleep( $this->hostInfo['poll_interval_secs'] );
			}
		}
		catch( Exception $exception ) {
			$this->logMessage( Logger::ERROR, 'process', "[error] $exception->getMessage()" );
//			echo 'Exception raised ['. $exception->getMessage()."]";
		}
	}

	function processPollData($pollData) {
		if (!isset($pollData )) return;

		foreach( $pollData["$this->localhost"]['index_state'] as $index_name => $index_state ) {

			$this->logMessage( dbLogger::DEBUG, 'process', 'Processing index [$index_name]' );

			// Guard clause. Only proceed if host and index is active...
			//
			if ( ($index_state->index_active == 0) || ($index_state->host_active == 0 ) ) continue;

						 

			if ( $index_state->countdown == 0 ) $this->doFullRebuild( 		 $index_state->host, $index_name );
			else								$this->doIncrementalIndex( $index_state->host, $index_name );


			$itemsIndexed = $this->getItemsIndexed( $this->hostInfo['sphinx_end_point'] );

			// If we just did an incremental update and the item count dropped, we assume that the index is corrupted
			//
			if ( ($index_state->countdown != 0 )
						&& ( $itemsIndexed < $index_state->last_item_count ) ) 				{
				$this->logMessage( dbLogger::ERROR, 'process', "Corrupted index detected [$itemsIndexed < $index_state->last_item_count]" 	);
				$this->doFullRebuild();
				$itemsIndexed = $this->getItemsIndexed();
			}


			$updates = array();
			$updates['countdown'] = (($index_state->countdown == 0)
												? $index_state->countdown_reset
												: $index_state->countdown - 1);
			$updates['last_indexed'] 	= 'unix_timestamp()';
			$updates['next_index']     	= "unix_timestamp() + " . $index_state->op_interval_secs;
			$updates['last_item_count'] = $itemsIndexed;

			$updatesql = dbHelper::generateUpdate( "sphinx_indexer_control"                        
										  , $updates                                      
										  , array( "index_id = $index_state->index_id" ) );

			$this->logMessage( Logger::DEBUG, 'process', "Next operation on $index_name in $index_state->op_interval_secs" );
			if ( $this->connection->query( $updatesql ) == false )	{
				if ( $this->connection->errno ) 	{
					$errmsg =  $this->connection->error;
					$this->logMessage( Logger::ERROR, 'process', "Error updating index state: $errmsg" );			
//					echo "Exception executing update [$updatesql]\n$errmsg\n";
				}
			}
		}

	}


	function getState($sql) {

		$this->logMessage( dbLogger::DEBUG, 'sphinx-indexer', 'Acquiring process state' );
		
		if ( !($indexes = $this->connection->query( $sql ) ) ) {
			$body = "[$this->localhost] Starting polling loop<br/>";
			$msg 	= $this->connection->errno . ":".  $this->connection->error;
			$this->logMessage( dbLogger::ERROR, 'process',  $msg );
			$this->emailMessage( "[$this->localhost] Failed to acquire indexer state [$msg]" );
		}
		else {
			$index_states 	= array();
			$host_state 	= 0;
			$noresults		= true;
			while ( $tuple = $indexes->fetch_object() ) {
				$noresults = false;
				$host_state 		= $tuple->host_active;
				$index_states["$tuple->index_name"] = $tuple;
			}
				
			if ( $noresults ) return; // array("$this->localhost" => array() );

			$temp = array( 'host_state'			=> $host_state
						   , 'index_state' 		=> $index_states );
			return array( "$this->localhost" 	=> $temp );
		}
	}

	function loadHostInfo() {
		$tuple = null;
		$rs = $this->connection->query( "select * from sphinx_indexer_hosts where host = '$this->localhost'" );
		if ( !($this->hostInfo = $rs->fetch_array(MYSQL_ASSOC) ) )
		{
			$this->logMessage( Logger::ERROR, 'process', "Unabled to acquire host info for host " . $this->localhost );
			exit;
		}
		$this->logger->setLogLevel( Logger::levelVal( $this->hostInfo['log_level'] ) );

	}

	function doFullRebuild( $host, $index) {
		if ( $this->verbose )  echo "full rebuild [$index]\n";
		$deltastarttime = microtime(true);
		$command = "/usr/local/bin/indexer --config /usr/local/etc/sphinx.conf --rotate ".$index."_srch_idx --quiet";
		$this->doSystem($command);
		//		$this->logMessage( dbLogger::DEBUG, 'process', "cmd: $command", $index );
		$deltaendtime = microtime(true);
		$this->logMessage(Logger::CRITICAL, 'stat', "full,$index,$deltastarttime,$deltaendtime,0", $index  );

		// Start capturing new metrics to facilitate inclusion of merge time
		//
		$this->logMessage(Logger::CRITICAL, 'stat1', "full,$index,$deltastarttime,$deltaendtime", $index  );
	}

	/**
	 * Performs an incremental index by first capturing a delta of the index then by applying
	 * the delta in a merge.
	 * 
	 * Note: in order to better facilitate the use of full index rebuilds and capture distinct delta and merge
	 * metrics, this method now outputs two separate stat log entries, one for the delta acquisition and another
	 * for the merge application.  In prior versions, all of the merge metrics were captured in a single stat
	 * log entry.  This new stat model is preferable because it more closely matches the reporting format and
	 * doesn't require specialized processing for delta/merge v full index operations when reporting.
	 */
	function doIncrementalIndex($host,$index) {
		if ( $this->verbose ) echo "incremental [$index]\n";
		$deltastarttime = microtime(true);
		$command = "/usr/local/bin/indexer --config /usr/local/etc/sphinx.conf --rotate ".$index."_srch_delta --quiet";
		//		$this->logMessage( dbLogger::DEBUG, 'process', "incremental: $command", $index );
		$this->doSystem($command);
		$deltaendtime = microtime(true);
		$this->logMessage(Logger::CRITICAL, 'stat1', "delta,$index,$deltastarttime,$deltaendtime", $index  );

		// Experimental:  Periodically, we get errors from incremental merging that various export files are missing.
		// On the hunch that this is due to some files still being open, adding a sleep here to see if that mitigates
		// the issue.
		sleep(5);

		$command = "/usr/local/bin/indexer --config /usr/local/etc/sphinx.conf --merge ".$index."_srch_idx ".$index."_srch_delta --rotate --quiet";
		//		$this->logMessage( dbLogger::DEBUG, 'process', "incremental: $command", $index );
		$this->doSystem($command);

		$mergeendtime = microtime(true);

		$this->logMessage(Logger::CRITICAL, 'stat', "incremental,$index,$deltastarttime,$deltaendtime,$mergeendtime", $index );
		$this->logMessage(Logger::CRITICAL, 'stat1', "merge,$index,$deltaendtime,$mergeendtime", $index  );
	}

	/**
	 * Helper function to perform logging.  Specifically, it supplies data to the logger that is static for the
	 * entire run of sphinxloop.
	 */
	function logMessage($level,$class,$msg, $index='') {
		$this->logger->logMessage($level,"$class", "$msg", array( 'host' => $this->localhost, 'index_name' => $index ) );
	}

	/**
	 * This function encapsulates call to php's system() function for the purpose of emailing an
	 * alert to configured mail recipients in the event that the command does not complete successfully
	 */
	function doSystem( $command ) {
		$ret = system($command);
		if (strlen(trim($ret)) == 0 ) return;
		$this->logMessage( Logger::WARN, 'process', "Execution of command [$command] returned [$ret]" );
	    $this->emailMessage( "[$this->localhost] Unexpected feedback: [$ret]\n executing command: [$command]\n" );
	}

	/**
	 * Given an array of changes in the indexer's state data, as detected by compareState(), 
	 * send an email to configured  recipients alerting them to the change.
	 */
	function reportStateChanges($state_changes) {
		if ( count($state_changes) <= 0 ) return;
		$this->logMessage( dbLogger::DEBUG, 'sphinx-indexer', 'Process state change detected' );
		$body = '';
		foreach( $state_changes as $v ) $body .= $v . "<br/>";
		$this->emailMessage($body );
	}

	/**
	 * Called when the indexer starts it's polling loop, this function sends an email to configured 
	 * recipients on the status of the indexer when it is started.  Modifications to the enabled/disabled
	 * state made after this call will be reported by reportStateChanges.
	 */
	function reportInitialState($initialstate) {
		$host_state = $initialstate["$this->localhost"]['host_state'];
		$body 		= "[$this->localhost] Starting polling loop<br/>";
		$body 		.= "Indexer at [$this->localhost] is ". ($host_state > 0 ? 'active' : 'inactive') . "<br/>";
		foreach( $initialstate["$this->localhost"]['index_state'] as $k => $v ) {
			$body .= "Index [$k] is " . (($v->index_active > 0 ) ? 'active' : 'inactive') . "<br/>";
		}
		$this->emailMessage( $body );
	}


	function compareState( $oldstate, $newstate ) 	{

		$messages = array();

		if ( $oldstate["$this->localhost"]['host_state'] != $newstate["$this->localhost"]['host_state'] )
		{
		    $messages[] = "Indexer at [$this->localhost] has been " . (($newstate["$this->localhost"]['host_state'] > 0)?'enabled':'disabled'); 
		}


		foreach( $oldstate["$this->localhost"]['index_state'] as $index_name => $old_index_state ) {

			$new_index_state = $newstate["$this->localhost"]['index_state'][$index_name];
			if ( !isset($new_index_state) ) {
				$messages[] = "Index [$index_name] at [$this->localhost] has been removed from the configuration!";
			}
			else {
				if ( $old_index_state->index_active != $new_index_state->index_active )
				{
					$messages[] = "Index [$index_name] at [$this->localhost] has been " . (($new_index_state->index_active> 0)?'enabled':'disabled'); 
				}
			}
		}
		
		// make a final pass over the new state to detect new index configurations
		foreach( $newstate["$this->localhost"]['index_state'] as $index_name => $new_index_state ) {
			$old_index_state = $oldstate["$this->localhost"]['index_state'][$index_name];

			if ( !isset($old_index_state) ) {
				$messages[] = "Index [$index_name] at [$this->localhost] has been added to the configuration!";
			}
		}
		
		
		return $messages;
	}
	
	/**
	 * Helper function to send an email to the configured email recipients.
	 */
	function emailMessage( $body ) {
		$address	= $this->email_alert_address;
		$subject 	= "Sphinx indexer alert from [$this->localhost]";
		$this->logMessage( Logger::INFO, 'email', "Sending email [$address][$subject][$body]" );
		$errlist = array();
		$errlist[] = array("Action", $body );
		mail_error($errlist, $subject , $address );
	}
};

?>



