-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 

drop table if exists sphinx_indexer_log;
drop table if exists sphinx_indexer_control;
drop table if exists sphinx_indexer_hosts;

-- Default poll interval is 5 minutes
-- Connecting to sphinx with the mysql client doesn't work by simply specifying local host

create table sphinx_indexer_hosts (
	host_id 			         int				auto_increment
	, host				      varchar(128)	not null
   , sphinx_end_point      VARCHAR(128)   DEFAULT NULL
	, active			         int				not null default 0
	, poll_interval_secs		int				not null default 300
   , primary key( host_id )
	, unique key( host )
);

create table sphinx_indexer_control (
   index_id 			int				auto_increment
	, host_id			int				not null
   , index_name    	varchar(64) 	not null
	, active			int 		 	not null default  0
	, countdown_reset 	int 			not null default  0
	, countdown     	int 		 	not null default 10
	, last_item_count	int				not null default  0
	, last_indexed		int				not null default  0
	, primary key(	index_id	)
    , unique key( 	host_id, index_name 	)	
	, foreign key (host_id) references sphinx_indexer_hosts (host_id) on delete cascade on update cascade
	
);

--	, foreign key (host_id) references sphinx_indexer_hosts (host_id) on delete cascade on update cascade
--	, foreign key (index_id) references sphinx_indexer_control (index_id) on delete cascade on update cascade
create table sphinx_indexer_log (
	lid					int 			   auto_increment
	, time 			   varchar(32)		not NULL
	, level				varchar(32)
	, class				varchar(32) 	not null
	, message			varchar(512) 	not null
	, host				varchar(128)
   , index_name  		varchar(64)
	, primary key( lid )
);


-- TODO: Break these out to individual solutions so that we're not keeping data for all
-- environments on all servers.
--

-- Bill's laptop
--
insert into sphinx_indexer_hosts 	   ( host                     , sphinx_end_point               , active , poll_interval_secs             ) 					
values                                 ( 'bandrew-PC'             , NULL                           , 1      , 420                            );

insert into sphinx_indexer_control 	   ( host_id      		      , index_name	                  , active , countdown_reset , countdown    ) 	
values                                 ( last_insert_id()	      , 'test1'                        , 1      , 10              , 10           )
		                                 , ( last_insert_id()	      , 'test2'                        , 1      , 10              , 10           );
		
-- Bill's home linux box
--
insert into sphinx_indexer_hosts 	   ( host                     , sphinx_end_point               , active , poll_interval_secs             ) 					
values                                 ( 'ubuntu-ursa'            , NULL                           , 1      , 420                            );

insert into sphinx_indexer_control 	   ( host_id      		      , index_name	                  , active , countdown_reset , countdown    ) 	
values                                 ( last_insert_id()	      , 'test1'                        , 1      , 10              , 10           )
		                                 ,( last_insert_id()	      , 'test2'                        , 1      , 10              , 10           );	
-- Zerby's linux box
--
insert into sphinx_indexer_hosts 	   (  host                    , sphinx_end_point               , active , poll_interval_secs             )					
values                                 (  'rzerby-lnx.kaneva.com' , 'rzerby-lnx.kaneva.com:9306'   , 1      , 420                            );

insert into sphinx_indexer_control     (  host_id			         , index_name	                  , active , countdown_reset  , countdown   )
values                                 (  last_insert_id()	      , 'wokitems'	                  , 1	   , 10              , 10           )
                                       ,( last_insert_id()        , 'people'	                     , 1	   , 10              , 10           );
-- Preview environment
--
insert into sphinx_indexer_hosts       (  host                    , sphinx_end_point               , active , poll_interval_secs             )                                 
values                                 ( 'pv-sphinx1'             , "pv-sphinx1.kaneva-dc.net:9306", 1      , 420                            );

insert into sphinx_indexer_control     (  host_id                 , index_name                     , active , countdown_reset , countdown    )
values                                 (  last_insert_id()        , 'wokitems'                     , 1      , 10              , 100          )
                                       ,( last_insert_id()        , 'people'                       , 1      , 10              , 100          );
-- Production environment
--
insert into sphinx_indexer_hosts       (  host                    , sphinx_end_point               , active , poll_interval_secs             )  
values                                 (  'sphinx1'               , "sphinx1.kaneva-dc.net:9306"   , 1      , 30                             );

insert into sphinx_indexer_control     (  host_id                 , index_name                     , active , countdown_reset , countdown    )
values                                 (  last_insert_id()        , 'wokitems'                     , 1      , 100             , 100          )
                                       ,( last_insert_id()        , 'people'                       , 1      , 100             , 100          );
	
