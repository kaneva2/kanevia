-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 


-- Add log level
ALTER TABLE sphinx_indexer_hosts
   ADD COLUMN sphinx_end_point VARCHAR(128) 
   , ADD COLUMN log_level VARCHAR(32) NOT NULL DEFAULT 'WARN';


-- Add field op_interval_secs
-- Add field next_index
-- change last_item_count, last_indexed to unsigned values

ALTER TABLE sphinx_indexer_control
   ADD COLUMN op_interval_secs int unsigned NOT NULL DEFAULT 300
   , ADD COLUMN next_index int unsigned NOT NULL DEFAULT 0
   , MODIFY COLUMN last_item_count int unsigned NOT NULL DEFAULT 0
   , MODIFY COLUMN last_indexed int unsigned NOT NULL DEFAULT 0;
