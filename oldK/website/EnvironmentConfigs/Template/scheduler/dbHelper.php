<?php

class dbHelper {
    static function generateUpdate( $table, $values, $conditions ) {
		$setClause = " set ";
		$delim = "";
		foreach ( $values as $k => $v ) {
			$setClause .= $delim . "$k=$v";
			$delim = ",";
		}
	
		$whereClause = "";
		$delim = "where ";
		foreach( $conditions as $k => $v )	{
			$whereClause .= $delim . $v;
			$delim = " and ";
		}
	
		$ret = "update $table $setClause $whereClause";
		return $ret;
	}

	static function generateInsert( $table, $values ) {
		if ( $table == null ) throw new Exception( "Null pointer exception" );
		if ( count($values) == 0 ) throw new Exception( "No values" );
		$ret 			= "";
		$valuesClause 	= "";
		$columnsClause 	= "";
		$delim 			= "";

		foreach ( $values as $k => $v ) {
			$columnsClause .= $delim . "$k";

			if ( stripos( $v, '#plain#' ) === false )
			{
				$valuesClause .= $delim . "'$v'";
			}
			else {
			    $v = str_replace( '#plain#', '', $v );
				$valuesClause .= $delim . "$v";
			}
					
			$delim = ",";
		}
		$ret = "insert into $table ($columnsClause) values ($valuesClause)";
		return $ret;
	}
};

?>