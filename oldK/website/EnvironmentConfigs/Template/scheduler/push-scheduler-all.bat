pscp dbConnector.php root@%1:/root
plink %1 -l root dos2unix /root/dbConnector.php

pscp dbHelper.php root@%1:/root
plink %1 -l root dos2unix /root/dbHelper.php

pscp dbLogger.php root@%1:/root
plink %1 -l root dos2unix /root/dbLogger.php

pscp email_config.php root@%1:/root
plink %1 -l root dos2unix /root/email_config.php

pscp fileLogger.php root@%1:/root
plink %1 -l root dos2unix /root/fileLogger.php

pscp index_list.php root@%1:/root
plink %1 -l root dos2unix /root/index_list.php

pscp Logger.php root@%1:/root
plink %1 -l root dos2unix /root/Logger.php

pscp sphinxloop.php root@%1:/root
plink %1 -l root dos2unix /root/sphinxloop.php

pscp os.php root@%1:/root
plink %1 -l root dos2unix /root/os.php

pscp sphinxmonitor.php root@%1:/root
plink %1 -l root dos2unix /root/sphinxmonitor.php

pscp sphinxreporter.php root@%1:/root
plink %1 -l root dos2unix /root/sphinxreporter.php

