<?php

include_once('dbConnector.php');
include_once('dbHelper.php' );
/**
 * This class is the precursor to a logging infrastructure for Kaneva PHP applications.
 * Currently it makes assumptions about 1) the output target and 2) application specific 
 * data (e.g. indexer_host, index name).  But there are some common pieces like logging level,
 * class and message we can leverage later on.
 * 
 * 1) add abstractions for application specific ancillary data (e.g. indexer_host, index).
 * 2) add abstractions for output target (e.g. file, db, email, remote call etc)
 *
 * NOTE: Investigate existing (e.g. PearLog) logging solutions before performing any of these 
 * abstractions
 */
abstract class Logger {
	const DEBUG 	= 100;
	const INFO  	=  75;
	const WARN  	=  50;
	const SYSTEM 	=  35;
	const ERROR 	=  25;		    
	const FATAL 	=   5;
	const CRITICAL  =   0;

	static $levelTags = array(
		Logger::DEBUG 		=> 'DEBUG'
		, Logger::INFO 		=> 'INFO'
		, Logger::WARN 		=> 'WARN'
		, Logger::SYSTEM 	=> 'SYSTEM'
		, Logger::ERROR 	=> 'ERROR'
		, Logger::FATAL 	=> 'FATAL'
		, Logger::CRITICAL  => 'CRITICAL'
	);

	static $levelVals = array(
		'DEBUG' 			=> Logger::DEBUG
		, 'INFO' 			=> Logger::INFO
		, 'WARN' 			=> Logger::WARN
		, 'SYSTEM' 			=> Logger::SYSTEM
		, 'ERROR' 			=> Logger::ERROR
		, 'FATAL' 			=> Logger::FATAL
		, 'CRITICAL'        => Logger::CRITICAL
	);

	private $log_level;
	
	function __construct( $level = dbLogger::LOG_LEVEL_INFO ) 	{ $this->log_level = $level;								}
	function intro() 											{ $this->logMessage( Logger::SYSTEM, 'logger', '*** logging started' );	}
    function logLevel() 										{ return $this->log_level;									}
	function setLogLevel($level)								{ $this->log_level = $level;								}
	function wouldLog( $level ) 								{ 
		return ( $level <= $this->log_level );					
	}

	static function levelTag( $level )							{ return self::$levelTags[$level];	 						}
	static function levelVal( $tag   )                          { return self::$levelVals[$tag];                            }
	

	abstract function logInternal( $level, $class, $msg, $ancillary = null);
	
	function logMessage( $level, $class, $msg, $ancillary = null) {
		if ( !$this->wouldLog($level ) ) return;
		$this->logInternal( $level, $class, $msg, $ancillary );
	}
	

	static function testout($message) {
   		$handle = fopen( "logger.dbg", "a" );
   		fputs( $handle, time() . " $message\n" );
   		fclose( $handle );
	}
};


?>