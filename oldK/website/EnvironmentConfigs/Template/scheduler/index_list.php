<?
$hostname = system("hostname");
print "$hostname\n";

$index_lists = array();

$index_lists['ubuntu-ursa'] 
	= array(	'connect' => 'sphinx_indexer:indexer@localhost/sphinx_indexer'
				, 'pollintervalsecs' => 4									);    

$index_lists['rzerby-lnx.kaneva.com'] 
	= array(	'connect' => 'sphinx_indexer:indexer@localhost/sphinx'
				, 'pollintervalsecs' => 500									);

// Same Host, different name.  What changed?
$index_lists['pv-sphinx1'] 
	= array( 	'connect' => 'sphinx_indexer:indexer@pv-db2.kaneva-dc.net/sphinx_indexer'
				, 'pollintervalsecs' => 500
				, 'dbaalerts@kaneva.com,bandrew@kaneva.com' 						);
$index_lists['pv-sphinx1.kaneva-dc.net'] 
	= array( 	'connect' => 'sphinx_indexer:indexer@pv-db2.kaneva-dc.net/sphinx_indexer'
				, 'pollintervalsecs' => 500
				, 'email' => 'dbaalerts@kaneva.com,bandrew@kaneva.com' 						);
$index_lists['sphinx2'] 
	= array(	'connect' => 'sphinx_indexer:indexer@db2.kaneva-dc.net/sphinx_indexer'
		   		, 'pollintervalsecs' => 30
				, 'email' => 'dbaalerts@kaneva.com' 	);

$index_lists['sphinx2.kaneva-dc.net'] 
	= array(	'connect' => 'sphinx_indexer:indexer@db2.kaneva-dc.net/sphinx_indexer'
		   		, 'pollintervalsecs' => 30
				, 'email' => 'dbaalerts@kaneva.com' 	);


$host_config = $index_lists[$hostname];
?>
