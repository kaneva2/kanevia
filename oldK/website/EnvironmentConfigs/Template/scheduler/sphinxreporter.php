#!/usr/bin/php
<?php
error_reporting(E_ALL);
include "email_config.php";
include_once 'dbConnector.php';
include_once 'os.php';
//include_once script_path() . "index_list.php";
include_once "index_list.php";


//$date = date("YmdHis",time());
//$date = date("Y-m-d",time());
$date = date("Y-m-d",time()  - 24*60*60);
echo "$date\n";


$connector = new dbConnector( $host_config['connect'] );
echo $connector->toString();
$connection = $connector->connect();


//$cfg 	= get_cfg( $connection, $host_config, $hostname );
$cfgrpt = report_cfg( get_cfg( $connection, $host_config, $hostname ) );

//$report = get_data( $connection, $host_config, $date );


$rpt = report_metrics(get_data( $connection, $host_config, $date ));

//$tableheader = array( 'header1' => ''
//					  , 'header2' => '' );

//$reportstr="<table border=1>";

//$reportstr.="<tr bgcolor=#DDDDFF><th>index</th><th>count</th><th>min</th><th>max</th><th>avg</th></tr>\n";
//foreach($report as $index=>$rollup) {
//    $reportstr .= "<tr><td>$index</td>" 
//					. gen_tr_data($rollup) 
//					.  "</tr>\n";
//}
//$reportstr.="</table>";





//echo $reportstr;
mail( $host_config['email']
	  ,"Sphinx Indexer Status from [$hostname] for [$date]"
	  , $cfgrpt . $rpt
	  , "From:sphinx@".exec('hostname')."\nContent-Type: text/html; charset=iso-8859-1\nContent-Transfer-Encoding: 8bit\n");

function report_metrics( $metrics ) {
	$tableheader = array( 'header1' => ''
					  , 'header2' => '' );

	$reportstr="<h3>Metrics</h3>";
	$reportstr.="<table border=1>";

	$reportstr.="<tr bgcolor=#DDDDFF><th>index</th><th>count</th><th>min</th><th>max</th><th>avg</th></tr>\n";
	foreach($metrics as $index=>$rollup) {
		$reportstr .= "<tr><td>$index</td>" 
					. gen_tr_data($rollup) 
					.  "</tr>\n";
	}
	$reportstr.="</table>";
	return $reportstr;
}

function get_cfg( $connection, $host_config, $hostname ) {
	$sql = "select * from sphinx_indexer_hosts h, sphinx_indexer_control c where h.host_id=c.host_id and host='$hostname' order by index_id";
	$rs = $connection->query($sql);
	$cfg = array();
	while( $tuple = $rs->fetch_object() ) {
	  $cfg[] = $tuple;
	}
	return $cfg;
}

function report_cfg($cfg) {
	$ret = "<h3>Configuration</h3>";
	$ret .= '<table border="1">';
	$ret .= "<tr bgcolor=#DDDDFF><th>Index Name</th><th>Active</th><th>Operation Interval (secs)</th><th>Full Rebuild Every</th></tr>";

	foreach( $cfg as $v ) {
		$t = "<tr>";
		$t .= "<td>$v->index_name</td>";
	    $t .= "<td>" . ($v->active==0?"inactive":"active") . "</td>";
		$t .= "<td>$v->op_interval_secs</td>";
		$t .= "<td>$v->countdown_reset</td>";
		$t .= "</tr>";
		$ret .= $t;
	}


	$ret .= "</table>";
	return $ret;
}


function get_data( $connection,$host_config, $date ) {
//	$connector = new dbConnector( $host_config['connect'] );
//	echo $connector->toString();
//	$connection = $connector->connect();
	$sql = "select * from sphinx_indexer_log where class = 'stat1' and time like '$date%' order by lid";
//	echo $sql;
	$rs=$connection->query( $sql );

	// In this array we roll up all of the actual output information
	//
	$report = array();

	// roll up the data...
	//
	while( $tuple = $rs->fetch_object() ) {
		$parts = explode( ',', $tuple->message );
		$operation 	= $parts[0];
		$index 	    = $parts[1];
        $rollupName = "$index - $operation";

		if ( !isset($report[$rollupName]))	$report[$rollupName]   = null; // 		= array();
		$report[$rollupName] = rollup( $parts, $report[$rollupName] );
	}
	$rs->free();

	ksort($report);
	return $report;
}



function gen_tr_data( $rollup ) {
    $reportstr = sprintf(  "<td align='right'>%d</td>",$rollup["count"]);
    $reportstr .= sprintf( "<td align='right'>%0.4f</td>",$rollup["min-elapsed"]);
    $reportstr .= sprintf( "<td align='right'>%0.4f</td>",$rollup["max-elapsed"]);
    $reportstr .= sprintf( "<td align='right'>%0.4f</td>",$rollup["cumm-elapsed"]/$rollup["count"] );
	return $reportstr;
}

function rollup( $parts, $rollup ) {

	if ( $rollup == null ) $rollup = array(	"count"				=> 0
											, "min-elapsed"		=> INF
											, "cumm-elapsed"	=> 0
											, "max-elapsed"		=> -1 	);

	$start 		= $parts[2];
	$end 		= $parts[3];

	// N.B.: Also note that the current implementation only tracks the time 
	// spent creating the merge file, not the time applying the merge...
	// So this implementation does this as well
	//
	$elapsed 		= $end - $start;
	$rollup["count"]++;
	$rollup["cumm-elapsed"] += $elapsed;
	$rollup["min-elapsed"] = min( $rollup["min-elapsed"], $elapsed );
	$rollup["max-elapsed"] = max( $rollup["max-elapsed"], $elapsed );
	return $rollup;
}
?>
