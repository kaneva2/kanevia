<?php

class os {
	static function isWindows() {
		if ( !array_key_exists( 'OS', $GLOBALS['_SERVER'])) return false;	
  		return ! stristr( $GLOBALS['_SERVER']['OS'], 'WINDOWS' );
	}

	static function processDetected($procfragment) {
//		$cmd = "ps -ef | grep -v grep | grep $procfragment | awk '{ print $2 }'";
		$output = system("ps -ef | grep -v grep | grep $procfragment | awk '{ print $2 }'");
		return is_numeric($output);
	}

	static function getProcessId($procfragment) {
		$output = system("ps -ef | grep -v grep | grep $procfragment | awk '{ print $2 }'");
		return is_numeric($output) ? $output : null;
	}
};

function script_path() {
	global $argc;
	global $argv;

	$pathparts = explode( '/', $argv[0] );

	// Check for relative path
	//
	if ( $pathparts[0] == '.' ) {
		$fullpath = getcwd() . "/" . $pathparts[count($pathparts)-1];
		$pathparts = explode( "/", $fullpath );
	}
	unset( $pathparts[count($pathparts)-1] );
	return implode( "/", $pathparts ) . "/";
}

?>