<?php

class dbConnector {

    private $dbhost;
	private $dbuser;
	private $dbpwd;
	private $dbname;
	
    function __construct( $connectString ) {
		$connectorParts = explode( '@', $connectString );
		if ( count($connectorParts) != 2 ) throw new Exception( "Invalid connect string [$connectString]" );
		$userParts = explode( ':', $connectorParts[0] );
		if ( count($userParts) != 2 ) throw new Exception( "Invalid connect string [$connectString]" );
		$dbParts = explode( '/', $connectorParts[1] );
		if ( count($dbParts) != 2 ) throw new Exception( "Invalid connect string [$connectString]" );
		
		$this->dbhost = $dbParts[0];
		
		$this->dbname = $dbParts[1];
		$this->dbuser = $userParts[0];
		$this->dbpwd  = $userParts[1];
	}
	function toString() { return "$this->dbuser:***@$this->dbhost/$this->dbname\n"; }
	function connect() 	{ 
		echo "Connecting...\n";
		$ret =  new mysqli( $this->dbhost, $this->dbuser, $this->dbpwd, $this->dbname );
		if ( mysqli_connect_errno() ) {
			throw new Exception( "Failed to connect to database: " . mysqli_connect_error() );			
		}
		return $ret;	
	}
};
?>
