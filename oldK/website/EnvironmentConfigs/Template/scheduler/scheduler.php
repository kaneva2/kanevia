#!/usr/bin/php
<?
$thisfile = $argv[0];
$command = "";
$seconds = 60;

for($i = 1; $i< $argc; $i++){
    $arg = $argv[$i];

    if($arg == '-c'){
        $i++;
        $command = $argv[$i];
    } elseif($arg == '-s') {
        $i++;
        $seconds = $argv[$i];
    }
}

if($command == ""){
    echo "No command given\n";
    exit();
}

$time = time();
$endtime = $time + 60;
$nexttime = $time;

while($endtime > time()){
    if($nexttime > time()){
    } else {
        $cmd="ps -ef | grep -v grep | grep -v $thisfile | grep \"$command\" | awk '{ print $2 }'";
        echo time()."::".$cmd."\n";
        $output = system($cmd);
        if(!is_numeric($output)){
            echo system($command);
        } 
        $time = time();
        while($nexttime < $time)
            $nexttime += $seconds;
    }
    sleep(1); // so we don't busywait...
}
?>
