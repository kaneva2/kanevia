function SelectAll (theElement, checkit)
{
 var theForm = theElement.form;
 for (var i = 0; i<theForm.elements.length; i++) 
  {
	if (theForm[i].type == 'checkbox')
		{theForm[i].checked = checkit;}
  }
}

function SelectAllByParent (theElement, checkit)
{
    var collection = document.getElementById(theElement).getElementsByTagName('input');

    for (var x=0; x<collection.length; x++) {
        if (collection[x].type.toUpperCase()=='CHECKBOX')
            collection[x].checked = checkit;
    }
}

function Select_All (checkit)
{
	var theForm = document.forms[0];
	for (var i = 0; i<theForm.elements.length; i++) 
	{
		if (theForm[i].type == 'checkbox' && !theForm[i].disabled)
			{theForm[i].checked = checkit;}
	}
}

function setCheckedValue (radioObj, newValue) {
	if(!radioObj)
		return;
	var radioLength = radioObj.length;
	if(radioLength == undefined) {
		radioObj.checked = (radioObj.value == newValue.toString());
		return;
	}
	for(var i = 0; i < radioLength; i++) {
		radioObj[i].checked = false;
		if(radioObj[i].value == newValue.toString()) {
			radioObj[i].checked = true;
		}
	}
}

// This routine is called by the onLoad event - Handles the init of the display
function OnLoad()
{
	// Change the "Loading..." text
	ShowLoading (true, 'Rendering...');
	// Make sure the "Loading..." display is hidden (so we can see the data)
	// Use .setTimeout() to deal with bug when page is refreshed (and "Loading..." doesn't hide)
	window.setTimeout ('ShowLoading (false);',100);
}

// Shows or Hides the "Loading..." message
function ShowLoading(blnShow, strText)
{
	var divLoadingText	= document.getElementById ('divLoadingText');
	var divData			= document.getElementById ('divData');
	var divLoading		= document.getElementById ('divLoading');

	if (!divLoadingText)	return;
	if (!divData)			return;
	if (!divLoading)		return;

	if (blnShow)
	{
		if (strText)	divLoadingText.innerHTML = strText;
		divData.style.display	 = 'none';
		divLoading.style.display = '';
	}
	else
	{
		divLoading.style.display = 'none';
		divData.style.display	 = '';
		divLoadingText.innerHTML = 'Loading...';
	}
}

function textAreasInit()
{  
	var objs = document.getElementsByTagName("textarea");
	var oi = 0; // object index  
	var thisObj;  
	for (oi=0;oi<objs.length;oi++) 
	{   
		thisObj = objs[oi];   // MaxLength is case sensitve   
		if (thisObj.getAttribute('MaxLength'))
		{    
			thisObj.onkeyup = forceMaxLength;   
		}   
		///thisObj.onchange = saveEntryValue;  
	} 
} 

function forceMaxLength()
{  
	var maxLength = parseInt(this.getAttribute('MaxLength'));  
	if(this.value.length > maxLength)
	{   
		this.value = this.value.substring(0,maxLength);  
	} 
}

// addEvent - cross-browser event handling for IE5+,  NS6 and Mozilla 
function addEvent(elm, evType, fn, useCapture) 
{   
	if (elm.addEventListener)
	{  
		elm.addEventListener (evType, fn, useCapture);  
		return true;   
	} 
	else if (elm.attachEvent)
	{  
		var r = elm.attachEvent("on"+evType, fn);  
		return r;   
	} 
	else 
	{  
	//alert("Handler could not be removed");   
	} 
}

function ShowConfirmMsg(objId, showDuration){
	var obj = $(objId);
	if (obj) 
	{
		obj.style.visibility = "visible";
		if (obj.style.display == "none") 
		{
			obj.style.display = "";
		}
		window.setTimeout(function () { $(objId).style.visibility = "hidden"; }, showDuration);
	}
}

// checkForEnterKey - this method checks to see if the enter key was pressed
// and returns true if it was the last key pressed
function checkForEnterKey (e) 
{	
	var key;	
	
	if(window.event)
		key = window.event.keyCode;     //IE
	else
		key = e.which;
		
	if (key == 13)
		return true;
	else
		return false;                      
}

// Replace all occurrances of a string within a string
function ReplaceAll( inText, inFindStr, inReplStr, inCaseSensitive ) 
{
   //	inText is the text in which to do the search;
   //	inFindStr is the string to find;
   //	inReplStr is the string to substitute into inText in place of inFindStr; and
   //	inCaseSensitive is a boolean value (defaults to false).
   
   var searchFrom = 0;
   var offset = 0;
   var outText = "";
   var searchText = "";
   if ( inCaseSensitive == null ) {
      inCaseSensitive = false;
   }
   if ( inCaseSensitive ) {
      searchText = inText.toLowerCase();
      inFindStr = inFindStr.toLowerCase();
   } else {
      searchText = inText;
   }
   offset = searchText.indexOf( inFindStr, searchFrom );
   while ( offset != -1 ) {
      outText += inText.substring( searchFrom, offset );
      outText += inReplStr;
      searchFrom = offset + inFindStr.length;
      offset = searchText.indexOf( inFindStr, searchFrom );
   }
   outText += inText.substring( searchFrom, inText.length );
   
   return ( outText );
}
