///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class TopNav : BaseUserControl
    {
        private int _selectedMainNavId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (Request.IsAuthenticated)
                {
                    //get parameters
                    GetRequestParams();

                    //loads and configures the main menu
                    LoadNavigation();

                    aSignIn.Visible = false;
                    lnkLogout.Visible = true;
                }
                else
                {
                    aSignIn.Visible = true;
                    aSignIn.HRef = ResolveUrl("~/login.aspx?") ;
                    lnkLogout.Visible = false;
                }

            }
        }

        #region Navigation Functions

        private void LoadNavigation()
        {
            //populate the website selection pulldown
            PopulateWebsitesDropDown();

            //populate the appropriate Main navigation
            PopulateMainNav();
        }

        #endregion

        #region Helper Functions

        /// <summary>
        /// Populates Available websites Pull down
        /// </summary>
        private void PopulateWebsitesDropDown()
        {
            //load data
            ddl_SiteSelector.DataSource = WebCache.GetAvailableWebsites();
            ddl_SiteSelector.DataTextField = "WebsiteName";
            ddl_SiteSelector.DataValueField = "WebsiteId";
            ddl_SiteSelector.DataBind();

            //set the default
            if (SELECTED_SITE_ID > 0)
            {
                ddl_SiteSelector.SelectedValue = SELECTED_SITE_ID.ToString();
            }
            else
            {
                ddl_SiteSelector.SelectedIndex = 0;
            }
        }

        private void PopulateMainNav()
        {
            try
            {
                //pull the selected website id
                int webSiteId = Convert.ToInt32(ddl_SiteSelector.SelectedValue);
                Session["SelectedSiteId"] = ddl_SiteSelector.SelectedValue;

                //get the users allowed privlegegs for menu filtering
                string privilegeList = GetUsersAllowedPrivileges();

                //pull the main menu from the DB
                dl_MainMenu.DataSource = this.GetSiteManagementFacade().GetMainMenuFiltered(webSiteId, privilegeList);

                //Bind Main Menu to control
                dl_MainMenu.DataBind();

                //indicate the selected menu item
                SELECTED_SITE_ID = Convert.ToInt32(ddl_SiteSelector.SelectedValue); 

            }
            catch (Exception)
            {
            }
        }

        //get all possible parameters
        private void GetRequestParams()
        {
            //check to see if there is an invitation id or not
            try
            {
                if (Request["ws"] != null)
                {
                    SELECTED_SITE_ID = Convert.ToInt32(Request["ws"].ToString());
                }
            }
            catch (Exception)
            {
            }

            //check to see if there is a validation status or not
            try
            {
                if (Request["mn"] != null)
                {
                    _selectedMainNavId = Convert.ToInt32(Request["mn"].ToString());
                }
            }
            catch (Exception)
            {
            }
        }

        #endregion

        #region Attributes

        protected int SELECTED_SITE_ID
        {
            get
            {

                if (ViewState["_selectedSiteId"] == null)
                {
                    ViewState["_selectedSiteId"] = 0;
                }

                return (int)ViewState["_selectedSiteId"];
            }
            set
            {
                ViewState["_selectedSiteId"] = value;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Log out of the site
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            System.Web.HttpCookie aCookie;
            int limit = Request.Cookies.Count - 1;

            for (int i = limit; i != -1; i--)
            {
                aCookie = Request.Cookies[i];
                aCookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(aCookie);
            }

            FormsAuthentication.SignOut();
            Session.Abandon();
            Response.Redirect(ResolveUrl("~/login.aspx?"));
        }

        protected void ddl_SiteSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            //get the selected site id set it and redirect
            Session["SelectedSiteId"] = SELECTED_SITE_ID = Convert.ToInt32(ddl_SiteSelector.SelectedValue);
            Response.Redirect(GetHomePageURL("ws=" + SELECTED_SITE_ID.ToString()));
            //PopulateMainNav();
        }

        public void Nav_Command(Object sender, CommandEventArgs e)
        {
            Response.Redirect(GetHomePageURL("ws=" + SELECTED_SITE_ID.ToString() + "&mn=" + e.CommandArgument.ToString()));
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.ddl_SiteSelector.SelectedIndexChanged += new System.EventHandler(this.ddl_SiteSelector_SelectedIndexChanged);
        }
        #endregion

    }
}