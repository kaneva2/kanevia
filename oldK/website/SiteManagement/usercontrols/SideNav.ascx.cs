///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class SideNav : BaseUserControl
    {

        private int _selectedSubNavId = 0;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //gets the menu items based on URL parameters
                GetRequestParams();
                //loads and configures the main menu
                LoadNavigation();
            }
        }

        #region Navigation Functions

        private void LoadNavigation()
        {
            //populate the appropriate Main navigation
            PopulateSubNav();
        }

        private void PopulateSubNav()
        {
            try
            {
                //get the users allowed privlegegs for menu filtering
                string privilegeList = GetUsersAllowedPrivileges();

                //pull the main menu from the DB
                dl_SubMenu.DataSource = this.GetSiteManagementFacade().GetSubMenuFiltered(SELECTED_MAINNAV_ID, privilegeList);

                //Bind Main Menu to control
                dl_SubMenu.DataBind();
            }
            catch (Exception)
            {
            }
        }

        //get all possible parameters
        private void GetRequestParams()
        {
            try
            {
                if (Request["ws"] != null)
                {
                    SELECTED_SITE_ID = Convert.ToInt32(Request["ws"].ToString());
                }
            }
            catch (Exception)
            {
            }

            try
            {
                if (Request["mn"] != null)
                {
                    SELECTED_MAINNAV_ID = Convert.ToInt32(Request["mn"].ToString());
                }
            }
            catch (Exception)
            {
            }

            try
            {
                if (Request["sn"] != null)
                {
                    _selectedSubNavId = Convert.ToInt32(Request["sn"]);
                }
            }
            catch (Exception)
            {
            }
        }

        #endregion

        #region Attributes

        protected int SELECTED_MAINNAV_ID
        {
            get
            {

                if (ViewState["_selectedMainNavId"] == null)
                {
                    ViewState["_selectedMainNavId"] = 0;
                }

                return (int)ViewState["_selectedMainNavId"];
            }
            set
            {
                ViewState["_selectedMainNavId"] = value;
            }
        }

        protected int SELECTED_SITE_ID
        {
            get
            {

                if (ViewState["_selectedSiteId"] == null)
                {
                    ViewState["_selectedSiteId"] = 0;
                }

                return (int)ViewState["_selectedSiteId"];
            }
            set
            {
                ViewState["_selectedSiteId"] = value;
            }
        }

        #endregion

        #region Event Handlers

        public void Nav_Command(Object sender, CommandEventArgs e)
        {

            Response.Redirect(GetHomePageURL("ws=" + SELECTED_SITE_ID.ToString() + "&mn=" + SELECTED_MAINNAV_ID + "&sn=" + e.CommandArgument.ToString()));
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

    }
}