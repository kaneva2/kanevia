<%@ Control Language="c#" AutoEventWireup="True" Codebehind="StoreFilter.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.StoreFilter" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<!-- Begin Filter --->
<table cellpadding="2" cellspacing="2" border="0" width="100%">
<tr runat="server" id="alphaNumerics">
<td align="center" >
	<asp:LinkButton CssClass="glossaryLink" Text="All" CommandArgument="" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="0-9" CommandArgument="0-9" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="Sym" ToolTip="Symbols" CommandArgument="@" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="A" CommandArgument="A" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="B" CommandArgument="B" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="C" CommandArgument="C" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="D" CommandArgument="D" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="E" CommandArgument="E" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="F" CommandArgument="F" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="G" CommandArgument="G" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="H" CommandArgument="H" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="I" CommandArgument="I" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="J" CommandArgument="J" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="K" CommandArgument="K" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="L" CommandArgument="L" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="M" CommandArgument="M" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="N" CommandArgument="N" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="O" CommandArgument="O" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="P" CommandArgument="P" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="Q" CommandArgument="Q" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="R" CommandArgument="R" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="S" CommandArgument="S" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="T" CommandArgument="T" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="U" CommandArgument="U" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="V" CommandArgument="V" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="W" CommandArgument="W" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="X" CommandArgument="X" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="Y" CommandArgument="Y" OnCommand="lbSearch_Click"  runat="server"/>
	<asp:LinkButton CssClass="glossaryLink" Text="Z" CommandArgument="Z" OnCommand="lbSearch_Click"  runat="server"/>
</td>
</tr>
<tr runat="server" id="itemsPerPage">
<td align="center">
	<span class="insideBoxText11">Show:</span>&nbsp;
	<span id="spanViewFilter" runat="server">
		<asp:DropdownList runat="server" size="1" class="formKanevaSelect" ID="drpView" AutoPostBack="True" OnSelectedIndexChanged="drpView_Change"></asp:DropdownList> 
		<span class="insideBoxText11">W/</span>
	</span> 
	<asp:DropdownList runat="server" size="1" class="formKanevaSelect" ID="drpPages" AutoPostBack="True" OnSelectedIndexChanged="drpPages_Change"></asp:DropdownList> 
	<span class="insideBoxText11">Items/Page</span>
</td>
</tr>	
<tr runat="server" id="pullDownAlphaNumerics">
<td align="center">
	<span class="insideBoxText11">Show:</span>&nbsp;
	<asp:DropdownList runat="server" size="1" class="formKanevaSelect" ID="drpSort" AutoPostBack="True" OnSelectedIndexChanged="drpSort_Change"></asp:DropdownList> 
</td>
</tr>	
</table>

