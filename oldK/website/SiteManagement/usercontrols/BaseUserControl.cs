///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Collections.Specialized;
using System.Collections;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

using Microsoft.Reporting.WebForms;

namespace KlausEnt.KEP.SiteManagement
{
	/// <summary>
	/// Summary description for BaseUserControl.
	/// </summary>
	public class BaseUserControl : System.Web.UI.UserControl
	{
        #region Declarations
        //Common Functions
        private SiteManagementCommonFunctions _devComnFunc; 
        
        // Sorting
        protected HtmlForm m_Form;
        protected Button btnSort;
        protected HtmlInputHidden hidSortColumn;
        #endregion


        /// <summary>
        /// Gets an instance of the SiteManagement Common Functions class
        /// </summary>
        /// <returns>SiteManagementCommonFunctions</returns>
        public SiteManagementCommonFunctions GetDCFInstance
        {
            get
            {
                if (_devComnFunc == null)
                {
                    _devComnFunc = new SiteManagementCommonFunctions();
                }
                return _devComnFunc;
            }
        }
        
        // ***********************************************
        // Helper functions
        // ***********************************************
        #region Helper Functions


        public SiteMgmtFacade GetSiteManagementFacade()
        {
            return GetDCFInstance.GetSiteManagementFacade;
        }

        public SubscriptionFacade GetSubscriptionFacade()
        {
            return GetDCFInstance.GetSubscriptionFacade;
        }

        public PromotionsFacade GetPromotionsFacade()
        {
            return GetDCFInstance.GetPromotionsFacade;
        }

        public UserFacade GetUserFacade()
        {
            return GetDCFInstance.GetUserFacade;
        }

        public ExperimentFacade GetExperimentFacade()
        {
            return GetDCFInstance.GetExperimentFacade;
        }

        public SiteSecurityFacade GetSiteSecurityFacade()
        {
            return GetDCFInstance.GetSiteSecurityFacade;
        }

        public FameFacade GetFameFacade()
        {
            return GetDCFInstance.GetFameFacade;
        }

        public GameFacade GetGameFacade()
        {
            return GetDCFInstance.GetGameFacade;
        }

        public TransactionFacade GetTransactionFacade()
        {
            return GetDCFInstance.GetTransactionFacade;
        }

        public DevelopmentCompanyFacade GetDevelopmentCompanyFacade()
        {
            return GetDCFInstance.GetDevelopmentCompanyFacade;
        }

        public BlogFacade GetBlogFacade()
        {
            return GetDCFInstance.GetBlogFacade;
        }

        public ShoppingFacade GetShoppingFacade()
        {
            return GetDCFInstance.GetShoppingFacade;
        }

        public CommunityFacade GetCommunityFacade ()
        {
            return GetDCFInstance.GetCommunityFacade;
        }

        public MetricsFacade GetMetricsFacade()
        {
            return GetDCFInstance.GetMetricsFacade;
        }

        public ScriptGameItemFacade GetScriptGameItemFacade()
        {
            return GetDCFInstance.GetScriptGameItemFacade;
        }

        public string GetUserSearchControlID ()
        {
            return SiteManagementCommonFunctions.GetUserSearchControlID;
        }

        public string GetUserSearchParentMenuID()
        {
            return SiteManagementCommonFunctions.GetUserSearchParentMenuID;
        }

        public string GetGameSearchControlID()
        {
            return SiteManagementCommonFunctions.GetGameSearchControlID;
        }

        public string GetZoneSettingsControlID()
        {
            return SiteManagementCommonFunctions.GetZoneSettingsControlID;
        }

        public string GetGameServersControlID()
        {
            return SiteManagementCommonFunctions.GetGameServersControlID;
        }

        public string GetWorldTemplateParentMenuID()
        {
            return SiteManagementCommonFunctions.GetWorldTemplateParentMenuID;
        }

        public string GetWorldTemplatesControlID()
        {
            return SiteManagementCommonFunctions.GetWorldTemplatesControlID;
        }

        public string GetWorldTemplateDefaultItemsControlID()
        {
            return SiteManagementCommonFunctions.GetWorldTemplateDefaultItemsControlID;
        }

        public string GetGameSearchParentMenuID()
        {
            return SiteManagementCommonFunctions.GetGameSearchParentMenuID;
        }

        public string GetGameSearchDefaultPage()
        {
            return SiteManagementCommonFunctions.GetGameSearchDefaultPage;
        }

        public string GetUserSearchDefaultPage()
        {
            return SiteManagementCommonFunctions.GetUserSearchDefaultPage;
        }

        /// <summary>
        /// Get the currently selected sight from the session. If session has failed/expired it defaults to the  site id in the web config
        /// that the calling page indicates
        /// </summary>
        public string GetCurrentSiteID(int searchSiteId)
        {
            string siteId = "0";
            if (Session["SelectedSiteId"] != null)
            {
                siteId = Session["SelectedSiteId"].ToString();
            }
            else
            {
                switch (searchSiteId)
                {
                    case (int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.KANEVA:
                        siteId = SiteManagementCommonFunctions.GetKanevaSiteID;
                        break;
                    case (int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.DEVELOPER:
                        siteId = SiteManagementCommonFunctions.GetDeveloperSiteID; ;
                        break;
                    case (int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.SHOPPING:
                        break;
                    case (int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.DESIGNER:
                        break;
                    case (int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.SITE_MANAGEMENT:
                        break;
                }
                Session["SelectedSiteId"] = siteId; ;
            }
            return siteId;
        }

        /// <summary>
        /// Get the community status
        /// </summary>
        public string GetStatus(object isPublic)
        {
            if (isPublic == null)
            {
                return "";
            }
            return CommunityUtility.GetStatus(isPublic.ToString());
        }

     
        /// <summary>
        /// TruncateWithEllipsis
        /// </summary>
        public string TruncateWithEllipsis(string text, int length)
        {
            return KanevaGlobals.TruncateWithEllipsis(text, length);
        }

        /// <summary>
        /// Get the type of member from a given accountTypeId
        /// </summary>
        /// <param name="accountTypeId"></param>
        /// <returns></returns>
        public string GetMemberTypeDescription(object accountTypeId, object cmStatusId)
        {
            return CommunityUtility.GetMemberTypeDescription(accountTypeId, cmStatusId);
        }

        /// <summary>
        /// GetAssetTypeName
        /// </summary>
        public string GetAssetTypeName(int assetTypeId)
        {
            try
            {
                DataTable dtAssetTypes = WebCache.GetAssetTypes();
                DataRow[] drAssetType = dtAssetTypes.Select("asset_type_id = " + assetTypeId, "");

                if (drAssetType != null && drAssetType.Length > 0)
                {
                    return drAssetType[0]["name"].ToString();
                }
            }
            catch (Exception) { };

            return "Unknown";
        }

        /// <summary>
        /// GetStatusText
        /// </summary>
        protected string GetStatusText(int publishStatus, int statusId, int assetId)
        {
            return StoreUtility.GetStatusText(publishStatus, statusId, assetId);
        }

        /// <summary>
        /// Return the delete javascript
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetDeleteScript(int assetId)
        {
            return "javascript:if (confirm(\"Are you sure you want to delete this asset?\")){DeleteAsset (" + assetId + ")};";
        }

        /// <summary>
        /// Get download Link
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetDownloadLink(int assetId)
        {
            return "http://" + SiteManagementCommonFunctions.GetKanevaURL + "/download.aspx?assetId=" + assetId;
        }

        /// <summary>
        /// Get transfer Link
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetTransferLink(int assetId)
        {
            return "http://" + SiteManagementCommonFunctions.GetKanevaURL + "/itemTransfer.aspx?assetId=" + assetId;
        }

        /// <summary>
        /// Return the broadcast channel link
        /// </summary>
        /// <param name="channelId"></param>
        /// <returns></returns>
        public string GetBroadcastChannelUrl(string nameNoSpaces)
        {
            return "http://" + SiteManagementCommonFunctions.GetKanevaURL + "/channel/" + nameNoSpaces + ".channel";
        }

        /// <summary>
        /// Return the photo image URL
        /// </summary>
        public string GetBroadcastChannelImageURL (string imagePath, string defaultSize)
        {
            return CommunityUtility.GetBroadcastChannelImageURL (imagePath, defaultSize);
        }

        /// <summary>
        /// Return the item photo image 
        /// </summary>
        public string GetItemImageURL (string imagePath, string defaultSize)
        {
            return StoreUtility.GetItemImageURL (imagePath, defaultSize);
        }


        /// <summary>
        /// Return the blog link
        /// </summary>
        /// <param name="blogId"></param>
        /// <returns></returns>
        public string GetBlogDetailsLink(int blogId, int communityId)
        {
            return "http://" + SiteManagementCommonFunctions.GetKanevaURL + "/blog/" + blogId.ToString() + ".blog";
        }

        /// <summary>
        /// Get the status link
        /// </summary>
        /// <param name="torrentStatus"></param>
        /// <returns></returns>
        protected string GetStatusLink(int torrentStatus)
        {
            return "javascript:window.open('http://" + SiteManagementCommonFunctions.GetKanevaURL + "/asset/publishStatus.aspx#" + torrentStatus + "','add','toolbar=no,width=600,height=450,menubar=no,scrollbars=yes,status=yes').focus();return false;";
        }
     
        /// <summary>
        /// GetLoginURL
        /// </summary>
        /// <returns></returns>
        public string GetLoginURL()
        {
            return ResolveUrl("~/login.aspx?logretURL=" + Server.UrlEncode(GetCurrentURL()));
        }

        /// <summary>
        /// GetLoginURL
        /// </summary>
        /// <returns></returns>
        public string GetHomePageURL()
        {
            return ResolveUrl("~/Default.aspx");
        }

        /// <summary>
        /// IsDeleted
        /// </summary>
        protected bool IsDeleted(int statusId)
        {
            return (statusId.Equals((int)Constants.eASSET_STATUS.MARKED_FOR_DELETION));
        }

        /// <summary>
        /// GetLoginURL with args
        /// </summary>
        /// <returns></returns>
        public string GetHomePageURL(string args)
        {
            return ResolveUrl("~/Default.aspx?" + args);
        }

        /// <summary>
        /// Get Asset Details Link
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetAssetDetailsLink(int assetId)
        {
            return "http://" + SiteManagementCommonFunctions.GetKanevaURL + "/asset/" + assetId.ToString() + ".media" ;
        }

        /// <summary>
        /// Return the personal channel link
        /// </summary>
        /// <param name="channelId"></param>
        /// <returns></returns>
        public string GetPersonalChannelUrl(string nameNoSpaces)
        {
            return "http://" + SiteManagementCommonFunctions.GetKanevaURL + "/channel/" + nameNoSpaces + ".people";
        }

        /// <summary>
        /// Get the forum Threads URL
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetThreadURL(int topicId, int threadId)
        {
            if (threadId > 0)
            {
                return "http://" + SiteManagementCommonFunctions.GetKanevaURL + "/forum/forumThreads.aspx?topicId=" + topicId + GetCommunityIdQueryString() + "#" + threadId;
            }
            else
            {
                return "http://" + SiteManagementCommonFunctions.GetKanevaURL + "/forum/forumThreads.aspx?topicId=" + topicId + GetCommunityIdQueryString();
            }
        }

        /// <summary>
        /// If they are in a community, get the communityId string
        /// </summary>
        /// <returns></returns>
        public string GetCommunityIdQueryString()
        {
            string commId = "";
            if (Request["communityId"] != null)
            {
                commId = "&communityId=" + Request["communityId"].ToString();
            }
            return commId;
        }

        /// <summary>
        /// KPointsToDollars
        /// </summary>
        /// <param name="dKPoints"></param>
        /// <returns></returns>
        protected Double ConvertKPointsToDollars(Double dKPoints)
        {
            return StoreUtility.ConvertKPointsToDollars(dKPoints);
        }

        /// <summary>
        /// Get asset edit Link
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetAssetEditLink(int assetId)
        {
            return "http://" + SiteManagementCommonFunctions.GetKanevaURL + "/asset/assetEdit.aspx?assetId=" + assetId;
        }

        /// <summary>
        /// Return the user image URL
        /// </summary>
        public string GetProfileImageURL(string imagePath, string defaultSize, string gender)
        {
            return UsersUtility.GetProfileImageURL(imagePath, defaultSize, gender);
        }

        /// <summary>
        /// GetCurrentURL
        /// </summary>
        /// <returns></returns>
        public string GetCurrentURL()
        {
            return Request.CurrentExecutionFilePath + "?" + Request.QueryString.ToString();
        }

        /// <summary>
        /// Redirect to login page
        /// </summary>
        public void RedirectToLogin()
        {
            Response.Redirect(GetLoginURL());
        }

        /// <summary>
        /// Redirect to home page
        /// </summary>
        public void RedirectToHomePage()
        {
            Response.Redirect(GetHomePageURL());
        }


        #endregion

        // ***********************************************
        // Reporting Services
        // ***********************************************
        #region Reporting Services

        public void LoadRSReport(ReportViewer ReportViewer1, string reportPath)
        {
            LoadRSReport(ReportViewer1, reportPath, null);
        }
        
        public void LoadRSReport(ReportViewer ReportViewer1, string reportPath, ReportParameter[] parameters)
        {
            GetDCFInstance.LoadRSReport(ReportViewer1,reportPath,parameters);
        }

        #endregion

        // ***********************************************
        // Security Functions
        // ***********************************************
        #region Security Functions
        /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public int IsGameMature(int ratingId)
        {
            return GetDCFInstance.IsGameMature(ratingId);
        }

        /// <summary>
        /// ShowCreditCardNumber
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        protected string ShowCreditCardNumber(string number)
        {
            int showLast = 4;

            number = KanevaGlobals.Decrypt(number);

            if (number.Length > showLast)
            {
                return ("***********" + number.Substring(number.Length - showLast));
            }
            else
            {
                return number;
            }
        }

        /// <summary>
        /// Return the current user id
        /// </summary>
        /// <returns></returns>
        public int GetUserId()
        {
            return SiteManagementCommonFunctions.GetUserId();
        }

        public string CleanJavascriptFull(string strText)
        {
            return GetDCFInstance.CleanJavascriptFull(strText);
        }

        #endregion

        /// <summary>
        /// Return the users privilege settings. All privileges available and their corresponding 
        /// access levels.
        /// </summary>
        /// <returns></returns>
        public int CheckUserAccess(int privilegeId)
        {
            int accessLevel = (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE;
            try
            {
                //users privileges
                NameValueCollection privileges = GetCurrentUser().SitePrivileges;

                //find their access level for the privilege being checked
                accessLevel = Convert.ToInt32(privileges[privilegeId.ToString()]);
            }
            catch { }

            return accessLevel;
        }

        /// <summary>
        /// Return a list comprised of just the privileges that the user has rights 
        /// to(read or read/write).
        /// Throws exception to calling function
        /// </summary>
        /// <returns></returns>
        public string GetUsersAllowedPrivileges()
        {
            string allowedPrivileges = "";

            //users privileges
            NameValueCollection privileges = GetCurrentUser().SitePrivileges;

            //loop through the privileges and create the list of denied privileges
            //done to avoid a trip to the database
            int accessLevel = 0;
            foreach (string privilegeId in privileges)
            {
                //get the access level of the privilege
                accessLevel = Convert.ToInt32(privileges[privilegeId]);

                if ((int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE != accessLevel)
                {
                    allowedPrivileges += privilegeId + ",";
                }
            }

            //process the string to remove the trailing comma if any values are there
            if (allowedPrivileges.Length > 0)
            {
                allowedPrivileges = allowedPrivileges.Substring(0, allowedPrivileges.Length - 1);
            }

            return allowedPrivileges;
        }

        public int GetKanevaAdminRoleID()
        {
            return SiteManagementCommonFunctions.GetKanevaAdminRoleID;
        }

        public int GetKanevaCompanyID()
        {
            return SiteManagementCommonFunctions.GetCompanyID;
        }

        //disables security panel for read only mode
        public virtual void ReadOnly(Panel pnl_Security)
        {
            pnl_Security.Enabled = false;
        }


        // ***********************************************
        // Game Functions
        // ***********************************************
        #region Game Functions
        public void UploadGameImage(int gameId, HtmlInputFile browseTHUMB)
        {
            GetDCFInstance.UploadGameImage(GetCommunityFacade().GetCommunityIdFromGameId(gameId), browseTHUMB);
        }

        public void UploadWOKItemImage(int userId, int itemId, HtmlInputFile browseTHUMB)
        {
            GetDCFInstance.UploadWOKItemImage(userId, itemId, browseTHUMB.PostedFile);
        }

        public void UploadAchievementImage (int userId, int achievementId, HtmlInputFile browseTHUMB)
        {
            GetDCFInstance.UploadAchievementImage (userId, achievementId, browseTHUMB.PostedFile);
        }

        /// <summary>
        /// Return the game image URL
        /// </summary>
        public string GetGameImageURL(string imagePath, string defaultSize)
        {
            return GetDCFInstance.GetGameImageURL(imagePath, defaultSize);
        }

        /// <summary>
        /// Return the acheivement image URL
        /// </summary>
        public string GetAchievementImageURL (string imagePath, string defaultSize)
        {
            return GetDCFInstance.GetAchievementImageURL (imagePath, defaultSize);
        }

        /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public string GetGameAccessDescription(int accessId)
        {
            return GetDCFInstance.GetGameAccessDescription(accessId);
        }

        /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public string GetGameRatingDescription(int ratingId)
        {
            return GetDCFInstance.GetGameRatingDescription(ratingId);
        }

        /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public string GetGameStatusDescription(int statusId)
        {
            return GetDCFInstance.GetGameStatusDescription(statusId);
        }

        /// <summary>
        /// checks to see if user is the owner of teh game and thus allowed to edit
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public bool UsersCompanyIsGameOwner(int userId, int gameId)
        {
            return GetDCFInstance.UsersCompanyIsGameOwner(userId, gameId);
        }

        /// <summary>
        /// checks to see if povided company/owner id is the owner of the provided game thus is allowed to edit
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public bool CompanyIsGameOwner(int companyId, int gameId)
        {
            return GetDCFInstance.CompanyIsGameOwner(companyId, gameId);
        }

        /// <summary>
        /// Queries server visibility data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public string GetGameVisibiltyDescription(int visibilityId)
        {
            return GetDCFInstance.GetGameVisibiltyDescription(visibilityId);
        }

        /// <summary>
        /// Queries server status data set to get the description
        /// </summary>
        public string GetServerStatusDescription(int statusId)
        {
            return GetDCFInstance.GetServerStatusDescription(statusId);
        }

        
        #endregion

        // ***********************************************
        // User Functions
        // ***********************************************
        #region User Functions

        public DataRow GetUserByEmail(string email)
        {
            return GetDCFInstance.GetUserByEmail(email);
        }

        public User GetCurrentUser()
        {
            return SiteManagementCommonFunctions.CurrentUser;
        }


        #endregion


        // ***********************************************
        // Data Display Functions
        // ***********************************************

        /// <summary>
        /// Format the date into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public string FormatDate(Object dtDate)
        {
            if (dtDate.Equals(DBNull.Value))
                return "";
            return KanevaGlobals.FormatDate((DateTime)dtDate);
        }

        /// <summary>
        /// Format the date into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public string FormatDateTime(Object dtDate)
        {
            if (dtDate.Equals(DBNull.Value))
                return "";
            return KanevaGlobals.FormatDateTime((DateTime)dtDate);
        }

        /// <summary>
        /// Format K-Points
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public string FormatKpoints(Object amount)
        {
            return FormatKpoints(amount, true);
        }

        /// <summary>
        /// Format K-Points
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public string FormatKpoints(Object amount, bool bShowFreeText)
        {
            if (amount.Equals(DBNull.Value))
                return "K-0";

            return KanevaGlobals.FormatKPoints(Convert.ToDouble(amount), true, bShowFreeText);
        }

        /// <summary>
        /// FormatCurrency
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public string FormatCurrency(Object amount)
        {
            return KanevaGlobals.FormatCurrency(Convert.ToDouble(amount));
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return KanevaGlobals.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return KanevaGlobals.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }


        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
        {
            return KanevaGlobals.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
        {
            return KanevaGlobals.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts);
        }


        #region DataGrid Functions
        /// <summary>
        /// SetDropDownIndex
        /// </summary>
        protected void SetDropDownIndex(DropDownList drp, string theValue)
        {
            try
            {
                drp.SelectedValue = theValue;
            }
            catch (Exception)
            { }
        }

        /// <summary>
        /// SetDropDownIndex
        /// </summary>
        protected void SetDropDownIndex(DropDownList drp, string theValue, bool bAddValue)
        {
            SetDropDownIndex(drp, theValue, bAddValue, theValue);
        }

        /// <summary>
        /// SetDropDownIndex
        /// </summary>
        protected void SetDropDownIndex(DropDownList drp, string theValue, bool bAddValue, string theDisplayName)
        {
            if (bAddValue)
            {
                ListItem li = drp.Items.FindByValue(theValue);
                if (li == null)
                {
                    drp.Items.Insert(drp.Items.Count, new ListItem(theDisplayName, theValue));
                }
            }
            SetDropDownIndex(drp, theValue);
        }

        /// <summary>
        /// Format image size
        /// </summary>
        /// <param name="imageSize"></param>
        /// <returns></returns>
        public string FormatImageSize(Object imageSize)
        {
            if (imageSize.Equals(DBNull.Value))
                return "";
            return KanevaGlobals.FormatImageSize(Convert.ToInt64(imageSize));
        }



        /// <summary>
        /// Current sort expression
        /// </summary>
        public virtual string CurrentSort
        {
            get
            {
                if (ViewState["cs"] == null)
                {
                    return DEFAULT_SORT;
                }
                else
                {
                    return ViewState["cs"].ToString();
                }
            }
            set
            {
                ViewState["cs"] = value;
            }
        }

        /// <summary>
        /// Current sort order
        /// </summary>
        public virtual string CurrentSortOrder
        {
            get
            {
                if (ViewState["cso"] == null)
                {
                    return DEFAULT_SORT_ORDER;
                }
                else
                {
                    return ViewState["cso"].ToString();
                }
            }
            set
            {
                ViewState["cso"] = value;
            }
        }

        /// <summary>
        /// They clicked to sort a column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void btnSort_Click(object sender, System.EventArgs e)
        {
            // Set the sort order
            if (CurrentSortOrder == "DESC")
            {
                CurrentSortOrder = "ASC";
            }
            else if (CurrentSortOrder == "ASC")
            {
                CurrentSortOrder = "DESC";
            }
            else
            {
                CurrentSortOrder = "ASC";
            }

            // Set the sort column
            if (!CurrentSort.Equals(hidSortColumn.Value))
            {
                // Changing sort expression, so set to ASC
                CurrentSortOrder = "ASC";
                CurrentSort = hidSortColumn.Value;
            }
        }

        /// <summary>
        /// Set Header Sort column Text
        /// </summary>
        protected void SetHeaderSortText(DataGrid dgrdToSort)
        {
            DataGridColumn dgrdColumn;
            string strippedHeader;

            // Which arrow to use?
            string arrowImage = "arrow_sort_up.gif";
            if (CurrentSortOrder.Equals("DESC"))
            {
                arrowImage = "arrow_sort.gif";
            }

            // Loop through all sortable columns
            for (int i = 0; i < dgrdToSort.Columns.Count; i++)
            {
                dgrdColumn = dgrdToSort.Columns[i];

                // Is it a sortable column?
                if (dgrdColumn.SortExpression.Length > 0 && dgrdToSort.AllowSorting)
                {
                    strippedHeader = dgrdColumn.HeaderText;

                    if (dgrdToSort.EnableViewState)
                    {
                        strippedHeader = strippedHeader.Replace("<img src=" + ResolveUrl("~/images/arrow_sort.gif") + " border=0/>", "");
                        strippedHeader = strippedHeader.Replace("<img src=" + ResolveUrl("~/images/arrow_sort_up.gif") + " border=0/>", "");
                    }

                    // Is this column the current sorted?
                    if (CurrentSort.Equals(dgrdColumn.SortExpression))
                    {
                        dgrdColumn.HeaderText = "<a href='#' onclick='javascript:document.frmMain." + hidSortColumn.ClientID + ".value=\"" + dgrdColumn.SortExpression + "\";javascript:document.frmMain." + btnSort.ClientID + ".click();'>" + strippedHeader + "<img src=" + ResolveUrl("~/images/" + arrowImage) + " border=0/></a>";
                    }
                    else
                    {
                        dgrdColumn.HeaderText = "<a href='#' onclick='javascript:document.frmMain." + hidSortColumn.ClientID + ".value=\"" + dgrdColumn.SortExpression + "\";javascript:document.frmMain." + btnSort.ClientID + ".click();'>" + strippedHeader + "</a>";
                    }
                }
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected virtual string DEFAULT_SORT
        {
            get
            {
                return "a.name";
            }
        }

        /// <summary>
        /// DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        protected virtual string DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }
        #endregion

        //overridable sorting event for gridview
        protected virtual void gridview_Sorting(Object sender, GridViewSortEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
        }

        //allows user to switch the ordering between DESC and ASC
        //based on column clicked
        protected void SortSwitch(string newSort)
        {
            if (CurrentSort == newSort)
            {
                if (CurrentSortOrder == "DESC")
                {
                    CurrentSortOrder = "ASC";
                }
                else
                {
                    CurrentSortOrder = "DESC";
                }
            }
            else
            {
                CurrentSort = newSort;
                CurrentSortOrder = "DESC";
            }
        }
        // ***********************************************
        // Error Message Display Functions
        // ***********************************************

        #region Error Message Functions

        /// <summary>
        /// Show an error message on startup
        /// </summary>
        /// <param name="errorMessage"></param>
        protected void ShowErrorOnStartup(string errorMessage)
        {
            ShowErrorOnStartup(errorMessage, true);
        }

        /// <summary>
        /// Show an error message on startup
        /// </summary>
        /// <param name="errorMessage"></param>
        protected void ShowErrorOnStartup(string errorMessage, bool bShowPleaseMessage)
        {
            string scriptString = "<script language=JavaScript>";
            if (bShowPleaseMessage)
            {
                scriptString += "alert ('Please correct the following errors:\\n\\n" + errorMessage + "');";
            }
            else
            {
                scriptString += "alert ('" + errorMessage + "');";
            }
            scriptString += "</script>";

            //if (!ClientScript.IsClientScriptBlockRegistered(this.GetType(), "ShowError"))
            //{
            //    ClientScript.RegisterStartupScript(this.GetType(), "ShowError", scriptString);
            //}
        }

        #endregion

        public void ConfigureCommunityMeetMe3D(global::System.Web.UI.WebControls.LinkButton lnkButton, int gameId, int communityId, string communityName, string requestTrackingGUID, int currentUserId)
        {
            lnkButton.Attributes.Add ("onclick", StpUrl.GetPluginJS (Request.IsAuthenticated, true, gameId, communityId, communityName, requestTrackingGUID, currentUserId));
        }

        public void ConfigureCommunityMeetMe3D(global::System.Web.UI.HtmlControls.HtmlAnchor aMeetMeLink, int gameId, int communityId, string communityName, string requestTrackingGUID, int currentUserId)
        {
            aMeetMeLink.Attributes.Add ("onclick", StpUrl.GetPluginJS (Request.IsAuthenticated, true, gameId, communityId, communityName, requestTrackingGUID, currentUserId));
        }

     
    }


}
