<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SideNav.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.SideNav" %>
<div id="smenu_container">
    <asp:DataList ID="dl_SubMenu" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
        <ItemTemplate>
             <asp:LinkButton ID="lb_menuItem" runat="server" OnCommand="Nav_Command" CausesValidation="false" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserControlId") %>' ><%# DataBinder.Eval(Container.DataItem, "NavigationTitle")%></asp:LinkButton>
        </ItemTemplate>
    </asp:DataList>		

</div> 