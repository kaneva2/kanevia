<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="UserTransactions.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.UserTransactions" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    <table id="tblUserTransactions" border="0" cellpadding="0" cellspacing="0" width="980">
        <tr>
            <td style="padding-left:40px; text-align:center; font-size:22px" colspan="2">
                USER TRANSACTIONS
            </td>
        </tr>
         <tr style="padding-bottom:10px; padding-top:20px">
	        <td valign="top" align="left" style="padding-left:40px; width:150px">Current Balance: </td>
	        <td valign="top" align="left" style="width:80%">
		        <span style="font-weight:bold;" >Rewards:</span><asp:Label ID="lblBalanceRewards" runat="server"></asp:Label> 
		        <span style="font-weight:bold; padding-left:20px">Credits:</span><asp:label ID="lblBalanceCredits" runat="server"></asp:label>        
	        </td>    
		</tr>
        <tr>
            <td style="padding-left:40px" colspan="2">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
	                <tr style="line-height:15px;">
	                    <td align="left" style="width:300px" class="belowFilter2" valign="middle" >
	                        <asp:Label runat="server" id="lblSelectedUser" />
	                    </td>
		                <td align="right" class="belowFilter2" valign="middle">
			                <asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop" /><br>
		                </td>						
	                </tr>
                </table>
             </td>
         </tr>
         <tr>
            <td style="padding-left:40px; padding-right:20px" colspan="2">
                <asp:LinkButton ID="lbn_showAddCredits" runat="server" CommandArgument="1" OnCommand="ConfigureAddDisplay_Command">Add Credits</asp:LinkButton>
                <asp:LinkButton ID="lbn_CancelAdd" runat="server" CommandArgument="0" Visible="false" OnCommand="ConfigureAddDisplay_Command">Cancel Add</asp:LinkButton>
            </td>
         </tr>

         <tr>
            <td style="padding-left:40px" colspan="2">
                <asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" OnSortCommand="UserInventory_Sorting" Width="100%" id="dgrdInventory" cellpadding="2" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 1px solid #cccccc; font-family:arial">  
	                <HeaderStyle BackColor="#cccccc" ForeColor="#000000" Font-Underline="false" Font-Bold="true" HorizontalAlign="Left" />
	                <ItemStyle BackColor="#ffffff" HorizontalAlign="Left" />
	                <AlternatingItemStyle BackColor="#eeeeee" HorizontalAlign="Left" />
	                <Columns>				
		                <asp:TemplateColumn HeaderText="Trans Id" SortExpression="transaction_id" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "transaction_id").ToString ())%> 
			                </ItemTemplate>
		                </asp:TemplateColumn>		
		                <asp:TemplateColumn HeaderText="Table" SortExpression="tablename" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "tablename").ToString ())%> 
			                </ItemTemplate>
		                </asp:TemplateColumn>	
		                <asp:TemplateColumn Visible="false" HeaderText="item name" SortExpression="name" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%> 
			                </ItemTemplate>
		                </asp:TemplateColumn>
                		

		                <asp:TemplateColumn HeaderText="Date" SortExpression="created_date" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "created_date").ToString())%> 
			                </ItemTemplate>
		                </asp:TemplateColumn>
                		
		                <asp:TemplateColumn HeaderText="Currency" SortExpression="currency_type" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "currency_type").ToString())%> 
			                </ItemTemplate>
		                </asp:TemplateColumn>
                				
		                <asp:TemplateColumn HeaderText="Amount" SortExpression="transaction_amount" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "transaction_amount").ToString())%> 
			                </ItemTemplate>
		                </asp:TemplateColumn>
                		
		                <asp:TemplateColumn HeaderText="Balance" SortExpression="balance" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# DataBinder.Eval(Container.DataItem, "balance")%> 
			                </ItemTemplate>
		                </asp:TemplateColumn>	
                		
                        <asp:TemplateColumn HeaderText="Qty." SortExpression="quantity" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# DataBinder.Eval(Container.DataItem, "quantity").ToString()%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
                			
                        <asp:TemplateColumn HeaderText="New Qty." SortExpression="qty_new" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# DataBinder.Eval(Container.DataItem, "qty_new").ToString()%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
                		
		                <asp:TemplateColumn HeaderText="Description" SortExpression="trans_desc" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "trans_desc").ToString())%> 
			                </ItemTemplate>
		                </asp:TemplateColumn>
                		
                	
                		
		                <asp:TemplateColumn HeaderText="Item" SortExpression="item_name" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "item_name").ToString ())%> 
			                </ItemTemplate>
		                </asp:TemplateColumn>								

		                <asp:TemplateColumn HeaderText="GLID" SortExpression="glid" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "glid").ToString ())%> 
			                </ItemTemplate>
		                </asp:TemplateColumn>	
		            </Columns>		
                </asp:datagrid>
            </td>
         </tr>
         <tr id="tr_AddCredits" runat="server" visible="false">
            <td style="padding-left:40px; padding-top:40px" colspan="2">
                <table border="0" cellpadding="0" cellspacing="0">
			        <tr>
				        <td valign="middle" align="right" width="150" style="padding-right:12px;">Transaction Type: </td>
				        <td align="left">
        																			
					        <table cellpadding="0" cellspacing="0" border="0">
						        <tr>
							        <td style="width:200px;"><asp:DropDownList class="formKanevaText" id="drpTransactionTypes" onselectedindexchanged="dropTransactionTypes_OnChange" autopostback="true" style="width:180px" runat="server"></asp:DropDownList></td>
							        <td style="width:350px;text-align:right;" valign="middle">
								        <span id="spnEventName" runat="server" style="width:135px;text;text-align:right;padding-right:10px;">Survey Name:</span><asp:DropDownList class="formKanevaText" id="drpEventName" style="width:200px;" runat="server"></asp:DropDownList>
							        </td>
						        </tr>
					        </table>
        																											   
				        </td>
			        </tr>
			        <tr>
				        <td valign="middle" align="right" style="padding-right:12px;">Currency: </td>
				        <td align="left">
					        <asp:DropDownList class="formKanevaText" id="drpCurrency" style="width:180px" runat="server">
						        <asp:ListItem Text="GPOINT (Rewards)" Value="GPOINT"></asp:ListItem>
						        <asp:ListItem Text="KPOINT (Credits)" Value="KPOINT"></asp:ListItem>
					        </asp:DropDownList>
				        </td>
			        </tr>
			        <tr>
				        <td valign="middle" align="right" style="padding-right:12px;">Amount: </td>
				        <td align="left">
					        <asp:textbox id="txtAmount" runat="server" style="width:180px" maxlength="5"></asp:textbox>	        
				        </td>
			        </tr>
			        <tr>
				        <td valign="middle" align="right"></td>
				        <td align="left">
					        <asp:radiobutton ID="rbCredit" runat="server" GroupName="operationType" Text="credit" Checked="true" ></asp:radiobutton>
					        <asp:radiobutton ID="rbDebit" runat="server" GroupName="operationType" Text="debit" ></asp:radiobutton>                            
				        </td>
			        </tr>
			        <tr>
				        <td valign="top" align="right" style="padding-right:12px;">Note: </td>
				        <td align="left">
					        <asp:textbox id="txtNote" runat="server" TextMode="MultiLine" Rows="3" Columns="60"></asp:textbox><br />
					        (Note will be prepeneded with the credit type and ammount.)
				        </td>
			        </tr>		
			        <tr>
			        <td align="right"></td>
			        <td valign="middle"><br />
				        <asp:button id="btnSave" runat="Server" onClick="btnSave_Click" CausesValidation="False" Text="Complete Transaction"/> 
			        </td>
			        </tr>	
                </table>
            </td>
         </tr>
         <tr>
            <td style="padding-left:40px; padding-top:40px" colspan="2">	                        
	            <div class="errBox black" style="position:relative;display:none;width:500px;margin-top:20px;" runat="server" id="divMessages"></div>

	            <br/>
            </td>
         </tr>
    </table>
</asp:Panel>
