<%@ Control Language="C#" AutoEventWireup="true" Codebehind="DefaultScriptGameItems.ascx.cs" Inherits="SiteManagement.usercontrols.DefaultScriptGameItems" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>

<script type="text/javascript" language="javascript" src="../../jscript/prototype.js"></script>

<asp:LinkButton ID="lbListGameItems" runat="server" Text="List GameItems" OnClick="btnListGameItems_Click"></asp:LinkButton> | 
<asp:LinkButton ID="lbNewTemplate" runat="server" Text="New" OnClick="btnNewGameItem_Click"></asp:LinkButton>
<br />
<br />
<div id="divErrorData" runat="server" class="error"></div>
<div id="dvGameItemEdit" runat="server">
    <table>
		<tr>
			<td>
				<table>
                    <tr id="trSuggestedGIGLID" runat="server" Visible="false">
                        <td><asp:Literal id="litSuggestedGIGLID" runat="server" /></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr id="trSuggestedGIID" runat="server" Visible="false">
                        <td><asp:Literal id="litSuggestedGIID" runat="server" /></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr id="trSuggestedBundleGlid" runat="server" Visible="false">
                        <td><asp:Literal id="litSuggestedBundleGlid" runat="server" /></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">Game Item Json:<asp:RequiredFieldValidator ID="rfGameItemJSON" ControlToValidate="txtGameItemJSON" Text="*" ErrorMessage="Game Item JSON is a required field." Display="Dynamic" runat="server"/></td>
                        <td>
                            <asp:TextBox ID="txtGameItemJSON" runat="server" Columns="100" Rows="20" TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
						<td style="vertical-align:top;">Automatic Inventory Insert:</td>
						<td>
							<asp:DropDownList ID="ddlAutomaticInsertion" runat="server" onchange="toggleAutoInsertionFields(this)">
                                <asp:ListItem Value="True" Text="True" />
								<asp:ListItem Value="False" Text="False" />
                            </asp:DropDownList>
                            <br />* Note: This adds/removes the item to/from all gaming templates. If True, it also adds item to the inventories of users who have not <br />
                            already received it, starting from the date below (or current date, if unspecified). It does NOT modify existing Worlds.
						</td>
						<td>
						</td>
					</tr>
                    <tr>
                        <td style="vertical-align:top">Insert Starting From<br />(Optional):</td>
                        <td>
                            <asp:TextBox ID="txtAutomaticInsertionStart" runat="server" Columns="20" type="date"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">Insert Sort Order<br />(Optional):</td>
                        <td>
                            <asp:TextBox ID="txtAutomaticInsertionOrder" runat="server" Columns="20"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
					<tr>
						<td>
						</td>
						<td>
							<asp:Button ID="btnSaveGameItem" Text="Save" OnClick="btnSaveGameItem_Click" runat="server" />
							<asp:Button ID="btnCancelEdit" Text="Cancel" OnClick="btnCancelEdit_Click" runat="server" />
						</td>
						<td>
						</td>
					</tr>
					<tr>
						<td>
						</td>
						<td>
							<asp:Label ID="txtSaveStatus" runat="server"></asp:Label></td>
						<td>
						</td>
					</tr>
				</table>
			</td>
			<td>
				<!-- Help Text -->
			</td>
		</tr>
	</table>
</div>
<div id="dvGameItems" runat="server">
	<asp:Label ID="txtGVStatus" runat="server"></asp:Label>
	<asp:GridView ID="gvGameItems" runat="server" OnRowEditing="gvGameItems_Edit" AutoGenerateColumns="false">
		<RowStyle BackColor="White"></RowStyle>
		<HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000"
			HorizontalAlign="Left"></HeaderStyle>
		<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
		<FooterStyle BackColor="#FFffff"></FooterStyle>
		<Columns>
			<asp:TemplateField>
				<ItemTemplate>
					&nbsp;<asp:LinkButton ID="lbnEdit" runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="GIGlid" HeaderText="GIGLID" />
            <asp:BoundField DataField="GIId" HeaderText="GIID" />
			<asp:BoundField DataField="Name" HeaderText="Name" />
            <asp:BoundField DataField="ItemType" HeaderText="Item Type" />
            <asp:BoundField DataField="InsertAutomatically" HeaderText="Insert Automatically" />
            <asp:BoundField DataField="AutomaticInsertionStart" HeaderText="Automatic Insertion Start" />
            <asp:BoundField DataField="AutomaticInsertionOrder" HeaderText="Automatic Insertion Order" />
		</Columns>
	</asp:GridView>
    <Kaneva:Pager runat="server" id="pgBottom" OnPageChanged="pg_PageChanged"/>
</div>