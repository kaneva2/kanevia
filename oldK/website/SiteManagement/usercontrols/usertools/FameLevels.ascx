<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="FameLevels.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.FameLevels" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    <ajax:ajaxpanel id="ajMessage" runat="server">
         <table id="tblFameLevels" border="0" cellpadding="0" cellspacing="0" width="980px" style="background-color:#ffffff">
            <tr>
                <td align="center">
			        <span style="height:30px; font-size:28px; font-weight:bold">World Fame Levels</span><br />
			        <asp:label runat="server" id="lblErrMessage" class="errBox black" style="width:60%;margin-top:20px;text-align:center;"></asp:label>
		        </td>
            </tr>
	        <tr>
		        <td style="height:20px"></td>
            </tr>
            <tr>
                 <td>
			        <asp:Label id="lblNoFameLevels" visible="false" runat="server">
				        <span style="color:Red; size:24pt; font-weight:bold">No Levels Found.</span>
			        </asp:Label>
		        </td>
            </tr>
            <tr>
                 <td align="center">
         
 			        <ajax:ajaxpanel id="ajpFameLevels" runat="server">  
                  
				        <asp:gridview id="dgFameLevels" runat="server" 
					        onsorting="dgFameLevels_Sorting" 
					        autogeneratecolumns="False" 
					        width="65%" 
					        AllowSorting="True" border="0" cellpadding="2" 
					        AllowPaging="False" Align="center" 
					        bordercolor="DarkGray" borderstyle="solid" borderwidth="1px">
        					
					        <HeaderStyle BackColor="LightGray" horizontalalign="Center" Font-Underline="True" Font-Bold="True" ForeColor="#000000"></HeaderStyle>
					        <RowStyle BackColor="White" horizontalalign="center"></RowStyle>
					        <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
					        <FooterStyle BackColor="#FFffff"></FooterStyle>
					        <Columns>
						        <asp:BoundField HeaderText="Level Id"  DataField="LevelId" ReadOnly="True" ConvertEmptyStringToNull="False" visible="false" ></asp:BoundField>
						        <asp:BoundField HeaderText="Level Number" DataField="LevelNumber" SortExpression="level_number" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
						        <asp:BoundField HeaderText="Start Points" DataField="StartPoints" SortExpression="start_points" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
						        <asp:BoundField HeaderText="End Points" DataField="EndPoints" SortExpression="end_points" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
						        <asp:BoundField HeaderText="Rewards" DataField="Rewards" SortExpression="rewards" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
					        </Columns>
				        </asp:gridview>

				        <div style="width:65%;text-align:right;margin:10px 6px 0 0;"><kaneva:pager runat="server" isajaxmode="True" onpagechanged="pgLevels_PageChange" id="pgLevels" maxpagestodisplay="5" shownextprevlabels="true" /></div>
        			
			        </ajax:ajaxpanel> 
                            
                 </td>
            </tr>
        </table>
    </ajax:ajaxpanel>
</asp:Panel>        