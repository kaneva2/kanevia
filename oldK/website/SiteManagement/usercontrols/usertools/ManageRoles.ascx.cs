using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;

using log4net;
using MagicAjax;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;
using System.Text;
using Kaneva;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class ManageRoles : BaseUserControl
    {
        #region Declarations

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private int selectedSiteRoleId = 0;

        #endregion
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.SITE_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                if (!IsPostBack)
                {
                    //get all Site roles and store for later binding
                    GetSiteRoles();

                    //get access levels for privileges
                    GetPrivilegeAccessLevels();

                    //gets Available user privileges
                    GetAvailablePrivileges();

                    //dont bing if there are no editable roles
                    if (SITE_ROLES.Count > 0)
                    {
                        //bind datagrids
                        BindPrivilegeData();
                    }
                    else
                    {
                        ibn_CommitChanges.Enabled = false;
                    }

                }

                //clear message
                messages.Text = "";
            }
            else
            {
                RedirectToHomePage();
            }
        }

        #region Main Functionality

        private void CommitPrivilegeChanges()
        {
            try
            {
                //get the need parameters
                int companyId = GetCurrentUser().SM_CompanyID;
                selectedSiteRoleId = Convert.ToInt32(ddl_SiteRoles.SelectedValue);

                //delete all existing privilege to role relationships
                GetSiteSecurityFacade().DeleteAllPrivilege2Role(selectedSiteRoleId, companyId);


                //loop through each  privilege and save to database
                foreach (RepeaterItem rptPrivilege in rpt_Privileges.Items)
                {
                    DropDownList ddlRights = (DropDownList)rptPrivilege.FindControl("ddl_Rights");
                    HtmlInputHidden hidPrivilegeId = (HtmlInputHidden)rptPrivilege.FindControl("hidPrivilegeId");

                    int accessLevelId = Convert.ToInt32(ddlRights.SelectedValue);
                    int privilegeId = Convert.ToInt32(hidPrivilegeId.Value);

                    GetSiteSecurityFacade().AddNewPrivilege2Role(selectedSiteRoleId, companyId, privilegeId, accessLevelId);
                }

                //give success message
                messages.ForeColor = System.Drawing.Color.Green;
                messages.Text = "Your privilege settings have been saved.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");

            }
            catch (Exception ex)
            {
                m_logger.Error("Error new role creation.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error: Sorry we were unable to create your new role.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

        }

        private bool SaveRoleChanges(int siteRoleId)
        {
            int roleId = 0;

            //create a new user role
            SiteRole role = new SiteRole();

            //populate the data for updating/adding
            role.CompanyId = GetCurrentUser().SM_CompanyID;
            role.RolesName = tbx_RoleName.Text;
            role.RoleId = Convert.ToInt32(tbx_KanevaRoleId.Text);
            role.SiteRoleId = Convert.ToInt32(hidSiteRoleId.Value);

            //check to see if the kaneva role id is already taken
            //kaneva website requires that this value be unique
            try
            {
                SiteRole checkRole = GetSiteSecurityFacade().GetCompanyRole(role.CompanyId, role.RoleId);
                if ((checkRole.RoleId == role.RoleId) && (checkRole.SiteRoleId != role.SiteRoleId))
                {
                    messages.ForeColor = System.Drawing.Color.Crimson;
                    messages.Text = "Another role with the rold id of " + role.RoleId + " already exist.";
                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");

                    return true; //this prevents the Parent error messages from overwriting this one
                }
            }
            catch (Exception) { }

            //select operation
            if (siteRoleId > 0)
            {
                //save to Database
                roleId = GetSiteSecurityFacade().UpdateUserRole(role);
            }
            else
            {
                //save to Database
                roleId = GetSiteSecurityFacade().AddUserRole(role);
            }

            //rebind the roles 
            GetSiteRoles();

            //set the new role as the active one
            ddl_SiteRoles.SelectedValue = roleId.ToString();

            //repull the privileges
            BindPrivilegeData();

            //reactivate incase it was deactivated on due t lack of editable roles
            ibn_CommitChanges.Enabled = true;

            //reset the edit area
            ResetEditRoleArea();

            return (roleId > 0);
        }

        private void BindPrivilegeData()
        {
            try
            {
                //gets currently selected Site role
                selectedSiteRoleId = Convert.ToInt32(ddl_SiteRoles.SelectedValue);

                //retrieves the privileges assigned to that role.
                ROLES_2_PRIVELEGES = this.GetSiteSecurityFacade().GetRolePrivileges(selectedSiteRoleId, GetCurrentUser().SM_CompanyID);

                //bind available privileges to repeater
                rpt_Privileges.DataSource = AVAIABLE_PRIVILEGES;
                rpt_Privileges.DataBind();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during role privileges load.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error: Sorry we were unable to load your role's privileges.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        private void GetPrivilegeAccessLevels()
        {
            try
            {
                ACCESS_LEVELS = WebCache.GetAccessLevels();
            }
            catch (Exception ex)
            {
                m_logger.Error("MyTeam error during retrieval of access levels.", ex);
            }
        }

        private void GetAvailablePrivileges()
        {
            try
            {
                //get the available privileges
                AVAIABLE_PRIVILEGES = (List<SitePrivilege>)WebCache.GetAllPrivileges();
            }
            catch (Exception ex)
            {
                m_logger.Error("error during retrieval of privileges", ex);
            }
        }

        private void GetSiteRoles()
        {
            try
            {
                //get the current site roles
                SITE_ROLES = (List<SiteRole>)GetSiteSecurityFacade().GetCompanyRolesList(GetCurrentUser().SM_CompanyID);

                //bind roles to pull down
                ddl_SiteRoles.DataSource = SITE_ROLES;
                ddl_SiteRoles.DataTextField = SiteRole.ROLE_NAME;
                ddl_SiteRoles.DataValueField = SiteRole.SITE_ROLE_ID;
                ddl_SiteRoles.DataBind();

                //bind the roles to the repeater
                //bind available privileges to repeater
                rpt_Roles.DataSource = SITE_ROLES;
                rpt_Roles.DataBind();

            }
            catch (Exception ex)
            {
                m_logger.Error("MyTeam error during retrieval of Site roles.", ex);
            }
        }

        #endregion

        #region Attributes

        //protected IList<SiteRole> SITE_ROLES
        protected List<SiteRole> SITE_ROLES
        {
            get
            {

                if (ViewState["SiteRoles"] == null)
                {
                    ViewState["SiteRoles"] = new List<SiteRole>();
                }

                //return (IList<SiteRole>)ViewState["SiteRoles"];
                return (List<SiteRole>)ViewState["SiteRoles"];
            }
            set
            {
                ViewState["SiteRoles"] = value;
            }
        }

        protected DataTable ROLES_2_PRIVELEGES
        {
            get
            {

                if (ViewState["roles2Privileges"] == null)
                {
                    ViewState["roles2Privileges"] = new DataTable();
                }

                return (DataTable)ViewState["roles2Privileges"];
            }
            set
            {
                ViewState["roles2Privileges"] = value;
            }
        }

        protected DataTable ACCESS_LEVELS
        {
            get
            {

                if (ViewState["accessLevels"] == null)
                {
                    ViewState["accessLevels"] = WebCache.GetAccessLevels();
                }

                return (DataTable)ViewState["accessLevels"];
            }
            set
            {
                ViewState["accessLevels"] = value;
            }
        }

        // protected IList<SitePrivilege> AVAIABLE_PRIVILEGES
        protected List<SitePrivilege> AVAIABLE_PRIVILEGES
        {
            get
            {

                if (ViewState["availablePrivileges"] == null)
                {
                    ViewState["availablePrivileges"] = new List<SitePrivilege>();
                }

                //return (IList<SitePrivilege>)ViewState["availablePrivileges"];
                return (List<SitePrivilege>)ViewState["availablePrivileges"];
            }
            set
            {
                ViewState["availablePrivileges"] = value;
            }
        }

        #endregion

        #region Helper Functions

        //delegate function used in LIst FindAll search
        private bool GetRole(SiteRole role)
        {
            return role.SiteRoleId == selectedSiteRoleId;
        }

        private void ResetEditRoleArea()
        {
            //hide the row
            tr_EditRole.Visible = false;

            //clear the fields
            tbx_KanevaRoleId.Text = "";
            tbx_RoleName.Text = "";
            hidSiteRoleId.Value = "";

        }
        #endregion

        #region Events

        protected void lbn_NewRole_Click(object sender, EventArgs e)
        {
            //clear the fields
            ResetEditRoleArea();

            //show the role area
            tr_EditRole.Visible = true;

            //set id to -1 <indicator that it is a new entry>
            hidSiteRoleId.Value = "-1";

        }

        protected void lbn_EditRole_Click(object sender, EventArgs e)
        {
            try
            {
                //first get a reference to the repeater item the drop down is in
                RepeaterItem item = (RepeaterItem)((LinkButton)sender).Parent;

                //next get a reference to all the items you can change
                Label lblRoleName = (Label)item.FindControl("lbl_RoleName");
                Label lblKanevaRoleId = (Label)item.FindControl("lbl_KanevaRoleId");
                Label lblSiteRoleId = (Label)item.FindControl("lbl_SiteRoleId");

                //populate data into editing fields
                tbx_KanevaRoleId.Text = lblKanevaRoleId.Text;
                tbx_RoleName.Text = lblRoleName.Text;
                hidSiteRoleId.Value = lblSiteRoleId.Text;

                //enable editing fields
                tr_EditRole.Visible = true;
            }
            catch (Exception)
            {
            }
        }

        protected void lbn_DeleteRole_Click(object sender, System.EventArgs e)
        {
            try
            {
                //first get a reference to the repeater item the drop down is in
                RepeaterItem item = (RepeaterItem)((LinkButton)sender).Parent;

                //gets currently selected Site role
                Label lblSiteRoleId = (Label)item.FindControl("lbl_SiteRoleId");
                Label lblRoleName = (Label)item.FindControl("lbl_RoleName");

                selectedSiteRoleId = Convert.ToInt32(lblSiteRoleId.Text);
                string roleName = lblRoleName.Text;

                //check to see if it is one of the default roles if it is dont allow deletion
                //currently they are designated by a role_id (not site_role_id)  greater than 0
                //this may change after open beta release
                SiteRole roleToDelete = SITE_ROLES.Find(GetRole);

                //remove the user role
                int result = GetSiteSecurityFacade().RemoveUserRole(selectedSiteRoleId, GetCurrentUser().SM_CompanyID);

                //check for and communicate if delete failed
                if (result < 1)
                {
                    throw new Exception("Role deletion failed!");
                }

                //rebind the pulldown 
                GetSiteRoles();

                //dont bing if there are no editable roles
                if (SITE_ROLES.Count > 0)
                {
                    //bind datagrids
                    BindPrivilegeData();
                    ibn_CommitChanges.Enabled = true;
                }
                else
                {
                    ibn_CommitChanges.Enabled = false;
                    this.rpt_Privileges.DataSource = new List<SitePrivilege>();
                    this.rpt_Privileges.DataBind();
                }

                messages.ForeColor = System.Drawing.Color.Green;
                messages.Text = "The role " + roleName + " has been removed.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");

            }
            catch (Exception ex)
            {
                m_logger.Error("Error during role deletion", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Editing Error: Sorry we were unable to delete the selected role.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

        }

        protected void ibn_CommitChanges_Click(object sender, System.EventArgs e)
        {
            CommitPrivilegeChanges();
        }

        protected void SaveRoleChanges_Click(object sender, System.EventArgs e)
        {
            try
            {
                //is it updating or adding the role
                int siteRoleId = Convert.ToInt32(hidSiteRoleId.Value);

                //add the new role
                if (!SaveRoleChanges(siteRoleId))
                {
                    messages.ForeColor = System.Drawing.Color.Crimson;
                    messages.Text = "Error: Sorry we were unable to save the changes to your role.";
                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
                }
            }
            catch (Exception ex)
            {
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error:" + ex.Message;
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        protected void btn_CancelRoleChange_Click(object sender, System.EventArgs e)
        {
            ResetEditRoleArea();        
        }

        
        protected void SiteRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindPrivilegeData();
        }

        private void rpt_Privileges_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //find all the controls in the repeater
                Label lblPrivilege = (Label)e.Item.FindControl("lbl_Privilege");
                Label lblDescription = (Label)e.Item.FindControl("lbl_Description");
                DropDownList ddlRights = (DropDownList)e.Item.FindControl("ddl_Rights");
                HtmlInputHidden hidPrivilegeId = (HtmlInputHidden)e.Item.FindControl("hidPrivilegeId");

                //set the pull down data
                ddlRights.DataSource = ACCESS_LEVELS;
                ddlRights.DataTextField = "access_level";
                ddlRights.DataValueField = "access_level_id";
                ddlRights.DataBind();

                //get the privilege's access level for this role
                int privilegeId = ((SitePrivilege)e.Item.DataItem).PrivilegeId;
                DataRow[] results = ROLES_2_PRIVELEGES.Select("privilege_id = " + privilegeId);

                string accessLevelId = GetSiteSecurityFacade().GetDefaultAccessLevel().ToString();
                //check first to see if anything was returned
                if (results.Length > 0)
                {
                    accessLevelId = results[0]["access_level_id"].ToString();
                }

                //set the data for each field
                lblDescription.Text = ((SitePrivilege)e.Item.DataItem).PrivilegeDescription;
                hidPrivilegeId.Value = privilegeId.ToString();
                lblPrivilege.Text = ((SitePrivilege)e.Item.DataItem).PrivilegeName;
                ddlRights.SelectedValue = accessLevelId;
            }
        }

        private void rpt_Roles_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //find all the controls in the repeater
                Label lblSiteRoleId = (Label)e.Item.FindControl("lbl_SiteRoleId");
                Label lblRoleName = (Label)e.Item.FindControl("lbl_RoleName");
                Label lblKanevaRoleId = (Label)e.Item.FindControl("lbl_KanevaRoleId");
                LinkButton lbnDelete = (LinkButton)e.Item.FindControl("lbn_delete");
                //HtmlInputHidden hidPrivilegeId = (HtmlInputHidden)e.Item.FindControl("hidPrivilegeId");

                //set alert box to delete button
                StringBuilder sb = new StringBuilder();
                sb.Append(" javascript: ");
                sb.Append(" if(!confirm('Are you sure you want to delete the selected site role \\n This cannot be undone.')) return false;");
                lbnDelete.Attributes.Add("onClick", sb.ToString());

                //set the data for each field
                lblSiteRoleId.Text = ((SiteRole)e.Item.DataItem).SiteRoleId.ToString();
                lblRoleName.Text = ((SiteRole)e.Item.DataItem).RolesName.ToString();
                lblKanevaRoleId.Text = ((SiteRole)e.Item.DataItem).RoleId.ToString();
            }
        }

        #endregion


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.rpt_Privileges.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_Privileges_ItemDataBound);
            this.rpt_Roles.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_Roles_ItemDataBound);
        }
        #endregion
    }
}
