using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;
using System.Text;
using MagicAjax;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class GameSearch : BaseUserControl
    {
        #region Declarations
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private int gameDeveloperId = 0;
        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.STAR_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        gameDeveloperId = -1; //indicates user is an admin and allowed to see all games
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    default:
                        gameDeveloperId = GetUserId();
                        break;
                }
            }
            else
            {
                RedirectToHomePage();
            }
        }

        #endregion

        #region Functions

        /// <summary>
        ///  Bind the data
        /// </summary>
        /// <param name="pageNumber"></param>
        private void BindData(int pageNumber, string filter)
        {
            string orderby = CurrentSort + " " + CurrentSortOrder;
            string gameName = Server.HtmlEncode(txtGameName.Text.Trim().ToLower());
            string wokGameName = KanevaGlobals.WokGameName.ToLower();

            // Usability tweak. If searching for wok, kaneva, world of kaneva default to wok game name
            if (gameName == "wok" ||
                gameName == "kaneva" ||
                gameName == "world of kaneva")
            {
                gameName = wokGameName;
            }

            // Log the CSR activity
            this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Searching games. game_name = " + gameName, 0, 0);

            //default empty set
            global::Kaneva.DataLayer.DataObjects.PagedDataTable pds = new global::Kaneva.DataLayer.DataObjects.PagedDataTable();

            // append the game name to the filter if one provided
            if (gameName.Length > 0)
            {
                string gameNameFIlter = "game_name='" + gameName + "'";
                filter = ((filter.Length > 0) ? (filter + " AND " + gameNameFIlter) : gameNameFIlter);
            }

            //get the user's company if a non-admin
            if(gameDeveloperId != -1)
            {
                DevelopmentCompany company = GetDevelopmentCompanyFacade().GetCompanyByDeveloperId(gameDeveloperId);
                if (company.CompanyId > 0)
                {
                    //this will get games owned by the user's company
                    pds = GetGameFacade ().GetGamesByOwner (gameDeveloperId, Server.HtmlEncode (filter), orderby, pageNumber, itemsPerPage.NumberOfPages);
                }
            }
            else if(gameDeveloperId == -1)
            {
                //this will get all games regardless of company
                pds = GetGameFacade().GetGamesByOwner(0, filter, orderby, pageNumber, itemsPerPage.NumberOfPages);
            }

            pgTop.NumberOfPages = Math.Ceiling((double)pds.TotalCount / itemsPerPage.NumberOfPages).ToString();
            pgTop.DrawControl();

            rptGames.DataSource = pds;
            rptGames.DataBind();

            // The results
            lblSearch.Text = GetResultsText(pds.TotalCount, pageNumber, itemsPerPage.NumberOfPages, pds.Rows.Count);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Search Event Handler
        /// </summary>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData(pgTop.CurrentPageNumber, filterBy.CurrentFilter);
        }

        /// <summary>
        /// Pager change event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            BindData(e.PageNumber, filterBy.CurrentFilter);
        }

        private void itemsPerPage_FilterChanged(object sender, FilterChangedEventArgs e)
        {
            pgTop.CurrentPageNumber = 1;
            BindData(1, filterBy.CurrentFilter);
        }
        
        /// <summary>
        /// filMembers_FilterChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void filterBy_FilterChanged(object sender, FilterChangedEventArgs e)
        {
            pgTop.CurrentPageNumber = 1;
            BindData(1, e.Filter);
        }

        /// <summary>
        /// They clicked to sort a column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void btnSort_Click(object sender, System.EventArgs e)
        {
            base.btnSort_Click(sender, e);
            BindData(pgTop.CurrentPageNumber, filterBy.CurrentFilter);
        }

        protected void User_Sorting(object sender, DataGridSortCommandEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            BindData(pgTop.CurrentPageNumber, filterBy.CurrentFilter);
        }

        /// <summary>
        /// Button purchase the item whos button was clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SelectGame_Command(Object sender, CommandEventArgs e)
        {
            if (Session["SelectedGameId"] == null || !Session["SelectedGameId"].Equals(e.CommandArgument.ToString())) // Use object.Equals or cast session variable to string
            {
                // Clear selected server if game ID has changed
                Session["SelectedWoKServerId"] = null;
            }

            //get the selected id from the button of the user clicked add to session
            Session["SelectedGameId"] = e.CommandArgument.ToString();

            string redirectURL = GetHomePageURL(GetGameSearchDefaultPage());

            //redirect the page back to where ever it came from
            if (Session["CallingPage"] != null)
            {
                string currentURL = Request.Url.ToString();
                string returnURL = Session["CallingPage"].ToString();
                if (currentURL != returnURL)
                {
                    redirectURL = returnURL;
                }
            }
            Response.Redirect(redirectURL);
        }

        private void rptGames_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton thumbnail = (LinkButton)e.Item.FindControl("thumbnail");
                LinkButton gameTitle = (LinkButton)e.Item.FindControl("gameTitle");

                //create the tool summary data
                string name = "\\<b>Name: </b>" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "game_name")) + "<br>";
                string description = "\\<b>Description: </b>" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "game_synopsis")) + "<br>";
                //string owner = "\\<b>Owner: </b>" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "username")) + "<br>";
                string restricted = "\\<b>Rating: </b>" + GetGameRatingDescription(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "game_rating_id"))) + "<br>";
                //string assetType = "\\<b>Type: </b>"; //+ GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "asset_type_id"))) + "<br>";
                string _public = "\\<b>Access: </b>" + GetGameAccessDescription(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "game_access_id"))) + "<br>";
                string createdOn = "\\<b>Added On: </b>" + FormatDateTime(Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "game_creation_date"))) + "<br>";

                string toolTip = "\\ <h4>Asset Details</h4>" + name + description + restricted + _public + createdOn;
                thumbnail.Attributes.Add("onmouseover", "whiteBalloonSans.showTooltip(event,'" + CleanJavascriptFull(toolTip) + "')");
                gameTitle.Attributes.Add("onmouseover", "whiteBalloonSans.showTooltip(event,'" + CleanJavascriptFull(toolTip) + "')");
            }
        }

        private void rptGames_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                LinkButton lnkEdit = (LinkButton)e.Item.FindControl("lnkEdit");
                CheckBox chkEdit = (CheckBox)e.Item.FindControl("chkEdit");
                Label lblMessage = (Label)e.Item.FindControl("lblMessage");

                int statusId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "game_status_id"));
                int ownerId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "owner_id"));
                string teaser = DataBinder.Eval(e.Item.DataItem, "game_synopsis") == null ? "" : DataBinder.Eval(e.Item.DataItem, "game_synopsis").ToString();

                //only asset owner and admin can edit the asset
                bool canEditGame = true;

                chkEdit.Visible = lnkEdit.Visible = canEditGame;
                lblMessage.Visible = !canEditGame;

                LinkButton hlStatus = (LinkButton)e.Item.FindControl("hlStatus");
                LinkButton hlAddData = (LinkButton)e.Item.FindControl("hlAddData");

                hlStatus.Text = GetGameStatusDescription(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "game_status_id")));
                hlStatus.CommandArgument = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "game_id"));

                string keywords = DataBinder.Eval(e.Item.DataItem, "game_keywords") == null ? "" : DataBinder.Eval(e.Item.DataItem, "game_keywords").ToString();

                //pull in the categories for the game and get the counts
                int communityId = GetCommunityFacade().GetCommunityIdFromGameId(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "game_id")));
                int categoryCount = GetCommunityFacade().GetCommunityCategories(communityId).Count;

                //pick the categories for the game.
                if (keywords.Length.Equals(0) || teaser.Length.Equals(0) || categoryCount.Equals(0))
                {
                    hlAddData.Text = ": Add details";
                    hlAddData.CommandArgument = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "game_id"));
                }

            }
        }

        #endregion


        #region Attributes


        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "game_name";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }


        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.rptGames.ItemCreated += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptGames_ItemCreated);
            this.rptGames.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptGames_ItemDataBound);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
            filterBy.FilterChanged += new FilterChangedEventHandler(filterBy_FilterChanged);
            itemsPerPage.FilterChanged += new FilterChangedEventHandler(itemsPerPage_FilterChanged);
        }

        #endregion

    }
}