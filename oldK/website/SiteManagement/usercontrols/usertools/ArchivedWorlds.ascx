<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ArchivedWorlds.ascx.cs" Inherits="SiteManagement.usercontrols.ArchivedWorlds" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>

<script type="text/javascript" language="javascript" src="../../jscript/prototype.js"></script>

<div class="listingHeader">
    <span style="float:right;">
        <label for="txtWorldName">World Name:</label>
        <asp:TextBox ID="txtWorldName" runat="server" ClientIDMode="Static"></asp:TextBox>
        <label for="txtCreatorName">Creator Name:</label>
        <asp:TextBox ID="txtCreatorName" runat="server" ClientIDMode="Static"></asp:TextBox>
        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
    </span>
    <asp:LinkButton ID="lbToggleMode" runat="server" OnClick="lbToggleMode_Click"></asp:LinkButton>
    <br />
    <br />
    <span id="spnErrorData" runat="server" class="error"></span>
    <span id="spnSuccessData" runat="server" class="confirm"></span>
</div>
<div id="dvArchivedWorlds" runat="server" class="listingContainer">
	<asp:GridView ID="gvArchivedWorlds" runat="server" OnRowDeleting="gvArchivedWorlds_Delete" AutoGenerateColumns="false">
		<RowStyle BackColor="White"></RowStyle>
		<HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000"
			HorizontalAlign="Left"></HeaderStyle>
		<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
		<FooterStyle BackColor="#FFffff"></FooterStyle>
		<Columns>
			<asp:TemplateField>
				<ItemTemplate>
					&nbsp;<asp:LinkButton ID="lbnRestore" runat="server" CausesValidation="false" CommandName="Delete">Restore</asp:LinkButton>
                    &nbsp;
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="CommunityId" HeaderText="CommunityId" />
            <asp:BoundField DataField="Name" HeaderText="Name" />
			<asp:BoundField DataField="CreatorUsername" HeaderText="Creator Username" />
            <asp:BoundField DataField="ArchivedVia" HeaderText="Archived In" />
            <asp:BoundField DataField="ArchivedOn" HeaderText="Archived On" />
		</Columns>
	</asp:GridView>
    <Kaneva:Pager runat="server" id="pgArchived" OnPageChanged="pg_PageChanged"/>
</div>
<div id="dvActiveWorlds" runat="server" class="listingContainer">
	<asp:GridView ID="gvActiveWorlds" runat="server" OnRowDeleting="gvActiveWorlds_RowDeleting" AutoGenerateColumns="false">
		<RowStyle BackColor="White"></RowStyle>
		<HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000"
			HorizontalAlign="Left"></HeaderStyle>
		<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
		<FooterStyle BackColor="#FFffff"></FooterStyle>
		<Columns>
			<asp:TemplateField>
				<ItemTemplate>
					&nbsp;<asp:LinkButton ID="lbnArchive" runat="server" CausesValidation="false" CommandName="Delete">Archive</asp:LinkButton>
                    &nbsp;
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="CommunityId" HeaderText="CommunityId" />
            <asp:BoundField DataField="Name" HeaderText="Name" />
			<asp:BoundField DataField="CreatorUsername" HeaderText="Creator Username" />
		</Columns>
	</asp:GridView>
    <Kaneva:Pager runat="server" id="pgActive" OnPageChanged="pgActive_PageChanged"/>
</div>