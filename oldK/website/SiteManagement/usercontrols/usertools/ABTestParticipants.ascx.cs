﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;
using Kaneva.BusinessLayer.Facade;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class ABTestParticipants : BaseUserControl
    {
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private int pageSize = 20;
        private int pageNum  = 1;
        private string updatedUserGroupId = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                divErrorTop.Style.Add("display", "none");

                if (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.KANEVA_PROPRIETARY) != (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL)
                {
                    return;
                }

                BindExperimentDropdown();

                // Default to current user
                // https://kaneva.atlassian.net/browse/ED-3208
                if (!IsPostBack)
                {
                    CurrentSearchString = GetCurrentUser().Username;
                    BindExperimentParticipants(true);

                    ClearParticipantFields();
                    divDetails.Visible = false;
                    divParticipantListing.Visible = true;
                    CurrentParticipant = null;
                }
            }
        }

        protected string GetExperimentName(UserExperimentGroup userGroup)
        {
            if (userGroup == null || userGroup.ExperimentGroup == null || userGroup.ExperimentGroup.Experiment == null)
            {
                return string.Empty;
            }

            return userGroup.ExperimentGroup.Experiment.Name;
        }

        protected string GetGroupName(UserExperimentGroup userGroup)
        {
            if (userGroup == null || userGroup.ExperimentGroup == null)
            {
                return string.Empty;
            }

            return userGroup.ExperimentGroup.Name;
        }

        private void BindExperimentDropdown()
        {
            Experiment blankExperiment = new Experiment();
            blankExperiment.Name = string.Empty;
            blankExperiment.ExperimentId = string.Empty;

            PagedList<Experiment> experiments = GetExperimentFacade().SearchExperiments(string.Empty, false, null, EXPERIMENT_STATUS_TYPE.STARTED, 
                "e.creation_date DESC", 1, Int32.MaxValue);
            experiments.Insert(0, blankExperiment);

            ddlExperiment.DataSource = experiments;
            ddlExperiment.DataTextField = "Name";
            ddlExperiment.DataValueField = "ExperimentId";
            ddlExperiment.DataBind();
        }

        private void BindExperimentParticipants(bool bExactMatch)
        {
            try
            {
                PagedList<UserExperimentGroup> participants = GetExperimentFacade().SearchUserExperimentGroups(CurrentSearchString, bExactMatch, ddlExperiment.SelectedValue,
                    EXPERIMENT_STATUS_TYPE.STARTED, "FIELD(e.status, 'S', 'C', 'E', 'A'), e.creation_date DESC", PageNum, PageSize);

                rptParticipants.DataSource = participants;
                rptParticipants.DataBind();

                // The results
                lblSearch.Text = GetResultsText(participants.TotalCount, PageNum, PageSize, participants.Count);

                pgTop.NumberOfPages = Math.Ceiling((double)participants.TotalCount / PageSize).ToString();
                pgTop.DrawControl();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, true);
            }
        }

             /// <summary>   
        /// Repeater rptParticipants ItemDataBound Event Handler
        /// </summary>
        protected void rptParticipants_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                // get the details
                UserExperimentGroup userExperimentGroup = (UserExperimentGroup)e.Item.DataItem;

                DropDownList ddlGroupInlineInput = ((DropDownList) e.Item.FindControl ("ddlGroupInlineInput"));

                ddlGroupInlineInput.DataSource = userExperimentGroup.ExperimentGroup.Experiment.ExperimentGroups;
                ddlGroupInlineInput.DataBind();
                ddlGroupInlineInput.SelectedValue = userExperimentGroup.GroupId;

                if (!string.IsNullOrWhiteSpace(updatedUserGroupId) && updatedUserGroupId == userExperimentGroup.UserGroupId)
                {
                    ShowMessage("Participant updated.", false, "spnMsgInline", e.Item);
                }
            }
        }

        protected void ddlGroupInlineInput_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlGroupInlineInput = (DropDownList)sender;
            RepeaterItem rpItem = ddlGroupInlineInput.NamingContainer as RepeaterItem;
            if (rpItem != null)
            {
                HiddenField hfUserGroupId = (HiddenField)rpItem.FindControl("hfUserGroupId");
                UserExperimentGroup userExperimentGroup = GetExperimentFacade().GetUserExperimentGroup(hfUserGroupId.Value);

                userExperimentGroup.GroupId = ddlGroupInlineInput.SelectedItem.Value;

                if (!GetExperimentFacade().SaveUserExperimentGroup(userExperimentGroup))
                {
                    updatedUserGroupId = "";
                }
                else
                {
                    updatedUserGroupId = userExperimentGroup.UserGroupId;
                }
                BindExperimentParticipants(true);
            }
        }

        private void BindExperimentDropdown(IList<Experiment> experiments)
        {
            try
            {
                ddlExperimentInput.DataSource = experiments;
                ddlExperimentInput.DataTextField = "Name";
                ddlExperimentInput.DataValueField = "ExperimentId";

                ddlExperimentInput.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, true);
            }
        }

        private void BindExperimentGroupsDrowdown(IList<ExperimentGroup> groups)
        {
            try
            {
                ddlGroupInput.DataSource = groups;
                ddlGroupInput.DataTextField = "Name";
                ddlGroupInput.DataValueField = "GroupId";

                ddlGroupInput.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, true);
            }
        }

        /// <summary>
        /// Configures the error message for display.
        /// </summary>
        private void ShowMessage(string msg, bool isError, string msgDiv = "", RepeaterItem container = null)
        {
            HtmlGenericControl spnMsgInline = new HtmlGenericControl();
            if (container != null)
            {
                spnMsgInline = (HtmlGenericControl)container.FindControl("spnMsgInline");
            }

            divErrorTop.InnerText = msg;
            spnMsgInline.InnerText = msg;

            // Choose which message container to display. Default sets all and relies on the ajax refresh to
            // only refresh the necessary ones.
            switch (msgDiv)
            {
                case "divErrorTop":
                    divErrorTop.Style.Add("display", "block");
                    break;
                case "spnMsgInline":
                    spnMsgInline.Style.Add("display", "inline");
                    break;
                default:
                    divErrorTop.Style.Add("display", "block");
                    spnMsgInline.Style.Add("display", "inline");
                    break;
            }

            divErrorTop.Attributes["class"] = (isError ? "error" : "success");
            spnMsgInline.Attributes["class"] = (isError ? "error" : "success");
        }

        //clear the server data fields
        private void ClearParticipantFields()
        {
            txtName.Text = "";
            ddlGroupInput.Items.Clear();
        }

        private void ParticipantDetailsReset()
        {
            ClearParticipantFields();
            divDetails.Visible = false;
            divParticipantListing.Visible = false;
            CurrentParticipant = null;
            CurrentSearchString = "";
            txtSearch.Text = "";
        }

        /// <summary>
        /// lbEdit_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbEdit_Click(Object sender, CommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                ClearParticipantFields();
                txtName.Enabled = false;
                ddlExperimentInput.Enabled = false;

                CurrentParticipant = GetExperimentFacade().GetUserExperimentGroup(e.CommandArgument.ToString());

                BindExperimentDropdown(new List<Experiment> { CurrentParticipant.ExperimentGroup.Experiment });
                BindExperimentGroupsDrowdown(CurrentParticipant.ExperimentGroup.Experiment.ExperimentGroups);

                try
                {
                    txtName.Text = CurrentParticipant.Username;
                    ddlExperimentInput.SelectedValue = CurrentParticipant.ExperimentGroup.ExperimentId;
                    ddlGroupInput.SelectedValue = CurrentParticipant.GroupId;
                }
                catch (Exception)
                {
                    ShowMessage("Error populating experiment participant fields for editing.", true);
                }

                divDetails.Visible = true;
                divParticipantListing.Visible = false;
            }
        }

        /// <summary>
        /// btnAdd_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAdd_Click(Object sender, EventArgs e)
        {
            ClearParticipantFields();
            txtName.Enabled = true;
            ddlExperimentInput.Enabled = true;

            CurrentParticipant = new UserExperimentGroup();

            PagedList<Experiment> experiments = GetExperimentFacade().SearchExperiments(string.Empty, false, null, EXPERIMENT_STATUS_TYPE.STARTED,
                "e.creation_date DESC", 1, Int32.MaxValue);

            BindExperimentDropdown(experiments);

            if (experiments.Count > 0)
            {
                BindExperimentGroupsDrowdown(experiments[0].ExperimentGroups);
            }

            divDetails.Visible = true;
            divParticipantListing.Visible = false;
        }

        protected void ddlExperimentInput_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            Experiment experiment = GetExperimentFacade().GetExperiment(ddlExperimentInput.SelectedValue);

            ddlGroupInput.DataSource = experiment.ExperimentGroups;
            ddlGroupInput.DataBind();
        }

        /// <summary>
        /// Page Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pgTop_PageChange(object sender, PageChangeEventArgs e)
        {
            PageNum = e.PageNumber;
            BindExperimentParticipants(true);
        }

        /// <summary>
        /// btnSearch_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(Object sender, EventArgs e)
        {
            if (!SiteManagementCommonFunctions.ContainsInjectScripts(txtSearch.Text))
            {
                CurrentSearchString = txtSearch.Text;
            }

            // Reset the page number.
            PageNum = 1;
            pgTop.CurrentPageNumber = PageNum;

            BindExperimentParticipants(true);

            ClearParticipantFields();
            divDetails.Visible = false;
            divParticipantListing.Visible = true;
            CurrentParticipant = null;
        }

        /// <summary>
        /// btnUpdateExperiments_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdateParticipants_Click(Object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsValid)
                {
                    ShowMessage("Invalid input.", true);
                }

                // If name is enabled, we're adding a new participant
                if (txtName.Enabled && !string.IsNullOrWhiteSpace(txtName.Text))
                {
                    int userId = GetUserFacade().GetUserIdFromUsername(txtName.Text);
                    if (userId > 0)
                    {
                        CurrentParticipant.UserId = userId;
                        CurrentParticipant.Username = txtName.Text;
                        CurrentParticipant.AssignmentDate = DateTime.Now;

                        UserExperimentGroup existingGroup = GetExperimentFacade().GetUserExperimentGroupByExperimentId(userId, ddlExperimentInput.SelectedValue);
                        
                        if (existingGroup != null)
                        {
                            ShowMessage("User has already been assigned to this experiment.", true);
                            return;
                        }
                    }
                    else
                    {
                        ShowMessage("Invalid user.", true);
                        return;
                    }
                }

                CurrentParticipant.GroupId = ddlGroupInput.SelectedValue;

                if (!GetExperimentFacade().SaveUserExperimentGroup(CurrentParticipant))
                {
                    ShowMessage("Error updating experiment participant", true);
                }
                else
                {
                    ShowMessage("Experiment Participant updated successfully", false);
                    ParticipantDetailsReset();
                }
            }
            catch( Exception ex )
            {
                ShowMessage(ex.Message, true);
            }
        }
        
        /// <summary>
        /// btnCancelExperiments_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancelParticipants_Click(Object sender, EventArgs e)
        {
            ParticipantDetailsReset();
            CurrentParticipant = null;
        }

        private void GetRequestValues()
        {
            // Get items per page count
            if (Request["pagesize"] != null)
            {
                try
                {
                    PageSize = Convert.ToInt32(Request["pagesize"]);
                }
                catch { }
            }

            // Get the current page number
            if (Request["p"] != null)
            {
                try
                {
                    PageNum = Convert.ToInt32(Request["p"]);
                }
                catch { }
            }
        }

        private int PageNum
        {
            set { this.pageNum = value; }
            get { return this.pageNum; }
        }
        private int PageSize
        {
            set { this.pageSize = value; }
            get { return this.pageSize; }
        }

        private string CurrentSearchString
        {
            get
            {
                if (ViewState["CurrentSearchString"] == null)
                {
                    return "";
                }

                return (string)ViewState["CurrentSearchString"];
            }
            set { ViewState["CurrentSearchString"] = value; }
        }

        private UserExperimentGroup CurrentParticipant
        {
            get
            {
                if (ViewState["currentParticipant"] == null)
                {
                    ViewState["currentParticipant"] = new UserExperimentGroup();
                }
                return (UserExperimentGroup)ViewState["currentParticipant"];
            }
            set
            {
                ViewState["currentParticipant"] = value;
            }
        }

        private bool IsPostBackKaneva
        {
            get
            {
                if (ViewState["ispostback"] == null)
                {
                    return false;
                }

                return (bool)ViewState["ispostback"];
            }
            set { ViewState["ispostback"] = value; }
        }


        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler(pgTop_PageChange);
        }

        #endregion Web Form Designer generated code
    }
}