using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using KlausEnt.KEP.Kaneva.framework.biz.widgets;
using KlausEnt.KEP.Kaneva.framework.widgets;
//using KlausEnt.KEP.Kaneva.mykaneva.widgets;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Linq;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class UserSuspensions : BaseUserControl
    {
        #region Declarations

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.USER_PROFILE_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                if (Session["SelectedUserId"] != null)
                {
                    // Load the current user's data the first time
                    SELECTED_USER_ID = Convert.ToInt32(Session["SelectedUserId"]);

                    if (!IsPostBack)
                    {
                        // Log the CSR activity
                        this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Suspension", 0, SELECTED_USER_ID);

                        // Set default dates
                        txtBanStartDate.Text = DateTime.Now.ToShortDateString();
                        txtBanEndDate.Text = DateTime.Now.AddDays(1).ToShortDateString();

                        txtIPBanStartDate.Text = DateTime.Now.ToShortDateString();
                        txtIPBanEndDate.Text = DateTime.Now.AddDays(1).ToShortDateString();

                        txtSystemBanStartDate.Text = DateTime.Now.ToShortDateString();
                        txtSystemBanEndDate.Text = DateTime.Now.AddDays(1).ToShortDateString();

                        InitialBind();
                        BindIPBanData(SELECTED_USER_ID);
                    }
                }
                else
                {
                    Session["CallingPage"] = Request.Url.ToString();
                    Response.Redirect(ResolveUrl("~/Default.aspx?ws=" + GetCurrentSiteID((int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.KANEVA) + "&mn=" + GetUserSearchParentMenuID() + "&sn=" + GetUserSearchControlID()));
                }

            }
            else
            {
                RedirectToHomePage();
            }
        }

        #endregion

        #region Functions

        private void InitialBind()
        {
            currentPage = 0;

            drp_Statuses.DataTextField = "name";
            drp_Statuses.DataValueField = "status_id";
            drp_Statuses.DataSource = UsersUtility.GetUserStatus();
            drp_Statuses.DataBind();

            dt = GetUserFacade().GetUserSuspensions(SELECTED_USER_ID);
            BindData();
        }

        private void BindData()
        {
            // Set the user's status
            lbl_UserStatus.Text = WebCache.GetStatusName(userStatusId);

            // Set the sortable columns
            string orderby = CurrentSort + " " + CurrentSortOrder;

            //set the current page
            this.dgrdBans.CurrentPageIndex = currentPage;

            if (CurrentSortOrder.Equals("DESC"))
            {
                dt = dt.OrderByDescending(s => s.BanEndDate).ToList();
            }
            else 
            {
                dt = dt.OrderBy(s => s.BanEndDate).ToList();
            }

            List<UserSystem> systems = GetUserFacade().GetUserSystems(SELECTED_USER_ID);
            lboxUserSystems.DataSource = systems;
            lboxUserSystems.DataTextField = "SystemId";
            lboxUserSystems.DataValueField = "SystemId";
            lboxUserSystems.DataBind();

            dgrdBans.DataSource = dt;
            dgrdBans.DataBind();
        }

        private void BindIPBanData(int userId)
        {
            divUsername.InnerText = UsersUtility.GetUserNameFromId(userId);

            DataRow drIPs = UsersUtility.GetIPsByUser(userId);

            if (drIPs == null || drIPs["ip_address"] == null) return;

            string ip = drIPs["ip_address"].ToString();
            string last_ip = drIPs["last_ip_address"].ToString();

            // User IP info
            chkIP.Enabled = true;
            chkLastIP.Enabled = true;

            chkIP.Checked = false;
            chkLastIP.Checked = false;

            chkIP.Text = ip;
            chkLastIP.Text = last_ip;

            lbUnBanIP.Style.Add("display", "none");
            lbUnBanLastIP.Style.Add("display", "none");

            lbUnBanIP.CommandArgument = ip;
            lbUnBanLastIP.CommandArgument = last_ip;

            if (UsersUtility.IsIPBanned(ip))
            {
                chkIP.Enabled = false;
                chkIP.Checked = true;
                lbUnBanIP.Style.Add("display", "block");
            }
            if (UsersUtility.IsIPBanned(last_ip))
            {
                chkLastIP.Enabled = false;
                chkLastIP.Checked = true;
                lbUnBanLastIP.Style.Add("display", "block");
            }

            // Related IP info
            DataTable dtRelatedIPs = UsersUtility.GetRelatedIPsByUser(userId);

            rptRelatedIPs.DataSource = dtRelatedIPs;
            rptRelatedIPs.DataBind();
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// ShowMessage
        /// </summary>
        protected void ShowMsg(HtmlContainerControl ctrl, bool isErr, string msg)
        {
            //  messages.ForeColor = isErr ? System.Drawing.Color.DarkRed : System.Drawing.Color.DarkGreen;
            ctrl.Attributes.Add("class", isErr ? "errBox darkRed" : "infoBox darkGreen");
            ctrl.InnerHtml = msg;
            ctrl.Style.Add("display", "block");
        }

        #endregion

        #region Event Handlers

        protected void dgrdBans_CancelCommand(object source, DataGridCommandEventArgs e)
        {
            this.dgrdBans.EditItemIndex = -1;
            BindData();
        }

        protected void dgrdBans_EditCommand(object source, DataGridCommandEventArgs e)
        {
            this.dgrdBans.EditItemIndex = e.Item.ItemIndex;
            BindData();
        }

        protected void dgrdBans_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            currentPage = e.NewPageIndex;
            BindData();
        }

        protected void dgrdBans_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            this.dgrdBans.EditItemIndex = -1; //closes any open edit boxes
            BindData();
        }

        protected void dgrdBans_DeleteCommand(object source, DataGridCommandEventArgs e)
        {
            int banId = Convert.ToInt32(e.Item.Cells[0].Text);

            try
            {
                GetUserFacade().DeleteUserSuspension(banId, SELECTED_USER_ID);
                lblDeleteMsg.Text = "Suspension Deleted. ";

                string strUserNote = "Deleted Suspension. ";
                UsersUtility.InsertUserNote(SELECTED_USER_ID, GetUserId(), strUserNote);
            }
            catch (Exception exc)
            {
                lblDeleteMsg.Text = "Error Deleting Suspension: " + exc;
            }
            InitialBind();
        }

        protected void dgSystemBans_CancelCommand(object source, DataGridCommandEventArgs e)
        {
            dgSystemBans.EditItemIndex = -1;
            BindData();
        }

        protected void dgSystemBans_DeleteCommand(object source, DataGridCommandEventArgs e)
        {
            int banId = Convert.ToInt32(e.Item.Cells[0].Text);

            try
            {
                GetUserFacade().DeleteUserSystemSuspension(banId);
                lblDeleteMsg.Text = "Suspension Deleted. ";

                string strUserNote = "Deleted System Suspension. ";
                UsersUtility.InsertUserNote(SELECTED_USER_ID, GetUserId(), strUserNote);
            }
            catch (Exception exc)
            {
                lblDeleteMsg.Text = "Error Deleting Suspension: " + exc;
            }
            InitialBind();
            BindSystemSuspensions(null);
        }

        protected void drpBanLength_Selected(object sender, System.EventArgs e)
        {
            if (drpBanLength.SelectedValue == "Perm")
            {
                txtBanEndDate.Text = "";
            }
            else
            {
                int banLength = Convert.ToInt16(drpBanLength.SelectedValue);
                DateTime banEnd = Convert.ToDateTime(txtBanStartDate.Text).AddDays(banLength);
                txtBanEndDate.Text = banEnd.ToShortDateString().ToString();
            }
        }

        protected void ddlSystemBanLength_Selected(object sender, System.EventArgs e)
        {
            if (ddlSystemBanLength.SelectedValue == "Perm")
            {
                txtSystemBanEndDate.Text = "";
            }
            else
            {
                int banLength = Convert.ToInt16(ddlSystemBanLength.SelectedValue);
                DateTime banEnd = Convert.ToDateTime(txtSystemBanStartDate.Text).AddDays(banLength);
                txtSystemBanEndDate.Text = banEnd.ToShortDateString().ToString();
            }
        }

        protected void drpIPBanLength_Selected(object sender, EventArgs e)
        {
            if (drpIPBanLength.SelectedValue == "Perm")
            {
                txtIPBanEndDate.Text = "";
            }
            else
            {
                int ipBanLength = Convert.ToInt16(drpIPBanLength.SelectedValue);
                DateTime ipBanEnd = Convert.ToDateTime(txtIPBanStartDate.Text).AddDays(ipBanLength);
                txtIPBanEndDate.Text = ipBanEnd.ToShortDateString().ToString();
            }
        }

        protected void btnAddIPBan_Click(object sender, EventArgs e)
        {
            try
            {
                string ipBanStart = txtIPBanStartDate.Text + " " + drpIPBanStartTime.SelectedValue;
                string ipBanEnd = string.Empty;

                // Make sure at least 1 ip is selected
                //if (!chkIP.Checked && !chkLastIP.Checked)
                //{
                //    ShowMsg (spnIPBanAlertMsg, "Please select an IP Address to ban.");
                //    return;
                //}

                if (((chkIP.Enabled && !chkIP.Checked) && ((chkLastIP.Enabled && !chkLastIP.Checked) || !chkLastIP.Enabled)) ||
                     ((chkLastIP.Enabled && !chkLastIP.Checked) && ((chkIP.Enabled && !chkIP.Checked) || !chkIP.Enabled)))
                {
                    ShowMsg(spnIPBanAlertMsg, true, "Please select an IP Address to ban.");
                    return;
                }

                // Check to see if both IPs are already banned
                if (!chkIP.Enabled && !chkLastIP.Enabled)
                {
                    ShowMsg(spnIPBanAlertMsg, true, "Both IP Addresses are already banned.");
                    return;
                }

                // Check if ban length is set via drop down
                if (drpIPBanLength.SelectedValue != "0")
                {
                    int banLength = Convert.ToInt32(drpIPBanLength.SelectedValue);

                    // check for permanent ban.  perm ban value is -1
                    if (banLength < 0)
                    {
                        ipBanEnd = DateTime.MaxValue.ToString();
                    }
                    else
                    {
                        DateTime startDate = new DateTime();
                        startDate = Convert.ToDateTime(ipBanStart);
                        ipBanEnd = startDate.AddDays(Convert.ToDouble(banLength)).ToString();
                    }
                }
                else
                {   // use end date/time to set ban length
                    ipBanEnd = txtIPBanEndDate.Text.Trim();
                    if (ipBanEnd == "")
                    {
                        ShowMsg(spnIPBanAlertMsg, true, "Please enter an End Date for IP Ban.");
                        return;
                    }
                    else
                    {
                        try
                        {
                            DateTime startDate = new DateTime();
                            startDate = Convert.ToDateTime(ipBanStart);

                            DateTime endDate = new DateTime();
                            endDate = Convert.ToDateTime(ipBanEnd + " " + drpIPBanEndTime.SelectedValue);

                            if (endDate <= startDate)
                            {
                                ShowMsg(spnIPBanAlertMsg, true, "Please enter an End Date/Time later than Start Date.");
                                return;
                            }
                        }
                        catch
                        {
                            ShowMsg(spnIPBanAlertMsg, true, "Please enter a valid End Date/Time.");
                            return;
                        }
                    }

                    ipBanEnd = txtIPBanEndDate.Text.Trim() + " " + drpIPBanEndTime.SelectedValue;
                }

                // is sign-up ip address checked?
                if (chkIP.Checked && chkIP.Enabled)
                {
                    try
                    {
                        // verify ip is not already banned.  will throw error if ip already banned
                        if (!UsersUtility.IsIPBanned(chkLastIP.Text))
                        {
                            // ban the ip
                            if (!UsersUtility.AddIPBan(chkIP.Text, ipBanStart, ipBanEnd).Equals(0))
                            {
                                ShowMsg(spnIPBanAlertMsg, false, "IP " + chkIP.Text + " added to ban list.");

                                string strUserNote = "IP " + chkIP.Text + " Banned. <br />";
                                if (txtIPBanReasonNotes.Text != "") { strUserNote += "Notes: " + txtIPBanReasonNotes.Text + "<br />"; }

                                // add user not explaining the ban
                                UsersUtility.InsertUserNote(SELECTED_USER_ID, GetUserId(), strUserNote);
                            }
                            else
                            {
                                ShowMsg(spnIPBanAlertMsg, true, "Failed to Add Ban for IP: " + chkIP.Text + ".  AddIPBan() call failed." +
                                    "<br/>IP : " + chkLastIP.Text +
                                    "<br/>Start Date : " + ipBanStart +
                                    "<br/>End Date : " + ipBanEnd);
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        ShowMsg(spnIPBanAlertMsg, true, "Error Banning IP " + chkIP.Text + ". Please check the date fields. <br />" + exc.ToString() + "<br />");
                    }
                }

                // is last ip-address checked?
                if (chkLastIP.Checked && chkLastIP.Enabled)
                {
                    try
                    {
                        // verify the ip is not already banned.  will throw error if ip already banned
                        if (!UsersUtility.IsIPBanned(chkLastIP.Text))
                        {
                            // ban the ip
                            if (!UsersUtility.AddIPBan(chkLastIP.Text, ipBanStart, ipBanEnd).Equals(0))
                            {
                                ShowMsg(spnIPBanAlertMsg, false, "IP " + chkLastIP.Text + " added to ban list.");

                                string strUserNote = "IP " + chkLastIP.Text + " Banned. <br />";
                                if (txtIPBanReasonNotes.Text != "") { strUserNote += "Notes: " + txtIPBanReasonNotes.Text + "<br />"; }

                                // add user not explaining the ban
                                UsersUtility.InsertUserNote(SELECTED_USER_ID, GetUserId(), strUserNote);
                            }
                            else
                            {
                                ShowMsg(spnIPBanAlertMsg, true, "Failed to Add Ban for Last IP: " + chkLastIP.Text + ".  AddIPBan() call failed." +
                                    "<br/>IP : " + chkLastIP.Text +
                                    "<br/>Start Date : " + ipBanStart +
                                    "<br/>End Date : " + ipBanEnd);
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        ShowMsg(spnIPBanAlertMsg, true, "Error Banning Last IP " + chkLastIP.Text + ". Please check the date fields. <br />" + exc.ToString() + "<br />");
                    }
                }

                BindIPBanData(SELECTED_USER_ID);
            }
            catch (Exception exc)
            {
                ShowMsg(spnIPBanAlertMsg, true, "Error Banning IP. Please check the date fields. <br />" + exc.ToString() + "<br />");
            }
        }

        protected void lboxUserSystems_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            string systemId = lboxUserSystems.SelectedItem.ToString();
            
            if (!string.IsNullOrWhiteSpace(systemId))
            {
                List<User> systemUsers = GetUserFacade().GetSystemUsers(systemId);
                lboxSystemUsers.DataSource = systemUsers;
                lboxSystemUsers.DataTextField = "Username";
                lboxSystemUsers.DataValueField = "Username";
                lboxSystemUsers.DataBind();

                BindSystemSuspensions(systemId);
            }
        }

        private void BindSystemSuspensions(string systemId)
        {
            List<UserSystemSuspension> suspensions = GetUserFacade().GetUserSystemSuspensions(systemId);
            dgSystemBans.DataSource = suspensions;
            dgSystemBans.DataBind();
        }

        protected void lbUnBanIP_Click(object sender, CommandEventArgs e)
        {
            try
            {
                string ip = e.CommandArgument.ToString();

                if (!UsersUtility.RemoveIPBan(ip).Equals(0))
                {
                    ShowMsg(spnIPBanAlertMsg, false, "Ban removed for IP " + ip + ".");
                }

                BindIPBanData(SELECTED_USER_ID);
            }
            catch (Exception exc)
            {
                ShowMsg(spnIPBanAlertMsg, true, "Error Removin ban for IP. <br />" + exc.ToString() + "<br />");
            }
        }

        protected void btnAddSystemBan_Click(object sender, EventArgs e)
        {
            // Make sure systemId is valid.
            string systemId = lboxUserSystems.SelectedValue;
            if (string.IsNullOrWhiteSpace(systemId))
            {
                lblSystemBanError.Text = "Please select a system id to ban.";
                return;
            }  

            try
            {
                // Make sure only one active ban is present for a system at a time.
                List<UserSystemSuspension> activeSuspensions = GetUserFacade().GetUserSystemSuspensions(systemId, true);
                if (activeSuspensions.Count > 0)
                {
                    lblSystemBanError.Text = "A system may not have more than 1 active ban at a time.";
                    return;
                }

                // Handle start and end dates.
                string banStart = txtSystemBanStartDate.Text + " " + ddlSystemBanStartTime.SelectedValue;
                DateTime dtBanStart = DateTime.Parse(banStart);
                DateTime? banEnd = null;

                if (txtSystemBanEndDate.Text != "")
                {
                    banEnd = DateTime.Parse(txtSystemBanEndDate.Text + " " + ddlSystemBanEndTime.SelectedValue);
                }
              
                // Perform the systems ban.  This will ban the system and all associated users.
                if (!GetUserFacade().SaveUserSystemSuspension(new UserSystemSuspension(0, systemId, dtBanStart, banEnd), GetUserId()).Equals(0))
                {
                    lblSystemBanError.Text = "System Suspension Added.";

                    // Add CSR log to track system ban.
                    GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Added system suspension for system id: " + systemId, 0, 0);
                    
                    // Cancel any subscriptions affected users may have if it is a permanent ban.
                    if (ddlSystemBanLength.SelectedValue == "Perm")
                    { 
                        List<User> users = GetUserFacade().GetSystemUsers(systemId);
                        CybersourceAdaptor cybersourceAdapter = new CybersourceAdaptor();

                        foreach(User user in users)
                        {
                            try
                            {
                                cybersourceAdapter.CancelAllUserSubscriptions(user.UserId, "User has been banned.");
                            }
                            catch (Exception)
                            {
                                lblSystemBanError.Text = "Error cancelling the users' subscriptions.";
                            }
                        }
                    }
                }
                else
                {
                    lblSystemBanError.Text = "Falied to Add System Suspension.";
                }
            }
            catch (Exception exc)
            {
                lblSystemBanError.Text = "Error Suspending User. Please check the date fields. <br />" + exc.ToString() + "<br />";
            }

            currentPage = 0;
            this.dgrdBans.EditItemIndex = 0;
            InitialBind();
            BindSystemSuspensions(systemId);
        }

        protected void btn_Add_Click(object sender, EventArgs e)
        {
            try
            {
                // Prepare user notes.
                string strUserNote = "User suspended. <br />";
                strUserNote += "Reason: " + drpBanReasons.SelectedValue + "<br />";
                if (txtBanReasonNotes.Text != "") { strUserNote += "Notes: " + txtBanReasonNotes.Text + "<br />"; }

                // Process start and end dates.
                string banStart = txtBanStartDate.Text + " " + drpBanStartTime.SelectedValue;
                DateTime dtBanStart = DateTime.Parse(banStart);
                DateTime? banEnd = null;

                if (txtBanEndDate.Text != "")
                {
                    banEnd = DateTime.Parse(txtBanEndDate.Text + " " + drpBanEndTime.SelectedValue);
                }

                // Save the suspension.
                if (!GetUserFacade().SaveUserSuspension(userStatusId, new UserSuspension(0, SELECTED_USER_ID, dtBanStart, banEnd, null), GetUserId(), strUserNote).Equals(0))
                {
                    lbl_Messages.Text = "Suspension Added.";

                    // Cancel any subscriptions they may have if it is a permanent ban.
                    if (drpBanLength.SelectedValue == "Perm")
                    {
                        try
                        {
                            CybersourceAdaptor cybersourceAdapter = new CybersourceAdaptor();
                            cybersourceAdapter.CancelAllUserSubscriptions(SELECTED_USER_ID, "User has been banned.");
                        }
                        catch (Exception exc)
                        {
                            lbl_Messages.Text = "Error cancelling the users subscriiption. <br />" + exc.ToString() + "<br />";
                        }

                    }
                }
                else
                {
                    lbl_Messages.Text = "Falied to Add Suspension.";
                }
            }
            catch (Exception exc)
            {
                lbl_Messages.Text = "Error Suspending User. Please check the date fields. <br />" + exc.ToString() + "<br />";
            }
            currentPage = 0;
            this.dgrdBans.EditItemIndex = 0;
            InitialBind();
        }

        protected void btn_StatusUpdate_Click(object sender, EventArgs e)
        {
            // Update the user's status.
            int oldUserStatusId = userStatusId;
            int newUserStatusId = Convert.ToInt32(drp_Statuses.SelectedValue);    

            // If new status is deleted, mark the user's profile and home world as deleted.
            if (newUserStatusId == (int)User.eUSER_STATUS.DELETEDBYUS
                || newUserStatusId == (int)User.eUSER_STATUS.DELETED
                || newUserStatusId == (int)User.eUSER_STATUS.DELETEDVALIDATED)
            {
                GetUserFacade().MarkUserDeletedRecoverable(SELECTED_USER_ID, newUserStatusId);
            }
            else if (oldUserStatusId == (int)User.eUSER_STATUS.DELETEDBYUS 
                || oldUserStatusId == (int)User.eUSER_STATUS.DELETED
                || oldUserStatusId == (int)User.eUSER_STATUS.DELETEDVALIDATED)
            {
                // If going from deleted to a non-deleted status, re-activate he home world and profile.
                GetUserFacade().RecoverDeletedUser(SELECTED_USER_ID, newUserStatusId);
            }
            else
            {
                // Otherwise, just update the user's status.
                GetUserFacade().UpdateUserStatus(SELECTED_USER_ID, newUserStatusId);

                // If manually moving the user back to a valid status, end date all active suspensions for the user.
                if (newUserStatusId == (int)User.eUSER_STATUS.REGVALIDATED ||
                    newUserStatusId == (int)User.eUSER_STATUS.REGNOTVALIDATED)
                {
                    List<UserSuspension> activeSuspensions = GetUserFacade().GetUserSuspensions(SELECTED_USER_ID, true);
                    foreach (UserSuspension suspension in activeSuspensions)
                    {
                        suspension.BanEndDate = DateTime.Now;
                        GetUserFacade().SaveUserSuspension(newUserStatusId, suspension, GetCurrentUser().UserId, "Manually setting user to active status ends all active suspensions.");
                    }
                }
            }

            // Update User Note
            string strUserNote = "User Status Updated. <br />";
            strUserNote += "From: " + WebCache.GetStatusName(oldUserStatusId) + " To: " + WebCache.GetStatusName(userStatusId) + "<br />";
            if (txtBanReasonNotes.Text != "") { strUserNote += "Notes: " + txtStatusChangeReason.Text + "<br />"; }
            UsersUtility.InsertUserNote(SELECTED_USER_ID, GetUserId(), strUserNote);

            lblStatusMsg.Text = "User Status Updated.";
            BindData();
        }

        protected void switchSelectedUser(Object sender, CommandEventArgs e)
        {
            Session["SelectedUserId"] = Convert.ToInt32(e.CommandArgument);
            Response.Redirect(Request.Url.ToString());
        }

        #endregion


        #region Attributes

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "ban_date_end";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "DESC";
            }
        }

        protected int userStatusId
        {
            get
            {
                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(SELECTED_USER_ID);
                return user.StatusId;
            }
        }

        protected string userStatusName
        {
            get
            {
                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(SELECTED_USER_ID);
                return user.Ustate;
            }
        }

        private List<UserSuspension> dt
        {
            get
            {
                if (Session["_permZoneRoles"] == null)
                {
                    Session["_permZoneRoles"] = new DataTable();
                }
                return (List<UserSuspension>)Session["_permZoneRoles"];
            }
            set
            {
                Session["_permZoneRoles"] = value;
            }
        }

        private int currentPage
        {
            get
            {
                if (Session["_currentPage"] == null)
                {
                    Session["_currentPage"] = 0;
                }
                return (int)Session["_currentPage"];
            }
            set
            {
                Session["_currentPage"] = value;
            }
        }

        private int SELECTED_USER_ID
        {
            get
            {
                return (int)ViewState["users_id"];
            }
            set
            {
                ViewState["users_id"] = value;
            }

        }

        private string SELECTED_USER_NAME
        {
            get
            {
                if (ViewState["users_name"] == null)
                {
                    ViewState["users_name"] = UsersUtility.GetUserNameFromId(SELECTED_USER_ID);
                }

                return ViewState["users_name"].ToString();
            }
            set
            {
                ViewState["users_name"] = value;
            }

        }


        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}