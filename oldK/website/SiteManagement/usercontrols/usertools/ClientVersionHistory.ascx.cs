using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KlausEnt.KEP.SiteManagement;
using Kaneva.DataLayer.DataObjects;

namespace SiteManagement.usercontrols
{
    public partial class ClientVersionHistory : BaseUserControl
    {
        private int _page = 1;
        private int _itemsPerPage = 10;

        protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
                ResetPage();
			}
		}

        #region Data Manipulation
        /// <summary>
        /// Called to bind data to page elements
        /// </summary>
        protected void BindData()
		{
            PagedDataTable dtClientVersions = GetMetricsFacade().GetClientVersionHistory(_page, _itemsPerPage);
            gvRecords.DataSource = dtClientVersions;
            gvRecords.DataBind();

            pgBottom.CurrentPageNumber = _page;
            pgBottom.NumberOfPages = Math.Ceiling((double)dtClientVersions.TotalCount / _itemsPerPage).ToString();
            pgBottom.DrawControl();
		}

        /// <summary>
        /// Called to fill input fields from a db record
        /// </summary>
        protected void FillInputFromDataRow(DataRow row)
        {
            txtKEPClientVersion.Text = row["kep_client_version"].ToString();
            txtReleaseDate.Text = row["release_date"].ToString();
            txtDeprecatedDate.Text = (row["deprecated_date"] == DBNull.Value ? string.Empty : row["deprecated_date"].ToString());
            txtComment.Text = (row["comment"] == DBNull.Value ? string.Empty : row["comment"].ToString());
        }

        /// <summary>
        /// Called to empty input fields to defaults.
        /// </summary>
        protected void ResetFields()
        {
            txtKEPClientVersion.Text = string.Empty;
            txtReleaseDate.Text = string.Empty;
            txtDeprecatedDate.Text = string.Empty;
            txtComment.Text = string.Empty;
            divErrorData.InnerText = "";
        }

        /// <summary>
        /// Called to reset the page
        /// </summary>
        protected void ResetPage(int page = 1)
        {
            _page = page;

            BindData();

            dvRecordEdit.Visible = false;
            dvRecords.Visible = true;

            ResetFields();
        }

        /// <summary>
        /// Called to validate the page before saving
        /// </summary>
        protected bool ValidatePage(out string errorMessage)
        {
            bool inputsAreValid = true;
            errorMessage = string.Empty;

            DateTime releaseDate = DateTime.Now;
            if (!DateTime.TryParse(txtReleaseDate.Text, out releaseDate))
            {
                inputsAreValid = false;
                errorMessage = "Release Date must be a valid date.";
            }

            DateTime deprecatedDate = releaseDate;
            if (!string.IsNullOrWhiteSpace(txtDeprecatedDate.Text) && !DateTime.TryParse(txtDeprecatedDate.Text, out deprecatedDate))
            {
                inputsAreValid = false;
                errorMessage = "Deprecated Date must be a valid date.";
                deprecatedDate = DateTime.Now; // Make sure deprecated date holds a valid value for later comparison
            }

            if (releaseDate > deprecatedDate)
            {
                inputsAreValid = false;
                errorMessage = "Deprecated Date must be a later date than Release Date.";
            }

            return inputsAreValid;
        }
        #endregion

        #region Event Handlers
        protected void btnNewRecord_Click(object sender, EventArgs e)
		{
            dvRecordEdit.Visible = true;
            dvRecords.Visible = false;
			ResetFields();
		}

		protected void btnListRecords_Click(object sender, EventArgs e)
		{
            ResetPage();
		}

		protected void btnCancelEdit_Click(object sender, EventArgs e)
		{
            ResetPage();
		}

		protected void btnSaveRecord_Click(object sender, EventArgs e)
		{
            string errorMessage = string.Empty;
            if (Page.IsValid && ValidatePage(out errorMessage))
            {
                PagedDataTable dtExisting = GetMetricsFacade().GetClientVersionHistory(_page, _itemsPerPage, txtKEPClientVersion.Text);
                bool recordExists = (dtExisting != null && dtExisting.Rows.Count > 0);
                
                string clientVersion = txtKEPClientVersion.Text;
                DateTime releaseDate = DateTime.Parse(txtReleaseDate.Text);
                DateTime deprecatedDate = (string.IsNullOrWhiteSpace(txtDeprecatedDate.Text) ? DateTime.MaxValue : DateTime.Parse(txtDeprecatedDate.Text));
                string comment = txtComment.Text;

                if (!recordExists)
                {
                    recordExists = (GetMetricsFacade().InsertClientVersionHistory(clientVersion, releaseDate, deprecatedDate, comment) >= 0);
                }
                else
                {
                    GetMetricsFacade().UpdateClientVersionHistory(clientVersion, releaseDate, deprecatedDate, comment);
                }

                if (!recordExists)
                {
                    divErrorData.InnerText = "Error updating game Record.";
                }
                else
                {
                    ResetPage();
                }
            }
            else
            {
                divErrorData.InnerText = errorMessage;
            }
		}

		protected void gvRecords_Edit(object sender, GridViewEditEventArgs e)
		{
            dvRecordEdit.Visible = true;
            dvRecords.Visible = false;
            
            GridViewRow gvr = gvRecords.Rows[e.NewEditIndex];
            PagedDataTable dtRecord = GetMetricsFacade().GetClientVersionHistory(_page, _itemsPerPage, gvr.Cells[1].Text);

            FillInputFromDataRow(dtRecord.Rows[0]);
            e.Cancel = true;
		}

        protected void gvRecords_Delete(object sender, GridViewDeleteEventArgs e)
        {
            // Cancel the delete operation if the user attempts to remove
            // the last record from the GridView control.
            if (gvRecords.Rows.Count <= 1)
            {
                e.Cancel = true;
            }

            GridViewRow gvr = gvRecords.Rows[e.RowIndex];
            PagedDataTable dtRecord = GetMetricsFacade().GetClientVersionHistory(_page, _itemsPerPage, gvr.Cells[1].Text);

            GetMetricsFacade().DeleteClientVersionHistory(dtRecord.Rows[0]["kep_client_version"].ToString());

            ResetPage();
        }

        protected void pg_PageChanged(object sender, PageChangeEventArgs e)
        {
            ResetPage(e.PageNumber);
        }
        #endregion
    }  
}