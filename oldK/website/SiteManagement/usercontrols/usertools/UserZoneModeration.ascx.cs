using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class UserZoneModeration : BaseUserControl
    {
        #region Declarations

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.USER_DATA_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                if (Session["SelectedUserId"] != null)
                {
                    // Load the current user's data the first time
                    SELECTED_USER_ID = Convert.ToInt32(Session["SelectedUserId"]);

                    if (!IsPostBack)
                    {
                        // Log the CSR activity
                        this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Zone Moderation", 0, SELECTED_USER_ID);
                        InitialBind();
                    }
                    else
                    {
                        BindData();
                    }
                }
                else
                {
                    Session["CallingPage"] = Request.Url.ToString();
                    Response.Redirect(ResolveUrl("~/Default.aspx?ws=" + GetCurrentSiteID((int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.KANEVA) + "&mn=" + GetUserSearchParentMenuID() + "&sn=" + GetUserSearchControlID()));
                }

            }
            else
            {
                RedirectToHomePage();
            }
        }

        #endregion

        #region Functions

        private void InitialBind()
        {
            BindZones();
            currentPage = 0;
            dt = UsersUtility.GetPermanentZoneRoles(SELECTED_USER_ID);
            BindData();
        }

        private void BindData()
        {
            // Set the sortable columns
            string orderby = CurrentSort + " " + CurrentSortOrder;

            //set the current page
            this.dgrdRoles.CurrentPageIndex = currentPage;

            dt.DefaultView.Sort = orderby;

            dgrdRoles.DataSource = dt;
            dgrdRoles.DataBind();

            //display user name
            lblSelectedUser.Text = "Notes for " + SELECTED_USER_NAME;
        }

        /// <summary>
        /// BindZones
        /// </summary>
        private void BindZones()
        {
            DataTable dt = UsersUtility.GetPermanentZonesNotModeratedByUser(SELECTED_USER_ID);
            drpZones.DataSource = dt;
            drpZones.DataBind();

            // If there are no more zones that can  be added disable the button.
            if (dt.Rows.Count < 1)
            {
                btn_Add.Enabled = false;
                btn_Add.Text = "No Zones To Add";
            }
            else
            {
                btn_Add.Enabled = true;
                btn_Add.Text = "New Role";
            }
        }

        #endregion

        #region Event Handlers

        protected void dgrdRoles_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            this.dgrdRoles.EditItemIndex = -1;
            BindData();
        }

        protected void dgrdRoles_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            this.dgrdRoles.EditItemIndex = e.Item.ItemIndex;
            BindData();
        }

        protected void dgrdRoles_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            currentPage = e.NewPageIndex;
            BindData();
        }

        protected void dgrdRoles_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            this.dgrdRoles.EditItemIndex = -1; //closes any open edit boxes
            BindData();
        }

        protected void dgrdRoles_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            try
            {
                UsersUtility.DeletePermanentRole(SELECTED_USER_ID, Convert.ToInt32(e.Item.Cells[0].Text));
                lbl_Messages.Text = "Role Deleted.";

                string strUserNote = "Deleted Permanent Zone Moderation Role for Zone: " + drpZones.SelectedItem.Text;
                UsersUtility.InsertUserNote(SELECTED_USER_ID, GetUserId(), strUserNote);
            }
            catch (Exception exc)
            {
                lbl_Messages.Text = "Error Deleting Role: " + exc;
            }
            InitialBind();
        }

        protected void btn_Add_Click(object sender, System.EventArgs e)
        {
            try
            {
                UsersUtility.AddPermanentZoneRole(SELECTED_USER_ID, Convert.ToInt32(drpZones.SelectedValue), Convert.ToInt32(drpRoles.SelectedValue));
                lbl_Messages.Text = "Role Added.";

                string strUserNote = "Added Permanent Zone Moderation Role for Zone: " + drpZones.SelectedItem.Text;
                UsersUtility.InsertUserNote(SELECTED_USER_ID, GetUserId(), strUserNote);
            }
            catch (Exception exc)
            {
                lbl_Messages.Text = "Error Adding Role" + exc.ToString();
                //m_logger.Error("Error adding role ", exc);              
            }
            currentPage = 0;
            this.dgrdRoles.EditItemIndex = 0;
            InitialBind();
        }


        #endregion


        #region Attributes

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "name";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "DESC";
            }
        }

        private int SELECTED_USER_ID
        {
            get
            {
                return (int)ViewState["users_id"];
            }
            set
            {
                ViewState["users_id"] = value;
            }

        }

        private string SELECTED_USER_NAME
        {
            get
            {
                if (ViewState["users_name"] == null)
                {
                    ViewState["users_name"] = UsersUtility.GetUserNameFromId(SELECTED_USER_ID);
                }

                return ViewState["users_name"].ToString();
            }
            set
            {
                ViewState["users_name"] = value;
            }

        }

        private DataTable dt
        {
            get
            {
                if (Session["_permZoneRoles"] == null)
                {
                    Session["_permZoneRoles"] = new DataTable();
                }
                return (DataTable)Session["_permZoneRoles"];
            }
            set
            {
                Session["_permZoneRoles"] = value;
            }
        }

        private int currentPage
        {
            get
            {
                if (Session["_currentPage"] == null)
                {
                    Session["_currentPage"] = 0;
                }
                return (int)Session["_currentPage"];
            }
            set
            {
                Session["_currentPage"] = value;
            }
        }


        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}