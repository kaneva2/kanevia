using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class FameLevels : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.KANEVA_PROPRIETARY))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                lblErrMessage.Text = "";

                if (!IsPostBack)
                {
                    //bind the CatalogItems
                    BindData(1);
                }
            }
            else
            {
                RedirectToHomePage();
            }

        }

        #region Helper Methods

        /// <summary>
        /// BindMemberFameData
        /// </summary>
        private void BindData(int pageNumber)
        {
            // Set the sortable columns
            string orderby = CurrentSort + " " + CurrentSortOrder;

            int pageSize = 40;

            // Get the User Fame Info
            FameFacade fameFacade = new FameFacade();
            PagedList<Level> fameLevels = fameFacade.GetLevels((int)FameTypes.World, "", orderby, pageNumber, pageSize);

            if ((fameLevels != null) && (fameLevels.Count > 0))
            {
                lblNoFameLevels.Visible = false;

                dgFameLevels.DataSource = fameLevels;
                dgFameLevels.DataBind();

                // Set current page
                pgLevels.CurrentPageNumber = pageNumber;
                pgLevels.NumberOfPages = Math.Ceiling((double)fameLevels.TotalCount / pageSize).ToString();
                pgLevels.DrawControl();
            }
            else
            {
                lblNoFameLevels.Visible = true;
            }

            lblErrMessage.Visible = false;
        }

        #endregion


        #region Event Handlers

        protected void pgLevels_PageChange(object sender, PageChangeEventArgs e)
        {
            BindData(e.PageNumber);
        }

        /// <summary>
        /// BindMemberFameData
        /// </summary>
        protected void dgFameLevels_Sorting(Object sender, GridViewSortEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            BindData(1);
        }

        #endregion


        #region Properties

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "level_number";
            }
        }

        /// <summary>
        /// DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }

        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

    }
}