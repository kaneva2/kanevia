using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class Items : BaseUserControl
    {

        #region Declarations

        protected HtmlTable tblNoCSR;
        protected Panel pnlCSR;

        protected Label lblPrice, lblPrice2, lblType;

        protected PlaceHolder phBreadCrumb;

        // Delete asset
        protected Button btnDeleteAsset;
        protected HtmlInputHidden hidAssetId;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.USER_DATA_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                if (!IsPostBack)
                {
                    // Log the CSR activity
                    GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Viewing all items", 0, 0);
                }

                BindData(1, "");

                // Add the javascript for deleting assets
                string scriptString = "<script language=\"javaScript\">\n<!--\n function DeleteAsset (id) {\n";
                scriptString += "   document.all." + hidAssetId.ClientID + ".value = id;\n";
                scriptString += "	" + Page.ClientScript.GetPostBackEventReference(btnDeleteAsset, "", false) + ";\n";
                scriptString += "}\n// -->\n";
                scriptString += "</script>";

                if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType(), "DeleteAsset"))
                {
                    Page.ClientScript.RegisterClientScriptBlock(GetType(), "DeleteAsset", scriptString);
                }

            }
            else
            {
                RedirectToHomePage();
            }
        }
        #endregion

        #region Event Handlers

        /// <summary>
        /// Delete an asset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDeleteAsset_Click(Object sender, EventArgs e)
        {
            StoreUtility.DeleteAsset(Convert.ToInt32(hidAssetId.Value), GetUserId());
            pg_PageChange(this, new PageChangeEventArgs(pgTop.CurrentPageNumber));
        }

        /// <summary>
        /// pg_PageChange
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            BindData(e.PageNumber, filStore.CurrentFilter);
        }

        /// <summary>
        /// filStore_FilterChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void filStore_FilterChanged(object sender, FilterChangedEventArgs e)
        {
            pgTop.CurrentPageNumber = 1;
            BindData(1, e.Filter);
        }

        protected void listAsset_Sorting(object sender, DataGridSortCommandEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            BindData(pgTop.CurrentPageNumber, filStore.CurrentFilter);
        }

        #endregion

        #region Primary Functions

        /// <summary>
        /// BindData
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="filter"></param>
        private void BindData(int pageNumber, string filter)
        {

            // Set the sortable columns
            string orderby = CurrentSort + " " + CurrentSortOrder;

            PagedDataTable pds = StoreUtility.GetCSRAssets(filter, orderby, pageNumber, filStore.NumberOfPages);
            pgTop.NumberOfPages = Math.Ceiling((double)pds.TotalCount / filStore.NumberOfPages).ToString();
            pgTop.DrawControl();

            listAsset.DataSource = pds;
            listAsset.DataBind();

            // The results
            lblSearch.Text = GetResultsText(pds.TotalCount, pageNumber, filStore.NumberOfPages, pds.Rows.Count);
        }

        #endregion

        #region Helper Functions / Attributes

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "created_date";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "DESC";
            }
        }

        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);

            // Delete asset controls
            btnDeleteAsset = new Button();
            hidAssetId = new HtmlInputHidden();

            btnDeleteAsset.Style.Add("DISPLAY", "none");
            btnDeleteAsset.Click += new EventHandler(btnDeleteAsset_Click);

            this.Controls.Add(btnDeleteAsset);
            this.Controls.Add(hidAssetId);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
            filStore.FilterChanged += new FilterChangedEventHandler(filStore_FilterChanged);
        }
        #endregion

    }
}