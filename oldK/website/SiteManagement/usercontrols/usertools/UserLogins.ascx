<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="UserLogins.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.UserLogins" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="980">
        <tr>
            <td style="padding-left:40px; text-align:center; font-size:22px">
                USER LOGINS
            </td>
        </tr>
        <tr>
            <td style="padding-left:40px">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
	                <tr style="line-height:15px;">
	                    <td align="left" style="width:300px" class="belowFilter2" valign="middle" >
	                        <asp:Label runat="server" id="lblSelectedUser" />
	                    </td>
		                <td align="right" class="belowFilter2" valign="middle">
			                <asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop" /><br>
		                </td>						
	                </tr>
                </table>
             </td>
         </tr>
         <tr>
            <td style="padding-left:40px">
                <asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" OnSortCommand="UserLogin_Sorting" Width="100%" id="dgrdLogins" cellpadding="2" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 1px solid #cccccc; font-family:arial">  
	                <HeaderStyle BackColor="#cccccc" ForeColor="#000000" Font-Underline="false" Font-Bold="true" HorizontalAlign="Left" />
	                <ItemStyle BackColor="#ffffff" HorizontalAlign="Left" />
	                <AlternatingItemStyle BackColor="#eeeeee" HorizontalAlign="Left" />
	                <Columns>				
		                <asp:TemplateColumn HeaderText="Login Date" SortExpression="created_date" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# DataBinder.Eval (Container.DataItem, "created_date") %> 
			                </ItemTemplate>
		                </asp:TemplateColumn>

                        <asp:TemplateColumn HeaderText="Action" SortExpression="action_type" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# DataBinder.Eval(Container.DataItem, "action_type").ToString()%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                
		                <asp:TemplateColumn HeaderText="Logout Date" SortExpression="logout_date" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# DataBinder.Eval(Container.DataItem, "logout_date").ToString()%>
			                </ItemTemplate>
		                </asp:TemplateColumn>

		                <asp:TemplateColumn HeaderText="Time Played" SortExpression="time_played_in_min" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# DataBinder.Eval(Container.DataItem, "time_played_in_min")%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
	                </Columns>
                </asp:datagrid>
            </td>
         </tr>
    </table>
</asp:Panel>
