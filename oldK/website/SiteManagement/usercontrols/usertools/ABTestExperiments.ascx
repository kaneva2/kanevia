﻿<%@ Control language="C#" autoeventwireup="true" codebehind="ABTestExperiments.ascx.cs" inherits="KlausEnt.KEP.SiteManagement.ABTestExperiments" %>
<%@ Register tagprefix="Kaneva" tagname="Pager" src="../Pager.ascx" %>

<asp:ScriptManager id="smKWAS" runat="server" enablepartialrendering="true"></asp:ScriptManager>

<link href="css/siteManagement.css" type="text/css" rel="stylesheet">
<link href="css/kaneva/ABTestExperiments.css?v=3" type="text/css" rel="stylesheet">

<asp:UpdatePanel id="udpExperiments" runat="server" rendermode="Block" updatemode="Conditional">
    <Triggers>
        <asp:AsyncPostBackTrigger controlid="btnDeleteExperiment" />
        <asp:AsyncPostBackTrigger controlid="btnUpdateExperiment" />
    </Triggers>
    <ContentTemplate>
        <header>
            <h1 class="title">A/B Test Experiments</h1>
            <div class="actions">
                <label for="ddlExperimentStatus">Filter:</label>
                <asp:DropDownList id="ddlSearchStatus" class="statusInput" runat="server" width="80px" clientidmode="Static" onselectedindexchanged="ddlSearchStatus_OnSelectedIndexChanged" autopostback="true">
                    <asp:ListItem text="" value=""></asp:ListItem>
                    <asp:ListItem text="Created" value="C"></asp:ListItem>
                    <asp:ListItem text="Ready" value="R"></asp:ListItem>
                    <asp:ListItem text="Started" value="S"></asp:ListItem>
                    <asp:ListItem text="Ended" value="E"></asp:ListItem>
                    <asp:ListItem text="Archived" value="A"></asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox id="txtSearch" runat="server"></asp:TextBox>
                <asp:Button id="btnSearch" runat="server" onclick="btnSearch_Click" text="Search" causesvalidation="false" />
                <asp:Button id="btnNewExperiment" runat="server" onclick="btnNewExperiment_Click" text="Add Experiment" causesvalidation="false" />
                <br />
                <asp:RadioButtonList id="rblExactName" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" ClientIDMode="Static">
                    <asp:ListItem Text="Partial" Value="Partial" Selected="True" />
                    <asp:ListItem Text="Exact" Value="Exact" />
                </asp:RadioButtonList>
                <div id="divMsgTop" runat="server" class="error" style="display: none;"></div>
            </div>
        </header>
        <div class="tabs">
            <asp:Repeater id="rptExperimentCategories" runat="server" onitemdatabound="rptExperimentCategories_ItemDataBound">
                <ItemTemplate>
                    <asp:LinkButton id="lbCategoryTab" runat="server" commandargument='<%# (DataBinder.Eval(Container.DataItem, "Name")) %>' oncommand="lbCategoryTab_Click" 
                        text='<%# (DataBinder.Eval(Container.DataItem, "Name")) %>' causesvalidation="false" />
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="tab-content-wrapper">
            <%-- Experiment Listings --%>
            <div id="divExperimentListing" runat="server" visible="false">
                <asp:Repeater id="rptExperiments" runat="server" onitemdatabound="rptExperiments_ItemDataBound" >
                    <ItemTemplate>
                        <div class="experiment-listing">
                            <div class="experiment-info row-data">
                                <div class="name"><asp:LinkButton id="lbEditExperiment" runat="server" commandargument='<%# (DataBinder.Eval(Container.DataItem, "ExperimentId")) %>' text="" oncommand="btnExperimentDetails_Click" causesvalidation="false"><%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></asp:LinkButton></div>
                                <div style="margin-left:12px;"><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "Description").ToString(), 200) %></div>
                                <div style="margin:0 0 0 12px;padding:0;height:auto;">
                                    <asp:Label runat="server" ID="lblUser"></asp:Label>
                                    <asp:DropDownList id="ddlGroupInlineInput" DataTextField="Name" DataValueField="GroupId" 
                                        cssclass="groupInput" runat="server" width="300px" AutoPostBack="true" 
                                        OnSelectedIndexChanged="ddlGroupInlineInput_SelectedIndexChanged" CausesValidation="false" />
                                    <asp:HiddenField id="hfExperimentId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ExperimentId") %>' />
                                    <div class="winner" id="spnWinner" runat="server" visible="false"><%# TruncateWithEllipsis(GetWinningGroupName((Kaneva.BusinessLayer.BusinessObjects.Experiment)Container.DataItem), 150) %></div>
                                    <span id="spnMsgInline" runat="server" class="error" style="display: none;"></span>
                                </div>
                            </div>
                            <div class="experiment-details row-data">
                                <p><span class='<%# GetStatusCSS(DataBinder.Eval(Container.DataItem, "Status").ToString()) %>'><%# Kaneva.BusinessLayer.BusinessObjects.EXPERIMENT_STATUS_TYPE.ConvertFromDBStatusToFullStatus(DataBinder.Eval(Container.DataItem, "Status").ToString()) %></span></p>
                            </div>
                        </div>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <div class="experiment-listing alternating">
                            <div class="experiment-info row-data">
                                <div class="name"><asp:LinkButton id="lbEditExperiment" runat="server" commandargument='<%# (DataBinder.Eval(Container.DataItem, "ExperimentId")) %>' text="" oncommand="btnExperimentDetails_Click" causesvalidation="false"><%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></asp:LinkButton></div>
                                <div style="margin-left:12px;"><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "Description").ToString(), 200) %></div>
                                <div style="margin:0 0 0 12px;padding:0;height:auto;">
                                    <asp:Label runat="server" ID="lblUser"></asp:Label>
                                    <asp:DropDownList id="ddlGroupInlineInput" DataTextField="Name" DataValueField="GroupId" 
                                        cssclass="groupInput" runat="server" width="300px" AutoPostBack="true" 
                                        OnSelectedIndexChanged="ddlGroupInlineInput_SelectedIndexChanged" CausesValidation="false" />
                                    <asp:HiddenField id="hfExperimentId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ExperimentId") %>' />
                                    <div class="winner" id="spnWinner" runat="server" visible="false"><%# TruncateWithEllipsis(GetWinningGroupName((Kaneva.BusinessLayer.BusinessObjects.Experiment)Container.DataItem), 150) %></div>
                                    <span id="spnMsgInline" runat="server" class="error" style="display: none;"></span>
                                </div>
                            </div>
                            <div class="experiment-details row-data">
                                <p><span class='<%# GetStatusCSS(DataBinder.Eval(Container.DataItem, "Status").ToString()) %>'><%# Kaneva.BusinessLayer.BusinessObjects.EXPERIMENT_STATUS_TYPE.ConvertFromDBStatusToFullStatus(DataBinder.Eval(Container.DataItem, "Status").ToString()) %></span></p>
                            </div>
                        </div>
                    </AlternatingItemTemplate>
                </asp:Repeater>
                <div class="resultcontainer">
                    <div class="results">
                        <asp:Label runat="server" id="lblSearch" />
                    </div>
                    <div class="pager">
                        <Kaneva:Pager runat="server" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
                    </div>
                </div>
                 <p class="clear"></p>
           </div>
            <%-- Experiment Details --%>
            <div id="divExperimentDetails" runat="server" visible="false" clientidmode="Static" class="tab-content">
                <asp:LinkButton id="lbBackToListing" runat="server" text="Back to List" onclick="lbBackToListing_Click" cssclass="breadcrumb" causesvalidation="false" />
                <asp:UpdatePanel id="udpExperimentDetails" runat="server" rendermode="Block" updatemode="Conditional" clientidmode="Static" class="form-wrapper-left">
                    <ContentTemplate>
                        <h2 class="form-title">Edit Experiment</h2>
                        <div id="divMsgExperimentDetails" runat="server" class="error" style="display: none;"></div>
                        <div class="form-row">
                            <label for="txtExperimentId">Experiment Id:</label>
                            <asp:TextBox id="txtExperimentId" runat="server" maxlength="200" clientidmode="Static"></asp:TextBox>
                        </div>
                        <div class="form-row">
                            <label for="txtExperimentName">Name:</label>
                            <asp:TextBox id="txtExperimentName" runat="server" maxlength="200" clientidmode="Static"></asp:TextBox>
                            <asp:RequiredFieldValidator id="rfExperimentName" runat="server" controltovalidate="txtExperimentName" errormessage="*" />
                        </div>
                        <div class="form-row">
                            <label for="txtExperimentDescription">Description:</label>
                            <asp:TextBox id="txtExperimentDescription" textmode="multiline" columns="34" runat="server" maxlength="1000" clientidmode="Static"></asp:TextBox>
                        </div>
                        <div class="form-row">
                            <label for="ddlAssignmentTarget">Assignment Target:</label>
                            <asp:DropDownList id="ddlAssignmentTarget" class="assignmentTargetInput" runat="server" width="200px" clientidmode="Static">
                                <asp:ListItem text="Users" value="Users"></asp:ListItem>
                                <asp:ListItem text="Worlds" value="Worlds"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-row">
                            <label for="ddlExperimentCategory">Category:</label>
                            <asp:DropDownList id="ddlExperimentCategory" class="categoryInput" runat="server" width="200px" clientidmode="Static" />
                        </div>
                        <div class="form-row">
                            <label for="ddlAssignGroupsOn">Assign Groups On:</label>
                            <asp:DropDownList id="ddlAssignGroupsOn" class="assignGroupsOnInput" runat="server" width="110px" clientidmode="Static">
                                <asp:ListItem text="CreationOnly" value="CreationOnly"></asp:ListItem>
                                <asp:ListItem text="Request" value="Request"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-row">
                            <label for="ddlExperimentStatus">Status:</label>
                            <asp:DropDownList id="ddlExperimentStatus" class="statusInput" runat="server" width="80px" clientidmode="Static">
                                <asp:ListItem text="Created" value="C"></asp:ListItem>
                                <asp:ListItem text="Ready" value="R"></asp:ListItem>
                                <asp:ListItem text="Started" value="S"></asp:ListItem>
                                <asp:ListItem text="Ended" value="E"></asp:ListItem>
                                <asp:ListItem text="Archived" value="A"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-row">
                            <label for="ddlExperimentStatus">JIRA Cleanup Issue:</label>
                            <asp:TextBox id="txtCleanupJIRA" runat="server" maxlength="13" clientidmode="Static"></asp:TextBox>
                        </div>
                        <div class="form-row">
                            <label for="ddlWinningGroup">Winning Group:</label>
                            <asp:DropDownList id="ddlWinningGroup" class="groupInput" runat="server" width="120px" clientidmode="Static" />
                        </div>
                        <div id="loadingSpinner" class="loader" style="display:none;">Saving...</div>
                        <asp:Button id="btnEditExperiment" runat="server" causesvalidation="false" onclick="btnEditExperiment_Click" text="Edit" clientidmode="Static" />
                        <asp:Button id="btnDeleteExperiment" runat="server" causesvalidation="false" onclick="btnDeleteExperiment_Click" text="Delete" OnClientClick="if(!confirm('Are you sure you want to delete the experiment?')) return false;" />
                        <asp:Button id="btnUpdateExperiment" clientidmode="Static" runat="server" onclick="btnUpdateExperiment_Click" OnClientClick="document.getElementById('loadingSpinner').style.display = 'inline-block';document.getElementById('btnUpdateExperiment').style.display = 'none'" text="Save" />&nbsp;&nbsp;
                        <asp:Button id="btnCancelExperiment" runat="Server" causesvalidation="False" onclick="btnCancelExperimentEdit_Click" text="Cancel" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel id="udpGroupDetails" runat="server" rendermode="Block" updatemode="Conditional" clientidmode="Static">
                    <ContentTemplate>
                        <div id="divEditPercentages" runat="server" clientidmode="Static" visible="false">
                            <h2 class="form-title">Edit Assignment Percentages</h2>
                            <div id="divMsgPercentages" runat="server" class="error" style="display: none;"></div>
                            <asp:Repeater id="rptPercentages" runat="server">
                                <ItemTemplate>
                                    <div class="form-row">
                                        <asp:Label id="lblGroupName" runat="server">(<%# (DataBinder.Eval(Container.DataItem, "Label")) %>):</asp:Label>
                                        <asp:HiddenField id="hfGroupId" runat="server" value='<%# (DataBinder.Eval(Container.DataItem, "GroupId")) %>' />
                                        <asp:TextBox id="txtAssignmentPercentage" runat="server" maxlength="10" text='<%# (DataBinder.Eval(Container.DataItem, "AssignmentPercentage")) %>' width="26px"></asp:TextBox>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:Button id="btnUpdatePercentages" runat="Server" onclick="btnUpdatePercentages_Click" text="Save" />&nbsp;&nbsp;
                            <asp:Button id="btnCancelPercentages" runat="Server" causesvalidation="False" onclick="btnCancelPercentages_Click" text="Cancel" />
                        </div>
                        <div id="divGroupListing" runat="server" clientidmode="Static" visible="false">
                            <h2 class="form-title">Experiment Groups</h2>
                            <div id="divMsgGroupListing" runat="server" class="error" style="display: none;"></div>
                            <asp:Repeater id="rptExperimentGroups" runat="server">
                                <HeaderTemplate>
                                    <ul>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <span>(<%# (DataBinder.Eval(Container.DataItem, "Label")) %>) <%# (DataBinder.Eval(Container.DataItem, "Name")) %></span>
                                        <asp:Button id="lbEditGroup" runat="server" text="Edit Group" oncommand="lbEditExperimentGroup_Click" commandargument='<%# (DataBinder.Eval(Container.DataItem, "GroupId")) %>' causesvalidation="false" />
                                        <asp:Button id="btnDeleteGroup" runat="server" causesvalidation="false" oncommand="btnDeleteGroup_Click" commandargument='<%# (DataBinder.Eval(Container.DataItem, "GroupId")) %>' text="Delete" 
                                            OnClientClick="if(!confirm('Are you sure you want to delete the selected group?')) return false;" />
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:Button id="btnNewExperimentGroup" runat="server" onclick="btnNewExperimentGroup_Click" causesvalidation="false" text="Add Group" />
                            <asp:Button id="btnEditPercentages" runat="server" onclick="btnEditPercentages_Click" causesvalidation="false" text="Edit Percentages" />
                            <div id="divGroupURLs" runat="server" clientidmode="static">
                                <h2 class="form-title">Assignment URLs</h2>
                                <asp:repeater id="rptExperimentGroupURLs" runat="server">
                                    <HeaderTemplate>
                                        <ul>
                                    </HeaderTemplate>
                                    <itemtemplate>
                                        <li>
                                            Group (<%# (DataBinder.Eval(Container.DataItem, "Label"))%>):  http://<%# KlausEnt.KEP.SiteManagement.SiteManagementCommonFunctions.GetKanevaURL %>/register/kaneva/registerInfo.aspx?ABExpID=<%# (DataBinder.Eval(Container.DataItem, "ExperimentId"))%>&ABExpGrpID=<%# (DataBinder.Eval(Container.DataItem, "GroupId"))%>
                                        </li>
                                    </itemtemplate>
                                    <FooterTemplate>
                                        </ul>
                                    </FooterTemplate>
                                </asp:repeater>
                            </div>
                        </div>
                        <div id="divGroupEdit" runat="server" clientidmode="Static" visible="false">
                            <h2 class="form-title">Edit Group</h2>
                            <div id="divMsgGroupEdit" runat="server" class="error" style="display: none;"></div>
                            <div class="form-row">
                                <label for="txtGroupId">Group Id:</label>
                                <asp:TextBox id="txtGroupId" runat="server" maxlength="200" clientidmode="Static"></asp:TextBox>
                            </div>
                            <div class="form-row">
                                <label for="txtGroupName">Name:</label>
                                <asp:TextBox id="txtGroupName" runat="server" maxlength="200" clientidmode="Static"></asp:TextBox>
                                <asp:RequiredFieldValidator id="rfGroupName" runat="server" controltovalidate="txtGroupName" errormessage="*" />
                            </div>
                            <div class="form-row">
                                <label for="txtGroupDescription">Description:</label>
                                <asp:TextBox id="txtGroupDescription" runat="server" textmode="MultiLine" columns="34" maxlength="1000" clientidmode="Static"></asp:TextBox>
                            </div>
                            <div class="form-row">
                                <label for="ddlGroupLabel">Label:</label>
                                <asp:DropDownList id="ddlGroupLabel" runat="server" clientidmode="Static">
                                    <asp:ListItem text="" value=""></asp:ListItem>
                                    <asp:ListItem text="A" value="A"></asp:ListItem>
                                    <asp:ListItem text="B" value="B"></asp:ListItem>
                                    <asp:ListItem text="C" value="C"></asp:ListItem>
                                    <asp:ListItem text="D" value="D"></asp:ListItem>
                                    <asp:ListItem text="E" value="E"></asp:ListItem>
                                    <asp:ListItem text="F" value="F"></asp:ListItem>
                                    <asp:ListItem text="G" value="G"></asp:ListItem>
                                    <asp:ListItem text="H" value="H"></asp:ListItem>
                                    <asp:ListItem text="I" value="I"></asp:ListItem>
                                    <asp:ListItem text="J" value="J"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator id="rfGroupLabel" runat="server" controltovalidate="ddlGroupLabel" errormessage="*" />
                            </div>
                            <div class="form-row">
                                <label for="ddlGroupEffect">Effect:</label>
                                <asp:DropDownList id="ddlGroupEffect" runat="server" width="200px" clientidmode="Static" />
                            </div>
                            <asp:Button id="btnUpdateGroup" runat="Server" onclick="btnUpdateExperimentGroup_Click" text="Save" />&nbsp;&nbsp;
                            <asp:Button id="btnCancelGroup" runat="Server" causesvalidation="False" onclick="btnCancelExperimentGroupEdit_Click" text="Cancel" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
    