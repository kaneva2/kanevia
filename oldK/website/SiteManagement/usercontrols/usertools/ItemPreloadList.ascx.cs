using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;
using System.Drawing;
using MagicAjax;
using System.IO;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Diagnostics;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class ItemPreloadList : BaseUserControl
    {
        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region PageLoad

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.STAR_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    default:
                        RedirectToHomePage();
                        break;
                }

                if (!IsPostBack)
                {
                    CurrentSort = "global_id";

                    // Populate lists
                    PopulateAllLists(-1);
                }
            }
            else
            {
                RedirectToHomePage();
            }
        }

        #endregion PageLoad

        #region Primary Functions

        private void PopulateAllLists(int selectedListId)
        {
            var dt = GetShoppingFacade().GetAllItemPreloadLists();
            dt.Columns.Add("FormattedName", typeof(string), "name + ' (' + IIF(ver>=0, 'version: ' + ver, 'Unpublished') + ')'");

            ddlAllLists.DataSource = dt;
            ddlAllLists.DataTextField = "FormattedName";
            ddlAllLists.DataValueField = "list_id";
            ddlAllLists.DataBind();
            ddlAllLists.Items.Insert(0, new ListItem("-- Select a List --", "-1"));

            ddlAllLists.SelectedValue = selectedListId.ToString();
            ddlAllLists_SelectedIndexChanged(null, null);

            ddlMergeList.DataSource = dt;
            ddlMergeList.DataTextField = "FormattedName";
            ddlMergeList.DataValueField = "list_id";
            ddlMergeList.DataBind();
            ddlMergeList.Items.Insert(0, new ListItem("-- Select a List to Merge --", "-1"));
        }

        private void PopulateListItems()
        {
            int listId = Convert.ToInt32(ddlAllLists.SelectedValue);
            Debug.Assert(listId > 0);

            DataTable dt = null;
            if (listId > 0)
            {
                dt = GetShoppingFacade().GetItemPreloadListItems(listId);
            }

            if (dt != null && dt.Rows.Count > 0)
            {
                // Set page index and sort order
                dgListItems.PageIndex = CurrentPage;
                dt.DefaultView.Sort = CurrentSort + " " + CurrentSortOrder;

                // Bind Data
                dgListItems.DataSource = dt;
                dgListItems.DataBind();
            }
            else
            {
                dgListItems.DataSource = null;
                dgListItems.DataBind();
            }

        }

        protected string GetUserName(int userID)
        {
            string userName = "N/A";

            try
            {
                if (userID == 0)
                {
                    userName = "Kaneva";
                }
                else if (userID > 0)
                {
                    userName = UsersUtility.GetUserName(userID);
                }
            }
            catch (Exception ex)
            {
                m_logger.Error("ItemPreloadList.aspx: error trying to get username from user id ", ex);
            }

            return userName;
        }

        protected void HideEditListPanel()
        {
            pnlEditList.Visible = false;
            lblEditListTitle.Text = "";
            hidListId.Value = "";
            txtListName.Text = "";
            hidOldListName.Value = "";
        }

        protected void ShowEditListPanel(string title)
        {
            lblEditListTitle.Text = title;
            hidListId.Value = "";
            txtListName.Text = "";
            hidOldListName.Value = "";
            pnlEditList.Visible = true;
        }

        #endregion Primary Functions


        #region Properties

        private int CurrentPage
        {
            get
            {
                if (ViewState["_currentPage"] == null)
                {
                    ViewState["_currentPage"] = 0;
                }
                return (int)ViewState["_currentPage"];
            }
            set
            {
                ViewState["_currentPage"] = value;
            }
        }

        #endregion Properties


        #region Event Handlers

        protected void ddlAllLists_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblEditListMessage.Text = "";
            HideEditListPanel();
            CurrentPage = 0;

            int listId = Convert.ToInt32(ddlAllLists.SelectedValue);
            if (listId < 0)
            {
                btnEditList.Enabled = false;
                btnEditList.Visible = false;
                btnPublishList.Enabled = false;
                btnPublishList.Visible = false;
                lblListTimestamp.Text = "";
                pnlListItemMaint.Visible = false;
            }
            else
            {
                // Load Existing
                Debug.Assert(listId != 0);

                var row = GetShoppingFacade().GetItemPreloadList(listId);
                Debug.Assert(row != null);

                bool unpublished = Convert.ToInt32(row["ver"]) == -1;

                btnEditList.Enabled = true;
                btnEditList.Visible = true;
                btnPublishList.Enabled = unpublished;
                btnPublishList.Visible = unpublished;
                lblListTimestamp.Text = "Created: " + Convert.ToString(row["created_date"]);
                if (unpublished)
                {
                    lblListTimestamp.Text += ", Unpublished";
                }
                else
                {
                    lblListTimestamp.Text += ", Published: " + Convert.ToString(row["published_date"]);
                }
                pnlListItemMaint.Visible = true;
            }

            PopulateListItems();
        }

        protected void btnCreateList_Click(object sender, EventArgs e)
        {
            ddlAllLists.SelectedValue = "-1";
            ddlAllLists_SelectedIndexChanged(null, null);

            ShowEditListPanel("Create a New List");
        }

        protected void btnEditList_Click(object sender, EventArgs e)
        {
            int listId = Convert.ToInt32(ddlAllLists.SelectedValue);
            Debug.Assert(listId > 0);

            if (listId > 0)
            {
                // Update
                var row = GetShoppingFacade().GetItemPreloadList(listId);
                Debug.Assert(row != null);

                ShowEditListPanel("Edit List");
                hidListId.Value = Convert.ToString(row["list_id"]);
                txtListName.Text = Convert.ToString(row["name"]);
                hidOldListName.Value = txtListName.Text;
            }
        }
        
        protected void btnPublishList_Click(object sender, EventArgs e)
        {
            int listId = Convert.ToInt32(ddlAllLists.SelectedValue);
            Debug.Assert(listId > 0);

            if (listId > 0)
            {
                var row = GetShoppingFacade().GetItemPreloadList(listId);
                Debug.Assert(row != null);

                int ver = Convert.ToInt32(row["ver"]);
                Debug.Assert(ver < 0);

                if (ver < 0)
                {
                    int newVer = GetShoppingFacade().PublishItemPreloadList(listId);
                    Debug.Assert(newVer > 0);

                    lblEditListMessage.Text = newVer <= 0 ? "Publishing failed" : "Publishing succeeded (version = " + newVer.ToString() + ")";
                }
                else
                {
                    lblEditListMessage.Text = "Already published (version = " + ver.ToString() + ")";
                }

                PopulateAllLists(listId);
            }
        }

        protected void btnSaveList_Click(object sender, EventArgs e)
        {
            lblEditListMessage.Text = "";

            if (!Page.IsValid)
            {
                return;
            }

            if (txtListName.Text == hidOldListName.Value)
            {
                lblEditListMessage.Text = "List name not changed";
                return;
            }

            if (hidListId.Value == "")
            {
                // Add New
                int newListId = GetShoppingFacade().CreateItemPreloadList(txtListName.Text);
                if (newListId <= 0)
                {
                    lblEditListMessage.Text = "List creation failed";
                }
                else
                {
                    lblEditListMessage.Text = "List created";

                    PopulateAllLists(newListId);
                }
            }
            else
            {
                // Update
                int listId = Convert.ToInt32(hidListId.Value);
                if (!GetShoppingFacade().UpdateItemPreloadList(listId, txtListName.Text))
                {
                    lblEditListMessage.Text = "Update failed";
                }
                else
                {
                    lblEditListMessage.Text = "Update succeeded";

                    PopulateAllLists(listId);
                }
            }
        }

        protected void btnAddItem_Click(object sender, EventArgs e)
        {
            int listId = Convert.ToInt32(ddlAllLists.SelectedValue);
            Debug.Assert(listId > 0);

            if (listId > 0)
            {
                try
                {
                    int globalId = Convert.ToInt32(txtItemGlobalId.Text);
                    GetShoppingFacade().AddItemPreloadListItem(listId, globalId);
                    lblEditListMessage.Text = "Added item " + globalId.ToString() + " to list";
                }
                catch (Exception ex)
                {
                    lblEditListMessage.Text = "Adding item failed: " + ex.Message;
                }

                PopulateListItems();

            }
        }

        protected void ddlMergeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnMergeList.Enabled = ddlMergeList.SelectedValue != "-1" && ddlMergeList.SelectedValue != ddlAllLists.SelectedValue;
        }

        protected void btnMergeList_Click(object sender, EventArgs e)
        {
            int listId = Convert.ToInt32(ddlAllLists.SelectedValue);
            Debug.Assert(listId > 0);

            int mergeListId = Convert.ToInt32(ddlMergeList.SelectedValue);
            Debug.Assert(mergeListId > 0);

            if (listId > 0 && mergeListId > -1)
            {
                int rowsAffected = GetShoppingFacade().MergeItemPreloadListItems(listId, mergeListId);
                lblEditListMessage.Text = "Merged " + rowsAffected.ToString() + " item(s)";
                PopulateListItems();
            }
        }

        protected void btnDeleteItem_Click(object sender, CommandEventArgs e)
        {
            int listId = Convert.ToInt32(ddlAllLists.SelectedValue);
            Debug.Assert(listId > 0);

            if (listId > 0)
            {
                try
                {
                    int globalId = Convert.ToInt32(e.CommandArgument);
                    GetShoppingFacade().RemoveItemPreloadListItem(listId, globalId);
                    lblEditListMessage.Text = "Deleted item " + globalId.ToString() + " from list";
                }
                catch (Exception ex)
                {
                    lblEditListMessage.Text = "Deleting item failed: " + ex.Message;
                }

                PopulateListItems();
            }
        }

        protected void dgListItems_DataBound(object sender, EventArgs e)
        {
            lblListEmpty.Visible = dgListItems.Rows.Count == 0;
        }

        protected void dgListItems_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
        }

        protected void dgListItems_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            CurrentPage = e.NewPageIndex;
            PopulateListItems();
        }

        protected void dgListItems_Sorting(Object sender, GridViewSortEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            PopulateListItems();
        }

        #endregion Event Handlers


        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

    }
}
