using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class UserSearch : BaseUserControl
    {
        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.USER_DATA_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }
                string test = txtUsername.Text;
                if (!IsPostBack)
                {
                    drpStatus.DataTextField = "name";
                    drpStatus.DataValueField = "status_id";
                    drpStatus.DataSource = UsersUtility.GetUserStatus();
                    drpStatus.DataBind();
                    drpStatus.Items.Insert(0, new ListItem("All Status", "0"));
                    drpStatus.SelectedValue = "0";
                }

            }
            else
            {
                RedirectToHomePage();
            }
        }

        #endregion

        /// <summary>
        /// Pager change event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            BindData(e.PageNumber, filMembers.CurrentFilter);
        }

        /// <summary>
        ///  Bind the data
        /// </summary>
        /// <param name="pageNumber"></param>
        private void BindData(int pageNumber, string filter)
        {
            string orderby = CurrentSort + " " + CurrentSortOrder;

            // Log the CSR activity
            this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Searching users. Username = " + Server.HtmlEncode(txtUsername.Text) + ", Email = " + Server.HtmlEncode(txtEmail.Text) + ", First Name = " + Server.HtmlEncode(txtFirstName.Text) + ", Last Name = " + Server.HtmlEncode(txtLastName.Text), 0, 0);

            PagedDataTable pds = UsersUtility.GetUsers(Server.HtmlEncode(txtUsername.Text), Server.HtmlEncode(txtEmail.Text), Server.HtmlEncode(txtFirstName.Text), Server.HtmlEncode(txtLastName.Text), Convert.ToInt32(drpStatus.SelectedValue), filter, orderby, pageNumber, filMembers.ItemsPerPages);
            pgTop.NumberOfPages = Math.Ceiling((double)pds.TotalCount / filMembers.ItemsPerPages).ToString();
            pgTop.DrawControl();

            dgrdMembers.DataSource = pds;
            dgrdMembers.DataBind();

            // The results
            lblSearch.Text = GetResultsText(pds.TotalCount, pageNumber, filMembers.ItemsPerPages, pds.Rows.Count);
        }

        /// <summary>
        /// filMembers_FilterChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void filMembers_FilterChanged(object sender, FilterChangedEventArgs e)
        {
            pgTop.CurrentPageNumber = 1;
            BindData(1, e.Filter);
        }

        /// <summary>
        /// They clicked to sort a column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void btnSort_Click(object sender, System.EventArgs e)
        {
            base.btnSort_Click(sender, e);
            BindData(pgTop.CurrentPageNumber, filMembers.CurrentFilter);
        }

        protected void User_Sorting(object sender, DataGridSortCommandEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            BindData(pgTop.CurrentPageNumber, filMembers.CurrentFilter);
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "user_id";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "desc";
            }
        }

        /// <summary>
        /// Search Event Handler
        /// </summary>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData(pgTop.CurrentPageNumber, filMembers.CurrentFilter);
        }

        /// <summary>
        /// Button purchase the item whos button was clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SelectUser_Command(Object sender, CommandEventArgs e)
        {
            //get the selected id from the button of the user clicked add to session
            Session["SelectedUserId"] = e.CommandArgument.ToString();

            string redirectURL = GetHomePageURL(GetUserSearchDefaultPage());

            //redirect the page back to where ever it came from
            if (Session["CallingPage"] != null)
            {
                string currentURL = Request.Url.ToString();
                string returnURL = Session["CallingPage"].ToString();
                if (currentURL != returnURL)
                {
                    redirectURL = returnURL;
                }
            }
            Response.Redirect(redirectURL);
        }


        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
            filMembers.FilterChanged += new FilterChangedEventHandler(filMembers_FilterChanged);
        }
        #endregion

    }
}