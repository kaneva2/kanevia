﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DeveloperAuthorizations.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.DeveloperAuthorizations" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

    <asp:label style="position:relative; font-size:18px; padding: 10px 0 10px 0" runat="server" id="messages" visible="true" />
    <table border="0" cellpadding="0" cellspacing="0" width="980px" style="padding-top:20px">
        <tr>
            <td style="width:980px; vertical-align:top; padding-left:10px">
                <h1>Developer Authorizations</h1>
            </td>
        </tr>
        <tr>
            <td style="width:980px; vertical-align:top; padding-left:10px">
                <table border="0" cellpadding="4" cellspacing="0" width="970px">
                    <tr>
                        <td >
                            <table border="0" rules="cols" cellpadding="4" cellspacing="0" style="background-color:ActiveBorder">
                                <tr>
                                    <td style="width:120px; padding-left:4px"><span style="font-weight:bold; padding-right:10px">Computer Name:</span></td>
                                    <td><asp:textbox id="txtComputerNameSearch" style="width:200px" maxlength="31" runat="server" text="" /></td>
                                    <td rowspan="2" style="padding-left:20px"><asp:button id="btnSearch" runat="Server" Text="Search" onclick="btnSearch_Click" causesvalidation="False"/></td>
                                </tr>
                            </table><br />
                        </td>                    
                    </tr>
                    <tr>
                        <td style="height:20px"></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:linkbutton id="lbtnDownload" runat="server" onclick="lbtnDownload_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:label id="lblSearch" runat="server" cssclass="insideTextNoBold" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:repeater id="rpt_SystemConfigs" runat="server" >
                                <headertemplate>
                                        <table border="0" cellpadding="4" cellspacing="0" width="100%">
                                            <tr style="background-color:#cccccc">
                                                <th align="left">CPU Count</th>
                                                <th align="left">CPU MHz</th>
                                                <th align="left">System Memory (GB)</th>
                                                <th align="left">GPU</th>
					                        </tr>
                                </headertemplate>
                                <itemtemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label id="lbCPUCount" runat="server" />
                                                </td>
                                                <td>
                                                    <asp:Label id="lbCPUMhz" runat="server" />
                                                </td>
                                                <td>
                                                    <asp:Label id="lbSysMemGB" runat="server" />
                                                    <asp:button id="btnFixSysMem" runat="Server" text="Fix" visible="false" onclick="btnFixSysMem_Click" causesvalidation="False"/>
                                                </td>
                                                <td>
                                                    <asp:Label id="lbGPU" runat="server" />
                                                </td>
                                           </tr>
                                </itemtemplate>
                                <alternatingitemtemplate>
                                            <tr style="background-color:#eeeeee">
                                                <td>
                                                    <asp:Label id="lbCPUCount" runat="server" />
                                                </td>
                                                <td>
                                                    <asp:Label id="lbCPUMhz" runat="server" />
                                                </td>
                                                <td>
                                                    <asp:Label id="lbSysMemGB" runat="server" />
                                                    <asp:button id="btnFixSysMem" runat="Server" text="Fix" visible="false" onclick="btnFixSysMem_Click" causesvalidation="False"/>
                                                </td>
                                                <td>
                                                    <asp:Label id="lbGPU" runat="server" />
                                                </td>
                                            </tr>
                                </alternatingitemtemplate>
                                <footertemplate>
                                    </table>
                                </footertemplate>
                            </asp:repeater>
                         </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:label id="lblSearch2" runat="server" cssclass="insideTextNoBold" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
