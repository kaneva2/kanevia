using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;
using System.Drawing;
using MagicAjax;
using System.IO;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class ManageCatalogItems : BaseUserControl
    {
        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private bool editItemSubmission = false;

        const int CURRENCY_TYPE_CREDITS = 256;
        const int CURRENCY_TYPE_REWARDS = 512;
        const int CURRENCY_TYPE_BOTH_R_C = 768;

        protected Label lblCategory1, lblCategory2, lblCategory3;

        #endregion

        #region PageLoad

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.KANEVA_PROPRIETARY))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        if (UserIsCustomerSupportRep) 
                        {
                            break;                     
                        }
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }
                tbx_DisplayName.Enabled = true;
                if (!IsPostBack)
                {
                    //check for  submission from another page
                    GetRequestParams();

                    //bind the CatalogItems
                    //BindCatalogItemData(currentPage);

                    //bind pulldowns
                    PopulateCurrencyTypeList();
                    PopulateBuyBackPercentages();

                    pnl_CatalogItemsDetails.Visible = false;

                    if (editItemSubmission)
                    {
                        //get the catalog item
                        try
                        {
                            PopulateWOKItemEdit (Convert.ToInt32 (Request["itemId"]));
                        }
                        catch { }
                    }
                }
            }
            else
            {
                RedirectToHomePage();
            }
        }

        #endregion PageLoad

        #region Primary Functions

        /// <summary>
        /// BindStoreList
        /// </summary>
        private void BindStoreList(int itemId)
        {
            DataTable itemsInStore = null;
            DataTable storesDT = null;

            //get all the current WOK stores
            storesDT = WOKStoreUtility.GetAllStores();

            //create the check field column for the get all stores data result
            DataColumn checkColumn = new DataColumn();
            checkColumn.DataType = System.Type.GetType("System.Boolean");
            checkColumn.AllowDBNull = false;
            checkColumn.ColumnName = "cbx_instore";
            checkColumn.DefaultValue = false;
            storesDT.Columns.Add(checkColumn);

            //get all the stores that this item is in
            if (itemId > 0)
            {
                itemsInStore = WOKStoreUtility.GetItemStores(itemId);
            }

            //mark the stores the the item is in if any
            if ((itemsInStore != null) && (itemsInStore.Rows.Count > 0))
            {
                foreach (DataRow dr in itemsInStore.Rows)
                {
                    string storeId = dr["store_id"].ToString();
                    DataRow[] storeItems = storesDT.Select("store_id = " + storeId);
                    if (storeItems.Length > 0)
                    {
                        DataRow storeItem = storeItems[0];
                        storeItem["cbx_instore"] = true;
                        storeItem.AcceptChanges();
                        storesDT.AcceptChanges();
                    }

                }
            }

            dgd_Stores.DataSource = storesDT;
            dgd_Stores.DataBind();
        }

        /// <summary>
        /// PopulateWOKItemEdit
        /// </summary>
        private void PopulateWOKItemEdit(int globalId)
        {
            WOKItem item = GetShoppingFacade ().GetItem (globalId);

            //clear the fields
            ClearCatalogItemData();

            //make the CatalogItems details panel visible
            pnl_CatalogItemsDetails.Visible = true;

            //load the gift sets and stores
            BindStoreList(item.GlobalId);

            // Show the categories
            BindItemCategories(item.GlobalId);

            // Set up the add categories
            SetAddCategoryToTopLevel();

            try
            {
                //populate fields
                //replace is a work around for unexplained &nbsp; showing up figure it out later
                tbx_ItemID.Text = item.GlobalId.ToString(); // gvr.Cells[0].Text.Replace ("&nbsp;", "");
                tbx_ItemName.Text = Server.HtmlDecode (item.Name); // Server.HtmlDecode (gvr.Cells[2].Text.Replace ("&nbsp;", ""));
                tbx_DisplayName.Text = Server.HtmlDecode (item.DisplayName); // Server.HtmlDecode (gvr.Cells[3].Text.Replace ("&nbsp;", ""));
                tbx_Description.Text = Server.HtmlDecode (item.Description); // Server.HtmlDecode (gvr.Cells[4].Text.Replace ("&nbsp;", ""));

                //parse dates
                DateTime itemAddDate = item.DateAdded; // Convert.ToDateTime (gvr.Cells[5].Text.Replace ("&nbsp;", ""));
                txtItemAddedDate.Text = itemAddDate.ToShortDateString ();

                tbx_MarketCost.Text = item.MarketCost.ToString(); // gvr.Cells[6].Text.Replace ("&nbsp;", "");
                tbx_DesignerPrice.Text = item.DesignerPrice.ToString ();
                tbx_SellingPrice.Text = item.SellingPrice.ToString (); // gvr.Cells[7].Text.Replace ("&nbsp;", "");
                tbx_CreatorID.Text = item.ItemCreatorId.ToString (); // gvr.Cells[8].Text.Replace ("&nbsp;", "");
                cbAP.Checked = item.PassTypeId == (int) Constants.ePASS_TYPE.ACCESS;
 
                // If this item is derived, show template item info
                divTemplateItem.Visible = false;
                if (item.BaseGlobalId > 0)
                {
                    divTemplateItem.Visible = true;
                    spnTemplateItemId.InnerText = item.BaseGlobalId.ToString ();

                    txtTemplatePrice.Text = "--";
                    txtTemplateDesignerPrice.Text = item.TemplateDesignerPrice.ToString ();
                }

                //set the buyback pulldown
                int percentage = Convert.ToInt32 (Math.Round ((Convert.ToDecimal (tbx_SellingPrice.Text) / Convert.ToDecimal ((tbx_MarketCost.Text.Equals ("0") ? "1" : tbx_MarketCost.Text))) * 100));
                int remainder = percentage % 5;
                if (remainder < 3)
                {
                    percentage -= remainder;
                }
                else
                {
                    percentage += (5 - remainder);
                }
                ddl_BBPrice.SelectedValue = (percentage.Equals (0) ? "50" : percentage.ToString ());

                //query to get the username for display
                try
                {
                    tbx_CreatorName.Text = GetUserName ((int)item.ItemCreatorId); //Convert.ToInt32 (gvr.Cells[8].Text));
                }
                catch (FormatException fe)
                {
                    m_logger.Error ("Catalog Administration ", fe);
                }

                string currencyType = item.InventoryType.ToString (); // gvr.Cells[9].Text.Replace ("&nbsp;", "");
                ddl_CurrencyType.SelectedValue = (currencyType == "0" ? DEFAULT_CURRENCYTYPE : currencyType);

                itemImage.ImageUrl = StoreUtility.GetItemImageURL (item.ThumbnailAssetdetailsPath, "ad");
                SetDropDownIndex (dgd_drpActive, item.ItemActive.ToString()); //gvr.Cells[10].Text.Replace ("&nbsp;", ""));

                //show display information as summary for user convenience
                lbl_DetailsTitle.Text = "<strong>" + tbx_ItemName.Text.Replace ("&nbsp;", "").Replace ("&quot;", "") + "</strong>" +
                    " <span style=\"font-size:14px;\">(" + tbx_ItemID.Text + ")</span> by " + tbx_CreatorName.Text + " <span style=\"font-size:14px;\">(" + tbx_CreatorID.Text + ")</span>";

                tbx_MarketCost.Enabled = true;
                tbx_DesignerPrice.Enabled = false;
                divBundlePrice.Visible = false;
                tbx_DesignerPrice.AutoPostBack = false;
                trBuyBackPrice.Visible = true;
                trBundleItems.Visible = false;
                if (item.UseType == WOKItem.USE_TYPE_BUNDLE)
                {
                    if (item.BundleItems.Count > 0)
                    {
                        rptBundleItems.DataSource = item.BundleItems;
                        rptBundleItems.DataBind ();
                        tdBundleItemsLabel.InnerText = "Bundle Items:";
                        trBundleItems.Visible = true;
                    }

                    // Get Bundle Price
                    SetPrices (item.BundleItems, (int) item.DesignerPrice, (int) item.ItemCreatorId);
                    divBundlePrice.Visible = true;

                    tbx_MarketCost.Enabled = false;
                    //tbx_DesignerPrice.Enabled = true;
                    //tbx_DesignerPrice.AutoPostBack = true;
                    trBuyBackPrice.Visible = false;
                }
                else   // Check to see if this item belongs to any bundles
                {
                    List<WOKItem> bundles = GetShoppingFacade().GetBundlesItemBelongsTo (item.GlobalId, "");
                    if (bundles.Count > 0)
                    {
                        rptBundleItems.DataSource = bundles;
                        rptBundleItems.DataBind ();
                        tdBundleItemsLabel.InnerText = "Include In These Bundles:";
                        trBundleItems.Visible = true;
                    }
                }

                if (UserIsCustomerSupportRep) { SetAccessForCustomerSupport (); }
            }
            catch (Exception ex)
            {
                ErrorMessages.ForeColor = Color.DarkRed;
                ErrorMessages.Text = "Error displaying details: " + ex.Message;
                if (UserIsCustomerSupportRep) { SetAccessForCustomerSupport (); }
                return;
            }
        }

        private void SetAccessForCustomerSupport ()
        {
            inp_itemImage.Disabled = true;
            browseTHUMB.Disabled = true;
            tbx_ItemName.Enabled = false;
            tbx_DisplayName.Enabled = false;
            tbx_Description.Enabled = false;
            tbx_MarketCost.Enabled = false;
            tbx_DesignerPrice.Enabled = false;
            txtTemplatePrice.Enabled = false;
            txtTemplateDesignerPrice.Enabled = false;
            tbx_SellingPrice.Enabled = false;
            ddl_BBPrice.Enabled = false;
            ddl_CurrencyType.Enabled = false;
            txtItemAddedDate.Enabled = false;
            tbx_CreatorName.Enabled = false;
            tbx_CreatorID.Enabled = false;
            tbx_OwnersSearchFilter.Enabled = false;
            btn_UsersSearch.Enabled = false;
            dgd_Users.Enabled = false;
            btnRemoveFromInventory.Enabled = false;
            AjaxpanelRemoveCat.Enabled = false;
            AjaxpanelAddCat.Enabled = false;
            lbDeleteCategory.Visible = false;
            lbDeleteCategory1.Visible = false;
            lbDeleteCategory2.Visible = false;
            lbDeleteCategory.Enabled = false;
            lbDeleteCategory1.Enabled = false;
            lbDeleteCategory2.Enabled = false;
            btnSaveCategory1.Enabled = false;
            btnSaveCategory2.Enabled = false;
            btnSaveCategory3.Enabled = false;
            btnAddCategory.Enabled = false;
            cblCategories.Enabled = false;
            dgd_Stores.Enabled = false;
        }

        /// <summary>
        /// Gets the user Name associated with the user Id
        /// </summary>
        protected string GetUserName(int userID)
        {
            string userName = "N/A";

            try
            {
                if (userID == 0)
                {
                    userName = "Kaneva";
                }
                else if (userID > 0)
                {
                    userName = UsersUtility.GetUserName(userID);
                }
            }
            catch (Exception ex)
            {
                m_logger.Error("Catalog Administration: error trying to get username from user id ", ex);
            }
            return userName;
        }
    
        /// <summary>
        /// BindCatalogItemsData
        /// </summary>
        private void BindCatalogItemData(int currentPage)
        {
            //query the database for existing CatalogItems
            DataTable dt = WOKStoreUtility.GetWOKItemsCatalog(WOKSearchFilter);

            // Set the sortable columns
            string orderby = CurrentSort + " " + CurrentSortOrder;

            if ((dt != null) && (dt.Rows.Count > 0))
            {
                //set the current page
                this.dg_CatalogItems.PageIndex = currentPage;

                //set the sort order
                dt.DefaultView.Sort = orderby;

                lbl_NoCatalogItems.Visible = false;

                dg_CatalogItems.DataSource = dt;
                dg_CatalogItems.DataBind();
            }
            else
            {
                lbl_NoCatalogItems.Visible = true;
            }

        }

        private void BindUserSearch()
        {
            dgd_Users.PageSize = PageSize();
            dgd_Users.PageIndex = WOKSearchCurrentPage;
            DataTable dt = UsersUtility.GetUsersList(-1, UserSearchFilter);
            dgd_Users.DataSource = dt;
            dgd_Users.DataBind();
        }

        private void TickleServer(int itemToUpdate)
        {
            // Dev note, the below assumes this is a pass subscription change
            //tickle the wok servers
            try
            {
                (new RemoteEventSender()).BroadcastItemChangeEvent(RemoteEventSender.ObjectType.ItemPassList, RemoteEventSender.ChangeType.UpdateObject, itemToUpdate);
            }
            catch (Exception ex)
            {
                m_logger.Error("Error while trying to tickle server", ex);
            }

        }

        private void SetPrices (IList<WOKItem> items, int commission, int bundleOwnerId)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            SetPriceFields(shoppingFacade.CalculateBundlePrice(items, commission, bundleOwnerId));
        }

        private void SetPriceFields (BundlePrice bp)
        {
            totalNotOwnedItems.InnerText = bp.TotalItemsNotOwned.ToString ();
            totalOwnedItems.InnerText = bp.TotalItemsOwnedPlusCommission.ToString ();
            totalDesignCommission.InnerText = bp.DesignCommissionPlusCommission.ToString ();
            totalBundle.InnerText = bp.TotalBundle.ToString ();

            CurrentBundlePrice = bp;
        }

        #region Disabled Mail Code
        // **NOTE** KWAS does not have any email ability at this time 9/2/11
        /*        private void SendMessage (BundleUpdateMsgTypes type, WOKItem item, WOKItem bundle)
                {
                    SendMessage (type, item, bundle, 0);
                }
                private void SendMessage (BundleUpdateMsgTypes type, WOKItem item, WOKItem bundle, int origBundlePrice)
                {
                    string subj = "";
                    string msg = "";
                    int fromUserId = 1;

                    GetMessageDetails (type, item, bundle, origBundlePrice, ref subj, ref msg);

                    // Insert the message
                    Message message = new Message (0, fromUserId, (int) bundle.ItemCreatorId, subj,
                        msg, new DateTime (), 0, (int) Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                    GetUserFacade().InsertMessage (message);

                    User userTo = GetUserFacade().GetUser ((int) bundle.ItemCreatorId);
                    if (userTo.UserId > 0)
                    {
                        if (Convert.ToInt32 (userTo.NotifyAnyoneMessages).Equals (1))
                        {
                            // They want emails for all messages
                            MailUtilityWeb.SendPrivateMessageNotificationEmail (userTo.Email, message.Subject, message.MessageId, message.MessageText, userTo.UserId, fromUserId);
                        }
                    }
                }
                private void GetMessageDetails (BundleUpdateMsgTypes type, WOKItem item, WOKItem bundle, int originalBundlePrice, ref string subj, ref string msg)
                {
                    // do not send message if item and bundle owner are the same
                    if (item.ItemCreatorId == bundle.ItemCreatorId)
                        return;

                    string itemOwnerName = GetUserFacade().GetUserName ((int) item.ItemCreatorId);

                    switch (type)
                    {
                        case BundleUpdateMsgTypes.AP:
                            subj = "Your bundle has become Access Pass required";
                            msg = itemOwnerName + " has made his item, " + item.DisplayName + ", as private and it was contained in your bundle, " + bundle.DisplayName + ".<br/><br/>" +
                                "This change invalidates your bundle and it has been removed from shop.  No follow up action is required.<br/><br/>" +
                                "Thank you,<br/>" +
                                "The Kaneva Team";
                            break;

                        case BundleUpdateMsgTypes.Delete:
                            subj = "Your bundle was removed from shop";
                            msg = itemOwnerName + " has removed his item, " + item.DisplayName + ", and it was contained in your bundle, " + bundle.DisplayName + ".<br/><br/>" +
                                "This change invalidates your bundle and it has been removed from shop.  No follow up action is required.<br/><br/>" +
                                "Thank you,<br/>" +
                                "The Kaneva Team";
                            break;

                        case BundleUpdateMsgTypes.InvType:
                            subj = "Your bundle has become Credits Only";
                            msg = "The item, " + item.DisplayName + ", has changed to 'Credits Only,' so this requires your bundle, " + bundle.DisplayName + ", to become Credits Only as well.  No follow up action is required on your part.<br/><br/>" +
                                "Thank you,<br/>" +
                                "The Kaneva Team";
                            break;

                        case BundleUpdateMsgTypes.Price:
                            subj = "Your bundle had a price change";
                            msg = itemOwnerName + " has modified their commission on item, " + item.DisplayName + " and it is contained in your bundle, " + bundle.DisplayName + ".<br/><br/>" +
                                "This changes the bundle price from " + originalBundlePrice + " to " + bundle.MarketCost.ToString () + ".   No follow up action is required on your part.<br/><br/>" +
                                "Thank you,<br/>" +
                                "The Kaneva Team";
                            break;

                        case BundleUpdateMsgTypes.Price_BaseItem:
                            subj = "Your bundle had a price change";
                            msg = "The price of " + item.DisplayName + " had changed and it is contained in your bundle, " + bundle.DisplayName + ".<br/><br/>" +
                                "This changes the bundle price from  " + originalBundlePrice + " to " + bundle.MarketCost.ToString () + ".   No follow up action is required on your part.<br/><br/>" +
                                "Thank you,<br/>" +
                                "The Kaneva Team";
                            break;

                        case BundleUpdateMsgTypes.Private:
                            subj = "Your bundle was removed from shop";
                            msg = itemOwnerName + " has made an item, " + item.DisplayName + ", private and it was contained in your bundle, " + bundle.DisplayName + ".<br/><br/>" +
                                "This change invalidates your bundle and it has been removed from shop.  No follow up action is required.<br/><br/>" +
                                "Thank you,<br/>" +
                                "The Kaneva Team";
                            break;
                    }
                }  */
        #endregion Disabled Mail Code

        #endregion Primary Functions


        #region Properties

        public int PageSize()
        {
            return 10;
        }

        private int WOKSearchCurrentPage
        {
            get
            {
                if (ViewState["_WOKcurrentPage"] == null)
                {
                    ViewState["_WOKcurrentPage"] = 0;
                }
                return (int)ViewState["_WOKcurrentPage"];
            }
            set
            {
                ViewState["_WOKcurrentPage"] = value;
            }
        }

        private void resetViewState()
        {
            WOKSearchFilter = null;
            currentPage = 0;
            storeChanges = null;
            passGroupChanges = null;
        }

        private string WOKSearchFilter
        {
            get
            {
                if (ViewState["_WOKSearchFilter"] == null)
                {
                    ViewState["_WOKSearchFilter"] = "";
                }
                return ViewState["_WOKSearchFilter"].ToString();
            }
            set
            {
                ViewState["_WOKSearchFilter"] = value;
            }
        }

        private int currentPage
        {
            get
            {
                if (ViewState["_currentPage"] == null)
                {
                    ViewState["_currentPage"] = 0;
                }
                return (int)ViewState["_currentPage"];
            }
            set
            {
                ViewState["_currentPage"] = value;
            }
        }

        private int UserSearchCurrentPage
        {
            get
            {
                if (ViewState["_UsercurrentPage"] == null)
                {
                    ViewState["_UsercurrentPage"] = 0;
                }
                return (int)ViewState["_UsercurrentPage"];
            }
            set
            {
                ViewState["_UsercurrentPage"] = value;
            }
        }

        private string UserSearchFilter
        {
            get
            {
                if (ViewState["_UserSearchFilter"] == null)
                {
                    ViewState["_UserSearchFilter"] = "";
                }
                return ViewState["_UserSearchFilter"].ToString();
            }
            set
            {
                ViewState["_UserSearchFilter"] = value;
            }
        }

        private string ItemImage
        {
            get
            {
                if (ViewState["_ItemImage"] == null)
                {
                    ViewState["_ItemImage"] = "";
                }
                return ViewState["_ItemImage"].ToString();
            }
            set
            {
                ViewState["_ItemImage"] = value;
            }
        }

        private bool UserIsCustomerSupportRep
        {
            get { return GetCurrentUser ().Role == (int) SiteRole.KanevaRoles.Customer_Service_Rep; }
        }

        private NameValueCollection passGroupChanges
        {
            get
            {
                if (ViewState["_passGroupChanges"] == null)
                {
                    ViewState["_passGroupChanges"] = new NameValueCollection();
                }
                return (NameValueCollection)ViewState["_passGroupChanges"];
            }
            set
            {
                ViewState["_passGroupChanges"] = value;
            }
        }

        private NameValueCollection storeChanges
        {
            get
            {
                if (ViewState["_storeChanges"] == null)
                {
                    ViewState["_storeChanges"] = new NameValueCollection();
                }
                return (NameValueCollection)ViewState["_storeChanges"];
            }
            set
            {
                ViewState["_storeChanges"] = value;
            }
        }

        /// <summary>
        /// DEFAULT_AVAILIBILITY
        /// </summary>
        /// <returns></returns>
        protected string DEFAULT_AVAILIBILITY
        {
            get
            {
                return "2";
            }
        }

        /// <summary>
        /// DEFAULT_CURRENCYTYPE
        /// </summary>
        /// <returns></returns>
        protected string DEFAULT_CURRENCYTYPE
        {
            get
            {
                return "256";
            }
        }

        /// <summary>
        /// DEFAULT_ACCESSTYPE
        /// has been altered to set access pass type to none
        /// </summary>
        /// <returns></returns>
        protected string DEFAULT_ACCESSTYPE
        {
            get
            {
                return "0";
            }
        }

        /// <summary>
        /// DEFAULT_EXCLUSIVITY
        /// </summary>
        /// <returns></returns>
        protected string DEFAULT_EXCLUSIVITY
        {
            get
            {
                return "3";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "name";
            }
        }

        /// <summary>
        /// DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }

        protected BundlePrice CurrentBundlePrice
        {
            get
            {
                if (ViewState["CurrentBundlePrice"] == null)
                {
                    return new BundlePrice ();
                }
                return (BundlePrice) ViewState["CurrentBundlePrice"];
            }
            set { ViewState["CurrentBundlePrice"] = value; }
        }

        enum BundleUpdateMsgTypes
        {
            Private = 1,
            Delete = 2,
            AP = 3,
            Price = 4,
            Price_BaseItem = 5,
            InvType = 6
        }

        #endregion Properties


        #region Helper Functions

        private void NoteChanges(NameValueCollection collection, string id, string value)
        {
            if (collection[id] != null)
            {
                collection.Remove(id);
            }
            else
            {
                collection.Add(id, value);
            }
        }

        private void CalculateSellBackPrice()
        {
            decimal sellPrice = 0.0m;
            try
            {
                sellPrice = Convert.ToDecimal (tbx_MarketCost.Text);
                int percent = Convert.ToInt32(ddl_BBPrice.SelectedValue);
                tbx_SellingPrice.Text = Convert.ToString (Math.Round (sellPrice * (percent / 100.0m)));
            }
            catch (FormatException)
            {
            }
        }

        private void GetRequestParams()
        {
            try
            {
                WOKSearchFilter = " i.global_id = " + Request["itemId"].ToString();
                editItemSubmission = true;
            }
            catch (Exception)
            {
            }
        }

        private void PopulateBuyBackPercentages()
        {
            for (int i = 100; i > 0; i = i - 5)
            {
                ddl_BBPrice.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }

        }

        /// <summary>
        /// Clears out all Promotion fields 
        /// </summary>
        private void PopulateCurrencyTypeList()
        {
            //ddl_CurrencyType.DataSource = WOKStoreUtility.GetCurrencyTypes();
            //ddl_CurrencyType.DataTextField = "currency_type";
            //ddl_CurrencyType.DataValueField = "currency_type_id";
            //ddl_CurrencyType.DataBind();

            ddl_CurrencyType.Items.Add(new ListItem("Cant Be Bought", "0"));
            ddl_CurrencyType.Items.Add(new ListItem("Credits Only", "256"));
            ddl_CurrencyType.Items.Add(new ListItem("Rewards Only", "512"));
            ddl_CurrencyType.Items.Add(new ListItem("Both Credits and Rewards", "768"));
        }

        /// <summary>
        /// Clears out all Promotion fields 
        /// </summary>
        private void ClearCatalogItemData()
        {
            tbx_ItemName.Text = "";
            tbx_ItemID.Text = "";
            tbx_DisplayName.Text = "";
            tbx_Description.Text = "";
            txtItemAddedDate.Text = DateTime.Now.Date.ToString("MM-dd-yyyy");
            tbx_MarketCost.Text = "";
            tbx_DesignerPrice.Text = "";
            tbx_SellingPrice.Text = "";
            tbx_CreatorName.Text = "";
            tbx_CreatorID.Text = "";
            itemImage.ImageUrl = "";
            ddl_CurrencyType.SelectedValue = DEFAULT_CURRENCYTYPE;
            ddl_BBPrice.SelectedValue = "50";
            //cbx_itemActive.Checked = true;
            SetDropDownIndex(dgd_drpActive, "1");
            dgd_Stores.DataSource = null;
            dgd_Stores.DataBind();
            dgd_Users.DataSource = null;
            dgd_Users.DataBind();
        }

        private void USERSearchClear()
        {
            tbx_OwnersSearchFilter.Text = "";
            dgd_Users.DataSource = null;
            dgd_Users.DataBind();
            UserSearchFilter = null;
            UserSearchCurrentPage = 0;
        }

        protected string ShowBundleItemQuantity (object quantity, object useType)
        {
            try
            {
                if (((int) useType) == WOKItem.USE_TYPE_BUNDLE)
                {
                    return "";
                }

                return "(" + quantity.ToString () + ")";
            }
            catch
            { }

            return "";
        }

        protected void BindItemCategories (int globalId)
        {
            // Show the categories
            ShoppingFacade shoppingFacade = new ShoppingFacade ();
            //List<ItemCategory> ics = shoppingFacade.GetItemCategoriesByItemId(globalId);
            WOKItem item = shoppingFacade.GetItem (globalId);

            if (item == null)
            {
                return;
            }

            if (item.Category1 > 0)
            {
                lblCategory1.Text = shoppingFacade.GetItemCategory (item.Category1).Name;
            }
            else
            {
                lblCategory1.Text = "N/A";
            }

            if (item.Category2 > 0)
            {
                lblCategory2.Text = shoppingFacade.GetItemCategory (item.Category2).Name;
            }
            else
            {
                lblCategory2.Text = "N/A";
            }

            if (item.Category3 > 0)
            {
                lblCategory3.Text = shoppingFacade.GetItemCategory (item.Category3).Name;
            }
            else
            {
                lblCategory3.Text = "N/A";
            }
        }

        /// <summary>
        /// SetAddCategoryToTopLevel
        /// </summary>
        private void SetAddCategoryToTopLevel ()
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade ();
            List<ItemCategory> lItemCategory = shoppingFacade.GetItemCategoriesByParentCategoryId ((uint) 0);

            rptAddCategories.DataSource = lItemCategory;
            rptAddCategories.DataBind ();

            cblCategories.DataTextField = "Name";
            cblCategories.DataValueField = "ItemCategoryId";
            cblCategories.DataSource = lItemCategory;
            cblCategories.DataBind ();

            // reset breadcrumb
            catBreadCrumb.InnerText = "";
        }

        private void SaveCategory (int categoryIndex)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade ();

            CheckBox chkSelect;
            HtmlInputHidden hidItemCategoryId;

            int globalId = Convert.ToInt32 (tbx_ItemID.Text);

            foreach (RepeaterItem dgiCategory in rptAddCategories.Items)
            {
                chkSelect = (CheckBox) dgiCategory.FindControl ("chkSelect");

                if (chkSelect.Checked)
                {
                    hidItemCategoryId = (HtmlInputHidden) dgiCategory.FindControl ("hidItemCategoryId");
                    shoppingFacade.AddCategoryToItem (globalId, Convert.ToUInt32 (hidItemCategoryId.Value), categoryIndex);
                    break;
                }
            }

            BindItemCategories (globalId);
        }

        #endregion  Helper Functions


        #region Event Handlers

        protected void btn_Cancel_Click(object sender, System.EventArgs e)
        {
            //clear the fields
            ClearCatalogItemData();

            //make the details panel visible
            pnl_CatalogItemsDetails.Visible = false;

        }

        protected void btn_UsersSearch_Click(object sender, EventArgs e)
        {
            string usersearchvalue = tbx_OwnersSearchFilter.Text.Trim();

            if ((usersearchvalue != "") && (KanevaGlobals.IsNumeric(usersearchvalue)))
            {
                UserSearchFilter = "user_id = " + tbx_OwnersSearchFilter.Text;
            }
            else
            {
                if (usersearchvalue == "")
                {
                    UserSearchFilter = "";
                }
                else
                {
                    UserSearchFilter = "username LIKE '%" + usersearchvalue + "%'";
                }
            }
            BindUserSearch();
        }

        protected void dgd_Users_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            UserSearchCurrentPage = e.NewPageIndex;
            BindUserSearch();
        }

        protected void dgd_Users_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbx_CreatorID.Text = dgd_Users.SelectedRow.Cells[0].Text;
            tbx_CreatorName.Text = dgd_Users.SelectedRow.Cells[1].Text;
        }

        public void btn_WOKSearch_Click(object sender, EventArgs e)
        {
            string wokSearchvalue = tbx_WOKSearchFilter.Text.Trim();

            if ((wokSearchvalue != "") && (KanevaGlobals.IsNumeric(wokSearchvalue)))
            {
                WOKSearchFilter = "i.global_id = " + tbx_WOKSearchFilter.Text + " OR i.item_creator_id = " + tbx_WOKSearchFilter.Text;
            }
            else
            {
                if (wokSearchvalue == "")
                {
                    WOKSearchFilter = "";
                }
                else
                {
                    WOKSearchFilter = "i.name LIKE '%" + wokSearchvalue + "%'";
                }
            }
            BindCatalogItemData(currentPage);
        }

        protected void dg_CatalogItems_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            //logic check to work around initial row creation wheere there is only one column
            if (e.Row.Cells.Count >= 4)
            {
                e.Row.Cells[2].Text = Server.HtmlDecode(e.Row.Cells[2].Text);
                e.Row.Cells[3].Text = Server.HtmlDecode(e.Row.Cells[3].Text);
                e.Row.Cells[4].Text = Server.HtmlDecode(e.Row.Cells[4].Text);
            }

        }

        protected void ShowEditFields_Click (object sender, CommandEventArgs e)
        {
            PopulateWOKItemEdit (Convert.ToInt32(e.CommandArgument));
        }
        protected void dg_CatalogItems_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            currentPage = e.NewPageIndex;
            BindCatalogItemData(currentPage);
        }

        protected void dg_CatalogItems_Sorting(Object sender, GridViewSortEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            BindCatalogItemData(currentPage);
        }

        protected void dg_CatalogItemsmyGrid_OnRowCreated(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            //logic check to work around initial row creation wheere there is only one column
            if (e.Row.Cells.Count > 10)
            {
                e.Row.Cells[0].Visible = true; //global id
                e.Row.Cells[2].Visible = false; //Internal Name
                e.Row.Cells[3].Visible = true; //Display Name
                e.Row.Cells[4].Visible = false; //Item Description
                e.Row.Cells[5].Visible = true; //Date Added
                e.Row.Cells[6].Visible = true; //Selling Price
                e.Row.Cells[7].Visible = true; //Buy Back Price
                e.Row.Cells[8].Visible = false; //Creator
                e.Row.Cells[9].Visible = false; //Inv type
                e.Row.Cells[10].Visible = false; //Image Path
                e.Row.Cells[11].Visible = false; //Active
            }

        }

        protected void tbx_MarketCost_TextChanged (object sender, EventArgs e)
        {
            CalculateSellBackPrice();
        }

        protected void ddl_BBPrice_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            CalculateSellBackPrice();
        }

        protected void cbx_stores_CheckedChanged(object sender, EventArgs e)
        {
            GridViewRow ritem = (GridViewRow)((Control)sender).NamingContainer;
            NoteChanges(storeChanges, ((Label)ritem.FindControl("lbl_storeId")).Text, ((CheckBox)ritem.FindControl("cbx_stores")).Checked.ToString());
        }


        protected void lbn_NewItems_Click(object sender, System.EventArgs e)
        {
            //clear the fields
            ClearCatalogItemData();

            //make the CatalogItems details panel visible
            pnl_CatalogItemsDetails.Visible = true;

            //load the gift sets and stores
            BindStoreList(-1);

            //set id to -1 <indicator that it is a new entry>
            tbx_ItemID.Text = "-1";

        }

        protected void btn_Save_Click (object sender, System.EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            // Grab original item so we can fallback for params not edited in this menu
            int gId = Convert.ToInt32(tbx_ItemID.Text);
            WOKItem updateItem = GetShoppingFacade().GetItem(gId);

            // Prep parameters for update
            updateItem.Name = Server.HtmlEncode(tbx_ItemName.Text.Trim());
            updateItem.DisplayName = Server.HtmlEncode(tbx_DisplayName.Text.Trim());   
            updateItem.Description = Server.HtmlEncode(tbx_Description.Text.Trim());
            updateItem.PassTypeId = Convert.ToInt32(cbAP.Checked ? (int)Constants.ePASS_TYPE.ACCESS : (int)Constants.ePASS_TYPE.GENERAL);
            updateItem.ItemActive = Convert.ToInt32(dgd_drpActive.SelectedValue);
            updateItem.InventoryType = Convert.ToInt32(ddl_CurrencyType.SelectedValue);
            updateItem.ItemCreatorId = Convert.ToUInt32(tbx_CreatorID.Text);
            updateItem.MarketCost = Convert.ToInt32(tbx_MarketCost.Text);
            
            // Perform the update
            List<BundleItemChange> associatedBundleChanges;
            List<DeedItemChange> associatedDeedChanges;
            GetShoppingFacade().UpdateShopItem(updateItem, SiteManagementCommonFunctions.CurrentUser.UserId, true, false, out associatedBundleChanges, out associatedDeedChanges);

            WOKItem item = new WOKItem ();
            bool errorsOccured = false;
            int idcheck = 0;

            // Manage inclusion in stores. ONLY happens via KWAS. Store removal will occur by default if item is set to status other than Public.
            if (storeChanges.Count > 0 && updateItem.ItemActive == (int)WOKItem.ItemActiveStates.Public)
            {
                //enumeration shouldn't cause over head issues due to low concurrent users
                foreach (string key in storeChanges.Keys)
                {
                    idcheck = -1;
                    int storeId = Convert.ToInt32 (key);
                    bool inSet = Convert.ToBoolean (storeChanges[key].ToLower ());

                    //check to see if record already exist
                    DataTable inStoreAlready = WOKStoreUtility.GetItemStoreByItemIDStoreID (item.GlobalId, storeId);

                    if ((inStoreAlready.Rows.Count > 0) && (!inSet))
                    {
                        //if item is unchecked (no longer in set) but exist in database remove it
                        idcheck = GetShoppingFacade().DeleteFromStoreInventories (storeId, item.GlobalId);
                    }
                    else if ((inStoreAlready.Rows.Count <= 0) && (inSet))
                    {
                        idcheck = GetShoppingFacade().InsertIntoStoreInventories(storeId, item.GlobalId);
                    }

                    if (idcheck <= 0)
                    {
                        errorsOccured = true;
                    }
                }
            }

            //check to see if images need to be uploaded and saved
            if (!ItemImage.Equals (inp_itemImage.Value))
            {
                this.UploadWOKItemImage (Convert.ToInt32 (tbx_CreatorID.Text), item.GlobalId, browseTHUMB);
            }

            // Only update servers for non-bundle items
            if (item != null && item.UseType != WOKItem.USE_TYPE_BUNDLE && item.UseType != WOKItem.USE_TYPE_CUSTOM_DEED)
            {
                TickleServer (item.GlobalId);
            }

            //reset viewstate
            resetViewState ();

            //pulledsaved info from database
            btn_WOKSearch_Click(btn_WOKSearch, new EventArgs());

            //make the details panel invisible
            pnl_CatalogItemsDetails.Visible = false;

            if (!errorsOccured)
            {
                ErrorMessages.Text = "Save successful!";
                ErrorMessages.ForeColor = Color.DarkGreen;
            }
            else
            {
                ErrorMessages.Text = "An error has occured during save!";
                ErrorMessages.ForeColor = Color.DarkRed;
            }
        }

        protected void tbx_DesignerPrice_TextChanged (object sender, EventArgs e)
        {
            BundlePrice bp = CurrentBundlePrice;
            bp.DesignCommission = Convert.ToInt32 (((TextBox) sender).Text);
            SetPriceFields (bp);
            tbx_MarketCost.Text = bp.TotalBundle.ToString ();
        }

        /// <summary>
        /// btnRemoveInventory_Click
        /// </summary>
        protected void btnRemoveInventory_Click(object sender, EventArgs e)
        {
            int globalId = Convert.ToInt32(tbx_ItemID.Text);

            WOKItem updateItem = GetShoppingFacade().GetItem(globalId);

            updateItem.ItemActive = (int)WOKItem.ItemActiveStates.Deleted;

            List<BundleItemChange> bundleChanges;
            List<DeedItemChange> deedChanges;
            GetShoppingFacade().UpdateShopItem(updateItem, SiteManagementCommonFunctions.CurrentUser.UserId, true, true, out bundleChanges, out deedChanges);

            //reset viewstate
            resetViewState();

            //pulledsaved info from database
            BindCatalogItemData(currentPage);

            //make the details panel invisible
            pnl_CatalogItemsDetails.Visible = false;

            ErrorMessages.Text = "Item removed from all inventories";
            ErrorMessages.ForeColor = Color.DarkGreen;
        }

        /// <summary>
        /// btnAddCategory_Click
        /// </summary>
        protected void btnAddCategory_Click(object sender, EventArgs e)
        {
            SetAddCategoryToTopLevel();
        }

        /// <summary>
        /// btnSaveCategory_Click
        /// </summary>
        protected void btnSaveCategory1_Click(object sender, EventArgs e)
        {
            SaveCategory(1);
        }

        /// <summary>
        /// btnSaveCategory_Click
        /// </summary>
        protected void btnSaveCategory2_Click(object sender, EventArgs e)
        {
            SaveCategory(2);
        }

        /// <summary>
        /// btnSaveCategory_Click
        /// </summary>
        protected void btnSaveCategory3_Click(object sender, EventArgs e)
        {
            SaveCategory(3);
        }

        /// <summary>
        /// 
        /// </summary>
        protected void lbCategoryDelete_Click(object sender, CommandEventArgs e)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            int globalId = Convert.ToInt32(tbx_ItemID.Text);
            shoppingFacade.DeleteCategoryFromItem (globalId, Convert.ToInt32(e.CommandArgument));
            BindItemCategories(globalId);
        }

        /// <summary>
        /// lbCategory_Click
        /// </summary>
        protected void lbCategory_Click(object sender, CommandEventArgs e)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            List<ItemCategory> lItemCategory = shoppingFacade.GetItemCategoriesByParentCategoryId(Convert.ToUInt32(e.CommandArgument));

            if (lItemCategory.Count > 0)
            {
                // update the breadcrumb
                if (catBreadCrumb.InnerText.Length > 0) {catBreadCrumb.InnerText += " >> ";}
                catBreadCrumb.InnerText += ((LinkButton) sender).Text;
            }

            if (lItemCategory.Count > 0)
            {
                rptAddCategories.DataSource = lItemCategory;
                rptAddCategories.DataBind();

                cblCategories.DataSource = lItemCategory;
                cblCategories.DataBind ();
            }
        }

        #endregion Event Handlers


        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

    }
}