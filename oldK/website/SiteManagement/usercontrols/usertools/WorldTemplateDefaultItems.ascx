<%@ Control Language="C#" AutoEventWireup="true" Codebehind="WorldTemplateDefaultItems.ascx.cs"
	Inherits="SiteManagement.usercontrols.WorldTemplateDefaultItems" %>

<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>

<div id="divErrorData" runat="server" class="error"></div>
<div id="dvTemplateEdit" runat="server">
	<table>
		<tr>
			<td>
				<table>
                    <tr>
                        <td>Add Default Items</td>
                        <td>
                            <asp:TextBox ID="txtDefaultItems" runat="server" Columns="60"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
					<tr>
						<td>
						</td>
						<td>
							<asp:Button ID="btnAddItems" Text="Add" OnClick="btnAddItems_Click" runat="server" />
						</td>
						<td>
						</td>
					</tr>
				</table>
			</td>
		</tr>
        <tr>
            <td>
                <hr />
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBoxList ID="cblDefaultItems" runat="server" DataTextField="GlobalIdName" DataValueField="GlobalId"></asp:CheckBoxList>
            </td>
        </tr>
        <tr>
            <td><Kaneva:Pager id="pgBottom" MaxPagesToDisplay="5" runat="server" IsAjaxMode="false" OnPageChanged="pg_PageChange" /></td>
        </tr>
        <tr>
            <td>
                <asp:Label id="lblSearch" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnDeleteSelected" Text="Delete Selected" OnClick="btnDeleteSelected_Click" runat="server" Visible="false" />
            </td>
        </tr>
	</table>
</div>