<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ScriptGameItemsGlobalConfig.ascx.cs" Inherits="SiteManagement.usercontrols.ScriptGameItemsGlobalConfig" %>

<h4>Game Item Configs</h4>
<div id="divErrorData" runat="server" class="error"></div>
<div id="dvGameItemEdit" runat="server">
    <asp:Repeater id="rptConfigSettings" runat="server">
        <HeaderTemplate>
            <table>
		        <tr>
			        <td>
				        <table>
        </HeaderTemplate>
        <ItemTemplate>
                        <tr id="trRepeaterRow" runat="server">
                            <td><asp:Label id="lblSGIConfigProp" runat="server" text='<%# DataBinder.Eval(Container.DataItem, "Key").ToString() %>'></asp:Label></td>
                            <td><asp:TextBox id="txtSGIConfigProp" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Value").ToString() %>'></asp:TextBox></td>
                            <td><asp:LinkButton ID="lbnDelete" runat="server" Visible='<%# AllowUserInsertsDeletes() %>' CausesValidation="false" OnClientClick="if(!confirm('Are you sure you want to delete this config property?')) return false;" OnClick="btnDeleteGameItemConfig_Click">Delete</asp:LinkButton></td>
                            <td></td>
                        </tr>
        </ItemTemplate>
        <FooterTemplate>
                        <tr id="trNewPropDivider1" runat="server" Visible='<%# AllowUserInsertsDeletes() %>'>
                            <td colspan="4"><hr /></td>
                        </tr>
                        <tr id="trNewProp" runat="server" Visible='<%# AllowUserInsertsDeletes() %>'>
                            <td><asp:Label id="lblNewPropName" runat="server" AssociatedControlID="txtNewPropName" text="New Property Name"></asp:Label></td>
                            <td><asp:TextBox id="txtNewPropName" runat="server"></asp:TextBox></td>
                            <td><asp:Label id="lblNewPropValue" runat="server" AssociatedControlID="txtNewPropValue" text="New Property Value"></asp:Label></td>
                            <td><asp:TextBox id="txtNewPropValue" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr id="trNewPropDivider2" runat="server" Visible='<%# AllowUserInsertsDeletes() %>'>
                            <td colspan="4"><hr /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
							    <asp:Button ID="btnSaveGameItemConfig" Text="Save" OnClick="btnSaveGameItemConfig_Click" runat="server" />
							    <asp:Button ID="btnCancelEdit" Text="Cancel" OnClick="btnCancelEdit_Click" runat="server" />
						    </td>
                            <td></td>
                            <td></td>
                        </tr>
                        </table>
			        </td>
			        <td></td>
		        </tr>
	        </table>
        </FooterTemplate>
    </asp:Repeater>
</div>