using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Drawing;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;

using Kaneva.BusinessLayer.BusinessObjects;
using Facade = Kaneva.BusinessLayer.Facade;


namespace KlausEnt.KEP.SiteManagement
{
    public partial class UserInventory : BaseUserControl
    {
        #region Declarations

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.USER_DATA_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                if (Session["SelectedUserId"] != null)
                {
                    // Load the current user's data the first time
                    SELECTED_USER_ID = Convert.ToInt32(Session["SelectedUserId"]);

                    if (!IsPostBack)
                    {
                        //populate pull downs
                        DefaultItemAddConfig();
                        PopulateWOKItems();
                        PopulateAvailablePackages();

                        // Log the CSR activity
                        this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Viewing user inventory", 0, SELECTED_USER_ID);
                    }
                    //populate post list grid
                    BindData(1, "");
                }
                else
                {
                    Session["CallingPage"] = Request.Url.ToString();
                    Response.Redirect(ResolveUrl("~/Default.aspx?ws=" + GetCurrentSiteID((int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.KANEVA) + "&mn=" + GetUserSearchParentMenuID() + "&sn=" + GetUserSearchControlID()));
                }

            }
            else
            {
                RedirectToHomePage();
            }
        }

        #endregion

        #region Functions

        private void DefaultItemAddConfig()
        {
            chkGift.Checked = false;
            chkBank.Checked = false;
            txtQuantity.Text = "1";
        }

        private void PopulateWOKItems()
        {
            drpItemName.DataValueField = "global_id";
            drpItemName.DataTextField = "glidName";
            drpItemName.DataSource = UsersUtility.GetWOKItems();
            drpItemName.DataBind();
        }

        private void PopulateAvailablePackages()
        {
            drpPackageList.DataValueField = "promotion_id";
            drpPackageList.DataTextField = "bundle_title";
            drpPackageList.DataSource = StoreUtility.GetPromotionalOffers();
            drpPackageList.DataBind();
        }
        
        /// <summary>
        /// BindData
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="filter"></param>
        private void BindData(int pageNumber, string filter)
        {
            // Set the sortable columns
            string orderby = CurrentSort + " " + CurrentSortOrder;

            int iItemsPerPage = 20;
            int userId = Convert.ToInt32(SELECTED_USER_ID);

            PagedDataTable pds = UsersUtility.GetUserInvetoryByCategory(SELECTED_USER_ID, 0, orderby, pageNumber, iItemsPerPage);
            pgTop.NumberOfPages = Math.Ceiling((double)pds.TotalCount / iItemsPerPage).ToString();
            pgTop.DrawControl();

            dgrdInventory.DataSource = pds;
            dgrdInventory.DataBind();

            //display user name
            lblSelectedUser.Text = "Notes for " + SELECTED_USER_NAME;
        }

        private void ConfigureDisplay(int setting)
        {
            switch (setting)
            {
                case 1:
                    tr_AddItem.Visible = true;
                    lbn_showAddItem.Visible = false;
                    lbn_CancelAdd.Visible = true;
                    break;
                case 2:
                    tr_AddPackage.Visible = true;
                    lbn_showAddPackage.Visible = false;
                    lbn_CancelAdd.Visible = true;
                    break;
                default:
                    lbn_showAddItem.Visible = true;
                    lbn_showAddPackage.Visible = true;
                    lbn_CancelAdd.Visible = false;
                    tr_AddItem.Visible = false;
                    tr_AddPackage.Visible = false;
                    break;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// pg_PageChange
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            BindData(e.PageNumber, "");
        }

        protected void UserInventory_Sorting(object sender, DataGridSortCommandEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            BindData(pgTop.CurrentPageNumber, "");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string invType = chkBank.Checked ? "B" : "P";
                int invSubType = chkGift.Checked ? 512 : 256;

                GetUserFacade ().AddItemToPendingInventory (SELECTED_USER_ID, invType, Convert.ToInt32 (drpItemName.SelectedValue), Convert.ToInt32 (txtQuantity.Text), invSubType); 
                ConfigureDisplay (0);

                // Add note
                string strUserNote = "Added: " + drpItemName.SelectedItem.Text + " <br/>" + txtNote.Text;

                UsersUtility.InsertUserNote (SELECTED_USER_ID, GetUserId (), strUserNote);

                lbl_Messages.Text = "Item added to inventory.";
                lbl_Messages.ForeColor = Color.Red;
            }
            catch (Exception ex)
            {
                lbl_Messages.Text = "Error Adding Item(s): " + ex;
                lbl_Messages.ForeColor = Color.Red;
            }
        }

        protected void ConfigureAddDisplay_Command(Object sender, CommandEventArgs e)
        {
            ConfigureDisplay(Convert.ToInt32(e.CommandArgument));
        }

        protected void btnSavePackage_Click(object sender, EventArgs e)
        {
            int returnValue = 0;

            returnValue = StoreUtility.AddWokItems(Convert.ToInt32(drpPackageList.SelectedValue), SELECTED_USER_ID);

            if (returnValue == 0)
            {
                lbl_MessagesPackage.Text = "Error Adding Item(s)";
                lbl_MessagesPackage.ForeColor = Color.Red;
            }
            else
            {
                ConfigureDisplay(0);
                // Add note

                string strUserNote = "Added Package: " + drpPackageList.SelectedItem.Text + " <br/>" + txtNotePackage.Text;

                UsersUtility.InsertUserNote(SELECTED_USER_ID, GetUserId(), strUserNote);

                lbl_MessagesPackage.Text = "Success!";
                lbl_MessagesPackage.ForeColor = Color.Red;
            }

        }

        protected void btnItemFilter_Click(object sender, EventArgs e)
        {
            drpItemName.DataValueField = "global_id";
            drpItemName.DataTextField = "glidName";
            drpItemName.DataSource = UsersUtility.GetWOKItems(txtItemFilter.Text);
            drpItemName.DataBind();
        }

        protected void lbtnLoadUgc_Click(object sender, EventArgs e)
        {
            drpItemName.DataValueField = "global_id";
            drpItemName.DataTextField = "glidName";
            drpItemName.DataSource = UsersUtility.GetWOKItemsIncludingUgc();
            drpItemName.DataBind();
        }


        #endregion


        #region Attributes

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "name";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "DESC";
            }
        }

        private int SELECTED_USER_ID
        {
            get
            {
                return (int)ViewState["users_id"];
            }
            set
            {
                ViewState["users_id"] = value;
            }

        }

        private string SELECTED_USER_NAME
        {
            get
            {
                if (ViewState["users_name"] == null)
                {
                    ViewState["users_name"] = UsersUtility.GetUserNameFromId(SELECTED_USER_ID);
                }

                return ViewState["users_name"].ToString();
            }
            set
            {
                ViewState["users_name"] = value;
            }

        }

        protected string inventoryPending(string location)
        {
            if (location == "Pnd Inv")
            {
                return "Pending";
            }
            else
            {
                return "";
            }
        }



        protected string inventoryLocation(string location)
        {
            if (location == "P")
            {
                return "Player";
            }
            else if (location == "B")
            {
                return "Bank";
            }
            else
            {
                return "";
            }
        }

        protected string creditType(string type)
        {
            if (type == "512")
            {
                return "Rewards";
            }
            else if (type == "256")
            {
                return "Credits";
            }
            else
            {
                return "";
            }
        }

        protected string itemActive(string itemActive)
        {
            int status = Convert.ToInt32(itemActive);

            if (status == (int)WOKItem.ItemActiveStates.Public)
            {
                return "Public";
            }
            else if (status == (int)WOKItem.ItemActiveStates.Private)
            {
                return "Private";
            }
            else
            {
                return "Deleted";
            }
        }

        protected string passGroup(string passGroupId)
        {
            int iPassGroupId = Convert.ToInt32(passGroupId);

            if (iPassGroupId == (int)Constants.ePASS_TYPE.ACCESS)
            {
                return "AP";
            }
            else
            {
                return "";
            }
        }

        protected string shopUrl(string strGlid)
        {
            int globalId = Convert.ToInt32(strGlid);

            return string.Format("http://{0}/ItemDetails.aspx?gId={1}", Facade.Configuration.ShoppingSiteName, globalId);
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
        }

        #endregion
    }
}