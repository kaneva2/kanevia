using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;
using MagicAjax;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class FamePerPacket : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.KANEVA_PROPRIETARY))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                lblErrMessage.Text = "";

                if (!IsPostBack)
                {
                    FameSearchFilter = "";

                    txtSearch.Text = DateTime.Now.ToString("MM-dd-yyyy");

                    //bind the CatalogItems
                    BindData(1);
                }
            }
            else
            {
                RedirectToHomePage();
            }
        }

        #region Helper Methods

        /// <summary>
        /// BindMemberFameData
        /// </summary>
        private void BindData(int pageNumber)
        {
            string startDate = DateTime.Now.ToString("yyyy-MM-dd");

            if (FameSearchFilter == "")
            {
                string endDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                FameSearchFilter = "date_rewarded > '" + startDate + "' AND " +
                    " date_rewarded < '" + endDate + "'";
            }
            if (txtSearch.Text.Trim() == "")
            {
                txtSearch.Text = startDate;
            }

            // Set the sortable columns
            string orderby = CurrentSort + " " + CurrentSortOrder;
            string groupby = "packet_id";
            int pageSize = 60;

            // Get the User Fame Info
            FameFacade fameFacade = new FameFacade();
            PagedList<Packet> famePackets = fameFacade.GetPackets((int)FameTypes.World, FameSearchFilter, orderby, groupby, pageNumber, pageSize);


            if (famePackets != null)
            {
                dgFamePackets.DataSource = famePackets;
                dgFamePackets.DataBind();

                spnResultFilter.InnerText = "Showing results for: " + txtSearch.Text;

                // Set current page
                pgPackets.CurrentPageNumber = pageNumber;
                pgPackets.NumberOfPages = Math.Ceiling((double)famePackets.TotalCount / pageSize).ToString();
                pgPackets.DrawControl();


                if (famePackets.TotalCount == 0)
                {
                    spnResultFilter.InnerText = "No results found for " + txtSearch.Text + ".";
                }
            }

            lblErrMessage.Visible = false;
        }

        #endregion


        #region Event Handlers

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string searchText = txtSearch.Text.Trim();

            try
            {
                if (searchText == "")
                {
                    FameSearchFilter = "";
                }
                else
                {
                    DateTime dt = Convert.ToDateTime(searchText);

                    string startDate = dt.ToString("yyyy-MM-dd");
                    string endDate = dt.AddDays(1).ToString("yyyy-MM-dd");
                    FameSearchFilter = "date_rewarded > '" + startDate + "' AND " +
                        " date_rewarded < '" + endDate + "'";
                }

                currentPage = 1;
                BindData(currentPage);
            }
            catch
            {
                //error not valid date
                lblErrMessage.Text = searchText + " is not a valid date.";
                lblErrMessage.Visible = true;
            }
        }

        /// <summary>
        /// BindMemberFameData
        /// </summary>
        protected void dgFamePackets_Sorting(Object sender, GridViewSortEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            BindData(currentPage);
        }

        protected void pgPackets_PageChange(object sender, PageChangeEventArgs e)
        {
            BindData(e.PageNumber);
        }

        #endregion


        #region Properties

        private int currentPage
        {
            get
            {
                if (ViewState["_currentPage"] == null)
                {
                    ViewState["_currentPage"] = 0;
                }
                return (int)ViewState["_currentPage"];
            }
            set
            {
                ViewState["_currentPage"] = value;
            }
        }
        private string FameSearchFilter
        {
            get
            {
                if (ViewState["_FameSearchFilter"] == null)
                {
                    ViewState["_FameSearchFilter"] = "";
                }
                return ViewState["_FameSearchFilter"].ToString();
            }
            set
            {
                ViewState["_FameSearchFilter"] = value;
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "packet_id";
            }
        }

        /// <summary>
        /// DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }

        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

    }
}
