<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ZoneSettings.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.ZoneSettings" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<script src="../jscript/xEvent.js" type="text/javascript"></script>
<script src="../jscript/balloon.js" type="text/javascript"></script>
<script src="../jscript/yahoo-dom-event.js" type="text/javascript"></script> 
 
<script type="text/JavaScript">
// white balloon with mostly default configuration
// (see http://www.wormbase.org/wiki/index.php/Balloon_Tooltips)
var whiteBalloon    = new Balloon;
whiteBalloon.balloonTextSize  = '100%';

// white ballon with some custom config:
var whiteBalloonSans  = new Balloon;
whiteBalloonSans.upLeftConnector    = '../images/balloons/balloonbottom_sans.png';
whiteBalloonSans.upRightConnector   = '../images/balloons/balloonbottom_sans.png';
whiteBalloonSans.downLeftConnector  = '../images/balloons/balloontop_sans.png';
whiteBalloonSans.downRightConnector = '../images/balloons/balloontop_sans.png';
whiteBalloonSans.upBalloon          = '../images/balloons/balloon_up_top.png';
whiteBalloonSans.downBalloon        = '../images/balloons/balloon_down_bottom.png';
whiteBalloonSans.paddingConnector = '22px';
</script>

<ajax:ajaxpanel id="ajMessage" runat="server">

    <asp:label style="position:relative; font-size:18px; padding: 10px 0 10px 0" runat="server" id="messages" visible="true" />

    <table id="tblZoneSettings" border="0" cellpadding="0" cellspacing="0" width="980px" style="padding-top:20px">
        <tr>
            <td style="width:980px; vertical-align:top; padding-left:10px">
                <h1>Zone Settings</h1>
            </td>
        </tr>
        <tr>
            <td style="width:980px; vertical-align:top; padding-left:10px">
                <table border="0" cellpadding="4" cellspacing="0" width="970px">
                    <tr>
                        <td >
                            <table border="0" rules="cols" cellpadding="4" cellspacing="0" style="background-color:ActiveBorder">
                                <tr>
                                    <td style="width:90px; padding-left:4px"><span style="font-weight:bold; padding-right:10px">Zone Name:</span></td>
                                    <td><asp:TextBox ID="txtChannelZoneName" style="width:200px" MaxLength="31" runat="server" text="" ontextchanged="txtChannelZoneName_TextChanged" /></td>
                                    <td rowspan="2" style="padding-left:20px"><asp:button id="btnSearch" runat="Server" Text="Search" onClick="btnSearch_Click" CausesValidation="False"/></td>
                                </tr>
                            </table><br />
                        </td>                    
                    </tr>
                    <tr>
                        <td>
                            <div>
                                <span style="font-weight:bold; font-size:12px">Selected WoK Server: </span>
                                <asp:DropDownList id="ddlGameServers" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlGameServers_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                            <div>
                                <span class="insideTextNoBold">(Only servers ran within the past 6 months are listed. For all servers, click [Servers] link to the left)</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px"></td>
                    </tr>
                    <tr>
                        <td><asp:label id="lblSearch" runat="server" cssclass="insideTextNoBold" /><Kaneva:Pager id="pgTop" MaxPagesToDisplay="5" runat="server" IsAjaxMode="false" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Repeater ID="rpt_ChannelZones" runat="server" >
                                <HeaderTemplate>
                                        <table border="0" cellpadding="4" cellspacing="0" width="100%">
                                            <tr style="background-color:#cccccc">
                                                <th align="left">Zone Name</th>
                                                <th align="left">Tied Status</th>
                                                <th align="left">Spin-Down Delay (minutes)</th>
                                                <th align="left">Current Script Server</th>
					                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label id="lb_ChannelZoneName" runat="server" />
                                                    <input type="hidden" runat="server" id="hidChannelZoneId" />
                                                    <input type="hidden" runat="server" id="hidZoneInstanceId" />
                                                    <input type="hidden" runat="server" id="hidZoneType" />
                                                </td>
                                                 <td>
                                                     <asp:DropDownList ID="ddlTiedStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTiedStatus_SelectedIndexChanged"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Textbox id="txtSpinDownDelay" runat="server" width="80" />
                                                    <asp:Button id="btnUpdateSpinDownDelay" runat="server" text="Update" onClick="btnUpdateSpinDownDelay_Click" />
                                                    <input type="hidden" runat="server" id="hidOldSpinDownDelay" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton id="lbtnScriptZoneServerId" runat="server" OnClick="lbtnScriptZoneServerId_Click" />
                                                </td>
                                           </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                            <tr style="background-color:#eeeeee">
                                                <td>
                                                    <asp:Label id="lb_ChannelZoneName" runat="server" />
                                                    <input type="hidden" runat="server" id="hidChannelZoneId" />
                                                    <input type="hidden" runat="server" id="hidZoneInstanceId" />
                                                    <input type="hidden" runat="server" id="hidZoneType" />
                                                </td>
                                                 <td>
                                                     <asp:DropDownList ID="ddlTiedStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTiedStatus_SelectedIndexChanged"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Textbox id="txtSpinDownDelay" runat="server" width="80" />
                                                    <asp:Button id="btnUpdateSpinDownDelay" runat="server" text="Update" onClick="btnUpdateSpinDownDelay_Click" />
                                                    <input type="hidden" runat="server" id="hidOldSpinDownDelay" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton id="lbtnScriptZoneServerId" runat="server" OnClick="lbtnScriptZoneServerId_Click" />
                                                </td>
                                           </tr>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                         </td>
                    </tr>
                    <tr>
                        <td><asp:label id="lblSearch2" runat="server" cssclass="insideTextNoBold" /><Kaneva:Pager id="pgBottom" MaxPagesToDisplay="5" runat="server" IsAjaxMode="false"></Kaneva:Pager></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </ajax:ajaxpanel>