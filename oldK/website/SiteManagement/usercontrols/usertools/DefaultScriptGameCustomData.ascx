<%@ Control Language="C#" AutoEventWireup="true" Codebehind="DefaultScriptGameCustomData.ascx.cs" Inherits="SiteManagement.usercontrols.DefaultScriptGameCustomData" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>

<asp:LinkButton ID="lbListGameCustomDataAttributes" runat="server" Text="List Attributes" OnClick="btnListCustomDataAttributes_Click"></asp:LinkButton> | 
<asp:LinkButton ID="lbNewCustomDataAttribute" runat="server" Text="New" OnClick="btnNewCustomDataAttribute_Click"></asp:LinkButton>
<br />
<br />
<div id="divErrorData" runat="server" class="error"></div>
<div id="dvCustomDataAttributeEdit" runat="server">
    <table>
		<tr>
			<td>
				<table>
                    <tr>
                        <td style="vertical-align:top">Attribute Name:<asp:RequiredFieldValidator ID="rfAttribute" ControlToValidate="txtAttributeName" Text="*" ErrorMessage="Attribute is a required field." Display="Dynamic" runat="server"/></td>
                        <td>
                            <asp:TextBox ID="txtAttributeName" runat="server" Columns="20"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">Value:<asp:RequiredFieldValidator ID="rfValue" ControlToValidate="txtValue" Text="*" ErrorMessage="Value is a required field." Display="Dynamic" runat="server"/></td>
                        <td>
                            <asp:TextBox ID="txtValue" runat="server" Columns="100" Rows="20" TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">Apply to Templates:</td>
                        <td>
                            <asp:CheckBox ID="cbApplyToTemplates" runat="server" />
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
					<tr>
						<td>
						</td>
						<td>
							<asp:Button ID="btnSaveCustomDataAttribute" Text="Save" OnClick="btnSaveCustomDataAttribute_Click" runat="server" />
							<asp:Button ID="btnCancelEdit" Text="Cancel" OnClick="btnCancelEdit_Click" runat="server" />
						</td>
						<td>
						</td>
					</tr>
					<tr>
						<td>
						</td>
						<td>
							<asp:Label ID="txtSaveStatus" runat="server"></asp:Label></td>
						<td>
						</td>
					</tr>
				</table>
			</td>
			<td>
				<!-- Help Text -->
			</td>
		</tr>
	</table>
</div>
<div id="dvCustomDataAttributes" runat="server">
	<asp:Label ID="txtGVStatus" runat="server"></asp:Label>
	<asp:GridView ID="gvCustomDataAttributes" runat="server" OnRowEditing="gvCustomDataAttributes_Edit" AutoGenerateColumns="false">
		<RowStyle BackColor="White"></RowStyle>
		<HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000"
			HorizontalAlign="Left"></HeaderStyle>
		<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
		<FooterStyle BackColor="#FFffff"></FooterStyle>
		<Columns>
			<asp:TemplateField>
				<ItemTemplate>
					&nbsp;<asp:LinkButton ID="lbnEdit" runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>&nbsp;
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="Attribute" HeaderText="Attribute" />
		</Columns>
	</asp:GridView>
    <Kaneva:Pager runat="server" id="pgBottom" OnPageChanged="pg_PageChanged"/>
</div>