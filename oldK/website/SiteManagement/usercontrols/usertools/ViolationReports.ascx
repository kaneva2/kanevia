<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ViolationReports.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.ViolationReports" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<script type="text/javascript" src="jscript/yahoo/yahoo-min.js"></script>  
<script type="text/javascript" src="jscript/yahoo/event-min.js" ></script>   
<script type="text/javascript" src="jscript/yahoo/dom-min.js" ></script>   
<script type="text/javascript" src="jscript/yahoo/calendar-min.js"></script>

<script type="text/javascript" language="javascript" src="jscript/prototype.js"></script>

<link href="css/yahoo/calendar.css" type="text/css" rel="stylesheet" />   


<style type="text/css">
	#cal1Container { display:none; position:absolute; z-index:100;}
	#cal2Container { display:none; position:absolute; z-index:100;}
</style>
 
 
<script type="text/javascript"><!--
 function setElementVisibility(clientId,value)
 {
    var element = $(clientId);
    element.style.display = value;
 }

 var txtStartDate_ID = '<%= txtStartDate.ClientID %>';
 var txtEndDate_ID = '<%= txtEndDate.ClientID %>';
  
YAHOO.namespace("example.calendar");

    
	function handleStartDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate1 = $(txtStartDate_ID);
		txtDate1.value = month + "-" + day + "-" + year;
		YAHOO.example.calendar.cal1.hide();
	}
	
	function initStartDate() {
		// Admin Calendar
		YAHOO.example.calendar.cal1 = new YAHOO.widget.Calendar ("cal1", "cal1Container", { iframe:true, zIndex:200, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal1.render();
		
		// Listener to show Admin Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgItemStartDate", "click", YAHOO.example.calendar.cal1.show, YAHOO.example.calendar.cal1, true);   
		YAHOO.example.calendar.cal1.selectEvent.subscribe(handleStartDate, YAHOO.example.calendar.cal1, true);
	}
	
		function handleEndDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate2 = $(txtEndDate_ID);
		txtDate2.value = month + "-" + day + "-" + year;
		YAHOO.example.calendar.cal2.hide();
	}
	
	function initEndDate() {
		// Admin Calendar
		YAHOO.example.calendar.cal2 = new YAHOO.widget.Calendar ("cal2", "cal2Container", { iframe:true, zIndex:200, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal2.render();
		
		// Listener to show Admin Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgItemEndDate", "click", YAHOO.example.calendar.cal1.show, YAHOO.example.calendar.cal2, true);   
		YAHOO.example.calendar.cal2.selectEvent.subscribe(handleEndDate, YAHOO.example.calendar.cal2, true);
	}
//--> 
</script> 

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">

    <ajax:ajaxpanel id="ajpFamepackets" runat="server">  
    <asp:Panel runat="server" id="pnbackground" width="980px" align="center" style="background-color:#fdfdfd;">           

            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label runat="server" id="lblErrMessage" class="errBox black"></asp:label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label id="lblNoViolations" style="font-size:16px;color:Red;font-weight:bold" visible="false" runat="server">No Violations Were Found.</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="width:10px">    
                                 </td>
                                <td style="width:100px">    
                                    Item Number
                                </td>
                                <td>    
                                    Item Name
                                </td>
                                <td>
                                    Owner
                                </td>
                                <td>
                                    Reported By
                                </td>
                                <td>
                                    Actioned By
                                </td>
                                <td >
                                    <span style="font-size:9px">Start Date</span>
                                </td>
                                <td >
                                    <span style="font-size:9px">End Date</span>
                                </td>
                                <td colspan="2">
                                </td>
                           </tr>
                            <tr>
                                <td style="width:10px">    
                                 </td>
                                <td style="width:100px">
                                    <asp:TextBox runat="server" width="80px" id="tbxItemNumSearch" ></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" id="tbxNameSearch" width="120px" ></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" id="tbxOwnerSearch" width="120px" ></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" id="tbxRptBySearch" width="120px" ></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" id="tbxActBySearch" width="120px" ></asp:TextBox>
                                </td>
                               <td>
                                    <div id="cal1Container"></div><asp:TextBox runat="server" id="txtStartDate" maxlength="10" width="80"></asp:TextBox>
                                    <img id="imgItemStartDate" name="imgItemStartDate" onload="initStartDate();" src="images/cal_16.gif"/>
                                </td>
                                <td>
                                    <div id="cal2Container"></div><asp:TextBox runat="server" id="txtEndDate" maxlength="10" width="80"></asp:TextBox>
                                    <img id="imgItemEndDate" name="imgItemEndDate" onload="initEndDate();" src="images/cal_16.gif"/>
                                </td>
                                <td style="width:100px" align="center">
                                    <asp:Button id="btnSearch" onclick="btnSearch_Click" causesvalidation="false" runat="server" text="Search"></asp:Button>
                                </td>                            
                                <td style="width:10px">    
                                 </td>
                           </tr>

                            <tr>
                                <td colspan="6">    
                                <td>
                                    <span style="font-size:9px">mm-dd-yyyy</span>
                                </td>
                                <td>
                                    <span style="font-size:9px">mm-dd-yyyy</span>
                                </td>
                                <td>
                                </td>
                                 <td style="width:10px">    
                                 </td>
                           </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr />
                    </td>
                </tr>
            </table>
             <Kaneva:Pager id="pgTop" MaxPagesToDisplay="5" runat="server" IsAjaxMode="false" />
            <asp:Repeater ID="rpt_ViolationReports" runat="server" >
               <HeaderTemplate>
                        <table border="0" rules="rows" cellpadding="10px" >
                            <tr style="background-color:ActiveBorder">
                                <th align="center"><asp:LinkButton ID="lbl1" style="color:#555555; font-weight:bold" runat="server" OnCommand="Sort_Command" CausesValidation="false" CommandArgument="violation_id" >Item</asp:LinkButton></th>
                                <th align="left"><asp:LinkButton ID="lbl3" style="color:#555555; font-weight:bold" runat="server" OnCommand="Sort_Command" CausesValidation="false" CommandArgument="owner_name" >Owner Name</asp:LinkButton></th>
                                <th align="left"><asp:LinkButton ID="lbl4" style="color:#555555; font-weight:bold" runat="server" OnCommand="Sort_Command" CausesValidation="false" CommandArgument="reporter_name" >Reported By</asp:LinkButton></th>
                                <th align="left"><asp:LinkButton ID="lbl5" style="color:#555555; font-weight:bold" runat="server" OnCommand="Sort_Command" CausesValidation="false" CommandArgument="violation_type_id" >Category</asp:LinkButton></th>
                                <th align="left">Member Comments</th>
                                <th align="left"><asp:LinkButton ID="lbl6" style="color:#555555; font-weight:bold" runat="server" OnCommand="Sort_Command" CausesValidation="false" CommandArgument="report_date" >Report Date</asp:LinkButton></th>
                                <th align="left">CSR Notes</th>
                                <th align="left"><asp:LinkButton ID="lbl7" style="color:#555555; font-weight:bold" runat="server" OnCommand="Sort_Command" CausesValidation="false" CommandArgument="violation_action_type_id" >Status</asp:LinkButton></th>
                                <th align="left"><asp:LinkButton ID="lbl8" style="color:#555555; font-weight:bold" runat="server" OnCommand="Sort_Command" CausesValidation="false" CommandArgument="modifiers_id" >Last Modifier</asp:LinkButton></th>
                                <th align="left">Action</th>
					        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                            <tr style="background-color:#ffeeee">
                                <td align="center"><!-- image of item -->
                                    <a id="lnk_ItemName" runat="server" target="_blank" />
                                    <div class="framesize-medium">
									    <div class="frame">
										    <span class="ct"><span class="cl"></span></span>
											<div class="imgconstrain">
											    <a id="lnk_Itemlocation" runat="server" target="_blank">
												    <img src="" alt="" style="border:0" id="img_ItemImage" runat="server" />
												</a>
											</div>	
											<span class="cb"><span class="cl"></span></span>
										</div>
									</div>										
                                     <input type="hidden" runat="server" id="hidViolationId" />
								    <!-- Item No --><a id="lnk_ItemNo" runat="server" target="_blank" />
                                </td>
                                <td><!-- Owner Name -->
								    <a id="lnk_OwnerName" runat="server" target="_blank" />
                                </td>
                                <td><!-- Reporting User -->
								    <a id="lnk_ReportingUser" runat="server" target="_blank" />
                                </td>
                                 <td><!-- Violation category -->
                                     <asp:label id="lbl_violation" runat="server" visible="true" />
                                </td>
                                <td><!-- Member Comments -->
								    <asp:LinkButton runat="server" id="lbn_MemberComments" AutoPostBack="true" onClick="lbnMemberComments_Click" />
								    <div id="divMemberComments" runat="server" visible="false" style="position:absolute; border:solid 3 teal; background-color:Teal">
								        <asp:TextBox id="tbx_MemberComment" runat="server" rows="8" columns="30" textmode="multiline" enabled="true" /><br />
								        <asp:Button id="dvbtn_MemberCancel" causevalidation="false" AutoPostBack="true" onclick="dvbtn_MemberCancel_Click" runat="server" Text="Cancel"></asp:Button>
								    </div>
                                </td>
                                <td><!-- Report date -->
								    <asp:label id="lbl_ReportDate" runat="server" visible="true" />
                                </td>
                                 <td><!-- CSR Comments -->
								     <asp:LinkButton runat="server" id="lbn_CSRComments" AutoPostBack="true" onClick="lbnCSRComments_Click" />
								     <div id="divCSRComments" runat="server" visible="false" style="position:absolute; border:solid 3 blue; background-color:Blue">
								        <asp:TextBox id="tbx_CSRComment" runat="server" rows="8" columns="30" textmode="multiline" enabled="true" /><br />
								        <div style="width:200px; ">
								        <asp:TextBox id="tbx_CSRAddComment" runat="server" columns="38" textmode="singleline" enabled="true" />
								        <asp:Button id="dvbtn_CCancel" runat="server" causevalidation="false" AutoPostBack="true" style="float:left" onclick="dvbtn_CSRCancel_Click" Text="Cancel"></asp:Button>
								        <asp:Button id="dvbtn_CSave" runat="server" AutoPostBack="true" style="float:left" onclick="lbnCSRSave_Click" Text="Save"></asp:Button>
								        </div>
								    </div>

                                </td>
                                 <td><!-- Status -->
								    <asp:label id="lbl_Status" runat="server" visible="true" />
                                </td>
                                <td><!-- Last Modifier -->
                                    <asp:label id="lbl_LastModifier" runat="server" visible="true" />
                                </td>
                                 <td><!-- Action to take on violation -->
                                     <asp:DropDownList ID="ddl_ViolationAction" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ViolationAction_SelectedIndexChanged" />
                                    <br />Reports = <asp:Label runat="server" id="tr_subTotal" />
                                </td>
                           </tr>

                </ItemTemplate>
                <AlternatingItemTemplate>
                            <tr style="background-color:#eeeeee">
                                <td align="center"><!-- image of item -->
                                    <a id="lnk_ItemName" runat="server" target="_blank" />
                                    <div class="framesize-medium">
									    <div class="frame">
										    <span class="ct"><span class="cl"></span></span>
											<div class="imgconstrain">
											    <a id="lnk_Itemlocation" runat="server" target="_blank">
												    <img src="" alt="" style="border:0" id="img_ItemImage" runat="server" />
												</a>
											</div>	
											<span class="cb"><span class="cl"></span></span>
										</div>
									</div>										
                                     <input type="hidden" runat="server" id="hidViolationId" />
								    <!-- Item No --><a id="lnk_ItemNo" runat="server" target="_blank" />
                                </td>
                                <td><!-- Owner Name -->
								    <a id="lnk_OwnerName" runat="server" target="_blank" />
                                </td>
                                <td><!-- Reporting User -->
								    <a id="lnk_ReportingUser" runat="server" target="_blank" />
                                </td>
                                 <td><!-- Violation category -->
                                    <asp:label id="lbl_violation" runat="server" visible="true" />
                                </td>
                                <td><!-- Member Comments -->
								    <asp:LinkButton runat="server" id="lbn_MemberComments" AutoPostBack="true" onClick="lbnMemberComments_Click" />
								    <div id="divMemberComments" runat="server" visible="false" style="position:absolute; text-align:right; border:solid 3 teal; background-color:Teal">
								        <asp:TextBox id="tbx_MemberComment" runat="server" rows="8" columns="30" textmode="multiline" enabled="true" />
								        <asp:Button id="dvbtn_MemberCancel" AutoPostBack="true" causevalidation="false" onclick="dvbtn_MemberCancel_Click" runat="server" Text="Cancel"></asp:Button>
								    </div>
                                </td>
                                <td><!-- Report date -->
								    <asp:label id="lbl_ReportDate" runat="server" visible="true" />
                                </td>
                                 <td><!-- CSR Comments -->
								     <asp:LinkButton runat="server" id="lbn_CSRComments" AutoPostBack="true" onClick="lbnCSRComments_Click" />
								     <div id="divCSRComments" runat="server" visible="false" style="position:absolute; text-align:right; border:solid 3 blue; background-color:Blue">
								        <asp:TextBox id="tbx_CSRComment" runat="server" rows="8" columns="30" textmode="multiline" enabled="true" />
								        <asp:TextBox id="tbx_CSRAddComment" runat="server" columns="38" textmode="singleline" enabled="true" />
								        <div style="width:200px; float:right">
								        <asp:Button id="dvbtn_CCancel" runat="server" causevalidation="false" AutoPostBack="true" style="float:left" onclick="dvbtn_CSRCancel_Click" Text="Cancel"></asp:Button>
								        <asp:Button id="dvbtn_CSave" runat="server" AutoPostBack="true" style="float:left" onclick="lbnCSRSave_Click" Text="Save"></asp:Button>
								        </div>
								    </div>
                                </td>
                                 <td><!-- Status -->
								    <asp:label id="lbl_Status" runat="server" visible="true" />
                                </td>
                                <td><!-- Last Modifier -->
                                    <asp:label id="lbl_LastModifier" runat="server" visible="true" />
                                </td>
                                <td><!-- Action to take on violation -->
                                     <asp:DropDownList ID="ddl_ViolationAction" runat="server" AutoPostBack="true"  OnSelectedIndexChanged="ViolationAction_SelectedIndexChanged" />
                                     <br />Reports = <asp:Label runat="server" id="tr_subTotal" />
                               </td>
                           </tr>

                </AlternatingItemTemplate>

                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <div class="colR result"><Kaneva:Pager id="pgBottom" MaxPagesToDisplay="5" runat="server" IsAjaxMode="false"></Kaneva:Pager></div>
            <div class="clear"><!-- clear the floats --></div>
		    
        </asp:Panel>
    <br />
    </ajax:ajaxpanel>
    
 </asp:Panel>