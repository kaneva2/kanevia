﻿<%@ Control language="C#" autoeventwireup="true" codebehind="ABTestParticipants.ascx.cs" inherits="KlausEnt.KEP.SiteManagement.ABTestParticipants" %>
<%@ Register tagprefix="Kaneva" tagname="Pager" src="../Pager.ascx" %>

<asp:ScriptManager id="smKWAS" runat="server" enablepartialrendering="true"></asp:ScriptManager>

<link href="css/siteManagement.css" type="text/css" rel="stylesheet">
<link href="css/kaneva/ABTestParticipants.css?v=2" type="text/css" rel="stylesheet">

<div id="abContainer">
    <header>
        <h1 class="title">A/B Test Participants</h1>
        <div class="actions">
            <asp:DropDownList id="ddlExperiment" runat="server"></asp:DropDownList>
            <asp:TextBox id="txtSearch" runat="server" maxlength="200"></asp:TextBox>
            <asp:Button id="lbSearch" runat="server" onclick="btnSearch_Click" causesvalidation="false" text="Search" />
            <asp:Button id="btnAdd" runat="server" onclick="btnAdd_Click" causesvalidation="false" text="Add Participant" />
        </div>
    </header>
    <div id="divErrorTop" runat="server" class="error" style="display: none;"></div>
    <%-- User Listings --%>
    <div id="divParticipantListing" runat="server" visible="false">
        <asp:Repeater id="rptParticipants" onitemdatabound="rptParticipants_ItemDataBound" EnableViewState="True" runat="server">
            <ItemTemplate>
                <div class="participant-listing">
                    <div class="participant-info">
                        <div class="name"><%# DataBinder.Eval(Container.DataItem, "Username").ToString() %></div>
                        <span class="detailslabel">Experiment: </span><span class="detailstext"><%# this.GetExperimentName((Kaneva.BusinessLayer.BusinessObjects.UserExperimentGroup)Container.DataItem) %></span><br />
                        <span class="detailslabel">Group: </span><span class="detailstext"><%# this.GetGroupName((Kaneva.BusinessLayer.BusinessObjects.UserExperimentGroup)Container.DataItem)%></span>
                    
                        <label for="ddlGroupInput">Group:</label>

                        <br /><br />
                        
                        <asp:DropDownList id="ddlGroupInlineInput" DataTextField="Name" DataValueField="GroupId" cssclass="groupInput" runat="server" width="250px"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlGroupInlineInput_SelectedIndexChanged" CausesValidation="false" />
                        <asp:HiddenField id="hfUserGroupId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "UserGroupId") %>' />
                        <span id="spnMsgInline" runat="server" class="error" style="display: none;"></span>
                    </div>
                    <div class="participant-details">
                        <div class="edit">
                            <asp:LinkButton id="lbEdit" runat="server" commandargument='<%# DataBinder.Eval(Container.DataItem, "UserGroupId") %>' causesvalidation="false" text="Edit" oncommand="lbEdit_Click"></asp:LinkButton></div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <div class="pagination">
            <div class="results">
                <asp:Label runat="server" id="lblSearch" /></div>
            <div class="pager">
                <Kaneva:Pager runat="server" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
            </div>
        </div>
    </div>
    <%-- Edit Section --%>
    <div id="divDetails" runat="server" visible="false" clientidmode="Static">
        <h2>Edit Participant</h2>
        <div class="dataContainer">
            <div class="form-row">
                <label for="txtName">Name:</label>
                <asp:TextBox id="txtName" cssclass="nameInput" runat="server" maxlength="200" clientidmode="Static"></asp:TextBox>
                <asp:RequiredFieldValidator id="rfUserName" runat="server" controltovalidate="txtName" errormessage="*" />
            </div>
            <asp:UpdatePanel id="udpExperimentDetails" runat="server" rendermode="Block" updatemode="Conditional" clientidmode="Static">
                <ContentTemplate>
                    <div class="form-row">
                        <label for="ddlExperimentInput">Experiment:</label>
                        <asp:DropDownList id="ddlExperimentInput" onselectedindexchanged="ddlExperimentInput_OnSelectedIndexChanged" 
                            cssclass="experimentInput" runat="server" width="250px" autopostback="true" />
                    </div>
                    <div class="form-row">
                        <label for="ddlGroupInput">Group:</label>
                        <asp:DropDownList id="ddlGroupInput" cssclass="groupInput" runat="server" width="250px" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <asp:Button id="btnUpdateParticipants" runat="server" onclick="btnUpdateParticipants_Click" text="Save" />
        <asp:Button id="btnCancelParticipants" runat="Server" causesvalidation="False" onclick="btnCancelParticipants_Click" text="Cancel" />
    </div>
</div>
