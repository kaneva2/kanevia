<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GameTransactions.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.GameTransactions" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>


<div id="transContainer">
	
	<div id="currentBalance">
		<div class="title">Current Earned Balance</div>
		<div class="credits">
			<div class="heading">Earned Credits</div>
			<div class="total" id="divTotalCredits" runat="server">182,925</div>
		</div>
		<div class="cash">
			<div class="heading">Cash Value</div>
			<div class="total" id="divTotalCash" runat="server">$917.63</div>
		</div>
	</div>
	<div class="info">*Minimum $1000 Cash Value balance required to request a cash payment.</div>
	<div class="info noasterisk">Minimum 20,000 Earned Credits required to request Credits.  See the <a href="">Redemption Policy</a> for more information.</div>

	<asp:repeater id="rptTransactions" runat="server">
		<headertemplate>
			<div id="header">
				<div class="date">Trans.<br />Date</div>
				<div class="buyer">Purchaser</div>
				<div class="itemnum">Item #</div>
				<div class="name">Item Name</div>
				<div class="price">Item<br />Credit<br />Price</div>
				<div class="qty">Qty</div>
				<div class="total">Trans.<br />Total</div>
				<div class="earned">Earned<br />Credits</div>
				<div class="balance">Earned<br />Credit<br />Balance</div>
				<div class="redeem">Redempt.</div>
			</div>
		</headertemplate>
		<itemtemplate>
			<div class="row">	
				<div class="date"><%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "PurchaseDate")).ToString("MM/dd/yyyy") %></div>
				<div class="buyer"><%# (DataBinder.Eval(Container.DataItem, "BuyerName")) %></div>
				<div class="itemnum"><%# (DataBinder.Eval(Container.DataItem, "GlobalId")) %></div>
				<div class="name"><%# (DataBinder.Eval(Container.DataItem, "DisplayName")) %></div>
				<div class="price"><%# (DataBinder.Eval(Container.DataItem, "ItemPrice")) %></div>
				<div class="qty"><%# (DataBinder.Eval(Container.DataItem, "Quantity")) %></div>
				<div class="total"><%# GetTotalPrice(DataBinder.Eval (Container.DataItem, "ItemPrice"), DataBinder.Eval (Container.DataItem, "Quantity")) %></div>
				<div class="earned"><%# GetEarnedAmount () %></div>
				<div class="balance">0</div>
				<div class="redeem"></div>
			</div>
		</itemtemplate>
	</asp:repeater>

	<div class="pager"><asp:Label runat="server" id="lblSearch" CssClass="dateStamp"></asp:Label><Kaneva:Pager runat="server" id="pgTop"></Kaneva:Pager></div>
</div>


<style type="text/css">
#transContainer {padding:10px 10px 10px 10px;}

#currentBalance {font-size:14px;font-weight:bold;border-bottom:solid 1px #ccc;margin-bottom:2px;height:38px;overflow:visible;}
#currentBalance .title {float:left;width:300px;padding-top:18px;}
#currentBalance .credits {float:left;width:240px;}
#currentBalance .cash {float:left;}
#currentBalance .heading {color:#999;padding-bottom:2px;}
#currentBalance .total {padding-bottom:6px;}

.info {clear:both;color:#999;font-size:9px;font-weight:none;margin-left:3px;}
.noasterisk {padding-left:7px;}

#header {font-weight:bold;font-size:10px;color:#000;overflow:hidden;padding:4px 2px 2px 2px;background-color:#ccc;width:100%;height:40px;margin:12px 0 8px 0;}
#header div {float:left;}
.row {width:100%;font-size:11px;clear:both;padding:4px 0 2px 0;border-bottom:solid 1px #ccc;height:14px;}
.row div {float:left;overflow:hidden;height:14px;}

.date {width:80px;}
.buyer {width:150px;}
.itemnum {width:80px;}
.name {width:250px;}
.price {width:60px;}
.qty {width:40px;}
.total {width:60px;}
.earned {width:70px;}
.balance {width:70px;}
.redeem {width:90px;}

.pager {clear:both;width:99%;padding-right:10px;margin-top:12px;font-size:10px;text-align:right;font-weight:bold;}

</style>