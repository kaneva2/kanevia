using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;

using System.Threading;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class UserPublishedItems : BaseUserControl
    {
        #region Declarations

        protected HtmlInputHidden hidAssetId;
        protected Button btnDeleteAsset;

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.USER_DATA_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                if (Session["SelectedUserId"] != null)
                {
                    // Load the current user's data the first time
                    SELECTED_USER_ID = Convert.ToInt32(Session["SelectedUserId"]);

                    if (!IsPostBack)
                    {

                        // Log the CSR activity
                        this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Viewing user published items", 0, SELECTED_USER_ID);

                    }
                    //populate post list grid
                    BindData(1, "");
                }
                else
                {
                    Session["CallingPage"] = Request.Url.ToString();
                    Response.Redirect(ResolveUrl("~/Default.aspx?ws=" + GetCurrentSiteID((int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.KANEVA) + "&mn=" + GetUserSearchParentMenuID() + "&sn=" + GetUserSearchControlID()));
                }

                // Add the javascript for deleting assets
                string scriptString = "<script language=\"javaScript\">\n<!--\n function DeleteAsset (id) {\n";
                scriptString += "   document.all." + hidAssetId.ClientID + ".value = id;\n";
                scriptString += "	" + Page.ClientScript.GetPostBackEventReference(btnDeleteAsset, "") + ";\n";
                scriptString += "}\n// -->\n";
                scriptString += "</script>";

                if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType(), "DeleteAsset"))
                {
                    Page.ClientScript.RegisterClientScriptBlock(GetType(), "DeleteAsset", scriptString);
                }

            }
            else
            {
                RedirectToHomePage();
            }
        }

        #endregion


        #region Helper Functions

        /// <summary>
        /// Get a string
        /// </summary>
        /// <param name="dtColumn"></param>
        /// <returns></returns>
        public string FormatString(Object dtColumn, int statusId)
        {
            if (statusId.Equals((int)Constants.eASSET_STATUS.NEW))
                return "<span class=\"offline\">NOT SET</span>";

            return dtColumn.ToString();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// BindData
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="filter"></param>
        private void BindData(int pageNumber, string filter)
        {
            string orderby = CurrentSort + " " + CurrentSortOrder;

            int iItemsPerPage = 20;
            int userChannelId = GetUserFacade().GetPersonalChannelId(SELECTED_USER_ID);

            PagedDataTable pds = StoreUtility.GetAssetsInChannel(userChannelId, false, true, 0, filter, orderby, pageNumber, iItemsPerPage);
            pgTop.NumberOfPages = Math.Ceiling((double)pds.TotalCount / iItemsPerPage).ToString();
            pgTop.DrawControl();

            dgrdRatings.DataSource = pds;
            dgrdRatings.DataBind();

            //display user name
            lblSelectedUser.Text = "Notes for " + SELECTED_USER_NAME;
        }

        /// <summary>
        /// They clicked delete a Asset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDeleteAsset_Click(object sender, System.EventArgs e)
        {
            int assetId = Convert.ToInt32(hidAssetId.Value);

            StoreUtility.DeleteAsset(assetId, GetUserId());
            BindData(pgTop.CurrentPageNumber, "");
        }

        /// <summary>
        /// pg_PageChange
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            BindData(e.PageNumber, "");
        }

        protected void UserPublished_Sorting(object sender, DataGridSortCommandEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            BindData(pgTop.CurrentPageNumber, "");
        }

        #endregion


        #region Attributes

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "created_date";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "DESC";
            }
        }

        private int SELECTED_USER_ID
        {
            get
            {
                return (int)ViewState["users_id"];
            }
            set
            {
                ViewState["users_id"] = value;
            }

        }

        private string SELECTED_USER_NAME
        {
            get
            {
                if (ViewState["users_name"] == null)
                {
                    ViewState["users_name"] = UsersUtility.GetUserNameFromId(SELECTED_USER_ID);
                }

                return ViewState["users_name"].ToString();
            }
            set
            {
                ViewState["users_name"] = value;
            }

        }

        #endregion


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);

            // Delete rating controls
            btnDeleteAsset = new Button();
            hidAssetId = new HtmlInputHidden();

            btnDeleteAsset.Style.Add("DISPLAY", "none");
            btnDeleteAsset.Click += new EventHandler(btnDeleteAsset_Click);

            this.Controls.Add(btnDeleteAsset);
            this.Controls.Add(hidAssetId);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
        }

        #endregion

    }
}