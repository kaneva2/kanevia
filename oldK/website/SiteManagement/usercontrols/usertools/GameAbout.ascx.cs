﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
namespace KlausEnt.KEP.SiteManagement
{
    public partial class GameAbout : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //community id check is to tell it to redirect to search page (for KWAS only)
            if (Request["communityId"] != null)
            {
                CommunityFacade communityFacade = new CommunityFacade();
                Community community = communityFacade.GetCommunity(Convert.ToInt32(Request["communityId"]));
                ConfigureCommunityMeetMe3D(btnMeet3D, community.WOK3App.GameId, community.CommunityId, community.Name, "", 1);
            }

            
        }
    }
}