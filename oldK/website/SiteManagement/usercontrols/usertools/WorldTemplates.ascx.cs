using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.Mailer;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using KlausEnt.KEP.Kaneva;
using System.IO;
using KlausEnt.KEP.SiteManagement;
using System.Linq;
using System.Collections.Generic;

namespace SiteManagement.usercontrols
{
    public partial class WorldTemplates : BaseUserControl
    {
        #region Declarations
        public const string TEMPLATE_SIZE_SMALL = "Small";
        public const string TEMPLATE_SIZE_MEDIUM = "Medium";
        public const string TEMPLATE_SIZE_LARGE = "Large";
        public const string TEMPLATE_SIZE_THUMBNAIL = "Thumbnail";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
                ResetPage();
			}
		}

        #region Data Manipulation
        /// <summary>
        /// Called to bind data to page elements
        /// </summary>
        protected void BindData()
		{
            GameFacade gameFacade = new GameFacade();

            // Bind templates
            int [] template_statuses = {Convert.ToInt32(WorldTemplate.WorldTemplateStatus.ACTIVE), Convert.ToInt32(WorldTemplate.WorldTemplateStatus.ADMIN_ONLY), Convert.ToInt32(WorldTemplate.WorldTemplateStatus.ARCHIVED)};
            int[] templatePassGroups = new int[] { 0, Kaneva.BusinessLayer.Facade.Configuration.AccessPassGroupID, Kaneva.BusinessLayer.Facade.Configuration.VipPassGroupID };
            PagedList<WorldTemplate> templates = gameFacade.GetWorldTemplates(template_statuses, templatePassGroups, 0, null, 1, 50, orderBy: "template_id ASC, sort_order ASC");
            gvTemplates.DataSource = templates;
            gvTemplates.DataBind();

            // Bind template categories
            ddlDefaultCategoryId.DataSource = GetCommunityFacade().GetCommunityCategories();
            ddlDefaultCategoryId.DataTextField = "CategoryName";
            ddlDefaultCategoryId.DataValueField = "CategoryId";
            ddlDefaultCategoryId.DataBind();
            ddlDefaultCategoryId.Items.Insert(0, new ListItem("", "0"));

            // Bind status categories
            ddlStatus.Items.Clear();
            ddlStatus.Items.Add(new ListItem("Archived", ((int)WorldTemplate.WorldTemplateStatus.ARCHIVED).ToString()));
            ddlStatus.Items.Add(new ListItem("Active", ((int)WorldTemplate.WorldTemplateStatus.ACTIVE).ToString()));
            ddlStatus.Items.Add(new ListItem("Admin Only", ((int)WorldTemplate.WorldTemplateStatus.ADMIN_ONLY).ToString()));

            // Bind the pass groups
            ddlPassGroup.Items.Clear();
            ddlPassGroup.Items.Add(new ListItem("", "0"));
            ddlPassGroup.Items.Add(new ListItem("Access Pass", Kaneva.BusinessLayer.Facade.Configuration.AccessPassGroupID.ToString()));
            ddlPassGroup.Items.Add(new ListItem("VIP Pass", Kaneva.BusinessLayer.Facade.Configuration.VipPassGroupID.ToString()));
            ddlUpsellPassGroup.Items.Clear();
            ddlUpsellPassGroup.Items.Add(new ListItem("", "0"));
            ddlUpsellPassGroup.Items.Add(new ListItem("Access Pass", Kaneva.BusinessLayer.Facade.Configuration.AccessPassGroupID.ToString()));
            ddlUpsellPassGroup.Items.Add(new ListItem("VIP Pass", Kaneva.BusinessLayer.Facade.Configuration.VipPassGroupID.ToString()));

            // Bind the experiments
            ddlExperiments.Items.Clear();
            ddlExperiments.DataSource = GetExperimentFacade().SearchExperiments(string.Empty, false, "e.creation_date ASC", 1, Int32.MaxValue);
            ddlExperiments.DataBind();

            // Bind the experiment groups
            ddlExperimentGroups.Items.Clear();
		}

        /// <summary>
        /// Called to fill a WorldTemplate BO from input fields.
        /// </summary>
        /// <param name="template">The WorldTemplate Business Object to fill.</param>
        /// <returns>True if this is a new template already exists in the database.</returns>
        protected bool FillWorldTemplateFromInput(ref WorldTemplate template)
        {
            DateTime outDT;

            // Try to get a template from the DB first
            int templateId = Convert.ToInt32(txtTemplateId.Text);
            template = GetGameFacade().GetWorldTemplate(templateId, true);
            bool templateExists = template.Name != string.Empty;

            template.TemplateId = templateId;
            template.Name = txtTemplateName.Text;
            template.Description = txtTemplateDescription.Text;
            template.Features = txtTemplateFeatureList.Text;
            template.SortOrder = Convert.ToInt32(string.IsNullOrWhiteSpace(txtSortOrder.Text) ? "0" : txtSortOrder.Text);
            template.DefaultCategoryId = Convert.ToInt32(ddlDefaultCategoryId.SelectedValue);
            template.Status = Convert.ToInt32(ddlStatus.SelectedValue);
            template.IsDefaultHomeTemplate = Convert.ToBoolean(ddlIsDefault.SelectedValue);
            template.KEPThumbnailId = Convert.ToInt32(txtKEPThumbnailId.Text);
            template.IncubatorHosted = Convert.ToBoolean(ddlIsIncubatorHosted.SelectedValue);
            template.DisplayPassGroupId = Convert.ToInt32(ddlPassGroup.SelectedValue);
            template.UpsellPassGroupId = Convert.ToInt32(ddlUpsellPassGroup.SelectedValue);
            template.GlobalId = Convert.ToInt32(string.IsNullOrWhiteSpace(txtGlobalId.Text) ? "0" : txtGlobalId.Text);
            template.GrandfatherDate = DateTime.TryParse(txtGrandfatherDate.Text, out outDT) ? (DateTime?)outDT : null;
            template.FirstPublicReleaseDate = DateTime.TryParse(txtFirstPublicReleaseDate.Text, out outDT) ? outDT : DateTime.Now;
            template.PrivateOnCreation = Convert.ToBoolean(ddlPrivateOnCreate.SelectedValue);

            string experimentGroupId;
            if (!string.IsNullOrWhiteSpace(ddlExperimentGroups.SelectedValue))
            {
                experimentGroupId = ddlExperimentGroups.SelectedValue;

                // Check each group, remove if it or any other groups in the experiment are already assigned to the template.
                foreach(WorldTemplateExperimentGroup group in template.ExperimentGroups)
                {
                    Experiment experiment = group.ExperimentGroup.Experiment;
                    if (experiment != null && experiment.ExperimentGroups.Where(g => g.GroupId.Equals(experimentGroupId)).Count() > 0)
                    {
                        template.ExperimentGroups.Remove(group);
                        break;
                    }
                }

                template.ExperimentGroups.Add(new WorldTemplateExperimentGroup(template.TemplateId, experimentGroupId));
            }

            return templateExists;
        }

        /// <summary>
        /// Called to fill input fields from a WorldTemplate BO.
        /// </summary>
        /// <param name="template">The WorldTemplate Business Object to use to fill.</param>
        protected void FillInputFromWorldTemplate(WorldTemplate template)
        {
            txtTemplateId.Text = template.TemplateId.ToString();
            txtTemplateName.Text = template.Name;
            txtTemplateDescription.Text = template.Description;
            txtTemplateFeatureList.Text = template.Features;
            txtSortOrder.Text = template.SortOrder.ToString();
            ddlDefaultCategoryId.SelectedValue = template.DefaultCategoryId.ToString();
            ddlStatus.SelectedValue = template.Status.ToString();
            ddlIsDefault.SelectedValue = template.IsDefaultHomeTemplate.ToString();
            ddlIsIncubatorHosted.SelectedValue = template.IncubatorHosted.ToString();
            txtKEPThumbnailId.Text = template.KEPThumbnailId.ToString();
            litPvImageSmallPath.Text = template.PreviewImagePathSmall;
            litPvImageMediumPath.Text = template.PreviewImagePathMedium;
            litPvImageLargePath.Text = template.PreviewImagePathLarge;
            litDefaultThumbnailPath.Text = template.DefaultThumbnail;
            ddlPassGroup.SelectedValue = template.DisplayPassGroupId.ToString();
            ddlUpsellPassGroup.SelectedValue = template.UpsellPassGroupId.ToString();
            txtGlobalId.Text = (template.GlobalId > 0 ? template.GlobalId.ToString() : string.Empty);
            txtGrandfatherDate.Text = template.GrandfatherDate == null ? string.Empty : template.GrandfatherDate.ToString();
            txtFirstPublicReleaseDate.Text = template.FirstPublicReleaseDate.Equals(DateTime.MaxValue) ? "" : template.FirstPublicReleaseDate.ToString();
            lbManageDefaultItems.CommandArgument = template.TemplateId.ToString();
            lbManageDefaultItems.Visible = true;
            ddlPrivateOnCreate.SelectedValue = template.PrivateOnCreation.ToString();
        }

        /// <summary>
        /// Called to empty input fields to defaults.
        /// </summary>
        protected void ResetFields()
        {
            txtTemplateId.Enabled = true;
            txtTemplateId.Text = "";
            txtTemplateName.Text = "";
            txtTemplateDescription.Text = "";
            txtTemplateFeatureList.Text = "";
            txtSortOrder.Text = "";
            ddlDefaultCategoryId.SelectedIndex = 0;
            ddlStatus.SelectedIndex = 0;
            ddlIsDefault.SelectedIndex = 0;
            txtKEPThumbnailId.Text = "";
            litPvImageSmallPath.Text = "";
            litPvImageMediumPath.Text = "";
            litPvImageLargePath.Text = "";
            litDefaultThumbnailPath.Text = "";
            ddlIsDefault.SelectedValue = "False";
            ddlIsIncubatorHosted.SelectedValue = "False";
            divErrorData.InnerText = "";
            ddlPassGroup.SelectedIndex = 0;
            ddlUpsellPassGroup.SelectedIndex = 0;
            txtGlobalId.Text = "";
            txtGrandfatherDate.Text = "";
            txtFirstPublicReleaseDate.Text = "";
            ddlExperiments.ClearSelection();
            ddlExperimentGroups.ClearSelection();
            ddlPrivateOnCreate.SelectedValue = "False";
        }

        /// <summary>
        /// Called to reset the page
        /// </summary>
        protected void ResetPage()
        {
            BindData();

            dvTemplateEdit.Visible = false;
            dvTemplates.Visible = true;

            ResetFields();
        }

        /// <summary>
        /// Called to validate the page before saving
        /// </summary>
        protected bool ValidatePage()
        {
            bool inputsAreValid = true;

            return inputsAreValid;
        }
        #endregion

        #region Event Handlers
        protected void btnNewTemplate_Click(object sender, EventArgs e)
		{
            dvTemplateEdit.Visible = true;
            dvTemplates.Visible = false;
			ResetFields();
		}

		protected void btnListTemplates_Click(object sender, EventArgs e)
		{
            ResetPage();
		}

		protected void btnCancelEdit_Click(object sender, EventArgs e)
		{
            ResetPage();
		}

		protected void btnSaveTemplate_Click(object sender, EventArgs e)
		{
            if (Page.IsValid && ValidatePage())
            {
                WorldTemplate template = null;
                bool templateExists = FillWorldTemplateFromInput(ref template);

                if (!templateExists)
                {
                    templateExists = (GetGameFacade().InsertWorldTemplate(template) > 0);
                }
                else
                {
                    GetGameFacade().UpdateWorldTemplate(template);
                }

                if (templateExists)
                {
                    // Make the template custom deed private
                    if (template.GlobalId > 0)
                    {
                        WOKItem templateZone = GetShoppingFacade().GetItem(template.GlobalId);
                        templateZone.ItemActive = (int)WOKItem.ItemActiveStates.Private;
                        GetShoppingFacade().UpdateCustomItem(templateZone);
                    }

                    //update the images separately for convenience
                    bool imageSmallUpdateSuccess = true;
                    bool imageMediumUpdateSuccess = true;
                    bool imageLargeUpdateSuccess = true;
                    bool imageThumbnailUpdateSuccess = true;

                    if ((fiPreviewImageSmall != null) && (fiPreviewImageSmall.Value != "") && (template.PreviewImagePathSmall != fiPreviewImageSmall.Value))
                    {
                        imageSmallUpdateSuccess = (UpdateWorldTemplateImage(ref template, TEMPLATE_SIZE_SMALL, fiPreviewImageSmall) > 0);
                    }
                    if ((fiPreviewImageMedium != null) && (fiPreviewImageMedium.Value != "") && (template.PreviewImagePathMedium != fiPreviewImageMedium.Value))
                    {
                        imageMediumUpdateSuccess = (UpdateWorldTemplateImage(ref template, TEMPLATE_SIZE_MEDIUM, fiPreviewImageMedium) > 0);
                    }
                    if ((fiPreviewImageLarge != null) && (fiPreviewImageLarge.Value != "") && (template.PreviewImagePathLarge != fiPreviewImageLarge.Value))
                    {
                        imageLargeUpdateSuccess = (UpdateWorldTemplateImage(ref template, TEMPLATE_SIZE_LARGE, fiPreviewImageLarge) > 0);
                    }
                    if ((fiDefaultThumbnail != null) && (fiDefaultThumbnail.Value != "") && (template.DefaultThumbnail != fiDefaultThumbnail.Value))
                    {
                        imageThumbnailUpdateSuccess = (UpdateWorldTemplateImage(ref template, TEMPLATE_SIZE_THUMBNAIL, fiDefaultThumbnail) > 0);
                    }

                    if (!(imageSmallUpdateSuccess && imageMediumUpdateSuccess && imageLargeUpdateSuccess && imageThumbnailUpdateSuccess))
                    {
                        divErrorData.InnerText = "Error updating template image.";
                    }
                    else
                    {
                        ResetPage();
                    }
                }
                else
                {
                    divErrorData.InnerText = "Error updating game template.";
                }
            }
            else
            {
                divErrorData.InnerText = "Invalid inputs.";
            }
		}

		protected void gvTemplates_Edit(object sender, GridViewEditEventArgs e)
		{
            dvTemplateEdit.Visible = true;
            dvTemplates.Visible = false;
            
            GridViewRow gvr = gvTemplates.Rows[e.NewEditIndex];
            WorldTemplate template = GetGameFacade().GetWorldTemplate(Convert.ToInt32(gvr.Cells[1].Text), true);

            FillInputFromWorldTemplate(template);
            e.Cancel = true;
		}

        protected void gvTemplates_Delete(object sender, GridViewDeleteEventArgs e)
        {
            // Cancel the delete operation if the user attempts to remove
            // the last record from the GridView control.
            if (gvTemplates.Rows.Count <= 1)
            {
                e.Cancel = true;
            }

            GridViewRow gvr = gvTemplates.Rows[e.RowIndex];
            WorldTemplate template = GetGameFacade().GetWorldTemplate(Convert.ToInt32(gvr.Cells[1].Text));

            GetGameFacade().DeleteWorldTemplate(template);

            ResetPage();
        }

        protected void lbManageDefaultItems_Click(object sender, CommandEventArgs e)
        {
            int templateId = -1;

            if (Int32.TryParse(e.CommandArgument.ToString(), out templateId))
            {
                txtTemplateId.Text = templateId.ToString();

                // Check to see if the template already exists. We can only associate default items if it's already been created.
                WorldTemplate template = null;
                if (FillWorldTemplateFromInput(ref template))
                {
                    // Snag the server id
                    Session["SelectedTemplateId"] = templateId;

                    Response.Redirect(ResolveUrl("~/Default.aspx?ws=" + GetCurrentSiteID((int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.DEVELOPER) + "&mn=" + GetWorldTemplateParentMenuID() + "&sn=" + GetWorldTemplateDefaultItemsControlID()));
                }
            }
        }

        protected void ddlExperiments_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            int templateId;
            if (Int32.TryParse(txtTemplateId.Text, out templateId) && !string.IsNullOrWhiteSpace(ddlExperiments.SelectedValue))
            {
                Experiment experiment = GetExperimentFacade().GetExperiment(ddlExperiments.SelectedValue);

                ddlExperimentGroups.DataSource = experiment.ExperimentGroups;
                ddlExperimentGroups.DataBind();

                WorldTemplate template = GetGameFacade().GetWorldTemplate(templateId, true);
                WorldTemplateExperimentGroup group = template.ExperimentGroups.SingleOrDefault(g => g.ExperimentGroup.ExperimentId.Equals(ddlExperiments.SelectedValue));

                if (group != null)
                {
                    ddlExperimentGroups.SelectedValue = group.GroupId;
                }
            }
        }

        #endregion

        #region Helper Functions
        protected int UpdateWorldTemplateImage(ref WorldTemplate template, string imageSize, HtmlInputFile inpFile)
        {
            try
            {
                //declare the file name of the image
                string fileName = null;

                //create PostedFiles
                System.Web.HttpPostedFile file = inpFile.PostedFile;

                //generate path prefixes based on naming scheme
                string pathPrefix = Path.Combine("GameTemplates", template.Name.Replace(" ", string.Empty), imageSize);

                //upload images
                ImageHelper.UploadImageFromUser(ref fileName, file, Path.Combine(KanevaGlobals.MediaRootPath, pathPrefix), KanevaGlobals.MaxUploadedImageSize);

                //set template image paths
                if (imageSize == TEMPLATE_SIZE_SMALL)
                {
                    template.PreviewImagePathSmall = ((pathPrefix + "/" + fileName).Replace(Path.DirectorySeparatorChar, '/'));
                }
                else if (imageSize == TEMPLATE_SIZE_MEDIUM)
                {
                    template.PreviewImagePathMedium = ((pathPrefix + "/" + fileName).Replace(Path.DirectorySeparatorChar, '/'));
                }
                else if (imageSize == TEMPLATE_SIZE_LARGE)
                {
                    template.PreviewImagePathLarge = ((pathPrefix + "/" + fileName).Replace(Path.DirectorySeparatorChar, '/'));
                }
                else if (imageSize == TEMPLATE_SIZE_THUMBNAIL)
                {
                    template.DefaultThumbnail = ((pathPrefix + "/" + fileName).Replace(Path.DirectorySeparatorChar, '/'));
                }

                return GetGameFacade().UpdateWorldTemplate(template);
            }
            catch { return 0; }
        }
        #endregion
    }  
}