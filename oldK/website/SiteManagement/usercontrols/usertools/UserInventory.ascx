<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="UserInventory.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.UserInventory" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    <table id="tblUserInventory" border="0" cellpadding="0" cellspacing="0" width="980">
        <tr>
            <td style="padding-left:40px; text-align:center; font-size:22px">
                USER INVENTORY
            </td>
        </tr>
        <tr>
            <td style="padding-left:40px">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
	                <tr style="line-height:15px;">
	                    <td align="left" style="width:300px" class="belowFilter2" valign="middle" >
	                        <asp:Label runat="server" id="lblSelectedUser" />
	                    </td>
		                <td align="right" class="belowFilter2" valign="middle">
			                <asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop" /><br>
		                </td>						
	                </tr>
                </table>
             </td>
         </tr>
         <tr>
            <td style="padding-left:40px; padding-right:20px">
                <asp:LinkButton ID="lbn_showAddItem" runat="server" CommandArgument="1" OnCommand="ConfigureAddDisplay_Command">Add Item</asp:LinkButton>
                <asp:LinkButton ID="lbn_showAddPackage" runat="server" CommandArgument="2" OnCommand="ConfigureAddDisplay_Command">Add Package</asp:LinkButton>
                <asp:LinkButton ID="lbn_CancelAdd" runat="server" CommandArgument="0" Visible="false" OnCommand="ConfigureAddDisplay_Command">Cancel Add</asp:LinkButton>
            </td>
         </tr>
         <tr>
            <td style="padding-left:40px">
                <asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" OnSortCommand="UserInventory_Sorting" Width="100%" id="dgrdInventory" cellpadding="2" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 1px solid #cccccc; font-family:arial">  
	                <HeaderStyle BackColor="#cccccc" ForeColor="#000000" Font-Underline="false" Font-Bold="true" HorizontalAlign="Left" />
	                <ItemStyle BackColor="#ffffff" HorizontalAlign="Left" />
	                <AlternatingItemStyle BackColor="#eeeeee" HorizontalAlign="Left" />
	                <Columns>				
		                <asp:TemplateColumn HeaderText="item name" SortExpression="name" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			                <ItemTemplate>
                                <a href="<%# shopUrl(DataBinder.Eval(Container.DataItem, "global_id").ToString()) %>" target="_blank">
				                    <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%> 
                                </a>
			                </ItemTemplate>
		                </asp:TemplateColumn>

                               <asp:TemplateColumn HeaderText="GLID" SortExpression="global_id" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# DataBinder.Eval(Container.DataItem, "global_id").ToString()%>
			                </ItemTemplate>
		                </asp:TemplateColumn>

                        <asp:TemplateColumn HeaderText="Qty." SortExpression="quantity" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# DataBinder.Eval(Container.DataItem, "quantity").ToString()%>
			                </ItemTemplate>
		                </asp:TemplateColumn>

		                <asp:TemplateColumn HeaderText="Credits / Rewards" SortExpression="inventory_sub_type" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# creditType(DataBinder.Eval(Container.DataItem, "inventory_sub_type").ToString())%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
                		
		                <asp:TemplateColumn HeaderText="On Player / In Bank" SortExpression="inventory_type" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# inventoryLocation(DataBinder.Eval(Container.DataItem, "inventory_type").ToString())%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
                		
		                <asp:TemplateColumn HeaderText="Pending" SortExpression="inv_location" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# inventoryPending(DataBinder.Eval (Container.DataItem, "inv_location").ToString ())%>
			                </ItemTemplate>
		                </asp:TemplateColumn>

                        <asp:TemplateColumn HeaderText="Pass Group" SortExpression="pass_group_id" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# passGroup(DataBinder.Eval (Container.DataItem, "pass_group_id").ToString ())%>
			                </ItemTemplate>
		                </asp:TemplateColumn>

                        <asp:TemplateColumn HeaderText="Status" SortExpression="item_active" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# itemActive(DataBinder.Eval (Container.DataItem, "item_active").ToString ())%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		            </Columns>		
                </asp:datagrid>
            </td>
         </tr>
         <tr id="tr_AddItem" runat="server" visible="false">
            <td style="padding-left:40px; padding-top:40px">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td valign="middle" align="right" width="100"><strong>Item Name: </strong></td>
	                    <td align="left">
	                        <asp:DropDownList class="formKanevaText" id="drpItemName" style="width:180px" runat="server"/>
	                        <asp:textbox id="txtItemFilter" runat="server"></asp:textbox>
	                        <asp:button id="btnItemFilter" text="Filter" runat="server" onClick="btnItemFilter_Click" /> 	        
	                        <asp:LinkButton ID="lbtnLoadUgc" text="[Load Dropdown Including UGC]" runat="server" OnClick="lbtnLoadUgc_Click"></asp:LinkButton>
	                    </td>
                    </tr>
                    <tr>
                        <td valign="middle" align="right" width="100"><strong>Quantity: </strong></td>
	                    <td align="left"><asp:TextBox id="txtQuantity" class="formKanevaText" style="width:150px" MaxLength="5" runat="server"/></td>
                    </tr>
                    <tr>
                        <td valign="middle" align="right" width="100"><strong>Gift: </strong></td>
	                    <td align="left"><asp:CheckBox id="chkGift" runat="server"/></td>
	                </tr>
	                <tr>
	                    <td valign="middle" align="right" width="100"><strong>Bank: </strong></td>
                        <td align="left"><asp:CheckBox id="chkBank" runat="server"/></td>
	                </tr>
	                <tr>
	                    <td valign="top" align="right" width="100"><strong>Note: </strong></td>
                        <td align="left">
                            <asp:textbox id="txtNote" runat="server" TextMode="MultiLine" Rows="3" Columns="60"></asp:textbox><br />
                            (Note will be prepeneded with the name of the item and quantity added)
                        </td>
	                </tr>		
	                <tr>
	                <td align="right"></td>
	                <td valign="middle" width="100">
	                    <asp:button id="btnSave" runat="Server" onClick="btnSave_Click" CausesValidation="False" Text="Add Item"/> 
	                </td>
	                </tr>                
                </table>
            </td>
         </tr>
         <tr id="tr_AddPackage" runat="server" visible="false">
            <td style="padding-left:40px; padding-top:40px">
                <table border="0" cellpadding="0" cellspacing="0">
	                <tr>
	                    <td align="right"><strong>Package: </strong></td>
	                    <td>
	                        <asp:DropDownList id="drpPackageList" runat="server"></asp:DropDownList> ( Will only add items associated with the package. No credits will be added.)
	                    </td>
	                </tr>
                    <tr>
	                    <td valign="top" align="right" width="100"><strong>Note: </strong></td>
                        <td align="left">
                            <asp:textbox id="txtNotePackage" runat="server" TextMode="MultiLine" Rows="3" Columns="60"></asp:textbox><br />
                            (Note will be prepeneded with the name of the item and quantity added)
                        </td>
	                </tr>	
	                <tr>
	                    <td></td>
	                    <td >
	                        <asp:button id="btnSavePackage" runat="Server" onClick="btnSavePackage_Click" CausesValidation="False" Text="Add Package"/>
	                    </td>
                	    
	                </tr>
	             </table>
            </td>
         </tr>
         <tr>
            <td style="padding-left:40px; padding-top:40px">	                        
                <asp:Label id=lbl_MessagesPackage runat="server"></asp:Label>	
	            <asp:Label id=lbl_Messages runat="server"></asp:Label>	

            </td>
         </tr>
    </table>
</asp:Panel>
