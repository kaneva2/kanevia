﻿<%@ Control language="C#" autoeventwireup="true" codebehind="ABTestCategories.ascx.cs" inherits="KlausEnt.KEP.SiteManagement.ABTestCategories" %>
<%@ Register tagprefix="Kaneva" tagname="Pager" src="../Pager.ascx" %>

<asp:ScriptManager id="smKWAS" runat="server" enablepartialrendering="true"></asp:ScriptManager>

<link href="css/siteManagement.css" type="text/css" rel="stylesheet">
<link href="css/kaneva/ABTestExperiments.css" type="text/css" rel="stylesheet">

<asp:UpdatePanel id="udpCategories" runat="server" rendermode="Block" updatemode="Conditional">
    <Triggers>
        <asp:PostBackTrigger controlid="btnDeleteCategory" />
    </Triggers>
    <ContentTemplate>
        <header>
            <h1 class="title">A/B Test Categories</h1>
            <div class="actions">
                <asp:TextBox id="txtSearch" runat="server"></asp:TextBox>
                <asp:Button id="btnSearch" runat="server" onclick="btnSearch_Click" text="Search" causesvalidation="false" />
                <asp:Button id="btnNewCategory" runat="server" onclick="btnNewCategory_Click" text="Add Category" causesvalidation="false" />
                <div id="divMsgTop" runat="server" class="error" style="display: none;"></div>
            </div>
        </header>
        <div class="tab-content-wrapper">
            <asp:LinkButton id="lbBackToListing" runat="server" text="Back to List" onclick="lbBackToListing_Click" cssclass="breadcrumb" causesvalidation="false" />
            <%-- Category Listings --%>
            <div id="divCategoryListing" runat="server" visible="false">
                <asp:Repeater id="rptCategories" runat="server">
                    <ItemTemplate>
                        <div class="experiment-listing">
                            <div class="experiment-info">
                                <div class="name"><%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></div>
                                <div><%# DataBinder.Eval(Container.DataItem, "Description").ToString() %></div>
                            </div>
                            <div class="experiment-details">
                                <asp:LinkButton id="btnEditCategory" runat="server" commandargument='<%# (DataBinder.Eval(Container.DataItem, "Name")) %>' text="Edit" oncommand="btnEditCategory_Click" causesvalidation="false" />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <div class="results">
                    <asp:Label runat="server" id="lblSearch" />
                </div>
                <div class="pager">
                    <Kaneva:Pager runat="server" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
                </div>
            </div>
            <%-- Category Details --%>
            <div id="divEditCategory" runat="server" visible="false" clientidmode="Static" class="tab-content">
                <div class="form-wrapper-left">
                    <h2 class="form-title">Edit Category</h2>
                    <div class="form-row">
                        <label for="txtName">Name:</label>
                        <asp:TextBox id="txtName" runat="server" maxlength="200" clientidmode="Static"></asp:TextBox>
                        <asp:RequiredFieldValidator id="rfName" runat="server" controltovalidate="txtName" errormessage="*" />
                    </div>
                    <div class="form-row">
                        <label for="txtDescription">Description:</label>
                        <asp:TextBox id="txtDescription" textmode="multiline" columns="34" runat="server" maxlength="1000" clientidmode="Static"></asp:TextBox>
                        <asp:RequiredFieldValidator id="rfDescription" runat="server" controltovalidate="txtDescription" errormessage="*" />
                    </div>
                    <asp:Button id="btnUpdateCategory" runat="server" onclick="btnUpdateCategory_Click" text="Save" />
                    <asp:Button id="btnDeleteCategory" runat="server" causesvalidation="false" onclick="btnDeleteCategory_Click" text="Delete" OnClientClick="if(!confirm('Are you sure you want to delete the category?')) return false;" />
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
