<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="UsersSYSDiagnostics.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.UsersSYSDiagnostics" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="980">
        <tr>
            <td style="padding-left:40px; text-align:center; font-size:22px">
                USER SYSTEM DIAGNOSTICS
            </td>
        </tr>
        <tr>
            <td style="padding-left:40px">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
	                <tr style="line-height:15px;">
	                    <td align="left" style="width:300px" class="belowFilter2" valign="middle" >
	                        <asp:Label runat="server" id="lblSelectedUser" />
	                    </td>
		                <td align="right" class="belowFilter2" valign="middle">
			                <asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop" /><br>
		                </td>						
	                </tr>
                </table>
             </td>
         </tr>
         <tr>
            <td style="padding-left:40px">
                <asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" OnSortCommand="UserDIAG_Sorting" Width="100%" id="dgrdDXDiag" cellpadding="2" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 1px solid #cccccc; font-family:arial">  
	                <HeaderStyle BackColor="#cccccc" ForeColor="#000000" Font-Underline="false" Font-Bold="true" HorizontalAlign="Left" />
	                <ItemStyle BackColor="#ffffff" HorizontalAlign="Left" />
	                <AlternatingItemStyle BackColor="#eeeeee" HorizontalAlign="Left" />
				        <Columns>					
        					
					        <asp:TemplateColumn HeaderText="Date" SortExpression="diag_date" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval (Container.DataItem, "diag_date") %> 
						        </ItemTemplate>
					        </asp:TemplateColumn>

					        <asp:TemplateColumn HeaderText="O/S" SortExpression="operating_system" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval(Container.DataItem, "operating_system").ToString()%>
						        </ItemTemplate>
					        </asp:TemplateColumn>

					        <asp:TemplateColumn HeaderText="Make" SortExpression="make" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval(Container.DataItem, "make")%>
						        </ItemTemplate>
					        </asp:TemplateColumn>
        					
					        <asp:TemplateColumn HeaderText="Model" SortExpression="model" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval(Container.DataItem, "model")%>
						        </ItemTemplate>
					        </asp:TemplateColumn>
        					
					        <asp:TemplateColumn HeaderText="Bios" SortExpression="bios" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval(Container.DataItem, "bios")%>
						        </ItemTemplate>
					        </asp:TemplateColumn>
        					
					        <asp:TemplateColumn HeaderText="Processor" SortExpression="proc" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval(Container.DataItem, "proc")%>
						        </ItemTemplate>
					        </asp:TemplateColumn>
        					
					        <asp:TemplateColumn HeaderText="Memory" SortExpression="memory" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval(Container.DataItem, "memory")%>
						        </ItemTemplate>
					        </asp:TemplateColumn>
        					
					        <asp:TemplateColumn HeaderText="DirectX Ver" SortExpression="directx_version" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval (Container.DataItem, "directx_version")%>
						        </ItemTemplate>
					        </asp:TemplateColumn>
        					
					        <asp:TemplateColumn HeaderText="Card Name" SortExpression="card_name" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval (Container.DataItem, "card_name")%>
						        </ItemTemplate>
					        </asp:TemplateColumn>
        					
					        <asp:TemplateColumn HeaderText="Card Make" SortExpression="card_make" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval (Container.DataItem, "card_make")%>
						        </ItemTemplate>
					        </asp:TemplateColumn>
        					
					        <asp:TemplateColumn HeaderText="Card Chip Type" SortExpression="card_chip_type" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval (Container.DataItem, "card_chip_type")%>
						        </ItemTemplate>
					        </asp:TemplateColumn>
        					
					        <asp:TemplateColumn HeaderText="DAC Type" SortExpression="dac_type" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval (Container.DataItem, "dac_type")%>
						        </ItemTemplate>
					        </asp:TemplateColumn>
        					
					        <asp:TemplateColumn HeaderText="Card Memory" SortExpression="card_memory" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval (Container.DataItem, "card_memory")%>
						        </ItemTemplate>
					        </asp:TemplateColumn>
        					
					        <asp:TemplateColumn HeaderText="Display Mode" SortExpression="display_mode" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval (Container.DataItem, "display_mode")%>
						        </ItemTemplate>
					        </asp:TemplateColumn>
        					
					        <asp:TemplateColumn HeaderText="Download Size" SortExpression="downloaded_size" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval (Container.DataItem, "downloaded_size")%>
						        </ItemTemplate>
					        </asp:TemplateColumn>
        					
					        <asp:TemplateColumn HeaderText="Elapsed Time" SortExpression="elapsed_time" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval (Container.DataItem, "elapsed_time")%>
						        </ItemTemplate>
					        </asp:TemplateColumn>
        					
					        <asp:TemplateColumn HeaderText="DL Complete" SortExpression="download_completed" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval (Container.DataItem, "download_completed")%>
						        </ItemTemplate>
					        </asp:TemplateColumn>
        					
					        <asp:TemplateColumn HeaderText="Driver Ver" SortExpression="driver_version" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval (Container.DataItem, "driver_version")%>
						        </ItemTemplate>
					        </asp:TemplateColumn>
        					
					        <asp:TemplateColumn HeaderText="Driver Date/Size" SortExpression="driver_date_size" ItemStyle-Wrap="true">
						        <ItemTemplate>
							        <%# DataBinder.Eval (Container.DataItem, "driver_date_size")%>
						        </ItemTemplate>
					        </asp:TemplateColumn>
        					
				        </Columns>
                </asp:datagrid>
            </td>
         </tr>
    </table>
</asp:Panel>
