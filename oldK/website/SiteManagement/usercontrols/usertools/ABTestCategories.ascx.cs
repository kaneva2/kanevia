﻿using Kaneva.BusinessLayer.BusinessObjects;
using log4net;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Linq;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class ABTestCategories : BaseUserControl
    {
        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected int PageNum
        {
            get
            {
                if (ViewState["PageNum"] == null)
                {
                    return 1;
                }
                return (int)ViewState["PageNum"];
            }
            set
            {
                ViewState["PageNum"] = value;
            }
        }

        protected int PageSize
        {
            get { return 20; }
        }

        protected ExperimentCategory CurrentCategory
        {
            get
            {
                if (ViewState["CurrentCategory"] == null)
                {
                    return null;
                }
                return (ExperimentCategory)ViewState["CurrentCategory"];
            }
            set
            {
                ViewState["CurrentCategory"] = value;
            }
        }

        protected string CurrentSearchString
        {
            get
            {
                if (ViewState["CurrentSearchString"] == null)
                {
                    return string.Empty;
                }

                return ViewState["CurrentSearchString"].ToString();
            }
            set { ViewState["CurrentSearchString"] = value; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                // Check user access to content.
                if (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.KANEVA_PROPRIETARY) != (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL)
                {
                    RedirectToHomePage();
                    return;
                }

                if (!IsPostBack)
                {
                    // Set pagination parameters once, they're not changed in ResetPage().
                    PageNum = 1;

                    CurrentSearchString = string.Empty;

                    ResetPage();
                    BindData();
                }
            }
        }

        #region Helper Functions

        /// <summary>
        /// Resets page state to defaults, excluding pagination and search strings
        /// which are preserved.
        /// </summary>
        private void ResetPage()
        {
            CurrentCategory = null;

            CurrentSearchString = txtSearch.Text;
            pgTop.CurrentPageNumber = PageNum;

            txtName.Text = string.Empty;
            txtDescription.Text = string.Empty;

            divMsgTop.Style.Add("display", "none");
        }

        /// <summary>
        /// Configures the page to bind and show a listing of Experiments.
        /// </summary>
        private void ShowCategoryListings()
        {
            // TODO: Respect search string!
            PagedList<ExperimentCategory> categories = GetExperimentFacade().SearchExperimentCategories(CurrentSearchString, "category ASC", PageNum, PageSize);

            rptCategories.DataSource = categories;
            rptCategories.DataBind();

            lblSearch.Text = GetResultsText(categories.TotalCount, PageNum, PageSize, categories.Count);

            pgTop.NumberOfPages = Math.Ceiling((double)categories.TotalCount / PageSize).ToString();
            pgTop.DrawControl();

            divCategoryListing.Visible = true;
            divEditCategory.Visible = false;
            lbBackToListing.Visible = false;
        }

        /// <summary>
        /// Configures the page to bind and show an edit form for an Experiment.
        /// </summary>
        private void ShowCategoryForm()
        {
            txtName.Text = CurrentCategory.Name;
            txtDescription.Text = CurrentCategory.Description;

            divCategoryListing.Visible = false;
            divEditCategory.Visible = true;
            lbBackToListing.Visible = true;

            txtName.Enabled = string.IsNullOrWhiteSpace(CurrentCategory.Name);
            btnDeleteCategory.Visible = !string.IsNullOrWhiteSpace(CurrentCategory.Name);
        }

        /// <summary>
        /// Binds page data based on the current context.
        /// </summary>
        private void BindData()
        {
            if (CurrentCategory == null)
            {
                // Bind category listings.
                ShowCategoryListings();
            }
            else
            {
                // Show category details.
                ShowCategoryForm();
            }
        }

        /// <summary>
        /// Configures the error message for display.
        /// </summary>
        private void ShowMessage(string msg, bool isError)
        {
            divMsgTop.InnerText = msg;

            divMsgTop.Style.Add("display", "block");

            divMsgTop.Attributes["class"] = (isError ? "error" : "success");
        }

        #endregion

        #region Edit Events

        /// <summary>
        /// Configures the page for creating an category.
        /// </summary>
        protected void btnNewCategory_Click(Object sender, EventArgs e)
        {
            // Reset the page number.
            PageNum = 1;

            txtSearch.Text = string.Empty;

            ResetPage();
            CurrentCategory = new ExperimentCategory();
            BindData();
        }

        /// <summary>
        /// Configures the page for viewing category details.
        /// </summary>
        protected void btnEditCategory_Click(Object sender, CommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                ResetPage();
                CurrentCategory = GetExperimentFacade().GetExperimentCategory(e.CommandArgument.ToString());
                BindData();
            }
        }

        /// <summary>
        /// Delete a category and return to listings
        /// </summary>
        protected void btnDeleteCategory_Click(Object sender, EventArgs e)
        {
            if (GetExperimentFacade().DeleteExperimentCategory(CurrentCategory))
            {
                // Reset the page number.
                PageNum = 1;

                ResetPage();
                BindData();

                ShowMessage("Category deleted successfully.", false);
            }
            else
            {
                ShowMessage("Error deleting category.", true);
            }
        }

        /// <summary>
        /// Saves changes to a category to the database.
        /// </summary>
        protected void btnUpdateCategory_Click(Object sender, EventArgs e)
        {
            try
            {
                CurrentCategory.Name = (string.IsNullOrWhiteSpace(txtName.Text) ? CurrentCategory.Name : txtName.Text);
                CurrentCategory.Description = txtDescription.Text;

                if (!GetExperimentFacade().SaveExperimentCategory(CurrentCategory))
                {
                    ShowMessage("Error updating category.", true);
                }
                else
                {
                    ResetPage();
                    BindData();

                    ShowMessage("Category saved successfully.", false);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, true);
            }
        }

        protected void lbBackToListing_Click(Object sender, EventArgs e)
        {
            ResetPage();
            BindData();
        }

        #endregion

        #region Pagination

        /// <summary>
        /// Rebinds experiment data on pagination change.
        /// </summary>
        private void pgTop_PageChange(object sender, PageChangeEventArgs e)
        {
            PageNum = e.PageNumber;
            BindData();
        }

        /// <summary>
        /// Rebinds experiment data on search text.
        /// </summary>
        protected void btnSearch_Click(Object sender, EventArgs e)
        {
            if (!SiteManagementCommonFunctions.ContainsInjectScripts(txtSearch.Text))
            {
                CurrentSearchString = txtSearch.Text;
            }

            // Reset the page number.
            PageNum = 1;

            ResetPage();
            BindData();
        }

        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler(pgTop_PageChange);
        }

        #endregion Web Form Designer generated code

    }
}