using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Configuration;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;

using KlausEnt.KEP.Kaneva;
using System.Net;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;


namespace KlausEnt.KEP.SiteManagement
{
    public partial class UserTransactions : BaseUserControl
    {
        #region Declarations

        private ILog m_logger = LogManager.GetLogger("Billing");

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.USER_DATA_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                if (Session["SelectedUserId"] != null)
                {
                    // Load the current user's data the first time
                    SELECTED_USER_ID = Convert.ToInt32(Session["SelectedUserId"]);

                    if (!IsPostBack)
                    {
                        //populate pull downs
                        PopulateTransTypeDropList();

                        DisplayMessageBox(false, "", false);

                        lblBalanceRewards.Text = UsersUtility.getUserBalance(SELECTED_USER_ID, "GPOINT").ToString();
                        lblBalanceCredits.Text = UsersUtility.getUserBalance(SELECTED_USER_ID, "KPOINT").ToString();

                        spnEventName.InnerHtml = "";

                        drpEventName.Items.Clear();
                        drpEventName.Enabled = false;

                        drpTransactionTypes.SelectedIndex = 0;
                        txtAmount.Text = "";
                        txtNote.Text = "";

                        // Log the CSR activity
                        this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR -  Viewing user transactions", 0, SELECTED_USER_ID);
                    }
                    //populate post list grid
                    BindData(1, "");
                }
                else
                {
                    Session["CallingPage"] = Request.Url.ToString();
                    Response.Redirect(ResolveUrl("~/Default.aspx?ws=" + GetCurrentSiteID((int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.KANEVA) + "&mn=" + GetUserSearchParentMenuID() + "&sn=" + GetUserSearchControlID()));
                }

            }
            else
            {
                RedirectToHomePage();
            }
        }

        #endregion

        #region Functions

        /// <summary>
        /// Populate the Transaction Type Drop List
        /// </summary>
        private void PopulateTransTypeDropList()
        {
            // Set filter to only show reward event transaction types.
            string filter = "transaction_type IN (6,15,22,23,24)";
            string orderBy = "trans_desc";

            // Get the transaction types
            DataTable dtTransTypes = UsersUtility.GetTransactionTypes(filter, orderBy);

            if (dtTransTypes.Rows.Count > 0)
            {
                drpTransactionTypes.DataSource = dtTransTypes;

                // Set the text a value fields
                drpTransactionTypes.DataTextField = "trans_desc";
                drpTransactionTypes.DataValueField = "transaction_type";

                // Bind the data to the drop list
                drpTransactionTypes.DataBind();

                // Insert a 'select...' message as first item in list
                drpTransactionTypes.Items.Insert(0, new ListItem("select transaction type...", "0"));
                drpTransactionTypes.Enabled = true;
            }
            else
            {
                drpTransactionTypes.Items.Insert(0, new ListItem("no transaction types found", "-1"));
            }
        }

        /// <summary>
        /// Display message box with message
        /// </summary>
        private void DisplayMessageBox(bool isError, string msg)
        {
            DisplayMessageBox(isError, msg, true);
        }
        private void DisplayMessageBox(bool isError, string msg, bool isVisible)
        {
            if (!isVisible)
            {
                divMessages.Style.Add("display", "none");
            }
            else
            {
                string className = isError ? "errBox black" : "infoBox black";
                divMessages.Attributes.Add("class", className);

                divMessages.InnerHtml = msg;
                divMessages.Style.Add("display", "block");
            }
        }
        
        /// <summary>
        /// BindData
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="filter"></param>
        private void BindData(int pageNumber, string filter)
        {
            // Set the sortable columns
            string orderby = CurrentSort + " " + CurrentSortOrder;

            int iItemsPerPage = 20;

            PagedDataTable pds = UsersUtility.GetUserTransactions(SELECTED_USER_ID, orderby, pageNumber, iItemsPerPage);
            pgTop.NumberOfPages = Math.Ceiling((double)pds.TotalCount / iItemsPerPage).ToString();
            pgTop.DrawControl();

            dgrdInventory.DataSource = pds;
            dgrdInventory.DataBind();

            //display user name
            lblSelectedUser.Text = "Notes for " + SELECTED_USER_NAME;
        }

        private void ConfigureDisplay(int setting)
        {
            switch (setting)
            {
                case 1:
                    tr_AddCredits.Visible = true;
                    lbn_showAddCredits.Visible = false;
                    lbn_CancelAdd.Visible = true;
                    break;
                default:
                    lbn_showAddCredits.Visible = true;
                    lbn_CancelAdd.Visible = false;
                    tr_AddCredits.Visible = false;
                    break;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// pg_PageChange
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            BindData(e.PageNumber, "");
        }

        protected void UserInventory_Sorting(object sender, DataGridSortCommandEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            BindData(pgTop.CurrentPageNumber, "");
        }

        /// <summary>
        /// Event fired when user clicks the Save button
        /// </summary>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (txtAmount.Text.Trim() != "" && KanevaGlobals.IsNumeric(txtAmount.Text))
            {
                double amount = Convert.ToDouble(txtAmount.Text.Trim());

                // Need to test to see if the new amount is larger than the db field
                if (amount + UsersUtility.getUserBalance(SELECTED_USER_ID, drpCurrency.SelectedValue) > 1000000000)
                {
                    DisplayMessageBox(true, "The amount can not cause the balance to be more or less than 1 Billion.");
                }
                else
                {
                    try
                    {
                        // Verify transaction type is selected
                        if (drpTransactionTypes.SelectedValue == "0")
                        {
                            DisplayMessageBox(true, "You must select a Transaction Type.");
                            return;
                        }

                        UInt16 transType = Convert.ToUInt16(drpTransactionTypes.SelectedValue);

                        // Check to see if Event Name value is selected
                        int rewardEventId = 0;
                        try
                        {
                            if (drpEventName.Enabled && drpEventName.SelectedValue != string.Empty)
                            {
                                rewardEventId = Convert.ToInt32(drpEventName.SelectedValue);
                            }
                        }
                        catch { }

                        // If transaction tyep is a contest, event, or survey, verify 
                        // that the appropriate type event name is also selected
                        if (transType == Constants.CASH_TT_CONTEST ||
                            transType == Constants.CASH_TT_EVENT ||
                            transType == Constants.CASH_TT_SURVEY)
                        {
                            if (rewardEventId == 0)
                            {
                                DisplayMessageBox(true, "You must select an event name for this transaction.");
                                return;
                            }
                        }

                        if (rbDebit.Checked) { amount = amount * -1; }

                        int wokTransLogId = 0;
                        // Adjust the users balance with amount value
                        UserFacade userFacade = GetUserFacade();
                        userFacade.AdjustUserBalance(SELECTED_USER_ID, drpCurrency.SelectedValue, amount, transType, ref wokTransLogId);


                        // Display Success message
                        DisplayMessageBox(false, "Transaction successfully applied to users balance.");

                        // Insert record into reward_log table
                        userFacade.InsertRewardLog(SELECTED_USER_ID, amount, drpCurrency.SelectedValue, transType, rewardEventId, wokTransLogId);

                        // Set user note
                        string strUserNote = "Balance Modification. Trans Type: " + drpTransactionTypes.SelectedItem.Text + " Currency: " + drpCurrency.SelectedValue + " Amount: " + amount;
                        strUserNote += "<br /> Note: " + txtNote.Text;
                        UsersUtility.InsertUserNote(SELECTED_USER_ID, GetUserId(), strUserNote, wokTransLogId);
                    }
                    catch (Exception exc)
                    {
                        m_logger.Error("Error in modifying user balance.", exc);
                        DisplayMessageBox(true, "Error in modifying user balance. " + exc);
                        return;
                    }
                }
            }
            else
            {
                // Display Error
                DisplayMessageBox(true, "Amount must be a positive numeric value.");
                return;
            }
            ConfigureDisplay(0);
            BindData(1,"");
            lblBalanceRewards.Text = UsersUtility.getUserBalance(SELECTED_USER_ID, "GPOINT").ToString();
            lblBalanceCredits.Text = UsersUtility.getUserBalance(SELECTED_USER_ID, "KPOINT").ToString();

        }

        /// <summary>
        /// Event fired when selection changes in the transaction type drop list.
        /// Populates the event name drop list with related transaction type data
        /// </summary>
        public void dropTransactionTypes_OnChange(object sender, EventArgs e)
        {
            try
            {
                // Disable drop list and clear all values from drop list
                drpEventName.Enabled = false;
                drpEventName.Items.Clear();

                spnEventName.InnerHtml = "";

                DropDownList ddlTransType = (DropDownList)sender;

                if (ddlTransType != null && ddlTransType.SelectedValue != "")
                {
                    int transTypeId = Convert.ToInt32(ddlTransType.SelectedValue);

                    if (transTypeId == Constants.CASH_TT_CONTEST ||
                        transTypeId == Constants.CASH_TT_EVENT ||
                        transTypeId == Constants.CASH_TT_SURVEY)
                    {
                        // Get the event list matching transaction type               
                        DataTable dtEvents = UsersUtility.GetKanevaEventsByTransactionType(transTypeId);

                        if (dtEvents.Rows.Count > 0)
                        {
                            drpEventName.DataSource = dtEvents;

                            // Set the text a value fields
                            drpEventName.DataTextField = "event_name";
                            drpEventName.DataValueField = "reward_event_id";

                            // Bind the data to the drop list
                            drpEventName.DataBind();

                            // Insert a 'select...' message as first item in list
                            drpEventName.Items.Insert(0, new ListItem("select " + ddlTransType.SelectedItem.ToString().ToLower() + " name...", "0"));
                            drpEventName.Enabled = true;
                        }
                        else
                        {
                            drpEventName.Items.Insert(0, new ListItem("no " + ddlTransType.SelectedItem.ToString().ToLower() + "s found", "-1"));
                        }
                        // Update the label
                        spnEventName.InnerHtml = ddlTransType.SelectedItem + " Name:";
                    }
                }
            }
            catch (Exception)
            {
                drpEventName.Enabled = false;
                spnEventName.InnerHtml = "";
            }
        }

        protected void ConfigureAddDisplay_Command(Object sender, CommandEventArgs e)
        {
            ConfigureDisplay(Convert.ToInt32(e.CommandArgument));
        }

        #endregion


        #region Attributes

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "created_date";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "DESC";
            }
        }

        private int SELECTED_USER_ID
        {
            get
            {
                return (int)ViewState["users_id"];
            }
            set
            {
                ViewState["users_id"] = value;
            }

        }

        private string SELECTED_USER_NAME
        {
            get
            {
                if (ViewState["users_name"] == null)
                {
                    ViewState["users_name"] = UsersUtility.GetUserNameFromId(SELECTED_USER_ID);
                }

                return ViewState["users_name"].ToString();
            }
            set
            {
                ViewState["users_name"] = value;
            }

        }

        protected string inventoryPending(string location)
        {
            if (location == "Pnd Inv")
            {
                return "Pending";
            }
            else
            {
                return "";
            }
        }



        protected string inventoryLocation(string location)
        {
            if (location == "P")
            {
                return "Player";
            }
            else if (location == "B")
            {
                return "Bank";
            }
            else
            {
                return "";
            }
        }

        protected string creditType(string type)
        {
            if (type == "512")
            {
                return "Rewards";
            }
            else if (type == "256")
            {
                return "Credits";
            }
            else
            {
                return "";
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
        }

        #endregion
    }
}