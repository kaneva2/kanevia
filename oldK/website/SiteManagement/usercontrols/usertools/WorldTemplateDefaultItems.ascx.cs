using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.Mailer;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using KlausEnt.KEP.Kaneva;
using System.IO;
using KlausEnt.KEP.SiteManagement;
using System.Collections.Generic;

namespace SiteManagement.usercontrols
{
    public partial class WorldTemplateDefaultItems : BaseUserControl
    {
        #region Declarations

        private int[] insertGlids;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
		{
            if (SelectedTemplateId >= 0)
            {
                if (!IsPostBack)
                {
                    ResetPage();
                }
            }
            else
            {
                RedirectToHomePage();
            }
		}

        #region Data Manipulation
        /// <summary>
        /// Called to bind data to page elements
        /// </summary>
        protected void BindData()
		{
            int itemsPerPage = 15;

            PagedList<WOKItem> defaultItems = GetGameFacade().GetWorldTemplateDefaultItems(SelectedTemplateId, pgBottom.CurrentPageNumber, itemsPerPage);
            cblDefaultItems.DataSource = defaultItems;
            cblDefaultItems.DataBind();

            pgBottom.NumberOfPages = Math.Ceiling((double)defaultItems.TotalCount / itemsPerPage).ToString();
            pgBottom.DrawControl();

            lblSearch.Text = GetResultsText(defaultItems.TotalCount, pgBottom.CurrentPageNumber, itemsPerPage, defaultItems.Count);

            if (defaultItems.Count > 0)
            {
                btnDeleteSelected.Visible = true;
            }
		}

        /// <summary>
        /// Called to reset the page
        /// </summary>
        protected void ResetPage(int pageNum = 1)
        {
            divErrorData.InnerText = string.Empty;
            txtDefaultItems.Text = string.Empty;
            pgBottom.CurrentPageNumber = pageNum;

            BindData();
        }

        /// <summary>
        /// Called to validate the page before saving
        /// </summary>
        protected bool ValidatePage()
        {
            bool inputsAreValid = true;

            try
            {
                string[] glidStrs = txtDefaultItems.Text.Trim(',').Replace(" ", "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (glidStrs.Length > 0)
                {
                    InsertGlids = Array.ConvertAll(glidStrs, new Converter<string, int>(Convert.ToInt32));
                }
            }
            catch (Exception)
            {
                inputsAreValid = false;
            }

            return inputsAreValid;
        }
        #endregion

        #region Event Handlers
        protected void btnDeleteSelected_Click(object sender, EventArgs e)
		{
            List<int> selectedValues = new List<int>();
            foreach (ListItem item in cblDefaultItems.Items)
            {
                if (item.Selected)
                {
                    selectedValues.Add(Int32.Parse(item.Value));
                }
            }

            if (selectedValues.Count > 0)
            {
                GetGameFacade().DeleteWorldTemplateDefaultItemIds(SelectedTemplateId, selectedValues.ToArray());
                ResetPage();
            }
		}

		protected void btnAddItems_Click(object sender, EventArgs e)
		{
            if (Page.IsValid && ValidatePage() && InsertGlids.Length > 0)
            {
                int recordsInserted = GetGameFacade().InsertWorldTemplateDefaultItemIds(SelectedTemplateId, InsertGlids);
                if (recordsInserted <= 0)
                {
                    divErrorData.InnerText = "Unable to add default items. Check to ensure that globalIds are correct.";
                }
                else
                {
                    ResetPage();
                }
            }
            else
            {
                divErrorData.InnerText = "Invalid inputs.";
            }
		}

        protected void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            ResetPage(e.PageNumber);
        }

        #endregion

        #region Properties

        public int SelectedTemplateId
        {
            get
            {
                if (ViewState["SelectedTemplateId"] == null)
                {
                    int intOut = -1;
                    Int32.TryParse((Session["SelectedTemplateId"] ?? "").ToString(), out intOut);

                    return intOut;
                }
                return (int)ViewState["SelectedTemplateId"];
            }
            set
            {
                ViewState["SelectedTemplateId"] = value;
            }
        }

        public int[] InsertGlids
        {
            get
            {
                if (insertGlids == null)
                {
                    insertGlids = new int[0];
                    return insertGlids;
                }
                return insertGlids;
            }
            set
            {
                insertGlids = value;
            }
        }

        #endregion
    }  
}