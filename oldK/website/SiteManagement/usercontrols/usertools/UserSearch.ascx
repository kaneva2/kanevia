<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="UserSearch.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.UserSearch" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="MembersFilter" Src="../../usercontrols/MembersFilter.ascx" %>

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
<center><br />
 <table cellpadding="0" cellspacing="0" border="0" width="750" >
    <tr>
            <td colspan="2" width="650" style="background-color: #ededed;" align="left">&nbsp;&nbsp;&nbsp;<span style="size:14px; font-weight:bold;">Search Users</span>&nbsp;<span style="color:orange;font-family:arial; font-size: 12px;">(Use single words only, search will return all matches that start with the word)</span></td>
    </tr>
    <tr>
            <td align="right" style="border-left: 1px solid #ededed; padding-right:20px; width:100px;">&nbsp;<b>username:</b></td>
            <td align="left" width="705" class="bodyText" style="border-right: 1px solid #ededed;">
                 <asp:TextBox ID="txtUsername" class="Filter2" style="width:300px" MaxLength="31" runat="server"/>
            </td>
    </tr>
    <tr>
            <td align="right" style="border-left: 1px solid #ededed; padding-right:20px; width:100px;">&nbsp;<b>email:</b></td>
            <td align="left" width="705" class="bodyText" bgcolor="#ededed" style="border-right: 1px solid #ededed;">
                    <asp:TextBox ID="txtEmail" class="Filter2" style="width:300px" MaxLength="31" runat="server"/>
            </td>
    </tr>
    <tr>
            <td align="right" style="border-left: 1px solid #ededed; padding-right:20px; width:100px;">&nbsp;<b>first name:</b></td>
            <td align="left" width="705" class="bodyText" style="border-right: 1px solid #ededed;">
                    <asp:TextBox ID="txtFirstName" class="Filter2" style="width:300px" MaxLength="31" runat="server"/>
            </td>
    </tr>
    <tr>
            <td align="right" style="border-left: 1px solid #ededed; padding-right:20px; width:100px;"><b>last name:</b></td>
            <td align="left" width="705" bgcolor="#ededed" class="bodyText" style="border-right: 1px solid #ededed;">
				<asp:TextBox ID="txtLastName" class="Filter2" style="width:300px" MaxLength="31" runat="server"/>  
				
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:button id="btnSearch" align="right" runat="Server" class="Filter2" Text="    Search    " onClick="btnSearch_Click" CausesValidation="False"/>          
			</td>
    </tr>  
     <tr>
            <td align="right" style="border-left: 1px solid #ededed; padding-right:20px; width:100px;">&nbsp;<b>status:</b></td>
            <td align="left" width="705" class="bodyText" style="border-right: 1px solid #ededed;">
                    <asp:DropDownList class="filter2" id="drpStatus" runat="Server" style="width:200px"/>
            </td>
    </tr>
    <tr>
            <td colspan="2">
            </td>
    </tr>
	<tr>
		<td colspan="2" align="right" class="belowFilter2" valign="middle" style="line-height:15px;">
			<asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop"/><br><Kaneva:MembersFilter runat="server" id="filMembers" RolesEnabled="false" StatusEnabled="false" InitialItemsPerPage="30"/>
		</td>						
	</tr>
</table>
</center>
<br />
<center>

<asp:DataGrid runat="server" EnableViewState="true" ShowFooter="False" Width="970" id="dgrdMembers" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" OnSortCommand="User_Sorting" CssClass="FullBorders">  
	<HeaderStyle BackColor="#cccccc" ForeColor="#000000" HorizontalAlign="Center" Font-Underline="false" Font-Bold="true" />
	<ItemStyle BackColor="#ffffff" />
	<AlternatingItemStyle BackColor="#eeeeee" />
	<Columns>
		<asp:TemplateColumn HeaderText="members" SortExpression="username" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="26%">
			<ItemTemplate>
			    <asp:LinkButton id="selectedUserId" OnCommand="SelectUser_Command" style="COLOR: #3258ba; font-size: 12px;" runat="server" CommandArgument='<%# Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "user_id"))%>'><%# DataBinder.Eval(Container.DataItem, "username") %></asp:LinkButton>
				<br/><B>(<%# DataBinder.Eval(Container.DataItem, "first_name") %>&nbsp;<%# DataBinder.Eval(Container.DataItem, "last_name") %>)</B>
				<br/><%# DataBinder.Eval(Container.DataItem, "email") %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="# logins" SortExpression="number_of_logins" ItemStyle-Width="7%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
			<ItemTemplate>
				<span class="money"><%# DataBinder.Eval(Container.DataItem, "number_of_logins") %></span>&nbsp;
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="birth date" SortExpression="birth_date" ItemStyle-Width="10%" ItemStyle-ForeColor="#4863a2" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
			<ItemTemplate>
				<span class="adminLinks"><%# FormatDate (DataBinder.Eval(Container.DataItem, "birth_date")) %></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="gender" SortExpression="gender" ItemStyle-Width="4%" ItemStyle-ForeColor="#4863a2" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "gender") %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="joined" SortExpression="user_id" ItemStyle-Width="14%" ItemStyle-ForeColor="#4863a2" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
			<ItemTemplate>
				<span class="adminLinks"><%# FormatDateTime (DataBinder.Eval(Container.DataItem, "signup_date")) %></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="last active" SortExpression="last_login" ItemStyle-Width="14%" HeaderStyle-Wrap="False" ItemStyle-ForeColor="#4863a2" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
			<ItemTemplate>
				<span class="adminLinks"><%# FormatDateTime (DataBinder.Eval(Container.DataItem, "last_login")) %></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="status" SortExpression="user_status_name" ItemStyle-Width="15%" HeaderStyle-Wrap="False" ItemStyle-ForeColor="#4863a2" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Top">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "user_status_name") %>								
			</ItemTemplate>
		</asp:TemplateColumn>
		
	</Columns>
</asp:datagrid>

</center>

</asp:Panel>