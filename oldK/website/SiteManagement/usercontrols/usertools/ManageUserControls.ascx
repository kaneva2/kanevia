<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ManageUserControls.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.ManageUserControls" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

  
<script type="text/JavaScript">

function CheckAll(div, checkBox) 
{
    var checkAllBox = document.getElementById(checkBox).checked;
    try
    {
	    SelectAllByParent(div, checkAllBox);
	}
	catch(e)
	{
	    alert('unable to auto select, please select your choices' );
	}
}

</script>



<asp:Panel ID="pnl_Security" runat="server" Enabled="true" style="padding-left:4px">
    

    <ajax:ajaxpanel id="ajMessage" runat="server">

    <asp:label style="position:relative; font-size:18px; padding: 10px 0 10px 0" runat="server" id="messages" visible="true" />

    <div id="availableUserControls" style="font-size:12px; height:96%; ">
        <br />
		<asp:LinkButton id="lbn_NewUserControl" runat="server" causesvalidation="false" onclick="lbn_NewUserControl_Click">Add New User Control</asp:LinkButton>
        <span><hr style="width:978px" title="Available User Control" /></span>
		<div style="overflow:auto; width:978; height:90%; ">
        <asp:Repeater ID="rpt_AvailableUserControls" runat="server" EnableViewState="false" >
            <HeaderTemplate>
                    <table style="width:960px" border="0">
                        <tr>
						    <th style="width:15px">
							    <input type="checkbox" id="cbxSelectAll" onclick="CheckAll('availableUserControls','cbxSelectAll')" class="Filter2" />
						    </th>
                            <th style="width:100px; text-align:center">UserControl Id</th>
                            <th style="text-align:left">Control Name/ Report Path</th>
                            <th style="text-align:left">Report Type</th>
                            <th style="width:114px; text-align:left"></th>
				        </tr>
            </HeaderTemplate>
            <ItemTemplate>
                        <tr>
                            <td style="width:15px">
                                 <asp:checkbox id="chkEdit" CssClass="Filter2" runat="server" Visible="true"/>
                            </td>
                            <td style="width:100px; text-align:center">
                                <asp:label id="lbl_ControlId" runat="server" visible="true" />
                            </td>
                            <td style="text-align:left">
							    <asp:TextBox id="tbx_ControlName" runat="server" Width="96%" visible="true" Enabled="false" />
                            </td>
                            <td style="text-align:left">
                                <asp:DropDownList ID="ddl_ControlType" Width="150px" runat="server" Enabled="false" AutoPostBack="false" /><br />
                            </td>
                             <td style="width:114px; text-align:left">
								<asp:LinkButton id="lnkEdit" Text="Edit" ToolTip="puts the row into edit mode" CommandName="cmdEdit" runat="server" style="text-decoration:underline;" />
								<asp:LinkButton id="lnkUpdate" Visible="false" Text="Update" ToolTip="Updates field with your changes" CommandName="cmdUpdate" runat="server" style="text-decoration:underline;" />
								<asp:LinkButton id="lnkCancel" Visible="false" Text="Cancel" ToolTip="Cancels operation" CommandName="cmdCancel" runat="server" style="text-decoration:underline;" />
                            </td>
                       </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                        <tr>
                            <td style="width:15px">
                                 <asp:checkbox id="chkEdit" CssClass="Filter2" runat="server" Visible="true"/>
                            </td>
                            <td style="width:100px; text-align:center">
                                <asp:label id="lbl_ControlId" runat="server" visible="true" />
                            </td>
                            <td style="text-align:left">
							    <asp:TextBox id="tbx_ControlName" runat="server" Width="96%" visible="true" Enabled="false" />
                            </td>
                            <td style="text-align:left">
                                <asp:DropDownList ID="ddl_ControlType" Width="150px" runat="server" Enabled="false" AutoPostBack="false" /><br />
                            </td>
                             <td style="width:114px; text-align:left">
								<asp:LinkButton id="lnkEdit" Text="Edit" ToolTip="puts the row into edit mode" CommandName="cmdEdit" runat="server" style="text-decoration:underline;" />
								<asp:LinkButton id="lnkUpdate" Visible="false" Text="Update" ToolTip="Updates field with your changes" CommandName="cmdUpdate" runat="server" style="text-decoration:underline;" />
								<asp:LinkButton id="lnkCancel" Visible="false" Text="Cancel" ToolTip="Cancels operation" CommandName="cmdCancel" runat="server" style="text-decoration:underline;" />
                            </td>
                       </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        </div>
        <div style="padding-top:10px">
            <asp:Button ID="btn_DeleteUserControls" runat="server" Text="Delete Selected" onclick="btn_DeleteUserControls_Click"  />
        </div>
    </div>
       
    <div class="clear"><!-- clear float --></div>
    </ajax:ajaxpanel>
    
    </asp:Panel>     