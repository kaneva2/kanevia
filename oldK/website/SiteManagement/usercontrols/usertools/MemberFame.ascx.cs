using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class MemberFame : BaseUserControl
    {

        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        protected void Page_Load (object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.USER_DATA_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                this.lblErrMessage.Text = "";

                if (!IsPostBack)
                {
                    FameSearchFilter = "";
                    
                    // bind the member fame data
                    BindData (1);
                }

                //clear message
                messages.Text = "";
            }
            else
            {
                RedirectToHomePage();
            }

        }

        #region Helper Functions


        #endregion

        #region Functions

        /// <summary>
        /// BindMemberFameData
        /// </summary>
        private void BindData (int pageNumber)
        {
            try
            {
                // Set current page
                pgUserFame.CurrentPageNumber = pageNumber;
                
                // Set the sortable columns
                string orderby = CurrentSort + " " + CurrentSortOrder;
                int pageSize = 20;

                // Get the User Fame Info
                FameFacade fameFacade = GetFameFacade();
                PagedList<UserFame> userFame = fameFacade.GetUserFame ((int) FameTypes.World, FameSearchFilter, orderby, pageNumber, pageSize);
                                           
                if ((userFame != null) && (userFame.Count > 0))
                {
                    lblNoUserFameItems.Visible = false;

                    //dgMemberFame.PageCount = userFame.TotalCount / pageSize;
                    dgUserFame.DataSource = userFame;
                    dgUserFame.DataBind ();

                    // Show Pager
                    pgUserFame.NumberOfPages = Math.Ceiling ((double) userFame.TotalCount / pageSize).ToString ();
                    pgUserFame.DrawControl ();
                }
                else
                {
                    lblNoUserFameItems.Visible = true;
                }

                lblErrMessage.Visible = false;
            }
            catch (Exception ex)
            {
                lblErrMessage.Text = "Error: " + ex.Message;
                lblErrMessage.Visible = true;
            }
        }

        /// <summary>
        /// PopulateWOKItemEdit
        /// </summary>
        private void GetMemberFameDetails (GridViewRow gvr, int pageNumber)
        {
            this.UserId = Convert.ToInt32 (gvr.Cells[0].Text);
            string userName = gvr.Cells[1].Text;

            //make the details panel visible
            divUserFameDetails.Style["display"] = "block";

            divUsername.InnerHtml = "<b>" + userName + " (" + this.UserId + ")</b>";

            try
            {
                GetPacketHistory (pageNumber);

                GetLevelHistory (pageNumber);

                lblErrMessage.Visible = false;
            }
            catch (Exception ex)
            {
                lblErrMessage.Text = "Error displaying member fame details: " + ex.Message;
                lblErrMessage.Visible = true;
            }
        }

        private void GetPacketHistory (int pageNumber)
        {
            string orderBy = PacketCurrentSort + " " + PacketCurrentSortOrder;
            int pageSize = 20;
            
            // Get the Users Packet History
            FameFacade fameFacade = new FameFacade ();
            PagedList<PacketHistory> packetHistory = fameFacade.GetPacketHistory (this.UserId, (int) FameTypes.World, "", orderBy, pageNumber, pageSize);

            dgPacketHistory.DataSource = packetHistory;
            dgPacketHistory.DataBind ();

            // Set current page
            pgPacketHistory.CurrentPageNumber = pageNumber;
            pgPacketHistory.NumberOfPages = Math.Ceiling ((double) packetHistory.TotalCount / pageSize).ToString ();
            pgPacketHistory.DrawControl ();
        }

        private void GetLevelHistory (int pageNumber)
        {
            string orderBy = LevelCurrentSort + " " + LevelCurrentSortOrder;
            int pageSize = 20;

            // Get the Users Level History
            FameFacade fameFacade = new FameFacade ();
            PagedList<LevelHistory> levelHistory = fameFacade.GetLevelHistory (this.UserId, (int) FameTypes.World, "", orderBy, pageNumber, pageSize);

            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser (this.UserId);
            
            LevelHistory levelZero = new LevelHistory (this.UserId, (int) FameTypes.World, 0, user.SignupDate, 0, 0);
            levelHistory.Insert (0, levelZero);
            
            dgLevelHistory.DataSource = levelHistory;
            dgLevelHistory.DataBind ();

            // Set current page
            pgLevelHistory.CurrentPageNumber = pageNumber;
            pgLevelHistory.NumberOfPages = Math.Ceiling ((double) levelHistory.TotalCount / pageSize).ToString ();
            pgLevelHistory.DrawControl ();
            //string.Format("{0:00}:{1:00}", seconds / 3600, seconds % 3600 / 60);
        }

        #endregion


        #region Event Handlers

        protected void btnSearch_Click (object sender, EventArgs e)
        {
            string searchText = txtSearch.Text.Trim ();

            if ((searchText != "") && (SiteManagementCommonFunctions.IsNumeric(searchText)))
            {
                FameSearchFilter = "uf.user_id = " + searchText;
            }
            else
            {
                if (searchText == "")
                {
                    FameSearchFilter = "";
                }
                else
                {
                    FameSearchFilter = "username LIKE '%" + searchText + "%'";
                }
            }

            BindData (1);
        }

        protected void dgUserFame_RowEditing (object source, GridViewEditEventArgs e)
        {
            //passin the selected row and populate the fields
            GetMemberFameDetails (dgUserFame.Rows[e.NewEditIndex],1);
        }

        /// <summary>
        /// BindMemberFameData
        /// </summary>
        protected void dgUserFame_Sorting (Object sender, GridViewSortEventArgs e)
        {
            SortSwitch (e.SortExpression); //sets the sort expression
            BindData (1);
        }

        protected void dgLevelHistory_RowDataBound (object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[2] != null && e.Row.Cells[2].Text != "")
                {
                    try
                    {
                        double seconds = Convert.ToDouble (e.Row.Cells[2].Text);
                        TimeSpan ts = TimeSpan.FromSeconds (seconds);
                        e.Row.Cells[2].Text = ts.ToString ();
                    }
                    catch { }
                }
            }
        }

        /// <summary>
        /// Execute when the user selects a page change link from the Pager control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pg_UserFame_PageChange (object sender, PageChangeEventArgs e)
        {
            BindData (e.PageNumber);
        }
        protected void pg_PacketHistory_PageChange (object sender, PageChangeEventArgs e)
        {
            GetPacketHistory (e.PageNumber);
        }
        protected void pg_LevelHistory_PageChange (object sender, PageChangeEventArgs e)
        {
            GetLevelHistory (e.PageNumber);
        }

        #endregion


        #region Properties

        private string FameSearchFilter
        {
            get
            {
                if (ViewState["_FameSearchFilter"] == null)
                {
                    ViewState["_FameSearchFilter"] = "";
                }
                return ViewState["_FameSearchFilter"].ToString ();
            }
            set
            {
                ViewState["_FameSearchFilter"] = value;
            }
        }
        private int UserId
        {
            get
            {
                if (ViewState["_FameUserId"] == null)
                {
                    ViewState["_FameUserId"] = 0;
                }
                return Convert.ToInt32(ViewState["_FameUserId"]);
            }
            set
            {
                ViewState["_FameUserId"] = value;
            }
        }

        #endregion

        #region User Fame Properties
        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "user_id";
            }
        }
        /// <summary>
        /// DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }
        #endregion

        #region Packet History Properties
        /// <summary>
        /// PACKET DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        private string PACKET_DEFAULT_SORT
        {
            get
            {
                return "packet_history_id";
            }
        }
        /// <summary>
        /// PACKET DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        private string PACKET_DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }
        /// <summary>
        /// Packet History Current sort expression
        /// </summary>
        private string PacketCurrentSort
        {
            get
            {
                if (ViewState["pcs"] == null)
                {
                    return PACKET_DEFAULT_SORT;
                }
                else
                {
                    return ViewState["pcs"].ToString ();
                }
            }
            set
            {
                ViewState["pcs"] = value;
            }
        }
        /// <summary>
        /// Packet History Current sort order
        /// </summary>
        private string PacketCurrentSortOrder
        {
            get
            {
                if (ViewState["pcso"] == null)
                {
                    return PACKET_DEFAULT_SORT_ORDER;
                }
                else
                {
                    return ViewState["pcso"].ToString ();
                }
            }
            set
            {
                ViewState["pcso"] = value;
            }
        }
        #endregion

        #region Level History Properties
        /// <summary>
        /// LEVEL DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        private string LEVEL_DEFAULT_SORT
        {
            get
            {
                return "level_number";
            }
        }
        /// <summary>
        /// LEVEL DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        private string LEVEL_DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }
        /// <summary>
        /// Level History Current sort expression
        /// </summary>
        private string LevelCurrentSort
        {
            get
            {
                if (ViewState["lcs"] == null)
                {
                    return LEVEL_DEFAULT_SORT;
                }
                else
                {
                    return ViewState["lcs"].ToString ();
                }
            }
            set
            {
                ViewState["lcs"] = value;
            }
        }
        /// <summary>
        /// Level History Current sort order
        /// </summary>
        private string LevelCurrentSortOrder
        {
            get
            {
                if (ViewState["lcso"] == null)
                {
                    return LEVEL_DEFAULT_SORT_ORDER;
                }
                else
                {
                    return ViewState["lcso"].ToString ();
                }
            }
            set
            {
                ViewState["lcso"] = value;
            }
        }
        #endregion
        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion


    }
}