using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using MagicAjax;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Net;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class BannedIPAddress : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.USER_DATA_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                this.lblErrMessage.Text = "";

                if (!IsPostBack)
                {
                    // bind the member fame data
                    BindData(1);
                }

            }
            else
            {
                RedirectToHomePage();
            }
        }


        private void BindData(int pageNumber)
        {
            try
            {
                // Set current page
                pgBannedIPs.CurrentPageNumber = pageNumber;

                // Set the sortable columns
                string orderby = CurrentSort + " " + CurrentSortOrder;
                int pageSize = 20;

                // Get the Banned IPs
                PagedDataTable pdtBannedIPs = UsersUtility.GetBannedIPs(DateTime.MinValue, DateTime.MaxValue, "", orderby, pgBannedIPs.CurrentPageNumber, 50);


                if (pdtBannedIPs.Rows.Count > 0)
                {
                    lblNoBannedIPs.Visible = false;

                    //dgMemberFame.PageCount = userFame.TotalCount / pageSize;
                    rptBannedIPs.DataSource = pdtBannedIPs;
                    rptBannedIPs.DataBind();

                    // Show Pager
                    pgBannedIPs.NumberOfPages = Math.Ceiling((double)pdtBannedIPs.TotalCount / pageSize).ToString();
                    pgBannedIPs.DrawControl();
                }
                else
                {
                    lblNoBannedIPs.Visible = true;
                }

                lblErrMessage.Visible = false;
            }
            catch (Exception ex)
            {
                lblErrMessage.Text = "Error: " + ex.Message;
                lblErrMessage.Visible = true;
            }
        }

        protected void rptBannedIPs_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            //TextBox tbxSortOrder = (TextBox) e.Item.FindControl ("tbxSortOrder");
            //tbxSortOrder.AutoPostBack = true;
            //tbxSortOrder.TextChanged += new EventHandler (tbxSortOrder_TextChanged);
            //HtmlAnchor thumbnail = (HtmlAnchor) e.Item.FindControl ("thumbnail");
            //HtmlAnchor mediaTitle = (HtmlAnchor) e.Item.FindControl ("mediaTitle");

            ////create the tool summary data
            //string title = "\\<b>Name: </b>" + Convert.ToString (DataBinder.Eval (e.Item.DataItem, "name")) + "<br>";
            //string description = "\\<b>Description: </b>" + Convert.ToString (DataBinder.Eval (e.Item.DataItem, "teaser")) + "<br>";
            //string owner = "\\<b>Owner: </b>" + Convert.ToString (DataBinder.Eval (e.Item.DataItem, "username")) + "<br>";
            //string restricted = "\\<b>Rating: </b>" + (Convert.ToBoolean (DataBinder.Eval (e.Item.DataItem, "mature_profile")) ? "Restricted" : "General Audience") + "<br>";
            //string assetType = "\\<b>Type: </b>" + GetAssetTypeName (Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "asset_type_id"))) + "<br>";
            //string _public = "\\<b>Access: </b>" + (Convert.ToString (DataBinder.Eval (e.Item.DataItem, "public")) == "Y" ? "Public" : "Private") + "<br>";
            //string fileSize = "\\<b>Size: </b>" + Math.Round ((Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "file_size"))) / 1000.0, 2) + " Mb" + "<br>";
            //string createdOn = "\\<b>Added On: </b>" + FormatDateTime (Convert.ToDateTime (DataBinder.Eval (e.Item.DataItem, "date_added"))) + "<br>";

            //string toolTip = "\\ <h4>Asset Details</h4>" + title + description + owner + restricted + assetType + _public + fileSize + createdOn;
            //thumbnail.Attributes.Add ("onmouseover", "whiteBalloonSans.showTooltip(event,'" + KanevaGlobals.CleanJavascriptFull (toolTip) + "')");
            //mediaTitle.Attributes.Add ("onmouseover", "whiteBalloonSans.showTooltip(event,'" + KanevaGlobals.CleanJavascriptFull (toolTip) + "')");
        }

        protected void rptBannedIPs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

            }
        }

        /// <summary>
        /// rptBannedIPs_ItemCommand
        /// </summary>
        protected void rptBannedIPs_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string command = e.CommandName;

            HtmlInputHidden hidIPAddress;

            switch (command)
            {
                case "cmdUnBan":
                    {
                        int index = e.Item.ItemIndex;
                        hidIPAddress = (HtmlInputHidden)rptBannedIPs.Items[index].FindControl("hidIPAddress");

                        UsersUtility.RemoveIPBan(hidIPAddress.Value);

                        BindData(1);
                        break;
                    }
            }
        }



        //protected void dgBannedIPs_OnRowCreated (object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        LinkButton lbUnBanIP = (LinkButton) e.Row.FindControl ("lbUnBanIP");
        //        lbUnBanIP.CommandArgument = DataBinder.Eval (e.Row.DataItem, "ip_address").ToString ();
        //        lbUnBanIP.CommandName = "UnBan";
        //        lbUnBanIP.ToolTip = "Un-Ban IP " + DataBinder.Eval (e.Row.DataItem, "ip_address").ToString ();
        //    }
        //}

        /// <summary>
        /// Execute when the user selects a page change link from the Pager control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pgBannedIPs_PageChange(object sender, PageChangeEventArgs e)
        {
            BindData(e.PageNumber);
        }

        protected void btnAddIPBan_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime ipBanStart = DateTime.Now;
                DateTime ipBanEnd = DateTime.Now;

                if (string.IsNullOrWhiteSpace(txtAddBan.Text))
                {
                    lblErrMessage.Text = "Error: Please enter an IP address to ban.";
                    lblErrMessage.Visible = true;
                    return;
                }

                // Split up the comma-delimited IPs
                string[] iPsToBan = txtAddBan.Text.Split(new char[] {','},StringSplitOptions.RemoveEmptyEntries);

                if (iPsToBan.Length == 0)
                {
                    lblErrMessage.Text = "Error: Please enter a valid IP address to ban.";
                    lblErrMessage.Visible = true;
                    return;
                }

                // Validate individual IPs
                IPAddress outAddress;
                foreach (string ip in iPsToBan)
                {
                    if (!IPAddress.TryParse(ip, out outAddress))
                    {
                        lblErrMessage.Text = string.Format("Error: {0} is not a valid IP address.", ip);
                        lblErrMessage.Visible = true;
                        return;
                    }

                    if (UsersUtility.IsIPBanned(ip))
                    {
                        lblErrMessage.Text = string.Format("Error: {0} is already banned.", ip);
                        lblErrMessage.Visible = true;
                        return;
                    }
                }

                // Validate ban dates
                if (drpBanLength.SelectedValue == "0")
                {
                    lblErrMessage.Text = "Error: Please select an end date for the ban.";
                    lblErrMessage.Visible = true;
                    return;
                }

                int banLength = Convert.ToInt32(drpBanLength.SelectedValue);

                // check for permanent ban.  perm ban value is -1
                if (banLength < 0)
                {
                    ipBanEnd = DateTime.MaxValue;
                }
                else
                {
                    ipBanEnd = ipBanStart.AddDays(Convert.ToDouble(banLength));
                }

                // Add the bans
                foreach (string ip in iPsToBan)
                {
                    if (!UsersUtility.AddIPBan(ip, ipBanStart.ToString(), ipBanEnd.ToString()).Equals(0))
                    {
                        // Log the CSR activity
                        this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), string.Format("CSR - Inserted new IP Ban: {0}", ip), 0, 0);
                    }
                    else
                    {
                        lblErrMessage.Text = "Error: Failed to add IP ban to database.";
                        lblErrMessage.Visible = true;
                        return;
                    }
                }

                BindData(1);
            }
            catch (Exception)
            {
                lblErrMessage.Text = "Error: Failed to add IP ban to database.";
                lblErrMessage.Visible = true;
            }
        }


        #region Properties

        /// <summary>
        /// Banned IP Search Filter
        /// </summary>
        /// <returns></returns>
        private string BannedIPSearchFilter
        {
            get
            {
                if (ViewState["_BannedIPSearchFilter"] == null)
                {
                    ViewState["_BannedIPSearchFilter"] = "";
                }
                return ViewState["_BannedIPSearchFilter"].ToString();
            }
            set
            {
                ViewState["_BannedIPSearchFilter"] = value;
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "ip_address";
            }
        }

        /// <summary>
        /// DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }

        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion


    }
}
