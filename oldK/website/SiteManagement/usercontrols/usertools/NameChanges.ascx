<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="NameChanges.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.NameChanges" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
     <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="998" style="background-color:#ffffff">
        <tr>
            <td align="center">
			    <span style="height:30px; font-size:28px; font-weight:bold">Name Changes</span><br />
			    <ajax:ajaxpanel id="ajpError" runat="server">
			    <asp:label runat="server" id="lblErrMessage" class="errBox black" style="width:60%;margin-top:20px;text-align:center;"></asp:label></ajax:ajaxpanel>
		    </td>
        </tr>
        <tr>
            <td style="padding:10px 0px 10px 24px">
                <asp:Label runat="server" id="lblSelectedUser" />
             </td>
         </tr>
	    <tr>
		    <td align="center">
     
			    <ajax:ajaxpanel id="ajpNameChangeDetails" runat="server">

				    <div id="divUsernameDetails" style="background-color:White;width:95%;margin-bottom:20px;" runat="server" >	 
        
    				    <div style="text-align:left; padding:0px 0px 0px 2px"><b>Change User Name</b></div>
                        <fieldset title="Change User Name" style="padding:0px 10px 0px 10px;width: 98%; margin:10px 0px 0px 10x">
					        <div style="width:50%;margin-bottom:20px;">
						        <asp:validationsummary cssclass="errBox black" id="valSum" runat="server" showmessagebox="False" showsummary="True"
							        displaymode="BulletList" headertext="Please correct the following errors:" forecolor="black"></asp:validationsummary>
						        <asp:customvalidator id="cvBlank" runat="server" display="None" enableclientscript="False"></asp:customvalidator>
					        </div>		
        					
					        <asp:requiredfieldvalidator id="rfUsername" runat="server" controltovalidate="txtUsername" text="*" 
						        errormessage="You must enter a nickname." display="none"></asp:requiredfieldvalidator>
					        <asp:regularexpressionvalidator id="revUsername" runat="server" controltovalidate="txtUsername" text="" display="none" 
						        errormessage="Nicknames should be 4-20 characters and a combination of upper and lower-case letters, numbers, and underscores (_) only. Do not use spaces."></asp:regularexpressionvalidator>
        				
					        <div style="width:60%; text-align:left; float:left;">
						        <div style="padding-left:20px;">New Username: &nbsp;&nbsp;<asp:textbox class="biginput" id="txtUsername" runat="server" maxlength="20" tabindex="1" width="220"></asp:textbox>
							        <asp:imagebutton id="btnCheckUsername" onclick="btnCheckUsername_Click" tabindex="2" runat="server"
									        width="124"
									        imageurl="~/images/wizard_btn_checkavail.gif"
									        alternatetext="Check Availability"
									        height="21"
									        border="0"
									        imagealign="AbsMiddle"
									        causesvalidation="False"></asp:imagebutton>&nbsp;&nbsp;<span id="spnCheckUsername" runat="server"></span></div>
        						
						        <div style="margin-top:6px;padding:0px 0px 20px 24px;">New password: &nbsp;&nbsp;<asp:textbox class="biginput" id="txtPassword" runat="server" maxlength="20" textmode="Password" tabindex="13" width="220"></asp:textbox>																								
						        <asp:requiredfieldvalidator id="rfPassword" runat="server" controltovalidate="txtPassword" text="*" errormessage="Please enter your password"
							        display="none"></asp:requiredfieldvalidator></div>
					        </div>
					        <div style="margin-top:0px;float:left; width:12%;text-align:right;">
					            <asp:button id="btnChange" runat="server" onclick="btnChange_Click" text="Change Name"/>
					        </div>
    				    </fieldset><br /><br />
    				    <div style="text-align:left; padding:0px 0px 0px 2px"><b>Change Name Color</b></div>
                        <fieldset title="Change Name Color" style="padding:10px 10px 10px 10px;width: 98%; margin:10px 0px 0px 10x" >
						        <div style="margin:10px 0px 0px 10x; text-align:left;">Name Color: &nbsp;&nbsp;<asp:DropDownList id="availNameColors" runat="server" width="220" Enabled="true" />&nbsp;<asp:button id="changeColor" CausesValidation="false" runat="server" onclick="changeColor_Click" text="Change Color"/></div>
 						        
                       </fieldset>
				    </div>
    				
			    </ajax:ajaxpanel>
    			
		    </td>
	    </tr>
    </table>
</asp:Panel>
