<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="FamePerPacket.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.FamePerPacket" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>

<script type="text/javascript" src="jscript/yahoo/yahoo-min.js"></script>  
<script type="text/javascript" src="jscript/yahoo/event-min.js" ></script>   
<script type="text/javascript" src="jscript/yahoo/dom-min.js" ></script>   
<script type="text/javascript" src="jscript/yahoo/calendar-min.js"></script>

<script type="text/javascript" language="javascript" src="jscript/prototype.js"></script>
<link href="css/yahoo/calendar.css" type="text/css" rel="stylesheet" />   


<style >
	#cal1Container { display:none; position:absolute; z-index:100;}
</style>
 
 
<script type="text/javascript"><!--
 function specialCharReplace()
 {
   //replace for item text, display text, and description;
    var itemName = document.getElementById("tbx_ItemName");
    itemName.value = itemName.value.replace(/amp;/g,"")

    var itemDisplayName = document.getElementById("tbx_DisplayName");
    itemDisplayName.value = itemDisplayName.value.replace(/amp;/g,"")

    var itemDescription = document.getElementById("tbx_Description");
    itemDescription.value = itemDescription.value.replace(/amp;/g,"")

 }
 
YAHOO.namespace("example.calendar");

     var txtSearch_ID = '<%= txtSearch.ClientID %>';

	function handleAddedDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate1 = $(txtSearch_ID);
		txtDate1.value = month + "-" + day + "-" + year;
		YAHOO.example.calendar.cal1.hide();
	}
	
	function initAddedDate() {
		// Admin Calendar
		YAHOO.example.calendar.cal1 = new YAHOO.widget.Calendar ("cal1", "cal1Container", { iframe:true, zIndex:200, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal1.render();
		
		// Listener to show Admin Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgItemAddDate", "click", YAHOO.example.calendar.cal1.show, YAHOO.example.calendar.cal1, true);   
		YAHOO.example.calendar.cal1.selectEvent.subscribe(handleAddedDate, YAHOO.example.calendar.cal1, true);
	}
	
//--> </script>  
<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    <ajax:ajaxpanel id="ajMessage" runat="server">
    
         <table id="tblFamePackets" border="0" cellpadding="0" cellspacing="0" width="998" style="background-color:#ffffff">
            <tr>
                <td align="center">
			        <span style="height:30px; font-size:28px; font-weight:bold">Fame Per Packet</span><br />
			        <ajax:ajaxpanel id="ajpError" runat="server">
			        <asp:label runat="server" id="lblErrMessage" class="errBox black" style="width:60%;margin-top:20px;text-align:center;"></asp:label></ajax:ajaxpanel>
		        </td>
            </tr>
	        <tr>
		        <td style="height:20px"></td>
            </tr>
            <tr>
                 <td align="center">
         
 			        <ajax:ajaxpanel id="ajpFamepackets" runat="server">  
                   
				        <div width="95%" style="padding: 6px 20px 6px 0px; font-size:10pt; text-align:right;">
					        <b>Search by Date:</b> &nbsp;<div id="cal1Container"></div><asp:TextBox runat="server" id="txtSearch" maxlength="10" width="80"></asp:TextBox>&nbsp;
					        <img id="imgItemAddDate" name="imgItemAddDate" onload="initAddedDate();" src="images/cal_16.gif"/>						
					        <asp:Button id="btnSearch" onclick="btnSearch_Click" causesvalidation="false" runat="server" text="Search"></asp:Button>
					        <br /><span style="font-size:9px;position:relative;left:-105px;top:-5px;">mm-dd-yyyy</span>
				        </div>
               
				        <div style="width:95%;margin-bottom:6px;text-align:left;"><span id="spnResultFilter" runat="server"></span></div>
			        <body>
				        <asp:gridview id="dgFamePackets" runat="server" 
					        onsorting="dgFamePackets_Sorting" 
					        autogeneratecolumns="False" 
					        width="80%" 
					        AllowSorting="True" border="0" cellpadding="2" 
					        AllowPaging="False" Align="center" 
					        bordercolor="DarkGray" borderstyle="solid" borderwidth="1px">
        					
					        <HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Center"></HeaderStyle>
					        <RowStyle BackColor="White" horizontalalign="center"></RowStyle>
					        <AlternatingRowStyle BackColor="Gainsboro" horizontalalign="center"></AlternatingRowStyle>
					        <FooterStyle BackColor="#FFffff"></FooterStyle>
					        <Columns>
						        <asp:BoundField HeaderText="Packet Id" DataField="PacketId" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
						        <asp:BoundField HeaderText="Packet Name" DataField="Name" SortExpression="name" ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-horizontalalign="Left" headerstyle-horizontalalign="Left"></asp:BoundField>
						        <asp:BoundField HeaderText="Points Gained" DataField="PointsEarned" SortExpression="points" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
						        <asp:BoundField HeaderText="Rewards Gained" DataField="Rewards" SortExpression="rewards" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
					        </Columns>
				        </asp:gridview>
			         </body>
				        <div style="width:65%;text-align:right;margin:10px 6px 0 0;"><kaneva:pager runat="server" isajaxmode="True" onpagechanged="pgPackets_PageChange" id="pgPackets" maxpagestodisplay="5" shownextprevlabels="true" /></div>

			        </ajax:ajaxpanel> 
                 
			        <br /><br />
                 
                 </td>
            </tr>
        </table>
    </ajax:ajaxpanel>
</asp:Panel> 
