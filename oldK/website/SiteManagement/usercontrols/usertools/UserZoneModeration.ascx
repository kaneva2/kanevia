<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="UserZoneModeration.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.UserZoneModeration" %>

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="980">
        <tr>
            <td style="padding-left:40px; text-align:center; font-size:22px">
                USER ZONE MODERATION
            </td>
        </tr>
        <TR>
            <TD align="center" style="padding-top: 30px">
                <asp:Label id="lbl_Messages" runat="server" ForeColor="red" Font-Names="Arial"></asp:Label> 
                <br />
                <asp:Label runat="server" id="lblSelectedUser" />
                <asp:dropdownlist ID="drpZones" runat="server" DataTextField="name" DataValueField="zone_index_plain" width="300" >
                </asp:dropdownlist>    
                <asp:dropdownlist ID="drpRoles" runat="server">
                    <asp:ListItem Value="1">Owner (1)</asp:ListItem>
                    <asp:ListItem Value="2">Moderator (2)</asp:ListItem>
                </asp:dropdownlist>
                <asp:Button id="btn_Add" onclick="btn_Add_Click" Text="New Role" runat="server"></asp:Button> 
            </TD>
        </TR>        
         <tr>
            <td align="center">
                <asp:DataGrid id="dgrdRoles" runat="server" EnableViewState="False" ShowFooter="False" OnPageIndexChanged="dgrdRoles_PageIndexChanged" 
                OnSortCommand="dgrdRoles_SortCommand" OnEditCommand="dgrdRoles_EditCommand"  OnDeleteCommand="dgrdRoles_DeleteCommand"
                OnCancelCommand="dgrdRoles_CancelCommand" PageSize="15" AllowPaging="True" AllowSorting="True" 
                AutoGenerateColumns="False" border="0" cellpadding="2" Width="800" style="border: 1px solid #cccccc; font-family:arial">  
	                <HeaderStyle BackColor="#cccccc" ForeColor="#000000" Font-Underline="false" Font-Bold="true" HorizontalAlign="Left" />
	                <ItemStyle BackColor="#ffffff" HorizontalAlign="Left" />
	                <AlternatingItemStyle BackColor="#eeeeee" HorizontalAlign="Left" />
                        <Columns>
                        <asp:BoundColumn DataField="zone_index" HeaderText="Zone Id" SortExpression="zone_index" ReadOnly="True">
                        <HeaderStyle HorizontalAlign="Left" Font-Bold="True" Width="50" ></HeaderStyle>
                        </asp:BoundColumn>
                        
                        <asp:BoundColumn DataField="name" HeaderText="Zone Name" SortExpression="name" ReadOnly="True">
                        <HeaderStyle HorizontalAlign="Left" Font-Bold="True" Width="400"></HeaderStyle>
                        </asp:BoundColumn>
                        
                        <asp:BoundColumn DataField="role" HeaderText="Role" SortExpression="role" ReadOnly="True">
                        <HeaderStyle HorizontalAlign="Left" Font-Bold="True" width="50"></HeaderStyle>
                        </asp:BoundColumn>
                        
                        <asp:ButtonColumn ButtonType="LinkButton" Text="Delete"  CommandName="Delete" >
                        <HeaderStyle HorizontalAlign="Left" Font-Bold="True" width="50"></HeaderStyle>
                        </asp:ButtonColumn>
                        </Columns>
                </asp:datagrid>
            </td>
         </tr>
    </table>
</asp:Panel>
