<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="UserNotes.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.UserNotes" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>


<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<table border="0" cellpadding="0" cellspacing="0" width="990" >
    <tr>
        <td style="padding-left:40px; text-align:center; font-size:22px">
            USER NOTES
        </td>
    </tr>
    <tr>
        <td align="center">
            <table cellpadding="0" cellspacing="0" border="0" width="980">
	            <tr style="line-height:15px;">
	                <td align="left" style="width:300px" class="belowFilter2" valign="middle" >
	                    <asp:Label runat="server" id="lblSelectedUser" />
	                </td>
		            <td align="right" class="belowFilter2" valign="middle">
			            <asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop"/><br>
		            </td>						
	            </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">
            <asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="980" id="dgrdNotes" cellpadding="0" cellspacing="0" border="1" AutoGenerateColumns="False" OnSortCommand="UserNote_Sorting" AllowSorting="True" CssClass="FullBorders">  
	            <HeaderStyle BackColor="#cccccc" ForeColor="#000000" Font-Underline="false" Font-Bold="true" HorizontalAlign="Left" />
	            <ItemStyle BackColor="#ffffff" HorizontalAlign="Left" />
	            <AlternatingItemStyle BackColor="#eeeeee" HorizontalAlign="Left" />
	            <Columns>
		            <asp:TemplateColumn HeaderText="created date" SortExpression="created_datetime" ItemStyle-Width="16%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Top">
			            <ItemTemplate>
				            <%# FormatDateTime (DataBinder.Eval(Container.DataItem, "created_datetime")) %>&nbsp;
			            </ItemTemplate>
		            </asp:TemplateColumn>
			            <asp:TemplateColumn HeaderText="posted by" SortExpression="username" ItemStyle-Width="16%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Top">
				            <ItemTemplate>
					            <%# DataBinder.Eval(Container.DataItem, "username") %>&nbsp;
				            </ItemTemplate>
			            </asp:TemplateColumn>
		            <asp:TemplateColumn HeaderText="note" SortExpression="note" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="68%">
			            <ItemTemplate> 
				            <%# DataBinder.Eval(Container.DataItem, "note") %>
			            </ItemTemplate>
		            </asp:TemplateColumn>		
	            </Columns>
            </asp:datagrid>        
         </td>
    </tr>
    <tr>
        <td style="height:40px">
            <hr style="width:100%" />
        </td>
    </tr>
    <tr>
        <td align="center">
            <table cellpadding="0" cellspacing="0" border="0" width="750">
                <tr>
                        <td align="left" ><b id="rounded2"><i>&nbsp;&nbsp;Add a Note</i></b></td>
                </tr>
                 <tr>
                        <td align="center" colspan="3" width="700" style="border-left: 1px solid #ededed;border-right: 1px solid #ededed;"><br>
                            <CE:Editor id="txtNote" BackColor="#ededed" runat="server" width="100%" Height="200px" ShowHtmlMode="False" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/asset.config" AutoConfigure="None" ></CE:Editor>
                            <asp:RequiredFieldValidator ID="rfTeaser" ControlToValidate="txtNote" Text="*" ErrorMessage="Note is a required field." Display="Dynamic" runat="server"/>
                            <br><br>
                        </td>
                </tr>
                <tr>
                        <td align="right" bgcolor="#ededed"><br>
                                <asp:button id="btnCancel" runat="Server" CausesValidation="False" onClick="btnCancel_Click" class="Filter2" Text="  cancel  "/>&nbsp;&nbsp;<asp:button id="btnUpdate" runat="Server" onClick="btnUpdate_Click" CausesValidation="False" class="Filter2" Text="  add note  "/>&nbsp;&nbsp;&nbsp;&nbsp;<br>
                        </td>
                </tr>
                
                
            </table>
        </td>
    </tr>
</table>



</asp:Panel>