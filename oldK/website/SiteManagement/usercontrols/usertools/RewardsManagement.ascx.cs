using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using MagicAjax;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class RewardsManagement : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.USER_DATA_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                //clear message
                messages.Text = "";

                if (!IsPostBack)
                {
                    CurrentRewardAdminType = RewardAdminType.Event_Participation;
                    SetValidators();
                    BindData();
                }

        }
        else
        {
            RedirectToHomePage();
        }
    }


        #region Helper Methods

        /// <summary>
        /// BindData
        /// </summary>
        private void BindData()
        {
            // Hide everything
            DisplaySection();

            // Get the transaction type and bind the data
            switch (CurrentRewardAdminType)
            {
                case RewardAdminType.Admin:
                    BindAdmin();
                    break;

                case RewardAdminType.Event_Participation:
                    BindEvents();
                    break;

                case RewardAdminType.Survey_Participation:
                    BindSurveys();
                    break;

                case RewardAdminType.Contest_Participation:
                    BindContest();
                    break;

                default:
                    BindAdmin();
                    break;
            }
        }

        /// <summary>
        /// Bind Data for Reward Event Admin Section
        /// </summary>
        private void BindAdmin()
        {
            // Populate the Transaction Types Drop Down
            PopulateTransactionTypes();

            // Populate DataGrid with existing Reward Events
            DataTable dtEvents = GetTransactionFacade().GetKanevaEvents();

            dgRewardEvents.DataSource = dtEvents;
            dgRewardEvents.DataBind();
        }

        /// <summary>
        /// Bind Data for the Reward Events Section
        /// </summary>
        private void BindEvents()
        {
            // Populate the Events Drop Down
            PopulateEvents(drpEvents);
        }

        /// <summary>
        /// Bind Data for the Survey Events section
        /// </summary>
        private void BindSurveys()
        {
            // Populate the Events Drop Down
            PopulateEvents(drpSurveyEvents);
        }

        /// <summary>
        /// Bind Data for the Contest Events section
        /// </summary>
        private void BindContest()
        {
            // Populate the Contest name drop list
            PopulateEvents(drpContest);

            for (int i = 1; i <= 10; i++)
            {
                // Clear the username textboxes
                TextBox txtUser = (TextBox)FindControl("txtUser" + i.ToString());
                if (txtUser != null)
                {
                    txtUser.Text = "";
                }

                // Clear the amount text boxes
                TextBox txtAmt = (TextBox)FindControl("txtAmt" + i.ToString());
                if (txtAmt != null)
                {
                    txtAmt.Text = "";
                }

                // Initialize the currency drop downs
                DropDownList drpCurrency = (DropDownList)FindControl("drpCurrency" + i.ToString());
                if (drpCurrency != null)
                {
                    drpCurrency.Items.Add(new ListItem("GPOINT (Rewards)", "GPOINT"));
                    drpCurrency.Items.Add(new ListItem("KPOINT (Credits)", "KPOINT"));
                }
            }
        }

        /// <summary>
        /// Displays section based on the current nav selection
        /// </summary>
        private void DisplaySection()
        {
            lbEventAdmin.Style.Add("font-weight", "none");
            lbEventParticipation.Style.Add("font-weight", "none");
            lbSurveyParticipation.Style.Add("font-weight", "none");
            lbContestParticipation.Style.Add("font-weight", "none");

            divEventAdmin.Style.Add("display", "none");
            divEventParticipation.Style.Add("display", "none");
            divSurveyParticipation.Style.Add("display", "none");
            divContestParticipation.Style.Add("display", "none");

            switch (CurrentRewardAdminType)
            {
                case RewardAdminType.Admin:
                    divEventAdmin.Style.Add("display", "block");
                    lbEventAdmin.Style.Add("font-weight", "bold");
                    break;

                case RewardAdminType.Event_Participation:
                    divEventParticipation.Style.Add("display", "block");
                    lbEventParticipation.Style.Add("font-weight", "bold");
                    break;

                case RewardAdminType.Survey_Participation:
                    divSurveyParticipation.Style.Add("display", "block");
                    lbSurveyParticipation.Style.Add("font-weight", "bold");
                    break;

                case RewardAdminType.Contest_Participation:
                    divContestParticipation.Style.Add("display", "block");
                    lbContestParticipation.Style.Add("font-weight", "bold");
                    break;
            }
        }

        /// <summary>
        /// Populate the Drop List control with the Reward Events list
        /// </summary>
        private void PopulateEvents(DropDownList drpCtrl)
        {
            DataTable dtEvents = GetTransactionFacade().GetKanevaEvents();

            if (dtEvents.Rows.Count > 0)
            {
                drpCtrl.DataSource = dtEvents;

                // Set the text a value fields
                drpCtrl.DataTextField = "event_name";
                drpCtrl.DataValueField = "reward_event_id";

                // Bind the data to the drop list
                drpCtrl.DataBind();

                // Insert a 'select...' message as first item in list
                drpCtrl.Items.Insert(0, new ListItem("select event name...", ""));
                drpCtrl.Enabled = true;
            }
            else
            {
                drpCtrl.Items.Insert(0, new ListItem("no events found", ""));
            }
        }

        /// <summary>
        /// Populate the Drop Down with Transaction Types
        /// </summary>
        private void PopulateTransactionTypes()
        {
            // Set filter to only show reward event transaction types.
            string filter = "transaction_type IN (6,15,22,23,24)";
            string orderBy = "trans_desc";

            // Get the transaction types
            DataTable dtTransTypes = GetTransactionFacade().GetTransactionTypes(filter, orderBy);

            if (dtTransTypes.Rows.Count > 0)
            {
                drpTransType.DataSource = dtTransTypes;

                // Set the text a value fields
                drpTransType.DataTextField = "trans_desc";
                drpTransType.DataValueField = "transaction_type";

                // Bind the data to the drop list
                drpTransType.DataBind();

                // Insert a 'select...' message as first item in list
                drpTransType.Items.Insert(0, new ListItem("select transaction type...", "0"));
                drpTransType.Enabled = true;
            }
            else
            {
                drpTransType.Items.Insert(0, new ListItem("no transaction types found", "-1"));
            }
        }

        /// <summary>
        /// Set the reward type
        /// </summary>
        private void SetCurrentRewardAdminType(string type)
        {
            switch (type.ToLower())
            {
                case "ep":
                    CurrentRewardAdminType = RewardAdminType.Event_Participation;
                    break;

                case "sp":
                    CurrentRewardAdminType = RewardAdminType.Survey_Participation;
                    break;

                case "cp":
                    CurrentRewardAdminType = RewardAdminType.Contest_Participation;
                    break;
                case "ea":
                default:
                    CurrentRewardAdminType = RewardAdminType.Admin;
                    break;

            }

            SetValidators();
        }

        /// <summary>
        /// Configure the validators
        /// </summary>
        private void SetValidators()
        {
            //  disable all validators
            rfvAmount.Enabled = false;
            rfvStartDate.Enabled = false;
            rfvEndDate.Enabled = false;
            rfvUserList.Enabled = false;
            rfvEventName.Enabled = false;

            // Based on the current visible section, set the appropriate validators
            switch (CurrentRewardAdminType)
            {
                case RewardAdminType.Admin:
                    rfvEventName.ControlToValidate = "txtEventName";
                    rfvEventName.ErrorMessage = "You must enter an Event Name.";
                    rfvEventName.Enabled = true;
                    break;

                case RewardAdminType.Event_Participation:
                    rfvAmount.Enabled = true;
                    rfvAmount.ControlToValidate = "txtAmount";
                    rfvStartDate.Enabled = true;
                    rfvStartDate.ControlToValidate = "txtStartDate";
                    rfvEndDate.Enabled = true;
                    rfvEndDate.ControlToValidate = "txtEndDate";
                    break;

                case RewardAdminType.Survey_Participation:
                    rfvAmount.Enabled = true;
                    rfvAmount.ControlToValidate = "txtSurveyAmount";
                    rfvUserList.Enabled = true;
                    rfvUserList.ControlToValidate = "txtSurveyUserList";
                    break;

                case RewardAdminType.Contest_Participation:
                    rfvEventName.ControlToValidate = "drpContest";
                    rfvEventName.ErrorMessage = "You must select a contest name.";
                    rfvEventName.Enabled = true;
                    break;
            }
        }

        /// <summary>
        /// Display message box with message
        /// </summary>
        private void DisplayMessageBox(bool isError, string msg)
        {
            DisplayMessageBox(isError, msg, true);
        }
        private void DisplayMessageBox(bool isError, string msg, bool isVisible)
        {
            if (!isVisible)
            {
                // Hide message box
                valSum.Style.Add("display", "none");
            }
            else
            {
                if (!isError)
                {
                    // If not error, then show info box style
                    valSum.CssClass = "infoBox";
                    valSum.HeaderText = "";
                    valSum.DisplayMode = ValidationSummaryDisplayMode.SingleParagraph;
                }
                else
                {
                    // If error, show error box style
                    valSum.CssClass = "errBox";
                    valSum.HeaderText = "Please correct the following errors:";
                    valSum.DisplayMode = ValidationSummaryDisplayMode.BulletList;
                }
                valSum.Style.Add("display", "block");
                cvBlank.IsValid = false;
                cvBlank.ErrorMessage = msg;
            }
        }

        /// <summary>
        /// Populate the Reward Event details with selected datagrid value
        /// </summary>
        private void GetRewardEventDetails(GridViewRow gvr, int pageNumber)
        {
            try
            {
                // Populate controls with values selcted form data grid
                txtEventName.Text = gvr.Cells[1].Text;
                SetDropDownIndex(drpTransType, gvr.Cells[3].Text);
                txtRewardEventId.Text = gvr.Cells[0].Text;
            }
            catch (Exception ex)
            {
                DisplayMessageBox(true, "Error displaying reward event details: " + ex.Message);
            }
        }

        #endregion Helper Methods


        #region Event Handlers

        /// <summary>
        /// Click Event for Award Reward button
        /// </summary>
        protected void btnAward_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if page validates.
                Page.Validate();
                if (!Page.IsValid)
                {
                    DisplayMessageBox(true, "");
                    return;
                }

                // Do validation on event
                if (drpEvents.SelectedValue == "" || Convert.ToInt32(drpEvents.SelectedValue) < 1)
                {
                    DisplayMessageBox(true, "You must select an event.");
                    return;
                }
                int rewardEventId = Convert.ToInt32(drpEvents.SelectedValue);

                // Validate the start and end dates
                DateTime startDate = Convert.ToDateTime(txtStartDate.Text);
                DateTime endDate = Convert.ToDateTime(txtEndDate.Text);
                if (DateTime.Compare(endDate, startDate) < 0)
                {
                    DisplayMessageBox(true, "Start Date/Time must be less than the End Date/Time.");
                    return;
                }

                // Validate the reward amount
                double amount = 0;
                try
                {
                    amount = Convert.ToDouble(txtAmount.Text);

                    if (amount <= 0)
                    {
                        DisplayMessageBox(true, "You must enter an amount value greater than 0.");
                        return;
                    }
                }
                catch
                {
                    DisplayMessageBox(true, "You must enter a valid amount.");
                    return;
                }

                string keiPointId = drpCurrency.SelectedValue;
                int transTypeId = Constants.CASH_TT_EVENT;

                // Give rewards to users who participated
                GetTransactionFacade().RewardCredits(amount, keiPointId, rewardEventId, transTypeId, startDate, endDate);
            }
            catch (Exception ex)
            {
                DisplayMessageBox(true, ex.Message);
            }
        }

        /// <summary>
        /// Click Event for Survey Award Reward button
        /// </summary>
        protected void btnAwardSurvey_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if page validates.
                Page.Validate();
                if (!Page.IsValid)
                {
                    DisplayMessageBox(true, "");
                    return;
                }

                // Do validation on event
                if (drpSurveyEvents.SelectedValue == "" || Convert.ToInt32(drpSurveyEvents.SelectedValue) < 1)
                {
                    DisplayMessageBox(true, "You must select an event.");
                    return;
                }
                int rewardEventId = Convert.ToInt32(drpSurveyEvents.SelectedValue);

                // Validate the reward amount
                double amount = 0;
                try
                {
                    amount = Convert.ToDouble(txtSurveyAmount.Text);

                    if (amount <= 0)
                    {
                        DisplayMessageBox(true, "You must enter an amount value greater than 0.");
                        return;
                    }
                }
                catch
                {
                    DisplayMessageBox(false, "You must enter a valid amount.");
                    return;
                }

                string userIdList = null;
                string userEmailList = null;
                // If @ is in the list, then we have email addresses
                if (txtSurveyUserList.Text.IndexOf("@") > 0)
                {
                    string[] arEmails;
                    char[] splitter = { ',' };
                    arEmails = txtSurveyUserList.Text.Split(splitter);
                    string strOut = "";
                    if (arEmails.Length > 0)
                    {
                        for (int i = 0; i < arEmails.Length; i++)
                        {
                            arEmails[i] = arEmails[i].Trim();
                            if (!arEmails[i].StartsWith("'"))
                            {
                                arEmails[i] = arEmails[i].Insert(0, "'");
                            }
                            if (!arEmails[i].EndsWith("'"))
                            {
                                arEmails[i] = arEmails[i].Insert(arEmails[i].Length, "'");
                            }

                            if (strOut.Length > 0) strOut += ",";
                            strOut += arEmails[i];
                        }
                    }

                    userEmailList = strOut;
                }
                else // else we have list of user id's
                {
                    userIdList = txtSurveyUserList.Text.Trim();
                }

                string keiPointId = drpCurrency.SelectedValue;
                int transTypeId = Constants.CASH_TT_SURVEY;
                string failedUserIdList = "";

                // Give rewards to users who participated
                int success_count = GetTransactionFacade().RewardCreditsToMultipleUsers(userIdList, userEmailList, amount, transTypeId, keiPointId, rewardEventId, ref failedUserIdList);

                // Display results
                if (success_count == 0)
                {
                    DisplayMessageBox(true, "Reward credits failed.  No users were updated.");
                }
                else
                {
                    DisplayMessageBox(false, success_count.ToString() + " users successfully awarded credits.");
                }

            }
            catch (Exception ex)
            {
                DisplayMessageBox(true, ex.Message);
            }
        }

        /// <summary>
        /// Click Event for Survey Award Reward button
        /// </summary>
        protected void btnAddEvent_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if page validates.
                Page.Validate();
                if (!Page.IsValid)
                {
                    DisplayMessageBox(true, "");
                    return;
                }

                // Do validation on transaction type
                if (drpTransType.SelectedValue == "" || Convert.ToInt32(drpTransType.SelectedValue) < 1)
                {
                    DisplayMessageBox(true, "You must select a Transaction Type.");
                    return;
                }

                string eventName = txtEventName.Text;
                int rc = 0;

                // If ID is -1, this is a new word
                if (Convert.ToInt32(txtRewardEventId.Text) < 0)
                {
                    // Add the new Reward Event
                    rc = GetTransactionFacade().AddKanevaRewardEvent(eventName, Convert.ToInt32(drpTransType.SelectedValue));
                }
                else  // Update the Reward Event
                {
                    rc = GetTransactionFacade().UpdateKanevaRewardEvent(Convert.ToInt32(txtRewardEventId.Text), eventName, Convert.ToInt32(drpTransType.SelectedValue));
                }

                // Display the Results
                if (rc == 1)
                {
                    DisplayMessageBox(false, "Event '" + eventName + "' was successfully added.");
                }
                else
                {
                    DisplayMessageBox(true, "Event '" + eventName + "' was NOT added.");
                }

                // Clear text box value
                txtEventName.Text = "";
                txtRewardEventId.Text = "-1";

                // Get the data for the data grid
                BindData();
            }
            catch (Exception ex)
            {
                DisplayMessageBox(true, ex.Message);
            }
        }

        /// <summary>
        /// Click Event for Contest Award Reward button
        /// </summary>
        protected void btnAwardContest_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if page validates.
                Page.Validate();
                if (!Page.IsValid)
                {
                    DisplayMessageBox(true, "");
                    return;
                }

                string contestName = drpContest.SelectedItem.Text;
                int rewardEventId = Convert.ToInt32(drpContest.SelectedValue);
                int wokTransLogId = 0;
                string failedUsers = "";
                string notFoundUsers = "";
                string username = "";
                string amount = "";
                int userId = 0;

                // Loop through the 10 contest places
                for (int i = 1; i <= 10; i++)
                {
                    try
                    {
                        TextBox tbUser = (TextBox)Page.FindControl("txtUser" + i.ToString());
                        TextBox tbAmt = (TextBox)Page.FindControl("txtAmt" + i.ToString());
                        DropDownList ddCurrency = (DropDownList)Page.FindControl("drpCurrency" + i.ToString());
                        wokTransLogId = 0;

                        if (tbUser != null && tbAmt != null && ddCurrency != null)
                        {
                            username = tbUser.Text.Trim();
                            amount = tbAmt.Text.Trim();

                            if (username != "" && amount != "" && ddCurrency.SelectedValue != "")
                            {
                                // Get the User Id from the username
                                userId = 0;
                                userId = GetUserFacade().GetUserIdFromUsername(tbUser.Text.Trim());

                                if (userId > 0)
                                {
                                    // Give rewards to users who participated
                                    GetUserFacade().AdjustUserBalance(userId, ddCurrency.SelectedValue,
                                        Convert.ToDouble(tbAmt.Text), Constants.CASH_TT_CONTEST, ref wokTransLogId);

                                    // Add to log
                                    GetUserFacade().InsertRewardLog(userId, Convert.ToDouble(tbAmt.Text),
                                        ddCurrency.SelectedValue, Constants.CASH_TT_CONTEST, rewardEventId, wokTransLogId);
                                }
                                else // user was not found using username
                                {
                                    if (notFoundUsers.Length > 0) notFoundUsers += ", ";
                                    notFoundUsers += username + "<br/>";
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (failedUsers.Length > 0) failedUsers += ", ";
                        failedUsers += username + ":" + ex.Message + "<br/>";
                    }
                }

                // Check for errors
                string msg = "";
                if (failedUsers.Length > 0)
                {
                    msg = "Errors were thrown while processing the following users:<br/>" + failedUsers;
                }
                if (notFoundUsers.Length > 0)
                {
                    if (msg.Length > 0) msg += "<br/><br/>";
                    msg += "The following users were not found using the username provided:<br/>" + notFoundUsers;
                }
                // If errors, display message
                if (msg.Length > 0)
                {
                    DisplayMessageBox(true, msg);
                }
            }
            catch (Exception ex)
            {
                DisplayMessageBox(true, ex.Message);
            }
        }

        /// <summary>
        /// Execute when the user selects Edit link from the grid view
        /// </summary>
        protected void dgRewardEvents_RowEditing(object source, GridViewEditEventArgs e)
        {
            //passin the selected row and populate the fields
            GetRewardEventDetails(dgRewardEvents.Rows[e.NewEditIndex], 1);
        }

        /// <summary>
        /// Execute when the user selects link from the Nav links
        /// </summary>
        protected void EventNav_OnCommand(object sender, CommandEventArgs e)
        {
            // Set section to display
            SetCurrentRewardAdminType(e.CommandName);

            // Clear hidden value for event id
            txtRewardEventId.Text = "-1";

            // Get the data
            BindData();
        }

        #endregion Event Handlers

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion


        #region Properties

        private RewardAdminType CurrentRewardAdminType
        {
            set
            {
                ViewState["CurrentRewardAdminType"] = value;
            }
            get
            {
                if (ViewState["CurrentRewardAdminType"] == null)
                {
                    return RewardAdminType.Admin; //cORDER_BY_DEFAULT;
                }
                else
                {
                    try
                    {
                        return (RewardAdminType)(ViewState["CurrentRewardAdminType"]);
                    }
                    catch
                    {
                        return RewardAdminType.Admin;
                    }
                }
            }
        }

        private enum RewardAdminType
        {
            Admin,
            Event_Participation,
            Survey_Participation,
            Contest_Participation
        }

        #endregion Properties


    }
}