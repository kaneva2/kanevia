<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="UserBlogs.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.UserBlogs" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="980">
        <tr>
            <td style="padding-left:40px; text-align:center; font-size:22px">
                USER BLOGS
            </td>
        </tr>
        <tr>
            <td style="padding-left:40px">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
	                <tr style="line-height:15px;">
	                    <td align="left" style="width:300px" class="belowFilter2" valign="middle" >
	                        <asp:Label runat="server" id="lblSelectedUser" />
	                    </td>
		                <td align="right" class="belowFilter2" valign="middle">
			                <asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop"/><br>
		                </td>						
	                </tr>
                </table>
             </td>
         </tr>
         <tr>
            <td style="padding-left:40px">
                <asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" OnSortCommand="UserBlog_Sorting" Width="100%" id="dgrdRatings" cellpadding="2" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 1px solid #cccccc; font-family:arial">  
	                <HeaderStyle BackColor="#cccccc" ForeColor="#000000" Font-Underline="false" Font-Bold="true" HorizontalAlign="Left" />
	                <ItemStyle BackColor="#ffffff" HorizontalAlign="Left" />
	                <AlternatingItemStyle BackColor="#eeeeee" HorizontalAlign="Left" />
	                <Columns>				
		                <asp:TemplateColumn HeaderText="blog" SortExpression="subject" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top" ItemStyle-Width="80%" ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <a id="A1" runat="server" target="_blank" class="assetLink" href='<%# GetBlogDetailsLink (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "BlogId")), Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "CommunityId"))) %>'><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "Subject").ToString(), 60)%></a><BR/>
				                <%# DataBinder.Eval(Container.DataItem, "BodyText")%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn ItemStyle-CssClass="adminLinks" HeaderText="date" SortExpression="created_date" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Width="20%" ItemStyle-Wrap="false">
			                <ItemTemplate>
				                <%# FormatDateTime(DataBinder.Eval(Container.DataItem, "CreatedDate"))%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
	                </Columns>
                </asp:datagrid>
            </td>
         </tr>
    </table>

</asp:Panel>