using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using KlausEnt.KEP.SiteManagement;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using Kaneva.BusinessLayer.Facade;

namespace SiteManagement.usercontrols
{
    public partial class DefaultScriptGameItems : BaseUserControl
    {
        #region Declarations

        private int _page = 1;
        private int _itemsPerPage = 30;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
                ResetPage();
			}

            if (!Page.ClientScript.IsStartupScriptRegistered("toggleAutoInsertionFields"))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "toggleAutoInsertionFields",
                        "<script type=\"text/javascript\">\n" +
                        "    function toggleAutoInsertionFields(ddl) {\n" +
                        "        var txtAutomaticInsertionStartId = '" + txtAutomaticInsertionStart.ClientID + "';\n" +
                        "        var txtAutomaticInsertionOrderId = '" + txtAutomaticInsertionOrder.ClientID + "';\n" +
                        "\n" +
                        "        if (ddl.selectedIndex == 0) {\n" +
                        "            $(txtAutomaticInsertionStartId).removeAttribute('disabled');\n" +
                        "            $(txtAutomaticInsertionOrderId).removeAttribute('disabled');\n" +
                        "        } else {\n" +
                        "            $(txtAutomaticInsertionStartId).setAttribute('disabled', 'disabled');\n" +
                        "            $(txtAutomaticInsertionOrderId).setAttribute('disabled', 'disabled');\n" +
                        "        }\n" +
                        "    }\n" +
                        "</script>");
            }
		}

        #region Data Manipulation
        /// <summary>
        /// Called to bind data to page elements
        /// </summary>
        protected void BindData()
		{
            // Bind default game items
            PagedList<ScriptGameItem> gameItems = GetScriptGameItemFacade().GetDefaultScriptGameItems("", "default_game_item_id asc", _page, _itemsPerPage);
            gvGameItems.DataSource = gameItems;
            gvGameItems.DataBind();

            pgBottom.CurrentPageNumber = _page;
            pgBottom.NumberOfPages = Math.Ceiling((double)gameItems.TotalCount / _itemsPerPage).ToString();
            pgBottom.DrawControl();
		}

        /// <summary>
        /// Called to fill a ScriptGameItem BO from input fields.
        /// </summary>
        /// <param name="GameItem">The ScriptGameItem Business Object to fill.</param>
        /// <returns>True if this ScriptGameItem already exists in the database.</returns>
        protected bool FillGameItemFromInput(out ScriptGameItem gameItem, out ScriptGameItem oldGameItem)
        {
            string strOut;
            bool gameItemExists = false;

            // Grab all properties of the ScriptGameItem class. We'll use these to determine property names are reserved, and may not be specified in the generic 
            // "Properties" collection.
            Dictionary<string, string> sgiPropNames = typeof(ScriptGameItem)
                .GetProperties()
                .ToDictionary(p => p.Name, p => p.Name, StringComparer.InvariantCultureIgnoreCase);

            try
            {
                // Parse JSON input and fill the game item
                JObject item = JsonConvert.DeserializeObject<JObject>(txtGameItemJSON.Text.Trim());
                gameItem = item.ToObject<ScriptGameItem>();
                gameItem.Properties = item.Properties()
                        .Where(p => !sgiPropNames.TryGetValue(p.Name, out strOut))
                        .ToDictionary(p => p.Name, p => GetDCFInstance.StripExcessJSONFormatting(p.Value.ToString()));

                // Need to set "AutomaticInsertionStart" manually, since the value is from the form, not JSON
                if (Convert.ToBoolean(ddlAutomaticInsertion.SelectedValue))
                {
                    gameItem.AutomaticInsertionStart = string.IsNullOrWhiteSpace(txtAutomaticInsertionStart.Text) ? DateTime.Now : (DateTime?)DateTime.Parse(txtAutomaticInsertionStart.Text);
                    gameItem.AutomaticInsertionOrder = string.IsNullOrWhiteSpace(txtAutomaticInsertionOrder.Text) ? null : (int?)Int32.Parse(txtAutomaticInsertionOrder.Text);
                }
                else
                {
                    gameItem.AutomaticInsertionStart = null;
                    gameItem.AutomaticInsertionOrder = null;
                }

                // Set inventory compatible flag, based on game item type
                gameItem.InventoryCompatible = GetScriptGameItemFacade().IsTypeInventoryCompatible(gameItem.ItemType);

                // Get bundled glids
                gameItem.BundledGlids = GetScriptGameItemFacade().ExtractBundledGlids(gameItem);

                // Try to get a game item from the db to determine if we're updating or deleting
                oldGameItem = GetScriptGameItemFacade().GetSnapshotScriptGameItem(gameItem.GIGlid, true);
                gameItemExists = oldGameItem.GIGlid > 0;
            }
            catch (Exception)
            {
                gameItem = new ScriptGameItem();
                oldGameItem = null;
            }

            return gameItemExists;
        }

        /// <summary>
        /// Called to fill input fields from a ScriptGameItem BO.
        /// </summary>
        /// <param name="GameItem">The ScriptGameItem Business Object to use to fill.</param>
        protected void FillInputFromGameItem(ScriptGameItem gameItem)
        {
            // Serialize the game item into JSON
            JObject jsonObject = new JObject();
            jsonObject.Add("name", gameItem.Name);
            jsonObject.Add("GIGLID", gameItem.GIGlid);
            jsonObject.Add("GIID", gameItem.GIId);
            jsonObject.Add("itemType", gameItem.ItemType);
            jsonObject.Add("GLID", gameItem.Glid);
            jsonObject.Add("rarity", gameItem.Rarity);
            jsonObject.Add("level", gameItem.Level);
            jsonObject.Add("BundleGlid", gameItem.BundleGlid);
            
            foreach (KeyValuePair<string, string> property in gameItem.Properties)
            {
                jsonObject.Add(property.Key, property.Value);
            }

            txtGameItemJSON.Text = JsonConvert.SerializeObject(jsonObject, Formatting.Indented);

            ddlAutomaticInsertion.SelectedValue = gameItem.InsertAutomatically.ToString();

            if (gameItem.InsertAutomatically)
            {
                txtAutomaticInsertionStart.Text = gameItem.AutomaticInsertionStart.Value.ToString("yyyy-MM-dd");

                if (gameItem.AutomaticInsertionOrder != null)
                {
                    txtAutomaticInsertionOrder.Text = gameItem.AutomaticInsertionOrder.ToString();
                }

                txtAutomaticInsertionStart.Attributes.Remove("disabled");
                txtAutomaticInsertionOrder.Attributes.Remove("disabled");
            }
            else
            {
                txtAutomaticInsertionStart.Attributes.Add("disabled", "disabled");
                txtAutomaticInsertionOrder.Attributes.Add("disabled", "disabled");
            }
        }

        /// <summary>
        /// Called to empty input fields to defaults.
        /// </summary>
        protected void ResetFields()
        {
            txtGameItemJSON.Text = "";
            divErrorData.InnerHtml = "";
            litSuggestedGIGLID.Text = "";
            litSuggestedGIID.Text = "";
            litSuggestedBundleGlid.Text = "";
            txtAutomaticInsertionStart.Text = "";
            txtAutomaticInsertionOrder.Text = "";
        }

        /// <summary>
        /// Called to reset the page
        /// </summary>
        protected void ResetPage(int page = 1)
        {
            _page = page;
            BindData();

            dvGameItemEdit.Visible = false;
            dvGameItems.Visible = true;
            trSuggestedGIGLID.Visible = false;
            trSuggestedBundleGlid.Visible = false;
            trSuggestedGIID.Visible = false;

            ResetFields();
        }

        /// <summary>
        /// Called to validate the page before saving
        /// </summary>
        protected bool ValidatePage()
        {
            try
            {
                string json = txtGameItemJSON.Text.Trim();
                JObject item = JsonConvert.DeserializeObject<JObject>(txtGameItemJSON.Text.Trim());

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected bool ValidateGameItemInsert(ScriptGameItem sgItem, out string errorMsg)
        {
            errorMsg = null;

            if (sgItem.GIGlid < ScriptGameItem.GIGLID_LOWER_LIMIT || sgItem.GIGlid > ScriptGameItem.GIGLID_UPPER_LIMIT)
            {
                errorMsg = string.Format("GIGLID is not specified or invalid. Must be between {0} and {1}.", ScriptGameItem.GIGLID_LOWER_LIMIT, ScriptGameItem.GIGLID_UPPER_LIMIT);
            }
            else if (sgItem.GIId <= 0)
            {
                errorMsg = "GIID is not specified or is invalid.";
            }
            else if (sgItem.BundledGlids.Count > 0 && (sgItem.BundleGlid < ScriptGameItem.GI_BUNDLE_GLID_LOWER_LIMIT || sgItem.BundleGlid > ScriptGameItem.GI_BUNDLE_GLID_UPPER_LIMIT))
            {
                errorMsg = string.Format("BundleGlid is required for this inventory-compatible object because it has glids associated with it.<br/>BundleGlid was not specified or invalid. Must be between {0} and {1}.", ScriptGameItem.GI_BUNDLE_GLID_LOWER_LIMIT, ScriptGameItem.GI_BUNDLE_GLID_UPPER_LIMIT);
            }

            return string.IsNullOrWhiteSpace(errorMsg);
        }

        protected bool ValidateGameItemUpdate(ScriptGameItem sgItem, out string errorMsg)
        {
            errorMsg = null;

            ScriptGameItem dbGameItem = GetScriptGameItemFacade().GetSnapshotScriptGameItem(sgItem.GIGlid, true);

            if ((dbGameItem.GIId != sgItem.GIId) || (dbGameItem.GIGlid != sgItem.GIGlid))
            {
                errorMsg = "GIID and GIGLID may not be modified once set.";
            }
            else if (sgItem.BundledGlids.Count > 0 && (sgItem.BundleGlid < ScriptGameItem.GI_BUNDLE_GLID_LOWER_LIMIT || sgItem.BundleGlid > ScriptGameItem.GI_BUNDLE_GLID_UPPER_LIMIT))
            {
                errorMsg = string.Format("BundleGlid is required for this inventory-compatible object because it has glids associated with it.<br/>BundleGlid was not specified or invalid. Must be between {0} and {1}.", ScriptGameItem.GI_BUNDLE_GLID_LOWER_LIMIT, ScriptGameItem.GI_BUNDLE_GLID_UPPER_LIMIT);
            }

            return string.IsNullOrWhiteSpace(errorMsg);
        }

        #endregion

        #region Event Handlers
        protected void btnNewGameItem_Click(object sender, EventArgs e)
		{
            dvGameItemEdit.Visible = true;
            dvGameItems.Visible = false;
			ResetFields();

            litSuggestedGIGLID.Text = string.Format("Suggested GIGLID: {0}", GetScriptGameItemFacade().GetSuggestedGIGLID());
            litSuggestedGIID.Text = string.Format("Suggested GIID: {0}", GetScriptGameItemFacade().GetSuggestedGIID());
            litSuggestedBundleGlid.Text = string.Format("Suggested Bundle Glid (if necessary): {0}", GetScriptGameItemFacade().GetSuggestedBundleGlid());

            trSuggestedGIGLID.Visible = true;
            trSuggestedGIID.Visible = true;
            trSuggestedBundleGlid.Visible = true;
		}

		protected void btnListGameItems_Click(object sender, EventArgs e)
		{
            ResetPage();
		}

		protected void btnCancelEdit_Click(object sender, EventArgs e)
		{
            ResetPage();
		}

        protected string PrepareLogMessage(ScriptGameItem oldGItem, ScriptGameItem newGItem)
        {
            string changeTemplate = ", {0}: CHANGED {1} -> {2}"; 
            string deleteTemplate = ", {0}: DELETED";
            string insertTemplate = ", {0}: ADDED {1}"; 

            StringBuilder changes = new StringBuilder();

            // Compare default properties
            if (oldGItem.Glid != newGItem.Glid)
                changes.Append(string.Format(changeTemplate, "GLID", oldGItem.Glid, newGItem.Glid));

            if (!oldGItem.Name.Equals(newGItem.Name))
                changes.Append(string.Format(changeTemplate, "name", oldGItem.Name, newGItem.Name));

            if (!oldGItem.ItemType.Equals(newGItem.ItemType))
                changes.Append(string.Format(changeTemplate, "itemType", oldGItem.ItemType, newGItem.ItemType));

            if (oldGItem.Level != newGItem.Level)
                changes.Append(string.Format(changeTemplate, "level", oldGItem.Level, newGItem.Level));

            if (!oldGItem.Rarity.Equals(newGItem.Rarity))
                changes.Append(string.Format(changeTemplate, "rarity", oldGItem.Rarity, newGItem.Rarity));

            if (!oldGItem.AutomaticInsertionStart.Equals(newGItem.AutomaticInsertionStart))
                changes.Append(string.Format(changeTemplate, "automaticInsertionStart", oldGItem.AutomaticInsertionStart, newGItem.AutomaticInsertionStart));

            // Compare all other old properties
            foreach (KeyValuePair<string, string> property in oldGItem.Properties)
            {
                // Deletions
                if (!newGItem.Properties.ContainsKey(property.Key))
                {
                    changes.Append(string.Format(deleteTemplate, property.Key));
                }
                else if(!newGItem.Properties[property.Key].Equals(property.Value)) // Modifications
                {
                    changes.Append(string.Format(changeTemplate, property.Key, property.Value, newGItem.Properties[property.Key]));
                }
            }

            // Check for insertions
            List<KeyValuePair<string, string>> newProperties = newGItem.Properties
                .Where(p => !oldGItem.Properties.ContainsKey(p.Key))
                .ToList();

            foreach (KeyValuePair<string, string> property in newProperties)
            {
                changes.Append(string.Format(insertTemplate, property.Key, property.Value));
            }

            string changeString = changes.ToString().TrimStart(new char[] {','});

            return string.Format("Admin - Updated a game item. GIGLID: {0} Modifications: {1}", oldGItem.GIGlid, (string.IsNullOrWhiteSpace(changeString) ? "None" : changeString));
        }

		protected void btnSaveGameItem_Click(object sender, EventArgs e)
		{
            if (Page.IsValid && ValidatePage())
            {
                string errorMsg;
                bool changeSuccessful = false;
                ScriptGameItem gItem = null;
                ScriptGameItem oldGItem = null;
                bool gameItemExists = FillGameItemFromInput(out gItem, out oldGItem);

                // Perform the insert or update
                if (gameItemExists && ValidateGameItemUpdate(gItem, out errorMsg))
                {
                    changeSuccessful = GetScriptGameItemFacade().UpdateSnapshotScriptGameItem(gItem, Configuration.KanevaUserId);
                }
                else if(ValidateGameItemInsert(gItem, out errorMsg))
                {
                    List<ScriptGameItem> gItems = new List<ScriptGameItem>();
                    gItems.Add(gItem);

                    changeSuccessful = (GetScriptGameItemFacade().InsertSnapshotScriptGameItems(ref gItems, Configuration.KanevaUserId, false, true));
                }

                if (changeSuccessful)
                {
                    // Log the CSR activity
                    if (gameItemExists)
                    {
                        string updateLogMsg = PrepareLogMessage(oldGItem, gItem);
                        this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), updateLogMsg, 0, gItem.GIGlid);
                    }
                    else
                    {
                        this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "Admin - Inserted new default game item.", 0, gItem.GIGlid);
                    }

                    ResetPage();
                }
                else
                {
                    divErrorData.InnerHtml = errorMsg;
                }
            }
            else
            {
                divErrorData.InnerHtml = "Unable to deserialize JSON string. Please ensure that input is correctly formatted.";
            }
		}

		protected void gvGameItems_Edit(object sender, GridViewEditEventArgs e)
		{
            dvGameItemEdit.Visible = true;
            dvGameItems.Visible = false;
            
            GridViewRow gvr = gvGameItems.Rows[e.NewEditIndex];
            ScriptGameItem gItem = GetScriptGameItemFacade().GetSnapshotScriptGameItem(Convert.ToInt32(gvr.Cells[1].Text), true);

            FillInputFromGameItem(gItem);

            litSuggestedBundleGlid.Text = string.Format("Suggested Bundle Glid (if necessary): {0}", GetScriptGameItemFacade().GetSuggestedBundleGlid());
            trSuggestedBundleGlid.Visible = true;

            e.Cancel = true;
		}

        protected void pg_PageChanged(object sender, PageChangeEventArgs e)
        {
            ResetPage(e.PageNumber);
        }

        #endregion
    }  
}