<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ManageCatalogItems.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.ManageCatalogItems" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<script type="text/javascript" src="../../jscript/yahoo/yahoo-min.js"></script>  
<script type="text/javascript" src="../../jscript/yahoo/event-min.js" ></script>   
<script type="text/javascript" src="../../jscript/yahoo/dom-min.js" ></script>   
<script type="text/javascript" src="../../jscript/yahoo/calendar-min.js"></script>
<script type="text/javascript" language="javascript" src="../../jscript/prototype.js"></script>

<link type="text/css" rel="stylesheet" href="../../css/yahoo/calendar.css" />   

<style >
	#cal1Container { display:none; position:absolute; z-index:100;}
	
	#popUpWinAddCat {
	width: 700px;
	background: #FFFFFF;
	text-align: center;
	padding: 40px 0px 10px;
	color: #990000;
	font-size: 14px;
	height: 275px;
	opacity:0.85;
	display: none;
	margin: 0px;
	border: 1px solid #990000;
	position: absolute;
	left: 100px;
	top: 350px;
	}
	
#popUpWinAddCat div {
	background: #FFFFFF;
	padding-right: 50px;
	padding-left: 50px;
	text-align: left;
	}
	
#popUpWinAddCat h1 {
	margin: 0px 0px 10px 5px;
	padding: 0px;
}
	
#popUpWinAddCat p {
	background: #FFFFFF;
	}

#popUpWinAddCat input {
	margin-top: 10px;
	width: 200px;
	}
	
#left td, #left td {padding:10px 5px;}
</style>
 
<script type="text/javascript"><!--
 function specialCharReplace()
 {
   //replace for item text, display text, and description;
    var itemName = document.getElementById('<%=tbx_ItemName.ClientID%>');
    itemName.value = itemName.value.replace(/amp;/g,"")

    var itemDisplayName = document.getElementById('<%=tbx_DisplayName.ClientID%>');
    itemDisplayName.value = itemDisplayName.value.replace(/amp;/g,"")

    var itemDescription = document.getElementById('<%=tbx_Description.ClientID%>');
    itemDescription.value = itemDescription.value.replace(/amp;/g,"")
 }

 try { YAHOO.namespace("example.calendar"); } catch (err) { }

    
function handleAddedDate(type,args,obj) {
	var dates = args[0]; 
	var date = dates[0];
	var year = date[0], month = date[1], day = date[2];
		
	var txtDate1 = document.getElementById('<%=txtItemAddedDate.ClientID%>');
	txtDate1.value = month + "-" + day + "-" + year;
	YAHOO.example.calendar.cal1.hide();
}
	
function initAddedDate() {
	try{
	// Admin Calendar
	YAHOO.example.calendar.cal1 = new YAHOO.widget.Calendar ("cal1", "cal1Container", { iframe:true, zIndex:200, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
	YAHOO.example.calendar.cal1.render();
		
	// Listener to show Admin Calendar when the button is clicked   
	YAHOO.util.Event.addListener("imgItemAddDate", "click", YAHOO.example.calendar.cal1.show, YAHOO.example.calendar.cal1, true);   
	YAHOO.example.calendar.cal1.selectEvent.subscribe(handleAddedDate, YAHOO.example.calendar.cal1, true);
	} catch (err){}
}

function updateItemImageURL() {
    var urlTextbox = $('<%=inp_itemImage.ClientID%>');
    var browseTHUMB = $('<%=browseTHUMB.ClientID%>');

    urlTextbox.value = browseTHUMB.value;
}
	
//--> </script>  

<center>
<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
 
 <table id="tbl_AvailableItems" border="0" cellpadding="0" cellspacing="0" width="970px" style="background-color:#ffffff">
    <tr>
        <td align="center"><span style="height:30px; font-size:28px; font-weight:bold">Catalog Item Management</span><br /><asp:Label runat="server" id="ErrorMessages"></asp:Label></td>
    </tr>
	<tr>
         <td style="height:20px"></td>
    </tr>
    <tr>
         <td><asp:Label id="lbl_NoCatalogItems" visible="false" runat="server"><span style="color:Navy; size:24pt; font-weight:bold">No Catalog Items Were Found.</span> </asp:Label></td>
    </tr>
    <tr>
		<td>
            <table border="0" width="98%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100px" align="left" ><asp:LinkButton visible="false" enabled="false" id="lbn_NewItem" runat="server" causesvalidation="false" onclick="lbn_NewItems_Click">Add New Item</asp:LinkButton></td>
                    <td align="right" style="padding:6px 0px 6px 6px; font-size:10pt"><b>Search by GLID, userId, or Name</b> &nbsp;<asp:TextBox runat="server" id="tbx_WOKSearchFilter"></asp:TextBox>&nbsp;<asp:Button id="btn_WOKSearch" causesvalidation="false" onclick="btn_WOKSearch_Click" runat="server" text="Search"></asp:Button></td>
                </tr>
            </table>
            
            <asp:gridview id="dg_CatalogItems" runat="server" onRowDataBound="dg_CatalogItems_RowDataBound" 
				onsorting="dg_CatalogItems_Sorting" OnRowCreated="dg_CatalogItemsmyGrid_OnRowCreated" 
				autogeneratecolumns="False" width="970px" OnPageIndexChanging="dg_CatalogItems_PageIndexChanging" 
				AllowSorting="True" border="0" cellpadding="2" 
				PageSize="5" AllowPaging="True" >
                
				<RowStyle BackColor="White"></RowStyle>
                <HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
                <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
                <FooterStyle BackColor="#FFffff"></FooterStyle>
                <Columns>
                    <asp:BoundField HeaderText="Item ID" DataField="global_id" SortExpression="global_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:TemplateField HeaderText="Username" SortExpression="item_creator_id">
                     <ItemTemplate> 
                        <asp:Label ID="Label1" runat="server" text='<%#GetUserName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "item_creator_id")))%>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Internal Name" DataField="name" SortExpression="name" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Display Name" DataField="display_name" SortExpression="display_name" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Item Description" DataField="description" SortExpression="description" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Date Added" DataField="date_added" SortExpression="date_added" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Market Cost" DataField="market_cost" SortExpression="market_cost" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Buy Back Price" DataField="selling_price" SortExpression="selling_price" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Creator" DataField="item_creator_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Inventory Type" DataField="inventory_type" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Active" DataField="item_active" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:TemplateField HeaderText="Catalog" SortExpression="item_active">
                     <ItemTemplate> 
                        <asp:Label ID="Label2" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "item_active").Equals(1)%>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                    <ItemTemplate> 
                        <asp:linkbutton ID="btnEdit" runat="server" text="Edit" commandargument='<%#DataBinder.Eval(Container.DataItem, "global_id")%>' oncommand="ShowEditFields_Click"></asp:linkbutton>
                    </ItemTemplate>
                    </asp:TemplateField>
			    </Columns>
            </asp:gridview>
         </td>
    </tr>
    <tr>
         <td style="height:20px"></td>
    </tr>
    <tr>
         <td>
            <asp:panel id="pnl_CatalogItemsDetails" runat="server" >

                <div style="border-top:1px solid #ccc;color:#000;text-align:left;margin-top:20px;padding:10px 0 16px 10px;width:940px;font-size:20px;"><asp:Label runat="server" id="lbl_DetailsTitle"></asp:Label></div>
				
				<table style="font-weight:bold" id="tbl_OfferDetails" border="0" frame="below" cellpadding="5px" cellspacing="0" width="970">
                    <tr>
						<td valign="top">
							<table id="left" cellpadding="5" cellspacing="0" border="0">
								<tr>
									<td colspan="2">
										<asp:dropdownlist id="dgd_drpActive" runat="server">
											<asp:ListItem Value="0">Inactive - Not shown in catalog or user inventory</asp:ListItem>
											<asp:ListItem Value="1">Public - Shown in catalog and inventory</asp:ListItem>
											<asp:ListItem Value="2">Private - Not shown in catalog</asp:ListItem>
											<asp:ListItem Value="3">Locked - Shown in catalog - owner can not edit or delete</asp:ListItem>
										</asp:dropdownlist><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small>(Inactive removes from Shopping site)</small>
									</td>
								</tr>
								<tr>
									<td align="left" valign="bottom" colspan="2">
										<div style="margin-left:12px;float:left;margin-right:12px;width:100px;height:100px;overflow:hidden;border:solid 1px #999;"><asp:Image runat="server" id="itemImage" width="100"></asp:Image></div>
										<div style="float:left;width:300px;height:100px;">
											<input class="formKanevaText" id="inp_itemImage" type="hidden" size="44" name="inp_itemImage" runat="server" /> 
											<input type="file" runat="server" name="browseTHUMB" id="browseTHUMB" onchange="updateItemImageURL();" style="width:200px;top:79px;position:relative;" />
										</div>
									</td>
								</tr>
								<tr>
								    <td align="right" style="width:120px"><label style="color: Red">*</label>Item Name:</td>
									<td align="left" style="width:350px">
										<asp:TextBox runat="server" maxlength="125" id="tbx_ItemName" width="280px"></asp:TextBox>
										<asp:RequiredFieldValidator runat="server" id="rfvItemName" controltovalidate="tbx_ItemName" text="* Please enter a value." errormessage="The item name is required." display="Dynamic"></asp:RequiredFieldValidator>
										<asp:TextBox runat="server" maxlength="125" visible="false" id="tbx_ItemID"></asp:TextBox>
									</td>
								</tr>
								<tr>
								    <td align="right" style="width:120px"><label style="color: Red">*</label>Display Name:</td>
									<td align="left" style="width:350px">
										<asp:TextBox runat="server" id="tbx_DisplayName" width="280px"></asp:TextBox>
										<asp:RequiredFieldValidator runat="server" id="rfvDisplayName" controltovalidate="tbx_DisplayName" text="* Please enter a value." errormessage="The display name is required." display="Dynamic"></asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td align="right" style="width:120px;padding-top:12px;" valign="top"><label style="color: Red">*</label>Description:</td>
									<td align="left" style="width:350px">
										<asp:TextBox runat="server" id="tbx_Description" width="280px" TextMode="MultiLine" Rows="4"></asp:TextBox>
										<asp:RequiredFieldValidator runat="server" id="rfvDescription" controltovalidate="tbx_Description" text="* Please enter a value." errormessage="The item description is required." display="Dynamic"></asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td align="right" valign="top" style="padding-top:12px;"><label style="color: Red">*</label>Market Cost:</td>
									<td align="left">
										<asp:TextBox runat="server" id="tbx_MarketCost" width="50px" causesvalidation="false" autopostback="true" ontextchanged="tbx_MarketCost_TextChanged"></asp:TextBox>
										<asp:RequiredFieldValidator runat="server" id="rfvMarketCost" controltovalidate="tbx_MarketCost" text="* Please enter a value." errormessage="The market cost is required." display="Dynamic"></asp:RequiredFieldValidator>
										&nbsp;
										<asp:TextBox runat="server" id="tbx_DesignerPrice" Enabled="False"  width="50px" causesvalidation="false" ontextchanged="tbx_DesignerPrice_TextChanged"></asp:TextBox>
											<br/>
											<small>Base Price</small>&nbsp;&nbsp;&nbsp;
											<small>Designer Commission</small>
										<div id="divTemplateItem" runat="server" visible="false" style="margin-top:16px;">
											<small>Template Global Id: <span id="spnTemplateItemId" runat="server"></span></small><br />
											<asp:TextBox runat="server" id="txtTemplatePrice" Enabled="False"  width="50px" causesvalidation="false" autopostback="true" ontextchanged="tbx_MarketCost_TextChanged"></asp:TextBox>
                            				&nbsp;
											<asp:TextBox runat="server" id="txtTemplateDesignerPrice" Enabled="False" width="50px" causesvalidation="false" autopostback="true"></asp:TextBox>
											<br/>
											<small>Base Price</small>&nbsp;&nbsp;&nbsp;
											<small>Designer Commission</small>
										</div>
										<div id="divBundlePrice" runat="server" visible="false">
											<div class="pricing">
												<div class="title">Bundle Pricing:</div>
												<div class="row"><span>Items Total*</span><div id="totalNotOwnedItems" runat="server">0</div></div>
												<div class="row"><span>Items You Own + 10% catalog fee</span><div id="totalOwnedItems" runat="server">0</div></div>
												<div class="row"><span>Design Commission + 10% catalog fee</span><div id="totalDesignCommission" runat="server">0</div></div>
												<div class="line"></div>
												<div class="row total"><span>Total Price:</span><div id="totalBundle" runat="server">0</div></div>
												<p>*Does not include items you own</p>
											</div>
										</div>
									</td>
								</tr>
								<tr id="trBuyBackPrice" runat="server">
									<td align="right"><label style="color: Red">*</label>Buy Back Price:</td>
									<td align="left" title="select the buyback percentage">
										<ajax:ajaxpanel id="Ajaxpanel2" runat="server"> 
										<asp:TextBox runat="server" id="tbx_SellingPrice" enabled="false" width="50px"></asp:TextBox> <asp:DropDownList autopostback="true" width="50px" runat="server" id="ddl_BBPrice" onselectedindexchanged="ddl_BBPrice_SelectedIndexChanged"></asp:DropDownList>
										<asp:RequiredFieldValidator runat="server" id="rfvSellingPrice" controltovalidate="tbx_SellingPrice" text="* Please enter a value." errormessage="The Selling Price is required." display="Dynamic"></asp:RequiredFieldValidator>
										</ajax:ajaxpanel>
									</td>
								</tr>
								<tr>
									<td align="right"><label style="color: Red">*</label>Currency Type:</td>
									<td align="left">
										<asp:DropDownList runat="server" id="ddl_CurrencyType" width="200px"></asp:DropDownList>
									</td>
								</tr>
								<tr>
									<td align="right" style="width:120px;padding-top:2px;"><label style="color: Red">*</label>Date Added:</td>
									<td align="left" style="width:350px">
										<asp:TextBox enabled="false" ID="txtItemAddedDate" class="formKanevaText" style="width:70px" MaxLength="10" runat="server"/>
										<img id="imgItemAddDate" name="imgItemAddDate" onload="initAddedDate();" src="images/cal_16.gif" />						
										<br/><small>MM-dd-yyyy</small><div id="cal1Container"></div>
										<asp:RequiredFieldValidator runat="server" id="rfvStartDate" controltovalidate="txtItemAddedDate" text="* Please enter a value." errormessage="The item creation date is required." display="Dynamic"></asp:RequiredFieldValidator>
								   </td>
								</tr>
								<tr>
									<td align="right" valign="top" style="width:120px;padding-top:12px;" ><label style="color: Red">*</label>Creator Name:</td>
									<td align="left" style="width:350px" valign="top" >
										<table border="0" cellpadding="0" cellspacing="0" width="300px">
											<tr>
												<td align="left" style="padding:0;">
													<asp:TextBox runat="server" maxlength="125" id="tbx_CreatorName" enabled="False" width="280px"></asp:TextBox>
													<asp:RequiredFieldValidator runat="server" id="rfvCreatorName" controltovalidate="tbx_CreatorName" text="* Please enter a value." errormessage="The item name is required." display="Dynamic"></asp:RequiredFieldValidator>
													<asp:TextBox runat="server" maxlength="125" visible="false" id="tbx_CreatorID"></asp:TextBox>
												</td>
											</tr>
											<tr>																																																																																																																																																																																																																																																		<tr>
												<td align="left" style="padding:15px 0 0 0;"">
													<b>Users:</b>&nbsp;&nbsp;<asp:TextBox runat="server" id="tbx_OwnersSearchFilter"></asp:TextBox>&nbsp;&nbsp;<asp:Button id="btn_UsersSearch" causesvalidation="false" onclick="btn_UsersSearch_Click" runat="server" text="  Find  "></asp:Button>   
                                        
													<asp:GridView runat="server" id="dgd_Users" style="margin-top:16px;" AllowPaging="True" cellpadding="1" PageSize="15" 
														OnPageIndexChanging="dgd_Users_PageIndexChanging" onselectedindexchanged="dgd_Users_SelectedIndexChanged" 
														autogeneratecolumns="False" width="280px" gridlines="None" borderwidth="1">
														<HeaderStyle Font-Bold="True" backcolor="#666666" forecolor="#ffffff"></HeaderStyle>
														<RowStyle BackColor="White" font-bold="false"></RowStyle>
														<AlternatingRowStyle BackColor="#eeeeee"></AlternatingRowStyle>
														<FooterStyle BackColor="#ffffff"></FooterStyle>
														<Columns>
															<asp:BoundField HeaderText="User ID" visible="true" DataField="user_id" controlstyle-height="12">
																<headerstyle horizontalalign="Left" />
															</asp:BoundField>
															<asp:BoundField ReadOnly="True" HeaderText="Username" DataField="username" controlstyle-height="12">
																<headerstyle horizontalalign="Left"  />
															</asp:BoundField>
															<asp:CommandField CausesValidation="False" ShowCancelButton="False" ShowSelectButton="True"></asp:CommandField>
														</Columns>
													</asp:GridView>
											   </td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td>
							<table id="right" cellpadding="5" cellspacing="0" border="0">
								<tr>
									<td align="center" colspan="2">
										<asp:button ID="btnRemoveFromInventory" runat="server" Text="Remove from Shopping and Remove From All Inventories" causesValidation="false" onclick="btnRemoveInventory_Click"/>
									</td>
								</tr>
					            <tr id="trBundleItems" runat="server" visible="false">
									<td align="right" valign="top" style="padding-top:20px;" id="tdBundleItemsLabel">Bundle Items:</td>
									<td align="left" style="padding-top:16px;">
										<div style="width:330px;height:80px;overflow:auto;border:solid 1px #ccc;">
										<asp:repeater id="rptBundleItems" runat="server" >
											<itemtemplate>
												<div style="margin:2px 0;padding:2px 0 2px 6px;">
													<img style="vertical-align:middle;margin-right:3px;" src='<%# GetItemImageURL (DataBinder.Eval (Container.DataItem, "ThumbnailSmallPath").ToString(), "sm")%>' />
													<%# DataBinder.Eval (Container.DataItem, "DisplayName") %>
													<%# ShowBundleItemQuantity (DataBinder.Eval (Container.DataItem, "Quantity"), DataBinder.Eval (Container.DataItem, "UseType"))%>	
													- <%# DataBinder.Eval (Container.DataItem, "GlobalId") %>
												</div>
											</itemtemplate>
										</asp:repeater>
										</div>
									</td>
								</tr>
								<tr>
									<td align="right" valign="top" style="padding-top:22px;">Access Type:</td>
									<td align="left" style="padding-top:18px;">
										<asp:CheckBox runat="server" id="cbAP" text="Access Pass" ></asp:CheckBox> 
									</td>
								</tr>
								<tr>
									<td align="right" valign="top" style="width:120px;padding-top:18px;">Categories:<br /><small>(Limit of 3)</small>&nbsp;</td>
									<td align="left" style="width:350px">
										<ajax:ajaxpanel id="AjaxpanelRemoveCat" runat="server"> 
											<ol style="padding-left:24px;">
												<li style="line-height:16px;"><asp:Label ID="lblCategory1" runat="server">N/A</asp:Label>&nbsp;<asp:LinkButton id="lbDeleteCategory" Text='Remove' OnCommand="lbCategoryDelete_Click" runat="server" CommandName="cmdClick" CommandArgument='1'/></li>
												<li style="line-height:16px;"><asp:Label ID="lblCategory2" runat="server">N/A</asp:Label>&nbsp;<asp:LinkButton id="lbDeleteCategory1" Text='Remove' OnCommand="lbCategoryDelete_Click" runat="server" CommandName="cmdClick" CommandArgument='2'/></li>
												<li style="line-height:16px;"><asp:Label ID="lblCategory3" runat="server">N/A</asp:Label>&nbsp;<asp:LinkButton id="lbDeleteCategory2" Text='Remove' OnCommand="lbCategoryDelete_Click" runat="server" CommandName="cmdClick" CommandArgument='3'/></li>
											</ol>
										</ajax:ajaxpanel>
									</td>
								</tr>
								<tr>
									<td align="right" valign="top" style="width:120px;padding-top:18px;">Category:</td>
									<td align="left">
										<ajax:ajaxpanel id="AjaxpanelAddCat" runat="server"> 
											<div id="catBreadCrumb" runat="server" style="padding:14px 0 16px 0;"></div>
											<asp:Repeater ID="rptAddCategories" runat="server">
												<ItemTemplate>                
												   <asp:checkbox runat="server" id="chkSelect"></asp:checkbox>
												   <asp:LinkButton id="lbCategory" AjaxCall="Async" CausesValidation="False" Text='<%# DataBinder.Eval(Container.DataItem, "name")%>' OnCommand="lbCategory_Click" runat="server" CommandName="cmdClick" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ItemCategoryId")%>'/>
												   <input type="hidden" runat="server" id="hidItemCategoryId" value='<%#DataBinder.Eval(Container.DataItem, "ItemCategoryId")%>' name="hidItemCategoryId"/>
												   <br />
												</ItemTemplate>
											</asp:Repeater>
                                
											<div style="padding:20px 0;">
											<asp:Button runat="server" AjaxCall="Async" id="btnSaveCategory1" Text="Add Cat 1" OnClick="btnSaveCategory1_Click" CausesValidation="False"></asp:Button>
											<asp:Button runat="server" AjaxCall="Async" id="btnSaveCategory2" Text="Add Cat 2" OnClick="btnSaveCategory2_Click" CausesValidation="False"></asp:Button>
											<asp:Button runat="server" AjaxCall="Async" id="btnSaveCategory3" Text="Add Cat 3" OnClick="btnSaveCategory3_Click" CausesValidation="False"></asp:Button>
											<asp:button runat="server" AjaxCall="Async" id="btnAddCategory" text="Go To Top" OnClick="btnAddCategory_Click" CausesValidation="False"/>
											</div>

											<asp:checkboxlist style="display:none;" id="cblCategories" runat="server" repeatcolumns="2" repeatdirection="Vertical">
												<asp:listitem style="margin-right:10px;"></asp:listitem>
											</asp:checkboxlist>
										</ajax:ajaxpanel>
									</td>
								</tr>
								<tr>
									<td align="right" valign="top">Store Locations:</td>
									<td align="left" valign="top">
										<ajax:ajaxpanel id="ajItemData3" runat="server"> 
											<div style="height:160px;width:240px;overflow:auto;border:1px solid #ccc;">
												<asp:GridView runat="server" id="dgd_Stores" AllowPaging="false" cellpadding="2" border="0" showheader="false" autogeneratecolumns="False" width="220px">
													<Columns>
														<asp:TemplateField>
															<ItemTemplate>
																<asp:CheckBox runat="server" id="cbx_stores" autopostback="true"  checked='<%# DataBinder.Eval(Container.DataItem, "cbx_instore")%>' oncheckedchanged="cbx_stores_CheckedChanged"></asp:CheckBox>
															</ItemTemplate>
														</asp:TemplateField>                                       
															<asp:TemplateField visible="false">
															<ItemTemplate>
																<asp:Label runat="server" id="lbl_storeId" Text='<%# DataBinder.Eval(Container.DataItem, "store_id")%>' style="text-align:left;"> </asp:Label>
															</ItemTemplate>
														</asp:TemplateField>                                       
														<asp:BoundField ReadOnly="True" HeaderText="Store Name" DataField="name"></asp:BoundField>
													</Columns>
												</asp:GridView>
											</div>
										</ajax:ajaxpanel>
									</td>
								</tr>
                    		</table>

						</td>
					</tr>
					<tr>
						<td align="center" colspan="2" style="padding:30px 0;">
							<asp:Button runat="server" id="btn_Cancel" Text="Cancel" causesvalidation="false" onclick="btn_Cancel_Click"></asp:Button>
							<asp:Button runat="server" id="btn_Save" Text="Update Item" onclientclick="specialCharReplace();" onclick="btn_Save_Click" style="margin-left:20px;"></asp:Button>
						</td>
					</tr>
				</table>
            </asp:panel>
        </td>
    </tr>
</table>

</asp:panel>

</center>