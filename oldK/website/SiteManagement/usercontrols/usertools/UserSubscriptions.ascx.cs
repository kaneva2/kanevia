using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;
using System.Text;
using MagicAjax;


namespace KlausEnt.KEP.SiteManagement
{
    public partial class UserSubscriptions : BaseUserControl
    {
        #region Declarations
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private uint USER_SUBSCRIPTIONID = 0;
        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.USER_DATA_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                if (Session["SelectedUserId"] != null)
                {
                    // Load the current user's data the first time
                    SELECTED_USER_ID = Convert.ToInt32(Session["SelectedUserId"]);

                    if (!IsPostBack)
                    {

                        // Log the CSR activity
                        this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Managing User Subscriptions", 0, SELECTED_USER_ID);

                        //get the available promotions
                        LoadPassBasedPromotions();

                        //load edit pull downs
                        PopulateSubscriptionTerms();
                        PopulateSubscriptionStatus();

                    }

                    //populate post list grid
                    BindData(1, "");
                }
                else
                {
                    Session["CallingPage"] = Request.Url.ToString();
                    Response.Redirect(ResolveUrl("~/Default.aspx?ws=" + GetCurrentSiteID((int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.KANEVA) + "&mn=" + GetUserSearchParentMenuID() + "&sn=" + GetUserSearchControlID()));
                }

            }
            else
            {
                RedirectToHomePage();
            }
        }

        #endregion

        #region Functions

        private void LoadPassBasedPromotions()
        {
            //set the promtions filter
            string promotionTypeFilter = ((int)Promotion.ePROMOTION_TYPE.SUBSCRIPTION_PASS).ToString() + "," + ((int)Promotion.ePROMOTION_TYPE.MATURE_SUBSCRIPTION_PASS).ToString();

            //get the list of promotions
            List<Promotion> passPromotions = GetPromotionsFacade().GetActivePromotions(promotionTypeFilter, Constants.CURR_KPOINT);

            //bind it to the drop down
            ddl_PassPromotions.DataSource = passPromotions;
            ddl_PassPromotions.DataTextField = "BundleTitle";
            ddl_PassPromotions.DataValueField = "PromotionId";
            ddl_PassPromotions.DataBind();

            //add an unavailable option for subscriptions tied to expired or deleted promotions
            ddl_PassPromotions.Items.Insert(0,new ListItem("Unavailable", "-1"));
        }

        private void LoadSubscriptions4Promotion()
        {
            try
            {
                //set a default empty list
                List<Subscription> relatedSubscriptions = new List<Subscription>();

                //get the select promotion Id
                int promoId = Convert.ToInt32(ddl_PassPromotions.SelectedValue);

                if (promoId >= 0)
                {
                    //get the associated subscription(s)
                    relatedSubscriptions = GetSubscriptionFacade().GetSubscriptionsByPromotionId((uint)promoId);

                }

                //bind to the pull down
                ddl_Subscriptions.DataSource = relatedSubscriptions;
                ddl_Subscriptions.DataTextField = "Name";
                ddl_Subscriptions.DataValueField = "SubscriptionId";
                ddl_Subscriptions.DataBind();

                //add an unavailable option for subscriptions tied to expired or deleted promotions
                ddl_Subscriptions.Items.Insert(0, new ListItem("Unavailable", "-1"));
            }
            catch (FormatException)
            { 
            }
        }

        private void PopulateSubscriptionTerms()
        {
            ddp_SubscriptionPeriod.DataSource = (new SubscriptionFacade()).GetSubscriptionTerms();
            ddp_SubscriptionPeriod.DataTextField = "subscription_term";
            ddp_SubscriptionPeriod.DataValueField = "subscription_term_id";
            ddp_SubscriptionPeriod.DataBind();

            ddl_InitialTerm.DataSource = ddp_SubscriptionPeriod.DataSource;
            ddl_InitialTerm.DataTextField = "subscription_term";
            ddl_InitialTerm.DataValueField = "subscription_term_id";
            ddl_InitialTerm.DataBind();
        }

        private void PopulateSubscriptionStatus()
        {
            ddp_SubscriptionStatus.DataSource = (new SubscriptionFacade()).GetSubscriptionStatus();
            ddp_SubscriptionStatus.DataTextField = "name";
            ddp_SubscriptionStatus.DataValueField = "status_id";
            ddp_SubscriptionStatus.DataBind();
        }

        private void LoadUserSubscriptionData(UserSubscription userSubscription)
        {
            //prepopulate fields with values based on subscription if new
            if (userSubscription == null)
            {
                //get the subscriptions object
                Subscription relatedSubscription = GetSubscriptionFacade().GetSubscription(Convert.ToUInt32(ddl_Subscriptions.SelectedValue));

                //create new user subscription
                userSubscription = new UserSubscription();
                userSubscription.PromotionId = Convert.ToUInt32(ddl_PassPromotions.SelectedValue);
                userSubscription.SubscriptionId = relatedSubscription.SubscriptionId;
                userSubscription.AutoRenew = false;
                userSubscription.RenewalTerm = relatedSubscription.Term;
                userSubscription.StatusId = Subscription.SubscriptionStatus.Pending;
                userSubscription.BillingType = (int)Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE;
                userSubscription.UserCancelled = false;
                userSubscription.InitalTerm = relatedSubscription.Term;
                userSubscription.PurchaseDate = DateTime.Now;
                userSubscription.CancelledDate = DateTime.Now;
                userSubscription.EndDate = DateTime.Now.AddDays(relatedSubscription.DaysFree);
                userSubscription.Price = (double)relatedSubscription.Price;
                userSubscription.MontlyAllowance = relatedSubscription.MonthlyAllowance;
                userSubscription.DiscountPercent = relatedSubscription.DiscountPercent;
                userSubscription.UserId = SELECTED_USER_ID;
                userSubscription.SubscriptionIdentifier = "-11";
                userSubscription.UserSubscriptionId = 0;
            }

            //populate fields
            try
            {
                ddl_PassPromotions.SelectedValue = userSubscription.PromotionId.ToString();
            }
            catch (Exception)
            {
                ddl_PassPromotions.SelectedValue = "-1";
            }
            try
            {
                LoadSubscriptions4Promotion();
                ddl_Subscriptions.SelectedValue = userSubscription.SubscriptionId.ToString();
            }
            catch (Exception)
            {
                ddl_PassPromotions.SelectedValue = "-1";
            }

            cbx_AutoRenew.Checked = userSubscription.AutoRenew;
            ddp_SubscriptionPeriod.SelectedValue = ((int)userSubscription.RenewalTerm).ToString();
            ddp_SubscriptionStatus.SelectedValue = ((int)userSubscription.StatusId).ToString();
            ddl_BillType.SelectedValue = userSubscription.BillingType.ToString();
            cbx_UserCancelled.Checked = userSubscription.UserCancelled;
            ddl_InitialTerm.SelectedValue = ((int)userSubscription.InitalTerm).ToString();
            tbx_PurchasedDate.Text = userSubscription.PurchaseDate.ToString();
            tbx_CancelledDate.Text = userSubscription.CancelledDate.ToString();
            tbx_EndDate.Text = userSubscription.EndDate.ToString();
            tbx_Price.Text = userSubscription.Price.ToString();
            tbx_MonthlyAllowance.Text = userSubscription.MontlyAllowance.ToString();
            tbx_Discount.Text = userSubscription.DiscountPercent.ToString();
            tbx_FreeTrialDate.Text = userSubscription.FreeTrialEndDate.ToString();
            lbl_selectedUserSubscriptionId.Text = userSubscription.UserSubscriptionId.ToString();
            
            //enable editing fields
            tr_EditPass.Visible = true;
        }

        /// <summary>
        /// BindData
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="filter"></param>
        private void BindData(int pageNumber, string filter)
        {
            string orderby = CurrentSort + " " + CurrentSortOrder;

            int passesPerPage = 10;

            USER_SUBSCRIPTIONS = GetSubscriptionFacade().GetUserSubscriptions(SELECTED_USER_ID);

            //pgTop.NumberOfPages = Math.Ceiling((double)userSubscriptions.Count / passesPerPage).ToString();
            //pgTop.DrawControl();

            rptPasses.DataSource = USER_SUBSCRIPTIONS;
            rptPasses.DataBind();

            // The results
            lblSearch.Text = GetResultsText(USER_SUBSCRIPTIONS.Count, pageNumber, passesPerPage, USER_SUBSCRIPTIONS.Count);

            //display user name
            lblSelectedUser.Text = "Notes for " + SELECTED_USER_NAME;
        }

        protected string GetPromotionName(uint suscriptionId)
        {
            string promotionName = "n/a";

            try
            {
                Subscription subscription = (new SubscriptionFacade()).GetSubscription(suscriptionId);
                promotionName = subscription.Name;
            }
            catch (Exception ex)
            {
                m_logger.Error("Error retrieving subscription data", ex);
            }

            return promotionName;
        }

        //delegate function used in LIst FindAll search
        private bool GetUserSubscription(UserSubscription userSubscript)
        {
            return userSubscript.UserSubscriptionId == USER_SUBSCRIPTIONID;
        }

        private void UpdateWOKData(UserSubscription userSubscription)
        {
                //get all the pass groups associated with the subscription
                DataRow[] matchingPassGroups = WebCache.GetPassGroupsToSubscription().Select("subscription_id = " + userSubscription.SubscriptionId);

                //process for each pass group associated with the subscription
                for (int i = 0; i < matchingPassGroups.Length; i++)
                {
                    DataRow drMPG = matchingPassGroups[i];
                    int passGroupId = Convert.ToInt32(drMPG["pass_group_id"]);
                    
                    //does extra features and emailing based on pass type
                    if (passGroupId == (int)KanevaGlobals.VipPassGroupID)
                    {
                        //if this fails keep processing
                        try
                        {
                            (new UserFacade()).ChangePlayerNameColor(userSubscription.UserId, (int)Constants.ePLAYER_NAME_COLORS.VIP);
                        }
                        catch (Exception) { }
                    }
                    else if (passGroupId == (int)KanevaGlobals.AccessPassGroupID)
                    {
                        //if this fails keep processing
                        try
                        {
                            (new UserFacade()).UpdateUser(userSubscription.UserId, 1);
                        }
                        catch (Exception) { }
                    }
                    
                    try
                    {
                        //add the user to the pass group table
                        //this will throw an exception if the user already has a pass
                        //add logic to check before adding later
                        UsersUtility.AddUserToPassGroup(userSubscription.UserId, passGroupId);
                    }
                    catch (Exception) { }
                }

        }

        private void CancelSubscription(UserSubscription userSubscription)
        {
            bool succeed = false;
            bool bSystemDown = false;
            string userErrorMessage = "";

            try
            {

                //create the class instances
                SubscriptionFacade subFacade = new SubscriptionFacade();
                CybersourceAdaptor cybersourceAdaptor = new CybersourceAdaptor();

                //logic to protect against users trying to hack a force cancel on grandfathered Access Passes
                //should be able to remove eventually
                //check for legacy and non renewing subscriptions
                bool autorenew = userSubscription.AutoRenew;
                string subscriptionIdentifier = userSubscription.SubscriptionIdentifier;
                if ((!autorenew) || (subscriptionIdentifier == null) || (subscriptionIdentifier.Length < 5))
                {
                    messages.ForeColor = System.Drawing.Color.DarkRed;
                    messages.Text = "This subscription may not be cancelled.";
                    return;
                }


                //cancel the subscription the user requested cancelling
                userSubscription.UserCancelled = true;
                userSubscription.UserCancellationReason = "Administrative Cancellation";
                userSubscription.CancelledDate = DateTime.Now;

                if (userSubscription.IsLegacy)
                {
                    succeed = cybersourceAdaptor.CancelSubscription(ref bSystemDown, ref userErrorMessage, userSubscription);
                }
                else
                {
                    // Update DB
                    userSubscription.StatusId = Subscription.SubscriptionStatus.Cancelled;
                    subFacade.UpdateUserSubscription(userSubscription);

                    // No need to call cybersource
                    succeed = true;
                }

                if (succeed)
                {

                    //tickle the wok servers
                    (new RemoteEventSender()).BroadcastItemChangeEvent(RemoteEventSender.ObjectType.PlayerPassList, RemoteEventSender.ChangeType.UpdateObject, userSubscription.UserId);

                    //new confirm with redirect as requested by business
                    //bypasses the rebind 
                    string operation = "alert('Subscription successfully cancelled.');";
                    operation += "\n\r window.location.replace(\"" + this.ResolveUrl("~/mykaneva/buySpecials.aspx?pass=true") + "\");";
                    MagicAjax.AjaxCallHelper.Write(operation);

                    //run specialized logic to offer VIP/AP Combo users the option to keep/get one of the other passes
                    //covered under the VIPAP Combo
                    if (userSubscription.SubscriptionId == (uint)KanevaGlobals.VipPassGroupID)
                    {
                        ////get the subscription Id of the AP Pass
                        //Subscription apSubscription = subFacade.GetSubscriptionByPromotionId((uint)KanevaGlobals.AccessPassGroupID);

                        ////get the users access pass subscription
                        //UserSubscription usersAPSubscription = subFacade.GetUserSubscription((uint)apSubscription.SubscriptionId, (uint)GetUserId());

                        ////adjust the price back to the normal AP price
                        //usersAPSubscription.Price = apSubscription.Price;
                        //subFacade.UpdateUserSubscription(usersAPSubscription);

                        ////adjust the price on cyber source as well
                        //succeed = cybersourceAdaptor.UpdateSubscription(ref bSystemDown, ref userErrorMessage, usersAPSubscription);

                        //if (!succeed)
                        //{
                        //    //display success message
                        //    spnMessage.InnerText = userErrorMessage;
                        //    return;
                        //}
                    }
                }
                //display results message
                messages.ForeColor = System.Drawing.Color.DarkGreen;
                messages.Text = succeed ? "Subscription successfully cancelled." : userErrorMessage;

            }

            catch (Exception ex)
            {
                m_logger.Error("Error trying to cancel cybersource subscription", ex);
                messages.ForeColor = System.Drawing.Color.DarkRed;
                messages.Text = "Error during cancellation of subscription.";
            }
        }

        private void TickleServer()
        {
            // Dev note, the below assumes this is a pass subscription change
            //tickle the wok servers
            try
            {
                (new RemoteEventSender()).BroadcastItemChangeEvent(RemoteEventSender.ObjectType.PlayerPassList, RemoteEventSender.ChangeType.UpdateObject, SELECTED_USER_ID);
            }
            catch (Exception ex)
            {
                m_logger.Error("Error while trying to tickle server", ex);
            }

        }

        private void ResetUserSubscriptionArea()
        {
            //hide the row
            tr_EditPass.Visible = false;

            //clear the fields
            //ddl_PassPromotions.SelectedIndex = 0;
            //ddl_Subscriptions.SelectedIndex = 0;
            cbx_AutoRenew.Checked = false;
            ddp_SubscriptionPeriod.SelectedIndex = 0;
            ddp_SubscriptionStatus.SelectedIndex = 0;
            ddl_BillType.SelectedValue = "0";
            cbx_UserCancelled.Checked = false;
            ddl_InitialTerm.SelectedIndex = 0;
            tbx_PurchasedDate.Text = "";
            tbx_CancelledDate.Text = "";
            tbx_EndDate.Text = "";
            tbx_Price.Text = "";
            tbx_MonthlyAllowance.Text = "";
            tbx_Discount.Text = "";
            tbx_FreeTrialDate.Text = "";

            ddl_PassPromotions.Enabled = false;
            ddl_Subscriptions.Enabled = false;

            USER_SUBSCRIPTIONID = 0;
        }

        #endregion

        #region Event Handlers

        protected void SavePassChanges_Click(object sender, System.EventArgs e)
        {
            //get the usersubscription id
            try
            {
                if (Page.IsValid)
                {
                    int userSubscriptionID = Convert.ToInt32(this.lbl_selectedUserSubscriptionId.Text);
                    UserSubscription userSubscription = new UserSubscription();

                    //if the user subscription id is -1 it is a new subscription other wise process as an update
                    if (userSubscriptionID > 0)
                    {
                        //update search(find) value
                        USER_SUBSCRIPTIONID = (uint)userSubscriptionID;
                        userSubscription = USER_SUBSCRIPTIONS.Find(GetUserSubscription);
                    }
                    else
                    {
                        //get the promotion and subscription this is to be tied to
                        int promotionId = Convert.ToInt32(ddl_PassPromotions.SelectedValue);
                        int subscriptionId = Convert.ToInt32(ddl_Subscriptions.SelectedValue);

                        if ((promotionId < 0) || (subscriptionId < 0))
                        {
                            messages.ForeColor = System.Drawing.Color.DarkRed;
                            messages.Text = "You must select both a promotion and a subscription for this user subscription.";
                            MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");

                            return;
                        }

                        userSubscription.PromotionId = (uint)promotionId;
                        userSubscription.SubscriptionId = (uint)subscriptionId;
                        userSubscription.UserSubscriptionId = 0;
                        userSubscription.UserId = SELECTED_USER_ID;
                        userSubscription.SubscriptionIdentifier = "-11";
                    }

                    //gather values
                    userSubscription.AutoRenew = cbx_AutoRenew.Checked;
                    userSubscription.RenewalTerm = (Subscription.SubscriptionTerm)Convert.ToUInt32(ddp_SubscriptionPeriod.SelectedValue);
                    userSubscription.StatusId = (Subscription.SubscriptionStatus)Convert.ToUInt32(ddp_SubscriptionStatus.SelectedValue);
                    userSubscription.BillingType = Convert.ToInt32(ddl_BillType.SelectedValue);
                    userSubscription.UserCancelled = cbx_UserCancelled.Checked;
                    userSubscription.InitalTerm = (Subscription.SubscriptionTerm)Convert.ToUInt32(ddl_InitialTerm.SelectedValue);
                    userSubscription.PurchaseDate = Convert.ToDateTime(tbx_PurchasedDate.Text);
                    userSubscription.CancelledDate = Convert.ToDateTime(tbx_CancelledDate.Text);
                    userSubscription.EndDate = Convert.ToDateTime(tbx_EndDate.Text);
                    userSubscription.Price = Convert.ToDouble(tbx_Price.Text);
                    userSubscription.MontlyAllowance = Convert.ToUInt32(tbx_MonthlyAllowance.Text);
                    userSubscription.DiscountPercent = Convert.ToUInt32(tbx_Discount.Text);
                    userSubscription.FreeTrialEndDate = Convert.ToDateTime(tbx_FreeTrialDate.Text);

                    //save changes
                    if (userSubscription.UserSubscriptionId > 0)
                    {
                        GetSubscriptionFacade().UpdateUserSubscription(userSubscription);

                        //process the cyber source account based on the select subscription status
                        switch (userSubscription.StatusId)
                        {
                            case Subscription.SubscriptionStatus.Active:
                                break;
                            case Subscription.SubscriptionStatus.Cancelled:
                                CancelSubscription(userSubscription);
                                break;
                            case Subscription.SubscriptionStatus.Expired:
                                break;
                            case Subscription.SubscriptionStatus.NoPayment:
                                break;
                            case Subscription.SubscriptionStatus.Pending:
                                break;
                            case Subscription.SubscriptionStatus.Refunded:
                                break;
                        }

                    }
                    else
                    {
                        GetSubscriptionFacade().InsertUserSubscription(userSubscription);
                    }

                    //update wok data
                    UpdateWOKData(userSubscription);

                    //tickle the servers
                    TickleServer();

                    //reset the the fields
                    ResetUserSubscriptionArea();

                    //bind the data 
                    BindData(pgTop.CurrentPageNumber, "");

                    //get the user a success message
                    messages.ForeColor = System.Drawing.Color.DarkGreen;
                    messages.Text = "Changes saved successfully.";
                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
                }
            }
            catch (FormatException)
            {
                messages.ForeColor = System.Drawing.Color.DarkRed;
                messages.Text = "Convert Error while processing provided values.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
            catch (Exception ex)
            {
                messages.ForeColor = System.Drawing.Color.DarkRed;
                messages.Text = "Error:" + ex.Message;
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        protected void btn_Cancel_Click(object sender, System.EventArgs e)
        {
            ResetUserSubscriptionArea();
        }

        protected void ddl_PassPromotions_SelectedIndexChanged(object sender, EventArgs e)
        {
            //load the relatedsubscriptions
            if (Convert.ToInt32(ddl_PassPromotions.SelectedValue) > 0)
            {
                LoadSubscriptions4Promotion();
            }
            else
            {
                messages.ForeColor = System.Drawing.Color.DarkRed;
                messages.Text = "You must select a promotion.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        protected void ddl_Subscriptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((Convert.ToInt32(ddl_Subscriptions.SelectedValue) > 0) && (Convert.ToInt32(ddl_PassPromotions.SelectedValue) > 0))
            {
                //display the user subscriptions data
                LoadUserSubscriptionData(null);
            }
            else
            {
                messages.ForeColor = System.Drawing.Color.DarkRed;
                messages.Text = "You must select both a promotion and a subscription for this user subscription.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        private void rptPasses_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //find all the controls in the repeater
                Label lblUserSubscriptionId = (Label)e.Item.FindControl("lbl_userSubscriptionId");
                Label lblPassName = (Label)e.Item.FindControl("lbl_PassName");
                Label lblPrice = (Label)e.Item.FindControl("lbl_Price");
                Label lblSubscrptIdent = (Label)e.Item.FindControl("lbl_SubscrptIdent");
                Label lblMonthlyAllowance = (Label)e.Item.FindControl("lbl_MonthlyAllowance");
                Label lblEndDate = (Label)e.Item.FindControl("lbl_EndDate");
                LinkButton lbnDelete = (LinkButton)e.Item.FindControl("lbn_delete");

                //set alert box to delete button
                StringBuilder sb = new StringBuilder();
                sb.Append(" javascript: ");
                sb.Append(" if(!confirm('Are you sure you want to delete the selected pass \\n This cannot be undone.')) return false;");
                lbnDelete.Attributes.Add("onClick", sb.ToString());

                //set the data for each field
                lblUserSubscriptionId.Text = ((UserSubscription)e.Item.DataItem).UserSubscriptionId.ToString();
                lblPassName.Text = GetPromotionName(((UserSubscription)e.Item.DataItem).SubscriptionId);
                lblPrice.Text = ((UserSubscription)e.Item.DataItem).Price.ToString();
                lblSubscrptIdent.Text = ((UserSubscription)e.Item.DataItem).SubscriptionIdentifier.ToString();
                lblMonthlyAllowance.Text = ((UserSubscription)e.Item.DataItem).MontlyAllowance.ToString();
                lblEndDate.Text = ((UserSubscription)e.Item.DataItem).EndDate.ToString();
            }
        }

        protected void lbn_AddPass_Click(object sender, EventArgs e)
        {
            ddl_PassPromotions.Enabled = true;
            ddl_Subscriptions.Enabled = true;
            tr_EditPass.Visible = true;
        }

        protected void lbn_EditPass_Click(object sender, EventArgs e)
        {
            try
            {
                //first get a reference to the repeater item the drop down is in
                RepeaterItem item = (RepeaterItem)((LinkButton)sender).Parent;

                //next get the usersubscription ID to pull object from datalist
                Label lblUserSubscriptionId = (Label)item.FindControl("lbl_userSubscriptionId");
                USER_SUBSCRIPTIONID = Convert.ToUInt32(lblUserSubscriptionId.Text);

                UserSubscription userSubscription = USER_SUBSCRIPTIONS.Find(GetUserSubscription);

                LoadUserSubscriptionData(userSubscription);

            }
            catch (Exception)
            {
            }
        }

        protected void lbn_DeletePass_Click(object sender, System.EventArgs e)
        {
            try
            {
                //first get a reference to the repeater item the drop down is in
                RepeaterItem item = (RepeaterItem)((LinkButton)sender).Parent;

                //next get the usersubscription ID to pull object from datalist
                Label lblUserSubscriptionId = (Label)item.FindControl("lbl_userSubscriptionId");
                USER_SUBSCRIPTIONID = Convert.ToUInt32(lblUserSubscriptionId.Text);

                //get the user subscription in question
                UserSubscription userSubscription = USER_SUBSCRIPTIONS.Find(GetUserSubscription);

                //delete the subscription
                //GetSubscriptionFacade().
            }
            catch (Exception)
            {
            }

        }

        protected void dgrdPasses_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            BindData(e.NewPageIndex, "");
        }


        protected void UserPass_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            BindData(pgTop.CurrentPageNumber, "");
        }

        #endregion


        #region Attributes

        protected List<UserSubscription> USER_SUBSCRIPTIONS
        {
            get
            {

                if (ViewState["userSubscription"] == null)
                {
                    ViewState["userSubscription"] = new List<UserSubscription>();
                }

                return (List<UserSubscription>)ViewState["userSubscription"];
            }
            set
            {
                ViewState["userSubscription"] = value;
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "created_date";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "DESC";
            }
        }

        private int SELECTED_USER_ID
        {
            get
            {
                return (int)ViewState["users_id"];
            }
            set
            {
                ViewState["users_id"] = value;
            }

        }

        private string SELECTED_USER_NAME
        {
            get
            {
                if (ViewState["users_name"] == null)
                {
                    ViewState["users_name"] = UsersUtility.GetUserNameFromId(SELECTED_USER_ID);
                }

                return ViewState["users_name"].ToString();
            }
            set
            {
                ViewState["users_name"] = value;
            }

        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.rptPasses.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptPasses_ItemDataBound);
        }

        #endregion
    }
}