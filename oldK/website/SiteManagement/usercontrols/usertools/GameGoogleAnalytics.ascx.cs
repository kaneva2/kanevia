﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class GameGoogleAnalytics : BaseUserControl
    {
        #region Declarations

        private const int MAX_TEXT_LENGTH = 26;

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private bool isAdministrator = false;
        private bool canEdit = false;
        private int ownerId = 0;

        #endregion Declarations

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                divError.Style.Add("display", "none");

                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.STAR_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        isAdministrator = true;
                        canEdit = true;
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    default:
                        break;
                }

                if ((Session["SelectedGameId"] != null) && Convert.ToInt32(Session["SelectedGameId"]) > 0)
                {
                    // Load the current user's data the first time
                    this.SelectedGameId = Convert.ToInt32(Session["SelectedGameId"]);

                    //get the company id the game is associated with
                    this.ownerId = GetGameFacade().GetGameOwner(this.SelectedGameId);

                    if (!isAdministrator)
                    {
                        canEdit = this.ownerId.Equals(GetUserId());
                    }

                    if (!canEdit)
                    {
                        RedirectToHomePage();
                    }
                    else
                    {
                        if (!IsPostBack || (Request["communityId"] != null))
                        {
                            // Log the CSR activity
                            this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Viewing game google analytics account info", 0, this.SelectedGameId);

                            //configure page
                            ConfigurePageForUse();
                        }
                    }
                }
            }
            else
            {
                RedirectToHomePage();
            }
        }

        #endregion Page Load


        #region Functions

        private void ConfigurePageForUse()
        {
            txtAccountID.MaxLength = MAX_TEXT_LENGTH;

            Game gameData = GetGameFacade().GetGameByGameId(this.SelectedGameId);

            if (gameData.GAAccountID.Length > 0)
            {
                txtAccountID.Text = gameData.GAAccountID;
            }
        }

        private void ShowMessage(string msg, bool isError)
        {
            divError.InnerText = msg;
            divError.Attributes.Add("class", isError ? "error" : "confirm");
            divError.Style.Add("display", "block");
        }

        #endregion Functions


        #region Event Handlers

        protected void btnSave_Click(Object sender, EventArgs e)
        {
            //set error message variable
            string error = "";

            //validate the required fields
            if (txtAccountID.Text.Trim ().Length  > 0 && txtAccountID.Text.Trim ().Length > MAX_TEXT_LENGTH )
            {
                error += "Google Account ID can only be " + MAX_TEXT_LENGTH + " characters long. ";
            }

            // Check for any inject scripts in large text fields
            if (SiteManagementCommonFunctions.ContainsInjectScripts(txtAccountID.Text))
            {
                error += "Your input contains invalid scripting, please remove script code and try again. ";
                m_logger.Warn ("User " + GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
            }

            if (error.Length > 0)
            {
                ShowMessage(error, true);
            }
            else
            {
                // Save the changes
                GetGameFacade().UpdateGameGAAccountID(this.SelectedGameId, txtAccountID.Text.Trim());

                //indicated a successful save
                ShowMessage("Your changes were saved successfully.", false);
            }
        }

        #endregion Event Handlers

        #region Properties

        private int SelectedGameId
        {
            get
            {
                return (int)ViewState["game_id"];
            }
            set
            {
                ViewState["game_id"] = value;
            }
        }

        #endregion Properties
    }
}