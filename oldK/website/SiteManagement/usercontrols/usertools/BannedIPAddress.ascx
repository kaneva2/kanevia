<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BannedIPAddress.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.BannedIPAddress" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<script type="text/javascript" language="javascript" src="../../jscript/prototype.js"></script>

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">

<table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="990" style="background-color:#ffffff">
	<tr>
        <td align="center">
			<span style="height:30px; font-size:28px; font-weight:bold">Banned IP Addresses</span><br /><br />
			<ajax:ajaxpanel id="ajpError" runat="server">
			<asp:label runat="server" id="lblErrMessage" class="errBox black" style="width:60%;margin-top:20px;text-align:center;"></asp:label></ajax:ajaxpanel>
		</td>
    </tr>
	<tr>
		<td style="height:20px"></td>
    </tr>
    <tr>
         <td align="center">

		 
			<ajax:ajaxpanel id="ajpBannedIPs" runat="server">  

                Add IP Ban: <asp:TextBox ID="txtAddBan" runat="server"></asp:TextBox>

                Suspension length: 
					<asp:DropDownList ID="drpBanLength" runat="server">
						<asp:ListItem Value="0">select</asp:ListItem>
						<asp:ListItem Value="1">1</asp:ListItem>
						<asp:ListItem Value="2">2</asp:ListItem>
						<asp:ListItem Value="3">3</asp:ListItem>
						<asp:ListItem Value="4">4</asp:ListItem>
						<asp:ListItem Value="5">5</asp:ListItem>
						<asp:ListItem Value="6">6</asp:ListItem>
						<asp:ListItem Value="7">7</asp:ListItem>
						<asp:ListItem Value="-1">Permanent</asp:ListItem>
					</asp:DropDownList> Days

                <asp:Button id="btn_Add" onclick="btnAddIPBan_Click" Text="Add Ban" runat="server"></asp:Button> 
                <br />
                <br />
		 
				<asp:repeater id="rptBannedIPs" runat="server" onitemcommand="rptBannedIPs_ItemCommand">
				
					<headertemplate>
						<div style="border-color:DarkGray;border-width:1px;border-style:Solid;font-size:12px;width:800px;">
						<table cellpadding="4" cellspacing="0" border="0" width="100%">
							<tr style="color:Black;background-color:LightGrey;font-weight:bold;">
								<th align="left">IP Address</th>
								<th align="left">Ban Start</th>
								<th align="left">Ban End</th>
								<th></th>
							</tr>
					</headertemplate>
					
					<itemtemplate>
							<tr>
								<td><%# DataBinder.Eval(Container.DataItem, "ip_address") %></td>
								<td><%# DataBinder.Eval(Container.DataItem, "ban_start_date") %></td>
								<td><%# DataBinder.Eval(Container.DataItem, "ban_end_date") %></td>
								<td>
									<asp:LinkButton id="lbUnBanIP" runat="server" commandname="cmdUnBan" causesvalidation="false" OnClientClick="return confirm('Are you sure you want to un-ban this IP address?');">Un-Ban</asp:LinkButton>
									<input type="hidden" runat="server" id="hidIPAddress" value='<%# DataBinder.Eval(Container.DataItem, "ip_address") %>' >
								</td>			
							</tr>
					</itemtemplate>
					
					<alternatingitemtemplate>
							<tr style="background-color:#DCDCDC;">
								<td><%# DataBinder.Eval(Container.DataItem, "ip_address") %></td>
								<td><%# DataBinder.Eval(Container.DataItem, "ban_start_date") %></td>
								<td><%# DataBinder.Eval(Container.DataItem, "ban_end_date") %></td>
								<td>
									<asp:LinkButton id="lbUnBanIP" runat="server" commandname="cmdUnBan" causesvalidation="false" OnClientClick="return confirm('Are you sure you want to un-ban this IP address?');">Un-Ban</asp:LinkButton>
									<input type="hidden" runat="server" id="hidIPAddress" value='<%# DataBinder.Eval(Container.DataItem, "ip_address") %>' >
								</td>			
							</tr>
					</alternatingitemtemplate>
					
					<footertemplate>
						</table></div>
					</footertemplate>
				
				</asp:repeater>
			
				<div style="width:95%;text-align:right;margin:10px 6px 0 0;"><kaneva:pager runat="server" isajaxmode="True" onpagechanged="pgBannedIPs_PageChange" id="pgBannedIPs" maxpagestodisplay="5" shownextprevlabels="true" /></div>
				
			</ajax:ajaxpanel> 
         
			<asp:Label id="lblNoBannedIPs" visible="false" runat="server">
				<span style="font-size:16px;color:Red;font-weight:bold;margin:30px 0;width:95%;text-align:center;">No Banned IP Addresses found.</span>
			</asp:Label>

			<br /><br />		 		 			
		</td>
	</tr>
</table>

</asp:Panel>