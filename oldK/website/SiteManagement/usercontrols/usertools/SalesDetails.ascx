<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SalesDetails.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.SalesDetails" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<script type="text/javascript" src="jscript/yahoo/yahoo-min.js"></script>  
<script type="text/javascript" src="jscript/yahoo/event-min.js" ></script>   
<script type="text/javascript" src="jscript/yahoo/dom-min.js" ></script>   
<script type="text/javascript" src="jscript/yahoo/calendar-min.js"></script>

<script type="text/javascript" language="javascript" src="jscript/prototype.js"></script>

<link href="css/yahoo/calendar.css" type="text/css" rel="stylesheet" />   


<style type="text/css">
	#cal1Container { display:none; position:absolute; z-index:100;}
	#cal2Container { display:none; position:absolute; z-index:100;}
	.salesDetails{padding:4px; font-size:12px;line-height:16px;color:#000000;border:solid 1px #000000; width:400px; position:absolute; z-index:500; background-color:#eeeeee;top:200px; left:290px;}
	.descript{text-align:right; line-height:16px; font-weight:bold; color:#000000; width:50px;}
	.data{text-align:left; line-height:16px; color:#000000; padding:4px 4px 4px 10px;}
	.closeButton{background-color:#ffffff; float:right; font-weight:bold; border:solid 1px #000000; padding:4px; text-decoration:none;}
	h2{font-size:16px;line-height:16px;color:#ffffff;padding:0px 10px 10px 10px;}
</style>
 
 
<script type="text/javascript"><!--
 function setElementVisibility(clientId,value)
 {
    var element = $(clientId);
    element.style.display = value;
 }

 var txtStartDate_ID = '<%= txtStartDate.ClientID %>';
 var txtEndDate_ID = '<%= txtEndDate.ClientID %>';
  
YAHOO.namespace("example.calendar");

    
	function handleStartDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate1 = $(txtStartDate_ID);
		txtDate1.value = month + "-" + day + "-" + year;
		YAHOO.example.calendar.cal1.hide();
	}
	
	function initStartDate() {
		// Admin Calendar
		YAHOO.example.calendar.cal1 = new YAHOO.widget.Calendar ("cal1", "cal1Container", { iframe:true, zIndex:200, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal1.render();
		
		// Listener to show Admin Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgItemStartDate", "click", YAHOO.example.calendar.cal1.show, YAHOO.example.calendar.cal1, true);   
		YAHOO.example.calendar.cal1.selectEvent.subscribe(handleStartDate, YAHOO.example.calendar.cal1, true);
	}
	
		function handleEndDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate2 = $(txtEndDate_ID);
		txtDate2.value = month + "-" + day + "-" + year;
		YAHOO.example.calendar.cal2.hide();
	}
	
	function initEndDate() {
		// Admin Calendar
		YAHOO.example.calendar.cal2 = new YAHOO.widget.Calendar ("cal2", "cal2Container", { iframe:true, zIndex:200, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal2.render();
		
		// Listener to show Admin Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgItemEndDate", "click", YAHOO.example.calendar.cal1.show, YAHOO.example.calendar.cal2, true);   
		YAHOO.example.calendar.cal2.selectEvent.subscribe(handleEndDate, YAHOO.example.calendar.cal2, true);
	}
//--> 
</script> 

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    <ajax:ajaxpanel id="ajMessage" runat="server">

    <!-- DISPLAYS the orrder details -->
    <div id="salesDetails" runat="server" visible="false" class="salesDetails">
        <table cellpadding="0" cellspacing="0" border="0" width="400px">
            <tr>
	            <td colspan="2" style="background-color: #000066">
		            <span><h2>Order Details</h2></span>
	            </td>
            </tr>
            <tr>
	            <td class="descript" valign="top">order&nbsp;id:</td>
	            <td class="data"  valign="top">
		            <b><asp:Label style="font-size: 12px;" id="lblOrderId" runat="server"/></b>
	            </td>
            </tr>
            <tr >
	            <td class="descript" valign="top">description:</td>
	            <td class="data"  valign="top">
		            <asp:Label id="lblDescription" runat="server"/>
	            </td>
            </tr>
            <tr>
	            <td class="descript" nowrap="true" valign="top">amount:</td>
	            <td class="data"  valign="top">
		            <asp:Label id="lblAmount" runat="server"/>
	            </td>
            </tr>
            <tr>
	            <td class="descript" nowrap="true"valign="top">k-Points received:</td>
	            <td class="data"  valign="top">
		            <asp:Label id="lblKPoints" runat="server"/>
	            </td>
            </tr>
            <tr>
	            <td class="descript" nowrap="true" valign="top">payment&nbsp;method:</td>
	            <td class="data"  valign="top">
		            <asp:Label id="lblPaymentMethod" runat="server"/>
	            </td>
            </tr>
            <tr>
	            <td class="descript" valign="top">status:</td>
	            <td class="data"  valign="top">
		            <asp:Label id="lblStatus" runat="server"/>
	            </td>
            </tr>
            <tr>
	            <td class="descript" nowrap="true" valign="top">purchase&nbsp;date:</td>
	            <td class="data"  valign="top">
		            <asp:Label id="lblPurchaseDate" runat="server"/>
	            </td>
            </tr>
            <tr>
	            <td class="descript" valign="top">username:</td>
	            <td class="data"  valign="top">
		            <A runat="server" id="aUsername" style="color: #4863a2; font-size: 11px;"></a>
	            </td>
            </tr>
            <tr>
	            <td class="descript" nowrap="true" valign="top">address:</td>
	            <td class="data"  valign="top">
		            <asp:Label id="lblAddress" runat="server"/>
	            </td>
            </tr>
            <tr>
	            <td class="descript" valign="top">billing info:</td>
	            <td class="data"  valign="top">
		            <asp:Label id="lblCreditInfo" runat="server"/>
	            </td>
            </tr>
            <tr>
	            <td class="descript" valign="top">Error:</td>
	            <td class="data"  valign="top">
		            <asp:Label id="lblErrorDescription" runat="server"/>
	            </td>
            </tr>
            <tr>
	            <td class="descript"></td>
	            <td class="data" align="right">
		            <asp:linkbutton Visible="true" CssClass="closeButton" Text="Close Details" ID="transActionDetails" runat="server" CommandName="Close" OnCommand="ShowDetails_Command" ></asp:linkbutton>
	            </td>
            </tr>
        </table>
    </div>
    
    
    <!-- displays sales orders -->
    <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="980">
        <tr>
            <td style="padding:20px 0px 20px 40px; text-align:center; font-size:22px">
                SALES DETAILS
            </td>
        </tr>
        <tr>
            <td style="padding-left:40px;" align="left">
                <table border="0" width="938px" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width:125px">
                            <span style="font-size:9px">Start Date</span>
                        </td>
                        <td style="width:125px">
                            <span style="font-size:9px">End Date</span>
                        </td>
                        <td style="width:125px">
                            <span style="font-size:9px">Username</span>
                        </td>                            
                        <td style="width:125px">
                            <span style="font-size:9px">Order Id</span>
                        </td>     
                        <td align="center">
                        
                        </td>
                        <td align="center">
                        </td>                       
                   </tr>
                    <tr>
                       <td style="width:125px">
                            <div id="cal1Container"></div><asp:TextBox runat="server" id="txtStartDate" maxlength="10" width="80"></asp:TextBox>
                            <img id="imgItemStartDate" name="imgItemStartDate" onload="initStartDate();" src="images/cal_16.gif"/>
                        </td>
                        <td style="width:125px">
                            <div id="cal2Container"></div><asp:TextBox runat="server" id="txtEndDate" maxlength="10" width="80"></asp:TextBox>
                            <img id="imgItemEndDate" name="imgItemEndDate" onload="initEndDate();" src="images/cal_16.gif"/>
                        </td>
                        <td>
                        <asp:TextBox runat="server" id="txtUserName" maxlength="50" width="80"></asp:TextBox>
                        </td>
                        <td>
                        KEN-KP-<asp:TextBox runat="server" id="txtOrderId" maxlength="50" width="80"></asp:TextBox>
                        </td>
                        <td style="width:50px" align="center">
                            <asp:Button id="btnSearch" onclick="btnSearch_Click" causesvalidation="false" runat="server" text="Search"></asp:Button>
                        </td>
                        <td>
                            <asp:label style="font-size:18px; padding: 0px 0 0px 20px" runat="server" id="messages" visible="true" />
                        </td>                            
                   </tr>

                    <tr>
                        <td>
                            <span style="font-size:9px">(MM-dd-yyyy) 12:00:00 AM *</span>
                        </td>
                        <td>
                            <span style="font-size:9px">(MM-dd-yyyy) 11:59:59 PM </span>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                                                <td>
                        </td>
                        <td>
                        </td>
                   </tr>
                </table>    

                <hr style="width:100%" />
            </td>
        </tr>
        <tr>
            <td style="padding-left:40px">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
	                <tr style="line-height:15px;">
	                    <td><asp:Label class="belowFilter2" id="lblSales" runat="server" Text="Click Search to start..."/></td>
		                <td align="right" class="belowFilter2" valign="middle">
		                    <asp:Label runat="server" id="pageCount" CssClass="dateStamp"/><br />
			                <Kaneva:Pager runat="server" onpagechanged="pgTop_PageChange" id="pgTop"/><br>
		                </td>						
	                </tr>
                </table>
             </td>
         </tr>
         <tr>
            <td style="padding-left:40px">
               <asp:Repeater ID="rptOrders" runat="server" >
                    <HeaderTemplate>
                            <table border="1" frame="border" cellpadding="4" cellspacing="0" width="100%">
                                <tr style="background-color:#000066; color:#ffffff"> 
                                    <th align="center" style="width:70px">User</th>
                                    <th align="left" style="width:220px" >Order Id</th>
                                    <th align="center" style="width:80px">Amount ($)</th>
                                    <th align="left" style="width:80px">Kpoints</th>
                                    <th align="left" style="width:140px">Payment Method</th>
                                    <th align="center" style="width:80px">Status Description</th>
                                    <th align="left" style="width:130px">Purchase Date</th>
	                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                                <tr>
                                    <td align="center">
                                        <div class="framesize-xsmall">
					                        <div class="restricted" visible="false" runat="server" id="divOwnerRestricted"></div>
					                        <div class="frame">
						                        <span class="ct"><span class="cl"></span></span>
						                        <div class="imgconstrain">
							                        <asp:HyperLink id="userName2" style="color:Blue" runat="server" >
							                            <img id="userPict" runat="server" border="0"/>
							                        </asp:HyperLink>	
						                        </div>
						                        <span class="cb"><span class="cl"></span></span>
					                        </div>
				                        </div>
				                        <asp:HyperLink id="userName" style="color:Blue; padding-top:10px" runat="server" />
				                        <asp:label id="userId" runat="server" visible="false" />
                                    </td>
                                     <td>
                                         <asp:linkbutton ID="transActionDetails" runat="server" CommandName="open" OnCommand="ShowDetails_Command">
                                            KEN-KP-<asp:label id="orderId" runat="server" visible="true" />
                                         </asp:linkbutton><br />
				                        <asp:label id="orderDesc" runat="server" visible="true" />
                                        <asp:label id="transactionId" runat="server" visible="false" /> 
                                    </td>
                                    <td align="center">
				                        <asp:label id="dollarAmount" runat="server" visible="true" />
                                    </td>
                                    <td>
				                        <asp:label id="kpointAmount" runat="server" visible="true" />
                                    </td>
                                    <td>
				                        <asp:label id="paymentMethod" runat="server" visible="true" />&nbsp;&nbsp;
                                        <asp:button id="btnRefund" runat="server" causesvalidation="false" Text="Refund" ToolTip="Mark Payment as Refunded" onclick="btnRefund_OnClick" /> 
                                      </td>
                                     <td align="center">
				                        <asp:label id="status" runat="server" visible="true" />
                                     </td>
                                      <td>
				                        <asp:label id="purchaseDate" runat="server" visible="true" />
                                    </td>
                              </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                                <tr style="background-color:#eeeeee">
                                    <td align="center">
                                        <div class="framesize-xsmall">
					                        <div class="restricted" visible="false" runat="server" id="divOwnerRestricted"></div>
					                        <div class="frame">
						                        <span class="ct"><span class="cl"></span></span>
						                        <div class="imgconstrain">
							                        <asp:HyperLink id="userName2" style="color:Blue" runat="server" >
							                            <img id="userPict" runat="server" border="0"/>
							                        </asp:HyperLink>	
						                        </div>
						                        <span class="cb"><span class="cl"></span></span>
					                        </div>
				                        </div>
				                        <asp:HyperLink id="userName" style="color:Blue; padding-top:10px" runat="server" />
				                        <asp:label id="userId" runat="server" visible="false" />
                                    </td>
                                     <td>
                                         <asp:linkbutton ID="transActionDetails" runat="server" CommandName="open" OnCommand="ShowDetails_Command">
                                            KEN-KP-<asp:label id="orderId" runat="server" visible="true" />
                                         </asp:linkbutton><br />
				                        <asp:label id="orderDesc" runat="server" visible="true" />
                                        <asp:label id="transactionId" runat="server" visible="false" /> 
                                    </td>
                                    <td align="center">
				                        <asp:label id="dollarAmount" runat="server" visible="true" />
                                    </td>
                                    <td>
				                        <asp:label id="kpointAmount" runat="server" visible="true" />
                                    </td>
                                    <td>
				                        <asp:label id="paymentMethod" runat="server" visible="true" />&nbsp;&nbsp;
                                        <asp:button id="btnRefund" runat="server" causesvalidation="false" Text="Refund" ToolTip="Mark Payment as Refunded" onclick="btnRefund_OnClick" /> 
                                      </td>
                                     <td align="center">
				                        <asp:label id="status" runat="server" visible="true" />
                                     </td>
                                      <td>
				                        <asp:label id="purchaseDate" runat="server" visible="true" />
                                    </td>
                              </tr>
                    </AlternatingItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
         </tr>
    </table>
    </ajax:ajaxpanel>
    
</asp:Panel>