<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GameUserRetention.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.GameUserRetention" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<link href="../../css/base/base.css" type="text/css" rel="stylesheet">
<link href="../../css/Communities/GameUserRetention.css" type="text/css" rel="stylesheet">

<div id="vcContainer">

    <div id="divVCTitle" class="title">User Retention</div>
    <div id="divErrorTop" runat="server" class="error" style="display:none;"></div>
    
    <div id="vcData">
        <div id="data">
            <asp:repeater id="rptUserRetention" runat="server">
                <HeaderTemplate>
                    <div class="header">
                        <div class="dates">Date Range</div>
                        <div class="one_day_retention">1</div>
                        <div class="seven_day_retention">7</div>
                        <div class="thirty_day_retention">30</div>
				    </div>
                </HeaderTemplate>
                <itemtemplate>
                    <div class="row">
                        <div class="retentionInfo">
						    <div class="dates">0</div>
                            <div class="one_day_retention">0</div>
                            <div class="seven_day_retention">0</div>
					        <div class="thirty_day_retention">0</div>
                        </div>
                    </div>
                </itemtemplate>
            </asp:repeater>
        </div>

        <div class="results"><asp:Label runat="server" id="lblSearch"/></div>
        <div class="pager"><Kaneva:Pager runat="server" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true"/></div>
    
        <a name="details" href="javascript:void(0);"></a>
		<a id="aScroll" href="#details" style="visibility:hidden;"></a>
    </div>
</div>
