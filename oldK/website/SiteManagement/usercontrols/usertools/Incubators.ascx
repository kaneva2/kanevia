<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Incubators.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.Incubators" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="css/kaneva/buttons.css" type="text/css" rel="stylesheet" />
<link href="css/kaneva/Incubators.css" type="text/css" rel="stylesheet" />

<div id="incubatorContainer">
	<div id="incubatorData">		
		<!-- Incubator List -->
		<ajax:ajaxpanel id="ajpIncubators" runat="server">	
			<asp:gridview id="dg_Incubators" runat="server" 
				onSorting="dg_Incubators_Sorting" 
				OnPageIndexChanging="dg_Incubators_PageIndexChanging" 
                OnRowCommand="FireRowCommand"
				AllowSorting="False" borderstyle="None" borderwidth="0" cellspacing="0" cellpadding="0" PageSize="10" 
				AllowPaging="True" autogeneratecolumns="False" width="100%" cssclass="" gridlines="none">
							
				<RowStyle cssclass="row"></RowStyle>
				<HeaderStyle cssclass="header"></HeaderStyle>
				
				<Columns>
				   <asp:BoundField HeaderText="Incubator" DataField="name" ReadOnly="True" ConvertEmptyStringToNull="False" ItemStyle-Width="624px"></asp:BoundField>
				   <asp:TemplateField ItemStyle-Width="227px">
					   <ItemTemplate>
						  <asp:LinkButton id="lbn_showAppHosts" causesvalidation="false" Runat="server" CommandName="ShowAppHosts" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "name")%>'>View App Controllers</asp:LinkButton>
					   </ItemTemplate>
				   </asp:TemplateField>
                  <%-- <asp:CommandField CausesValidation="False" ShowEditButton="True" EditText="Configure"></asp:CommandField>--%>        
				</Columns>
			</asp:gridview>
            <br />
            <!-- App Controller List -->
            <div id="appControllerContainer" runat="server" visible="false">
                <span id="spnSelectedIncubator" runat="server"></span>
                <asp:gridview id="dg_AppControllers" runat="server" 
				    onSorting="dg_AppControllers_Sorting" 
				    OnPageIndexChanging="dg_AppControllers_PageIndexChanging" 
                    OnRowCommand="FireRowCommand"
				    AllowSorting="False" borderstyle="None" borderwidth="0" cellspacing="0" cellpadding="0" PageSize="10" 
				    AllowPaging="True" autogeneratecolumns="False" width="100%" cssclass="" gridlines="none">
							
				    <RowStyle cssclass="row"></RowStyle>
				    <HeaderStyle cssclass="header"></HeaderStyle>
				
				    <Columns>
                       <asp:BoundField HeaderText="Host" DataField="name" ReadOnly="True" ConvertEmptyStringToNull="False" ItemStyle-Width="200px"></asp:BoundField>
				       <asp:BoundField HeaderText="Slots" DataField="slots" ReadOnly="True" ConvertEmptyStringToNull="False" ItemStyle-Width="60px"></asp:BoundField>
				       <asp:BoundField HeaderText="Available" DataField="available" ReadOnly="True" ConvertEmptyStringToNull="False" ItemStyle-Width="176px"></asp:BoundField>
                       <asp:BoundField HeaderText="Last Ping" DataField="lastPing" ReadOnly="True" ConvertEmptyStringToNull="False" ItemStyle-Width="275px"></asp:BoundField>
				       <asp:TemplateField>
					       <ItemTemplate>
						      <asp:LinkButton id="lbn_showApps" causesvalidation="false" Runat="server" CommandName="ShowApps" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "name")%>'>View App Servers</asp:LinkButton>
					       </ItemTemplate>
				       </asp:TemplateField>
                       <%--<asp:CommandField CausesValidation="False" ShowEditButton="True" EditText="Configure"></asp:CommandField>        --%>
				    </Columns>
			    </asp:gridview>
            </div>
            <br />
            <!-- App Server List -->
            <div id="appServerContainer" runat="server" visible="false">
                <span id="spnSelectedAppController" runat="server"></span>
                <asp:gridview id="dg_AppServers" runat="server" 
				    onSorting="dg_AppServers_Sorting" 
				    OnPageIndexChanging="dg_AppServers_PageIndexChanging" 
                    OnRowCommand="FireRowCommand"
				    AllowSorting="False" borderstyle="None" borderwidth="0" cellspacing="0" cellpadding="0" PageSize="10" 
				    AllowPaging="True" autogeneratecolumns="False" width="100%" cssclass="" gridlines="none">
							
				    <RowStyle cssclass="row"></RowStyle>
				    <HeaderStyle cssclass="header"></HeaderStyle>
				
				    <Columns>
                        <asp:BoundField HeaderText="AppID" DataField="appID" ReadOnly="True" ConvertEmptyStringToNull="False" ItemStyle-Width="200px"></asp:BoundField>
                        <asp:BoundField HeaderText="PID" DataField="pid" ReadOnly="True" ConvertEmptyStringToNull="False" ItemStyle-Width="60px"></asp:BoundField>
                        <asp:BoundField HeaderText="TID" DataField="tid" ReadOnly="True" ConvertEmptyStringToNull="False" ItemStyle-Width="455px"></asp:BoundField>
                        <asp:TemplateField>
					        <ItemTemplate>
						        <asp:LinkButton id="lbn_showAppDetails" causesvalidation="false" Runat="server" CommandName="ShowAppDetail" 
                                CommandArgument='<%#DataBinder.Eval(Container.DataItem, "appID")%>' >
                                    View Details
                                </asp:LinkButton>
					        </ItemTemplate>
				        </asp:TemplateField>    
				    </Columns>
			    </asp:gridview>
            </div>
            <br />
            <!-- App Detail -->
            <div id="appDetailContainer" runat="server" visible="false">
                <span id="spnSelectedAppServer" runat="server"></span>
                <table id="tblAppData" class="AppData" runat="server" border="0" cellspacing="0" cellpadding="0" style="border-width:0px;border-style:None;width:100%;border-collapse:collapse;">
                    <tbody>
                        <tr class="header">
                            <th>AppID:</th>
                            <th id="thAppID" colspan="3" runat="server"></th>
                        </tr>
                        <tr class="row">
                            <td>TemplateId:</td>
                            <td id="tdAppSlot" runat="server"></td>
                        </tr>
                        <tr class="row">
                            <td>App Directory:</td>
                            <td id="tdAppDirectory" runat="server"></td>
                        </tr>
                        <tr class="row">
                            <td>Patch Directory:</td>
                            <td id="tdPatchDirectory" runat="server"></td>
                        </tr>
                        <tr class="row">
                            <td>Database Host:</td>
                            <td id="tdAppDBHost" runat="server"></td>
                        </tr>
                        <tr class="row">
                            <td>Database Name:</td>
                            <td id="tdAppDBName" runat="server"></td>
                        </tr>
                        <tr class="row">
                            <td>Port:</td>
                            <td id="tdAppPort" runat="server"></td>
                        </tr>
                        <tr class="row">
                            <td>Bound:</td>
                            <td id="tdAppBound" runat="server"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
		</ajax:ajaxpanel>
	</div>
</div>