using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;
using Kaneva.BusinessLayer.Facade;

using KlausEnt.KEP.Kaneva;
using KlausEnt.KEP.Kaneva.framework.utils;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class GameViralCoefficients : BaseUserControl
    {
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private bool isAdministrator = false;
        private bool canEdit = false;
        private int ownerId = 0;
        private int pageSize = 10;
        private int pageNum = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.STAR_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        isAdministrator = true;
                        canEdit = true;
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    default:
                        break;
                }

                if ((Session["SelectedGameId"] != null) && Convert.ToInt32(Session["SelectedGameId"]) > 0)
                {
                    // Load the current user's data the first time
                    SELECTED_GAME_ID = Convert.ToInt32(Session["SelectedGameId"]);

                    //get the company id the game is associated with
                    this.ownerId = GetGameFacade().GetGameOwner(SELECTED_GAME_ID);

                    if (!isAdministrator)
                    {
                        canEdit = this.ownerId.Equals(GetUserId());
                    }

                    if (!canEdit)
                    {
                        RedirectToHomePage();
                    }
                    else
                    {
                        if (!IsPostBack || !IsPostBackKaneva)
                        {
                            BindExperiments();

                            IsPostBackKaneva = true;
                        }
                    }
                }
            }
        }

        private void BindExperiments()
        {
            try
            {
                DateTime endDate = DateTime.Now;
                DateTime startDate = DateTime.Now.AddDays((-(7 * 8) - (int)endDate.DayOfWeek)); // include current week and two months worth of data

                DataTable data = ReportUtility.GetViralCoefficientData(SELECTED_GAME_ID, startDate, endDate);
                data.Columns.Add("accepted_invites",typeof(Double));
                data.Columns.Add("viral_coefficient",typeof(Double));

                if (data.Rows.Count > 0)
                {
                    Hashtable googleData = ExternalURLHelper.GoogleAnalyticsReportInfo(SELECTED_GAME_ID, startDate, endDate, m_logger);

                    if (googleData != null && (googleData.Count > 0) )
                    {
                        AddGoogleDataToViralData(data, googleData);

                        rptViralCoefficients.DataSource = data;
                        rptViralCoefficients.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
                m_logger.Error("Error loading viral data.", ex);
            }
        }

        private void AddGoogleDataToViralData(DataTable data, Hashtable googleData)
        {
            foreach (DataRow row in data.Rows)
            {
                string weekId = row["week_id"].ToString();
                if (googleData.ContainsKey(weekId.Substring(weekId.Length - 2, 2)))
                {
                    int googleWeekData = (int)googleData[weekId.Substring(weekId.Length - 2, 2)];
                    if ((long)row["total_sent"] > 0)
                    {
                        row["accepted_invites"] = (Convert.ToDouble(googleWeekData) / Convert.ToDouble(row["total_sent"])) * 100;
                        row["viral_coefficient"] = Convert.ToDouble(row["accepted_invites"]) * Convert.ToDouble(row["avg_invitees_by_inviter"]) * Convert.ToDouble(row["inviters_over_total_visitors"]) / (100 * 100);
                    } 
                }
            }
        }

        private void ShowMessage(string msg)
        {
            divErrorTop.InnerText = msg;
            divErrorTop.Style.Add("display", "block");
        }

        private void GetRequestValues()
        {
            // Get items per page count
            if (Request["pagesize"] != null)
            {
                try
                {
                    PageSize = Convert.ToInt32(Request["pagesize"]);
                }
                catch { }
            }

            // Get the current page number
            if (Request["p"] != null)
            {
                try
                {
                    PageNum = Convert.ToInt32(Request["p"]);
                }
                catch { }
            }
        }

        private int PageNum
        {
            set { this.pageNum = value; }
            get { return this.pageNum; }
        }
        private int PageSize
        {
            set { this.pageSize = value; }
            get { return this.pageSize; }
        }

        private int SELECTED_GAME_ID
        {
            get { return (int)ViewState["game_id"]; }
            set { ViewState["game_id"] = value; }
        }

        private bool IsPostBackKaneva
        {
            get
            {
                if (ViewState["ispostback"] == null)
                {
                    return false;
                }

                return (bool)ViewState["ispostback"];
            }
            set { ViewState["ispostback"] = value; }
        }

    }
}