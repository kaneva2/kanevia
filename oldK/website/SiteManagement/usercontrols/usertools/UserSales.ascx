<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="UserSales.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.UserSales" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="980">
        <tr>
            <td style="padding-left:40px; text-align:center; font-size:22px">
                USER SALES
            </td>
        </tr>
        <tr>
            <td style="padding-left:40px">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
	                <tr style="line-height:15px;">
	                    <td align="left" style="width:300px" class="belowFilter2" valign="middle" >
	                        <asp:Label runat="server" id="lblSelectedUser" />
	                    </td>
		                <td align="right" class="belowFilter2" valign="middle">
			                <asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop" /><br>
		                </td>						
	                </tr>
                </table>
             </td>
         </tr>
         <tr>
            <td style="padding-left:40px">
                <asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" OnSortCommand="UserSales_Sorting" Width="100%" id="dgrdRatings" cellpadding="2" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 1px solid #cccccc; font-family:arial">  
	                <HeaderStyle BackColor="#cccccc" ForeColor="#000000" Font-Underline="false" Font-Bold="true" HorizontalAlign="Left" />
	                <ItemStyle BackColor="#ffffff" HorizontalAlign="Left" />
	                <AlternatingItemStyle BackColor="#eeeeee" HorizontalAlign="Left" />
	                <Columns>				
		                <asp:TemplateColumn ItemStyle-CssClass="assetLink" HeaderText="item name" SortExpression="name" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top" ItemStyle-Width="40%" ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <a target="_blank" title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' CssClass="assetLink" href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 40)%></a>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn HeaderText="# sold" SortExpression="count" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Width="10%" ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%# DataBinder.Eval(Container.DataItem, "count")%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn HeaderText="royalty" SortExpression="royalty_amount" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center">
			                <ItemTemplate>
				                <span class="money"><%#FormatCurrency (ConvertKPointsToDollars (Convert.ToDouble (DataBinder.Eval(Container.DataItem, "royalty_amount"))))%></span>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn HeaderText="current price" SortExpression="amount" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center">
			                <ItemTemplate>
				                <span class="money"><%# FormatKpoints (DataBinder.Eval (Container.DataItem, "amount"), false) %></span> 
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn ItemStyle-CssClass="adminLinks" HeaderText="since" SortExpression="created_date" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Width="10%" ItemStyle-Wrap="false">
			                <ItemTemplate>
				                <%# FormatDateTime (DataBinder.Eval(Container.DataItem, "created_date"))%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
	                </Columns>
                </asp:datagrid>
            </td>
         </tr>
    </table>
</asp:Panel>
