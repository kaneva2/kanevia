using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using log4net;
using MagicAjax;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;
using System.Text;
using Kaneva;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class ManageSiteMenus : BaseUserControl
    {
        #region Declarations

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {

                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.SITE_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                BindAvailableWebsites();
                BindTopMenu();
                BindSubMenu();
            }
            else
            {
                RedirectToHomePage();
            }
        }

        #endregion

        #region Attributes

        protected int SELECTED_SITE_INDEX
        {
            get
            {

                if (ViewState["selectedSiteIndex"] == null)
                {
                    ViewState["selectedSiteIndex"] = 0;
                }

                return (int)ViewState["selectedSiteIndex"];
            }
            set
            {
                ViewState["selectedSiteIndex"] = value;
            }
        }

        protected int SELECTED_MENU_INDEX
        {
            get
            {

                if (ViewState["selectedMenuIndex"] == null)
                {
                    ViewState["selectedMenuIndex"] = 0;
                }

                return (int)ViewState["selectedMenuIndex"];
            }
            set
            {
                ViewState["selectedMenuIndex"] = value;
            }
        }

        protected string ORIGINAL_SUB_PRIVILEGE_ID
        {
            get
            {

                if (ViewState["orginalSubPrivilegeId"] == null)
                {
                    ViewState["orginalSubPrivilegeId"] = "0";
                }

                return ViewState["orginalSubPrivilegeId"].ToString();
            }
            set
            {
                ViewState["orginalSubPrivilegeId"] = value;
            }
        }


        protected string ORIGINAL_USER_CONTROL_ID
        {
            get
            {

                if (ViewState["orginalUserControlId"] == null)
                {
                    ViewState["orginalUserControlId"] = "0";
                }

                return ViewState["orginalUserControlId"].ToString();
            }
            set
            {
                ViewState["orginalUserControlId"] = value;
            }
        }

        protected string ORIGINAL_SUBMENU_NAME
        {
            get
            {

                if (ViewState["orginalSubMenuName"] == null)
                {
                    ViewState["orginalSubMenuName"] = "";
                }

                return ViewState["orginalSubMenuName"].ToString();
            }
            set
            {
                ViewState["orginalSubMenuName"] = value;
            }
        }

        protected string ORIGINAL_PRIVILEGE_ID
        {
            get
            {

                if (ViewState["orginalPrivilegeId"] == null)
                {
                    ViewState["orginalPrivilegeId"] = "0";
                }

                return ViewState["orginalPrivilegeId"].ToString();
            }
            set
            {
                ViewState["orginalPrivilegeId"] = value;
            }
        }

        protected string ORIGINAL_MENU_NAME
        {
            get
            {

                if (ViewState["orginalMenuName"] == null)
                {
                    ViewState["orginalMenuName"] = "";
                }

                return ViewState["orginalMenuName"].ToString();
            }
            set
            {
                ViewState["orginalMenuName"] = value;
            }
        }

        protected string ORIGINAL_CONNECTION_STRING
        {
            get
            {

                if (ViewState["orginalConnectionString"] == null)
                {
                    ViewState["orginalConnectionString"] = "";
                }

                return ViewState["orginalConnectionString"].ToString();
            }
            set
            {
                ViewState["orginalConnectionString"] = value;
            }
        }

        protected string ORIGINAL_SITENAME
        {
            get
            {

                if (ViewState["orginalSiteName"] == null)
                {
                    ViewState["orginalSiteName"] = "";
                }

                return ViewState["orginalSiteName"].ToString();
            }
            set
            {
                ViewState["orginalSiteName"] = value;
            }
        }

        protected string ORIGINAL_SITEURL
        {
            get
            {

                if (ViewState["orginalSiteURL"] == null)
                {
                    ViewState["orginalSiteURL"] = "";
                }

                return ViewState["orginalSiteURL"].ToString();
            }
            set
            {
                ViewState["orginalSiteURL"] = value;
            }
        }

        protected IList<WebSite> AVAILABLE_WEBSITES
        {
            get
            {

                if (ViewState["availableWebsites"] == null)
                {
                    ViewState["availableWebsites"] = GetAvailableWebsites();
                }

                return (IList<WebSite>)ViewState["availableWebsites"];
            }
            set
            {
                ViewState["availableWebsites"] = value;
            }
        }

        protected IList<SM_MenuItem> SELECTED_TOP_NAV
        {
            get
            {

                if (ViewState["selectedTopNav"] == null)
                {
                    ViewState["selectedTopNav"] = GetTopLevelMenu();
                }

                return (IList<SM_MenuItem>)ViewState["selectedTopNav"];
            }
            set
            {
                ViewState["selectedTopNav"] = value;
            }
        }

        protected IList<SM_MenuItem> SELECTED_SUB_NAV
        {
            get
            {
                if (ViewState["selectedSubNav"] == null)
                {
                    ViewState["selectedSubNav"] = GetSubMenu();
                }

                return (IList<SM_MenuItem>)ViewState["selectedSubNav"];
            }
            set
            {
                ViewState["selectedSubNav"] = value;
            }
        }

        protected IList<SitePrivilege> SITE_PRIVILEGES
        {
            get
            {

                if (ViewState["sitePrivileges"] == null)
                {
                    ViewState["sitePrivileges"] = GetSitePrivileges();
                }

                return (IList<SitePrivilege>)ViewState["sitePrivileges"];
            }
            set
            {
                ViewState["sitePrivileges"] = value;
            }
        }

        protected IList<KanevaUserControl> USER_CONTROLS
        {
            get
            {

                if (ViewState["userControls"] == null)
                {
                    ViewState["userControls"] = GetUserControls();
                }

                return (IList<KanevaUserControl>)ViewState["userControls"];
            }
            set
            {
                ViewState["userControls"] = value;
            }
        }


        #endregion


        #region Helper Functions

        private void ClearAvailableWebsites()
        {
            ViewState["availableWebsites"] = null;
        }

        private void ClearSelectedTopNav()
        {
            ViewState["selectedTopNav"] = null;
        }

        private void ClearSelectedSubNav()
        {
            ViewState["selectedSubNav"] = null;
        }

        private IList<KanevaUserControl> GetAvailableUserControls()
        {
            IList<KanevaUserControl> userControls = new List<KanevaUserControl>();

            try
            {
                //get the available privileges
                userControls = GetSiteManagementFacade().GetAllUserControls();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during loading of user controls", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error: Sorry we were unable to retreive the available user controls.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

            return userControls;
        }

        private IList<SitePrivilege> GetSitePrivileges()
        {
            IList<SitePrivilege> sitePrivileges = new List<SitePrivilege>();

            try
            {
                //get the available privileges
                sitePrivileges = WebCache.GetAdminSitePrivileges();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during get site privileges", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error: Sorry we were unable to retreive the available privileges.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

            return sitePrivileges;
        }


        private IList<WebSite> GetAvailableWebsites()
        {
            IList<WebSite> availableWebsites = new List<WebSite>();

            try
            {
                //load data source and convert from IList to data table for easier editing
                availableWebsites = GetSiteManagementFacade().GetAvailableWebsites();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during available website retreival", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error: Sorry we were unable to retreive the available websites.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

            return availableWebsites;
        }

        private IList<SM_MenuItem> GetTopLevelMenu()
        {
            IList<SM_MenuItem> mainNavigation = new List<SM_MenuItem>();

            try
            {
                //pull the selected website id
                int webSiteId = Convert.ToInt32(ddl_SiteSelector.SelectedValue);

                //pull the main menu from the DB
                mainNavigation = this.GetSiteManagementFacade().GetMainMenu(webSiteId);
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during main navigation retreival", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error: Sorry we were unable to retreive the main navigation data.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

            return mainNavigation;
        }

        private IList<SM_MenuItem> GetSubMenu()
        {
            IList<SM_MenuItem> subNavigation = new List<SM_MenuItem>();

            try
            {
                //pull the selected website id
                int mainMenuId = Convert.ToInt32(ddl_MainMenuSelector.SelectedValue);

                //pull the main menu from the DB
                subNavigation = this.GetSiteManagementFacade().GetSubMenu(mainMenuId);
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during sub-navigation retreival", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error: Sorry we were unable to retreive the sub-navigation data.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

            return subNavigation;
        }

        private IList<KanevaUserControl> GetUserControls()
        {
            IList<KanevaUserControl> usercontrol = new List<KanevaUserControl>();

            try
            {
                //pull the main menu from the DB
                usercontrol = this.GetSiteManagementFacade().GetAllUserControls();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during usercontrol retreival", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error: Sorry we were unable to retreive the usercontrol data.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

            return usercontrol;
        }

        private void BindAvailableWebsites()
        {
            //update the repeater
            rpt_AvailableSites.DataSource = AVAILABLE_WEBSITES;
            rpt_AvailableSites.DataBind();

            if (AVAILABLE_WEBSITES.Count > 0)
            {
                //update the pulldown
                ddl_SiteSelector.DataSource = AVAILABLE_WEBSITES;
                ddl_SiteSelector.DataTextField = "WebsiteName";
                ddl_SiteSelector.DataValueField = "WebsiteId";
                ddl_SiteSelector.DataBind();

                //set the selected site
                try
                {
                    ddl_SiteSelector.SelectedIndex = SELECTED_SITE_INDEX;
                }
                catch (IndexOutOfRangeException)
                {
                    ddl_SiteSelector.SelectedIndex = SELECTED_SITE_INDEX = 0;
                }
            }

        }

        private void BindTopMenu()
        {
            //update the repeater
            rpt_TopNavItems.DataSource = SELECTED_TOP_NAV;
            rpt_TopNavItems.DataBind();

            //update the pulldown
            if (SELECTED_TOP_NAV.Count > 0)
            {
                ddl_MainMenuSelector.DataSource = SELECTED_TOP_NAV;
                ddl_MainMenuSelector.DataTextField = "NavigationTitle";
                ddl_MainMenuSelector.DataValueField = "NavigationId";
                ddl_MainMenuSelector.DataBind();
                //set the selected menu
                try
                {
                    ddl_MainMenuSelector.SelectedIndex = SELECTED_MENU_INDEX;
                }
                catch (IndexOutOfRangeException)
                {
                    ddl_MainMenuSelector.SelectedIndex = SELECTED_MENU_INDEX = 0;
                }
            }
        }

        private void BindSubMenu()
        {
            //update the repeater
            rpt_SubMenuItems.DataSource = SELECTED_SUB_NAV;
            rpt_SubMenuItems.DataBind();            
        }


        #endregion

        #region Functions

        /// <summary>
        /// Add a new site to the list
        /// </summary>
        protected void AddSubMenuItem()
        {
            //create new website object
            SM_MenuItem subMenuItem = new SM_MenuItem(-1, "new sub-menu item", 1, 0,0, Convert.ToInt32(ddl_MainMenuSelector.SelectedValue));

            //add the new site
            ((IList<SM_MenuItem>)rpt_SubMenuItems.DataSource).Add(subMenuItem);

            //update list
            rpt_SubMenuItems.DataBind();

            //get the new count and set the new row as active
            int rowId = rpt_SubMenuItems.Items.Count - 1;

            Label lblSubMenuId = (Label)rpt_SubMenuItems.Items[rowId].FindControl("lbl_SubMenuId");
            TextBox tbxSubMenuName = (TextBox)rpt_SubMenuItems.Items[rowId].FindControl("tbx_SubMenuName");
            DropDownList ddlPrivileges = (DropDownList)rpt_SubMenuItems.Items[rowId].FindControl("ddl_Privilege_Selector");
            DropDownList ddlUserControl = (DropDownList)rpt_SubMenuItems.Items[rowId].FindControl("ddl_UserControl");
            LinkButton lbnEditSubMenu = (LinkButton)rpt_SubMenuItems.Items[rowId].FindControl("lnkEditSubMenu");
            LinkButton lbnUpdateSubMenu = (LinkButton)rpt_SubMenuItems.Items[rowId].FindControl("lnkUpdateSubMenu");
            LinkButton lbnCancelSubMenu = (LinkButton)rpt_SubMenuItems.Items[rowId].FindControl("lnkCancelSubMenu");

            //set the original values to check for changes
            ORIGINAL_SUB_PRIVILEGE_ID = ORIGINAL_USER_CONTROL_ID = ORIGINAL_SUBMENU_NAME = "";

            //reconfigure
            tbxSubMenuName.Enabled = true;
            ddlPrivileges.Enabled = true;
            ddlUserControl.Enabled = true;
            lbnEditSubMenu.Visible = false;
            lbnUpdateSubMenu.Visible = true;
            lbnCancelSubMenu.Visible = true;

        }

                /// <summary>
        /// Save the Main Menu Item changes
        /// </summary>
        protected int SaveSubMenuItem(SM_MenuItem subMenuItem)
        {
            int result = 0;

            if (subMenuItem.NavigationId > 0)
            {
                result = GetSiteManagementFacade().UpdateSubMenuItem(subMenuItem);
            }
            else
            {
                result = GetSiteManagementFacade().AddSubMenuItem(subMenuItem);
            }

            return result;
        }


        
        /// <summary>
        /// Delete selected Sub Menu Items
        /// </summary>
        protected void DeleteCheckedSubMenuItems()
        {
            CheckBox chkEdit;
            int deletionCount = 0;
            int result = 0;

            foreach (RepeaterItem rptSubNavItems in rpt_SubMenuItems.Items)
            {
                chkEdit = (CheckBox)rptSubNavItems.FindControl("chkEditSubMenu");

                if (chkEdit.Checked)
                {
                    int subMenuId = Convert.ToInt32(((Label)rptSubNavItems.FindControl("lbl_SubMenuId")).Text);

                    result += this.GetSiteManagementFacade().DeleteSubMenuItem(subMenuId);
                    deletionCount++;
                }
            }

            //message to user
            if (deletionCount == 0)
            {
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "No sub menu items were selected.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
            else if (deletionCount != result)
            {
                //rest the data set to prepare for new data reteival
                ClearSelectedSubNav();
                //bind the data to the repeater and drop down (gets data from database if null)
                BindSubMenu();

                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = result + " of " + deletionCount + " sub-menu item" + (deletionCount > 1 ? "s were " : " was ") + "deleted.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
            else
            {
                //rest the data set to prepare for new data reteival
                ClearSelectedSubNav();
                //bind the data to the repeater and drop down (gets data from database if null)
                BindSubMenu();

                messages.ForeColor = System.Drawing.Color.DarkGreen;
                messages.Text = deletionCount + " sub-menu item" + (deletionCount > 1 ? "s were " : " was ") + "deleted.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }
        
        /// <summary>
        /// Add a new site to the list
        /// </summary>
        protected void AddMainMenuItem()
        {
            //create new website object
            SM_MenuItem menuItem = new SM_MenuItem(-1, "new menu item", 1, Convert.ToInt32(ddl_SiteSelector.SelectedValue), 0, 0);

            //add the new site
            ((IList<SM_MenuItem>)rpt_TopNavItems.DataSource).Add(menuItem);

            //update list
            rpt_TopNavItems.DataBind();

            //get the new count and set the new row as active
            int rowId = rpt_TopNavItems.Items.Count - 1;

            Label lblMenuId = (Label)rpt_TopNavItems.Items[rowId].FindControl("lbl_MenuId");
            TextBox tbxMenuName = (TextBox)rpt_TopNavItems.Items[rowId].FindControl("tbx_MenuName");
            DropDownList ddlPrivileges = (DropDownList)rpt_TopNavItems.Items[rowId].FindControl("ddl_PrivilegeSelector");
            LinkButton lbnEditMenu = (LinkButton)rpt_TopNavItems.Items[rowId].FindControl("lnkEditMenu");
            LinkButton lbnUpdateMenu = (LinkButton)rpt_TopNavItems.Items[rowId].FindControl("lnkUpdateMenu");
            LinkButton lbnCancelMenu = (LinkButton)rpt_TopNavItems.Items[rowId].FindControl("lnkCancelMenu");

            //set the original values to check for changes
            ORIGINAL_PRIVILEGE_ID = ORIGINAL_MENU_NAME = "";

            //reconfigure
            tbxMenuName.Enabled = true;
            ddlPrivileges.Enabled = true;
            lbnEditMenu.Visible = false;
            lbnUpdateMenu.Visible = true;
            lbnCancelMenu.Visible = true;

        }

        /// <summary>
        /// Save the Main Menu Item changes
        /// </summary>
        protected int SaveMainMenuItem(SM_MenuItem menuItem)
        {
            int result = 0;

            if (menuItem.NavigationId > 0)
            {
                result = GetSiteManagementFacade().UpdateTopMenuItem(menuItem);
            }
            else
            {
                result = GetSiteManagementFacade().AddTopMenuItem(menuItem);
            }

            return result;
        }

        /// <summary>
        /// Delete selected Top Menu Items
        /// </summary>
        protected void DeleteCheckedMenuItems()
        {
            CheckBox chkEdit;
            int deletionCount = 0;
            int result = 0;

            foreach (RepeaterItem rptTopNavItems in rpt_TopNavItems.Items)
            {
                chkEdit = (CheckBox)rptTopNavItems.FindControl("chkEditMenu");

                if (chkEdit.Checked)
                {
                    int menuId = Convert.ToInt32(((Label)rptTopNavItems.FindControl("lbl_MenuId")).Text);

                    result += this.GetSiteManagementFacade().DeleteTopMenuItem(menuId);
                    deletionCount++;
                }
            }

            //message to user
            if (deletionCount == 0)
            {
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "No menu items were selected.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
            else if (deletionCount != result)
            {
                //rest the data set to prepare for new data reteival
                ClearSelectedTopNav();
                ClearSelectedSubNav();
                //bind the data to the repeater and drop down (gets data from database if null)
                BindSubMenu();
                BindTopMenu();

                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = result + " of " + deletionCount + " menu item" + (deletionCount > 1 ? "s were " : " was ") + "deleted.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
            else
            {
                //rest the data set to prepare for new data reteival
                ClearSelectedTopNav();
                ClearSelectedSubNav();
                //bind the data to the repeater and drop down (gets data from database if null)
                BindSubMenu();
                BindTopMenu();

                messages.ForeColor = System.Drawing.Color.DarkGreen;
                messages.Text = deletionCount + " menu item" + (deletionCount > 1 ? "s were " : " was ") + "deleted.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        /// <summary>
        /// Add a new site to the list
        /// </summary>
        protected void AddNewSite()
        {
            //create new website object
            WebSite website = new WebSite(-1, "new website", "", "");

            //add the new site
            //AVAILABLE_WEBSITES.Add(website);
            //rpt_AvailableSites.DataSource = AVAILABLE_WEBSITES;
            ((IList<WebSite>)rpt_AvailableSites.DataSource).Add(website);

            //update list
            rpt_AvailableSites.DataBind();

            //get the new count and set the new row as active
            int rowId = rpt_AvailableSites.Items.Count - 1;

            TextBox tbxSiteName = (TextBox)rpt_AvailableSites.Items[rowId].FindControl("tbx_SiteName");
            TextBox tbxSiteURL = (TextBox)rpt_AvailableSites.Items[rowId].FindControl("tbx_SiteURL");
            TextBox tbxConnectionString = (TextBox)rpt_AvailableSites.Items[rowId].FindControl("tbx_ConnectionString");
            LinkButton lbnEdit = (LinkButton)rpt_AvailableSites.Items[rowId].FindControl("lnkEdit");
            LinkButton lbnUpdate = (LinkButton)rpt_AvailableSites.Items[rowId].FindControl("lnkUpdate");
            LinkButton lbnCancel = (LinkButton)rpt_AvailableSites.Items[rowId].FindControl("lnkCancel");

            //set the original values to check for changes
            ORIGINAL_SITEURL = ORIGINAL_SITENAME = ORIGINAL_CONNECTION_STRING = "";

            //reconfigure
            tbxSiteName.Enabled = true;
            tbxSiteURL.Enabled = true;
            tbxConnectionString.Enabled = true;
            lbnEdit.Visible = false;
            lbnUpdate.Visible = true;
            lbnCancel.Visible = true;

        }


        /// <summary>
        /// Save the available wesbite changes
        /// </summary>
        protected int SaveAvailableSite(WebSite website)
        {
            int result = 0;

            if (website.WebsiteId > 0)
            {
                result = GetSiteManagementFacade().UpdateAvailableWebsite(website);
            }
            else
            {
                result = GetSiteManagementFacade().AddAvailableWebsite(website);
            }

            return result;
        }


        /// <summary>
        /// Delete selected Team Members
        /// </summary>
        protected void DeleteCheckedSites()
        {
            CheckBox chkEdit;
            int deletionCount = 0;
            int result = 0;

            foreach (RepeaterItem rptAvailableSites in rpt_AvailableSites.Items)
            {
                chkEdit = (CheckBox)rptAvailableSites.FindControl("chkEdit");

                if (chkEdit.Checked)
                {
                    int siteId = Convert.ToInt32(((Label)rptAvailableSites.FindControl("lbl_SiteId")).Text);

                    result += this.GetSiteManagementFacade().DeleteAvailableWebsite(siteId);
                    deletionCount++;
                }
            }

            //message to user
            if (deletionCount == 0)
            {
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "No sites were selected.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
            else if (deletionCount != result)
            {
                //rest the data set to prepare for new data reteival
                ClearAvailableWebsites();
                ClearSelectedTopNav();
                ClearSelectedSubNav();
                //bind the data to the repeater and drop down (gets data from database if null)
                BindAvailableWebsites();
                BindSubMenu();
                BindTopMenu();


                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = result + " of " + deletionCount + " site" + (deletionCount > 1 ? "s were " : " was ") + "deleted.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
            else
            {
                //rest the data set to prepare for new data reteival
                ClearAvailableWebsites();
                ClearSelectedTopNav();
                ClearSelectedSubNav();
                //bind the data to the repeater and drop down (gets data from database if null)
                BindAvailableWebsites();
                BindSubMenu();
                BindTopMenu();

                messages.ForeColor = System.Drawing.Color.DarkGreen;
                messages.Text = deletionCount + " site" + (deletionCount > 1 ? "s were " : " was ") + "deleted.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        #endregion

        #region Event Handlers

        protected void ddl_MainMenuSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //set the new selected index
                SELECTED_MENU_INDEX = ddl_MainMenuSelector.SelectedIndex;

                //clear the data set so that it will retreive new data
                ClearSelectedSubNav();

                //retrieve new data based on newly selected website
                BindSubMenu();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during sub menu item retrieval", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Editing Error: Sorry we were unable to retrieve the selected menu's sub menu's items.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        protected void ddl_SiteSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //set the new selected index
                SELECTED_SITE_INDEX = ddl_SiteSelector.SelectedIndex;

                //clear the data set so that it will retreive new data
                ClearSelectedTopNav();
                ClearSelectedSubNav();

                //bind the data to the repeater and drop down (gets data from database if null)
                BindTopMenu();
                BindSubMenu();


                //retrieve new data based on newly selected website
                BindTopMenu();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during menu item retrieval", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Editing Error: Sorry we were unable to retrieve the selected site's menu items.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        /// <summary>
        /// btn_DeleteMenuItems_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_DeleteSubMenuItems_Click(object sender, System.EventArgs e)
        {
            try
            {
                DeleteCheckedSubMenuItems();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during sub-menu item deletion", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Editing Error: Sorry we were unable to delete the selected sub-menu items.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

        }

        /// <summary>
        /// lbn_NewMenuItem_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void lbn_NewSubMenu_Click(object sender, System.EventArgs e)
        {
            try
            {
                AddSubMenuItem();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during sub-menu item addition", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Editing Error: Sorry we were unable to create a new sub-menu item.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        /// <summary>
        /// btn_DeleteMenuItems_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_DeleteMenuItems_Click(object sender, System.EventArgs e)
        {
            try
            {
                DeleteCheckedMenuItems();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during menu item deletion", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Editing Error: Sorry we were unable to delete the selected menu items.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

        }

        /// <summary>
        /// lbn_NewMenuItem_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void lbn_NewMenuItem_Click(object sender, System.EventArgs e)
        {
            try
            {
                AddMainMenuItem();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during menu item addition", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Editing Error: Sorry we were unable to create a new menu item.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        /// <summary>
        /// btn_DeleteSites_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_DeleteSites_Click(object sender, System.EventArgs e)
        {
            try
            {
                DeleteCheckedSites();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during site deletion", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Editing Error: Sorry we were unable to delete the selected website.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

        }

        /// <summary>
        /// lnkAddSite_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void lbn_NewSite_Click(object sender, System.EventArgs e)
        {
            try
            {
                AddNewSite();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during site addition", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Editing Error: Sorry we were unable to create a new website entry.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        /// <summary>
        /// rptGames_ItemCommand
        /// </summary>
        private void rpt_AvailableSites_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //get the controls
            TextBox tbxSiteName = (TextBox)rpt_AvailableSites.Items[e.Item.ItemIndex].FindControl("tbx_SiteName");
            Label lbSiteId = (Label)rpt_AvailableSites.Items[e.Item.ItemIndex].FindControl("lbl_SiteId");
            TextBox tbxSiteURL = (TextBox)rpt_AvailableSites.Items[e.Item.ItemIndex].FindControl("tbx_SiteURL");
            TextBox tbxConnectionString = (TextBox)rpt_AvailableSites.Items[e.Item.ItemIndex].FindControl("tbx_ConnectionString");
            LinkButton lbnEdit = (LinkButton)rpt_AvailableSites.Items[e.Item.ItemIndex].FindControl("lnkEdit");
            LinkButton lbnUpdate = (LinkButton)rpt_AvailableSites.Items[e.Item.ItemIndex].FindControl("lnkUpdate");
            LinkButton lbnCancel = (LinkButton)rpt_AvailableSites.Items[e.Item.ItemIndex].FindControl("lnkCancel");


            switch (e.CommandName)
            {
                case "cmdSiteEdit":
                {
                    //set the original values to check for changes
                    ORIGINAL_SITEURL = tbxSiteURL.Text;
                    ORIGINAL_SITENAME = tbxSiteName.Text;
                    ORIGINAL_CONNECTION_STRING = tbxConnectionString.Text;


                    //reconfigure
                    tbxSiteName.Enabled = true;
                    tbxSiteURL.Enabled = true;
                    tbxConnectionString.Enabled = true;
                    lbnEdit.Visible = false;
                    lbnUpdate.Visible = true;
                    lbnCancel.Visible = true;
                    break;
                }
                case "cmdSiteUpdate":
                {
                    //check for changes
                    string ORIGINAL_VALUE = ORIGINAL_SITENAME + ORIGINAL_SITEURL + ORIGINAL_CONNECTION_STRING;
                    if (!ORIGINAL_VALUE.Equals(tbxSiteName.Text + tbxSiteURL.Text + tbxConnectionString.Text))
                    {
                        try
                        {
                            //write changes to database
                            WebSite tempsite = new WebSite();
                            tempsite.WebsiteId = Convert.ToInt32(lbSiteId.Text);
                            tempsite.WebsiteName = tbxSiteName.Text;
                            tempsite.WebsiteURL = tbxSiteURL.Text;
                            tempsite.ConnectionString = tbxConnectionString.Text;

                            int result = SaveAvailableSite(tempsite);

                            //message to user
                            if (result == 0)
                            {
                                messages.ForeColor = System.Drawing.Color.Crimson;
                                messages.Text = "Unable to save your website changes.";
                                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
                            }
                            else
                            {
                                //get newest data for websites
                                ClearAvailableWebsites();
                                //bind the data to the repeater and drop down
                                BindAvailableWebsites();

                                //alert user to success
                                messages.ForeColor = System.Drawing.Color.DarkGreen;
                                messages.Text = "Website changes saved successfully.";
                                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
                            }

                        }
                        catch (Exception)
                        {
                        }
                    }

                    //reconfigure
                    tbxSiteName.Enabled = false;
                    tbxSiteURL.Enabled = false;
                    tbxConnectionString.Enabled = false;
                    lbnEdit.Visible = true;
                    lbnUpdate.Visible = false;
                    lbnCancel.Visible = false;
                    break;
                }
                case "cmdCancel":
                {
                    if (Convert.ToInt32(lbSiteId.Text) < 1 )
                    {
                        ((IList<WebSite>)rpt_AvailableSites.DataSource).RemoveAt(e.Item.ItemIndex);
                        rpt_AvailableSites.DataBind();
                    }
                    else
                    {
                        //set the original values back
                        tbxSiteName.Text = ORIGINAL_SITENAME;
                        tbxSiteURL.Text = ORIGINAL_SITEURL;
                        tbxConnectionString.Text = ORIGINAL_CONNECTION_STRING;

                        //reconfigure
                        tbxSiteName.Enabled = false;
                        tbxSiteURL.Enabled = false;
                        tbxConnectionString.Enabled = false;

                        lbnEdit.Visible = true;
                        lbnUpdate.Visible = false;
                        lbnCancel.Visible = false;
                    }

                    break;
                }
            }
        }

        private void rpt_AvailableSites_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
        }

        private void rpt_AvailableSites_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //get the controls
                Label lblSiteId = (Label)e.Item.FindControl("lbl_SiteId");
                TextBox tbxSiteName = (TextBox)e.Item.FindControl("tbx_SiteName");
                TextBox tbxSiteURL = (TextBox)e.Item.FindControl("tbx_SiteURL");
                TextBox tbxConnectionString = (TextBox)e.Item.FindControl("tbx_ConnectionString");

                lblSiteId.Text = ((WebSite)e.Item.DataItem).WebsiteId.ToString();
                tbxSiteName.Text = ((WebSite)e.Item.DataItem).WebsiteName;
                tbxSiteURL.Text = ((WebSite)e.Item.DataItem).WebsiteURL;
                tbxConnectionString.Text = ((WebSite)e.Item.DataItem).ConnectionString;
            }
        }

        private void rpt_TopNavItems_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
        }

        private void rpt_TopNavItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //get the controls
                Label lblMenuId = (Label)e.Item.FindControl("lbl_MenuId");
                TextBox tbxMenuName = (TextBox)e.Item.FindControl("tbx_MenuName");
                DropDownList ddlPrivileges = (DropDownList)e.Item.FindControl("ddl_PrivilegeSelector");

                //set the pull down data
                ddlPrivileges.DataSource = SITE_PRIVILEGES;
                ddlPrivileges.DataTextField = "PrivilegeName";
                ddlPrivileges.DataValueField = "PrivilegeId";
                ddlPrivileges.DataBind();

                lblMenuId.Text = ((SM_MenuItem)e.Item.DataItem).NavigationId.ToString();
                tbxMenuName.Text = ((SM_MenuItem)e.Item.DataItem).NavigationTitle;
                ddlPrivileges.SelectedValue = ((SM_MenuItem)e.Item.DataItem).PrivilegeId.ToString();
            }
        }

        /// <summary>
        /// rptGames_ItemCommand
        /// </summary>
        private void rpt_TopNavItems_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //get the controls
            Label lblMenuId = (Label)rpt_TopNavItems.Items[e.Item.ItemIndex].FindControl("lbl_MenuId");
            TextBox tbxMenuName = (TextBox)rpt_TopNavItems.Items[e.Item.ItemIndex].FindControl("tbx_MenuName");
            DropDownList ddlPrivileges = (DropDownList)rpt_TopNavItems.Items[e.Item.ItemIndex].FindControl("ddl_PrivilegeSelector");
            LinkButton lbnEditMenu = (LinkButton)rpt_TopNavItems.Items[e.Item.ItemIndex].FindControl("lnkEditMenu");
            LinkButton lbnUpdateMenu = (LinkButton)rpt_TopNavItems.Items[e.Item.ItemIndex].FindControl("lnkUpdateMenu");
            LinkButton lbnCancelMenu = (LinkButton)rpt_TopNavItems.Items[e.Item.ItemIndex].FindControl("lnkCancelMenu");


            switch (e.CommandName)
            {
                case "cmdMenuEdit":
                    {
                        //set the original values to check for changes
                        ORIGINAL_PRIVILEGE_ID = ddlPrivileges.SelectedValue;
                        ORIGINAL_MENU_NAME = tbxMenuName.Text;

                        //reconfigure
                        tbxMenuName.Enabled = true;
                        ddlPrivileges.Enabled = true;

                        lbnEditMenu.Visible = false;
                        lbnUpdateMenu.Visible = true;
                        lbnCancelMenu.Visible = true;
                        break;
                    }
                case "cmdMenuUpdate":
                    {
                        //check for changes
                        string ORIGINAL_VALUE = ORIGINAL_MENU_NAME + ORIGINAL_PRIVILEGE_ID;
                        if (!ORIGINAL_VALUE.Equals(tbxMenuName.Text + ddlPrivileges.SelectedValue))
                        {
                            try
                            {
                                //write changes to database
                                SM_MenuItem menuItem = new SM_MenuItem();
                                menuItem.NavigationId = Convert.ToInt32(lblMenuId.Text);
                                menuItem.NavigationTitle = tbxMenuName.Text;
                                menuItem.PrivilegeId = Convert.ToInt32(ddlPrivileges.SelectedValue);
                                menuItem.WebsiteId = Convert.ToInt32(ddl_SiteSelector.SelectedValue);

                                int result = SaveMainMenuItem(menuItem);

                                //message to user
                                if (result == 0)
                                {
                                    messages.ForeColor = System.Drawing.Color.Crimson;
                                    messages.Text = "Unable to save your navigation changes.";
                                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
                                }
                                else
                                {
                                    //get newest data for websites
                                    ClearSelectedTopNav();
                                    //bind the data to the repeater and drop down
                                    BindTopMenu();

                                    //alert user to success
                                    messages.ForeColor = System.Drawing.Color.DarkGreen;
                                    messages.Text = "Navigation changes saved successfully.";
                                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
                                }

                            }
                            catch (Exception)
                            {
                            }
                        }

                        //reconfigure
                        tbxMenuName.Enabled = false;
                        ddlPrivileges.Enabled = false;

                        lbnEditMenu.Visible = true;
                        lbnUpdateMenu.Visible = false;
                        lbnCancelMenu.Visible = false;
                        break;
                    }
                case "cmdMenuCancel":
                    {
                        if (Convert.ToInt32(lblMenuId.Text) < 1)
                        {
                            ((IList<SM_MenuItem>)rpt_TopNavItems.DataSource).RemoveAt(e.Item.ItemIndex);
                            rpt_TopNavItems.DataBind();
                        }
                        else
                        {
                            //set the original values back
                            ddlPrivileges.SelectedValue = ORIGINAL_PRIVILEGE_ID;
                            tbxMenuName.Text = ORIGINAL_MENU_NAME;

                            //reconfigure
                            tbxMenuName.Enabled = false;
                            ddlPrivileges.Enabled = false;

                            lbnEditMenu.Visible = true;
                            lbnUpdateMenu.Visible = false;
                            lbnCancelMenu.Visible = false;
                        }

                        break;
                    }
            }
        }

        private void rpt_SubMenuItems_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
        }

        private void rpt_SubMenuItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //get the controls
                Label lblSubMenuId = (Label)e.Item.FindControl("lbl_SubMenuId");
                TextBox tbxSubMenuName = (TextBox)e.Item.FindControl("tbx_SubMenuName");
                DropDownList ddlPrivileges = (DropDownList)e.Item.FindControl("ddl_Privilege_Selector");
                DropDownList ddlUserControl = (DropDownList)e.Item.FindControl("ddl_UserControl");

                //set the pull down data
                ddlPrivileges.DataSource = SITE_PRIVILEGES;
                ddlPrivileges.DataTextField = "PrivilegeName";
                ddlPrivileges.DataValueField = "PrivilegeId";
                ddlPrivileges.DataBind();

                //set the pull down data
                ddlUserControl.DataSource = USER_CONTROLS;
                ddlUserControl.DataTextField = "UserControlName";
                ddlUserControl.DataValueField = "UserControlId";
                ddlUserControl.DataBind();

                lblSubMenuId.Text = ((SM_MenuItem)e.Item.DataItem).NavigationId.ToString();
                tbxSubMenuName.Text = ((SM_MenuItem)e.Item.DataItem).NavigationTitle;
                ddlPrivileges.SelectedValue = ((SM_MenuItem)e.Item.DataItem).PrivilegeId.ToString();
                ddlUserControl.SelectedValue = ((SM_MenuItem)e.Item.DataItem).UserControlId.ToString();
            }
        }

        /// <summary>
        /// rptGames_ItemCommand
        /// </summary>
        private void rpt_SubMenuItems_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //get the controls
            Label lblSubMenuId = (Label)rpt_SubMenuItems.Items[e.Item.ItemIndex].FindControl("lbl_SubMenuId");
            TextBox tbxSubMenuName = (TextBox)rpt_SubMenuItems.Items[e.Item.ItemIndex].FindControl("tbx_SubMenuName");
            DropDownList ddlPrivileges = (DropDownList)rpt_SubMenuItems.Items[e.Item.ItemIndex].FindControl("ddl_Privilege_Selector");
            DropDownList ddlUserControl = (DropDownList)rpt_SubMenuItems.Items[e.Item.ItemIndex].FindControl("ddl_UserControl");
            LinkButton lbnEditSubMenu = (LinkButton)rpt_SubMenuItems.Items[e.Item.ItemIndex].FindControl("lnkEditSubMenu");
            LinkButton lbnUpdateSubMenu = (LinkButton)rpt_SubMenuItems.Items[e.Item.ItemIndex].FindControl("lnkUpdateSubMenu");
            LinkButton lbnCancelSubMenu = (LinkButton)rpt_SubMenuItems.Items[e.Item.ItemIndex].FindControl("lnkCancelSubMenu");


            switch (e.CommandName)
            {
                case "cmdSubMenuEdit":
                    {
                        //set the original values to check for changes
                        ORIGINAL_SUB_PRIVILEGE_ID = ddlPrivileges.SelectedValue;
                        ORIGINAL_USER_CONTROL_ID = ddlUserControl.SelectedValue;
                        ORIGINAL_SUBMENU_NAME = tbxSubMenuName.Text;

                        //reconfigure
                        tbxSubMenuName.Enabled = true;
                        ddlPrivileges.Enabled = true;
                        ddlUserControl.Enabled = true;

                        lbnEditSubMenu.Visible = false;
                        lbnUpdateSubMenu.Visible = true;
                        lbnCancelSubMenu.Visible = true;
                        break;
                    }
                case "cmdSubMenuUpdate":
                    {
                        //check for changes
                        string ORIGINAL_VALUE = ORIGINAL_SUBMENU_NAME + ORIGINAL_SUB_PRIVILEGE_ID + ORIGINAL_USER_CONTROL_ID;
                        if (!ORIGINAL_VALUE.Equals(tbxSubMenuName.Text + ddlPrivileges.SelectedValue + ddlUserControl.SelectedValue))
                        {
                            try
                            {
                                //write changes to database
                                SM_MenuItem SubMenuItem = new SM_MenuItem();
                                SubMenuItem.NavigationId = Convert.ToInt32(lblSubMenuId.Text);
                                SubMenuItem.NavigationTitle = tbxSubMenuName.Text;
                                SubMenuItem.PrivilegeId = Convert.ToInt32(ddlPrivileges.SelectedValue);
                                SubMenuItem.ParentNavId = Convert.ToInt32(ddl_MainMenuSelector.SelectedValue);
                                SubMenuItem.UserControlId = Convert.ToInt32(ddlUserControl.SelectedValue);

                                int result = SaveSubMenuItem(SubMenuItem);

                                //message to user
                                if (result == 0)
                                {
                                    messages.ForeColor = System.Drawing.Color.Crimson;
                                    messages.Text = "Unable to save your sub-navigation changes.";
                                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
                                }
                                else
                                {
                                    //get newest data for websites
                                    ClearSelectedSubNav();

                                    //bind the data to the repeater and drop down
                                    BindSubMenu();

                                    //alert user to success
                                    messages.ForeColor = System.Drawing.Color.DarkGreen;
                                    messages.Text = "Sub-navigation changes saved successfully.";
                                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
                                }

                            }
                            catch (Exception)
                            {
                            }
                        }

                        //reconfigure
                        tbxSubMenuName.Enabled = false;
                        ddlPrivileges.Enabled = false;
                        ddlUserControl.Enabled = false;
                        lbnEditSubMenu.Visible = true;
                        lbnUpdateSubMenu.Visible = false;
                        lbnCancelSubMenu.Visible = false;
                        break;
                    }
                case "cmdSubMenuCancel":
                    {
                        if (Convert.ToInt32(lblSubMenuId.Text) < 1)
                        {
                            ((IList<SM_MenuItem>)rpt_SubMenuItems.DataSource).RemoveAt(e.Item.ItemIndex);
                            rpt_SubMenuItems.DataBind();
                        }
                        else
                        {
                            //set the original values back
                            ddlPrivileges.SelectedValue = ORIGINAL_PRIVILEGE_ID;
                            ddlUserControl.SelectedValue = ORIGINAL_USER_CONTROL_ID;
                            tbxSubMenuName.Text = ORIGINAL_SUBMENU_NAME;

                            //reconfigure
                            tbxSubMenuName.Enabled = false;
                            ddlPrivileges.Enabled = false;
                            ddlUserControl.Enabled = false;

                            lbnEditSubMenu.Visible = true;
                            lbnUpdateSubMenu.Visible = false;
                            lbnCancelSubMenu.Visible = false;
                        }

                        break;
                    }
            }
        }


        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rpt_AvailableSites.ItemCreated += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_AvailableSites_ItemCreated);
            this.rpt_AvailableSites.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_AvailableSites_ItemDataBound);
            this.rpt_AvailableSites.ItemCommand += new System.Web.UI.WebControls.RepeaterCommandEventHandler(this.rpt_AvailableSites_ItemCommand);
            this.Load += new System.EventHandler(this.Page_Load);
            this.rpt_TopNavItems.ItemCreated += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_TopNavItems_ItemCreated);
            this.rpt_TopNavItems.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_TopNavItems_ItemDataBound);
            this.rpt_TopNavItems.ItemCommand += new System.Web.UI.WebControls.RepeaterCommandEventHandler(this.rpt_TopNavItems_ItemCommand);

            this.rpt_SubMenuItems.ItemCreated += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_SubMenuItems_ItemCreated);
            this.rpt_SubMenuItems.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_SubMenuItems_ItemDataBound);
            this.rpt_SubMenuItems.ItemCommand += new System.Web.UI.WebControls.RepeaterCommandEventHandler(this.rpt_SubMenuItems_ItemCommand);

            this.ddl_SiteSelector.SelectedIndexChanged += new System.EventHandler(this.ddl_SiteSelector_SelectedIndexChanged);
            this.ddl_MainMenuSelector.SelectedIndexChanged += new System.EventHandler(this.ddl_MainMenuSelector_SelectedIndexChanged);

        }
        #endregion

    }
}