using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;

using log4net;
using MagicAjax;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;
using System.Text;
using Kaneva;
using System.Diagnostics;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class ZoneSettings : BaseUserControl
    {
        #region Declarations

        private bool canEdit = false;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            // Clear gameSearch redirection
            Session["CallingPage"] = null;

            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.STAR_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        canEdit = true;
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    default:
                        break;
                }

                if (!canEdit)
                {
                    RedirectToHomePage();
                }
                else
                {
                    //next get the user id of the developer in that repeater item
                    if (Session["SelectedGameId"] == null || Convert.ToInt32(Session["SelectedGameId"]) <= 0)
                    {
                        //Must select a community first
                        //store this pages URL for return
                        Session["CallingPage"] = Request.Url.ToString();
                        Response.Redirect(ResolveUrl("~/Default.aspx?ws=" + GetCurrentSiteID((int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.DEVELOPER) + "&mn=" + GetGameSearchParentMenuID() + "&sn=" + GetGameSearchControlID()));
                    }

                    int gameId = Convert.ToInt32(Session["SelectedGameId"]);
                    string filter = "timestampdiff(MONTH, last_ping_datetime, now()) < 6";  // less than 6 months old
                    if (SELECTED_WOK_SERVER_ID != -1)
                    {
                        // Allowed the currently selected server to be included in the list in case it does not pass the primary filter
                        filter = "(" + filter + " OR server_id=" + SELECTED_WOK_SERVER_ID.ToString() + ")";
                    }

                    var dtGameServers = GetGameFacade().GetServerByGameId(gameId, filter, "server_name, port");

                    if (dtGameServers != null && dtGameServers.Rows.Count > 0)
                    {
                        dtGameServers.Columns.Add("FormattedName", typeof(string), "server_name + ':' + port + ' (ID: ' + server_id + ', ' + " + getServerVisibilityExpression() + " + ', ' + " + getServerStatusExpression() + " + ')'");
                        ddlGameServers.DataSource = dtGameServers;
                        ddlGameServers.DataTextField = "FormattedName";
                        ddlGameServers.DataValueField = "server_id";
                        ddlGameServers.DataBind();
                        ddlGameServers.Items.Insert(0, new ListItem("(Not Selected)", "-1"));
                        ddlGameServers.SelectedValue = SELECTED_WOK_SERVER_ID.ToString();
                    }
                    else
                    {
                        ddlGameServers.DataSource = null;
                        ddlGameServers.DataBind();
                        ddlGameServers.Items.Insert(0, new ListItem("(No Servers Found)", "-1"));
                    }

                    if (!IsPostBack)
                    {

                        // Set the page on the paging control for team members
                        pgTop.CurrentPageNumber = 1;
                        pgBottom.CurrentPageNumber = 1;

                        //bind datagrids
                        BindChannelZoneData(1);    // No search will be done if txtChannelZoneName.Text is empty
                    }

                    //clear message
                    messages.Text = "";
                }
            }
            else
            {
                RedirectToHomePage();
            }
        }

        #region Attributes

        /// <summary>
        /// stores the list of company developers
        /// </summary>
        /// <returns>int</returns>
        private List<WOK3DPlace> CHANNEL_ZONE_RESULTS
        {
            get
            {
                if (ViewState["channelZoneResults"] == null)
                {
                    return new List<WOK3DPlace>();
                }
                return (List<WOK3DPlace>)ViewState["channelZoneResults"];
            }
            set
            {
                ViewState["channelZoneResults"] = value;
            }
        }

        /// <summary>
        /// stores the server that the zone is being tied to
        /// </summary>
        /// <returns>int</returns>
        private int SELECTED_WOK_SERVER_ID
        {
            get
            {
                if (ViewState["SelectedWoKServerId"] == null)
                {
                    return (int)(Session["SelectedWoKServerId"] ?? -1);
                }
                return (int)ViewState["SelectedWoKServerId"];
            }
            set
            {
                ViewState["SelectedWoKServerId"] = value;
                Session["SelectedWoKServerId"] = value;
            }
        }


        #endregion

        #region Main Functionality

        private void BindChannelZoneData(int pageNumber)
        {
            if (txtChannelZoneName.Text == "")
            {
                // Do not search if keyword is empty
                pageNumber = -1;
            }

            try
            {
                //set initial values
                PagedList<WOK3DPlace> pdl = new PagedList<WOK3DPlace>();

                //get default pagesize
                int itemsPerPage = 14;

                //added to skip initial load
                if (pageNumber > 0)
                {
                    //retreive the authorized users list
                    pdl = GetSiteManagementFacade().Search3DPlaceByName(txtChannelZoneName.Text, pageNumber, itemsPerPage);

                    //store users
                    CHANNEL_ZONE_RESULTS = (List<WOK3DPlace>)pdl.List;
                }

                //bind the result set to the repeater control
                rpt_ChannelZones.DataSource = pdl;
                rpt_ChannelZones.DataBind();

                //set the paging controls
                pgTop.NumberOfPages = Math.Ceiling((double)pdl.TotalCount / itemsPerPage).ToString();
                pgTop.DrawControl();

                pgBottom.NumberOfPages = Math.Ceiling((double)pdl.TotalCount / itemsPerPage).ToString();
                pgBottom.DrawControl();

                // Test the results display message
                lblSearch2.Text = lblSearch.Text = GetResultsText(pdl.TotalCount, pageNumber, itemsPerPage, pdl.Count);
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during zone listing load.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error: Sorry we were unable to load the zone you're searching for.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

        }

        protected string getServerVisibilityExpression()
        {
            // Return a string that can be used in DataColumn.Expression to format visibility_id column
            // e.g. IIF(visibility_id=2, 'Private', IIF(visibility_id=1, 'Public', 'visibility:' + visibility_id))
            string expression = "'visibility: ' + visibility_id";  // Evaluate to raw integer value if it does not match any of the known value

            var dt = WebCache.GetGameServerVisbility();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    expression = "IIF(visibility_id=" + Convert.ToString(row["visibility_id"]) + ", '" + Convert.ToString(row["name"]) + "', " + expression + ")";
                }
            }
            
            return expression;
        }

        protected string getServerStatusExpression()
        {
            // Return a string that can be used in DataColumn.Expression to format server_status_id column
            // e.g. IIF(server_status_id=2, 'stopping', IIF(server_status_id=1, 'running', IIF(server_status_id=0, 'stopped', 'status: ' + server_status_id)))
            string expression = "'status: ' + server_status_id";  // Evaluate to raw integer value if it does not match any of the known value

            var dt = WebCache.GetGameServerStatus();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    expression = "IIF(server_status_id=" + Convert.ToString(row["server_status_id"]) + ", '" + Convert.ToString(row["name"]) + "', " + expression + ")";
                }
            }

            return expression;
        }

        #endregion

        #region Events

        protected void ddlGameServers_SelectedIndexChanged(object sender, EventArgs e)
        {
            SELECTED_WOK_SERVER_ID = Convert.ToInt32(ddlGameServers.SelectedValue);
            BindChannelZoneData(1);
        }

        /// <summary>
        /// Search Event Handler
        /// </summary>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindChannelZoneData(1);
        }

        protected void ddlTiedStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //first get a reference to the repeater item the drop down is in
                RepeaterItem item = (RepeaterItem)((DropDownList)sender).Parent;

                //get channelZoneId from repeater item
                HtmlInputHidden hidChannelZoneId = (HtmlInputHidden)item.FindControl("hidChannelZoneId");
                int selectedChannelZoneId = Convert.ToInt32(hidChannelZoneId.Value);

                //get the Drop down causing the change
                DropDownList activeDropDown = (DropDownList)item.FindControl("ddlTiedStatus");
                if (activeDropDown.SelectedValue == "")
                {
                    // Keep current server tie
                    return;
                }

                //save change to the Database
                int result = GetSiteManagementFacade().Set3DPlaceTiedStatus(selectedChannelZoneId, SELECTED_WOK_SERVER_ID, activeDropDown.SelectedValue);

                //check for and communicate if insert failed
                if (result < 1)
                {
                    throw new Exception("Update failed!");
                }

                //broadcast to servers
                GetGameFacade().BroadcastUpdateScriptZoneSpinDownDelayEvent(selectedChannelZoneId);

                //refresh page data
                BindChannelZoneData(pgTop.CurrentPageNumber);
                messages.ForeColor = System.Drawing.Color.Green;
                messages.Text = "Zone updated successfully.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during zone edit.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Editing Error: Sorry we were unable to save your zone changes.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        protected void btnUpdateSpinDownDelay_Click(object sender, EventArgs e)
        {
            try
            {
                //first get a reference to the repeater item the drop down is in
                RepeaterItem item = (RepeaterItem)((Button)sender).Parent;

                //get channelZoneId from repeater item
                HtmlInputHidden hidChannelZoneId = (HtmlInputHidden)item.FindControl("hidChannelZoneId");
                int selectedChannelZoneId = Convert.ToInt32(hidChannelZoneId.Value);

                //get zoneInstanceId and zoneType from repeater item
                HtmlInputHidden hidZoneInstanceId = (HtmlInputHidden)item.FindControl("hidZoneInstanceId");
                int selectedZoneInstanceId = Convert.ToInt32(hidZoneInstanceId.Value);

                HtmlInputHidden hidZoneType = (HtmlInputHidden)item.FindControl("hidZoneType");
                int selectedZoneType = Convert.ToInt32(hidZoneType.Value);

                //get old delay value
                HtmlInputHidden hidOldSpinDownDelay = (HtmlInputHidden)item.FindControl("hidOldSpinDownDelay");
                int oldSpinDownDelay = Convert.ToInt32(hidOldSpinDownDelay.Value);

                //get the text box with the new value
                TextBox activeTextBox = (TextBox)item.FindControl("txtSpinDownDelay");
                int spinDownDelay = Convert.ToInt32(activeTextBox.Text);
                if (spinDownDelay < 0)
                {
                    throw new Exception("Spin down delay must not be negative");
                }

                if (spinDownDelay == oldSpinDownDelay)
                {
                    // Not changed
                    return;
                }

                //save change to the Database
                int result = GetSiteManagementFacade().Set3DPlaceSpinDownDelayMinutes(selectedZoneInstanceId, selectedZoneType, spinDownDelay);

                //check for and communicate if insert failed
                if (result < 1)
                {
                    throw new Exception("Update failed!");
                }

                //broadcast to servers
                GetGameFacade().BroadcastUpdateScriptZoneSpinDownDelayEvent(selectedChannelZoneId);

                //refresh page data
                BindChannelZoneData(pgTop.CurrentPageNumber);
                messages.ForeColor = System.Drawing.Color.Green;
                messages.Text = "Zone updated successfully.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");

            }
            catch (Exception ex)
            {
                m_logger.Error("Error during zone edit.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Editing Error: Sorry we were unable to save your zone changes.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        protected void lbtnScriptZoneServerId_Click(object sender, EventArgs e)
        {
            LinkButton lbtnScriptZoneServerId = null;
            GameServer scriptServerInfo = null;
            try
            {
                // Get the link button
                lbtnScriptZoneServerId = (LinkButton)sender;
                if (lbtnScriptZoneServerId.Attributes["onmouseover"] != null)
                {
                    // Already hooked
                    return;
                }

                int scriptZoneServerId = Convert.ToInt32(lbtnScriptZoneServerId.Text);
                Debug.Assert(scriptZoneServerId > 0);

                scriptServerInfo = GetGameFacade().GetServer(scriptZoneServerId);
            }
            catch (Exception ex)
            {
                m_logger.Error("Error while looking up server details.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Sorry we were unable to lookup server details.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

            if (lbtnScriptZoneServerId != null)
            {
                string toolTip;
                if (scriptServerInfo == null)
                {
                    toolTip = "\\ <h4>Server Details</h4>" +
                              "\\<b>Error loading server details<br>";
                }
                else
                {
                    toolTip = "\\ <h4>Server Details</h4>" +
                              "\\<b>Server/Port: </b>" + scriptServerInfo.ServerName + ":" + scriptServerInfo.Port + "<br>" +
                              "\\<b>Visibility: </b>" + GetGameVisibiltyDescription(scriptServerInfo.VisibiltyId) + "<br>" +
                              "\\<b>Status: </b>" + GetServerStatusDescription(scriptServerInfo.ServerStatusId) + "<br>";
                }

                // Hook-up onmouseover event on the client-side
                lbtnScriptZoneServerId.Attributes.Add("onmouseover", "whiteBalloonSans.showTooltip(event,'" + CleanJavascriptFull(toolTip) + "')");
            }
        }

        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            pgTop.CurrentPageNumber = e.PageNumber;
            pgTop.DrawControl();
            pgBottom.CurrentPageNumber = e.PageNumber;
            pgBottom.DrawControl();
            BindChannelZoneData(e.PageNumber);
        }

        private void rpt_ChannelZones_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //find all the controls in the repeater
                Label lbChannelZoneName = (Label)e.Item.FindControl("lb_ChannelZoneName");
                DropDownList ddlTiedStatus = (DropDownList)e.Item.FindControl("ddlTiedStatus");
                HtmlInputHidden hidChannelZoneId = (HtmlInputHidden)e.Item.FindControl("hidChannelZoneId");
                HtmlInputHidden hidZoneInstanceId = (HtmlInputHidden)e.Item.FindControl("hidZoneInstanceId");
                HtmlInputHidden hidZoneType = (HtmlInputHidden)e.Item.FindControl("hidZoneType");
                TextBox txtSpinDownDelay = (TextBox)e.Item.FindControl("txtSpinDownDelay");
                HtmlInputHidden hidOldSpinDownDelay = (HtmlInputHidden)e.Item.FindControl("hidOldSpinDownDelay");
                Button btnUpdateSpinDownDelay = (Button)e.Item.FindControl("btnUpdateSpinDownDelay");
                LinkButton lbtnScriptZoneServerId = (LinkButton)e.Item.FindControl("lbtnScriptZoneServerId");

                //set the data for each field
                WOK3DPlace zoneInfo = (WOK3DPlace)e.Item.DataItem;
                lbChannelZoneName.Text = zoneInfo.Name;

                //current tied server
                GameServer tiedServer = null;
                if (zoneInfo.TiedToServer != "NOT_TIED" && zoneInfo.ServerId != 0)
                {
                    tiedServer = GetGameFacade().GetServer(zoneInfo.ServerId);
                }

                //<asp:ListItem Text="TIED -> <server>:<port> (Current)" Value=""></asp:ListItem>
                //<asp:ListItem Text="NOT_TIED" Value="NOT_TIED"></asp:ListItem>
                //<asp:ListItem Text="TIED -> <server>:<port>" Value="TIED"></asp:ListItem>
                //<asp:ListItem Text="PENDING_TIED -> <server>:<port>" Value="PENDING_TIED"></asp:ListItem>
                //<asp:ListItem Text="PERMANENTLY_TIED -> <server>:<port>" Value="PERMANENTLY_TIED"></asp:ListItem>
                ddlTiedStatus.Items.Clear();
                if (tiedServer != null) {
                    // Currently tied to a server
                    ddlTiedStatus.Items.Add(new ListItem(zoneInfo.TiedToServer + " -> " + tiedServer.ServerName + ":" + tiedServer.Port.ToString() + " (Current)", ""));
                    ddlTiedStatus.SelectedValue = "";
                }
                // Options for switching to a new server or untie from current server
                ddlTiedStatus.Items.Add(new ListItem("NOT_TIED", "NOT_TIED"));

                if (SELECTED_WOK_SERVER_ID != -1)
                {
                    GameServer selectedServer = GetGameFacade().GetServer(SELECTED_WOK_SERVER_ID);
                    Debug.Assert(selectedServer != null);
                    if (selectedServer != null)
                    {
                        ddlTiedStatus.Items.Add(new ListItem("TIED -> " + selectedServer.ServerName + ":" + selectedServer.Port.ToString(), "NOT_TIED"));
                        ddlTiedStatus.Items.Add(new ListItem("PENDING_TIED -> " + selectedServer.ServerName + ":" + selectedServer.Port.ToString(), "PENDING_TIED"));
                        ddlTiedStatus.Items.Add(new ListItem("PERMANENTLY_TIED -> " + selectedServer.ServerName + ":" + selectedServer.Port.ToString(), "PERMANENTLY_TIED"));
                        if (tiedServer == null)
                        {
                            ddlTiedStatus.SelectedValue = zoneInfo.ServerId == selectedServer.ServerId ? zoneInfo.TiedToServer : "NOT_TIED";
                        }
                    }
                }

                hidChannelZoneId.Value = zoneInfo.ChannelZoneId.ToString();
                hidZoneInstanceId.Value = zoneInfo.ZoneInstanceId.ToString();
                hidZoneType.Value = zoneInfo.ZoneType.ToString();
                int spinDownDelayMinutes = zoneInfo.SpinDownDelayMinutes;
                if (spinDownDelayMinutes >= 0)
                {
                    txtSpinDownDelay.Text = spinDownDelayMinutes.ToString();
                }
                hidOldSpinDownDelay.Value = spinDownDelayMinutes.ToString();
                lbtnScriptZoneServerId.Text = zoneInfo.ScriptZoneServerId.ToString();

                // Disable spin-down delay update button for permanent and arena zones
                btnUpdateSpinDownDelay.Enabled = zoneInfo.ZoneType != (int)WOK3DPlace.eZoneType.PERMANENT && zoneInfo.ZoneType != (int)WOK3DPlace.eZoneType.ARENA;

                // Disable link button if script server ID is 0
                lbtnScriptZoneServerId.Enabled = zoneInfo.ScriptZoneServerId != 0;
            }
        }

        protected void txtChannelZoneName_TextChanged(object sender, EventArgs e)
        {
            // Use a blank TextChanged handler to force TextBox to save viewstate
            // for "Text" property. For detail, see TextBox::SaveTextViewState at 
            // http://referencesource.microsoft.com/#System.Web/UI/WebControls/TextBox.cs,c0dfe95cd38a62bc,references

            // Also txtChannelZoneName.Text must be explicitly assigned on GET 
            // (currently assigned in ZoneSettings.aspx). Otherwise textbox's initial 
            // view state will not contains "Text" property which will cause the first 
            // post value of txtChannelZoneName not being parsed until after Page_Load.

            // We only need a blank function. Do nothing for now
        }

        #endregion


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.rpt_ChannelZones.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_ChannelZones_ItemDataBound);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
            pgBottom.PageChanged += new PageChangeEventHandler(pg_PageChange);
        }
        #endregion
    }
}
