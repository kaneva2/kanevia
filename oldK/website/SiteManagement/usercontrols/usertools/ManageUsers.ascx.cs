using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;

using log4net;
using MagicAjax;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;
using System.Text;
using Kaneva;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class ManageUsers : BaseUserControl
    {
        #region Declarations

        protected Pager pgTop, pgBottom, ivTop, ivBottom;
        protected DropDownList ddl_KanevaRoles;
        private int selectedUserId = 0;
        private int currentInvitationRoleId = 0;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.SITE_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                if (!IsPostBack)
                {

                    // Set the page on the paging control for team members
                    pgTop.CurrentPageNumber = 1;
                    pgBottom.CurrentPageNumber = 1;

                    //get all kaneva roles and store for later binding
                    GetKanevaRoles();

                    //bind datagrids
                    BindAuthUserData(-1);
                }

                //clear message
                messages.Text = "";
            }
            else
            {
                RedirectToHomePage();
            }
        }

        #region Attributes

        protected List<SiteRole> KANEVA_ROLES
        {
            get
            {

                if (ViewState["kanevaRoles"] == null)
                {
                    ViewState["kanevaRoles"] = new List<SiteRole>();
                }

                return (List<SiteRole>)ViewState["kanevaRoles"];
            }
            set
            {
                ViewState["kanevaRoles"] = value;
            }
        }


        /// <summary>
        /// stores the list of company developers
        /// </summary>
        /// <returns>int</returns>
        private List<User> KANEVA_AUTH_USERS
        {
            get
            {
                if (ViewState["kanevaAuthUsers"] == null)
                {
                    return new List<User>();
                }
                return (List<User>)ViewState["kanevaAuthUsers"];
            }
            set
            {
                ViewState["kanevaAuthUsers"] = value;
            }
        }

        /// <summary>
        /// stores the current company administrators
        /// </summary>
        /// <returns>int</returns>
        private List<User> KANEVA_ADMINISTRATORS
        {
            get
            {
                if (ViewState["kanevaAdministrators"] == null)
                {
                    return null;
                }
                return (List<User>)ViewState["kanevaAdministrators"];
            }
            set
            {
                ViewState["kanevaAdministrators"] = value;
            }
        }


        #endregion

        #region Helper Functions

        //delegate function used in LIst FindAll search
        private bool IsAuthUser(User usr)
        {
            return usr.UserId == selectedUserId;
        }

        //delegate function used in LIst FindAll search
        private bool IsIndicatedRole(SiteRole role)
        {
            return role.SiteRoleId == currentInvitationRoleId;
        }

        //function to check for last administrator
        private bool IsLastAdministrator(int userIdToCheck)
        {
            bool lastAdministrator = false;

            //if there is only one admin make sure they are not the one being changed if so reject the change
            if (KANEVA_ADMINISTRATORS.Count == 1)
            {
                int AdminId = ((User)KANEVA_ADMINISTRATORS[0]).UserId;
                if (AdminId == userIdToCheck)
                {
                    messages.ForeColor = System.Drawing.Color.Crimson;
                    messages.Text = "Operation Not Allowed: You must always have atleast one company administrator.";
                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
                    lastAdministrator = true;
                }
            }
            if (KANEVA_ADMINISTRATORS.Count == 0)
            {
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Urgent! You must always have atleast one company administrator. Please designate one now!";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

            return lastAdministrator;
        }

        private void GetCompanyAdmins()
        {
            KANEVA_ADMINISTRATORS = WebCache.GetCurrentSiteAdmins(GetKanevaCompanyID());
        }

        #endregion

        #region Main Functionality

        private void BindAuthUserData(int pageNumber)
        {
            try
            {
                //set initial values
                PagedList<User> pdl = new PagedList<User>();

                //get default pagesize
                int itemsPerPage = 14;

                //added to skip initial load
                if (pageNumber > 0)
                {
                    //retrieve the list of company administrators 
                    GetCompanyAdmins();

                    //retreive the authorized users list
                    pdl = GetSiteManagementFacade().GetAuthorizedUsersList(Server.HtmlEncode(txtUsername.Text), Server.HtmlEncode(txtEmail.Text), Server.HtmlEncode(txtFirstName.Text), Server.HtmlEncode(txtLastName.Text), 0, "", "name", pageNumber, itemsPerPage);

                    //store users
                    KANEVA_AUTH_USERS = (List<User>)pdl.List;
                }

                //bind the result set to the repeater control
                rpt_MyTeam.DataSource = pdl;
                rpt_MyTeam.DataBind();

                //set the paging controls
                pgTop.NumberOfPages = Math.Ceiling((double)pdl.TotalCount / itemsPerPage).ToString();
                pgTop.DrawControl();

                pgBottom.NumberOfPages = Math.Ceiling((double)pdl.TotalCount / itemsPerPage).ToString();
                pgBottom.DrawControl();

                // Tset the results display message
                lblSearch2.Text = lblSearch.Text = GetResultsText(pdl.TotalCount, pageNumber, itemsPerPage, pdl.Count);
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during Team member load.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error: Sorry we were unable to load your team members.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

        }

        private void GetKanevaRoles()
        {
            try
            {
                KANEVA_ROLES = (List<SiteRole>)(GetSiteSecurityFacade().GetCompanyRolesList(0));
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during retrieval of available roles.", ex);
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Search Event Handler
        /// </summary>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindAuthUserData(1);
        }

        protected void KanevaRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //first get a reference to the repeater item the drop down is in
                RepeaterItem item = (RepeaterItem)((DropDownList)sender).Parent;

                //next get the user id of the developer in that repeater item
                HtmlInputHidden hidUserId = (HtmlInputHidden)item.FindControl("hidUserId");
                selectedUserId = Convert.ToInt32(hidUserId.Value);

                //get the Drop down causing the change
                DropDownList activeDropDown = (DropDownList)item.FindControl("ddl_KanevaRoles");

                //if there is only one admin make sure they are not the one being changed if so reject the change
                if (IsLastAdministrator(selectedUserId))
                {
                    //reset drop down to original value
                    try
                    {
                        activeDropDown.SelectedValue = (KANEVA_AUTH_USERS[item.ItemIndex]).Role.ToString();
                    }
                    catch (Exception)
                    { }

                    return;
                }

                //if no constraints find the Game Developer
                User modifiedUser = KANEVA_AUTH_USERS.Find(IsAuthUser);
                modifiedUser.Role = Convert.ToInt32(activeDropDown.SelectedValue);

                //save change to the Database
                int result = GetUserFacade().UpdateUserSecurity(modifiedUser);
                selectedUserId = 0;

                //check for and communicate if insert failed
                if (result < 1)
                {
                    throw new Exception("Update failed!");
                }

                BindAuthUserData(pgTop.CurrentPageNumber);
                messages.ForeColor = System.Drawing.Color.Green;
                messages.Text = modifiedUser.Username + " has been changed to the role of " + activeDropDown.SelectedItem.Text;
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");

            }
            catch (Exception ex)
            {
                m_logger.Error("Error during Team member edit.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Editing Error: Sorry we were unable to save your team member changes.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            pgTop.CurrentPageNumber = e.PageNumber;
            pgTop.DrawControl();
            pgBottom.CurrentPageNumber = e.PageNumber;
            pgBottom.DrawControl();
            BindAuthUserData(e.PageNumber);
        }

        private void rpt_MyTeam_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //find all the controls in the repeater
                HtmlAnchor lbUserName = (HtmlAnchor)e.Item.FindControl("lb_UserName");
                Label lblRealName = (Label)e.Item.FindControl("lbl_RealName");
                DropDownList ddlRolesList = (DropDownList)e.Item.FindControl("ddl_KanevaRoles");
                HtmlInputHidden hidUserId = (HtmlInputHidden)e.Item.FindControl("hidUserId");

                //set the pull down data
                ddlRolesList.DataSource = KANEVA_ROLES;
                ddlRolesList.DataTextField = SiteRole.ROLE_NAME;
                ddlRolesList.DataValueField = SiteRole.ROLE_ID;
                ddlRolesList.DataBind();

                //set the data for each field
                string firstName = ((User)e.Item.DataItem).FirstName;
                string lastName = ((User)e.Item.DataItem).LastName;
                lblRealName.Text = firstName + " " + lastName;
                ddlRolesList.SelectedValue = ((User)e.Item.DataItem).Role.ToString();
                //lbUserName.HRef = GetPersonalChannelUrl(((GameDeveloper)e.Item.DataItem).UserInfo.NameNoSpaces);
                lbUserName.InnerText = ((User)e.Item.DataItem).Username;
                hidUserId.Value = ((User)e.Item.DataItem).UserId.ToString();
            }
        }

        #endregion


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.rpt_MyTeam.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_MyTeam_ItemDataBound);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
            pgBottom.PageChanged += new PageChangeEventHandler(pg_PageChange);
        }
        #endregion
    }
}
