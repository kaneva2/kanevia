<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="MemberFame.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.MemberFame" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>

<script type="text/javascript" src="../../jscript/yahoo/yahoo-min.js"></script>  
<script type="text/javascript" src="../../jscript/yahoo/event-min.js" ></script>   
<script type="text/javascript" src="../../jscript/yahoo/dom-min.js" ></script>   
<script type="text/javascript" src="../../jscript/yahoo/calendar-min.js"></script>

<script type="text/javascript" language="javascript" src="../../jscript/prototype.js"></script>

<link href="../../css/yahoo/calendar.css" type="text/css" rel="stylesheet" />   


<style >
	#cal1Container { display:none; position:absolute; z-index:100;}
</style>
 
 
<script type="text/javascript"><!--
 function specialCharReplace()
 {
   //replace for item text, display text, and description;
    var itemName = document.getElementById("tbx_ItemName");
    itemName.value = itemName.value.replace(/amp;/g,"")

    var itemDisplayName = document.getElementById("tbx_DisplayName");
    itemDisplayName.value = itemDisplayName.value.replace(/amp;/g,"")

    var itemDescription = document.getElementById("tbx_Description");
    itemDescription.value = itemDescription.value.replace(/amp;/g,"")

 }
 
YAHOO.namespace("example.calendar");

    
	function handleAddedDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate1 = document.getElementById("txtItemAddedDate");
		txtDate1.value = month + "-" + day + "-" + year;
		YAHOO.example.calendar.cal1.hide();
	}
	
	function initAddedDate() {
		// Admin Calendar
		YAHOO.example.calendar.cal1 = new YAHOO.widget.Calendar ("cal1", "cal1Container", { iframe:true, zIndex:200, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal1.render();
		
		// Listener to show Admin Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgItemAddDate", "click", YAHOO.example.calendar.cal1.show, YAHOO.example.calendar.cal1, true);   
		YAHOO.example.calendar.cal1.selectEvent.subscribe(handleAddedDate, YAHOO.example.calendar.cal1, true);
	}
	
//--> </script>  
<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    <ajax:ajaxpanel id="ajMessage" runat="server">
        <asp:label style="position:relative; font-size:18px; padding: 10px 0 10px 0" runat="server" id="messages" visible="true" />

             <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="996px" style="background-color:#ffffff">
                <tr>
                    <td align="center">
			            <span style="height:30px; font-size:28px; font-weight:bold">Member Fame</span><br />
			            <ajax:ajaxpanel id="ajpError" runat="server">
			            <asp:label runat="server" id="lblErrMessage" class="errBox black" style="width:60%;margin-top:20px;text-align:center;"></asp:label></ajax:ajaxpanel>
		            </td>
                </tr>
	            <tr>
		            <td style="height:20px"></td>
                </tr>
                <tr>
                     <td align="center">
                        
                        <div width="95%" style="padding: 6px 20px 6px 0px; font-size:10pt; text-align:right;">
				            <b>Search</b> &nbsp;<asp:TextBox runat="server" id="txtSearch"></asp:TextBox>&nbsp;<asp:Button id="btnSearch" onclick="btnSearch_Click" causesvalidation="false" runat="server" text="Search"></asp:Button>
                        </div>
            	  
			            <ajax:ajaxpanel id="ajpUserFame" runat="server">  
            		 
				            <asp:gridview id="dgUserFame" runat="server"
					            onsorting="dgUserFame_Sorting" 
					            OnRowEditing="dgUserFame_RowEditing" 
					            autogeneratecolumns="False" 
					            width="95%" 
					            AllowSorting="True" border="0" cellpadding="2" 
					            PageSize="20" AllowPaging="False" Align="center" 
					            bordercolor="DarkGray" borderstyle="solid" borderwidth="1px">
            					
					            <HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
					            <RowStyle BackColor="White"></RowStyle>
					            <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
					            <FooterStyle BackColor="#FFffff"></FooterStyle>
					            <Columns>
						            <asp:BoundField HeaderText="User Id" DataField="UserId" ReadOnly="True" SortExpression="user_id" ConvertEmptyStringToNull="False" itemstyle-horizontalalign="center" itemstyle-width="120px" headerstyle-horizontalalign="center"></asp:BoundField>
						            <asp:BoundField HeaderText="User Name" ItemStyle-HorizontalAlign="Left" DataField="UserName" SortExpression="username" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
						            <asp:BoundField HeaderText="Fame Points" DataField="Points" SortExpression="points" ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-horizontalalign="center" headerstyle-horizontalalign="center"></asp:BoundField>
						            <asp:BoundField HeaderText="Fame Level" DataField="LevelId" SortExpression="level_id" ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-horizontalalign="center" headerstyle-horizontalalign="center"></asp:BoundField>
						            <asp:CommandField CausesValidation="False" ShowEditButton="True"></asp:CommandField>        
					            </Columns>
				            </asp:gridview>
            		 
				            <div style="width:95%;text-align:right;margin:10px 6px 0 0;"><kaneva:pager runat="server" isajaxmode="True" onpagechanged="pg_UserFame_PageChange" id="pgUserFame" maxpagestodisplay="5" shownextprevlabels="true" /></div>
            				
			            </ajax:ajaxpanel> 
                     
			            <asp:Label id="lblNoUserFameItems" visible="false" runat="server">
				            <span style="font-size:16px;color:Red;font-weight:bold;margin:30px 0;width:95%;text-align:center;">No Members Were Found.</span>
			            </asp:Label>


			            <br /><br />
                     
                     </td>
                </tr>
	            <tr>
		            <td align="center">
             
			            <ajax:ajaxpanel id="ajpUserFameDetails" runat="server">
             
				            <div id="divUserFameDetails" style="display:none;background-color:White;width:95%;margin-bottom:20px;" runat="server" >	 

					            <div id="divUsername" runat="server" style="width:100%;text-align:left;font-size:20px;font-weight:bold;margin-bottom:10px;"></div>
            					
					            <div id="divPacketHistory">
            	   
						            <span style="text-align:left;font-weight:bold;font-size:12px;width:100%;margin-bottom:10px;">Packets History:</span>
            		
						            <asp:gridview id="dgPacketHistory" runat="server" 
							            autogeneratecolumns="False" 
							            width="100%" 
							            AllowSorting="false" border="0" cellpadding="2" 
							            AllowPaging="False"
							            bordercolor="DarkGray" borderstyle="solid" borderwidth="1px">
            							
							            <RowStyle BackColor="White"></RowStyle>
							            <HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
							            <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
							            <FooterStyle BackColor="#FFffff"></FooterStyle>
							            <Columns>
            								
								            <asp:BoundField HeaderText="Packet ID" DataField="PacketId" ReadOnly="True" ConvertEmptyStringToNull="False" 
									            itemstyle-horizontalalign="center" headerstyle-horizontalalign="center"  itemstyle-width="90px"></asp:BoundField>
            								
								            <asp:BoundField HeaderText="Packet Name" DataField="PacketName" SortExpression="name" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
            								
								            <asp:BoundField HeaderText="Points" DataField="PointsAwarded" SortExpression="points_rewarded" 
									            ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-horizontalalign="center" itemstyle-width="80px" 
									            headerstyle-horizontalalign="center"></asp:BoundField>
            								
								            <asp:BoundField HeaderText="Rewards" DataField="RewardsAwarded" SortExpression="rewards_rewarded" 
									            ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-horizontalalign="center" itemstyle-width="130px"
									            headerstyle-horizontalalign="center"></asp:BoundField>
            								
								            <asp:BoundField HeaderText="Date Rewarded" DataField="DateRewarded" SortExpression="date_rewarded" 
									            ReadOnly="True" ConvertEmptyStringToNull="False"></asp:BoundField>
            							
							            </Columns>
						            </asp:gridview>
                
						            <div style="width:95%;text-align:right;margin:10px 6px 0 0;"><kaneva:pager runat="server" isajaxmode="True" onpagechanged="pg_PacketHistory_PageChange" id="pgPacketHistory" maxpagestodisplay="5" shownextprevlabels="true" /></div>
            					
					            </div>

					            <br />
                
					            <div id="divLevelHistory">

						            <span style="text-align:left;font-weight:bold;font-size:12px;width:100%;margin-bottom:10px;">Level History:</span>
            						
						            <asp:gridview id="dgLevelHistory" runat="server" 
							            onRowDataBound="dgLevelHistory_RowDataBound"
							            autogeneratecolumns="False" 
							            width="100%" 
							            AllowSorting="True" border="0" cellpadding="2" 
							            AllowPaging="False"
							            bordercolor="DarkGray" borderstyle="solid" borderwidth="1px">
            							
							            <RowStyle BackColor="White" horizontalalign="center"></RowStyle>
							            <HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="center"></HeaderStyle>
							            <AlternatingRowStyle BackColor="Gainsboro" horizontalalign="center"></AlternatingRowStyle>
							            <FooterStyle BackColor="#FFffff"></FooterStyle>
							            <Columns>
								            <asp:BoundField HeaderText="Level ID" DataField="LevelId" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
								            <asp:BoundField HeaderText="Level Number" DataField="LevelNumber" SortExpression="level_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
								            <asp:BoundField HeaderText="Time To Reach Level (days.hrs:min:sec)" DataField="SecondsToLevel" SortExpression="seconds_to_level" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
								            <asp:BoundField HeaderText="Date Reached" DataField="LevelDate" SortExpression="level_date" ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-horizontalalign="left" headerstyle-horizontalalign="Left"></asp:BoundField>
							            </Columns>
						            </asp:gridview>
            			    
						            <div style="width:95%;text-align:right;margin:10px 6px 0 0;"><kaneva:pager runat="server" isajaxmode="True" onpagechanged="pg_LevelHistory_PageChange" id="pgLevelHistory" maxpagestodisplay="5" shownextprevlabels="true" /></div>
            					
					            </div>
                
				            </div>

			            </ajax:ajaxpanel>

		            </td>
	            </tr> 
            </table>

    </ajax:ajaxpanel>
</asp:Panel>    