﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.IO;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class DeveloperAuthorizations : BaseUserControl
    {
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly DateTime m_earliestDate = new DateTime(2016, 1, 1);
        private static readonly uint m_dropBoxLifeSeconds = 30 * 60; // developer.dat dropbox entry expires in 30 minutes

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        private void BindData()
        {
            try
            {
                //set initial values
                IList<DevAuthSystemConfig> allItems = new List<DevAuthSystemConfig>();

                if (txtComputerNameSearch.Text != "")
                {
                    // Raw records
                    IList<UserSystemConfiguration> result = GetUserFacade().GetUserSystemConfiguationsBySystemName(txtComputerNameSearch.Text);

                    // Data item
                    var query = from row in result
                                where row.GpuName.Length > 0 && row.DateLastUsed >= m_earliestDate
                                orderby row.SystemName, row.CpuCount, row.CpuVersion, row.CpuMhz, row.SystemMemory, row.GpuName
                                select new DevAuthSystemConfig(row);

                    allItems = query.Distinct().ToList();
                }

                //cache results
                SEARCH_RESULTS = allItems;

                //bind the result set to the repeater control
                rpt_SystemConfigs.DataSource = allItems;
                rpt_SystemConfigs.DataBind();

                // Results display message
                lblSearch2.Text = lblSearch.Text = allItems.Count.ToString() + " results";
                if (SYSTEM_NAME != null)
                {
                    lbtnDownload.Text = "Download developer.dat for [" + SYSTEM_NAME + "]";
                }
                else
                {
                    lbtnDownload.Text = "";
                }
            }
            catch (Exception ex)
            {
                m_logger.Error("Error searching user system configurations.", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error: Sorry we were unable to load the results you're searching for.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

        }

        private void rpt_SystemConfigs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //find all the controls in the repeater
                Label lbCPUCount = (Label)e.Item.FindControl("lbCPUCount");
                Label lbCPUMhz = (Label)e.Item.FindControl("lbCPUMhz");
                Label lbSysMemGB = (Label)e.Item.FindControl("lbSysMemGB");
                Label lbGPU = (Label)e.Item.FindControl("lbGPU");

                //set the data for each field
                DevAuthSystemConfig item = (DevAuthSystemConfig)e.Item.DataItem;

                lbCPUCount.Text = item.CPUCount.ToString();
                lbCPUMhz.Text = item.CPUMHz.ToString();
                double systemMemory = Convert.ToDouble(item.SystemMemory);
                lbSysMemGB.Text = (systemMemory / 1024.0 / 1024.0 / 1024.0).ToString();
                lbGPU.Text = item.GPU;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            // Search user_system_configurations by computer name
            BindData();
        }

        protected void lbtnDownload_Click(object sender, EventArgs e)
        {
            if (SEARCH_RESULTS.Count == 0 || SYSTEM_NAME == null)
            {
                //TODO: report error
                return;
            }

            // Cleanup expired dropbox records
            GetUserFacade().CleanupDeveloperAuthorizationDropBox();

            // Generate developer.dat
            DevAuthEncrypter devDat = new DevAuthEncrypter(SEARCH_RESULTS);
            Response.ContentType = "application/binary";
            string uuid = GetUserFacade().InsertDeveloperAuthorizationDropBox(GetUserId(), SYSTEM_NAME, devDat.AESData, m_dropBoxLifeSeconds);
            if (uuid != null)
            {
                Response.Redirect("~/tools/DownloadDeveloperAuthorization.aspx?systemName=" + HttpUtility.UrlEncode(SYSTEM_NAME) + "&id=" + HttpUtility.UrlEncode(uuid));
            }
            else
            {

            }
        }
        
        protected void btnFixSysMem_Click(object sender, EventArgs e)
        {
            //TODO
        }

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.rpt_SystemConfigs.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_SystemConfigs_ItemDataBound);
            base.OnInit(e);
        }

        #region Attributes

        private IList<DevAuthSystemConfig> SEARCH_RESULTS
        {
            get
            {
                if (ViewState["devAuthResults"] == null)
                {
                    return new List<DevAuthSystemConfig>();
                }
                return (IList<DevAuthSystemConfig>)ViewState["devAuthResults"];
            }
            set
            {
                ViewState["devAuthResults"] = value;
            }
        }

        private string SYSTEM_NAME
        {
            get {
                if (SEARCH_RESULTS.Count > 0)
                {
                    return SEARCH_RESULTS.First().SystemName;
                }
                return null;
            }
        }

        #endregion

        #region ByteStreamEncoder
        private class ByteStringEncoder
        {
            public ByteStringEncoder()
            {
                byteString = new List<byte>();
            }

            public void EncodeUInt(uint num)
            {
                EncodeUInt(num, 4);
            }

            public void EncodeUInt(uint num, int width)
            {
                byteString.Add(Convert.ToByte(num & 0xff));
                if (width == 1) return;
                byteString.Add(Convert.ToByte((num >> 8) & 0xff));
                if (width == 2) return;
                byteString.Add(Convert.ToByte((num >> 16) & 0xff));
                if (width == 3) return;
                byteString.Add(Convert.ToByte((num >> 24) & 0xff));
            }

            public void EncodeString(string str, int width)
            {
                char[] chars = str.ToCharArray();
                for (int i = 0; i < str.Length && i < width; i++)
                {
                    byteString.Add(Convert.ToByte(chars[i]));
                }
                for (int i = str.Length; i < width; i++)
                {
                    byteString.Add(0);
                }
            }

            public void EncodeBytes(byte[] byteArray)
            {
                byteString.AddRange(byteArray);
            }

            public byte[] GetBytes()
            {
                return byteString.ToArray();
            }

            private List<byte> byteString;
        }
        #endregion

        #region DevAuthSystemConfig
        [Serializable]
        private struct DevAuthSystemConfig
        {
            public DevAuthSystemConfig(UserSystemConfiguration config)
            {
                SystemName = config.SystemName;
                CPUCount = config.CpuCount.GetValueOrDefault();
                CPUVersion = config.CpuVersion;
                CPUMHz = config.CpuMhz.GetValueOrDefault();
                SystemMemory = config.SystemMemory.GetValueOrDefault();
                GPU = config.GpuName;
            }


            public readonly string SystemName;
            public readonly uint CPUCount;
            public readonly string CPUVersion;
            public readonly uint CPUMHz;
            public readonly ulong SystemMemory;
            public readonly string GPU;
        }
        #endregion

        #region DevAuthEncrypter
        private struct DevAuthEncrypter
        {
            public DevAuthEncrypter(IList<DevAuthSystemConfig> configs)
            {
                ByteStringEncoder encoder = new ByteStringEncoder();
                // Signature (4 bytes)
                encoder.EncodeString("DEV@", 4);
                // Version (2 bytes)
                encoder.EncodeUInt(Version, 2);
                // Hash count (2 bytes)
                encoder.EncodeUInt(Convert.ToUInt32(configs.Count), 2);
                // Privileges
                encoder.EncodeUInt(Scripting);
                encoder.EncodeUInt(REPLConsole);
                for (uint i = 0; i < 8; i++)
                {
                    encoder.EncodeUInt(0);
                }
                // Hashes (with order)
                List<byte[]> hashes = new List<byte[]>();
                foreach (DevAuthSystemConfig config in configs)
                {
                    hashes.Add(GetConfigHash(config));
                }
                foreach (byte[] hash in hashes.OrderBy(hash => BytesToHexString(hash)))
                {
                    encoder.EncodeBytes(hash);
                }

                RawData = encoder.GetBytes();

                using (Aes AES = Aes.Create())
                {
                    AES.Key = AESKey;
                    AES.IV = AESIV;

                    // Cryptor
                    ICryptoTransform cryptor = AES.CreateEncryptor(AES.Key, AES.IV);

                    // Output stream
                    using (MemoryStream ms = new MemoryStream())
                    {
                        // Encryption stream
                        using (CryptoStream cs = new CryptoStream(ms, cryptor, CryptoStreamMode.Write))
                        {
                            cs.Write(RawData, 0, RawData.Length);
                            AESData = ms.ToArray();
                        }
                    }
                }

            }

            private static byte[] GetConfigHash(DevAuthSystemConfig config)
            {
                ByteStringEncoder encoder = new ByteStringEncoder();
                encoder.EncodeUInt(config.CPUCount);
                encoder.EncodeString(config.CPUVersion, 32);
                encoder.EncodeUInt(config.CPUMHz);
                encoder.EncodeUInt(Convert.ToUInt32(config.SystemMemory & 0xffffffff));
                encoder.EncodeUInt(Convert.ToUInt32(config.SystemMemory >> 32));
                encoder.EncodeString(config.SystemName, 128);
                encoder.EncodeString(config.GPU, 128);

                using (MD5 md5Hash = MD5.Create())
                {
                    return md5Hash.ComputeHash(encoder.GetBytes());
                }
            }

            private static string BytesToHexString(byte[] bytes)
            {
                string hexString = "";
                foreach (byte b in bytes)
                {
                    hexString += b.ToString("X2");
                }
                return hexString;
            }

            public readonly byte[] RawData;
            public readonly byte[] AESData;

            private const uint Version = 0;
            private const uint Scripting = 1251;
            private const uint REPLConsole = 1252;
            private static readonly byte[] AESKey = new byte[] { 0xcc, 0x9d, 0x30, 0x25, 0x4f, 0xc3, 0x88, 0x5e, 0x71, 0x8e, 0x92, 0xbc, 0x85, 0x35, 0xbf, 0x93 };
            private static readonly byte[] AESIV = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        }
        #endregion
    }
}