using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class GameTransactions : BaseUserControl
    {
        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
        private bool isAdministrator = false;
        private int pageNum = 1;
        private int pageSize = 10;
        private uint totalPrice = 0;
        private int selectedGameId = 0;
        protected HtmlContainerControl dialogAvailable, dialogCredits, dialogTotalAfterRedeem, transContainer;
        protected HtmlTable tblRedeem;
        protected HtmlAnchor btnRedeem;

        #endregion Declerations
        
        #region Page Load

        protected void Page_Load (object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess ((int) SitePrivilege.ePRIVILEGE.STAR_ADMIN))
                {
                    case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        isAdministrator = true;
                        break;
                    case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    default:
                        break;
                }

                if (((Session["SelectedGameId"] != null) && Convert.ToInt32 (Session["SelectedGameId"]) > 0) ||
                    ((Session["AppManagementSelectedCommunityId"] != null) && Convert.ToInt32 (Session["AppManagementSelectedCommunityId"]) > 0))
                {
                    // Load the current user's data the first time
                    SelectedCommunityId = Convert.ToInt32 (Session["AppManagementSelectedCommunityId"]);

                    SelectedGameId = Session["SelectedGameId"] != null ? Convert.ToInt32 (Session["SelectedGameId"]) : SelectedGameId;

                    //get the company id the game is associated with
                    bool canEdit = GetCommunityFacade ().IsCommunityOwner (SelectedCommunityId, GetUserId());

                    if (!canEdit && !isAdministrator)
                    {
                        RedirectToHomePage ();
                    }
                    else
                    {
                        //community id check is to allow kaneva website to make use of this usercontrol
                        if (!IsPostBack || (Request["communityId"] != null))
                        {
                            // Log the CSR activity
                            this.GetSiteManagementFacade ().InsertCSRLog (GetUserId (), "CSR - Viewing game transaction information page", 0, SelectedGameId);

                            //configure page
                            ConfigurePageForUse ();
                        }
                    }
                }
                else
                {
                    //community id check is to tell it to redirect to search page (for KWAS only)
                    if (Request["communityId"] == null)
                    {
                        //store this pages URL for return
                        Session["CallingPage"] = Request.Url.ToString ();
                        Response.Redirect (ResolveUrl ("~/Default.aspx?ws=" + GetCurrentSiteID ((int) SiteManagementCommonFunctions.SUPPORTED_WEBSITES.DEVELOPER) + "&mn=" + GetGameSearchParentMenuID () + "&sn=" + GetGameSearchControlID ()));
                    }
                }
            }
            else
            {
                RedirectToHomePage ();
            }
        }

        #endregion Page Load

        #region Functions

        private void ConfigurePageForUse ()
        {
            ShowTotals ();
            BindTransactions (false);
        }

        public void BindTransactions (bool redeem)
        {
            //get the games servers for the game
            try
            {
                //get the premium items from the database
                PagedList<PurchaseTransactionPremiumItem> trans = GetTransactionFacade ().GetPurchaseTransactionsByGame (SelectedCommunityId, "", OrderBy, PageNum, PageSize);

                if (trans.TotalCount > 0)
                {
                    pgTop.NumberOfPages = Math.Ceiling ((double) trans.TotalCount / pageSize).ToString ();
                    pgTop.DrawControl ();

                    rptTransactions.DataSource = trans;
                    rptTransactions.DataBind ();

                    // The results
                    lblSearch.Text = GetResultsText (trans.TotalCount, PageNum, pageSize, trans.Count);
                }
                else
                {
                    transContainer.Visible = false;
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error Loading premium items.", ex);
            }
        }

        private PremiumItemRedeemAmounts GetUserTransactionBalances ()
        {
            return GetTransactionFacade ().GetTotalTransactionCreditAmountsByGame (SelectedCommunityId, KanevaGlobals.PremiumItemDaysPendingBeforeRedeem);
        }

        private void ShowTotals ()
        {
            PremiumItemRedeemAmounts amts = GetUserTransactionBalances ();

            divTotalCredits.InnerText = amts.Total == 0 ? "0" : GetUserRedemptionAmount (amts.Total).ToString ("#,###");
            divTotalCash.InnerText = amts.Available == 0 ? "0" : GetUserRedemptionAmount (amts.Available).ToString ("#,###");

            ConfigureRedeemDialog (amts);
        }

        public int GetUserRedemptionAmount (int amt)
        {
            try
            {
                return Convert.ToInt32 (amt * KanevaGlobals.PremiumItemUserRedemptionPercentage);        
            }
            catch
            {
                return 0;
            }
        }

        private void ConfigureRedeemDialog (PremiumItemRedeemAmounts amts)
        {
            int available = GetUserRedemptionAmount (amts.Available);
            int credits = (int)GetCurrentUser().Balances.Credits;

            if (available > 0)
            {
                dialogAvailable.InnerText = available.ToString ("#,###");
                dialogCredits.InnerText = credits.ToString ("#,###");
                dialogTotalAfterRedeem.InnerText = (available + credits).ToString ("#,###");
            }
            else
            {
                btnRedeem.Attributes.Add ("class", "btnmedium grey disable");
                btnRedeem.Disabled = true;
            }
        }

        #endregion Functions

        #region Event Handlers

        public void rptTransactions_ItemDataBound (object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlTableCell status = (HtmlTableCell) e.Item.FindControl ("status");

                DateTime purchaseDate = Convert.ToDateTime (DataBinder.Eval (e.Item.DataItem, "purchaseDate"));
                PurchaseTransactionPremiumItem.RedeemStatus redeem = (PurchaseTransactionPremiumItem.RedeemStatus)DataBinder.Eval (e.Item.DataItem, "RedemptionStatus");

                if (redeem == PurchaseTransactionPremiumItem.RedeemStatus.Pending)
                {   //Pending
                    status.InnerHtml = "<span>Pending</span>";
                }
                else if (redeem == PurchaseTransactionPremiumItem.RedeemStatus.Available)
                {   //Available
                    status.InnerHtml = "<span class=\"available\">Available</span>";
                }
                else
                {  // Redeemed
                    status.InnerHtml = "<span class=\"redeemed\">Redeemed</span>";   
                }
            }            
        }

        /// <summary>
        /// Page Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pgTop_PageChange (object sender, PageChangeEventArgs e)
        {
            PageNum = e.PageNumber;
            BindTransactions (false);
        }

        protected void btnDoRedeem_Click (object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                // Verify user has credits to redeem            
                PremiumItemRedeemAmounts amts = GetUserTransactionBalances();
                if (amts.Available > 0)
                {
                    int successfulPayment = -1;

                    // Redeem credits                     
                    try
                    {
                        successfulPayment = GetUserFacade ().AdjustUserBalance (GetUserId (),
                            Constants.CURR_CREDITS, amts.Available, Constants.CASH_TT_PREMIUM_ITEM_CREDIT_REDEEM);
                    }
                    catch (Exception)
                    { }

                    // If credits were redeemed successfully
                    if (successfulPayment == 0)
                    {
                        // Update Redeem dialog 
    //                    GetTransactionFacade ().RedeemPremiumItemCredits (SelectedCommunityId);

                        // Update totals
                        ShowTotals ();

                        BindTransactions (true);
                    }
                }
            }
        }


        #endregion Event Handlers

        #region Attributes

        private int PageNum
        {
            set
            {
                this.pageNum = value;
            }
            get
            {
                return this.pageNum;
            }
        }
        private int PageSize
        {
            set
            {
                this.pageSize = value;
            }
            get
            {
                return this.pageSize;
            }
        }
        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        public string OrderBy
        {
            get
            {
                return "purchase_id DESC";
            }
        }

        private uint TotalPrice
        {
            get {return totalPrice;}
            set {totalPrice = value;}
        }

        /// <summary>
        /// SELECTED_COMMUNITY_ID
        /// </summary>
        /// <returns></returns>
        private int SelectedCommunityId
        {
            get
            {
                return (int) ViewState["community_id"];
            }
            set
            {
                ViewState["community_id"] = value;
            }
         }

        /// <summary>
        /// SELECTED_Game_ID
        /// </summary>
        /// <returns></returns>
        private int SelectedGameId
        {
            get
            {
                if (selectedGameId > 0)
                {
                    return selectedGameId;
                }
                else
                {
                    selectedGameId = GetCommunityFacade ().GetCommunityGameId (SelectedCommunityId);
                    return selectedGameId;
                }
            }
            set
            {
                selectedGameId = value;
            }

        }

        #endregion Attributes

        #region Web Form Designer generated code

        override protected void OnInit (EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent ();
            base.OnInit (e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.Load += new System.EventHandler (this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler (pgTop_PageChange);
        }

        #endregion Web Form Designer generated code
    }
}