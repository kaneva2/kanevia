﻿using Kaneva.BusinessLayer.BusinessObjects;
using log4net;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Linq;
using Atlassian.Jira;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class ABTestExperiments : BaseUserControl
    {
        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected const string UNDEFINED_CATEGORY = "Uncategorized";

        private string updatedExperimentId = "";

        protected int PageNum
        {
            get
            {
                if (ViewState["PageNum"] == null)
                {
                    return 1;
                }
                return (int)ViewState["PageNum"];
            }
            set
            {
                ViewState["PageNum"] = value;
            }
        }

        protected int PageSize
        {
            get { return 20; }
        }

        protected string CurrentCategory
        {
            get
            {
                if (ViewState["CurrentCategory"] == null)
                {
                    return string.Empty;
                }
                return ViewState["CurrentCategory"].ToString();
            }
            set
            {
                ViewState["CurrentCategory"] = value;
            }
        }

        protected Experiment CurrentExperiment
        {
            get
            {
                if (ViewState["CurrentExperiment"] == null)
                {
                    return null;
                }
                return (Experiment)ViewState["CurrentExperiment"];
            }
            set
            {
                ViewState["CurrentExperiment"] = value;
            }
        }

        protected bool EditingCurrentExperiment
        {
            get
            {
                if (ViewState["EditingCurrentExperiment"] == null)
                {
                    return false;
                }
                return (bool)ViewState["EditingCurrentExperiment"];
            }
            set
            {
                ViewState["EditingCurrentExperiment"] = value;
            }
        }

        protected bool EditingPercentages
        {
            get
            {
                if (ViewState["EditingPercentages"] == null)
                {
                    return false;
                }
                return (bool)ViewState["EditingPercentages"];
            }
            set
            {
                ViewState["EditingPercentages"] = value;
            }
        }

        protected ExperimentGroup CurrentExperimentGroup
        {
            get
            {
                if (ViewState["CurrentExperimentGroup"] == null)
                {
                    return null;
                }
                return (ExperimentGroup)ViewState["CurrentExperimentGroup"];
            }
            set
            {
                ViewState["CurrentExperimentGroup"] = value;
            }
        }

        protected string CurrentSearchString
        {
            get
            {
                if (ViewState["CurrentSearchString"] == null)
                {
                    return string.Empty;
                }

                return ViewState["CurrentSearchString"].ToString();
            }
            set { ViewState["CurrentSearchString"] = value; }
        }

        protected bool CurrentSearchExact
        {
            get
            {
                return rblExactName.SelectedValue.Equals("Exact");
            }
            set
            {
                rblExactName.SelectedValue = (value ? "Exact" : "Partial");
            }
        }

        protected string CurrentSearchStatus
        {
            get
            {
                if (ViewState["CurrentSearchStatus"] == null)
                {
                    return string.Empty;
                }

                return ViewState["CurrentSearchStatus"].ToString();
            }
            set { ViewState["CurrentSearchStatus"] = value; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                // Check user access to content.
                if (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.KANEVA_PROPRIETARY) != (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL)
                {
                    RedirectToHomePage();
                    return;
                }

                if (!IsPostBack)
                {
                    // Set pagination parameters once, they're not changed in ResetPage().
                    PageNum = 1;

                    CurrentCategory = UNDEFINED_CATEGORY;
                    CurrentSearchString = string.Empty;
                    CurrentSearchStatus = string.Empty;

                    ResetPage();
                    BindData();
                }
            }
        }

        #region Helper Functions

        protected string GetWinningGroupName(Experiment experiment)
        {
            if (experiment == null || experiment.WinningGroup == null || string.IsNullOrWhiteSpace(experiment.WinningGroup.Name))
            {
                return string.Empty;
            }

            return experiment.WinningGroup.Name;
        }

        /// <summary>
        /// Resets page state to defaults, excluding pagination and search strings
        /// which are preserved.
        /// </summary>
        private void ResetPage(Experiment newExperiment = null)
        {
            CurrentExperiment = newExperiment;
            CurrentExperimentGroup = null;
            EditingCurrentExperiment = false;
            EditingPercentages = false;

            CurrentSearchString = txtSearch.Text;
            CurrentSearchStatus = ddlSearchStatus.SelectedValue;
            pgTop.CurrentPageNumber = PageNum;

            txtExperimentId.Text = string.Empty;
            txtExperimentName.Text = string.Empty;
            txtExperimentDescription.Text = string.Empty;
            ddlExperimentCategory.SelectedValue = UNDEFINED_CATEGORY;
            ddlAssignGroupsOn.SelectedValue = Experiment.ASSIGN_ON_CREATION_ONLY;
            ddlExperimentStatus.SelectedValue = EXPERIMENT_STATUS_TYPE.CREATED;
            ddlWinningGroup.SelectedValue = string.Empty;
            ddlAssignmentTarget.SelectedValue = Experiment.ASSIGNMENT_TARGET_USERS;
            txtCleanupJIRA.Text = string.Empty;

            txtGroupId.Text = string.Empty;
            txtGroupName.Text = string.Empty;
            txtGroupDescription.Text = string.Empty;
            ddlGroupLabel.SelectedValue = string.Empty;
            ddlGroupEffect.SelectedValue = string.Empty;

            divMsgTop.Style.Add("display", "none");
            divMsgExperimentDetails.Style.Add("display", "none");
            divMsgGroupListing.Style.Add("display", "none");
            divMsgGroupEdit.Style.Add("display", "none");
            divMsgPercentages.Style.Add("display", "none");
        }

        /// <summary>
        /// Configures the page to bind and show a listing of Experiments.
        /// </summary>
        private void ShowExperimentListings()
        {
            PagedList<Experiment> experiments = GetExperimentFacade().SearchExperiments(CurrentSearchString, CurrentSearchExact, (CurrentCategory.Equals(UNDEFINED_CATEGORY) ? string.Empty : CurrentCategory),
                    CurrentSearchStatus, "FIELD(e.status, 'S', 'C', 'E', 'A'), e.creation_date DESC", PageNum, PageSize);

            rptExperiments.DataSource = experiments;
            rptExperiments.DataBind();

            lblSearch.Text = GetResultsText(experiments.TotalCount, PageNum, PageSize, experiments.Count);

            pgTop.NumberOfPages = Math.Ceiling((double)experiments.TotalCount / PageSize).ToString();
            pgTop.DrawControl();

            divExperimentListing.Visible = true;
            divExperimentDetails.Visible = false;
        }

        /// <summary>
        /// Configures the page to bind and show an edit form for an Experiment.
        /// </summary>
        private void ShowExperimentForm(IEnumerable<ExperimentCategory> categories)
        {
            // Bind winning group dropdown.
            ExperimentGroup blankGroup = new ExperimentGroup();
            blankGroup.GroupId = string.Empty;
            blankGroup.Name = string.Empty;

            CurrentExperiment.ExperimentGroups.Insert(0, blankGroup);
            ddlWinningGroup.DataSource = CurrentExperiment.ExperimentGroups;
            ddlWinningGroup.DataTextField = "Name";
            ddlWinningGroup.DataValueField = "GroupId";
            ddlWinningGroup.DataBind();
            CurrentExperiment.ExperimentGroups.RemoveAt(0);

            // Bind category dropdown.
            ddlExperimentCategory.DataSource = categories;
            ddlExperimentCategory.DataTextField = "Name";
            ddlExperimentCategory.DataValueField = "Name";
            ddlExperimentCategory.DataBind();

            // Set inputs based on selected experiment.
            txtExperimentId.Text = CurrentExperiment.ExperimentId;
            txtExperimentName.Text = CurrentExperiment.Name;
            txtExperimentDescription.Text = CurrentExperiment.Description;
            ddlAssignGroupsOn.SelectedValue = CurrentExperiment.AssignGroupsOn;
            ddlExperimentStatus.SelectedValue = CurrentExperiment.Status;
            ddlAssignmentTarget.SelectedValue = CurrentExperiment.AssignmentTarget;
            txtCleanupJIRA.Text = CurrentExperiment.CleanupJIRA;

            if (ddlExperimentCategory.Items.Count > 0)
            {
                ddlExperimentCategory.SelectedValue = (string.IsNullOrWhiteSpace(CurrentExperiment.Category) ? CurrentCategory : CurrentExperiment.Category);
            }

            if (ddlWinningGroup.Items.Count > 0)
            {
                ddlWinningGroup.SelectedValue = CurrentExperiment.WinningGroupId;
            }

            // Enable fields based on whether or not we're editing.
            txtExperimentId.Enabled = EditingCurrentExperiment && string.IsNullOrWhiteSpace(txtExperimentId.Text);
            txtExperimentName.Enabled = EditingCurrentExperiment;
            txtExperimentDescription.Enabled = EditingCurrentExperiment;
            ddlExperimentCategory.Enabled = EditingCurrentExperiment;
            ddlAssignGroupsOn.Enabled = EditingCurrentExperiment;
            ddlExperimentStatus.Enabled = EditingCurrentExperiment;
            ddlWinningGroup.Enabled = EditingCurrentExperiment;
            ddlAssignmentTarget.Enabled = EditingCurrentExperiment;
            txtCleanupJIRA.Enabled = EditingCurrentExperiment;

            btnEditExperiment.Visible = !EditingCurrentExperiment;
            btnDeleteExperiment.Visible = !EditingCurrentExperiment;
            btnUpdateExperiment.Visible = EditingCurrentExperiment;
            btnCancelExperiment.Visible = (EditingCurrentExperiment && !string.IsNullOrWhiteSpace(CurrentExperiment.ExperimentId));
        }

        /// <summary>
        /// Configures the page to bind and show a listing of ExperimentGroups.
        /// </summary>
        private void ShowExperimentGroupListings()
        {
            rptExperimentGroups.DataSource = CurrentExperiment.ExperimentGroups.OrderBy(g => g.Label);
            rptExperimentGroups.DataBind();

            rptExperimentGroupURLs.DataSource = CurrentExperiment.ExperimentGroups.OrderBy(g => g.Label);
            rptExperimentGroupURLs.DataBind();

            divEditPercentages.Visible = false;
            divGroupListing.Visible = true;
            divGroupEdit.Visible = false;
            btnEditPercentages.Visible = (CurrentExperiment.ExperimentGroups.Count > 0);
        }

        /// <summary>
        /// Configures the page to bind and show an edit form for an ExperimentGroup.
        /// </summary>
        private void ShowExperimentGroupForm()
        {
            PagedList<ExperimentEffect> effects = GetExperimentFacade().GetExperimentEffects(string.Empty, 1, Int32.MaxValue);
            effects.Add(new ExperimentEffect(string.Empty, string.Empty));

            ddlGroupEffect.DataSource = effects;
            ddlGroupEffect.DataTextField = "Effect";
            ddlGroupEffect.DataValueField = "Effect";
            ddlGroupEffect.DataBind();

            txtGroupId.Text = CurrentExperimentGroup.GroupId;
            txtGroupName.Text = CurrentExperimentGroup.Name;
            txtGroupDescription.Text = CurrentExperimentGroup.Description;
            ddlGroupLabel.SelectedValue = CurrentExperimentGroup.Label;
            txtGroupId.Enabled = string.IsNullOrWhiteSpace(txtGroupId.Text);
            ddlGroupEffect.SelectedValue = (CurrentExperimentGroup.Effects.Count > 0 ? CurrentExperimentGroup.Effects[0].Effect : string.Empty);

            divEditPercentages.Visible = false;
            divGroupListing.Visible = false;
            divGroupEdit.Visible = true;
        }

        /// <summary>
        /// Configures the page to bind and show an edit form for an ExperimentGroup's percentages.
        /// </summary>
        private void ShowPercentagesForm()
        {
            rptPercentages.DataSource = CurrentExperiment.ExperimentGroups;
            rptPercentages.DataBind();

            divEditPercentages.Visible = true;
            divGroupListing.Visible = false;
            divGroupEdit.Visible = false;
        }

        /// <summary>
        /// Binds page data based on the current context.
        /// </summary>
        private void BindData()
        {
            // Bind category tabs.
            PagedList<ExperimentCategory> categories = GetExperimentFacade().SearchExperimentCategories(string.Empty, "category ASC", 1, Int32.MaxValue);
            categories.Insert(0, new ExperimentCategory(UNDEFINED_CATEGORY, UNDEFINED_CATEGORY));

            rptExperimentCategories.DataSource = categories;
            rptExperimentCategories.DataBind();

            if (CurrentExperiment == null)
            {
                // Bind experiment listings.
                ShowExperimentListings();
            }
            else
            {
                // Show experiment details.
                ShowExperimentForm(categories);

                if (CurrentExperimentGroup == null)
                {
                    if (EditingPercentages)
                    {
                        // Show percentage edit form.
                        ShowPercentagesForm();
                    }
                    else
                    {
                        // Bind experiment group listings
                        ShowExperimentGroupListings();
                    }
                }
                else
                {
                    // Bind current group edit data
                    ShowExperimentGroupForm();
                }

                udpGroupDetails.Visible = (!string.IsNullOrWhiteSpace(CurrentExperiment.ExperimentId));
                divExperimentListing.Visible = false;
                divExperimentDetails.Visible = true;
            }
        }

        /// <summary>
        /// Configures the error message for display.
        /// </summary>
        private void ShowMessage(string msg, bool isError, string msgDiv = "", RepeaterItem container = null)
        {
            HtmlGenericControl spnMsgInline = new HtmlGenericControl();
            if (container != null)
            {
                spnMsgInline = (HtmlGenericControl)container.FindControl("spnMsgInline");
            }

            divMsgTop.InnerHtml = msg;
            divMsgExperimentDetails.InnerHtml = msg;
            divMsgGroupListing.InnerHtml = msg;
            divMsgGroupEdit.InnerHtml = msg;
            divMsgPercentages.InnerHtml = msg;
            spnMsgInline.InnerHtml = msg;

            // Choose which message container to display. Default sets all and relies on the ajax refresh to
            // only refresh the necessary ones.
            switch(msgDiv)
            {
                case "divMsgTop":
                    divMsgTop.Style.Add("display", "block");
                    break;
                case "divMsgExperimentDetails":
                    divMsgExperimentDetails.Style.Add("display", "block");
                    break;
                case "divMsgGroupListing":
                    divMsgGroupListing.Style.Add("display", "block");
                    break;
                case "divMsgGroupEdit":
                    divMsgGroupEdit.Style.Add("display", "block");
                    break;
                case "divMsgPercentages":
                    divMsgPercentages.Style.Add("display", "block");
                    break;
                case "spnMsgInline":
                    spnMsgInline.Style.Add("display", "inline");
                    break;
                default:
                    divMsgTop.Style.Add("display", "block");
                    divMsgExperimentDetails.Style.Add("display", "block");
                    divMsgGroupListing.Style.Add("display", "block");
                    divMsgGroupEdit.Style.Add("display", "block");
                    divMsgPercentages.Style.Add("display", "block");
                    spnMsgInline.Style.Add("display", "inline");
                    break;
            }

            divMsgTop.Attributes["class"] = (isError ? "error" : "success");
            divMsgExperimentDetails.Attributes["class"] = (isError ? "error" : "success");
            divMsgGroupListing.Attributes["class"] = (isError ? "error" : "success");
            divMsgGroupEdit.Attributes["class"] = (isError ? "error" : "success");
            divMsgPercentages.Attributes["class"] = (isError ? "error" : "success");
            spnMsgInline.Attributes["class"] = (isError ? "error" : "success");
        }

        protected string GetStatusCSS(string status)
        {
            switch (status)
            {
                case "C":
                    return "created";
                case "S":
                    return "started";
                case "E":
                    return "ended";
                case"A":
                    return "archived";
                default:
                    return "";
            }
        }

        private Issue CreateJIRAIssue(Experiment experiment)
        {
            if (experiment.Status != EXPERIMENT_STATUS_TYPE.ENDED
                || experiment.WinningGroup == null
                || string.IsNullOrWhiteSpace(experiment.WinningGroupId)
                || !Configuration.JIRAConnectionEnabled)
                return null;

            try
            {
                var jira = Jira.CreateRestClient(Configuration.JIRAUrl, Configuration.JIRAUsername, Configuration.JIRAPassword);
                var issue = jira.CreateIssue("ED");
                issue.Type = "Task";
                issue.Priority = "Medium";
                issue.Summary = string.Format("Cleanup Experiment \"{0}\" (\"{1}\")", experiment.Name, experiment.ExperimentId);
                issue.Description = string.Format("Group \"{0}\" was chosen as the winner.", experiment.WinningGroup.Label);
                issue.CustomFields.Add("Owner", "mvastani");
                issue.CustomFields.Add("Epic Link", "ED-4315");
                issue.SaveChanges();
                return issue;
            }
            catch (Exception ex)
            {
                m_logger.Error("Jira communication error: " + ex.Message);
                return null;
            }
        }

        private Issue GetJIRAIssue(string issueId)
        {
            if (string.IsNullOrWhiteSpace(issueId))
                return null;

            try
            {
                var jira = Jira.CreateRestClient(Configuration.JIRAUrl, Configuration.JIRAUsername, Configuration.JIRAPassword);
                return jira.GetIssue(issueId);
            }
            catch (Exception ex)
            {
                m_logger.Error("Jira communication error: " + ex.Message);
                return null;
            }
        }

        #endregion

        #region Experiment Edit Events

        /// <summary>
        /// Configures the page for creating an experiment.
        /// </summary>
        protected void btnNewExperiment_Click(Object sender, EventArgs e)
        {
            // Reset the page number.
            PageNum = 1;

            txtSearch.Text = string.Empty;

            ResetPage();
            CurrentExperiment = new Experiment();
            EditingCurrentExperiment = true;
            BindData();
        }

        /// <summary>
        /// Configures the page for editing an experiment.
        /// </summary>
        protected void btnEditExperiment_Click(Object sender, EventArgs e)
        {
            ResetPage(CurrentExperiment);
            EditingCurrentExperiment = true;
            BindData();
        }

        /// <summary>
        /// Delete an experiment and return to listings
        /// </summary>
        protected void btnDeleteExperiment_Click(Object sender, EventArgs e)
        {
            if (GetExperimentFacade().DeleteExperiment(CurrentExperiment))
            {
                // Reset the page number.
                PageNum = 1;

                ResetPage();
                BindData();

                ShowMessage("Experiment deleted successfully.", false);
            }
            else
            {
                ShowMessage("Error deleting experiment", true);
            }
        }

        /// <summary>
        /// Configures the page for viewing experiment details.
        /// </summary>
        protected void btnExperimentDetails_Click(Object sender, CommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                ResetPage();
                CurrentExperiment = GetExperimentFacade().GetExperiment(e.CommandArgument.ToString(), true);
                BindData();
            }
        }

        /// <summary>
        /// Saves changes to an Experiment to the database.
        /// </summary>
        protected void btnUpdateExperiment_Click(Object sender, EventArgs e)
        {
            try
            {
                // If this is a new experiment, make sure the input ExperimentId is not already taken.
                if (string.IsNullOrWhiteSpace(CurrentExperiment.ExperimentId) && !string.IsNullOrWhiteSpace(txtExperimentId.Text))
                {
                    Experiment duplicate = GetExperimentFacade().GetExperiment(txtExperimentId.Text, false);
                    if (duplicate != null && !string.IsNullOrWhiteSpace(duplicate.ExperimentId))
                    {
                        ShowMessage("ExperimentId " + duplicate.ExperimentId + " is already taken.", true, "divMsgExperimentDetails");
                        return;
                    }
                }

                CurrentExperiment.ExperimentId = (string.IsNullOrWhiteSpace(txtExperimentId.Text) ? CurrentExperiment.ExperimentId : txtExperimentId.Text);
                CurrentExperiment.Name = txtExperimentName.Text;
                CurrentExperiment.Description = txtExperimentDescription.Text;
                CurrentExperiment.AssignGroupsOn = ddlAssignGroupsOn.SelectedValue;
                CurrentExperiment.CreatorId = (CurrentExperiment.CreatorId > 0 ? CurrentExperiment.CreatorId : GetCurrentUser().UserId);
                CurrentExperiment.AssignmentTarget = ddlAssignmentTarget.SelectedValue;

                // Make sure start and end dates make sense based on status transition.
                if (CurrentExperiment.Status != ddlExperimentStatus.SelectedValue)
                {
                    switch (ddlExperimentStatus.SelectedValue)
                    {
                        case EXPERIMENT_STATUS_TYPE.CREATED:
                            CurrentExperiment.StartDate = null;
                            CurrentExperiment.EndDate = null;
                            break;
                        case EXPERIMENT_STATUS_TYPE.READY:
                            CurrentExperiment.StartDate = null;
                            CurrentExperiment.EndDate = null;
                            break;
                        case EXPERIMENT_STATUS_TYPE.STARTED:
                            CurrentExperiment.StartDate = DateTime.Now;
                            CurrentExperiment.EndDate = null;
                            break;
                        case EXPERIMENT_STATUS_TYPE.ENDED:
                            CurrentExperiment.StartDate = (CurrentExperiment.StartDate ?? DateTime.Now);
                            CurrentExperiment.EndDate = (CurrentExperiment.EndDate ?? DateTime.Now);
                            break;
                        case EXPERIMENT_STATUS_TYPE.ARCHIVED:
                            CurrentExperiment.StartDate = (CurrentExperiment.StartDate ?? DateTime.Now);
                            CurrentExperiment.EndDate = (CurrentExperiment.EndDate ?? DateTime.Now);
                            break;
                    }
                }
                CurrentExperiment.Status = ddlExperimentStatus.SelectedValue;

                CurrentExperiment.Category = (ddlExperimentCategory.SelectedValue.Equals(UNDEFINED_CATEGORY) ? string.Empty : ddlExperimentCategory.SelectedValue);
     
                CurrentExperiment.WinningGroupId = ddlWinningGroup.SelectedValue;

                if (CurrentExperiment.Status == EXPERIMENT_STATUS_TYPE.ENDED)
                {
                    // Make sure experiment can't be ended without a winning group
                    if (string.IsNullOrWhiteSpace(CurrentExperiment.WinningGroupId))
                    {
                        ShowMessage("A winning group must be selected when ending the experiment.", true, "divMsgExperimentDetails");
                        return;
                    }

                    // Check the Jira issue.
                    Issue issue;
                    if (string.IsNullOrWhiteSpace(txtCleanupJIRA.Text))
                    {
                        issue = CreateJIRAIssue(CurrentExperiment);
                    }
                    else
                    {
                        issue = GetJIRAIssue(txtCleanupJIRA.Text);
                    }
                    if (issue == null || string.IsNullOrWhiteSpace(issue.Key.ToString()))
                    {
                        ShowMessage("Error determining JIRA cleanup task. Check cleanup issue key.", true, "divMsgExperimentDetails");
                        return;
                    }
                    else
                    {
                        CurrentExperiment.CleanupJIRA = issue.Key.ToString();
                    }
                }
                else if (CurrentExperiment.Status != EXPERIMENT_STATUS_TYPE.ARCHIVED)
                {
                    CurrentExperiment.CleanupJIRA = null;
                    CurrentExperiment.WinningGroupId = null;
                }

                if (!GetExperimentFacade().SaveExperiment(CurrentExperiment))
                {
                    ShowMessage("Error updating experiment", true, "divMsgExperimentDetails");
                }
                else
                {
                    ResetPage(CurrentExperiment);
                    BindData();

                    if (!string.IsNullOrWhiteSpace(CurrentExperiment.CleanupJIRA))
                    {
                        string message = "Experiment saved successfully. <a href=\"" + Configuration.JIRAUrl + "/browse/" + CurrentExperiment.CleanupJIRA + "\">JIRA Link</a>";
                        ShowMessage(message, false, "divMsgExperimentDetails");
                    }
                    else
                    {
                        ShowMessage("Experiment saved successfully.", false, "divMsgExperimentDetails");
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, true, "divMsgExperimentDetails");
            }
        }

        /// <summary>
        /// Cancels editing of an Experiment and returns to Experiment listings.
        /// </summary>
        protected void btnCancelExperimentEdit_Click(Object sender, EventArgs e)
        {
            ResetPage(CurrentExperiment);
            BindData();
        }

        protected void lbBackToListing_Click(Object sender, EventArgs e)
        {
            ResetPage();
            BindData();
        }

        /// <summary>   
        /// Repeater rptExperiments ItemDataBound Event Handler
        /// </summary>
        protected void rptExperiments_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                // get the details
                Experiment experiment = (Experiment)e.Item.DataItem;

                UserExperimentGroup userExperimentGroup = GetExperimentFacade().GetUserExperimentGroupCommon(GetCurrentUser().UserId, experiment.ExperimentId);
                DropDownList ddlGroupInlineInput = ((DropDownList)e.Item.FindControl("ddlGroupInlineInput"));
                Label lblUser = ((Label)e.Item.FindControl("lblUser"));

                if (userExperimentGroup != null && experiment.Status.Equals(EXPERIMENT_STATUS_TYPE.STARTED))
                {
                    lblUser.Text = "Current User: " + userExperimentGroup.Username;

                    ddlGroupInlineInput.DataSource = experiment.ExperimentGroups;
                    ddlGroupInlineInput.DataBind();
                    ddlGroupInlineInput.SelectedValue = userExperimentGroup.GroupId;
                }
                else
                {
                    lblUser.Text = "";
                    ddlGroupInlineInput.Visible = false;
                }

                if (experiment.WinningGroup != null && !string.IsNullOrWhiteSpace(experiment.WinningGroup.Name))
                {
                    HtmlContainerControl spnWinner = ((HtmlContainerControl)e.Item.FindControl("spnWinner"));
                    spnWinner.InnerHtml = "<span>Winner:</span> " + experiment.WinningGroup.Name;
                    spnWinner.Visible = true;
                }

                if (!string.IsNullOrWhiteSpace(updatedExperimentId) && updatedExperimentId == experiment.ExperimentId)
                {
                    ShowMessage("Participant updated.", false, "spnMsgInline", e.Item);
                }
            }
        }

        protected void ddlGroupInlineInput_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlGroupInlineInput = (DropDownList)sender;
            RepeaterItem rpItem = ddlGroupInlineInput.NamingContainer as RepeaterItem;
            if (rpItem != null)
            {
                HiddenField hfExperimentId = (HiddenField)rpItem.FindControl("hfExperimentId");
                string experimentId = hfExperimentId.Value;
                UserExperimentGroup userExperimentGroup = GetExperimentFacade().GetUserExperimentGroupCommon(GetCurrentUser().UserId, experimentId);

                userExperimentGroup.GroupId = ddlGroupInlineInput.SelectedItem.Value;

                ResetPage(CurrentExperiment);
                if (!GetExperimentFacade().SaveUserExperimentGroup(userExperimentGroup))
                {
                    updatedExperimentId = string.Empty;
                }
                else
                {
                    updatedExperimentId = experimentId;
                }
                BindData();
            }
        }
        

        #endregion

        #region Experiment Group Edit Events

        /// <summary>
        /// Configures the page for creating an experiment group.
        /// </summary>
        protected void btnNewExperimentGroup_Click(Object sender, EventArgs e)
        {
            ResetPage(CurrentExperiment);
            CurrentExperimentGroup = new ExperimentGroup();
            BindData();
        }

        /// <summary>
        /// Configures the page for editing an experiment group.
        /// </summary>
        protected void lbEditExperimentGroup_Click(Object sender, CommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                ResetPage(CurrentExperiment);
                CurrentExperimentGroup = CurrentExperiment.ExperimentGroups.SingleOrDefault(g => g.GroupId.Equals(e.CommandArgument.ToString()));
                BindData();
            }
        }

        /// <summary>
        /// Configures the page for editing percentages.
        /// </summary>
        protected void btnEditPercentages_Click(Object sender, EventArgs e)
        {
            ResetPage(CurrentExperiment);
            EditingPercentages = true;
            BindData();
        }

        /// <summary>
        /// Saves changes to percentages
        /// </summary>
        protected void btnUpdatePercentages_Click(Object sender, EventArgs e)
        {
            try
            {
                // Search repeater controls for the inputs.
                foreach(RepeaterItem rItem in rptPercentages.Items)
                {
                    if (rItem.ItemType == ListItemType.Item || rItem.ItemType == ListItemType.AlternatingItem)
                    {
                        int percentage = 0;
                        string percentTxt = (((TextBox)rItem.FindControl("txtAssignmentPercentage")).Text ?? string.Empty);

                        // If the percentage is valid, update the group.
                        if (Int32.TryParse(percentTxt, out percentage))
                        {
                            string groupId = ((HiddenField)rItem.FindControl("hfGroupId")).Value;
                            ExperimentGroup group = CurrentExperiment.ExperimentGroups.SingleOrDefault(g => g.GroupId.Equals(groupId));

                            group.AssignmentPercentage = percentage;
                        }
                        else
                        {
                            ShowMessage("Invalid percentage: " + percentTxt, true, "divMsgPercentages");
                            break;
                        }
                    }
                }

                if (!GetExperimentFacade().SaveExperimentGroups(CurrentExperiment, false))
                {
                    ShowMessage("Error updating group percentages", true, "divMsgPercentages");
                }
                else
                {
                    ResetPage(CurrentExperiment);
                    BindData();
                    ShowMessage("Group percentages updated successfully", false, "divMsgGroupListing");
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, true, "divMsgPercentages");
            }
        }

        /// <summary>
        /// Cancels editing of percentages.
        /// </summary>
        protected void btnCancelPercentages_Click(Object sender, EventArgs e)
        {
            ResetPage(CurrentExperiment);
            BindData();
        }

        /// <summary>
        /// Configures the page for deleting an experiment group.
        /// </summary>
        protected void btnDeleteGroup_Click(Object sender, CommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                ExperimentGroup groupToDelete = CurrentExperiment.ExperimentGroups.SingleOrDefault(g => g.GroupId.Equals(e.CommandArgument.ToString()));

                if (groupToDelete != null && GetExperimentFacade().DeleteExperimentGroup(groupToDelete))
                {
                    ResetPage(CurrentExperiment);
                    BindData();
                    ShowMessage("Group deleted successfully.", false, "divMsgGroupListing");
                    return;
                }
            }

            ShowMessage("Error deleting group", true, "divMsgGroupListing");
        }

        /// <summary>
        /// Saves changes to an ExperimentGroup to the database.
        /// </summary>
        protected void btnUpdateExperimentGroup_Click(Object sender, EventArgs e)
        {
            try
            {
                // If this is a new group, make sure the input GroupId is not already taken.
                if (string.IsNullOrWhiteSpace(CurrentExperimentGroup.GroupId) && !string.IsNullOrWhiteSpace(txtGroupId.Text))
                {
                    ExperimentGroup duplicate = GetExperimentFacade().GetExperimentGroup(txtGroupId.Text);
                    if (duplicate != null && !string.IsNullOrWhiteSpace(duplicate.GroupId))
                    {
                        ShowMessage("GroupId " + duplicate.GroupId + " is already taken.", true, "divMsgGroupEdit");
                        return;
                    }
                }

                CurrentExperimentGroup.GroupId = (string.IsNullOrWhiteSpace(txtGroupId.Text) ? CurrentExperimentGroup.GroupId : txtGroupId.Text);
                CurrentExperimentGroup.Name = txtGroupName.Text;
                CurrentExperimentGroup.Description = txtGroupDescription.Text;
                CurrentExperimentGroup.Label = ddlGroupLabel.SelectedValue;
                CurrentExperimentGroup.ExperimentId = CurrentExperiment.ExperimentId;
                CurrentExperimentGroup.Experiment = CurrentExperiment;

                CurrentExperimentGroup.Effects.Clear();
                if (!string.IsNullOrWhiteSpace(ddlGroupEffect.SelectedValue))
                {
                    CurrentExperimentGroup.Effects.Add(new ExperimentEffect(ddlGroupEffect.SelectedValue, string.Empty));
                }

                ExperimentGroup matchingGroup = CurrentExperiment.ExperimentGroups.SingleOrDefault(g => g.GroupId.Equals(CurrentExperimentGroup.GroupId));

                // If this is a new group, we need to adjust the assignment percentages for all groups to even out to 100%.
                if (matchingGroup == null)
                {
                    CurrentExperiment.ExperimentGroups.Add(CurrentExperimentGroup);
                }
                else
                {
                    CurrentExperiment.ExperimentGroups.Remove(matchingGroup);
                    CurrentExperiment.ExperimentGroups.Add(CurrentExperimentGroup);
                }

                if (!GetExperimentFacade().SaveExperimentGroups(CurrentExperiment, matchingGroup == null))
                {
                    // Make sure to remove the group from the current if it's new and invalid.
                    if (matchingGroup == null)
                    {
                        CurrentExperiment.ExperimentGroups.Remove(CurrentExperimentGroup);
                    }

                    ShowMessage("Error updating group", true, "divMsgGroupEdit");
                }
                else
                {
                    ResetPage(CurrentExperiment);
                    BindData();
                    ShowMessage("Group updated successfully", false, "divMsgGroupListing");
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, true, "divMsgGroupEdit");
            }
        }

        /// <summary>
        /// Cancels editing of an experiment group and returns to experiment group listings.
        /// </summary>
        protected void btnCancelExperimentGroupEdit_Click(Object sender, EventArgs e)
        {
            ResetPage(CurrentExperiment);
            BindData();
        }

        #endregion

        #region Pagination

        /// <summary>
        /// Rebinds experiment data on pagination change.
        /// </summary>
        private void pgTop_PageChange(object sender, PageChangeEventArgs e)
        {
            PageNum = e.PageNumber;
            BindData();
        }

        /// <summary>
        /// Rebinds experiment data on search text.
        /// </summary>
        protected void ddlSearchStatus_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            CurrentSearchStatus = ddlSearchStatus.SelectedValue;

            // Reset the page number.
            PageNum = 1;

            ResetPage();
            BindData();
        }

        /// <summary>
        /// Rebinds experiment data on search text.
        /// </summary>
        protected void btnSearch_Click(Object sender, EventArgs e)
        {
            if (!SiteManagementCommonFunctions.ContainsInjectScripts(txtSearch.Text))
            {
                CurrentSearchString = txtSearch.Text;
            }

            // Reset the page number.
            PageNum = 1;

            ResetPage();
            BindData();
        }

        /// <summary>
        /// Changes Experiment listing based on category selection.
        /// </summary>
        protected void lbCategoryTab_Click(Object sender, CommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                CurrentCategory = e.CommandArgument.ToString();

                // Reset the page number.
                PageNum = 1;

                ResetPage();
                BindData();
            }
        }

        #endregion

        #region DataBinding Events

        /// <summary>
        /// Updates tab selection on data binding.
        /// </summary>
        protected void rptExperimentCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton tabLink = (LinkButton)e.Item.FindControl("lbCategoryTab");
                tabLink.CssClass = (tabLink.CommandArgument.Equals(CurrentCategory) ? "tab selected" : "tab");
            }
        }

        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler(pgTop_PageChange);
        }

        #endregion Web Form Designer generated code

    }
}