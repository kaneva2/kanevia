﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GameAchievements.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.GameAchievements" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<link href="css/kaneva/buttons.css" type="text/css" rel="stylesheet">
<link href="css/kaneva/GameAchievements.css" type="text/css" rel="stylesheet">


<div id="achContainer">

	<div id="achData">
		<div class="buttons">
			<asp:LinkButton cssclass="buttonFixed" id="lbReset" runat="server" onclick="lbReset_Click"  OnClientClick="return confirm('Are you sure you want to reset all badges?');" causesvalidation="false"><span class="delete">Reset Badges</span></asp:LinkButton>
			<asp:LinkButton cssclass="buttonFixed" id="lbNew" runat="server" onclick="lbNew_Click" causesvalidation="false"><span class="add">Add an Item</span></asp:LinkButton>
		</div>
		<div id="divErrorTop" runat="server" class="error errtop" style="display:none;"></div>
		
		<div id="data">
		<asp:repeater id="rptBadges" runat="server" >
			<itemtemplate>
				<div class="dataRow" id="rowContainer" runat="server">
					<div class="framesize-small">
						<div class="frame">
							<div class="imgconstrain">
								<img border="0" src='<%# GetAchievementImageURL (DataBinder.Eval(Container.DataItem, "ImageUrl").ToString(), "me") %>' border="0"/>
							</div>	
						</div>
					</div>
					<div class="info">		    
						<div class="name"><%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></div>
						<div class="desc"><%# DataBinder.Eval(Container.DataItem, "Description").ToString() %></div>
					</div>
					<div class="gameBadges">		 
						<div class="points"><%# DataBinder.Eval(Container.DataItem, "Points").ToString() %> points</div>
						<div class="date"></div>
						<div class="edit"><asp:LinkButton id="lbEdit" runat="server" commandargument='<%# (DataBinder.Eval(Container.DataItem, "AchievementsId")) %>' causesvalidation="false" oncommand="lbEdit_Click">edit</asp:LinkButton> | <asp:linkbutton id="lbDelete" runat="server" visible='true' commandargument='<%# (DataBinder.Eval(Container.DataItem, "AchievementsId")) %>' text="Delete" oncommand="lbDelete_Click"/></div>
					</div>					
				</div>
			</itemtemplate>						
		</asp:repeater>
        </div>
		            
        <div class="results"><asp:Label runat="server" id="lblSearch"/></div>
        <div class="pager"><Kaneva:Pager runat="server" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true"/></div>
        
        <!-- Edit Section -->           
		<div id="divDetails" runat="server" class="detailsContainer">
			<div id="divEditTitle" runat="server" class="title"></div>
			<div id="divError" runat="server" class="error" style="display:none;"></div>
			<div class="imgContainer">
				<div class="frame" style="margin-bottom:8px;">
					<div class="imgconstrain">
						<img id="imgThumbnail" runat="server" border="0" src="" />
					</div>	
				</div>
				<input runat="server" ID="inpThumbnail" type="file" size="45" />
				<div class="note">We recommend 90x90</div>
			</div>
			<div class="dataContainer">
				<div class="input"><div class="label">Badge Name:</div> <asp:textbox id="txtName" runat="server" maxlength="45" ></asp:textbox></div>
				<div class="input"><div class="label">Description:</div> <asp:textbox id="txtDescription" runat="server" maxlength="100"></asp:textbox></div>
				<div class="input points"><div class="label">Point Value:</div> <asp:textbox id="txtPoints" runat="server" text="1"  maxlength="4"></asp:textbox>
					<div id="Div1" runat="server" class="availPts">You have <span id="spnAvailablePts" runat="server"></span> points remaining to use.
					<div class="note">The sum of your assigned badge points can not exceed <asp:literal id="litMaxPts" runat="server"></asp:literal>.</div>
					</div>
				</div>
			</div>
			<div class="updateButtons">
                <asp:button id="btnUpdateAchievement" runat="server" causesvalidation="False" onclick="btnUpdateAchievements_Click" text="Save" />&nbsp;&nbsp;
                <asp:button id="btnCancelAchievement" runat="Server" CausesValidation="False" onclick="btnCancelAchievements_Click" Text="Cancel"/>
			</div>
        </div>
		<a name="details" href="javascript:void(0);"></a>
		<a id="aScroll" href="#details" style="visibility:hidden;"></a>
	</div>

</div>

<script type="text/javascript">
	if ($('<%= divDetails.ClientID %>').style.display == 'block') {
		$('aScroll').click();
	}
</script>
