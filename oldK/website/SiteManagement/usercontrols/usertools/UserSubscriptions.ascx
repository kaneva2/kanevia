
<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="UserSubscriptions.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.UserSubscriptions" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<script type="text/javascript" src="jscript/yahoo/yahoo-min.js"></script>  
<script type="text/javascript" src="jscript/yahoo/event-min.js" ></script>   
<script type="text/javascript" src="jscript/yahoo/dom-min.js" ></script>   
<script type="text/javascript" src="jscript/yahoo/calendar-min.js"></script>

<script type="text/javascript" language="Javascript" src="jscript/PageLayout/colorpicker.js"></script>
<script type="text/javascript" language="javascript" src="jscript/prototype.js"></script>

<link type="text/css" rel="stylesheet" href="css/yahoo/calendar.css">   
<link href="css/editWidgets.css" rel="stylesheet" type="text/css" />


<style >
	#cal1Container { display:none; position:absolute; z-index:100;}
	#cal2Container { display:none; position:absolute; z-index:100;}
	#cal3Container { display:none; position:absolute; z-index:100;}
	#cal4Container { display:none; position:absolute; z-index:100;}
</style>

<script type="text/javascript"><!--

 var tbx_PurchasedDate_ID = '<%= tbx_PurchasedDate.ClientID %>';
 var tbx_CancelledDate_ID = '<%= tbx_CancelledDate.ClientID %>';
 var tbx_EndDate_ID = '<%= tbx_EndDate.ClientID %>';
 var tbx_FreeTrialDate_ID = '<%= tbx_FreeTrialDate.ClientID %>';

YAHOO.namespace("example.calendar");
    
	function handlePurchasedDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate1 = document.getElementById(tbx_PurchasedDate_ID);
		txtDate1.value = month + "-" + day + "-" + year;
		YAHOO.example.calendar.cal1.hide();
	}
	
	function handleCancelledDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate2 = document.getElementById(tbx_CancelledDate_ID);
		txtDate2.value = month + "-" + day + "-" + year;
		YAHOO.example.calendar.cal2.hide();
	}

	function handleEndDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate3 = document.getElementById(tbx_EndDate_ID);
		txtDate3.value = month + "-" + day + "-" + year;
		YAHOO.example.calendar.cal3.hide();
	}

	function handleFreeTrialEndDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate4 = document.getElementById(tbx_FreeTrialDate_ID);
		txtDate4.value = month + "-" + day + "-" + year;
		YAHOO.example.calendar.cal4.hide();
	}
		
	function initPurchasedDate() {
		// Admin Calendar
		YAHOO.example.calendar.cal1 = new YAHOO.widget.Calendar ("cal1", "cal1Container", { iframe:true, zIndex:200, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal1.render();
		
		// Listener to show Admin Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgPurchasedDate", "click", YAHOO.example.calendar.cal1.show, YAHOO.example.calendar.cal1, true);   
		YAHOO.example.calendar.cal1.selectEvent.subscribe(handlePurchasedDate, YAHOO.example.calendar.cal1, true);
	}
	
	function initCancelledDate() {
		// Event Calendar
		YAHOO.example.calendar.cal2 = new YAHOO.widget.Calendar ("cal2", "cal2Container", { iframe:true, zIndex:1000, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal2.render();
		
		// Listener to show the Event Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgCancelledDate", "click", YAHOO.example.calendar.cal2.show, YAHOO.example.calendar.cal2, true);   
		YAHOO.example.calendar.cal2.selectEvent.subscribe(handleCancelledDate, YAHOO.example.calendar.cal2, true);
	}

	function initEndDate() {
		// Event Calendar
		YAHOO.example.calendar.cal3 = new YAHOO.widget.Calendar ("cal3", "cal3Container", { iframe:true, zIndex:1000, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal3.render();
		
		// Listener to show the Event Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgEndDate", "click", YAHOO.example.calendar.cal3.show, YAHOO.example.calendar.cal3, true);   
		YAHOO.example.calendar.cal3.selectEvent.subscribe(handleEndDate, YAHOO.example.calendar.cal3, true);
	}

	function initFreeTrialEndDate() {
		// Event Calendar
		YAHOO.example.calendar.cal4 = new YAHOO.widget.Calendar ("cal4", "cal4Container", { iframe:true, zIndex:1000, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal4.render();
		
		// Listener to show the Event Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgFreeTrialEndDate", "click", YAHOO.example.calendar.cal4.show, YAHOO.example.calendar.cal4, true);   
		YAHOO.example.calendar.cal4.selectEvent.subscribe(handleFreeTrialEndDate, YAHOO.example.calendar.cal4, true);
	}

//--> </script> 

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    <ajax:ajaxpanel id="ajMessage" runat="server">

    <asp:label style="position:relative; font-size:18px; padding: 10px 0 10px 0" runat="server" id="messages" visible="true" />
    <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="980">
        <tr>
            <td style="padding-left:40px; text-align:center; font-size:22px">
                USER SUBSCRIPTIONS
            </td>
        </tr>
        <tr>
            <td style="padding-left:40px">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
	                <tr style="line-height:15px;">
	                    <td align="left" style="width:300px" class="belowFilter2" valign="middle" >
	                        <asp:Label runat="server" id="lblSelectedUser" />
	                    </td>
		                <td align="right" class="belowFilter2" valign="middle">
			                <asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop"/><br>
		                </td>						
	                </tr>
                </table>
             </td>
         </tr>
         <tr>
            <td style="padding-left:40px">
                <div style="width:900px;text-align:left;margin:0 0 12px 0;">
				    <asp:LinkButton id="lbn_NewUserSubscription" runat="server" causesvalidation="false" onclick="lbn_AddPass_Click">Add New Subscription</asp:LinkButton>
			    </div>
               <asp:Repeater ID="rptPasses" runat="server" >
                    <HeaderTemplate>
                            <table border="1" frame="border" cellpadding="4" cellspacing="0" width="100%">
                                <tr style="background-color:#cccccc"> 
                                    <th align="center" style="width:110px;">Pass</th>
                                    <th align="left" style="width:125px;">Price</th>
                                    <th align="left">Subscription Identifier</th>
                                    <th align="left">Allowance</th>
                                    <th align="left">Expires</th>
                                    <th align="left">-</th>
	                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                                <tr>
                                    <td>
				                        <asp:label id="lbl_userSubscriptionId" runat="server" visible="false" />
				                        <asp:label id="lbl_PassName" runat="server" visible="true" />
                                    </td>
                                     <td>
                                         <asp:label id="lbl_Price" runat="server" visible="true" />
                                    </td>
                                    <td>
				                        <asp:label id="lbl_SubscrptIdent" runat="server" visible="true" />
                                    </td>
                                    <td>
				                        <asp:label id="lbl_MonthlyAllowance" runat="server" visible="true" />
                                    </td>
                                    <td>
				                        <asp:label id="lbl_EndDate" runat="server" visible="true" />
                                    </td>
                                      <td>
                                         <asp:LinkButton id="lbn_edit" runat="server" causesvalidation="false" onclick="lbn_EditPass_Click">edit</asp:LinkButton>
                                         <!--<asp:LinkButton id="lbn_delete" runat="server" causesvalidation="false" onclick="lbn_DeletePass_Click">delete</asp:LinkButton>-->
                                    </td>
                              </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                                <tr style="background-color:#eeeeee">
                                    <td>
				                        <asp:label id="lbl_userSubscriptionId" runat="server" visible="false" />
				                        <asp:label id="lbl_PassName" runat="server" visible="true" />
                                    </td>
                                     <td>
                                         <asp:label id="lbl_Price" runat="server" visible="true" />
                                    </td>
                                    <td>
				                        <asp:label id="lbl_SubscrptIdent" runat="server" visible="true" />
                                    </td>
                                    <td>
				                        <asp:label id="lbl_MonthlyAllowance" runat="server" visible="true" />
                                    </td>
                                    <td>
				                        <asp:label id="lbl_EndDate" runat="server" visible="true" />
                                    </td>
                                      <td>
                                         <asp:LinkButton id="lbn_edit" runat="server" causesvalidation="false" onclick="lbn_EditPass_Click">edit</asp:LinkButton>
                                         <!--<asp:LinkButton id="lbn_delete" runat="server" causesvalidation="false" onclick="lbn_DeletePass_Click">delete</asp:LinkButton>-->
                                    </td>
                              </tr>
                    </AlternatingItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
         </tr>
        <tr id="tr_EditPass" runat="server" visible="false">
            <td colspan="2" align="center" style="padding-left:40px"><br />
                <table border="1" width="98%" cellpadding="0" cellspacing="4px">
                    <tr>
                        <td align="right">Promotion:</td>
                        <td align="left">
                            <asp:label id="lbl_selectedUserSubscriptionId" runat="server" visible="false" />
                            <asp:DropDownList ID="ddl_PassPromotions" runat="server" enabled="false" CausesValidation="false" AutoPostBack="true" OnSelectedIndexChanged="ddl_PassPromotions_SelectedIndexChanged" style="width: 180px; vertical-align: top;" />
                        </td>
                        <td align="right">Subscription:</td>
                        <td align="left">
                            <asp:DropDownList ID="ddl_Subscriptions" runat="server" enabled="false" CausesValidation="false" AutoPostBack="true" OnSelectedIndexChanged="ddl_Subscriptions_SelectedIndexChanged" style="width: 180px; vertical-align: top;" />
                        </td>
                        <td align="right">Auto Renew:</td>
                        <td align="left">
                            <asp:CheckBox ID="cbx_AutoRenew" runat="server" />
                        </td>
                    </tr>                    
                    <tr>
                        <td align="right">Subscription Period:</td>
                        <td align="left">
                            <asp:DropDownList runat="server" id="ddp_SubscriptionPeriod" style="width: 180px; vertical-align: top;"></asp:DropDownList>
                        </td>
                        <td align="right">Subscription Status:</td>
                        <td align="left">
                            <asp:DropDownList runat="server" id="ddp_SubscriptionStatus" style="width: 180px; vertical-align: top;"></asp:DropDownList>
                        </td>
                        <td align="right">Initial Term:</td>
                        <td align="left">
                            <asp:DropDownList runat="server" id="ddl_InitialTerm" style="width: 180px; vertical-align: top;"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                         <td align="right">Free Trial Date:</td>
                        <td align="left">
                            <asp:TextBox runat="server" Enabled="false" id="tbx_FreeTrialDate"></asp:TextBox>
		                    <img id="imgFreeTrialEndDate" name="imgFreeTrialEndDate" onload="initFreeTrialEndDate();" src="images/cal_16.gif"/> 
                            <div id="cal4Container"></div>                        
		                    <asp:RequiredFieldValidator runat="server" id="rfv_FreeTrialDate" controltovalidate="tbx_FreeTrialDate" text="* Please enter a a value." errormessage="The Free Trial End Date is required." display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td align="right">End Date:</td>
                        <td align="left">
                            <asp:TextBox runat="server" Enabled="false" id="tbx_EndDate"></asp:TextBox>
		                    <img id="imgEndDate" name="imgEndDate" onload="initEndDate();" src="images/cal_16.gif"/> 
                            <div id="cal3Container"></div>
		                    <asp:RequiredFieldValidator runat="server" id="rfv_EndDate" controltovalidate="tbx_EndDate" text="* Please enter a a value." errormessage="The end date is required." display="Dynamic"></asp:RequiredFieldValidator>
                        </td>                        
                        <td align="right">Billing Type:</td>
                        <td align="left">
                            <asp:Dropdownlist runat="server" ID="ddl_BillType" style="width: 180px; vertical-align: top;">
							    <asp:ListItem value="1">Tester</asp:ListItem >
							    <asp:ListItem value="2">No Charge</asp:ListItem >
							    <asp:ListItem value="3">Pay Pal</asp:ListItem >
							    <asp:ListItem value="4">Invite User</asp:ListItem >
							    <asp:ListItem value="5">CyberSource</asp:ListItem >
							    <asp:ListItem value="6">Incom Gift Card</asp:ListItem >
							</asp:Dropdownlist>
                        </td>

                   </tr>
                    <tr>
                        <td align="right">Purchased Date:</td>
                        <td align="left">
                            <asp:TextBox runat="server" Enabled="false" id="tbx_PurchasedDate"></asp:TextBox>
		                    <img id="imgPurchasedDate" name="imgPurchasedDate" onload="initPurchasedDate();" src="images/cal_16.gif"/> 
                            <div id="cal1Container"></div>                        
		                    <asp:RequiredFieldValidator runat="server" id="rfv_PurchasedDate" controltovalidate="tbx_PurchasedDate" text="* Please enter a a value." errormessage="The purchased date is required." display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td align="right">Cancelled Date:</td>
                        <td align="left">
                            <asp:TextBox runat="server" Enabled="false" id="tbx_CancelledDate"></asp:TextBox>
		                    <img id="imgCancelledDate" name="imgCancelledDate" onload="initCancelledDate();" src="images/cal_16.gif"/> 
                            <div id="cal2Container"></div>
		                    <asp:RequiredFieldValidator runat="server" id="rfv_CancelledDate" controltovalidate="tbx_CancelledDate" text="* Please enter a a value." errormessage="The cancelled date is required." display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td align="right">User Cancelled:</td>
                        <td align="left">
                            <asp:CheckBox ID="cbx_UserCancelled" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Price:</td>
                        <td align="left">
                            <asp:TextBox runat="server" id="tbx_Price"></asp:TextBox>
		                    <asp:RequiredFieldValidator runat="server" id="rfv_Price" controltovalidate="tbx_Price" text="* Please enter a a value." errormessage="The price is required." display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td align="right">Monthly Allowance:</td>
                        <td align="left">
                            <asp:TextBox runat="server" id="tbx_MonthlyAllowance"></asp:TextBox>
		                    <asp:RequiredFieldValidator runat="server" id="rfv_MonthlyAllowance" controltovalidate="tbx_MonthlyAllowance" text="* Please enter a a value." errormessage="The monthly allowance is required." display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td align="right">Discount (%):</td>
                        <td align="left">
                            <asp:TextBox runat="server" id="tbx_Discount"></asp:TextBox>
		                    <asp:RequiredFieldValidator runat="server" id="rfv_Discount" controltovalidate="tbx_Discount" text="* Please enter a a value." errormessage="The discount is required." display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left"><br />
                            <asp:Button ID="ibn_SaveRoleChanges" OnClick="SavePassChanges_Click" runat="server" text="Save" CausesValidation="true" />
                            <asp:Button ID="btn_CancelRoleChange" OnClick="btn_Cancel_Click" runat="server" text="Cancel" CausesValidation="false" />
                        </td>
                    </tr>
                 </table>
            </td>
        </tr>
         
    </table>
    </ajax:ajaxpanel>
</asp:Panel>