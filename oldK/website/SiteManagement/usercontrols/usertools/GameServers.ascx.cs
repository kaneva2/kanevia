using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;
using System.Text;
using MagicAjax;
using System.Drawing;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class GameServers : BaseUserControl
    {
        #region Declarations
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private bool isAdministraitor = false;
        private bool canEdit = false;
        private int ownerId = 0;
        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.STAR_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        isAdministraitor = true;
                        canEdit = true;
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    default:
                        break;
                }

                if ((Session["SelectedGameId"] != null) && Convert.ToInt32(Session["SelectedGameId"]) > 0)
                {
                    // Load the current user's data the first time
                    SELECTED_GAME_ID = Convert.ToInt32(Session["SelectedGameId"]);

                    //get the company id the game is associated with
                    this.ownerId = GetGameFacade().GetGameOwner(SELECTED_GAME_ID);

                    if (!isAdministraitor)
                    {
                        canEdit = this.ownerId.Equals(GetUserId());
                    }

                    if (!canEdit)
                    {
                        RedirectToHomePage();
                    }
                    else
                    {
                        //community id check is to allow kaneva website to make use of this usercontrol
                        if (!IsPostBack || (!IsKanevaPostBack))
                        {
                            // Log the CSR activity
                            this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Viewing game server information page", 0, SELECTED_GAME_ID);

                            //configure page
                            ConfigurePageForUse();

                            IsKanevaPostBack = true;
                        }
                    }
                }
                else
                {
                    //community id check is to tell it to redirect to search page (for KWAS only)
                    if (Request["communityId"] == null)
                    {
                        //store this pages URL for return
                        Session["CallingPage"] = Request.Url.ToString();
                        Response.Redirect(ResolveUrl("~/Default.aspx?ws=" + GetCurrentSiteID((int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.DEVELOPER) + "&mn=" + GetGameSearchParentMenuID() + "&sn=" + GetGameSearchControlID()));
                    }
                }
            }
            else
            {
                RedirectToHomePage();
            }
        }

        #endregion

        #region Functions

        private void ConfigurePageForUse()
        {
            //populate the page's drop downs
            PopulateGameServerVisibilityDropDown();

            //bind data for page
            BindGame();
        }

        private void BindGame()
        {
            //bind the main data
            BindGameData();

            //get the current servers associate with the game
            BindGameServers();

            //get the current license associated with the game
            BindPatchURLs();
        }

        private void BindGameData()
        {
            //get the game to be edited from the database
            GameToEdit = GetGameFacade().GetGameByGameId(SELECTED_GAME_ID);


            if (GameToEdit.IsIncubatorHosted)
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "ShowHosted", "<script type=\"text/javascript\">$('divKanevaHosted').style.display = '';</script>");
                Page.ClientScript.RegisterStartupScript(GetType(), "ShowServers", "<script type=\"text/javascript\">$('serverContainer').style.display = 'none';</script>");
            }

            //get the games servers for the game
            try
            {
                string orderby = DEFAULT_SERVER_SORT + " " + base.DEFAULT_SORT_ORDER;
                Game_Servers = GetGameFacade().GetServerByGameId(SELECTED_GAME_ID, "", orderby);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
                m_logger.Error("Error on GameServer Load for editing", ex);
            }
            //get the patchURLs for the game
            try
            {
                string orderby = DEFAULT_PATCH_SORT + " " + base.DEFAULT_SORT_ORDER;
                GamePatchURLs = GetGameFacade().GetPatchURLsByGameId(SELECTED_GAME_ID, "", orderby);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
                m_logger.Error("Error on Patch URL Load for editing", ex);
            }
        }

        private void BindGameServers()
        {
            //dg_GameServers.Sort
            dg_GameServers.PageIndex = CurrentServerPage;
            dg_GameServers.DataSource = Game_Servers;
            dg_GameServers.DataBind();
        }

        private void BindPatchURLs()
        {
            dg_PatchURLS.PageIndex = CurrentPatchURLPage;
            dg_PatchURLS.DataSource = GamePatchURLs;
            dg_PatchURLS.DataBind();
        }

        #endregion

        #region Helper Functions

        /// <summary>
        /// Populates Server visibility options Pull down
        /// </summary>
        private void PopulateGameServerVisibilityDropDown()
        {
            drpServerVisibility.DataSource = WebCache.GetGameServerVisbility();
            drpServerVisibility.DataTextField = "name";
            drpServerVisibility.DataValueField = "visibility_id";
            drpServerVisibility.DataBind();
        }

        private void PopulateGameServerEditFields(GridViewRow gvr)
        {
            try
            {
                drpServerVisibility.SelectedValue = gvr.Cells[2].Text.Trim();
                txtServerName.Text = Server.HtmlDecode(gvr.Cells[4].Text.Trim());
                tbx_PortNumber.Text = Server.HtmlDecode(gvr.Cells[9].Text.Trim());
                hdn_ServerID.Text = gvr.Cells[0].Text.Trim();
                hdn_ServerStartedDate.Text = gvr.Cells[7].Text.Trim();
                hdn_IPAddress.Text = gvr.Cells[8].Text.Trim();
                hdn_NumberOfPlayers.Text = gvr.Cells[10].Text.Trim();
                hdn_MaxPlayers.Text = gvr.Cells[11].Text.Trim();
                hdn_LastPingDate.Text = gvr.Cells[12].Text.Trim();
            }
            catch (Exception)
            {
                ShowMessage("Error populating Server fields for editing.");
            }
        }

        private void PopulatePatchURLEditFields(GridViewRow gvr)
        {
            try
            {
                hdn_patchURLID.Text = gvr.Cells[0].Text.Trim();
                hdn_PatchId.Text = gvr.Cells[1].Text.Trim();
                hdn_GameId.Text = gvr.Cells[2].Text.Trim();
                txtPatchURL.Text = Server.HtmlDecode(gvr.Cells[3].Text.Trim());
            }
            catch (Exception)
            {
                ShowMessage("Error populating patchURL fields for editing.");
            }
        }

        private void PatchURLPrep()
        {
            ClearPatchURLDataFields();
            hdn_patchURLID.Text = "-1";
            divPatchDetails.Visible = true;
            btnUpdatePatchURL.Text = "Add";
            divEditPatchTitle.InnerText = "Add New Patch URL";
        }

        private void PatchURLReset()
        {
            ClearPatchURLDataFields();
            divPatchDetails.Visible = false;
            btnUpdatePatchURL.Text = "Add";
        }

        private void ServerFieldsPrep()
        {
            ClearServerDataFields();
            hdn_ServerID.Text = "-1";
            divDetails.Visible = true;
            btnUpdateServer.Text = "Add";
            divEditTitle.InnerText = "Add New Server";
        }

        private void ServerReset()
        {
            ClearServerDataFields();
            divDetails.Visible = false;
            btnUpdateServer.Text = "Add";
        }

        /// <summary>
        /// Return the delete javascript
        /// </summary>
        /// <param name="TopicID"></param>
        /// <returns></returns>
        public string GetServerDeleteScript(int gameLicenseId)
        {
            return "javascript:if (confirm(\"Are you sure you want to delete this server?\")){DeleteServer (" + gameLicenseId + ")};";
        }

        //clear the patchURL fields
        private void ClearPatchURLDataFields()
        {
            txtPatchURL.Text = "";
            patchURLRequired.Visible = false;
            hdn_PatchIndex.Text = "";
            hdn_patchURLID.Text = "";
 			hdn_PatchId.Text = "";
			hdn_GameId.Text = "";
       }

        //clear the server data fields
        private void ClearServerDataFields()
        {
            txtServerName.Text = "";
            hdn_ServerID.Text = "";
            hdn_ServerListIndex.Text = "";
            drpServerVisibility.SelectedValue = Convert.ToString((int)GameServer.Visibility.Private);
            serverNameRequired.Visible = false;
            tbx_PortNumber.Text = Convert.ToString(SiteManagementCommonFunctions.DEFAULT_PORT);
            portNumberRequired.Visible = false;
            hdn_ServerStartedDate.Text = "";
            hdn_IPAddress.Text = "";
            hdn_NumberOfPlayers.Text = "";
            hdn_MaxPlayers.Text = "";
            hdn_LastPingDate.Text = "";
        }

        private void ShowMessage (string msg)
        {
            ShowMessage (msg, true);
        }
        private void ShowMessage (string msg, bool isError)
        {
            divErrorServer.InnerText = msg;
            divErrorServer.Attributes.Add ("class", isError ? "error" : "confirm");
            divErrorServer.Style.Add ("display", "block");
        }

        #endregion


        #region Attributes

        /// <summary>
        /// stores the current page of the server gridview
        /// </summary>
        /// <returns>int</returns>
        private int CurrentServerPage
        {
            get
            {
                if (ViewState["serverPage"] == null)
                {
                    ViewState["serverPage"] = 0;
                }
                return (int)ViewState["serverPage"];
            }
            set
            {
                ViewState["serverPage"] = value;
            }
        }

        /// <summary>
        /// stores the patchURL data to reduce database round trips
        /// </summary>
        /// <returns>GamePatchURLs</returns>
        private DataTable Game_Servers
        {
            get
            {
                if (ViewState["gameServers"] == null)
                {
                    ViewState["gameServers"] = new DataTable();
                }
                return (DataTable)ViewState["gameServers"];
            }
            set
            {
                ViewState["gameServers"] = value;
            }
        }

        /// <summary>
        /// stores the current page of the patchURL gridview
        /// </summary>
        /// <returns>int</returns>
        private int CurrentPatchURLPage
        {
            get
            {
                if (ViewState["patchURLPage"] == null)
                {
                    ViewState["patchURLPage"] = 0;
                }
                return (int)ViewState["patchURLPage"];
            }
            set
            {
                ViewState["patchURLPage"] = value;
            }
        }

        /// <summary>
        /// stores the patchURL data to reduce database round trips
        /// </summary>
        /// <returns>GamePatchURLs</returns>
        private DataTable GamePatchURLs
        {
            get
            {
                if (ViewState["patchURLs"] == null)
                {
                    ViewState["patchURLs"] = new DataTable();
                }
                return (DataTable)ViewState["patchURLs"];
            }
            set
            {
                ViewState["patchURLs"] = value;
            }
        }

        /// <summary>
        /// SELECTED_GAME_ID
        /// </summary>
        /// <returns></returns>
        private int SELECTED_GAME_ID
        {
            get
            {
                return (int)ViewState["game_id"];
            }
            set
            {
                ViewState["game_id"] = value;
            }

        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        public string DEFAULT_SERVER_SORT
        {
            get
            {
                return "server_name";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        public string DEFAULT_PATCH_SORT
        {
            get
            {
                return "patch_url";
            }
        }

        /// <summary>
        /// stores the game data to reduce database round trips
        /// </summary>
        /// <returns>Game</returns>
        private Game GameToEdit
        {
            get
            {
                if (ViewState["game"] == null)
                {
                    return null;
                }
                return (Game)ViewState["game"];
            }
            set
            {
                ViewState["game"] = value;
            }
        }

        /// <summary>
        /// stores if post back on kaneva site
        /// </summary>
        /// <returns>int</returns>
        private bool IsKanevaPostBack
        {
            get
            {
                if (ViewState["isKanevaPostBack"] == null)
                {
                    ViewState["isKanevaPostBack"] = false;
                }
                return (bool) ViewState["isKanevaPostBack"];
            }
            set
            {
                ViewState["isKanevaPostBack"] = value;
            }
        }
        
        #endregion


        #region Event Handlers

        //---event handlers for the Game server Gridview------------------
        protected void dg_GameServers_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            // If we're editing servers for the Kaneva game, add a link to tie to a server
            if (GameToEdit != null && GameToEdit.GameId == KanevaGlobals.WokGameId && e.Row.Cells.Count == 16)
            {
                LinkButton lbn_tieZone = (LinkButton)e.Row.Cells[15].FindControl("lbn_tieZone");
                if (lbn_tieZone != null)
                {
                    lbn_tieZone.Visible = true;
                }
            }
        }

        protected void dg_GameServers_OnRowCreated(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            //logic check to work around initial row creation wheere there is only one column
            if (e.Row.Cells.Count > 10)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[7].Visible = false;
                e.Row.Cells[11].Visible = false;
            }
        }

        /// <summary>
        /// They clicked to edit the pathc URL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_GameServers_RowEditing(object source, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            //passin the selected row and populate the fields
            ServerFieldsPrep();
            btnUpdateServer.Text = "Update";
            hdn_ServerListIndex.Text = e.NewEditIndex.ToString();
            divEditTitle.InnerText = "Update Server";
            PopulateGameServerEditFields (dg_GameServers.Rows[e.NewEditIndex]);
        }

        /// <summary>
        /// They clicked to change pages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_GameServers_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            CurrentServerPage = e.NewPageIndex;
            //bind data
            BindGameServers();
        }

        /// <summary>
        /// They clicked to sort a column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_GameServers_Sorting(object sender, GridViewSortEventArgs e)
        {
            base.gridview_Sorting(sender, e);
            //bind data
            BindGameServers();
        }

        /// <summary>
        /// lbn_NewServer_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbn_NewServer_Click(Object sender, EventArgs e)
        {
            ServerFieldsPrep();
        }

        /// <summary>
        /// btnServerCancel_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnServerCancel_Click(Object sender, EventArgs e)
        {
            ServerReset();
        }

        protected void dg_GameServers_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GridViewRow gvr = dg_GameServers.Rows[e.RowIndex];
                GetGameFacade().DeleteGameServer(Convert.ToInt32(gvr.Cells[0].Text));
                BindGame();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during game server deletion", ex);
                ShowMessage("Error during game server deletion.");
            }
        }

        protected void dg_GameServers_RowCommand(Object sender, GridViewCommandEventArgs e)
        {
            // If multiple buttons are used in a GridView control, use the
            // CommandName property to determine which button was clicked.
            if (e.CommandName == "TieZone")
            {
                // Snag the server id
                Session["SelectedWoKServerId"] = Convert.ToInt32(e.CommandArgument);

                // Redirect
                Response.Redirect(ResolveUrl("~/Default.aspx?ws=" + GetCurrentSiteID((int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.DEVELOPER) + "&mn=" + GetGameSearchParentMenuID() + "&sn=" + GetZoneSettingsControlID()));
            }
        }   

        /// <summary>
        /// btnUpdateServer_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdateServer_Click(Object sender, EventArgs e)
        {
            //set error message variable
            string error = "";

            //validate the required fields
            //validate the required fields
            if (txtServerName.Text == "")
            {
                error += "Server Name is required. ";
                serverNameRequired.Visible = true;
            }
            if (tbx_PortNumber.Text == "")
            {
                error += "Port Number is required. ";
                portNumberRequired.Visible = true;
            }
            // Check for any inject scripts in large text fields
            if ((SiteManagementCommonFunctions.ContainsInjectScripts(txtServerName.Text)) || (SiteManagementCommonFunctions.ContainsInjectScripts(tbx_PortNumber.Text)))
            {
                error += "Script Violation ";
                m_logger.Warn("User " + GetUserId() + " tried to script from IP " + Common.GetVisitorIPAddress());
                ShowErrorOnStartup("Your input contains invalid scripting, please remove script code and try again.", true);
            }
            if (error.Length > 0)
            {
                ShowMessage(error);
                return;
            }


            try
            {
                GameServer server = new GameServer();

                if (btnUpdateServer.Text.ToUpper().Equals("ADD"))
                {
                    try
                    {

                        server.ServerId = Convert.ToInt32(hdn_ServerID.Text);
                        server.GameId = SELECTED_GAME_ID;
                        server.VisibiltyId = Convert.ToInt32(drpServerVisibility.SelectedValue);
                        server.GameServerModifiersId = GetUserId();
                        server.ServerName = txtServerName.Text.Trim();
                        server.Port = Convert.ToInt32(tbx_PortNumber.Text.Trim());
                        server.ServerStartedDate = DateTime.Now;
                        server.IPAddress = "0";
                        server.NumberOfPlayers = 0;
                        server.MaxPlayers = 0;
                        server.LastPingDate = DateTime.Now;

                        GetGameFacade().AddNewGameServer(server);
                    }
                    catch (Exception ex)
                    {
                        m_logger.Error("Error on game editing - Update", ex);
                        throw new Exception("Unable to add server");
                    }
                }
                else  // edit server
                {
                    try
                    {
                        server.ServerId = Convert.ToInt32(hdn_ServerID.Text);
                        server.GameId = SELECTED_GAME_ID;
                        server.VisibiltyId = Convert.ToInt32(drpServerVisibility.SelectedValue);
                        server.GameServerModifiersId = GetUserId();
                        server.ServerName = txtServerName.Text.Trim();
                        server.Port = Convert.ToInt32(tbx_PortNumber.Text.Trim());
                        server.ServerStartedDate = Convert.ToDateTime(hdn_ServerStartedDate.Text);
                        server.IPAddress = hdn_IPAddress.Text;
                        server.NumberOfPlayers = Convert.ToInt32(hdn_NumberOfPlayers.Text);
                        server.MaxPlayers = Convert.ToInt32(hdn_MaxPlayers.Text);
                        server.LastPingDate = Convert.ToDateTime(hdn_LastPingDate.Text);

                        GetGameFacade().UpdateGameServer(server);
                    }
                    catch (Exception ex)
                    {
                        m_logger.Error("Error on game editing - Update", ex);
                        throw new Exception("Unable to update server");
                    }
                }

                //indicated a successful save
                ShowMessage("Your changes were saved successfully.", false);

                //clear the fields
                ServerReset();

                //bindthe data
                BindGame();

            }
            catch (Exception ex)
            {
                ShowMessage(error + ex.Message);
                m_logger.Error("Error on Game Server Management ", ex);
            }
        }

        //---event handlers for the Patch URL Gridview------------------
        protected void dg_PatchURLS_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GridViewRow gvr = dg_PatchURLS.Rows[e.RowIndex];
                GetGameFacade().DeletePatchURLs(SELECTED_GAME_ID,Convert.ToInt32(gvr.Cells[0].Text));
                BindGame();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during patch server deletion", ex);
                ShowMessage("Error during patch server deletion.");
            }
        }

        /// <summary>
        /// btnUpdatePatchURL_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdatePatchURL_Click(Object sender, EventArgs e)
        {
            //set error message variable
            string error = "";

            //validate the required fields
            //validate the required fields
            if (txtPatchURL.Text == "")
            {
                error += "Patch URL is required. ";
                patchURLRequired.Visible = true;
            }
            // Check for any inject scripts in large text fields
            if (SiteManagementCommonFunctions.ContainsInjectScripts(txtPatchURL.Text))
            {
                error += "Script Violation ";
                m_logger.Warn("User " + GetUserId() + " tried to script from IP " + Common.GetVisitorIPAddress());
                ShowErrorOnStartup("Your input contains invalid scripting, please remove script code and try again.", true);
            }
            if (error.Length > 0)
            {
                ShowMessage(error);
                return;
            }

            try
            {
                if (btnUpdatePatchURL.Text.ToUpper().Equals("ADD"))
                {
                    try
                    {
                        GetGameFacade().AddNewPatchURLs(SELECTED_GAME_ID, txtPatchURL.Text);
                    }
                    catch (Exception ex)
                    {
                        m_logger.Error("Error on patch server editing - Add", ex);
                        throw new Exception("Unable to add patch URL");
                    }
                }
                else
                {               
                    try
                    {
                        GetGameFacade().UpdatePatchURLs(SELECTED_GAME_ID,Convert.ToInt32(hdn_patchURLID.Text), txtPatchURL.Text);
                    }
                    catch (Exception ex)
                    {
                        m_logger.Error("Error on patch server editing - Update", ex);
                        throw new Exception("Unable to update patch URL");
                    }
                }

                //indicated a successful save
                ShowMessage("Your changes were saved successfully.", false);

                //clear the fields
                PatchURLReset();

                //bind the data
                BindGame();

            }
            catch (Exception ex)
            {
                ShowMessage(error + ex.Message);
                m_logger.Error("Error on patchURL Management", ex);
            }
        }

        /// <summary>
        /// lbn_NewPatchURL_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbn_NewPatchURL_Click(Object sender, EventArgs e)
        {
            PatchURLPrep();
        }

        /// <summary>
        /// btnPatchURLCancel_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPatchURLCancel_Click(Object sender, EventArgs e)
        {
            PatchURLReset();
        }

        protected void dg_PatchURLS_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
        }

        protected void dg_PatchURLS_OnRowCreated(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            //logic check to work around initial row creation wheere there is only one column
            if (e.Row.Cells.Count > 4)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
            }
        }

        /// <summary>
        /// They clicked to edit the pathc URL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_PatchURLS_RowEditing(object source, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            PatchURLPrep();
            btnUpdatePatchURL.Text = "Update";
            //passin the selected row and populate the fields
            hdn_PatchIndex.Text = e.NewEditIndex.ToString();
            divEditPatchTitle.InnerText = "Update Patch URL";
            PopulatePatchURLEditFields (dg_PatchURLS.Rows[e.NewEditIndex]);
        }

        /// <summary>
        /// They clicked to change pages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_PatchURLS_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            CurrentPatchURLPage = e.NewPageIndex;

            //bind data
            BindPatchURLs();
        }

        /// <summary>
        /// They clicked to sort a column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_PatchURLS_Sorting(object sender, GridViewSortEventArgs e)
        {
            base.gridview_Sorting(sender, e);
            //bind data
            BindPatchURLs();
        }
        
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion

    }
}