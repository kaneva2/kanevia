<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ManageUsers.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.ManageUsers" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    
<ajax:ajaxpanel id="ajMessage" runat="server">

    <asp:label style="position:relative; font-size:18px; padding: 10px 0 10px 0" runat="server" id="messages" visible="true" />

    <table id="tblManageUsers" border="0" cellpadding="0" cellspacing="0" width="980px" style="padding-top:20px">
        <tr>
            <td style="width:980px; vertical-align:top; padding-left:10px">
                <table border="0" cellpadding="4" cellspacing="0" width="970px">
                    <tr>
                        <td ><h3><asp:Label ID="lbl_MyTeam" runat="server" Text="Current Authorized Users"></asp:Label></h3></td>
                    </tr>
                    <tr>
                        <td >
                            <table border="0" rules="cols" cellpadding="4" cellspacing="0" style="background-color:ActiveBorder">
                                <tr>
                                    <td style="width:90px; padding-left:4px"><span style="font-weight:bold; padding-right:10px">username:</span></td>
                                    <td><asp:TextBox ID="txtUsername" style="width:200px" MaxLength="31" runat="server"/></td>
                                    <td style="width:90px; padding-left:20px"><span style="font-weight:bold; padding-right:10px">email:</span></td>
                                    <td><asp:TextBox ID="txtEmail" style="width:200px" MaxLength="31" runat="server"/></td>
                                    <td rowspan="2" style="padding-left:20px"><asp:button id="btnSearch" runat="Server" Text="Search" onClick="btnSearch_Click" CausesValidation="False"/></td>
                                </tr>
                                <tr>
                                    <td style="width:90px; padding-left:4px"><span style="font-weight:bold; padding-right:10px">first name:</span></td>
                                    <td><asp:TextBox ID="txtFirstName" style="width:200px" MaxLength="31" runat="server"/></td>
                                    <td style="width:90px; padding-left:20px"><span style="font-weight:bold; padding-right:10px">last name:</span></td>
                                    <td><asp:TextBox ID="txtLastName" style="width:200px" MaxLength="31" runat="server"/></td>
                                </tr>
                            </table><br />
                        </td>                    
                    </tr>
                    <tr>
                        <td><asp:label id="lblSearch" runat="server" cssclass="insideTextNoBold" /><Kaneva:Pager id="pgTop" MaxPagesToDisplay="5" runat="server" IsAjaxMode="false" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Repeater ID="rpt_MyTeam" runat="server" >
                                <HeaderTemplate>
                                        <table border="0" cellpadding="4" cellspacing="0" width="100%">
                                            <tr style="background-color:#cccccc">
                                                <th align="left">Nickname</th>
                                                <th align="center">Name</th>
                                                <th align="left">Role</th>
					                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <a id="lb_UserName" runat="server" target="_blank" /><input type="hidden" runat="server" id="hidUserId"  />
                                                </td>
                                                <td>
								                    <asp:label id="lbl_RealName" runat="server" visible="true" />
                                                </td>
                                                 <td>
                                                     <asp:DropDownList ID="ddl_KanevaRoles" runat="server" AutoPostBack="true" OnSelectedIndexChanged="KanevaRole_SelectedIndexChanged" />
                                                </td>
                                           </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                            <tr style="background-color:#eeeeee">
                                                <td>
                                                    <a id="lb_UserName" runat="server" target="_blank" /><input type="hidden" runat="server" id="hidUserId"  />
                                                </td>
                                                <td>
								                    <asp:label id="lbl_RealName" runat="server" visible="true" />
                                                </td>
                                                 <td>
                                                     <asp:DropDownList ID="ddl_KanevaRoles" runat="server" AutoPostBack="true" OnSelectedIndexChanged="KanevaRole_SelectedIndexChanged" />
                                                </td>
                                           </tr>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                         </td>
                    </tr>
                    <tr>
                        <td><asp:label id="lblSearch2" runat="server" cssclass="insideTextNoBold" /><Kaneva:Pager id="pgBottom" MaxPagesToDisplay="5" runat="server" IsAjaxMode="false"></Kaneva:Pager></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </ajax:ajaxpanel>
    
    </asp:Panel>     
