using System;
using System.Data;
using System.IO;
using System.Net;
using System.Web.UI.WebControls;
using System.Xml;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class Incubators : BaseUserControl
    {
        #region Declarations
        private bool isAdministraitor = false;
        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            // Check user credentials before displaying page if user is authenticated.
            if (Request.IsAuthenticated)
            {
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.STAR_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        isAdministraitor = true;
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    default:
                        break;
                }

                if (!isAdministraitor)
                {
                    RedirectToHomePage();
                }
                else
                {
                    if (!IsPostBack)
                    {
                        //configure page
                        ConfigurePageForUse();
                    }
                }
            }
            else
            {
                RedirectToHomePage();
            }
        }

        #endregion

        #region Databinding Functions
        /// <summary>
        /// Called to perform initial page configuration upon load.
        /// </summary>
        private void ConfigurePageForUse()
        {
            //bind data for page
            BindIncubators();
        }

        /// <summary>
        /// Called to bind Incubator data to the datagrid.
        /// </summary>
        private void BindIncubators()
        {
            dg_Incubators.PageIndex = CurrentIncubatorPage;
            dg_Incubators.DataSource = GetIncubatorData();
            dg_Incubators.DataBind();
        }

        /// <summary>
        /// Called to bind App Controller data to the datagrid.
        /// </summary>
        private void BindAppControllers()
        {
            dg_AppControllers.PageIndex = CurrentAppControllerPage;
            dg_AppControllers.DataSource = GetAppControllerData(SelectedIncubator);
            dg_AppControllers.DataBind();
        }

        /// <summary>
        /// Called to bind App Server data to the datagrid.
        /// </summary>
        private void BindAppServers()
        {
            dg_AppServers.PageIndex = CurrentAppServerPage;
            dg_AppServers.DataSource = GetAppServerData(SelectedIncubator, SelectedAppController);
            dg_AppServers.DataBind();
        }

        /// <summary>
        /// Called to bind App detail data to the datagrid.
        /// </summary>
        private void BindApps()
        {
            DataRow appData = GetAppData(SelectedIncubator, SelectedApp);
            thAppID.InnerText = appData.Field<string>("appId");
            tdAppSlot.InnerText = appData.Field<string>("templateId");
            tdAppDirectory.InnerText = appData.Field<string>("appDir");
            tdPatchDirectory.InnerText = appData.Field<string>("patchUrl");
            tdAppDBHost.InnerText = appData.Field<string>("dbHost");
            tdAppDBName.InnerText = appData.Field<string>("dbName");
            tdAppPort.InnerText = appData.Field<string>("port");
            tdAppBound.InnerText = appData.Field<string>("bound");
        }

        /// <summary>
        /// Accessor method for the Incubator data.
        /// </summary>
        /// <returns>AppControllers</returns>
        private DataTable GetIncubatorData()
        {
            // Note: This data will be returned from the webservice call in xml format
            // Including the xml manually until implementation of the service.
            string incubatorXML = "";

            // Show/Hide environments based on the environment user is logged into.
            // This is the easiest way to do this for the moment.
            if (Request.Url.Host.ToLower() == "pv-kwas.kaneva.com")
            {
                incubatorXML = @"<?xml version=""1.0"" encoding=""utf-8""?>
                                    <incubators>
                                        <incubator>
                                        <name>pv-robin.kaneva.com</name>
                                        </incubator>
                                    </incubators>";
            }
            else if (Request.Url.Host.ToLower() == "dev-kwas.kaneva.com")
            {
                incubatorXML =    @"<?xml version=""1.0"" encoding=""utf-8""?>
                                    <incubators>
                                        <incubator>
                                        <name>dev-appman1.kaneva.com</name>
                                        </incubator>
                                    </incubators>";
            }
            // Prod gets to see everything.
            else
            {
                incubatorXML = @"<?xml version=""1.0"" encoding=""utf-8""?>
                                    <incubators>
                                        <incubator>
                                        <name>robin.kaneva.com</name>
                                        </incubator>
                                        <incubator>
                                        <name>pv-robin.kaneva.com</name>
                                        </incubator>
                                        <incubator>
                                        <name>dev-appman1.kaneva.com</name>
                                        </incubator>
                                    </incubators>";
            }

            DataSet incubatorList = new DataSet();
            incubatorList.ReadXml(new XmlTextReader(new StringReader(incubatorXML)));
            return incubatorList.Tables[0];
        }

        /// <summary>
        /// Allows access to the app controller data for the currently selected Incubator.
        /// </summary>
        /// <param name="selectedIncubator">The currently selected incubator.</param>
        /// <returns>AppControllers</returns>
        private DataTable GetAppControllerData(string selectedIncubator)
        {
            // Pull the controller data from the service call.
            string url = "http://" + selectedIncubator + ":8080/EnumerateHosts";

            WebRequest request = HttpWebRequest.Create(url);
            request.Timeout = 30000;
            WebResponse response = request.GetResponse();
            XmlDocument doc = new XmlDocument();
            doc.Load(response.GetResponseStream());

            string controllerXML = doc.ChildNodes[0].InnerXml;

            DataSet controllerList = new DataSet();
            controllerList.ReadXml(new XmlTextReader(new StringReader(controllerXML)));
            return controllerList.Tables[1];
        }

        /// <summary>
        /// Allows access to the app server data for the currently selected app controller.
        /// </summary>
        /// <param name="selectedIncubator">The currently selected incubator.</param>
        /// <param name="selectedAppController">The currenlty selected app controller.</param>
        /// <returns>AppControllers</returns>
        private DataTable GetAppServerData(string selectedIncubator, string selectedAppController)
        {
            // Pull the controller data from the service call.
            // TODO: Put postfix stuff in web.config
            string postfix = ".kanevia.com";
            string url = "http://" + selectedAppController + postfix + ":8080/EnumerateServers";

            WebRequest request = HttpWebRequest.Create(url);
            request.Timeout = 30000;
            WebResponse response = request.GetResponse();
            XmlDocument doc = new XmlDocument();
            doc.Load(response.GetResponseStream());

            string serverXML = doc.ChildNodes[0].InnerXml;

            DataSet serverList = new DataSet();
            serverList.ReadXml(new XmlTextReader(new StringReader(serverXML)));
            return serverList.Tables[1];
        }

        /// <summary>
        /// Allows access to the detailed app data for the currently selected App Server.
        /// </summary>
        /// <param name="selectedIncubator">The currenlty selected incubator.</param>
        /// <param name="selectedApp">The currently selected app server.</param>
        /// <returns>Apps</returns>
        private DataRow GetAppData(string selectedIncubator, string selectedApp)
        {
            // Pull the controller data from the service call.
            string url = "http://" + selectedIncubator + ":8080/GetAppSlotInfo?appid=" + selectedApp;

            WebRequest request = HttpWebRequest.Create(url);
            request.Timeout = 30000;
            WebResponse response = request.GetResponse();
            XmlDocument doc = new XmlDocument();
            doc.Load(response.GetResponseStream());

            string appXML = doc.InnerXml;

            DataSet appList = new DataSet();
            appList.ReadXml(new XmlTextReader(new StringReader(appXML)));
            return appList.Tables[0].Rows[0];
        }

        #endregion

        #region Attributes
        /// <summary>
        /// Stores the current page of the Incubator gridview.
        /// </summary>
        /// <returns>int</returns>
        private int CurrentIncubatorPage
        {
            get
            {
                if (ViewState["incubatorPage"] == null)
                {
                    ViewState["incubatorPage"] = 0;
                }
                return (int)ViewState["incubatorPage"];
            }
            set
            {
                ViewState["incubatorPage"] = value;
            }
        }

        /// <summary>
        /// stores the current page of the App Controller gridview
        /// </summary>
        /// <returns>int</returns>
        private int CurrentAppControllerPage
        {
            get
            {
                if (ViewState["appControllerPage"] == null)
                {
                    ViewState["appControllerPage"] = 0;
                }
                return (int)ViewState["appControllerPage"];
            }
            set
            {
                ViewState["appControllerPage"] = value;
            }
        }

        /// <summary>
        /// Stores the current page of the App Server gridview
        /// </summary>
        /// <returns>int</returns>
        private int CurrentAppServerPage
        {
            get
            {
                if (ViewState["appServerPage"] == null)
                {
                    ViewState["appServerPage"] = 0;
                }
                return (int)ViewState["appServerPage"];
            }
            set
            {
                ViewState["appServerPage"] = value;
            }
        }

        /// <summary>
        /// The currently selected Incubator. Stored in viewstate so it is retained across
        /// postbacks.
        /// </summary>
        /// <returns>int</returns>
        private string SelectedIncubator
        {
            get
            {
                return (string)ViewState["selectedIncubator"];
            }
            set
            {
                ViewState["selectedIncubator"] = value;
            }
        }

        /// <summary>
        /// The currently selected App Controller. Stored in viewstate so it is retained across
        /// postbacks.
        /// </summary>
        /// <returns>int</returns>
        private string SelectedAppController
        {
            get
            {
                return (string)ViewState["selectedAppController"];
            }
            set
            {
                ViewState["selectedAppController"] = value;
            }
        }

        /// <summary>
        /// The currently selected App. Stored in viewstate so it is retained across
        /// postbacks.
        /// </summary>
        /// <returns>int</returns>
        private string SelectedApp
        {
            get
            {
                return (string)ViewState["selectedApp"];
            }
            set
            {
                ViewState["selectedApp"] = value;
            }
        }
        #endregion

        #region Event Handlers
        #region Incubator Gridview Event Handlers
        /// <summary>
        /// Triggered when the page index of Incubator data is changed.
        /// </summary>
        /// <param name="sender">dg_Incubators Gridview control.</param>
        /// <param name="e">The event parameters.</param>
        protected void dg_Incubators_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            // Update the page and bind data.
            CurrentIncubatorPage = e.NewPageIndex;
            BindIncubators();
        }

        /// <summary>
        /// Triggered when the sorting functionality of the Incubator datagrid is used.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_Incubators_Sorting(object sender, GridViewSortEventArgs e)
        {
            // Perform the sort and re-bind.
            base.gridview_Sorting(sender, e);
            BindIncubators();
        }
        #endregion

        #region App Controller gridview event handlers
        /// <summary>
        /// Triggered when the selected page index of the App Controller gridview is changed.
        /// </summary>
        /// <param name="sender">dg_AppControllers Gridview control.</param>
        /// <param name="e">Event parameters.</param>
        protected void dg_AppControllers_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            CurrentAppControllerPage = e.NewPageIndex;
            BindAppControllers();
        }

        /// <summary>
        /// Called when the sorting functionality of the App Controller Gridview is invoked.
        /// </summary>
        /// <param name="sender">dg_AppControllers Gridview control.</param>
        /// <param name="e">Event parameters.</param>
        protected void dg_AppControllers_Sorting(object sender, GridViewSortEventArgs e)
        {
            base.gridview_Sorting(sender, e);
            BindAppControllers();
        }
        #endregion

        #region App Server griveview event handlers
        /// <summary>
        /// They clicked to change pages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_AppServers_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            CurrentAppServerPage = e.NewPageIndex;

            //bind data
            BindAppServers();
        }

        /// <summary>
        /// They clicked to sort a column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dg_AppServers_Sorting(object sender, GridViewSortEventArgs e)
        {
            base.gridview_Sorting(sender, e);
            //bind data
            BindAppServers();
        }
        #endregion

        /// <summary>
        /// Command function triggered by mutliple Gridviews.
        /// Handles data population and display.
        /// </summary>
        /// <param name="sender">A Gridview control.</param>
        /// <param name="e">Event parameters.</param>
        protected void FireRowCommand(object sender, GridViewCommandEventArgs e)
        {
            string command = e.CommandName;
            string[] commandArgs = e.CommandArgument.ToString().Split(';');

            switch (command)
            {
                case "ShowAppHosts":
                    appControllerContainer.Visible = true;
                    appServerContainer.Visible = false;
                    appDetailContainer.Visible = false;
                    SelectedIncubator = commandArgs[0];
                    spnSelectedIncubator.InnerText = "App Controllers for: " + SelectedIncubator;
                    BindAppControllers();
                    break;
                case "ShowApps":
                    appServerContainer.Visible = true;
                    appDetailContainer.Visible = false;
                    SelectedAppController = commandArgs[0];
                    spnSelectedAppController.InnerText = "App Servers for: " + SelectedAppController;
                    BindAppServers();
                    break;
                case "ShowAppDetail":
                    appDetailContainer.Visible = true;
                    SelectedApp = commandArgs[0];
                    spnSelectedAppServer.InnerText = "App Details";
                    BindApps();
                    break;
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion

    }
}
