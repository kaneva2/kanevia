<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ItemPreloadList.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.ItemPreloadList" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<script type="text/javascript" language="javascript" src="../../jscript/prototype.js"></script>

<style >
#left td, #left td {padding:10px 5px;}
</style>
 
<center>
<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
 
 <table id="tblMain" border="0" cellpadding="0" cellspacing="0" width="970px" style="background-color:#ffffff">
    <tr>
        <td align="center"><span style="height:30px; font-size:28px; font-weight:bold">Item Preload List</span><br /><asp:Label runat="server" id="ErrorMessages"></asp:Label></td>
    </tr>
	<tr>
        <td style="height:20px"></td>
    </tr>
	<tr>
        <td>
            <asp:dropdownlist runat="server" autopostback="true" id="ddlAllLists" onselectedindexchanged="ddlAllLists_SelectedIndexChanged" width="300px"></asp:dropdownlist>
            <asp:button runat="server" id="btnEditList" text="Edit" onclick="btnEditList_Click" />
            <asp:button runat="server" id="btnPublishList" text="Publish" visible="false" enabled="false" onclick="btnPublishList_Click" onclientclick="return confirm('Publish selected list?');" />
            &nbsp;&nbsp;
            <asp:button runat="server" id="btnCreateList" text="Create New" onclick="btnCreateList_Click" />
        </td>
    </tr>
	<tr>
        <td><asp:label id="lblListTimestamp" runat="server" cssclass="insideTextNoBold" /></td>
    </tr>
	<tr>
        <td>
            <asp:panel id="pnlEditList" runat="server" visible="false" >
                <asp:hiddenfield runat="server" id="hidListId" />
				<table style="font-weight:bold" id="tblEditList" border="0" frame="below" cellpadding="5px" cellspacing="0" width="970">
	                <tr>
                        <td style="height:20px"></td>
                    </tr>
                    <tr>
                        <td align="left" style="width:350px">
                            <asp:Label runat="server" id="lblEditListTitle"></asp:Label>
                        </td>
                    </tr>
					<tr>
					    <td align="left">
                            <label style="color: Red">*</label>List Name:
						    <asp:textbox runat="server" maxlength="125" id="txtListName" width="280px"></asp:textbox>
						    <asp:hiddenfield runat="server" id="hidOldListName"></asp:hiddenfield>
						    <asp:requiredfieldvalidator runat="server" id="rfvListName" controltovalidate="txtListName" validationgroup="GroupEditList" text="* Please enter a value." errormessage="List name is required." display="Dynamic"></asp:requiredfieldvalidator>					    
                            <asp:button runat="server" id="btnSaveList" text="Save" validationgroup="GroupEditList" onclick="btnSaveList_Click" />
					    </td>
					</tr>
				</table>
            </asp:panel>
        </td>
    </tr>
    <tr>
        <td><asp:label style="position:relative; font-size:18px; padding: 10px 0 10px 0" id="lblEditListMessage" runat="server" cssclass="insideTextNoBold" /></td>
    </tr>
	<tr>
        <td style="height:20px"></td>
    </tr>
    <tr>
         <td><asp:Label id="lblListEmpty" visible="true" runat="server"><span style="color:Navy; size:24pt; font-weight:bold">List is empty.</span> </asp:Label></td>
    </tr>
    <tr>
		<td>
            <asp:panel id="pnlListItemMaint" runat="server" visible="false">
            <table border="0" width="98%" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="right" style="padding:6px 0px 6px 6px; font-size:10pt">
                        <b>Enter item global ID: </b> &nbsp;
                        <asp:textbox runat="server" id="txtItemGlobalId"></asp:textbox>&nbsp;
                        <asp:button id="btnAddItem" causesvalidation="false" onclick="btnAddItem_Click" runat="server" text="Add to List"></asp:button>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="padding:6px 0px 6px 6px; font-size:10pt">
                        <asp:dropdownlist runat="server" autopostback="true" id="ddlMergeList" onselectedindexchanged="ddlMergeList_SelectedIndexChanged" width="200px"></asp:dropdownlist>
                        <asp:button id="btnMergeList" causesvalidation="false" enabled="false" onclick="btnMergeList_Click" onclientclick="return confirm('Merge selected list?');" runat="server" text="Merge List"></asp:button>
                    </td>
                </tr>
            </table>
            </asp:panel>

            <asp:gridview id="dgListItems" runat="server" ondatabound="dgListItems_DataBound" onrowdatabound="dgListItems_RowDataBound" onsorting="dgListItems_Sorting" onpageindexchanging="dgListItems_PageIndexChanging" 
				autogeneratecolumns="False" width="970px" allowsorting="True" border="0" cellpadding="2" pagesize="25" allowpaging="True" >
                
                <pagersettings mode="NumericFirstLast" firstpagetext="First" lastpagetext="Last" nextpagetext="Next" previouspagetext="Prev" pagebuttoncount="20" position="TopAndBottom"/> 

				<rowstyle backcolor="White"></rowstyle>
                <headerstyle backcolor="LightGray" font-underline="True" font-bold="True" forecolor="#000000" horizontalalign="Left"></headerstyle>
                <alternatingrowstyle backcolor="Gainsboro"></alternatingrowstyle>
                <footerstyle backcolor="#FFffff"></footerstyle>
                <columns>
                    <asp:boundfield headertext="Item ID" datafield="global_id" sortexpression="global_id" readonly="True" convertemptystringtonull="False" ></asp:boundfield>
                    <asp:boundfield headertext="Item Name" datafield="name" sortexpression="name" readonly="True" convertemptystringtonull="False" ></asp:boundfield>
                    <asp:templatefield headertext="Creator" sortexpression="item_creator_id">
                        <itemtemplate> 
                            <asp:Label ID="Label1" runat="server" text='<%#GetUserName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "item_creator_id")))%>'></asp:Label>
                        </itemtemplate>
                    </asp:templatefield>
                    <asp:boundfield headertext="Item Use Type" datafield="use_type" sortexpression="use_type" readonly="True" convertemptystringtonull="False" ></asp:boundfield>
                    <asp:templatefield headertext="">
                        <itemtemplate> 
                            <asp:linkbutton ID="btnDeleteItem" runat="server" text="Delete" commandargument='<%#DataBinder.Eval(Container.DataItem, "global_id")%>' oncommand="btnDeleteItem_Click"></asp:linkbutton>
                        </itemtemplate>
                    </asp:templatefield>
			    </columns>
            </asp:gridview>
         </td>
    </tr>
</table>

</asp:panel>

</center>
