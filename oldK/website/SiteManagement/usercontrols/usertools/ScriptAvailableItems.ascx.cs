using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using KlausEnt.KEP.SiteManagement;

namespace SiteManagement.usercontrols
{
    public partial class ScriptAvailableItems : BaseUserControl
    {
        #region Declarations

        private int[] insertGlids;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
		{
            if (Request.IsAuthenticated)
            {
                if (!IsPostBack)
                {
                    ResetPage();
                }
            }
            else
            {
                RedirectToHomePage();
            }
		}

        #region Data Manipulation
        /// <summary>
        /// Called to bind data to page elements
        /// </summary>
        protected void BindData()
		{
            int itemsPerPage = 15;

            PagedList<WOKItem> defaultItems = GetGameFacade().GetScriptAvailableItems(pgBottom.CurrentPageNumber, itemsPerPage);
            cblItems.DataSource = defaultItems;
            cblItems.DataBind();

            pgBottom.NumberOfPages = Math.Ceiling((double)defaultItems.TotalCount / itemsPerPage).ToString();
            pgBottom.DrawControl();

            lblSearch.Text = GetResultsText(defaultItems.TotalCount, pgBottom.CurrentPageNumber, itemsPerPage, defaultItems.Count);

            if (defaultItems.Count > 0)
            {
                btnDeleteSelected.Visible = true;
            }
		}

        /// <summary>
        /// Called to reset the page
        /// </summary>
        protected void ResetPage(int pageNum = 1)
        {
            divErrorData.InnerText = string.Empty;
            txtItems.Text = string.Empty;
            pgBottom.CurrentPageNumber = pageNum;

            BindData();
        }

        /// <summary>
        /// Called to validate the page before saving
        /// </summary>
        protected bool ValidatePage()
        {
            bool inputsAreValid = true;

            try
            {
                string[] glidStrs = txtItems.Text.Trim(',')
                    .Replace(" ", "")
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Distinct()
                    .ToArray();

                if (glidStrs.Length > 0)
                {
                    InsertGlids = Array.ConvertAll(glidStrs, new Converter<string, int>(Convert.ToInt32));
                }
            }
            catch (Exception)
            {
                inputsAreValid = false;
            }

            return inputsAreValid;
        }
        #endregion

        #region Event Handlers
        protected void btnDeleteSelected_Click(object sender, EventArgs e)
		{
            List<int> selectedValues = new List<int>();
            foreach (ListItem item in cblItems.Items)
            {
                if (item.Selected)
                {
                    selectedValues.Add(Int32.Parse(item.Value));
                }
            }

            if (selectedValues.Count > 0)
            {
                GetGameFacade().DeleteScriptAvailableItemsIds(selectedValues.ToArray());
                ResetPage();
            }
		}

		protected void btnAddItems_Click(object sender, EventArgs e)
		{
            if (Page.IsValid && ValidatePage() && InsertGlids.Length > 0)
            {
                int recordsInserted = GetGameFacade().InsertScriptAvailableItemsIds(InsertGlids);
                if (recordsInserted <= 0)
                {
                    divErrorData.InnerText = "Unable to add items. Check to ensure that globalIds are correct.";
                }
                else
                {
                    ResetPage();
                }
            }
            else
            {
                divErrorData.InnerText = "Invalid inputs.";
            }
		}

        protected void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            ResetPage(e.PageNumber);
        }

        #endregion

        #region Properties

        public int[] InsertGlids
        {
            get
            {
                if (insertGlids == null)
                {
                    insertGlids = new int[0];
                    return insertGlids;
                }
                return insertGlids;
            }
            set
            {
                insertGlids = value;
            }
        }

        #endregion
    }  
}