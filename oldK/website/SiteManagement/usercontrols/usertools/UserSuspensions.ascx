<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="UserSuspensions.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.UserSuspensions" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<script type="text/javascript" src="jscript/yahoo/yahoo-min.js"></script>  
<script type="text/javascript" src="jscript/yahoo/event-min.js" ></script>   
<script type="text/javascript" src="jscript/yahoo/dom-min.js" ></script>   
<script type="text/javascript" src="jscript/yahoo/calendar-min.js"></script>

<link type="text/css" rel="stylesheet" href="css/yahoo/calendar.css">   

<style >
	#cal1Container { display:none; position:absolute; z-index:100;}
	#cal2Container { display:none; position:absolute; z-index:100;}
    #cal3Container { display:none; position:absolute; z-index:100;}
</style>

<script type="text/javascript"><!--
 var txtIPBanEndDate_ID = '<%= txtIPBanEndDate.ClientID %>';
 var txtBanEndDate_ID = '<%= txtBanEndDate.ClientID %>';
 var txtSystemBanEndDate_ID = '<%= txtSystemBanEndDate.ClientID %>';

YAHOO.namespace("example.calendar");
    
	function handleIPBanEndDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate1 = document.getElementById(txtIPBanEndDate_ID);
		txtDate1.value = month + "/" + day + "/" + year;
		YAHOO.example.calendar.cal1.hide();
	}
	
	function handleSuspensionEndDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate2 = document.getElementById(txtBanEndDate_ID);
		txtDate2.value = month + "/" + day + "/" + year;
		YAHOO.example.calendar.cal2.hide();
	}

	function handleSystemSuspensionEndDate(type, args, obj) {
	    var dates = args[0];
	    var date = dates[0];
	    var year = date[0], month = date[1], day = date[2];

	    var txtDate3 = document.getElementById(txtSystemBanEndDate_ID);
	    txtDate3.value = month + "/" + day + "/" + year;
	    YAHOO.example.calendar.cal3.hide();
	}

	function initIPBanEnd() {
		// Admin Calendar
		YAHOO.example.calendar.cal1 = new YAHOO.widget.Calendar ("cal1", "cal1Container", { iframe:true, zIndex:200, mindate:"1/1/2008", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal1.render();
		
		// Listener to show Admin Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgSelectIPEndDate", "click", YAHOO.example.calendar.cal1.show, YAHOO.example.calendar.cal1, true);   
		YAHOO.example.calendar.cal1.selectEvent.subscribe(handleIPBanEndDate, YAHOO.example.calendar.cal1, true);
	}
	
	function initSuspensionEnd() {
		// Event Calendar
		YAHOO.example.calendar.cal2 = new YAHOO.widget.Calendar ("cal2", "cal2Container", { iframe:true, zIndex:1000, mindate:"1/1/2008", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal2.render();
		
		// Listener to show the Event Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgSelectEndDate", "click", YAHOO.example.calendar.cal2.show, YAHOO.example.calendar.cal2, true);   
		YAHOO.example.calendar.cal2.selectEvent.subscribe(handleSuspensionEndDate, YAHOO.example.calendar.cal2, true);
	}

	function initSystemSuspensionEnd() {
	    // Event Calendar
	    YAHOO.example.calendar.cal3 = new YAHOO.widget.Calendar("cal3", "cal3Container", { iframe: true, zIndex: 1000, mindate: "1/1/2008", title: "Choose a date:", close: true });
	    YAHOO.example.calendar.cal3.render();

	    // Listener to show the Event Calendar when the button is clicked   
	    YAHOO.util.Event.addListener("imgSelectSBEndDate", "click", YAHOO.example.calendar.cal3.show, YAHOO.example.calendar.cal3, true);
	    YAHOO.example.calendar.cal3.selectEvent.subscribe(handleSystemSuspensionEndDate, YAHOO.example.calendar.cal3, true);
	}
//--> </script>  

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
<TABLE cellSpacing=0 cellPadding=0 width="980px" border="0" style="padding-left:10px">
	<TBODY>
	<TR>
		<TD>
		 
			<fieldset title="Suspension Status" style="padding:0px 14px 10px 14px;width:934px;">
				<legend>Suspension Status</legend>
				
				<br style="line-height:10px;"/>
				
				<!-- User Status -->
				<div style="width:270px; float:left;">
					Current Status: <asp:Label ID="lbl_UserStatus" runat="server"></asp:Label>  
				</div>
				<!-- Update Status -->
				<div style="width:300px; float:left;" >
					<asp:DropDownList ID="drp_Statuses" runat="server"></asp:DropDownList> 
					<asp:Button ID="btnUpdateStatus" runat="server" Text="Update Status" OnClick="btn_StatusUpdate_Click" OnClientClick="return confirm('Are you sure you want to change this status?');"/>
				</div>
			    
				<div style="width:300px; float:left;">
					<table>
					<tr>
						<td valign="top">Notes: </td>
						<td>                                
							<asp:TextBox ID="txtStatusChangeReason" runat="server" TextMode="MultiLine" Rows="2" Width="300"></asp:TextBox>                           
						</td>
					</tr>
					</table>    
				</div>
			    
				<div style="width:100%; clear:both;">
					<asp:Label id="lblStatusMsg" runat="server" ForeColor="red" Font-Names="Arial"></asp:Label> 
				</div>
			</fieldset>
		    
			<br />
		 
			<fieldset title="New Suspension" style="padding:0px 14px 10px 14px;width:934px;">
				<legend>New Suspension</legend>   

				<br style="line-height:10px;"/>
					        
				<table cellpadding="0" cellspacing="6" border="0">
					<tr>
						<td>Reason: </td>
						<td>
							<asp:DropDownList ID="drpBanReasons" runat="server">
								<asp:ListItem>Underage User</asp:ListItem>
								<asp:ListItem>Spammer</asp:ListItem>
								<asp:ListItem>Inappropriate Content</asp:ListItem>         
								<asp:ListItem>Cyberbullying</asp:ListItem> 
								<asp:ListItem>Copyright Violation</asp:ListItem>
								<asp:ListItem>Scams</asp:ListItem>     
								<asp:ListItem>Designer Catalog</asp:ListItem>  
								<asp:ListItem>Other</asp:ListItem>  
							</asp:DropDownList>
						</td>            
					</tr>
					<tr>
						<td valign="top">Notes: </td>
						<td>                                
							<asp:TextBox ID="txtBanReasonNotes" runat="server" TextMode="MultiLine" Rows="2" Width="400"></asp:TextBox>                           
						</td>
					</tr>
				</table>
			        
				<br />    
		        
				<div style="margin-left:6px;">
					Suspension length: 
					<asp:DropDownList ID="drpBanLength" runat="server" OnSelectedIndexChanged="drpBanLength_Selected" AutoPostBack="True">
						<asp:ListItem Value="1">1</asp:ListItem>
						<asp:ListItem Value="2">2</asp:ListItem>
						<asp:ListItem Value="3">3</asp:ListItem>
						<asp:ListItem Value="4">4</asp:ListItem>
						<asp:ListItem Value="5">5</asp:ListItem>
						<asp:ListItem Value="6">6</asp:ListItem>
						<asp:ListItem Value="7">7</asp:ListItem>
						<asp:ListItem Value="Perm">Permanent</asp:ListItem>    
					</asp:DropDownList> Days
					<div id="cal2Container"></div>   
				</div>
				
				<br />
		    
				<div style="float:left; padding:10px 20px 10px 6px;">
					Start Date/Time: <br />                                
					<asp:TextBox enabled="false" ID="txtBanStartDate" class="formKanevaText" style="width:80px" MaxLength="10" runat="server"/>                            
					<asp:Dropdownlist runat="server" ID="drpBanStartTime" style="width: 80px; vertical-align: top;" Enabled="false">
						<asp:ListItem value="00:00:00">12:00 AM</asp:ListItem >
						<asp:ListItem value="00:30:00">12:30 AM</asp:ListItem >
						<asp:ListItem value="01:00:00">1:00 AM</asp:ListItem >
						<asp:ListItem value="01:30:00">1:30 AM</asp:ListItem >
						<asp:ListItem value="02:00:00">2:00 AM</asp:ListItem >
						<asp:ListItem value="02:30:00">2:30 AM</asp:ListItem >
						<asp:ListItem value="03:00:00">3:00 AM</asp:ListItem >
						<asp:ListItem value="03:30:00">3:30 AM</asp:ListItem >
						<asp:ListItem value="04:00:00">4:00 AM</asp:ListItem >
						<asp:ListItem value="04:30:00">4:30 AM</asp:ListItem >
						<asp:ListItem value="05:00:00">5:00 AM</asp:ListItem >
						<asp:ListItem value="05:30:00">5:30 AM</asp:ListItem >
						<asp:ListItem value="06:00:00">6:00 AM</asp:ListItem >
						<asp:ListItem value="06:30:00">6:30 AM</asp:ListItem >
						<asp:ListItem value="07:00:00">7:00 AM</asp:ListItem >
						<asp:ListItem value="07:30:00">7:30 AM</asp:ListItem >
						<asp:ListItem value="08:00:00">8:00 AM</asp:ListItem >
						<asp:ListItem value="08:30:00">8:30 AM</asp:ListItem >
						<asp:ListItem value="09:00:00">9:00 AM</asp:ListItem >
						<asp:ListItem value="09:30:00">9:30 AM</asp:ListItem >
						<asp:ListItem value="10:00:00">10:00 AM</asp:ListItem >
						<asp:ListItem value="10:30:00">10:30 AM</asp:ListItem >
						<asp:ListItem value="11:00:00">11:00 AM</asp:ListItem >
						<asp:ListItem value="11:30:00">11:30 AM</asp:ListItem >
						<asp:ListItem value="12:00:00">12:00 PM</asp:ListItem >
						<asp:ListItem value="12:30:00">12:30 PM</asp:ListItem >
						<asp:ListItem value="13:00:00">1:00 PM</asp:ListItem >
						<asp:ListItem value="13:30:00">1:30 PM</asp:ListItem >
						<asp:ListItem value="14:00:00">2:00 PM</asp:ListItem >
						<asp:ListItem value="14:30:00">2:30 PM</asp:ListItem >
						<asp:ListItem value="15:00:00">3:00 PM</asp:ListItem >
						<asp:ListItem value="15:30:00">3:30 PM</asp:ListItem >
						<asp:ListItem value="16:00:00">4:00 PM</asp:ListItem >
						<asp:ListItem value="16:30:00">4:30 PM</asp:ListItem >
						<asp:ListItem value="17:00:00">5:00 PM</asp:ListItem >
						<asp:ListItem value="17:30:00">5:30 PM</asp:ListItem >
						<asp:ListItem value="18:00:00">6:00 PM</asp:ListItem >
						<asp:ListItem value="18:30:00">6:30 PM</asp:ListItem >
						<asp:ListItem value="19:00:00">7:00 PM</asp:ListItem >
						<asp:ListItem value="19:30:00">7:30 PM</asp:ListItem >
						<asp:ListItem value="20:00:00">8:00 PM</asp:ListItem >
						<asp:ListItem value="20:30:00">8:30 PM</asp:ListItem >
						<asp:ListItem value="21:00:00">9:00 PM</asp:ListItem >
						<asp:ListItem value="21:30:00">9:30 PM</asp:ListItem >
						<asp:ListItem value="22:00:00">10:00 PM</asp:ListItem >
						<asp:ListItem value="22:30:00">10:30 PM</asp:ListItem >
						<asp:ListItem value="23:00:00">11:00 PM</asp:ListItem >
						<asp:ListItem value="23:30:00">11:30 PM</asp:ListItem >
					</asp:Dropdownlist>&nbsp;EST						
					<asp:RequiredFieldValidator runat="server" id="rfvStartDate" controltovalidate="txtBanStartDate" 
					text="* Please enter a a value." errormessage="The promotion start date is required." display="Dynamic"></asp:RequiredFieldValidator>                                                                                                       
				</div>
		        
				<div  style="float:left; padding:10px 20px 10px 0px;">
					End Date/Time: <br />              
					<asp:TextBox  ID="txtBanEndDate" class="formKanevaText" style="width:80px" MaxLength="10" runat="server" />
					<img id="imgSelectEndDate" name="imgSelectEndDate" onload="initSuspensionEnd();" src="images/cal_16.gif"/> 
					<asp:Dropdownlist runat="server" ID="drpBanEndTime" style="width: 80px; vertical-align:top;" >
						<asp:ListItem value="00:00:00">12:00 AM</asp:ListItem >
						<asp:ListItem value="00:30:00">12:30 AM</asp:ListItem >
						<asp:ListItem value="01:00:00">1:00 AM</asp:ListItem >
						<asp:ListItem value="01:30:00">1:30 AM</asp:ListItem >
						<asp:ListItem value="02:00:00">2:00 AM</asp:ListItem >
						<asp:ListItem value="02:30:00">2:30 AM</asp:ListItem >
						<asp:ListItem value="03:00:00">3:00 AM</asp:ListItem >
						<asp:ListItem value="03:30:00">3:30 AM</asp:ListItem >
						<asp:ListItem value="04:00:00">4:00 AM</asp:ListItem >
						<asp:ListItem value="04:30:00">4:30 AM</asp:ListItem >
						<asp:ListItem value="05:00:00">5:00 AM</asp:ListItem >
						<asp:ListItem value="05:30:00">5:30 AM</asp:ListItem >
						<asp:ListItem value="06:00:00">6:00 AM</asp:ListItem >
						<asp:ListItem value="06:30:00">6:30 AM</asp:ListItem >
						<asp:ListItem value="07:00:00">7:00 AM</asp:ListItem >
						<asp:ListItem value="07:30:00">7:30 AM</asp:ListItem >
						<asp:ListItem value="08:00:00">8:00 AM</asp:ListItem >
						<asp:ListItem value="08:30:00">8:30 AM</asp:ListItem >
						<asp:ListItem value="09:00:00">9:00 AM</asp:ListItem >
						<asp:ListItem value="09:30:00">9:30 AM</asp:ListItem >
						<asp:ListItem value="10:00:00">10:00 AM</asp:ListItem >
						<asp:ListItem value="10:30:00">10:30 AM</asp:ListItem >
						<asp:ListItem value="11:00:00">11:00 AM</asp:ListItem >
						<asp:ListItem value="11:30:00">11:30 AM</asp:ListItem >
						<asp:ListItem value="12:00:00">12:00 PM</asp:ListItem >
						<asp:ListItem value="12:30:00">12:30 PM</asp:ListItem >
						<asp:ListItem value="13:00:00">1:00 PM</asp:ListItem >
						<asp:ListItem value="13:30:00">1:30 PM</asp:ListItem >
						<asp:ListItem value="14:00:00">2:00 PM</asp:ListItem >
						<asp:ListItem value="14:30:00">2:30 PM</asp:ListItem >
						<asp:ListItem value="15:00:00">3:00 PM</asp:ListItem >
						<asp:ListItem value="15:30:00">3:30 PM</asp:ListItem >
						<asp:ListItem value="16:00:00">4:00 PM</asp:ListItem >
						<asp:ListItem value="16:30:00">4:30 PM</asp:ListItem >
						<asp:ListItem value="17:00:00">5:00 PM</asp:ListItem >
						<asp:ListItem value="17:30:00">5:30 PM</asp:ListItem >
						<asp:ListItem value="18:00:00">6:00 PM</asp:ListItem >
						<asp:ListItem value="18:30:00">6:30 PM</asp:ListItem >
						<asp:ListItem value="19:00:00">7:00 PM</asp:ListItem >
						<asp:ListItem value="19:30:00">7:30 PM</asp:ListItem >
						<asp:ListItem value="20:00:00">8:00 PM</asp:ListItem >
						<asp:ListItem value="20:30:00">8:30 PM</asp:ListItem >
						<asp:ListItem value="21:00:00">9:00 PM</asp:ListItem >
						<asp:ListItem value="21:30:00">9:30 PM</asp:ListItem >
						<asp:ListItem value="22:00:00">10:00 PM</asp:ListItem >
						<asp:ListItem value="22:30:00">10:30 PM</asp:ListItem >
						<asp:ListItem value="23:00:00">11:00 PM</asp:ListItem >
						<asp:ListItem value="23:30:00">11:30 PM</asp:ListItem >
					</asp:Dropdownlist>&nbsp;EST
				</div>  
		                                                                                       
				<div  style="float:left; padding:10px 20px 10px 0px;">
					<br />
					<asp:Button id="btn_Add" onclick="btn_Add_Click" Text="Add Suspension" runat="server" OnClientClick="return confirm('Are you sure you want to ban this user?');"></asp:Button>  
					<asp:Label id="lbl_Messages" runat="server" ForeColor="red" Font-Names="Arial"></asp:Label> 
				</div>
		 
			</fieldset>

			<br />

            <fieldset title="User Systems" style="padding:14px;width:934px;">
				<legend>User Systems</legend>
                <label for="lboxUserSystems" style="vertical-align: top;">Systems:</label>
                <asp:ListBox id="lboxUserSystems" runat="server" onselectedindexchanged="lboxUserSystems_SelectedIndexChanged" 
                    autopostback="true" height="200" clientidmode="Static" style="margin-right: 80px;"></asp:ListBox>
                <label for="lboxUserSystems" style="vertical-align: top;">System Users:</label>
                <asp:ListBox id="lboxSystemUsers" runat="server" height="200" clientidmode="Static"></asp:ListBox>
                <br />
                <div style="margin-left:6px;margin-top:20px;">
                    <label for="ddlSystemBanReason" style="vertical-align: top;">Reason:</label>
				    <asp:DropDownList ID="ddlSystemBanReason" runat="server" clientidmode="Static">
					    <asp:ListItem>Underage User</asp:ListItem>
					    <asp:ListItem>Spammer</asp:ListItem>
					    <asp:ListItem>Inappropriate Content</asp:ListItem>         
					    <asp:ListItem>Cyberbullying</asp:ListItem> 
					    <asp:ListItem>Copyright Violation</asp:ListItem>
					    <asp:ListItem>Scams</asp:ListItem>     
					    <asp:ListItem>Designer Catalog</asp:ListItem>  
					    <asp:ListItem>Other</asp:ListItem>  
				    </asp:DropDownList>
				</div>
                <div style="margin-left:6px;margin-top:20px;margin-bottom:10px;">
					Suspension length: 
					<asp:DropDownList ID="ddlSystemBanLength" runat="server" OnSelectedIndexChanged="ddlSystemBanLength_Selected" AutoPostBack="True">
						<asp:ListItem Value="1">1</asp:ListItem>
						<asp:ListItem Value="2">2</asp:ListItem>
						<asp:ListItem Value="3">3</asp:ListItem>
						<asp:ListItem Value="4">4</asp:ListItem>
						<asp:ListItem Value="5">5</asp:ListItem>
						<asp:ListItem Value="6">6</asp:ListItem>
						<asp:ListItem Value="7">7</asp:ListItem>
						<asp:ListItem Value="Perm">Permanent</asp:ListItem>    
					</asp:DropDownList> Days
					<div id="cal3Container"></div>   
				</div>
                <div style="float:left; padding:10px 20px 10px 6px;">
					Start Date/Time: <br />                                
					<asp:TextBox enabled="false" ID="txtSystemBanStartDate" class="formKanevaText" style="width:80px" MaxLength="10" runat="server"/>                            
					<asp:Dropdownlist runat="server" ID="ddlSystemBanStartTime" style="width: 80px; vertical-align: top;" Enabled="false">
						<asp:ListItem value="00:00:00">12:00 AM</asp:ListItem >
						<asp:ListItem value="00:30:00">12:30 AM</asp:ListItem >
						<asp:ListItem value="01:00:00">1:00 AM</asp:ListItem >
						<asp:ListItem value="01:30:00">1:30 AM</asp:ListItem >
						<asp:ListItem value="02:00:00">2:00 AM</asp:ListItem >
						<asp:ListItem value="02:30:00">2:30 AM</asp:ListItem >
						<asp:ListItem value="03:00:00">3:00 AM</asp:ListItem >
						<asp:ListItem value="03:30:00">3:30 AM</asp:ListItem >
						<asp:ListItem value="04:00:00">4:00 AM</asp:ListItem >
						<asp:ListItem value="04:30:00">4:30 AM</asp:ListItem >
						<asp:ListItem value="05:00:00">5:00 AM</asp:ListItem >
						<asp:ListItem value="05:30:00">5:30 AM</asp:ListItem >
						<asp:ListItem value="06:00:00">6:00 AM</asp:ListItem >
						<asp:ListItem value="06:30:00">6:30 AM</asp:ListItem >
						<asp:ListItem value="07:00:00">7:00 AM</asp:ListItem >
						<asp:ListItem value="07:30:00">7:30 AM</asp:ListItem >
						<asp:ListItem value="08:00:00">8:00 AM</asp:ListItem >
						<asp:ListItem value="08:30:00">8:30 AM</asp:ListItem >
						<asp:ListItem value="09:00:00">9:00 AM</asp:ListItem >
						<asp:ListItem value="09:30:00">9:30 AM</asp:ListItem >
						<asp:ListItem value="10:00:00">10:00 AM</asp:ListItem >
						<asp:ListItem value="10:30:00">10:30 AM</asp:ListItem >
						<asp:ListItem value="11:00:00">11:00 AM</asp:ListItem >
						<asp:ListItem value="11:30:00">11:30 AM</asp:ListItem >
						<asp:ListItem value="12:00:00">12:00 PM</asp:ListItem >
						<asp:ListItem value="12:30:00">12:30 PM</asp:ListItem >
						<asp:ListItem value="13:00:00">1:00 PM</asp:ListItem >
						<asp:ListItem value="13:30:00">1:30 PM</asp:ListItem >
						<asp:ListItem value="14:00:00">2:00 PM</asp:ListItem >
						<asp:ListItem value="14:30:00">2:30 PM</asp:ListItem >
						<asp:ListItem value="15:00:00">3:00 PM</asp:ListItem >
						<asp:ListItem value="15:30:00">3:30 PM</asp:ListItem >
						<asp:ListItem value="16:00:00">4:00 PM</asp:ListItem >
						<asp:ListItem value="16:30:00">4:30 PM</asp:ListItem >
						<asp:ListItem value="17:00:00">5:00 PM</asp:ListItem >
						<asp:ListItem value="17:30:00">5:30 PM</asp:ListItem >
						<asp:ListItem value="18:00:00">6:00 PM</asp:ListItem >
						<asp:ListItem value="18:30:00">6:30 PM</asp:ListItem >
						<asp:ListItem value="19:00:00">7:00 PM</asp:ListItem >
						<asp:ListItem value="19:30:00">7:30 PM</asp:ListItem >
						<asp:ListItem value="20:00:00">8:00 PM</asp:ListItem >
						<asp:ListItem value="20:30:00">8:30 PM</asp:ListItem >
						<asp:ListItem value="21:00:00">9:00 PM</asp:ListItem >
						<asp:ListItem value="21:30:00">9:30 PM</asp:ListItem >
						<asp:ListItem value="22:00:00">10:00 PM</asp:ListItem >
						<asp:ListItem value="22:30:00">10:30 PM</asp:ListItem >
						<asp:ListItem value="23:00:00">11:00 PM</asp:ListItem >
						<asp:ListItem value="23:30:00">11:30 PM</asp:ListItem >
					</asp:Dropdownlist>&nbsp;EST						
					<asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator1" controltovalidate="txtBanStartDate" 
					text="* Please enter a a value." errormessage="The start date is required." display="Dynamic"></asp:RequiredFieldValidator>                                                                                                       
				</div>	        
				<div  style="float:left; padding:10px 20px 10px 0px;">
					End Date/Time: <br />              
					<asp:TextBox  ID="txtSystemBanEndDate" class="formKanevaText" style="width:80px" MaxLength="10" runat="server" />
					<img id="imgSelectSBEndDate" name="imgSelectSBEndDate" onload="initSystemSuspensionEnd();" src="images/cal_16.gif"/> 
					<asp:Dropdownlist runat="server" ID="ddlSystemBanEndTime" style="width: 80px; vertical-align:top;" >
						<asp:ListItem value="00:00:00">12:00 AM</asp:ListItem >
						<asp:ListItem value="00:30:00">12:30 AM</asp:ListItem >
						<asp:ListItem value="01:00:00">1:00 AM</asp:ListItem >
						<asp:ListItem value="01:30:00">1:30 AM</asp:ListItem >
						<asp:ListItem value="02:00:00">2:00 AM</asp:ListItem >
						<asp:ListItem value="02:30:00">2:30 AM</asp:ListItem >
						<asp:ListItem value="03:00:00">3:00 AM</asp:ListItem >
						<asp:ListItem value="03:30:00">3:30 AM</asp:ListItem >
						<asp:ListItem value="04:00:00">4:00 AM</asp:ListItem >
						<asp:ListItem value="04:30:00">4:30 AM</asp:ListItem >
						<asp:ListItem value="05:00:00">5:00 AM</asp:ListItem >
						<asp:ListItem value="05:30:00">5:30 AM</asp:ListItem >
						<asp:ListItem value="06:00:00">6:00 AM</asp:ListItem >
						<asp:ListItem value="06:30:00">6:30 AM</asp:ListItem >
						<asp:ListItem value="07:00:00">7:00 AM</asp:ListItem >
						<asp:ListItem value="07:30:00">7:30 AM</asp:ListItem >
						<asp:ListItem value="08:00:00">8:00 AM</asp:ListItem >
						<asp:ListItem value="08:30:00">8:30 AM</asp:ListItem >
						<asp:ListItem value="09:00:00">9:00 AM</asp:ListItem >
						<asp:ListItem value="09:30:00">9:30 AM</asp:ListItem >
						<asp:ListItem value="10:00:00">10:00 AM</asp:ListItem >
						<asp:ListItem value="10:30:00">10:30 AM</asp:ListItem >
						<asp:ListItem value="11:00:00">11:00 AM</asp:ListItem >
						<asp:ListItem value="11:30:00">11:30 AM</asp:ListItem >
						<asp:ListItem value="12:00:00">12:00 PM</asp:ListItem >
						<asp:ListItem value="12:30:00">12:30 PM</asp:ListItem >
						<asp:ListItem value="13:00:00">1:00 PM</asp:ListItem >
						<asp:ListItem value="13:30:00">1:30 PM</asp:ListItem >
						<asp:ListItem value="14:00:00">2:00 PM</asp:ListItem >
						<asp:ListItem value="14:30:00">2:30 PM</asp:ListItem >
						<asp:ListItem value="15:00:00">3:00 PM</asp:ListItem >
						<asp:ListItem value="15:30:00">3:30 PM</asp:ListItem >
						<asp:ListItem value="16:00:00">4:00 PM</asp:ListItem >
						<asp:ListItem value="16:30:00">4:30 PM</asp:ListItem >
						<asp:ListItem value="17:00:00">5:00 PM</asp:ListItem >
						<asp:ListItem value="17:30:00">5:30 PM</asp:ListItem >
						<asp:ListItem value="18:00:00">6:00 PM</asp:ListItem >
						<asp:ListItem value="18:30:00">6:30 PM</asp:ListItem >
						<asp:ListItem value="19:00:00">7:00 PM</asp:ListItem >
						<asp:ListItem value="19:30:00">7:30 PM</asp:ListItem >
						<asp:ListItem value="20:00:00">8:00 PM</asp:ListItem >
						<asp:ListItem value="20:30:00">8:30 PM</asp:ListItem >
						<asp:ListItem value="21:00:00">9:00 PM</asp:ListItem >
						<asp:ListItem value="21:30:00">9:30 PM</asp:ListItem >
						<asp:ListItem value="22:00:00">10:00 PM</asp:ListItem >
						<asp:ListItem value="22:30:00">10:30 PM</asp:ListItem >
						<asp:ListItem value="23:00:00">11:00 PM</asp:ListItem >
						<asp:ListItem value="23:30:00">11:30 PM</asp:ListItem >
					</asp:Dropdownlist>&nbsp;EST
				</div>  		                                                                                       
				<div  style="float:left; padding:10px 20px 10px 0px;">
					<br />
					<asp:Button id="btnAddSystemBan" onclick="btnAddSystemBan_Click" Text="Add System Suspension" runat="server" OnClientClick="return confirm('Are you sure you want to ban this system and all associated users?');"></asp:Button>  
					<asp:Label id="lblSystemBanError" runat="server" ForeColor="red" Font-Names="Arial"></asp:Label> 
				</div>
                <asp:DataGrid 
			    id="dgSystemBans" runat="server" OnDeleteCommand="dgSystemBans_DeleteCommand" OnCancelCommand="dgSystemBans_CancelCommand"
			    AutoGenerateColumns="False" border="0" cellpadding="2" Width="600" style="margin-top:22px;">
		    
				    <AlternatingItemStyle CssClass="lineItemOddCSR"></AlternatingItemStyle>
				    <ItemStyle CssClass="lineItemEvenCSR"></ItemStyle>
				    <HeaderStyle CssClass="lineItemColHeadCSR"></HeaderStyle>
			    
				    <Columns>
					    <asp:BoundColumn DataField="SystemBanId" HeaderText="Ban Id" ReadOnly="True">
					        <HeaderStyle HorizontalAlign="Left" Font-Bold="True" Width="50" ></HeaderStyle>
					    </asp:BoundColumn>
				    
					    <asp:BoundColumn DataField="BanStartDate" HeaderText="Start Date" ReadOnly="True">
					        <HeaderStyle HorizontalAlign="Left" Font-Bold="True" Width="250"></HeaderStyle>
					    </asp:BoundColumn>
				    
					    <asp:BoundColumn DataField="BanEndDate" HeaderText="End Date" ReadOnly="True">
					        <HeaderStyle HorizontalAlign="Left" Font-Bold="True" width="250"></HeaderStyle>
					    </asp:BoundColumn>
				    
					    <asp:ButtonColumn ButtonType="LinkButton" Text="Delete"  CommandName="Delete">
					        <HeaderStyle HorizontalAlign="Left" Font-Bold="True" width="50"></HeaderStyle>
					    </asp:ButtonColumn>
				    </Columns>
			
			    </asp:DataGrid> 
			</fieldset>

            <br />

			<fieldset title="IP Ban" style="padding:0px 14px 10px 14px;width:934px;">
				<legend>IP Ban</legend>

				<br style="line-height:10px;"/>						 

				<div style="width:100%;text-align:center;">
				<span id="spnIPBanAlertMsg" runat="server" class="errBox black" style="margin-bottom:14px;display:none;"></span>
				</div>
													
				<div style="margin-left:6px;width:300px;border-color:DarkGray;border-width:1px;border-style:Solid;font-size:12px;float:left;">
					<div id="divUsername" runat="server" style="width:100%;padding:4px;color:Black;background-color:LightGrey;font-weight:bold;">Username</div>	
					<table cellpadding="4" cellspacing="0" border="0" width="100%" style="font-size:12px;">
						<tr>
							<td align="left"><asp:checkbox id="chkLastIP" runat="server" style="float:left;"></asp:checkbox></td>
							<td align="center"><asp:linkbutton id="lbUnBanLastIP" oncommand="lbUnBanIP_Click" runat="server" style="color:Red;">un-ban</asp:linkbutton></td>
							<td align="right">(last ip)</td>
						</tr>
						<tr>
							<td align="left"><asp:checkbox id="chkIP" runat="server" style="float:left;"></asp:checkbox></td>
							<td align="center"><asp:linkbutton id="lbUnBanIP" oncommand="lbUnBanIP_Click" runat="server" style="color:Red;">un-ban</un-ban></asp:linkbutton></td>
							<td align="right">(ip address)</td>
						</tr>
					</table>										  
				</div>
				
				<div style="margin-right:6px;width:300px; height:350px; overflow:auto; border-color:DarkGray;border-width:1px;border-style:Solid;font-size:12px; float:right">
					<div style="width:90%;padding:4px;color:Black;background-color:LightGrey;font-weight:bold;">Related Accounts</div>	
					
					<asp:repeater id="rptRelatedIPs" runat="server">
						
						<itemtemplate>
							<div style="width:90%;padding:4px;"><span style="width:50%;"><%# DataBinder.Eval (Container.DataItem, "last_ip_address").ToString () %>&nbsp;-&nbsp;</span><span style="width:50%; color:Blue"><asp:LinkButton runat="server" style="COLOR: #3258ba;" OnCommand="switchSelectedUser" CausesValidation="false" CommandArgument='<%# DataBinder.Eval (Container.DataItem, "user_id").ToString () %>' ><%# DataBinder.Eval (Container.DataItem, "username").ToString () %></asp:LinkButton></span></div>
						</itemtemplate>
					
					</asp:repeater>
					
				</div>

				<br /><br /><br /><br /><br /><br />    
		        
				<div style="margin-left:6px;">
				<div style="position:relative;float:left;">Notes:</div> 
					<asp:TextBox ID="txtIPBanReasonNotes" runat="server" TextMode="MultiLine" Rows="2" Width="400"></asp:TextBox>                           
				</div>
				
				<br />
				
				<div style="margin-left:6px;">
					Suspension length: 
					<asp:DropDownList ID="drpIPBanLength" runat="server" OnSelectedIndexChanged="drpIPBanLength_Selected" AutoPostBack="False">
						<asp:ListItem Value="0">select</asp:ListItem>
						<asp:ListItem Value="1">1</asp:ListItem>
						<asp:ListItem Value="2">2</asp:ListItem>
						<asp:ListItem Value="3">3</asp:ListItem>
						<asp:ListItem Value="4">4</asp:ListItem>
						<asp:ListItem Value="5">5</asp:ListItem>
						<asp:ListItem Value="6">6</asp:ListItem>
						<asp:ListItem Value="7">7</asp:ListItem>
						<asp:ListItem Value="-1">Permanent</asp:ListItem>    
					</asp:DropDownList> Days <div id="cal1Container"></div>
				</div>
				
				<br />
				
				<div style="margin-left:140px;font-size:12px;">(or enter end date/time)</div>
				
				<div style="float:left; padding:13px 20px 10px 6px;">
					Start Date/Time: <br />                                
					<asp:TextBox enabled="false" ID="txtIPBanStartDate" class="formKanevaText" style="width:80px" MaxLength="10" runat="server"/>                            
					<asp:Dropdownlist runat="server" ID="drpIPBanStartTime" style="width: 80px; vertical-align: top;" Enabled="false">
						<asp:ListItem value="00:00:00">12:00 AM</asp:ListItem >
						<asp:ListItem value="00:30:00">12:30 AM</asp:ListItem >
						<asp:ListItem value="01:00:00">1:00 AM</asp:ListItem >
						<asp:ListItem value="01:30:00">1:30 AM</asp:ListItem >
						<asp:ListItem value="02:00:00">2:00 AM</asp:ListItem >
						<asp:ListItem value="02:30:00">2:30 AM</asp:ListItem >
						<asp:ListItem value="03:00:00">3:00 AM</asp:ListItem >
						<asp:ListItem value="03:30:00">3:30 AM</asp:ListItem >
						<asp:ListItem value="04:00:00">4:00 AM</asp:ListItem >
						<asp:ListItem value="04:30:00">4:30 AM</asp:ListItem >
						<asp:ListItem value="05:00:00">5:00 AM</asp:ListItem >
						<asp:ListItem value="05:30:00">5:30 AM</asp:ListItem >
						<asp:ListItem value="06:00:00">6:00 AM</asp:ListItem >
						<asp:ListItem value="06:30:00">6:30 AM</asp:ListItem >
						<asp:ListItem value="07:00:00">7:00 AM</asp:ListItem >
						<asp:ListItem value="07:30:00">7:30 AM</asp:ListItem >
						<asp:ListItem value="08:00:00">8:00 AM</asp:ListItem >
						<asp:ListItem value="08:30:00">8:30 AM</asp:ListItem >
						<asp:ListItem value="09:00:00">9:00 AM</asp:ListItem >
						<asp:ListItem value="09:30:00">9:30 AM</asp:ListItem >
						<asp:ListItem value="10:00:00">10:00 AM</asp:ListItem >
						<asp:ListItem value="10:30:00">10:30 AM</asp:ListItem >
						<asp:ListItem value="11:00:00">11:00 AM</asp:ListItem >
						<asp:ListItem value="11:30:00">11:30 AM</asp:ListItem >
						<asp:ListItem value="12:00:00">12:00 PM</asp:ListItem >
						<asp:ListItem value="12:30:00">12:30 PM</asp:ListItem >
						<asp:ListItem value="13:00:00">1:00 PM</asp:ListItem >
						<asp:ListItem value="13:30:00">1:30 PM</asp:ListItem >
						<asp:ListItem value="14:00:00">2:00 PM</asp:ListItem >
						<asp:ListItem value="14:30:00">2:30 PM</asp:ListItem >
						<asp:ListItem value="15:00:00">3:00 PM</asp:ListItem >
						<asp:ListItem value="15:30:00">3:30 PM</asp:ListItem >
						<asp:ListItem value="16:00:00">4:00 PM</asp:ListItem >
						<asp:ListItem value="16:30:00">4:30 PM</asp:ListItem >
						<asp:ListItem value="17:00:00">5:00 PM</asp:ListItem >
						<asp:ListItem value="17:30:00">5:30 PM</asp:ListItem >
						<asp:ListItem value="18:00:00">6:00 PM</asp:ListItem >
						<asp:ListItem value="18:30:00">6:30 PM</asp:ListItem >
						<asp:ListItem value="19:00:00">7:00 PM</asp:ListItem >
						<asp:ListItem value="19:30:00">7:30 PM</asp:ListItem >
						<asp:ListItem value="20:00:00">8:00 PM</asp:ListItem >
						<asp:ListItem value="20:30:00">8:30 PM</asp:ListItem >
						<asp:ListItem value="21:00:00">9:00 PM</asp:ListItem >
						<asp:ListItem value="21:30:00">9:30 PM</asp:ListItem >
						<asp:ListItem value="22:00:00">10:00 PM</asp:ListItem >
						<asp:ListItem value="22:30:00">10:30 PM</asp:ListItem >
						<asp:ListItem value="23:00:00">11:00 PM</asp:ListItem >
						<asp:ListItem value="23:30:00">11:30 PM</asp:ListItem >
					</asp:Dropdownlist>&nbsp;EST						
					<div id="Div1"></div>
					<asp:RequiredFieldValidator runat="server" id="rfvIPStartDate" controltovalidate="txtIPBanStartDate" 
					text="* Please enter a a value." errormessage="The IP Ban start date is required." display="Dynamic"></asp:RequiredFieldValidator>                                                                                                       
				</div>
		        
				<div  style="float:left; padding:10px 20px 10px 0px;">
					End Date/Time: <br />              
					<asp:TextBox  ID="txtIPBanEndDate" class="formKanevaText" style="width:80px" MaxLength="10" runat="server" />
					<img id="imgSelectIPEndDate" name="imgSelectIPEndDate" onload="initIPBanEnd();" src="images/cal_16.gif"/> 
					<asp:Dropdownlist runat="server" ID="drpIPBanEndTime" style="width: 80px; vertical-align:top;" >
						<asp:ListItem value="00:00:00">12:00 AM</asp:ListItem >
						<asp:ListItem value="00:30:00">12:30 AM</asp:ListItem >
						<asp:ListItem value="01:00:00">1:00 AM</asp:ListItem >
						<asp:ListItem value="01:30:00">1:30 AM</asp:ListItem >
						<asp:ListItem value="02:00:00">2:00 AM</asp:ListItem >
						<asp:ListItem value="02:30:00">2:30 AM</asp:ListItem >
						<asp:ListItem value="03:00:00">3:00 AM</asp:ListItem >
						<asp:ListItem value="03:30:00">3:30 AM</asp:ListItem >
						<asp:ListItem value="04:00:00">4:00 AM</asp:ListItem >
						<asp:ListItem value="04:30:00">4:30 AM</asp:ListItem >
						<asp:ListItem value="05:00:00">5:00 AM</asp:ListItem >
						<asp:ListItem value="05:30:00">5:30 AM</asp:ListItem >
						<asp:ListItem value="06:00:00">6:00 AM</asp:ListItem >
						<asp:ListItem value="06:30:00">6:30 AM</asp:ListItem >
						<asp:ListItem value="07:00:00">7:00 AM</asp:ListItem >
						<asp:ListItem value="07:30:00">7:30 AM</asp:ListItem >
						<asp:ListItem value="08:00:00">8:00 AM</asp:ListItem >
						<asp:ListItem value="08:30:00">8:30 AM</asp:ListItem >
						<asp:ListItem value="09:00:00">9:00 AM</asp:ListItem >
						<asp:ListItem value="09:30:00">9:30 AM</asp:ListItem >
						<asp:ListItem value="10:00:00">10:00 AM</asp:ListItem >
						<asp:ListItem value="10:30:00">10:30 AM</asp:ListItem >
						<asp:ListItem value="11:00:00">11:00 AM</asp:ListItem >
						<asp:ListItem value="11:30:00">11:30 AM</asp:ListItem >
						<asp:ListItem value="12:00:00">12:00 PM</asp:ListItem >
						<asp:ListItem value="12:30:00">12:30 PM</asp:ListItem >
						<asp:ListItem value="13:00:00">1:00 PM</asp:ListItem >
						<asp:ListItem value="13:30:00">1:30 PM</asp:ListItem >
						<asp:ListItem value="14:00:00">2:00 PM</asp:ListItem >
						<asp:ListItem value="14:30:00">2:30 PM</asp:ListItem >
						<asp:ListItem value="15:00:00">3:00 PM</asp:ListItem >
						<asp:ListItem value="15:30:00">3:30 PM</asp:ListItem >
						<asp:ListItem value="16:00:00">4:00 PM</asp:ListItem >
						<asp:ListItem value="16:30:00">4:30 PM</asp:ListItem >
						<asp:ListItem value="17:00:00">5:00 PM</asp:ListItem >
						<asp:ListItem value="17:30:00">5:30 PM</asp:ListItem >
						<asp:ListItem value="18:00:00">6:00 PM</asp:ListItem >
						<asp:ListItem value="18:30:00">6:30 PM</asp:ListItem >
						<asp:ListItem value="19:00:00">7:00 PM</asp:ListItem >
						<asp:ListItem value="19:30:00">7:30 PM</asp:ListItem >
						<asp:ListItem value="20:00:00">8:00 PM</asp:ListItem >
						<asp:ListItem value="20:30:00">8:30 PM</asp:ListItem >
						<asp:ListItem value="21:00:00">9:00 PM</asp:ListItem >
						<asp:ListItem value="21:30:00">9:30 PM</asp:ListItem >
						<asp:ListItem value="22:00:00">10:00 PM</asp:ListItem >
						<asp:ListItem value="22:30:00">10:30 PM</asp:ListItem >
						<asp:ListItem value="23:00:00">11:00 PM</asp:ListItem >
						<asp:ListItem value="23:30:00">11:30 PM</asp:ListItem >
						<asp:ListItem value="23:59:50" selected>11:59 PM</asp:ListItem >
					</asp:Dropdownlist>&nbsp;EST
					<div id="Div2"></div>   
				</div>  
		                                                                                       
				<div  style="float:left; padding:10px 20px 10px 0px;">
					<br />
					<asp:Button id="btnAddIPBan" onclick="btnAddIPBan_Click" Text="Add Suspension" runat="server" OnClientClick="return confirm('Are you sure you want to ban this IP?');"></asp:Button>  
					<asp:Label id="lblIPBanMessages" runat="server" ForeColor="red" Font-Names="Arial"></asp:Label> 
				</div>

			</fieldset>
			
			<br />

			<asp:Label id="lblDeleteMsg" runat="server" ForeColor="red" Font-Names="Arial"></asp:Label><br />

		</TD>
	</TR>
	<TR>
		<TD>
			<asp:DataGrid style="BORDER-RIGHT: #cccccc 1px solid; BORDER-TOP: #cccccc 1px solid; BORDER-LEFT: #cccccc 1px solid; BORDER-BOTTOM: #cccccc 1px solid; FONT-FAMILY: arial" 
			id="dgrdBans" runat="server" OnPageIndexChanged="dgrdBans_PageIndexChanged" OnSortCommand="dgrdBans_SortCommand" OnEditCommand="dgrdBans_EditCommand"  
			OnDeleteCommand="dgrdBans_DeleteCommand" OnCancelCommand="dgrdBans_CancelCommand"  PageSize="15" AllowPaging="True" AllowSorting="True" 
			AutoGenerateColumns="False" border="0" cellpadding="2" Width="600">
		    
				<PagerStyle Mode="NumericPages"></PagerStyle>
				<AlternatingItemStyle CssClass="lineItemOddCSR"></AlternatingItemStyle>
				<ItemStyle CssClass="lineItemEvenCSR"></ItemStyle>
				<HeaderStyle CssClass="lineItemColHeadCSR"></HeaderStyle>
			    
				<Columns>
					<asp:BoundColumn DataField="BanId" HeaderText="Ban Id" SortExpression="BanId" ReadOnly="True">
					<HeaderStyle HorizontalAlign="Left" Font-Bold="True" Width="50" ></HeaderStyle>
					</asp:BoundColumn>
				    
					<asp:BoundColumn DataField="BanStartDate" HeaderText="Start Date" SortExpression="BanStartDate" ReadOnly="True">
					<HeaderStyle HorizontalAlign="Left" Font-Bold="True" Width="250"></HeaderStyle>
					</asp:BoundColumn>
				    
					<asp:BoundColumn DataField="BanEndDate" HeaderText="End Date" SortExpression="BanEndDate" ReadOnly="True">
					<HeaderStyle HorizontalAlign="Left" Font-Bold="True" width="250"></HeaderStyle>
					</asp:BoundColumn>
				    
					<asp:ButtonColumn ButtonType="LinkButton" Text="Delete"  CommandName="Delete">
					<HeaderStyle HorizontalAlign="Left" Font-Bold="True" width="50"></HeaderStyle>
					</asp:ButtonColumn>
				</Columns>
			
			</asp:DataGrid> 
		</TD>
	</TR>
	</TBODY>
</TABLE>

</asp:Panel>            