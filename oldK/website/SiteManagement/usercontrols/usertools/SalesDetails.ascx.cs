using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;
using Kaneva.DataLayer.DataObjects;
using System.Text;
using MagicAjax;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class SalesDetails : BaseUserControl
    {
        #region Declarations
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region PageLoad
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.KANEVA_PROPRIETARY))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                if (!IsPostBack)
                {

                    // Log the CSR activity
                    this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Viewing Sales Details", 0, 0);

                    //initialize the date range
                    IntializePage();
                }

                //populate the list 
                BindData();
            }
            else
            {
                RedirectToHomePage();
            }

        }

        #endregion

        #region Functions

        private void GetOrderDetails(int pointTransactionId)
        {
            // Get the info
            DataRow drPpt = StoreUtility.GetPurchasePointTransaction(pointTransactionId);

            int userId = Convert.ToInt32(drPpt["user_id"]);
            lblOrderId.Text = "KEN-" + drPpt["order_id"].ToString();
            lblDescription.Text = drPpt["description"].ToString();
            lblAmount.Text = FormatCurrency(drPpt["amount_debited"]);
            lblKPoints.Text = FormatKpoints(drPpt["totalPoints"], false);
            lblPaymentMethod.Text = drPpt["name"].ToString();
            lblPurchaseDate.Text = FormatDateTime(drPpt["transaction_date"]);

            if (Convert.ToInt32(drPpt["transaction_status"]).Equals((int)Constants.eTRANSACTION_STATUS.VERIFIED))
            {
                lblErrorDescription.Text = "Ok";
            }
            else
            {
                lblErrorDescription.Text = drPpt["error_description"].ToString();
            }

            //aStatus.HRef = GetTransactionStatusLink(Convert.ToInt32(drPpt["transaction_status"]), drPpt["status_description"].ToString(), Convert.ToDateTime(drPpt["transaction_date"]));
            //aStatus.InnerHtml = GetTransactionStatus(Convert.ToInt32(drPpt["transaction_status"]), drPpt["status_description"].ToString(), Convert.ToDateTime(drPpt["transaction_date"]));
            aUsername.HRef = GetPersonalChannelUrl(drPpt["username"].ToString());
            aUsername.InnerText = drPpt["username"].ToString();

            // What is the payment method?
            int paymentMethodId = Convert.ToInt32(drPpt["payment_method_id"]);

            if (paymentMethodId.Equals((int)Constants.ePAYMENT_METHODS.PAYPAL))
            {
                lblCreditInfo.Text = "Pay using PayPal";
                lblAddress.Text = "Not required for PayPal";
            }
            else
            {
                // Credit card
                if (!drPpt["billing_information_id"].Equals(DBNull.Value))
                {
                    int billingInfoId = Convert.ToInt32(drPpt["billing_information_id"]);
                    DataRow drCreditCard = UsersUtility.GetCreditCard(userId, billingInfoId);
                    lblCreditInfo.Text = drCreditCard["name_on_card"].ToString() + "<br>" +
                        drCreditCard["card_type"].ToString() + ":" + ShowCreditCardNumber(drCreditCard["card_number"].ToString()) + "<br>" +
                        "Exp: " + drCreditCard["exp_month"].ToString() + "/" + drCreditCard["exp_year"].ToString() + "<BR>";
                }
                else
                {
                    lblCreditInfo.Text = "Unknown";
                }

                if (!drPpt["address_id"].Equals(DBNull.Value))
                {
                    // Address
                    int addressId = Convert.ToInt32(drPpt["address_id"]);
                    DataRow drAddress = UsersUtility.GetAddress(userId, addressId);

                    if (drAddress != null)
                    {
                        lblAddress.Text = drAddress["name"].ToString() + "<BR>" +
                            drAddress["address1"].ToString() + "<BR>"; ;
                        if (!drAddress["address2"].Equals(DBNull.Value) && drAddress["address2"].ToString().Length > 0)
                        {
                            lblAddress.Text += drAddress["address2"].ToString() + "<BR>";
                        }
                        lblAddress.Text += drAddress["city"].ToString() + "," + drAddress["state_name"].ToString() + " " + drAddress["zip_code"].ToString() + "<BR>" +
                            drAddress["country_name"].ToString() + "<BR>";
                    }
                    else
                    {
                        lblAddress.Text = "Address not found";
                    }
                }
                else
                {
                    lblAddress.Text = "Unknown";
                }
            }
        }

        private void IntializePage()
        {
            txtStartDate.Text = (DateTime.Now.AddDays(-1.0)).ToShortDateString();
            txtEndDate.Text = DateTime.Now.ToShortDateString();
        }

        private void BindData()
        {
            global::Kaneva.DataLayer.DataObjects.PagedDataTable salesOrderList = new global::Kaneva.DataLayer.DataObjects.PagedDataTable();
            try
            {
                lblSales.Text = "Orders Report from '" + txtStartDate.Text + " 12:00 AM' to '" + txtEndDate.Text + " 11:59 PM'";

                // Set current page
                pgTop.CurrentPageNumber = CURRENTPAGE;

                // Set the sortable columns
                string orderby = "ppt.transaction_date desc";
                int pageSize = 10;

                DateTime dtStartDate = KanevaGlobals.UnFormatDate(txtStartDate.Text + " 00:00:00");
                DateTime dtEndDate = KanevaGlobals.UnFormatDate(txtEndDate.Text + " 23:59:59");

                // Get the violation info (rolled up)
                salesOrderList = (global::Kaneva.DataLayer.DataObjects.PagedDataTable)GetTransactionFacade().GetSalesOrderTransactions(
                    dtStartDate, dtEndDate, SEARCHFILTER, orderby, this.CURRENTPAGE, pageSize);

                if ((salesOrderList != null) && (salesOrderList.TotalCount > 0))
                {
                    messages.Visible = false;
                    pageCount.Visible = true;
                    pgTop.Visible = true;

                    // Show Pager
                    pgTop.NumberOfPages = Math.Ceiling((double)salesOrderList.TotalCount / pageSize).ToString();
                    pageCount.Text = "Page " + CURRENTPAGE + " of " + pgTop.NumberOfPages;
                    pgTop.DrawControl();
                }
                else
                {
                    messages.Visible = true;
                    messages.Text = "No Sales Found";
                    pageCount.Visible = false;
                    pgTop.Visible = false;
                }
                rptOrders.DataSource = salesOrderList;
                rptOrders.DataBind();

            }
            catch (Exception ex)
            {
                messages.Text = "Error: " + ex.Message;
                messages.Visible = true;
            }
        }

        #endregion


        #region Properties
        private string SEARCHFILTER
        {
            get
            {
                if (ViewState["_SearchFilter"] == null)
                {
                    ViewState["_SearchFilter"] = "";
                }
                return ViewState["_SearchFilter"].ToString();
            }
            set
            {
                ViewState["_SearchFilter"] = value;
            }
        }

        private int CURRENTPAGE
        {
            get
            {
                if (ViewState["_CurrentPage"] == null)
                {
                    ViewState["_CurrentPage"] = 1;
                }
                return (int)ViewState["_CurrentPage"];
            }
            set
            {
                ViewState["_CurrentPage"] = value;
            }
        }
        #endregion

        #region Event Handlers

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string errorMessage = "";
            SEARCHFILTER = "";

            try
            {
                //if ((txtStartDate.Text != "") && (txtEndDate.Text != ""))
                //{
                //    errorMessage = "Invalid date(s) has been entered.";
                //}
                //else
                //{

                    if (txtUserName.Text.Trim().Length > 0)
                    {
                        // Look up userId
                        User user = GetUserFacade().GetUser (txtUserName.Text.Trim());

                        if (user != null && user.UserId > 0)
                        {
                            SEARCHFILTER = " ppt.user_id = " + user.UserId.ToString() + " ";
                        }
                        else
                        {
                            errorMessage = "User Not found";
                            throw new Exception("User not found");
                        }
                    }

                    if (txtOrderId.Text.Trim().Length > 0)
                    {
                        SEARCHFILTER = " ppt.order_id = " + txtOrderId.Text.Trim().ToString() + " ";
                    }
               // }

                CURRENTPAGE = 1;
                BindData();
            }
            catch
            {
                //error not valid date
                messages.Text = errorMessage;
                messages.Visible = true;
            }
        }

        protected void pgTop_PageChange(object sender, PageChangeEventArgs e)
        {
            this.CURRENTPAGE = e.PageNumber;
            BindData();
        }

        /// <summary>
        /// They clicked to sort a column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void btnSort_Click(object sender, System.EventArgs e)
        {
            base.btnSort_Click(sender, e);
            BindData();
        }

        protected void btnRefund_OnClick(object sender, System.EventArgs e)
        {
            try
            {
                //first get a reference to the repeater item the save button is in
                RepeaterItem rptOrdersRow = (RepeaterItem)((Button)sender).NamingContainer;

                Label transId = (Label)rptOrdersRow.FindControl("transactionId");
                Label orderId = (Label)rptOrdersRow.FindControl("orderId");
                int pointTransactionId = Convert.ToInt32(transId.Text);

                // Log the activity
                SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
                siteMgmtFacade.InsertCSRLog(GetUserId(), "Admin - Refunding transaction id = " + pointTransactionId, 0, 0);

                StoreUtility.UpdatePointTransaction(pointTransactionId, (int)Constants.eTRANSACTION_STATUS.REFUNDED, "", "");

                // Update order
                StoreUtility.UpdateOrderToRefunded(Convert.ToInt32(orderId.Text));

                BindData();
            }
            catch (Exception ex)
            {
                messages.Text = "Error: " + ex.Message;
                messages.Visible = true;
            }

        }

        public void ShowDetails_Command(Object sender, CommandEventArgs e)
        {
            try
            {
                switch (e.CommandName.ToLower())
                {
                    case "open":
                        GetOrderDetails(Convert.ToInt32(e.CommandArgument));
                        salesDetails.Visible = true;
                        break;
                    case "close":
                        salesDetails.Visible = false;
                        break;
                }
            }
            catch (FormatException fe)
            {
                messages.Text = "Error: " + fe.Message;
                messages.Visible = true;
            }
        }


        private void rptOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //find all the controls in the repeater
                HyperLink userName = (HyperLink)e.Item.FindControl("userName");
                HyperLink userName2 = (HyperLink)e.Item.FindControl("userName2");
                HtmlImage userPict = (HtmlImage)e.Item.FindControl("userPict");
                
                Label userId = (Label)e.Item.FindControl("userId");
                Label transactionId = (Label)e.Item.FindControl("transactionId");
                Label orderDesc = (Label)e.Item.FindControl("orderDesc");
                Label orderId = (Label)e.Item.FindControl("orderId");
                Label dollarAmount = (Label)e.Item.FindControl("dollarAmount");
                Label paymentMethod = (Label)e.Item.FindControl("paymentMethod");
                Label purchaseDate = (Label)e.Item.FindControl("purchaseDate");
                Label kpointAmount = (Label)e.Item.FindControl("kpointAmount");
                Label status = (Label)e.Item.FindControl("status");
                Button btnRefund = (Button)e.Item.FindControl("btnRefund");
                LinkButton transActionDetails = (LinkButton)e.Item.FindControl("transActionDetails");

                //set alert box to delete button
                StringBuilder sb = new StringBuilder();
                sb.Append(" javascript: ");
                sb.Append(" if(!confirm('Are you sure you want to mark this as refunded \\n This cannot be undone.')) return false;");
                btnRefund.Attributes.Add("onClick", sb.ToString());
                btnRefund.Enabled = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "transaction_status")).Equals((int)Constants.eTRANSACTION_STATUS.VERIFIED);

                //assign the user image
                userPict.Src = this.GetProfileImageURL(DataBinder.Eval(e.Item.DataItem, "thumbnail_small_path").ToString(), "sm", "M");

                //set the data for each field
                userName2.ToolTip = userName.ToolTip = DataBinder.Eval(e.Item.DataItem, "com_name").ToString();
                userName.Text = TruncateWithEllipsis(DataBinder.Eval(e.Item.DataItem, "com_name").ToString(), 10);
                userName2.NavigateUrl = userName.NavigateUrl = GetPersonalChannelUrl(DataBinder.Eval(e.Item.DataItem, "name_no_spaces").ToString());
                userId.Text = DataBinder.Eval(e.Item.DataItem, "user_id").ToString();
                transactionId.Text = DataBinder.Eval(e.Item.DataItem, "point_transaction_id").ToString();
                orderDesc.Text = DataBinder.Eval(e.Item.DataItem, "ppt_description").ToString();
                orderId.Text = DataBinder.Eval(e.Item.DataItem, "order_id").ToString();
                dollarAmount.Text = FormatCurrency(DataBinder.Eval(e.Item.DataItem, "amount_debited").ToString());
                paymentMethod.Text = DataBinder.Eval(e.Item.DataItem, "name").ToString();
                purchaseDate.Text = DataBinder.Eval(e.Item.DataItem, "transaction_date").ToString();
                status.Text = DataBinder.Eval(e.Item.DataItem, "status_description").ToString();
                kpointAmount.Text = FormatKpoints(DataBinder.Eval(e.Item.DataItem, "kpoints").ToString());

                //set the action for the transaction id
                transActionDetails.CommandArgument = transactionId.Text;
                transActionDetails.Visible = true;

            }
        }
        
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.rptOrders.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptOrders_ItemDataBound);
        }

        #endregion

    }
}