<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="GameSearch.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.GameSearch" %>
<%@ Register TagPrefix="Kaneva" TagName="StoreFilter" Src="../StoreFilter.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<style type="text/css">
	img { behavior: url(../css/iepngfix.htc); }
</style>

<script src="../jscript/xEvent.js" type="text/javascript"></script>
<script src="../jscript/balloon.js" type="text/javascript"></script>
<script src="../jscript/yahoo-dom-event.js" type="text/javascript"></script> 
 
<script type="text/JavaScript">

function changeBG(obj) {
	obj.style.background = "url('../images/media/tabsMedia.gif')";
}
function changeBG2(objeto) {
	objeto.style.background = "url('../images/media/tabsMedia_on.gif')";
}
function changeBGB(objeto) {
	objeto.style.background = "url('../images/media/tabsMedia2.gif')";
}
function changeBGB2(objeto) {
	objeto.style.background = "url('../images/media/tabsMedia2_on.gif')";
}

function CheckAll() 
{
	Select_All(document.getElementById("cbxSelectAll").checked);
}

// white balloon with mostly default configuration
// (see http://www.wormbase.org/wiki/index.php/Balloon_Tooltips)
var whiteBalloon    = new Balloon;
whiteBalloon.balloonTextSize  = '100%';

// white ballon with some custom config:
var whiteBalloonSans  = new Balloon;
whiteBalloonSans.upLeftConnector    = '../images/balloons/balloonbottom_sans.png';
whiteBalloonSans.upRightConnector   = '../images/balloons/balloonbottom_sans.png';
whiteBalloonSans.downLeftConnector  = '../images/balloons/balloontop_sans.png';
whiteBalloonSans.downRightConnector = '../images/balloons/balloontop_sans.png';
whiteBalloonSans.upBalloon          = '../images/balloons/balloon_up_top.png';
whiteBalloonSans.downBalloon        = '../images/balloons/balloon_down_bottom.png';
whiteBalloonSans.paddingConnector = '22px';
   
</script>
<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    <!-- Search criteria section --->
    <div id="searchCriteria" style="margin: 10px 0px 10px 10px; width:965px">
        <div style="text-align:center; font-size:22px; margin:0px 0px 10px 0px">
            GAME SEARCH <i>(Find 3D App)</i>
        </div>
        <div style="text-align:right;margin: 0px 10px 0px 10px">
            <b><asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/></b>&nbsp;<Kaneva:Pager runat="server" id="pgTop"/>
        </div>
        <TABLE id="TABLE1" cellSpacing="0" cellPadding="0" width="963px" border="0" style="font-weight:bold; background-color:#cccccc"> 
            <!-- Begin Game management controls -->
            <tr>
                <td style="width:486px;" align="right">
                    Filter By Game Name &nbsp;
				    <asp:textbox id="txtGameName" Width="326px" runat="server"></asp:textbox>
				</td>
                <!-- number to show per page -->
                <td style="width:100px"  align="right">																													
			        <Kaneva:StoreFilter id="filterBy" runat="server" AssetType="0" HideAlphaNumericsPullDown="false" HideAlphaNumerics="true" HideItemsPerPagePullDown="true" HideThumbView="True" IsAjaxMode="True"></Kaneva:StoreFilter>
                </td> 
                <!-- filter by alpha and symbols -->
                <td style="width:186px"  align="right">																													
			        <Kaneva:StoreFilter id="itemsPerPage" runat="server" AssetType="0" HideAlphaNumerics="true" HideThumbView="True" IsAjaxMode="True"></Kaneva:StoreFilter>
			    </td> 
			    <td style="width:70px" align="center">
			        <asp:button runat="server" id="btnSearch" text="Search" onclick="btnSearch_Click"/>
			    </td>
            </tr>
        </table>
    </div>
    
    
    <!-- Search results section --->
    <div id="searchResults" style="margin: 10px 0px 10px 10px; width:965px">

        <ajax:ajaxpanel id="ajGameLibrary" runat="server">
        <!-- Main Body structure table -->
		                <asp:Repeater id="rptGames" runat="server">
		                    <HeaderTemplate>
	                            <table width="963px" cellspacing="0" cellpadding="0" border="0" >
		                            <tr style="background-color:#dddddd; line-height:20px; font-weight:bold; font-size:14px">
										<TD style="width:15px" align="left">
											<input type=checkbox id="cbxSelectAll" onclick="CheckAll()" class="Filter2" />
										</td>
			                            <td runat="server" id="tdOrderHeading" align="left" style="width:490px; padding: 0px 0px 0px 8px">Name</td>
			                            <td align="center" style="width:80px">Access</td>
			                            <td align="left" style="width:393px; padding: 0px 0px 0px 10px">Details</td>
			                            <td align="left" ></td>
		                            </tr>
		                    </HeaderTemplate>
			                <ItemTemplate>
				                <tr style="background-color:#ffffff; line-height:18px">
					                <td align="center" style="width:15px">
						                <span style="horizontal-align: right;">
							                <asp:checkbox id="chkEdit" CssClass="Filter2" runat="server" Visible="true"/>
							                <input type="hidden" runat="server" id="hidGameId" value='<%#DataBinder.Eval(Container.DataItem, "game_id")%>' game_name="hidGameId">
						                </span>
					                </td>
					                <td style="width:490px">
						                <!--image holder-->
						                <table border="0" cellpadding="0" cellspacing="0" width="100%" >
							                <tr>
								                <td align="right" width="36">
									                <div class="framesize-tiny">
										                <div class="frame">
											                <div class="restricted" runat="server" visible='<%# IsGameMature(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "game_rating_id"))).Equals(1) %>' ID="Div2"></div>
											                <span class="ct">
												                <span class="cl"></span>
											                </span>
											                <div class="imgconstrain">
																<asp:LinkButton id="thumbnail" OnCommand="SelectGame_Command" style="COLOR: #3258ba; font-size: 12px;text-decoration:none" runat="server" CommandArgument='<%# Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "game_id"))%>'>
																	<img src='<%#GetGameImageURL (DataBinder.Eval(Container.DataItem, "game_image_path").ToString () ,"sm")%>' border="0"/>
																</asp:LinkButton>
											                </div>
											                <span class="cb">
												                <span class="cl"></span>
											                </span>
										                </div>
									                </div>
								                </td>
								                <td align="left" style="padding: 0px 0px 0px 10px">
								                    <asp:LinkButton id="gameTitle" OnCommand="SelectGame_Command" style="COLOR: #3258ba; font-size: 12px;text-decoration:none" runat="server" CommandArgument='<%# Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "game_id"))%>'>
								                        <%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_name").ToString ()), 30) %>
								                    </asp:LinkButton>
								                </td>
							                </tr>
						                </table>
						                <asp:Label ID="lblMessage" runat="server" />
						                <!--end image holder-->
					                </td>
					                <td align="center" style="width:80px">
						                <span style="horizontal-align: center;">
						                    <asp:label id="lbl_gameAccess" runat="server" alt='<%#GetGameAccessDescription(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "game_access_id")))%>' text='<%#GetGameAccessDescription(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "game_access_id")))%>' visible="true" />
						                </span>
					                </td>
					                <td valign="middle" align="left" style="width:393px; padding: 0px 0px 0px 10px">
						                <span>Status:&nbsp;<asp:LinkButton runat="server" OnCommand="SelectGame_Command" id="hlStatus" />&nbsp;<asp:LinkButton runat="server" OnCommand="SelectGame_Command" runat="server" id="hlAddData" /></span><span class="insideTextNoBold">
							                <br />Added on: <%# FormatDateTime(DataBinder.Eval(Container.DataItem, "game_creation_date"))%></span>
							                <br />
					                </td>
					                <td align="center">
					                    <asp:LinkButton id="lnkEdit" Text="Edit" ToolTip="Edit" OnCommand="SelectGame_Command" style="COLOR: #3258ba; font-size: 12px;text-decoration:underline" runat="server" CommandArgument='<%# Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "game_id"))%>' />
					                </td>
				                </tr>
			                </ItemTemplate>
			                <AlternatingItemTemplate>
				                <tr style="background-color:#eeeeee; line-height:18px">
					                <td align="center"style="width:15px">
						                <span style="horizontal-align: right;">
							                <asp:checkbox id="chkEdit" CssClass="Filter2" runat="server" Visible="true"/>
							                <input type="hidden" runat="server" id="hidGameId" value='<%#DataBinder.Eval(Container.DataItem, "game_id")%>' game_name="hidGameId">
						                </span>
					                </td>
					                <td style="width:490px">
						                <!--image holder-->
						                <table border="0" cellpadding="0" cellspacing="0" width="100%">
							                <tr>
								                <td align="right" width="36">
									                <div class="framesize-tiny">
										                <div class="frame">
											                <div class="restricted" runat="server" visible='<%# IsGameMature(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "game_rating_id"))).Equals(1) %>' ID="Div2"></div>
											                <span class="ct">
												                <span class="cl"></span>
											                </span>
											                <div class="imgconstrain">
																<asp:LinkButton id="thumbnail" OnCommand="SelectGame_Command" style="COLOR: #3258ba; font-size: 12px;text-decoration:none" runat="server" CommandArgument='<%# Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "game_id"))%>'>
																	<img src='<%#GetGameImageURL (DataBinder.Eval(Container.DataItem, "game_image_path").ToString () ,"sm")%>' border="0"/>
																</asp:LinkButton>
											                </div>
											                <span class="cb">
												                <span class="cl"></span>
											                </span>
										                </div>
									                </div>
								                </td>
								                <td align="left" style="padding: 0px 0px 0px 10px">
								                    <asp:LinkButton id="gameTitle" OnCommand="SelectGame_Command" style="COLOR: #3258ba; font-size: 12px;text-decoration:none" runat="server" CommandArgument='<%# Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "game_id"))%>'>
								                        <%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_name").ToString ()), 30) %>
								                    </asp:LinkButton>
								                </td>
							                </tr>
						                </table>
						                <asp:Label ID="lblMessage" runat="server" />
						                <!--end image holder-->
					                </td>
					                <td align="center" style="width:80px">
						                <span style="horizontal-align: center;">
						                    <asp:label id="lbl_gameAccess" runat="server" alt='<%#GetGameAccessDescription(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "game_access_id")))%>' text='<%#GetGameAccessDescription(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "game_access_id")))%>' visible="true" />
						                </span>
					                </td>
					                <td valign="middle" align="left" style="width:393px; padding: 0px 0px 0px 10px">
						                <span>Status:&nbsp;<asp:LinkButton runat="server" OnCommand="SelectGame_Command" id="hlStatus" />&nbsp;<asp:LinkButton runat="server" OnCommand="SelectGame_Command" runat="server" id="hlAddData" /></span><span class="insideTextNoBold">
							                <br />Added on: <%# FormatDateTime(DataBinder.Eval(Container.DataItem, "game_creation_date"))%></span>
							                <br />
					                </td>
					                <td align="center">
					                    <asp:LinkButton id="lnkEdit" Text="Edit" ToolTip="Edit" OnCommand="SelectGame_Command" style="COLOR: #3258ba; font-size: 12px;text-decoration:underline" runat="server" CommandArgument='<%# Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "game_id"))%>' />
					                </td>
				                </tr>
			                </AlternatingItemTemplate>
			                <FooterTemplate>
			                    </table>
			                </FooterTemplate>
		                </asp:Repeater>
        <!-- END MAIN BODY -->
        </ajax:ajaxpanel>
   
    </div>
    <!-- end search results section -->
    
</asp:Panel>