﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class GameAchievements : BaseUserControl
    {
        #region Declarations

        bool isAdministrator = false;
        bool canEdit = false;
        int ownerId = 0;
        UInt32 achievementId = 0;

        private int pageSize = 30;
        private int pageNum = 1;
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion


        #region PageLoad

        protected void Page_Load (object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                divError.Style.Add ("display", "none");
                divErrorTop.Style.Add ("display", "none");

                //check security level
                switch (CheckUserAccess ((int) SitePrivilege.ePRIVILEGE.STAR_ADMIN))
                {
                    case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        isAdministrator = true;
                        canEdit = true;
                        break;
                    case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    default:
                        break;
                }
                
                if ((Session["AppManagementSelectedCommunityId"] != null) && Convert.ToInt32 (Session["AppManagementSelectedCommunityId"]) > 0)
                {
                    // Load the current user's data the first time
                    SelectedGameId = GetCommunityFacade().GetCommunityGameId(Convert.ToInt32 (Session["AppManagementSelectedCommunityId"]));
                    
                    //get the company id the game is associated with
                    this.ownerId = GetGameFacade ().GetGameOwner (SelectedGameId);

                    if (!isAdministrator)
                    {
                        canEdit = this.ownerId.Equals (GetUserId ());
                    }

                    if (!canEdit)
                    {
                        RedirectToHomePage ();
                    }
                    else
                    {
                        //community id check is to allow kaneva website to make use of this usercontrol
                        if (!IsPostBack || !IsPostBackKaneva)
                        {
                            // Log the CSR activity
                            this.GetSiteManagementFacade ().InsertCSRLog (GetUserId (), "CSR - Viewing game premium item page", 0, SelectedGameId);

                            // populate page
                            BindAchievements ();

                            // Have to manually track post back for Kaneva site.  Even on first page load
                            // the IsPostBack property is set to true. So we'll keep up with it ourselves best we can.
                            IsPostBackKaneva = true;
                        }
                    }
                }
                else
                {
                    //community id check is to tell it to redirect to search page (for KWAS only)
                    if (Request["communityId"] == null)
                    {
                        //store this pages URL for return
                        Session["CallingPage"] = Request.Url.ToString ();
                        Response.Redirect (ResolveUrl ("~/Default.aspx?ws=" + GetCurrentSiteID ((int) SiteManagementCommonFunctions.SUPPORTED_WEBSITES.DEVELOPER) + "&mn=" + GetGameSearchParentMenuID () + "&sn=" + GetGameSearchControlID ()));
                    }
                }

                IsCurrentHostKWAS = System.IO.Path.GetFileName (Request.Path).ToLower () == "default.aspx";
            }
            else
            {
                RedirectToHomePage ();
            }
        }

        #endregion

        #region Functions
        //
        private void BindAchievements ()
        {
            try
            {
                // Check to see if owner has created a leaderboard for this 3D App
                PagedList<Achievement> achievements = GetGameFacade ().AchievementForApp (CommunityId, "", PageNum, PageSize);

                rptBadges.DataSource = achievements;
                rptBadges.DataBind ();

                // The results
                lblSearch.Text = GetResultsText (achievements.TotalCount, PageNum, pageSize, achievements.Count);

                pgTop.NumberOfPages = Math.Ceiling ((double) achievements.TotalCount / pageSize).ToString ();
                pgTop.DrawControl ();
            }
            catch (Exception ex)
            {
                ShowMessage (ex.Message);
                m_logger.Error ("Error Loading badges.", ex);
            }
        }
        //
        private void ShowMessage (string msg)
        {
            ShowMessage (msg, true, true);
        }
        private void ShowMessage (string msg, bool isError, bool isTopMsg)
        {
            if (isTopMsg)
            {
                divErrorTop.InnerText = msg;
                divErrorTop.Attributes.Add ("class", (isError ? "error" : "confirm") + " errtop");
                divErrorTop.Style.Add ("display", "block");
            }
            else
            {
                divError.InnerText = msg;
                divError.Attributes.Add ("class", isError ? "error" : "confirm");
                divError.Style.Add ("display", "block");
            }
        }

        private void GetRequestValues ()
        {
            // Get items per page count
            if (Request["pagesize"] != null)
            {
                try
                {
                    PageSize = Convert.ToInt32 (Request["pagesize"]);
                }
                catch { }
            }

            // Get the current page number
            if (Request["p"] != null)
            {
                try
                {
                    PageNum = Convert.ToInt32 (Request["p"]);
                }
                catch { }
            }
        }

        //clear the server data fields
        private void ClearAchievementDataFields ()
        {
            txtName.Text = "";
            txtDescription.Text = "";
            txtPoints.Text = "1";
            imgThumbnail.Src = "";
        }

        private void PopulateAchievementEditFields (Achievement achievement)
        {
            try
            {
                txtName.Text = achievement.Name;
                txtDescription.Text = achievement.Description;
                txtPoints.Text = achievement.Points.ToString ();
                imgThumbnail.Src = GetAchievementImageURL (achievement.ImageUrl, "la");
                imgThumbnail.Style.Add ("display", "block");
                CurrentAchievement = achievement;

                if (achievement.ImageUrl != null && achievement.ImageUrl.Length > 0)
                {
                    ViewState.Add("imageurl", achievement.ImageUrl);
                }
            }
            catch (Exception)
            {
                ShowMessage ("Error populating badge fields for editing.", true, false);
            }
        }

        private void AchievementsReset ()
        {
            ClearAchievementDataFields ();

            divDetails.Style.Add ("display", "none");
            imgThumbnail.Style.Add ("display", "none");

            btnUpdateAchievement.Text = "Add";
            CurrentAchievement = null;
        }

        private int GetAvailablePoints (uint achievementId)
        {
            int totalPoints = 0;
            string filter = achievementId > 0 ? "achievement_id <> " + achievementId : "";
            
            totalPoints = GetGameFacade ().GetTotalAchievementPoints (CommunityId, filter);
            int maxPoints = global::KlausEnt.KEP.Kaneva.KanevaGlobals.MaxAchievementPoints;

            int availPoints = (maxPoints - totalPoints);
            
            spnAvailablePts.InnerText = availPoints.ToString ();
            litMaxPts.Text = maxPoints.ToString ();

            return availPoints;
        }

        private void ResetAchievements ()
        {
            try
            {
                if (GetGameFacade ().ResetAchievements (CommunityId) > 0)
                {
                    ShowMessage ("Achievements have been reset.", false, true);
                }
                else
                {
                    ShowMessage ("Achievements were not reset.");
                }
            }
            catch (Exception)
            {
                ShowMessage ("Error clearing users that have badges", true, false);
            }
        }

        #endregion Functions

        #region Event Handlers

        protected void lbEdit_Click (object sender, CommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                this.achievementId = Convert.ToUInt32 (e.CommandArgument);

                ClearAchievementDataFields ();
                btnUpdateAchievement.Text = "Update";
                divEditTitle.InnerText = "Edit Badge";

                PopulateAchievementEditFields (GetGameFacade().GetAchievement (this.achievementId));
                GetAvailablePoints (this.achievementId);
                divDetails.Style.Add ("display", "block");
            }
        }

        protected void lbDelete_Click (object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    this.achievementId = Convert.ToUInt32 (e.CommandArgument);

                    GetGameFacade ().DeleteAchievement (this.achievementId);
                }

                ShowMessage ("Badge was deleted successfully.", false, true);
                BindAchievements ();
                
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error trying to delete premium item(s) : ", ex);
                ShowMessage ("Badge was not deleted.");
            }
        }

        /// <summary>
        /// btnServerCancel_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancelAchievements_Click (Object sender, EventArgs e)
        {
            AchievementsReset ();
        }

        /// <summary>
        /// btnUpdateServer_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdateAchievements_Click (Object sender, EventArgs e)
        {
            //set error message variable
            string error = "";

            //validate the required fields
            if (txtName.Text.Trim() == "")
            {
                error += "Name is required. ";
            }
            if (txtName.Text.Trim ().Length > 45)
            {
                error += "Name is too long. Name can not exceed 45 characters. ";
            }
            //validate the required fields
            if (txtDescription.Text.Trim() == "")
            {
                error += "Description is required. ";
            } 
            if (txtDescription.Text.Trim().Length > 100)
            {
                error += "Name is too long. Name can not exceed 100 characters. ";
            }
            //validate the price
            if (txtPoints.Text.Trim() == "")
            {
                error += "Point value is required. ";
            }
            try       
            {
                int pts = Convert.ToInt32 (txtPoints.Text);
                int availPts = GetAvailablePoints (CurrentAchievement.AchievementsId);
                if (availPts == 0)
                {
                    error += "You have no available points to use.  You can lower the point value or delete an existing badge. ";   
                }
                else if (pts <= 0 || pts > availPts)
                {
                    error += "Point value must be a whole number greater than 0 and less than " + availPts.ToString() + ". ";
                }
            }
            catch
            {
                error += "Point value must be a number greater than 0. ";
            }
            // Check for any inject scripts in large text fields
            if ((SiteManagementCommonFunctions.ContainsInjectScripts (txtName.Text)) ||
                (SiteManagementCommonFunctions.ContainsInjectScripts (txtDescription.Text)) ||
                (SiteManagementCommonFunctions.ContainsInjectScripts (txtPoints.Text)))
            {
                error += "Your input contains invalid scripting, please remove script code and try again. ";
                m_logger.Warn ("User " + GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
            }
            // Check to make sure user did not enter any "potty mouth" words
            if (SiteManagementCommonFunctions.isTextRestricted (txtName.Text, global::KlausEnt.KEP.Kaneva.Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                SiteManagementCommonFunctions.isTextRestricted (txtDescription.Text, global::KlausEnt.KEP.Kaneva.Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                error += global::KlausEnt.KEP.Kaneva.Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
            } 
            if (error.Length > 0)
            {
                ShowMessage (error, true, false);
            }
            else
            {
                try
                {
                    if ((inpThumbnail != null) && (inpThumbnail.Value != ""))
                    {
                        int MaxImageUploadSize = KlausEnt.KEP.Kaneva.KanevaGlobals.MaxUploadedImageSize;
                        if (inpThumbnail.PostedFile.ContentLength > MaxImageUploadSize)
                        {
                            ShowMessage ("This image is too large, please select an image smaller than " + KlausEnt.KEP.Kaneva.KanevaGlobals.FormatImageSize (MaxImageUploadSize) + ".", true, false);
                            return;
                        }
                    }

                    Achievement achievement = new Achievement ();

                    // build the achievement
                    achievement.AchievementsId = CurrentAchievement.AchievementsId;
                    achievement.CommunityId = CommunityId;
                    achievement.Name = txtName.Text;
                    achievement.Description = txtDescription.Text;
                    achievement.Points = Convert.ToUInt32 (txtPoints.Text);

                    if (ViewState["imageurl"] != null)
                    {
                        achievement.ImageUrl = ViewState["imageurl"].ToString ();
                    }

                    // save the achievement
                    achievement.AchievementsId = GetGameFacade ().SaveAchievement (achievement);

                    if (achievement.AchievementsId > 0)
                    {
                        FameFacade famefaceade = new FameFacade();
                        famefaceade.RedeemPacket(GetCurrentUser().UserId, 5003, (int) FameTypes.Creator);
                    }

                    // if an image was selected, then save that image url
                    if ((inpThumbnail != null) && (inpThumbnail.Value != ""))
                    {
                        this.UploadAchievementImage (GetUserId (), (int)achievement.AchievementsId, inpThumbnail);
                    }

                    //indicated a successful save
                    ShowMessage ("Your changes were saved successfully.", false, true);

                    //clear the fields
                    AchievementsReset ();

                    //bind the data
                    BindAchievements ();
                }
                catch (Exception ex)
                {
                    ShowMessage (ex.Message, true, false);
                    m_logger.Error ("Error in Edit Achievement (Badges)", ex);
                }
            }
        }

        /// <summary>
        /// Page Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pgTop_PageChange (object sender, PageChangeEventArgs e)
        {
            PageNum = e.PageNumber;
            BindAchievements ();
        }

        /// <summary>
        /// lbNew_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbNew_Click (Object sender, EventArgs e)
        {
            AchievementsReset ();
            GetAvailablePoints (0);
            divEditTitle.InnerText = "Add Badge";
            divDetails.Style.Add ("display", "block");
        }

        /// <summary>
        /// lbReset_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbReset_Click (Object sender, EventArgs e)
        {
            ResetAchievements ();
        }

        #endregion Event Handlers

        #region Properties

        /// <summary>
        /// SelectedGameId
        /// </summary>
        /// <returns></returns>
        private int SelectedGameId
        {
            get
            {
                return (int) ViewState["game_id"];
            }
            set
            {
                ViewState["game_id"] = value;
            }
        }
        private int SelectedCommunityId
        {
            get
            {
                return (int) ViewState["community_id"];
            }
            set
            {
                ViewState["community_id"] = value;
            }
        }

        private bool IsPostBackKaneva
        {
            get
            {
                if (ViewState["ispostback"] == null)
                {
                    return false;
                }

                return (bool) ViewState["ispostback"];
            }
            set { ViewState["ispostback"] = value; }
        }
        private bool IsCurrentHostKWAS
        {
            get
            {
                if (ViewState["iskwas"] == null)
                {
                    return false;
                }

                return (bool) ViewState["iskwas"];
            }
            set { ViewState["iskwas"] = value; }
        }
        /// <summary>
        /// CommunityId
        /// </summary>
        /// <returns></returns>
        private int CommunityId
        {
            get
            {
                if (ViewState["community_id"] != null)
                {
                    return (int) ViewState["community_id"];
                }
                else if (SelectedGameId > 0)
                {
                    return GetCommunityFacade ().GetCommunityIdFromGameId (SelectedGameId);
                }
                return 0;
            }
            set
            {
                ViewState["community_id"] = value;
            }
        }
        /// <summary>
        /// </summary>
        /// <returns></returns>
        private Achievement CurrentAchievement
        {
            get
            {
                if (ViewState["currentAchievement"] == null)
                {
                    ViewState["currentAchievement"] = new Achievement ();
                }
                return (Achievement) ViewState["currentAchievement"];
            }
            set
            {
                ViewState["currentAchievement"] = value;
            }
        }

        
        private int PageNum
        {
            set { this.pageNum = value; }
            get { return this.pageNum; }
        }
        private int PageSize
        {
            set { this.pageSize = value; }
            get { return this.pageSize; }
        }

        #endregion Properties

        #region Web Form Designer generated code

        override protected void OnInit (EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent ();
            base.OnInit (e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.Load += new System.EventHandler (this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler (pgTop_PageChange);
        }

        #endregion Web Form Designer generated code
    }
}