<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserPublishedItems.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.UserPublishedItems" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="980">
        <tr>
            <td style="padding-left:40px; text-align:center; font-size:22px">
                USER PUBLISHED ITEMS
            </td>
        </tr>
        <tr>
            <td style="padding-left:40px">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
	                <tr style="line-height:15px;">
	                    <td align="left" style="width:300px" class="belowFilter2" valign="middle" >
	                        <asp:Label runat="server" id="lblSelectedUser" />
	                    </td>
		                <td align="right" class="belowFilter2" valign="middle">
			                <asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop"/><br>
		                </td>						
	                </tr>
                </table>
             </td>
         </tr>
         <tr>
            <td style="padding-left:40px">
                <asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" OnSortCommand="UserPublished_Sorting" Width="100%" id="dgrdRatings" cellpadding="2" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 1px solid #cccccc; font-family:arial">  
	            <HeaderStyle BackColor="#cccccc" ForeColor="#000000" Font-Underline="false" Font-Bold="true" HorizontalAlign="Left" />
	            <ItemStyle BackColor="#ffffff" HorizontalAlign="Left" />
	            <AlternatingItemStyle BackColor="#eeeeee" HorizontalAlign="Left" />
	                <Columns>				
		                <asp:TemplateColumn HeaderText="action" SortExpression="" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Width="10%" ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <asp:hyperlink id="hlEdit" runat="server" Target="_blank" CssClass="adminLinks" NavigateURL='<%#GetAssetEditLink (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "asset_id")))%>' ToolTip="Edit" Visible='<%#!IsDeleted (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "status_id")))%>'>edit</asp:hyperlink> 
				                &nbsp;&nbsp;<asp:hyperlink id="hlDelete" CssClass="adminLinks" runat="server" NavigateURL='<%#GetDeleteScript (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id"))) %>' ToolTip="Delete" Visible='<%#!IsDeleted (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "status_id")))%>'>delete</asp:hyperlink>
				                <br><asp:hyperlink id="hlDownload" CssClass="adminLinks" runat="server" NavigateURL='<%#GetDownloadLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id"))) %>' ToolTip="Download">download</asp:hyperlink>
				                &nbsp;&nbsp;<asp:hyperlink id="hlTransfer" Target="_blank" CssClass="adminLinks" runat="server" NavigateURL='<%#GetTransferLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id"))) %>' ToolTip="Transfer" Visible='<%#!IsDeleted (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "status_id")))%>'>transfer</asp:hyperlink>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn ItemStyle-CssClass="assetLink"  HeaderText="item name" SortExpression="name" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top" ItemStyle-Width="30%" ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <a Target="_blank" title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' CssClass="assetLink" href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 40)%></a>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn HeaderText="category" SortExpression="asset_type_id" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Width="10%" ItemStyle-Wrap="false">
			                <ItemTemplate>
				                <%# FormatString(GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id"))), Convert.ToInt32(DataBinder.Eval(Container.DataItem, "status_id")))%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn ItemStyle-ForeColor="green" HeaderText="size" SortExpression="file_size" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Width="10%" ItemStyle-Wrap="false">
			                <ItemTemplate>
				                <%# FormatImageSize (DataBinder.Eval(Container.DataItem, "file_size"))%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn HeaderText="status" SortExpression="status_id" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top" ItemStyle-Width="10%" ItemStyle-Wrap="false">
			                <ItemTemplate>
				                <A id="A1" runat="server" style="color: #4863a2; font-size: 11px;" HREF='#' onclick='<%# GetStatusLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "publish_status_id")))%>'><%# GetStatusText (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "publish_status_id")), Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "status_id")), Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id"))) %></a>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn ItemStyle-CssClass="adminLinks" HeaderText="date" SortExpression="created_date" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Width="20%" ItemStyle-Wrap="false">
			                <ItemTemplate>
				                <%# FormatDateTime (DataBinder.Eval(Container.DataItem, "created_date"))%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
	                </Columns>
                </asp:datagrid>
            </td>
         </tr>
    </table>

</asp:Panel>            