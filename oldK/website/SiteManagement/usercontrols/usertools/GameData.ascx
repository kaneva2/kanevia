<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="GameData.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.GameData" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>

<link href="css/kaneva/GameData.css" type="text/css" rel="stylesheet">

<script type="text/javascript">
<!--
window.onload = ttINIT;
function ttINIT ()
{
	Tooltip.init();
	OnLoad();
}

var selectedCategories = new Array()
function categorySelected(obj)
{										 
	if (obj.checked)
	{    
		//a new checkbox is selected, pop the first selection if 3 are already selected
		if (selectedCategories.length == 3)
		{											   
			firstEle = selectedCategories.shift();
			firstEle.checked = false;
		}
		selectedCategories.push(obj);  
	}else
	{		   
		var index = -1;		  
		//remove it from the array if it exists
		for (var i=0; i < selectedCategories.length; i++)
		{
			if (selectedCategories[i] == obj)
			{
				index = i;
				break;          
			}
		}
		if (index >= 0)
		{
			//and rearrange the list
			for (var i=index; i < selectedCategories.length-1; i++)
			{
				selectedCategories[i] = selectedCategories[i+1];
			}
			selectedCategories.pop();
		}
	}
}
//--></script>
 
<div id="infoContainer">
	
	<!--BASIC INFO -->
    <div id="infoData">
		<div class="title"><h4>Describe your World</h4></div>
		<div id="divErrorData" runat="server" class="error"></div>
		<div style="clear:both;"></div>
		<p>
			<div>Name <asp:RegularExpressionValidator id="revName" runat="server" Display="Dynamic" Text="*" ControlToValidate="txtGameName" EnableClientScript="True"></asp:RegularExpressionValidator> 
			<asp:RequiredFieldValidator ID="rfItemName" ControlToValidate="txtGameName" Text="*" ErrorMessage="Game name is a required field." Display="Dynamic" runat="server"/></div>
			<div class="name"><asp:TextBox ID="txtGameName" Enabled="False" MaxLength="40" width="400" runat="server"/> <div class="owner">Owner: <asp:Label id="lblCreatedBy" runat="server"/></div></div>
		</p>
		<p><div>Game Id: <asp:Label id="lblGameId" runat="server"/></div></p>
		<p>
			<div>3DApp OAuth Key</div>
			<div class="name" style="height:100px"> 
                <div>Consumer Secret: <asp:Label id="lblConsumerSecret" runat="server"/></div>
                <div>Consumer Key: <asp:Label id="lblConsumerKey" runat="server"/></div>
                <div class="info">(This key is used for authentication bewteen your World and the Kaneva APIs. <a target="_blank" href="http://docs.kaneva.com/mediawiki/index.php/API_Documentation">Learn more</a>)</div>
                <div class="info">Warning: Reseting your consumer key/secret will invalidate your previous consumer key/secret</div>
                <div><asp:button id="btnCreateKey" runat="Server" onClick="btnCreate_Click" CausesValidation="False" Text="Create"/></div>
            </div>
		</p>
		<p>
			<div>Brief Description <asp:RequiredFieldValidator id="rftxtDescription" runat="server" Display="Dynamic" ErrorMessage="Description is a required field." Text="*" ControlToValidate="txtDescription"></asp:RequiredFieldValidator></div>
			<div><asp:TextBox ID="txtDescription" TextMode="multiline" Rows="4" width="700" MaxLength="1000" runat="server"/></div>
		</p>
		<p>
			<div>Category  <asp:CustomValidator id="cvCategories" Text="*" Display="Dynamic" ErrorMessage="You must select at least one category." runat="server" Enabled="False"/></div>                     
		    <ajax:ajaxpanel ID="AjaxpanelAsset">
		        <asp:DataList runat="server" EnableViewState="True" ShowFooter="False" Width="100%" 
					id="dlCategories" cellpadding="0" cellspacing="0" border="0" style="margin-left:10px;" 
					RepeatColumns="4" RepeatDirection="Horizontal" onitemdatabound="dlCategories_OnDataBind" >
			        <ItemStyle HorizontalAlign="left"/>
			        <ItemTemplate>
				        <asp:CheckBox id="chkCategory" runat="server" Checked='<%#GetCatChecked (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "CategoryId")))%>' onclick="javascript:categorySelected(this)"/><%# DataBinder.Eval(Container.DataItem, "CategoryName")%>
				        <input type="hidden" runat="server" id="hidCatId" value='<%#DataBinder.Eval(Container.DataItem, "CategoryId")%>' NAME="hidCatId"> 
			        </ItemTemplate>
		        </asp:DataList>
		    </ajax:ajaxpanel>  
		</p>
		
		<hr class="divider" />
		
		<div class="access">
			<div>Access Type
				<asp:RequiredFieldValidator ID="rfdrpPermission" ControlToValidate="drpPermission" Text="*" ErrorMessage="Permissions is a required field." Display="Dynamic" runat="server"/></div>
			<asp:DropDownList id="drpPermission" runat="Server" Width="150px"/>
			<asp:DropDownList id="drpGameStatus" runat="Server" Enabled="false" visible="false"/>
		</div>
		
		<hr class="divider" />
		
		<div id="profilePhoto">
			<h4>Image Preview</h4>
		    <div class="photo">
                <img runat="server" id="imgPreview"  />
            </div>
            <div class="photoDescription">
                Upload a photo that describes your <asp:literal id="litCommType8" runat="server"></asp:literal>.<br />
                Acceptible formats include: JPG, JPEG or GIF photos under 200k that meet the <a href="http://www.kaneva.com/overview/termsandconditions.aspx" target="_resource" >Terms & Conditions</a>.<br />		        
                Copyrighted photos are NOT allowed.<br />		        
                Users posting explicit photos will be banned.<br />			        
                <b>We recommend 150 x 150</b>		        
            </div>                
	        <div class="photoLink">
                <input type="file" runat="server" id="browseTHUMB" size="45" />
			    <asp:linkbutton id="btnClearThumb" OnClientClick="return confirm('This will remove your community photo, are you sure you wish to proceed?');" runat="Server" Text="Remove" CausesValidation="False"></asp:linkbutton>
	        </div>  
		</div>		
		
													
		<!-- SUBMIT BUTTONS -->
		<div class="btnContainer">
		<asp:button id="btnUpdate" runat="Server" onClick="btnUpdate_Click" CausesValidation="False" Text="Commit Changes"/>
		<asp:button id="btnCancel" runat="Server" CausesValidation="False" onClick="btnCancel_Click" Text="Cancel"/>
		</div>

		<asp:DropDownList id="drpMaturityLevel" runat="Server" visible="false"/>
		<asp:RequiredFieldValidator ID="rfdrpMature" ControlToValidate="drpMaturityLevel" Text="*" ErrorMessage="Permissions is a required field." Display="Dynamic" runat="server"/>
		<!-- SHORT DESCRIPTION -->	
		<asp:TextBox ID="txtTeaser" MaxLength="50" Width="95%" runat="server" visible="false"/>
		<asp:CheckBox id="chkGameStar" runat="server" Checked='false' visible="false"/>
		<asp:DropDownList enabled="false" id="ddlLicenseTypes" runat="server" visible="false"/>
	</div>

</div>

<script type="text/javascript">
<asp:literal id="litJS" runat="server"></asp:literal>
</script>