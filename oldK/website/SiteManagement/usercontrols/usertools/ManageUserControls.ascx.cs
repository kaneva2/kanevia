using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using log4net;
using MagicAjax;
using Kaneva.DataLayer.DataObjects;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;
using System.Text;
using Kaneva;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class ManageUserControls : BaseUserControl
    {
        #region Declarations

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.SITE_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                BindAvailableControls();
            }
            else
            {
                RedirectToHomePage();
            }

        }

        #endregion

        #region Attributes

        protected string ORIGINAL_CONTROLTYPE_ID
        {
            get
            {

                if (ViewState["orginalControlTypeId"] == null)
                {
                    ViewState["orginalControlTypeId"] = "0";
                }

                return ViewState["orginalControlTypeId"].ToString();
            }
            set
            {
                ViewState["orginalControlTypeId"] = value;
            }
        }

        protected string ORIGINAL_CONTROL_NAME
        {
            get
            {

                if (ViewState["orginalControlName"] == null)
                {
                    ViewState["orginalControlName"] = "";
                }

                return ViewState["orginalControlName"].ToString();
            }
            set
            {
                ViewState["orginalControlName"] = value;
            }
        }

        protected IList<KanevaUserControl> USER_CONTROLS
        {
            get
            {

                if (ViewState["userControls"] == null)
                {
                    ViewState["userControls"] = GetUserControls();
                }

                return (IList<KanevaUserControl>)ViewState["userControls"];
            }
            set
            {
                ViewState["userControls"] = value;
            }
        }

        protected DataTable USER_CONTROL_TYPES
        {
            get
            {

                if (ViewState["userControlTypes"] == null)
                {
                    ViewState["userControlTypes"] = WebCache.GetUserControlTypes();
                }

                return (DataTable)ViewState["userControlTypes"];
            }
            set
            {
                ViewState["userControlTypes"] = value;
            }
        }

        #endregion

        #region Helper Functions

        private void ClearUserControls()
        {
            ViewState["userControls"] = null;
        }

        private IList<KanevaUserControl> GetUserControls()
        {
            IList<KanevaUserControl> usercontrol = new List<KanevaUserControl>();

            try
            {
                //pull the main menu from the DB
                usercontrol = this.GetSiteManagementFacade().GetAllUserControls();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during usercontrol retreival", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Error: Sorry we were unable to retreive the usercontrol data.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

            return usercontrol;
        }

        private void BindAvailableControls()
        {
            //update the repeater
            rpt_AvailableUserControls.DataSource = USER_CONTROLS;
            rpt_AvailableUserControls.DataBind();
        }

        #endregion

        #region Functions

        /// <summary>
        /// Add a new site to the list
        /// </summary>
        protected void AddNewControl()
        {
            //create new website object
            KanevaUserControl userControl = new KanevaUserControl(-1, 1, "UC", "n/a");

            ((IList<KanevaUserControl>)rpt_AvailableUserControls.DataSource).Add(userControl);

            //update list
            rpt_AvailableUserControls.DataBind();

            //get the new count and set the new row as active
            int rowId = rpt_AvailableUserControls.Items.Count - 1;

            TextBox tbxControlName = (TextBox)rpt_AvailableUserControls.Items[rowId].FindControl("tbx_ControlName");
            DropDownList ddlControlType = (DropDownList)rpt_AvailableUserControls.Items[rowId].FindControl("ddl_ControlType");
            LinkButton lbnEdit = (LinkButton)rpt_AvailableUserControls.Items[rowId].FindControl("lnkEdit");
            LinkButton lbnUpdate = (LinkButton)rpt_AvailableUserControls.Items[rowId].FindControl("lnkUpdate");
            LinkButton lbnCancel = (LinkButton)rpt_AvailableUserControls.Items[rowId].FindControl("lnkCancel");

            //set the original values to check for changes
            ORIGINAL_CONTROLTYPE_ID = ORIGINAL_CONTROL_NAME = "";

            //reconfigure
            tbxControlName.Enabled = true;
            ddlControlType.Enabled = true;
            lbnEdit.Visible = false;
            lbnUpdate.Visible = true;
            lbnCancel.Visible = true;

        }

        /// <summary>
        /// Save the available wesbite changes
        /// </summary>
        protected int SaveAvailableControls(KanevaUserControl userControl)
        {
            int result = 0;

            if (userControl.UserControlId > 0)
            {
                result = GetSiteManagementFacade().UpdateUserControl(userControl);
            }
            else
            {
                result = GetSiteManagementFacade().AddUserControls(userControl);
            }

            return result;
        }
        
        
        /// <summary>
        /// Delete selected Team Members
        /// </summary>
        protected void DeleteCheckedControls()
        {
            CheckBox chkEdit;
            int deletionCount = 0;
            int result = 0;

            foreach (RepeaterItem rptAvailableUserControls in rpt_AvailableUserControls.Items)
            {
                chkEdit = (CheckBox)rptAvailableUserControls.FindControl("chkEdit");

                if (chkEdit.Checked)
                {
                    int controlId = Convert.ToInt32(((Label)rptAvailableUserControls.FindControl("lbl_ControlId")).Text);

                    result += this.GetSiteManagementFacade().DeleteUserControl(controlId);
                    deletionCount++;
                }
            }

            //message to user
            if (deletionCount == 0)
            {
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "No controls were selected.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
            else if (deletionCount != result)
            {
                //rest the data set to prepare for new data reteival
                ClearUserControls();
                //bind the data to the repeater and drop down (gets data from database if null)
                BindAvailableControls();

                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = result + " of " + deletionCount + " control" + (deletionCount > 1 ? "s were " : " was ") + "deleted.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
            else
            {
                //rest the data set to prepare for new data reteival
                ClearUserControls();
                //bind the data to the repeater and drop down (gets data from database if null)
                BindAvailableControls();

                messages.ForeColor = System.Drawing.Color.DarkGreen;
                messages.Text = deletionCount + " control" + (deletionCount > 1 ? "s were " : " was ") + "deleted.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }

        #endregion

        #region event handlers

        /// <summary>
        /// btn_DeleteSites_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_DeleteUserControls_Click(object sender, System.EventArgs e)
        {
            try
            {
                DeleteCheckedControls();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during control deletion", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Editing Error: Sorry we were unable to delete the selected control.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }

        }

        /// <summary>
        /// lnkAddSite_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void lbn_NewUserControl_Click(object sender, System.EventArgs e)
        {
            try
            {
                AddNewControl();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error during control addition", ex);
                messages.ForeColor = System.Drawing.Color.Crimson;
                messages.Text = "Editing Error: Sorry we were unable to create a new control entry.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
            }
        }


        private void rpt_AvailableUserControls_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
        }

        private void rpt_AvailableUserControls_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //get the controls
                Label lblControlId = (Label)e.Item.FindControl("lbl_ControlId");
                TextBox tbxControlName = (TextBox)e.Item.FindControl("tbx_ControlName");
                DropDownList ddlControlType = (DropDownList)e.Item.FindControl("ddl_ControlType");

                //set the pull down data
                ddlControlType.DataSource = USER_CONTROL_TYPES;
                ddlControlType.DataTextField = "control_type";
                ddlControlType.DataValueField = "control_type_id";
                ddlControlType.DataBind();

                lblControlId.Text = ((KanevaUserControl)e.Item.DataItem).UserControlId.ToString();
                tbxControlName.Text = ((KanevaUserControl)e.Item.DataItem).UserControlName;
                ddlControlType.SelectedValue = ((KanevaUserControl)e.Item.DataItem).ControlTypeId.ToString();
            }
        }

        /// <summary>
        /// rptGames_ItemCommand
        /// </summary>
        private void rpt_AvailableUserControls_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //get the controls
            Label lblControlId = (Label)rpt_AvailableUserControls.Items[e.Item.ItemIndex].FindControl("lbl_ControlId");
            TextBox tbxControlName = (TextBox)rpt_AvailableUserControls.Items[e.Item.ItemIndex].FindControl("tbx_ControlName");
            DropDownList ddlControlType = (DropDownList)rpt_AvailableUserControls.Items[e.Item.ItemIndex].FindControl("ddl_ControlType");
            LinkButton lbnEdit = (LinkButton)rpt_AvailableUserControls.Items[e.Item.ItemIndex].FindControl("lnkEdit");
            LinkButton lbnUpdate = (LinkButton)rpt_AvailableUserControls.Items[e.Item.ItemIndex].FindControl("lnkUpdate");
            LinkButton lbnCancel = (LinkButton)rpt_AvailableUserControls.Items[e.Item.ItemIndex].FindControl("lnkCancel");


            switch (e.CommandName)
            {
                case "cmdEdit":
                    {
                        //set the original values to check for changes
                        ORIGINAL_CONTROLTYPE_ID = ddlControlType.SelectedValue;
                        ORIGINAL_CONTROL_NAME = tbxControlName.Text;

                         //reconfigure
                        tbxControlName.Enabled = true;
                        ddlControlType.Enabled = true;

                        lbnEdit.Visible = false;
                        lbnUpdate.Visible = true;
                        lbnCancel.Visible = true;
                        break;
                    }
                case "cmdUpdate":
                    {
                        //check for changes
                        string ORIGINAL_VALUE = ORIGINAL_CONTROL_NAME + ORIGINAL_CONTROLTYPE_ID;
                        if (!ORIGINAL_VALUE.Equals(tbxControlName.Text + ddlControlType.SelectedValue))
                        {
                            try
                            {
                                //write changes to database
                                KanevaUserControl usercontrol = new KanevaUserControl();
                                usercontrol.UserControlId = Convert.ToInt32(lblControlId.Text);
                                usercontrol.UserControlName = tbxControlName.Text;
                                usercontrol.ControlTypeId = Convert.ToInt32(ddlControlType.SelectedValue);
                                usercontrol.ControlType = ddlControlType.Text;

                                int result = SaveAvailableControls(usercontrol);

                                //message to user
                                if (result == 0)
                                {
                                    messages.ForeColor = System.Drawing.Color.Crimson;
                                    messages.Text = "Unable to save your user control changes.";
                                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
                                }
                                else
                                {
                                    //get newest data for websites
                                    ClearUserControls();
                                    //bind the data to the repeater and drop down
                                    BindAvailableControls();

                                    //alert user to success
                                    messages.ForeColor = System.Drawing.Color.DarkGreen;
                                    messages.Text = "User Control changes saved successfully.";
                                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + messages.ClientID + "', 5000);");
                                }

                            }
                            catch (Exception)
                            {
                            }
                        }
                        //reconfigure
                        tbxControlName.Enabled = false;
                        ddlControlType.Enabled = false;

                        lbnEdit.Visible = true;
                        lbnUpdate.Visible = false;
                        lbnCancel.Visible = false;

                        break;
                    }
                case "cmdCancel":
                    {
                        if (Convert.ToInt32(lblControlId.Text) < 1)
                        {
                            ((IList<KanevaUserControl>)rpt_AvailableUserControls.DataSource).RemoveAt(e.Item.ItemIndex);
                            rpt_AvailableUserControls.DataBind();
                        }
                        else
                        {
                            //set the original values back
                            ddlControlType.SelectedValue = ORIGINAL_CONTROLTYPE_ID;
                            tbxControlName.Text = ORIGINAL_CONTROL_NAME;

                            //reconfigure
                            tbxControlName.Enabled = false;
                            ddlControlType.Enabled = false;

                            lbnEdit.Visible = true;
                            lbnUpdate.Visible = false;
                            lbnCancel.Visible = false;
                        }

                        break;
                    }
            }
        }

        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.rpt_AvailableUserControls.ItemCreated += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_AvailableUserControls_ItemCreated);
            this.rpt_AvailableUserControls.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_AvailableUserControls_ItemDataBound);
            this.rpt_AvailableUserControls.ItemCommand += new System.Web.UI.WebControls.RepeaterCommandEventHandler(this.rpt_AvailableUserControls_ItemCommand);
        }
        #endregion

    }
}