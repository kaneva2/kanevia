<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="RewardsManagement.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.RewardsManagement" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>


<script type="text/javascript" src="jscript/yahoo/yahoo-min.js"></script>  
<script type="text/javascript" src="jscript/yahoo/event-min.js" ></script>   
<script type="text/javascript" src="jscript/yahoo/dom-min.js" ></script>   
<script type="text/javascript" src="jscript/yahoo/calendar-min.js"></script>

<script type="text/javascript" language="javascript" src="jscript/prototype.js"></script>

<link href="css/yahoo/calendar.css" type="text/css" rel="stylesheet">   

<style >
	#cal1Container { display:none; position:absolute; z-index:100;}
	#cal2Container { display:none; position:absolute; z-index:100;}
</style>

<script type="text/javascript"><!--

YAHOO.namespace("example.calendar");

 var txtStartDate_ID = '<%= txtStartDate.ClientID %>';
 var txtEndDate_ID = '<%= txtEndDate.ClientID %>';
    
	function handleStartDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate1 = document.getElementById(txtStartDate_ID);
		txtDate1.value = month + "/" + day + "/" + year + " 00:00:01";
		YAHOO.example.calendar.cal1.hide();
	}
	
	function handleEndDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate2 = document.getElementById(txtEndDate_ID);
		txtDate2.value = month + "/" + day + "/" + year + " 23:59:59";
		YAHOO.example.calendar.cal2.hide();
	}

	function initEventStart() {
		// Admin Calendar
		YAHOO.example.calendar.cal1 = new YAHOO.widget.Calendar ("cal1", "cal1Container", { iframe:true, zIndex:200, mindate:"1/1/2008", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal1.render();
		
		// Listener to show Admin Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgSelectStartDate", "click", YAHOO.example.calendar.cal1.show, YAHOO.example.calendar.cal1, true);   
		YAHOO.example.calendar.cal1.selectEvent.subscribe(handleStartDate, YAHOO.example.calendar.cal1, true);
	}
	
	function initEventEnd() {
		// Event Calendar
		YAHOO.example.calendar.cal2 = new YAHOO.widget.Calendar ("cal2", "cal2Container", { iframe:true, zIndex:1000, mindate:"1/1/2008", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal2.render();
		
		// Listener to show the Event Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgSelectEndDate", "click", YAHOO.example.calendar.cal2.show, YAHOO.example.calendar.cal2, true);   
		YAHOO.example.calendar.cal2.selectEvent.subscribe(handleEndDate, YAHOO.example.calendar.cal2, true);
	}
//--> 
</script>  
<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    <ajax:ajaxpanel id="ajMessage" runat="server">
        <asp:label style="position:relative; font-size:18px; padding: 10px 0 10px 0" runat="server" id="messages" visible="true" />
             <table id="tblRestrictions" border="0" cellpadding="0" cellspacing="0" width="998" style="background-color:#ffffff">
                <tr>
                    <td align="center">
			            <span style="height:30px; font-size:24px; font-weight:bold">Rewards Management</span><br />
            			
			            <div style="width:600px;margin-top:20px;text-align:center;">
				            <asp:validationsummary cssclass="errbox" id="valSum" runat="server" showmessagebox="False" showsummary="True"
				 	            displaymode="BulletList" headertext="Please correct the following errors:" forecolor="black"></asp:validationsummary>
				            <asp:customvalidator id="cvBlank" runat="server" display="None" enableclientscript="False"></asp:customvalidator>
			            </div>	
            			
			            <!-- validators -->
			            <asp:requiredfieldvalidator id="rfvAmount" runat="server" display="none" enableclientscript="false" controltovalidate="txtAmount" enabled="false" errormessage="You must enter a valid amount." ></asp:requiredfieldvalidator>
			            <asp:requiredfieldvalidator id="rfvEndDate" runat="server" display="none" enableclientscript="false" controltovalidate="txtEndDate" enabled="false" errormessage="You must enter a valid end date and time." ></asp:requiredfieldvalidator>
			            <asp:requiredfieldvalidator id="rfvStartDate" runat="server" display="none" enableclientscript="false" controltovalidate="txtStartDate" enabled="false" errormessage="You must enter a valid start date and time." ></asp:requiredfieldvalidator>
			            <asp:requiredfieldvalidator id="rfvUserList" runat="server" display="none" enableclientscript="false" controltovalidate="txtSurveyUserList" enabled="false" errormessage="You must enter either email addresses or userId seperated by commas." ></asp:requiredfieldvalidator>
			            <asp:requiredfieldvalidator id="rfvEventName" runat="server" display="none" enableclientscript="false" controltovalidate="txtEventName" enabled="false" errormessage="You must enter an Event Name." ></asp:requiredfieldvalidator>
            															
		            </td>
                </tr>
                <tr>
		            <td align="center">
            			
				            <!-- Rewards Mgt Nav -->
				            <div style="text-align:center;margin:20px 0 20px 0; width:100%">
					            <asp:linkbutton id="lbEventAdmin" oncommand="EventNav_OnCommand" commandname="ea" runat="server" text="Event Administration" causesvalidation="false"></asp:linkbutton>&nbsp;&nbsp;|&nbsp;&nbsp; 
					            <asp:linkbutton id="lbEventParticipation" oncommand="EventNav_OnCommand" commandname="ep" runat="server" text="Event Participation" causesvalidation="false"></asp:linkbutton>&nbsp;&nbsp;|&nbsp;&nbsp; 
					            <asp:linkbutton id="lbSurveyParticipation" oncommand="EventNav_OnCommand" commandname="sp" runat="server" text="Survey Participation" causesvalidation="false"></asp:linkbutton>&nbsp;&nbsp;|&nbsp;&nbsp; 
					            <asp:linkbutton id="lbContestParticipation" oncommand="EventNav_OnCommand" commandname="cp" runat="server" text="Contest Participation" causesvalidation="false"></asp:linkbutton> 
				            </div>
            				
				            <div id="divEventAdmin" runat="server" style="display:none; ">
					            <fieldset title="Event Administration" style="padding:0px 10px 10px 10px;width: 96%;">
						            <legend><h3>Event Administration</h3></legend>   
						            <table cellpadding="5" cellspacing="0" border="0" style="margin:20px 0 20px 0; width: 94%">
							            <tr>
								            <td align="right" style="width:100px">Event Name:</td>
								            <td align="left"><asp:textbox id="txtEventName" runat="server" style="width:80px"></asp:textbox></td>
							            </tr>
							            <tr>
								            <td align="right" style="width:100px">Tranaction Type:</td>
								            <td align="left">
									            <asp:DropDownList class="formKanevaText" id="drpTransType" style="width:180px" runat="server"></asp:DropDownList>
								            </td>
							            </tr>
							            <tr>
								            <td align="right" style="width:100px"><asp:textbox id="txtRewardEventId" runat="server" visible="false"></asp:textbox></td>
								            <td align="left"><br />
									            <asp:button id="btnAddEvent" runat="server" onclick="btnAddEvent_Click" text=" Save Event " />
									            
								            </td>
							            </tr>
						            </table>
            						
						            <div style="height:20px;"></div>
            						
						            <asp:gridview id="dgRewardEvents" runat="server" 
							            OnRowEditing="dgRewardEvents_RowEditing"
							            AllowSorting="True" border="0" cellpadding="4" 
							            autogeneratecolumns="False" 
							            bordercolor="DarkGray" borderstyle="solid" borderwidth="1px" 
							            PageSize="50" AllowPaging="False" width="94%" >
            							
							            <RowStyle BackColor="White"></RowStyle>
							            <HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
							            <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
							            <FooterStyle BackColor="#FFffff"></FooterStyle>
							            <Columns>
								            <asp:BoundField HeaderText="Id" DataField="reward_event_id" ReadOnly="True" ConvertEmptyStringToNull="False" visible="True" itemstyle-width="30px" ></asp:BoundField>
								            <asp:BoundField HeaderText="Event Name" DataField="event_name" SortExpression="event_name" ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-width="250px" ></asp:BoundField>
								            <asp:BoundField HeaderText="Transaction Type" DataField="trans_desc" SortExpression="trans_desc" ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-width="120px"></asp:BoundField>
								            <asp:BoundField HeaderText="Id" DataField="transaction_type_id" SortExpression="transaction_type_id" itemstyle-width="1px" headerstyle-width="1px" ReadOnly="True" ConvertEmptyStringToNull="False"></asp:BoundField>
								            <asp:CommandField CausesValidation="False" ShowEditButton="True" itemstyle-width="40px"></asp:CommandField>
								            <asp:TemplateField itemstyle-width="60px" visible="false">
									            <ItemTemplate>
										            <asp:LinkButton id="lbnConfirmDelete" Runat="server" causesvalidation="false" OnClientClick="return confirm('Are you sure you want to delete this event?');" CommandName="Delete">Delete</asp:LinkButton>
									            </ItemTemplate>
								            </asp:TemplateField>
            			        
							            </Columns>
						            </asp:gridview>
					            </fieldset>
				            </div>
            				
				            <div id="divEventParticipation" runat="server" style="display:none;">
					            <fieldset title="Event Participation" style="padding:0px 10px 10px 10px;width:50%;">
						            <legend><h3>Event Participation</h3></legend>   
						            <table cellpadding="5" cellspacing="0" border="0" align="left" style="margin:20px 0 20px 0;">
							            <tr>
								            <td align="right">Event:</td>
								            <td align="left"><asp:DropDownList class="formKanevaText" id="drpEvents" style="width:200px" runat="server"></asp:DropDownList></td>
							            </tr>
							            <tr>
								            <td align="right">Start Date/Time:</td>
								            <td align="left">
									            <asp:textbox id="txtStartDate" runat="server" style="width:180px"  ></asp:textbox>
									            <img id="imgSelectStartDate" name="imgSelectStartDate" onload="initEventStart();" src="images/cal_16.gif"/> 
									            <div id="cal1Container"></div>
								            </td>
							            </tr>
							            <tr>
								            <td align="right">End Date/Time:</td>
								            <td align="left">
									            <asp:textbox id="txtEndDate" runat="server" style="width:180px"  ></asp:textbox>
									            <img id="imgSelectEndDate" name="imgSelectEndDate" onload="initEventEnd();" src="images/cal_16.gif"/> 
									            <div id="cal2Container"></div>
								            </td>
							            </tr>
							            <tr>
								            <td align="right">Currency:</td>
								            <td align="left">
									            <asp:DropDownList class="formKanevaText" id="drpCurrency" style="width:180px" runat="server">
										            <asp:ListItem Text="GPOINT (Rewards)" Value="GPOINT"></asp:ListItem>
										            <asp:ListItem Text="KPOINT (Credits)" Value="KPOINT"></asp:ListItem>
									            </asp:DropDownList>
								            </td>
							            </tr>
							            <tr>
								            <td align="right">Amount:</td>
								            <td align="left">
									            <asp:textbox id="txtAmount" runat="server" style="width:180px"></asp:textbox>
								            </td>
							            </tr>
							            <tr>
								            <td colspan="2" align="center">
									            <asp:button id="btnAward" runat="server" onclick="btnAward_Click" text="Award Now" />
								            </td>
							            </tr>
						            </table>
					            </fieldset>
				            </div>
            				
				            <div id="divSurveyParticipation" runat="server" style="display:none;">
					            <fieldset title="Survey Participation" style="padding:0px 10px 10px 10px;width:60%;">
						            <legend><h3>Survey Participation</h3></legend>   
						            <table cellpadding="5" cellspacing="0" border="0" align="left" style="margin:20px 0 20px 0;">
							            <tr>
								            <td align="right">Event:</td>
								            <td align="left"><asp:DropDownList class="formKanevaText" id="drpSurveyEvents" style="width:200px" runat="server"></asp:DropDownList></td>
							            </tr>
							            <tr>
								            <td align="right">Amount:</td>
								            <td align="left">
									            <asp:textbox id="txtSurveyAmount" runat="server" style="width:180px"></asp:textbox>
								            </td>
							            </tr>
							            <tr>
								            <td align="right">Currency:</td>
								            <td align="left">
									            <asp:DropDownList class="formKanevaText" id="drpSurveyCurrency" style="width:180px" runat="server">
										            <asp:ListItem Text="GPOINT (Rewards)" Value="GPOINT"></asp:ListItem>
										            <asp:ListItem Text="KPOINT (Credits)" Value="KPOINT"></asp:ListItem>
									            </asp:DropDownList>
								            </td>
							            </tr>
							            <tr valign="top">
								            <td align="right">User List:</td>
								            <td style="text-align:left">
									            <asp:textbox id="txtSurveyUserList" TextMode="multiline" Rows="8" style="width:400px" runat="server" cssclass="insideBoxText11" /><br/>
									            (Email addresses or user ids separated by commas)<br />
									            'admin@kaneva.com','csr@kaneva.com','boss@kaneva.com'<br />
									            34234,23423,234235,234234,234256,53452
								            </td>
							            </tr>
							            <tr>
								            <td colspan="2" align="right"><br />
									            <asp:button id="btnAwardSurvey" runat="server" onclick="btnAwardSurvey_Click" text="Award Now" />
								            </td>
							            </tr>
						            </table>
					            </fieldset>
				            </div>
	            <body>		
				            <div id="divContestParticipation" runat="server" style="display:none;">
					            <fieldset title="Contest Participation" style="padding:0px 10px 10px 10px;width:50%;">
						            <legend><h3>Contest Participation</h3></legend>   
						            <table cellpadding="5" cellspacing="0" border="0" align="left" style="margin:20px 0 20px 0;">
							            <tr>
								            <td colspan="4">Constest Name: <asp:dropdownlist id="drpContest" runat="server" style="width:200px;"></asp:dropdownlist><br /><br /></td>
							            </tr>
							            <tr>
								            <th></th>
								            <th>Username</th>
								            <th>Amount</th>
								            <th>Currency</th>
							            </tr>
							            <tr>
								            <td>1.</td>
								            <td><asp:textbox id="txtUser1" runat="server" maxlength="50" style="width:160px"></asp:textbox></td>
								            <td><asp:textbox id="txtAmt1" runat="server" maxlength="50" style="width:60px"></asp:textbox></td>
								            <td><asp:dropdownlist id="drpCurrency1" runat="server"></asp:dropdownlist></td>
							            </tr>
							            <tr>
								            <td>2.</td>
								            <td><asp:textbox id="txtUser2" runat="server" maxlength="50" style="width:160px"></asp:textbox></td>
								            <td><asp:textbox id="txtAmt2" runat="server" maxlength="50" style="width:60px"></asp:textbox></td>
								            <td><asp:dropdownlist id="drpCurrency2" runat="server"></asp:dropdownlist></td>
							            </tr>
							            <tr>
								            <td>3.</td>
								            <td><asp:textbox id="txtUser3" runat="server" maxlength="50" style="width:160px"></asp:textbox></td>
								            <td><asp:textbox id="txtAmt3" runat="server" maxlength="50" style="width:60px"></asp:textbox></td>
								            <td><asp:dropdownlist id="drpCurrency3" runat="server"></asp:dropdownlist></td>
							            </tr>
							            <tr>
								            <td>4.</td>
								            <td><asp:textbox id="txtUser4" runat="server" maxlength="50" style="width:160px"></asp:textbox></td>
								            <td><asp:textbox id="txtAmt4" runat="server" maxlength="50" style="width:60px"></asp:textbox></td>
								            <td><asp:dropdownlist id="drpCurrency4" runat="server"></asp:dropdownlist></td>
							            </tr>
							            <tr>
								            <td>5.</td>
								            <td><asp:textbox id="txtUser5" runat="server" maxlength="50" style="width:160px"></asp:textbox></td>
								            <td><asp:textbox id="txtAmt5" runat="server" maxlength="50" style="width:60px"></asp:textbox></td>
								            <td><asp:dropdownlist id="drpCurrency5" runat="server"></asp:dropdownlist></td>
							            </tr>
							            <tr>
								            <td>6.</td>
								            <td><asp:textbox id="txtUser6" runat="server" maxlength="50" style="width:160px"></asp:textbox></td>
								            <td><asp:textbox id="txtAmt6" runat="server" maxlength="50" style="width:60px"></asp:textbox></td>
								            <td><asp:dropdownlist id="drpCurrency6" runat="server"></asp:dropdownlist></td>
							            </tr>
							            <tr>
								            <td>7.</td>
								            <td><asp:textbox id="txtUser7" runat="server" maxlength="50" style="width:160px"></asp:textbox></td>
								            <td><asp:textbox id="txtAmt7" runat="server" maxlength="50" style="width:60px"></asp:textbox></td>
								            <td><asp:dropdownlist id="drpCurrency7" runat="server"></asp:dropdownlist></td>
							            </tr>
							            <tr>
								            <td>8.</td>
								            <td><asp:textbox id="txtUser8" runat="server" maxlength="50" style="width:160px"></asp:textbox></td>
								            <td><asp:textbox id="txtAmt8" runat="server" maxlength="50" style="width:60px"></asp:textbox></td>
								            <td><asp:dropdownlist id="drpCurrency8" runat="server"></asp:dropdownlist></td>
							            </tr>
							            <tr>
								            <td>9.</td>
								            <td><asp:textbox id="txtUser9" runat="server" maxlength="50" style="width:160px"></asp:textbox></td>
								            <td><asp:textbox id="txtAmt9" runat="server" maxlength="50" style="width:60px"></asp:textbox></td>
								            <td><asp:dropdownlist id="drpCurrency9" runat="server"></asp:dropdownlist></td>
							            </tr>
							            <tr>
								            <td>10.</td>
								            <td><asp:textbox id="txtUser10" runat="server" maxlength="50" style="width:160px"></asp:textbox></td>
								            <td><asp:textbox id="txtAmt10" runat="server" maxlength="50" style="width:60px"></asp:textbox></td>
								            <td><asp:dropdownlist id="drpCurrency10" runat="server"></asp:dropdownlist></td>
							            </tr>
							            <tr>
								            <td colspan="4" align="right"><br />
									            <asp:button id="btnAwardContest" runat="server" onclick="btnAwardContest_Click" text="Award Now" />
								            </td>
							            </tr>
						            </table>	  
					            </fieldset>
				            </div>
	            </body>				
            			
			            <br />
		            </td>
	            </tr>
            </table>
    </ajax:ajaxpanel>
</asp:Panel> 