<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GameViralCoefficients.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.GameViralCoefficients" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<link href="../../css/base/base.css" type="text/css" rel="stylesheet">
<link href="../../css/Communities/GameViralCoefficient.css" type="text/css" rel="stylesheet">

<div id="vcContainer">

    <div id="divVCTitle" class="title">Viral Coefficient</div>
    <div id="divErrorTop" runat="server" class="error" style="display:none;"></div>
    
    <div id="vcData">
        <div id="data">
            <asp:repeater id="rptViralCoefficients" runat="server">
                <HeaderTemplate>
                    <div class="header">
                        <div class="dates">Date Range</div>
                        <div class="invitees_count">Average Number of Invitees</div>
                        <div class="accepted_invites">Percent of Invites Accepted</div>
                        <div class="inviters">Percent of Inviters</div>
                        <div class="viral_coefficient">Viral Coefficient</div>
				    </div>
                </HeaderTemplate>
                <itemtemplate>
                    <div class="row">
                        <div class="coefficientInfo">
						    <div class="dates">0</div>
                            <div class="invitees_count">0</div>
                            <div class="accepted_invites">0</div>
					        <div class="inviters">0</div>
                            <div class="viral_coefficient">0</div>
                        </div>
                    </div>
                </itemtemplate>
            </asp:repeater>
        </div>

        <div class="results"><asp:Label runat="server" id="lblSearch"/></div>
        <div class="pager"><Kaneva:Pager runat="server" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true"/></div>
    
        <a name="details" href="javascript:void(0);"></a>
		<a id="aScroll" href="#details" style="visibility:hidden;"></a>
    </div>
</div>
