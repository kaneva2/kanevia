using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using System.Linq;

using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;
using System.Text;
using MagicAjax;
using System.Drawing;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class GameData : BaseUserControl
    {
        #region Declarations
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private bool isAdministraitor = false;
        private bool canEdit = false;
        private int ownerId = 0;
        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.STAR_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        isAdministraitor = true;
                        canEdit = true;
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    default:
                        break;
                }

                // Hide error section
                divErrorData.Style.Add ("display", "none");

                if ((Session["SelectedGameId"] != null) && Convert.ToInt32(Session["SelectedGameId"]) > 0)
                {
                    // Load the current user's data the first time
                    SELECTED_GAME_ID = Convert.ToInt32(Session["SelectedGameId"]);

                    //get the company id the game is associated with
                    this.ownerId = GetGameFacade().GetGameOwner(SELECTED_GAME_ID);

                    if (!isAdministraitor)
                    {
                        canEdit = this.ownerId.Equals(GetUserId());
                    }

                    if (!canEdit)
                    {
                        RedirectToHomePage();
                    }
                    else
                    {
                        //community id check is to allow kaneva website to make use of this usercontrol
                        if (!IsPostBack || (Request["communityId"] != null))
                        {
                            // Log the CSR activity
                            this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Viewing game information page", 0, SELECTED_GAME_ID);

                            //configure page
                            ConfigurePageForUse();
                        }
                    }
                }
                else
                {
                    //community id check is to tell it to redirect to search page (for KWAS only)
                    if (Request["communityId"] == null)
                    {
                        //store this pages URL for return
                        Session["CallingPage"] = Request.Url.ToString();
                        Response.Redirect(ResolveUrl("~/Default.aspx?ws=" + GetCurrentSiteID((int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.DEVELOPER) + "&mn=" + GetGameSearchParentMenuID() + "&sn=" + GetGameSearchControlID()));
                    }
                }

                //set validation values
                revName.ValidationExpression = Constants.VALIDATION_REGEX_CHANNEL_NAME;
                revName.ErrorMessage = Constants.VALIDATION_REGEX_GAME_NAME_ERROR_MESSAGE;
                //regtxtKeywords.ValidationExpression = Kaneva.Constants.VALIDATION_REGEX_TAG;
                //regtxtKeywords.ErrorMessage = Kaneva.Constants.VALIDATION_REGEX_TAG_ERROR_MESSAGE;
            }
            else
            {
                RedirectToHomePage();
            }
        }

        #endregion

        #region Functions

        private void BindGameData()
        {
            //get the game to be edited from the database
            GameToEdit = GetGameFacade().GetGameByGameId(SELECTED_GAME_ID);

            //get the related license for the game
            try
            {
                GameLicense = GetGameFacade().GetGameLicenseByGameId(SELECTED_GAME_ID);
            }
            catch (Exception ex)
            {
                ShowMessage ("Error on GameLicense Load for editing");
                m_logger.Error("Error on GameLicense Load for editing", ex);
            }

            //if game could not be loaded stop processing and alert the user
            if (GameToEdit != null)
            {
                try
                {
                    string gameName = Server.HtmlDecode(GameToEdit.GameName);
                    drpGameStatus.SelectedValue = GameToEdit.GameStatusId.ToString();
                    drpMaturityLevel.SelectedValue = GameToEdit.GameRatingId.ToString();
                    //txtKeywords.Text = GameToEdit.GameKeyWords;

                    PopulateGameAccessDropDown ();
                    drpPermission.SelectedValue = GameToEdit.GameAccessId.ToString ();

                    //cbOver21.Checked = GameToEdit.Over21Required;

                    if (gameName.IndexOf('-') >= 0)
                    {
                        gameName = gameName.Substring(gameName.IndexOf('-') + 1);
                    }

                    // Per AS, don't allow to change game name
                    txtGameName.Enabled = false;

                    txtGameName.Text = gameName;
                    lblGameId.Text = GameToEdit.GameId.ToString ();
                    txtTeaser.Text = Server.HtmlDecode(GameToEdit.GameSynopsis);
                    txtDescription.Text = Server.HtmlDecode(GameToEdit.GameDescription);
                    chkGameStar.Checked = GameToEdit.ParentGameId != 0;
                    chkGameStar.Enabled = false; // can't change it after creation

                    // attempt to load and display the thumbnail
                    if ((GameToEdit.GameImagePath != null) && (GameToEdit.GameImagePath != ""))
                    {
                        //tblThumbPreview.Visible = true;
                        //generate the full path from config and data base values
                        imgPreview.Src = GetBroadcastChannelImageURL (GameToEdit.GameImagePath, "me");
                    }

                    int communityId = GetCommunityFacade().GetCommunityIdFromGameId(SELECTED_GAME_ID);
                    GamesCategories = GetCommunityFacade().GetCommunityCategories(communityId);

                    lblCreatedBy.Text = GetUserFacade().GetUserName (GameToEdit.OwnerId);

                    ddlLicenseTypes.SelectedValue = GameLicense.LicenseSubscriptionId.ToString();

                    //populate the available game categories
                    //must happen after the bind for editing to capture the checked fields
                    dlCategories.DataSource = WebCache.GetGameCategories ();
                    dlCategories.DataBind ();

                    // The API auth Key
                    APIAuthentication apiAuth = GetGameFacade().GetAPIAuth(GameToEdit.GameId);

                    if (apiAuth.CommunityId > 0)
                    {
                        lblConsumerSecret.Text = apiAuth.ConsumerSecret;
                        lblConsumerKey.Text = apiAuth.ConsumerKey;
                    }
                    else
                    {
                        lblConsumerSecret.Text = "*** Not created yet ***";
                    }

                // Hide for incubator PER Animesh (WARNING: this is bad end user has no way to change if thier key is stolen)
                if (GameToEdit.IsIncubatorHosted)
                {
                    btnCreateKey.Visible = false;
                }

                }
                catch (Exception ex)
                {
                    ShowMessage ("Error while loading game data");
                    m_logger.Error("Error gameEdit: while loading game data", ex);
                }
            }
            else
            {
                ShowMessage ("No Game Information found");
                m_logger.Error("No Game Information found");
            }
        }

        
        private void ConfigurePageForUse()
        {
            try
            {
                //populate the page's drop downs
                PopulateLicenseTypeDropDown();
                PopulateGameStatusDropDown();
                PopulateRestrictionLevelDropDown();

                //populate the available game categories

                //bind the main data
                BindGameData ();
            }
            catch (Exception ex)
            {
                ShowMessage("Error while loading game data");
                m_logger.Error("Error gameEdit: while loading game data", ex);
            }

        }

        /// <summary>
        /// Populates Game Status Options Pull down
        /// </summary>
        private void PopulateGameStatusDropDown()
        {
            drpGameStatus.DataSource = WebCache.GetGameStatus();
            drpGameStatus.DataTextField = "game_status";
            drpGameStatus.DataValueField = "game_status_id";
            drpGameStatus.DataBind();
        }

        /// <summary>
        /// Populates License Type Pull down
        /// </summary>
        private void PopulateLicenseTypeDropDown()
        {
            ddlLicenseTypes.DataSource = WebCache.GetGameLicenseTypes();
            ddlLicenseTypes.DataTextField = "Name";
            ddlLicenseTypes.DataValueField = "LicenseSubscriptionId";
            ddlLicenseTypes.DataBind();
        }

        /// <summary>
        /// Populates Game Access Options Pull down
        /// </summary>
        private void PopulateGameAccessDropDown()
        {
            bool isSiteAdmin = false;
            
            // Need to find out if user is a site admin or just an owner/moderator
            switch (CheckUserAccess ((int) SitePrivilege.ePRIVILEGE.SITE_ADMIN))
            {
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                    isSiteAdmin = true;
                    break;
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                default:
                    break;
            }
            
            // This case handles owner/moderators only.  They only see a subset
            // of the access options.  Site Admins see all the access options
            if (!isSiteAdmin)
            {
                drpPermission.Items.Clear ();

                if (GameToEdit.GameAccessId == (int) eGAME_ACCESS.BANNED)
                {
                    drpPermission.Items.Add (new ListItem ("banned", ((int) eGAME_ACCESS.BANNED).ToString ()));
                    drpPermission.Enabled = false;
                }
                else
                {
                    DataTable dtAccess = WebCache.GetGameAccess ();
                    for (int i = 0; i < dtAccess.Rows.Count; i++)
                    {
                        if (Convert.ToInt32 (dtAccess.Rows[i]["game_access_id"]) == (int) eGAME_ACCESS.PUBLIC ||
                        Convert.ToInt32 (dtAccess.Rows[i]["game_access_id"]) == (int) eGAME_ACCESS.PRIVATE)
                        {
                            drpPermission.Items.Add (new ListItem (dtAccess.Rows[i]["game_access"].ToString (), dtAccess.Rows[i]["game_access_id"].ToString ()));
                        }
                    }
                }
            }
            else
            {
                drpPermission.DataSource = WebCache.GetGameAccess ();
                drpPermission.DataTextField = "game_access";
                drpPermission.DataValueField = "game_access_id";
                drpPermission.DataBind ();
            }
        }

        /// <summary>
        /// Populates Restrcition Levels Pull down
        /// </summary>
        private void PopulateRestrictionLevelDropDown()
        {
            drpMaturityLevel.DataSource = WebCache.GetGameRatings();
            drpMaturityLevel.DataTextField = "game_rating";
            drpMaturityLevel.DataValueField = "game_rating_id";
            drpMaturityLevel.DataBind();
        }

        private void ShowMessage (string msg)
        {
            ShowMessage (msg, true);
        }
        private void ShowMessage (string msg, bool isError)
        {
            divErrorData.InnerText = msg;
            divErrorData.Attributes.Add ("class", isError ? "error" : "confirm");
            divErrorData.Style.Add ("display", "block");
        }

        #endregion


        #region Attributes
       
        /// <summary>
        /// SELECTED_GAME_ID
        /// </summary>
        /// <returns></returns>
        private int SELECTED_GAME_ID
        {
            get
            {
                return (int)ViewState["game_id"];
            }
            set
            {
                ViewState["game_id"] = value;
            }

        }

        /// <summary>
        /// stores the game id of the game to be added/edited
        /// </summary>
        /// <returns>int</returns>
        private List<CommunityCategory> GamesCategories
        {
            get
            {
                if (ViewState["gamesCategories"] == null)
                {
                    return null;
                }
                return (List<CommunityCategory>)ViewState["gamesCategories"];
            }
            set
            {
                ViewState["gamesCategories"] = value;
            }
        }

        /// <summary>
        /// stores the gamelicense data to reduce database round trips
        /// </summary>
        /// <returns>GameLicense</returns>
        private GameLicense GameLicense
        {
            get
            {
                if (ViewState["gameLicense"] == null)
                {
                    return null;
                }
                return (GameLicense)ViewState["gameLicense"];
            }
            set
            {
                ViewState["gameLicense"] = value;
            }
        }

        /// <summary>
        /// stores the game data to reduce database round trips
        /// </summary>
        /// <returns>Game</returns>
        private Game GameToEdit
        {
            get
            {
                if (ViewState["game"] == null)
                {
                    return null;
                }
                return (Game)ViewState["game"];
            }
            set
            {
                ViewState["game"] = value;
            }
        }

        #endregion


        #region Event Handlers

        /// <summary>
        /// Is the category checked?
        /// </summary>
        /// <param name="catId"></param>
        /// <returns></returns>
        protected bool GetCatChecked(int catId)
        {
            bool isChecked = false;
            //searh stored categories
            if ((GamesCategories != null) && (GamesCategories.Count > 0))
            {
                isChecked = ((GamesCategories.Select(m => m.CategoryId == catId).Count()) > 0 ? true : false);
            }
            return isChecked;
        }

        /// <summary>
        /// Update Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                Game game = GetGameFacade().GetGameByGameId(SELECTED_GAME_ID);
                
                string consumerKey = game.GameName.Replace(" ", "");
                string consumerSecret = Guid.NewGuid().ToString().Replace ("-", "K");

                GetCommunityFacade().InsertAPIAuth(GetCommunityFacade ().GetCommunityIdFromGameId (SELECTED_GAME_ID) , consumerKey, consumerSecret);

                lblConsumerKey.Text = consumerKey;
                lblConsumerSecret.Text = consumerSecret;
            }
            catch (Exception exc)
            {
                m_logger.Error(exc);
            }
        
        }

        /// <summary>
        /// Delete Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Click(object sender, EventArgs e)
        {

            int communityId = GetCommunityFacade().GetCommunityIdFromGameId(SELECTED_GAME_ID);

            if (GetCommunityFacade().IsCommunityOwner(communityId, GetUserId()))
            {
                // Don't chagne this to Facade until everything is implemented there!!!
                // Facade did not implement deleteing members, 
                //      removing assets and adjusting counts, 
                //      set last update so it would be removed from search, 
                //      call correct SP so wok items get taken care of.
                CommunityUtility.DeleteCommunity(communityId, GetUserId());
            }

            Response.Redirect(ResolveUrl("~/Default.aspx?ws=" + Session["SelectedSiteId"].ToString() + "&mn=" + GetGameSearchParentMenuID() + "&sn=" + GetGameSearchControlID()));
        }
        

        /// <summary>
        /// Update Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            bool gameImageChanged = true;
            bool gameSaveSuccessful = false;

            try
            {
                // Check to make sure user did not enter any "potty mouth" words
                if (SiteManagementCommonFunctions.isTextRestricted (txtGameName.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                    SiteManagementCommonFunctions.isTextRestricted (txtDescription.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    m_logger.Warn ("User " + GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
                    ShowMessage (Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE);
                    return;
                }
                else if (SiteManagementCommonFunctions.ContainsInjectScripts (txtGameName.Text) ||
                    SiteManagementCommonFunctions.ContainsInjectScripts (txtDescription.Text))
                {
                    m_logger.Warn ("User " + GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
                    ShowMessage ("Your input contains invalid scripting, please remove script code and try again.");
                    return;
                }

                // Server validation
                Page.Validate();
                if (!Page.IsValid)
                {
                    string errMsg = "";
                    for (int i = 0; i < Page.Validators.Count; i++)
                    {
                        if (!Page.Validators[i].IsValid)
                        {
                            errMsg += Page.Validators[i].ErrorMessage + "  ";   
                        }
                    }
                    ShowMessage (errMsg);
                    return;
                }

                string gameName;
                gameName = txtGameName.Text;

                // Veryify name does not already exist
                int communityId = GetCommunityFacade ().GetCommunityIdFromGameId (SELECTED_GAME_ID);
                if (GetCommunityFacade ().IsCommunityNameTaken (false, gameName.Replace (" ", ""), communityId))
                {
                    ShowMessage ("Name " + gameName + " already exists, please chose a different name.");
                    return;
                }

                // Save the game
                {
                    GameToEdit.GameId = SELECTED_GAME_ID;
                    GameToEdit.GameAccessId = Convert.ToInt32(drpPermission.SelectedValue);
                    GameToEdit.GameDescription = txtDescription.Text;
                    if ((browseTHUMB != null) && (browseTHUMB.Value != "") && (GameToEdit.GameImagePath != browseTHUMB.Value))
                    {
                        GameToEdit.GameImagePath = browseTHUMB.Value;
                    }
                    else
                    {
                        gameImageChanged = true;
                    }
                    GameToEdit.GameKeyWords = "";
                    GameToEdit.GameModifiersId = GetUserId();
                    GameToEdit.GameName = gameName;
                    GameToEdit.GameRatingId = Convert.ToInt32(drpMaturityLevel.SelectedValue);
                    GameToEdit.GameStatusId = Convert.ToInt32(drpGameStatus.SelectedValue);
                    GameToEdit.GameSynopsis = txtTeaser.Text;
                    GameToEdit.LastUpdated = DateTime.Now;

                    int rowsAffected = GetGameFacade().SaveGame(GameToEdit);
                    gameSaveSuccessful = rowsAffected >= 0;

                    if (rowsAffected < 1)
                    {
                        throw new Exception("Unable to update game");
                    }
                }

                try
                {
                    //save thumbnail
                    if (gameImageChanged && (browseTHUMB.Value != ""))
                    {
                        UploadGameImage(SELECTED_GAME_ID, browseTHUMB);
                    }
                }
                catch (Exception)
                {
                    //do Not stop if Image fails
                }

                //save Categories
                //first clear them all out
                GetCommunityFacade().DeleteCommunityCategories(communityId);
                //then add them all back in
                foreach (DataListItem dliCategory in dlCategories.Items)
                {
                    CheckBox chkCategory = (CheckBox)dliCategory.FindControl("chkCategory");

                    if (chkCategory.Checked)
                    {
                        HtmlInputHidden hidCatId = (HtmlInputHidden)dliCategory.FindControl("hidCatId");
                        GetCommunityFacade().InsertCommunityCategory(communityId, Convert.ToInt32(hidCatId.Value));
                    }
                }

                //save licenseType
                if (GameLicense == null)
                {
                    GameLicense license = new GameLicense();

                    //temporarily hard coded to be professional for closed beta
                    //license.LicenseSubscriptionId = Convert.ToInt32(ddlLicenseTypes.SelectedValue);
                    license.LicenseSubscriptionId = 3;
                    license.GameId = SELECTED_GAME_ID;
                    license.GameKey = this.ownerId + "O" + SELECTED_GAME_ID + "G" + SiteManagementCommonFunctions.GenerateUniqueString(15);
                    license.ModifierId = GetUserId();
                    license.LicenseStatusId = SiteManagementCommonFunctions.DEFAULT_LICENSE_STATUS_ID;

                    int licenseId = GetGameFacade().AddNewGameLicense(license, 1000);

                    if (licenseId < 1)
                    {
                        throw new Exception("Unable to add license");
                    }
                }
                else
                {
                    //temporarily hard coded to be professional for closed beta
                    //license.LicenseSubscriptionId = Convert.ToInt32(ddlLicenseTypes.SelectedValue);
                    GameLicense.LicenseSubscriptionId = 3;
                    GameLicense.ModifierId = GetUserId();

                    int rowsAffected = GetGameFacade().UpdateGameLicense(GameLicense);

                    // This rowsAffected check is unnecessary, and will fail if the user is updating a second time.
                    // This kind of check only works on records that have a 'lastUpdatedOn' sort of field.
                    //if (rowsAffected < 1)
                    //{
                    //    throw new Exception("Unable to update license");
                    //}
                }

                // Award creator fame
                FameFacade famefaceade = new FameFacade();
                famefaceade.RedeemPacket(GetCurrentUser().UserId, 5007, (int)FameTypes.Creator);


                //indicated a successful save
                ShowMessage("Your changes were saved successfully.", false);
            }
            catch (Exception exc)
            {
                m_logger.Error("Error saving game ", exc);
                ShowMessage("Error saving game " + exc.Message);
            }

						if (GameToEdit.GameAccessId == 5 && gameSaveSuccessful)
						{
							Response.Redirect("~/mykaneva/my3dapps.aspx");
						}


            litJS.Text = ""; // "/* " + litJS.Text + " */";
            BindGameData ();
        }

        /// <summary>
        /// Cancel Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(ResolveUrl("~/Default.aspx?ws=" + Session["SelectedSiteId"].ToString() + "&mn=" + GetGameSearchParentMenuID() + "&sn=" + GetGameSearchControlID()));
        }

        protected void dlCategories_OnDataBind (object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem)
            {
                // Retrieve the Label control in the current DataListItem.
                CheckBox checkBox = (CheckBox) e.Item.FindControl ("chkCategory");

                if (checkBox != null && checkBox.Checked)
                {
                    litJS.Text += "categorySelected($('" + checkBox.ClientID + "'));";
                }
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion

    }
}
