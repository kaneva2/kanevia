using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Drawing;
using System.Collections.Generic;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class GamePremiumItems : BaseUserControl
    {
        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private bool isAdministrator = false;
        private bool canEdit = false;
        private int ownerId = 0;
        private int pageNum = 1;
        private int pageSize = 10;
        private int globalId = 0;
        private int selectedGameId = 0;

        #endregion Declerations


        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                divError.Style.Add ("display", "none");
                divErrorTop.Style.Add ("display", "none");

                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.STAR_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        isAdministrator = true;
                        canEdit = true;
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    default:
                        break;
                }

                if (((Session["SelectedGameId"] != null) && Convert.ToInt32 (Session["SelectedGameId"]) > 0) ||
                    ((Session["AppManagementSelectedCommunityId"] != null) && Convert.ToInt32 (Session["AppManagementSelectedCommunityId"]) > 0))
                {
                    // Load the current user's data the first time
                    SelectedCommunityId = Convert.ToInt32 (Session["AppManagementSelectedCommunityId"]);

                    SelectedGameId = Session["SelectedGameId"] != null ? Convert.ToInt32 (Session["SelectedGameId"]) : SelectedGameId;

                    //get the company id the game is associated with
                    this.ownerId = GetGameFacade ().GetGameOwner (SelectedGameId);

                    if (!isAdministrator)
                    {
                        canEdit = this.ownerId.Equals(GetUserId());
                    }

                    if (!canEdit)
                    {
                        RedirectToHomePage();
                    }
                    else
                    {
                        //community id check is to allow kaneva website to make use of this usercontrol
                        if (!IsPostBack || !IsPostBackKaneva)
                        {
                            // Log the CSR activity
                            this.GetSiteManagementFacade ().InsertCSRLog (GetUserId (), "CSR - Viewing game premium item page", 0, SelectedGameId);

                            //configure page
                            ConfigurePageForUse();

                            // Have to manually track post back for Kaneva site.  Even on first page load
                            // the IsPostBack property is set to true. So we'll keep up with it ourselves best we can.
                            IsPostBackKaneva = true;
                        }
                    }
                }
                else
                {
                    //community id check is to tell it to redirect to search page (for KWAS only)
                    if (Request["communityId"] == null)
                    {
                        //store this pages URL for return
                        Session["CallingPage"] = Request.Url.ToString();
                        Response.Redirect(ResolveUrl("~/Default.aspx?ws=" + GetCurrentSiteID((int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.DEVELOPER) + "&mn=" + GetGameSearchParentMenuID() + "&sn=" + GetGameSearchControlID()));
                    }
                }

                IsCurrentHostKWAS = System.IO.Path.GetFileName (Request.Path).ToLower () == "default.aspx";
            }
            else
            {
                RedirectToHomePage();
            }
        }

        #endregion Page Load


        #region Functions

        private void ConfigurePageForUse()
        {
            SetStatusDropDown(false);
            BindPremiumItems();
        }

        private void BindPremiumItems()
        {                                             
            //get the games servers for the game
            try
            {
                //get the premium items from the database
                PagedList<WOKItem> premiumItemsList = GetShoppingFacade ().GetPremiumItems (SelectedGameId, pageSize, PageNum, DEFAULT_ITEM_SORT, true, true);

                PREMIUM_ITEMS = (List<WOKItem>)premiumItemsList.List;
                rptPremiumItems.DataSource = premiumItemsList;
                rptPremiumItems.DataBind ();

                // The results
                lblSearch.Text = GetResultsText(premiumItemsList.TotalCount, PageNum, pageSize, premiumItemsList.Count);

                pgTop.NumberOfPages = Math.Ceiling ((double) premiumItemsList.TotalCount / pageSize).ToString ();
                pgTop.DrawControl ();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
                m_logger.Error("Error Loading premium items.", ex);
            }
        }

        #endregion Functions


        #region Helper Functions

        private void SetStatusDropDown(bool isNewItem)
        {
            ddlStatus.Items.Clear ();

            ddlStatus.Items.Add (new ListItem (WOKItem.ItemActiveStates.Public.ToString (), ((int) WOKItem.ItemActiveStates.Public).ToString ()));
            ddlStatus.Items.Add (new ListItem (WOKItem.ItemActiveStates.Private.ToString (), ((int) WOKItem.ItemActiveStates.Private).ToString ()));

            if (!isNewItem)
            {
                ddlStatus.Items.Add (new ListItem (WOKItem.ItemActiveStates.Deleted.ToString (), ((int) WOKItem.ItemActiveStates.Deleted).ToString ()));
            }
        }

        private void SetApprovalDropDown (WOKItem.ItemApprovalStatus approvalStatus, bool isNewItem)
        {
            ddlApproval.Items.Clear ();

            if (isNewItem)
            {
                ddlApproval.Items.Add(new ListItem(WOKItem.DEFAULT_PREMIUM_ITEM_STATUS.ToString(), ((int)WOKItem.DEFAULT_PREMIUM_ITEM_STATUS).ToString()));
                ddlApproval.Enabled = false;
            }
            else
            {
                if (IsCurrentHostKWAS)
                {
                    ddlApproval.Items.Add (new ListItem (WOKItem.ItemApprovalStatus.Pending.ToString (), ((int) WOKItem.ItemApprovalStatus.Pending).ToString ()));
                    ddlApproval.Items.Add (new ListItem (WOKItem.ItemApprovalStatus.Requested.ToString (), ((int) WOKItem.ItemApprovalStatus.Requested).ToString ()));
                    ddlApproval.Items.Add (new ListItem (WOKItem.ItemApprovalStatus.Approved.ToString (), ((int) WOKItem.ItemApprovalStatus.Approved).ToString ()));
                    ddlApproval.Items.Add (new ListItem (WOKItem.ItemApprovalStatus.Denied.ToString (), ((int) WOKItem.ItemApprovalStatus.Denied).ToString ()));
                    ddlApproval.SelectedValue = ((int) approvalStatus).ToString();
                    ddlApproval.Enabled = true;
                }
                else
                {
                    switch (approvalStatus)
                    {
                        case WOKItem.ItemApprovalStatus.Pending:
                            ddlApproval.Items.Add (new ListItem (WOKItem.ItemApprovalStatus.Pending.ToString (), ((int) WOKItem.ItemApprovalStatus.Pending).ToString ()));
                            ddlApproval.Items.Add (new ListItem (WOKItem.ItemApprovalStatus.Requested.ToString (), ((int) WOKItem.ItemApprovalStatus.Requested).ToString ()));
                            ddlApproval.Enabled = true;
                            break;
                        case WOKItem.ItemApprovalStatus.Requested:
                            ddlApproval.Items.Add (new ListItem (WOKItem.ItemApprovalStatus.Requested.ToString (), ((int) WOKItem.ItemApprovalStatus.Requested).ToString ()));
                            ddlApproval.Enabled = false;
                            break;
                        case WOKItem.ItemApprovalStatus.Approved:
                            ddlApproval.Items.Add (new ListItem (WOKItem.ItemApprovalStatus.Approved.ToString (), ((int) WOKItem.ItemApprovalStatus.Approved).ToString ()));
                            ddlApproval.Enabled = false;
                            break;
                        case WOKItem.ItemApprovalStatus.Denied:
                            ddlApproval.Items.Add (new ListItem (WOKItem.ItemApprovalStatus.Denied.ToString (), ((int) WOKItem.ItemApprovalStatus.Denied).ToString ()));
                            ddlApproval.Enabled = false;
                            break;
                    }
                }
            }
        }

        //clear the server data fields
        private void ClearItemDataFields()
        {
            txtItemName.Text = "";
            txtItemDesc.Text = "";
            txtPrice.Text = "0";
            txtGlobalId.Text = "";
            ddlStatus.SelectedValue = ((int) WOKItem.ItemActiveStates.Private).ToString ();
            imgThumbnail.Src = "";
            if (txtDenyReason != null)
            {
                txtDenyReason.Text = "";
            }
        }

        private void PremiumItemsReset()
        {
            ClearItemDataFields();
            
            divDetails.Style.Add ("display", "none");
            imgThumbnail.Style.Add ("display", "none");
            
            // Hide the deny reason box and set correct height
            divReason.Style.Add ("display", "none");
            divPriceContainer.Style.Add ("height", "60px");

            btnUpdatePremiumItems.Text = "Add";
            CurrentItem = null;

            SetStatusDropDown (true);
            SetApprovalDropDown(WOKItem.DEFAULT_PREMIUM_ITEM_STATUS, true);
        }

        private void PopulatePremiumItemEditFields(WOKItem premItem)
        {
            try
            {
                txtItemName.Text = premItem.Name;                     
                txtItemDesc.Text = premItem.Description;
                txtPrice.Text = premItem.DesignerPrice.ToString();
                txtGlobalId.Text = premItem.GlobalId.ToString();
                ddlStatus.SelectedValue = premItem.ItemActive.ToString ();
                imgThumbnail.Src = GetItemImageURL (premItem.ThumbnailLargePath, "la");
                imgThumbnail.Style.Add ("display", "block");
                CurrentItem = premItem;
                SetApprovalDropDown (premItem.ApprovalStatus, false);
                
                if (CurrentItem.ApprovalStatus == WOKItem.ItemApprovalStatus.Denied)
                {
                    divReason.Style.Add ("display", "block");
                    divPriceContainer.Style.Add ("height", "140px");

                    // Populate the reason text box
                    txtDenyReason.Text = GetShoppingFacade ().GetItemDeniedReason (CurrentItem.GlobalId);

                    if (IsCurrentHostKWAS)
                    {
                        txtDenyReason.Enabled = true;
                    }
                }
                else if (IsCurrentHostKWAS)
                {
                    divReason.Style.Add ("display", "block");
                    divPriceContainer.Style.Add ("height", "140px");
                }
                else
                {
                    divReason.Style.Add ("display", "none");
                    divPriceContainer.Style.Add ("height", "60px");
                }
            }
            catch (Exception)
            {
                ShowMessage ("Error populating premium item fields for editing.", true, false);
            }
        }

        private void ShowMessage (string msg)
        {
            ShowMessage (msg, true, true);
        }
        private void ShowMessage (string msg, bool isError, bool isTopMsg)
        {
            if (isTopMsg)
            {
                divErrorTop.InnerText = msg;
                divErrorTop.Attributes.Add ("class", (isError ? "error" : "confirm") + " errtop");
                divErrorTop.Style.Add ("display", "block");
            }
            else
            {
                divError.InnerText = msg;
                divError.Attributes.Add ("class", isError ? "error" : "confirm");
                divError.Style.Add ("display", "block");
            }
        }

        //delegate function used in LIst FindAll search
        private bool GetPremiumItem(WOKItem premItem)
        {
            return premItem.GlobalId == this.globalId;
        }
        
        public string GetItemApprovalStatusDesc (int status)
        {
            switch (status)
            {
                case (int) WOKItem.ItemApprovalStatus.Pending:
                    return "Pending";
                case (int) WOKItem.ItemApprovalStatus.Requested:
                    return "Requested";
                case (int) WOKItem.ItemApprovalStatus.Approved:
                    return "Approved";
                case (int) WOKItem.ItemApprovalStatus.Denied:
                    return "Denied";
                default:
                    return "";
            }
        }

        private WOKItem.ItemApprovalStatus GetItemApprovalStatus (int status)
        {
            switch (status)
            {
                case (int) WOKItem.ItemApprovalStatus.Pending:
                    return WOKItem.ItemApprovalStatus.Pending;
                case (int) WOKItem.ItemApprovalStatus.Requested:
                    return WOKItem.ItemApprovalStatus.Requested;
                case (int) WOKItem.ItemApprovalStatus.Approved:
                    return WOKItem.ItemApprovalStatus.Approved;
                case (int) WOKItem.ItemApprovalStatus.Denied:
                    return WOKItem.ItemApprovalStatus.Denied;
                default:
                    return WOKItem.ItemApprovalStatus.Pending;
            }
        }

        private void SendItemApprovalEmail (WOKItem item)
        {
            SendItemApprovalEmail (item, "");
        }
        private void SendItemApprovalEmail (WOKItem item, string denyReason)
        {
            if (item.ApprovalStatus == WOKItem.ItemApprovalStatus.Requested)
            {
                // Send request approval email
                Game game = GetGameFacade ().GetGameByGameId (item.GameId);
                string from = System.Configuration.ConfigurationManager.AppSettings["FromApprovalEmail"].ToString ();
                string to = System.Configuration.ConfigurationManager.AppSettings["ApprovalEmailRecipients"].ToString ();
                string subject = "Premium Item Approval Request : " + game.GameName + " : " + item.Name + " (" + item.GlobalId.ToString () + ")"; 
                string body = "<a href=\"http://" + SiteManagementCommonFunctions.GetKWASSiteName + "/Default.aspx?ws=2&mn=10&sn=50\">Click here to find game " + game.GameName + ".</a>";
                KlausEnt.KEP.Kaneva.MailUtility.SendEmail (from, to, subject, body, true, true, 2);
            }
            else if (item.ApprovalStatus == WOKItem.ItemApprovalStatus.Approved)
            {
                // Send item approved email
                Game game = GetGameFacade ().GetGameByGameId (item.GameId);
                string from = System.Configuration.ConfigurationManager.AppSettings["FromApprovalEmail"].ToString ();
                string to = System.Configuration.ConfigurationManager.AppSettings["ApprovalEmailRecipients"].ToString ();
                string subject = "Premium Item " + item.Name + " has been Approved";
                string body = "Your Premium Item " + item.Name + " belonging to 3DApp " + game.GameName + " has been approved and is ready for general use." + 
                    "<br/><br/>Once you change the item Status to Public the item will be available for users to purchase and use in your 3D App.";
                KlausEnt.KEP.Kaneva.MailUtility.SendEmail (from, to, subject, body, true, true, 2);
            }
            else if (item.ApprovalStatus == WOKItem.ItemApprovalStatus.Denied)
            {
                // Send item denied email
                Game game = GetGameFacade ().GetGameByGameId (item.GameId);
                string from = System.Configuration.ConfigurationManager.AppSettings["FromApprovalEmail"].ToString ();
                string to = System.Configuration.ConfigurationManager.AppSettings["ApprovalEmailRecipients"].ToString ();
                string subject = "Premium Item " + item.Name + " approval request has been Denied";
                string body = "Your Premium Item " + item.Name + " belonging to 3DApp " + game.GameName + " approval request has been Denied.<br/><br/>" +
                    "Denial Reason: " + denyReason;
                KlausEnt.KEP.Kaneva.MailUtility.SendEmail (from, to, subject, body, true, true, 2);
            }
        }

        #endregion Helper Functions


        #region Properties

        private int PageNum
        {
            set 
            { 
                this.pageNum = value; 
            }
            get 
            { 
                return this.pageNum; 
            }
        }

        private List<WOKItem> PREMIUM_ITEMS
        {
            get
            {

                if (ViewState["premiumItems"] == null)
                {
                    ViewState["premiumItems"] = new List<WOKItem>();
                }

                return (List<WOKItem>)ViewState["premiumItems"];
            }
            set
            {
                ViewState["premiumItems"] = value;
            }
        }

        /// <summary>
        /// SELECTED_COMMUNITY_ID
        /// </summary>
        /// <returns></returns>
        private int SelectedCommunityId
        {
            get
            {
                return (int) ViewState["community_id"];
            }
            set
            {
                ViewState["community_id"] = value;
            }
        }

        /// <summary>
        /// SELECTED_Game_ID
        /// </summary>
        /// <returns></returns>
        private int SelectedGameId
        {
            get
            {
                if (selectedGameId > 0)
                {
                    return selectedGameId;
                }
                else
                {
                    selectedGameId = GetCommunityFacade ().GetCommunityGameId (SelectedCommunityId);
                    return selectedGameId;
                }
            }
            set
            {
                selectedGameId = value;
            }

        }

        /// <summary>
        /// stores the patchURL data to reduce database round trips
        /// </summary>
        /// <returns>GamePatchURLs</returns>
        private WOKItem CurrentItem
        {
            get
            {
                if (ViewState["currentItem"] == null)
                {
                    ViewState["currentItem"] = new WOKItem ();
                }
                return (WOKItem) ViewState["currentItem"];
            }
            set
            {
                ViewState["currentItem"] = value;
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        public string DEFAULT_ITEM_SORT
        {
            get
            {
                return "name";
            }
        }

        private bool IsCurrentHostKWAS
        {
            get 
            {
                if (ViewState["iskwas"] == null)
                {
                    return false;
                }
                
                return (bool) ViewState["iskwas"];
            }
            set {ViewState["iskwas"] = value;}
        }

        private bool IsPostBackKaneva
        {
            get
            {
                if (ViewState["ispostback"] == null)
                {
                    return false;
                }

                return (bool) ViewState["ispostback"];
            }
            set { ViewState["ispostback"] = value; }
        }

        #endregion Properties


        #region Event Handlers

        public void ddlApproval_IndexChanged (object sender, EventArgs e)
        {
            txtDenyReason.Enabled = false;
            if (((DropDownList) sender).SelectedValue == (Convert.ToInt16 (WOKItem.ItemApprovalStatus.Denied)).ToString ())
            {
                txtDenyReason.Enabled = true;
            }
        }

        protected void lbEdit_Click (object sender, CommandEventArgs e)
        {
            if (e.CommandArgument != null)
            {
                this.globalId = Convert.ToInt32 (e.CommandArgument);

                ClearItemDataFields ();
                btnUpdatePremiumItems.Text = "Update";
                divEditTitle.InnerText = "Edit Premium Item";
                
                PopulatePremiumItemEditFields (GetShoppingFacade().GetItem(this.globalId));
                SetStatusDropDown (false);
                divDetails.Style.Add ("display", "block");
            }
        }

        protected void lbDelete_Click (object sender, EventArgs e)
        {
            try
            {
                CheckBox chkEdit;
                int deletionCount = 0;
                int globalId = 0;

                foreach (RepeaterItem item in rptPremiumItems.Items)
                {
                    try
                    {
                        chkEdit = (CheckBox) item.FindControl ("chkEdit");

                        if (chkEdit.Checked)
                        {
                            globalId = 0;
                            globalId = Convert.ToInt32 (chkEdit.Attributes["globalid"]);
                            if (globalId > 0)
                            {
                                GetShoppingFacade().DeletePremiumItem (globalId);
                                deletionCount++;
                            }
                        }
                    }
                    catch { }
                }

                //message to user
                if (deletionCount == 0)
                {
                    ShowMessage ("No items were selected.");
                }
                else
                {
                    ShowMessage (deletionCount + " item" + (deletionCount > 1 ? "s were " : " was ") + "deleted from 3D App.");

                    // Update the repeater data
                    BindPremiumItems ();
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error trying to delete premium item(s) : ", ex);
                ShowMessage ("Item was not deleted from 3D App.");
            }
        }

        /// <summary>
        /// lbn_NewServer_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbNew_Click(Object sender, EventArgs e)
        {
            PremiumItemsReset();
            divEditTitle.InnerText = "Add Premium Item";
            divDetails.Style.Add ("display", "block");
        }

        protected void lbRequestApproval_Click (Object sender, EventArgs e)
        {
            try
            {
                CheckBox chkEdit;
                int globalId = 0;
                int requestCount = 0;

                foreach (RepeaterItem item in rptPremiumItems.Items)
                {
                    try
                    {
                        chkEdit = (CheckBox) item.FindControl ("chkEdit");
                        if (chkEdit.Checked)
                        {
                            globalId = 0;
                            globalId = Convert.ToInt32 (chkEdit.Attributes["globalid"]);
                            if (globalId > 0)
                            {
                                WOKItem wokitem = GetShoppingFacade ().GetItem (globalId);
                                if (wokitem.GlobalId > 0 && wokitem.ApprovalStatus == WOKItem.ItemApprovalStatus.Pending)
                                {
                                    // Set the new approval Status
                                    wokitem.ApprovalStatus = WOKItem.ItemApprovalStatus.Requested;

                                    // Save the item
                                    GetShoppingFacade ().UpdatePremiumItemApprovalStatus (wokitem);
                                    
                                    // Send the approval request email
                                    SendItemApprovalEmail (wokitem);
                                    requestCount++;
                                }
                            }
                        }
                    }
                    catch { }
                }

                //message to user
                if (requestCount == 0)
                {
                    ShowMessage ("No items were selected.");
                }
                else
                {
                    ShowMessage ("Approval Request was sent successfully.", false, true);
                    BindPremiumItems ();
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error trying to send approval request for premium item(s) : ", ex);
                ShowMessage ("Approval Request was not sent.");
            }
        }

        /// <summary>
        /// btnServerCancel_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPremiumItemsCancel_Click(Object sender, EventArgs e)
        {
            PremiumItemsReset();
        }

        protected void Sort_Command(Object sender, CommandEventArgs e)
        {
            SortSwitch(e.CommandArgument.ToString()); //sets the sort expression
            BindPremiumItems();
        }

        /// <summary>
        /// btnUpdateServer_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdatePremiumItems_Click(Object sender, EventArgs e)
        {
            //set error message variable
            string error = "";

            //validate the required fields
            if (txtItemName.Text == "")
            {
                error += "Name is required. ";
            }
            //validate the required fields
            if (txtItemDesc.Text == "")
            {
                error += "Description is required. ";
            }
            //validate the price
            if (txtPrice.Text == "")
            {
                error += "Price is required. ";
            }
            try
            {
                int price = Convert.ToInt32 (txtPrice.Text);
                if (price <= 0)
                {
                    error += "Price must be a whole number greater than 0. ";
                }
            }
            catch
            {
                error += "Price must be a number greater than 0. ";
            }
            // Check for any inject scripts in large text fields
            if ((SiteManagementCommonFunctions.ContainsInjectScripts (txtItemName.Text)) || 
                (SiteManagementCommonFunctions.ContainsInjectScripts (txtItemDesc.Text)) || 
                (SiteManagementCommonFunctions.ContainsInjectScripts (txtPrice.Text)) ||
                (SiteManagementCommonFunctions.ContainsInjectScripts (txtDenyReason.Text)))
            {
                error += "Your input contains invalid scripting, please remove script code and try again. ";
                m_logger.Warn("User " + GetUserId() + " tried to script from IP " + Common.GetVisitorIPAddress());
            }
            if (error.Length > 0)
            {
                ShowMessage (error, true, false);
            }
            else
            {
               try
                {
                    if (btnUpdatePremiumItems.Text.ToUpper().Equals("ADD"))
                    {
                        try
                        {
                            if ((inpThumbnail != null) && (inpThumbnail.Value != ""))
                            {
                                int MaxImageUploadSize = KlausEnt.KEP.Kaneva.KanevaGlobals.MaxUploadedImageSize;
                                if (inpThumbnail.PostedFile.ContentLength > MaxImageUploadSize)
                                {
                                    ShowMessage ("This photo is too large, please select an photo smaller than " + KlausEnt.KEP.Kaneva.KanevaGlobals.FormatImageSize (MaxImageUploadSize) + ".", true, false);
                                    return;
                                }
                            }

                            WOKItem item = new WOKItem ();

                            //build the Premium Item - fill in object values
                            item.BaseGlobalId = 0;
                            item.ItemCreatorId = Convert.ToUInt32(GetUserId());
                            item.Name = Server.HtmlEncode(txtItemName.Text.Trim());
                            item.DisplayName = Server.HtmlEncode(txtItemName.Text.Trim());
                            item.Description = Server.HtmlEncode(txtItemDesc.Text.Trim());
                            item.Keywords = "";
                            item.ArmAnywhere = 1;
                            item.DestroyWhenUsed = 0;
                            item.Disarmable = 1;
                            item.InventoryType = 256;
                            item.ItemActive = Convert.ToInt32 (ddlStatus.SelectedValue);
                            item.MarketCost = 0;
                            item.RequiredSkill = 0;
                            item.RequiredSkillLevel = 0;              
                            item.SellingPrice = 0;
                            item.Stackable = 1;
                            item.UseType = WOKItem.USE_TYPE_PREMIUM;
                            item.DerivationLevel = WOKItem.DRL_KANEVAITEM;
                            item.ApprovalStatus = WOKItem.DEFAULT_PREMIUM_ITEM_STATUS;

                            try
                            {
                                item.DesignerPrice = Convert.ToUInt32(txtPrice.Text.Trim());
                            }
                            catch (Exception)
                            {
                                item.DesignerPrice = 0;
                            }

                            //create the template and get the base id for item
                            int baseGlobalId = GetShoppingFacade().CreateItemTemplate(item);
                            item.BaseGlobalId = Convert.ToUInt32(baseGlobalId);

                            //add the texture to the database
                            int globalId = GetShoppingFacade().AddCustomItem(item);

                            //add the item to the game
                            GetShoppingFacade ().AddItemToGame (globalId, SelectedGameId, (int) item.ApprovalStatus);

                            if ((inpThumbnail != null) && (inpThumbnail.Value != ""))
                            {
                                UploadWOKItemImage(GetUserId(), globalId, inpThumbnail);
                            }
                        }
                        catch (Exception ex)
                        {
                            m_logger.Error("Error on premium item editing - ADD", ex);
                            throw ex;
                        }
                    }
                    else  // edit item
                    {
                        try
                        {
                            CurrentItem.Name = Server.HtmlEncode(txtItemName.Text.Trim());
                            CurrentItem.DisplayName = Server.HtmlEncode(txtItemName.Text.Trim());
                            CurrentItem.Description = Server.HtmlEncode(txtItemDesc.Text.Trim());
                            CurrentItem.ItemActive = Convert.ToInt32 (ddlStatus.SelectedValue);
                            CurrentItem.DesignerPrice = Convert.ToUInt32(txtPrice.Text.Trim());

                            // If approval status has changed, then save item with status change 
                            if (CurrentItem.ApprovalStatus != GetItemApprovalStatus (Convert.ToInt32 (ddlApproval.SelectedValue)) || IsCurrentHostKWAS)
                            {
                                CurrentItem.ApprovalStatus = GetItemApprovalStatus (Convert.ToInt32 (ddlApproval.SelectedValue));

                                if (CurrentItem.ApprovalStatus == WOKItem.ItemApprovalStatus.Approved ||
                                    CurrentItem.ApprovalStatus == WOKItem.ItemApprovalStatus.Denied)
                                {
                                    // If status was changed to Approved or Denied, make sure the request
                                    // is coming from KWAS and not the website.  Should only be able to set
                                    // these two status from KWAS.
                                    if (IsCurrentHostKWAS)
                                    {
                                        GetShoppingFacade ().UpdatePremiumItem (CurrentItem);

                                        // If item is denied, save denied reason to db
                                        if (CurrentItem.ApprovalStatus == WOKItem.ItemApprovalStatus.Denied &&
                                            txtDenyReason.Text.Length > 0)
                                        {
                                            GetShoppingFacade ().UpdateItemDeniedReason (CurrentItem.GlobalId, txtDenyReason.Text);
                                        }

                                        // Send the approval request email
                                        SendItemApprovalEmail (CurrentItem, txtDenyReason.Text);
                                    }
                                }
                                else
                                {
                                    GetShoppingFacade ().UpdatePremiumItem (CurrentItem);

                                    // Send the approval request email
                                    SendItemApprovalEmail (CurrentItem);
                                }
                            }
                            else // approval status has not changed, so just save item, not approval status
                            {
                                GetShoppingFacade ().UpdateCustomItem (CurrentItem);
                            }

                            if ((inpThumbnail != null) && (inpThumbnail.Value != "") && (CurrentItem.ThumbnailAssetdetailsPath != inpThumbnail.Value))
                            {
                                UploadWOKItemImage (GetUserId (), CurrentItem.GlobalId, inpThumbnail);
                            }
                        }
                        catch (Exception ex)
                        {
                            m_logger.Error("Error on game editing - Update", ex);
                            throw ex;
                        }
                    }

                    //indicated a successful save
                    ShowMessage ("Your changes were saved successfully.", false, true);

                    //clear the fields
                    PremiumItemsReset();

                    //bind the data
                    BindPremiumItems();

                    // Award creator fame
                    FameFacade famefaceade = new FameFacade();
                    famefaceade.RedeemPacket(GetCurrentUser().UserId, 5004, (int)FameTypes.Creator);
                }
                catch (Exception ex)
                {
                    ShowMessage (ex.Message, true, false);
                    m_logger.Error("Error in Premium Item Management ", ex);
                }
            }
        }

        /// <summary>
        /// Page Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pgTop_PageChange (object sender, PageChangeEventArgs e)
        {
            PageNum = e.PageNumber;
            BindPremiumItems ();
        }

        #endregion Event Handlers

        #region Web Form Designer generated code
        
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler (pgTop_PageChange);
        }

        #endregion Web Form Designer generated code

    }
}