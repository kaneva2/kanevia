<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ClientVersionHistory.ascx.cs"
	Inherits="SiteManagement.usercontrols.ClientVersionHistory" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
	
<asp:LinkButton ID="lbListRecord" runat="server" Text="List Records" OnClick="btnListRecords_Click"></asp:LinkButton> | 
<asp:LinkButton ID="lbNewRecord" runat="server" Text="New" OnClick="btnNewRecord_Click"></asp:LinkButton>
<br />
<br />
<div id="divErrorData" runat="server" class="error"></div>
<div id="dvRecordEdit" runat="server">
	<table>
		<tr>
			<td>
				<table>
                    <tr>
						<td>KEP Client Version<asp:RequiredFieldValidator ID="rfKEPClientVersion" ControlToValidate="txtKEPClientVersion" Text="*" ErrorMessage="Version is a required field." Display="Dynamic" runat="server"/></td>
						<td>
							<asp:TextBox ID="txtKEPClientVersion" runat="server" Columns="60"></asp:TextBox>
                        </td>
						<td>
						</td>
					</tr>
					<tr>
						<td>Release Date<asp:RequiredFieldValidator ID="rfReleaseDate" ControlToValidate="txtReleaseDate" Text="*" ErrorMessage="Release Date is a required field." Display="Dynamic" runat="server"/></td>
						<td>
							<asp:TextBox ID="txtReleaseDate" runat="server" Columns="60"></asp:TextBox>
                        </td>
						<td>
						</td>
					</tr>
                    <tr>
						<td>Deprecated Date</td>
						<td>
							<asp:TextBox ID="txtDeprecatedDate" runat="server" Columns="60"></asp:TextBox>
                        </td>
						<td>
						</td>
					</tr>
                    <tr>
						<td>Comment</td>
						<td>
							<asp:TextBox ID="txtComment" runat="server" Columns="60"></asp:TextBox>
                        </td>
						<td>
						</td>
					</tr>
					<tr>
						<td>
						</td>
						<td>
							<asp:Button ID="btnSaveRecord" Text="Save" OnClick="btnSaveRecord_Click" runat="server" />
							<asp:Button ID="btnCancelEdit" Text="Cancel" OnClick="btnCancelEdit_Click" runat="server" />
						</td>
						<td>
						</td>
					</tr>
					<tr>
						<td>
						</td>
						<td>
							<asp:Label ID="txtSaveStatus" runat="server"></asp:Label></td>
						<td>
						</td>
					</tr>
				</table>
			</td>
			<td>
				<!-- Help Text -->
			</td>
		</tr>
	</table>
</div>
<div id="dvRecords" runat="server">
	<asp:Label ID="txtGVStatus" runat="server"></asp:Label>
	<asp:GridView ID="gvRecords" runat="server" OnRowEditing="gvRecords_Edit" OnRowDeleting="gvRecords_Delete" AutoGenerateColumns="false">
		<RowStyle BackColor="White"></RowStyle>
		<HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000"
			HorizontalAlign="Left"></HeaderStyle>
		<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
		<FooterStyle BackColor="#FFffff"></FooterStyle>
		<Columns>
			<asp:TemplateField>
				<ItemTemplate>
					<asp:LinkButton ID="lbnEdit" runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                    <asp:LinkButton ID="lbnDelete" runat="server" CausesValidation="false" OnClientClick="if(!confirm('Are you sure you want to delete the selected record?')) return false;" CommandName="Delete">Delete</asp:LinkButton>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="kep_client_version" HeaderText="KEPClientVersion" />
			<asp:BoundField DataField="release_date" HeaderText="ReleaseDate" />
			<asp:BoundField DataField="deprecated_date" HeaderText="DeprecatedDate" />
            <asp:BoundField DataField="comment" HeaderText="Comment" />
		</Columns>
	</asp:GridView>
    <Kaneva:Pager runat="server" id="pgBottom" OnPageChanged="pg_PageChanged"/>
</div>
