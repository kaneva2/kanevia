using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using MagicAjax;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Security.Cryptography;
using System.Drawing;


namespace KlausEnt.KEP.SiteManagement
{
    public partial class NameChanges : BaseUserControl
    {

        #region Declerations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.USER_DATA_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                if (Session["SelectedUserId"] != null)
                {
                    // Load the current user's data the first time
                    SELECTED_USER_ID = Convert.ToInt32(Session["SelectedUserId"]);

                    if (!IsPostBack)
                    {
                        // Log the CSR activity
                        this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Viewing user login history", 0, SELECTED_USER_ID);
                    }

                    lblErrMessage.Text = "";

                    //populate the pull down
                    GetAvailableNameColors();

                    //populate user name data
                    BindData();
                }
                else
                {
                    Session["CallingPage"] = Request.Url.ToString();
                    Response.Redirect(ResolveUrl("~/Default.aspx?ws=" + GetCurrentSiteID((int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.KANEVA) + "&mn=" + GetUserSearchParentMenuID() + "&sn=" + GetUserSearchControlID()));
                }

                // Set up regular expression validators
                revUsername.ValidationExpression = Constants.VALIDATION_REGEX_USERNAME;
            }
            else
            {
                RedirectToHomePage();
            }
        }

        #region Functions

        private void BindData()
        {
            //get the user data
            User selectedUser = GetUserFacade().GetUser(SELECTED_USER_ID);
            SELECTED_USER_WOKPLAYER_ID = selectedUser.WokPlayerId;

            GetUserNameColor(SELECTED_USER_WOKPLAYER_ID);

            //display user name
            SELECTED_USER_NAME = selectedUser.Username;
            lblSelectedUser.Text = "Notes for " + SELECTED_USER_NAME;

        }

        #endregion

        #region Helper Functions

        private void TickleServer(RemoteEventSender.ObjectType objectType, RemoteEventSender.ChangeType changeType)
        {
            // Dev note, the below assumes this is a pass subscription change
            //tickle the wok servers
            try
            {
                (new RemoteEventSender()).BroadcastItemChangeEvent(objectType, changeType, SELECTED_USER_ID);
            }
            catch (Exception ex)
            {
                m_logger.Error("Error while trying to tickle server", ex);
            }

        }

        private void GetAvailableNameColors()
        {
            //get the available colors from the webcache and
            //populate the drop down
            availNameColors.DataSource = WebCache.GetAvailableNameColors();
            availNameColors.DataTextField = "ColorName";
            availNameColors.DataValueField = "colorType";
            availNameColors.DataBind();
        }

        private void GetUserNameColor(int wokPlayerId)
        {
            int colorId = 0;

            try
            {
                //pull current user name color from wok DB
                colorId = GetUserFacade().GetUsersNameColor(wokPlayerId);
            }
            catch (Exception ex)
            {
                m_logger.Error("Error retrieving name color value", ex);
            }

            //set the selected value
            availNameColors.SelectedValue = colorId.ToString();
        }

        #endregion

        #region Event Handlers

        protected void btnChange_Click(object sender, EventArgs e)
        {
            UserFacade userFacade = new UserFacade();

            try
            {
                if (Page.IsValid)
                {

                    string newUserName = txtUsername.Text.Trim();
                    string oldUserName = SELECTED_USER_NAME;

                    // Verify username is still available
                    if (UsersUtility.GetUserIdFromUsername(newUserName) > 0)
                    {
                        lblErrMessage.Text = "The Nickname already exists. Please change your Nickname.";
                        lblErrMessage.ForeColor = Color.DarkRed;
                        lblErrMessage.Visible = true;
                    }
                    else
                    {

                        //encode and then update the users name
                        string strPassword = Server.HtmlEncode(txtPassword.Text.Trim());
                        int ret = userFacade.UpdateUserName(SELECTED_USER_ID, oldUserName, newUserName);

                        //if name update was successful update the password hash
                        if (ret == 0)
                        {
                            // Update the password with new username
                            byte[] salt = new byte[9];
                            new RNGCryptoServiceProvider().GetBytes(salt);
                            string hashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(UsersUtility.MakeHash(strPassword + newUserName.ToLower()) + Convert.ToBase64String(salt), "MD5");

                            byte[] keyvalue = new byte[10];
                            new RNGCryptoServiceProvider().GetBytes(keyvalue);

                            // Update user record with new hashed password
                            userFacade.UpdatePassword(SELECTED_USER_ID, hashPassword, Convert.ToBase64String(salt));

                            // get the user object with the new name and password
                            User user = userFacade.GetUser(newUserName);

                            //
                            if (user != null)
                            {
                                lblErrMessage.Text = "Username successfully changed!";
                                lblErrMessage.ForeColor = Color.DarkGreen;
                                lblErrMessage.Visible = true;


                                try
                                {
                                    //tickle server
                                    //this.TickleServer(RemoteEventSender.ObjectType.PlayerInventory, RemoteEventSender.ChangeType.UpdateObject);

                                    // Send out link to username reset email
                                    MailUtility.SendUsernameResetEmail(user.Username, strPassword, user.Email);
                                }
                                catch (Exception ex)
                                {
                                    lblErrMessage.Text = "An error was encountered trying to send email to " + newUserName + ". Error : " + ex;
                                    lblErrMessage.ForeColor = Color.DarkRed;
                                    lblErrMessage.Visible = true;

                                    return;
                                }
                            }
                            else
                            {
                                lblErrMessage.Text = "Could not retrieve user with username = " + newUserName + ".  The email was NOT sent to " + newUserName + ".";
                                lblErrMessage.ForeColor = Color.DarkRed;
                                lblErrMessage.Visible = true;

                                return;
                            }
                        }
                        else
                        {
                            throw new Exception("Failed to update user record with new name.");
                        }

                        // show success message***************
                    }
                }
            }
            catch (Exception ex)
            {
                lblErrMessage.Text = "An error was encountered while processing your request. Err: " + ex.Message;
                lblErrMessage.ForeColor = Color.DarkRed;
                lblErrMessage.Visible = true;

                m_logger.Error("Error saving name change: userId=" + SELECTED_USER_ID.ToString(), ex);
            }
        }

        protected void btnCheckUsername_Click(object sender, ImageClickEventArgs e)
        {
            revUsername.Validate();

            if (revUsername.IsValid)
            {
                if (txtUsername.Text.Trim() != string.Empty)
                {
                    if (UsersUtility.GetUserIdFromUsername(txtUsername.Text.Trim()) > 0)
                    {
                        spnCheckUsername.InnerText = "Not available";
                        spnCheckUsername.Attributes.Add("class", "failure");
                    }
                    else
                    {
                        spnCheckUsername.InnerText = "Available";
                        spnCheckUsername.Attributes.Add("class", "success");
                    }
                }
            }
            else
            {
                spnCheckUsername.InnerText = "Invalid name";
                spnCheckUsername.Attributes.Add("class", "failure");
            }
        }

        protected void changeColor_Click(object sender, EventArgs e)
        {
            try
            {
                int nameColorId = Convert.ToInt32(availNameColors.SelectedValue);

                int result = GetUserFacade().UpdateUsersNameColor(SELECTED_USER_WOKPLAYER_ID, nameColorId);

                if (result > 0)
                {
                    //tickle the server to update wok
                    //this.TickleServer(RemoteEventSender.ObjectType.PlayerInventory, RemoteEventSender.ChangeType.UpdateObject);

                    lblErrMessage.Text = "Username successfully changed!";
                    lblErrMessage.ForeColor = Color.DarkGreen;
                    lblErrMessage.Visible = true;

                    BindData();
                }
                else
                {
                    lblErrMessage.Text = "Failed to update user record with new name color.";
                    lblErrMessage.ForeColor = Color.DarkRed;
                    lblErrMessage.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblErrMessage.Text = "An error was encountered while processing your request. Err: " + ex.Message;
                lblErrMessage.ForeColor = Color.DarkRed;
                lblErrMessage.Visible = true;

                m_logger.Error("Error saving name color change: userId=" + SELECTED_USER_ID.ToString(), ex);
            }
        }

        #endregion

        #region Properties

        private int SELECTED_USER_ID
        {
            get
            {
                return (int)ViewState["users_id"];
            }
            set
            {
                ViewState["users_id"] = value;
            }

        }

        private int SELECTED_USER_WOKPLAYER_ID
        {
            get
            {
                return (int)ViewState["wok_player_id"];
            }
            set
            {
                ViewState["wok_player_id"] = value;
            }

        }

        private string SELECTED_USER_NAME
        {
            get
            {
                if (ViewState["users_name"] == null)
                {
                    ViewState["users_name"] = UsersUtility.GetUserNameFromId(SELECTED_USER_ID);
                }

                return ViewState["users_name"].ToString();
            }
            set
            {
                ViewState["users_name"] = value;
            }

        }


        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

    }
}
