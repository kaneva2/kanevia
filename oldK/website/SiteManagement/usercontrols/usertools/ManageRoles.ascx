<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ManageRoles.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.ManageRoles" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">

    <ajax:ajaxpanel id="ajMessage" runat="server">

    <asp:label style="position:relative; font-size:18px; padding: 10px 0 10px 0" runat="server" id="messages" visible="true" />

    <table id="tblManageUsers" border="0" cellpadding="0" cellspacing="0" width="980px" style="padding-top:20px">
        <tr>
            <td style="width:650px; vertical-align:top; padding-left:10px">
                <table border="0" cellpadding="4" cellspacing="0" width="640px">
                    <tr>
		                <td colspan="2"><h3><asp:label id="lbl_SelectRole" runat="server" visible="true" Text="Edit Roles Privileges" /></h3></td>
		            </tr>
		            <tr>
		                <td colspan="2">
		                    <asp:DropDownList ID="ddl_SiteRoles" runat="server" AutoPostBack="true" OnSelectedIndexChanged="SiteRole_SelectedIndexChanged" style="width: 200px" />
		                </td>
		            </tr>
                    <tr>
		                <td colspan="2"> <br />
                            <asp:Repeater ID="rpt_Privileges" runat="server" >
                                <HeaderTemplate>
                                        <table border="1" frame="border" cellpadding="4" cellspacing="0" width="100%">
                                            <tr style="background-color:#cccccc"> 
                                                <th align="left" style="width:125px;">Feature</th>
                                                <th align="left">Description</th>
                                                <th align="center" style="width:110px;">Access Level</th>
					                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                            <tr>
                                                <td>
								                    <asp:label id="lbl_Privilege" runat="server" visible="true" />
                                                     <input type="hidden" runat="server" id="hidPrivilegeId"  />
                                                </td>
                                                <td>
								                    <asp:label id="lbl_Description" runat="server" visible="true" />
                                                </td>
                                                 <td>
                                                     <asp:DropDownList ID="ddl_Rights" runat="server"  />
                                                </td>
                                           </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                            <tr style="background-color:#eeeeee">
                                                <td>
								                    <asp:label id="lbl_Privilege" runat="server" visible="true" />
                                                     <input type="hidden" runat="server" id="hidPrivilegeId"  />
                                                </td>
                                                <td>
								                    <asp:label id="lbl_Description" runat="server" visible="true" />
                                                </td>
                                                 <td>
                                                     <asp:DropDownList ID="ddl_Rights" runat="server"  />
                                                </td>
                                           </tr>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                   </tr>
                    <tr>
		                <td colspan="2"><br /><asp:button id="ibn_CommitChanges" runat="server" causesvalidation="False" OnClick="ibn_CommitChanges_Click" text="Submit" TabIndex="2" style="width:150px" /></td>
		            </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td style="width:650px; vertical-align:top; padding-left:10px">
                <table border="0" cellpadding="4" cellspacing="0" width="640px">
                    <tr>
		                <td ><h3><asp:label id="Label1" runat="server" visible="true" Text="Edit Security Roles" /></h3></td>
		            </tr>
		            <tr>
		                <td >
		                    <asp:LinkButton id="lbn_NewRole" runat="server" causesvalidation="false" onclick="lbn_NewRole_Click">Add New Role</asp:LinkButton>
		                </td>
		            </tr>
                    <tr>
		                <td colspan="2"> <br />
                            <asp:Repeater ID="rpt_Roles" runat="server" >
                                <HeaderTemplate>
                                        <table border="1" frame="border" cellpadding="4" cellspacing="0" width="100%">
                                            <tr style="background-color:#cccccc"> 
                                                <th align="center" style="width:110px;">Site Role Id</th>
                                                <th align="left" style="width:125px;">Role</th>
                                                <th align="left">Kaneva Role Id</th>
                                                <th align="left">-</th>
					                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                            <tr>
                                                <td>
								                    <asp:label id="lbl_SiteRoleId" runat="server" visible="true" />
                                                </td>
                                                <td>
								                    <asp:label id="lbl_RoleName" runat="server" visible="true" />
                                                </td>
                                                 <td>
                                                     <asp:label id="lbl_KanevaRoleId" runat="server" visible="true" />
                                                </td>
                                                 <td>
                                                     <asp:LinkButton id="lbn_edit" runat="server" causesvalidation="false" onclick="lbn_EditRole_Click">edit</asp:LinkButton>
                                                     <asp:LinkButton id="lbn_delete" runat="server" causesvalidation="false" onclick="lbn_DeleteRole_Click">delete</asp:LinkButton>
                                                </td>
                                           </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                            <tr style="background-color:#eeeeee">
                                                <td>
								                    <asp:label id="lbl_SiteRoleId" runat="server" visible="true" />
                                                </td>
                                                <td>
								                    <asp:label id="lbl_RoleName" runat="server" visible="true" />
                                                </td>
                                                 <td>
                                                     <asp:label id="lbl_KanevaRoleId" runat="server" visible="true" />
                                                </td>
                                                  <td>
                                                     <asp:LinkButton id="lbn_edit" runat="server" causesvalidation="false" onclick="lbn_EditRole_Click">edit</asp:LinkButton>
                                                     <asp:LinkButton id="lbn_delete" runat="server" causesvalidation="false" onclick="lbn_DeleteRole_Click">delete</asp:LinkButton>
                                                </td>
                                          </tr>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                   </tr>
                    <tr id="tr_EditRole" runat="server" visible="false">
                        <td colspan="2"><br />
                            <table border="0" width="100%" rules="cols" cellpadding="0" cellspacing="4px">
                                <tr>
                                    <td align="right">Role Name:</td>
                                    <td>
                                        <input type="hidden" runat="server" id="hidSiteRoleId"  />
                                        <asp:TextBox ID="tbx_RoleName" runat="server" ></asp:TextBox><asp:RequiredFieldValidator ID="rfv_RoleName" ControlToValidate="tbx_RoleName" Text="Required" ForeColor="#ce3b3b" Font-Size="13px" Display="Dynamic" runat="server"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Role ID:</td>
                                    <td><asp:TextBox ID="tbx_KanevaRoleId" runat="server" ></asp:TextBox><asp:RequiredFieldValidator ID="rfv_RoleId" ControlToValidate="tbx_KanevaRoleId" Text="Required" ForeColor="#ce3b3b" Font-Size="13px" Display="Dynamic" runat="server"></asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left"><br />
                                        <asp:Button ID="ibn_SaveRoleChanges" runat="server" OnClick="SaveRoleChanges_Click" text="Save" CausesValidation="true" />
                                        <asp:Button ID="btn_CancelRoleChange" runat="server" OnClick="btn_CancelRoleChange_Click" text="Cancel" CausesValidation="false" />
                                    </td>
                                </tr>
                             </table>
                        </td>
                    </tr>
                 </table>
            </td>
        </tr>
    </table>
    </ajax:ajaxpanel>

</asp:Panel>