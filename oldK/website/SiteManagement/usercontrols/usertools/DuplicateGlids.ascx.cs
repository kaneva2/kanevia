using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using KlausEnt.KEP.SiteManagement;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;

namespace SiteManagement.usercontrols
{
    public partial class DuplicateGlids : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
		{

		}

        #region Event Handlers
        
        protected void btnCopyGlids_Click(object sender, EventArgs e)
		{
            if (!string.IsNullOrWhiteSpace(txtGlidList.Text))
            {
                try
                {
                    IEnumerable<int> glidsToCopy = txtGlidList.Text.Split(new char[] { ',' }).Select(g => Int32.Parse(g));
                    IEnumerable<KeyValuePair<int, int>> copiedGlids = GetShoppingFacade().CopyGlids(glidsToCopy);

                    StringBuilder sb = new StringBuilder();
                    foreach(KeyValuePair<int, int> kvp in copiedGlids)
                    {
                        sb.Append("<tr><td>");
                        sb.Append(kvp.Key);
                        sb.Append("</td><td>");
                        sb.Append(kvp.Value);
                        sb.Append("</td></tr>");
                    }

                    litResults.Text = sb.ToString();
                }
                catch(Exception ex)
                {
                    litResults.Text = ex.Message;
                }
            }
		}

        #endregion
    }  
}