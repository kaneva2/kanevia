<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="UserReplies.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.UserReplies" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<asp:Panel ID="pnl_Security" runat="server" Enabled="true">
    <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="980">
        <tr>
            <td style="padding-left:40px; text-align:center; font-size:22px">
                USER REPLIES
            </td>
        </tr>
        <tr>
            <td style="padding-left:40px">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
	                <tr style="line-height:15px;">
	                    <td align="left" style="width:300px" class="belowFilter2" valign="middle" >
	                        <asp:Label runat="server" id="lblSelectedUser" />
	                    </td>
		                <td align="right" class="belowFilter2" valign="middle">
			                <asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop"/><br>
		                </td>						
	                </tr>
                </table>
             </td>
         </tr>
         <tr>
            <td style="padding-left:40px">
                <asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" OnSortCommand="UserReply_Sorting" Width="100%" id="dgrdPosts" cellpadding="2" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 1px solid #cccccc; font-family:arial">  
	            <HeaderStyle BackColor="#cccccc" ForeColor="#000000" Font-Underline="false" Font-Bold="true" HorizontalAlign="Left" />
	            <ItemStyle BackColor="#ffffff" HorizontalAlign="Left" />
	            <AlternatingItemStyle BackColor="#eeeeee" HorizontalAlign="Left" />
	                <Columns>				
		                <asp:TemplateColumn ItemStyle-CssClass="bodyBold" HeaderText="subject" SortExpression="subject" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top" ItemStyle-Width="20%" ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <A id="A1" runat="server" target="_top" href='<%#GetThreadURL (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "topic_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thread_id")))%>'><%#DataBinder.Eval(Container.DataItem, "subject")%></a>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn HeaderText="Replies" SortExpression="body_text" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top" ItemStyle-Width="750px" ItemStyle-Wrap="true">
			                <ItemTemplate>
				                <%#DataBinder.Eval(Container.DataItem, "body_text")%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn ItemStyle-CssClass="adminLinks" HeaderText="Date" SortExpression="created_date" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Width="10%" ItemStyle-Wrap="false">
			                <ItemTemplate>
				                <%# FormatDateTime (DataBinder.Eval(Container.DataItem, "created_date"))%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
	                </Columns>
                </asp:datagrid>            
            </td>
         </tr>
    </table>

</asp:Panel>            