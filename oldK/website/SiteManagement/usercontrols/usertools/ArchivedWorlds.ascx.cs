using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using KlausEnt.KEP.SiteManagement;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using Kaneva.BusinessLayer.Facade;

namespace SiteManagement.usercontrols
{
    public partial class ArchivedWorlds : BaseUserControl
    {
        #region Declarations

        private int _page = 1;
        private int _itemsPerPage = 30;

        private string Mode
        {
            get
            {
                return (ViewState["mode"] ?? string.Empty).ToString();
            }
            set
            {
                ViewState["mode"] = value;
            }

        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
                Mode = "Archived";
                ResetPage(1, true);
			}
		}

        #region Data Manipulation
        /// <summary>
        /// Called to bind data to page elements
        /// </summary>
        protected void BindData()
		{
            if (Mode == "Active")
            {
                string searchString = (txtWorldName.Text ?? string.Empty);
                searchString += (string.IsNullOrWhiteSpace(txtCreatorName.Text) ? "" : " " + txtCreatorName.Text);
                searchString = searchString.Trim();

                // Bind default game items
                PagedList<Community> activeWorlds;
                if (!string.IsNullOrWhiteSpace(searchString))
                {
                    activeWorlds = GetCommunityFacade().SearchCommunities(false, true, searchString, false, string.Empty, 0,
                        new int[] { (int)CommunityType.COMMUNITY, (int)CommunityType.HOME }, "created_date ASC", _page, _itemsPerPage, 0);
                }
                else
                {
                    activeWorlds = new PagedList<Community>();
                }
                gvActiveWorlds.DataSource = activeWorlds;
                gvActiveWorlds.DataBind();

                pgActive.CurrentPageNumber = _page;
                pgActive.NumberOfPages = Math.Ceiling((double)activeWorlds.TotalCount / _itemsPerPage).ToString();
                pgActive.DrawControl();
            }
            else
            {
                // Bind default game items
                PagedList<ArchivedWorld> archivedWorlds = GetGameFacade().GetArchivedWorlds(txtWorldName.Text, txtCreatorName.Text, "c.name ASC", _page, _itemsPerPage);
                gvArchivedWorlds.DataSource = archivedWorlds;
                gvArchivedWorlds.DataBind();

                pgArchived.CurrentPageNumber = _page;
                pgArchived.NumberOfPages = Math.Ceiling((double)archivedWorlds.TotalCount / _itemsPerPage).ToString();
                pgArchived.DrawControl();
            }
		}

        /// <summary>
        /// Called to empty input fields to defaults.
        /// </summary>
        protected void ResetFields(bool resetInput)
        {
            spnErrorData.InnerHtml = "";
            spnSuccessData.InnerHtml = "";
            if (resetInput)
            {
                txtCreatorName.Text = "";
                txtWorldName.Text = "";
            }
        }

        /// <summary>
        /// Called to reset the page
        /// </summary>
        protected void ResetPage(int page = 1, bool resetInput = false)
        {
            _page = page;
            if (Mode == "Active")
            {
                dvActiveWorlds.Visible = true;
                dvArchivedWorlds.Visible = false;
                lbToggleMode.Text = "View Archived Worlds";
            }
            else
            {
                dvActiveWorlds.Visible = false;
                dvArchivedWorlds.Visible = true;
                lbToggleMode.Text = "View Active Worlds";
            }
            ResetFields(resetInput);
            BindData();        
        }

        #endregion

        #region Event Handlers

		protected void gvArchivedWorlds_Delete(object sender, GridViewDeleteEventArgs e)
		{            
            GridViewRow gvr = gvArchivedWorlds.Rows[e.RowIndex];
            int storedProcResult = GetGameFacade().UnarchiveInactiveWorld(Convert.ToInt32(gvr.Cells[1].Text));

            if (storedProcResult == 0)
            {
                BindData();
                spnSuccessData.InnerHtml = "World restored successfully.";
            }
            else
            { 
                spnErrorData.InnerHtml = "Error restoring archived world. Please try again.";
                e.Cancel = true;
            }
		}

        protected void pg_PageChanged(object sender, PageChangeEventArgs e)
        {
            ResetPage(e.PageNumber);
        }

        #endregion

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ResetPage();
        }

        protected void gvActiveWorlds_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow gvr = gvActiveWorlds.Rows[e.RowIndex];
            int storedProcResult = GetGameFacade().ArchiveInactiveWorld(Convert.ToInt32(gvr.Cells[1].Text), "KWAS");

            if (storedProcResult == 0)
            {
                BindData();
                spnSuccessData.InnerHtml = "World archived successfully.";
            }
            else
            {
                spnErrorData.InnerHtml = "Error archiving world. Please try again.";
                e.Cancel = true;
            }
        }

        protected void pgActive_PageChanged(object sender, PageChangeEventArgs e)
        {
            ResetPage(e.PageNumber);
        }

        protected void lbToggleMode_Click(object sender, EventArgs e)
        {
            if (Mode == "Active")
                Mode = "Archived";
            else
                Mode = "Active";

            ResetPage(1, true);
        }
    }  
}