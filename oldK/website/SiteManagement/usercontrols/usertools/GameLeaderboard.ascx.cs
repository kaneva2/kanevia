using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;
//using Kaneva.BusinessLayer.Facade;
//using System.Drawing;
//using System.Collections.Generic;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class GameLeaderboard : BaseUserControl
    {
        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
        private bool isAdministrator = false;
        private bool canEdit = false;
        private int ownerId = 0;

        private const int maxTextLen = 45;

        #endregion Declerations

        #region Page Load

        protected void Page_Load (object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                divError.Style.Add ("display", "none");

                //check security level
                switch (CheckUserAccess ((int) SitePrivilege.ePRIVILEGE.STAR_ADMIN))
                {
                    case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        isAdministrator = true;
                        canEdit = true;
                        break;
                    case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    default:
                        break;
                }

                if ((Session["SelectedGameId"] != null) && Convert.ToInt32 (Session["SelectedGameId"]) > 0)
                {
                    // Load the current user's data the first time
                    this.SelectedGameId = Convert.ToInt32 (Session["SelectedGameId"]);

                    //get the company id the game is associated with
                    this.ownerId = GetGameFacade ().GetGameOwner (this.SelectedGameId);

                    if (!isAdministrator)
                    {
                        canEdit = this.ownerId.Equals (GetUserId ());
                    }

                    if (!canEdit)
                    {
                        RedirectToHomePage ();
                    }
                    else
                    {
                        //community id check is to allow kaneva website to make use of this usercontrol
                        if (!IsPostBack || !IsPostBackKaneva)
                        {
                            // Log the CSR activity
                            this.GetSiteManagementFacade ().InsertCSRLog (GetUserId (), "CSR - Viewing game premium item page", 0, this.SelectedGameId);

                            //configure page
                            ConfigurePageForUse ();

                            // Have to manually track post back for Kaneva site.  Even on first page load
                            // the IsPostBack property is set to true. So we'll keep up with it ourselves best we can.
                            IsPostBackKaneva = true;
                        }
                    }
                }
                else
                {
                    //community id check is to tell it to redirect to search page (for KWAS only)
                    if (Request["communityId"] == null)
                    {
                        //store this pages URL for return
                        Session["CallingPage"] = Request.Url.ToString ();
                        Response.Redirect (ResolveUrl ("~/Default.aspx?ws=" + GetCurrentSiteID ((int) SiteManagementCommonFunctions.SUPPORTED_WEBSITES.DEVELOPER) + "&mn=" + GetGameSearchParentMenuID () + "&sn=" + GetGameSearchControlID ()));
                    }
                }

//                IsCurrentHostKWAS = System.IO.Path.GetFileName (Request.Path).ToLower () == "default.aspx";
            }
            else
            {
                RedirectToHomePage ();
            }
        }

        #endregion Page Load

        #region Functions

        private void ConfigurePageForUse ()
        {
            //get the games servers for the game
            try
            {
                // Set up radio button values for consistency
                rblOrder.Items.Add (new ListItem ("Ascending", Leaderboard.SORTBY_ASC));
                rblOrder.Items.Add (new ListItem ("Descending", Leaderboard.SORTBY_DESC));

                // Set max length
                txtName.MaxLength = maxTextLen;
                txtLabel.MaxLength = maxTextLen;

                //get the leaderboard from database
                Leaderboard lb = GetGameFacade ().GetLeaderBoard (this.SelectedCommunityId);

                // if leaderboard does not exist, then set page to add new leaderboard
                if (lb.CommunityId.Equals(0))
                {
                    divEditTitle.InnerText = "Create Leaderboard";
                    txtName.Text = "";
                    txtLabel.Text = "";
                    rblOrder.SelectedValue = Leaderboard.SORTBY_DESC;
                    rblType.SelectedValue = ((int)Leaderboard.FormatType.General).ToString();
                }
                else
                {
                    divEditTitle.InnerText = "Edit Leaderboard";

                    // Populate edit fields
                    txtName.Text = lb.Name;
                    txtLabel.Text = lb.ColumnName;
                    rblOrder.SelectedValue = lb.SortBy;
                    rblType.SelectedValue = lb.FormatForValue.ToString();
                }
            }
            catch (Exception ex)
            {
                ShowMessage (ex.Message, true);
                m_logger.Error ("Error Loading edit leaderboard.", ex);
            }
        }

        private void ShowMessage (string msg, bool isError)
        {
            divError.InnerText = msg;
            divError.Attributes.Add ("class", isError ? "error" : "confirm");
            divError.Style.Add ("display", "block");
        }

        private void ResetLeaderboard ()
        {
            try
            {

            }
            catch (Exception ex)
            {
                ShowMessage (ex.Message, true);
                m_logger.Error ("Error in resetting leaderboard ", ex);
            }
        }

        #endregion Functions

        #region Event Handlers

        protected void btnSave_Click (Object sender, EventArgs e)
        {
            //set error message variable
            string error = "";

            //validate the required fields
            if (txtName.Text == "")
            {
                error += "Name is required. ";
            }
            else if (txtName.Text.Trim ().Length > 45)
            {
                error += "Name can only be " + maxTextLen + " characters long.";
            }
            //validate the required fields
            if (txtLabel.Text == "")
            {
                error += "Label is required. ";
            }
            else if (txtLabel.Text.Trim ().Length > 45)
            {
                error += "Name can only be " + maxTextLen + " characters long.";
            }
            // Check for any inject scripts in large text fields
            if ((SiteManagementCommonFunctions.ContainsInjectScripts (txtName.Text)) ||
                (SiteManagementCommonFunctions.ContainsInjectScripts (txtLabel.Text)))
            {
                error += "Your input contains invalid scripting, please remove script code and try again. ";
                m_logger.Warn ("User " + GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
            }
            if (error.Length > 0)
            {
                ShowMessage (error, true);
            }
            else
            {
                try
                {
                    Leaderboard leaderboard = new Leaderboard ();

                    //build the Leaderboard - fill in object values
                    leaderboard.Name = txtName.Text.Trim ();
                    leaderboard.ColumnName = txtLabel.Text.Trim ();
                    leaderboard.FormatForValue = Convert.ToInt32(rblType.SelectedValue);
                    leaderboard.SortBy = rblOrder.SelectedValue;
                    leaderboard.CommunityId = this.SelectedCommunityId;

                    // Save the changes
                    GetGameFacade ().SaveLeaderBoard (leaderboard);

                    // Award creator fame
                    FameFacade famefaceade = new FameFacade();
                    famefaceade.RedeemPacket(GetCurrentUser().UserId, 5005, (int)FameTypes.Creator);

                    //indicated a successful save
                    ShowMessage ("Your changes were saved successfully.", false);
                }
                catch (Exception ex)
                {
                    ShowMessage (ex.Message, true);
                    m_logger.Error ("Error in Leaderboard Management ", ex);
                }
            }
        }

        #endregion Event Handlers

        #region Properties

        /// <summary>
        /// SELECTED_GAME_ID
        /// </summary>
        /// <returns></returns>
        private int SelectedGameId
        {
            get
            {
                return (int) ViewState["game_id"];
            }
            set
            {
                ViewState["game_id"] = value;
            }
        }

        private int SelectedCommunityId
        {
            get
            {
                if (ViewState["community_id"] == null || ViewState["community_id"].ToString() == "")
                {
                    if (SelectedGameId > 0)
                    {
                        ViewState["community_id"] = GetCommunityFacade ().GetCommunityIdFromGameId (SelectedGameId);
                    }
                    else
                    {
                        return 0;
                    }
                }
                return (int) ViewState["community_id"];
            }
            set
            {
                ViewState["community_id"] = value;
            }
        }

        private bool IsPostBackKaneva
        {
            get
            {
                if (ViewState["ispostback"] == null)
                {
                    return false;
                }

                return (bool) ViewState["ispostback"];
            }
            set { ViewState["ispostback"] = value; }
        }

        #endregion Properties

    }
}