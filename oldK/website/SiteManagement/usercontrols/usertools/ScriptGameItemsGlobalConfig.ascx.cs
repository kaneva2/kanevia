using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using KlausEnt.KEP.SiteManagement;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using System.Data;

namespace SiteManagement.usercontrols
{
    public partial class ScriptGameItemsGlobalConfig : BaseUserControl
    {
        private bool isSiteAdmin = false;

        protected void Page_Load(object sender, EventArgs e)
		{
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.SITE_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        isSiteAdmin = true;
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    default:
                        break;
                }

			    if(!IsPostBack)
			    {
                    ResetPage();
			    }
            }
            else
            {
                RedirectToHomePage();
            }
		}

        #region Data Manipulation
        /// <summary>
        /// Called to bind data to page elements
        /// </summary>
        protected void BindData()
		{
            Dictionary<string, string> config = GetScriptGameItemFacade().GetSGIGlobalConfigValues();
            rptConfigSettings.DataSource = config.OrderBy(d => d.Key);
            rptConfigSettings.DataBind();
		}

        /// <summary>
        /// Called to empty input fields to defaults.
        /// </summary>
        protected void ResetFields()
        {
            divErrorData.InnerText = "";
        }

        /// <summary>
        /// Called to reset the page
        /// </summary>
        protected void ResetPage()
        {
            BindData();
            ResetFields();
        }

        /// <summary>
        /// Called to validate the page before saving
        /// </summary>
        protected bool ValidatePage()
        {
            try
            {
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void ShowMessage(string msg, bool isError)
        {
            divErrorData.InnerText = msg;
            divErrorData.Attributes.Add("class", isError ? "error" : "confirm");
            divErrorData.Style.Add("display", "block");
        }

        protected bool AllowUserInsertsDeletes()
        {
            return isSiteAdmin;
        }

        #endregion

        #region Event Handlers

        protected void btnCancelEdit_Click(object sender, EventArgs e)
		{
            ResetPage();
		}

        protected string PrepareLogMessage(Dictionary<string, string> oldValues, Dictionary<string, string> newValues)
        {
            string changeTemplate = ", {0}: CHANGED {1} -> {2}"; 
            string deleteTemplate = ", {0}: DELETED";
            string insertTemplate = ", {0}: ADDED {1}"; 

            StringBuilder changes = new StringBuilder();

            // Compare all other old properties
            foreach (KeyValuePair<string, string> property in oldValues)
            {
                // Deletions
                if (!newValues.ContainsKey(property.Key))
                {
                    changes.Append(string.Format(deleteTemplate, property.Key));
                }
                else if(!newValues[property.Key].Equals(property.Value)) // Modifications
                {
                    changes.Append(string.Format(changeTemplate, property.Key, property.Value, newValues[property.Key]));
                }
            }

            // Check for insertions
            List<KeyValuePair<string, string>> newProperties = newValues
                .Where(p => !oldValues.ContainsKey(p.Key))
                .ToList();

            foreach (KeyValuePair<string, string> property in newProperties)
            {
                changes.Append(string.Format(insertTemplate, property.Key, property.Value));
            }

            string changeString = changes.ToString().TrimStart(new char[] {','});

            return string.Format("Admin - Updated game item global settings. Modifications: {0}", (string.IsNullOrWhiteSpace(changeString) ? "None" : changeString));
        }

		protected void btnSaveGameItemConfig_Click(object sender, EventArgs e)
		{
            if (Page.IsValid && ValidatePage())
            {
                bool changeSuccessful = false;
                string errorMsg = string.Empty;

                // Get old values for comparison
                Dictionary<string, string> oldConfig = GetScriptGameItemFacade().GetSGIGlobalConfigValues();
                Dictionary<string, string> newConfig = new Dictionary<string, string>();

                try
                {
                    // Get property updates
                    foreach (RepeaterItem item in rptConfigSettings.Items)
                    {
                        if (item.ItemType == ListItemType.Item ||
                            item.ItemType == ListItemType.AlternatingItem)
                        {
                            Label label = (Label)item.FindControl("lblSGIConfigProp");
                            TextBox txtBox = (TextBox)item.FindControl("txtSGIConfigProp");

                            newConfig.Add(label.Text, txtBox.Text);
                        }
                    }

                    // Get property inserts
                    if (isSiteAdmin)
                    {
                        TextBox txtNewPropName = (TextBox)rptConfigSettings.Controls[rptConfigSettings.Controls.Count - 1].Controls[0].FindControl("txtNewPropName");
                        TextBox txtNewPropValue = (TextBox)rptConfigSettings.Controls[rptConfigSettings.Controls.Count - 1].Controls[0].FindControl("txtNewPropValue");

                        if (!string.IsNullOrWhiteSpace(txtNewPropName.Text) && !string.IsNullOrWhiteSpace(txtNewPropValue.Text))
                        {
                            newConfig.Add(txtNewPropName.Text, txtNewPropValue.Text);
                        }
                    }

                    changeSuccessful = GetScriptGameItemFacade().UpdateSGIGlobalConfig(newConfig);
                }
                catch (Exception ex)
                {
                    errorMsg = ex.Message;
                }

                if (changeSuccessful)
                {
                    // Log the CSR activity
                    string updateLogMsg = PrepareLogMessage(oldConfig, newConfig);
                    this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), updateLogMsg, 0, 0);

                    ResetPage();

                    ShowMessage("Changes saved successfully.", false);
                }
                else
                {
                    ShowMessage(string.IsNullOrWhiteSpace(errorMsg) ? "Unable to update Script Game Item configs." : errorMsg, true);
                }
            }
            else
            {
                ShowMessage("Invalid input.", true);
            }
		}

        protected void btnDeleteGameItemConfig_Click(object sender, EventArgs e)
        {
            if (isSiteAdmin)
            {
                Control parentRow = ((Control)sender).Parent;
                Label lblPropName = (Label)parentRow.FindControl("lblSGIConfigProp");
                string propName = (string.IsNullOrWhiteSpace(lblPropName.Text) ? string.Empty : lblPropName.Text);

                bool changeSuccessful = GetScriptGameItemFacade().DeleteSGIGlobalConfig(propName);

                if (changeSuccessful)
                {
                    // Log the CSR activity
                    string updateLogMsg = string.Format("Admin - Updated game item global settings. Modifications: {0} DELETED", propName);
                    this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), updateLogMsg, 0, 0);

                    ResetPage();

                    ShowMessage("Changes saved successfully.", false);
                }
                else
                {
                    ShowMessage("Unable to delete Script Game Item config.", true);
                }
            }
            else
            {
                ShowMessage("Unauthorized user attempting delete.", true);
            }
        }
        #endregion
    }  
}