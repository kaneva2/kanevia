using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using KlausEnt.KEP.SiteManagement;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;

namespace SiteManagement.usercontrols
{
    public partial class DefaultScriptGameCustomData : BaseUserControl
    {
        #region Declarations

        private int _page = 1;
        private int _itemsPerPage = 30;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
                ResetPage();
			}
		}

        #region Data Manipulation
        /// <summary>
        /// Called to bind data to page elements
        /// </summary>
        protected void BindData()
		{
            // Bind default game items
            PagedList<SGCustomDataItem> gameItems = GetGameFacade().GetDefaultSGCustomData("attribute ASC", _page, _itemsPerPage);
            gvCustomDataAttributes.DataSource = gameItems;
            gvCustomDataAttributes.DataBind();

            pgBottom.CurrentPageNumber = _page;
            pgBottom.NumberOfPages = Math.Ceiling((double)gameItems.TotalCount / _itemsPerPage).ToString();
            pgBottom.DrawControl();
		}

        /// <summary>
        /// Called to fill a ScriptGameItem BO from input fields.
        /// </summary>
        /// <param name="GameItem">The ScriptGameItem Business Object to fill.</param>
        /// <returns>True if this ScriptGameItem already exists in the database.</returns>
        protected bool FillSGCustomDataItemFromInput(out SGCustomDataItem sGCustomDataItem, out SGCustomDataItem oldSGCustomDataItem)
        {
            bool dataItemExists = false;

            try
            {
                string attribute = txtAttributeName.Text.Trim();
                string value = GetDCFInstance.StripExcessJSONFormatting(txtValue.Text ?? string.Empty);

                sGCustomDataItem = new SGCustomDataItem(attribute, value);

                // Check to see if the item exists in the db already
                oldSGCustomDataItem = GetGameFacade().GetDefaultSGCustomDataItem(attribute);
                dataItemExists = !string.IsNullOrWhiteSpace(oldSGCustomDataItem.Attribute);
            }
            catch (Exception)
            {
                sGCustomDataItem = new SGCustomDataItem();
                oldSGCustomDataItem = null;
            }

            return dataItemExists;
        }

        /// <summary>
        /// Called to fill input fields from a ScriptGameItem BO.
        /// </summary>
        /// <param name="GameItem">The ScriptGameItem Business Object to use to fill.</param>
        protected void FillInputFromSGCustomDataItem(SGCustomDataItem sGCustomDataItem)
        {
            txtAttributeName.Text = sGCustomDataItem.Attribute;

            string value = (sGCustomDataItem.Value ?? string.Empty);

            if (value.StartsWith("{") && value.EndsWith("}"))
            {
                // Serialize the game item into JSON for better formatting
                JObject jsonObject = JsonConvert.DeserializeObject<JObject>(value);
                txtValue.Text = JsonConvert.SerializeObject(jsonObject, Formatting.Indented);
            }
            else
            {
                txtValue.Text = value;
            }
        }

        /// <summary>
        /// Called to empty input fields to defaults.
        /// </summary>
        protected void ResetFields()
        {
            txtValue.Text = "";
            divErrorData.InnerText = "";
            txtAttributeName.Text = "";
            cbApplyToTemplates.Checked = false;
        }

        /// <summary>
        /// Called to reset the page
        /// </summary>
        protected void ResetPage(int page = 1)
        {
            _page = page;
            BindData();

            dvCustomDataAttributeEdit.Visible = false;
            dvCustomDataAttributes.Visible = true;

            ResetFields();
        }

        /// <summary>
        /// Called to validate the page before saving
        /// </summary>
        protected bool ValidatePage(out string errMsg)
        {
            errMsg = string.Empty;

            try
            {
                string attribute = txtAttributeName.Text.Trim();
                string value = (txtValue.Text ?? string.Empty).Trim();

                if (value.StartsWith("{") && value.EndsWith("}"))
                {
                    JObject item = JsonConvert.DeserializeObject<JObject>(value);

                    if (item == null)
                    {
                        errMsg = "Invalid JSON";
                    }
                    else if (attribute.Equals("WorldSettings"))
                    {
                        JProperty frameworkEnabledProp = item.Properties().SingleOrDefault(s => s.Name.Equals("frameworkEnabled"));

                        if (frameworkEnabledProp == null || !frameworkEnabledProp.Value.ToString().ToLower().Equals("true"))
                        {
                            errMsg = "WorldSettings requires 'frameworkEnabled' property to be 'true'";
                        }
                    }
                }
            }
            catch (Exception)
            {
                errMsg = "Invalid JSON";
            }

            return string.IsNullOrWhiteSpace(errMsg);
        }

        #endregion

        #region Event Handlers
        protected void btnNewCustomDataAttribute_Click(object sender, EventArgs e)
		{
            dvCustomDataAttributeEdit.Visible = true;
            dvCustomDataAttributes.Visible = false;
			ResetFields();
		}

		protected void btnListCustomDataAttributes_Click(object sender, EventArgs e)
		{
            ResetPage();
		}

		protected void btnCancelEdit_Click(object sender, EventArgs e)
		{
            ResetPage();
		}

        protected string PrepareLogMessage(SGCustomDataItem oldGItem, SGCustomDataItem newGItem)
        {
            string changeTemplate = ", {0}: CHANGED {1} -> {2}"; 

            StringBuilder changes = new StringBuilder();

            // Compare default properties
            if (!oldGItem.Value.Equals(newGItem.Value))
                changes.Append(string.Format(changeTemplate, "value", oldGItem.Value, newGItem.Value));

            string changeString = changes.ToString().TrimStart(new char[] {','});

            return string.Format("Admin - Updated default script game custom data. Attribute: {0} Modifications: {1}", oldGItem.Attribute, (string.IsNullOrWhiteSpace(changeString) ? "None" : changeString));
        }

		protected void btnSaveCustomDataAttribute_Click(object sender, EventArgs e)
		{
            string errMsg = string.Empty;

            if (Page.IsValid && ValidatePage(out errMsg))
            {
                bool changeSuccessful = false;
                SGCustomDataItem sGDataItem = null;
                SGCustomDataItem oldSGDataItem = null;
                bool gameItemExists = FillSGCustomDataItemFromInput(out sGDataItem, out oldSGDataItem);

                // Perform the insert or update
                changeSuccessful = GetGameFacade().ModifyDefaultSGCustomDataItem(sGDataItem, cbApplyToTemplates.Checked);

                if (changeSuccessful)
                {
                    // Log the CSR activity
                    if (gameItemExists)
                    {
                        string updateLogMsg = PrepareLogMessage(oldSGDataItem, sGDataItem);
                        this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), updateLogMsg, 0, 0);
                    }
                    else
                    {
                        this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "Admin - Inserted new custom data item.", 0, 0);
                    }

                    ResetPage();
                }
                else
                {
                    if (gameItemExists)
                    {
                        divErrorData.InnerText = "Unable to update custom data item.";
                    }
                    else
                    {
                        divErrorData.InnerText = "Unable to insert custom data item.";
                    }
                }
            }
            else
            {
                divErrorData.InnerText = errMsg;
            }
		}

		protected void gvCustomDataAttributes_Edit(object sender, GridViewEditEventArgs e)
		{
            dvCustomDataAttributeEdit.Visible = true;
            dvCustomDataAttributes.Visible = false;
            
            GridViewRow gvr = gvCustomDataAttributes.Rows[e.NewEditIndex];
            SGCustomDataItem sGDataItem = GetGameFacade().GetDefaultSGCustomDataItem(gvr.Cells[1].Text);

            FillInputFromSGCustomDataItem(sGDataItem);
            e.Cancel = true;
		}

        protected void pg_PageChanged(object sender, PageChangeEventArgs e)
        {
            ResetPage(e.PageNumber);
        }

        #endregion
    }  
}