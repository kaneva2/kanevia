using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using System.Web.Security;
using System.Security.Cryptography;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class UserDetails : BaseUserControl
    {
        #region Declerations

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Declerations

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.USER_DATA_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        //do nothing
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                        ReadOnly(pnl_Security);
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                        RedirectToHomePage();
                        break;
                    default:
                        break;
                }

                if (Session["SelectedUserId"] != null)
                {
                    // Load the current user's data the first time
                    SELECTED_USER_ID = Convert.ToInt32(Session["SelectedUserId"]);

                    if (!IsPostBack)
                    {
                        LoadData(SELECTED_USER_ID);

                        // Notes
                        // Log the CSR activity
                        this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Viewing user notes", 0, SELECTED_USER_ID);
                        BindNotes (1, "");
                        
                        //lblSize.Text = "Photo width must be 740 pixels wide or less, please keep photo size under " + KanevaGlobals.FormatImageSize(KanevaGlobals.MaxUploadedScreenShotSize);
                    }
                    // Set up regular expression validators
                    revEmail.ValidationExpression = Constants.VALIDATION_REGEX_EMAIL;
                    revPassword.ValidationExpression = Constants.VALIDATION_REGEX_PASSWORD;
                }
                else
                {
                    Session["CallingPage"] = Request.Url.ToString();
                    Response.Redirect(ResolveUrl("~/Default.aspx?ws=" + GetCurrentSiteID((int)SiteManagementCommonFunctions.SUPPORTED_WEBSITES.KANEVA) + "&mn=" + GetUserSearchParentMenuID() + "&sn=" + GetUserSearchControlID()));
                }

            }
            else
            {
                RedirectToHomePage();
            }
        }

        #endregion

        #region Functions

        private void BindNotes(int pageNumber, string filter)
        {
            // Set the sortable columns
            string orderby = CurrentSort + " " + CurrentSortOrder;

            int notesPerPage = 10;

            PagedDataTable pds = UsersUtility.GetUserNotes(SELECTED_USER_ID, filter, orderby, pageNumber, notesPerPage);
            pgTop.NumberOfPages = Math.Ceiling((double)pds.TotalCount / notesPerPage).ToString();
            pgTop.DrawControl();

            dgrdNotes.DataSource = pds;
            dgrdNotes.DataBind();

            // The results
            lblNoteSearch.Text = GetResultsText(pds.TotalCount, pageNumber, notesPerPage, pds.Rows.Count);

            //display user name
            //lblSelectedUser.Text = "Notes for " + SELECTED_USER_NAME;
        }

        /// <summary>
        /// Pager change event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            BindNotes(e.PageNumber, "");
        }

        /// <summary>
        /// Update Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddNote_Click(object sender, EventArgs e)
        {
            // Server validation
            if (txtNote.Text.Trim ().Length >  0)
            {
                // Log the CSR activity
                this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Adding user note.", 0, SELECTED_USER_ID);
                UsersUtility.InsertUserNote(SELECTED_USER_ID, GetUserId(), txtNote.Text.Trim());
            }

            BindNotes(1, "");
        }

        protected void UserNote_Sorting(object sender, DataGridSortCommandEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            BindNotes(pgTop.CurrentPageNumber, "");
        }

        /// <summary>
        /// AddDropDownPermissions
        /// </summary>
        private void AddDropDownPermissions(DropDownList drpDropDown)
        {
            drpDropDown.Items.Insert(0, new ListItem("Only I", "M"));
            drpDropDown.Items.Insert(0, new ListItem("All friends", "A"));
            drpDropDown.Items.Insert(0, new ListItem("Kaneva members", "K"));
            drpDropDown.Items.Insert(0, new ListItem("Everyone", "Y"));
        }

        /// <summary>
        /// Load data to screen
        /// </summary>
        /// <param name="userId"></param>
        private void LoadData(int userId)
        {
            UserFacade userFacade = GetUserFacade();

            User user = userFacade.GetUser(userId);

            DataRow drUserProfile = UsersUtility.GetUserProfile(userId);

            // Log the CSR activity
            GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Viewing user details for userId = " + userId + ", username = " + user.Username, 0, userId);

            // Set up the permission dropdowns
            PagedDataTable dtFriendGroups = UsersUtility.GetFriendGroups(userId, "", "name", 1, Int32.MaxValue);

            if (UsersUtility.GetClanName(userId) != null)
            {
                lblClan.Text = UsersUtility.GetClanName(userId).ToString();
            }
            else
            {
                trClan.Visible = false;
            }

            if (user.MatureProfile)
            {
                chkProfileRestrict.Checked = true;
            }
            else
            {
                chkProfileRestrict.Checked = false;
            }


            // Is the email address validated?
            trError.Visible = (user.StatusId.Equals((int)Constants.eUSER_STATUS.REGNOTVALIDATED));
            lblEmail.Text = user.Email;

            // Set up rest of form
            lblUsername.Text = KanevaGlobals.TruncateWithEllipsis(user.Username, 19);
            lblUserId.Text = user.UserId.ToString();

            txtUserName.Text = Server.HtmlDecode(user.Username);
            txtDisplayName.Text = Server.HtmlDecode(user.DisplayName);
            txtFirstName.Text = Server.HtmlDecode(user.FirstName);
            txtLastName.Text = Server.HtmlDecode(user.LastName);

            txtEmail.Text = Server.HtmlDecode(user.Email);

            chkShowMature.Checked = user.ShowMature;

            chkShowEmail.Checked = false;
            txtConfirmEmail.Text = Server.HtmlDecode(user.Email);
            txtHomepage2.Text = Server.HtmlDecode(user.Homepage);

            SetDropDownIndex(drpMonth, (Convert.ToDateTime(user.BirthDate)).Month.ToString());
            SetDropDownIndex(drpDay, (Convert.ToDateTime(user.BirthDate)).Day.ToString());
            SetDropDownIndex(drpYear, (Convert.ToDateTime(user.BirthDate)).Year.ToString());

            if (userFacade.IsUserAdult(user))
            {
                chkShowMature.Attributes.Add("onClick", "javascript: if (" + chkShowMature.ClientID + ".checked){if (!confirm('I am at least 18 years old. I agree to enter this site of my own free will and for my personal entertainment only. I understand this site contains material that may be considered objectionable. I will not pass any information contained herein to Minors or to anyone who would find such photos offensive. By continuing past this point, I affirm, under penalty of perjury that I am not an agent or attorney for any government, governmental agency or branch, law enforcement agency or military agency or branch. I pledge that my interest in viewing the content of this site is for my personal entertainment only. I specifically pledge not to use my viewing experience against the publisher, provider, or designer of this site. Upon accessing this site, I hereby release, indemnify and hold harmless the providers, owners, models and creators of this site from any and all liability arising from my use of this site. By accessing this site, I am not violating any law of my locality, city, town, state, province, or nation. I subscribe to the principles of the First Amendment which holds that free adult Americans have the right to decide for themselves what they will read and view. By entering this website, you agree to the above terms and conditions.')){	" + chkShowMature.ClientID + ".checked = false;}}");
            }
            else
            {
                chkShowMature.Enabled = false;
                chkShowMature.Checked = false;
                //drpMonth.Enabled = false;
                //drpDay.Enabled = fals;e
                //drpYear.Enabled = false;
            }

            chkProfileRestrict.Checked = false;
            if (user.MatureProfile)
            {
                chkProfileRestrict.Checked = true;
            }


            txtPostalCode.Text = Server.HtmlDecode(user.ZipCode);

            // Set up country dropdown
            drpCountry.DataTextField = "name";
            drpCountry.DataValueField = "countryId";
            drpCountry.DataSource = WebCache.GetCountries();
            drpCountry.DataBind();

            drpCountry.Items.Insert(0, new ListItem("United States", Constants.COUNTRY_CODE_UNITED_STATES));
            drpCountry.Items.Insert(0, new ListItem("select...", ""));

            SetDropDownIndex(drpCountry, Server.HtmlDecode(user.Country).ToUpper());
            SetDropDownIndex(drpGender, Server.HtmlDecode(user.Gender));
            CountryCheck();

            imgAvatar.Src = GetProfileImageURL(user.ThumbnailSmallPath, "sm", user.Gender);

            txtDescription.Text = Server.HtmlDecode(user.Description);

            lblCash.Text = KanevaGlobals.FormatCurrency(UsersUtility.getUserBalance(userId, Constants.CURR_DOLLAR));
            lblKPoint.Text = KanevaGlobals.FormatKPoints(UsersUtility.getUserBalance(userId, Constants.CURR_KPOINT) + UsersUtility.getUserBalance(userId, Constants.CURR_MPOINT), true, false);
            //lblLicenseKey.Text = drUser ["registration_key"].ToString ();

            //// Get the logical accounts under this user if they are a master user
            //bool bAccountsVisible = false;

            //// Show add and delete buttons
            //// If master and it's a logical account
            //btnDeleteAccount.Visible = false;
            //btnAddAccount.Visible = false;

            //if (!IsPostBack)
            //{
            //    drpAccounts.Visible = bAccountsVisible;
            //}

            //// License Type
            //DataRow drUserLicense = StoreUtility.GetActiveUserLicenseSubscription (userId);

            //if (drUserLicense == null || drUserLicense ["license_subscription_id"] == null)
            //{
            //    lblLicenseType.Text = "Basic";
            //}
            //else
            //{
            //    switch (Convert.ToInt32 (drUserLicense ["license_subscription_id"]))
            //    {
            //        case (int) Constants.eLICENSE_SUBSCRIPTIONS.PILOT:
            //        {
            //            lblLicenseType.Text = "Enhanced";
            //            break;
            //        }
            //        case (int) Constants.eLICENSE_SUBSCRIPTIONS.DEVELOPER:
            //        {
            //            lblLicenseType.Text = "Basic";
            //            break;
            //        }
            //        case (int) Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL:
            //        {
            //            lblLicenseType.Text = "Commercial";
            //            break;
            //        }
            //        default:
            //        {
            //            lblLicenseType.Text = "Unknown";
            //            break;
            //        }

            //    }

            //}

            // Get any name changes
            PagedDataTable pdtNameChanges = UsersUtility.GetUserChanges(userId, "username <> username_old", "date_modified DESC", 1, 100);
            if (pdtNameChanges.Rows.Count > 0)
            {
                rptNameChanges.DataSource = pdtNameChanges;
                rptNameChanges.DataBind();
                divNameChanges.Style.Add("display", "block");
            }

            // Email Preferences
            chkNewsletter.Checked = user.Newsletter.Equals("Y");
            chkUpdates.Checked = (Convert.ToInt32(user.ReceiveUpdates).Equals(1));
            chkBlogcomments.Checked = (Convert.ToInt32(user.NotifyBlogComments).Equals(1));
            chkProfile.Checked = (Convert.ToInt32(user.NotifyProfileComments).Equals(1));
            chkFriendMessage.Checked = (Convert.ToInt32(user.NotifyFriendMessages).Equals(1));
            chkAnyoneMessage.Checked = (Convert.ToInt32(user.NotifyAnyoneMessages).Equals(1));
            chkFriendRequest.Checked = (Convert.ToInt32(user.NotifyFriendRequests).Equals(1));
            chkNewFriends.Checked = user.NotifyNewFriends;
            chkBlastComments.Checked = user.NotificationPreference.NotifyBlastComments;
            chkMediaComments.Checked = user.NotificationPreference.NotifyMediaComments;
            chkShopComments.Checked = user.NotificationPreference.NotifyShopCommentsPurchases;
            chkEventInvites.Checked = user.NotificationPreference.NotifyEventInvites;
            chkFriendBirthdays.Checked = user.NotificationPreference.NotifyFriendBirthdays;
            chkWorldBlasts.Checked = user.NotificationPreference.NotifyWorldBlasts;
            chkWorldRequests.Checked = user.NotificationPreference.NotifyWorldRequests;
        }

        protected void CountryCheck()
        {
            if (drpCountry.SelectedValue == "US")
            {
                txtPostalCode.Enabled = true;
                rftxtPostalCode.Enabled = true;
                spnPostalCodeRFIndicator.Visible = true;
            }
            else
            {
                txtPostalCode.Enabled = false;
                rftxtPostalCode.Enabled = false;
                spnPostalCodeRFIndicator.Visible = false;
            }
        }

        private int SELECTED_USER_ID
        {
            get
            {
                return (int)ViewState["users_id"];
            }
            set
            {
                ViewState["users_id"] = value;
            }

        }


        #endregion

        #region Event Handlers

        /// <summary>
        /// btnRegEmail_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRegEmail_Click(object sender, EventArgs e)
        {
            int userId = Convert.ToInt32(Session["SelectedUserId"]);

            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);

            MailUtility.SendRegistrationEmail(user.Username, user.Email, user.KeyValue);
            ShowErrorOnStartup("Your validation email has been sent.", false);
        }

        /// <summary>
        /// Change account
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void drpCountry_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            CountryCheck();
        }

        ///// <summary>
        ///// Change account
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void drpAccounts_Change(Object sender, EventArgs e)
        //{
        //    LoadData(Convert.ToInt32(drpAccounts.SelectedValue));
        //}

        ///// <summary>
        ///// Add a logical account
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void btnAddAccount_Click(object sender, ImageClickEventArgs e)
        //{
        //    int userId = Convert.ToInt32(Session["SelectedUserId"]);
        //    Response.Redirect(ResolveUrl(KanevaGlobals.JoinLocation + "?masterUserId=" + userId));
        //}

        ///// <summary>
        ///// Delete the users account
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void btnDeleteAccount_Click(object sender, ImageClickEventArgs e)
        //{
        //    int userId = Convert.ToInt32(Session["SelectedUserId"]);
        //    UserFacade userFacade = new UserFacade();
        //    userFacade.UpdateUserStatus(Convert.ToInt32(drpAccounts.SelectedValue), (int)Constants.eUSER_STATUS.DELETED);

        //    drpAccounts.Items.Remove(drpAccounts.SelectedValue);
        //    LoadData(userId);
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnChange_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("changeMyAccount.aspx");
        }


        /// <summary>
        /// btnUpdate_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClanDisband_Click(object sender, EventArgs e)
        {
            int userId = Convert.ToInt32(Session["SelectedUserId"]);
            UsersUtility.ClanDisband(userId);
            LoadData(userId);
        }


        /// <summary>
        /// btnClearProfilePic_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbRemovePic_Click(object sender, EventArgs e)
        {
            int userId = Convert.ToInt32(Session["SelectedUserId"]);
            UsersUtility.ClearUserProfilePic(userId);
            LoadData(userId);
        }


        /// <summary>
        /// btnUpdate_Clikc
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            UserFacade userFacade = new UserFacade();
            int userId = Convert.ToInt32(Session["SelectedUserId"]);

            // Log the CSR activity
            GetSiteManagementFacade().InsertCSRLog(GetUserId(), "CSR - Updating user details for userId = " + userId + ", username = " + txtUserName.Text, 0, userId);

            //// Are they editing logical accounts?
            //if (drpAccounts.Visible)
            //{
            //    userId = Convert.ToInt32(drpAccounts.SelectedValue);
            //}

            // Are they changing the password?
            if (txtPassword.Text.Trim().Length > 0)
            {
                string strPassword = Server.HtmlEncode(txtPassword.Text.Trim());
                string strUserName = Server.HtmlEncode(txtUserName.Text.Trim());

                // Update the password
                byte[] salt = new byte[9];
                new RNGCryptoServiceProvider().GetBytes(salt);
                string hashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(UsersUtility.MakeHash(strPassword + strUserName.ToLower()) + Convert.ToBase64String(salt), "MD5");

                byte[] keyvalue = new byte[10];
                new RNGCryptoServiceProvider().GetBytes(keyvalue);

                userFacade.UpdatePassword(userId, hashPassword, Convert.ToBase64String(salt));
            }

            // Did they change email? Now they must validate it.
            User user = userFacade.GetUser(userId);

            if (!Server.HtmlDecode(user.Email).ToLower().Equals(txtEmail.Text.ToLower()))
            {
                // They cannot change thier email if it is already in use
                if (userFacade.EmailExists(txtEmail.Text))
                {
                    ShowErrorOnStartup("The email address you provided is already associated with a Kaneva account. Please provide another email address.");
                    return;
                }

                userFacade.UpdateUserStatus(userId, (int)Constants.eUSER_STATUS.REGNOTVALIDATED);
            }

            // Update user
            userFacade.UpdateUserIdentityCSR(userId, Server.HtmlEncode(txtFirstName.Text), Server.HtmlEncode(txtLastName.Text), Server.HtmlEncode(txtDescription.Text),
                Server.HtmlEncode(drpGender.SelectedValue), Server.HtmlEncode(txtHomepage2.Text), Server.HtmlEncode(txtEmail.Text), chkShowEmail.Checked, chkShowMature.Checked, Server.HtmlEncode(drpCountry.SelectedValue), Server.HtmlEncode(txtPostalCode.Text),
                new DateTime(Convert.ToInt32(drpYear.SelectedValue), Convert.ToInt32(drpMonth.SelectedValue), Convert.ToInt32(drpDay.SelectedValue)), Server.HtmlEncode(txtDisplayName.Text));

            // Update Restriction            
            if (chkProfileRestrict.Checked)
            {
                userFacade.UpdateUserRestriction(userId, 1);
            }
            else
            {
                userFacade.UpdateUserRestriction(userId, 0);
            }

            // Did email notification prefs chage?  First check those on the user's record...
            if (!((user.Newsletter.Equals("Y")).Equals(chkNewsletter.Checked)) ||
                !((Convert.ToInt32(user.ReceiveUpdates).Equals(1)).Equals(chkUpdates.Checked)) ||
                !((Convert.ToInt32(user.NotifyBlogComments).Equals(1)).Equals(chkBlogcomments.Checked)) ||
                !((Convert.ToInt32(user.NotifyProfileComments).Equals(1)).Equals(chkProfile.Checked)) ||
                !((Convert.ToInt32(user.NotifyFriendMessages).Equals(1)).Equals(chkFriendMessage.Checked)) ||
                !((Convert.ToInt32(user.NotifyAnyoneMessages).Equals(1)).Equals(chkAnyoneMessage.Checked)) ||
                !((Convert.ToInt32(user.NotifyFriendRequests).Equals(1)).Equals(chkFriendRequest.Checked)) ||
                !((user.NotifyNewFriends).Equals(chkNewFriends.Checked)))
            {
                userFacade.UpdateUser(userId, (user.ShowMature ? 1 : 0), (user.BrowseAnonymously ? 1 : 0), (user.ShowOnline ? 1 : 0),
                    (chkNewsletter.Checked ? "Y" : "N"), (chkBlogcomments.Checked ? 1 : 0), (chkProfile.Checked ? 1 : 0), (chkFriendMessage.Checked ? 1 : 0), (chkAnyoneMessage.Checked ? 1 : 0),
                    (chkFriendRequest.Checked ? 1 : 0), (chkNewFriends.Checked ? 1 : 0), user.FriendsCanComment, user.EveryoneCanComment, (user.MatureProfile ? 1 : 0), (chkUpdates.Checked ? 1 : 0));
            }

            // ... Then check those stored in notification preferences.
            if (!(user.NotificationPreference.NotifyBlastComments == chkBlastComments.Checked) ||
                !(user.NotificationPreference.NotifyMediaComments == chkMediaComments.Checked) ||
                !(user.NotificationPreference.NotifyShopCommentsPurchases == chkShopComments.Checked) ||
                !(user.NotificationPreference.NotifyEventInvites == chkEventInvites.Checked) ||
                !(user.NotificationPreference.NotifyFriendBirthdays == chkFriendBirthdays.Checked) ||
                !(user.NotificationPreference.NotifyWorldBlasts == chkWorldBlasts.Checked) ||
                !(user.NotificationPreference.NotifyWorldRequests == chkWorldRequests.Checked))
            {
                user.NotificationPreference.UserId = userId;
                user.NotificationPreference.NotifyBlastComments = (chkBlastComments.Checked ? true : false);
                user.NotificationPreference.NotifyMediaComments = (chkMediaComments.Checked ? true : false);
                user.NotificationPreference.NotifyShopCommentsPurchases = (chkShopComments.Checked ? true : false);
                user.NotificationPreference.NotifyEventInvites = (chkEventInvites.Checked ? true : false);
                user.NotificationPreference.NotifyFriendBirthdays = (chkFriendBirthdays.Checked ? true : false);
                user.NotificationPreference.NotifyWorldBlasts = (chkWorldBlasts.Checked ? true : false);
                user.NotificationPreference.NotifyWorldRequests = (chkWorldRequests.Checked ? true : false);

                userFacade.UpdateUserNotificationPreferences(user.NotificationPreference);
            }

            LoadData(userId);
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "created_datetime";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "desc";
            }
        }

        /// <summary>
        /// Age Disclaimer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkShowMature_Click()
        {
            Response.Redirect(ResolveUrl(KanevaGlobals.JoinLocation));
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
        }
        #endregion

    }
}