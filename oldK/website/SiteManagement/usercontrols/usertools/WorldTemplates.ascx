<%@ Control Language="C#" AutoEventWireup="true" Codebehind="WorldTemplates.ascx.cs"
	Inherits="SiteManagement.usercontrols.WorldTemplates" %>

<asp:ScriptManager ID="smWorldTemplates" runat="server" EnablePartialRendering="true"></asp:ScriptManager>	
<asp:LinkButton ID="lbListTemplates" runat="server" Text="List Templates" OnClick="btnListTemplates_Click"></asp:LinkButton> | 
<asp:LinkButton ID="lbNewTemplate" runat="server" Text="New" OnClick="btnNewTemplate_Click"></asp:LinkButton>
<br />
<br />
<div id="divErrorData" runat="server" class="error"></div>
<div id="dvTemplateEdit" runat="server">
    <asp:LinkButton id="lbManageDefaultItems" causesvalidation="false" Runat="server" OnCommand="lbManageDefaultItems_Click" Visible="false">Manage Default Items</asp:LinkButton>
	<table>
		<tr>
			<td>
				<table>
                    <tr>
                        <td>Template Id<asp:RequiredFieldValidator ID="rfTemplateId" ControlToValidate="txtTemplateId" Text="*" ErrorMessage="TemplateId is a required field." Display="Dynamic" runat="server"/></td>
                        <td>
                            <asp:TextBox ID="txtTemplateId" runat="server" Columns="3"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
					<tr>
						<td>Name<asp:RequiredFieldValidator ID="rfName" ControlToValidate="txtTemplateName" Text="*" ErrorMessage="Name is a required field." Display="Dynamic" runat="server"/></td>
						<td>
							<asp:TextBox ID="txtTemplateName" runat="server" Columns="60"></asp:TextBox>
                        </td>
						<td>
						</td>
					</tr>
					<tr>
						<td>Description<asp:RequiredFieldValidator ID="rfTemplateDescription" ControlToValidate="txtTemplateDescription" Text="*" ErrorMessage="Description is a required field." Display="Dynamic" runat="server"/></td>
						<td>
							<asp:TextBox ID="txtTemplateDescription" runat="server" Columns="60"></asp:TextBox>
                        </td>
						<td>
						</td>
					</tr>
                    <tr>
						<td>Feature List<asp:RequiredFieldValidator ID="rfFeatureList" ControlToValidate="txtTemplateFeatureList" Text="*" ErrorMessage="Feature List is a required field." Display="Dynamic" runat="server"/></td>
						<td>
							<asp:TextBox ID="txtTemplateFeatureList" runat="server" Columns="60"></asp:TextBox>
                        </td>
						<td>
						</td>
					</tr>
					<tr>
						<td>Default Category</td>
						<td>
							<asp:DropDownList ID="ddlDefaultCategoryId" runat="server"></asp:DropDownList>
						</td>
						<td>
						</td>
					</tr>
					<tr>
						<td>Sort Order</td>
						<td>
							<asp:TextBox ID="txtSortOrder" runat="server" Columns="3"></asp:TextBox>
                        </td>
                        <td>
						</td>
					</tr>
					<tr>
						<td>Status</td>
						<td>
							<asp:DropDownList ID="ddlStatus" runat="server"></asp:DropDownList>
						</td>
						<td>
						</td>
					</tr>
                    <tr>
						<td>First Public Release Date<asp:RequiredFieldValidator ID="rfFirstPublicReleaseDate" ControlToValidate="txtFirstPublicReleaseDate" Text="*" ErrorMessage="First public release date is a required field." Display="Dynamic" runat="server"/></td>
						<td>
							<asp:TextBox ID="txtFirstPublicReleaseDate" runat="server" Columns="14"></asp:TextBox>
                        </td>
                        <td>
						</td>
					</tr>
					<tr>
						<td>Preview Image Small</td>
                        <td>
                            <asp:Literal id="litPvImageSmallPath" runat="server" />
						</td>
                        <td>
							<input type="file" runat="server" id="fiPreviewImageSmall" size="45" />
                        </td>
					</tr>
                    <tr>
						<td>Preview Image Medium</td>
                        <td>
                            <asp:Literal id="litPvImageMediumPath" runat="server" />
						</td>
						<td>
							<input type="file" runat="server" id="fiPreviewImageMedium" size="45" />
                        </td>
					</tr>
                    <tr>
						<td>Preview Image Large</td>
                        <td>
                            <asp:Literal id="litPvImageLargePath" runat="server" />
						</td>
						<td>
							<input type="file" runat="server" id="fiPreviewImageLarge" size="45" />
                        </td>
					</tr>
                    <tr>
						<td>Default Thumbnail</td>
                        <td>
                            <asp:Literal id="litDefaultThumbnailPath" runat="server" />
						</td>
						<td>
							<input type="file" runat="server" id="fiDefaultThumbnail" size="45" />
                        </td>
					</tr>
                    <tr>
						<td>Is Default Home Template</td>
						<td>
							<asp:DropDownList ID="ddlIsDefault" runat="server">
                                <asp:ListItem Value="True" Text="True" />
								<asp:ListItem Value="False" Text="False" />
                            </asp:DropDownList>
						</td>
						<td>
						</td>
					</tr>
                    <tr>
						<td>Incubator Hosted</td>
						<td>
							<asp:DropDownList ID="ddlIsIncubatorHosted" runat="server">
                                <asp:ListItem Value="True" Text="True" />
								<asp:ListItem Value="False" Text="False" />
                            </asp:DropDownList>
						</td>
						<td>
						</td>
					</tr>
                    <tr>
						<td>KEP Thumbnail Id<asp:RequiredFieldValidator ID="rfKepThumbnailId" ControlToValidate="txtKEPThumbnailId" Text="*" ErrorMessage="KEPThumbnailId is a required field." Display="Dynamic" runat="server"/></td>
						<td>
							<asp:TextBox ID="txtKEPThumbnailId" runat="server" Columns="3"></asp:TextBox>
                        </td>
                        <td>
						</td>
					</tr>
                    <tr>
						<td>Display Pass Group</td>
						<td>
							<asp:DropDownList ID="ddlPassGroup" runat="server"></asp:DropDownList>
                        </td>
                        <td>
						</td>
					</tr>
                    <tr>
						<td>UpsellPass Group</td>
						<td>
							<asp:DropDownList ID="ddlUpsellPassGroup" runat="server"></asp:DropDownList>
                        </td>
                        <td>
						</td>
					</tr>
                    <tr>
						<td>GlobalId</td>
						<td>
							<asp:TextBox ID="txtGlobalId" runat="server" Columns="3"></asp:TextBox>
                        </td>
                        <td>
						</td>
					</tr>
                    <tr>
						<td>Grandfather Date</td>
						<td>
							<asp:TextBox ID="txtGrandfatherDate" runat="server" Columns="14"></asp:TextBox>
                        </td>
                        <td>
						</td>
					</tr>
                    <tr>
						<td>Private on Create</td>
						<td>
							<asp:DropDownList ID="ddlPrivateOnCreate" runat="server">
                                <asp:ListItem Value="True" Text="True" />
								<asp:ListItem Value="False" Text="False"/>
                            </asp:DropDownList>
						</td>
						<td>
						</td>
					</tr>
                    <tr>
                        <td>A/B Experiments</td>
                        <td>
							<asp:DropDownList ID="ddlExperiments" runat="server" DataTextField="Name" DataValueField="ExperimentId" OnSelectedIndexChanged="ddlExperiments_OnSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </td>
                        <td>
                            <asp:UpdatePanel ID="udpExperimentGroups" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlExperiments" EventName="SelectedIndexChanged" />
                                </Triggers>
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlExperimentGroups" runat="server" DataTextField="Name" DataValueField="GroupId"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
						</td>
                    </tr>
					<tr>
						<td>
						</td>
						<td>
							<asp:Button ID="btnSaveTemplate" Text="Save" OnClick="btnSaveTemplate_Click" runat="server" />
							<asp:Button ID="btnCancelEdit" Text="Cancel" OnClick="btnCancelEdit_Click" runat="server" />
						</td>
						<td>
						</td>
					</tr>
					<tr>
						<td>
						</td>
						<td>
							<asp:Label ID="txtSaveStatus" runat="server"></asp:Label></td>
						<td>
						</td>
					</tr>
				</table>
			</td>
			<td>
				<!-- Help Text -->
			</td>
		</tr>
	</table>
</div>
<div id="dvTemplates" runat="server">
	<asp:Label ID="txtGVStatus" runat="server"></asp:Label>
	<asp:GridView ID="gvTemplates" runat="server" OnRowEditing="gvTemplates_Edit" OnRowDeleting="gvTemplates_Delete" AutoGenerateColumns="false">
		<RowStyle BackColor="White"></RowStyle>
		<HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000"
			HorizontalAlign="Left"></HeaderStyle>
		<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
		<FooterStyle BackColor="#FFffff"></FooterStyle>
		<Columns>
			<asp:TemplateField>
				<ItemTemplate>
					&nbsp;<asp:LinkButton ID="lbnEdit" runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton> |
                    <asp:LinkButton ID="lbnDelete" runat="server" CausesValidation="false" OnClientClick="if(!confirm('Are you sure you want to delete the selected template?')) return false;" CommandName="Delete">Delete</asp:LinkButton>&nbsp;
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="TemplateId" HeaderText="Id" />
			<asp:BoundField DataField="Name" HeaderText="Name" />
			<asp:BoundField DataField="SortOrder" HeaderText="Sort Order" />
            <asp:BoundField DataField="Status" HeaderText="Status" />
            <asp:BoundField DataField="DisplayPassGroupId" HeaderText="Pass Group" />
		</Columns>
	</asp:GridView>
</div>
