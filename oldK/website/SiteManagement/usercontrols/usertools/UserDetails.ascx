<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="UserDetails.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.UserDetails" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<asp:Panel id="pnl_Security" runat="server" Enabled="true">
<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="980">
    <tr>
        <td style="padding-left:40px">
			<table cellpadding="0" cellspacing="0" border="0" width="750" >
                <tr>
                    <td style="text-align:center; font-size:22px">
                        USER DETAILS
                    </td>
                </tr>
				<tr>
					<td><asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="Validationsummary1" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/></td>
				</tr>
				<tr id="trError" runat="server">
					<td class="messageBox">
						<br/>			
						NOTICE: Please validate your 
						current email address '<b><asp:Label runat="server" id="lblEmail" CssClass="datestamp"/></b>'. 
						Follow the instructions in the message Kaneva 
						sends to this address to validate your address, or click 'Send Validation' if 
						you'd like another copy of the message.
						<br/><br/><center><asp:button id="btnRegEmail" align="right" runat="Server" onClick="btnRegEmail_Click" class="Filter2" Text=" Send Validation " CausesValidation="False"/></center>
				
					</td>
				</tr>
				<tr>
					<td colspan="3"></td>
				</tr>
				
			</table> 
        
        </td>
    </tr>
    <tr>
        <td style="padding-left:40px">
            <table cellpadding="0" cellspacing="0" border="0" width="750" bgcolor="#ededed">							
				<tr>
					<td valign="top" align="center" class="bodyText" bgcolor="#ededed">
						<img alt="spacer" runat="server" src="~/images/spacer.gif" width="100" height="1" id="Img2"/>
						<br/>
						<img runat="server" id="imgAvatar" alt="avatar" src="http://www.kaneva.com/images/KanevaIcon01.gif" border="0" style="border: 1px solid #666666;" width="64" height="64"/><br/><br/>
						<br/>
					</td>
					<td width="100%" valign="top">
						<table cellpadding="0" cellspacing="0" border="1">
							<tr>
								<td colspan="3" class="bodyText">
									
									<asp:linkbutton id="lbRemovePic" runat="server" onclientclick="return confirm('Are you sure you want to clear the profile picture?');" onclick="lbRemovePic_Click">Clear profile pic</asp:linkbutton>
								    <br/>
									
									<b><font size="3" face="verdana"><asp:Label runat="server" id="lblUsername"/> (UserID: <asp:Label runat="server" id="lblUserId"/>)</font></b>
								</td>
							</tr>
							
							<tr>
								<td class="bodyBold" align="right" valign="middle" width="70">Account:</td>
								<td width="2">&nbsp;</td>
								<td class="bodyText" align="left" valign="middle" width="100%"> <b style="color: green; font-size: 14px;"><asp:Label runat="server" id="lblLicenseType"/></b>&nbsp;</td>
							</tr>
							<tr>
								<td class="bodyBold" align="right" valign="middle" width="70">K-Points:</td>
								<td width="2">&nbsp;</td>
								<td class="bodyText" align="left" valign="middle" width="100%"><b style="color: green;"><asp:Label runat="server" id="lblKPoint"/></b></td>
							</tr>
							<tr>
								<td class="bodyBold" align="right" valign="middle" width="70">Cash:</td>
								<td width="2">&nbsp;</td>
								<td class="bodyText" align="left" valign="middle" width="100%"><b style="color: green;">
									<asp:Label runat="server" id="lblCash"/></b>
									
								</td>
							</tr>
							<tr id="trClan" runat="server">
								<td class="bodyBold" align="right" valign="middle" width="70">Clan:</td>
								<td width="2">&nbsp;</td>
								<td class="bodyText" align="left" valign="middle" width="100%">
									<asp:Label runat="server" id="lblClan" />
									<asp:button runat="server" text="Disband" id="btnClanDisband" OnClientClick="return confirm('This action removes all members and deltes this clan. Are you sure?');" onClick="btnClanDisband_Click" />
								</td>
							</tr>									
							<tr>
								<td class="bodyBold" align="right" valign="middle" width="70">Homepage:</td>
								<td width="2">&nbsp;</td>
								<td class="bodyText" align="left" valign="middle" width="100%">
									<small>http://</small><asp:TextBox id="txtHomepage2" class="Filter2" style="width:242px" MaxLength="242" runat="server"/>
								</td>
							</tr>
							
							<tr>
								<td class="bodyBold" align="right" valign="middle" width="70"></td>
								<td width="2">&nbsp;</td>
								<td class="bodyText" align="left" valign="middle" width="100%">
									<asp:Checkbox runat="server" id="chkShowMature"/><b> Show mature content.</b><br/><br/>
									<asp:Checkbox runat="server" id="chkProfileRestrict"/><b> Restrict user.</b><br/><br/>
								</td>
							</tr>
						</table>
					</td>
				</tr>
                	<tr>
					<td bgcolor="#dddddd" class="blogHeadline2" align="right" width="100" valign="middle" style="background-color: #dddddd;">Tell People About Yourself:</td>
					<td class="bodyText" bgcolor="#dddddd" align="left" valign="middle" height="100" colspan="2">
						<asp:TextBox id="txtDescription" TextMode="multiline" Rows="2" cols="76" class="Filter2" MaxLength="400" runat="server"/><br/><br/>
					</td>
				</tr>
                <tr>
					<td colspan="3" align="left" valign="middle" style="background-color: #FFC96C;">
						&nbsp;&nbsp;<span class="blogHeadline2"><b>Notes</b></span>
					</td>
					<td colspan="2" style="background-color: #FFC96C;" width="570">&nbsp;&nbsp;</td>
				</tr>

                <tr>
                <td>Notes</td>
                <td colspan="2">
                    
                    Note <asp:Label runat="server" id="lblNoteSearch" CssClass="dateStamp"/>
                     <asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop"/><br>

                     <asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="700" id="dgrdNotes" cellpadding="0" cellspacing="0" border="1" AutoGenerateColumns="False" OnSortCommand="UserNote_Sorting" AllowSorting="True" CssClass="FullBorders">  
	                    <HeaderStyle BackColor="#cccccc" ForeColor="#000000" Font-Underline="false" Font-Bold="true" HorizontalAlign="Left" />
	                    <ItemStyle BackColor="#ffffff" HorizontalAlign="Left" />
	                    <AlternatingItemStyle BackColor="#eeeeee" HorizontalAlign="Left" />
	                    <Columns>
		                    <asp:TemplateColumn HeaderText="created date" SortExpression="created_datetime" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Top">
			                    <ItemTemplate>
				                    <%# FormatDateTime (DataBinder.Eval(Container.DataItem, "created_datetime")) %>&nbsp;
			                    </ItemTemplate>
		                    </asp:TemplateColumn>
			                    <asp:TemplateColumn HeaderText="posted by" SortExpression="username" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Top">
				                    <ItemTemplate>
					                    <%# DataBinder.Eval(Container.DataItem, "username") %>&nbsp;
				                    </ItemTemplate>
			                    </asp:TemplateColumn>
		                    <asp:TemplateColumn HeaderText="note" SortExpression="note" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="80%">
			                    <ItemTemplate> 
				                    <%# DataBinder.Eval(Container.DataItem, "note") %>
			                    </ItemTemplate>
		                    </asp:TemplateColumn>		
	                    </Columns>
                    </asp:datagrid>        
                
                    <br/>
                    <asp:TextBox id="txtNote" TextMode="multiline" Rows="6" cols="76" class="Filter2" MaxLength="400" runat="server"/>
                    <br/>
                    <asp:button id="btnAddNote" runat="Server" onClick="btnAddNote_Click" CausesValidation="False" class="Filter2" Text="  add note  "/>
                    <br/>
                </td>


                </tr>


			
			</table>
        </td>
    </tr>
    <tr>
        <td style="padding-left:40px">
            <ajax:ajaxpanel id="ajpUserForm" runat="server">
            <table cellpadding="0" cellspacing="0" border="0" width="750" id="Table2" style="border-left: 1px solid #ededed; border-right: 1px solid #ededed; border-bottom: 1px solid #ededed;">
				<tr>
					<td colspan="3" align="left" valign="middle" style="background-color: #FFC96C;">
						&nbsp;&nbsp;<span class="blogHeadline2"><b>Credentials</b></span>
					</td>
					<td colspan="2" style="background-color: #FFC96C;" width="570">&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2" bgcolor="#dddddd">
						<font color="red">*</font> <b>Username:</b>&nbsp;
					</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:TextBox id="txtUserName" Enabled="False" class="Filter2" style="width:300px" MaxLength="80" runat="server"/><asp:RequiredFieldValidator id="rfUsername" ControlToValidate="txtUserName" Text="*" ErrorMessage="Username is a required field." Display="Dynamic" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2" bgcolor="#dddddd">
						<font color="red">*</font> <b>Display Name:</b>&nbsp;
					</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:TextBox id="txtDisplayName" class="Filter2" style="width:300px" MaxLength="30" runat="server"/><asp:RequiredFieldValidator id="rfDisplayname" ControlToValidate="txtDisplayName" Text="*" ErrorMessage="Display Name is a required field." Display="Dynamic" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2">
						<font color="red">*</font> <b>Email:</b>&nbsp; 
					</td>
					<td colspan="2" class="bodyText">
						<asp:TextBox id="txtEmail" class="Filter2" style="width:300px" MaxLength="100" runat="server"/>
						<asp:RequiredFieldValidator id="rfEmail" ControlToValidate="txtEmail" Text="*" ErrorMessage="Email is a required field." Display="Dynamic" runat="server"/>
						<asp:RegularExpressionValidator id="revEmail" ControlToValidate="txtEmail" Text="*" Display="Static" ErrorMessage="Invalid email address." EnableClientScript="True" runat="server"/><asp:Checkbox runat="server" id="chkShowEmail"/>show 
					</td>
				</tr>							
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2" bgcolor="#dddddd">
						<font color="red">*</font> <b>Confirm Email:</b>&nbsp; 
					</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:TextBox id="txtConfirmEmail" class="Filter2" style="width:300px" MaxLength="100" runat="server"/><asp:CompareValidator id="cmpEmail" ControlToValidate="txtConfirmEmail" ControlToCompare="txtEmail" Type="String" Operator="Equal" ErrorMessage="Email and Confirm Email must match." Text="*" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2">
						<b>New Password:</b>&nbsp; 
					</td>
					<td colspan="2">
						<asp:TextBox id="txtPassword" TextMode="Password" class="Filter2" style="width:300px" MaxLength="20" runat="server"/>
						<asp:RegularExpressionValidator id="revPassword" runat="server" Text="*" ErrorMessage="Password contains illegal characters or is too short. Password must contain only letters, numbers, and underscore,<br/> and must contain at least 4 characters." Display="Dynamic" ControlToValidate="txtPassword"/>
					</td>
				</tr>	
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2" bgcolor="#dddddd">
						<b>Confirm Password:</b>&nbsp; 
					</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:TextBox id="txtConfirmPassword" TextMode="Password" class="Filter2" style="width:300px" MaxLength="20" runat="server"/><asp:CompareValidator Text="*" id="cmpPassword" ControlToValidate="txtConfirmPassword" ControlToCompare="txtPassword" Type="String" Operator="Equal" ErrorMessage="Password and Confirm Password must match." runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2">
						<font color="red">*</font> <b>First Name:</b>&nbsp; 
					</td>
					<td colspan="2">
						<asp:TextBox id="txtFirstName" class="Filter2" style="width:200px" MaxLength="50" runat="server"/><asp:RequiredFieldValidator id="rffirstname" ControlToValidate="txtFirstName" Text="*" ErrorMessage="First name is a required field." Display="Dynamic" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2" bgcolor="#dddddd">
						<font color="red">*</font> <b>Last Name:</b>&nbsp; 
					</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:TextBox id="txtLastName" class="Filter2" style="width:200px" MaxLength="50" runat="server"/><asp:RequiredFieldValidator id="rflastname" ControlToValidate="txtLastName" Text="*" ErrorMessage="Last name is a required field." Display="Dynamic" runat="server"/>
					</td>
				</tr>							
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2">
						<font color="red">*</font> <b>Date of Birth:</b>&nbsp;
					</td>
					<td colspan="2">
						<asp:Dropdownlist runat="server" class="Filter2" id="drpMonth">
							<asp:ListItem  value="">select...</asp:ListItem >
							<asp:ListItem  value="1">January</asp:ListItem >
							<asp:ListItem  value="2">February</asp:ListItem >
							<asp:ListItem  value="3">March</asp:ListItem >
							<asp:ListItem  value="4">April</asp:ListItem >
							<asp:ListItem  value="5">May</asp:ListItem >
							<asp:ListItem  value="6">June</asp:ListItem >
							<asp:ListItem  value="7">July</asp:ListItem >
							<asp:ListItem  value="8">August</asp:ListItem >
							<asp:ListItem  value="9">September</asp:ListItem >
							<asp:ListItem  value="10">October</asp:ListItem >
							<asp:ListItem  value="11">November</asp:ListItem >
							<asp:ListItem  value="12">December</asp:ListItem >
						</asp:Dropdownlist><asp:RequiredFieldValidator id="rfMonth" ControlToValidate="drpMonth" Text="*" ErrorMessage="Birth month is a required field." Display="Static" runat="server"/>
						<asp:Dropdownlist runat="server" class="Filter2" id="drpDay" name="drpDay">
							<asp:ListItem  value="">select...</asp:ListItem >
							<asp:ListItem  value="1">01</asp:ListItem >
							<asp:ListItem  value="2">02</asp:ListItem >
							<asp:ListItem  value="3">03</asp:ListItem >
							<asp:ListItem  value="4">04</asp:ListItem >
							<asp:ListItem  value="5">05</asp:ListItem >
							<asp:ListItem  value="6">06</asp:ListItem >
							<asp:ListItem  value="7">07</asp:ListItem >
							<asp:ListItem  value="8">08</asp:ListItem >
							<asp:ListItem  value="9">09</asp:ListItem >
							<asp:ListItem  value="10">10</asp:ListItem >
							<asp:ListItem  value="11">11</asp:ListItem >
							<asp:ListItem  value="12">12</asp:ListItem >
							<asp:ListItem  value="13">13</asp:ListItem >
							<asp:ListItem  value="14">14</asp:ListItem >
							<asp:ListItem  value="15">15</asp:ListItem >
							<asp:ListItem  value="16">16</asp:ListItem >
							<asp:ListItem  value="17">17</asp:ListItem >
							<asp:ListItem  value="18">18</asp:ListItem >
							<asp:ListItem  value="19">19</asp:ListItem >
							<asp:ListItem  value="20">20</asp:ListItem >
							<asp:ListItem  value="21">21</asp:ListItem >
							<asp:ListItem  value="22">22</asp:ListItem >
							<asp:ListItem  value="23">23</asp:ListItem >
							<asp:ListItem  value="24">24</asp:ListItem >
							<asp:ListItem  value="25">25</asp:ListItem >
							<asp:ListItem  value="26">26</asp:ListItem >
							<asp:ListItem  value="27">27</asp:ListItem >
							<asp:ListItem  value="28">28</asp:ListItem >
							<asp:ListItem  value="29">29</asp:ListItem >
							<asp:ListItem  value="30">30</asp:ListItem >
							<asp:ListItem  value="31">31</asp:ListItem >
						</asp:Dropdownlist><asp:RequiredFieldValidator id="rfDay" ControlToValidate="drpDay" Text="*" ErrorMessage="Birth day is a required field." Display="Static" runat="server"/>
						<asp:Dropdownlist runat="server" class="Filter2" id="drpYear">
							<asp:ListItem  value="">select...</asp:ListItem >
							<asp:ListItem  value="1940">< 1940</asp:ListItem >
							<asp:ListItem  value="1941">1941</asp:ListItem >
							<asp:ListItem  value="1942">1942</asp:ListItem >
							<asp:ListItem  value="1943">1943</asp:ListItem >
							<asp:ListItem  value="1944">1944</asp:ListItem >
							<asp:ListItem  value="1945">1945</asp:ListItem >
							<asp:ListItem  value="1946">1946</asp:ListItem >
							<asp:ListItem  value="1947">1947</asp:ListItem >
							<asp:ListItem  value="1948">1948</asp:ListItem >
							<asp:ListItem  value="1949">1949</asp:ListItem >
							<asp:ListItem  value="1950">1950</asp:ListItem >
							<asp:ListItem  value="1951">1951</asp:ListItem >
							<asp:ListItem  value="1952">1952</asp:ListItem >
							<asp:ListItem  value="1953">1953</asp:ListItem >
							<asp:ListItem  value="1954">1954</asp:ListItem >
							<asp:ListItem  value="1955">1955</asp:ListItem >
							<asp:ListItem  value="1956">1956</asp:ListItem >
							<asp:ListItem  value="1957">1957</asp:ListItem >
							<asp:ListItem  value="1958">1958</asp:ListItem >
							<asp:ListItem  value="1959">1959</asp:ListItem >
							<asp:ListItem  value="1960">1960</asp:ListItem >
							<asp:ListItem  value="1961">1961</asp:ListItem >
							<asp:ListItem  value="1962">1962</asp:ListItem >
							<asp:ListItem  value="1963">1963</asp:ListItem >
							<asp:ListItem  value="1964">1964</asp:ListItem >
							<asp:ListItem  value="1965">1965</asp:ListItem >
							<asp:ListItem  value="1966">1966</asp:ListItem >
							<asp:ListItem  value="1967">1967</asp:ListItem >
							<asp:ListItem  value="1968">1968</asp:ListItem >
							<asp:ListItem  value="1969">1969</asp:ListItem >
							<asp:ListItem  value="1970">1970</asp:ListItem >
							<asp:ListItem  value="1971">1971</asp:ListItem >
							<asp:ListItem  value="1972">1972</asp:ListItem >
							<asp:ListItem  value="1973">1973</asp:ListItem >
							<asp:ListItem  value="1974">1974</asp:ListItem >
							<asp:ListItem  value="1975">1975</asp:ListItem >
							<asp:ListItem  value="1976">1976</asp:ListItem >
							<asp:ListItem  value="1977">1977</asp:ListItem >
							<asp:ListItem  value="1978">1978</asp:ListItem >
							<asp:ListItem  value="1979">1979</asp:ListItem >
							<asp:ListItem  value="1980">1980</asp:ListItem >
							<asp:ListItem  value="1981">1981</asp:ListItem >
							<asp:ListItem  value="1982">1982</asp:ListItem >
							<asp:ListItem  value="1983">1983</asp:ListItem >
							<asp:ListItem  value="1984">1984</asp:ListItem >
							<asp:ListItem  value="1985">1985</asp:ListItem >
							<asp:ListItem  value="1986">1986</asp:ListItem >
							<asp:ListItem  value="1987">1987</asp:ListItem >
							<asp:ListItem  value="1988">1988</asp:ListItem >
							<asp:ListItem  value="1989">1989</asp:ListItem >
							<asp:ListItem  value="1990">1990</asp:ListItem >
							<asp:ListItem  value="1991">1991</asp:ListItem >
							<asp:ListItem  value="1992">1992</asp:ListItem >
							<asp:ListItem  value="1993">1993</asp:ListItem >
							<asp:ListItem  value="1994">1994</asp:ListItem >
							<asp:ListItem  value="1995">1995</asp:ListItem >
							<asp:ListItem  value="1996">1996</asp:ListItem >
							<asp:ListItem  value="1997">1997</asp:ListItem >
							<asp:ListItem  value="1998">1998</asp:ListItem >
							<asp:ListItem  value="1999">1999</asp:ListItem >
							<asp:ListItem  value="2000">2000</asp:ListItem >
							<asp:ListItem  value="2001">2001</asp:ListItem >
							<asp:ListItem  value="2002">2002</asp:ListItem >
                            <asp:ListItem  value="2003">2003</asp:ListItem >
                            <asp:ListItem  value="2004">2004</asp:ListItem >
                            <asp:ListItem  value="2005">2005</asp:ListItem >
						</asp:Dropdownlist><asp:RequiredFieldValidator id="rfYear" ControlToValidate="drpYear" Text="*" ErrorMessage="Birth year is a required field." Display="Static" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2" bgcolor="#dddddd">
						<font color="red">*</font> <b>Gender:</b>&nbsp; 
					</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:Dropdownlist runat="server" class="Filter2" id="drpGender">
							<asp:ListItem value="">select...</asp:ListItem >
							<asp:ListItem value="M">Male</asp:ListItem >
							<asp:ListItem  value="F">Female</asp:ListItem >
						</asp:Dropdownlist><asp:RequiredFieldValidator id="rfdrpGender" ControlToValidate="drpGender" Text="*" ErrorMessage="Gender is a required field." Display="Dynamic" runat="server"/>
					</td>
				</tr>	
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2">
						<span style="color:Red;" id="spnPostalCodeRFIndicator" runat="server">*</span> <b>Postal Code:</b>&nbsp;
					</td>
					<td colspan="2">
						<asp:TextBox id="txtPostalCode" class="Filter2" Width="100" MaxLength="25" runat="server"/><asp:RequiredFieldValidator id="rftxtPostalCode" ControlToValidate="txtPostalCode" Text="*" ErrorMessage="Postal code is a required field." Display="Dynamic" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2"  bgcolor="#dddddd">
						<font color="red">*</font> <b>Country:</b>&nbsp;
					</td>
					<td colspan="2"  bgcolor="#dddddd">
						<asp:Dropdownlist runat="server" class="Filter2" id="drpCountry" AutoPostBack="true" OnSelectedIndexChanged="drpCountry_OnSelectedIndexChanged">
						</asp:Dropdownlist><asp:RequiredFieldValidator id="rfdrpCountry" ControlToValidate="drpCountry" Text="*" ErrorMessage="Country is a required field." Display="Dynamic" runat="server"/>
                    </td>
				</tr>
				<tr>
					<td colspan="8" valign="top" align="center">
						<div id="divNameChanges" runat="server"	style="display:none;">
							<asp:repeater id="rptNameChanges" runat="server">
								<headertemplate>
									<div class="filter2" style="font-weight:bold;margin:20px 0 6px 0;width:600px;text-align:left;">Name Changes:</div>
									<div style="border:solid 1px gray;width:600px;">
									<table cellpadding="5" cellspacing="0" border="0">
										<tr>
											<td width="200" class="filter2" style="font-weight:bold;background-color:#dddddd;">New Username</td>
											<td width="200" class="filter2" style="font-weight:bold;background-color:#dddddd;">Old Username</td>
											<td width="200" class="filter2" style="font-weight:bold;background-color:#dddddd;">Date Changed</td></tr>
								</headertemplate>
								<itemtemplate>
									<tr>
										<td class="filter2"><%# DataBinder.Eval(Container.DataItem, "username") %></td>
										<td class="filter2"><%# DataBinder.Eval(Container.DataItem, "username_old") %></td>
										<td class="filter2"><%# DataBinder.Eval(Container.DataItem, "date_modified") %></td>
									</tr>	
								</itemtemplate>
								<alternatingitemtemplate>
									<tr>
										<td class="filter2" style="background-color:#ffffff;"><%# DataBinder.Eval(Container.DataItem, "username") %></td>
										<td class="filter2" style="background-color:#ffffff;"><%# DataBinder.Eval(Container.DataItem, "username_old") %></td>
										<td class="filter2" style="background-color:#ffffff;"><%# DataBinder.Eval(Container.DataItem, "date_modified") %></td>
									</tr>	
								</alternatingitemtemplate>
								<footertemplate></table></div></footertemplate>
							</asp:repeater>
						</div>
						<br/>
					</td>
				</tr>
			</table>
            </ajax:ajaxpanel>
        </td>
    </tr>

    
    <tr>
        <td style="padding-left:40px">
			<table cellpadding="0" cellspacing="0" border="0" width="750" style="border-left: 1px solid #ededed; border-right: 1px solid #ededed;">				
				<tr>
					<td colspan="5" align="left" valign="middle" class="blogHeadline2" style="line-height:22px;" bgcolor="#FFC96C">
						<span class="blogHeadline2">&nbsp;&nbsp;<b>Email Preferences</b></span>&nbsp; 							
					</td>
				</tr>
				<tr>
					<td colspan="5"><br/></td>
				</tr>
                <tr>
					<td><asp:checkbox runat="server" id="chkNewsletter"/></td>
					<td colspan="4">Send me the Kaneva Newsletter</td>
				</tr>								
				<tr>
					<td><asp:checkbox runat="server" id="chkUpdates"/></td>
					<td colspan="4">Send me Kaneva Update</td>
				</tr>
				<tr>
					<td><asp:checkbox runat="server" id="chkBlogcomments"/></td>
					<td colspan="4">Send me Blog Comments </td>
				</tr>														        														        
				<tr>
					<td><asp:checkbox runat="server" id="chkProfile"/></td>
					<td>Send me Profile Comments</td>
				</tr>														        
				<tr>
					<td><asp:checkbox runat="server" id="chkFriendMessage"/></td>
					<td colspan="4">Send me Friend Messages (private messages from my friends)</td>
				</tr>														        
				<tr>
					<td><asp:checkbox runat="server" id="chkAnyoneMessage"/></td>
					<td colspan="4">Send me Member Messages (private messages from other Kaneva members)</td>
				</tr>
				<tr>
					<td><asp:checkbox runat="server" id="chkFriendRequest"/></td>
					<td colspan="4">Send me Friend Requests</td>
				</tr>
				<tr>
					<td><asp:checkbox runat="server" id="chkNewFriends"/></td>
					<td>Send me New Friend Notications</td>
				</tr>
				<tr>
					<td><asp:checkbox runat="server" id="chkBlastComments"/></td>
					<td colspan="4">Send me Blast Comments</td>
				</tr>
				<tr>
					<td><asp:checkbox runat="server" id="chkMediaComments"/></td>
					<td colspan="4">Send me Media Comments</td>
				</tr>
				<tr>
					<td><asp:checkbox runat="server" id="chkShopComments"/></td>
					<td colspan="4">Send me Shop Comments and Purchases</td>
				</tr>
				<tr>
					<td><asp:checkbox runat="server" id="chkEventInvites"/></td>
					<td colspan="4">Send me Event Invites and Comments</td>
				</tr>
				<tr>
					<td><asp:checkbox runat="server" id="chkFriendBirthdays"/></td>
					<td colspan="4">Send me Friends' Birthdays</td>
				</tr>
				<tr>
					<td><asp:checkbox runat="server" id="chkWorldBlasts"/></td>
					<td colspan="4">Send me World Blast Emails</td>
				</tr>
                <tr>
					<td><asp:checkbox runat="server" id="chkWorldRequests"/></td>
					<td colspan="4">Send me World Request Emails</td>
				</tr>
				<tr>
					<td colspan="5"><br/></td>
				</tr>	
			</table>
        </td>
    </tr>
    <tr>
        <td style="padding-left:40px">
			<table cellpadding="0" cellspacing="0" border="0" width="750">
				<tr>	
					<td align="right" bgcolor="#ededed" width="100%"><br/>
						<asp:button id="btnUpdate" runat="Server" onClick="btnUpdate_Click" class="Filter2" Text="  submit  "/>&nbsp;&nbsp;&nbsp;<br/><br/>
					</td>
				</tr>
			</table>
        </td>
    </tr>
  </table>
</asp:Panel>