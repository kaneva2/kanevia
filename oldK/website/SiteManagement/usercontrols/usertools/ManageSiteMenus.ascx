<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ManageSiteMenus.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.ManageSiteMenus" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

  
<script type="text/JavaScript">

function CheckAll(div, checkBox) 
{
    var checkAllBox = document.getElementById(checkBox).checked;
    try
    {
	    SelectAllByParent(div, checkAllBox);
	}
	catch(e)
	{
	    alert('unable to auto select, please select your choices' );
	}
}

</script>



<asp:Panel ID="pnl_Security" runat="server" Enabled="true" style="padding-left:4px">
    

    <ajax:ajaxpanel id="ajMessage" runat="server">

    <asp:label style="position:relative; font-size:18px; padding: 10px 0 10px 0" runat="server" id="messages" visible="true" />

    <div id="availableSites" style="width:98%; font-size:12px">
        <span><hr style="width:98%" title="Available Sites" /></span>
		<asp:LinkButton id="lbn_NewSite" runat="server" causesvalidation="false" onclick="lbn_NewSite_Click">Add New Site</asp:LinkButton>
		<div style="overflow:auto; height:140px; width:100%">
        <asp:Repeater ID="rpt_AvailableSites" runat="server" EnableViewState="false">
            <HeaderTemplate>
                    <table style="width:96%" border="0">
                        <tr>
						    <th style="width:15px">
							    <input type="checkbox" id="cbxSelectAll" onclick="CheckAll('availableSites','cbxSelectAll')" class="Filter2" />
						    </th>
                            <th style="width:60px; text-align:center">Site Id</th>
                            <th style="text-align:left">Site Name</th>
                            <th style="text-align:left">Site URL</th>
                            <th style="text-align:left">Connection String</th>
                            <th style="width:114px; text-align:left"></th>
				        </tr>
            </HeaderTemplate>
            <ItemTemplate>
                        <tr>
                            <td style="width:15px">
                                 <asp:checkbox id="chkEdit" CssClass="Filter2" runat="server" Visible="true"/>
                            </td>
                            <td style="width:60px; text-align:center">
                                <asp:label id="lbl_SiteId" runat="server" visible="true" />
                            </td>
                            <td style="text-align:left">
							    <asp:TextBox id="tbx_SiteName" runat="server" Width="96%" visible="true" Enabled="false" />
                            </td>
                            <td style="text-align:left">
							    <asp:TextBox id="tbx_SiteURL" runat="server" Width="96%" visible="true" Enabled="false" />
                            </td>
                            <td style="text-align:left">
                                <asp:TextBox id="tbx_ConnectionString" runat="server" Width="96%" visible="true" Enabled="false" />
                            </td>
                             <td style="width:114px; text-align:left">
								<asp:LinkButton id="lnkEdit" Text="Edit" ToolTip="puts the row into edit mode" CommandName="cmdSiteEdit" runat="server" style="text-decoration:underline;" />
								<asp:LinkButton id="lnkUpdate" Visible="false" Text="Update" ToolTip="Updates field with your changes" CommandName="cmdSiteUpdate" runat="server" style="text-decoration:underline;" />
								<asp:LinkButton id="lnkCancel" Visible="false" Text="Cancel" ToolTip="Cancels operation" CommandName="cmdCancel" runat="server" style="text-decoration:underline;" />
                            </td>
                       </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                        <tr>
                            <td style="width:15px">
                                 <asp:checkbox id="chkEdit" CssClass="Filter2" runat="server" Visible="true"/>
                            </td>
                            <td style="width:60px; text-align:center">
                                <asp:label id="lbl_SiteId" runat="server" visible="true" />
                            </td>
                            <td style="text-align:left">
							    <asp:TextBox id="tbx_SiteName" runat="server" Width="96%" visible="true" Enabled="false" />
                            </td>
                            <td style="text-align:left">
							    <asp:TextBox id="tbx_SiteURL" runat="server" Width="96%" visible="true" Enabled="false" />
                            </td>                            
                            <td style="text-align:left">
							    <asp:TextBox id="tbx_ConnectionString" runat="server" Width="96%" visible="true" Enabled="false" />
                            </td>
                             <td style="width:114px; text-align:left">
								<asp:LinkButton id="lnkEdit" Text="Edit" ToolTip="Edit" CommandName="cmdSiteEdit" runat="server" style="text-decoration:underline;" />
 								<asp:LinkButton id="lnkUpdate" Visible="false" Text="Update" ToolTip="Updates field with your changes" CommandName="cmdSiteUpdate" runat="server" style="text-decoration:underline;" />
								<asp:LinkButton id="lnkCancel" Visible="false" Text="Cancel" ToolTip="Cancels operation" CommandName="cmdCancel" runat="server" style="text-decoration:underline;" />
                           </td>
                       </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        </div>
        <div style="padding-top:10px">
            <asp:Button ID="btn_DeleteSites" runat="server" Text="Delete Selected" onclick="btn_DeleteSites_Click"  />
        </div>
    </div>
    
    <div id="editTopMenu" style="padding-top:20px; width:98%; font-size:12px">
        <span><hr style="width:98%" title="Top Level Menu" /></span>
        <asp:DropDownList ID="ddl_SiteSelector" runat="server" Width="160px" EnableViewState="false" AutoPostBack="true" /><br /><br />
		<asp:LinkButton id="lb_NewMainMenuItem" runat="server" causesvalidation="false" onclick="lbn_NewMenuItem_Click">Add New Top Menu</asp:LinkButton>
		<div style="overflow:auto; height:140px; width:100%">
        <asp:Repeater ID="rpt_TopNavItems" runat="server" EnableViewState="false">
            <HeaderTemplate>
                    <table style="width:96%" border="0">
                        <tr>
						    <th style="width:15px">
							    <input type="checkbox" id="cbxSelectAllMenu" onclick="CheckAll('editTopMenu','cbxSelectAllMenu')" class="Filter2" />
						    </th>
                            <th style="width:60px; text-align:center">Menu Id</th>
                            <th style="width:240px; text-align:left">Menu Name</th>
                            <th style="width:240px; text-align:left">Privilege</th>
                            <th style="width:114px; text-align:left"></th>
				        </tr>
            </HeaderTemplate>
            <ItemTemplate>
                        <tr>
                            <td style="width:15px">
                                 <asp:checkbox id="chkEditMenu" CssClass="Filter2" runat="server" Visible="true"/>
                            </td>
                            <td style="width:60px; text-align:center">
                                <asp:label id="lbl_MenuId" runat="server" visible="true" />
                            </td>
                            <td style="text-align:left">
							    <asp:TextBox id="tbx_MenuName" runat="server" Width="240px" visible="true" Enabled="false" />
                            </td>
                            <td style="text-align:left">
                                <asp:DropDownList ID="ddl_PrivilegeSelector" runat="server" Width="240px" Enabled="false" AutoPostBack="false" /><br />
                            </td>
                             <td style="text-align:left">
								<asp:LinkButton id="lnkEditMenu" Text="Edit" ToolTip="puts the row into edit mode" CommandName="cmdMenuEdit" runat="server" style="text-decoration:underline;" />
								<asp:LinkButton id="lnkUpdateMenu" Visible="false" Text="Update" ToolTip="Updates field with your changes" CommandName="cmdMenuUpdate" runat="server" style="text-decoration:underline;" />
								<asp:LinkButton id="lnkCancelMenu" Visible="false" Text="Cancel" ToolTip="Cancels operation" CommandName="cmdMenuCancel" runat="server" style="text-decoration:underline;" />
                            </td>
                       </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                        <tr>
                            <td style="width:15px">
                                 <asp:checkbox id="chkEditMenu" CssClass="Filter2" runat="server" Visible="true"/>
                            </td>
                            <td style="width:60px; text-align:center">
                                <asp:label id="lbl_MenuId" runat="server" visible="true" />
                            </td>
                            <td style="text-align:left">
							    <asp:TextBox id="tbx_MenuName" runat="server" Width="240px" visible="true" Enabled="false" />
                            </td>
                            <td style="text-align:left">
                                <asp:DropDownList ID="ddl_PrivilegeSelector" runat="server" Width="240px" AutoPostBack="false" Enabled="false"/><br />
                            </td>
                             <td style="text-align:left">
								<asp:LinkButton id="lnkEditMenu" Text="Edit" ToolTip="puts the row into edit mode" CommandName="cmdMenuEdit" runat="server" style="text-decoration:underline;" />
								<asp:LinkButton id="lnkUpdateMenu" Visible="false" Text="Update" ToolTip="Updates field with your changes" CommandName="cmdMenuUpdate" runat="server" style="text-decoration:underline;" />
								<asp:LinkButton id="lnkCancelMenu" Visible="false" Text="Cancel" ToolTip="Cancels operation" CommandName="cmdMenuCancel" runat="server" style="text-decoration:underline;" />
                            </td>
                       </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        </div>
        <div style="padding-top:10px">
            <asp:Button ID="btnDeleteMenu" runat="server" Text="Delete Selected" onclick="btn_DeleteMenuItems_Click" />
        </div>		
	</div>
        
    <div id="editSubMenu" style="padding-top:20px;  width:98%; font-size:12px">
        <span><hr style="width:98%" title="Sub Menu Items" /></span>
        <asp:DropDownList ID="ddl_MainMenuSelector" runat="server" Width="160px" EnableViewState="false" AutoPostBack="true" /><br /><br />
		<asp:LinkButton id="lbn_NewSubMenu" runat="server" causesvalidation="false" onclick="lbn_NewSubMenu_Click">Add New Control</asp:LinkButton>
		<div style="overflow:auto; height:140px; width:100%">
        <asp:Repeater ID="rpt_SubMenuItems" runat="server" EnableViewState="false">
            <HeaderTemplate>
                    <table style="width:96%" border="0">
                        <tr>
						    <th style="width:15px">
							    <input type="checkbox" id="cbxSelectAllSubMenu" onclick="CheckAll('editSubMenu','cbxSelectAllSubMenu')" class="Filter2" />
						    </th>
                            <th style="width:80px; text-align:center">Sub Menu Id</th>
                            <th style="text-align:left">Sub Menu Name</th>
                            <th style="text-align:left">Privilege</th>
                            <th style="text-align:left">User Control</th>
                            <th style="width:114px; text-align:left"></th>
				        </tr>
            </HeaderTemplate>
            <ItemTemplate>
                        <tr>
                            <td style="width:15px">
                                 <asp:checkbox id="chkEditSubMenu" CssClass="Filter2" runat="server" Visible="true"/>
                            </td>
                            <td style="width:80px; text-align:center">
                                <asp:label id="lbl_SubMenuId" runat="server" visible="true" />
                            </td>
                            <td style="text-align:left">
							    <asp:TextBox id="tbx_SubMenuName" runat="server" visible="true" Enabled="false" />
                            </td>
                            <td style="text-align:left">
                                <asp:DropDownList ID="ddl_Privilege_Selector" Width="150px" runat="server" Enabled="false" AutoPostBack="false" /><br />
                            </td>
                            <td style="text-align:left">
                                <asp:DropDownList ID="ddl_UserControl" Width="170px" runat="server" Enabled="false" AutoPostBack="false" /><br />
                            </td>
                             <td style="text-align:left">
								<asp:LinkButton id="lnkEditSubMenu" Text="Edit" ToolTip="puts the row into edit mode" CommandName="cmdSubMenuEdit" runat="server" style="text-decoration:underline;" />
								<asp:LinkButton id="lnkUpdateSubMenu" Visible="false" Text="Update" ToolTip="Updates field with your changes" CommandName="cmdSubMenuUpdate" runat="server" style="text-decoration:underline;" />
								<asp:LinkButton id="lnkCancelSubMenu" Visible="false" Text="Cancel" ToolTip="Cancels operation" CommandName="cmdSubMenuCancel" runat="server" style="text-decoration:underline;" />
                            </td>
                       </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                        <tr>
                            <td style="width:15px">
                                 <asp:checkbox id="chkEditSubMenu" CssClass="Filter2" runat="server" Visible="true"/>
                            </td>
                            <td style="width:80px; text-align:center">
                                <asp:label id="lbl_SubMenuId" runat="server" visible="true" />
                            </td>
                            <td style="text-align:left">
							    <asp:TextBox id="tbx_SubMenuName" runat="server" visible="true" Enabled="false" />
                            </td>
                            <td style="text-align:left">
                                <asp:DropDownList ID="ddl_Privilege_Selector" Width="150px" runat="server" Enabled="false" AutoPostBack="false" /><br />
                            </td>
                            <td style="text-align:left">
                                <asp:DropDownList ID="ddl_UserControl" Width="170px" runat="server" Enabled="false" AutoPostBack="false" /><br />
                            </td>
                             <td style="text-align:left">
								<asp:LinkButton id="lnkEditSubMenu" Text="Edit" ToolTip="puts the row into edit mode" CommandName="cmdSubMenuEdit" runat="server" style="text-decoration:underline;" />
								<asp:LinkButton id="lnkUpdateSubMenu" Visible="false" Text="Update" ToolTip="Updates field with your changes" CommandName="cmdSubMenuUpdate" runat="server" style="text-decoration:underline;" />
								<asp:LinkButton id="lnkCancelSubMenu" Visible="false" Text="Cancel" ToolTip="Cancels operation" CommandName="cmdSubMenuCancel" runat="server" style="text-decoration:underline;" />
                            </td>
                       </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        </div>
        <div style="padding-top:10px">
            <asp:Button ID="btn_SubMenuDelete" runat="server" Text="Delete Selected" onclick="btn_DeleteSubMenuItems_Click" />
        </div>
     </div>
        
        
   
    <div class="clear"><!-- clear float --></div>
    </ajax:ajaxpanel>
    
    </asp:Panel>     