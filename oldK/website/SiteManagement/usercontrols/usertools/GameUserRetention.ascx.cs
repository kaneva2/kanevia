using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;
using Kaneva.BusinessLayer.Facade;

using KlausEnt.KEP.Kaneva;
using KlausEnt.KEP.Kaneva.framework.utils;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class GameUserRetention : BaseUserControl
    {
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private bool isAdministrator = false;
        private bool canEdit = false;
        private int ownerId = 0;
        private int pageSize = 10;
        private int pageNum = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                switch (CheckUserAccess((int)SitePrivilege.ePRIVILEGE.STAR_ADMIN))
                {
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        isAdministrator = true;
                        canEdit = true;
                        break;
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    default:
                        break;
                }

                if ((Session["SelectedGameId"] != null) && Convert.ToInt32(Session["SelectedGameId"]) > 0)
                {
                    // Load the current user's data the first time
                    SELECTED_GAME_ID = Convert.ToInt32(Session["SelectedGameId"]);

                    //get the company id the game is associated with
                    this.ownerId = GetGameFacade().GetGameOwner(SELECTED_GAME_ID);

                    if (!isAdministrator)
                    {
                        canEdit = this.ownerId.Equals(GetUserId());
                    }

                    if (!canEdit)
                    {
                        RedirectToHomePage();
                    }
                    else
                    {
                        if (!IsPostBack || !IsPostBackKaneva)
                        {
                            BindExperiments();

                            IsPostBackKaneva = true;
                        }
                    }
                }
            }
        }

        protected string GetFormattedOutput(string output, string fieldName, int currentIndex)
        {
            if (fieldName == "seven_day_retention" && currentIndex == 0)
            {
                return "-----";
            }
            else if (fieldName == "thirty_day_retention" && (currentIndex == 0 || currentIndex == 1 || currentIndex == 2 || currentIndex == 3))
            {
                return "-----";
            }
            else
            {
                return output;
            }
        }

        private void BindExperiments()
        {
            try
            {
                DateTime endDate = DateTime.Now;
                DateTime startDate = DateTime.Now.AddDays((-(7 * 8) - (int)endDate.DayOfWeek)); // include current week and two months worth of data

                DataTable data = ReportUtility.GetuserRetentionData(SELECTED_GAME_ID, startDate, endDate);

                rptUserRetention.DataSource = data;
                rptUserRetention.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
                m_logger.Error("Error loading user retention data.", ex);
            }
        }

        private void ShowMessage(string msg)
        {
            divErrorTop.InnerText = msg;
            divErrorTop.Style.Add("display", "block");
        }

        private void GetRequestValues()
        {
            // Get items per page count
            if (Request["pagesize"] != null)
            {
                try
                {
                    PageSize = Convert.ToInt32(Request["pagesize"]);
                }
                catch { }
            }

            // Get the current page number
            if (Request["p"] != null)
            {
                try
                {
                    PageNum = Convert.ToInt32(Request["p"]);
                }
                catch { }
            }
        }

        private int PageNum
        {
            set { this.pageNum = value; }
            get { return this.pageNum; }
        }
        private int PageSize
        {
            set { this.pageSize = value; }
            get { return this.pageSize; }
        }

        private int SELECTED_GAME_ID
        {
            get { return (int)ViewState["game_id"]; }
            set { ViewState["game_id"] = value; }
        }

        private bool IsPostBackKaneva
        {
            get
            {
                if (ViewState["ispostback"] == null)
                {
                    return false;
                }

                return (bool)ViewState["ispostback"];
            }
            set { ViewState["ispostback"] = value; }
        }

    }
}