<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="TopNav.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.TopNav" %>

<div id="mainmenubar">
    <div id="siteselect">
        <div id="ManagementLogo"></div>
        <div style="padding-left:20px;">
        <asp:DropDownList ID="ddl_SiteSelector" runat="server" Width="160px" AutoPostBack="true" /></div>
    </div>
    <div id="securityLinks">
        <asp:linkbutton id="lnkLogout" onclick="lnkLogout_Click" runat="server" text="Sign Out" causesvalidation="false"></asp:linkbutton><a runat="server" id="aSignIn">Sign In</a>
    </div>
    <div id="menu_container">
        <asp:DataList ID="dl_MainMenu" runat="server" RepeatColumns="8" RepeatDirection="Horizontal" RepeatLayout="Flow">
            <HeaderTemplate>
                <table border="0" cellspacing="0">
                    <tr>
            </HeaderTemplate>
            <ItemTemplate>
                        <td align="left" >
                            <asp:LinkButton ID="lb_menuItem" runat="server" OnCommand="Nav_Command" CausesValidation="false" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "NavigationId") %>' ><%# DataBinder.Eval(Container.DataItem, "NavigationTitle")%></asp:LinkButton>
                        </td>
            </ItemTemplate>
            <FooterTemplate>
                    </tr>
                </table>
            </FooterTemplate>
        </asp:DataList>		
    </div>
</div>