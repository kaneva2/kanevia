<%@ Control Language="C#" AutoEventWireup="true" Codebehind="EmailTemplates.ascx.cs"
	Inherits="SiteManagement.usercontrols.mail.EmailTemplates" %>
<!-- Template List -->
<div id="dvTemplates" runat="server">
	<div id="dvFilters" runat="server" visible="false">
		Filters<br />
		<asp:CheckBox ID="cbShowAllVersions" runat="server" Checked="false" Text="Show All Versions" />
		<asp:CheckBox ID="cbShowDisabled" runat="server" Checked="false" Text="Show Disabled" />
	</div>
	<p>
		<asp:LinkButton ID="lbNewTemplate" OnClick="lbNewTemplate_Click" runat="server" Text="New Template"></asp:LinkButton>

	</p>
	<asp:Label ID="txtGVStatus" runat="server"></asp:Label>
	<asp:Literal ID="litHeaderEnabledTemplates" runat="server"><p>Enabled</p></asp:Literal>
	<asp:GridView ID="gvTemplates" runat="server" AutoGenerateColumns="false" OnRowDeleting="gvTemplate_Delete"
		OnRowEditing="gvTemplate_Edit">
		<RowStyle BackColor="White"></RowStyle>
		<HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000"
			HorizontalAlign="Left"></HeaderStyle>
		<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
		<FooterStyle BackColor="#FFffff"></FooterStyle>
		<Columns>
			<asp:TemplateField>
				<ItemTemplate>
					<asp:LinkButton ID="lbnConfirmDelete" runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you want to delete this template?');"
						CommandName="Delete">Delete</asp:LinkButton>
					<asp:LinkButton ID="lbnEdit" runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="email_template_id" HeaderText="Id" />
			<asp:BoundField DataField="type_name" HeaderText="Type" />
			<asp:BoundField DataField="template_version" HeaderText="Ver" />
			<asp:BoundField DataField="template_subversion" HeaderText="SubVer" />
			<asp:BoundField DataField="template_name" HeaderText="Name" />
			<asp:BoundField DataField="email_subject" HeaderText="Subject" />
			<asp:TemplateField HeaderText="Enabled">
				<ItemTemplate>
					<%#Convert.ToInt32(DataBinder.Eval(Container.DataItem, "enabled")).Equals(1)%>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="rotation_weight" HeaderText="Weight" />
			<asp:BoundField DataField="date_created" HeaderText="Created" DataFormatString="{0:d}" />
		</Columns>
	</asp:GridView>
	<asp:Literal ID="litHeaderDisabledTemplates" runat="server"><p>Disabled</p></asp:Literal>
	<asp:GridView ID="gvTemplatesDisabled" runat="server" AutoGenerateColumns="false"
		OnRowDeleting="gvTemplateDisabled_Delete" OnRowEditing="gvTemplateDisabled_Edit">
		<RowStyle BackColor="White"></RowStyle>
		<HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000"
			HorizontalAlign="Left"></HeaderStyle>
		<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
		<FooterStyle BackColor="#FFffff"></FooterStyle>
		<Columns>
			<asp:TemplateField>
				<ItemTemplate>
					<asp:LinkButton ID="lbnConfirmDelete" runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you want to delete this template?');"
						CommandName="Delete">Delete</asp:LinkButton>
					<asp:LinkButton ID="lbnEdit" runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="email_template_id" HeaderText="Id" />
			<asp:BoundField DataField="type_name" HeaderText="Type" />
			<asp:BoundField DataField="template_version" HeaderText="Ver" />
			<asp:BoundField DataField="template_subversion" HeaderText="SubVer" />
			<asp:BoundField DataField="template_name" HeaderText="Name" />
			<asp:BoundField DataField="email_subject" HeaderText="Subject" />
			<asp:TemplateField HeaderText="Enabled">
				<ItemTemplate>
					<%#Convert.ToInt32(DataBinder.Eval(Container.DataItem, "enabled")).Equals(1)%>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="rotation_weight" HeaderText="Weight" />
			<asp:BoundField DataField="date_created" HeaderText="Created" DataFormatString="{0:d}" />
		</Columns>
	</asp:GridView>
</div>
<!-- Edit Templates -->
<div id="dvTemplateEdit" runat="server">
	<table>
		<tr>
			<td align="right" colspan="2">
				<asp:Button ID="Button1" Text="Save New Version" OnClick="btnTemplateSave_Click"
					OnClientClick="return confirm('Save as new version?');" CausesValidation="false"
					runat="server" />
				<asp:Button ID="Button2" Text="Update This Version" OnClick="btnTemplateUpdate_Click"
					OnClientClick="return confirm('Update this template, increment subversion?');"
					CausesValidation="false" runat="server" />
				<asp:Button ID="Button3" Text="Cancel" OnClick="btnTemplateCancel_Click" runat="server" /><br />
				<asp:Label ID="Label1" runat="server"></asp:Label>
			</td>
		</tr>
		<tr>
			<td>
				<table>
					<tr>
						<td>
							<p style="text-align: right;">
								Id:</p>
						</td>
						<td>
							<asp:TextBox ID="txtId" runat="server"></asp:TextBox></td>
					</tr>
					<tr>
						<td>
							<p style="text-align: right;">
								Type:</p>
						</td>
						<td>
							<asp:DropDownList ID="ddlTypes" runat="server" DataTextField="type_name" DataValueField="email_type_id">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td>
							<p style="text-align: right;">
								Name:</p>
						</td>
						<td>
							<asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
					</tr>
					<tr>
						<td>
							<p style="text-align: right;">
								Version:</p>
						</td>
						<td>
							<asp:TextBox ID="txtVersion" runat="server" Enabled="false"></asp:TextBox></td>
					</tr>
					<tr>
						<td>
							<p style="text-align: right;">
								Sub Version:</p>
						</td>
						<td>
							<asp:TextBox ID="txtSubVersion" runat="server" Enabled="false"></asp:TextBox></td>
					</tr>
					<tr>
						<td>
							<p style="text-align: right;">
								From:</p>
						</td>
						<td>
							<asp:TextBox ID="txtFrom" runat="server"></asp:TextBox></td>
					</tr>
				</table>
			</td>
			<td>
				<table>
					<tr>
						<td>
							<p style="text-align: right;">
								Language:</p>
						</td>
						<td>
							<asp:TextBox ID="txtLanguage" runat="server">en</asp:TextBox></td>
					</tr>
					<tr>
						<td>
							<p style="text-align: right;">
								Enabled:</p>
						</td>
						<td>
							<asp:CheckBox ID="cbEnabled" runat="server" /></td>
					</tr>
					<tr>
						<td>
							<p style="text-align: right;">
								Use Header:</p>
						</td>
						<td>
							<asp:DropDownList ID="ddlHeaders" runat="server" DataTextField="template_name" DataValueField="email_template_id">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td>
							<p style="text-align: right;">
								Use Footer:</p>
						</td>
						<td>
							<asp:DropDownList ID="ddlFooters" runat="server" DataTextField="template_name" DataValueField="email_template_id">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td>
							<p style="text-align: right;">
								Rotation Weight:</p>
						</td>
						<td>
							<asp:TextBox ID="txtRotWeight" runat="server"></asp:TextBox></td>
					</tr>
                    <tr>
						<td>
							<p style="text-align: right;">
								From Display Name:</p>
						</td>
						<td>
							<asp:TextBox ID="txtFromDisplayName" runat="server"></asp:TextBox></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table>
		<tr>
			<td>
				<p style="text-align: right;">
					Subject:</p>
			</td>
			<td>
				<asp:TextBox ID="txtSubject" runat="server" Columns="50"></asp:TextBox></td>
		</tr>
		<tr>
			<td>
				<p style="text-align: right;">
					Text:</p>
			</td>
			<td>
				<asp:TextBox ID="txtText" runat="server" TextMode="MultiLine" Rows="10" Columns="50"></asp:TextBox></td>
		</tr>
		<tr>
			<td>
				<p style="text-align: right;">
					HTML:</p>
			</td>
			<td>
				<asp:TextBox ID="txtHtml" runat="server" TextMode="MultiLine" Rows="10" Columns="50"></asp:TextBox></td>
		</tr>
		<tr>
			<td>
				<p style="text-align: right;">
					Description:</p>
			</td>
			<td>
				<asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Rows="2" Columns="50"></asp:TextBox></td>
		</tr>
		<tr>
			<td>
				<p style="text-align: right;">
					Comments:</p>
			</td>
			<td>
				<asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Rows="2" Columns="50"></asp:TextBox></td>
		</tr>
		<!--
		<tr>
			<td>
			</td>
			<td align="right">
				<asp:Button ID="Button4" Text="Save New Version" OnClick="btnTemplateSave_Click"
					OnClientClick="return confirm('Save as new version?');" CausesValidation="false"
					runat="server" />
				<asp:Button ID="Button5" Text="Update This Version" OnClick="btnTemplateUpdate_Click"
					OnClientClick="return confirm('Update this template, increment subversion?');"
					CausesValidation="false" runat="server" />
				<asp:Button ID="Button6" Text="Cancel" OnClick="btnTemplateCancel_Click" runat="server" /><br />
				<asp:Label ID="lblSaveStatus" runat="server"></asp:Label>
			</td>
		</tr>
		-->
		
	</table>
</div>

<!-- Template Preview -->
<div id="dvTemplatePreview" runat="server">
	<iframe id="ifPreview" runat="server" width="700" height="500"></iframe>
</div>

<!-- Table to show the different types -->
<div id="dvTypeHelper" runat="server">
</div>
