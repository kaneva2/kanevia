///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.Mailer;

namespace SiteManagement.usercontrols.mail
{
	public partial class EmailTemplates : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				ToggleDivVisibility("dvTemplates");
				BindData();
			}

		}

		protected void BindData()
		{
			if (Request["type"] != null)
			{
				int emailTypeId = Convert.ToInt32(Request["type"]);
				DataTable dt = EmailTemplateUtility.GetTemplatesByTypeId(emailTypeId, 1);
				gvTemplates.DataSource = dt;
				gvTemplates.DataBind();
				if (gvTemplates.Rows.Count.Equals(0)) litHeaderEnabledTemplates.Visible = false;

				DataTable dtDisabled = EmailTemplateUtility.GetTemplatesByTypeId(emailTypeId, 0);
				gvTemplatesDisabled.DataSource = dtDisabled;
				gvTemplatesDisabled.DataBind();
				if (gvTemplatesDisabled.Rows.Count.Equals(0)) litHeaderDisabledTemplates.Visible = false;

			}
			else
			{
				DataTable dt = EmailTemplateUtility.GetAllTemplates(1);
				gvTemplates.DataSource = dt;
				gvTemplates.DataBind();
				if (gvTemplates.Rows.Count.Equals(0)) litHeaderEnabledTemplates.Visible = false;

				DataTable dtDisabled = EmailTemplateUtility.GetAllTemplates(0);
				gvTemplatesDisabled.DataSource = dtDisabled;
				gvTemplatesDisabled.DataBind();
				if (gvTemplatesDisabled.Rows.Count.Equals(0)) litHeaderDisabledTemplates.Visible = false;
			}

			//DataTable dt = EmailTemplateUtility.GetTemplates(); 

			//get email types for edit dropdown
			DataTable dtTypes = EmailTypeUtility.GetEmailTypes();
			ddlTypes.DataSource = dtTypes;
			ddlTypes.DataBind();
			ddlTypes.Items.Insert(0, new ListItem("----", "0"));


			// load headers
			DataTable dtHeaders = EmailTemplateUtility.GetTemplatesByTypeName("Header", 1);
			ddlHeaders.DataSource = dtHeaders;
			ddlHeaders.DataBind();
			if (ddlHeaders.Items.Count.Equals(0)) ddlHeaders.Items.Insert(0, new ListItem("--No Valid Headers--", "0"));


			// load footers
			DataTable dtFooters = EmailTemplateUtility.GetTemplatesByTypeName("Footer", 1);
			ddlFooters.DataSource = dtFooters;
			ddlFooters.DataBind();
			if (ddlFooters.Items.Count.Equals(0)) ddlFooters.Items.Insert(0, new ListItem("--No Valid Footers--", "0"));


			//Get email types for helper
			//gvEmailTypes.DataSource = dtTypes;
			//gvEmailTypes.DataBind();

			ToggleDivVisibility("dvTemplates");
		}

		protected void chkHideDisabled_Change(object sender, EventArgs e)
		{
			BindData();
		}

		protected void lbNewTemplate_Click(object sender, EventArgs e)
		{
			ResetFields();
			ToggleDivVisibility("dvTemplateEdit");

		}

		protected void lbAllTemplates_Click(object sender, EventArgs e)
		{
			Response.Redirect("Templates.aspx");
		}

		protected void gvTemplate_Delete(object sender, GridViewDeleteEventArgs e)
		{
			GridViewRow gvr = gvTemplates.Rows[e.RowIndex];
			templateDelete(Convert.ToInt32(gvr.Cells[1].Text));
		}

		protected void gvTemplateDisabled_Delete(object sender, GridViewDeleteEventArgs e)
		{
			GridViewRow gvr = gvTemplatesDisabled.Rows[e.RowIndex];
			templateDelete(Convert.ToInt32(gvr.Cells[1].Text));
		}

		protected void templateDelete(int tmplId)
		{
			try
			{
				EmailTemplateUtility.DeleteTemplate(tmplId);
			}
			catch (Exception ex)
			{
				txtGVStatus.Text = "Delete failed: " + ex + ".";
			}
			finally
			{
				//txtGVStatus.Text = "Template Deleted.";
				BindData();
			}
		}

		protected void gvTemplate_Edit(object sender, GridViewEditEventArgs e)
		{
			GridViewRow gvr = gvTemplates.Rows[e.NewEditIndex];
			loadEdit(Convert.ToInt32(gvr.Cells[1].Text));
		}

		protected void gvTemplateDisabled_Edit(object sender, GridViewEditEventArgs e)
		{
			GridViewRow gvr = gvTemplatesDisabled.Rows[e.NewEditIndex];
			loadEdit(Convert.ToInt32(gvr.Cells[1].Text));
		}

		protected void loadEdit(int tmplId)
		{
			ToggleDivVisibility("dvTemplateEdit");

			DataRow drTemplate = EmailTemplateUtility.GetTemplateById(tmplId);

			txtId.Text = drTemplate["email_template_id"].ToString();

			try
			{
				ddlTypes.SelectedValue = drTemplate["email_type_id"].ToString();
			}
			catch
			{
			}

			txtName.Text = drTemplate["template_name"].ToString();
			txtVersion.Text = drTemplate["template_version"].ToString();
			txtSubVersion.Text = drTemplate["template_subversion"].ToString();
			txtFrom.Text = drTemplate["reply_address"].ToString();
            txtFromDisplayName.Text = Convert.ToString(drTemplate["reply_address_display_name"]);
			txtText.Text = drTemplate["template_text"].ToString();
			txtHtml.Text = drTemplate["template_html"].ToString();
			txtSubject.Text = drTemplate["email_subject"].ToString();
			txtLanguage.Text = drTemplate["language_code"].ToString();

			try
			{
				ddlHeaders.SelectedValue = drTemplate["header_template_id"].ToString();
				ddlFooters.SelectedValue = drTemplate["footer_template_id"].ToString();
			}
			catch
			{
				lblSaveStatus.Text = "Error selecting header/footer.";
			}

			txtRotWeight.Text = drTemplate["rotation_weight"].ToString();
			txtDescription.Text = drTemplate["description"].ToString();
			txtComments.Text = drTemplate["comments"].ToString();

			cbEnabled.Checked = drTemplate["enabled"].ToString().Equals("1");

			ifPreview.Attributes.Add("src", ResolveUrl("~/usercontrols/mail/EmailTemplatePreview.aspx?tmplId=" + tmplId));
		}

		protected void btnTemplateSave_Click(object sender, EventArgs e)
		{


			int intEnabled = 0; // New templates default to off

			int typeId = Convert.ToInt32(ddlTypes.SelectedValue);

			int intVersion = EmailTemplateUtility.GetNextTemplateVersion(typeId);

			int intSubVersion = 1;

			try
			{
				if (EmailTemplateUtility.InsertEmailTemplate(typeId,
				  txtName.Text, intVersion, intSubVersion, txtFrom.Text, txtFromDisplayName.Text, txtText.Text, txtHtml.Text, txtSubject.Text,
				  txtLanguage.Text, intEnabled, Convert.ToInt32(ddlFooters.SelectedValue), Convert.ToInt32(ddlHeaders.SelectedValue),
				  Convert.ToInt32(txtRotWeight.Text), txtDescription.Text, txtComments.Text) != 0)
				{
					//BindData();
					lblSaveStatus.Text = "Template Saved.";
				}
				else
				{
					lblSaveStatus.Text = "Error saving template.";
				}
			}
			catch (Exception exc)
			{
				lblSaveStatus.Text = "Error saving template: " + exc;
			}
		}

		protected void btnTemplateUpdate_Click(object sender, EventArgs e)
		{
			int intEnabled = 0;

			if (cbEnabled.Checked)
				intEnabled = 1;


			int intSubVersion = 1;
			if (txtSubVersion.Text != "")
			{
				intSubVersion = Convert.ToInt32(txtSubVersion.Text) + 1;
			}

			try
			{
				if (EmailTemplateUtility.UpdateEmailTemplate(Convert.ToInt32(txtId.Text), Convert.ToInt32(ddlTypes.SelectedValue),
				  txtName.Text, Convert.ToInt32(txtVersion.Text), intSubVersion, txtFrom.Text, txtFromDisplayName.Text, txtText.Text,
				  txtHtml.Text, txtSubject.Text, txtLanguage.Text, intEnabled, Convert.ToInt32(ddlFooters.SelectedValue),
				  Convert.ToInt32(ddlHeaders.SelectedValue), Convert.ToInt32(txtRotWeight.Text), txtDescription.Text, txtComments.Text) != 0)
				{
					BindData();
					//lblSaveStatus.Text = "Template updated.";
				}
				else
				{
					//lblSaveStatus.Text = "Error updating template.";
				}
			}
			catch (Exception exc)
			{
				lblSaveStatus.Text = "Error updating template: " + exc;
			}
		}

		protected void btnTemplateCancel_Click(object sender, EventArgs e)
		{
			ResetFields();
			BindData();
		}

		protected void ResetFields()
		{
			txtId.Text = "";
			txtName.Text = "";
			txtVersion.Text = "";
			txtSubVersion.Text = "";
			txtFrom.Text = "";
            txtFromDisplayName.Text = "";
			txtText.Text = "";
			txtHtml.Text = "";
			txtSubject.Text = "";
			txtLanguage.Text = "en";
			ddlHeaders.SelectedIndex = 0;
			ddlFooters.SelectedIndex = 0;
			txtRotWeight.Text = "10";
			txtDescription.Text = "";
			txtComments.Text = "";
			cbEnabled.Checked = false;
			ifPreview.Attributes.Clear();
		}

		protected void ToggleDivVisibility(string divName)
		{
			dvTemplates.Visible = false;
			dvTemplateEdit.Visible = false;
			dvTypeHelper.Visible = false;
			dvTemplatePreview.Visible = false;

			switch (divName)
			{
				case "dvTemplateEdit":
					dvTemplateEdit.Visible = true;
					dvTemplatePreview.Visible = true;
					break;

				case "dvTemplates":
					dvTemplates.Visible = true;
					dvTypeHelper.Visible = true;
					break;
			}
		}

	}
}