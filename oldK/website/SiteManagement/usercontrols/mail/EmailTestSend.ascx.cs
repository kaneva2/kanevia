///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.Mailer;
using Kaneva;

namespace SiteManagement.usercontrols.mail
{
	public partial class EmailTestSend : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void btnSend_Click(object sender, EventArgs e)
		{
			KlausEnt.KEP.Kaneva.MailUtility.SendEmail(txtEmailFrom.Text, txtEmailTo.Text, txtSubject.Text, txtBody.Text, Convert.ToBoolean(txtIsHtml.Text),
			  Convert.ToBoolean(txtCC.Text), Convert.ToInt16(txtTier.Text));

			UpdateStatus("Email Sent.");
		}

		protected void UpdateStatus(string message)
		{
			lblStatus.Text = lblStatus.Text.Insert(0, DateTime.Now.ToString() + " " + message + "<br/>");
		}

	}
}