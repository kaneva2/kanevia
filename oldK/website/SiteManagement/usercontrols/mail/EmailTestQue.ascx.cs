///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.Mailer;
using Kaneva;

namespace SiteManagement.usercontrols.mail
{
	public partial class EmailTestQue : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void btnQueEmail_click(object sender, EventArgs e)
		{
			try
			{
				int typeId = Convert.ToInt16(txtTypeIdQE.Text);
				int emailTemplateId = Convert.ToInt16(txtEmailTemplateIdQE.Text);
				int userId = Convert.ToInt16(txtUserIdQE.Text);
				int inviteId = Convert.ToInt16(txtInviteIdQE.Text);

				Queuer.QueueEmail(typeId, emailTemplateId, userId, txtToEmailQE.Text, txtFromEmailQE.Text, inviteId);
				UpdateStatus("Que email_template_id:" + emailTemplateId);
			}
			catch
			{
				UpdateStatus("Failure: Que email_template_id");
			}
		}

		protected void btnChew_click(object sender, EventArgs e)
		{
			Chewer.Chew();
			UpdateStatus("Run Chewer");
		}

		protected void UpdateStatus(string message)
		{
			lblStatus.Text = lblStatus.Text.Insert(0, DateTime.Now.ToString() + " " + message + "<br/>");
		}

	}
}