<%@ Control Language="C#" AutoEventWireup="true" Codebehind="EmailTestQue.ascx.cs"
	Inherits="SiteManagement.usercontrols.mail.EmailTestQue" %>
<div id="dvQueEmail" runat="server">
	<h3>
		Que Email</h3>
	<p>
		Queuer.QueueEmail(typeId, email_template_id, userId, toEmail, fromEmail, inviteID);</p>
	<table>
		<tr>
			<td>
				typeId</td>
			<td>
				<asp:TextBox ID="txtTypeIdQE" runat="server"></asp:TextBox></td>
		</tr>
		<tr>
			<td>
				email_template_id</td>
			<td>
				<asp:TextBox ID="txtEmailTemplateIdQE" runat="server"></asp:TextBox></td>
		</tr>
		<tr>
			<td>
				userId</td>
			<td>
				<asp:TextBox ID="txtUserIdQE" runat="server"></asp:TextBox></td>
		</tr>
		<tr>
			<td>
				toEmail</td>
			<td>
				<asp:TextBox ID="txtToEmailQE" runat="server"></asp:TextBox></td>
		</tr>
		<tr>
			<td>
				fromEmail</td>
			<td>
				<asp:TextBox ID="txtFromEmailQE" runat="server"></asp:TextBox></td>
		</tr>
		<tr>
			<td>
				inviteID</td>
			<td>
				<asp:TextBox ID="txtInviteIdQE" runat="server"></asp:TextBox></td>
		</tr>
	</table>
	<asp:Button ID="btnQueEmail" Text="QueueEmail" OnClick="btnQueEmail_click" runat="server" />

</div>
<div id="dvRunChewer" runat="server">
	<h3>
		Run Chewer</h3>
	<p>
		Chewer.Chew()</p>
	<table>
		<tr>
			<td>
			</td>
			<td>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
			</td>
		</tr>
	</table>
	<asp:Button ID="btnChew" Text="Chew" OnClick="btnChew_click" runat="server" />
</div>
<div id="dvRside">
	Status:<br />
	<asp:Label ID="lblStatus" runat="server"></asp:Label>
</div>
