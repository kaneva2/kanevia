///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.Mailer;

namespace SiteManagement.usercontrols.mail
{
	public partial class EmailTemplatePreview : System.Web.UI.Page
    {
        #region Declarations;
        private string templatePreviewHTML;
        #endregion;

        protected void Page_Load(object sender, EventArgs e)
		{
            DataRow drTemplate = null;
            DataRow drHeader = null;
            DataRow drFooter = null;

            try
            {
                // Get the template record
                int templateID = Convert.ToInt32(Request.Params["tmplId"]);
                drTemplate = EmailTemplateUtility.GetTemplateById(templateID);

                // Get the header and footer records
                drHeader = EmailTemplateUtility.GetTemplateById(Convert.ToInt32(drTemplate["header_template_id"]));
                drFooter = EmailTemplateUtility.GetTemplateById(Convert.ToInt32(drTemplate["footer_template_id"]));

                // Build the template preview HTML
                templatePreviewHTML = drTemplate["template_html"].ToString();
                templatePreviewHTML = templatePreviewHTML.Replace("#header#", drHeader["template_html"].ToString());
                templatePreviewHTML = templatePreviewHTML.Replace("#footer#", drFooter["template_html"].ToString());
            }
            catch
            {
                // Write error messages if selection fails
                if(drTemplate == null)
                    templatePreviewHTML = "Error selecting template HTML.";
                else if(drHeader == null || drFooter == null)
                    templatePreviewHTML = "Error selecting header/footer.";
            }
		}
        
        protected override void Render(HtmlTextWriter writer)
        {
            // Render the template preview HTML as the page contents
            writer.Write(templatePreviewHTML);
            base.Render(writer);
        }
	}
}
