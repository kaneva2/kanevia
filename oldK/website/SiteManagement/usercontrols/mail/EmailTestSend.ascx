<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailTestSend.ascx.cs" Inherits="SiteManagement.usercontrols.mail.EmailTestSend" %>
      <div id="dvSendTemplate" runat="server">  
        <h3>Send Email</h3>                    
        <p>MailUtility.SendEmail(emailFrom, emailTo, subject, body, ishtml, cc, tier);</p>
        
        <table>        
        <tr><td>emailFrom</td><td><asp:TextBox ID="txtEmailFrom" runat="server">kaneva@kaneva.com</asp:TextBox></td></tr> 
        <tr><td>emailTo</td><td><asp:TextBox ID="txtEmailTo" runat="server">cmullins@kaneva.com</asp:TextBox></td></tr>
        <tr><td>subject</td><td><asp:TextBox ID="txtSubject" runat="server">subject</asp:TextBox></td></tr>
        <tr><td>body</td><td><asp:TextBox ID="txtBody" runat="server">body</asp:TextBox></td></tr>
        <tr><td>ishtml</td><td><asp:TextBox ID="txtIsHtml" runat="server">true</asp:TextBox></td></tr>
        <tr><td>cc</td><td><asp:TextBox ID="txtCC" runat="server">false</asp:TextBox></td></tr>
        <tr><td>tier</td><td><asp:TextBox ID="txtTier" runat="server">4</asp:TextBox></td></tr>
        </table>                     
        <asp:Button ID="btnSend" Text="SendEmail" OnClick="btnSend_Click" runat="server" />                               
      </div>
      
           <div id="dvRside">Status:<br />
      <asp:label ID="lblStatus" runat="server"></asp:label>    
    </div>