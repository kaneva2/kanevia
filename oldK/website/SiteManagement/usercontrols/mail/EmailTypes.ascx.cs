///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.Mailer;

namespace SiteManagement.usercontrols.mail
{
	public partial class EmailTypes : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				BindData();
			}
		}

		protected void BindData()
		{
			DataTable dt = EmailTypeUtility.GetEmailTypes();
			gvEmailTypes.DataSource = dt;
			gvEmailTypes.DataBind();

			dvTypeEdit.Visible = false;
			dvTypes.Visible = true;

			ResetFields();
		}

		protected void btnNewType_Click(object sender, EventArgs e)
		{
			dvTypeEdit.Visible = true;
			dvTypes.Visible = false;
			ResetFields();
		}

		protected void btnListTypes_Click(object sender, EventArgs e)
		{
			dvTypeEdit.Visible = false;
			dvTypes.Visible = true;
		}

		protected void btnCancelEdit_Click(object sender, EventArgs e)
		{
			dvTypeEdit.Visible = false;
			dvTypes.Visible = true;
			ResetFields();
		}

		protected void btnSaveType_Click(object sender, EventArgs e)
		{
			string typeId = txtTypeId.Text;
			string name = txtTypeName.Text;
			string description = txtDescription.Text;
			int tier = Convert.ToInt32(ddlTier.SelectedValue);
			int enabled = 0;
			if (ckEnabled.Checked)
			enabled = 1;

			int intervalQue = Convert.ToInt32(txtIntervalQue.Text);
			int intervalChew = Convert.ToInt32(txtIntervalChew.Text);
			string query = txtTypeQuery.Text;

			if (typeId == "")
			{
				if (EmailTypeUtility.InsertEmailType(name, description, enabled, tier, intervalQue, intervalChew, query) != 0)
			{
				BindData();
				//txtSaveStatus.Text = "Type Saved.";
			}
			else
			{
				txtSaveStatus.Text = "Error saving type.";
			}
			}
			else
			{
				if (EmailTypeUtility.UpdateEmailType(Convert.ToInt32(typeId), name, description, enabled, tier, intervalQue, intervalChew, query) != 0)
				{
					BindData();
					//txtSaveStatus.Text = "Type Saved.";
				}
					else
				{
					txtSaveStatus.Text = "Error updating type.";
				}
			}
		}

		protected void gvEmailTypes_Delete(object sender, GridViewDeleteEventArgs e)
		{
			GridViewRow gvr = gvEmailTypes.Rows[e.RowIndex];
			try
			{
				EmailTypeUtility.DeleteEmailType(Convert.ToInt32(gvr.Cells[1].Text));
			}
			catch(Exception ex)
			{
				txtGVStatus.Text = "Delete failed: " + ex + "."; 
			}
			finally
			{
				//txtGVStatus.Text = "Type Deleted.";
				BindData();
			}
		}

		protected void gvEmailTypes_Edit(object sender, GridViewEditEventArgs e)
		{
			dvTypeEdit.Visible = true;
			dvTypes.Visible = false;

			GridViewRow gvr = gvEmailTypes.Rows[e.NewEditIndex];
			//txtGVStatus.Text = "Editing. " + e.NewEditIndex + ", " + gvr.Cells[1].Text;

			DataRow drEmailType = EmailTypeUtility.GetEmailType(Convert.ToInt32(gvr.Cells[1].Text));

			string typeId = drEmailType["email_type_id"].ToString();
			txtTypeId.Text = typeId ;
			txtTypeName.Text = drEmailType["type_name"].ToString();
			txtDescription.Text = drEmailType["description"].ToString();
			ddlTier.SelectedValue = drEmailType["tier"].ToString();
			ckEnabled.Checked = drEmailType["enabled"].ToString().Equals("1");
			txtIntervalChew.Text = drEmailType["interval_chew"].ToString();
			txtIntervalQue.Text = drEmailType["interval_que"].ToString();
			txtTypeQuery.Text = drEmailType["list_query"].ToString();

		}

		protected void ResetFields()
		{
			txtTypeId.Text = "";
			txtTypeName.Text = "";
			txtDescription.Text = "";
			ddlTier.SelectedValue = "1";
			ckEnabled.Checked = false;
			txtIntervalChew.Text = "";
			txtIntervalQue.Text = "";
			txtTypeQuery.Text = "";
		}

		/// <summary>
		/// GetTemplatePath
		/// </summary>
		protected string GetTemplatePath(string emailTypeId)
		{
			//return "templates.aspx?type=";			
			return "Default.aspx?ws="+ Request.Params["ws"].ToString() + "&mn=" + Request.Params["mn"].ToString() + 
				"&sn=46&type=" + emailTypeId;
		}
	}
}