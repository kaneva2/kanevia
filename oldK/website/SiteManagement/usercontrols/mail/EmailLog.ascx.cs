///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace SiteManagement.usercontrols.mail
{
	public partial class EmailLog : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{

				BindData();
			}
		}

		protected void BindData()
		{
			string sqlSelect = "SELECT * FROM kes_log ORDER BY kes_log_id DESC LIMIT 40";

			DataTable dt = KlausEnt.KEP.Kaneva.KanevaGlobals.GetDatabaseUtilityEmailViral().GetDataTable(sqlSelect);
			gvLog.DataSource = dt;
			gvLog.DataBind();
		}
	}
}