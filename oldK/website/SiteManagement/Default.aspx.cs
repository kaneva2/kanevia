///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class ManagementDefault : BasePage
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                //get parameters
                GetRequestParams();
            }

            // Extend the timeout for this page
            this.AddKeepAlive();

            //load the usercontrol - on every post back 
            LoadControl();
        }

        #region Attributes

        protected string ACTIVE_CONTROL
        {
            get
            {

                if (ViewState["activeControl"] == null)
                {
                    ViewState["activeControl"] = "";
                }

                return ViewState["activeControl"].ToString();
            }
            set
            {
                ViewState["activeControl"] = value;
            }
        }

        protected int ACTIVE_CONTROL_TYPE
        {
            get
            {

                if (ViewState["activeControlType"] == null)
                {
                    ViewState["activeControlType"] = 0;
                }

                return (int)ViewState["activeControlType"];
            }
            set
            {
                ViewState["activeControlType"] = value;
            }
        }

        protected int SELECTED_CONTROL_ID
        {
            get
            {

                if (ViewState["_selectedUserControlId"] == null)
                {
                    ViewState["_selectedUserControlId"] = 0;
                }

                return (int)ViewState["_selectedUserControlId"];
            }
            set
            {
                ViewState["_selectedUserControlId"] = value;
            }
        }


        #endregion

        #region Functions

        //get all possible parameters
        private void GetRequestParams()
        {
            //check to see if there is a valid usercontrol id in URL
            try
            {
                SELECTED_CONTROL_ID = Convert.ToInt32(Request["sn"].ToString());
            }
            catch (Exception)
            {
            }
        }

        private void LoadControl()
        {
            //load usercontrol
            try
            {
                if (SELECTED_CONTROL_ID > 0)
                {
                    //only call the database if it is a new load
                    if (!Page.IsPostBack)
                    {
                        //get the usercontrol name from Database
                        KanevaUserControl user_control = GetSiteManagementFacade().GetUserControlByID(SELECTED_CONTROL_ID);
                        ACTIVE_CONTROL_TYPE = user_control.ControlTypeId;
                        ACTIVE_CONTROL = user_control.UserControlName;
                    }

                    //reset visibility
                    ReportViewer1.Visible = false;
                    ucShell.Visible = false;
                    this.ucShell.Controls.Clear();

                    //based on usercontrol type determine which control to load
                    switch (ACTIVE_CONTROL_TYPE)
                    {
                        case 2: //report type
                            ReportViewer1.Height = Unit.Pixel(680);
                            ReportViewer1.Width = Unit.Pixel(997);
                            LoadRSReport(ReportViewer1, ACTIVE_CONTROL);

                            //log the activity
                            this.GetSiteManagementFacade().InsertCSRLog(GetUserId(), "Viewing the " + ACTIVE_CONTROL + " Report", 0, GetUserId());

                            break;
                        case 1: //tool type
                        default:
                            //make usercontrol visible
                            ucShell.Visible = true;

							//determine the control path
							string controlPath = SiteManagementCommonFunctions.GetUserControlPath;
							if(ACTIVE_CONTROL.Contains("/"))
							{
								controlPath = "~/usercontrols/";
							}
							
                            //load the control							
                            Control activeControl = Page.LoadControl( controlPath + ACTIVE_CONTROL);
                            this.ucShell.Controls.Add(activeControl);
                            break;
                    }

                }
            }
            catch (Exception ex)
            {
                Messages.Visible = true;
                Messages.BackColor = System.Drawing.Color.White;
                Messages.ForeColor = System.Drawing.Color.DarkRed;
                Messages.Text = "Error While Loading Control: " + ex.Message;
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

    }
}
