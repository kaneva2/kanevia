///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using System.Reflection;
using System.Resources;
using System.Globalization;
using System.Xml;
using System.Text.RegularExpressions;
using System.IO;
using System.Web.UI.WebControls.WebParts;
using CuteEditor;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using KlausEnt.KEP.Kaneva;
using Microsoft.Reporting.WebForms;

namespace KlausEnt.KEP.SiteManagement
{
	/// <summary>
    /// Summary description for SiteManagementCommonFunctions.
	/// </summary>
	public class SiteManagementCommonFunctions
	{
        
       #region Declarations

        private static ResourceManager resmgr = new ResourceManager("SiteManagement", Assembly.GetExecutingAssembly());
        private UserFacade _userFacade;
        private FameFacade _fameFacade;
        private ExperimentFacade _experimentFacade;
        private BlogFacade _blogFacade;
        private GameFacade _gameFacade;
        private ShoppingFacade _shoppingFacade;
        private DevelopmentCompanyFacade _developmentCompanyFacade;
        private TransactionFacade _transactionFacade;
        private SiteMgmtFacade _gameSiteManagementFacade;
        private SiteSecurityFacade _siteSecurityFacade;
        private SubscriptionFacade _subscriptionFacade;
        private PromotionsFacade _promotionsFacade;
        private CommunityFacade communityFacade;
        private MetricsFacade _metricsFacade;
        private ScriptGameItemFacade _scriptGameItemFacade;

        #endregion

        // ***********************************************
        // Static functions
        // ***********************************************
        #region Static Functions 

            // ***********************************************
            // Static Miscellaneous functions
            // ***********************************************
            #region Static Miscellaneous

        /// <summary>
        /// GetJoinLink
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string EncryptString(string plainText)
        {
            return KanevaGlobals.Encrypt(plainText);
        }
         
        /// <summary>
        /// GetJoinLink
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string DecryptString(string encryptedText)
        {
            return KanevaGlobals.Decrypt(encryptedText);
        }

        /// <summary>
        /// GetJoinLink
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GenerateUniqueString(int maxsize)
        {
            return KanevaGlobals.GenerateUniqueString(maxsize);
        }

        /// <summary>
        /// GetKanevaURL
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetKanevaURL
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["kanevaURL"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["kanevaURL"];
                }
                else
                {
                    return "http://www.kaneva.com";
                }


            }
        }

        /// <summary>
        /// GetKanevaURL
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetSiteName
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["SiteName"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["SiteName"];
                }
                else
                {
                    return "http://kwas.kaneva.com";
                }


            }
        }

        /// <summary>
        /// GetKWASSiteName
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetKWASSiteName
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["KWASSiteName"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["KWASSiteName"];
                }
                else
                {
                    return "http://kwas.kaneva.com";
                }
            }
        }

        /// <summary>
        /// GetKanevaURL
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetCurrentSiteName
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["SiteName"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["SiteName"];
                }
                else
                {
                    return GetKanevaURL;
                }


            }
        }

        /// <summary>
        /// GetKanevaURL
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetUserControlPath
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["UserControlPath"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["UserControlPath"];
                }
                else
                {
                    return "../usercontrols";
                }


            }
        }

        /// <summary>
        /// GetKanevaURL
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetUserSearchControlID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["UserSearchControlID"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["UserSearchControlID"];
                }
                else
                {
                    return "20";
                }
            }
        }

        /// <summary>
        /// GetKanevaURL
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetGameSearchParentMenuID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["GameSearchParentMenuID"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["GameSearchParentMenuID"];
                }
                else
                {
                    return "8";
                }
            }
        }


        /// <summary>
        /// GetGameSearchControlID
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetGameSearchControlID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["GameSearchControlID"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["GameSearchControlID"];
                }
                else
                {
                    return "46";
                }
            }
        }

        public static string GetZoneSettingsControlID
        {
            get
            {
                //TODO: rename key in configuration files
                if (System.Configuration.ConfigurationManager.AppSettings["TieZoneControlID"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["TieZoneControlID"];
                }
                else
                {
                    return "79";
                }
            }
        }

        public static string GetGameServersControlID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["GameServersControlID"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["GameServersControlID"];
                }
                else
                {
                    return "52";
                }
            }
        }

        public static string GetWorldTemplateParentMenuID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["WorldTemplateParentMenuID"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["WorldTemplateParentMenuID"];
                }
                else
                {
                    return "22";
                }
            }
        }

        public static string GetWorldTemplatesControlID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["WorldTemplatesControlID"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["WorldTemplatesControlID"];
                }
                else
                {
                    return "77";
                }
            }
        }

        public static string GetWorldTemplateDefaultItemsControlID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["WorldTemplateDefaultItemsControlID"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["WorldTemplateDefaultItemsControlID"];
                }
                else
                {
                    return "80";
                }
            }
        }

        /// <summary>
        /// GetKanevaURL
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetUserSearchParentMenuID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["UserSearchParentMenuID"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["UserSearchParentMenuID"];
                }
                else
                {
                    return "7";
                }
            }
        }

        /// <summary>
        /// GameSearchDefaultPage
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetGameSearchDefaultPage
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["GameSearchDefaultPage"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["GameSearchDefaultPage"];
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// GameSearchDefaultPage
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetUserSearchDefaultPage
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["UserSearchDefaultPage"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["UserSearchDefaultPage"];
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// GetKanevaSiteID
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetKanevaSiteID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["GetKanevaSiteID"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["GetKanevaSiteID"];
                }
                else
                {
                    return "1";
                }
            }
        }

        /// <summary>
        /// GetDeveloperSiteID
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetDeveloperSiteID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["GetDeveloperSiteID"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["GetDeveloperSiteID"];
                }
                else
                {
                    return "2";
                }
            }
        }

        /// <summary>
        /// GetKanevaURL
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetReportServerPath
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ReportServerPath"] != null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["ReportServerPath"];
                }
                else
                {
                    return "http://cerebrum/ReportsManager/Pages/Report.aspx";
                }


            }
        }

        /// <summary>
        /// GetMaxCommentLength
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static int GetResultsPerPage
        {
            get
            {
                int numPerPage = 10;
                if (System.Configuration.ConfigurationManager.AppSettings["ResultsPerPage"] != null)
                {

                    try
                    {
                        numPerPage = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ResultsPerPage"]);
                    }
                    catch (FormatException)
                    {
                    }
                }

                return numPerPage;

            }
        }

        /// <summary>
        /// GetCompanyID
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static int GetCompanyID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["KanevaCompanyId"] != null)
                {
                    return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["KanevaCompanyId"]);
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// GetKanevaAdminRoleID
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static int GetKanevaAdminRoleID
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["KanevaAdminRoleId"] != null)
                {
                    return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["KanevaAdminRoleId"]);
                }
                else
                {
                    return 56;
                }
            }
        }

            #endregion


            // ***********************************************
            // Static User functions
            // ***********************************************
            #region Static User Functions

            /// <summary>
            /// Get the user info from the current logged in user
            /// </summary>
            /// <returns></returns>
            public static User CurrentUser
            {
                get
                {
                    User user = (User)HttpContext.Current.Items["User"];
                    return user;
                }
            }

            /// <summary>
            /// Get the user id from the current logged in user
            /// </summary>
            /// <returns></returns>
            public static int GetUserId()
            {
                return CurrentUser.UserId;
            }

            #endregion


        #endregion

        // ***********************************************
        // CONSTANTS
        // ***********************************************
        #region Constants

        public enum SUPPORTED_WEBSITES
        {
            KANEVA = 1,
            DEVELOPER = 2,
            SHOPPING = 3,
            DESIGNER = 4,
            SITE_MANAGEMENT = 5
        }

        #endregion

        // ***********************************************
        // Formatting functions
        // ***********************************************
        #region Formatting functions

        /// <summary>
        /// Strips excess formatting from a JSON-encoded string.
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public string StripExcessJSONFormatting(string json)
        {
            return Common.StripExcessJSONFormatting(json);
        }

        /// <summary>
        /// Format currency
        /// </summary>
        /// <param name="dblCurrency"></param>
        /// <returns></returns>
        public string FormatCurrencyForTextBox(Object dblCurrency)
        {
            return KanevaGlobals.FormatCurrencyForTextBox(dblCurrency);
        }

        /// <summary>
        /// Format the date into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public string FormatDate(Object dtDate)
        {
            if (dtDate.Equals(DBNull.Value))
                return "";
            return KanevaGlobals.FormatDate((DateTime)dtDate);
        }

        public string FormatDateNumbersOnly(Object dtDate)
        {
            if (dtDate.Equals(DBNull.Value))
                return "";
            return KanevaGlobals.FormatDateNumbersOnly((DateTime)dtDate);
        }

        /// <summary>
        /// Format the date into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public string FormatDateTime(Object dtDate)
        {
            if (dtDate.Equals(DBNull.Value))
                return "";
            return KanevaGlobals.FormatDateTime((DateTime)dtDate);
        }
        /// <summary>
        /// Format date as a time span
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public string FormatDateTimeSpan(Object dtDate)
        {
            return KanevaGlobals.FormatDateTimeSpan(dtDate);
        }
        /// <summary>
        /// Format image size
        /// </summary>
        /// <param name="imageSize"></param>
        /// <returns></returns>
        public string FormatImageSize(Object imageSize)
        {
            if (imageSize.Equals(DBNull.Value))
                return "";
            return KanevaGlobals.FormatImageSize(Convert.ToInt64(imageSize));
        }


        /// <summary>
        /// FormatCurrency
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public string FormatCurrency(Object amount)
        {
            return KanevaGlobals.FormatCurrency(Convert.ToDouble(amount));
        }

        /// <summary>
        /// Get a string
        /// </summary>
        public string FormatString(Object dtColumn, int statusId)
        {
            return FormatString(dtColumn, statusId, "");
        }

        /// <summary>
        /// Get a string
        /// </summary>
        public string FormatString(Object dtColumn, int statusId, string notSetText)
        {
            //if (statusId.Equals((int)Constants.eASSET_STATUS.NEW))
            return "<span class=\"offline\">" + notSetText + "</span>";
        }

        /// <summary>
        /// Format the date into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public string FormatDateTimeSpan(Object dtDate, Object dtDateNow)
        {
            if (dtDate.Equals(DBNull.Value))
                return "";
            return KanevaGlobals.FormatDateTimeSpan(dtDate, dtDateNow);
        }

        /// <summary>
        /// TruncateWithEllipsis
        /// </summary>
        public string TruncateWithEllipsis(string text, int length)
        {
            return KanevaGlobals.TruncateWithEllipsis(text, length);
        }

        /// <summary>
        /// TruncateWithEllipsis
        /// </summary>
        public string FormatSecondsAsTime(Object seconds)
        {
            if (seconds.Equals(DBNull.Value))
                return "00:00:00";

            return KanevaGlobals.FormatSecondsAsTime(Convert.ToInt32(seconds));
        }

        #endregion

        // ***********************************************
        // Reporting Services
        // ***********************************************
        #region Reporting Services

        public void LoadRSReport(ReportViewer ReportViewer1, string reportPath, ReportParameter[] parameters)
        {
            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
            ReportViewer1.ServerReport.ReportPath = reportPath;
            ReportViewer1.ServerReport.ReportServerUrl = new System.Uri(SiteManagementCommonFunctions.GetReportServerPath);
            ReportViewer1.Visible = true;

            if ((parameters != null) && (parameters.Length > 0))
            {
                ReportViewer1.ServerReport.SetParameters(parameters);

                //hide the parameters tool bar
                ReportViewer1.ShowParameterPrompts = false;
            }
        }
        #endregion

        // ***********************************************
        // Helper functions
        // ***********************************************
        #region Helper Functions

        /// <summary>
        /// Return the delete javascript
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetDeleteScript(int gameId)
        {
            return GetDeleteScript(gameId, false);
        }
        /// <summary>
        /// Is the number numeric?
        /// </summary>
        /// <param name="candidate"></param>
        /// <returns></returns>
        public static bool IsNumeric(string candidate)
        {

            char[] ca = candidate.ToCharArray();
            for (int i = 0; i < ca.Length; i++)
            {
                if (ca[i] > 57 || ca[i] < 48)
                    return false;
            }
            return true;
        } 

        /// <summary>
        /// Return the delete javascript
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetDeleteScript(int gameId, bool removeOnly)
        {
            if (removeOnly)
            {
                return "javascript:if (confirm(\"Are you sure you want to remove this Star from the channel?\")){DeleteSTAR (" + gameId + ")};";
            }
            else
            {
                return "javascript:if (confirm(\"Are you sure you want to delete this Star?\")){DeleteSTAR (" + gameId + ")};";
            }
        }

        public SiteSecurityFacade GetSiteSecurityFacade
        {
            get
            {
                if (_siteSecurityFacade == null)
                {
                    _siteSecurityFacade = new SiteSecurityFacade();
                }
                return _siteSecurityFacade;
            }
        }

        public SiteMgmtFacade GetSiteManagementFacade
        {
            get
            {
                if (_gameSiteManagementFacade == null)
                {
                    _gameSiteManagementFacade = new SiteMgmtFacade();
                }
                return _gameSiteManagementFacade;
            }
        }

        public SubscriptionFacade GetSubscriptionFacade
        {
            get
            {
                if (_subscriptionFacade == null)
                {
                    _subscriptionFacade = new SubscriptionFacade();
                }
                return _subscriptionFacade;
            }
        }

        public PromotionsFacade GetPromotionsFacade
        {
            get
            {
                if (_promotionsFacade == null)
                {
                    _promotionsFacade = new PromotionsFacade();
                }
                return _promotionsFacade;
            }
        }

        public GameFacade GetGameFacade
        {
            get
            {
                if (_gameFacade == null)
                {
                    _gameFacade = new GameFacade();
                }
                return _gameFacade;
            }
        }

        public UserFacade GetUserFacade
        {
            get
            {
                if (_userFacade == null)
                {
                    _userFacade = new UserFacade();
                }
                return _userFacade;
            }
        }

        public ExperimentFacade GetExperimentFacade
        {
            get
            {
                if (_experimentFacade == null)
                {
                    _experimentFacade = new ExperimentFacade();
                }
                return _experimentFacade;
            }
        }

        public FameFacade GetFameFacade
        {
            get
            {
                if (_fameFacade == null)
                {
                    _fameFacade = new FameFacade();
                }
                return _fameFacade;
            }
        }

        public TransactionFacade GetTransactionFacade
        {
            get
            {
                if (_transactionFacade == null)
                {
                    _transactionFacade = new TransactionFacade();
                }
                return _transactionFacade;
            }
        }

        public BlogFacade GetBlogFacade
        {
            get
            {
                if (_blogFacade == null)
                {
                    _blogFacade = new BlogFacade();
                }
                return _blogFacade;
            }
        }

        public ShoppingFacade GetShoppingFacade
        {
            get
            {
                if (_shoppingFacade == null)
                {
                    _shoppingFacade = new ShoppingFacade();
                }
                return _shoppingFacade;
            }
        }

        public CommunityFacade GetCommunityFacade
        {
            get
            {
                if (this.communityFacade == null)
                {
                    this.communityFacade = new CommunityFacade ();
                }
                return this.communityFacade;
            }
        }

        public DevelopmentCompanyFacade GetDevelopmentCompanyFacade
        {
            get
            {
                if (_developmentCompanyFacade == null)
                {
                    _developmentCompanyFacade = new DevelopmentCompanyFacade();
                }
                return _developmentCompanyFacade;
            }
        }

        public MetricsFacade GetMetricsFacade
        {
            get
            {
                if (this._metricsFacade == null)
                {
                    this._metricsFacade = new MetricsFacade();
                }
                return this._metricsFacade;
            }
        }

        public ScriptGameItemFacade GetScriptGameItemFacade
        {
            get
            {
                if (this._scriptGameItemFacade == null)
                {
                    this._scriptGameItemFacade = new ScriptGameItemFacade();
                }
                return this._scriptGameItemFacade;
            }
        }

        public ListItem CreateListItem(string name, string theValue)
        {
            return CreateListItem(name, theValue, false);
        }

        public ListItem CreateListItem(string name, string theValue, bool bold)
        {
            ListItem liNew = new ListItem(name, theValue);
            if (bold)
            {
                liNew.Attributes.Add("style", "font-weight: bold;");
            }
            return liNew;
        }

        /// <summary>
        /// SetDropDownIndex
        /// </summary>
        public void SetDropDownIndex(DropDownList drp, string theValue)
        {
            try
            {
                drp.SelectedValue = theValue;
            }
            catch (Exception)
            { }
        }

        /// <summary>
        /// SetDropDownIndex
        /// </summary>
        public void SetDropDownIndex(DropDownList drp, string theValue, bool bAddValue)
        {
            SetDropDownIndex(drp, theValue, bAddValue, theValue);
        }

        /// <summary>
        /// SetDropDownIndex
        /// </summary>
        public void SetDropDownIndex(DropDownList drp, string theValue, bool bAddValue, string theDisplayName)
        {
            if (bAddValue)
            {
                ListItem li = drp.Items.FindByValue(theValue);
                if (li == null)
                {
                    drp.Items.Insert(drp.Items.Count, new ListItem(theDisplayName, theValue));
                }
            }
            SetDropDownIndex(drp, theValue);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return KanevaGlobals.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return KanevaGlobals.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }


        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
        {
            return KanevaGlobals.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
        {
            return KanevaGlobals.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts);
        }

        /// <summary>
        /// AddHrefNewWindow
        /// </summary>
        /// <param name="theText"></param>
        /// <returns></returns>
        public string AddHrefNewWindow(string theText)
        {
            return theText.Replace("<A href=", "<A target=_blank href=");
        }

        /// <summary>
        /// removeHTML
        /// </summary>
        /// <param name="strMessageInput"></param>
        /// <returns></returns>
        public string RemoveHTML(string strMessageInput)
        {
            int lngMessagePosition = 0;		// Holds the message position
            int intHTMLTagLength = 0;		// Holds the length of the HTML tags
            string strHTMLMessage = "";			// Holds the HTML message
            string strTempMessageInput = "";	// Temp store for the message input

            // Get out quick?
            if (strMessageInput.Length == 0)
            {
                return "";
            }

            // Place the message input into a temp store
            strTempMessageInput = strMessageInput;

            // If an HTML tag is found then jump to the end so we can strip it
            lngMessagePosition = strMessageInput.IndexOf("<", lngMessagePosition);

            // Loop through each character in the post message
            while (lngMessagePosition > -1)
            {
                // Get the length of the HTML tag
                intHTMLTagLength = (strMessageInput.IndexOf(">", lngMessagePosition) - lngMessagePosition);

                if (intHTMLTagLength > 0)
                {
                    // Place the HTML tag back into the temporary message store
                    strHTMLMessage = strMessageInput.Substring(lngMessagePosition, intHTMLTagLength + 1);

                    // Strip the HTML from the temp message store
                    strTempMessageInput = strTempMessageInput.Replace(strHTMLMessage, "");
                }

                // If an HTML tag is found then jump to the end so we can strip it
                lngMessagePosition = strMessageInput.IndexOf("<", lngMessagePosition + 1);
            }

            // Replace a few characters in the remaining text
            strTempMessageInput = strTempMessageInput.Replace("<", "&lt;");
            strTempMessageInput = strTempMessageInput.Replace(">", "&gt;");
            strTempMessageInput = strTempMessageInput.Replace("'", "&#039;");
            strTempMessageInput = strTempMessageInput.Replace("\"", "&#034;");
            strTempMessageInput = strTempMessageInput.Replace("&nbsp;", " ");

            //Return the function
            return strTempMessageInput;
        }

        #endregion

        // ***********************************************
        // Game Functions
        // ***********************************************
        #region Game Functions

        /// <summary>
        /// Return the game image URL
        /// </summary>
        public string GetGameImageURL(string imagePath, string defaultSize)
        {
            if (imagePath.Length.Equals(0))
            {
                return KanevaGlobals.GameImageServer + "/KanevaGameIcon_" + defaultSize + ".gif";
            }
            else
            {
                if ((defaultSize == null) || (defaultSize == ""))
                {
                    defaultSize = "me";
                }
                return KanevaGlobals.GameImageServer + "/" + Path.GetDirectoryName(imagePath).Replace("\\", "/") + "/" + Path.GetFileNameWithoutExtension(imagePath) + "_" + defaultSize + Path.GetExtension(imagePath);
            }
        }

        public void UploadGameImage (int communityId, HtmlInputFile inpThumbnail)
        {
            try
            {
                //ImageHelper.UploadImageFromUser (userId, itemId, browseTHUMB, KanevaGlobals.GameImagesServerPath, ImageHelper.GAME_IMAGE);

                string contentRepository = "";

                // Files are now stored in content repository
                if (KanevaGlobals.ContentServerPath != null)
                {
                    contentRepository = KanevaGlobals.ContentServerPath;

                    //declare the file name of the image
                    string filename = null;

                    //create PostedFile
                    System.Web.HttpPostedFile File = inpThumbnail.PostedFile;

                    //generate newpath
                    string newPath = Path.Combine (contentRepository, GetUserId().ToString ());

                    //upload image from client to image repository
                    ImageHelper.UploadImageFromUser (ref filename, File, newPath, KanevaGlobals.MaxUploadedImageSize);

                    //generate newpath and channel id
                    string imagePath = newPath + Path.DirectorySeparatorChar + filename;
                    GetCommunityFacade.UpdateCommunity (communityId, 0, false, imagePath, File.ContentType, true);

                    //create any and all thumbnails needed
                    ImageHelper.GenerateAllCommuntyThumbnails (filename, imagePath, contentRepository, GetUserId (), communityId);
                }
            }
            catch { }
        }

        /// <summary>
        /// UploadWOKItemImage - throws errors to the calling function
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="itemId"></param>
        public void UploadWOKItemImage(int userId, int itemId, HttpPostedFile fThumbnail)
        {
            string thumbnailFilename = "";
            string newPath = Path.Combine(Path.Combine(KanevaGlobals.TexturePath, SiteManagementCommonFunctions.CurrentUser.UserId.ToString()), itemId.ToString());

            if (fThumbnail != null && fThumbnail.ContentLength > 0)
            {
                ImageHelper.UploadImageFromUser(ref thumbnailFilename, fThumbnail, newPath, KanevaGlobals.MaxUploadedImageSize);
                string imagePath = fThumbnail.FileName.Remove(0, fThumbnail.FileName.LastIndexOf("\\") + 1);
                string origImagePath = newPath + Path.DirectorySeparatorChar + thumbnailFilename;

                ImageHelper.GenerateWOKItemThumbs(thumbnailFilename, origImagePath, KanevaGlobals.TexturePath, SiteManagementCommonFunctions.CurrentUser.UserId, itemId);
            }
        }

        /// <summary>
        /// UploadAchievementImage - throws errors to the calling function
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="achievementId"></param>
        public void UploadAchievementImage (int userId, int achievementId, HttpPostedFile fThumbnail)
        {
            string thumbnailFilename = "";

            string newPath = Path.Combine (KanevaGlobals.ContentServerPath, SiteManagementCommonFunctions.CurrentUser.UserId.ToString ());
            newPath = Path.Combine (newPath, "ach" + achievementId.ToString ());

            if (fThumbnail != null && fThumbnail.ContentLength > 0)
            {
                ImageHelper.UploadImageFromUser (ref thumbnailFilename, fThumbnail, newPath, KanevaGlobals.MaxUploadedImageSize);
                string imagePath = fThumbnail.FileName.Remove (0, fThumbnail.FileName.LastIndexOf ("\\") + 1);
                string origImagePath = newPath + Path.DirectorySeparatorChar + thumbnailFilename;

                ImageHelper.GenerateAchievementThumbs (thumbnailFilename, origImagePath, KanevaGlobals.ContentServerPath, SiteManagementCommonFunctions.CurrentUser.UserId, achievementId);
            }
        }

        /// <summary>
        /// Return the achievement image URL
        /// </summary>
        public string GetAchievementImageURL (string imagePath, string defaultSize)
        {
            if (imagePath.Length.Equals (0))
            {
                return KanevaGlobals.ImageServer + "/defaultworld_" + defaultSize + ".jpg";
            }
            else
            {
                if ((defaultSize == null) || (defaultSize == ""))
                {
                    defaultSize = "me";
                }
                return KanevaGlobals.ImageServer + "/" + Path.GetDirectoryName (imagePath).Replace ("\\", "/") + "/" + Path.GetFileNameWithoutExtension (imagePath) + "_" + defaultSize + Path.GetExtension (imagePath);
            }
        }

        /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public string GetGameAccessDescription(int accessId)
        {
            string status = "";

            DataRow[] results = WebCache.GetGameAccess().Select("game_access_id = " + accessId);
            if ((results != null) && (results.Length > 0))
            {
                status = results[0]["game_access"].ToString();
            }
            return status;
        }

        /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public string GetGameRatingDescription(int ratingId)
        {
            string status = "";

            DataRow[] results = WebCache.GetGameRatings().Select("game_rating_id = " + ratingId);
            if ((results != null) && (results.Length > 0))
            {
                status = results[0]["game_rating"].ToString();
            }
            return status;
        }

        /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public string GetGameStatusDescription(int statusId)
        {
            string status = "";

            DataRow[] results = WebCache.GetGameStatus().Select("game_status_id = " + statusId);
            if ((results != null) && (results.Length > 0))
            {
                status = results[0]["game_status"].ToString();
            }
            return status;
        }

        /// <summary>
        /// checks to see if user is the owner of teh game and thus allowed to edit
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public bool UsersCompanyIsGameOwner(int userId, int gameId)
        {
            DevelopmentCompany company = GetDevelopmentCompanyFacade.GetCompanyByDeveloperId(userId);
            int ownerId = GetGameFacade.GetGameOwner(gameId);
            return ownerId.Equals(company.CompanyId);
        }

        /// <summary>
        /// checks to see if user is the owner of teh game and thus allowed to edit
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public bool CompanyIsGameOwner(int companyId, int gameId)
        {
            //get owber id - which is teh companyId
            int ownerId = GetGameFacade.GetGameOwner(gameId);
            return ownerId.Equals(companyId);
        }

        /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public string GetGameVisibiltyDescription(int visibilityId)
        {
            string visibility = "";

            DataRow[] results = WebCache.GetGameServerVisbility().Select("visibility_id = " + visibilityId);
            if ((results != null) && (results.Length > 0))
            {
                visibility = results[0]["name"].ToString();
            }
            return visibility;
        }


        /// <summary>
        /// Queries server status data set to get the description
        /// </summary>
        public string GetServerStatusDescription(int statusId)
        {
            string status = "";

            DataRow[] results = WebCache.GetGameServerStatus().Select("server_status_id = " + statusId);
            if ((results != null) && (results.Length > 0))
            {
                status = results[0]["name"].ToString();
            }
            return status;
        }


        public const int DEFAULT_PORT = 25857; //default provided by wok team
        public const int DEFAULT_LICENSE_STATUS_ID = 1; //default provided by wok team

        #endregion


        // ***********************************************
        // Security Functions
        // ***********************************************
        #region Security Functions

        public string CleanJavascriptFull(string strText)
        {
            return KanevaGlobals.CleanJavascript(strText);
        }
        #endregion

        /// <summary>
        /// Queries server status data set to get the description
        /// <remarks>THROWS EXCEPTION TO CALLLING FUNCTION</remarks>
        /// </summary>
        public int IsGameMature(int ratingId)
        {
            int mature = 0;

            DataRow[] results = WebCache.GetGameRatings().Select("game_rating_id = " + ratingId);
            if ((results != null) && (results.Length > 0))
            {
                mature = Convert.ToInt32(results[0]["is_mature"]);
            }
            return mature;
        }

        // ***********************************************
        // Static Security functions
        // ***********************************************
        #region Static Security

        /// <summary>
        /// RemoveScripts
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool ContainsInjectScripts(string text)
        {
            bool bInvalid = false;

            try
            {
                XmlDocument doc = EditorUtility.ConvertToXmlDocument(text);
                bInvalid = ContainsScriptsRecursive(doc.DocumentElement);
            }
            catch (Exception) { }

            return bInvalid;
        }


        /// <summary>
        /// RemoveScriptsRecursive
        /// </summary>
        /// <param name="element"></param>
        private static bool ContainsScriptsRecursive(XmlElement element)
        {
            if (ShouldRemove(element))
            {
                return true;
            }
            foreach (XmlElement child in element.SelectNodes("*"))
            {
                return (ContainsScriptsRecursive(child));
            }

            return false;
        }

        /// <summary>
        /// ShouldRemove
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private static bool ShouldRemove(XmlElement element)
        {
            string name = element.LocalName.ToLower();
            //check the element name
            switch (name)
            {
                case "link"://maybe link to another css that contains scripts(behavior,expression)
                case "script":
                    return true;
            }
            //check the attribute
            foreach (XmlAttribute attr in element.Attributes)
            {
                string attrname = attr.LocalName.ToLower();
                //<img onclick=....
                if (attrname.StartsWith("on"))
                    return true;
                string val = attr.Value.ToLower();
                //<a href="javascript:scriptcode()"..
                if (val.IndexOf("script") != -1)
                    return true;
                //<a style="behavior:url(http://another/code.htc)"
                if (attrname == "style")
                {
                    if (val.IndexOf("behavior") != -1)
                        return true;
                    if (val.IndexOf("expression") != -1)
                        return true;
                }

            }
            return false;
        }

        public static bool isTextRestricted (string text, Constants.eRESTRICTION_TYPE restriction_type)
        {
            return isTextRestricted (text, restriction_type, false);
        }

        /// <summary>
        /// isTextRestricted
        /// </summary>
        /// <param name="text"></param>
        /// <param name="restriction_type"></param>
        /// <param name="replace"></param>
        /// <returns>boolean</returns>
        public static bool isTextRestricted (string text, Constants.eRESTRICTION_TYPE restriction_type, bool replace)
        {
            try
            {
                string pattern = "";
                text = text.ToUpper ();
                Regex regx = null;

                // Only check potty mouth words if potty mouth or all type is requested
                if (restriction_type == Constants.eRESTRICTION_TYPE.POTTY_MOUTH ||
                    restriction_type == Constants.eRESTRICTION_TYPE.ALL)
                {
                    // Add potty mouth exact match word list to the REGEX expression
                    if (WebCache.GetRestrictedWordsList (Constants.eRESTRICTION_TYPE.POTTY_MOUTH, Constants.eMATCH_TYPE.EXACT).Length > 0)
                    {
                        pattern = @"\b(" + WebCache.GetRestrictedWordsList (Constants.eRESTRICTION_TYPE.POTTY_MOUTH, Constants.eMATCH_TYPE.EXACT) + ")" + @"\b";
                    }

                    // If there are potty mouth match anywhere words, add them to the REGEX expression
                    if (WebCache.GetRestrictedWordsList (Constants.eRESTRICTION_TYPE.POTTY_MOUTH, Constants.eMATCH_TYPE.ANYWHERE).Length > 0)
                    {
                        if (pattern.Length > 0) pattern += "|";
                        pattern += "(" + WebCache.GetRestrictedWordsList (Constants.eRESTRICTION_TYPE.POTTY_MOUTH, Constants.eMATCH_TYPE.ANYWHERE) + ")";
                    }

                    // create the Regex obj
                    regx = new Regex (pattern);

                    // If we have a match, return true.  No need to continue.
                    if (regx.IsMatch (text, 0))
                    {
                        return true;
                    }
                }


                // Only check reserved words if reserved or all type is requested              
                if (restriction_type == Constants.eRESTRICTION_TYPE.RESERVED ||
                    restriction_type == Constants.eRESTRICTION_TYPE.ALL)
                {
                    // Add reserved exact match word list to the REGEX expression
                    if (WebCache.GetRestrictedWordsList (Constants.eRESTRICTION_TYPE.RESERVED, Constants.eMATCH_TYPE.EXACT).Length > 0)
                    {
                        pattern = @"\b(" + WebCache.GetRestrictedWordsList (Constants.eRESTRICTION_TYPE.RESERVED, Constants.eMATCH_TYPE.EXACT) + ")" + @"\b";
                    }

                    // If there are reserved match anywhere words, add them to the REGEX expression
                    if (WebCache.GetRestrictedWordsList (Constants.eRESTRICTION_TYPE.RESERVED, Constants.eMATCH_TYPE.ANYWHERE).Length > 0)
                    {
                        if (pattern.Length > 0) pattern += "|";
                        pattern += "(" + WebCache.GetRestrictedWordsList (Constants.eRESTRICTION_TYPE.RESERVED, Constants.eMATCH_TYPE.ANYWHERE) + ")";
                    }

                    // create the Regex obj
                    regx = new Regex (pattern);

                    // return result of match
                    return regx.IsMatch (text, 0);
                }
            }
            catch (Exception)
            {

            }
            return false;
        }

        #endregion

        // ***********************************************
        // User Functions
        // ***********************************************
        #region User Functions

        public int AuthorizeUser(string email, string password, int gameId, ref int roleMembership, string UserHostAddress, bool bSendTrackerMessage)
        {
            return UsersUtility.Authorize(email, password, gameId, ref roleMembership, UserHostAddress, bSendTrackerMessage);
        }

        public DataRow GetUserByEmail(string email)
        {
            return UsersUtility.GetUserFromEmail(email);
        }

        public int UpdateLastLogin(int userId, string UserHostAddress, string serverName)
        {
            return _userFacade.UpdateLastLogin(userId, UserHostAddress, serverName);
        }


        #endregion */

    }
}
