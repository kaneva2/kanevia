///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Collections;
using KlausEnt.KEP.Kaneva;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using Microsoft.Reporting.WebForms;

namespace KlausEnt.KEP.SiteManagement
{
	/// <summary>
	/// Summary description for BasePage.
	/// </summary>
	public class BasePage : System.Web.UI.Page
    {
        #region Declarations
        //Common Functions
        private SiteManagementCommonFunctions _devComnFunc;

        // Sorting
        protected Button btnSort;
        protected HtmlInputHidden hidSortColumn;
        #endregion

        /// <summary>
        /// Gets an instance of the SiteManagement Common Functions class
        /// </summary>
        /// <returns>SiteManagementCommonFunctions</returns>
        public SiteManagementCommonFunctions GetDCFInstance
        {
            get
            {
                if (_devComnFunc == null)
                {
                    _devComnFunc = new SiteManagementCommonFunctions();
                }
                return _devComnFunc;
            }
        }
        // ***********************************************
        // Reporting Services
        // ***********************************************
        #region Reporting Services

        public void LoadRSReport(ReportViewer ReportViewer1, string reportPath)
        {
            LoadRSReport(ReportViewer1, reportPath, null);
        }

        public void LoadRSReport(ReportViewer ReportViewer1, string reportPath, ReportParameter[] parameters)
        {
            GetDCFInstance.LoadRSReport(ReportViewer1, reportPath, parameters);
        }

        #endregion

        // ***********************************************
        // Helper functions
        // ***********************************************
        #region Helper Functions

        /// <summary>
        /// AddKeepAlive - Avoid timeout issues
        /// </summary>
        public void AddKeepAlive()
        {
            // Set timer 30 seconds before the timeout will expire
            int int_MilliSecondsTimeOut = (this.Session.Timeout * 60000) - 30000;

            string str_Script = "<script type='text/javascript'>\n" +
                "//Number of Reconnects\n" +
                "var count=0;\n" +
                //Maximum reconnects setting\n" +
                "var max = 5;\n\n" +
                "function Reconnect()\n" +
                "{\n" +
                "	count++;\n" +
                "	if (count < max)\n" +
                "	{\n" +
                "		window.status = 'Session refreshed ' + count.toString()+' time(s)' ;\n" +
                "		var img = new Image(1,1);\n" +
                "		img.src = '" + ResolveUrl("~/sessionKeepAlive.aspx") + "';\n" +
                "	}\n" +
                "}\n\n" +
                "window.setInterval('Reconnect()'," + int_MilliSecondsTimeOut.ToString() + @");" +
                "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(this.GetType(), "Reconnect"))
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "Reconnect", str_Script);
            }
        }

        public SiteMgmtFacade GetSiteManagementFacade()
        {
            return GetDCFInstance.GetSiteManagementFacade;
        }

        public UserFacade GetUserFacade()
        {
            return GetDCFInstance.GetUserFacade;
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return GetDCFInstance.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return GetDCFInstance.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }


        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
        {
            return GetDCFInstance.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
        {
            return GetDCFInstance.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts);
        }

        /// <summary>
        /// AddHrefNewWindow
        /// </summary>
        /// <param name="theText"></param>
        /// <returns></returns>
        public string AddHrefNewWindow(string theText)
        {
            return GetDCFInstance.AddHrefNewWindow(theText);
        }

        /// <summary>
        /// GetLoginURL
        /// </summary>
        /// <returns></returns>
        public string GetLoginURL()
        {
            return ResolveUrl("~/login.aspx?logretURL=" + Server.UrlEncode(GetCurrentURL()));
        }

        /// <summary>
        /// GetLoginURL
        /// </summary>
        /// <returns></returns>
        public string GetHomePageURL()
        {
            return ResolveUrl("~/Default.aspx");
        }

        /// <summary>
        /// GetLoginURL with args
        /// </summary>
        /// <returns></returns>
        public string GetHomePageURL(string args)
        {
            return ResolveUrl("~/Default.aspx?" + args);
        }

        /// <summary>
        /// GetCurrentURL
        /// </summary>
        /// <returns></returns>
        public string GetCurrentURL()
        {
            return Request.CurrentExecutionFilePath + "?" + Request.QueryString.ToString();
        }

        /// <summary>
        /// Redirect to login page
        /// </summary>
        public void RedirectToLogin()
        {
            Response.Redirect(GetLoginURL());
        }

        /// <summary>
        /// Redirect to home page
        /// </summary>
        public void RedirectToHomePage()
        {
            Response.Redirect(GetHomePageURL());
        }

        /// <summary>
        /// removeHTML
        /// </summary>
        /// <param name="strMessageInput"></param>
        /// <returns></returns>
        public string RemoveHTML(string strMessageInput)
        {
            return GetDCFInstance.RemoveHTML(strMessageInput);
        }

        #endregion


        // ***********************************************
        // Security Functions
        // ***********************************************
        #region Security Functions

        /// <summary>
        /// Return the current user id
        /// </summary>
        /// <returns></returns>
        public int GetUserId()
        {
            return SiteManagementCommonFunctions.GetUserId();
        }

        public string CleanJavascriptFull(string strText)
        {
            return GetDCFInstance.CleanJavascriptFull(strText);
        }

        #endregion

        // ***********************************************
        // User Functions
        // ***********************************************
        #region User Functions

        public int AuthorizeUser(string email, string password, int gameId, ref int roleMembership, string UserHostAddress, bool bSendTrackerMessage)
        {
            return GetDCFInstance.AuthorizeUser(email, password, gameId, ref roleMembership, UserHostAddress, bSendTrackerMessage);
        }

        public DataRow GetUserByEmail(string email)
        {
            return GetDCFInstance.GetUserByEmail(email);
        }

        public int UpdateLastLogin(int userId, string UserHostAddress, string serverName)
        {
            return GetDCFInstance.UpdateLastLogin(userId, UserHostAddress, serverName);
        }

        public User GetCurrentUser()
        {
            return SiteManagementCommonFunctions.CurrentUser;
        }

        #endregion


        // ***********************************************
        // Bread Crumbs
        // ***********************************************
        #region Bread Crumbs
        /// <summary>
        /// Gets New BreadCrumb From Kaneva Frame Work
        /// </summary>
        /// <returns></returns>
        public BreadCrumb GetNewBreadCrumb(string crumbName, string URL, string sort, int pageNumber)
        {
            return new BreadCrumb(crumbName, URL, sort, pageNumber);
        }


        /// <summary>
        /// GetBreadCrumbs
        /// </summary>
        /// <returns></returns>
        public BreadCrumbs GetBreadCrumbs()
        {
            if (Session["breadCrumb"] == null)
            {
                BreadCrumbs breadCrumbs = new BreadCrumbs();
                Session["breadCrumb"] = breadCrumbs;
                return breadCrumbs;
            }

            return (BreadCrumbs)Session["breadCrumb"];
        }

        /// <summary>
        /// GetLastBreadCrumb
        /// </summary>
        /// <returns>the last entered bread crumb or null if none found</returns>
        public BreadCrumb GetLastBreadCrumb()
        {
            BreadCrumb crumb = null;
            BreadCrumbs crumbList = GetBreadCrumbs();
            if (crumbList.Count > 0)
            {
                crumb = crumbList.Item(crumbList.Count - 1);
            }
            return crumb;
        }


        /// <summary>
        /// ResetBreadCrumb
        /// </summary>
        public void ResetBreadCrumb()
        {
            Session["breadCrumb"] = null;
        }

        public void AddBreadCrumb(BreadCrumb newBreadCrumb)
        {
            BreadCrumbs breadCrumbs = GetBreadCrumbs();
            BreadCrumb breadCrumb;

            // See if this already exists, if it does, remove that one and add new one
            for (int i = 0; i < breadCrumbs.Count; i++)
            {
                breadCrumb = (BreadCrumb)breadCrumbs.Item(i);

                if (breadCrumb.Text.Equals(newBreadCrumb.Text))
                {
                    breadCrumbs.Remove(i);
                }
            }

            breadCrumbs.Add(newBreadCrumb);
        }

        /// <summary>
        /// DisplayBreadCrumb
        /// </summary>
        [Obsolete("This is for backward compatibilities, will be removed in next version")]
        public void DisplayBreadCrumb(PlaceHolder phBreadCrumb)
        {
        }

        public BreadCrumb GetBreadCrumbByName(string breadCrumbText)
        {
            BreadCrumbs breadCrumbs = GetBreadCrumbs();
            return breadCrumbs.FindByText(breadCrumbText);
        }

        /// <summary>
        /// NavigateBackToBreadCrumb
        /// </summary>
        public void NavigateBackToBreadCrumb(int backCount)
        {
            // Make sure it exists first
            BreadCrumbs breadCrumbs = GetBreadCrumbs();
            string qs = "";

            if (breadCrumbs.Count >= backCount)
            {
                BreadCrumb bcPage = (BreadCrumb)breadCrumbs.Item(breadCrumbs.Count - backCount);
                if (bcPage.PageNumber > 0)
                {
                    qs += "&" + Constants.PARAM_PAGE_NUMBER + "=" + bcPage.PageNumber;
                }
                if (bcPage.Sort.Length > 0)
                {
                    qs += "&" + Constants.PARAM_SORT + "=" + bcPage.Sort;
                }
                if (bcPage.SortOrder.Length > 0)
                {
                    qs += "&" + Constants.PARAM_SORT_ORDER + "=" + bcPage.SortOrder;
                }
                if (bcPage.Filter.Length > 0)
                {
                    qs += "&" + Constants.PARAM_FILTER + "=" + bcPage.Filter;
                }
                Response.Redirect(ResolveUrl(bcPage.Hyperlink) + qs);
            }
            else
            {
                // Go home
                RedirectToHomePage();
            }
        }

        public void NavigateBackToBreadCrumb(string breadCrumbText)
        {
            // Make sure it exists first
            BreadCrumbs breadCrumbs = GetBreadCrumbs();
            BreadCrumb requestedCrumb = breadCrumbs.FindByText(breadCrumbText);
            string qs = "";

            if (requestedCrumb != null)
            {
                if (requestedCrumb.PageNumber > 0)
                {
                    qs += "&" + Constants.PARAM_PAGE_NUMBER + "=" + requestedCrumb.PageNumber;
                }
                if (requestedCrumb.Sort.Length > 0)
                {
                    qs += "&" + Constants.PARAM_SORT + "=" + requestedCrumb.Sort;
                }
                if (requestedCrumb.SortOrder.Length > 0)
                {
                    qs += "&" + Constants.PARAM_SORT_ORDER + "=" + requestedCrumb.SortOrder;
                }
                if (requestedCrumb.Filter.Length > 0)
                {
                    qs += "&" + Constants.PARAM_FILTER + "=" + requestedCrumb.Filter;
                }
                Response.Redirect(ResolveUrl(requestedCrumb.Hyperlink) + qs);
            }
            else
            {
                // Go home
                RedirectToHomePage();
            }

        }
        #endregion
    }


}
