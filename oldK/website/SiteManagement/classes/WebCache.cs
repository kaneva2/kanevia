///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web.Caching;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.SiteManagement
{
	/// <summary>
	/// Summary description for Cache.
	/// </summary>
	public class WebCache
	{
		public WebCache ()
		{
		}
        
        /// <summary>
        /// Get a specific country
        /// </summary>
        /// <returns></returns>
        public static Country GetCountry (string countryId)
        {
            IList <Country> dtCountries = GetCountries ();
            Country _country = null;

            foreach (Country c in dtCountries)
            {
                if (c.CountryId == countryId)
                {
                    _country = c;
                    break;
                }
            }

            return _country;
        }
        /// <summary>
        /// GetStatusName
        /// </summary>
        public static string GetStatusName(int status_id)
        {
            DataTable dtStatuses = (DataTable)Global.Cache()[cSTATUS_NAMES];
            if (dtStatuses == null)
            {
                // Add to the cache
                dtStatuses = UsersUtility.GetUserStatus();
                Global.Cache().Insert(cSTATUS_NAMES, dtStatuses, null, DateTime.Now.AddMinutes(15), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            DataRow[] dr = dtStatuses.Select("status_id = " + status_id);
            if (dr.Length > 0)
                return dr[0]["name"].ToString();
            else
                return "";
        }
   
        /// <summary>
		/// GetCountries
		/// </summary>
        public static IList<Country> GetCountries()
		{
			// Load countries
            IList<Country> dtCountries = (IList<Country>)Global.Cache()[cCOUNTRIES];

			if (dtCountries == null)
			{
				// Get and add to cache
                UserFacade userFacade = new UserFacade();
				dtCountries = userFacade.GetCountries ();
                Global.Cache().Insert(cCOUNTRIES, dtCountries, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
			}
            
			return dtCountries;
		}

        /// <summary>
        /// GetAssetTypes
        /// </summary>
        public static DataTable GetAssetTypes()
        {
            string cacheAssetTypes = "assetTypes";

            // Load asset perms
            DataTable dtAssetTypes = (DataTable)Global.Cache()[cacheAssetTypes];
            if (dtAssetTypes == null)
            {
                // Add the community list to the cache
                dtAssetTypes = StoreUtility.GetAssetTypes(); ;
                Global.Cache().Insert(cacheAssetTypes, dtAssetTypes, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtAssetTypes;
        }


        /// <summary>
        /// GetStates
        /// </summary>
        public static IList<State> GetStates()
        {
            // Load states
            IList<State> dtStates = (IList<State>)Global.Cache()[cSTATES];

            if (dtStates == null)
            {
                // Get and add to cache
                UserFacade userFacade = new UserFacade();
                dtStates = userFacade.GetStates();
                Global.Cache().Insert(cSTATES, dtStates, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtStates;
        }

        /// <summary>
        /// GetAvailableWebsites
        /// </summary>
        public static IList<WebSite> GetAvailableWebsites()
        {
            // Load asset perms
            IList<WebSite> dtAvailableSites = (IList<WebSite>)Global.Cache()[cAVAILABLE_SIGHTS];

            if (dtAvailableSites == null)
            {
                // Add the community list to the cache
                dtAvailableSites = (new SiteMgmtFacade()).GetAvailableWebsites();
                Global.Cache().Insert(cAVAILABLE_SIGHTS, dtAvailableSites, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtAvailableSites;
        }

        /// <summary>
        /// GetAllViolationTypes
        /// </summary>
        public static IList<ViolationType> GetAllViolationTypes()
        {
            string cacheViolationTypeName = "violationTypes";

            // Load countries
            IList<ViolationType> dtViolationTypes = (IList<ViolationType>)Global.Cache()[cacheViolationTypeName];

            if (dtViolationTypes == null)
            {
                // Get and add to cache
                ViolationsFacade violationFacade = new ViolationsFacade();
                dtViolationTypes = violationFacade.GetAllViolationTypes();
                Global.Cache().Insert(cacheViolationTypeName, dtViolationTypes, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtViolationTypes;
        }

        /// <summary>
        /// GetAllViolationActionTypes
        /// </summary>
        public static IList<ViolationActionType> GetAllViolationActionTypes()
        {
            string cacheViolationActionTypeName = "violationActionTypes";

            // Load countries
            IList<ViolationActionType> dtViolationActionTypes = (IList<ViolationActionType>)Global.Cache()[cacheViolationActionTypeName];

            if (dtViolationActionTypes == null)
            {
                // Get and add to cache
                ViolationsFacade violationFacade = new ViolationsFacade();
                dtViolationActionTypes = violationFacade.GetAllViolationActionTypes();
                Global.Cache().Insert(cacheViolationActionTypeName, dtViolationActionTypes, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtViolationActionTypes;
        }

        /// <summary>
        /// GetGameStatus
        /// </summary>
        public static List<SitePrivilege> GetAvailableUserPrivileges()
        {
            List<SitePrivilege> availablePrivileges = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            availablePrivileges = (List<SitePrivilege>)cache[cAVAILABLE_PRIVILEDGES];
            if (availablePrivileges == null)
            {
                SiteSecurityFacade siteSecurityFacade = new SiteSecurityFacade();

                availablePrivileges = (List<SitePrivilege>)siteSecurityFacade.GetAvailableUserPrivileges();
                cache.Insert(cAVAILABLE_PRIVILEDGES, availablePrivileges, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return availablePrivileges;
        }
        
        /// <summary>
        /// GetAllPrivileges
        /// </summary>
        public static List<SitePrivilege> GetAllPrivileges()
        {
            List<SitePrivilege> allPrivileges = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            allPrivileges = (List<SitePrivilege>)cache[cALL_PRIVILEDGES];
            if (allPrivileges == null)
            {
                SiteSecurityFacade siteSecurityFacade = new SiteSecurityFacade();

                allPrivileges = (List<SitePrivilege>)siteSecurityFacade.GetAllPrivileges();
                cache.Insert(cALL_PRIVILEDGES, allPrivileges, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return allPrivileges;
        }

        /// <summary>
        /// GetAvailableWebsites
        /// </summary>
        public static DataTable GetAccessLevels()
        {
            // Load asset perms
            DataTable dtAccessLevels = (DataTable)Global.Cache()[cACCESS_LEVELS];
            if (dtAccessLevels == null)
            {
                // Add the community list to the cache
                dtAccessLevels = (new SiteSecurityFacade()).GetAllAccessLevels();
                Global.Cache().Insert(cACCESS_LEVELS, dtAccessLevels, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtAccessLevels;
        }

        /// <summary>
        /// GetAvailableWebsites
        /// </summary>
        public static DataTable GetUserControlTypes()
        {
            // Load asset perms
            DataTable dtUserControls = (DataTable)Global.Cache()[cUSER_CONTOLS];
            if (dtUserControls == null)
            {
                // Add the community list to the cache
                dtUserControls = (new SiteMgmtFacade()).GetUserControlTypes();
                Global.Cache().Insert(cUSER_CONTOLS, dtUserControls, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtUserControls;
        }

        /// <summary>
        /// GetAvailableWebsites
        /// </summary>
        public static IList<SitePrivilege> GetAdminSitePrivileges()
        {
            // Load asset perms
            IList<SitePrivilege> dtSitePrivileges = (IList<SitePrivilege>)Global.Cache()[cSITE_PRIVILEDGES];
            if (dtSitePrivileges == null)
            {
                // Add the community list to the cache
                dtSitePrivileges = (new SiteSecurityFacade()).GetAdminSitePrivileges();
                Global.Cache().Insert(cSITE_PRIVILEDGES, dtSitePrivileges, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtSitePrivileges;
        }

        /// <summary>
        /// GetAvailableWebsites
        /// </summary>
        public static List<User> GetCurrentSiteAdmins(int companyID)
        {
            // Load asset perms
            List<User> dtSiteAdmins = (List<User>)Global.Cache()[cCURRENT_SITEADMINS];
            if (dtSiteAdmins == null)
            {
                // Add the community list to the cache
                dtSiteAdmins = (new SiteMgmtFacade()).GetSiteAdminsList(companyID);
                Global.Cache().Insert(cCURRENT_SITEADMINS, dtSiteAdmins, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtSiteAdmins;
        }


        /// <summary>
        /// Used to clear the cache value for a specified key string
        /// </summary>
        public static int ClearCacheValue(string cacheKeyString)
        {
            Global.Cache().Remove(cacheKeyString);

            if (Global.Cache()[cacheKeyString] == null)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// GetPassGroupsToSubscription
        /// </summary>
        public static DataTable GetPassGroupsToSubscription()
        {
            string cachePassGroup2SubscriptName = "passGroup2Subscription";

            // Load table
            DataTable dtPassGroupsToSubscription = (DataTable)Global.Cache()[cachePassGroup2SubscriptName];

            if (dtPassGroupsToSubscription == null)
            {
                // Get and add to cache
                SubscriptionFacade subscriptionFacade = new SubscriptionFacade();
                dtPassGroupsToSubscription = subscriptionFacade.GetPassGroupsToSubscription();
                Global.Cache().Insert(cachePassGroup2SubscriptName, dtPassGroupsToSubscription, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtPassGroupsToSubscription;
        }

        /// <summary>
        /// GetAllPrivileges
        /// </summary>
        public static List<KanevaColor> GetAvailableNameColors()
        {
            List<KanevaColor> availableColors = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            availableColors = (List<KanevaColor>)cache[cAVAILABLE_COLORS];
            if (availableColors == null)
            {
                UserFacade userFacade = new UserFacade();

                availableColors = (List<KanevaColor>)userFacade.GetAvailableNameColors();
                cache.Insert(cAVAILABLE_COLORS, availableColors, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return availableColors;
        }


        /// <summary>
        /// GetRestrictedWords
        /// </summary>
        public static DataTable GetRestrictedWords ()
        {
            DataTable dtRestrictedWords = (DataTable) Global.Cache ()[cRESTWORDS];
            if (dtRestrictedWords == null)
            {
                // Add to the cache
                dtRestrictedWords = StoreUtility.GetRestrictedWords ();
                Global.Cache ().Insert (cRESTWORDS, dtRestrictedWords, null, DateTime.Now.AddMinutes (15), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtRestrictedWords;
        }

        /// <summary>
        /// GetRestrictedWordsList - This method caches and gets a specified string of words based on the restricion type
        /// and the match type.  The string is generated with | (pipe) seperator which is the format used by the REGEX object
        /// for the regular expression matching.
        /// </summary>
        public static string GetRestrictedWordsList (Constants.eRESTRICTION_TYPE rest_type, Constants.eMATCH_TYPE match_type)
        {
            string cacheID = string.Empty;

            // Set the cacheID so we can see if string is already cached
            if (rest_type == Constants.eRESTRICTION_TYPE.POTTY_MOUTH)
            {
                cacheID = (match_type == Constants.eMATCH_TYPE.EXACT ? cPMEXACT : cPMANY);
            }
            else if (rest_type == Constants.eRESTRICTION_TYPE.RESERVED)
            {
                cacheID = (match_type == Constants.eMATCH_TYPE.EXACT ? cRESTEXACT : cRESTANY);
            }

            // Try to get string from cache
            string strWordList = (string) Global.Cache ()[cacheID];
            if (strWordList == null)
            {
                strWordList = string.Empty;

                // Set the match type param for the query string
                string matchTypeClause = "match_type=" + (match_type == Constants.eMATCH_TYPE.EXACT ? "'exact'" : "'anywhere'");

                // Set the restriction type param for the query string
                string restTypeClause = "restriction_type_id=" + (rest_type == Constants.eRESTRICTION_TYPE.RESERVED ?
                    (int) Constants.eRESTRICTION_TYPE.RESERVED : (int) Constants.eRESTRICTION_TYPE.POTTY_MOUTH);

                // Get the word list
                DataRow[] drRestWords = WebCache.GetRestrictedWords ().Select (restTypeClause + " AND " + matchTypeClause);
                if (drRestWords.Length > 0)
                {
                    // Let's build the list of words we need to search for
                    for (int i = 0; i < drRestWords.Length; i++)
                    {
                        strWordList += drRestWords[i]["word"].ToString () + "|";
                    }

                    // Remove the trailing pipe |
                    if (strWordList.EndsWith ("|"))
                    {
                        strWordList = strWordList.Substring (0, strWordList.Length - 1);
                    }
                }

                // Add to the cache
                Global.Cache ().Insert (cacheID, strWordList, null, DateTime.Now.AddMinutes (15), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return strWordList;
        }
        
        #region Game Related Functions
        /// <summary>
        /// GetGameRatings
        /// </summary>
        public static DataTable GetGameRatings()
        {
            DataTable dtGameRatings = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            dtGameRatings = (DataTable)cache[cGAME_RATINGS];
            if (dtGameRatings == null)
            {
                GameFacade gFacade = new GameFacade();

                dtGameRatings = gFacade.GetGameRatingOptionsLevels();
                cache.Insert(cGAME_RATINGS, dtGameRatings, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtGameRatings;
        }

        /// <summary>
        /// GetGameAccess
        /// </summary>
        public static DataTable GetGameAccess()
        {
            DataTable dtGameAccess = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            dtGameAccess = (DataTable)cache[cGAME_ACCESS];
            if (dtGameAccess == null)
            {
                GameFacade gFacade = new GameFacade();

                dtGameAccess = gFacade.GetGameAccessLevels();
                cache.Insert(cGAME_ACCESS, dtGameAccess, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtGameAccess;
        }
		/// <summary>
        /// GetGameCategories
		/// </summary>
        public static List<CommunityCategory> GetGameCategories()
		{
            List<CommunityCategory> dtGameCategories = null;
			Cache cache = Global.Cache ();
			int cacheTime = 15;

            dtGameCategories = (List<CommunityCategory>)cache[cGAME_CATEGORIES];
            if (dtGameCategories == null)
			{
                CommunityFacade communityFacade = new CommunityFacade();

                dtGameCategories = communityFacade.GetCommunityCategories();
                cache.Insert(cGAME_CATEGORIES, dtGameCategories, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
			}

            return dtGameCategories;
		}


        /// <summary>
        /// GetGameStatus
        /// </summary>
        public static DataTable GetGameStatus()
        {
            DataTable dtGameStatus = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            dtGameStatus = (DataTable)cache[cGAME_STATUS];
            if (dtGameStatus == null)
            {
                GameFacade gFacade = new GameFacade();

                dtGameStatus = gFacade.GetGameStatusOptions();
                cache.Insert(cGAME_STATUS, dtGameStatus, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtGameStatus;
        }

        public static List<GameLicenseSubscription> GetGameLicenseTypes()
        {
            List<GameLicenseSubscription> dtGameLicenseTypes = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            dtGameLicenseTypes = (List<GameLicenseSubscription>)cache[cGAME_LICENSETYPE];
            if (dtGameLicenseTypes == null)
            {
                GameFacade gFacade = new GameFacade();

                dtGameLicenseTypes = gFacade.GetGameLicenseTypes();
                cache.Insert(cGAME_LICENSETYPE, dtGameLicenseTypes, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtGameLicenseTypes;
        }

        /// GetGameStatus
        /// </summary>
        public static DataTable GetGameServerVisbility()
        {
            DataTable dtGameServerVisibility = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            dtGameServerVisibility = (DataTable)cache[cSERVER_VISIBILITY];
            if (dtGameServerVisibility == null)
            {
                GameFacade gFacade = new GameFacade();

                dtGameServerVisibility = gFacade.GetServerVisibilityOptions();
                cache.Insert(cSERVER_VISIBILITY, dtGameServerVisibility, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtGameServerVisibility;
        }

        /// GetGameStatus
        /// </summary>
        public static DataTable GetGameServerStatus()
        {
            DataTable dtGameServerStatus = null;
            Cache cache = Global.Cache();
            int cacheTime = 15;

            dtGameServerStatus = (DataTable)cache[cSERVER_STATUS];
            if (dtGameServerStatus == null)
            {
                GameFacade gFacade = new GameFacade();

                dtGameServerStatus = gFacade.GetServerStatusOptions();
                cache.Insert(cSERVER_STATUS, dtGameServerStatus, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtGameServerStatus;
        }

        #endregion

        /// GetGameStatus
        /// </summary>
      /*  public static DataTable GetAvailableWebsites()
        {
            DataTable dtAvailableWebsites = null;
            Cache cache = Global.Cache();
            int cacheTime = 120;

            dtAvailableWebsites = (DataTable)cache[cAVAILABLE_SIGHTS];
            if (dtAvailableWebsites == null)
            {
                SiteMgmtFacade smFacade = new SiteMgmtFacade();

                dtAvailableWebsites = smFacade.GetAvailableWebsites();
                cache.Insert(cAVAILABLE_SIGHTS, dtAvailableWebsites, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtAvailableWebsites;
        }*/

        // Featured item cache constants
        private const string cSERVER_STATUS = "srvStatus";
        private const string cSERVER_VISIBILITY = "srvVisibility";
        private const string cGAME_LICENSETYPE = "gamLicenseType";
        private const string cGAME_CATEGORIES = "gamCats";
        private const string cGAME_STATUS = "gamStatus";
        private const string cAVAILABLE_SIGHTS = "availSights";
        private const string cAVAILABLE_COLORS = "availColors";
        private const string cCOUNTRIES = "countries";
        private const string cSTATES = "states";
        private const string cACCESS_LEVELS = "accessLevels";
        private const string cSITE_PRIVILEDGES = "sitePrivileges";
        private const string cAVAILABLE_PRIVILEDGES = "availablePrivileges";
        private const string cALL_PRIVILEDGES = "allPrivileges";
        private const string cCURRENT_SITEADMINS = "currentSiteAdmins";
        private const string cGAME_RATINGS = "gamRatings";
        private const string cUSER_CONTOLS = "userControls";
        private const string cSTATUS_NAMES = "STATUS_NAMES";
        private const string cGAME_ACCESS = "gamAccess";
        private const string cSMUT = "POTTY_MOUTH";
        public const string cRESTWORDS = "RESTRICTED_WORDS";    // Used for DataTable of restricted words
        public const string cPMEXACT = "POTTY_MOUTH_EXACT";     // Used for Potty Mouth Exact Match word list
        public const string cPMANY = "POTTY_MOUTH_ANYWHERE";    // Used for Potty Mouth Anywhere Match word list
        public const string cRESTEXACT = "RESERVED_EXACT";      // Used for Reserved Exact Match word list
        public const string cRESTANY = "RESERVED_ANYWHERE";     // Used for Reserved Anywhere Match word list
        

	}
}
