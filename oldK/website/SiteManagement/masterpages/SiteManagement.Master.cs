///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class MainTemplatePage : System.Web.UI.MasterPage
    {
        #region Declarations

        protected System.Web.UI.WebControls.Literal litStyleSheet, litJavascripts, litJavascriptDetect;

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            // Style sheets
            litStyleSheet.Text = "<link id=\"styleSheet\" rel=\"stylesheet\" href=\"" + ResolveUrl("~/css/siteManagement.css") + "\" type=\"text/css\"/>\n\r";

            // Global Javascripts
            litJavascripts.Text = "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" + ResolveUrl("~/jscript/prototype.js") + "\"></script>" + 
                "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" + ResolveUrl("~/jscript/kaneva.js") + "\"></script>";

            litJavascriptDetect.Text = "<NOSCRIPT><META HTTP-EQUIV=\"refresh\" CONTENT=\"1; URL=" + ResolveUrl("~/noJavascript.aspx") + "\"></NOSCRIPT>";

        }

    }
}
