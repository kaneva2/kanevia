///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Security.Principal;
using System.Data;
using System.Configuration;
using System.Web.Security;
using KlausEnt.KEP.Kaneva;
// Import log4net classes.
using log4net;
using log4net.Config;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.SiteManagement
{
    public class Global : System.Web.HttpApplication
    {
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        /// <summary>
        /// Get the current cache
        /// </summary>
        /// <returns></returns>
        public static System.Web.Caching.Cache Cache()
        {
            return HttpRuntime.Cache;
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            // Change to log4net to be consistent with other modules
            XmlConfigurator.Configure(new System.IO.FileInfo(Server.MapPath("~") + "\\" + System.Configuration.ConfigurationManager.AppSettings["LogConfigFile"]));
        }

        /// <summary>
        /// Application_AuthenticateRequest
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            // Get the user information and save it to context for later.
            // This is for speed so we only have to get this data once per request.

            if (Request.IsAuthenticated)
            {
                // Load user data
                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(Convert.ToInt32 (User.Identity.Name));
                HttpContext.Current.Items["User"] = user;

                ArrayList ar = UsersUtility.GetUserRoleArray(user.Role);
                string[] roles = (string[])ar.ToArray(Type.GetType("System.String"));

                HttpContext.Current.User = new GenericPrincipal(User.Identity, roles);
            }
            else
            {
                User user = new User();
                HttpContext.Current.Items["User"] = user;
            }
        }

        /// <summary>
        /// Application_Error
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_Error(Object sender, EventArgs e)
        {
            string strURL = Request.Url.ToString();

            if (Server.GetLastError() is HttpException)
            {
                // Ignore known issue with Cutesoft TextEditor
                if (String.Compare("http://" + KanevaGlobals.SiteName + "/mykaneva/null", strURL, true) != 0)
                {
                    m_logger.Warn("HttpException from " + strURL, Server.GetLastError());
                }
            }
            else
            {
                m_logger.Error("Unhandled application error in " + strURL, Server.GetLastError());
            }
        }

        protected void Application_BeginRequest (Object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}