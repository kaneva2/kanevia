<%@ Page Language="C#" MasterPageFile="~/masterpages/SiteManagement.Master" AutoEventWireup="false" ValidateRequest="false" CodeBehind="Default.aspx.cs" Inherits="KlausEnt.KEP.SiteManagement.ManagementDefault" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="cnt_starsDetails" runat="server" ContentPlaceHolderID="cph_Body" >

        <asp:Label runat="server" id="Messages" visible="false"></asp:Label>
        <asp:PlaceHolder runat="server" id="ucShell" Visible="false"></asp:PlaceHolder>
        <rsweb:ReportViewer ID="ReportViewer1" Visible="false" ShowZoomControl="true" ShowFindControls="false" ZoomMode="PageWidth" runat="server">
        </rsweb:ReportViewer>
</asp:Content>