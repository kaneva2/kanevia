<%@ Page Language="C#" MasterPageFile="~/masterpages/SiteManagement.Master" AutoEventWireup="false" CodeBehind="login.aspx.cs" Inherits="KlausEnt.KEP.SiteManagement.login" %>

 <asp:Content ID="cnt_login" runat="server" ContentPlaceHolderID="cph_Body" >

    <script type="text/javascript">
    <!--
    WebForm_AutoFocus('_ctl0_cph_Body_txtUserName');// -->
    </script> 
      
	    <div style="width:980px; padding-left:10px; padding-top:20px">
		    <h3>Welcome To Site Management</h3>
			
		    <div style="width:400px; padding-left:10px; line-height:20px">
			    <fieldset title="Please Login Below to Get Started">
				    <legend>Please Login Below to Get Started</legend>
				    			
				    <table border="0" cellpadding="4px" cellspacing="2px" >
				        <tr>
				            <td><label for="email">Email:</label></td>
				            <td><input type="text" style="width: 220px;" class="biginput" maxlength="100" id="txtUserName" runat="server" name="txtUserName" /></td>
				        </tr>
				        <tr>
				            <td><label for="password">Password:</label></td>
				            <td><input type="password" style="width: 220px;" class="biginput" maxlength="30" id="txtPassword" runat="server" onkeydown="javascript:checkEnter(event)" name="txtPassword"></td>
				        </tr>
				        <tr>
				            <td colspan="2"><label for="password">&nbsp;</label><span title="Remember Password"><asp:checkbox runat="server" id="chkRememberLogin" tooltip="Remember Password"/></span> Remember my login info</td>
				        </tr>
				        <tr>
				            <td colspan="2"><asp:button id="imgLogin" runat="server" Text="Sign In" causesvalidation="False" alternatetext="Sign in to Kaneva" imageurl="~/images/login/btn_signin.gif" onclick="imgLogin_Click" border="0" TabIndex="2"/></td>
				        </tr>
				    </table>												
			    </fieldset>
		    </div>
			
            <div style="width:400px; padding-left:10px; line-height:20px">
	            <h2>Need Help?</h2>
	            <a runat="server" id="aLostPassword" target="_blank" style="vertical-align:top;">Forgot password?</a> Hint: The password for this site is the same as your kaneva.com password.
            </div>
            
            <div style="margin-top:40px; padding-left:10px; font-size:x-small; font-style:italic">
                <asp:Label runat="server" id="lbl_server" />
            </div>

        </div>

</asp:Content>