///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Web.Security;
using System.Security.Principal;
using log4net;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.SiteManagement
{
    public partial class login : BasePage
    {
        #region Declarations
        private string results = "";
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        protected login () 
		{
        }

		private void Page_Load (object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				// Set the focus
				SetFocus (txtUserName);

				if (Request.UrlReferrer != null)
				{
					ViewState ["rurl"] = Request.UrlReferrer.AbsoluteUri.ToString ();
				}

                lbl_server.Text = SiteManagementCommonFunctions.GetSiteName; // this.System.Environment.MachineName.ToString();
			}


            int userId = this.GetCurrentUser().UserId;

			string strJavascript = "<script language=\"JavaScript\">" +
				"function checkEnter(event){\n" +
				"if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13))\n" +
                "{event.returnValue=false;event.cancel=true;" + ClientScript.GetPostBackEventReference(this.imgLogin, "", false) + ";return false;}\n else \n{return true;}" +
				"}</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType (), "checkEnter"))
			{
                ClientScript.RegisterClientScriptBlock(GetType(), "checkEnter", strJavascript);	
			}
		}

		/// <summary>
		/// The login click event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        protected void imgLogin_Click(object sender, EventArgs e) 
		{
			// Try to log in here
			int roleMembership = 0;
			string email = Server.HtmlEncode (txtUserName.Value);
			string password = Server.HtmlEncode (txtPassword.Value);

			// May want to persist login info later.
			bool bPersistLogin = chkRememberLogin.Checked;

            int validLogin = AuthorizeUser(email, password, 0, ref roleMembership, Common.GetVisitorIPAddress(), true);

			switch (validLogin)       
			{       
				case 0:  
					results = "Not authenticated.";
					break; 
				case (int) Kaneva.Constants.eLOGIN_RESULTS.NOT_VALIDATED:
                case (int)Kaneva.Constants.eLOGIN_RESULTS.SUCCESS:  
					LoginUser (email, bPersistLogin);
					break;
                case (int)Kaneva.Constants.eLOGIN_RESULTS.USER_NOT_FOUND: 
					results = "Email address " + email + " was not found.";
					SetFocus (txtUserName);
					break;
                case (int)Kaneva.Constants.eLOGIN_RESULTS.INVALID_PASSWORD:  
				{
					m_logger.Warn ("Failed login (invalid password) for email '" + email + "' from IP " + Common.GetVisitorIPAddress());
					results = "Invalid password.";
					SetFocus (txtPassword);
					break;
				}
                case (int)Kaneva.Constants.eLOGIN_RESULTS.NO_GAME_ACCESS:   
					results = "No access to this game.";
					break;
                case (int)Kaneva.Constants.eLOGIN_RESULTS.ACCOUNT_DELETED:
					results = "This account has been deleted.";
					SetFocus (txtUserName);
					break;
                case (int)Kaneva.Constants.eLOGIN_RESULTS.ACCOUNT_LOCKED:
				{
					m_logger.Warn ("Locked account " + email + " tried to sign in from IP " + Common.GetVisitorIPAddress());
					results = "This account has been locked by the Kaneva administrator";
					SetFocus (txtUserName);
					break; 
				}
				default:            
					results = "Not authenticated.";           
					break;      
			}

			// Did they fail login?
			if (results.Length > 0)
			{
				// Show an alert
				string strScript = "<script language=JavaScript>";
				strScript += "alert(\"Login Failed: " + results + "\");";
				strScript += "</script>";

                if (!ClientScript.IsStartupScriptRegistered(GetType(), "invalidLogin"))
				{
                    ClientScript.RegisterStartupScript(GetType(), "invalidLogin", strScript);
				}
			}

		}

		/// <summary>
		/// LoginUser
		/// </summary>
		private void LoginUser (string email, bool bPersistLogin)
		{
            string url = ResolveUrl("~/Default.aspx");
            User user = this.GetUserFacade().GetUserByEmail(email);

            FormsAuthentication.SetAuthCookie(user.UserId.ToString(), bPersistLogin);
            this.UpdateLastLogin(user.UserId, Common.GetVisitorIPAddress(), Server.MachineName);

            // Set the userId in the session for keeping track of current users online
            Session["User"] = user;
            Session["userId"] = user.UserId;

            // Forward them somewhere
            if (Request["logretURL"] != null)
            {
                url = Server.UrlDecode(Request.QueryString.ToString()).Replace("logretURL=", "");
            }

            Response.Redirect(url);

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
