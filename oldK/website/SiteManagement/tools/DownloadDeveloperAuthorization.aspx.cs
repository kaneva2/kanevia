﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.SiteManagement
{
    public class DownloadDeveloperAuthorization : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			User user = GetCurrentUser();
            string systemName = Request["systemName"];
            string uuid = Request["id"];

            if (user == null || systemName == null || uuid == null)
            {
                Response.Write("<h2>Invalid request</h2>");
                return;
            }

            byte[] devAuthData = GetUserFacade().GetDeveloperAuthorizationFromDropBox(user.UserId, systemName, uuid);
            if (devAuthData ==null)
            {
                Response.Write("<h2>Authorization not found or expired</h2>");
                return;
            }

            Response.AddHeader("content-disposition", "attachment; filename=developer.dat");
            Response.ContentType = "application/binary";
            Response.BinaryWrite(devAuthData);
        }
    }
}