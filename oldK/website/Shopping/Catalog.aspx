<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="true" CodeBehind="Catalog.aspx.cs" Inherits="Kaneva.PresentationLayer.Shopping.Catalog" %>
<%@ Register TagPrefix="Shopping" TagName="Pager" Src="./usercontrols/Pager.ascx" %>

<asp:Content ID="cntUpload" runat="server" ContentPlaceHolderID="cphBody">
 <style>
        .img-center {display: table-cell; vertical-align: middle;background-position: center center;}
</style>  	
	<div id="content" class="browseWidth">
			<asp:Literal ID="litSoundStyle" runat="server"></asp:Literal>
			<ul id="browse">
			    <asp:Repeater ID="rptItems" runat="server">
                    <ItemTemplate>
                        <li class="img-center" style='<%# GetStyleImage(DataBinder.Eval (Container.DataItem, "ThumbnailLargePath").ToString (), DataBinder.Eval (Container.DataItem, "texturePath").ToString (), "la")%>'>
							<a id="A1" href='<%# GetItemDetailsURL (DataBinder.Eval (Container.DataItem, "GlobalId").ToString ())%>'>
							<%# DataBinder.Eval (Container.DataItem, "Name") %><span class="price"><%# DataBinder.Eval(Container.DataItem, "WebPrice")%></span></a>
							<div class="restricted" runat="server" visible='<%# IsAccessPass (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "PassTypeId")))%>'><!-- empty --></div>
							<div class="animation" runat="server" visible='<%# Convert.ToBoolean ( DataBinder.Eval(Container.DataItem, "IsAnimated")) %>'><!-- empty --></div>
                            <div class="smart" runat="server" visible='<%# BelongsToSmartObjectCategory ((Kaneva.BusinessLayer.BusinessObjects.WOKItem)Container.DataItem) %>'>Smart</div>
						</li>                       
                    </ItemTemplate>
                </asp:Repeater>
                
                <asp:Literal ID="litFiller" runat="server"></asp:Literal>
			</ul>
            <div id="pageList" class="clearit" style="width:97%;">
              <ul style="height:26px;">
                <li style="margin-top:4px;text-align:center;float:left;background-image:url(images/accessPassShopFilterImage.gif);width:251px;height:26px;" id="li_accessPassFilter" runat="server">
                    <asp:CheckBox id="cbx_AccessPass" runat="server" AutoPostBack="true" OnCheckedChanged="APCheckChanged" /> Show only <a id="lnk_AccesPass" target="_blank" href="#" runat="server" >Access Pass</a> items 
                </li>
                <li>
                    <Shopping:Pager runat="server" isajaxmode="False" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
                </li>
              </ul>
            </div>        
		    <div class="clear"></div>
		    <asp:Label id="lbl_NoCatalogItems" visible="false" runat="server"><span style="color:Navy; size:24pt; font-weight:bold">No Catalog Items Were Found.</span> </asp:Label>
		    
		    
		     <!-- r: start New Content -->
		
            <div class=" catCol1">
              <div id="sortNav">
                <ul>
                    <li class="header">Sort by:</li>                    
                    <li><asp:linkbutton text="Best Selling Last 30 Days" id="lbSelling30" commandname="b30" oncommand="lbSort_Click" tooltip="View results by best selling 30 Days" runat="server" /></li>
                    <li><asp:linkbutton text="Best Selling All time" id="lbBestSelling" commandname="b" oncommand="lbSort_Click" tooltip="View results by best selling" runat="server" /></li>
                    <li><asp:linkbutton text="Most Raved All Time" id="lbRaved" commandname="r" oncommand="lbSort_Click" tooltip="View results by most raved" runat="server" /></li>
                    <li><asp:linkbutton text="Most Raved Last 30 Days" id="lbRaved30" commandname="r30" oncommand="lbSort_Click" tooltip="View results by most raved 30 Days" runat="server" /></li>
                    <li><asp:linkbutton text="Price - High to Low" id="lbPriceHigh" commandname="ph" oncommand="lbSort_Click" tooltip="View results by higest price" runat="server" /></li>
                    <li><asp:linkbutton text="Price - Low to High" id="lbPriceLow" commandname="pl" oncommand="lbSort_Click" tooltip="View results by lowest price" runat="server" /></li>                                        
                    <li><asp:linkbutton text="Date - Newest First" id="lbNewest" commandname="n" oncommand="lbSort_Click" tooltip="View results by newest" runat="server" /></li>
                    <!-- <li><asp:linkbutton text="Date - Oldest First" id="lbOldest" commandname="o" oncommand="lbSort_Click" tooltip="View results by oldest" runat="server" /></li> -->
                    <li><asp:HyperLink text="Show Only Private" id="hlShowPriv" tooltip="Show only private" runat="server" Visible="false" /></li>
                    <li><asp:HyperLink text="Show Textures" id="hlShowTextures" tooltip="Show textures" runat="server" Visible="false" /></li>
                </ul>
              </div>
            </div>
            <asp:Panel ID="pnlUGC" runat="server" Visible="False">
          <div class="catCol2">
               <div class="catCopy_border">
                    
                        <div id="sortNav2">
                          <ul>
                            <li class="header">Kaneva Member&#8217;s Designs</li>
                            <li><asp:linkbutton text="View Latest Uploads" id="Linkbutton1" commandname="n" oncommand="lbSort_Click" tooltip="View Latest Uploads" runat="server" /></li>
                            <li><asp:linkbutton text="See Most Popular Items" id="Linkbutton2" commandname="b" oncommand="lbSort_Click" tooltip="View Most Popular Items" runat="server" /></li>
                          </ul>                
                        </div>
                        <div class="catCopy">
                          <p>Looking for items designed by a specific Kaneva member?</p>
                          <p>Type the designer&#8217;s Avatar name in the Search field at the top.</p>
                        </div>
                        <div class="clearit"><!-- clear the floats --></div>
                      </div>
		            
                <!-- r: End New Contemt -->
           </div>
           </asp:Panel>
          <div class="clearit"><!-- Clear the flaots --></div>
	 	
	  </div>	
    	

  </asp:Content>          

