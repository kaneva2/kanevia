///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KlausEnt.KEP.Kaneva;
using System.Data;
using System.Text;

namespace Kaneva.PresentationLayer.Shopping
{
    public class checkDb : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder output = new StringBuilder();
            DataTable dtUsers = UsersUtility.CheckDb();

            if (dtUsers != null)
            {
                for (int i = 0; i < dtUsers.Rows.Count; i++)
                {
                    output.Append(dtUsers.Rows[i]["username"].ToString() + "<BR>");
                }
            }
            else
            {
                output.Append("No results from db");
            }

            litUsernames.Text = output.ToString();
        }

        protected Literal litUsernames;
    }
}