///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
    public partial class BundleItemsData : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "text/plain";
            Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "no-cache");
            Response.Expires = -1;

            BindDeedItems();
        }

        private void BindDeedItems()
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            int gId = 0;

            try
            {
                gId = Convert.ToInt32(Request["gId"]);
            }
            catch (Exception)
            {
                gId = 0;
            }

            WOKItem deed = shoppingFacade.GetItem(gId);

            List<DeedItem> deedList = shoppingFacade.GetDeedSalesItemList(gId, (int)deed.ItemCreatorId);
            rptDeedItems.DataSource = deedList;
            rptDeedItems.DataBind();

            //divBundleItems.Visible = true;

            //divBundleListTitle.InnerText = "Included in this Pre Built World:";
        }

        protected string ShowBundleItemQuantity(object quantity, object useType)
        {
            try
            {
                if (((int)useType) == WOKItem.USE_TYPE_BUNDLE)
                {
                    return "";
                }

                return "(" + quantity.ToString() + ")";
            }
            catch
            { }

            return "";
        }
    }
}