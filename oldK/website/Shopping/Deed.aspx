﻿<%@ Page language="C#" masterpagefile="~/masterpages/MainTemplatePage.Master" autoeventwireup="true" codebehind="Deed.aspx.cs" inherits="Kaneva.PresentationLayer.Shopping.Deed" %>

<asp:Content id="Content1" contentplaceholderid="cphHeaderCSS" runat="server">
    <link href="./jscript/colorbox/colorbox.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content id="cntUpload" runat="server" contentplaceholderid="cphBody">
    <asp:ScriptManager id="smDeeds" runat="server" enablepartialrendering="true"></asp:ScriptManager>
    <asp:ValidationSummary showmessagebox="False" showsummary="True" id="valSum" class="errSum"
        displaymode="BulletList" runat="server" headertext="Please correct the following errors:" />
    <div id="divLoading">
        <table height="300" cellspacing="0" cellpadding="0" width="99%" border="0">
            <tr>
                <th class="loadingText" id="divLoadingText">
                    Loading...
                    <div style="display: none">
                        <img src="./images/progress_bar.gif">
                    </div>
                </th>
            </tr>
        </table>
    </div>
    <div id="divData" style="display: none" class="deedData">  
        <span id="requiredFieldMsg">* Fields marked with an asterisk (*) are required.</span>
        <div class="colWidth">
            <div class="formStyle">
                <h2 class="columnTitle">World Info</h2>
                <hr />
                <div class="required">
                    <label>World Name*</label>
                    <asp:RequiredFieldValidator id="rfvTitle" class="validator" runat="server" controltovalidate="txtTitle"
                        text="*" errormessage="World Name is a required field." display="static"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator id="revTitle" runat="server" display="Dynamic" text="*"
                        controltovalidate="txtTitle" enableclientscript="True" errormessage="World name should be at least four characters and can contain only letters, numbers, spaces or underscore"></asp:RegularExpressionValidator>
                    <br />
                    <asp:TextBox id="txtTitle" runat="server" width="342px" cssclass="inputText" maxlength="125"></asp:TextBox>
                </div>
                <div id="thumbnailEditContainer" class="required">
                    <label for="thumbnail_image">Thumbnail Image</label>
                    <asp:RegularExpressionValidator id="revThumbnail" class="validator1" runat="server"
                        text="*" errormessage="Thumbnail Image must be a .jpg, .jpeg, or .gif file type."
                        validationexpression="^.+\.([jJ][pP][gG]|[gG][iI][fF]|[jJ][pP][eE][gG])$" controltovalidate="inpThumbnail"
                        display="static"></asp:RegularExpressionValidator>
                    <br />
                    <img runat="server" id="imgItem" src="" alt="" clientidmode="Static" />                 
                    <div id="thumbnailEditButtons">
                        <input runat="server" id="inpThumbnail" type="file" size="15" clientidmode="Static" />
                        <input type="button" class="popFacebox photoBtn" value="Upload Image" />
                        <span class="formNote" id="spnUploadMsg">Upload a .jpg image from your computer.</span>
                        <input type="button" name="btnMediaLibrary" id="btnMediaLibrary" class="popFacebox photoBtn" value="Choose Image" />
                        <span class="formNote">Choose an image from your Media Library.</span>
                        <asp:HiddenField id="hfMediaLibPhoto" runat="server" clientidmode="Static" />
                    </div>
                </div>
                <div class="required">
                    <label for="description">Description*</label>
                    <asp:RequiredFieldValidator id="rfvDesc" runat="server" class="validator" controltovalidate="txtDescription"
                        text="*" errormessage="Description is a required field." display="static"></asp:RequiredFieldValidator>
                    <asp:TextBox id="txtDescription" runat="server" maxlength="300" textmode="MultiLine"
                        rows="4" columns="25" style="width:342px"></asp:TextBox>
                    <br />
                </div>
                <div>
                    <label>Blast</label><input runat="server" class="checkBox" id="chkBlastUpload" type="checkbox" />
                    <span class="formNote">
                        Blast item details to all your friends to let them know about
                        your new item available for purchase.
                    </span>
                </div>
            </div>
        </div>
        <div class="colWidth last">
            <asp:UpdateProgress id="udProgress" runat="server" associatedupdatepanelid="udpPricing" displayafter="0">
                <ProgressTemplate>
                    <img id="Img4" class="imgLoading" runat="server" src="~/images/ajax-loader.gif" alt="Loading..." border="0"/>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:updatepanel id="udpPricing" runat="server" childrenastriggers="true" class="formStyle">
                <ContentTemplate>
                    <h2 class="columnTitle">
                        World Pricing
                    </h2>
                    <hr />
                    <div>
                        <div id="divPricing" runat="server" class="pricing" visible="true">
                            <div class="row">
                                <span>Items Total</span>&nbsp;<asp:Literal id="litItemCount" runat="server" visible="false"></asp:Literal>&nbsp;
                                <asp:LinkButton id="btnShowItems" runat="server" onclick="btnShowItems_Click" text="view items" causesvalidation="false" visible="false"></asp:LinkButton>
                                <div id="totalNotOwnedItems" runat="server">0</div>
                            </div>
                            <div class="row">
                                <span>Kaneva Base Price</span>
                                <div id="totalOwnedItems" runat="server">0</div>
                            </div>
                            <div class="row">
                                <span>Your Commission (Kaneva Credits)</span>
                                <asp:RangeValidator id="rvPrice" class="validator1" controltovalidate="txtPrice"
                                    minimumvalue="0" maximumvalue="5000000" type="Integer" runat="server" errormessage="Commission must be a numeric value between 0 and 5,000,000."
                                    text="*" display="Static"></asp:RangeValidator>
                                <asp:TextBox id="txtPrice" maxlength="7" width="70px" cssclass="inputText" ontextchanged="txtPrice_TextChanged"
                                    autopostback="true" runat="server" text="0"></asp:TextBox>
                            </div>
                            <div class="row">
                                <span>Catalog Fee</span>
                                <div id="catalogFee" runat="server">0</div>
                            </div>
                            <div class="line">
                            </div>
                            <div class="row total">
                                <span>Total Price</span>
                                <div id="totalDeed" runat="server">0</div>
                            </div>
                            <div class="row total vip">
                                <span>VIP Price</span>
                                <div id="totalVIP" runat="server">0</div>
                            </div>
                            <p>Note: Pricing is in Credits or Rewards. You will receive your commission only if the world is bought using Credits.</p>
                            <p id="pCreatorMsg" runat="server" visible="false">Design commissions are removed from items you created in the world, so as to allow you to put a commission on the world.</p>
                        </div>
                        <div id="divDeedItems" class="deedItems pricing" runat="server" visible="false">
                            <div class="row">
                                <asp:Literal id="litItemCountHide" runat="server" text="Items in World (2134)"></asp:Literal>&nbsp;
                                <asp:LinkButton id="btnHideItems" runat="server" onclick="btnHideItems_Click" text="close" causesvalidation="false"></asp:LinkButton>
                                <br />
                            </div>
                            <div class="row columnHeader">
                                <span>Item</span>
                                <span class="priceColumn">Price</span>
                            </div>
                            <div class="iIContainer">
                                <asp:repeater id="rptDeedItems" runat="server">
                                    <ItemTemplate>
                                        <div class="iIRow">
							                <img src='<%# GetItemImageURL (DataBinder.Eval (Container.DataItem, "ThumbnailSmallPath").ToString (), "sm")%>' alt="" />
							                <a href='ItemDetails.aspx?gId=<%# DataBinder.Eval (Container.DataItem, "GlobalId") %>'><%# DataBinder.Eval (Container.DataItem, "DisplayName") %></a>&nbsp;
                                            (<%# DataBinder.Eval (Container.DataItem, "Quantity") %>)
                                            <span class="priceColumn"><%# DataBinder.Eval (Container.DataItem, "DeedItemPrice") %></span>
						                </div>
                                    </ItemTemplate>
                                </asp:repeater>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:updatepanel>
        </div>
        <div class="formBtns2 clearit">
            <asp:LinkButton id="btnSubmit" text="Upload World" class="btnxlarge orange inline"
                onclientclick="if (typeof(Page_ClientValidate) == 'function') if (!Page_ClientValidate()){return false;};CreateDeed();"
                onclick="btnAdd_Click" runat="server"></asp:LinkButton>
            &nbsp;
            <a id="btnCancel" class="btnxlarge orange inline" runat="server" href="~/MyInventory.aspx">
                Cancel</a>
        </div>
    </div>
</asp:Content>

<asp:Content id="Content2" contentplaceholderid="cphFooterJS" runat="server">
    <script type="text/javascript" src="./jscript/colorbox/jquery.colorbox-min.js"></script>
    <script type="text/javascript">
        var $j = jQuery.noConflict();

        $j(document).ready(function () {
            $j("#btnMediaLibrary").colorbox({ iframe: true, fastIframe: false, innerWidth: 566, innerHeight: 508, scrolling: false, overlayClose: false, closeButton: true, href: "MediaLibraryData.aspx", close: "&#10005;", closeTop: true });

            $j('#inpThumbnail').change(function () {
                PreviewThumbnailUpload("inpThumbnail", "spnUploadMsg", "imgItem");
            });
        });

        function CreateDeed() {
            ShowLoading(true, 'Please wait while your world is being created...<br/><img src="./images/progress_bar.gif">');
        }
    </script>
</asp:Content>
