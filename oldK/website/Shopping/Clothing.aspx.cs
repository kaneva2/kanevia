///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
    public class Clothing : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set text in search bar
            ((MainTemplatePage)Master).SearchTitle = "Clothing";
            ((MainTemplatePage)Master).SearchBoxText = "Clothing";

            Title = "Shop Kaneva - Clothing";

            if (!IsPostBack)
            {
                //bind the CatalogItems
                BindItemData();
            }
        }

        /// <summary>
        /// BindItemData
        /// </summary>
        private void BindItemData()
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            UInt32 categoryId = 3;
            bool showRestricted = false;

            if (Request["cId"] != null)
            {
                try
                {
                    categoryId = Convert.ToUInt32(Request["cId"]);
                }
                catch (Exception) { };
            }

            try
            {
                showRestricted = UserHasMaturePass();
            }
            catch (Exception) { }

            PagedList<WOKItem> plWokItems = shoppingFacade.SearchItems("", 0, categoryId, 8, 1, "date_added DESC", showRestricted, false, 0);
            rptItems.DataSource = plWokItems;
            rptItems.DataBind();
        }

        protected Repeater rptItems;

    }
}
