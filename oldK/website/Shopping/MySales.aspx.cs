///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
    public class MySales : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set text in search bar
            ((MainTemplatePage)Master).SearchTitle = "My Store";
            Title = "Shop Kaneva - My Store";

            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

            if (!IsPostBack)
            {
                BindPopularItems();
                BindSoldItems();
            }
        }

        /// <summary>
        /// BindSoldItems
        /// </summary>
        private void BindSoldItems()
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();

            // Weekly sales
            int credits = 0;
            int numberOfSales = 0;
            shoppingFacade.GetWeeklySales(KanevaWebGlobals.CurrentUser.UserId, ref credits, ref numberOfSales);
            lblCreditsThisWeek.Text = credits.ToString();
            lblItemsSoldThisWeek.Text = numberOfSales.ToString();
            
            //PagedList<ItemPurchase> plItems = shoppingFacade.GetItemPurchases (KanevaWebGlobals.CurrentUser.UserId, 100, 1);
            PagedList<WOKItem> plWokItems = shoppingFacade.GetOwnerItems("", KanevaWebGlobals.CurrentUser.UserId, 0, 250, 1, "");

            rptItems.DataSource = plWokItems;
            rptItems.DataBind();

            if (plWokItems.TotalCount.Equals(0))
            {
                lblNoSales.Visible = true;
            }
            else
            {
                lblNoSales.Visible = false;
            }
        }

        /// <summary>
        /// BindPopularItems
        /// </summary>
        private void BindPopularItems ()
        {
            int pgSize = 9;
            ShoppingFacade shoppingFacade = new ShoppingFacade();

            PagedList<WOKItem> plWokItems = shoppingFacade.GetOwnerItems("", KanevaWebGlobals.CurrentUser.UserId, 0, pgSize, 1, "iw.number_sold_on_web DESC");

            if (plWokItems.Count.Equals(0))
            {
                litFiller.Text = "<li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>";
            }
            else
            {
                rptPopular.DataSource = plWokItems;
                rptPopular.DataBind();

                StringBuilder sbFiller = new StringBuilder(200);

                for (int i = plWokItems.Count; i < pgSize; i++)
                {
                    sbFiller.Append("<li></li>");
                }

                litFiller.Text = sbFiller.ToString();
            }
        }

        protected Repeater rptItems, rptPopular;
        protected Literal litFiller;
        protected Label lblNoSales, lblItemsSoldThisWeek, lblCreditsThisWeek;
        
    }
}
