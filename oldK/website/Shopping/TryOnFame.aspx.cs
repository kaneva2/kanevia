///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
    public partial class TryOnFame : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FameFacade fameFacade = new FameFacade();
            ShoppingFacade shoppingFacade = new ShoppingFacade();

            WOKItem item = shoppingFacade.GetItem(GlobalId());
            //if (item.UseType == 0) //0 is clothing, 10,11 is furniture
            //{
                fameFacade.RedeemPacket(Convert.ToInt32(item.ItemCreatorId), (int)PacketIdFashion.TRY_ON, (int)FameTypes.Fashion);
            //}
        }

     /// <summary>
        /// GlobalId
        /// </summary>
        private int GlobalId()
        {
            int gId = 0;

            try
            {
                gId = Convert.ToInt32(Request["gId"]);
            }
            catch (Exception)
            {
                Response.Redirect("~/Catalog.aspx");
                Response.End ();
            }

            return gId;
        }
    }
}
