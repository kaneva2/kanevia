///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Title = "Shop Kaneva";

            aDesigner1.HRef = KanevaGlobals.DesignerSiteLink;
            aDesigner2.HRef = KanevaGlobals.DesignerSiteLink;

            // If they have been banned, log them out!
            if (Request.IsAuthenticated &&
                (KanevaWebGlobals.CurrentUser.StatusId.Equals((int)Constants.eUSER_STATUS.LOCKED) || KanevaWebGlobals.CurrentUser.StatusId.Equals((int)Constants.eUSER_STATUS.LOCKEDVALIDATED)))
            {
                System.Web.HttpCookie aCookie;
                int limit = Request.Cookies.Count - 1;

                for (int i = limit; i != -1; i--)
                {
                    aCookie = Request.Cookies[i];
                    aCookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(aCookie);
                }

                // Logout of single sign-on
                (new UserFacade()).CleanupSingleSignOn(KanevaWebGlobals.CurrentUser.UserId);

                FormsAuthentication.SignOut();
                Session.Abandon();

                Response.Redirect("~/default.aspx");
            }
        }
    }
}
