///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Security.Principal;

// Import log4net classes.
using log4net;
using log4net.Config;

using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

using System.Runtime.InteropServices;

namespace Kaneva.PresentationLayer.Shopping
{
    public class Global : System.Web.HttpApplication
    {
        /// <summary>
        /// Get the current cache
        /// </summary>
        /// <returns></returns>
        public static System.Web.Caching.Cache Cache()
        {
            return HttpRuntime.Cache;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct STARTUPINFO
        {
            public int cb;
            public String lpReserved;
            public String lpDesktop;
            public String lpTitle;
            public uint dwX;
            public uint dwY;
            public uint dwXSize;
            public uint dwYSize;
            public uint dwXCountChars;
            public uint dwYCountChars;
            public uint dwFillAttribute;
            public uint dwFlags;
            public short wShowWindow;
            public short cbReserved2;
            public IntPtr lpReserved2;
            public IntPtr hStdInput;
            public IntPtr hStdOutput;
            public IntPtr hStdError;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct PROCESS_INFORMATION
        {
            public IntPtr hProcess;
            public IntPtr hThread;
            public uint dwProcessId;
            public uint dwThreadId;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SECURITY_ATTRIBUTES
        {
            public int Length;
            public IntPtr lpSecurityDescriptor;
            public bool bInheritHandle;
        }

        [DllImport("kernel32.dll", EntryPoint = "CloseHandle", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public extern static bool CloseHandle(IntPtr handle);

        [DllImport("advapi32.dll", EntryPoint = "CreateProcessAsUser", SetLastError = true, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public extern static bool CreateProcessAsUser(IntPtr hToken, String lpApplicationName, String lpCommandLine, ref SECURITY_ATTRIBUTES lpProcessAttributes,
            ref SECURITY_ATTRIBUTES lpThreadAttributes, bool bInheritHandle, int dwCreationFlags, IntPtr lpEnvironment,
            String lpCurrentDirectory, ref STARTUPINFO lpStartupInfo, out PROCESS_INFORMATION lpProcessInformation);

        [DllImport("advapi32.dll", EntryPoint = "DuplicateTokenEx")]
        public extern static bool DuplicateTokenEx(IntPtr ExistingTokenHandle, uint dwDesiredAccess,
            ref SECURITY_ATTRIBUTES lpThreadAttributes, int TokenType,
            int ImpersonationLevel, ref IntPtr DuplicateTokenHandle);

        protected void Application_Start(object sender, EventArgs e)
        {
            // Change to log4net to be consistent with other modules
            XmlConfigurator.Configure(new System.IO.FileInfo(Server.MapPath("~") + "\\" + System.Configuration.ConfigurationManager.AppSettings["LogConfigFile"]));

            //////try
            //////{
            //////    //Create process
            //////    System.Diagnostics.ProcessStartInfo processInfo =
            //////        new System.Diagnostics.ProcessStartInfo(@"C:\\Development\\Teamsite\\Shopping\\bin\\nvdxt.exe", "-quality_production -dxt1c -file M_Shirts_000_UVs.jpg");

            //////    //System.Diagnostics.ProcessStartInfo processInfo =
            //////    //    new System.Diagnostics.ProcessStartInfo(@"C:\\Development\\Teamsite\\Shopping\\bin\\nvdxt.exe", "-quality_production -dxt1c -file M_Shirts_000_UVs.jpg -outdir \\\\kanevadev\\DevContentStore\\Textures\\4\\3000082");

            //////    //System.Diagnostics.ProcessStartInfo processInfo =     
            //////    //    new System.Diagnostics.ProcessStartInfo(@"C:\\Development\\Teamsite\\Shopping\\bin\\nvdxt.exe",
            //////    //    "-quality_production -dxt1c -file \\\\kanevadev\\DevContentStore\\Textures\\4\\3000082\\M_Shirts_000_UVs.jpg -outdir \\\\kanevadev\\DevContentStore\\Textures\\4\\3000082");
                

            //////    //Some other useful options if you want to capture the output to process in your app, good for command line apps
            //////    processInfo.UseShellExecute = false;
            //////    processInfo.CreateNoWindow = false;
            //////    processInfo.WorkingDirectory = @"C:\\Development\\Teamsite\\Shopping\\bin\\";
            //////    //processInfo.WorkingDirectory = @"\\\\kanevadev\\DevContentStore\\Textures\\4\\3000082\\";
            //////    //////processInfo.UserName = "wwwuser";
            //////    //////processInfo.Domain = "jboduch-lptp";
            //////    //////System.Security.SecureString password = new System.Security.SecureString();
            //////    //////string sPassword = "devel123*";
            //////    //////foreach (Char c in sPassword)
            //////    //////{
            //////    //////    password.AppendChar(c);
            //////    //////}

            //////    //////processInfo.Password = password;
            //////    //processInfo.RedirectStandardError = true;
            //////    //processInfo.RedirectStandardOutput = true;

            //////    System.Diagnostics.Process dxtProcess = System.Diagnostics.Process.Start(processInfo);
            //////    dxtProcess.WaitForExit();
            //////    int exitCode = dxtProcess.ExitCode;
            //////    dxtProcess.Close();

            //////    // Move the file
            //////    System.IO.File.Copy(@"C:\\Development\\Teamsite\\Shopping\\bin\\M_Shirts_000_UVs.dds", @"\\\\kanevadev\\DevContentStore\\Textures\\4\\3000082\\M_Shirts_000_UVs.dds");
            //////}
            //////catch (Exception exc)
            //////{

            //////}



            //////IntPtr Token = new IntPtr(0);
            //////IntPtr DupedToken = new IntPtr(0);
            //////bool ret;
            //////string test = WindowsIdentity.GetCurrent().Name.ToString();


            //////SECURITY_ATTRIBUTES sa = new SECURITY_ATTRIBUTES();
            //////sa.bInheritHandle = false;
            //////sa.Length = Marshal.SizeOf(sa);
            //////sa.lpSecurityDescriptor = (IntPtr)0;

            //////Token = WindowsIdentity.GetCurrent().Token;

            //////const uint GENERIC_ALL = 0x10000000;

            //////const int SecurityImpersonation = 2;
            //////const int TokenType = 1;

            //////ret = DuplicateTokenEx(Token, GENERIC_ALL, ref sa, SecurityImpersonation, TokenType, ref DupedToken);

            //////if (ret == false)
            //////    System.Diagnostics.Trace.Assert (true, "DuplicateTokenEx failed with " + Marshal.GetLastWin32Error());


            //////STARTUPINFO si = new STARTUPINFO();
            //////si.cb = Marshal.SizeOf(si);
            //////si.lpDesktop = "";

            //////string commandLinePath;
            //////commandLinePath = "C:\\Development\\Teamsite\\Shopping\\bin\\nvdxt.exe -quality_production -dxt1c -file M_Shirts_000_UVs.jpg";

            //////PROCESS_INFORMATION pi = new PROCESS_INFORMATION();
            //////ret = CreateProcessAsUser(DupedToken, null, commandLinePath, ref sa, ref sa, false, 0, (IntPtr)0, "c:\\", ref si, out pi);

            //////if (ret == false)
            //////    System.Diagnostics.Trace.Assert (true, "CreateProcessAsUser failed with " + Marshal.GetLastWin32Error());
            //////else
            //////{
            //////    CloseHandle(pi.hProcess);
            //////    CloseHandle(pi.hThread);
            //////}

            //////ret = CloseHandle(DupedToken);
            //////if (ret == false)
            //////    System.Diagnostics.Trace.Assert (true, Marshal.GetLastWin32Error().ToString ());

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Application_AuthenticateRequest
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            // Get the user information and save it to context for later.
            // This is for speed so we only have to get this data once per request.

            if (Request.IsAuthenticated)
            {
                // Load user data
                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(Convert.ToInt32(User.Identity.Name));
                HttpContext.Current.Items["User"] = user;

                ArrayList ar = UsersUtility.GetUserRoleArray(user.Role);
                string[] roles = (string[])ar.ToArray(Type.GetType("System.String"));

                HttpContext.Current.User = new GenericPrincipal(User.Identity, roles);
            }
            else
            {
                User user = new User();
                HttpContext.Current.Items["User"] = user;
            }
        }

        /// <summary>
        /// Application_Error
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_Error(Object sender, EventArgs e)
        {
            string strURL = Request.Url.ToString();
            m_logger.Error("Unhandled application error in " + strURL, Server.GetLastError());
        }

        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}