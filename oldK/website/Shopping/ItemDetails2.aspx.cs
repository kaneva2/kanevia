///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using System.Text.RegularExpressions;
using System.Xml;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;


namespace Kaneva.PresentationLayer.Shopping
{
    public partial class ItemDetails2 : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["gId"] == null)
            {
                Response.Redirect("~/Catalog.aspx");
            }

            // Per AS 11/10 Email titled "Good demo Thanks. Minor changes list:"
            // 3.	Back link should not start the processing again
            //clears local cache so data is always new
            Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "no-cache");
            Response.Expires = -1;

            int gId = GlobalId();

            // Had to add these to get them to hide correctly when displaying the buy this item section
            lbGetForFree.Visible = false;
            btnAddReview.Visible = false;
            lblDescription.Visible = false;

            // This redirect is done because the dance floor in the flash header is no longer
            // available so we redirect to the dance floor that is available for sale.  This
            // can be removed when the flash is updated.
            if (gId == 1836) { Response.Redirect("ItemDetails.aspx?gId=3324"); }

            if (!IsPostBack)
            {
                // Request tracking
                if (Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM] != null)
                {
                    int iResult = GetUserFacade.AcceptTrackingRequest(Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM], KanevaWebGlobals.CurrentUser.UserId);
                }

                UInt32 relatedCategoryId = 0;
                WOKItem item = GetShoppingFacade.GetItem(gId);
                ItemCreatorId = item.ItemCreatorId;

                try
                {
                    if (item.DisplayName.Length <= 0)
                    {
                        // invalid name gives lets us know this is an invalid item.
                        Response.Redirect("error.aspx");
                    }
                }
                catch
                {
                    Response.Redirect("error.aspx");
                }

                // Set the page title
                Title += " - " + item.Name;
                litShareItemName.Text = item.Name;

                // Don't allow user to comment if they have not already logged into wok.
                if (KanevaWebGlobals.CurrentUser.HasWOKAccount)
                {
                    btnAddReview.Visible = true;
                }

                // Add facebook Metadata
                HtmlMeta metaTag = new HtmlMeta();
                metaTag.Attributes.Add("property", "og:title");
                metaTag.Content = item.Name;
                Page.Header.Controls.Add(metaTag);
                metaTag = new HtmlMeta();
                metaTag.Attributes.Add("property", "og:type");
                metaTag.Content = "article";
                Page.Header.Controls.Add(metaTag);
                metaTag = new HtmlMeta();
                metaTag.Attributes.Add("property", "og:url");
                metaTag.Content = "http://" + KanevaGlobals.ShoppingSiteName + "/ItemDetails.aspx?gId=" + gId;
                Page.Header.Controls.Add(metaTag);
                metaTag = new HtmlMeta();
                metaTag.Attributes.Add("property", "og:image");
                metaTag.Content = StoreUtility.GetItemImageURL(item.ThumbnailAssetdetailsPath, "ad");

                Page.Header.Controls.Add(metaTag);
                metaTag = new HtmlMeta();
                metaTag.Attributes.Add("property", "og:site_name");
                metaTag.Content = "Kaneva";
                Page.Header.Controls.Add(metaTag);
                metaTag = new HtmlMeta();
                metaTag.Attributes.Add("property", "og:admins");
                metaTag.Content = "672338765";
                Page.Header.Controls.Add(metaTag);
                metaTag = new HtmlMeta();
                metaTag.Attributes.Add("property", "og:description");
                metaTag.Content = item.Description;

                bool showRestricted = false;

                //check to see if user can edit the page (i.e admin or owner
                if (Request.IsAuthenticated && item.ItemCreatorId.Equals((uint)KanevaWebGlobals.CurrentUser.UserId) || IsAdministrator())
                {
                    _canEdit = true;
                }

                //check to see if the user has the right to see the page
                if (!UserHasMaturePass() && item.PassTypeId.Equals((int)Constants.ePASS_TYPE.ACCESS) && !_canEdit)
                {
                    divDetails.Visible = false;
                    divAccessMessage.Visible = true;

                    if (!Request.IsAuthenticated)
                    {
                        h2Title.InnerText = "This Item is Restricted";

                        tdActionText.InnerHtml = "The item you are trying to access has been set to \"Restricted\" by its " +
                        "owner and may contain material inappropriate for anyone under the age of 18. Please log in to verify your right " +
                        "to access this page.";

                        tdPolicyText.Visible = false;

                        btnAction.Text = " Sign in ";
                        btnAction.Attributes.Add("onclick", "location.href='" + GetLoginURL() + "';return false;");

                        divAccessMessage.Visible = true;
                    }
                    else if (KanevaWebGlobals.CurrentUser.IsAdult)   // user is 18 or over
                    {
                        h2Title.InnerText = "This item can only be seen by people with an Access Pass";

                        tdActionText.InnerHtml = "The item's creator has taken advantage of our Access Pass subscription " +
                            "and has set this item so that it can only be seen by other people with Access Pass. " +
                            "<br/><br/>What are you waiting for? Get Yours now. <br />";
                        tdActionText2.InnerHtml = "Kaneva's Access Pass includes: <br /><ul><li style=\"line-height:11px; font-size:11px; list-style-type:disc\">Access to restricted (18 and over) clubs and hangouts</li>" +
                                "<li style=\"line-height:11px; font-size:11px; list-style-type:disc\">Exclusive clothing and accessories for restricted areas</li><li style=\"line-height:11px; font-size:11px; list-style-type:disc\">Special privileges only Access Pass members enjoy</li>" +
                                "<li style=\"line-height:11px; font-size:11px; list-style-type:disc\">Access to private events</li><li style=\"line-height:11px; font-size:11px; list-style-type:disc\">And more!</li><br />";

                        tdPolicyText.Visible = false;


                        btnAction.Text = "Purchase Access Pass";
                        btnAction.Attributes.Add("onclick", "javacript:location.href='" + ResolveUrl("~/mykaneva/passDetails.aspx?pass=true&passID=" + KanevaGlobals.AccessPassGroupID) + "';return false;");

                        divAccessMessage.Visible = true;
                    }
                    else   // user is under 18 or age unknown
                    {
                        h2Title.InnerText = "This item is Restricted";

                        tdActionText.InnerHtml = "The item you are trying to access has been set to \"Restricted\" by its " +
                            "owner and may contain material inappropriate for anyone under the age of 18.<br/><br/>" +
                            "NOTE:  If you believe you are receiving this page in error, please contact support via the Support Page.";

                        tdPolicyText.Visible = false;

                        btnAction.Text = " Go Back ";
                        btnAction.Attributes.Add("onclick", "javacript:history.back();return false;");

                        divAccessMessage.Visible = true;
                    }
                }
                else
                {
                    divDetails.Visible = true;
                    divAccessMessage.Visible = false;

                    if (item.ItemActive.Equals((int)WOKItem.ItemActiveStates.Deleted) || item.UseType == WOKItem.USE_TYPE_PREMIUM)
                    {
                        ShowErrorOnStartup("This item is not available for purchase.", false);

                        // Allow GM's to view the deleted product but still allow the pop up. 
                        // Everyone else fails and the item is not loaded.
                        if (!IsAdministrator())
                        {
                            return;
                        }
                    }

                    // Make sure only only privates can update it
                    if (item.ItemActive.Equals((int)WOKItem.ItemActiveStates.Private))
                    {
                        if (!(item.ItemCreatorId.Equals(Convert.ToUInt32(KanevaWebGlobals.CurrentUser.UserId))) && !IsAdministrator())
                        {
                            Response.Redirect("~/Catalog.aspx");
                            Response.End();
                        }
                    }

                    lblName.Text = item.Name;
                    divRestricted.Visible = item.PassTypeId.Equals((int)Constants.ePASS_TYPE.ACCESS);

                    // if this is a sound use the sound fpo image
                    // Sound is use_type = 10 and item_parameters.value < 0 where param_type_id = 10
                    bool isSound = false;

                    if (item.UseType == (int)WOKItem.USE_TYPE_ADD_DYN_OBJ)
                    {
                        ItemParameter itemParamter = GetShoppingFacade.GetItemParameter(gId, (int)WOKItem.USE_TYPE_ADD_DYN_OBJ);
                        if (itemParamter != null && KanevaGlobals.IsNumeric(itemParamter.ParamValue))
                        {
                            isSound = (Convert.ToInt32(itemParamter.ParamValue) < 0);
                        }
                    }

                    if (isSound)
                    {
                        imgItem.Src = StoreUtility.GetItemImageURL(item.ThumbnailAssetdetailsPath, "ad", "Sound");
                    }
                    else
                    {
                        if (item.UseType == 200)
                        {
                            ifItemPreview.Attributes["src"] = "ItemPreview.aspx?gId=" + gId;
                            ifItemPreview.Visible = true;
                            imgItem.Visible = false;
                        }

                        imgItem.Src = StoreUtility.GetItemImageURL(item.ThumbnailAssetdetailsPath, "ad");
                    }

                    lblDescription.Text = item.Description;
                    lblDescription.Visible = true;
                    litRaveCount.Text = GetRaveFacade.GetUGCRaveCount(gId).ToString();
                    txtQuantity.Text = "1";

                    List<ItemCategory> icCats = GetShoppingFacade.GetItemCategoriesByItemId(gId);
                    BindItemCategories(icCats);

                    foreach (ItemCategory iCat in icCats)
                    {
                        if (iCat.ItemCategoryId > relatedCategoryId)
                        {
                            relatedCategoryId = iCat.ItemCategoryId;
                        }
                    }

                    if (item.InventoryType.Equals(CURRENCY_TYPE_REWARDS) || item.InventoryType.Equals(CURRENCY_TYPE_BOTH_R_C))
                    {
                        lblPurchaseType.Text = "Rewards";
                        lbCompleteRewards.Visible = true;
                    }
                    else
                    {
                        lblPurchaseType.Text = "Credits";
                        lbCompleteRewards.Visible = false;
                    }

                    rptItems.DataSource = GetShoppingFacade.GetRelatedItems(item, relatedCategoryId, 8, 1, showRestricted);
                    rptItems.DataBind();

                    // Owner username
                    if (!item.IsKanevaOwned)
                    {
                        User user = GetUserFacade.GetUser(Convert.ToInt32(item.ItemCreatorId));
                        lblUsername.Text = user.Username;
                        aUser.HRef = GetPersonalChannelUrl(user.NameNoSpaces);
                    }
                    else
                    {
                        lblUsername.Text = "Kaneva";
                        aUser.HRef = "#";
                    }

                    //Report Abuse
                    lbRpt.Attributes.Add("onclick", "javascript: window.open('" + "http://" + KanevaGlobals.SiteName + "/suggestions.aspx?mode=WB&category=KANEVA%20Web%20Site&rurl=" + Server.UrlEncode(Request.Url.ToString()) + "','add','toolbar=no,menubar=no,scrollbars=yes');");
                    aAdminEdit.Visible = IsAdministrator();
                    aAdminEdit.HRef = "http://" + KanevaGlobals.SiteManagementName + "/ManagementDefault.aspx?ws=3&mn=8&sn=7&itemId=" + gId;

                    bool isItemOwner = (item.ItemCreatorId.Equals(Convert.ToUInt32(KanevaWebGlobals.CurrentUser.UserId)) && KanevaWebGlobals.CurrentUser.UserId > 0);
                    aOwnerEdit.Visible = isItemOwner;
                    aOwnerEdit.HRef = ResolveUrl("~/ItemEdit.aspx?gId=" + gId.ToString());

                    aTransTracker.Visible = isItemOwner;
                    aTransTracker.HRef = ResolveUrl("~/MySales.aspx");

                    if (isItemOwner)
                    {
                        divOwnerBtns.Visible = true;
                    }

                    // Try on button
                    if (item.UseType == WOKItem.USE_TYPE_CUSTOM_DEED)
                    {
                        // Per brett meet me 3d

                        // Look up the community id based on template id
                        int communityId = GetShoppingFacade.GetCommunityIdFromDeedTemplateId(gId);
                        if (communityId > 0)
                        {
                            Community c = new CommunityFacade().GetCommunity(Convert.ToInt32(communityId));
                            string stpUrl = StpUrl.MakeUrlPrefix(KanevaGlobals.WokGameId.ToString()); // default stp url

                            if (c.CommunityId > 0)
                            { // this is a regular community
                                stpUrl += "/" + StpUrl.MakeCommunityUrlPath(c.Name);
                            }
                            else
                            {
                                // shouldn't ever happen, only if caller bad
                                Response.Write("<script type=\"text/javascript\">alert('Invalid destination for Community Url: " + communityId + "' );</script>");
                                return;
                            }

                            string playNowGuid = Guid.NewGuid().ToString();
                            stpUrl = System.Web.HttpUtility.UrlPathEncode(stpUrl);
                            btn_preview.Attributes.Add("onclick", "javascript:TryOnDeed('" + stpUrl + "'," + KanevaGlobals.WokGameId.ToString() + ",'" + KanevaWebGlobals.CurrentUser.Email + "','" + KanevaGlobals.CurrentPatcherVersion.ToString() + "', '" + playNowGuid + "');jQuery.ajax('http://" + KanevaGlobals.ShoppingSiteName + "/TryOnFame.aspx', {data: {gId: '" + gId.ToString() + "'}}); return false;");
                        }
                    }
                    else
                    {
                        string playNowGuid = Guid.NewGuid().ToString();
                        btn_preview.Attributes.Add("onclick", "javascript:TryOn(" + KanevaGlobals.WokGameId.ToString() + "," + gId.ToString() + "," + item.UseType.ToString() + ",'" + KanevaWebGlobals.CurrentUser.Email + "','" + KanevaGlobals.CurrentPatcherVersion.ToString() + "', '" + playNowGuid + "');jQuery.ajax('http://" + KanevaGlobals.ShoppingSiteName + "/TryOnFame.aspx', {data: {gId: '" + gId.ToString() + "'}}); return false;");
                    }
                }

                litGlobalId.Text = gId.ToString();

                // Animations
                if (item.UseType == WOKItem.USE_TYPE_ANIMATION)
                {
                    WOKItem actor = GetShoppingFacade.GetAnimationActor(gId);
                    if (actor.GlobalId > 0)
                    {
                        aReqItem.HRef = "ItemDetails.aspx?gId=" + actor.GlobalId;
                        aReqItem.InnerText = actor.DisplayName;

                        divRequiredItem.Visible = true;

                        // Turn off try-on button for ADO animations
                        btn_preview.Style.Add("visibility", "hidden");
                    }
                }
                // Script Game Items
                else if (item.UseType == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM)
                {
                    // Turn off try-on button for game items (for now)
                    btn_preview.Style.Add("visibility", "hidden");
                }
                // Particles
                else if (item.UseType == WOKItem.USE_TYPE_PARTICLE)
                {
                    // Turn off try-on button for particles (for now)
                    btn_preview.Style.Add("visibility", "hidden");
                }
                else if (item.UseType == WOKItem.USE_TYPE_ADD_DYN_OBJ || item.UseType == WOKItem.USE_TYPE_EQUIP)
                {
                    bool showPrivate = false;
                    string filter = "";

                    if (_canEdit)
                    {
                        // owner and admins can see all animations
                        showPrivate = true;
                    }
                    else if (!UserHasMaturePass())
                    {
                        // only show general-access animations if Non-AP user (NOTE: do we want VIP support here?)
                        filter = "IFNULL(pgi.pass_group_id,0) <> " + (int)Constants.ePASS_TYPE.ACCESS;
                    }

                    List<WOKItem> animations = GetShoppingFacade.GetItemAnimations(gId, filter, showPrivate);
                    if (animations.Count > 0)
                    {
                        rptAnimations.DataSource = animations;
                        rptAnimations.DataBind();

                        tabAnimations.Visible = true;
                        //for (int i = 0; i < animations.Count; i++)
                        //{
                        //    divAnimations.InnerHtml += "<a href=\"ItemDetails.aspx?gId=" +
                        //      animations[i].GlobalId + "\">" + animations[i].DisplayName + "</a>";

                        //    if (i < animations.Count - 1) { divAnimations.InnerHtml += ";  "; }
                        //}
                        // This property is not set when object is created, so set it here
                        item.isAnimated = true;
                    }
                }
                else if (item.UseType == WOKItem.USE_TYPE_CUSTOM_DEED)
                {
                    tabDeedItems.Visible = true;

                    // Load via Ajax
                    litLoadDeedItems.Text = "$j.ajax({url: 'BundleItemsData.aspx?gId=" + gId + "', success: function(result){" +
                         " $j('#divDeedItemsData').html(result); " +
                     "}});";

                }
                else if (item.UseType == WOKItem.USE_TYPE_BUNDLE)
                {
                    if (item.BundleItems.Count > 0)
                    {
                        rptBundleItems.DataSource = item.BundleItems;
                        rptBundleItems.DataBind();
                        tabBundleItems.Visible = true;

                        aTabBundles.InnerText = "Included in this Bundle";
                    }

                    // Turn off try-on button for bundles
                    btn_preview.Style.Add("visibility", "hidden");
                }

                // Check to see if this item belongs to any bundles
                if (item.UseType != WOKItem.USE_TYPE_BUNDLE)
                {
                    List<WOKItem> bundles = GetShoppingFacade.GetBundlesItemBelongsTo(item.GlobalId, "");
                    if (bundles.Count > 0)
                    {
                        rptBundleItems.DataSource = bundles;
                        rptBundleItems.DataBind();
                        tabBundleItems.Visible = true;

                        aTabBundles.InnerText = "Bundles with this Item";
                    }
                }

                if (GetUserFacade.IsUserBlocked((int)item.ItemCreatorId, KanevaWebGlobals.CurrentUser.UserId))
                {
                    txtComment.Disabled = true;
                    lbSubmitComment.Visible = false;
                }

                SetItemTypeIcon(item);

                // Reviews
                ShowTabs(gId);

                // Pricing
                // Save the price in case price changes upon buy
                OriginalPrice = GetItemCheckoutPrice(item, KanevaWebGlobals.CurrentUser.HasVIPPass);

                lblPrice.Text = item.WebPrice.ToString("N0");

                lblVIPPurchaseType.Text = lblPurchaseType.Text;

                var vipPrice = GetItemCheckoutPrice(item, true);
                var one = item.WebPrice - vipPrice;
                decimal two = ((decimal)one / (decimal)item.WebPrice);
                decimal three = two * 100;

                decimal four = ((decimal)one / (decimal)item.WebPrice) * 100;

                int savePct = (int)(((decimal)one / (decimal)item.WebPrice) * 100);

                if (KanevaWebGlobals.CurrentUser.HasVIPPass)
                {
                    buyVIPContainer.Visible = false;

                    //var savePct = Math.Round( (decimal)((item.WebPrice-item.WebPriceVIP) / item.WebPrice)*100, 0 );


                    divVIPSave.InnerText = "You Save " + savePct + "%!";
                    divVIPSave.Visible = true;
                    divVIPSave.Style.Add("float", "none");
                    lnkVIP.Visible = false;
                    pricecontainer.Visible = true;
                    litIsVIP.Text = "true";
                    divListPriceLabel.InnerText = "List Price:";
                    divListPriceLabel.Style.Add("color", "#ccc");
                    listPrice.Style.Add("color", "#ccc");

                    divVIPPriceLabel.Style.Add("width", "90px");
                    vippricecontainer.Style.Add("margin-left", "0");
                    vippricecontainer.Style.Add("font-weight", "bold");
                    h3VIP.Style.Add("background-color", "#fff");
                }
                else
                {   // Only show if user does not have VIP
                    aBuyVIP.HRef = "http://" + KanevaGlobals.SiteName + "/mykaneva/passDetailsVIPb.aspx?pass=true&passId=74";
                    buyVIPContainer.Visible = true;
                    lnkVIP.HRef = "http://" + KanevaGlobals.SiteName + "/mykaneva/passDetailsVIPb.aspx?pass=true&passId=74";
                    lnkVIP.Visible = true;
                    divVIPSave.Visible = false;
                    litIsVIP.Text = "false";
                    divListPriceLabel.InnerText = "Your Price:";
                    divListPriceLabel.Style.Add("color", "");
                    listPrice.Style.Add("color", "#997b62");
                    lblPurchaseType.Style.Add("color", "#997b62");

                    divVIPSave.InnerText = "VIP saves you " + savePct + "%!";
                    divVIPSave.Visible = true;

                }

                // Check if user is admin, if so then show purchase for free button
                if (UserIsGM)
                {
                    lbGetForFree.Visible = true;
                    pnlBuy.Visible = true;
                    pnlBuy.Style.Add("display", "none");
                    litGMPurchase.Text = "function GMPurchase() " +
                        "{ " +
                        "    $j('#spnCreditBalance').hide(); " +
                        "    $j('#pnlRelated').hide(); " +
                        "    $j('#pnlComplete').hide(); " +
                        "    $j('#pnlCompleteZonePurchase').hide(); " +
                        "    $j('#divCol2').hide(); " +
                        "    $j('#pnlBuy').show(); " +
                        "    ProcessOrder(); " +
                        "}";
                }
            }
        }

        #region Helper Methods

        private void ShowTabs(int gId)
        {
            if (tabBundleItems.Visible)
            {
                liTabBundleItems.Visible = true;
            }
            else if (tabDeedItems.Visible)
            {
                liTabDeedItems.Visible = true;
            }
            else if (tabAnimations.Visible)
            {
                liTabAnimations.Visible = true;
            }

            BindReviews(gId, 1, 50);
        }

        protected string ShowBundleItemQuantity(object quantity, object useType)
        {
            try
            {
                if (((int)useType) == WOKItem.USE_TYPE_BUNDLE)
                {
                    return "";
                }

                return "(" + quantity.ToString() + ")";
            }
            catch
            { }

            return "";
        }

        private int GetQuantity()
        {
            int quantity = 1;

            try
            {
                quantity = Convert.ToInt32(Server.HtmlEncode(txtQuantity.Text));
            }
            catch (Exception)
            {
                quantity = 1;
            }

            if (quantity > 1000)
            {
                quantity = 1000;
            }

            return quantity;
        }

        /// <summary>
        /// CompletePurchase
        /// </summary>
        private void CompletePurchase(bool bRewards)
        {
            int gId = GlobalId();
            WOKItem item = GetShoppingFacade.GetItem(gId);
            int quantity = GetQuantity();

            msgDialog.Title = "Purchase Summary";
            msgDialog.MessageHeader = "Warning:";

            bool isCustomDeed = (item.UseType == WOKItem.USE_TYPE_CUSTOM_DEED);
            bool isScriptGameItem = (item.UseType == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM);

            if (isCustomDeed)
            {
                // Hard code quantity
                quantity = 1;
            }

            if (isScriptGameItem)
            {
                // Hard code quantity
                quantity = 1;
            }

            int successfulPayment = -1;
            int wokTransactionLogId = 0;
            int itemCheckoutPrice = GetItemCheckoutPrice(item);
            int iOrderTotal = itemCheckoutPrice * quantity;

            if (isCustomDeed)
            {
                // Validate community name if new
                if (radImport.Checked)
                {
                    // No validation because we do auto-naming of community
                }

                // Validate World name if new
                if (radNew3DApp.Checked)
                {
                    if (!ValidateApplyZoneFields())
                    {
                        CancelPurchase();
                        return;
                    }
                }

                if (radExisting.Checked)
                {
                    // Get the world to which they want to apply the new zone
                    Community community = GetCommunityFacade.GetCommunity(Convert.ToInt32(ddlWorlds.SelectedValue));

                    if (community.CommunityId == 0)
                    {
                        CancelPurchase();
                        msgDialog.Message = "User world does not exist";
                        ShowErrorMessageDialog(msgDialog);
                        return;
                    }
                }
            }

            // Make sure it is a valid quantity
            if (quantity < 1)
            {
                // Redisplays the purchase button since they were hidden when purchase initiated by user
                ResetPurchaseButtons();
                msgDialog.Message = "Quantity must be greater than 0";
                ShowErrorMessageDialog(msgDialog);
                return;
            }

            if (item.ItemActive.Equals((int)WOKItem.ItemActiveStates.Deleted))
            {
                ResetPurchaseButtons();
                msgDialog.Message = "This item is not available for purchase.";
                ShowErrorMessageDialog(msgDialog);
                return;
            }

            // Make sure the price has not changed
            if (!itemCheckoutPrice.Equals(OriginalPrice))
            {
                ResetPurchaseButtons();
                msgDialog.Message = "This price of this item has been changed. Please refresh your browser to see the new price.";
                ShowErrorMessageDialog(msgDialog);
                return;
            }

            // Make sure this item can be purchased with rewards
            if (bRewards)
            {
                // Make sure they have enough rewards
                UInt64 iRewards = Convert.ToUInt64(UsersUtility.getUserBalance(KanevaWebGlobals.CurrentUser.UserId, Constants.CURR_GPOINT).ToString());

                if ((iRewards < Convert.ToUInt64(iOrderTotal)))
                {
                    ResetPurchaseButtons();
                    msgDialog.Message = "You do not have enough Rewards to purchase this item";
                    ShowErrorMessageDialog(msgDialog);
                    return;
                }

                // Make sure it is correct currency type for rewards.
                if (!item.InventoryType.Equals(CURRENCY_TYPE_REWARDS) && !item.InventoryType.Equals(CURRENCY_TYPE_BOTH_R_C))
                {
                    ResetPurchaseButtons();
                    msgDialog.Message = "This item cannot be purchased with Rewards.";
                    ShowErrorMessageDialog(msgDialog);
                    return;
                }
            }
            else
            {
                // Does the user have enough credits?
                UInt64 iCredits = Convert.ToUInt64(UsersUtility.GetUserPointTotal(KanevaWebGlobals.CurrentUser.UserId).ToString());

                if ((iCredits < Convert.ToUInt64(iOrderTotal)))
                {
                    ResetPurchaseButtons();
                    msgDialog.Message = "You do not have enough Credits to purchase this item." +
                       " Would you like to purchase more Credits?";
                    msgDialog.OkButtonUrl = "http://" + KanevaGlobals.SiteName + "/mykaneva/buyCreditPackages.aspx";
                    ShowErrorMessageDialog(msgDialog);
                    return;
                }
            }

            try
            {
                if (bRewards)
                {
                    successfulPayment = GetUserFacade.AdjustUserBalanceWithDetails(KanevaWebGlobals.CurrentUser.UserId, Constants.CURR_GPOINT, -Convert.ToDouble(iOrderTotal), Constants.CASH_TT_BOUGHT_ITEM_ON_WEB, gId, quantity, ref wokTransactionLogId);
                }
                else
                {
                    successfulPayment = GetUserFacade.AdjustUserBalanceWithDetails(KanevaWebGlobals.CurrentUser.UserId, Constants.CURR_KPOINT, -Convert.ToDouble(iOrderTotal), Constants.CASH_TT_BOUGHT_ITEM_ON_WEB, gId, quantity, ref wokTransactionLogId);
                }
            }
            catch (Exception)
            {
                ResetPurchaseButtons();
                msgDialog.Message = "You do not have enough Credits to purchase this item." +
                    " Would you like to purchase more Credits?";
                msgDialog.OkButtonUrl = "http://" + KanevaGlobals.SiteName + "/mykaneva/buyCreditPackages.aspx";
                ShowErrorMessageDialog(msgDialog);
                return;
            }

            if (successfulPayment == 0)  //0 is success according to AdjustUserBalance notes
            {
                // Purhcase the bundle or item
                if (item.UseType == (int)WOKItem.USE_TYPE_CUSTOM_DEED)
                {
                    if (!PurchaseDeed(item, quantity, bRewards, iOrderTotal))
                    {
                        return;
                    }
                }
                else if (item.UseType == (int)WOKItem.USE_TYPE_BUNDLE)
                {
                    PurchaseBundle(item, quantity, bRewards, iOrderTotal, 0);
                }
                else if (item.UseType == (int)WOKItem.USE_TYPE_SCRIPT_GAME_ITEM)
                {
                    PurchaseScriptGameItem(item, quantity, bRewards, iOrderTotal);
                }
                else
                {
                    PurchaseItem(item, quantity, bRewards, iOrderTotal, 0, true, true, false);
                }

                // Add Google Ecommerce Tracking here
                if (bRewards)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "rewardtrack", "LogGoogleAnalyticsCommerce('" + KanevaGlobals.GoogleAnalyticsAccountRewards + "', " +
                        " '" + wokTransactionLogId.ToString() + "', '" + iOrderTotal.ToString() + "', '', '', '" + KanevaWebGlobals.CurrentUser.Country + "', " +
                        " '" + item.GlobalId.ToString() + "', '" + item.Name.Replace("\"", "").Replace("'", "").ToString() + "', '" + item.Category1 + "-" + item.Category2 + "-" + item.Category3 + "', " +
                        " '" + itemCheckoutPrice.ToString() + "', '" + quantity.ToString() + "');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "credittrack", "LogGoogleAnalyticsCommerce('" + KanevaGlobals.GoogleAnalyticsAccountCredits + "', " +
                        " '" + wokTransactionLogId.ToString() + "', '" + iOrderTotal.ToString() + "', '', '', '" + KanevaWebGlobals.CurrentUser.Country + "', " +
                        " '" + item.GlobalId.ToString() + "', '" + item.Name.Replace("\"", "").Replace("'", "").ToString() + "', '" + item.Category1 + "-" + item.Category2 + "-" + item.Category3 + "', " +
                        " '" + itemCheckoutPrice.ToString() + "', '" + quantity.ToString() + "');", true);
                }

                // Blast if they wanted to Blast
                if (chkBlastPurchase.Checked)
                {
                    try
                    {
                        // Metrics Tracking
                        // Metrics tracking for accepts
                        UserFacade userFacade = new UserFacade();
                        string requestId = userFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_SHOP_PURCHASE, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
                        string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_BLAST, 0);

                        //string url = "<a href=\"http://" + KanevaGlobals.SiteName + "/ItemDetails.aspx?gId=" + item.GlobalId + "\"><img src=\"" + StoreUtility.GetItemImageURL(item.ThumbnailLargePath, "la") + "\"/></a>";
                        string url = "http://" + KanevaGlobals.ShoppingSiteName + "/ItemDetails.aspx?gId=" + item.GlobalId + "&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeSentIdBlast;
                        string itemThumbnail = StoreUtility.GetItemImageURL(item.ThumbnailLargePath, "la");

                        BlastFacade blastFacade = new BlastFacade();
                        blastFacade.SendShopPurchaseItemBlast(KanevaWebGlobals.CurrentUser.UserId, KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.NameNoSpaces,
                            item.DisplayName, item.Description, url, itemThumbnail, "", KanevaWebGlobals.CurrentUser.ThumbnailSmallPath);

                    }
                    catch (Exception ex)
                    {
                        m_logger.Error("Blasting Item Purchase Failed. Item:" + item.GlobalId + " UserId: " + KanevaWebGlobals.CurrentUser.UserId + " " + ex.ToString());
                    }
                }

                // Find which server user is on and tickle that server for inventory update
                try
                {
                    // return server user is currently on
                    DataTable dt = new GameFacade().GetGameUserIn(KanevaWebGlobals.CurrentUser.UserId);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        // Check to see if user is GM and send event to server which will make the item available to the appserver
                        int gameId = Convert.ToInt32(dt.Rows[0]["game_id"]);
                        int serverId = Convert.ToInt32(dt.Rows[0]["server_id"]);
                        (new RemoteEventSender()).broadcastItemChangeEventToServer(RemoteEventSender.ObjectType.PlayerInventory, RemoteEventSender.ChangeType.AddObject, KanevaWebGlobals.CurrentUser.UserId, serverId);
                    }
                }
                catch (Exception exc)
                {
                    m_logger.Error("Tickler failure buying item", exc);
                }
            }
            else
            {
                ResetPurchaseButtons();
                msgDialog.Message = "There was a problem processing your payment.  Please try purchase again.";
                ShowErrorMessageDialog(msgDialog);
                return;
            }

            pnlRelated.Visible = false;
            pnlBuy.Visible = false;
            pnlComplete.Visible = true;
            litSuccessMessage.Text = string.Format("<p>You can find your new purchase in <span>Inventory{0}</span>.</p>", item.UseType == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM ? " in the Gaming tab" : "");
            pnlCompleteZonePurchase.Visible = false;

            if (item.UseType == WOKItem.USE_TYPE_CUSTOM_DEED)
            {
                pnlComplete.Visible = false;
                pnlCompleteZonePurchase.Visible = true;

                // Track the page in Google Analytics
                if (radNew3DApp.Checked)
                {
                    // Log world creation as well as purchase
                    ScriptManager.RegisterStartupScript(this, GetType(), "worldcreate", "logPageView('" + KanevaGlobals.GoogleAnalyticsAccountWorlds + "', '.kaneva.com', '/Shop');", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "zonepurchase", "logPageView('" + KanevaGlobals.GoogleAnalyticsAccountZonePurchase + "', '.kaneva.com', '/NewWorld');", true);
                }
                else if (radImport.Checked)
                {
                    // Log purchase
                    ScriptManager.RegisterStartupScript(this, GetType(), "importworld", "logPageView('" + KanevaGlobals.GoogleAnalyticsAccountZonePurchase + "', '.kaneva.com', '/ImportList');", true);
                }
                else if (radExisting.Checked)
                {
                    // Log purchase
                    ScriptManager.RegisterStartupScript(this, GetType(), "existingworld", "logPageView('" + KanevaGlobals.GoogleAnalyticsAccountZonePurchase + "', '.kaneva.com', '/YourHome');", true);
                }

                // Set the GA account back to normal so we don't affect anything
                ScriptManager.RegisterStartupScript(this, GetType(), "reset", "_gaq.push(['_setAccount', '" + KanevaGlobals.GoogleAnalyticsAccount + "']);", true);
            }
            else
            {
                // Configure Play Now Button to go to users home
                ScriptManager.RegisterStartupScript(this, GetType(), "playnow", "pn_js = function() {" + StpUrl.GetPluginJSForUser(Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, KanevaWebGlobals.CurrentUser.Username, "", KanevaWebGlobals.CurrentUser.UserId) + "};", true);
            }
        }

        /// <summary>
        /// PurchaseItem
        /// </summary>
        private int PurchaseItem(WOKItem item, int quantity, bool bRewards, int iOrderTotal, int parentPurchaseId, bool bAllowOwnerCommission, bool bAddToInventory, bool bAddToBank)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            FameFacade fameFacade = new FameFacade();
            BlastFacade blastFacade = new BlastFacade();
            UserFacade userFacade = new UserFacade();

            UInt32 iDesignerCommision = Convert.ToUInt32(item.DesignerPrice * quantity);

            UInt32 iTemplateCreatorId = 0;
            UInt32 iTemplateCreatorsComission;
            GetUGCTemplateInfo(item, out iTemplateCreatorId, out iTemplateCreatorsComission);
            iTemplateCreatorsComission *= Convert.ToUInt32(quantity);

            int currUserId = KanevaWebGlobals.CurrentUser.UserId;
            int purchaseId = 0;
            int txnType = (int)(KanevaWebGlobals.CurrentUser.HasVIPPass ? ItemPurchase.eItemTxnType.VIP_PURCHASE : ItemPurchase.eItemTxnType.PURCHASE);

            // Give them the item
            if (bRewards)
            {
                if (bAddToInventory)
                {
                    userFacade.AddItemToPendingInventory(currUserId, WOK_INVENTORY_TYPE_PERSONAL, item.GlobalId, quantity, 512);
                }
                else if (bAddToBank)
                {
                    userFacade.AddItemToPendingInventory(currUserId, WOK_INVENTORY_TYPE_BANK, item.GlobalId, quantity, 512);
                }

                // Add it to thier history
                purchaseId = shoppingFacade.AddItemPurchase(item.GlobalId, txnType, currUserId, item.ItemCreatorId, iOrderTotal, 0, Common.GetVisitorIPAddress(), Constants.CURR_GPOINT, quantity, parentPurchaseId);
            }
            else
            {
                if (bAddToInventory)
                {
                    userFacade.AddItemToPendingInventory(currUserId, WOK_INVENTORY_TYPE_PERSONAL, item.GlobalId, quantity, 256);
                }
                else if (bAddToBank)
                {
                    userFacade.AddItemToPendingInventory(currUserId, WOK_INVENTORY_TYPE_BANK, item.GlobalId, quantity, 256);
                }

                // Add it to thier history
                purchaseId = shoppingFacade.AddItemPurchase(item.GlobalId, txnType, currUserId, item.ItemCreatorId, iOrderTotal, iDesignerCommision, Common.GetVisitorIPAddress(), Constants.CURR_KPOINT, quantity, parentPurchaseId);
            }

            // Credit the owner if it is a custom item
            // Animesh email titled "UGC credits/Rewards and upload changes" - UGC commission will be based on credits purchases only             
            if (iDesignerCommision > 0 && bAllowOwnerCommission)
            {
                if (!bRewards)
                {
                    GetUserFacade.AdjustUserBalance(Convert.ToInt32(item.ItemCreatorId), Constants.CURR_KPOINT, Convert.ToDouble(iDesignerCommision), Constants.CASH_TT_ROYALITY);
                }
            }

            if (!bRewards)
            {
                //if (item.UseType == 0) // 0 is clothing, 10/11 is furniture, we only want clothing
                // {
                fameFacade.RedeemPacket(Convert.ToInt32(item.ItemCreatorId), (int)PacketIdFashion.PURCHASE_WITH_CREDS, (int)FameTypes.Fashion);
                fameFacade.RedeemPacket(Convert.ToInt32(item.ItemCreatorId), (int)PacketId.FASHION_PURCHASE_WITH_CREDS, (int)FameTypes.World);
                // }
            }
            else if (bRewards)
            {
                fameFacade.RedeemPacket(Convert.ToInt32(item.ItemCreatorId), (int)PacketIdFashion.PURCHASE_WITH_REWARDS, (int)FameTypes.Fashion);
            }


            // Credit template designer if it is based on a custom template
            if (iTemplateCreatorsComission > 0 && bAllowOwnerCommission)
            {
                if (!bRewards)
                {
                    GetUserFacade.AdjustUserBalance(Convert.ToInt32(iTemplateCreatorId), Constants.CURR_KPOINT, Convert.ToDouble(iTemplateCreatorsComission), Constants.CASH_TT_ROYALITY);

                    // Get base item global ID (the first derivative from the same template created by template designer)
                    int baseItemGlid = shoppingFacade.GetBaseItemGlobalId(item.GlobalId);

                    // Add template commission transaction to shopping history
                    shoppingFacade.AddItemPurchase(baseItemGlid, (int)ItemPurchase.eItemTxnType.COMMISSION, currUserId, iTemplateCreatorId, 0, iTemplateCreatorsComission, Common.GetVisitorIPAddress(), Constants.CURR_KPOINT, quantity, parentPurchaseId);
                }
            }

            if (!bRewards)
            {
                //if (item.UseType == 0) // 0 is clothing, 10/11 is furniture, we only want clothing
                //{
                fameFacade.RedeemPacket(Convert.ToInt32(iTemplateCreatorId), (int)PacketIdFashion.PURCHASE_WITH_CREDS, (int)FameTypes.Fashion);
                fameFacade.RedeemPacket(Convert.ToInt32(iTemplateCreatorId), (int)PacketId.FASHION_PURCHASE_WITH_CREDS, (int)FameTypes.World);
                //}							
            }
            else if (bRewards)
            {
                fameFacade.RedeemPacket(Convert.ToInt32(iTemplateCreatorId), (int)PacketIdFashion.PURCHASE_WITH_REWARDS, (int)FameTypes.Fashion);
            }


            // Give purchaser fashion fame
            //if (item.UseType == 0) // 0 is clothing, 10/11 is furniture, we only want clothing
            //{
            fameFacade.RedeemPacket(currUserId, (int)PacketIdFashion.PURCHASED_ITEM, (int)FameTypes.Fashion);
            fameFacade.RedeemPacket(currUserId, (int)PacketId.BUY_AN_ITEM, (int)FameTypes.World);
            fameFacade.RedeemPacket(currUserId, (int)PacketId.BUY_SHOP_1_ITEM, (int)FameTypes.World);
            fameFacade.RedeemPacket(currUserId, (int)PacketId.BUY_SHOP_3_ITEMS, (int)FameTypes.World);
            fameFacade.RedeemPacket(currUserId, (int)PacketId.BUY_SHOP_5_ITEMS, (int)FameTypes.World);
            fameFacade.RedeemPacket(currUserId, (int)PacketId.BUY_SHOP_10_ITEMS, (int)FameTypes.World);
            fameFacade.RedeemPacket(currUserId, (int)PacketId.BUY_SHOP_20_ITEMS, (int)FameTypes.World);
            fameFacade.RedeemPacket(currUserId, (int)PacketId.BUY_SHOP_50_ITEMS, (int)FameTypes.World);
            fameFacade.RedeemPacket(currUserId, (int)PacketId.BUY_SHOP_100_ITEMS, (int)FameTypes.World);
            fameFacade.RedeemPacket(currUserId, (int)PacketId.BUY_SHOP_250_ITEMS, (int)FameTypes.World);
            fameFacade.RedeemPacket(currUserId, (int)PacketId.BUY_SHOP_500_ITEMS, (int)FameTypes.World);
            fameFacade.RedeemPacket(currUserId, (int)PacketId.BUY_SHOP_1000_ITEMS, (int)FameTypes.World);

            if (item.UseType == WOKItem.USE_TYPE_ANIMATION)
            {
                fameFacade.RedeemPacket(currUserId, (int)PacketId.BUY_ANIMATION, (int)FameTypes.World);
            }

            return purchaseId;
        }

        /// <summary>
        /// PurchaseBundle
        /// </summary>
        private void PurchaseBundle(WOKItem bundle, int quantity, bool bRewards, int iOrderTotal, int parentPurchaseId, bool addOnlyToBank = false)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();

            UInt32 iDesignerCommision = Convert.ToUInt32(bundle.DesignerPrice * quantity);
            int purchaseId = 0;
            int txnType = (int)(KanevaWebGlobals.CurrentUser.HasVIPPass ? ItemPurchase.eItemTxnType.VIP_PURCHASE : ItemPurchase.eItemTxnType.PURCHASE);

            // add the bundle purchase 
            if (bRewards)
            {
                purchaseId = shoppingFacade.AddItemPurchase(bundle.GlobalId, txnType, KanevaWebGlobals.CurrentUser.UserId, bundle.ItemCreatorId, iOrderTotal, 0, Common.GetVisitorIPAddress(), Constants.CURR_GPOINT, quantity, parentPurchaseId);
            }
            else
            {
                purchaseId = shoppingFacade.AddItemPurchase(bundle.GlobalId, txnType, KanevaWebGlobals.CurrentUser.UserId, bundle.ItemCreatorId, iOrderTotal, 0, Common.GetVisitorIPAddress(), Constants.CURR_KPOINT, quantity, parentPurchaseId);
            }

            // Give bundle commission
            if (iDesignerCommision > 0)
            {
                if (!bRewards)
                {
                    GetUserFacade.AdjustUserBalance(Convert.ToInt32(bundle.ItemCreatorId), Constants.CURR_KPOINT, Convert.ToDouble(iDesignerCommision), Constants.CASH_TT_ROYALITY);
                }
            }

            bool bAllowOwnerCommission = true;

            // add items to users inventory
            foreach (WOKItem item in bundle.BundleItems)
            {
                bAllowOwnerCommission = !(bundle.ItemCreatorId.Equals(item.ItemCreatorId));

                PurchaseItem(item, quantity, bRewards, GetItemCheckoutPrice(item), purchaseId, bAllowOwnerCommission, !addOnlyToBank, addOnlyToBank);
            }

        }

        /// <summary>
        /// PurchaseScriptGameItem
        /// </summary>
        private void PurchaseScriptGameItem(WOKItem wokItem, int quantity, bool bRewards, int iOrderTotal)
        {
            sgItem = (sgItem ?? GetScriptGameItemFacade.GetSnapshotScriptGameItem(wokItem.GlobalId));

            int purchaseId = PurchaseItem(wokItem, quantity, bRewards, GetItemCheckoutPrice(wokItem), 0, true, true, false);

            if (sgItem.BundleGlid > 0)
            {
                gameItemBundle = (gameItemBundle ?? GetShoppingFacade.GetItem(sgItem.BundleGlid, false, false));
                PurchaseBundle(gameItemBundle, quantity, bRewards, iOrderTotal, purchaseId, true);
            }
        }

        private void PurchaseDeedItem(WOKItem deed, DeedItem item, bool bRewards, int quantity, int parentPurchaseId)
        {
            UInt32 iDesignerCommision = 0;
            UInt32 iTemplateCreatorId = 0;
            UInt32 iTemplateCreatorsComission;

            // No commissions or fee for kaneva items 
            // "PRD 5.3 - The base price and commission for all Kaneva owned items will be 0. "
            // Regarding !bRewards - See Animesh email titled "UGC credits/Rewards and upload changes" - UGC commission will be based on credits purchases only    
            // Deed owner does not get commision on any items in his deed
            if (!item.IsKanevaOwned && !bRewards && !item.ItemCreatorId.Equals(deed.ItemCreatorId))
            {
                WOKItem wokItem = GetShoppingFacade.GetItem(item.GlobalId);

                if (wokItem.DesignerPrice > 0)
                {
                    iDesignerCommision = Convert.ToUInt32(wokItem.DesignerPrice);

                    GetUserFacade.AdjustUserBalance(Convert.ToInt32(item.ItemCreatorId), Constants.CURR_KPOINT, Convert.ToDouble(iDesignerCommision), Constants.CASH_TT_ROYALITY);

                    // Show it as a sale for item owner
                    GetShoppingFacade.AddItemPurchase(wokItem.GlobalId, (int)ItemPurchase.eItemTxnType.COMMISSION, KanevaWebGlobals.CurrentUser.UserId, wokItem.ItemCreatorId, 0, iDesignerCommision, Common.GetVisitorIPAddress(), Constants.CURR_KPOINT, quantity, parentPurchaseId);
                }


                GetUGCTemplateInfo(wokItem, out iTemplateCreatorId, out iTemplateCreatorsComission);

                // Credit template designer if it is based on a custom template
                if (iTemplateCreatorsComission > 0)
                {
                    if (!bRewards && !iTemplateCreatorId.Equals(deed.ItemCreatorId))
                    {
                        GetUserFacade.AdjustUserBalance(Convert.ToInt32(iTemplateCreatorId), Constants.CURR_KPOINT, Convert.ToDouble(iTemplateCreatorsComission), Constants.CASH_TT_ROYALITY);

                        // Get base item global ID (the first derivative from the same template created by template designer)
                        int baseItemGlid = GetShoppingFacade.GetBaseItemGlobalId(item.GlobalId);

                        // Add template commission transaction to shopping history
                        GetShoppingFacade.AddItemPurchase(baseItemGlid, (int)ItemPurchase.eItemTxnType.COMMISSION, KanevaWebGlobals.CurrentUser.UserId, iTemplateCreatorId, 0, iTemplateCreatorsComission, Common.GetVisitorIPAddress(), Constants.CURR_KPOINT, quantity, parentPurchaseId);
                    }
                }
            }
        }

        /// <summary>
        /// PurchaseDeed
        /// </summary>
        private bool PurchaseDeed(WOKItem deed, int quantity, bool bRewards, int iOrderTotal)
        {
            msgDialog.Title = "Purchase Summary";
            msgDialog.MessageHeader = "Error:";

            // Buy the deed                                                     
            int parentPurchaseId = PurchaseItem(deed, quantity, bRewards, iOrderTotal, 0, true, false, false);

            if (parentPurchaseId < 1)
            {
                msgDialog.Message = "An error occurred during the purchase process.";
                ShowErrorMessageDialog(msgDialog);
                return false;
            }

            // Add deed item commisions
            HashSet<int> processedGlids = new HashSet<int>();
            deedList = GetShoppingFacade.GetDeedSalesItemList(deed.GlobalId, (int)deed.ItemCreatorId);

            foreach (DeedItem item in deedList)
            {
                if (!processedGlids.Contains(item.GlobalId))
                {
                    processedGlids.Add(item.GlobalId);
                    PurchaseDeedItem(deed, item, bRewards, quantity, parentPurchaseId);

                    if (item.UseType == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM)
                    {
                        foreach (DeedItem deedItem in item.BundleItems)
                        {
                            if (!processedGlids.Contains(deedItem.GlobalId))
                            {
                                processedGlids.Add(deedItem.GlobalId);
                                PurchaseDeedItem(deed, deedItem, bRewards, quantity, parentPurchaseId);
                            }
                        }
                    }
                }
            }

            string communityName = "";

            // Set unique name for community
            for (int i = 0; i < 10; i++)
            {
                // Create a unique community name to hold the zone
                communityName = deed.Name;
                if (communityName.Length > 30)
                {
                    communityName = communityName.Substring(0, 30);
                }
                communityName = deed.Name + " Copy" + (i == 0 ? "" : i.ToString()) + " " + KanevaWebGlobals.CurrentUser.UserId.ToString();
                if (!GetCommunityFacade.IsCommunityNameTaken(false, communityName.Replace(" ", ""), 0))
                {
                    break;
                }
                if (i == 9)
                {   // If we can't get unique name through 10 tries, just append date
                    communityName = communityName + " " + KanevaWebGlobals.CurrentUser.UserId.ToString() + " " + DateTime.Now.ToString("MMddyy_hhmmss");
                }
            }


            // Add the new zone to a community so the user will always be able to find
            // it in their import zones menu in world.  We do this for all zone purchases
            int importCommunityId;
            if (!CreateCommunity(communityName, deed, true, out importCommunityId))
            {
                return false;
            }


            // Apply the deed
            if (radImport.Checked || radNew3DApp.Checked)  // Add to Inventory or Create New World
            {
                // Create new world (community) and apply zone to it
                if (radNew3DApp.Checked)
                {
                    // Create the new community and apply the zone
                    int newCommunityId;
                    if (!CreateCommunity(txt3DAppName.Text.Trim(), deed, false, out newCommunityId))
                    {
                        msgDialog.MessageHeader = "Error:";
                        msgDialog.Message = "Error creating new World. Your World should be accessible from your import worlds list in build mode.";
                        ShowErrorMessageDialog(msgDialog);
                        return false;
                    }

                    // Configure Play Now Button
                    ScriptManager.RegisterStartupScript(this, GetType(), "playnow1", "pn_js = function() {" + StpUrl.GetPluginJS(Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, 0, newCommunityId, txt3DAppName.Text.Trim(), "", KanevaWebGlobals.CurrentUser.UserId) + "};", true);
                }
                else  // Don't need to create new community, zone already in users import menu.  Just set play now link
                {
                    string commNameNoSpaces = communityName.Replace(" ", "");

                    // Configure Play Now Button to go to users home
                    ScriptManager.RegisterStartupScript(this, GetType(), "playnow2", "pn_js = function() {" + StpUrl.GetPluginJSForUser(Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, KanevaWebGlobals.CurrentUser.Username, "", KanevaWebGlobals.CurrentUser.UserId) + "};", true);
                }
            }
            else if (radExisting.Checked)   // Apply new zone to an existing world
            {
                Community existingWorld = GetCommunityFacade.GetCommunity(Convert.ToInt32(ddlWorlds.SelectedValue));

                if (existingWorld.CommunityId == 0)
                {
                    msgDialog.Message = "User World does not exist.";
                    ShowErrorMessageDialog(msgDialog);
                }
                else
                {
                    // Home or Community
                    if (existingWorld.CommunityTypeId == (int)CommunityType.COMMUNITY ||
                        existingWorld.CommunityTypeId == (int)CommunityType.HOME)
                    {
                        // Call Move objects, populate the zone
                        int resultVal = 0;
                        int resultZoneIndex = 0;

                        bool isHome = false;
                        int zoneInstanceId = existingWorld.CommunityId;
                        if (existingWorld.CommunityTypeId == (int)CommunityType.HOME)
                        {
                            isHome = true;
                            zoneInstanceId = KanevaWebGlobals.CurrentUser.WokPlayerId;
                        }
                        DataRow drChannelZone = WOKStoreUtility.GetHangoutChannelZone(zoneInstanceId, isHome);
                        // this param is incorrect !!!!!
                        // Get Zone info for the source zone (zone jsut purchased).  We use the community that 
                        // was created to hold the newly purchased zone in the inventory.
                        DataRow drSourceChannelZone = WOKStoreUtility.GetHangoutChannelZone(importCommunityId, false);
                        ZoneIndex newZoneIndex = new ZoneIndex(Convert.ToInt32(drSourceChannelZone["zone_index"]));
                        int i = newZoneIndex.ZoneIndexPlain;

                        // Import the new zone
                        GetShoppingFacade.ImportZone(KanevaWebGlobals.CurrentUser.WokPlayerId, Convert.ToInt32(drChannelZone["zone_index"]),
                            Convert.ToInt32(drChannelZone["zone_instance_id"]), newZoneIndex.ZoneIndexPlain, Convert.ToInt32(drSourceChannelZone["zone_index"]),
                            Convert.ToInt32(drSourceChannelZone["zone_instance_id"]), true, ref resultVal, ref resultZoneIndex);

                        GetGameFacade.ClearZoneCustomizationsCache(Convert.ToInt32(drChannelZone["zone_index"]), Convert.ToInt32(drChannelZone["zone_instance_id"]));

                        // Set server for restart
                        try
                        {
                            GetGameFacade.BroadcastUpdateScriptZoneSpinDownDelayEvent(Convert.ToInt32(drChannelZone["channel_zone_id"]));
                        }
                        catch (Exception) { }
                    }

                    // Configure play now button
                    ScriptManager.RegisterStartupScript(this, GetType(), "playnow3", "pn_js = function() {" + StpUrl.GetPluginJS(Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, existingWorld.WOK3App.GameId, existingWorld.CommunityId, existingWorld.Name, "", KanevaWebGlobals.CurrentUser.UserId) + "};", true);
                }
            }

            int currUserId = KanevaWebGlobals.CurrentUser.UserId;
            GetFameFacade.RedeemPacket(currUserId, (int)PacketId.BUILDER_BOUGHT_A_ZONE, (int)FameTypes.Builder);
            GetFameFacade.RedeemPacket(Convert.ToInt32(deed.ItemCreatorId), (int)PacketId.BUILDER_SOLD_1ST_ZONE, (int)FameTypes.Builder);
            if (bRewards)
            {
                GetFameFacade.RedeemPacket(Convert.ToInt32(deed.ItemCreatorId), (int)PacketId.BUILDER_SOLD_3_ZONES_FOR_REWARDS, (int)FameTypes.Builder);
                GetFameFacade.RedeemPacket(Convert.ToInt32(deed.ItemCreatorId), (int)PacketId.BUILDER_SOLD_10_ZONES_FOR_REWARDS, (int)FameTypes.Builder);
                GetFameFacade.RedeemPacket(Convert.ToInt32(deed.ItemCreatorId), (int)PacketId.BUILDER_SOLD_25_ZONES_FOR_REWARDS, (int)FameTypes.Builder);
                GetFameFacade.RedeemPacket(Convert.ToInt32(deed.ItemCreatorId), (int)PacketId.BUILDER_SOLD_100_ZONES_FOR_REWARDS, (int)FameTypes.Builder);
            }
            else
            {
                GetFameFacade.RedeemPacket(Convert.ToInt32(deed.ItemCreatorId), (int)PacketId.BUILDER_SOLD_10_ZONES_FOR_CREDITS, (int)FameTypes.Builder);
                GetFameFacade.RedeemPacket(Convert.ToInt32(deed.ItemCreatorId), (int)PacketId.BUILDER_SOLD_100_ZONES_FOR_CREDITS, (int)FameTypes.Builder);
                GetFameFacade.RedeemPacket(Convert.ToInt32(deed.ItemCreatorId), (int)PacketId.BUILDER_SOLD_500_ZONES_FOR_CREDITS, (int)FameTypes.Builder);
                GetFameFacade.RedeemPacket(Convert.ToInt32(deed.ItemCreatorId), (int)PacketId.BUILDER_SOLD_1000_ZONES_FOR_CREDITS, (int)FameTypes.Builder);
                GetFameFacade.RedeemPacket(Convert.ToInt32(deed.ItemCreatorId), (int)PacketId.BUILDER_SOLD_10000_ZONES_FOR_CREDITS, (int)FameTypes.Builder);
            }

            return true;
        }

        private bool CreateCommunity(string communityName, WOKItem deed, bool isForImport, out int communityId)
        {
            communityId = 0;
            try
            {
                Community community = new Community();
                // Create a community per Brett request so travel SPs create the try on zone
                community.Name = Server.HtmlEncode(communityName);
                community.NameNoSpaces = Server.HtmlEncode(communityName.Replace(" ", ""));
                community.IsPersonal = 0; //0 is false
                community.CommunityTypeId = (int)CommunityType.COMMUNITY;
                community.StatusId = (int)CommunityStatus.ACTIVE;
                community.CreatorId = KanevaWebGlobals.CurrentUser.UserId;
                community.CreatorUsername = KanevaWebGlobals.CurrentUser.Username;
                community.IsAdult = Server.HtmlEncode("N");
                community.IsPublic = Server.HtmlEncode("N");
                community.PlaceTypeId = isForImport ? 99 : 0;

                communityId = GetCommunityFacade.InsertCommunity(community);

                if (communityId > 0)
                {
                    //add the creator to the community
                    CommunityMember member = new CommunityMember();
                    member.CommunityId = communityId;
                    member.UserId = community.CreatorId;
                    member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.OWNER;
                    member.Newsletter = "Y";
                    member.InvitedByUserId = 0;
                    member.AllowAssetUploads = "Y";
                    member.AllowForumUse = "Y";
                    member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

                    GetCommunityFacade.InsertCommunityMember(member);

                    // Insert default template_id for communities if not for import
                    if (!isForImport)
                    {
                        GetGameFacade.InsertWorldsToTemplate(communityId, 10);
                    }
                }
                else
                {
                    msgDialog.Message = "Unable to apply World.";
                    ShowErrorMessageDialog(msgDialog);
                    return false;
                }

                DataRow drCustomDeedUsage = GetShoppingFacade.GetCustomDeedUsage(deed.GlobalId);

                if (drCustomDeedUsage == null)
                {
                    msgDialog.Message = "Error finding this deed.";
                    ShowErrorMessageDialog(msgDialog);
                    return false;
                }

                int newZoneIndex = GetShoppingFacade.MakeZoneIndex(Convert.ToInt32(drCustomDeedUsage["zone_index_plain"]), 6);

                // Create a channel Zone record 
                int czRecordsInserted = GetShoppingFacade.InsertChannelZone(KanevaWebGlobals.CurrentUser.UserId, newZoneIndex, communityId, communityName,
                    0, "NOT_TIED", KanevaWebGlobals.CurrentUser.Country, KanevaWebGlobals.CurrentUser.Age, 0);

                if (czRecordsInserted <= 0)
                {
                    msgDialog.Message = "Unable to make World.";
                    ShowErrorMessageDialog(msgDialog);
                    return false;
                }


                // Call Move objects, populate the zone
                int result = 0;
                string urlResult = "";

                // Add item parameters per Ryan
                GetShoppingFacade.UpdateItemParameter(deed.GlobalId, 209, Convert.ToInt32(drCustomDeedUsage["zone_index_plain"]).ToString());

                m_logger.Debug("Calling shoppingFacade.ChangeZoneMap()");
                m_logger.Debug("oldZoneIndex=" + newZoneIndex + ";zoneInstanceId=" + communityId + ";newZoneIndex=" + newZoneIndex + ";playerName=" + KanevaWebGlobals.CurrentUser.Username + ";templateId=" + deed.GlobalId + ";gameNameNoSpaces=" + KanevaGlobals.WokGameName);

                GetShoppingFacade.ChangeZoneMap(newZoneIndex, communityId, newZoneIndex, KanevaWebGlobals.CurrentUser.Username, deed.GlobalId,
                    KanevaGlobals.WokGameName, ref result, ref urlResult);

                m_logger.Debug("Returned from shoppingFacade.ChangeZoneMap()");
            }
            catch (Exception e)
            {
                msgDialog.Message = "Error occurred applying new World.";
                ShowErrorMessageDialog(msgDialog);
                m_logger.Error("Catch in ItemDetails.CreateCommunity : " + e);
                return false;
            }

            return true;
        }

        protected void ResetPurchaseButtons()
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "resetprocess", "ResetProcess()", true);
        }

        /// <summary>
        /// BindReviews
        /// </summary>
        protected void BindReviews(int gId, int pageNumber, int pageSize)
        {
            PagedList<ItemReview> itemReview = GetShoppingFacade.GetItemReviews(gId, pageSize, pageNumber);

            if (itemReview.Count > 0)
            {
                litNoReviews.Visible = false;
            }
            else
            {
                if (KanevaWebGlobals.CurrentUser.UserId == 0)
                {
                    litNoReviews.InnerText = "Log in to create your review.";
                }

                litNoReviews.Visible = true;
            }

            lblNumberOfReviews1.Text = itemReview.Count.ToString();

            rptReviews.DataSource = itemReview;
            rptReviews.DataBind();
        }

        protected void BindItemCategories(List<ItemCategory> icCats)
        {
            // Show the categories
            rptCategories.DataSource = icCats;
            rptCategories.DataBind();
        }

        private void ShowZoneSelect()
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

            int gId = GlobalId();
            WOKItem item = GetShoppingFacade.GetItem(gId);
            int quantity = GetQuantity();

            // Is it available for purchase?
            if (item.InventoryType.Equals(0))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertnotavail", "alert('This item is not available for purchase.');", true);
                return;
            }

            // Is it access pass?
            if (GetShoppingFacade.IsItemAccessPass(gId) && !KanevaWebGlobals.CurrentUser.HasAccessPass)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertap", "alert('This item requires Access Pass to purchase! Please purchase an Access Pass first.');", true);
                return;
            }

            pnlZoneSelect.Visible = true;
            pnlRelated.Visible = false;
            pnlBuy.Visible = false;
            pnlComplete.Visible = false;
            pnlCompleteZonePurchase.Visible = false;

            // Set default selections
            radExisting.Checked = false;
            radImport.Checked = false;
            radNew3DApp.Checked = true;
            rblTemplates.SelectedIndex = 0;

            // Populate Worlds into Existing World drop-down
            PopulateWorldsList();

            // New Style for purchase forms
            divZoneSelect.Attributes.Add("class", "purchasenew");
            HideSectionsForPurchase();
        }

        private void PopulateWorldsList()
        {
            // Clear out the existing list. This is relevant for ajax post back
            ddlWorlds.Items.Clear();

            int[] communityTypeIds = { Convert.ToInt32(CommunityType.APP_3D), Convert.ToInt32(CommunityType.COMMUNITY), Convert.ToInt32(CommunityType.HOME) };
            PagedList<Community> worlds = GetCommunityFacade.GetUserCommunities(KanevaWebGlobals.CurrentUser.UserId, communityTypeIds, "name", "place_type_id<99 AND c.creator_id=@userId", 1, 100, true, "");

            foreach (Community c in worlds)
            {
                ListItem li = new ListItem(c.Name, c.CommunityId.ToString());
                li.Attributes.Add("type", c.CommunityTypeId.ToString());

                if (c.CommunityTypeId == (int)CommunityType.HOME)
                {
                    li.Text = c.Name + " (Home World)";
                }

                ddlWorlds.Items.Add(li);
            }
            ddlWorlds.Items.Insert(0, new ListItem("Choose a World", "-1"));
            ddlWorlds.SelectedIndex = 0;
        }

        private void HideSectionsForPurchase()
        {
            spnHideDuringPurchase.Visible = false;
            //pBack.Visible = false;
        }

        private void ShowPurchaseSummary()
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

            int gId = GlobalId();
            WOKItem item = GetShoppingFacade.GetItem(gId);
            int quantity = GetQuantity();

            // Is it available for purchase?
            if (item.InventoryType.Equals(0))
            {
                CancelPurchase();
                ScriptManager.RegisterStartupScript(this, GetType(), "alertsum1", "alert('This item is not available for purchase.');", true);
                return;
            }

            // Is it access pass?
            if (GetShoppingFacade.IsItemAccessPass(gId) && !KanevaWebGlobals.CurrentUser.HasAccessPass)
            {
                CancelPurchase();
                ScriptManager.RegisterStartupScript(this, GetType(), "alertsum2", "alert('This item requires Access Pass to purchase! Please purchase an Access Pass first.');", true);
                return;
            }

            // Does the user have enough credits?
            UInt64 iCredits = Convert.ToUInt64(UsersUtility.GetUserPointTotal(KanevaWebGlobals.CurrentUser.UserId));
            UInt64 iRewards = Convert.ToUInt64(UsersUtility.getUserBalance(KanevaWebGlobals.CurrentUser.UserId, Constants.CURR_GPOINT));

            int itemCheckoutPrice = GetItemCheckoutPrice(item);

            // Set the prices
            lblItemNamePurchase.Text = KanevaGlobals.TruncateWithEllipsis(item.Name, 60);
            lblPriceCheckout.Text = itemCheckoutPrice > 0 ? itemCheckoutPrice.ToString("N0") : "Free";
            lblCreditsCheckout.Text = iCredits.ToString("N0");
            lblRewards.Text = iRewards.ToString("N0");

            Int64 iEndBalance = (Convert.ToInt64(iCredits) - (itemCheckoutPrice * quantity));

            pnlRelated.Visible = false;
            pnlBuy.Visible = true;
            pnlBuy.Style.Add("display", "block");
            pnlComplete.Visible = false;
            pnlCompleteZonePurchase.Visible = false;

            if (itemCheckoutPrice == 0)
            {
                lbCompleteRewards.Text = "Get For Free!";
                lbComplete.Text = "Get For Free!";
            }

            if (item.InventoryType == (int)WOKItem.InventoryTypes.Credits ||
                item.InventoryType == (int)WOKItem.InventoryTypes.BothCurrencies)
            {
                // Not enough credits for purchase
                if (iCredits < Convert.ToUInt64(itemCheckoutPrice * quantity))
                {
                    // show Get Credits button
                    lbGetCredits.Visible = true;
                    lbGetCredits.Attributes.Add("href", "http://" + KanevaGlobals.SiteName + "/mykaneva/buyCreditPackages.aspx");
                    lbGetCredits.Attributes.Add("target", "_blank");
                    lbComplete.Visible = false;

                    // show number of credits needed
                    divCreditsMsg.InnerText = "+" + (Convert.ToUInt64(itemCheckoutPrice * quantity) - iCredits).ToString("N0") + " Required for Purchase";
                }

                spnRewardsMsg.InnerText = "This item may only be purchased with credits";
                lbCompleteRewards.Visible = false;
            }
            else if (item.InventoryType == (int)WOKItem.InventoryTypes.Rewards)
            {
                // Not enough rewards to purchase
                if (iRewards < Convert.ToUInt64(itemCheckoutPrice * quantity))
                {
                    lbCompleteRewards.OnClientClick = "return false;";
                    lbCompleteRewards.CssClass = lbCompleteRewards.CssClass + " disabled";
                    lbCompleteRewards.Visible = false;

                    // show number of Rewards needed
                    divRewardsMsg.InnerText = "+" + (Convert.ToUInt64(itemCheckoutPrice * quantity) - iRewards).ToString("N0") + " Required for Purchase";
                    lnkEarnRewards.Visible = true;
                }

                spnCreditsMsg.InnerText = "This item may only be purchased with rewards";
                lbComplete.Visible = false;
            }

            HideSectionsForPurchase();
        }

        private void CancelPurchase()
        {
            pnlRelated.Visible = true;
            spnHideDuringPurchase.Visible = true;
            pBack.Visible = true;
            pnlBuy.Visible = false;
            pnlComplete.Visible = false;
            pnlCompleteZonePurchase.Visible = false;
            pnlZoneSelect.Visible = false;
        }

        private bool ValidateApplyZoneFields()
        {
            if (radNew3DApp.Checked)
            {
                msgDialog.Title = "Purchase Summary";
                msgDialog.MessageHeader = "Warning:";

                // 3dapp limits
                if (!IsAdministrator() && !UsersUtility.IsUserBetatester())
                {
                    int numberOf3DApps = GetCommunityFacade.GetNumberOf3DApps(KanevaWebGlobals.CurrentUser.UserId, (int)Constants.eCOMMUNITY_STATUS.ACTIVE);
                    int numberOfCommunities = GetCommunityFacade.GetNumberOfCommunities(KanevaWebGlobals.CurrentUser.UserId, (int)Constants.eCOMMUNITY_STATUS.ACTIVE, new int[] { (int)CommunityType.COMMUNITY });

                    // Per discussion with Billy and Animesh, we still want separate limits on creation for 3dapps and communities.
                    if (numberOf3DApps >= Kaneva.BusinessLayer.Facade.Configuration.MaxNumberOf3DAppsPerUser)
                    {
                        msgDialog.Message = "Users are restricted to a maximum of " + Kaneva.BusinessLayer.Facade.Configuration.MaxNumberOf3DAppsPerUser + " Developer Worlds.";
                        ShowErrorMessageDialog(msgDialog);
                        PopulateWorldsList();
                        return false;
                    }
                    else if (numberOfCommunities >= Kaneva.BusinessLayer.Facade.Configuration.MaxNumberOfCommunitiesPerUser)
                    {
                        msgDialog.Message = "Users are restricted to a maximum of " + Kaneva.BusinessLayer.Facade.Configuration.MaxNumberOfCommunitiesPerUser + " Standard Worlds.";
                        ShowErrorMessageDialog(msgDialog);
                        PopulateWorldsList();
                        return false;
                    }
                }

                if (!Regex.IsMatch(txt3DAppName.Text.Trim(), "^" + Constants.VALIDATION_REGEX_CHANNEL_NAME + "$"))
                {
                    string errorStr = "The name you entered must be between 4 - 50 characters in length.";
                    ShowInlineErrorMessage(divNewWorldTxtBoxErr, errorStr);
                    txt3DAppName.Style.Add("border", "1px solid #ee2d2a");
                    PopulateWorldsList();
                    return false;
                }

                string strOriginalName = txt3DAppName.Text.Trim();

                // Check for community already exists
                if (GetCommunityFacade.IsCommunityNameTaken(false, strOriginalName.Trim().Replace(" ", ""), 0))
                {
                    msgDialog.Message = "World " + strOriginalName + " already exists, please change the name. We recommend " + GetSuggestedName(strOriginalName);
                    ShowErrorMessageDialog(msgDialog);
                    PopulateWorldsList();
                    return false;
                }
            }
            else if (radImport.Checked)
            {
                // We auto-create the community name for this zone, nothing to validate //
            }
            else if (radExisting.Checked)
            {
                if (ddlWorlds.SelectedValue == null || ddlWorlds.SelectedValue == String.Empty || Convert.ToInt32(ddlWorlds.SelectedValue) < 0)
                {
                    string errorStr = "You must select a world.";
                    ShowInlineErrorMessage(divExistingWorldError, errorStr);
                    ddlWorlds.Style.Add("border", "1px solid #ee2d2a");
                    aNew3DWorld.Attributes.Remove("class");
                    radNew3DAppMsg.Style.Add("display", "none");
                    aExistingWorld.Attributes.Add("class", "selected");
                    radExistingMsg.Style.Add("display", "block");
                    PopulateWorldsList();
                    return false;
                }
            }

            return true;
        }

        private void ShowErrorMessage(string err)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "err", "alert(" + err + ");", true);
        }

        private void ShowErrorMessageDialog(MessageDialog dialog)
        {
            if (dialog.OkButtonUrl.Length > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg1", "openMsgDialog('" + dialog.Title + "','" + dialog.FormattedMessage + "','" + dialog.OkButtonUrl + "');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg2", "openMsgDialog('" + dialog.Title + "','" + dialog.FormattedMessage + "');", true);
            }
        }

        private void ShowInlineErrorMessage(HtmlContainerControl divErr, string err)
        {
            divErr.Style.Add("display", "block");
            divErr.InnerHtml = err;
        }

        /// <summary>
        /// GetSuggestedName
        /// </summary>
        /// <param name="originalName"></param>
        /// <returns></returns>
        private string GetSuggestedName(string originalName)
        {
            int nameAddition = 1;

            while (GetCommunityFacade.IsCommunityNameTaken(false, originalName + nameAddition.ToString().Trim().Replace(" ", ""), 0))
            {
                nameAddition += 1;
            }

            return originalName + nameAddition.ToString();
        }

        private void SetItemTypeIcon(WOKItem item)
        {
            string imgName = "Non-Interactive";
            string title = "Non-Interactive objects are used to decorate your world.";

            if (item.isAnimated)
            {
                imgName = "AnimatedObject";
                title = "An Animated Object has available animations that can be purchased separately.";
            }
            else if (item.UseType == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM)
            {
                // Find out what category game item belongs to
                switch (item.Category1)
                {
                    case (int)ItemCategory.GameCategories.Animals:
                        imgName = "Animal";
                        title = "Animals can drop loot when killed and are part of the Character System.";
                        break;
                    case (int)ItemCategory.GameCategories.Armor:
                        imgName = "Armor";
                        title = "Armor adds to a Player’s health, mitigates damage, and is part of the Loot System.";
                        break;
                    case (int)ItemCategory.GameCategories.Consumables:
                        imgName = "Consumable";
                        title = "Consumables grant status effects, such as regenerating Health, and are part of the Loot System.";
                        break;
                    case (int)ItemCategory.GameCategories.Energy:
                        imgName = "Energy";
                        title = "Energy items can be consumed by Players and are part of the Loot System.";
                        break;
                    case (int)ItemCategory.GameCategories.Generic_Loot:
                        imgName = "GenericLoot";
                        title = "Generic Loot can be used for a variety of purposes and are part of the Loot System.";
                        break;
                    case (int)ItemCategory.GameCategories.Monsters:
                        imgName = "Monster";
                        title = "Monsters are computer controlled adversaries, that can drop loot when killed, and are part of the Character System.";
                        break;
                    case (int)ItemCategory.GameCategories.NPC:
                        imgName = "NPC";
                        title = "NPCs are computer controlled characters who can interact with players and are part of the Character System.";
                        break;
                    case (int)ItemCategory.GameCategories.Pets:
                        imgName = "Pet";
                        title = "Pets can obey an Owner’s commands and are part of the Character System.";
                        break;
                    case (int)ItemCategory.GameCategories.Placeables:
                        imgName = "Placeable";
                        title = "Placeables are Game Objects both Players and World Owners can add to a World and are part of the Placeables System.";
                        break;
                    case (int)ItemCategory.GameCategories.Vendors:
                        imgName = "Vendor";
                        title = "Vendors sell items to players and are part of the Character System.";
                        break;
                    case (int)ItemCategory.GameCategories.Weapons:
                        imgName = "Weapon";
                        title = "Weapons can damage other players and monsters and are part of the Loot System.";
                        break;
                }
            }
            else if (item.UseType == WOKItem.USE_TYPE_CLOTHING)
            {
                imgName = "Clothing";
                title = "Clothing allows you to personalize your avatar.";
            }
            else if (item.UseType == WOKItem.USE_TYPE_EQUIP)
            {
                imgName = "Accessory";
                title = "An Accessory can be attached to specific points on your avatar for personalization.";
            }
            else if (item.UseType == WOKItem.USE_TYPE_SOUND)
            {
                imgName = "Sound";
                title = "A Sound can be added to your world to give it life.";
            }
            else if (item.UseType == WOKItem.USE_TYPE_PARTICLE)
            {
                imgName = "Particle";
                title = "Particles are simple graphic effects that spice up your world.";
            }
            else if (item.UseType == WOKItem.USE_TYPE_BUNDLE)
            {
                imgName = "Bundle";
                title = "Bundles are groups of objects or effects for added functionality.";
            }
            else if (item.UseType == WOKItem.USE_TYPE_CUSTOM_DEED)
            {
                imgName = "Zone";
                title = "Worlds include everything in the World and can entirely replace any other World.";
            }
            else if (item.UseType == WOKItem.USE_TYPE_ANIMATION)
            {
                imgName = "Animation";
                title = "An Animation is applied to the required object to give it motion.";
                if (item.Category3 == 73)
                {
                    imgName = "Emote";
                    title = "Emotes are avatar animations for expressing emotions.";
                }
            }

            imgUseType.Src = ResolveUrl("~/images/icons/") + "ItemType_" + imgName + ".png";
            imgUseType.Attributes.Add("name", "img_" + imgName.ToLower());
            imgUseType.Attributes.Add("title", title);
        }

        private int GetItemCheckoutPrice(WOKItem item)
        {
            return GetItemCheckoutPrice(item, KanevaWebGlobals.CurrentUser.HasVIPPass);
        }
        private int GetItemCheckoutPrice(WOKItem item, bool isUserVIP)
        {
            if (isUserVIP)
            {
                // If item is a zone then we have to calcuate the designer fee for each item included
                // in the zone and the zone designer fee to get the VIP pricing.
                if (item.UseType == WOKItem.USE_TYPE_CUSTOM_DEED)
                {
                    // If we don't already have the deed list then we need to get it
                    if (deedList == null)
                    {
                        deedList = GetShoppingFacade.GetDeedSalesItemList(item.GlobalId, (int)item.ItemCreatorId);
                    }

                    HashSet<int> usedGlids = new HashSet<int>();
                    uint itemsNotOwnedDesignerPrice = 0;
                    foreach (DeedItem di in deedList)
                    {
                        if (!usedGlids.Contains(di.GlobalId) || di.UseType == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM)
                        {
                            usedGlids.Add(di.GlobalId);
                            itemsNotOwnedDesignerPrice += di.DeedItemPrice;

                            // Game items may have bundled glids that we need to process.
                            if (di.UseType == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM)
                            {
                                foreach (DeedItem bundledItem in di.BundleItems)
                                {
                                    if (!usedGlids.Contains(bundledItem.GlobalId))
                                    {
                                        usedGlids.Add(bundledItem.GlobalId);
                                        itemsNotOwnedDesignerPrice += bundledItem.DeedItemPrice;
                                    }
                                }
                            }
                        }
                    }

                    return (int)(itemsNotOwnedDesignerPrice + item.DesignerPrice);
                }
                else if (item.UseType == WOKItem.USE_TYPE_BUNDLE)
                {
                    return item.BundleWebPriceVIP;
                }
                else if (item.UseType == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM)
                {
                    sgItem = (sgItem ?? GetScriptGameItemFacade.GetSnapshotScriptGameItem(item.GlobalId));
                    gameItemBundle = (gameItemBundle ?? GetShoppingFacade.GetItem(sgItem.BundleGlid, false, false));
                    ScriptGameItemPrice sgiPrice = new ScriptGameItemPrice(item, gameItemBundle);

                    return sgiPrice.WebPriceVIP;
                }

                return item.WebPriceVIP;
            }

            return item.WebPrice;
        }

        #endregion Helper Methods

        #region Event Handlers

        /// <summary>
        /// lbPurchase_OnClick
        /// </summary>
        protected void lbPurchase_OnClick(object sender, EventArgs e)
        {
            int gId = GlobalId();
            WOKItem item = GetShoppingFacade.GetItem(gId);

            if (item.UseType == WOKItem.USE_TYPE_CUSTOM_DEED)
            {
                ShowZoneSelect();
            }
            else
            {
                ShowPurchaseSummary();
            }
        }

        /// <summary>
        /// lbCompleteRewards_OnClick
        /// </summary>
        protected void lbCompleteRewards_OnClick(object sender, EventArgs e)
        {
            CompletePurchase(true);
        }

        /// <summary>
        /// lbCompleteCredits_OnClick
        /// </summary>
        protected void lbCompleteCredits_OnClick(object sender, EventArgs e)
        {
            CompletePurchase(false);
        }

        /// <summary>
        /// btnChooseCommNext_OnClick
        /// </summary>
        protected void btnChooseCommNext_OnClick(object sender, EventArgs e)
        {
            // Validate Fields
            if (ValidateApplyZoneFields())
            {
                pnlZoneSelect.Visible = false;
                ShowPurchaseSummary();
            }
        }

        /// <summary>
        /// lbSubmitComment_OnClick
        /// </summary>
        protected void lbSubmitComment_OnClick(object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

            int gId = GlobalId();
            WOKItem item = GetShoppingFacade.GetItem(gId);

            // Blocked user cannot create a review. Only users who have logged into wok can submit a review.
            if (!GetUserFacade.IsUserBlocked((int)item.ItemCreatorId, KanevaWebGlobals.CurrentUser.UserId) && KanevaWebGlobals.CurrentUser.HasWOKAccount)
            {
                ItemReview itemReview = new ItemReview(0, gId, KanevaWebGlobals.CurrentUser.UserId, Server.HtmlEncode(txtComment.Value), Common.GetVisitorIPAddress(), new DateTime(), new DateTime(), 0, 1, "", "");

                itemReview.IpAddress = "127.0.0.1";
                GetShoppingFacade.AddItemReview(itemReview);


                //Send email to item creator					            
                UserFacade userFacade = new UserFacade();
                User userTo = userFacade.GetUser(Convert.ToInt32(item.ItemCreatorId));

                if (userTo.NotificationPreference.NotifyShopCommentsPurchases)
                {
                    // They want emails for blasts
                    MailUtilityWeb.SendItemCommentNotificationEmail(userTo.Email, gId, Server.HtmlEncode(txtComment.Value), userTo.UserId, KanevaWebGlobals.CurrentUser.UserId);
                }

                txtComment.Value = "";

                BindReviews(gId, 1, 50);
            }
        }

        /// <summary>
        /// lbRave_Click
        /// </summary>
        protected void lbRave_OnClick(Object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

            ShoppingFacade shoppingFacade = new ShoppingFacade();
            RaveFacade raveFacade = new RaveFacade();
            int gId = GlobalId();

            if (!raveFacade.IsUGCRaveFree(KanevaWebGlobals.CurrentUser.UserId, gId,
                RaveType.eRAVE_TYPE.SINGLE) && !IsAdministrator())
            {
                // this rave is not free, show buy rave dialog
                ScriptManager.RegisterStartupScript(this, GetType(), "payrave", "popFacebox();", true);
            }
            else  // first time rave
            {
                // first rave is free.  Insert a normal rave.
                raveFacade.RaveUGCItem(KanevaWebGlobals.CurrentUser.UserId, gId, RaveType.eRAVE_TYPE.SINGLE, "");
                litRaveCount.Text = raveFacade.GetUGCRaveCount(gId).ToString();
            }
        }

        protected void lbDeleteReview_OnClick(object sender, EventArgs e)
        {
            uint reviewId;
            LinkButton btn = (LinkButton)(sender);
            int gId = GlobalId();

            if (Request.IsAuthenticated && IsAdministrator())
            {
                if (UInt32.TryParse(btn.CommandArgument, out reviewId))
                {
                    GetShoppingFacade.DeleteItemReview(reviewId);
                    BindReviews(gId, 1, 50);
                }
            }
        }

        protected void rptReviews_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Request.IsAuthenticated && IsAdministrator())
                {
                    e.Item.FindControl("lbDeleteComment").Visible = true;
                }
            }
        }

        protected void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            ShowPurchaseSummary();
        }

        #endregion Event Handlers

        #region GM Purchase For Free 

        private void CompletePurchaseGetForFree()
        {
            int gId = GlobalId();
            WOKItem item = GetShoppingFacade.GetItem(gId);
            int quantity = 1;
            int purchaseId = 0;

            msgDialog.Title = "Purchase Summary";
            msgDialog.MessageHeader = "Warning:";

            bool isCustomDeed = (item.UseType == WOKItem.USE_TYPE_CUSTOM_DEED);
            bool isScriptGameItem = (item.UseType == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM);
            bool bRewards = item.InventoryType.Equals(CURRENCY_TYPE_CREDITS) ? false : true;
            int itemCheckoutPrice = GetItemCheckoutPrice(item);
            int iOrderTotal = itemCheckoutPrice * quantity;

            if (isCustomDeed)
            {
                // Validate community name if new
                if (radImport.Checked)
                {
                    // No validation because we do auto-naming of community
                }

                // Validate World name if new
                if (radNew3DApp.Checked)
                {
                    if (!ValidateApplyZoneFields())
                    {
                        CancelPurchase();
                        return;
                    }
                }

                if (radExisting.Checked)
                {
                    // Get the world to which they want to apply the new zone
                    Community community = GetCommunityFacade.GetCommunity(Convert.ToInt32(ddlWorlds.SelectedValue));

                    if (community.CommunityId == 0)
                    {
                        CancelPurchase();
                        msgDialog.Message = "User world does not exist";
                        ShowErrorMessageDialog(msgDialog);
                        return;
                    }
                }
            }

            if (item.ItemActive.Equals((int)WOKItem.ItemActiveStates.Deleted))
            {
                ResetPurchaseButtons();
                msgDialog.Message = "This item is not available for purchase. It have been removed from shop.";
                ShowErrorMessageDialog(msgDialog);
                return;
            }

            // Purhcase the bundle or item
            if (item.UseType == (int)WOKItem.USE_TYPE_CUSTOM_DEED)
            {
                if (!PurchaseDeedGetForFree(item, quantity, bRewards, iOrderTotal))
                {
                    return;
                }
            }
            else if (item.UseType == (int)WOKItem.USE_TYPE_BUNDLE)
            {
                purchaseId = PurchaseBundleGetForFree(item, quantity, bRewards, iOrderTotal, 0);
            }
            else if (item.UseType == (int)WOKItem.USE_TYPE_SCRIPT_GAME_ITEM)
            {
                purchaseId = PurchaseScriptGameItemGetForFree(item, quantity, bRewards, iOrderTotal);
            }
            else
            {
                purchaseId = PurchaseItemGetForFree(item, quantity, bRewards, iOrderTotal, 0, true, false);
            }

            if (purchaseId.Equals(0) && item.UseType != (int)WOKItem.USE_TYPE_CUSTOM_DEED)
            {
                ResetPurchaseButtons();
                msgDialog.Message = "There was an error with your purchase. Please try again.";
                ShowErrorMessageDialog(msgDialog);
                return;
            }

            // Find which server user is on and tickle that server for inventory update
            try
            {
                // return server user is currently on
                DataTable dt = new GameFacade().GetGameUserIn(KanevaWebGlobals.CurrentUser.UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    // Check to see if user is GM and send event to server which will make the item available to the appserver
                    int gameId = Convert.ToInt32(dt.Rows[0]["game_id"]);
                    int serverId = Convert.ToInt32(dt.Rows[0]["server_id"]);
                    (new RemoteEventSender()).broadcastItemChangeEventToServer(RemoteEventSender.ObjectType.PlayerInventory, RemoteEventSender.ChangeType.AddObject, KanevaWebGlobals.CurrentUser.UserId, serverId);
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Tickler failure buying item", exc);
            }

            pnlRelated.Visible = false;
            pnlBuy.Visible = false;
            pnlComplete.Visible = true;
            litSuccessMessage.Text = string.Format("<p>You can find your new purchase in <span>Inventory{0}</span>.</p>", item.UseType == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM ? " in the Gaming tab" : "");
            pnlCompleteZonePurchase.Visible = false;

            if (item.UseType == WOKItem.USE_TYPE_CUSTOM_DEED)
            {
                pnlComplete.Visible = false;
                pnlCompleteZonePurchase.Visible = true;
            }
            else
            {
                // Configure Play Now Button to go to users home
                ScriptManager.RegisterStartupScript(this, GetType(), "playnow", "pn_js = function() {" + StpUrl.GetPluginJSForUser(Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, KanevaWebGlobals.CurrentUser.Username, "", KanevaWebGlobals.CurrentUser.UserId) + "};", true);
            }
        }

        private int PurchaseItemGetForFree(WOKItem item, int quantity, bool bRewards, int iOrderTotal, int parentPurchaseId, bool bAddToInventory, bool bAddToBank)
        {
            UInt32 iTemplateCreatorId = 0;
            UInt32 iTemplateCreatorsComission = 0;
            GetUGCTemplateInfo(item, out iTemplateCreatorId, out iTemplateCreatorsComission);
            iTemplateCreatorsComission = 0;

            int currUserId = KanevaWebGlobals.CurrentUser.UserId;
            int purchaseId = 0;

            // Give them the item
            if (bAddToInventory)
            {
                GetUserFacade.AddItemToPendingInventory(currUserId, WOK_INVENTORY_TYPE_PERSONAL, item.GlobalId, quantity, 512);
            }
            else if (bAddToBank)
            {
                GetUserFacade.AddItemToPendingInventory(currUserId, WOK_INVENTORY_TYPE_BANK, item.GlobalId, quantity, 512);
            }


            // Add it to their history but will not get added to their sales numbers
            purchaseId = GetShoppingFacade.AddItemPurchase(item.GlobalId, (int)ItemPurchase.eItemTxnType.FREE_FOR_GM, currUserId, item.ItemCreatorId, iOrderTotal, 0, Common.GetVisitorIPAddress(), bRewards ? Constants.CURR_GPOINT : Constants.CURR_KPOINT, quantity, parentPurchaseId);

            return purchaseId;
        }

        private int PurchaseBundleGetForFree(WOKItem bundle, int quantity, bool bRewards, int iOrderTotal, int parentPurchaseId, bool addOnlyToBank = false)
        {
            UInt32 iDesignerCommision = Convert.ToUInt32(bundle.DesignerPrice * quantity);
            int purchaseId = 0;

            // add the bundle purchase 
            purchaseId = GetShoppingFacade.AddItemPurchase(bundle.GlobalId, (int)ItemPurchase.eItemTxnType.FREE_FOR_GM, KanevaWebGlobals.CurrentUser.UserId, bundle.ItemCreatorId, iOrderTotal, 0, Common.GetVisitorIPAddress(), Constants.CURR_GPOINT, quantity, parentPurchaseId);

            // add items to users inventory
            foreach (WOKItem item in bundle.BundleItems)
            {
                PurchaseItemGetForFree(item, quantity, bRewards, iOrderTotal, purchaseId, !addOnlyToBank, addOnlyToBank);
            }

            return purchaseId;
        }

        private int PurchaseScriptGameItemGetForFree(WOKItem wokItem, int quantity, bool bRewards, int iOrderTotal)
        {
            sgItem = (sgItem ?? GetScriptGameItemFacade.GetSnapshotScriptGameItem(wokItem.GlobalId));

            int purchaseId = PurchaseItemGetForFree(wokItem, quantity, bRewards, iOrderTotal, 0, true, false);

            if (sgItem.BundleGlid > 0)
            {
                gameItemBundle = (gameItemBundle ?? GetShoppingFacade.GetItem(sgItem.BundleGlid, false, false));
                PurchaseBundleGetForFree(gameItemBundle, quantity, bRewards, iOrderTotal, purchaseId, true);
            }

            return purchaseId;
        }

        private void PurchaseDeedItemGetForFree(WOKItem deed, DeedItem item, bool bRewards, int quantity, int parentPurchaseId)
        {
            UInt32 iDesignerCommision = 0;
            UInt32 iTemplateCreatorId = 0;
            UInt32 iTemplateCreatorsComission;

            // No commissions or fee for kaneva items 
            // "PRD 5.3 - The base price and commission for all Kaneva owned items will be 0. "
            // Regarding !bRewards - See Animesh email titled "UGC credits/Rewards and upload changes" - UGC commission will be based on credits purchases only    
            // Deed owner does not get commision on any items in his deed
            if (!item.IsKanevaOwned && !bRewards && !item.ItemCreatorId.Equals(deed.ItemCreatorId))
            {
                WOKItem wokItem = GetShoppingFacade.GetItem(item.GlobalId);

                if (wokItem.DesignerPrice > 0)
                {
                    iDesignerCommision = Convert.ToUInt32(wokItem.DesignerPrice);

                    GetUserFacade.AdjustUserBalance(Convert.ToInt32(item.ItemCreatorId), Constants.CURR_KPOINT, Convert.ToDouble(iDesignerCommision), Constants.CASH_TT_ROYALITY);

                    // Show it as a sale for item owner
                    GetShoppingFacade.AddItemPurchase(wokItem.GlobalId, (int)ItemPurchase.eItemTxnType.COMMISSION, KanevaWebGlobals.CurrentUser.UserId, wokItem.ItemCreatorId, 0, iDesignerCommision, Common.GetVisitorIPAddress(), Constants.CURR_KPOINT, quantity, parentPurchaseId);
                }


                GetUGCTemplateInfo(wokItem, out iTemplateCreatorId, out iTemplateCreatorsComission);

                // Credit template designer if it is based on a custom template
                if (iTemplateCreatorsComission > 0)
                {
                    if (!bRewards && !iTemplateCreatorId.Equals(deed.ItemCreatorId))
                    {
                        GetUserFacade.AdjustUserBalance(Convert.ToInt32(iTemplateCreatorId), Constants.CURR_KPOINT, Convert.ToDouble(iTemplateCreatorsComission), Constants.CASH_TT_ROYALITY);

                        // Get base item global ID (the first derivative from the same template created by template designer)
                        int baseItemGlid = GetShoppingFacade.GetBaseItemGlobalId(item.GlobalId);

                        // Add template commission transaction to shopping history
                        GetShoppingFacade.AddItemPurchase(baseItemGlid, (int)ItemPurchase.eItemTxnType.COMMISSION, KanevaWebGlobals.CurrentUser.UserId, iTemplateCreatorId, 0, iTemplateCreatorsComission, Common.GetVisitorIPAddress(), Constants.CURR_KPOINT, quantity, parentPurchaseId);
                    }
                }
            }
        }

        private bool PurchaseDeedGetForFree(WOKItem deed, int quantity, bool bRewards, int iOrderTotal)
        {
            msgDialog.Title = "Purchase Summary";
            msgDialog.MessageHeader = "Error:";

            // Buy the deed                                                     
            int parentPurchaseId = PurchaseItemGetForFree(deed, quantity, bRewards, iOrderTotal, 0, false, false);

            if (parentPurchaseId < 1)
            {
                msgDialog.Message = "An error occurred during the purchase process.";
                ShowErrorMessageDialog(msgDialog);
                return false;
            }

            // Add deed item commisions
            HashSet<int> processedGlids = new HashSet<int>();
            deedList = GetShoppingFacade.GetDeedSalesItemList(deed.GlobalId, (int)deed.ItemCreatorId);

            foreach (DeedItem item in deedList)
            {
                if (!processedGlids.Contains(item.GlobalId))
                {
                    processedGlids.Add(item.GlobalId);
                    PurchaseDeedItemGetForFree(deed, item, bRewards, quantity, parentPurchaseId);

                    if (item.UseType == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM)
                    {
                        foreach (DeedItem deedItem in item.BundleItems)
                        {
                            if (!processedGlids.Contains(deedItem.GlobalId))
                            {
                                processedGlids.Add(deedItem.GlobalId);
                                PurchaseDeedItemGetForFree(deed, deedItem, bRewards, quantity, parentPurchaseId);
                            }
                        }
                    }
                }
            }

            string communityName = "";

            // Set unique name for community
            for (int i = 0; i < 10; i++)
            {
                // Create a unique community name to hold the zone
                communityName = deed.Name;
                if (communityName.Length > 30)
                {
                    communityName = communityName.Substring(0, 30);
                }
                communityName = deed.Name + " Copy" + (i == 0 ? "" : i.ToString()) + " " + KanevaWebGlobals.CurrentUser.UserId.ToString();
                if (!GetCommunityFacade.IsCommunityNameTaken(false, communityName.Replace(" ", ""), 0))
                {
                    break;
                }
                if (i == 9)
                {   // If we can't get unique name through 10 tries, just append date
                    communityName = communityName + " " + KanevaWebGlobals.CurrentUser.UserId.ToString() + " " + DateTime.Now.ToString("MMddyy_hhmmss");
                }
            }

            // Add the new zone to a community so the user will always be able to find
            // it in their import zones menu in world.  We do this for all zone purchases
            int importCommunityId;
            if (!CreateCommunity(communityName, deed, true, out importCommunityId))
            {
                return false;
            }

            // Apply the deed
            if (radImport.Checked || radNew3DApp.Checked)  // Add to Inventory or Create New World
            {
                // Create new world (community) and apply zone to it
                if (radNew3DApp.Checked)
                {
                    // Create the new community and apply the zone
                    int newCommunityId;
                    if (!CreateCommunity(txt3DAppName.Text.Trim(), deed, false, out newCommunityId))
                    {
                        msgDialog.MessageHeader = "Error:";
                        msgDialog.Message = "Error creating new World. Your World should be accessible from your import worlds list in build mode.";
                        ShowErrorMessageDialog(msgDialog);
                        return false;
                    }

                    // Configure Play Now Button
                    ScriptManager.RegisterStartupScript(this, GetType(), "playnow1", "pn_js = function() {" + StpUrl.GetPluginJS(Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, 0, newCommunityId, txt3DAppName.Text.Trim(), "", KanevaWebGlobals.CurrentUser.UserId) + "};", true);
                }
                else  // Don't need to create new community, zone already in users import menu.  Just set play now link
                {
                    string commNameNoSpaces = communityName.Replace(" ", "");

                    // Configure Play Now Button to go to users home
                    ScriptManager.RegisterStartupScript(this, GetType(), "playnow2", "pn_js = function() {" + StpUrl.GetPluginJSForUser(Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, KanevaWebGlobals.CurrentUser.Username, "", KanevaWebGlobals.CurrentUser.UserId) + "};", true);
                }
            }
            else if (radExisting.Checked)   // Apply new zone to an existing world
            {
                Community existingWorld = GetCommunityFacade.GetCommunity(Convert.ToInt32(ddlWorlds.SelectedValue));

                if (existingWorld.CommunityId == 0)
                {
                    msgDialog.Message = "User World does not exist.";
                    ShowErrorMessageDialog(msgDialog);
                }
                else
                {
                    // Home or Community
                    if (existingWorld.CommunityTypeId == (int)CommunityType.COMMUNITY ||
                        existingWorld.CommunityTypeId == (int)CommunityType.HOME)
                    {
                        // Call Move objects, populate the zone
                        int resultVal = 0;
                        int resultZoneIndex = 0;

                        bool isHome = false;
                        int zoneInstanceId = existingWorld.CommunityId;
                        if (existingWorld.CommunityTypeId == (int)CommunityType.HOME)
                        {
                            isHome = true;
                            zoneInstanceId = KanevaWebGlobals.CurrentUser.WokPlayerId;
                        }
                        DataRow drChannelZone = WOKStoreUtility.GetHangoutChannelZone(zoneInstanceId, isHome);
                        // this param is incorrect !!!!!
                        // Get Zone info for the source zone (zone just purchased).  We use the community that 
                        // was created to hold the newly purchased zone in the inventory.
                        DataRow drSourceChannelZone = WOKStoreUtility.GetHangoutChannelZone(importCommunityId, false);
                        ZoneIndex newZoneIndex = new ZoneIndex(Convert.ToInt32(drSourceChannelZone["zone_index"]));
                        int i = newZoneIndex.ZoneIndexPlain;

                        // Import the new zone
                        GetShoppingFacade.ImportZone(KanevaWebGlobals.CurrentUser.WokPlayerId, Convert.ToInt32(drChannelZone["zone_index"]),
                            Convert.ToInt32(drChannelZone["zone_instance_id"]), newZoneIndex.ZoneIndexPlain, Convert.ToInt32(drSourceChannelZone["zone_index"]),
                            Convert.ToInt32(drSourceChannelZone["zone_instance_id"]), true, ref resultVal, ref resultZoneIndex);

                        GetGameFacade.ClearZoneCustomizationsCache(Convert.ToInt32(drChannelZone["zone_index"]), Convert.ToInt32(drChannelZone["zone_instance_id"]));

                        // Set server for restart
                        try
                        {
                            GetGameFacade.BroadcastUpdateScriptZoneSpinDownDelayEvent(Convert.ToInt32(drChannelZone["channel_zone_id"]));
                        }
                        catch (Exception) { }
                    }

                    // Configure play now button
                    ScriptManager.RegisterStartupScript(this, GetType(), "playnow3", "pn_js = function() {" + StpUrl.GetPluginJS(Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, existingWorld.WOK3App.GameId, existingWorld.CommunityId, existingWorld.Name, "", KanevaWebGlobals.CurrentUser.UserId) + "};", true);
                }
            }

            return true;
        }

        private bool ValidateApplyZoneFieldsGetForFree()
        {
            if (radNew3DApp.Checked)
            {
                msgDialog.Title = "Purchase Summary";
                msgDialog.MessageHeader = "Warning:";

                if (!Regex.IsMatch(txt3DAppName.Text.Trim(), "^" + Constants.VALIDATION_REGEX_CHANNEL_NAME + "$"))
                {
                    string errorStr = "The name you entered must be between 4 - 50 characters in length.";
                    ShowInlineErrorMessage(divNewWorldTxtBoxErr, errorStr);
                    txt3DAppName.Style.Add("border", "1px solid #ee2d2a");
                    PopulateWorldsList();
                    return false;
                }

                string strOriginalName = txt3DAppName.Text.Trim();

                // Check for community already exists
                if (GetCommunityFacade.IsCommunityNameTaken(false, strOriginalName.Trim().Replace(" ", ""), 0))
                {
                    msgDialog.Message = "World " + strOriginalName + " already exists, please change the name. We recommend " + GetSuggestedName(strOriginalName);
                    ShowErrorMessageDialog(msgDialog);
                    PopulateWorldsList();
                    return false;
                }
            }
            else if (radImport.Checked)
            {
                // We auto-create the community name for this zone, nothing to validate //
            }
            else if (radExisting.Checked)
            {
                if (ddlWorlds.SelectedValue == null || ddlWorlds.SelectedValue == String.Empty || Convert.ToInt32(ddlWorlds.SelectedValue) < 0)
                {
                    string errorStr = "You must select a world.";
                    ShowInlineErrorMessage(divExistingWorldError, errorStr);
                    ddlWorlds.Style.Add("border", "1px solid #ee2d2a");
                    aNew3DWorld.Attributes.Remove("class");
                    radNew3DAppMsg.Style.Add("display", "none");
                    aExistingWorld.Attributes.Add("class", "selected");
                    radExistingMsg.Style.Add("display", "block");
                    PopulateWorldsList();
                    return false;
                }
            }

            return true;
        }

        protected void lbGetForFree_OnClick(object sender, EventArgs e)
        {
            // They must be GM 
            if (!UserIsGM)
            {
                Response.Redirect(ResolveUrl("~/default.aspx"));
                return;
            }

            ShoppingFacade shoppingFacade = new ShoppingFacade();
            int gId = GlobalId();
            WOKItem item = shoppingFacade.GetItem(gId);
            int quantity = GetQuantity();

            // Is it available for purchase?
            if (item.InventoryType.Equals(0))
            {
                CancelPurchase();
                ScriptManager.RegisterStartupScript(this, GetType(), "alertsum1", "alert('This item is not available for purchase.');", true);
                return;
            }

            // Is it access pass?
            if (GetShoppingFacade.IsItemAccessPass(gId) && !KanevaWebGlobals.CurrentUser.HasAccessPass)
            {
                CancelPurchase();
                ScriptManager.RegisterStartupScript(this, GetType(), "alertsum2", "alert('This item requires Access Pass to purchase! Please purchase an Access Pass first.');", true);
                return;
            }

            if (item.UseType == WOKItem.USE_TYPE_CUSTOM_DEED)
            {
                pnlZoneSelect.Visible = true;
                pnlRelated.Visible = false;
                pnlBuy.Visible = false;
                pnlComplete.Visible = false;
                pnlCompleteZonePurchase.Visible = false;

                // Set default selections
                radExisting.Checked = false;
                radImport.Checked = false;
                radNew3DApp.Checked = true;
                rblTemplates.SelectedIndex = 0;

                // Populate Worlds into Existing World drop-down
                PopulateWorldsList();

                // Set the continue button
                lbCompleteCommunityGetForFree.Style.Add("display", "block");
                lnkZoneNext.Style.Add("display", "none");

                // New Style for purchase forms
                divZoneSelect.Attributes.Add("class", "purchasenew");
                HideSectionsForPurchase();
            }
            else
            {
                CompletePurchaseGetForFree();
                /*              
                               // Is it available for purchase?
                               if (item.InventoryType.Equals(0))
                               {
                                   CancelPurchase();
                                   ScriptManager.RegisterStartupScript(this, GetType(), "alertsum1", "alert('This item is not available for purchase.');", true);
                                   return;
                               }

                               // Is it access pass?
                               if (GetShoppingFacade.IsItemAccessPass(gId) && !KanevaWebGlobals.CurrentUser.HasAccessPass)
                               {
                                   CancelPurchase();
                                   ScriptManager.RegisterStartupScript(this, GetType(), "alertsum2", "alert('This item requires Access Pass to purchase! Please purchase an Access Pass first.');", true);
                                   return;
                               }

                               // Does the user have enough credits?
                               UInt64 iCredits = 0;
                               UInt64 iRewards = 0;

                               int itemCheckoutPrice = 0;

                               // Set the prices
                               lblItemNamePurchase.Text = KanevaGlobals.TruncateWithEllipsis(item.Name, 60);
                               lblPriceCheckout.Text = itemCheckoutPrice > 0 ? itemCheckoutPrice.ToString("N0") : "Free";
                               lblCreditsCheckout.Text = iCredits.ToString("N0");
                               lblRewards.Text = iRewards.ToString("N0");
                               Int64 iEndBalance = (Convert.ToInt64(iCredits) - (itemCheckoutPrice * quantity));

                               spnRewardBalance.Visible = false;
                               spnCreditBalance.Visible = false;
                               pnlRelated.Visible = false;
                               pnlComplete.Visible = false;
                               pnlCompleteZonePurchase.Visible = false;
                               divCol2.Visible = false;
                               lbCompleteRewards.Visible = false;
                               lbComplete.Visible = false;

                               pnlBuy.Visible = true;
                               lbCompleteGMForFree.Visible = true;

                               HideSectionsForPurchase();
                 * */
            }
        }

        protected void lbCompletePurchaseGetForFree_OnClick(object sender, EventArgs e)
        {
            //    CompletePurchaseGetForFree();   
        }

        protected void lbCompleteCommunityGetForFree_OnClick(object sender, EventArgs e)
        {
            // They must be GM 
            if (!UserIsGM)
            {
                Response.Redirect(ResolveUrl("~/default.aspx"));
                return;
            }

            ShoppingFacade shoppingFacade = new ShoppingFacade();
            int gId = GlobalId();
            WOKItem item = shoppingFacade.GetItem(gId);
            int quantity = GetQuantity();

            // Is it available for purchase?
            if (item.InventoryType.Equals(0))
            {
                CancelPurchase();
                ScriptManager.RegisterStartupScript(this, GetType(), "alertsum1", "alert('This item is not available for purchase.');", true);
                return;
            }

            // Is it access pass?
            if (GetShoppingFacade.IsItemAccessPass(gId) && !KanevaWebGlobals.CurrentUser.HasAccessPass)
            {
                CancelPurchase();
                ScriptManager.RegisterStartupScript(this, GetType(), "alertsum2", "alert('This item requires Access Pass to purchase! Please purchase an Access Pass first.');", true);
                return;
            }

            // Validate Fields
            if (ValidateApplyZoneFields())
            {
                pnlZoneSelect.Visible = false;

                CompletePurchaseGetForFree();
            }
        }

        #endregion GM Purchase For Free

        #region Properties

        /// <summary>
        /// GlobalId
        /// </summary>
        private int GlobalId()
        {
            int gId = 0;

            try
            {
                gId = Convert.ToInt32(Request["gId"]);
            }
            catch (Exception)
            {
                Response.Redirect("~/Catalog.aspx");
                Response.End();
            }

            return gId;
        }

        /// <summary>
        /// OriginalPrice
        /// </summary>
        private int OriginalPrice
        {
            get
            {
                if (ViewState["OriginalPrice"] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(ViewState["OriginalPrice"]);
                }
            }
            set
            {
                ViewState["OriginalPrice"] = value;
            }
        }

        private uint ItemCreatorId
        {
            get
            {
                if (ViewState["ItemCreatorId"] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToUInt32(ViewState["ItemCreatorId"]);
                }
            }
            set
            {
                ViewState["ItemCreatorId"] = value;
            }
        }

        private class MessageDialog
        {
            private string title = "";
            private string msg = "";
            private string msgHeader = "";
            private string okBtnUrl = "";

            public MessageDialog() { }

            public string Title
            {
                get { return title; }
                set { title = value; }
            }
            public string Message
            {
                get { return msg; }
                set { msg = value; }
            }
            public string MessageHeader
            {
                get { return msgHeader; }
                set { msgHeader = value; }
            }
            public string OkButtonUrl
            {
                get { return okBtnUrl; }
                set { okBtnUrl = value; }
            }
            public string FormattedMessage
            {
                get
                {
                    return "<p class=\"title red\">" + msgHeader + "</p><p class=\"desc\">" + msg + "</p>";
                }
            }
        }

        private bool UserIsGM
        {
            get
            {
                bool isAdmin = false;
                bool isGM = false;
                DateTime secondToLastLogin = DateTime.MinValue;
                GetUserFacade.GetPlayerAdmin(KanevaWebGlobals.CurrentUser.UserId, out isAdmin, out isGM, out secondToLastLogin);
                return isAdmin;
            }
        }

        #endregion Properties

        #region Declerations

        private MessageDialog msgDialog = new MessageDialog();
        private const UInt32 ERR_PARSING_FAILED = 0xFFFFFFFF;
        string WOK_INVENTORY_TYPE_PERSONAL = "P";
        string WOK_INVENTORY_TYPE_BANK = "B";
        List<DeedItem> deedList = null;
        ScriptGameItem sgItem = null;
        WOKItem gameItemBundle = null;

        protected Button btnPurchase, btnAction;
        protected LinkButton btn_preview, lbPurchase, lbRpt, lbRave, lbGetForFree, lbCompleteGetForFree, lbCompleteCommunityGetForFree, lbSubmitComment;
        protected HtmlAnchor aNew3DWorld, aExistingWorld, aImportZone;
        protected Label lblName, lblDescription, lblPrice, lblPurchaseType, lblVIPPrice, lblVIPPurchaseType, lblUsername;
        protected HtmlImage imgItem;
        protected Repeater rptItems, rptReviews, rptCategories, rptBundleItems;
        protected HtmlGenericControl spnRewardsMsg, divCreditsMsg, divRewardsMsg, buyVIPContainer, spnCreditBalance, spnRewardBalance;
        protected HtmlGenericControl spnAnimationOnly, divRequiredItem, divAnimations;
        protected HtmlContainerControl litNoReviews;
        protected HtmlIframe ifItemPreview;
        protected HtmlAnchor aReqItem;
        protected HtmlAnchor aReport, aShare, aAdminEdit, aOwnerEdit, aBuyVIP;
        protected HtmlTextArea txtComment;
        protected HtmlImage imgUseType;
        protected Literal litRaveCount, litPlayNowJS, litGMPurchase;
        protected Label lblNumberOfReviews1;
        protected Panel pnlRelated, pnlBuy, pnlComplete, pnlZoneSelect, pnlCompleteZonePurchase;
        protected Label lblPriceCheckout, lblCreditsCheckout, lblRewards;
        protected CheckBox chkBlastPurchase;
        protected LinkButton lbCompleteRewards, lbComplete, lnkZoneNext, btn_purchase, lbGetCredits;
        protected Label lblItemNamePurchase;
        protected TextBox txtQuantity;
        protected HtmlTableCell tdActionText, tdButton, tdPolicyText, tdActionText2;
        protected Literal litGlobalId, litCtrlRaveCount;
        protected HtmlAnchor aUser;
        protected HtmlAnchor aTransTracker;
        protected Literal litJSVars, litSuccessMessage;
        protected HtmlControl divRestricted;
        private bool _canEdit = false;
        protected HtmlContainerControl h2Title, divAccessMessage, divDetails, divSoundPreview, divExistingWorldError;
        protected HtmlContainerControl divIEplayer, divFFplayer, tabBundleItems, divOwnerBtns, divNewWorldTxtBoxErr, tabAnimations;
        protected Literal litSoundPreview, litSoundPreview2;
        protected HtmlInputRadioButton radImport, radExisting, radNew3DApp, rbCommTemplate;
        protected RadioButtonList rblTemplates;
        protected TextBox txtName, txt3DAppName;
        protected HtmlContainerControl pricecontainer, divZoneSelect, divBuy, spnHideDuringPurchase, pBack, radNew3DAppMsg, radExistingMsg, radImportMsg;
        protected DropDownList ddlWorlds;

        // Google Tracking 
        protected Literal litGAAcct, litOrderId1, litOrderId2, litOrderTotal, litTEST;
        protected Literal litOrderCity, litOrderState, litOrderCountry;
        protected Literal litItemSKU, litItemName, litItemCategory, litItemPrice, litQuantity, litLoadDeedItems;

        const int CURRENCY_TYPE_CREDITS = 256;
        const int CURRENCY_TYPE_REWARDS = 512;
        const int CURRENCY_TYPE_BOTH_R_C = 768;

        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Declerations
    }
}
