///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Linq;

using System.Data;
using log4net;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
    public partial class Deed : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

            // Check for params
            if ((Request.Params["zoneIndex"] == null) || (Request.Params["instanceId"] == null) || (Request.Params["zone_index_plain"] == null))
            {
                string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>zoneIndex or instanceId or zone_index_plain not specified</ResultDescription>\r\n</Result>";
                Response.Clear ();
                Response.Write(errorStr);
                return;
            }

            int totalObjectsInZone = DeedItemList.Count;

            // Per discussion with AS, admins are exempt from business rules here. This allows us to create templates easily.
            if (!IsAdministrator())
            {
                // Check they own the zone
                if (!GetCommunityFacade.IsUserWOK3DPlaceOwner(KanevaWebGlobals.CurrentUser.UserId, ZoneIndex(), ZoneInstanceId()))
                {
                    ShowErrorOnStartup("You must be the owner to create this World");
                    return;
                }

                // Check to see if this is a base deed
                if (!GetShoppingFacade.IsOriginalUGCZone(ZoneIndex(), ZoneInstanceId()))
                {
                    ShowErrorOnStartup("This World cannot be uploaded to the Shop since it was originally owned by another creator.");
                    return;
                }

                // Check number of items in zone
                if (totalObjectsInZone > Configuration.MaxObjectsAllowedInZone)
                {
                    ShowErrorOnStartup("Too many objects are in this world, world must be under " + Configuration.MaxObjectsAllowedInZone + " objects to create a Deed. Current count is " + totalObjectsInZone + ".");
                    return;
                }

                // Prevent upload of parent zones for non-admins
                if (GetGameFacade.IsParentZone(ZoneInstanceId(), ZoneType()))
                {
                    ShowErrorOnStartup("Linked worlds may not be uploaded to shop.");
                    return;
                }
            }

            // Even admins can't upload child zones
            int parentCommunityId;
            if (GetGameFacade.IsChildZone(ZoneInstanceId(), ZoneType(), out parentCommunityId))
            {
                ShowErrorOnStartup("Linked worlds may not be uploaded to shop.");
                return;
            }

            IsDeedAP (DeedItemList);
            IsDeedCreditsOnly(DeedItemList);

            litItemCount.Text = "(" + totalObjectsInZone + " items)";
            litItemCountHide.Text = "Items in World (" + totalObjectsInZone + ")";
            litItemCount.Visible = totalObjectsInZone > 0;
            btnShowItems.Visible = totalObjectsInZone > 0;

            if (DeedItemList.Where(di => di.ItemCreatorId == KanevaWebGlobals.CurrentUser.UserId && di.UseType != (int)WOKItem.USE_TYPE_SCRIPT_GAME_ITEM).Count() > 0)
            {
                pCreatorMsg.Visible = true;
            }

            revTitle.ValidationExpression = Constants.VALIDATION_REGEX_CHANNEL_NAME;
            revTitle.ErrorMessage = "World name should be at least four characters and can contain only letters, numbers, spaces or underscore.";

            if (!IsPostBack)
            {
                Title = "Upload World to Shop Kaneva";
                ((MainTemplatePage)Master).SearchTitle = "Upload World to Shop Kaneva";

                SetDefaultThumbnail();
                SetPriceFields(DeedItemList);
            }
        }

        #region Helper Methods

        /// <summary>
        /// ZoneIndex
        /// </summary>
        private int ZoneIndex()
        {
            int zoneIndex = 0;

            try
            {
                zoneIndex = Convert.ToInt32(Request["zoneIndex"]);
            }
            catch (Exception)
            {
                Response.Redirect("~/Catalog.aspx");
                Response.End();
            }

            return zoneIndex;
        }

        private int ZoneType()
        {
            return (new ZoneIndex(ZoneIndex())).ZoneType;
        }


        /// <summary>
        /// ZoneInstanceId
        /// </summary>
        private int ZoneInstanceId()
        {
            int zoneInstanceId = 0;

            try
            {
                zoneInstanceId = Convert.ToInt32(Request["instanceId"]);
            }
            catch (Exception)
            {
                Response.Redirect("~/Catalog.aspx");
                Response.End();
            }

            return zoneInstanceId;
        }


        /// <summary>
        /// ZoneIndexPlain
        /// </summary>
        private int ZoneIndexPlain()
        {
            int zone_index_plain = 0;

            try
            {
                zone_index_plain = Convert.ToInt32(Request["zone_index_plain"]);
            }
            catch (Exception)
            {
                Response.Redirect("~/Catalog.aspx");
                Response.End();
            }

            return zone_index_plain;
        }


        private void SetPriceFields(List<DeedItem> items)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            DeedPrice prices = shoppingFacade.CalculateInitialDeedPrice(items, GetCommissionPrice(), (uint)KanevaWebGlobals.CurrentUser.UserId, FrameworkEnabled);

            totalNotOwnedItems.InnerText = prices.TotalItemsNotOwned.ToString();
            totalOwnedItems.InnerText = prices.TotalItemsOwned.ToString();
            catalogFee.InnerText = (prices.TotalDeedWithCommission - (prices.TotalItemsNotOwned + prices.TotalItemsOwned + prices.DesignCommission)).ToString();
            totalDeed.InnerText = prices.TotalDeedWithCommission.ToString();
            totalVIP.InnerText = (prices.TotalItemsNotOwnedPlusDesignDeCommission > 0 ? prices.TotalItemsNotOwnedPlusDesignDeCommission.ToString() : "FREE");
        }

        private void SetDefaultThumbnail()
        {
            int communityId = GetCommunityFacade.GetCommunityIdFromWorldIds(KanevaGlobals.WokGameId, 0, ZoneIndex(), ZoneInstanceId(), ZoneType());
            Community world = GetCommunityFacade.GetCommunity(communityId);

            if (world.HasThumbnail && !string.IsNullOrWhiteSpace(world.ThumbnailPath) && !string.IsNullOrWhiteSpace(world.ThumbnailMediumPath))
            {
                if (world.ThumbnailPath.StartsWith("GameTemplates"))
                {
                    world.ThumbnailPath = Server.MapPath(Path.Combine("~/images/world_template_thumbs", world.ThumbnailLargePath));                  
                }

                if (File.Exists(world.ThumbnailPath))
                {
                    DefaultThumbnailFilePath = world.ThumbnailPath;
                    DefaultThumbnailUrl = CommunityUtility.GetBroadcastChannelImageURL(world.ThumbnailMediumPath, "me");
                }
            }

            imgItem.Src = DefaultThumbnailUrl;
        }

        private int GetCommision(int TotalDeed)
        {
            return (int) (TotalDeed * .1);
        }

        private int GetCommissionPrice()
        {
            int ret = 0;
            try
            {
                if (txtPrice.Text.Trim().Length > 0)
                {
                    ret = Convert.ToInt32(txtPrice.Text);
                }
            }
            catch { }
            return ret;
        }

        private void IsDeedAP(List<DeedItem> items)
        {
            foreach (DeedItem di in items)
            {
                if (di.IsAP)
                {
                    DeedIsAP = true;
                    break;
                }
            }
        }

        private void IsDeedCreditsOnly(List<DeedItem> items)
        {
            if (FrameworkEnabled)
            {
                DeedIsCreditsOnly = true;
                return;
            }

            foreach (DeedItem di in items)
            {
                if (di.CreditsOnly)
                {
                    DeedIsCreditsOnly = true;
                    break;
                }
            }
        }

        public string GetItemPrice(object webPrice, object basePrice, object itemOwnerId)
        {
            int ownerId = Convert.ToInt32(itemOwnerId);

            if (KanevaWebGlobals.CurrentUser.UserId == ownerId)
            {
                return basePrice.ToString();
            }
            else
            {
                return webPrice.ToString();
            }
        }

        #endregion Helper Methods


        #region Event Handlers

        public void btnAdd_Click(object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

            ShoppingFacade shoppingFacade = new ShoppingFacade();
            WOKItem item = new WOKItem();
            bool isErr = false;
            int assetId = 0;

            // Validate image upload. Must be either nothing, a media library photo, or an upload.
            HttpPostedFile fThumbnail = null;
            if (string.IsNullOrWhiteSpace(hfMediaLibPhoto.Value))
            {
                // Get the thumbnail to upload
                fThumbnail = inpThumbnail.PostedFile;
                if (fThumbnail != null && !fThumbnail.ContentLength.Equals(0))
                {
                    if (fThumbnail.ContentLength > KanevaGlobals.MaxUploadedImageSize)
                    {
                        revThumbnail.IsValid = false;
                        revThumbnail.ErrorMessage = "Thumbnail Image is too large, please select a photo smaller than " + KanevaGlobals.FormatImageSize(KanevaGlobals.MaxUploadedImageSize) + ".";
                        isErr = true;
                    }
                    else
                    {
                        // Thumbnail must be a JPG, JPEG, GIF!!!
                        string strExtensionUpperCase = Path.GetExtension(fThumbnail.FileName).ToUpper();
                        if (!strExtensionUpperCase.Equals(".JPG") && !strExtensionUpperCase.Equals(".JPEG") &&
                            !strExtensionUpperCase.Equals(".GIF"))
                        {
                            revThumbnail.IsValid = false;
                            isErr = true;
                        }
                    }
                }
            }
            else if (!Int32.TryParse(hfMediaLibPhoto.Value, out assetId) || assetId <= 0)
            {
                revThumbnail.IsValid = false;
                isErr = true;
            }

            // If any of the above validations failed, return to page and display error messages
            if (isErr || !Page.IsValid)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error","<script language='javascript'>ShowLoading(false);</script>"); 
                return;
            }

            item.BaseGlobalId = 0;

            // Get the other params
            item.ItemCreatorId = Convert.ToUInt32(KanevaWebGlobals.CurrentUser.UserId);
            item.Name = Server.HtmlEncode(txtTitle.Text.Trim());
            item.DisplayName = Server.HtmlEncode(txtTitle.Text.Trim());
            item.Description = Server.HtmlEncode(txtDescription.Text.Trim());

            // Set whether deed is credits only purchase
            item.InventoryType = DeedIsCreditsOnly ? DeedItem.CURRENCY_TYPE_CREDITS : DeedItem.CURRENCY_TYPE_BOTH_R_C;

            // Set whether deed is AP or not
            item.PassTypeId = DeedIsAP ? (int)Constants.ePASS_TYPE.ACCESS : (int)Constants.ePASS_TYPE.GENERAL;

            item.ItemActive = (int)WOKItem.ItemActiveStates.Public;

            // Get the price of the deed 
            DeedPrice bp = shoppingFacade.CalculateInitialDeedPrice(DeedItemList, GetCommissionPrice(), (uint)KanevaWebGlobals.CurrentUser.UserId, FrameworkEnabled);
            // For Deeds Only: The stored market price is the actual web price because we can not
            // calcuate the web price without looping through all the items in the deed to determine
            // which items already have commission added (items not owned by deed owner) and which items
            // do not have commission added (items owned by deed owner).  We store the price this way because
            // the web price is calculated everywhere it is displayed except in search and we do not want to 
            // recalculate the price on every search.  So the web price total is stored for easy retrieval and speed.
            item.MarketCost = bp.TotalDeedWithCommission;

            item.UseType = WOKItem.USE_TYPE_CUSTOM_DEED;
            item.DerivationLevel = WOKItem.DRL_KANEVAITEM;

            try
            {
                item.DesignerPrice = Convert.ToUInt32(txtPrice.Text.Trim());
            }
            catch (Exception) { }

            if (item.DesignerPrice > 5000000)
            {
                item.DesignerPrice = 5000000;
            }

            // Add it to the db
            int deedGlobalId = shoppingFacade.AddCustomItem(item);

            // Create a community per Brett request so travel SPs create the try on zone
            CommunityFacade communityFacade = new CommunityFacade ();
            Community community = new Community();
            community.Name = "Preview Deed - " + Server.HtmlEncode(txtTitle.Text.Trim()) + " " + deedGlobalId;
            community.NameNoSpaces = "PreviewDeed-" + Server.HtmlEncode(txtTitle.Text.Trim().Replace(" ", "")) + "" + deedGlobalId;
            community.IsPersonal = 0; //0 is false
            community.CommunityTypeId = (int)CommunityType.TRY_ON;
            community.StatusId = (int)CommunityStatus.ACTIVE;
            community.CreatorId = 1;
            community.CreatorUsername = "Administrator";
            community.IsAdult = Server.HtmlEncode("N");
            community.IsPublic = Server.HtmlEncode("Y");

            int communityId = communityFacade.InsertCommunity (community);
             
            if (communityId > 0)
            {
                //add the creator to the community
                CommunityMember member = new CommunityMember();
                member.CommunityId = communityId;
                member.UserId = community.CreatorId;
                member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.OWNER;
                member.Newsletter = "Y";
                member.InvitedByUserId = 0;
                member.AllowAssetUploads = "Y";
                member.AllowForumUse = "Y";
                member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

                communityFacade.InsertCommunityMember(member);
            }
            else
            {
                shoppingFacade.DeleteCustomItemTexture (deedGlobalId, KanevaWebGlobals.CurrentUser.UserId, false);
                ShowErrorOnStartup ("Unable to create deed");
                return;
            }

            // Call SP to create zone template
            bool createdTemplate = shoppingFacade.CreateCustomZoneTemplate(ZoneIndex(), ZoneInstanceId(), deedGlobalId, KanevaWebGlobals.CurrentUser.UserId);

            if (!createdTemplate)
            {
                shoppingFacade.DeleteCustomItemTexture(deedGlobalId, KanevaWebGlobals.CurrentUser.UserId, false);
                ShowErrorOnStartup("Unable to make world template");
                return;
            }
            
            // Always make a hangout 6
            int newZoneType = (int)WOK3DPlace.eZoneType.HANGOUT;
            int newZoneIndex = GetShoppingFacade.MakeZoneIndex(ZoneIndexPlain(), newZoneType);

            if (newZoneIndex < 1)
            {
                shoppingFacade.DeleteCustomItemTexture (deedGlobalId, KanevaWebGlobals.CurrentUser.UserId, false);
                ShowErrorOnStartup ("Unable to make zone index");
                return;
            }

            // Create a channel Zone record for Try On purposes
            int czRecordsInserted = GetShoppingFacade.InsertChannelZone(1, newZoneIndex, communityId, "Preview Deed - " + Server.HtmlEncode(txtTitle.Text.Trim()) + " " + deedGlobalId, 
                0, "NOT_TIED", KanevaWebGlobals.CurrentUser.Country, KanevaWebGlobals.CurrentUser.Age, 0);

            if (czRecordsInserted <= 0)
            {
                shoppingFacade.DeleteCustomItemTexture(deedGlobalId, KanevaWebGlobals.CurrentUser.UserId, false);
                ShowErrorOnStartup("Unable to make channel zone");
                return;
            }

            // Call Move objects, again for try on purposes
            int result = 0;
            string urlResult = "";

            // Add item parameters per Ryan
            shoppingFacade.UpdateItemParameter(deedGlobalId, ItemParameter.PARAM_TYPE_CUSTOM_DEED, ZoneIndexPlain().ToString());

            // Apply the deed to the try on zone, make sure it's owned by the "kaneva" player so players can't build in it.
            shoppingFacade.ChangeZoneMap(newZoneIndex, communityId, newZoneIndex, "kaneva", deedGlobalId,
                KanevaGlobals.WokGameName, ref result, ref urlResult);

            bool customDataOk = GetGameFacade.ConfigureTryOnZoneSGCustomData(communityId, newZoneType);

            if (result > 0 || !customDataOk)
            {
                shoppingFacade.DeleteCustomItemTexture (deedGlobalId, KanevaWebGlobals.CurrentUser.UserId, false);
                ShowErrorOnStartup("Error creating world objects");
                return;
            }


            // Set the category to architecture, home, and hangout
            //shoppingFacade.AddCategoryToItem(deedGlobalId, 1, 1);
            //shoppingFacade.AddCategoryToItem(deedGlobalId, 4, 2);
            //shoppingFacade.AddCategoryToItem(deedGlobalId, 5, 3);
            shoppingFacade.AddCategoryToItem(deedGlobalId, 1000, 1);

            // Generate the thumbnails. First try using a media library image.
            if (assetId > 0)
            {
                SetItemThumbnailsToAssetImage(assetId, deedGlobalId);
            }
            // Next try using an uploaded file.
            else if (fThumbnail != null && fThumbnail.ContentLength > 0)
            {
                string thumbnailFilename = "";
                string strPathUserIdGlobalId = Path.Combine(KanevaWebGlobals.CurrentUser.UserId.ToString(), deedGlobalId.ToString());
                string strFilePathTexture = Path.Combine(KanevaGlobals.TexturePath, strPathUserIdGlobalId);

                try
                {
                    ImageHelper.UploadImageFromUser(ref thumbnailFilename, fThumbnail, strFilePathTexture, KanevaGlobals.MaxUploadedImageSize);
                    string imagePath = fThumbnail.FileName.Remove(0, fThumbnail.FileName.LastIndexOf("\\") + 1);
                    string origImagePath = strFilePathTexture + Path.DirectorySeparatorChar + thumbnailFilename;

                    ImageHelper.GenerateWOKItemThumbs(thumbnailFilename, origImagePath, KanevaGlobals.TexturePath, KanevaWebGlobals.CurrentUser.UserId, deedGlobalId);
                }
                catch { }
            }
            // If all else fails, fallback to the default thumbnail.
            else
            {
                string filename = Path.GetFileName(DefaultThumbnailFilePath);
                ImageHelper.GenerateWOKItemThumbs(filename, DefaultThumbnailFilePath, KanevaGlobals.TexturePath, KanevaWebGlobals.CurrentUser.UserId, deedGlobalId);
            }

            // Blast if they wanted to Blast
            if (chkBlastUpload.Checked)
            {
                int currUserId = KanevaWebGlobals.CurrentUser.UserId;

                // getting a new instance of the item so all the fields are updated.
                WOKItem item2 = shoppingFacade.GetItem(item.GlobalId);

                m_logger.Debug("Blasting Item");
                try
                {
                    // Metrics tracking for accepts
                    UserFacade userFacade = new UserFacade();
                    string requestId = userFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_DEED_UPLOAD, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
                    string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_BLAST, 0);

                    string url = "http://" + KanevaGlobals.ShoppingSiteName + "/ItemDetails.aspx?gId=" + item2.GlobalId + "&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeSentIdBlast;
                    string itemThumbnail = StoreUtility.GetItemImageURL(item2.ThumbnailLargePath, "la");

                    BlastFacade blastFacade = new BlastFacade();

                    blastFacade.SendShopUploadItemBlast(currUserId, KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.NameNoSpaces,
                        item2.DisplayName, item2.Description, url, itemThumbnail, "", KanevaWebGlobals.CurrentUser.ThumbnailSmallPath);

                }
                catch (Exception ex)
                {
                    m_logger.Error("Blasting Item upload Failed. Item:" + item2.GlobalId + " UserId: " + currUserId + " " + ex.ToString());

                }
            }

            Response.Redirect(ResolveUrl("~/MyInventory.aspx?up=1&c=" + item.Category1.ToString()));
        }

        protected void btnShowItems_Click(object sender, EventArgs e)
        {
            rptDeedItems.DataSource = DeedItemList;
            rptDeedItems.DataBind();

            divDeedItems.Visible = true;
            divPricing.Visible = false;
        }

        protected void btnHideItems_Click(object sender, EventArgs e)
        {
            divDeedItems.Visible = false;
            divPricing.Visible = true;
        }

        /// <summary>
        /// 
        /// </summary>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MyInventory.aspx");
        }

        protected void txtPrice_TextChanged(object sender, EventArgs e)
        {
            SetPriceFields(this.DeedItemList);
        }

        #endregion Event Handlers


        #region Properties

        protected List<DeedItem> DeedItemList
        {
             get         
            {             
                if (ViewState["DeedItems"] == null)             
                {
                    List<DeedItem> lDeeditem = GetShoppingFacade.GetInitialDeedItemList(KanevaWebGlobals.CurrentUser.UserId, ZoneIndex(), ZoneInstanceId()).ToList();

                    ViewState["DeedItems"] = lDeeditem;
                    return lDeeditem;
                }
                return (List<DeedItem>)ViewState["DeedItems"];         
            }         
            set { ViewState["DeedItems"] = value; } 
           
        }

        protected bool DeedIsAP
        {
            get
            {
                if (ViewState["DeedIsAP"] == null)
                {
                    return false;
                }
                return Convert.ToBoolean(ViewState["DeedIsAP"]);
            }
            set { ViewState["DeedIsAP"] = value; }
        }

        protected bool DeedIsCreditsOnly
        {
            get
            {
                if (ViewState["DeedIsCreditsOnly"] == null)
                {
                    return false;
                }
                return Convert.ToBoolean(ViewState["DeedIsCreditsOnly"]);
            }
            set { ViewState["DeedIsCreditsOnly"] = value; }
        }

        protected bool FrameworkEnabled
        {
            get
            {
                if (ViewState["FrameworkEnabled"] == null)
                {
                    ViewState["FrameworkEnabled"] = GetGameFacade.IsFrameworkEnabled(ZoneInstanceId(), ZoneType());
                }
                return Convert.ToBoolean(ViewState["FrameworkEnabled"]);
            }
            set { ViewState["FrameworkEnabled"] = value; }
        }

        protected string DefaultThumbnailFilePath
        {
            get
            {
                if (ViewState["DefaultThumbnailFilePath"] == null)
                {
                    return Server.MapPath("~/images/default_world_thumb.jpg");
                }
                return ViewState["DefaultThumbnailFilePath"].ToString();
            }
            set { ViewState["DefaultThumbnailFilePath"] = value; }
        }

        protected string DefaultThumbnailUrl
        {
            get
            {
                if (ViewState["DefaultThumbnailUrl"] == null)
                {
                    return "images/default_world_thumb.jpg";
                }
                return ViewState["DefaultThumbnailUrl"].ToString();
            }
            set { ViewState["DefaultThumbnailUrl"] = value; }
        }

        #endregion Properties


        #region Declerations

        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Declerations
    }
}
