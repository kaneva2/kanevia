///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
    public class Floorplans : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set text in search bar
            ((MainTemplatePage)Master).SearchTitle = "Floorplans";
            ((MainTemplatePage)Master).SearchBoxText = "Floorplans";

            Title = "Shop Kaneva - Floorplans";

            if (!IsPostBack)
            {
                //ShoppingFacade shoppingFacade = new ShoppingFacade();
                //UInt32 categoryId = 1;

                //if (Request["cId"] != null)
                //{
                //    try
                //    {
                //        categoryId = Convert.ToUInt32(Request["cId"]);
                //    }
                //    catch (Exception) { };
                //}

                //rptPromotions.DataSource = shoppingFacade.GetItemPromotions(categoryId, 3);
                //rptPromotions.DataBind();
            }
        }

        //protected Repeater rptPromotions;
    }
}
