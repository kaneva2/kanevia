<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Kaneva.PresentationLayer.Shopping._Default" %>

<asp:Content ID="cntUpload" runat="server" ContentPlaceHolderID="cphBody">    

<script type="text/javascript" src="./jscript/swfobject.js"><!-- // External JS --></script>

<div id="content">

    <script type="text/javascript">
			var flashvars = {};
			var params = {};
			params.quality = "high";
			params.wmode = "transparent";
			params.allowscriptaccess="always";
			var attributes = {};
			attributes.id = "flashContent";
			swfobject.embedSWF("http://streaming.kaneva.com/ImageServer/media/shop/home/home-wrapper.swf", "flash1", "721", "269", "9.0.0", "expressInstall.swf", flashvars, params, attributes);
		</script>
          <div id="flashContent" class="flashArea">
            <div id="flash1"><a href="http://www.adobe.com/go/getflashplayer">
		<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
	</a></div></div>
                
  <div class="endRule"><img src="./images/content_sideRule.gif" alt="Verticle Rule" /></div>
  <div class="featColWrapper">
    <div class="featureCol">
    <div class="head1"><span>Buy Stuff</span></div>
    <div class="featureContent">
      <ul class="shoppingLinks">
      <li><a runat="server" href="~/CatalogLanding.aspx?cId=1"><img src="./images/icon_home.gif" alt="Shop for a new home" />Shop for a New Home</a></li>
      <li><a runat="server" href="~/CatalogLanding.aspx?cId=3"><img src="./images/icon_clothing.gif" alt="Shop for new Clothes" />Shop for New Clothes </a></li>
      <li><a runat="server" href="~/CatalogLanding.aspx?cId=2"><img src="./images/icon_furnishings.gif" alt="Shop for home furnisings" />Shop for Home Furnishings</a></li>
    </ul>
    </div>
    <div><img src="./images/featureCol_bottom.gif" alt="sdf" /></div>
  </div>
  <div class="featureCol">
    <div class="head2"><span>Create My Own Content</span></div>
    <div class="featureContent">
      <div><a runat="server" id="aDesigner1"><img src="./images/icon_tee.gif" alt="Download textures now!" /></a></div>
      <div><a runat="server" id="aDesigner2">Download Textures Now!</a></div>
	</div>
    <div><img src="./images/featureCol_bottom.gif" alt="sdf" /></div>
  </div>
  <div class="featureCol">
    <div class="head3"><span>Open My Own Store</span></div>
    <div class="featureContent">
      <div><a runat="server" href="~/MySales.aspx"><img src="./images/icon_store.gif" alt="Sell Your Own 3D Merchandise and Earn Kaneva Credits!" /></a></div>
      <div><a runat="server" href="~/MySales.aspx">Sell Your Own 3D Merchandise and Earn Kaneva Credits</a>!</div>
    </div>
    <div><img src="./images/featureCol_bottom.gif" alt="sdf" /></div>
  </div>
  </div>
<div class="endRule"><img src="./images/content_sideRule.gif" alt="Verticle Rule" /></div>
</div>

</asp:Content>