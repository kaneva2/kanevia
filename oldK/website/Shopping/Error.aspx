<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplateNoLeftNav.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="Kaneva.PresentationLayer.Shopping.Error" %>

<asp:Content ID="cntUpload" runat="server" ContentPlaceHolderID="cphBody">
    <div id="content">
        <table border="0" cellpadding="10" cellspacing="0" width="560">
            <tr align="center">
	            <td align="left" class="formError"><span class="formError"><font size="+1">Application Error</font></span><br>
	            <hr size="1px" noshade style="color:red;" width="65%"/><br>
	            We are sorry, but Kaneva experienced a problem completing your request. 
	            <BR><BR>
	            </td>
            </tr>
            <tr align="left">
	            <td class="bodyText">
	            You can:<BR>
	            -Use your back button and retry your request<BR>
	            -Return to the <a id="A1" runat="server" href="~/default.aspx">home page</a>
	            </td>
            </tr>
        </table>
    </div>
</asp:Content>
