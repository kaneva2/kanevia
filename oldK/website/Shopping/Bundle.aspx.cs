///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using log4net;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
    public partial class Bundle : BasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

            if (!IsPostBack)
            {
                Title = "Create a Bundle";
                ((MainTemplatePage) Master).SearchTitle = "Create a Bundle";
            }
        }

        #region Helper Methods

        private void SetPriceFields (List<BundleItem> items)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            BundlePrice prices = CalculateBundlePrice(items);

            totalNotOwnedItems.InnerText = prices.TotalItemsNotOwned.ToString ();
            totalOwnedItems.InnerText = prices.TotalItemsOwnedPlusCommission.ToString ();
            totalDesignCommission.InnerText = prices.DesignCommissionPlusCommission.ToString ();
            totalBundle.InnerText = prices.TotalBundle.ToString ();
        }

        private BundlePrice CalculateBundlePrice(List<BundleItem> items)
        {
            int fullPrice = 0;
            int basePrice = 0;
            HashSet<int> distinctIds = new HashSet<int>();


            foreach (BundleItem bi in items)
            {
                if (distinctIds.Contains(bi.GlobalId))
                {
                    continue;
                }
                else
                {
                    distinctIds.Add(bi.GlobalId);
                    if (bi.ItemCreatorId == KanevaWebGlobals.CurrentUser.UserId)
                    {
                        basePrice += bi.MarketCost;
                    }
                    else
                    {
                        fullPrice += bi.WebPrice;
                    }
                }
            }

            BundlePrice bp = new BundlePrice();
            bp.TotalItemsNotOwned = fullPrice;
            bp.TotalItemsOwned = basePrice;
            bp.DesignCommission = GetCommissionPrice();
            return bp;
        }

        private int GetCommissionPrice ()
        {
            int ret = 0;
            try
            {
                if (txtPrice.Text.Trim ().Length > 0)
                {
                    ret = Convert.ToInt32 (txtPrice.Text);
                }
            }
            catch { }
            return ret;
        }

        private void IsBundleAP (List<BundleItem> items)
        {
            BundleItem item = items.Find (delegate (BundleItem bi) { return bi.IsAP == true; });
            if (item != null && item.GlobalId > 0)
            {
                divAP.Visible = true;
                BundleIsAP = true;
            }
            else
            {
                divAP.Visible = false;
                BundleIsAP = false;
            }   
        }

        private void IsBundleCreditsOnly (List<BundleItem> items)
        {
            BundleItem item = items.Find (delegate (BundleItem bi) { return bi.CreditsOnly == true; });
            if (item != null && item.GlobalId > 0)
            {
                spnPurchaseType.InnerText = "Credits";
                BundleIsCreditsOnly = true;
            }
            else
            {
                spnPurchaseType.InnerText = "Rewards";
                BundleIsCreditsOnly = false;
            }
        }
        
        public string IsAPItem (object isap)
        {
            try
            {
                return Convert.ToBoolean (isap) ? "X" : "";
            }
            catch { }
            return "";       
        }

        public string GetItemPrice (object webPrice, object basePrice, object itemOwnerId)
        {
            int ownerId = Convert.ToInt32 (itemOwnerId);

            if (KanevaWebGlobals.CurrentUser.UserId == ownerId)
            {
                return basePrice.ToString ();
            }
            else
            {
                return webPrice.ToString();
            }
        }

        #endregion Helper Methods


        #region Event Handlers

        public void btnAddToBundle_Click (object sender, EventArgs e)
        {
            string[] aGIDs = txtGIDs.Text.Split (new char[]{','});
            string errorList = "";
            string notValidGLID = "";
            
            // Clear any previous errors
            divError.InnerHtml = "";
            divError.Visible = false;
            
            if (aGIDs.Length > 0)
            {
                spnPurchaseType.InnerText = "Rewards";
                List<BundleItem> items = BundleItemList;
                for (int i = 0; i < aGIDs.Length; i++)
                {
                    try
                    {
                        int globalId = 0;
                        try
                        {
                            globalId = Convert.ToInt32 (aGIDs[i].Trim ());
                        }
                        catch 
                        {
                            if (notValidGLID.Length > 0) { notValidGLID += ", "; }
                            notValidGLID += aGIDs[i];
                            continue;
                        }


                        WOKItem item = GetShoppingFacade.GetItem (globalId);
                        if (item.GlobalId > 0 &&  // item was found
                            item.ItemActive == (int) WOKItem.ItemActiveStates.Public &&
                            item.UseType != WOKItem.USE_TYPE_BUNDLE &&
                            item.UseType != WOKItem.USE_TYPE_CUSTOM_DEED)
                        {
                            BundleItem bundleItem = new BundleItem (item);

                            // Check if item is Access pass
                            if (bundleItem.PassTypeId.Equals ((int) Constants.ePASS_TYPE.GENERAL) ||
                                  (bundleItem.PassTypeId.Equals ((int) Constants.ePASS_TYPE.ACCESS) &&
                                   KanevaWebGlobals.CurrentUser.HasAccessPass)   // if item is AP user must have AP
                                )
                            {
                                items.Insert (0, bundleItem);
                                if (bundleItem.IsAP)
                                {
                                    divAP.Visible = true;
                                    BundleIsAP = true;
                                }

                                // Check if item is credits only
                                if (new BundleItem (item).CreditsOnly || BundleIsCreditsOnly)
                                {
                                    spnPurchaseType.InnerText = "Credits";
                                    BundleIsCreditsOnly = true;
                                }
                            }
                            else
                            {
                                if (errorList.Length > 0) { errorList += ", "; }
                                errorList += aGIDs[i];
                            }
                        }
                        else
                        {
                            if (errorList.Length > 0) { errorList += ", "; }
                            errorList += aGIDs[i];
                        }
                    }
                    catch
                    {
                        if (errorList.Length > 0) { errorList += ", "; }
                        errorList += aGIDs[i];
                    }
                }

      //          items.Sort (delegate (BundleItem i1, BundleItem i2) { return i1.GlobalId.CompareTo (i2.GlobalId); });

                rptBundleGIDs.DataSource = items;
                rptBundleGIDs.DataBind ();
                BundleItemList = items;

                if (notValidGLID.Length > 0)
                {
                    divError.InnerHtml = "Some GLIDs entered are not valid: " + notValidGLID;
                    divError.Visible = true;
                }
                if (errorList.Length > 0)
                {
                    if (divError.InnerHtml.Length > 0) { divError.InnerHtml += "<br/>"; }
                    divError.InnerHtml += " Some items were not added (not allowed in a bundle): " + errorList;
                    divError.Visible = true;
                }

                SetPriceFields (items);
            }

            txtGIDs.Text = "";
        }

        public void btnDeleteFromBundle_Click (object sender, EventArgs e)
        {
            string[] aGIDs = hidDelIDs.Value.Split (new char[] { ',' });
            if (aGIDs.Length > 0)
            {
                List<BundleItem> items = BundleItemList;
                for (int i = 0; i < aGIDs.Length; i++)
                {
                    items.Remove(items.Find (delegate (BundleItem bi) { return bi.GlobalId == Convert.ToInt32(aGIDs[i]); }));
                }

                items.Sort (delegate (BundleItem i1, BundleItem i2) { return i1.GlobalId.CompareTo (i2.GlobalId); });
                
                rptBundleGIDs.DataSource = items;
                rptBundleGIDs.DataBind ();
                BundleItemList = items;

                // Check the remaining items in list to see if any AP.  
                // Only need to check if AP is already visible
                if (BundleIsAP)
                {
                    IsBundleAP (items);
                }
                // Check the remaining items in list to see if any are credits only.  
                // Only need to check if Credit Only is already set
                if (BundleIsCreditsOnly)
                {
                    IsBundleCreditsOnly (items);
                }

                SetPriceFields (items);
            }
        }

        public void btnAdd_Click (object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
                return;
            }

            ShoppingFacade shoppingFacade = new ShoppingFacade ();
            WOKItem item = new WOKItem ();
            bool isErr = false;

            // Verify more than 1 item has been added to this bundle
            if (BundleItemList.Count < 2)
            {
                cvItems.IsValid = false;
                isErr = true;
            }

            // Get the thumbnail to upload
            HttpPostedFile fThumbnail = inpThumbnail.PostedFile;
            if (fThumbnail == null || fThumbnail.ContentLength.Equals (0))
            {
                rfvThumbnail.IsValid = false;
                isErr = true;
            }

            // Thumbnail must be a JPG, JPEG, GIF, or PNG!!!
            string strExtensionUpperCase = Path.GetExtension (fThumbnail.FileName).ToUpper ();
            if (!strExtensionUpperCase.Equals (".JPG") && !strExtensionUpperCase.Equals (".JPEG") &&
                !strExtensionUpperCase.Equals (".GIF") && !strExtensionUpperCase.Equals (".PNG"))
            {
                revThumbnail.IsValid = false;
                isErr = true;
            }

            // If any of the above validations failed, return to page and display error messages
            if (isErr)
            {
                return;
            }


            item.BaseGlobalId = 0;

            // Get the other params
            item.ItemCreatorId = Convert.ToUInt32 (KanevaWebGlobals.CurrentUser.UserId);
            item.Name = Server.HtmlEncode (txtTitle.Text.Trim ());
            item.DisplayName = Server.HtmlEncode (txtTitle.Text.Trim ());
            item.Description = Server.HtmlEncode (txtDescription.Text.Trim ());
            item.Keywords = Server.HtmlEncode (txtKeywords.Text.Trim ());

            // Set whether bundle is credits only purchase
            item.InventoryType = BundleIsCreditsOnly ? BundleItem.CURRENCY_TYPE_CREDITS : BundleItem.CURRENCY_TYPE_BOTH_R_C;

            // Set whether bundle is AP or not
            item.PassTypeId = BundleIsAP ? (int) Constants.ePASS_TYPE.ACCESS : (int) Constants.ePASS_TYPE.GENERAL;
            item.ItemActive = (int) WOKItem.ItemActiveStates.Public;

            // Get the price of the bundle
            BundlePrice bp = CalculateBundlePrice(BundleItemList);
            // For Bundles Only: The stored market price is the actual web price because we can not
            // calcuate the web price without looping through all the items in the bundle to determine
            // which items already have commission added (items not owned by bundle owner) and which items
            // do not have commission added (items owned by bundle owner).  We store the price this way because
            // the web price is calculated everywhere it is displayed except in search and we do not want to 
            // recalculate the price on every search.  So the web price total is stored for easy retrieval and speed.
            item.MarketCost = bp.TotalBundle;           

            item.UseType = WOKItem.USE_TYPE_BUNDLE;
            item.DerivationLevel = WOKItem.DRL_KANEVAITEM; 

            try
            {
                item.DesignerPrice = Convert.ToUInt32 (txtPrice.Text.Trim ());
            }
            catch (Exception) { }

            if (item.DesignerPrice > 5000000)
            {
                item.DesignerPrice = 5000000;
            }


            // Add it to the db
            int bundleGlobalId = shoppingFacade.AddCustomItem (item);

            // Set the category
            List<ItemCategory> itemCatagories = GetShoppingFacade.GetItemCategoriesByFilter("name='Bundles'");
            if (itemCatagories.Count > 0 && itemCatagories[0].ItemCategoryId > 0)
            {
                shoppingFacade.AddCategoryToItem (bundleGlobalId, itemCatagories[0].ItemCategoryId, 1);
            }

            // Generate the thumbnails
            string thumbnailFilename = "";
            string strPathUserIdGlobalId = Path.Combine (KanevaWebGlobals.CurrentUser.UserId.ToString (), bundleGlobalId.ToString ());
            string strFilePathTexture = Path.Combine (KanevaGlobals.TexturePath, strPathUserIdGlobalId);
            if (fThumbnail != null && fThumbnail.ContentLength > 0)
            {
                try
                {
                    ImageHelper.UploadImageFromUser (ref thumbnailFilename, fThumbnail, strFilePathTexture, KanevaGlobals.MaxUploadedImageSize);
                    string imagePath = fThumbnail.FileName.Remove (0, fThumbnail.FileName.LastIndexOf ("\\") + 1);
                    string origImagePath = strFilePathTexture + Path.DirectorySeparatorChar + thumbnailFilename;

                    ImageHelper.GenerateWOKItemThumbs (thumbnailFilename, origImagePath, KanevaGlobals.TexturePath, KanevaWebGlobals.CurrentUser.UserId, bundleGlobalId);
                }
                catch { }
            }

            BundleItemList.Sort (delegate (BundleItem i1, BundleItem i2) { return i1.GlobalId.CompareTo (i2.GlobalId); });
            int qty = 1;
            int prevId = 0;
            int currCount = 1;
            bool addLastItem = false;
            foreach (BundleItem bi in BundleItemList)
            {
                if (currCount == BundleItemList.Count)
                {
                    addLastItem = true;
                }
                // handle the first item in the list
                if (prevId == 0)
                {
                    prevId = bi.GlobalId;
                    currCount++;
                    continue;
                }

                if (bi.GlobalId == prevId)
                {                                       
                    qty++;
                    addLastItem = false;
                    if (currCount < BundleItemList.Count)
                    {
                        currCount++;
                        continue;
                    }
                }

                // Add item to the bundle
                GetShoppingFacade.AddItemToBundle (bundleGlobalId, prevId, qty);

                qty = 1;
                prevId = bi.GlobalId;
                currCount++;

                if (addLastItem)
                {
                    GetShoppingFacade.AddItemToBundle (bundleGlobalId, prevId, qty);
                }
            }

            // Blast if they wanted to Blast
            if (chkBlastUpload.Checked)
            {
                int currUserId = KanevaWebGlobals.CurrentUser.UserId;

                // getting a new instance of the item so all the fields are updated.
                WOKItem item2 = shoppingFacade.GetItem (item.GlobalId);

                m_logger.Debug ("Blasting Item");
                try
                {
                    // Metrics tracking for accepts
                    UserFacade userFacade = new UserFacade();
                    string requestId = userFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_BUNDLE_UPLOAD, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
                    string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_BLAST, 0);

                    string url = "http://" + KanevaGlobals.ShoppingSiteName + "/ItemDetails.aspx?gId=" + item2.GlobalId + "&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeSentIdBlast;
                    string itemThumbnail = StoreUtility.GetItemImageURL (item2.ThumbnailLargePath, "la");

                    BlastFacade blastFacade = new BlastFacade ();

                    blastFacade.SendShopUploadItemBlast (currUserId, KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.NameNoSpaces,
                        item2.DisplayName, item2.Description, url, itemThumbnail, "", KanevaWebGlobals.CurrentUser.ThumbnailSmallPath);

                }
                catch (Exception ex)
                {
                    m_logger.Error ("Blasting Item upload Failed. Item:" + item2.GlobalId + " UserId: " + currUserId + " " + ex.ToString ());

                }
            }

            Response.Redirect (ResolveUrl ("~/MyInventory.aspx?up=1&c=" + item.Category1.ToString ()));
        }

        /// <summary>
        /// 
        /// </summary>
        protected void btnCancel_Click (object sender, EventArgs e)
        {
            Response.Redirect ("~/MyInventory.aspx");
        }

        protected void txtPrice_TextChanged (object sender, EventArgs e)
        {
            SetPriceFields (this.BundleItemList);
        }

        #endregion Event Handlers


        #region Properties

        protected List<BundleItem> BundleItemList
        {
            get         
            {             
                if (ViewState["BundleItems"] == null)             
                {
                    return new List<BundleItem> ();             
                }
                return (List<BundleItem>) ViewState["BundleItems"];         
            }         
            set { ViewState["BundleItems"] = value; } 
        }

        protected bool BundleIsAP
        {
            get
            {
                if (ViewState["BundleIsAP"] == null)
                {
                    return false;
                }
                return Convert.ToBoolean (ViewState["BundleIsAP"]);
            }
            set { ViewState["BundleIsAP"] = value; }
        }
        protected bool BundleIsCreditsOnly
        {
            get
            {
                if (ViewState["BundleIsCreditsOnly"] == null)
                {
                    return false;
                }
                return Convert.ToBoolean (ViewState["BundleIsCreditsOnly"]);
            }
            set { ViewState["BundleIsCreditsOnly"] = value; }
        }

        #endregion Properties


        #region Declerations

        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
    }
}