<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="true" CodeBehind="Clothing.aspx.cs" Inherits="Kaneva.PresentationLayer.Shopping.Clothing" %>

<asp:Content ID="cntUpload" runat="server" ContentPlaceHolderID="cphBody">

    <script type="text/javascript" src="./jscript/swfobject.js"><!-- // External JS --></script>

	<div id="content" class="browseWidth">
     
        <script type="text/javascript">
					var flashvars = {};
					var params = {};
					params.quality = "high";
					params.wmode = "transparent";
					params.allowscriptaccess="always";
					var attributes = {};
					attributes.id = "flashContent";
					swfobject.embedSWF("http://streaming.kaneva.com/ImageServer/media/shop/clothing/clothing-wrapper.swf", "flash1", "720", "180", "9.0.0", "expressInstall.swf", flashvars, params, attributes);
				</script>
                  <div id="flashContent" class="flashArea3">
                    <div id="flash1"><a href="http://www.adobe.com/go/getflashplayer">
				<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
			</a></div></div>
      
      <ul id="browse">
        <asp:Repeater ID="rptItems" runat="server">
            <ItemTemplate>
                <li style='<%# GetStyleImage (DataBinder.Eval (Container.DataItem, "ThumbnailLargePath").ToString (), "la")%>'><a id="A1" href='<%# GetItemDetailsURL (DataBinder.Eval (Container.DataItem, "GlobalId").ToString ())%>'><%# DataBinder.Eval (Container.DataItem, "Name") %><span class="price"><%# DataBinder.Eval (Container.DataItem, "WebPrice") %></span></a></li>
            </ItemTemplate>
        </asp:Repeater>
      </ul>
		
      <div class="clearit"><!-- Clear the floats --></div>                
      <div class="endRule"><img src="./images/content_sideRule.gif" alt="Verticle Rule" /></div>
      
      <script type="text/javascript">
					var flashvars = {};
					var params = {};
					params.quality = "high";
					params.wmode = "transparent";
					params.allowscriptaccess="always";
					var attributes = {};
					attributes.id = "flashContent";
					swfobject.embedSWF("http://streaming.kaneva.com/ImageServer/media/shop/clothing/clothingFeature-wrapper.swf", "flash2", "719", "315", "9.0.0", "expressInstall.swf", flashvars, params, attributes);
				</script>
                  <div class="featFlshWrapper">
                    <div id="flash2" class="flashArea2"><a href="http://www.adobe.com/go/getflashplayer">
				<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
			</a></div>
            </div>
      
    <div class="endRule"><img src="./images/content_sideRule.gif" alt="Verticle Rule" /></div>
  </div>
  


</asp:Content>