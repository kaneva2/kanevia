///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
    public partial class isBlocked : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "text/plain";
            Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "no-cache");
            Response.Expires = -1;

            bool IsBlocked = false;

            try
            {
                int userId = Convert.ToInt32(Request["uId"].ToString());
                int gameId = Convert.ToInt32(Request["gId"].ToString());
                int communityId = Convert.ToInt32(Request["cId"].ToString());

                if (new UserFacade().IsUserBlocked(userId, gameId, communityId))
                {
                    IsBlocked = true;
                }
            }
            catch (Exception)
            {
                IsBlocked = false;
            }

            Response.Clear();

            if (IsBlocked)
            {
                Response.Write("YES");
            }
            else
            {
                Response.Write("NO");
            }
        }
    }
}