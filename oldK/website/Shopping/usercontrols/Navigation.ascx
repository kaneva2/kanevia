<%@ Control Language="C#" enableviewstate="false" AutoEventWireup="false" CodeBehind="Navigation.ascx.cs" Inherits="Kaneva.PresentationLayer.Shopping.usercontrols.Navigation" %>

<div id="navigation">
    <ul>
    <li class="section_head">AVATAR</li>
    <asp:Repeater ID="rpt_topNav" runat="server">
        <ItemTemplate>
            <asp:placeholder id="phHeading" runat="server"></asp:placeholder>
			<asp:placeholder id="phGamingHeading" runat="server"></asp:placeholder>
			<li><a id="lnk_TopMenuItem" runat="server" />
				<asp:placeholder id="phSubCategories" runat="server"></asp:placeholder>
			</li>
        </ItemTemplate>
    </asp:Repeater>
   </ul> 
</div>

<div id="promotionsNav">
    <h4>Promotions</h4>
    <ul>
        <li id="nav-help"><a id="A3" runat="server" href="~/Catalog.aspx?setcs=ph">Featured Designs</a></li>
        <li id="nav-inventory"><a id="A4" runat="server" href="~/Catalog.aspx?setcs=r">Most Raved Items</a></li>
        <li id="nav-sales"><a runat="server" href="~/Catalog.aspx?setcs=n">Newest Items</a></li>
    </ul>
</div>

<div id="designerNav">
	<ul>
		<li id="nav-store" class="selected"><a id="A1" runat="server" href="~/MySales.aspx">My Store</a></li>
		<li id="Li1"><a id="A2" runat="server" href="~/MyInventory.aspx">My Merchandise</a></li>
		<li id="Li2"><a id="aTransTracker" runat="server">My Purchases</a></li>
	</ul>
</div>

<div class="clearit" id="buyBtn"><a runat="server" id="aBuyCredits">Get Credits</a></div>