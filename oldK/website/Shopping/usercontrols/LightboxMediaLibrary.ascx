<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="LightboxMediaLibrary.ascx.cs" Inherits="Kaneva.PresentationLayer.Shopping.usercontrols.LightboxMediaLibrary" %>

<script type="text/javascript"><!--

function setSelectedImage(imageId) 
{
    // assigns the selected value to the holding field
    jQuery('<%=ihSinglePictureId.ClientID %>').value = imageId;
}

//--></script>
<div id="lightboxContent" style="display:none;">
    <asp:UpdatePanel ID="udpLibraryPhotos" runat="server" ChildrenAsTriggers="true">
        <ContentTemplate>
            <div><asp:DropdownList ID="ddlGroup" runat="server" style="width:200px" class="Filter2" autopostback="true" /></div>
            <div id="libraryContainer">
                <asp:DataList Visible="true" RepeatLayout="Table" runat="server" ShowFooter="False" Width="100%" id="dlPictures" cellpadding="2" cellspacing="2" RepeatColumns="5" RepeatDirection="Horizontal" >
	                <ItemStyle CssClass="lineItemThumb" HorizontalAlign="Center"/>
	                <ItemTemplate>
	                    <div id="objectContainer">
		                    <div id="radioContainer" >
			                    <input id="rbPicture" style="vertical-align:middle" name="rbPicture" type="radio" value='<%# DataBinder.Eval( Container.DataItem, "asset_id") %>' onclick='setSelectedImage(this.value)' />
		                    </div>
		                    <div id="imageNameContainer">
		                        <img id="Img1" runat="server" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")))%>' /><br />
		                        <span class="insideTextNoBold" id="spnImageTitle" runat="server"></span>
		                    </div>
		                </div>
	                </ItemTemplate>
                </asp:DataList>	
            </div>
            <div>
                <asp:LinkButton ID="lbtnNext" runat="server">Next</asp:LinkButton>
                <asp:LinkButton ID="btnCancel" Runat="server" Text=" Cancel " CausesValidation="false" CssClass="btnsmall grey btnphotolibselect"></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton ID="btnSave" Runat="server" Text=" Save Changes " CausesValidation="false" CssClass="btnsmall grey btnphotolibselect"></asp:LinkButton>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<input type="hidden" name="ihSinglePictureId" id="ihSinglePictureId" runat="server" />