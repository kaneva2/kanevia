///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace Kaneva.PresentationLayer.Shopping.usercontrols
{
    public partial class NavSubCategories : System.Web.UI.UserControl
    {
        #region Declerations

        private UInt32 _CategoryId = 0;
        private bool navAnimationClicked = false;
        ShoppingFacade shoppingFacade = new ShoppingFacade ();

        #endregion

        protected void Page_Load (object sender, EventArgs e)
        {
            // Get the parameters
            GetRequestParams ();

            // Get the Categories
            GetSubCategories ();
        }

        private void GetSubCategories ()
        {
            if (this.CategoryId > 0)
            {
                ItemCategory itemCategory = shoppingFacade.GetItemCategory (this.CategoryId);
                List<ItemCategory> itemCategories = WebCache.GetCachedCategories (this.CategoryId);

                // See if we are at the bottom node
                if (itemCategories.Count.Equals (0))
                {
                    UInt32 parentCatId = itemCategory.ParentCategoryId;
                    if (parentCatId == 0)
                    {
                        subNav.Visible = false;
                    }
                    else
                    {
                        // At bottom, stay one level up
                        itemCategory = shoppingFacade.GetItemCategory (parentCatId);
                        itemCategories = WebCache.GetCachedCategories (itemCategory.ItemCategoryId);
                        SetDisplay (itemCategory, itemCategories);
                    }
                }
                else
                {
                    SetDisplay (itemCategory, itemCategories);
                }
            }
        }

        private void GetRequestParams ()
        {
            if (Request["anim"] != null)
            {
                try
                {
                    navAnimationClicked = (Convert.ToUInt32 (Request["anim"])).Equals (1);
                }
                catch (Exception) { };
            }
        }

        protected void SetDisplay (ItemCategory itemCategory, List<ItemCategory> itemCategories)
        {
            if (itemCategory.Description.ToLower().Contains ("for men") || itemCategory.Description.ToLower().Contains ("for women"))
            {
                lblHeader.Text = "<h4>" + itemCategory.Description + "</h4>";
            }
            else
            {
                lblHeader.Text = "";
                lblHeader.Visible = false;
            }
            rptSubCategories.DataSource = itemCategories;
            rptSubCategories.DataBind ();
        }

        protected string GetCatalogURL (int itemCatalogId)
        {
            return ResolveUrl ("~/Catalog.aspx?cId=" + itemCatalogId.ToString ());
        }

        protected string GetCatalogLandingURL(int itemCatalogId)
        {
            return ResolveUrl("~/CatalogLanding.aspx?cId=" + itemCatalogId.ToString());
        }

        protected string GetSelectedClass (UInt32 catId)
        {
            if (CategoryId.Equals (catId))
            {
                return "selected";
            }

            return "";
        }

        protected void rptSubCategories_ItemDataBound (object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //find all the controls in the repeater
                HtmlAnchor lnkSubMenuItem = (HtmlAnchor) e.Item.FindControl ("lnk_SubMenuItem");
                uint catId = 0;
                bool isAnimation = false;

                if (((ItemCategory) e.Item.DataItem).Name.Equals ("Animated"))
                {
                    catId = Convert.ToUInt32 (((ItemCategory) e.Item.DataItem).ParentCategoryId);
                    isAnimation = true;

                    if (navAnimationClicked)
                        lnkSubMenuItem.Attributes.Add ("class", "selected");
                    else
                        lnkSubMenuItem.Attributes.Add ("class", "");
                }
                else
                {
                    //get the category id
                    catId = Convert.ToUInt32 (((ItemCategory) e.Item.DataItem).ItemCategoryId);

                    //set the class on the select menu item
                    lnkSubMenuItem.Attributes.Add ("class", GetSelectedClass (catId));
                }

                //set the href navigation
                if (!string.IsNullOrWhiteSpace(((ItemCategory)e.Item.DataItem).MarketingPath))
                {
                    lnkSubMenuItem.HRef = GetCatalogLandingURL((int)catId) + (isAnimation ? "&anim=1" : "");
                }
                else
                {
                    lnkSubMenuItem.HRef = GetCatalogURL((int)catId) + (isAnimation ? "&anim=1" : "");
                }

                //set the text on the top navigation
                lnkSubMenuItem.InnerText = Server.HtmlDecode (((ItemCategory) e.Item.DataItem).Description);

            }
        }

        public UInt32 CategoryId
        {
            get { return _CategoryId; }
            set { _CategoryId = value; }
        }

    }
}