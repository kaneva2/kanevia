///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kaneva.PresentationLayer.Shopping.usercontrols
{
    public partial class TitleSearchBar : System.Web.UI.UserControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {

            lblSearchTitle.Text = SearchTitle;
            txtSearch.Attributes.Add ("onkeydown", "javascript:checkEnter(event)");

            if (!IsPostBack)
            {
                txtSearch.Text = SearchBoxText;
            }

            string strJavascript = "<script language=\"JavaScript\">" +
                "function checkEnter(event){\n" +
                "if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13))\n" +
                "{event.returnValue=false;event.cancel=true;" + Page.ClientScript.GetPostBackEventReference (this.btnSearch, "", false) + ";return false;}\n else \n{return true;}" +
                "}</script>";

            if (!Page.ClientScript.IsClientScriptBlockRegistered (GetType (), "checkEnter"))
            {
                Page.ClientScript.RegisterClientScriptBlock (GetType (), "checkEnter", strJavascript);
            }

            if (TitleClass.Length > 0)
            {
                litTitleClass.Text = "class=\"" + TitleClass + "\"";
            }
        }


        /// <summary>
        /// btnSearch_OnClick
        /// </summary>
        protected void btnSearch_OnClick (object sender, EventArgs e)
        {
            int categoryId = 0;

            if (Request["cId"] != null)
            {
                try
                {
                    categoryId = Convert.ToInt32 (Request["cId"]);
                }
                catch (Exception) { };
            }

            // Only if the search changed
            string searchString = txtSearch.Text.Trim ();

            if (txtSearch.Text.Equals (SearchBoxText))
            {
                searchString = "";
            }

            Response.Redirect ("~/Catalog.aspx?s=" + searchString + "&cId=" + categoryId);
        }


        public string SearchBoxText
        {
            set
            {
                ViewState[this.ClientID + "sbt"] = value;
            }
            get
            {
                if (ViewState[this.ClientID + "sbt"] != null)
                {
                    return ViewState[this.ClientID + "sbt"].ToString ();
                }
                else
                {
                    return "<All>";
                }
            }
        }

        public string SearchTitle
        {
            set
            {
                ViewState[this.ClientID + "st"] = value;
            }
            get
            {
                if (ViewState[this.ClientID + "st"] != null)
                {
                    return ViewState[this.ClientID + "st"].ToString ();
                }
                else
                {
                    return "Shop Kaneva";
                }
            }
        }

        public string TitleClass
        {
            set
            {
                _TitleClass = value;
            }
            get
            {
                return _TitleClass;
            }
        }

        protected string _TitleClass = "";
    
    }
}