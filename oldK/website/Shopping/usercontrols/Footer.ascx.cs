///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping.usercontrols
{
    public class Footer : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                aAbout.HRef = "http://" + KanevaGlobals.SiteName + "/overview/aboutLanding.aspx";
                aNews.HRef = "http://" + KanevaGlobals.SiteName + "/news";
                aSupport.HRef = Common.GetHelpURL(KanevaWebGlobals.CurrentUser.FirstName, KanevaWebGlobals.CurrentUser.LastName, KanevaWebGlobals.CurrentUser.Email);
                aCareers.HRef = "http://" + KanevaGlobals.SiteName + "/community/Careers.kaneva";
                aPolicies.HRef = "http://" + KanevaGlobals.SiteName + "/overview/guidelines.aspx";
                aSafty.HRef = "http://" + KanevaGlobals.SiteName + "/community/safety.kaneva";
                aContactUs.HRef = "http://" + KanevaGlobals.SiteName + "/overview/contactUs.aspx";
                aDesigner.HRef = KanevaGlobals.DesignerSiteLink;
            }
        }

        protected HtmlAnchor aAbout, aNews, aSupport, aCareers, aPolicies, aSafty, aContactUs, aDesigner;
    }
}