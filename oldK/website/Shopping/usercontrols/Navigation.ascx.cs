///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;


namespace Kaneva.PresentationLayer.Shopping.usercontrols
{
    public class Navigation : System.Web.UI.UserControl
    {
        #region Declarations
        private UInt32 categoryId = 0;
        private int topLevelParentId = -1;
        private bool navAnimationClicked = false;
        ShoppingFacade shoppingFacade = new ShoppingFacade ();

        protected Repeater rpt_topNav;
        protected HtmlAnchor aBuyCredits, lnk_ShopHome;
        protected PlaceHolder phSubCategories, phHeading;
        protected HtmlGenericControl navHome, navfloorplans, navfurnishings, navclothing, navbuilding, navcollectable, navsounds, navanimations;
        protected HtmlAnchor aTransTracker;
        private int count = 0;
        private bool gameHeadingWasAdded = false;
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Page Load
        protected void Page_Load (object sender, EventArgs e)
        {
            //get the parameters
            GetRequestParams ();

            //load in the top navigation
            GetTopLevelCategories ();

            //set generic non dynamic links and settings
            aBuyCredits.HRef = "http://" + KanevaGlobals.SiteName + "/mykaneva/buyCredits.aspx";
            aTransTracker.HRef = "http://" + KanevaGlobals.SiteName + "/myKaneva/transactions.aspx?CP=Y";
        }
        #endregion

        #region Helper Functions

        private void GetRequestParams ()
        {
            if (Request["cId"] != null)
            {
                try                                                 
                {   // This is so we can get the multi-id selections in Gaming section
                    string[] ids = Request["cId"].ToString ().Split ('|');
                    CategoryId = Convert.ToUInt32 (ids[0]);
                }
                catch (Exception) { };
            }
            if (Request["anim"] != null)
            {
                try
                {
                    navAnimationClicked = (Convert.ToUInt32 (Request["anim"])).Equals (1);
                }
                catch (Exception) { };
            }

        }

        private void GetTopLevelCategories ()
        {
            List<ItemCategory> parentCategories = WebCache.GetTopLevelCategories ();
            rpt_topNav.DataSource = parentCategories;

            if (KanevaWebGlobals.CurrentUser.UserId > 0)
            {
                CommunityFacade commFacade = new CommunityFacade();
                int[] type_ids = { 2, 5 };

                PagedList<Community> communities = commFacade.GetUserCommunities(KanevaWebGlobals.CurrentUser.UserId, type_ids, "", "c.status_id=1 AND c.is_public='Y'", 1, 10);

                if (communities.Count == 1)
                {
                    // If the user only has 1 world, check to see if this is the home world with the child world template
                    // Added 10/11/16 - Also checking for beach house without building enabled
                    var templateId = commFacade.GetWorldTemplateId(communities[0].CommunityId);
                    if (templateId.Equals(30) || templateId.Equals(31))
                    {
                        // Only show the avatar parent categories for this user
                        rpt_topNav.DataSource = parentCategories.GetRange(0, 4);
                    }
                }
            }

            rpt_topNav.DataBind ();
        }

        protected string GetCatalogLandingURL (string itemCatalogId)
        {
            return ResolveUrl ("~/CatalogLanding.aspx?cId=" + itemCatalogId);
        }

        protected string GetCatalogURL (int itemCatalogId)
        {
            return ResolveUrl ("~/Catalog.aspx?cId=" + itemCatalogId.ToString ());
        }

        protected string GetSelectedClass (UInt32 catId)
        {
            if (CategoryId.Equals (catId))
            {
                return "selected";
            }

            return "";
        }

        private UInt32 GetTopLevelParentId (UInt32 catId)
        {
            if (TopLevelParentId < 0)
            {
                int i = 0;

                while (i < 6)
                {
                    ItemCategory itemCat = shoppingFacade.GetItemCategory (catId);
                    if (itemCat.ParentCategoryId == 0)
                    {
                        TopLevelParentId = (int)itemCat.ItemCategoryId;
                        return itemCat.ItemCategoryId;
                    }
                    else
                    {
                        catId = itemCat.ParentCategoryId;
                    }
                    i++;
                }

                TopLevelParentId = 0;
                return 0;
            }

            return (UInt32)TopLevelParentId;
        }

        #endregion

        #region Event Handlers
        private void rpt_topNav_ItemDataBound (object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                count++;

                // Add Building Category
                if (count == 5)
                {
                    // Insert the BUILDING menu heading
                    HtmlGenericControl liHeading = new HtmlGenericControl ("li");
                    liHeading.InnerText = "BUILDING";
                    liHeading.Attributes.Add ("class", "section_head midlist");
                    PlaceHolder phHeading = (PlaceHolder) e.Item.FindControl ("phHeading");
                    phHeading.Controls.Add (liHeading);
                }

                // Add Gaming Category
                if (((ItemCategory) e.Item.DataItem).ParentCategoryId == 9999 && !gameHeadingWasAdded)
                {
                    // Insert the BUILDING menu heading
                    HtmlGenericControl liHeading = new HtmlGenericControl ("li");
                    liHeading.InnerText = "GAMING";
                    liHeading.Attributes.Add ("class", "section_head midlist");
                    PlaceHolder phGamingHeading = (PlaceHolder) e.Item.FindControl ("phGamingHeading");
                    phGamingHeading.Controls.Add (liHeading);
                    gameHeadingWasAdded = true;
                }

                //find all the controls in the repeater
                HtmlAnchor lnkMenuItem = (HtmlAnchor) e.Item.FindControl ("lnk_TopMenuItem");

                if (((ItemCategory) e.Item.DataItem).ParentCategoryId == 9999 && 
                    (((ItemCategory) e.Item.DataItem).ItemCategoryId == (int)ItemCategory.GameCategories.Pets || 
                    ((ItemCategory) e.Item.DataItem).ItemCategoryId == (int)ItemCategory.GameCategories.Energy ||
                    ((ItemCategory) e.Item.DataItem).ItemCategoryId == (int)ItemCategory.GameCategories.NPC))
                {
                    lnkMenuItem.Style.Add ("display", "none");
                    return;
                }

                //get the category id
                uint catId = Convert.ToUInt32 (((ItemCategory) e.Item.DataItem).ItemCategoryId);

                if (catId == GetTopLevelParentId(CategoryId))
                {
                    PlaceHolder phSubCats = (PlaceHolder) e.Item.FindControl ("phSubCategories");
                    if (phSubCats != null)
                    {
                        NavSubCategories ucSubNav = (NavSubCategories)LoadControl ("~/usercontrols/navsubcategories.ascx");
                        ucSubNav.CategoryId = CategoryId;
                        phSubCats.Controls.Add (ucSubNav);
                    }
                }

                //set the text on the top navigation
                lnkMenuItem.InnerText = Server.HtmlDecode (((ItemCategory) e.Item.DataItem).Description);
                
                string secondCatId = "";
                if (((ItemCategory) e.Item.DataItem).ParentCategoryId == 9999)
                {
                    string catName = "";
                    if (((ItemCategory) e.Item.DataItem).ItemCategoryId == (int) ItemCategory.GameCategories.Animals)
                    {
                        catName = "Animals & Pets";
                        secondCatId = "|" + (int) ItemCategory.GameCategories.Pets;
                    }
                    else if (((ItemCategory) e.Item.DataItem).ItemCategoryId == (int) ItemCategory.GameCategories.Consumables)
                    {
                        catName = "Consumables & Energy";
                        secondCatId = "|" + (int) ItemCategory.GameCategories.Energy;
                    }
                    else if (((ItemCategory) e.Item.DataItem).ItemCategoryId == (int) ItemCategory.GameCategories.Vendors)
                    {
                        catName = "Vendors & NPCs";
                        secondCatId = "|" + (int) ItemCategory.GameCategories.NPC;
                    }

                    if (catName != "")
                    {
                        lnkMenuItem.InnerText = catName;
                    }
                }

                //set the href navigation
                lnkMenuItem.HRef = GetCatalogLandingURL (catId + secondCatId);

                // Added this to handle the hiliting of the Animated sub-menu items since it
                // is handled a little differently than the other categories
                if (navAnimationClicked)
                {
                    lnkMenuItem.Attributes.Add ("class", "");
                }
                else
                {
                    lnkMenuItem.Attributes.Add ("class", GetSelectedClass (catId));
                }
            }
        }

        #endregion

        #region Properties

        private uint CategoryId
        {
            get
            {
                if (categoryId == 0)
                {
                    if (Request["cId"] != null)
                    {
                        try
                        {
                            categoryId = Convert.ToUInt32 (Request["cId"]);
                        }
                        catch { }
                    }
                }

                return categoryId;
            }

            set { categoryId = value; }
        }

        private int TopLevelParentId
        {
            get { return topLevelParentId; }
            set { topLevelParentId = value; }
        }

        #endregion
        #region Web Form Designer generated code
        override protected void OnInit (EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent ();
            base.OnInit (e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.Load += new System.EventHandler (this.Page_Load);
            this.rpt_topNav.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler (this.rpt_topNav_ItemDataBound);
        }
        #endregion
    }
}
