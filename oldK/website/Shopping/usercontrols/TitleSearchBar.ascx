﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TitleSearchBar.ascx.cs" Inherits="Kaneva.PresentationLayer.Shopping.usercontrols.TitleSearchBar" %>

<div id="titleBar" <asp:Literal runat="server" ID="litTitleClass"></asp:Literal>>
    <div id="headline">
    	<h1 class="title"><asp:Label id="lblSearchTitle" runat="server" Text="Shop Kaneva"/></h1>
    </div><!-- << This has moved here from below -->
	<div id="searchArea"><label for="search">Search</label> <asp:textbox id="txtSearch" MaxLength="100" Width="150px" CssClass="inputText" style="font-style: italic" onfocus="clearDefaultandCSS(this)" runat="server"/><asp:ImageButton runat="server" ID="btnSearch" OnClick="btnSearch_OnClick" ImageUrl="~/images/btn_search.gif" CssClass="searchBTN"/>
	</div>				
</div>
