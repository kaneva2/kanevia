///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using KlausEnt.KEP.Kaneva;


namespace Kaneva.PresentationLayer.Shopping.usercontrols
{
    public partial class LightboxMediaLibrary : System.Web.UI.UserControl
    {
        #region Declarations
        int assetId = 0;
        TextBox returnField = null;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData(KanevaWebGlobals.GetUserId(), KanevaWebGlobals.CurrentUser.CommunityId);
            }
        }

        /// <summary>
        /// BindData
        /// </summary>
        private void BindData(int userId, int channelId)
        {
            // Groups
            ddlGroup.Items.Add(new ListItem("All", "-1"));

            if (userId != -1)
            {
                MediaFacade mediaFacade = new MediaFacade();
                PagedList<AssetGroup> pdtAssetGroups = mediaFacade.GetAssetGroups(channelId, "", "", 1, Int32.MaxValue);
                if (pdtAssetGroups.Count > 0)
                {
                    for (int i = 0; i < pdtAssetGroups.Count; i++)
                    {
                        ddlGroup.Items.Add(new ListItem(pdtAssetGroups[i].Name.ToString(), pdtAssetGroups[i].AssetGroupId.ToString()));
                    }
                }
            }

            int group_id = Convert.ToInt32(ddlGroup.SelectedValue);
            BindPictures(group_id, userId, channelId);
        }

        private void BindPictures(int group_id, int user_id, int channelId)
        {
            PagedDataTable pds;

            // if the group id is valid, get pictures for that group
            if (group_id != -1)
            {
                string orderby = "asset_id" + " " + "ASC";

                pds = StoreUtility.GetAssetsInGroup(group_id, channelId, (int)Constants.eASSET_TYPE.PICTURE, orderby, "", 1, Int32.MaxValue);
            }
            // otherwise get all pics
            else
            {
                string orderby = "created_date" + " " + "ASC";
                string filter = "a.status_id <> " + (int)Constants.eASSET_STATUS.MARKED_FOR_DELETION;
                int assetTypeId = (int)Constants.eASSET_TYPE.PICTURE;

                pds = StoreUtility.GetAssetsInChannel(channelId, true, true, assetTypeId, filter, orderby, 1, Int32.MaxValue);
            }

            dlPictures.DataSource = pds;
            dlPictures.DataBind();
        }

        /// <summary>
        /// dlPictures_ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dlPictures_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;
                HtmlGenericControl spnImageTitle = (HtmlGenericControl)e.Item.FindControl("spnImageTitle");

                if (spnImageTitle != null)
                {
                    spnImageTitle.InnerText = KanevaGlobals.TruncateWithEllipsis(drv["name"].ToString(), 13);
                }
            }
        }

        /// <summary>
        /// ddlGroup_SelectedIndexChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            int group_id = Convert.ToInt32(ddlGroup.SelectedValue);

            BindPictures(group_id, KanevaWebGlobals.GetUserId(), KanevaWebGlobals.CurrentUser.CommunityId);
        }

        /// <summary>
        /// btnSave_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedAsset = Convert.ToInt32(ihSinglePictureId.Value);
                DataRow drAsset = StoreUtility.GetAsset(SelectedAsset);
                ReturnField.Text = StoreUtility.GetPhotoImageURL(drAsset["image_full_path"].ToString(), "la");
            }
            catch (Exception)
            {}
            this.Visible = false;
        }

        /// <summary>
        /// btnCancel_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        /// <summary>
        /// btnCancel_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selectedImage_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        #region Accessory Methods

        public int SelectedAsset
        {
            get
            {
                return assetId;
            }
            set
            {
                assetId = value;
            }
        }

        public TextBox ReturnField
        {
            get
            {
                return returnField;
            }
            set
            {
                returnField = value;
            }
        }

        public UpdatePanelUpdateMode UpdateMode
        {
            get { return this.udpLibraryPhotos.UpdateMode; }
            set { this.udpLibraryPhotos.UpdateMode = value; }
        }

        public void Update()
        {
            this.udpLibraryPhotos.Update();
        }

        protected string GetMediaImageURL(string imagePath, string size, int assetId, int assetTypeId)
        {
            return StoreUtility.GetMediaImageURL(imagePath, size, assetId, assetTypeId, 0, Page);
        }

        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.dlPictures.ItemDataBound += new DataListItemEventHandler(dlPictures_ItemDataBound);
            this.ddlGroup.SelectedIndexChanged += new EventHandler(ddlGroup_SelectedIndexChanged);
            this.btnSave.Click += new EventHandler(this.btnSave_Click);
            this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
        }
        #endregion

    }
}