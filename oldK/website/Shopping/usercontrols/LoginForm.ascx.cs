///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;

using log4net;


namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class LoginForm : System.Web.UI.UserControl
    {
        private void Page_Load (object sender, System.EventArgs e)
        {
            // Set the postback url for the signup form
            String strTestDNS;
            strTestDNS = Request.Url.Host;
            strTestDNS = strTestDNS.ToLower ();
            strTestDNS = strTestDNS.Replace ("3d-developer.kaneva.com", "3dapps.kaneva.com");
            strTestDNS = strTestDNS.Replace ("pv-developer.kaneva.com", "preview.kaneva.com");
            strTestDNS = strTestDNS.Replace ("developer.kaneva.com", "www.kaneva.com");
            strTestDNS = strTestDNS.Replace ("localhost", "localhost/kaneva");

            string signInUrl = (Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["SSLLogin"]) ? "https://" : "http://") + strTestDNS + "/loginsecure.aspx";
            lbLogin.PostBackUrl = signInUrl;




            if (!IsPostBack)
            {
                aLostPassword.HRef = KanevaGlobals.SiteName + "/lostPassword.aspx";

                // Set the focus
                Page.SetFocus (txtUserName);
            }

            if (!ShowFacebookLogin)
            {
                fbControls.Visible = false;
            }
        }

        #region Properties

        public string Username
        {
            get
            {
                return txtUserName.Value;
            }
        }
        public string PassWord
        {
            get
            {
                return txtPassword.Value;
            }
        }
        public bool RememberMe
        {
            get
            {
                return chkRememberLogin.Checked;
            }
        }
        public bool ShowFacebookLogin
        {
            set { _showFB = value; }
            get { return _showFB; }
        }

        #endregion

        #region Declarations

        protected HtmlInputText txtUserName;
        protected HtmlInputText txtPassword;
        protected LinkButton lbLogin;
        protected CheckBox chkRememberLogin;
        protected HtmlAnchor aLostPassword;

        private bool _showFB = true;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit (EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent ();
            base.OnInit (e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.Load += new System.EventHandler (this.Page_Load);
        }
        #endregion

    }
}