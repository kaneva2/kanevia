<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="Kaneva.PresentationLayer.Shopping.usercontrols.Header" %>
<%@ Register TagPrefix="Kaneva" TagName="LoginForm" Src="~/usercontrols/LoginForm.ascx" %>

<asp:literal id="litCSS" runat="server"></asp:literal>
<asp:literal id="litJS" runat="server"></asp:literal>
<asp:literal id="Literal1" runat="server"></asp:literal>

<span id="spnAuthenticated" runat="server" visible="false">
<script type="text/javascript">
	try {
		function GetFortune() { __doPostBack("GetUserFortune", ""); }
	} catch (err) { }

	var $j = jQuery.noConflict();
	function ToggleDD(menu, btn) {
		menu.toggle();
		btn.toggleClass("selected", menu.is(":visible"));
	}

	$j(document).ready(function () {
		$j("html").click(function () {
			if ($j("#profiledd").is(":visible")) {
				$j("#profiledd").toggle();
				$j("#profile_selector").toggleClass("selected");
			}
			if ($j("#fortunedd").is(":visible")) {
				$j("#fortunedd").toggle();
				$j("#fortune_selector").toggleClass("selected");
			}
		});

		$j("#profiledd").click(function (e) {
			e.stopPropagation();
		});
		$j("#profile_selector").click(function (e) {
			e.stopPropagation();
			ToggleDD($j('#profiledd'), $j('#profile_selector'));
		});

		$j("#fortune_selector").click(function (e) {
			e.stopPropagation();
			ToggleDD($j('#fortunedd'), $j('#fortune_selector'));
		});
	});
</script>
<div class="nav_wrapper">
	<ul class="menu_wrapper">
		<li class="topmenu">
			<ul>
				<li class="logo"><a id="aLogo" href="" runat="server"></a></li>
				<li class="fortune"><div><a href="#" id="fortune_selector"></a></div>
					<div id="fortunedd" class="fortunedd_container">
						<ul>
							<li class="fortune_header">Your Balance</li>
							<li class="credits">Credits: <span id="spnCredits" runat="server">0</span> <a id="aBuyCredits" href="" runat="server"></a></li>
							<li class="horiz_divide"></li>
							<li class="rewards">Rewards: <span id="spnRewards" runat="server">0</span></li>
						</ul>
					</div>	
				</li>
				<li class="inbox"><a id="aMailbox" href="" runat="server"><div class="newmsg" id="divMsgCount" runat="server">0</div></a></li>
				<li class="world_inbox"><a id="aMsgCenter" href="" runat="server"><div class="newmsg" id="divWorldReqCount" runat="server">0</div></a></li>
			</ul>
		</li>
		<li class="botmenu">
			<ul class="menu_text">
				<li class="explore"><a href="" runat="server" id="aFind3DApps">Explore Worlds</a></li>
				<li class="shop"><a href="/" runat="server" id="aShop">Shop Now</a></li>
				<li class="create"><a href="" runat="server" id="aMake3DApp">Create Your World</a></li>
				<li class="profile"><a href="javascript:void(0);" id="profile_selector" class="unselected"><img id="Img3" runat="server" src="~/images/header/down_arrow.gif" /></a>
						
				</li>
				<li class="home"><a href="" runat="server" id="aHome">Home</a></li>
				<li class="divide">
					<ul id="profiledd">
						<li><a href="" runat="server" id="aMyProfile2">My Profile</a></li>
						<li><a href="" runat="server" id="aMessages">Messages</a></li>
						<li class="horiz_divide"></li>
						<li><a href="" runat="server" id="aMyWorlds">My Worlds</a></li>
						<li><a href="" runat="server" id="aExploreWorlds">Explore Worlds</a></li>
						<li><a href="" runat="server" id="aCreateWorld">Create World</a></li>
						<li class="horiz_divide"></li>
						<li><a href="" runat="server" id="aFindFriends">Find/Invite Friends</a></li>
						<li><a href="" runat="server" id="aSearchPeople">Search People</a></li>
						<li class="horiz_divide"></li>
						<li><a href="/" id="aShopDD" runat="server">Shopping</a></li>
						<li><a href="" runat="server" id="aGetCredits">Get Credits</a></li>
						<li><a href="" runat="server" id="aContests">Contests</a></li>
						<li class="horiz_divide"></li>
						<li><a href="" runat="server" id="aAcctSettings">Account Settings</a></li>
						<li class="horiz_divide"></li>
						<li><asp:linkbutton id="lnkLogout" onclick="lnkLogout_Click" runat="server" text="Log Out" causesvalidation="false"></asp:linkbutton></li>
					</ul>
				</li>
				<li class="username"><a href="" runat="server" id="aMyProfile">Hi, <span id="spnUsername" runat="server"></span></a></li>
			</ul>
		</li>
		<li class="subnav"><asp:placeholder runat="server" id="phNavLvl3" /></li>
		<li class="validate" id="validateContainer" runat="server"><div class="top"><a href="" id="aValidate" runat="server"></a></div><div class="shadow"></div></li>
	</ul>
</div>
<div id="divFlashSSO" runat="server" visible="false">
	<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="kaneva_sinfo" align="middle">
		<param name="movie" value="<%:FlashSSOPath %>" />
		<param name="quality" value="high" />
		<param name="bgcolor" value="#ffffff" />
		<param name="play" value="true" />
		<param name="loop" value="false" />
		<param name="wmode" value="transparent" />
		<param name="scale" value="showall" />
		<param name="menu" value="true" />
		<param name="devicefont" value="false" />
		<param name="salign" value="" />
		<param name="allowScriptAccess" value="sameDomain" />
        <asp:Literal ID="litFlashSSOVars" runat="server" />
		<!--[if !IE]>-->
		<object type="application/x-shockwave-flash" data="<%:FlashSSOPath %>" width="1" height="1">
			<param name="movie" value="<%:FlashSSOPath %>" />
			<param name="quality" value="high" />
			<param name="bgcolor" value="#ffffff" />
			<param name="play" value="true" />
			<param name="loop" value="false" />
			<param name="wmode" value="transparent" />
			<param name="scale" value="showall" />
			<param name="menu" value="true" />
			<param name="devicefont" value="false" />
			<param name="salign" value="" />
			<param name="allowScriptAccess" value="sameDomain" />
            <asp:Literal ID="litFlashSSOVarsIE" runat="server" />
		</object>
		<!--<![endif]-->
	</object>
</div>
</span>

<span id="spnNotAuthenticated" runat="server" visible="false">
	<div class="nav_wrapper_noauth">
		<div class="pageHeader">
			<div class="logo"><a href="~/default.aspx" runat="server" id="aLogoLink"></a></div>  
			<div class="headerLinks">
				<a id="aJoin" runat="server" href="">Join Today</a>&nbsp;|&nbsp;<a id="aSignIn" runat="server" href="">Sign In</a>
			</div>
			<div id="fb-root"></div>
		</div>
	</div>
</span>

<style type="text/css"><asp:literal id="litStyle" runat="server"></asp:literal></style>

