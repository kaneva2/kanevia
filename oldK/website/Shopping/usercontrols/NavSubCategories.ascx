﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavSubCategories.ascx.cs" Inherits="Kaneva.PresentationLayer.Shopping.usercontrols.NavSubCategories" %>

<div id="subNav" runat="server" clientidmode="Static">
    <asp:Label runat="server" ID="lblHeader"></asp:Label>
    <asp:Repeater ID="rptSubCategories" runat="server" onitemdatabound="rptSubCategories_ItemDataBound">
        <HeaderTemplate>
            <ul>
        </HeaderTemplate>
        <ItemTemplate>
            <li><a id="lnk_SubMenuItem" runat="server" /></li>
        </ItemTemplate>
        <FooterTemplate>
           </ul> 
        </FooterTemplate>
    </asp:Repeater>
</div>
