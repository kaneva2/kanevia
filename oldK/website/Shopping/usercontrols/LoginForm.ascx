<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="LoginForm.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.LoginForm" %>
    <asp:Panel id="pnlLoginForm" DefaultButton="lbLogin" CssClass="signIn" runat="server">
          <ul>
			<span id="fbControls" runat="server">
				<li class="fbButton"><a title="Sign In with Facebook" onclick="FBLogin(FBAction.LOGIN);"></a></li>
				<li class="divider"><img src='<%=ResolveUrl("~/images/header/graphic_4x72_VerticalHeaderBreak.png") %>' width="4" height="72" /></li>
			</span>
            <li class="inputField">
                <label for='<%=txtUserName.ClientID %>'>Email</label>
			    <input type="text" style="width:160px;" class="start" maxlength="100" id="txtUserName" runat="server" value="" TabIndex="1"  >                                
                <br />
                <asp:checkbox runat="server" id="chkRememberLogin" tooltip="Remember Password" TabIndex="3" CssClass="chkRemember" Checked="True" />
                <label for='<%=chkRememberLogin.ClientID %>' class="keepSignedIn">Keep me signed in</label>
            </li>
            <li class="inputField">   
                <label for='<%=txtPassword.ClientID %>'>Password</label>
                <input type="password" style="width: 160px;" class="start" maxlength="30" id="txtPassword" runat="server" TabIndex="2">                
                <br />
                <a runat="server" id="aLostPassword" class="forgot">Forget your password?</a>                
            </li>
            <li class="button">               
                <asp:LinkButton id="lbLogin" runat="server" causesvalidation="False"  TabIndex="4"/>
                <div style="width:0px;height:0px; overflow:hidden;"><asp:Button ID="loginSubmit" runat="server" TabIndex="5" /></div>
            </li>
		  </ul>
          <input type="hidden" id="loginForm_txtUserNameUID" name="loginForm_txtUserNameUID" value="<%=txtUserName.UniqueID %>" />
          <input type="hidden" id="loginForm_txtPasswordUID" name="loginForm_txtPasswordUID" value="<%=txtPassword.UniqueID %>" />
    </asp:Panel>