///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using KlausEnt.KEP.Kaneva;

namespace Kaneva.PresentationLayer.Shopping.usercontrols
{
    public class Header : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string KanevaUrl = "http://" + KanevaGlobals.SiteName;

            if (Request.IsAuthenticated)
            {
                aLogo.HRef = KanevaUrl + "/default.aspx";
                aValidate.HRef = KanevaUrl + "/mykaneva/general.aspx";
                aHome.HRef = KanevaUrl + "/default.aspx";
                aBuyCredits.HRef = KanevaUrl + "/mykaneva/buyCreditPackages.aspx";
                aMailbox.HRef = KanevaUrl + "/mykaneva/mailbox.aspx";
                aMsgCenter.HRef = KanevaUrl + "/mykaneva/messagecenter.aspx?mc=ar";
                aFind3DApps.HRef = KanevaUrl + "/community/channel.kaneva?3dapps=";
                aMake3DApp.HRef = KanevaUrl + "/community/StartWorld.aspx";
                aMyProfile.HRef = KanevaUrl + "/community/ProfilePage.aspx";
                aMyProfile2.HRef = KanevaUrl + "/community/ProfilePage.aspx";
                aAcctSettings.HRef = KanevaUrl + "/mykaneva/general.aspx";
                aMessages.HRef = KanevaUrl + "/mykaneva/messagecenter.aspx";
                aMyWorlds.HRef = KanevaUrl + "/mykaneva/my3dapps.aspx";
                aExploreWorlds.HRef = KanevaUrl + "/community/channel.kaneva?3dapps=";
                aCreateWorld.HRef = KanevaUrl + "/community/StartWorld.aspx";
                aFindFriends.HRef = KanevaUrl + "/mykaneva/inviteFriend.aspx";
                aSearchPeople.HRef = KanevaUrl + "/people/people.kaneva";
                aGetCredits.HRef = KanevaUrl + "/mykaneva/buyCreditPackages.aspx";
                aContests.HRef = KanevaUrl + "/mykaneva/contests.aspx";

                aShop.HRef = "http://" + KanevaGlobals.ShoppingSiteName;
                aShopDD.HRef = "http://" + KanevaGlobals.ShoppingSiteName;

                // Grab single sign-on session variables used to display the sso Flash content.
                string singleSignOnToken = (new UserFacade()).GetSingleSignOnToken(KanevaWebGlobals.CurrentUser.UserId);
                if (!string.IsNullOrWhiteSpace(singleSignOnToken))
                {
                    string fSSOVars = "<param name=\"FlashVars\" value=\"" + singleSignOnToken + "\" />";

                    divFlashSSO.Visible = true;
                    litFlashSSOVars.Text = fSSOVars;
                    litFlashSSOVarsIE.Text = fSSOVars;

                    litJS.Text += "<script type=\"text/javascript\">var flashVarsToken='" + singleSignOnToken + "';</script>";
                }
            }
            else
            {
                aJoin.HRef = "http://" + KanevaGlobals.SiteName + "/register/kaneva/registerInfo.aspx";
                aSignIn.HRef = (Convert.ToBoolean (System.Configuration.ConfigurationManager.AppSettings["SSLLogin"]) ? "https://" : "http://") + KanevaGlobals.SiteName + "/loginSecure.aspx";
            }

            litJS.Text += "<script type=\"text/javascript\" src=\"" + ResolveUrl ("~/jscript/jquery/jquery-1.8.2.min.js") + "\"></script>" +
                          "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/jscript/notifications/jquery.notifyBar.min.js") + "\"></script>";

            litJS.Text += "<script type=\"text/javascript\">var gameName = '" + KanevaGlobals.WokGameName + "';" +
                "var patchUrl = '" + KanevaGlobals.WokPatcherUrl + "';" +
                "var root = '" + ResolveUrl("~/") + "';" +
                "var nopluginUrl = 'http://" + KanevaGlobals.SiteName + "/community/install3d.kaneva';</script>";

            if (Request.IsAuthenticated)
            {
                spnAuthenticated.Visible = true;

                SetTertiaryButtons (Request.IsAuthenticated);

                if (Request.IsAuthenticated)
                {
                    GetUserFortune ();
                }
            }
            else  // User not logged in
            {
                spnNotAuthenticated.Visible = true;

                SetPagePadding (false, false);

                litJS.Text += "<script type=\"text/javascript\" src=\"" + ResolveUrl ("~/jscript/login_base.js") + "\"></script>";
            }

            litCSS.Text += "<link href=\"" + ResolveUrl ("~/css/topnav.css") + "?v4\" rel=\"stylesheet\" type=\"text/css\">" +
                           "<link href=\"" + ResolveUrl("~/jscript/notifications/css/jquery.notifyBar.min.css") + "\" rel=\"stylesheet\" type=\"text/css\">";
        }

        private void SetTertiaryButtons (bool bLoggedIn)
        {
            if (bLoggedIn)
            {
                // Display new message count
                int numberNewMessages = KanevaWebGlobals.CurrentUser.Stats.NumberOfNewMessages;
                divMsgCount.InnerText = numberNewMessages > 99 ? "99" : numberNewMessages.ToString ();
                if (numberNewMessages == 0) { divMsgCount.Visible = false; }

                // Display new world requests count
                int numberNewWorldReq = KanevaWebGlobals.CurrentUser.Stats.Number3DAppRequests;
                divWorldReqCount.InnerText = numberNewWorldReq > 99 ? "99" : numberNewWorldReq.ToString ();
                if (numberNewWorldReq == 0) { divWorldReqCount.Visible = false; }

                spnUsername.InnerText = KanevaWebGlobals.CurrentUser.Username;
                validateContainer.Visible = false;

                // Per Billy, do not show the validate email message on shop 3/6
                /*
                string validateMsg = "";
                if (KanevaWebGlobals.CurrentUser.StatusId.Equals ((int) Constants.eUSER_STATUS.REGNOTVALIDATED))
                {
                    // Checking fame to see if they've been rewarded for validating their email.
                    // Users will only be rewarded the first time they validate.
                    FameFacade fameFacade = new FameFacade ();
                    if (fameFacade.IsOneTimePacketRedeemed (KanevaWebGlobals.CurrentUser.UserId, (int) PacketId.WEB_OT_EMAIL_VALIATION))
                    {
                        validateMsg = "Click here to verify your contact email address.";
                    }
                    else
                    {
                        validateMsg = "Click here to verify your email address and receive 1000 Rewards!";
                    }
                }

                SetPagePadding (true, KanevaWebGlobals.CurrentUser.StatusId.Equals ((int) Constants.eUSER_STATUS.REGNOTVALIDATED));

                // If we've marked them as a bounce show an alert.
                if (KanevaWebGlobals.CurrentUser.EmailStatus > 0 && !KanevaWebGlobals.CurrentUser.StatusId.Equals ((int) Constants.eUSER_STATUS.REGNOTVALIDATED))
                {
                    validateMsg = "We're unable to reach you! Please update your contact email address.";
                }

                if (validateMsg != "")
                {
                    aValidate.InnerText = validateMsg;
                    validateContainer.Visible = true;
                }
                */

                SetPagePadding (true, false);
            }
            else
            {
                lnkLogout.Visible = false;
            }

            // Shop and Buy Buttons
//            aShop.HRef = "http://" + KanevaGlobals.ShoppingSiteName;
//            aMake3DApp.HRef = ResolveUrl ("~/community/StartWorld.aspx");
        }

        private void SetPagePadding (bool isLoggedIn, bool emailNotValidated)
        {
            int pxCount = 76;

            if (isLoggedIn && emailNotValidated)
            {
                pxCount += 40;
            }

            litStyle.Text = "#pageBody {padding-top:" + pxCount + "px;} ";
        }

        public void GetUserFortune ()
        {
            // Get user point balances
            UserBalances ub = new UserFacade ().GetUserBalancesCached (KanevaWebGlobals.CurrentUser.UserId);

            spnCredits.InnerHtml = ub.Credits == 0 ? "0" : ub.Credits.ToString ("#,###");
            spnRewards.InnerHtml = ub.Rewards == 0 ? "0" : ub.Rewards.ToString ("#,###");
        }

        /// <summary>
        /// Log out of the site
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            System.Web.HttpCookie aCookie;
            int limit = Request.Cookies.Count - 1;

            for (int i = limit; i != -1; i--)
            {
                aCookie = Request.Cookies[i];
                aCookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(aCookie);
            }

            // Logout of single sign-on
            (new UserFacade()).CleanupSingleSignOn(KanevaWebGlobals.CurrentUser.UserId);

            FormsAuthentication.SignOut();
            Session.Abandon ();

            Response.Redirect ("~/default.aspx");

            // Get rid of warning message via this script code
            /*		string strJavascript = "<script language=\"JavaScript\">" +
                        "window.location = (\"http://" + KanevaGlobals.SiteName + "/loginHelper.aspx?action=logout\");" +
                        "</script>";

                    if (!Page.ClientScript.IsClientScriptBlockRegistered (GetType (), "logout"))
                    {
                        Page.ClientScript.RegisterClientScriptBlock(GetType(), "logout", strJavascript);	
                    }	
             */
        }

        public string FlashSSOPath
        {
            get { return ResolveUrl("~/flash/kaneva_sinfo.swf"); }
        }

        protected HtmlContainerControl spnAuthenticated, spnNotAuthenticated, spnUsername, validateContainer;
        protected HtmlContainerControl divMsgCount, divWorldReqCount, divFlashSSO, spnCredits, spnRewards;
        protected Literal litCSS, litStyle, litJS, litFlashSSOVars, litFlashSSOVarsIE;
        protected HtmlAnchor aMake3DApp, aValidate, aLogo, aHome, aBuyCredits, aMailbox, aMsgCenter;
        protected HtmlAnchor aFind3DApps, aMyProfile, aAcctSettings, aMyProfile2, aJoin, aSignIn;
        protected HtmlAnchor aMessages, aMyWorlds, aExploreWorlds, aCreateWorld, aFindFriends, aSearchPeople, aGetCredits, aContests;
        protected HtmlAnchor aShop, aShopDD;
        protected LinkButton lnkLogout;
        protected ImageButton btnSearch;
    }
}