<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="Kaneva.PresentationLayer.Shopping.usercontrols.Footer" %>

<div id="footer">
	<hr />
	<div id="footerContent">
		<ul>
			<li class="header"><a runat="server" href="~/Default.aspx">Shop Kaneva</a></li>
		    <li><a runat="server" href="~/CatalogLanding.aspx?cId=1">Floorplans</a></li>
	        <li><a runat="server" href="~/CatalogLanding.aspx?cId=2">Furnishings</a></li>
	        <li><a runat="server" href="~/CatalogLanding.aspx?cId=3">Clothing</a></li>
	        <li><a runat="server" href="~/CatalogLanding.aspx?cId=34">Building Materials</a></li>
	        <li><a runat="server" href="~/CatalogLanding.aspx?cId=35">Collectibles & Gifts</a></li>
			<li><a runat="server" href="~/MySales.aspx">My Store</a></li>
			<li><a runat="server" id="aDesigner" href="http://designer.kaneva.com">Designer Help</a></li>
		</ul>
		
		<ul>
			<li class="header"><a href="http://www.kaneva.com">Kaneva.com</a></li>
			<li><a runat="server" id="aAbout" href="#">About Kaneva</a></li>
			<li><a runat="server" id="aNews" href="#">News/Press Center</a></li>
			<li><a runat="server" id="aSupport" href="#" target="_blank">Support Center</a></li>
			<li><a runat="server" id="aCareers" href="#">Careers</a></li>
			<li><a runat="server" id="aPolicies" href="#">Policies & Guidelines</a></li>
			<li><a runat="server" id="aSafty" href="#">Safety Tips/Parental Resources</a></li>
			<li><a runat="server" id="aContactUs" href="#">Contact Us</a></li>
		</ul>
			
		<p class="copyright">�2000-<%= DateTime.Now.ToString("yyyy") %>  Kaneva, Inc.</p>
	</div>
</div>