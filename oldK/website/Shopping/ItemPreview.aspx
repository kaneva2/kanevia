<%@ Page Language="C#" AutoEventWireup="true" Codebehind="ItemPreview.aspx.cs" Inherits="Kaneva.PresentationLayer.Shopping.ItemPreview" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Item Preview</title>
</head>
<body style="padding:0;margin:0;">
	<form id="form1" runat="server">
		<div id="divAnimationPreview" runat="server" visible="true">
			<img src="" name="Rotating" id="Rotating1">

			<script language="JavaScript">
			  var ImageArr1 = new Array(<asp:literal id="litImgArray" runat="server"></asp:literal>);
				
			  var ImageHolder1 = document.getElementById('Rotating1');
			  		  
			  function RotateImages(whichHolder,Start)
			  {
  				var a = eval("ImageArr"+whichHolder);
  				var b = eval("ImageHolder"+whichHolder);
  				if(Start>=a.length)
  					Start=0;
  				b.src = a[Start];
  				window.setTimeout("RotateImages("+whichHolder+","+(Start+1)+")",250);
			  }
			  
			  RotateImages(1,0);		  
			</script>

		</div>
		<div id="divSoundPreview" runat="server" visible="false">
			<div id="div1" runat="server" visible="false">
				<!-- SOUND DEMO GOES HERE -->
				<div id="divIEplayer" runat="server">
					<object id='vlc1_IE' codebase="http://downloads.videolan.org/pub/videolan/vlc/latest/win32/axvlc.cab"
						height="40" width="320" classid="clsid:9BE31822-FDAD-461B-AD51-BE1D1C159921">
						<embed type="application/x-vlc-plugin" pluginspage="http://www.videolan.org" version="VideoLAN.VLCPlugin.2"
							width="320" height="240" id="vlc1">   
			   </embed>
					</object>

					<script type="text/javascript" language="javascript">
			   function play(tgt) {
					var uri = "<asp:Literal ID="litSoundPreview" runat="server">Test</asp:Literal>";
				   
					if (document.all) tgt += "_IE"
					var tgt = document.getElementById(tgt);
						 // alert(tgt);
					if (document.all) tgt.playlist.add(uri,uri, new Array());
					else     tgt.playlist.add(uri,uri, "");
					tgt.playlist.play(); 
			   }
					</script>

					<button onclick="play('vlc1')">
						Play</button>
				</div>
				<div id="divFFplayer" runat="server">
					<video controls>  
			   <asp:Literal ID="litSoundPreview2" runat="server">Test</asp:Literal>				
			   Your browser does not support the <code>video</code> element.  
		  </video>
				</div>
			</div>
		</div>
		<!--
			<asp:Literal ID="litStatus" runat="server"></asp:Literal>
		-->
		
	</form>
</body>
</html>
