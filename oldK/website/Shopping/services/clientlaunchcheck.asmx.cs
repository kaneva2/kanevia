///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;

namespace Kaneva.PresentationLayer.Shopping.services
{
    /// <summary>
    /// Summary description for clientlaunchcheck
    /// </summary>
    [WebService (Namespace = "http://www.kaneva.com/")]
    [WebServiceBinding (ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem (false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class clientlaunchcheck : System.Web.Services.WebService
    {
        [WebMethod]
        public bool HasClientLaunched (string playNowGuid)
        {
            return (new GameFacade()).CheckPlayNowStartedKey(playNowGuid);
        }
    }
}
