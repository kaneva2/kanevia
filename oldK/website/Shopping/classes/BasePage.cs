///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Diagnostics;
using System.IO;

using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.Metrics;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
    /// <summary>
    /// Summary description for BasePage.
    /// </summary>
    public class BasePage : System.Web.UI.Page
    {
        #region Declarations
        private UserFacade _userFacade;
        private TransactionFacade _transactionFacade;
        private PromotionsFacade _promotionsFacade;
        private ShoppingFacade _shoppingFacade;
        private CommunityFacade _communityFacade;
        private GameFacade _gameFacade;
        private FameFacade _fameFacade;
        private ScriptGameItemFacade _scriptGameItemFacade;
        private RaveFacade _raveFacade;

        #endregion

          /// <summary>
        /// OnPreRender
        /// </summary>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (Configuration.MetricPageTimingEnabled)
            {
                // Metric Timing
                Stopwatch stopwatch = (Stopwatch)this.Context.Items["MetricPageTiming"];
                stopwatch.Stop();

                TimeSpan ts = stopwatch.Elapsed;

                string pageName = Path.GetFileName(Page.Request.Url.AbsolutePath).ToUpper();

                // Is this a page to record metrics on
                if (Configuration.MetricsPageTiming.ContainsKey(pageName))
                {
                    PageTiming pt = (PageTiming)Configuration.MetricsPageTiming[pageName];
                    MetricsFacade metricsFacade = new MetricsFacade();

                    if (metricsFacade.CanLogPageMetrics(pt.URL.ToUpper()))
                    {
                        metricsFacade.InsertMetricsPageTiming(pt.Name, ts.TotalMilliseconds);
                    }
                }
            }
        }


        /// <summary>
		/// OnInit
		/// </summary>
		/// <param name="e"></param>
        protected override void OnInit(System.EventArgs e)
        {
            if (Configuration.MetricPageTimingEnabled)
            {
                // Metric Timing
                Stopwatch stopwatch = new Stopwatch();
                this.Context.Items["MetricPageTiming"] = stopwatch;
                stopwatch.Start();
            }

            base.OnInit(e);
        }


        // ***********************************************
        // Get Facade Functions
        // ***********************************************
        #region Get Facades

        protected PromotionsFacade GetPromotionsFacade
        {
            get
            {
                if (_promotionsFacade == null)
                {
                    _promotionsFacade = new PromotionsFacade();
                }
                return _promotionsFacade;
            }
        }

        protected TransactionFacade GetTransactionFacade
        {
            get
            {
                if (_transactionFacade == null)
                {
                    _transactionFacade = new TransactionFacade();
                }
                return _transactionFacade;
            }
        }

        protected UserFacade GetUserFacade
        {
            get
            {
                if (_userFacade == null)
                {
                    _userFacade = new UserFacade();
                }
                return _userFacade;
            }
        }

        protected ShoppingFacade GetShoppingFacade
        {
            get
            {
                if (_shoppingFacade == null)
                {
                    _shoppingFacade = new ShoppingFacade();
                }
                return _shoppingFacade;
            }
        }

        protected CommunityFacade GetCommunityFacade
        {
            get
            {
                if (_communityFacade == null)
                {
                    _communityFacade = new CommunityFacade ();
                }
                return _communityFacade;
            }
        }

        protected GameFacade GetGameFacade
        {
            get
            {
                if (_gameFacade == null)
                {
                    _gameFacade = new GameFacade ();
                }
                return _gameFacade;
            }
        }

        protected FameFacade GetFameFacade
        {
            get
            {
                if (_fameFacade == null)
                {
                    _fameFacade = new FameFacade ();
                }
                return _fameFacade;
            }
        }

        protected ScriptGameItemFacade GetScriptGameItemFacade
        {
            get
            {
                if (_scriptGameItemFacade == null)
                {
                    _scriptGameItemFacade = new ScriptGameItemFacade();
                }
                return _scriptGameItemFacade;
            }
        }

        protected RaveFacade GetRaveFacade
        {
            get
            {
                if (_raveFacade == null)
                {
                    _raveFacade = new RaveFacade();
                }
                return _raveFacade;
            }
        }


        #endregion


        /// <summary>
        /// Is the current session user a site administrator?
        /// </summary>
        /// <returns></returns>
        public bool IsAdministrator()
        {
            return UsersUtility.IsUserAdministrator();
        }

        /// <summary>
        /// Returns a bool indicating whether they have an access pass or not
        /// </summary>
        /// <returns></returns>
        protected bool UserHasMaturePass()
        {
            return KanevaWebGlobals.UserHasMaturePass();
        }

        /// <summary>
        /// Return the user image URL
        /// </summary>
        public string GetProfileImageURL(string imagePath, string defaultSize, string gender)
        {
            return UsersUtility.GetProfileImageURL(imagePath, defaultSize, gender);
        }

        /// <summary>
        /// Return the personal channel link
        /// </summary>
        /// <param name="channelId"></param>
        /// <returns></returns>
        public static string GetPersonalChannelUrl(string nameNoSpaces)
        {
            return KanevaGlobals.GetPersonalChannelUrl(nameNoSpaces);
        }

        /// <summary>
        /// GetItemDetailsURL
        /// </summary>
        protected string GetItemDetailsURL(string globalId)
        {
            return ResolveUrl("~/ItemDetails.aspx?gId=" + globalId);
        }

        /// <summary>
        /// GetItemEditURL
        /// </summary>
        protected string GetItemEditURL(string globalId)
        {
            return ResolveUrl("~/ItemEdit.aspx?gId=" + globalId);
        }

        /// <summary>
        /// GetItemImageURL
        /// </summary>
        protected string GetItemImageURL(string imagePath, string defaultSize)
        {
			
			// get upload_type by category ID
			ShoppingFacade shoppingFacade = new ShoppingFacade();
			int uploadType = (int)UGCUpload.eType.UNKNOWN;
			

			// for catalog pages
			try
			{				
				uint catId = Convert.ToUInt16(Request.Params["cId"]);

				if (catId > 0)
				{
					uploadType = WebCache.GetCachedUploadType(catId);
				}
			}
			catch { }
			

			if (uploadType == (int)UGCUpload.eType.SND_OGG_KRX)
			{
				return StoreUtility.GetItemImageURL(imagePath, defaultSize, "Sound");
			}
			else
			{
				return StoreUtility.GetItemImageURL(imagePath, defaultSize);
			}
        }

        /// <summary>
        /// GetStyleImage
        /// </summary>
        protected string GetStyleImage(string imagePath, string defaultSize)
        {		
			 return "background: transparent url(" + GetItemImageURL(imagePath, defaultSize ) + ") center center; background-repeat:no-repeat";
        }

		/// <summary>
		/// GetStyleImage
		/// </summary>
		protected string GetStyleImage(string imagePath, string texturePath, string defaultSize)
        {
			string style = "background: transparent url(" + GetItemImageURL(imagePath, defaultSize) + ") center center; background-repeat:no-repeat";
			
			if (IsAdministrator())
			{
				if (Request["text"] != null)
				{
					style = "background: transparent url(" + GetItemImageURL(texturePath, defaultSize) + ") center center; background-repeat:no-repeat";
				}
			}
			
			return style;
			
        }

        /// <summary>
        /// GetLoginURL
        /// </summary>
        /// <returns></returns>
        public string GetLoginURL()
        {
            if (Configuration.SSLLogin)
            {
                return "https://" + KanevaGlobals.SiteName + "/loginSecure.aspx?logretURLNH=" + Server.UrlEncode(GetCurrentURL());
            }
            else
            {
                return ResolveUrl("~/loginSecure.aspx?logretURLNH=" + Server.UrlEncode(GetCurrentURL()));
            }
        }

        /// <summary>
        /// GetCurrentURL
        /// </summary>
        /// <returns></returns>
        public string GetCurrentURL()
        {
            return Request.Url.AbsoluteUri;
        }

        /// <summary>
        /// Get Access Pass Link
        /// </summary>
        public string GetAccessPassLink()
        {
            return "http://" + KanevaGlobals.SiteName + "/mykaneva/passDetails.aspx?pass=true&passId=" + KanevaGlobals.AccessPassPromotionID;
        }

        /// <summary>
        /// GetShareLink
        /// </summary>
        public string GetShareLink(int globalId)
        {
            return "http://" + KanevaGlobals.SiteName + "/mykaneva/newMessage.aspx?gId=" + globalId + "&URL=" + GetItemDetailsLink(globalId);
        }

        public string GetItemDetailsLink(int globalId)
        {
            return "http://" + KanevaGlobals.ShoppingSiteName + "/itemDetails.aspx?gId=" + globalId;
        }

        /// <summary>
        /// Show an error message on startup
        /// </summary>
        /// <param name="errorMessage"></param>
        protected void ShowErrorOnStartup(string errorMessage)
        {
            ShowErrorOnStartup(errorMessage, true);
        }

        /// <summary>
        /// Show an error message on startup
        /// </summary>
        /// <param name="errorMessage"></param>
        protected void ShowErrorOnStartup(string errorMessage, bool bShowPleaseMessage)
        {

            string scriptString = "";

            if (!MagicAjax.MagicAjaxContext.Current.IsAjaxCallForPage (Page))
            {
                scriptString = "<script language=JavaScript>";
            }

            if (bShowPleaseMessage)
            {
                scriptString += "alert ('Please correct the following errors:\\n\\n" + errorMessage + "');";
            }
            else
            {
                scriptString += "alert ('" + errorMessage + "');";
            }

            if (!MagicAjax.MagicAjaxContext.Current.IsAjaxCallForPage(Page))
            {
                scriptString += "</script>";
            }

            if (!ClientScript.IsClientScriptBlockRegistered(this.GetType(), "ShowError"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "ShowError", scriptString);
            }
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return KanevaGlobals.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return KanevaGlobals.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }


        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
        {
            return KanevaGlobals.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
        {
            return KanevaGlobals.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts);
        }

        /// <summary>
        /// Get UGC template info
        /// </summary>
        /// <returns>true if succeeded. false if no template.</returns>
        public bool GetUGCTemplateInfo(WOKItem item, out UInt32 templateCreatorId, out UInt32 commission)
        {
            return (new ShoppingFacade()).GetUGCTemplateInfo(item, out templateCreatorId, out commission);
        }

        public void ConfigureCommunityMeetMe3D(global::System.Web.UI.WebControls.LinkButton lnkButton, int gameId, int communityId, string communityName, string requestTrackingGUID)
        {
            lnkButton.Attributes.Add("onclick", StpUrl.GetPluginJS(Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, gameId, communityId, communityName, requestTrackingGUID, KanevaWebGlobals.CurrentUser.UserId));
        }

        public void ConfigureCommunityMeetMe3D(global::System.Web.UI.HtmlControls.HtmlAnchor aMeetMeLink, int gameId, int communityId, string communityName, string requestTrackingGUID)
        {
            aMeetMeLink.Attributes.Add ("onclick", StpUrl.GetPluginJS (Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, gameId, communityId, communityName, requestTrackingGUID, KanevaWebGlobals.CurrentUser.UserId));
        }

        protected void SetItemThumbnailsToAssetImage(int assetId, int globalId)
        {
            Asset chosenAsset = (new MediaFacade()).GetNewAssetInformation(assetId);

            string imageHttpUrl = KanevaGlobals.ImageServer + "/" + chosenAsset.ImageFullPath;
            string thumbnailFilename = Path.GetFileName(chosenAsset.ImageFullPath);
            string strPathUserIdGlobalId = Path.Combine(KanevaWebGlobals.CurrentUser.UserId.ToString(), globalId.ToString());
            string strFilePathTexture = Path.Combine(KanevaGlobals.TexturePath, strPathUserIdGlobalId);
            string origImagePath = strFilePathTexture + Path.DirectorySeparatorChar + thumbnailFilename;

            try
            {
                ImageHelper.UploadImageFromServer(thumbnailFilename, imageHttpUrl, strFilePathTexture);
                ImageHelper.GenerateWOKItemThumbs(thumbnailFilename, origImagePath, KanevaGlobals.TexturePath, KanevaWebGlobals.CurrentUser.UserId, globalId);
            }
            catch { }
        }
    }
}
