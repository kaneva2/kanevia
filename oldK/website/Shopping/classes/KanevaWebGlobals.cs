///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using System.Reflection;
using System.Resources;
using System.Globalization;
using System.Xml;

using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
	/// <summary>
	/// Summary description for KanevaWebGlobals.
	/// </summary>
	public class KanevaWebGlobals
	{
		private static ResourceManager resmgr = new ResourceManager ("KlausEnt.KEP.Kaneva.Kaneva", Assembly.GetExecutingAssembly());

		/// <summary>
		/// KanevaWebGlobals
		/// </summary>
		public KanevaWebGlobals ()
		{
		}

        /// <summary>
        /// Get whether the user has a mature ("access") pass
        /// </summary>
        /// <returns></returns>
        public static bool UserHasMaturePass()
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                User userInfo = (User)HttpContext.Current.Items["User"];
                return userInfo.HasAccessPass;
            }
            else
            {
                return false;
            }
        }	

		/// <summary>
		/// Get the user info from the current logged in user
		/// </summary>
		/// <returns></returns>
		public static User CurrentUser
		{
            get
            {
                User user = (User)HttpContext.Current.Items["User"];
                return user;

                //if (HttpContext.Current.Request.IsAuthenticated)
                //{
                //    User user = (User)HttpContext.Current.Items["User"];
                //    return user;
                //}
                //else
                //{
                //    return null;
                //}
            }
		}

		/// <summary>
		/// Get the user id from the current logged in user
		/// </summary>
		/// <returns></returns>
		public static int GetUserId ()
		{
			if (HttpContext.Current.Request.IsAuthenticated)
			{
				User userInfo = (User) HttpContext.Current.Items ["User"];
				return userInfo.UserId;
			}
			else
			{
				return 0;
			}
		}	

		/// <summary>
		/// Get the user id from the current logged in user
		/// </summary>
		/// <returns></returns>
		public static string GetUserState (string ustate)
		{
			switch (ustate)
			{
				case Constants.ONLINE_USTATE_OFF:
					return "";
				case Constants.ONLINE_USTATE_ON:		// logged into web site
					return "on-web";
				case Constants.ONLINE_USTATE_INWORLD:	// logged into WOK
					return "in-world";
				case Constants.ONLINE_USTATE_ONINWORLD:	// logged into web site and WOK
					return "in-world";
				default:
					return "";
			}
		}	

		/// <summary>
		/// Is the user in the Virtual World?
		/// </summary>
		/// <param name="ustate"></param>
		/// <returns></returns>
		public static bool IsInWorld (string ustate)
		{
			return (ustate.Equals (Constants.ONLINE_USTATE_INWORLD) || ustate.Equals (Constants.ONLINE_USTATE_ONINWORLD));
		}
	}
}
