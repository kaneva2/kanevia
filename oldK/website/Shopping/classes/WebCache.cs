///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web.Caching;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using KlausEnt.KEP.Kaneva;

namespace Kaneva.PresentationLayer.Shopping
{
    /// <summary>
    /// Summary description for Cache.
    /// </summary>
    public class WebCache
    {
        public WebCache()
        {
        }

        #region Cashed Functions


		/// <summary>
		/// GetCachedUploadType
		/// </summary>
		/// <returns></returns>
		public static int GetCachedUploadType(uint catId)
		{
			string cacheKey = "cacheUploadType" + catId;

			ShoppingFacade shoppingFacade = new ShoppingFacade();

			string sUploadType = "0";
            sUploadType = (string)Global.Cache()[cacheKey];

            if (sUploadType == null)
			{
				// Add to the cache
				//lItemTemplates = shoppingFacade.GetItemTemplates();
				sUploadType = shoppingFacade.GetUploadTypeByItemCategoryId(Convert.ToInt32(catId), 0).ToString ();
                Global.Cache().Insert(cacheKey, sUploadType, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
			}

			return Convert.ToInt32 (sUploadType);
		}



        /// <summary>
        /// GetCachedTemplates
        /// </summary>
        /// <returns></returns>
        public static List<ItemTemplate> GetCachedTemplates()
        {
            string cacheKey = "cacheItemTemplates";

            ShoppingFacade shoppingFacade = new ShoppingFacade();
            List<ItemTemplate> lItemTemplates = (List<ItemTemplate>)Global.Cache()[cacheKey];

            if (lItemTemplates == null)
            {
                // Add to the cache
                lItemTemplates = shoppingFacade.GetItemTemplates();
                Global.Cache().Insert(cacheKey, lItemTemplates, null, DateTime.Now.AddMinutes(15), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return lItemTemplates;
        }

        /// <summary>
        /// GetPassGroupListFiltered
        /// </summary>
        /// <returns></returns>
        public static DataTable GetPassGroupListFiltered(string passTypeFilter)
        {
            string cacheKey = "cachePassGroupTypesFiltered";

            DataTable passGroupListF = (DataTable)Global.Cache()[cacheKey];

            if (passGroupListF == null)
            {
                // Add to the cache
                passGroupListF = WOKStoreUtility.GetPassGroups(passTypeFilter);
                Global.Cache().Insert(cacheKey, passGroupListF, null, DateTime.Now.AddMinutes(15), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return passGroupListF;
        }

        /// <summary>
        /// GetPassGroupList
        /// </summary>
        /// <returns></returns>
        public static DataTable GetPassGroupList()
        {
            string cacheKey = "cachePassGroupTypes";

            DataTable passGroupList = (DataTable)Global.Cache()[cacheKey];

            if (passGroupList == null)
            {
                // Add to the cache
                passGroupList = WOKStoreUtility.GetPassGroups();
                Global.Cache().Insert(cacheKey, passGroupList, null, DateTime.Now.AddMinutes(15), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return passGroupList;
        }

        /// <summary>
        /// GetCachedCategories
        /// </summary>
        public static List<ItemCategory> GetTopLevelCategories()
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            string cacheKey = "TopLevelCats";
            List<ItemCategory> itemCategories = (List<ItemCategory>)Global.Cache()[cacheKey];

            if (itemCategories == null)
            {
                // Add to the cache
                itemCategories = shoppingFacade.GetTopLevelCategories();
                Global.Cache().Insert(cacheKey, itemCategories, null, DateTime.Now.AddMinutes(15), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return itemCategories;
        }

        /// <summary>
        /// GetCachedCategories
        /// </summary>
        public static List<ItemCategory> GetCachedCategories(uint ItemCategoryId)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            string cacheKey = "ShopCats" + ItemCategoryId.ToString();
            List<ItemCategory> itemCategories = (List<ItemCategory>)Global.Cache()[cacheKey];

            if (itemCategories == null)
            {
                // Add to the cache
                itemCategories = shoppingFacade.GetItemCategoriesByParentCategoryId(ItemCategoryId);
                Global.Cache().Insert(cacheKey, itemCategories, null, DateTime.Now.AddMinutes(15), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return itemCategories;
        }

        /// <summary>
        /// GetAllBottomLevelCategories
        /// </summary>
        public static List<ItemCategory> GetAllBottomLevelCategories(string filter)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            string cacheKey = "DOCategoryList";
            List<ItemCategory> itemCategories = (List<ItemCategory>)Global.Cache()[cacheKey];

            if (itemCategories == null)
            {
                // Add to the cache
                itemCategories = shoppingFacade.GetCategoriesThatHaveItems(filter);
                Global.Cache().Insert(cacheKey, itemCategories, null, DateTime.Now.AddMinutes(15), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return itemCategories;
        }

        /// <summary>
        /// GetAllRaveTypes
        /// </summary>
        public static IList<RaveType> GetAllRaveTypes ()
        {
            string cacheItemsName = "raveTypes";

            // Load countries
            IList<RaveType> dtRaveTypes = (IList<RaveType>) Global.Cache ()[cacheItemsName];

            if (dtRaveTypes == null)
            {
                // Get and add to cache
                RaveFacade raveFacade = new RaveFacade ();
                dtRaveTypes = raveFacade.GetRaveTypes ();
                Global.Cache ().Insert (cacheItemsName, dtRaveTypes, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtRaveTypes;
        }

        #endregion


        #region constants

        private const string cKACHING_LEADER = "commCategories";

        #endregion

    }
}
