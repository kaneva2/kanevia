///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Data;
using System.IO;
using System.Drawing;
using System.Web.Hosting;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using KlausEnt.KEP.Kaneva.Mailer;

namespace Kaneva.PresentationLayer.Shopping
{
	/// <summary>
	/// Summary description for MailUtilityWeb.
	/// </summary>
	public class MailUtilityWeb
	{
		public MailUtilityWeb()
		{
		}
   	
		/// <summary>
		/// SendBlastNotificationEmail
		/// </summary>
		public static void SendItemCommentNotificationEmail(string toEmail,int gId, string commentText, int toUserId, int fromUserId)
		{
			// store subject and message as overload fields		
			int overloadId = 0;
			overloadId = Queuer.AddOverloadField(overloadId, "#CommentText#", commentText);
			overloadId = Queuer.AddOverloadField(overloadId, "#GlobalId#", gId.ToString());

			//if overloadid did not work don't send the email.
			if (overloadId != 0)
			{
				int typeId = EmailTypeUtility.GetTypeIdByName("ShopItemComment");				
				TemplateSherpa.DressAndSendTemplate(typeId, 0, toEmail, 0, fromUserId, toUserId, "", 0, "", "", 0, overloadId);
			}
		}

        /// <summary>
        /// SendPrivateMessageNotificationEmail
        /// </summary>
        public static void SendPrivateMessageNotificationEmail (string toEmail, string strSubject, int messageId, string messageText, int toUserId, int fromUserId)
        {
            // store subject and message as overload fields		
            int overloadId = 0;
            overloadId = Queuer.AddOverloadField (overloadId, "#PMSubject#", strSubject);
            overloadId = Queuer.AddOverloadField (overloadId, "#PMMessageText#", messageText);

            //if overloadid did not work don't send the email.
            if (overloadId != 0)
            {
                int typeId = EmailTypeUtility.GetTypeIdByName ("PrivateMessage");
                string strMessageId = messageId.ToString ();
                TemplateSherpa.DressAndSendTemplate (typeId, 0, toEmail, 0, fromUserId, toUserId, "", 0, strMessageId, "", 0, overloadId);
            }
        }

        // Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

	}
}
