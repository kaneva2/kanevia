﻿<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplateNoLeftNav.Master" AutoEventWireup="true" Codebehind="ItemDetails.aspx.cs" Inherits="Kaneva.PresentationLayer.Shopping.ItemDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="cntUpload" runat="server" ContentPlaceHolderID="cphBody">
	<script src="jscript/PatcherDetect.js?v5" language="JavaScript" type="text/javascript"></script>
	<script src="jscript/jquery/jquery-ui.min.js" language="JavaScript" type="text/javascript"></script>
	<script src="jscript/facebox/jquery.facebox.js" language="JavaScript" type="text/javascript"></script>
	<script type="text/javascript">    
		<!--
		var $j = jQuery.noConflict();
		var raveCountCtrlName = '<asp:literal id="litCtrlRaveCount" runat="server"></asp:literal>'; 
		var pn_js = function playnow() {}
		// Length of Comment
		var textboxtextSave = '';
		var maxChars = 256;
        var gId = <asp:literal id="litGlobalId" runat="server"></asp:literal>;

		$j(document).ready(function () {

            $j.ajax({url: 'VIPPriceData.aspx?gId=' + gId, success: function(result){
                 $j('#pVIPPrice').html(result);
             }});

            <asp:literal id="litLoadDeedItems" runat="server"></asp:literal>

			popFacebox = function () {
				$j.facebox({ iframe: 'buyRave.aspx?globalId=' + gId});
			};

			$j('.infoButton').on('mouseenter mouseleave', function () {
				$j('.smartObjectInfo').toggle();
			});

			msg_dialog = $j('<div></div>')
				.html('')
				.dialog({
					autoOpen: false,
					resizable: false,
					modal: true,
					height: 192,
					width: 367,
					buttons: [
						{
							text: "OK",
							id: "dialogButtonOK",
							click: function () {
								$j(this).dialog("close");
							}
						},
						{
							text: "Cancel",
							click: function () {
								$j(this).dialog("close");
							}
						}
					],
					open: function () {
						$j(":button:contains('OK')").removeClass('dialogButton okButton');
						$j(":button:contains('OK')").addClass('dialogButton okButton');

						$j(":button:contains('Cancel')").removeClass('dialogButton cancelButton');
						$j(":button:contains('Cancel')").addClass('dialogButton cancelButton');
					}
				});

		});
		
		function openMsgDialog(title, msg, okurl) {
			msg_dialog.dialog({ title: title });
			msg_dialog.html(msg);
			
			if (okurl) {
				$j(":button:contains('OK')").unbind("click").click(
					function () {
						parent.document.location = okurl;
					}
				);

				$j(":button:contains('Cancel')").prop("disabled", false);
				$j(":button:contains('Cancel')").show();
				$j(":button:contains('OK')").removeClass('okButtonCenter');
			}
			else {
				$j(":button:contains('OK')").unbind("click").click(
					function () {
						msg_dialog.dialog("close");
					}
				);
				
				$j(":button:contains('Cancel')").prop("disabled", true);
				$j(":button:contains('Cancel')").hide();
				$j(":button:contains('OK')").addClass('okButtonCenter');
			}

			msg_dialog.dialog('open');
		}

		function TryOnDeed(url, gameId, email, version, playNowGuid)
		{
		    url += "/?";
		    TryIt(url, gameId, 0, '0', email, version, playNowGuid);
		}

		function TryOn(gameId, gId, useType, email, version, playNowGuid)
		{
			var url = 'kaneva://' + gameId + "/?";
			TryIt(url, gameId, gId, useType, email, version, playNowGuid);
		}

		function TryIt (url, gameId, gId, useType, email, version, playNowGuid)
		{																  
			var strTryOn = '';
			if (gId > 0)
			{
				strTryOn = '' + gId + ':' + useType;
			}

			DetectPlugin(version, gameId, url, strTryOn, email, true, playNowGuid);
		}

		function CharsRemain(t, span_id)
		{								 						   
			var val = $j(t).val().length;
	 	 	
			if (val > maxChars) {
				$j(t).val(textboxtextSave);
			}
			else {	 
				$j('#'+span_id).text(maxChars-val+' ');
				textboxtextSave = $j(t).val();
			}   
		}
		  
		function ProcessOrder ()
		{										 			  
			$j('#divComplete').hide();
			$j('#tblBalances').hide();	  
			$j('#divProcessing').show();
		}
		  
		function ResetProcess ()
		{													 
			$j('#divComplete').hide();
			$j('#divProcessing').hide();
			$j('#tblBalances').show();
		}	
	 	 	
		function LogGoogleAnalyticsCommerce (account, orderId, total, city, state, country, sku, name, cat, price, quantity)
        {
			var _gaq2 = _gaq || []; 
			_gaq2.push(['t2._setAccount', account]); 
			_gaq2.push(['t2._setDomainName', 'kaneva.com']);
			_gaq2.push(['t2._trackPageview']); 
			_gaq2.push(['t2._addTrans', 
				orderId,    // order ID - required 
				'Shop',		// affiliation or store name 
				total,      // total - required 
				'0',        // tax 
				'0',        // shipping 
				city,       // city 
				state,		// state or province 
				country     // country 
			]); 
         
			// add item might be called for every item in the shopping cart 
			// where your ecommerce engine loops through each item in the cart and 
			// prints out _addItem for each  
			_gaq2.push(['t2._addItem', 
				orderId,    // order ID - required 
				sku,        // SKU/code 
				name,       // product name 
				cat,		// category or variation 
				price,      // unit price - required 
				quantity    // quantity - required 
			]); 
			_gaq2.push(['t2._trackTrans']); //submits transaction to the Analytics servers 
		}

        function popWarning ()
		{							 
			var availHeight; 
			var availWidth; 
			var divContain = $('mainContent');
			availHeight = divContain.offsetHeight;
			availWidth = divContain.offsetWidth; 
			var popupHeight = 230; 		
			var popupWidth = 400; 		
			var pop = $('divWarning');
			pop.style.top = (availHeight-popupHeight)/2;			
			pop.style.left = (availWidth-popupWidth)/2;
			pop.style.width = popupWidth + "px";
			pop.style.height = popupHeight + "px";
			pop.style.display = "block"; 		
		}
		
		function showMsg()
		{			 
			var type = $j('#ddlWorlds option:selected').attr('type');
			var msgbox = $j('#existingMsgbox');
			var show = false;
			var head = '<p class="bold warning">Warning:</p>';
					
			switch (type)
			{
				case '2':
					txt='<span class="bold warning">This entire world will be replaced</span>. All objects will be removed from the world and the game system.<br/><span class="bold">This cannot be undone.</span></p>';
					show = true;
					break;
				case '3':
					head = '<p class="bold">Note</p>';
					txt='Find this World in your <span class="bold">Import World Menu</span> in world.</p>';
					show = true;
					break;
				case '5':
					txt='This new world will <span class="bold">overwrite your Home</span>. All items in your Home will be returned to your inventory/storage.</p>';
					show = true;
					break;
			}
			
			if (show)
			{
				msgbox.html(head+txt);
				msgbox.show();
			}
			else {msgbox.hide();}
		}
				
		function showzone(id)
		{
			if (!$j('#'+id).hasClass('selected'))
			{
				$j('[name="zonesection"].selected').nextAll('div .blank').slideUp();
				$j('[name="zonesection"]').removeClass('selected');
			
				$j('#'+id).addClass('selected');
				$j('#'+id + ' input:radio').prop('checked',true);
				$j('#'+$j('#'+id + ' input:radio').attr('id')+'Msg').slideToggle();
			}
		}

		$j(function () {
	   		$j('#imgUseType').tooltip({
	   			position: {
					my: "left-34 bottom+64",
	   				at: "bottom",
	   				using: function (position, feedback) {
	   					$j(this).css(position);
	   					$j("<div>")
						.addClass("arrow")
						.addClass(feedback.vertical)
						.appendTo(this);
	   				}
	   			}
	   		});
		});
		<asp:literal id="litGMPurchase" runat="server"></asp:literal>
        
	//--> 
	</script>
    
	<link href="jscript/facebox/jquery.facebox.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="jscript/jquery/jquery-ui.min.css" type="text/css" media="all" />
	<style>
	  .ui-tooltip, .arrow:after {
		background: #fff;
		border: 1px solid #ccc;
		width: 290px;
	  }
	  .ui-tooltip {
		padding: 10px 20px;
		color: #333;
		box-shadow: 0 0 2px #000; 
	  }
	  .arrow {
		width: 35px;
		height: 12px;
		overflow: hidden;
		position: absolute;
		left: 16px;
		margin-left: 0px;	  
		bottom: 0px;
	  }
	  .arrow.top {
		top: -12px;
		bottom: auto;
	  }
	  .arrow.left {
		left: 0;
	  }
	  .arrow:after {
		content: "";
		position: absolute;	  
		left: 6px;
		top: -0px;
		width: 14px;
		height: 26px;
		box-shadow: 6px 5px 9px -9px black;
		-webkit-transform: rotate(45deg);
		-ms-transform: rotate(45deg);
		transform: rotate(45deg);
	  }
	  .arrow.top:after {
		bottom: -20px;
		top: auto;
	  }
      .adminfree, .purchasenew a.adminfree {
          float:right; 
          margin-top:8px;
          border:none;
          background:transparent;
          font-weight:bold;
          text-decoration:underline;
      }
      .adminfree {margin-top:20px;}
      .purchasenew a.adminfree:hover {text-decoration:none;}
	</style>

	<div id="divAccessMessage" runat="server" visible="false">
		<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
			<tr>
				<td valign="top" align="center">
					<table border="0" cellpadding="0" cellspacing="0" width="75%">
						<tr>
							<td class="frTopLeft"></td>
							<td class="frTop"></td>
							<td class="frTopRight"></td>
						</tr>
						<tr>
							<td class="frBorderLeft">
								<img id="Img1" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td valign="top" class="frBgIntMembers">
								<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td valign="top" align="center">
											<div class="module whitebg">
												<span class="ct"><span class="cl"></span></span>
												<h2 id="h2Title" runat="server"></h2>
												<table cellpadding="6" cellspacing="0" border="0" width="95%">
													<tr>
														<td rowspan="3" valign="top">
															<img src="../images/access_pass.png" border="0" />
														</td>
														<td align="left" valign="top" id="tdActionText" runat="server" style="padding:0 20px 20px 20px;width:50%"></td>
													</tr>
													<tr>
														<td align="center" valign="top" id="tdButton" runat="server">
															<asp:Button ID="btnAction" runat="server"></asp:Button>
														</td>
													</tr>
													<tr>
														<td align="left" valign="top" id="tdActionText2" runat="server" style="padding:20px 20px 20px 20px;width:50%"></td>
													</tr>
													<tr>
														<td colspan="2" align="left" valign="top" id="tdPolicyText" runat="server"></td>
													</tr>
												</table>
												<span class="cb"><span class="cl"></span></span>
											</div>
										</td>
									</tr>
								</table>
							</td>
							<td class="frBorderRight"><img id="Img2" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
						</tr>
						<tr>
							<td class="frBottomLeft"></td>
							<td class="frBottom"></td>
							<td class="frBottomRight"></td>
						</tr>
						<tr>
							<td><img id="Img3" runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
    
	<asp:scriptmanager id="smMain" runat="server" enablepartialrendering="true"></asp:scriptmanager>

	<div id="divDetails" runat="server" visible="false" style="width:920px;display:block;">
		<div id="content" style="padding-top:16px;display:block;clear:both;">
			<div id="itemDescription">
				<img id="imgUseType" clientidmode="Static" runat="server" style="float:left;" title="" />
				<div style="float:left;margin-left:8px;">
					<h2 style="text-align:left;margin:2px 0 4px 0;width:260px;"><asp:Label runat="server" ID="lblName"></asp:Label>
						<span id="spnAnimationOnly" runat="server" visible="false" style="padding-left:2px;font-size:10px;font-weight:normal;">(animation only)</span>
					</h2>
					<h3 id="pricecontainer" runat="server" style="text-align:left;"><span  style="font-weight:bold;">List Price:</span>
						<asp:Label runat="server" ID="lblPrice" style=""></asp:Label>
						<asp:Label runat="server" ID="lblPurchaseType"></asp:Label>
					</h3><br />
					<h3 style="text-align:left;"><span  style="font-weight:bold;">VIP Price:</span>
                        <span id="pVIPPrice"><img id="Img6" class="imgLoading" runat="server" src="~/images/ajax-loader_vote_trans.gif" alt="Loading..." border="0"/><span class="loadingText"></span></span> 
						<asp:Label runat="server" ID="lblVIPPurchaseType"></asp:Label> 
                        <span id="buyVIPContainer" runat="server" visible="true" style="font-size:11px;">(<a href="javascript:void(0);" id="aBuyVIP" runat="server" style="font-size:11px;">What's this?</a>)</span>
					</h3>
				</div>
				<div id="divRequiredItem" runat="server" visible="false" style="clear:both;padding-top:2px;margin-left:9px;display:block;"><span style="color:Red;font-weight:bold;font-size:11px;">REQUIRES: </span> <a id="aReqItem" runat="server"></a></div>
				<div class="clearit"></div>
					<!-- Item Preview -->
					<iframe id="ifItemPreview" runat="server" scrolling="no" width="320" height="240" frameborder="0" style="border:1px;" visible="false"></iframe> 					
					
					<div class="itemImage" style="clear:both;margin-top:14px;">
						<img runat="server" id="imgItem" src="" alt="" />
						<div id="divRestricted" class="restricted" runat="server"></div>
					</div>
					<!-- Item Preview End -->
				
					<asp:updatepanel id="upHidePurchase" runat="server">
						<contenttemplate>
							<span id="spnHideDuringPurchase" runat="server">

							<div class="clearit"></div>
							<div class="floatLeft" id="divTryOn">
								<a id="btn_preview" href="#" class="btn_preview" name="btn_preview" runat="Server" title="Try This" onclientclick="_gaq.push(['_trackPageview','/clickTryItem.aspx'])"></a>
							</div>
							<div class="floatLeft">
								<asp:LinkButton ID="btn_purchase" name="btn_purchase" runat="Server" CssClass="btn_purchase"
									OnClick="lbPurchase_OnClick" Text="" ToolTip="Buy This Item" OnClientClick="_gaq.push(['_trackPageview','/clickPurchaseItemClick.aspx'])"></asp:LinkButton>
							</div>

							<div class="clearit"></div>
							<div class="alignLeft">
							
							</div>
							<div class="clearit"></div>
							<div id="addthisStuff" class="floatLeft">
								<!-- AddThis Button BEGIN -->
							
								<!-- AddThis Button END -->
							</div>
							<table style="width:100%;margin-top:20px;" border="0">
								<tr>
									<td align="left">
										<div id="ravethis">
											<asp:LinkButton ID="lbRave" runat="Server" OnClick="lbRave_OnClick"  
												Text="" OnClientClick="_gaq.push(['_trackPageview','/clickRaveItem.aspx'])">
											<span><asp:literal ID="litRaveCount" runat="server"/></span></asp:LinkButton>
										</div>
									</td>
									<td align="center" style="overflow:hidden;">
										<div class="addthis_toolbox addthis_default_style" style="overflow:hidden;">
											<a class="addthis_button_tweet"></a>
											<a class="addthis_button_facebook_like" <%="fb:like:layout"%>="button_count" style="margin-left:-10px;"></a>
										</div>
									</td>
									<td align="right"><a class="addthis_counter addthis_pill_style"></a></td>
								</tr>
							</table>
							<div class="clearit"></div>
							<div class="floatLeft">
								<p style="padding-left:0;">
								<asp:LinkButton ID="lbRpt" runat="Server" CssClass="btn_rpt" Text="" OnClientClick="_gaq.push(['_trackPageview','/clickReportItem.aspx'])">Report This</asp:LinkButton>
								</p>
							</div>
							</span>
						</contenttemplate>
					</asp:updatepanel>


					<div class="clearit"></div>
					<table cellpadding="0" cellspacing="0" border="0" style="margin-top:10px;width:100%;">
						<tr><th>Category:</th></tr>
						<tr><td style="padding-left:20px;">
							<asp:Repeater ID="rptCategories" runat="server">
								<ItemTemplate>
									<a href="<%# (Convert.ToInt32(DataBinder.Eval(Container.DataItem, "ParentCategoryId")) == 0 ? "CatalogLanding" : "Catalog") %>.aspx?cId=<%# DataBinder.Eval(Container.DataItem, "ItemCategoryId")%>"><%# DataBinder.Eval(Container.DataItem, "Description") %></a>
								</ItemTemplate>
								<separatortemplate> > </separatortemplate>
							</asp:Repeater>
						</td></tr>
					</table>
					<br />
					<span class="bold">Description:</span><br />
					<p style="padding:3px 0 10px 20px;"><asp:Label runat="server" ID="lblDescription"></asp:Label></p>
					<p><asp:Label runat="server" ID="lblKeywords"></asp:Label></p>
					<asp:updatepanel id="upBack" runat="server">
						<contenttemplate>
							<p id="pBack" runat="server"><a id="A1" href="JavaScript:history.back(1)">Back to Previous Page</a></p>
						</contenttemplate>
					</asp:updatepanel>




			</div>
		</div>
		
		<!-- Start the second column -->
		<asp:updatepanel id="upReviews" runat="server">
			<contenttemplate>
				<div class="col2" runat="server" id="divCol2" style="margin-top:0;" clientidmode="static">
					<div class="topTabSelect">
						<ul>
							<li class="selected"><a href="javascript:void(0);" onclick="ToggleTabs('itemDetail', this)" class="tab_itemDetail">Details</a></li>
							<li class="centerTop"><!-- between the columns --></li>
							<li><a href="javascript:void(0);" onclick="ToggleTabs('userReview', this)" class="tab_itemReview">Reviews (<asp:Label ID="lblNumberOfReviews1" runat="server" />)</a></li>
						</ul>
					</div>
					<div id="itemDetail">
						<table>
							<tr>
								<td>Sale Type:</td>
								<td><asp:Label runat="server" ID="lblSaleType"></asp:Label></td>
							</tr>
						</table>
						<table>
							<tr>
								<td>Item History:</td>
								<td>Raves:
									<asp:Label runat="server" ID="lblRaves"></asp:Label>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>Uploaded:
									<asp:Label runat="server" ID="lblDate"></asp:Label></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td><a runat="server" id="aAdminEdit" visible="false">Admin Edit</a></td>
							</tr>
						</table>
						<h3>Created by:</h3>
						<div class="userImg floatLeft"><img id="imgOwner" runat="server" src="./images/blackdryer_40x40.jpg" alt="" /></div>
						<div class="user floatLeft">
							<a id="aUser" runat="server" href="#">
								<asp:Label runat="server" ID="lblUsername"></asp:Label></a></div>
						<div class="clearit"></div>
						<div class="sponsorLink"></div>
						<asp:Panel runat="server" ID="pnlFirstReview" Visible="false">
							<h3>Latest Reviews:</h3>
							<div class="userImg floatLeft"><img runat="server" id="imgFirstReviewImage" src="" alt="" class="floatLImg" /></div>
							<div class="user floatLeft">
								<a id="aFirstReview" runat="server" href="#">
									<asp:Label runat="server" ID="lblFirstReviewUsername"></asp:Label></a></div>
							<div class="clearit"></div>
							<p><asp:Label runat="server" ID="lblFirstReviewComment"></asp:Label></p>
						</asp:Panel>
					</div>
					<!-- start the hidden User Review column -->
					<div id="userReview">
						<!-- Add server functionality to change the display atribute from none to block if there are no reviews -->
						<asp:Literal runat="server" ID="litNoReviews" Visible="False">
							<div id="noReview">Be the first to review this item.</div>
						</asp:Literal>
						<h3>Latest Reviews:</h3>
						<asp:Repeater ID="rptReviews" runat="server" onitemdatabound="rptReviews_ItemDataBound">
							<ItemTemplate>
								<!-- Start Review -->
								<div class="userImg floatLeft">
									<img src='<%# GetProfileImageURL (DataBinder.Eval (Container.DataItem, "ThumbnailSmallPath").ToString(), "sm", "M") %>' alt="" class="floatLImg" />
								</div>
								<div class="user floatLeft">
									<a href='<%# GetPersonalChannelUrl (DataBinder.Eval (Container.DataItem, "username").ToString()) %>'>
										<%# DataBinder.Eval (Container.DataItem, "Username") %><br />
                                        <asp:LinkButton id="lbDeleteComment" runat="server" visible="false" text="Delete Comment" 
                                            commandargument='<%# DataBinder.Eval (Container.DataItem, "ReviewId") %>' onclick="lbDeleteReview_OnClick" cssclass="btn_deleteReview"></asp:LinkButton>
									</a>
								</div>
								<div class="clearit"></div>
								<p><%# DataBinder.Eval (Container.DataItem, "Comment") %></p>
								<!-- End Review -->
							</ItemTemplate>
						</asp:Repeater>
					</div>
					<!-- start the hidden Add Reviews column -->
					<div id="addReview">
						<div class="addForm">
							<h3>Tell us what you think:</h3>
							<div class="note"><span id="spnRemain">256</span> characters remaining</div>
							<div class="clearit"></div>
							<textarea runat="server" id="txtComment" rows="5" style="width: 285px" name="txtComment"
								onkeyup="CharsRemain(this,'spnRemain')" maxlength="140"></textarea>
							<asp:Button ID="lbSubmitComment" runat="Server" OnClick="lbSubmitComment_OnClick"
								OnClientClick="ToggleTabs('userReview')" Text=" Submit "></asp:Button>
						</div>
					</div>
					<div class="tabSelect">
						<ul>
							<li><a href="javascript:void(0);" onclick="ToggleTabs('addReview', this)" class="btn_addReview" id="btn_addReview" runat="server">Write a Review</a></li>
							<li class="centerBtm"><!-- between the columns --></li>
							<li><a href="javascript:void(0);" onclick="ToggleTabs('userReview', this)" class="btn_seeReview">Read More Reviews</a></li>
						</ul>
					</div>
					<div id="divOwnerBtns" runat="server" visible="false" class="ownerBtn" style="clear:both;">
						<ul>
							<li><a runat="server" id="aOwnerEdit" visible="false" class="btnEdit">Edit Item</a></li>
							<li><a id="aTransTracker" runat="server" class="btnTracker">View My Sales</a></li>
						</ul>
					</div>
					<div class="clearit"></div>
					<div id="divAnimationContainer" runat="server" visible="false" style="margin-top:20px;">
						<span style="font-weight:bold;">Animations for this item:</span>
						<div id="divAnimations" runat="server"></div>
					</div>
					<div class="clearit"></div>
                    
                    <div id="divDeedItems" class="bundleItems" runat="server" visible="false">
	                    <div id="divDeedListTitle" runat="server" style="font-weight:bold;">Included in this Pre Built World:</div>
	                    <div class="biContainer">

                            <div id="divDeedItemsData"><img id="Img4" class="imgLoading" runat="server" src="~/images/ajax-loader.gif" alt="Loading..." border="0"/><div class="loadingText">Loading...</div></div> 

                    	</div>
                    </div>

					<div id="divBundleItems" class="bundleItems" runat="server" visible="false">
						<div id="divBundleListTitle" runat="server" style="font-weight:bold;"></div>
						<div class="biContainer">
						<asp:repeater id="rptBundleItems" runat="server" >
							<itemtemplate>
								<div class="biRow">
									<img src='<%# GetItemImageURL (DataBinder.Eval (Container.DataItem, "ThumbnailSmallPath").ToString(), "sm")%>' />
									<a href='ItemDetails.aspx?gId=<%# DataBinder.Eval (Container.DataItem, "GlobalId") %>'><%# DataBinder.Eval (Container.DataItem, "DisplayName") %></a>
									<%# ShowBundleItemQuantity (DataBinder.Eval (Container.DataItem, "Quantity"), DataBinder.Eval (Container.DataItem, "UseType"))%>	
								</div>
							</itemtemplate>
						</asp:repeater>
						</div>
					</div>
				</div>
			</contenttemplate>
		</asp:updatepanel>
		
		<asp:updatepanel id="upThirdColumn" runat="server">
			<contenttemplate>

				<asp:Panel ID="pnlRelated" runat="server" clientidmode="static">
					<div id="related">
						<h3 style="margin-top:0;text-align:left;">Related Items</h3>
						<ul id="browse">
							<asp:Repeater ID="rptItems" runat="server">
								<ItemTemplate>
									<li style='<%# GetStyleImage (DataBinder.Eval (Container.DataItem, "ThumbnailLargePath").ToString (), "la")%>'>
										<a href='<%# GetItemDetailsURL (DataBinder.Eval (Container.DataItem, "GlobalId").ToString ())%>'>
											<%# DataBinder.Eval (Container.DataItem, "Name") %>
											<div id="Div2" class="animation" runat="server" visible='<%# Convert.ToBoolean ( DataBinder.Eval(Container.DataItem, "IsAnimated")) %>'><!-- empty --></div>
											<span class="price"><%# DataBinder.Eval(Container.DataItem, "WebPrice")%></span></a>
									</li>
								</ItemTemplate>
							</asp:Repeater>
						</ul>
                        <asp:linkbutton id="lbGetForFree" OnClientClick="GMPurchase();" onclick="lbGetForFree_OnClick" class="adminfree" runat="server" visible="false" text="Admin Get For Free >>"></asp:linkbutton>	
					</div>
				</asp:Panel>

				<asp:Panel ID="pnlBuy" runat="server" Visible="False" clientidmode="static">
					<div id="divBuy" clientidmode="static" class="purchasenew" runat="server">
						<div class="boxTop"></div>
						<div class="boxMiddle">
							<h3>Purchase Summary</h3><div class="chkbox"><label><asp:CheckBox runat="server" ID="chkBlastPurchase" />Blast This Purchase</label></div> 
							<hr />
							<div class="divBorder margintop">
								<div class="boxTopWhite"></div>
								<div class="boxMiddleWhite desc">
									<table callpadding="0" cellspacing="0">
										<tr>
											<td class="bold">Item</td>
											<td class="tdWidth price">Price</td>
											<td class="bold tdWidth hide">Qty</td>
										</tr>
										<tr>
											<td><asp:Label ID="lblItemNamePurchase" runat="server"></asp:Label></td>
											<td class="tdWidth"><asp:Label ID="lblPriceCheckout" runat="server" class="price"></asp:Label></td>
											<td class="tdWidth hide">
												<asp:TextBox ID="txtQuantity" MaxLength="3" OnTextChanged="txtQuantity_TextChanged"
													AutoPostBack="true" runat="server" Width="20px"></asp:TextBox>
											</td>
										</tr>
									</table>
								</div>
								<div class="boxBottomWhite"></div>
							</div>
							<div class="divBorder margintop">
								<div class="boxTopWhite"></div>
								<div class="boxMiddleWhite">
									<table class="balances" id="tblBalances">
										<tr>
											<td class="bold rule label"><span id="spnCreditBalance" runat="server" clientidmode="static">Credit Balance: <asp:Label ID="lblCreditsCheckout" runat="server" Text="0"></asp:Label><div class="purchase_msg alert" id="divCreditsMsg" runat="server"></div></span></td>
											<td class="rule"><asp:LinkButton ID="lbComplete" runat="Server" OnClientClick="ProcessOrder();_gaq.push(['_trackPageview','/clickPurchaseItemCredits.aspx']);"
											OnClick="lbCompleteCredits_OnClick" CssClass="btn_buy right buywithbtn" Text="Buy with Credits"></asp:LinkButton>
											<asp:LinkButton ID="lbGetCredits" visible="false" runat="Server" CssClass="btn_buy right buywithbtn" Text="Get Credits"></asp:LinkButton>
                                            <span id="spnCreditsMsg" runat="server"></span></td>
										</tr>
										<tr class="bold">
											<td class="label"><span id="spnRewardBalance" runat="server">Reward Balance: <asp:Label ID="lblRewards" runat="server" Text="0"></asp:Label><div class="purchase_msg" id="divRewardsMsg" runat="server"></div></span></td>
											<td class="tdWidth"><asp:LinkButton ID="lbCompleteRewards" runat="Server" OnClientClick="ProcessOrder();_gaq.push(['_trackPageview','/clickPurchaseItemRewards.aspx']);"
											OnClick="lbCompleteRewards_OnClick" CssClass="btn_buy right buywithbtn" Text="Buy with Rewards"></asp:LinkButton>
                                                <span id="spnRewardsMsg" runat="server"></span></td>
										</tr>
									</table>
								
									<div id="divProcessing"><div>Processing...</div></div>
									<div id="divComplete" style="display:none;">
									
									</div>
								</div>
								<div class="boxBottomWhite"></div>
							</div>
							<!-- New HTML: ad the id to the atag and removed the class button1 -->
							<div class="aleignButton" id="divCancel">
							</div>
						</div>
						<div class="boxBottom"></div>
					</div>
				</asp:Panel>

				<asp:Panel ID="pnlZoneSelect" runat="server" Visible="false">	
					<div id="divZoneSelect" class="purchasenew" runat="server">
						<div class="boxTop"></div>
						<div class="boxMiddle zonesmiddle">
							<h3>Apply Pre Built World To</h3>
							<hr />	 
							<div class="divBorder zoneselect">
								<a href="javascript:void(0);" onclick="showzone(this.id);" id="aNew3DWorld" name="zonesection" class="selected" runat="server" clientidmode="static">
								<input checked="true" type="radio" class="nopad" runat="server" id="radNew3DApp" name="rdoCommType" value="new3dapp" clientidmode="static" />
								<span class="radiolabel">New World</span></a>
								<div id="radNew3DAppMsg" style="display:block;" class="blank" name="zonemsg" runat="server" clientidmode="static">
									<div class="indent">
										<asp:TextBox id="txt3DAppName" runat="server" width="200px" cssclass="inputText" onmouseup="$j(this).addClass('clicked');" maxlength="50" text=""></asp:TextBox>
										<div class="inlineerr" id="divNewWorldTxtBoxErr" runat="server"></div>
									</div>
									<div class="selection">
										<hr class="dotted" style="display:none;" />
										<div class="indent" style="display:none;">
											<p>Select Template:</p>
											<asp:radiobuttonlist id="rblTemplates" runat="server">
												<asp:listitem value="3"> <img id="Img5" src="images/purchase/graphic_52x52_communityTemplate.png" width="52" height="52" /> Community Template</asp:listitem>
											</asp:radiobuttonlist>
										</div>
										<div style="display:none;">
											<span id="rbCommTemplateMsg"></span>
											<span id="rbAdvTemplateMsg"><p class="bold">Note:</p><p>Find this World in your <span class="bold">Import World Menu</span>.</p></span>
											<span id="rbTreasureTemplateMsg"><p class="bold">Note:</p><p>Find this World in your <span class="bold">Import World Menu</span>.</p></span>
										</div>
										<div id="templatemsg" class="templatemsg" style="display:none;"></div>
									</div>
								</div>
 
								<a href="javascript:void(0);" onclick="showzone(this.id);" id="aExistingWorld" name="zonesection" runat="server" clientidmode="static">
								<input type="radio" runat="server" id="radExisting" name="rdoCommType" value="homeComm" clientidmode="static" />
								<span class="radiolabel">Existing World</span></a>
								<div id="radExistingMsg" style="display:none;" name="zonemsg" class="blank" runat="server" clientidmode="static">
									<div class="indent">
										<asp:dropdownlist id="ddlWorlds" clientidmode="Static" runat="server" width="200px" cssclass="worldlist" onchange="showMsg();" ></asp:dropdownlist>
										<div class="inlineerr existing" id="divExistingWorldError" runat="server"></div>
									</div>	
									<div class="selection">
										<div id="existingMsgbox" class="homemsg" style="display:none;">
											<p class="bold">Note:</p> Find this World in your <span class="bold">Import World Menu</span>.</p>
										</div>
									</div>
								</div>

								<a href="javascript:void(0);" onclick="showzone(this.id);" id="aImportZone" name="zonesection" runat="server" clientidmode="static">
								<input type="radio" runat="server" id="radImport" name="rdoCommType" value="importList" clientidmode="static" />
								<span class="radiolabel">Import World List</span></a>
								<div id="radImportMsg" style="display:none;" name="zonemsg" class="blank" runat="server" clientidmode="static">
									<div class="selection">
										<div class="homemsg">
											<p class="bold ">Note:</p> 
											Find this World in your <span class="bold">Import World Menu.</span>
										</div>
									</div>
								</div>
 
								<span style="display:none;">
								<input type="radio" runat="server" onclick="javascript:$('divNewComm').style.display='inline';$('divHomeWarning').style.display='none';$('divCommWarning').style.display='none';$('divNew3Dapp').style.display='none';" id="radNew" name="rdoCommType" value="newComm" />
								<div id="divNewComm" style="display:none;">
									<asp:TextBox id="txtName" runat="server" onfocus="if(this.value=='Enter Name Here')this.value='';this.className='inputText';"></asp:TextBox>
								</div>
								</span> 
							</div>
							<div class="buttonalign">
								<asp:LinkButton ID="lnkZoneNext" runat="Server" onclick="btnChooseCommNext_OnClick" CssClass="btn_cancel" Text=" Next "></asp:LinkButton>
                                <asp:LinkButton ID="lbCompleteCommunityGetForFree" runat="Server" onclick="lbCompleteCommunityGetForFree_OnClick" CssClass="adminfree" Text="Admin Get For Free >>" style="display:none;"></asp:LinkButton>
							</div>		   
						</div>
						<div class="boxBottom"></div>
						<p class="zonecopy">A copy of this World can be found in your <span class="bold">Import World Menu.</span></p>
					</div>
				</asp:Panel>

				<asp:Panel ID="pnlComplete" runat="server" Visible="False" clientidmode="static">
					<div id="divComplete" class="purchasenew">
						<div class="boxTop"></div>
						<div class="boxMiddle">
							<h3>Purchase Summary</h3> 
							<hr />
							<div class="divBorder margintop">
								<div class="boxTopWhite"></div>
								<div class="boxMiddleWhite desc">
									<p>Thank You!</p>
                                    <asp:Literal ID="litSuccessMessage" runat="server" />
									<img src="images/purchase/findininventory.jpg" />
									<div class="btncontainer">
										<div class="holder">
										<a id="lbContinueShopping" clientidmode="static" CssClass="" href="~/" runat="server" OnClientClick="_gaq.push(['_trackPageview','/clickPurchaseItemDone.aspx'])">Continue Shopping</a>
										<div>OR</div>
										<a href="javascript:void(0);" runat="server" id="lbPlayNow_Complete" onclick="pn_js();" class="playnow"></a>
										</div>
									</div>
								</div>
								<div class="boxBottomWhite"></div>
								<div class="aleignButton" id="div1"></div>
							</div>
						</div>
						<div class="boxBottom"></div>
					
					</div>
				</asp:Panel>

				<asp:Panel ID="pnlCompleteZonePurchase" runat="server" Visible="False" clientidmode="static">
					<div id="divCompleteZone" clientidmode="static" class="purchasenew" runat="server">
						<div class="boxTop"></div>
						<div class="boxMiddle">
							<h3>Thank You</h3>
							<hr />
							<div class="divBorder margintop">
								<div class="boxTopWhite"></div>
								<div class="boxMiddleWhite">
									<p>Your purchase is complete.</p>
									<div class="btncontainer">
										<a href="~/" runat="server" >Continue Shopping</a>
										<a href="javascript:void(0);" runat="server" id="aPlayNow" onclick="pn_js();" class="playnow"></a>
									</div>
								</div>
								<div class="boxBottomWhite"></div>
								<div class="aleignButton"></div>
							</div>	
						</div>
						<div class="boxBottom"></div>
					</div>
				</asp:Panel>

			</contenttemplate>
		</asp:updatepanel>
	</div>
 
	<style>
		.ui-dialog {background-color:transparent;padding:0;background-image:url(images/purchase/graphic_367x185_popUpWindow.png);z-index:2;}
		.ui-widget {border:none;}
		.ui-dialog-titlebar	{color:#fff;background-color:transparent;background-image:none;border:none;}
		.ui-dialog-titlebar .ui-dialog-titlebar-close {background-image:url(images/buttons/closeoutButton.gif);right:0.6em;top:16px;}
		.ui-dialog .ui-dialog-buttonpane {background-color:transparent;text-align:center;padding:0 0 26px 0;border:none;}
		.ui-dialog .ui-dialog-buttonpane .ui-dialog-buttonset {float:none;}
		.ui-dialog-buttonset {display:block;width:298px;margin:auto;padding:0;margin-top:0;text-align:center;}
		.ui-dialog-buttonset button.dialogButton {background-image:url(images/btn_purchase.jpg);border:none;font-size:14px;line-height:2em;color:#fff;text-decoration:none;background-color:transparent;display:block;text-align:center;height:30px;width:144px;margin-right:auto;margin-left:auto;margin-top:0;}
		.ui-dialog-buttonset button.okButton {float:left;margin-right:10px;}
		.ui-dialog-buttonset button.okButtonCenter {float:none;margin-right:auto;}
		.ui-dialog .ui-dialog-content {padding-top:.2em;padding-left:2em;padding-right:2em;height:65px !important;margin-top:20px;overflow:visible;}
		.ui-dialog-content p {padding:0;margin:0;}
		.ui-dialog-content p.title {font-weight:bold;margin:0;}
		.ui-dialog-content p.desc {font-size:14px;margin-top:6px;text-align:center;}
		.ui-widget-overlay {Alpha(Opacity=50);opacity:0.5;background-color:rgb(0,0,0);background-image:none;height:100%;width:100%;}
	    .ui-dialog img:focus, .ui-dialog a:focus {border:none;}
	
		.addthis_button_tweet iframe {padding-left:0px;} 
        .addthis_button_facebook_like {position:relative;left:40px;}
	</style>

</asp:Content>
