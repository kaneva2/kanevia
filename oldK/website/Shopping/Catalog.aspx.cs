///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using KlausEnt.KEP.Kaneva;
using Kaneva.PresentationLayer.Shopping.usercontrols;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
    public class Catalog : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Title = "Shop Kaneva";
            string strCatagory = "";
            string searchBoxText = "All";

            if (!IsPostBack)
            {
                // Did we pass a sort from a page that links here?
                if (Request["setcs"] != null)
                {
                    SetOrderByValue(Request["setcs"].ToString());
                }
                else
                {
                    // No sort specified, so if there is a search term do relavance, otherwise default is raves.
                    if (Request["s"] != null && Request["s"].Trim ().Length > 0)
                    {
                        SetOrderByValue("");
                    }
                    else
                    {
                        SetOrderByValue(SEARCH_ORDERBY_BEST_SELLING_30DAYS);
                    }
                }

                //set the access pass filter toggle disable and invisible then process
                cbx_AccessPass.Enabled = li_accessPassFilter.Visible = false;

                if(Request.IsAuthenticated && KanevaWebGlobals.CurrentUser.IsAdult)
                {
                    //set the Access Pass link value
                    lnk_AccesPass.HRef = GetAccessPassLink();

                    //disable/enable show access pass only based on users access pass status
                    cbx_AccessPass.Enabled = KanevaWebGlobals.CurrentUser.HasAccessPass;
                    li_accessPassFilter.Visible = true;
                }

				if (IsAdministrator())
				{
					hlShowPriv.Visible = true;
					hlShowTextures.Visible = true;
					if (Request.Url.PathAndQuery.IndexOf('?') >= 0)
					{
						if (!Request.Url.PathAndQuery.Contains("&priv=1"))
						{
							hlShowPriv.NavigateUrl = Request.Url.PathAndQuery + "&priv=1";
						}
						if (!Request.Url.PathAndQuery.Contains("&text=1"))
						{
							hlShowTextures.NavigateUrl = Request.Url.PathAndQuery + "&text=1";
						}
					}
					else
					{
						hlShowPriv.NavigateUrl = Request.Url.PathAndQuery + "?priv=1";
						hlShowTextures.NavigateUrl = Request.Url.PathAndQuery + "?text=1";
					}
					
				}

                //bind the CatalogItems
                BindItemData (1);

                // Build the header
                if (CategoryIds[0] > 0)
                {
                    ItemCategory itemCategory; 
                    ItemCategory itemCategory2 = new ItemCategory();
                    UInt32 categoryId = CategoryIds[0];
                    
                    while (categoryId > 0)
                    {
 //                       itemCategory = GetShoppingFacade.GetItemCategory(categoryId);
                        itemCategory = GetShoppingFacade.GetItemCategory (categoryId);

                        if (CategoryIds.Count > 1 && CategoryIds[1] > 0)
                        {
                            itemCategory2 = GetShoppingFacade.GetItemCategory (CategoryIds[1]);
                        }

                        // Populate it with the category passed in first time
                        if (searchBoxText.Equals ("All"))
                        {
                            searchBoxText = itemCategory.Name + (itemCategory2.ItemCategoryId > 0 ? " & " + itemCategory2.Name : "");
                        }

                        if (strCatagory.Length > 0)
                        {
                            strCatagory = itemCategory.Description + " > " + strCatagory;
                        }
                        else
                        {
                            strCatagory = itemCategory.Description + (itemCategory2.ItemCategoryId > 0 ? " & " + itemCategory2.Description : "");
                            strCatagory = strCatagory.Replace ("Accessories For", "For");
                        }

                        // Show clothing links?
                        if (categoryId.Equals(3))
                        {
                            pnlUGC.Visible = true;
                        }

                        categoryId = itemCategory.ParentCategoryId == 9999 ? 0 : itemCategory.ParentCategoryId;
                    }

                    // Had to add this code to display animated category since animations
                    // are hanled differently and are not their own category
                    if (isAnimation)
                    {
                        strCatagory += " > " + "Animated";
                    }
                }

                // Set text in search bar
                ((MainTemplatePage)Master).SearchTitle = strCatagory;
                ((MainTemplatePage)Master).SearchBoxText = searchBoxText;
            }
        }

        #region Helper Methods

        /// <summary>
        /// BindItemData
        /// </summary>
        private void BindItemData (int currentPage)
        {
            int pgSize = 24;
            string searchString = "";
            int showPrivate = 0;
            //int showTexture = 0;

            bool showRestricted = false;

            // Set current page
            pgTop.CurrentPageNumber = currentPage;

            try
            {
                if (CategoryIds[0] > 0)
                {
                    if (WebCache.GetCachedUploadType (CategoryIds[0]) == (int)UGCUpload.eType.SND_OGG_KRX)
                    {
                        litSoundStyle.Text = "<style>ul#browse li a{ text-indent: 0px; background: #ffffff; filter:alpha(opacity=85); " +
                            "-moz-opacity:0.85; -khtml-opacity: 0.85; opacity: 0.85; }</style>";
                    }
                }
            }
            catch { }

            if (Request["s"] != null)
            {
                searchString = Request["s"].ToString ();
            }

            if (Request["priv"] != null)
            {
                if (IsAdministrator ())
                {
                    showPrivate = 1;
                }
            }

            int ownerId = 0;

            // Filter out UGC?
            if (Request["kbill"] != null)
            {
                if (Request["kbill"].Equals ("1"))
                {
                    ownerId = -1;
                }
            }

            try
            {
                showRestricted = UserHasMaturePass ();
            }
            catch (Exception) { }

            // User selected animation sug-category
            if (Request["anim"] != null)
            {
                try
                {
                    isAnimation = (Convert.ToInt32 (Request["anim"])).Equals (1);
                }
                catch (Exception) { };
            }
                                                                              
            //   if (categoryId == (int)ItemCategory.GameCategories.
            PagedList<WOKItem> plWokItems = GetShoppingFacade.SearchItems (searchString, ownerId, CategoryIds.ToArray(), pgSize, currentPage, CurrentSort, showRestricted, ShowOnlyAccessPass, showPrivate, isAnimation);

            if (plWokItems.Count.Equals (0))
            {
                lbl_NoCatalogItems.Visible = true;
                litFiller.Text = "<li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>" +
                    "<li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>";
            }
            else
            {
                lbl_NoCatalogItems.Visible = false;

                rptItems.DataSource = plWokItems;
                rptItems.DataBind ();

                StringBuilder sbFiller = new StringBuilder (200);

                for (int i = plWokItems.Count; i < 24; i++)
                {
                    sbFiller.Append ("<li></li>");
                }

                litFiller.Text = sbFiller.ToString ();
            }

            pgTop.NumberOfPages = Math.Ceiling ((double) plWokItems.TotalCount / pgSize).ToString ();
            pgTop.DrawControl ();
        }

        protected bool BelongsToSmartObjectCategory (WOKItem item)
        {
            int ult1 = WebCache.GetCachedUploadType(item.Category1);
            int ult2 = WebCache.GetCachedUploadType(item.Category2);
            int ult3 = WebCache.GetCachedUploadType(item.Category3);

            return (ult1 == (int)UGCUpload.eType.ACTIONITEM || ult2 == (int)UGCUpload.eType.ACTIONITEM || ult3 == (int)UGCUpload.eType.ACTIONITEM);
        }

        /// <summary>
        /// Show Restricted Symbol
        /// </summary>
        protected bool IsAccessPass(int PassTypeId)
        {
            return (PassTypeId.Equals((int)Constants.ePASS_TYPE.ACCESS));
        }

        /// <summary>
        /// SetOrderByValue
        /// </summary>
        private string SetOrderByValue(string cs)
        {
            // How they are sorted
            if (cs.Equals(SEARCH_ORDERBY_NEWEST))
            {
                // Most Relevant
                CurrentSort = "date_added DESC";
            }
            else if (cs.Equals(SEARCH_ORDERBY_OLDEST))
            {
                CurrentSort = "global_id ASC";
            }
            else if (cs.Equals(SEARCH_ORDERBY_BEST_SELLING_30DAYS))
            {
                CurrentSort = "qty_sold_recent DESC, number_sold_on_web DESC";
            }
            else if (cs.Equals(SEARCH_ORDERBY_BEST_SELLING))
            {
                CurrentSort = "number_sold_on_web DESC";
            }
            else if (cs.Equals(SEARCH_ORDERBY_RAVED))
            {
                CurrentSort = "number_of_raves DESC";
            }
            else if (cs.Equals(SEARCH_ORDERBY_RAVED_30DAYS))
            {
                CurrentSort = "raved_recent DESC, number_of_raves DESC";
            }
            else if (cs.Equals(SEARCH_ORDERBY_PRICE_HIGH))
            {
                CurrentSort = "web_price DESC";
            }
            else if (cs.Equals(SEARCH_ORDERBY_PRICE_LOW))
            {
                CurrentSort = "web_price ASC";
            }
            else // default to relevance?
            {
                CurrentSort = "";
            }

            // Set sort and order by
            return CurrentSort;
        }

        #endregion Helper Methods


        #region Event Handlers

        private void pg_PageChange (object sender, PageChangeEventArgs e)
        {
            BindItemData (e.PageNumber);
        }

        protected void APCheckChanged (object sender, EventArgs e)
        {
            ShowOnlyAccessPass = cbx_AccessPass.Checked;
            BindItemData (1);
        }

        /// <summary>
        /// Execute when the user clicks the sort button
        /// </summary>
        protected void lbSort_Click (object sender, CommandEventArgs e)
        {
            SetOrderByValue (e.CommandName);
            BindItemData (1);
        }
        
        #endregion Event Handlers


        #region Properties

        /// <summary>
        /// Current sort expression
        /// </summary>
        public string CurrentSort
        {
            get
            {
                if (ViewState["cs"] == null)
                {
                    return DEFAULT_SORT + " " + DEFAULT_SORT_ORDER;
                }
                else
                {
                    return ViewState["cs"].ToString ();
                }
            }
            set
            {
                ViewState["cs"] = value;
            }
        }

        /// <summary>
        /// Current sort order
        /// </summary>
        public string CurrentSortOrder
        {
            get
            {
                if (ViewState["cso"] == null)
                {
                    return DEFAULT_SORT_ORDER;
                }
                else
                {
                    return ViewState["cso"].ToString ();
                }
            }
            set
            {
                ViewState["cso"] = value;
            }
        }

        /// <summary>
        /// indicator to show only access pass or not
        /// </summary>
        private bool ShowOnlyAccessPass
        {
            get
            {
                if (ViewState["showOnlyAP"] == null)
                {
                    return false;
                }
                else
                {
                    return (bool) ViewState["showOnlyAP"];
                }
            }
            set
            {
                ViewState["showOnlyAP"] = value;
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected string DEFAULT_SORT
        {
            get
            {
                return "number_of_raves";
            }
        }

        /// <summary>
        /// DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        protected string DEFAULT_SORT_ORDER
        {
            get
            {
                return "DESC";
            }
        }

        private List<uint> CategoryIds
        {
            get
            {
                if (listCategoryIds.Count < 1)
                {
                    if (Request["cId"] != null)
                    {
                        try
                        {
                            string[] ids = Request["cId"].ToString ().Split ('|');
                            for (int i = 0; i < ids.Length; i++)
                            {
                                listCategoryIds.Add (uint.Parse (ids[i]));
                            }
                        }
                        catch (Exception)
                        {
                            listCategoryIds.Clear ();
                            listCategoryIds.Add (0);
                        };
                    }
                    else
                    {
                        listCategoryIds.Add (0);
                    }
                }

                return listCategoryIds;
            }

            set { listCategoryIds = value; }
        }

        #endregion Properties


        #region Declerations

        private List<uint> listCategoryIds = new List<uint> ();
        private bool isAnimation = false;

        protected Repeater rptItems;
        protected GridView dg_CatalogItems;
        protected Label lbl_NoCatalogItems;
		protected Literal litFiller, litSoundStyle;
        protected HtmlAnchor lnk_AccesPass;
        protected CheckBox cbx_AccessPass;
        protected HtmlGenericControl li_accessPassFilter;
		protected HyperLink hlShowPriv, hlShowTextures;
        protected Panel pnlUGC;

        protected usercontrols.Pager pgTop;

        private const string SEARCH_ORDERBY_NEWEST = "n";
        private const string SEARCH_ORDERBY_OLDEST = "o";
        private const string SEARCH_ORDERBY_BEST_SELLING = "b";
        private const string SEARCH_ORDERBY_BEST_SELLING_30DAYS = "b30";
        private const string SEARCH_ORDERBY_RAVED = "r";
        private const string SEARCH_ORDERBY_RAVED_30DAYS = "r30";
        private const string SEARCH_ORDERBY_PRICE_HIGH = "ph";
        private const string SEARCH_ORDERBY_PRICE_LOW = "pl";

        #endregion Declerations


        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
        }
		#endregion
    }
}
