<%@ page language="C#" masterpagefile="~/masterpages/MainTemplatePage.Master" autoeventwireup="true" codebehind="ItemEdit.aspx.cs" inherits="Kaneva.PresentationLayer.Shopping.ItemEdit" %>

<asp:Content contentplaceholderid="cphHeaderCSS" runat="server">
    <link href="./jscript/colorbox/colorbox.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:content id="cntUpload" runat="server" contentplaceholderid="cphBody">
    <asp:validationsummary showmessagebox="False" showsummary="True" id="valSum" class="errSum" displaymode="BulletList" runat="server" headertext="Please correct the following errors:" />
	<asp:customvalidator id="cvGeneral" runat="server" visible="false"></asp:customvalidator>
	<div id="divItemLocked" runat="server" class="errSumCenter locked" visible="false">This item is Locked. You will not be able to modify this item in any way
		as long as it is Locked.</div>
	<div id="content">
		<div class="colWidth">
			<div class="formStyle">
				<h3>Basic Information</h3>
				<div class="itemName">
					<label class="required">Item Name:*</label><br />
					<asp:requiredfieldvalidator id="rfvTitle" runat="server" controltovalidate="txtTitle" class="validator" text="*" errormessage="Item Name is a required field." display="static"></asp:requiredfieldvalidator>
					<asp:textbox id="txtTitle" runat="server" width="260px" cssclass="inputText" maxlength="125"></asp:textbox>
				</div>
                <div id="thumbnailEditContainer" class="required">
                    <label for="thumbnail_image">Thumbnail Image:</label>
                    <asp:RegularExpressionValidator id="revThumbnail" class="validator1" runat="server"
                        text="*" errormessage="Thumbnail Image must be a .jpg, .jpeg, or .gif file type."
                        validationexpression="^.+\.([jJ][pP][gG]|[gG][iI][fF]|[jJ][pP][eE][gG])$" controltovalidate="inpThumbnail"
                        display="static"></asp:RegularExpressionValidator>
                    <br />
                    <img runat="server" id="imgItem" src="" alt="" clientidmode="Static" />                 
                    <div id="thumbnailEditButtons">
                        <input runat="server" id="inpThumbnail" type="file" size="15" clientidmode="Static" />
                        <input type="button" class="popFacebox photoBtn" value="Upload Image" />
                        <span class="formNote" id="spnUploadMsg">Upload a .jpg image from your computer.</span>
                        <input type="button" name="btnMediaLibrary" id="btnMediaLibrary" class="popFacebox photoBtn" value="Choose Image" />
                        <span class="formNote">Choose an image from your Media Library.</span>
                        <asp:HiddenField id="hfMediaLibPhoto" runat="server" clientidmode="Static" />
                    </div>
                </div>
				<div class="formBox clearit">
					<label>Description:*</label><br />
					<asp:requiredfieldvalidator id="rfvDesc" runat="server" controltovalidate="txtDescription" text="*" class="validator" errormessage="Description is a required field." display="static"></asp:requiredfieldvalidator>
					<asp:textbox id="txtDescription" runat="server" maxlength="300" textmode="MultiLine" rows="3" columns="20"></asp:textbox>
				</div>
				<div>
					<label>Tags:</label><br />
					<asp:customvalidator id="cvTags" runat="server" text="*" class="validator" display="static"></asp:customvalidator>
					<asp:textbox id="txtKeywords" maxlength="100" textmode="MultiLine" runat="server"></asp:textbox>
					<span class="formNote">Tags are unique keywords that help people find items on Shop Kaneva. (use spaces between words)</span>
				</div>
				<div id="divCategory" runat="server">
					<label>Category:</label>
					<asp:repeater id="rptCategories" runat="server">
						<itemtemplate><%# DataBinder.Eval(Container.DataItem, "name")%><br /></itemtemplate>
					</asp:repeater>
				</div>
				<div>
					<label>Owner:</label>  <asp:label runat="server" id="lblUsername"></asp:label>
				</div>
                <div>
					<label>Blast:</label>&nbsp;<input runat="server" class="checkBox" id="chkBlastUpload" type="checkbox" />
					<span class="formNote">Blast item details to all your friends to let them know about your new item available for purchase.</span>
				</div>
			</div>
		</div>	
		<div class="colWidth last">
			<div class="formStyle">
				<div>
					<label>Design Commission:</label>
					<asp:rangevalidator id="rvPrice" class="validator1" controltovalidate="txtPrice" minimumvalue="0" maximumvalue="5000000" type="Integer" runat="server" errormessage="Commission must be a numeric value between 0 and 5,000,000." text="*" display="Static"></asp:rangevalidator>
					<asp:textbox id="txtPrice" maxlength="7" width="60px" ontextchanged="txtPrice_TextChanged" cssclass="inputText"  runat="server"></asp:textbox>
					<span class="formNote">Enter the number of Credits you want to earn from the sale of this item.<br />
						Note: The base price and a listing fee will be added to your Design Commission, for the final price of the item.
					</span>
					<span class="formNote" id="spnBundleNote" runat="server">Your Design Commission is not included in the price of items you own.</span>
				</div>
				<div id="divBasePrice" runat="server" visible="false">
					<label>Base Price:</label>&nbsp;<asp:label runat="server" id="lblBasePrice"></asp:label>
					<asp:label runat="server" id="lblPrice" visible="False"></asp:label>
					<span class="formNote">Note: The base price and a listing fee will be added to your Design Commission, for the final price of the item.</span>
				</div>
				<div id="divBundlePrice" runat="server" visible="false">
					<div class="pricing">
						<div class="title">Bundle Pricing:</div>
						<div class="row"><span>Items Total*</span><div id="totalNotOwnedItems" runat="server">0</div></div>
						<div class="row"><span>Items You Own + 10% catalog fee</span><div id="totalOwnedItems" runat="server">0</div></div>
						<div class="row"><span>Design Commission + 10% catalog fee</span><div id="totalDesignCommission" runat="server">0</div></div>
						<div class="line"></div>
						<div class="row total"><span>Total Price:</span><div id="totalBundle" runat="server">0</div></div>
						<p>*Does not include items you own</p>
					</div>
				</div>
				<div id="divOnlyCredits" visible="False" runat="server">
					<label>Credits Only:</label>&nbsp;<asp:checkbox runat="server" id="chkOnlyCredits" />
				</div>
				<div>
					<label>Pass Type:</label><br />  
					<div class="passType indent"><asp:dropdownlist id="ddl_PassType" runat="server" causesvalidation="false" enabled="false" width="150px" /></div>
				</div>
				<span id="spnAccessability" runat="server">
				<div class="required">
					<label class="publicLabel">Public:</label>
					&nbsp;<input runat="server" id="rdoPublic" name="rdoPerm" type="radio" class="checkBox" checked="true" />
					<span class="formNote">Items marked as Public can be viewed, shared, and bought by other Kaneva members.</span>
				</div>
				<div class="required">
					<label class="privateLabel">Private:</label>
					&nbsp;<input runat="server" type="radio" id="rdoPrivate" name="rdoPerm" class="checkBox" />
					<span class="formNote">Items marked as Private can only be viewed by you when you&#8217;re signed in, and cannot be bought by other Kaneva members.</span>
				</div>
				</span>
				<div id="divBaseItemInfo" runat="server">
					<label>Based on:</label>&nbsp;<asp:hyperlink runat="server" id="lnkBaseItem"></asp:hyperlink>
					<span class="formNote">(Base Commission:&nbsp;<asp:label runat="server" id="lblTemplateDesignerCommission"></asp:label>)</span>
				</div>
			</div>
		</div>
		<div class="formBtns clearit">
			<asp:button runat="server" id="bAddItem" onclick="btnUpdate_Click" text="Save Item"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:button runat="server" id="bCancel" causesvalidation="false" onclick="btnCancel_Click" text="Cancel"></asp:button>
		</div>
		<div class="clearit"></div>
	</div>
</asp:content>

<asp:Content contentplaceholderid="cphFooterJS" runat="server">
    <script type="text/javascript" src="./jscript/colorbox/jquery.colorbox-min.js"></script>
    <script type="text/javascript">
        var $j = jQuery.noConflict();

        $j(document).ready(function () {
            $j("#btnMediaLibrary").colorbox({ iframe: true, fastIframe: false, innerWidth: 566, innerHeight: 508, scrolling: false, overlayClose: false, closeButton: true, href: "MediaLibraryData.aspx", close: "&#10005;", closeTop: true });

            $j('#inpThumbnail').change(function () {
                PreviewThumbnailUpload("inpThumbnail", "spnUploadMsg", "imgItem");
            });
        });
    </script>
</asp:Content>