<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="true"
	 Codebehind="UploadMemberDesigns.aspx.cs" Inherits="Kaneva.PresentationLayer.Shopping.UploadMemberDesigns" %>

<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<asp:Content ID="cntUpload" runat="server" ContentPlaceHolderID="cphBody">

	 <script src="./jscript/prototype.js" type="text/javascript" language="javascript"></script>

	 <script type="text/javascript"><!--
function ShowUploadProgress()
{
    ShowLoading(true,'Please wait for the upload to complete...<br/><img src="./images/progress_bar.gif">');
}

function HideUploadProgress()
{
    ShowLoading(false,'Please wait for the upload to complete...<br/><img src="./images/progress_bar.gif">');
}

//--> </script>

	 <script type="text/javascript" src="../jscript/xEvent.js"></script>

	 <script src="../jscript/balloon.js" type="text/javascript"></script>

	 <script src="../jscript/yahoo-dom-event.js"></script>

	 <script type="text/JavaScript">

		  // white balloon with mostly default configuration
		  // (see http://www.wormbase.org/wiki/index.php/Balloon_Tooltips)
		  var whiteBalloon    = new Balloon;
		  whiteBalloon.balloonTextSize  = '100%';

		  // white ballon with some custom config:
		  var whiteBalloonSans  = new Balloon;
		  whiteBalloonSans.upLeftConnector    = '../images/balloons/balloonbottom_sans.png';
		  whiteBalloonSans.upRightConnector   = '../images/balloons/balloonbottom_sans.png';
		  whiteBalloonSans.downLeftConnector  = '../images/balloons/balloontop_sans.png';
		  whiteBalloonSans.downRightConnector = '../images/balloons/balloontop_sans.png';
		  whiteBalloonSans.upBalloon          = '../images/balloons/balloon_up_top.png';
		  whiteBalloonSans.downBalloon        = '../images/balloons/balloon_down_bottom.png';
		  whiteBalloonSans.paddingConnector = '22px';
	 				   
	 </script>

	 <asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" ID="valSum" DisplayMode="BulletList"
		  runat="server" HeaderText="Please correct the following errors:" />
	 <ajax:AjaxPanel runat="Server" ID="ajaxForm">
		  <div id="content">
			   <div id="popUpWin2">
					<div class="popUpWinMsgTitleBar">
						 <asp:Button runat="server" ID="btnCancelTitleBar" OnClick="btnCancel_Click" Text=""
							  CausesValidation="False"></asp:Button>
						 Does This Violate Our Terms of Service?
					</div>
					<div class="popUpWinMsg">
						 <p>
							  By clicking the button below, you agree the item you are uploading conforms to Kaneva&#8217;s
							  <a href="http://www.kaneva.com/overview/termsandconditions.aspx" target="_blank">Terms of Service</a>, including:</p>
						 <ul>
							  <li>Not infringing the copyright or intellectual property right of any third party</li>
							  <li>Not being indecent, obscene, or otherwise objectionable</li>
						 </ul>
						 <p>
							  Kaneva reserves the right to remove any item that violates these conditions (along
							  with any Credits earned from sales), as well as suspend or delete any member&#8217;s
							  Kaneva account.
							  <br />
							  <br />
							  <asp:Button runat="server" ID="btnConfirm" OnClick="btnConfirm_Click" Text="I Agree With the Terms of Service"
								   OnClientClick="$('popUpWin2').style.display = 'none';ShowUploadProgress();"></asp:Button>
							  &nbsp;&nbsp;&nbsp;<asp:Button runat="server" ID="btnCancel" OnClick="btnCancel_Click"
								   Text="Cancel" CausesValidation="False"></asp:Button>
							  <br />
						 </p>
					</div>
			   </div>
			   <div id="divLoading" style="display: none">
					<table height="300" cellspacing="0" cellpadding="0" width="70%" border="0">
						 <tr>
							  <th class="loadingText" id="divLoadingText">
								   Loading...
								   <div style="display: none">
										<img src="./images/progress_bar.gif"></div>
							  </th>
						 </tr>
					</table>
			   </div>
			   <div id="divData">
					<div class="clearit">
						 <!-- test -->
					</div>
					<div class="colWidth" style="background-color: #DBDBDB; width: 300px; height: 100%">
						 

						 <asp:label runat="server" ID="lblUploadType"></asp:label>

						 <!-- Preview -->						 						 						 
						 <div class="formStyle ">
						 
							  <label for="uploadId">Item Preview</label>
															  
							  <div class="required">
								   <asp:Image ID="imgPreview" Width="200px" Height="200px" runat="server" />
								   <div class="formBox" for="icon">
										<span class="note"></span>
								   </div>
							  </div>
							  <div style="display:none;">
							  <label for="uploadId">Use Uploaded Mesh:</label>
							  
							  <div class="required">
								   <asp:DropDownList ID="drpUGCUploads" runat="server" OnSelectedIndexChanged="drpUGCUploads_SelectedIndexChanged"
										AutoPostBack="true" Width="200px">
								   </asp:DropDownList>
								   <asp:HiddenField ID="hidUploadType" runat="server" />
								   <br />
								   <span class="uploadDate">Uploaded at:
										<asp:Label ID="lblUploadTime" runat="server" Width="130"></asp:Label></span><br />
								   <span class="uploadDate">
										<asp:Label ID="lblUploadSummary" runat="server" Width="130"></asp:Label></span>
							  </div>
							  </div>
						 </div>
						 
						 
						 <!--  Pricing -->
						 <div class="formBox ">
							  <label for="tags">
								   Item Pricing</label>
							  <table cellspacing="1px" cellpadding="0" border="0">
								   <tr>
										<td>
											 Base Price:</td>
										<td align="right">
											 <asp:Label ID="lblBasePrice" runat="server"></asp:Label></td>
								   </tr>
								   <tr runat="server" id="trBaseCommission">
										<td>
											 Template Designer&#39;s Comission:</td>
										<td align="right">
											 <asp:Label ID="lblBaseCommission" runat="server"></asp:Label></td>
								   </tr>
								   <tr>
										<td>
											 Design Comission:</td>
										<td align="right">
											 <asp:Label ID="lblCommission" runat="server"></asp:Label></td>
								   </tr>
								   <tr>
										<td>
											 Catalog Charge:</td>
										<td align="right">
											 <asp:Label ID="lblCatalogCharge" runat="server"></asp:Label></td>
								   </tr>
								   <tr height="1px" bgcolor="#000000">
										<td colspan="2">
										</td>
								   </tr>
								   <tr>
										<td>
											 Total Price:</td>
										<td align="right" class="bold">
											 <asp:Label ID="lblSellingPrice" runat="server"></asp:Label></td>
								   </tr>
							  </table>
							  <br />
							  <span class="">Total selling price = base price (determined by mesh dimension and complexity)
								   + design commission + 10% catalog fee.</span>
						 </div>
					</div>
					<div class="colWidth last" style="width: 382px">
						 <div class="formStyle" style="padding: 0px">
							  *Fields marked with an asterisk (*) are required.
							  <div class="required">
								   <table border="0" width="100%">
										<tr>
											 <td style="text-align: right; vertical-align: top; width: 160px">
												  <img id="Img9" runat="server" src="~/images/icons/toolTip.gif" onmouseover="whiteBalloonSans.showTooltip(event,'<strong>Select a Category</strong><br/>Selecting a category will help others find your item.')"
													   alt="" />
												  <span id="spnNameLabel" runat="server">Item</span> Name:*
											 </td>
											 <td>
												  <asp:TextBox ID="txtTitle" runat="server" Width="100%" CssClass="inputText" MaxLength="125"></asp:TextBox>
												  <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle"
													   Text="*" ErrorMessage="Title is a required field." Display="Dynamic"></asp:RequiredFieldValidator>
											 </td>
										</tr>
								   </table>
							  </div>
							  <div class="required" id="divCategory" runat="server">
								   <table border="0" width="100%">
										<tr>
											 <td style="text-align: right; vertical-align: top; width: 160px">
												  <img id="Img7" runat="server" src="~/images/icons/toolTip.gif" onmouseover="whiteBalloonSans.showTooltip(event,'<strong>Select a Category</strong><br/>Selecting a category will help others find your item.')"
													   alt="" />
												  Select a Category:
											 </td>
											 <td>
												  <asp:DropDownList ID="ddlCategories" runat="server" Width="100%" AutoPostBack="false">
												  </asp:DropDownList>
												  <asp:RequiredFieldValidator ID="rfvCategory" runat="server" InitialValue="-1" ControlToValidate="ddlCategories"
													   Text="*" ErrorMessage="Please select a category." Display="Dynamic"></asp:RequiredFieldValidator>
											 </td>
										</tr>
								   </table>
							  </div>
							  <div class="required">
								   <table border="0" width="100%">
										<tr>
											 <td style="text-align: right; vertical-align: top; width: 160px">
												  Description:*
											 </td>
											 <td>
												  <asp:TextBox ID="txtDescription" runat="server" Width="100%" MaxLength="300" TextMode="MultiLine"
													   Rows="3" Columns="20"></asp:TextBox>
												  <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription"
													   Text="*" ErrorMessage="Description is a required field." Display="Dynamic"></asp:RequiredFieldValidator>
											 </td>
										</tr>
								   </table>
							  </div>
							  <div class="required">
								   <table border="0" width="100%">
										<tr>
											 <td style="text-align: right; vertical-align: top; width: 160px">
												  <img id="Img5" runat="server" src="~/images/icons/toolTip.gif" onmouseover="whiteBalloonSans.showTooltip(event,'<strong>Tags</strong><br/>Tags are unique keywords that help people find items on Shop Kaneva.')"
													   alt="" />
												  Tags:
											 </td>
											 <td>
												  <asp:TextBox ID="txtKeywords" MaxLength="100" Width="100%" TextMode="MultiLine" runat="server"></asp:TextBox>
											 </td>
										</tr>
								   </table>
							  </div>
							  <div class="required">
								   <table border="0" width="100%">
										<tr>
											 <td style="text-align: right; vertical-align: top; width: 160px">
												  <img id="Img6" runat="server" src="~/images/icons/toolTip.gif" onmouseover="whiteBalloonSans.showTooltip(event,'<strong>Availability</strong><br/><br/><strong>Public</strong><br/>Items marked as Public can be viewed, shared, and bought by other Kaneva members.<br/><br/><strong>Private</strong><br/>Items marked as Private can only be viewed by you when you&#8217;re signed in, and cannot be bought by other Kaneva members.')"
													   alt="" />
												  Availability:
											 </td>
											 <td>
												  <div class="required" id="divPublicAccess" runat="server">
													  <input runat="server" id="rdoPublic" name="rdoPerm" type="radio" class="checkBox"
														   checked="true" />
													  Public
												  </div>
												  <input runat="server" type="radio" id="rdoPrivate" name="rdoPerm" class="checkBox" />
												  Private
											 </td>
										</tr>
								   </table>
							  </div>
							  <div class="required">
								   <table border="0" width="100%">
										<tr>
											 <td style="text-align: right; vertical-align: top; width: 160px">
												  <img id="Img8" runat="server" src="~/images/icons/toolTip.gif" onmouseover="whiteBalloonSans.showTooltip(event,'<strong>Pass Type</strong><br/>Select the most appropriate pass type for your item. Select `Access Pass` if the item may not be appropriate for Kaneva members under the age of 18.')"
													   alt="" />
												  Pass Type:
											 </td>
											 <td>
												  <asp:DropDownList ID="ddl_PassType" runat="server" Width="100%" CausesValidation="false" />
												  <asp:RequiredFieldValidator ID="rfvPassType" runat="server" InitialValue="-1" ControlToValidate="ddl_PassType"
													   Text="*" ErrorMessage="Please select a pass type." Display="Dynamic"></asp:RequiredFieldValidator>
											 </td>
										</tr>
								   </table>
							  </div>
							  <div class="required">
								   <table border="0" width="100%">
										<tr>
											 <td style="text-align: right; vertical-align: top; width: 160px">
												  <img id="Img1" runat="server" src="~/images/icons/toolTip.gif" onmouseover="whiteBalloonSans.showTooltip(event,'<strong>Design Commission</strong><br/>Enter the number of Credits you want to earn from the sale of this item.<br/>Note: The base price and a catalog charge will be added to your Design Commission, for the final price of the item.')"
													   alt="" />
												  Design Commission:
											 </td>
											 <td>
												  <asp:TextBox ID="txtPrice" MaxLength="25" Width="100%" CssClass="inputText" runat="server"
													   AutoPostBack="true" OnTextChanged="txtPrice_TextChanged"></asp:TextBox>
												  <asp:RequiredFieldValidator ID="rfvPrice" runat="server" ControlToValidate="txtPrice"
													   ErrorMessage="Design commission is a required field." Display="Dynamic"></asp:RequiredFieldValidator>
											 </td>
										</tr>
								   </table>
							  </div>
							  <div class="required" id="divUploadFee" runat="server">
								   <table border="0" width="100%">
										<tr>
											 <td style="text-align: right; vertical-align: top; width: 160px">
												  <img id="Img3" runat="server" src="~/images/icons/toolTip.gif" onmouseover="whiteBalloonSans.showTooltip(event,'<strong>Upload Fee</strong><br/>You will be charged an upload fee determined by the mesh dimension and complexity of this item.')"
													   alt="" />
												  Upload Fee:
											 </td>
											 <td>
												  <asp:Image ID="imgUploadFeeCurrency" runat="server" ImageUrl="~/images/silver_coins.jpg" />
												  <asp:Label ID="lblUploadFee" Width="100%" runat="server"></asp:Label>
												 </td>
											 <td>
											      <asp:Panel runat="server" ID="pnlPayWith"> Pay with:<br/>
							                            <input runat="server" id="rdoCredits" name="rdoPayWith" type="radio" class="checkBox"/>Credits&nbsp;
							                            <input runat="server" id="rdoRewards" name="rdoPayWith" type="radio" class="checkBox" checked="true"/>Rewards
												  </asp:Panel>
											 </td>
										</tr>
								   </table>
							  </div>
							  <div class="required" id="divDerivable" runat="server" visible="false">
								   <table border="0" width="100%">
										<tr>
											 <td style="text-align: right; vertical-align: top; width: 160px">
												  <img id="Img4" runat="server" src="~/images/icons/toolTip.gif" onmouseover="whiteBalloonSans.showTooltip(event,'<strong>Derivable</strong><br/>Check this box if you want other users to customize your design. They will be able to download your texture for modification, add animations to the object, and/or add sounds or other effects.')"
													   alt="" />
												  Derivable:
											 </td>
											 <td>
												  <input runat="server" class="checkBox" id="chkDerivable" type="checkbox" />
											 </td>
										</tr>
								   </table>
							  </div>
							  
							  <div id="div1" runat="server">
								   <table border="0" width="100%">
										<tr>
											 <td style="text-align: right; vertical-align: top; width: 160px">
												  <img id="Img2" runat="server" src="~/images/icons/toolTip.gif" onmouseover="whiteBalloonSans.showTooltip(event,'<strong>Blast</strong><br/>Check this box if you want to send a blast about this item.')"
													   alt="" />
												  Blast Item:
											 </td>
											 <td>
												  <input runat="server" class="checkBox" id="chkBlastUpload" type="checkbox" />
											 </td>
										</tr>
								   </table>
							  </div>
						 </div>
						 <div id="divbtn">
							  <a href="#" id="btn_submit" onclick="if (typeof(Page_ClientValidate) == 'function') if (!Page_ClientValidate()){return false;};$('popUpWin2').style.display = 'block';">
								   Create Item</a> <a id="btn_cancel" runat="server" href="~/MyInventory.aspx">Cancel</a>
						 </div>
					</div>
			   </div>
		  </div>
	 </ajax:AjaxPanel>
</asp:Content>
