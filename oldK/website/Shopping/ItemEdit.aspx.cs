///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Linq;

using log4net;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
    public class ItemEdit : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set text in search bar
            ((MainTemplatePage)Master).SearchTitle = "Edit Item";
            Title = "Shop Kaneva = Edit Item";

            divBaseItemInfo.Visible = false;

            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

            if (Request["gId"] == null)
            {
                Response.Redirect("~/Catalog.aspx");
            }

            ShoppingFacade shoppingFacade = new ShoppingFacade();
            int gId = GlobalId();

            WOKItem item = shoppingFacade.GetItem(gId);

            // Make sure they are admin or item owner
            if (!item.ItemCreatorId.Equals(Convert.ToUInt32(KanevaWebGlobals.CurrentUser.UserId)) || item.UseType == WOKItem.USE_TYPE_PREMIUM)
            {
                // No rights
                if (!IsAdministrator())
                {
                    Response.Redirect("~/Default.aspx");
                }
            }

            if (!IsPostBack)
            {
                PopulatePassGroupList();

                txtTitle.Text = Server.HtmlDecode(item.Name);
                txtKeywords.Text = Server.HtmlDecode(item.Keywords);
                txtDescription.Text = Server.HtmlDecode(item.Description);
                ddl_PassType.SelectedValue = item.PassTypeId.ToString();
                lblPrice.Text = item.WebPrice.ToString();
                lblBasePrice.Text = (item.UseType == WOKItem.USE_TYPE_CUSTOM_DEED ? Kaneva.BusinessLayer.Facade.Configuration.BaseDeedPrice : item.MarketCost).ToString();
                if (item.DerivationLevel == 0 && item.BaseGlobalId != 0)
                    txtPrice.Text = item.TemplateDesignerPrice.ToString();
                else
                    txtPrice.Text = item.DesignerPrice.ToString();
                imgItem.Src = StoreUtility.GetItemImageURL(item.ThumbnailLargePath, "la");

                // http://bugz.kaneva.com/mantis/view.php?id=9430
                if (item.UseType == WOKItem.USE_TYPE_ACTION_ITEM)
                {
                    txtPrice.Enabled = false;
                }

                // Owner username
                if (!item.IsKanevaOwned)
                {
                    UserFacade userFacade = new UserFacade();
                    User user = userFacade.GetUser(Convert.ToInt32(item.ItemCreatorId));
                    lblUsername.Text = user.Username;
                }
                else
                {
                    lblUsername.Text = "Kaneva";
                }
               
                if (item.BaseGlobalId != 0 && item.DerivationLevel > 0)
                {
                    //NOTE: Not to confuse baseItemId with base_global_id, which is the global ID of a template item 
                    //  while base item is the first item created along with the template.
                    int baseItemId = shoppingFacade.GetBaseItemGlobalId(gId);
                    if (baseItemId != 0)
                    {
                        WOKItem baseItem = shoppingFacade.GetItem(baseItemId);
                        if (baseItem != null)
                        {
                            lnkBaseItem.Text = baseItem.Name;
                            lnkBaseItem.NavigateUrl = "~/ItemDetails.aspx?gId=" + baseItemId.ToString();
                            lblTemplateDesignerCommission.Text = item.TemplateDesignerPrice.ToString();
                            divBaseItemInfo.Visible = true;
                        }
                    }
                }

                // Case for editing Bundles
                if (item.UseType == WOKItem.USE_TYPE_BUNDLE)
                {
                    ResetBundleInfo(item);
                }
                else
                {
                    if (item.UseType == WOKItem.USE_TYPE_CUSTOM_DEED)
                    {
                        ResetDeedInfo(item);
                    }

                    if (item.ItemActive.Equals ((int) WOKItem.ItemActiveStates.Private))
                    {
                        rdoPrivate.Checked = true;
                        rdoPublic.Checked = false;
                    }
                    else
                    {
                        rdoPublic.Checked = true;
                        rdoPrivate.Checked = false;
                    }

                    // Categories
                    rptCategories.DataSource = shoppingFacade.GetItemCategoriesByItemId (gId);
                    rptCategories.DataBind ();
                    divCategory.Visible = true;

                    //1) derived UGC DO
                    //derivationlevel=1 and base_global_id>=3000000
                    //2) not-derived UGC DO
                    //derivationlevel=0 and global_id>=3000000
                    //3) UGC clothing/furniture
                    //derivationlevel=1 and base_global_id<3000000
                    //4) template items or kaneva items
                    //derivationlevel=-1
                    //for 2) not-derived UGC DO: base_global_id=0 always

                    // If it is not a derived UGC DO and not derived UGC,
                    // allow them to change the price if over fashion fame level X

                    // ** ED-7690 ALL UGC IS CREDITS ONLY. This section is no longer valid so commenting out
                    // to keep from makeing an additional query to get user fame
                    /*
                    if ((item.DerivationLevel == 0 && item.GlobalId >= WOKItem.UGC_BASE_GLID) ||
                        (item.DerivationLevel == 1 && item.BaseGlobalId < WOKItem.UGC_BASE_GLID))
                    {
                        FameFacade fameFacade = new FameFacade ();
                        UserFame userFashionFame = fameFacade.GetUserFame (KanevaWebGlobals.CurrentUser.UserId, (int) FameTypes.Fashion);

                        if (userFashionFame.CurrentLevel.LevelNumber >= Kaneva.BusinessLayer.Facade.Configuration.UGC_CreditOnlyFashionFameLevel)
                        {
                            divOnlyCredits.Visible = true;

                            // Check box if it is credits only
                            if (item.InventoryType == 256)
                            {
                                chkOnlyCredits.Checked = true;
                            }
                        }
                    }
                    */

                    divBasePrice.Visible = true;
                }
            }
        }

        #region Helper Methods

        private void ResetBundleInfo(WOKItem bundle)
        {
            divCategory.Visible = false;
            spnAccessability.Visible = false;

            // Get bundle items
            List<WOKItem> bundleItems = GetShoppingFacade.GetBundleItems(bundle.GlobalId);

            // Get bundle Price
            SetPrices(bundleItems, (int)bundle.DesignerPrice, (int)bundle.ItemCreatorId);
            divBundlePrice.Visible = true;

            txtPrice.AutoPostBack = true;

            // Don't accidentally re-activate private bundles!
            if (bundle.ItemActive.Equals((int)WOKItem.ItemActiveStates.Private))
            {
                rdoPrivate.Checked = true;
                rdoPublic.Checked = false;
            }
        }

        private void ResetDeedInfo(WOKItem deedItem)
        {
            divCategory.Visible = false;

            // Get deed items
            List<DeedItem> deedItems = GetShoppingFacade.GetDeedSalesItemList(deedItem.GlobalId, (int)deedItem.ItemCreatorId);
        }
        
        private void PopulatePassGroupList ()
        {
            ddl_PassType.DataSource = WebCache.GetPassGroupListFiltered (((int) Constants.ePASS_TYPE.ACCESS).ToString () + "," + ((int) Constants.ePASS_TYPE.GENERAL).ToString ());
            ddl_PassType.DataTextField = "name";
            ddl_PassType.DataValueField = "pass_group_id";
            ddl_PassType.SelectedValue = ((int) Constants.ePASS_TYPE.ACCESS).ToString ();
            ddl_PassType.DataBind ();
        }

        private void SetPrices (List<WOKItem> items, int commission, int bundleOwnerId)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            SetPriceFields (shoppingFacade.CalculateBundlePrice (items, commission, bundleOwnerId));
        }

        private void SetPriceFields (BundlePrice bp)
        {
            totalNotOwnedItems.InnerText = bp.TotalItemsNotOwned.ToString ();
            totalOwnedItems.InnerText = bp.TotalItemsOwnedPlusCommission.ToString ();
            totalDesignCommission.InnerText = bp.DesignCommissionPlusCommission.ToString ();
            totalBundle.InnerText = bp.TotalBundle.ToString ();

            CurrentBundlePrice = bp;
        }

        private void SendMessage (BundleUpdateMsgTypes type, WOKItem item, WOKItem bundle)
        {
            SendMessage (type, item, bundle, 0);
        }
        private void SendMessage (BundleUpdateMsgTypes type, WOKItem item, WOKItem bundle, int origBundlePrice)
        {
            string subj = "";
            string msg = "";
            int fromUserId = 1;

            if (GetMessageDetails(type, item, bundle, origBundlePrice, ref subj, ref msg))
            {

                // Insert the message
                Message message = new Message(0, fromUserId, (int)bundle.ItemCreatorId, subj,
                    msg, new DateTime(), 0, (int)Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                GetUserFacade.InsertMessage(message);

                User userTo = GetUserFacade.GetUser((int)bundle.ItemCreatorId);
                if (userTo.UserId > 0)
                {
                    if (Convert.ToInt32(userTo.NotifyAnyoneMessages).Equals(1))
                    {
                        // They want emails for all messages
                        MailUtilityWeb.SendPrivateMessageNotificationEmail(userTo.Email, message.Subject, message.MessageId, message.MessageText, userTo.UserId, fromUserId);
                    }
                }
            }
        }

        private bool GetMessageDetails (BundleUpdateMsgTypes type, WOKItem item, WOKItem bundle, int originalBundlePrice, ref string subj, ref string msg)
        {
            // do not send message if item and bundle owner are the same
            if (item.ItemCreatorId == bundle.ItemCreatorId)
                return false;

            string itemOwnerName = GetUserFacade.GetUserName ((int)item.ItemCreatorId);

            switch (type)
            {
                case BundleUpdateMsgTypes.AP:
                    subj = "Your bundle has become Access Pass required";
                    msg = itemOwnerName + " has made his item, " + item.DisplayName + ", as private and it was contained in your bundle, " + bundle.DisplayName + ".<br/><br/>" +
                        "This change invalidates your bundle and it has been removed from shop.  No follow up action is required.<br/><br/>" +
                        "Thank you,<br/>" +
                        "The Kaneva Team";
                    break;

                case BundleUpdateMsgTypes.Delete:
                    subj = "Your bundle was removed from shop";
                    msg = itemOwnerName + " has removed his item, " + item.DisplayName + ", and it was contained in your bundle, " + bundle.DisplayName + ".<br/><br/>" +
                        "This change invalidates your bundle and it has been removed from shop.  No follow up action is required.<br/><br/>" +
                        "Thank you,<br/>" +
                        "The Kaneva Team";
                    break;

                case BundleUpdateMsgTypes.InvType:
                    subj = "Your bundle has become Credits Only";
                    msg = "The item, " + item.DisplayName + ", has changed to 'Credits Only,' so this requires your bundle, " + bundle.DisplayName + ", to become Credits Only as well.  No follow up action is required on your part.<br/><br/>" +
                        "Thank you,<br/>" +
                        "The Kaneva Team";
                    break;

                case BundleUpdateMsgTypes.Price:
                    subj = "Your bundle had a price change";
                    msg = itemOwnerName + " has modified their commission on item, " + item.DisplayName + " and it is contained in your bundle, " + bundle.DisplayName + ".<br/><br/>" +
                        "This changes the bundle price from " + originalBundlePrice + " to " + bundle.MarketCost.ToString () + ".   No follow up action is required on your part.<br/><br/>" +
                        "Thank you,<br/>" +
                        "The Kaneva Team";
                    break;

                case BundleUpdateMsgTypes.Price_BaseItem:
                    subj = "Your bundle had a price change";
                    msg = "The price of " + item.DisplayName + " changed and it is contained in your bundle, " + bundle.DisplayName + ".<br/><br/>" +
                        "This changes the bundle price from  " + originalBundlePrice + " to " + bundle.MarketCost.ToString () + ".   No follow up action is required on your part.<br/><br/>" +
                        "Thank you,<br/>" +
                        "The Kaneva Team";
                    break;

                case BundleUpdateMsgTypes.Private:
                    subj = "Your bundle was removed from shop";
                    msg = itemOwnerName + " has made an item, " + item.DisplayName + ", private and it was contained in your bundle, " + bundle.DisplayName + ".<br/><br/>" +
                        "This change invalidates your bundle and it has been removed from shop.  No follow up action is required.<br/><br/>" +
                        "Thank you,<br/>" +
                        "The Kaneva Team";
                    break;
            }

            return true;
        }

        private void SendMessage(DeedUpdateMsgTypes type, WOKItem item, WOKItem deed)
        {
            SendMessage(type, item, deed, 0);
        }
        private void SendMessage(DeedUpdateMsgTypes type, WOKItem item, WOKItem deed, int origBundlePrice)
        {
            string subj = "";
            string msg = "";
            int fromUserId = 1;

            if (GetMessageDetails(type, item, deed, origBundlePrice, ref subj, ref msg))
            {

                // Insert the message
                Message message = new Message(0, fromUserId, (int)deed.ItemCreatorId, subj,
                    msg, new DateTime(), 0, (int)Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                GetUserFacade.InsertMessage(message);

                User userTo = GetUserFacade.GetUser((int)deed.ItemCreatorId);
                if (userTo.UserId > 0)
                {
                    if (Convert.ToInt32(userTo.NotifyAnyoneMessages).Equals(1))
                    {
                        // They want emails for all messages
                        MailUtilityWeb.SendPrivateMessageNotificationEmail(userTo.Email, message.Subject, message.MessageId, message.MessageText, userTo.UserId, fromUserId);
                    }
                }
            }
        }

        private bool GetMessageDetails(DeedUpdateMsgTypes type, WOKItem item, WOKItem bundle, int originalBundlePrice, ref string subj, ref string msg)
        {
            // do not send message if item and bundle owner are the same
            if (item.ItemCreatorId == bundle.ItemCreatorId)
                return false;

            string itemOwnerName = GetUserFacade.GetUserName((int)item.ItemCreatorId);

            switch (type)
            {
                case DeedUpdateMsgTypes.AP:
                    subj = "Your World on shop has become Access Pass required";
                    msg = itemOwnerName + " has made his item, " + item.DisplayName + ", as private and it was contained in your World, " + bundle.DisplayName + ".<br/><br/>" +
                        "This change invalidates your World and it has been removed from shop.  No follow up action is required.<br/><br/>" +
                        "Thank you,<br/>" +
                        "The Kaneva Team";
                    break;

                case DeedUpdateMsgTypes.Delete:
                    subj = "Your World was removed from shop";
                    msg = itemOwnerName + " has removed his item, " + item.DisplayName + ", and it was contained in your bundle, " + bundle.DisplayName + ".<br/><br/>" +
                        "This change invalidates your World and it has been removed from shop.  No follow up action is required.<br/><br/>" +
                        "Thank you,<br/>" +
                        "The Kaneva Team";
                    break;

                case DeedUpdateMsgTypes.InvType:
                    subj = "Your World on shop has become Credits Only";
                    msg = "The item, " + item.DisplayName + ", has changed to 'Credits Only,' so this requires your World, " + bundle.DisplayName + ", to become Credits Only as well.  No follow up action is required on your part.<br/><br/>" +
                        "Thank you,<br/>" +
                        "The Kaneva Team";
                    break;

                case DeedUpdateMsgTypes.Price:
                    subj = "Your World on shop had a price change";
                    msg = itemOwnerName + " has modified their commission on item, " + item.DisplayName + " and it is contained in your World, " + bundle.DisplayName + ".<br/><br/>" +
                        "This changes the World price from " + originalBundlePrice + " to " + bundle.MarketCost.ToString() + ".   No follow up action is required on your part.<br/><br/>" +
                        "Thank you,<br/>" +
                        "The Kaneva Team";
                    break;

                case DeedUpdateMsgTypes.Price_BaseItem:
                    subj = "Your World on shop had a price change";
                    msg = "The price of " + item.DisplayName + " changed and it is contained in your World, " + bundle.DisplayName + ".<br/><br/>" +
                        "This changes the World price from  " + originalBundlePrice + " to " + bundle.MarketCost.ToString() + ".   No follow up action is required on your part.<br/><br/>" +
                        "Thank you,<br/>" +
                        "The Kaneva Team";
                    break;

                case DeedUpdateMsgTypes.Private:
                    subj = "Your World was removed from shop";
                    msg = itemOwnerName + " has made an item, " + item.DisplayName + ", private and it was contained in your World, " + bundle.DisplayName + ".<br/><br/>" +
                        "This change invalidates your World and it has been removed from shop.  No follow up action is required.<br/><br/>" +
                        "Thank you,<br/>" +
                        "The Kaneva Team";
                    break;
            }

            return true;
        }
        private void ShowError (string msg)
        {
            cvGeneral.ErrorMessage = msg;
            cvGeneral.IsValid = false;
        }

        #endregion Helper Methods

        #region Event Handlers

        /// <summary>
        /// btnUpdate_Click
        /// </summary>
        protected void btnUpdate_Click (object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
                return;
            }

            if (KanevaGlobals.IsPottyMouth (txtTitle.Text.Trim ()))
            {
                ShowError ("Item Name includes one or more restricted words.");
                rfvTitle.IsValid = false;
                rfvTitle.ErrorMessage = "";
                return;
            }
            if (KanevaGlobals.IsPottyMouth (txtDescription.Text.Trim ()))
            {
                ShowError ("Description includes one or more restricted words.");
                rfvDesc.IsValid = false;
                rfvDesc.ErrorMessage = "";
                return;
            }
            if (KanevaGlobals.IsPottyMouth (txtKeywords.Text.Trim ()))
            {
                ShowError ("Tags includes one or more restricted words.");
                cvTags.IsValid = false;
                return;
            }

            // Validate image upload. Must be either nothing, a media library photo, or an upload.
            int assetId = 0;
            HttpPostedFile fThumbnail = null;
            if (string.IsNullOrWhiteSpace(hfMediaLibPhoto.Value))
            {
                // Get the thumbnail to upload
                fThumbnail = inpThumbnail.PostedFile;
                if (fThumbnail != null && !fThumbnail.ContentLength.Equals(0))
                {
                    if (fThumbnail.ContentLength > KanevaGlobals.MaxUploadedImageSize)
                    {
                        ShowError("Thumbnail Image is too large, please select a photo smaller than " + KanevaGlobals.FormatImageSize(KanevaGlobals.MaxUploadedImageSize) + ".");
                        revThumbnail.IsValid = false;
                        revThumbnail.ErrorMessage = "";
                        return;
                    }
                    else
                    {
                        // Thumbnail must be a JPG, JPEG, GIF, or PNG!!!
                        string strExtensionUpperCase = Path.GetExtension(fThumbnail.FileName).ToUpper();
                        if (!strExtensionUpperCase.Equals(".JPG") && !strExtensionUpperCase.Equals(".JPEG") &&
                            !strExtensionUpperCase.Equals(".GIF"))
                        {
                            ShowError("Thumbnail Image must be a .jpg, .jpeg, or .gif file type.");
                            revThumbnail.IsValid = false;
                            revThumbnail.ErrorMessage = "";
                            return;
                        }
                    }
                }
            }
            else if (!Int32.TryParse(hfMediaLibPhoto.Value, out assetId) || assetId <= 0)
            {
                ShowError("Thumbnail Image must be a .jpg, .jpeg, or .gif file type.");
                revThumbnail.IsValid = false;
                revThumbnail.ErrorMessage = "";
                return;
            }

            int gId = GlobalId ();

            // Generate the thumbnails. First try using a media library image.
            if (assetId > 0)
            {
                SetItemThumbnailsToAssetImage(assetId, gId);
            }
            // Next try using an uploaded file.
            else if (fThumbnail != null && fThumbnail.ContentLength > 0)
            {
                string thumbnailFilename = "";
                string strPathUserIdGlobalId = Path.Combine(KanevaWebGlobals.CurrentUser.UserId.ToString(), gId.ToString());
                string strFilePathTexture = Path.Combine(KanevaGlobals.TexturePath, strPathUserIdGlobalId);

                try
                {
                    ImageHelper.UploadImageFromUser(ref thumbnailFilename, fThumbnail, strFilePathTexture, KanevaGlobals.MaxUploadedImageSize);
                    string imagePath = fThumbnail.FileName.Remove(0, fThumbnail.FileName.LastIndexOf("\\") + 1);
                    string origImagePath = strFilePathTexture + Path.DirectorySeparatorChar + thumbnailFilename;

                    ImageHelper.GenerateWOKItemThumbs(thumbnailFilename, origImagePath, KanevaGlobals.TexturePath, KanevaWebGlobals.CurrentUser.UserId, gId);
                }
                catch { }
            }

            WOKItem updateItem = GetShoppingFacade.GetItem(gId);

            // Prep parameters for update
            updateItem.Name = Server.HtmlEncode(txtTitle.Text.Trim());
            updateItem.DisplayName = updateItem.Name;
            updateItem.Description = Server.HtmlEncode(txtDescription.Text.Trim());
            updateItem.Keywords = Server.HtmlEncode(txtKeywords.Text.Trim());
            updateItem.PassTypeId = Convert.ToInt32(ddl_PassType.SelectedValue);
            updateItem.ItemActive = (rdoPublic.Checked ? (int) WOKItem.ItemActiveStates.Public : (int) WOKItem.ItemActiveStates.Private);
            
            try
            {
                updateItem.DesignerPrice = Convert.ToUInt32 (txtPrice.Text.Trim ());
            }
            catch (Exception) { }

            if (divOnlyCredits.Visible)
            {
                updateItem.InventoryType = chkOnlyCredits.Checked ? 256 : 768;
            }

            // Perform the update
            List<BundleItemChange> associatedBundleChanges;
            List<DeedItemChange> associatedDeedChanges;
            GetShoppingFacade.UpdateShopItem(updateItem, KanevaWebGlobals.CurrentUser.UserId, false, false, out associatedBundleChanges, out associatedDeedChanges);

            // Notify associated bundle owners if there were any changes
            foreach (BundleItemChange change in associatedBundleChanges)
            {
                // Status
                if (change.ModifiedBundle.ItemActive != change.OriginalBundle.ItemActive)
                {
                    BundleUpdateMsgTypes mt = change.ModifiedBundle.ItemActive == (int)WOKItem.ItemActiveStates.Private ?
                            BundleUpdateMsgTypes.Private : BundleUpdateMsgTypes.Delete;

                    SendMessage(mt, change.OriginalItem, change.ModifiedBundle);
                }
                // Pass type
                else if (change.ModifiedBundle.PassTypeId != change.OriginalBundle.PassTypeId && change.ModifiedBundle.PassTypeId == (int)Constants.ePASS_TYPE.ACCESS)
                {
                    SendMessage(BundleUpdateMsgTypes.AP, change.OriginalItem, change.ModifiedBundle);
                }
                // Derived item price
                else if (change.ModifiedItem.BaseGlobalId == change.OriginalItem.BaseGlobalId &&
                    change.ModifiedItem.TemplateDesignerPrice != change.OriginalItem.TemplateDesignerPrice)
                {
                    SendMessage(BundleUpdateMsgTypes.Price_BaseItem, change.ModifiedItem, change.ModifiedBundle, change.OriginalBundlePrice);
                }
                // Item Price
                else if (change.ModifiedBundle.WebPrice != change.OriginalBundlePrice)
                {
                    SendMessage(BundleUpdateMsgTypes.Price, change.OriginalItem, change.ModifiedBundle, change.OriginalBundlePrice);
                }
                // Inventory type
                else if (change.ModifiedBundle.InventoryType != change.OriginalBundle.InventoryType)
                {
                    SendMessage(BundleUpdateMsgTypes.InvType, change.OriginalItem, change.ModifiedBundle);
                }
            }

            // Notify associated deed owners if there were any changes
            foreach (DeedItemChange change in associatedDeedChanges)
            {
                // Status
                //if (change.ModifiedDeed.ItemActive != change.OriginalDeed.ItemActive)
                //{
                //    DeedUpdateMsgTypes mt = change.ModifiedDeed.ItemActive == (int)WOKItem.ItemActiveStates.Private ?
                //        DeedUpdateMsgTypes.Private : DeedUpdateMsgTypes.Delete;

                //    SendMessage(mt, change.OriginalItem, change.ModifiedDeed);
                //}
                // Pass Type
                if (change.ModifiedDeed.PassTypeId != change.OriginalDeed.PassTypeId && change.ModifiedDeed.PassTypeId == (int)Constants.ePASS_TYPE.ACCESS)
                {
                    SendMessage(DeedUpdateMsgTypes.AP, change.OriginalItem, change.ModifiedDeed);
                }
                // Derived item price
                else if (change.ModifiedItem.BaseGlobalId == change.OriginalItem.BaseGlobalId &&
                    change.ModifiedItem.TemplateDesignerPrice != change.OriginalItem.TemplateDesignerPrice)
                {
                    SendMessage(DeedUpdateMsgTypes.Price_BaseItem, change.ModifiedItem, change.ModifiedDeed, change.OriginalDeedPrice);
                }
                // Price 
                else if (change.ModifiedDeed.WebPrice != change.OriginalDeedPrice)
                {
                    SendMessage(DeedUpdateMsgTypes.Price, change.OriginalItem, change.ModifiedDeed, change.OriginalDeedPrice);
                }
                // Inventory type
                else if (change.ModifiedDeed.InventoryType != change.OriginalDeed.InventoryType)
                {
                    SendMessage(DeedUpdateMsgTypes.InvType, change.OriginalItem, change.ModifiedDeed);
                }
            }

            // Blast if they wanted to Blast
            if (chkBlastUpload.Checked)
            {
                int currUserId = KanevaWebGlobals.CurrentUser.UserId;

                // getting a new instance of the item so all the fields are updated.
                WOKItem item2 = GetShoppingFacade.GetItem (gId);

                m_logger.Debug ("Blasting Item");
                try
                {
                    // Metrics tracking for accepts
                    UserFacade userFacade = new UserFacade();
                    string requestId = userFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_SHOP_ITEM_UPLOAD, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
                    string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_BLAST, 0);


                    string url = "http://" + KanevaGlobals.ShoppingSiteName + "/ItemDetails.aspx?gId=" + item2.GlobalId + "&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeSentIdBlast;
                    string itemThumbnail = StoreUtility.GetItemImageURL (item2.ThumbnailLargePath, "la");

                    BlastFacade blastFacade = new BlastFacade ();

                    blastFacade.SendShopUploadItemBlast (currUserId, KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.NameNoSpaces,
                        item2.DisplayName, item2.Description, url, itemThumbnail, "", KanevaWebGlobals.CurrentUser.ThumbnailSmallPath);
                }
                catch (Exception ex)
                {
                    m_logger.Error ("Blasting Item Edit Failed. Item:" + item2.GlobalId + " UserId: " + currUserId + " " + ex.ToString ());
                }
            }

            Response.Redirect (ResolveUrl ("~/MyInventory.aspx"));
        }

        /// <summary>
        /// btnCancel_Click
        /// </summary>
        protected void btnCancel_Click (object sender, EventArgs e)
        {
            Response.Redirect (ResolveUrl ("~/MyInventory.aspx"));
        }

        /// <summary>
        /// btnClear_Click
        /// </summary>
        protected void btnClear_Click (object sender, EventArgs e)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade ();
            int gId = GlobalId ();

            shoppingFacade.DeleteCustomItemThumbnail (gId);

            WOKItem item = shoppingFacade.GetItem (gId);
            imgItem.Src = StoreUtility.GetItemImageURL (item.ThumbnailLargePath, "la");
            hfMediaLibPhoto.Value = string.Empty;
        }

        protected void txtPrice_TextChanged (object sender, EventArgs e)
        {
            BundlePrice bp = CurrentBundlePrice;
            bp.DesignCommission = Convert.ToInt32 (((TextBox) sender).Text);
            SetPriceFields (bp);
        }

        #endregion Event Handlers

        #region Properties

        /// <summary>
        /// GlobalId
        /// </summary>
        private int GlobalId()
        {
            int gId = 0;

            try
            {
                gId = Convert.ToInt32(Request["gId"]);
            }
            catch (Exception)
            {
                Response.Redirect("~/Catalog.aspx");
                Response.End();
            }

            return gId;
        }

        protected BundlePrice CurrentBundlePrice
        {
            get
            {
                if (ViewState["CurrentBundlePrice"] == null)
                {
                    return new BundlePrice ();
                }
                return (BundlePrice) ViewState["CurrentBundlePrice"];
            }
            set { ViewState["CurrentBundlePrice"] = value; }
        }

        enum BundleUpdateMsgTypes
        {
            Private = 1,
            Delete = 2,
            AP = 3,
            Price = 4,
            Price_BaseItem = 5,
            InvType = 6
        }

        enum DeedUpdateMsgTypes
        {
            Private = 1,
            Delete = 2,
            AP = 3,
            Price = 4,
            Price_BaseItem = 5,
            InvType = 6
        }

        #endregion Properties

        #region Declerations

        protected HtmlInputFile inpThumbnail;
        protected CustomValidator cvGeneral, cvTags;
		protected CheckBox chkOnlyCredits;
        protected TextBox txtTitle, txtDescription, txtKeywords, txtPrice;
		protected HtmlInputCheckBox chkRestricted, chkPrivate, chkBlastUpload;
        protected Label lblUsername, lblBasePrice, lblPrice;
        protected HtmlImage imgItem;
        protected Repeater rptCategories;
        protected HtmlInputRadioButton rdoPublic, rdoPrivate;
        protected DropDownList ddl_PassType;
        protected HtmlGenericControl divBaseItemInfo, divOnlyCredits, divUploadButtons, divItemLocked;
        protected HtmlContainerControl divCategory, spnAccessability, divBasePrice, divBundlePrice;
        protected HtmlContainerControl totalNotOwnedItems, totalOwnedItems, totalDesignCommission, totalBundle;
        protected HyperLink lnkBaseItem;
        protected Label lblTemplateDesignerCommission;
        protected Button bAddItem;
        protected RequiredFieldValidator rfvTitle, rfvDesc;
        protected RegularExpressionValidator revThumbnail;
        protected HiddenField hfMediaLibPhoto;

        const int CURRENCY_TYPE_CREDITS = 256;
        const int CURRENCY_TYPE_REWARDS = 512;
        const int CURRENCY_TYPE_BOTH_R_C = 768;
        
        /// <summary>
		/// Logger
		/// </summary>
		private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Declerations
    }
}
