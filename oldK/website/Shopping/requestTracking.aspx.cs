///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
    public partial class requestTracking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Request tracking
                if (Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM] != null)
                {
                    UserFacade userFacade = new UserFacade();
                    int iResult = userFacade.AcceptTrackingRequest(Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM], KanevaWebGlobals.CurrentUser.UserId);
                }
            }
        }
    }
}