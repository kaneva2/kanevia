<%@ page language="C#" masterpagefile="~/masterpages/MainTemplatePage.Master" autoeventwireup="true" codebehind="Upload.aspx.cs" inherits="Kaneva.PresentationLayer.Shopping.Upload" %>

<asp:Content id="Content1" contentplaceholderid="cphHeaderCSS" runat="server">
    <link href="./jscript/colorbox/colorbox.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:content id="cntUpload" runat="server" contentplaceholderid="cphBody">
	<script src="./jscript/prototype.js" type="text/javascript" language="javascript"></script>
	<script type="text/javascript"><!--
function UploadFiles()
{
    ShowLoading(true,'Please wait for the upload to complete...<br/><img src="./images/progress_bar.gif">');
}

function TextureChange(theValue)
{
    <asp:Literal id="litJS" runat="server"/>

    if (theValue != '')
    {
        $('text' + theValue).style.display = 'block';   
    }
}
//--> </script>
	<asp:validationsummary showmessagebox="False" showsummary="True" id="valSum" class="errSum" displaymode="BulletList" runat="server" headertext="Please correct the following errors:" />
	<asp:customvalidator id="cvGeneral" runat="server" visible="false"></asp:customvalidator>
	<div id="content">
		<div id="popUpWin">
			<div>
				<h1>
					Does This Violate Our Terms of Service?</h1>
				<p>
					By clicking the button below, you agree the item you are uploading conforms to Kaneva&#8217;s Terms of Service, including:</p>
				<ul>
					<li>Not infringing the copyright or intellectual property right of any third party</li>
					<li>Not being indecent, obscene, or otherwise objectionable</li>
				</ul>
				<p>
					Kaneva reserves the right to remove any item that violates these conditions (along with any Credits earned from sales), as well as suspend or delete any member&#8217;s Kaneva account.
					<br />
					<asp:button runat="server" id="button" onclick="btnUpload_Click" text="I Agree With the Terms of Service" onclientclick="$('popUpWin').style.display = 'none';UploadFiles();"></asp:button>
					&nbsp;&nbsp;&nbsp;<asp:button runat="server" id="Reset1" onclientclick="$('popUpWin').style.display = 'none';return false;" text="Cancel" causesvalidation="False"></asp:button>
				</p>
			</div>
		</div>
		
		<div id="divLoading">
			<table height="300" cellspacing="0" cellpadding="0" width="99%" border="0">
				<tr>
					<th class="loadingText" id="divLoadingText">
						Loading...
						<div style="display: none">
							<img src="./images/progress_bar.gif"></div>
					</th>
				</tr>
			</table>
		</div>
		
		<div id="divData" style="display: none">
			<div class="clearit"><!-- test --></div>
			<div class="colWidth">
				<div class="formStyle">
					<span class="fieldNote">* Fields marked with an asterisk (*) are required.</span>
					<div class="required">
						<label>Item Name:*</label><br />
						<asp:requiredfieldvalidator id="rfvTitle" class="validator" runat="server" controltovalidate="txtTitle" text="*" errormessage="Item Name is a required field." display="static"></asp:requiredfieldvalidator>
						<asp:textbox id="txtTitle" runat="server" width="260px" cssclass="inputText" maxlength="125"></asp:textbox>
					</div>
					<div class="required">
						<label>Texture File:*</label><br />
						<asp:RegularExpressionValidator id="revTexture" class="validator1" runat="server" text="*" ErrorMessage="Texture File must be a .jpg or .jpeg file type." ValidationExpression="^.+\.([jJ][pP][gG]|[jJ][pP][eE][gG])$" ControlToValidate="inpTexture" display="Static"></asp:RegularExpressionValidator><asp:requiredfieldvalidator id="rfvTexture" runat="server" class="validator1" controltovalidate="inpTexture" text="*" errormessage="Texture File is a required field." display="static"></asp:requiredfieldvalidator>
						<input runat="server" class="inputText" id="inpTexture" type="file" size="35" />
						<span class="formNote">Texture file must be .jpg format</span>
					</div>
					<div class="required">
						<label for="textureType">Texture Type:*</label><br />
				   		<asp:requiredfieldvalidator id="rfvTextureType" initialvalue="" class="validator" runat="server" controltovalidate="cboTextureType" text="*" errormessage="Texture Type is a required field." display="Static"></asp:requiredfieldvalidator>
						<select runat="server" size="1" id="cboTextureType" class="inputText" style="width:220px;" onchange="TextureChange(this.options[this.selectedIndex].value);"></select>
						<span class="formNote">Select an item from the Texture Type menu above.</span>
					</div>
					<asp:repeater id="rptTextures" runat="server">
						<itemtemplate>
							<div class="textureArea" id='<%# "text" + DataBinder.Eval (Container.DataItem, "GlobalId") %>' style="display: none;">
								<div class="textureTitle">
									<h2><%# DataBinder.Eval (Container.DataItem, "Name") %></h2>
									<%# DataBinder.Eval (Container.DataItem, "GlobalId") %></div>
								<div class="textureImage">
									<img runat="server" id="imgTexture" name='<%# DataBinder.Eval (Container.DataItem, "GlobalId") %>' src='<%#GetItemImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailLargePath").ToString () ,"la")%>' alt="fpo" /></div>
								<div class="clearit">
									<!-- clear the float -->
								</div>
							</div>
						</itemtemplate>
					</asp:repeater>
					<div id="thumbnailEditContainer" class="required">
                        <label for="thumbnail_image">Thumbnail Image:</label>
                        <asp:RegularExpressionValidator id="revThumbnail" class="validator1" runat="server"
                            text="*" errormessage="Thumbnail Image must be a .jpg, .jpeg, or .gif file type."
                            validationexpression="^.+\.([jJ][pP][gG]|[gG][iI][fF]|[jJ][pP][eE][gG])$" controltovalidate="inpThumbnail"
                            display="static"></asp:RegularExpressionValidator>
                        <br />
                        <img runat="server" id="imgItem" src="" alt="" clientidmode="Static" />                 
                        <div id="thumbnailEditButtons">
                            <input runat="server" id="inpThumbnail" type="file" size="15" clientidmode="Static" />
                            <input type="button" class="popFacebox photoBtn" value="Upload Image" />
                            <span class="formNote" id="spnUploadMsg">Upload a .jpg image from your computer.</span>
                            <input type="button" name="btnMediaLibrary" id="btnMediaLibrary" class="popFacebox photoBtn" value="Choose Image" />
                            <span class="formNote">Choose an image from your Media Library.</span>
                            <asp:HiddenField id="hfMediaLibPhoto" runat="server" clientidmode="Static" />
                        </div>
                    </div>
					<div class="required">
						<label for="description">Description:*</label><br />
						<asp:requiredfieldvalidator id="rfvDesc" runat="server" class="validator" controltovalidate="txtDescription" text="*" errormessage="Description is a required field." display="static"></asp:requiredfieldvalidator>
						<asp:textbox id="txtDescription" runat="server" maxlength="300" textmode="MultiLine" rows="3" columns="20"></asp:textbox>
					</div>
					<div class="required">
						<label>Tags:</label><br />
						<asp:customvalidator id="cvTags" runat="server" text="*" display="Static"></asp:customvalidator>
						<asp:textbox id="txtKeywords" class="formNote" maxlength="100" textmode="MultiLine" runat="server"></asp:textbox>
						<span class="formNote">Tags are unique keywords that help people find items on Shop Kaneva.</span>
					</div>
				</div>
			</div>
			<div class="colWidth last">
				<div class="formStyle">
					<div class="required formBox">
						<label>Design Commission:</label>
						<asp:rangevalidator id="rvPrice" class="validator1" controltovalidate="txtPrice" minimumvalue="0" maximumvalue="5000000" type="Integer" runat="server" errormessage="Commission must be a numeric value between 0 and 5,000,000." text="*" display="Static"></asp:rangevalidator>
						<asp:textbox id="txtPrice" maxlength="7" width="60px" cssclass="inputText" runat="server"></asp:textbox>
						<span class="formNote">Enter the number of Credits you want to earn from the sale of this item.<br />
							Note: The base price and a listing fee will be added to your Design Commission, for the final price of the item.</span>
					</div>
					<div class="required">
						<asp:literal id="litAccessPass" runat="server"></asp:literal>
						<div style="padding:0;">
							<label class="restrictedSymbol">Pass Type:*</label><br />
							<asp:requiredfieldvalidator id="rfvPassType" initialvalue="-1" class="validator" runat="server" controltovalidate="ddl_PassType" text="*" errormessage="Pass Type is a required field." display="Static"></asp:requiredfieldvalidator>
							<asp:dropdownlist id="ddl_PassType" runat="server" causesvalidation="false" width="150px" />
						</div>
						<div>
							<!--<IMG id="IMG1" src="../images/pass_content.gif" width="15" runat="server" style="position:absolute; padding-left:95px">-->
							<span class="formNote">Select the most appropriate pass type for your item. For more information see our <a href="http://www.kaneva.com/overview/rulesofconduct.aspx" target="blank">guidelines</a>.</span>
						</div>
					</div>
					<div class="required">
						<label class="publicLabel">Public:</label>
						<input runat="server" id="rdoPublic" name="rdoPerm" type="radio" class="radio" checked="true" />
						<span class="formNote">Items marked as Public can be viewed, shared, and bought by other Kaneva members.</span>
					</div>
					<div class="required">
						<label class="privateLabel">Private:</label>
						<input runat="server" type="radio" id="rdoPrivate" name="rdoPerm" class="radio" />
						<span class="formNote">Items marked as Private can only be viewed by you when you&#8217;re signed in, and cannot be bought by other Kaneva members.</span>
					</div>
					<div>
						<label>Blast:</label>&nbsp;<input runat="server" class="checkBox" id="chkBlastUpload" type="checkbox" />
						<span class="formNote">Blast item details to all your friends to let them know about your new item available for purchase.</span>
					</div>
				</div>
			</div>
			<div class="formBtns2 clearit">
				<a href="#" id="btn_submit" class="btn_submit" onclick="if (typeof(Page_ClientValidate) == 'function') if (!Page_ClientValidate()){return false;};$('popUpWin').style.display = 'block';">Add Item</a> <a id="btn_cancel" class="btn_submit" runat="server" href="~/MyInventory.aspx">Cancel</a>
			</div>
		</div>
	</div>	   
</asp:content>

<asp:Content id="ctnFooterJS" contentplaceholderid="cphFooterJS" runat="server">
    <script type="text/javascript" src="./jscript/colorbox/jquery.colorbox-min.js"></script>
    <script type="text/javascript">
        var $j = jQuery.noConflict();

        $j(document).ready(function () {
            $j("#btnMediaLibrary").colorbox({ iframe: true, fastIframe: false, innerWidth: 566, innerHeight: 508, scrolling: false, overlayClose: false, closeButton: true, href: "MediaLibraryData.aspx", close: "&#10005;", closeTop: true });

            $j('#inpThumbnail').change(function () {
                PreviewThumbnailUpload("inpThumbnail", "spnUploadMsg", "imgItem");
            });
        });
    </script>
</asp:Content>