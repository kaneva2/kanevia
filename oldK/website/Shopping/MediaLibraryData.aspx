﻿<%@ page language="C#" autoeventwireup="True" codebehind="MediaLibraryData.aspx.cs" inherits="Kaneva.PresentationLayer.Shopping.MediaLibraryData" MasterPageFile="~/masterpages/MainTemplateNoBorder.Master" %>
<%@ Register TagPrefix="Shopping" TagName="Pager" Src="./usercontrols/Pager.ascx" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="cphBody">
<asp:scriptmanager id="smMediaLibraryData" runat="server" enablepartialrendering="true"></asp:scriptmanager>
<asp:UpdateProgress id="upLibPhotosProgress" runat="server" associatedupdatepanelid="udpLibraryPhotos" class="mediaLibraryUpdateProgress" displayafter="0">
    <ProgressTemplate>
        <img src="jscript/colorbox/images/loading.gif" />
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:updatepanel id="udpLibraryPhotos" runat="server" childrenastriggers="true" class="mediaLibraryContent">
    <ContentTemplate>
        <h2 class="title">Media Library Images</h2>
        <div><asp:DropdownList ID="ddlGroup" runat="server" style="width:200px;margin-left:8px;margin-top:4px;" class="Filter2" autopostback="true" OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged" /></div>
        <div id="divMediaLibraryPhotos" runat="server" class="mediaLibraryPhotos">
            <asp:DataList Visible="true" RepeatLayout="Table" runat="server" ShowFooter="False" id="dlPictures" cellpadding="2" cellspacing="2" RepeatColumns="5" RepeatDirection="Horizontal" >
	            <ItemStyle CssClass="lineItemThumb" HorizontalAlign="Center"/>
	            <ItemTemplate>
	                <div id="objectContainer">
		                <div id="radioContainer" >
			                <input id="rbPicture" class="chkPicture" style="vertical-align:middle" name="rbPicture" type="checkbox" value='<%# DataBinder.Eval( Container.DataItem, "asset_id") %>' data-thumb='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")))%>' onclick='setSelectedImage(this, this.value, this.getAttribute("data-thumb"))' />
		                </div>
		                <div class="imageNameContainer">
		                    <img id="Img1" runat="server" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")))%>' /><br />
		                    <span class="insideTextNoBold" id="spnImageTitle" runat="server"></span>
		                </div>
		            </div>
	            </ItemTemplate>
            </asp:DataList>	
        </div>
        <asp:Label id="lblNoAssets" runat="server" visible="false" text="No images found." cssclass="mediaLibraryEmptyMsg"></asp:Label>
        <div class="mediaLibraryPager">
            <Shopping:Pager runat="server" isajaxmode="False" id="pgBottom" maxpagestodisplay="5" shownextprevlabels="true" />
        </div>
        <div class="mediaLibraryBtnContainer">
            <a id="btnSave" runat="server" class="btnsmall grey inline" onclick="setIframeParentData()" href="javascript:void(0)">Use Selected</a>
            <a id="btnCancel" class="btnxsmall grey inline" onclick="closeLightBox()" href="javascript:void(0)">Cancel</a>
        </div>
    </ContentTemplate>
</asp:updatepanel>
<input type="hidden" name="hfAssetId" id="hfAssetId" />
<input type="hidden" name="hfAssetThumb" id="hfAssetThumb" />
</asp:Content>

<asp:Content id="cntFooterJS" runat="server" contentplaceholderid="cphFooterJS">
<script type="text/javascript" src="jscript/jquery/jquery-1.8.2.min.js"></script>
<script type="text/javascript"><!--
    // Sets the selected image based on radio button input.
    function setSelectedImage(button, imageId, thumbnailPath) {
        document.getElementById("hfAssetThumb").value = thumbnailPath;
        document.getElementById("hfAssetId").value = imageId;

        jQuery(".chkPicture").prop('checked', false);
        jQuery(".selected").removeClass("selected");
        jQuery(button).prop('checked', true);

        var parent = button.parentNode.parentNode;
        parent.className = parent.className + " selected";
    }

    // Updates appropriate fields in the containing page based upon selected image.
    function setIframeParentData() {
        var hfAssetThumb = document.getElementById("hfAssetThumb");
        var hfAssetId = document.getElementById("hfAssetId");

        if (hfAssetThumb.value
                && hfAssetThumb.value.length > 0
                && hfAssetId.value
                && hfAssetId.value.length > 0) {
            var parentImage = parent.document.getElementById("imgItem");
            var parentInput = parent.document.getElementById("hfMediaLibPhoto");
            var parentLblItem = parent.document.getElementById("lblItem");

            if (typeof (parentImage) !== "undefined" && parentImage !== null) {
                parentImage.src = hfAssetThumb.value;
            }

            if (typeof (parentInput) !== "undefined" && parentInput !== null) {
                parentInput.value = hfAssetId.value;
            }

            if (typeof (parentLblItem) !== "undefined" && parentLblItem !== null) {
                var filename = hfAssetThumb.value.substring(hfAssetThumb.value.lastIndexOf('/') + 1);
                parentLblItem.innerHTML = filename;
            }
        }

        closeLightBox();
    }

    // Closes the ligbox that contains this page, if any.
    function closeLightBox() {
        var parentCloseButton = parent.document.getElementById("cboxClose");

        if (typeof (parentCloseButton) !== "undefined" && parentCloseButton !== null) {
            parentCloseButton.click();
        }
    }

    // Hide updatepanel while loading ajax data.
    (function () {
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(
            function (sender, args) {
                document.getElementById("<%=udpLibraryPhotos.ClientID %>").style.display = "none";
            }
        );
        prm.add_endRequest(
            function (sender, args) {
                document.getElementById("<%=udpLibraryPhotos.ClientID %>").style.display = "";
            }
        );
    })();

    //--></script>
</asp:Content>