///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using KlausEnt.KEP.Kaneva;
using Kaneva.PresentationLayer.Shopping.usercontrols;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;

namespace Kaneva.PresentationLayer.Shopping
{
    public class MyInventory : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set text in search bar
            ((MainTemplatePage)Master).SearchTitle = "My Merchandise";
            Title = "Shop Kaneva - My Merchandise";

            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

            if (!IsPostBack)
            {
                //bind the CatalogItems
                BindItemData(1);


				// Track successful uploads via GA
				if (!String.IsNullOrEmpty(Request["up"]))
				{
					string category = "";
					if (!String.IsNullOrEmpty(Request["cat"]))
					{
						category = Request["cat"].ToString();
					}
					if (Request["up"].ToString() == "1")
					{	// GA Event
						Page.ClientScript.RegisterClientScriptBlock(GetType(), "gaUploadEvent", "callGAEvent('Shop', 'Upload', '" + category + "');", true);
					}
				}
            }
			
        }

        /// <summary>
        /// BindItemData
        /// </summary>
        private void BindItemData(int currentPage)
        {
            int pgSize = 50;
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            string filter = "";

            // Set current page
            pgTop.CurrentPageNumber = currentPage;

            if (ddlSort.SelectedValue.Length > 0)
            {
                filter = Server.HtmlEncode(ddlSort.SelectedValue) + "%";
            }

            PagedList<WOKItem> plWokItems = shoppingFacade.GetOwnerItems(filter, KanevaWebGlobals.CurrentUser.UserId, 0, pgSize, currentPage, "");
            
            lbl_NoCatalogItems.Visible = (plWokItems.Count.Equals(0));
            rptItems.DataSource = plWokItems;
            rptItems.DataBind();
            pgTop.NumberOfPages = Math.Ceiling((double)plWokItems.TotalCount / pgSize).ToString();
            pgTop.DrawControl();
        }
        public string GetItemPrice (object webPrice, object mktCost, object useType)
        {
            try
            {
                if (Convert.ToInt32 (useType) == WOKItem.USE_TYPE_BUNDLE)
                {
                    return mktCost.ToString ();
                }
                else
                {
                    return webPrice.ToString ();
                }
            }
            catch { }
            return webPrice.ToString ();
        }
        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            BindItemData(e.PageNumber);
        }
        protected void lnkDelete_Click(object sender, System.EventArgs e)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();

            CheckBox chkEdit;
            HtmlInputHidden hidGlboalId;
            int deletedItems = 0;

            foreach (RepeaterItem dgiAsset in rptItems.Items)
            {
                chkEdit = (CheckBox)dgiAsset.FindControl("chkEdit");

                if (chkEdit.Checked)
                {
                    hidGlboalId = (HtmlInputHidden)dgiAsset.FindControl("hidGlboalId");
                    
                    WOKItem updateItem = shoppingFacade.GetItem(Convert.ToInt32(hidGlboalId.Value));

                    updateItem.ItemActive = (int)WOKItem.ItemActiveStates.Deleted;

                    List<BundleItemChange> bundleChanges;
                    List<DeedItemChange> deedChanges;
                    shoppingFacade.UpdateShopItem(updateItem, KanevaWebGlobals.CurrentUser.UserId, false, false, out bundleChanges, out deedChanges);

                    foreach(BundleItemChange change in bundleChanges)
                    {
                        if (change.ModifiedBundle.ItemActive != change.OriginalBundle.ItemActive)
                        {
                            SendMessage(updateItem, change.ModifiedBundle);
                        }
                    }

                    //foreach (DeedItemChange change in deedChanges)
                    //{
                    //    if (change.ModifiedDeed.ItemActive != change.ModifiedDeed.ItemActive)
                    //    {
                    //        SendMessage(updateItem, change.ModifiedDeed);
                    //    }
                    //}

                    deletedItems++;
                }
            }

            BindItemData(1);
        }

        private bool GetMessageDetails(WOKItem item, WOKItem bundle, ref string subj, ref string msg)
        {
            // do not send message if item and bundle owner are the same
            if (item.ItemCreatorId == bundle.ItemCreatorId)
                return false;

            string itemOwnerName = GetUserFacade.GetUserName((int)item.ItemCreatorId);

            subj = "Your bundle was removed from shop";
            msg = itemOwnerName + " has removed his item, " + item.DisplayName + ", and it was contained in your bundle, " + bundle.DisplayName + ".<br/><br/>" +
                "This change invalidates your bundle and it has been removed from shop.  No follow up action is required.<br/><br/>" +
                "Thank you,<br/>" +
                "The Kaneva Team";

            return true;
        }

        private void SendMessage(WOKItem item, WOKItem bundle)
        {
            string subj = "";
            string msg = "";
            int fromUserId = 1;

            if (GetMessageDetails(item, bundle, ref subj, ref msg))
            {

                // Insert the message
                Message message = new Message(0, fromUserId, (int)bundle.ItemCreatorId, subj,
                    msg, new DateTime(), 0, (int)Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                GetUserFacade.InsertMessage(message);

                User userTo = GetUserFacade.GetUser((int)bundle.ItemCreatorId);
                if (userTo.UserId > 0)
                {
                    if (Convert.ToInt32(userTo.NotifyAnyoneMessages).Equals(1))
                    {
                        // They want emails for all messages
                        MailUtilityWeb.SendPrivateMessageNotificationEmail(userTo.Email, message.Subject, message.MessageId, message.MessageText, userTo.UserId, fromUserId);
                    }
                }
            }
        }

        /// <summary>
        /// ddlSort_OnSelectedIndexChanged
        /// </summary>
        protected void ddlSort_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            BindItemData(1);
        }

        protected Label lbl_NoCatalogItems;
        protected Repeater rptItems;
        protected DropDownList ddlSort;
        protected usercontrols.Pager pgTop;

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
        }
        #endregion
    }
}
