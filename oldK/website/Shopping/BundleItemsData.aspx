﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BundleItemsData.aspx.cs" Inherits="Kaneva.PresentationLayer.Shopping.BundleItemsData" %>
	
    <asp:repeater id="rptDeedItems" runat="server" >
		<itemtemplate>
			<div class="biRow" style="clear:both;margin-top:7px;display:block;height:26px;">
				<div class="floatLeft" style="width:25px;height:25px;border:1px solid #f0f0f0;margin-right:15px;overflow:hidden;">
                    <div class="img-center" style="width:25px;height:25px;">
                        <img style="width:100%;height:100%;" src='<%# GetItemImageURL (DataBinder.Eval (Container.DataItem, "ThumbnailSmallPath").ToString(), "sm")%>' />
                    </div>
                </div>
				<div class="floatLeft" style="margin-top:4px;">
                    <a href='ItemDetails.aspx?gId=<%# DataBinder.Eval (Container.DataItem, "GlobalId") %>'><%# DataBinder.Eval (Container.DataItem, "DisplayName") %></a>
				    <%# ShowBundleItemQuantity (DataBinder.Eval (Container.DataItem, "Quantity"), DataBinder.Eval (Container.DataItem, "UseType"))%>
                </div>	
			</div>
		</itemtemplate>
	</asp:repeater>
