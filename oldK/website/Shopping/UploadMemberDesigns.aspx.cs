///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Net;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Generic;

using log4net;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
	public class UploadMemberDesigns : BasePage
	{
        private static int previewWidth = 320;
		private static int previewHeight = 240;
		private static UGCUpload.eType uploadType = UGCUpload.eType.UNKNOWN;

		public UploadMemberDesigns()
		{
			autoPurchaseUploadedItem = Configuration.UGC_AutoPurchaseUploadedItem;
			autoPurchaseUploadedDerivativeItem = Configuration.UGC_AutoPurchaseUploadedDerivativeItem;
			uploadFeeCalculator = new UGCUtility.UploadFeeCalculator(Configuration.UGC_UploadFee_BasePriceFactor,
														   Configuration.UGC_UploadFee_BaseCommissionFactor,
														   Configuration.UGC_UploadFeeBase,
														   Configuration.UGC_UploadFeeFactor,
														   Configuration.CatalogChargeFactor,
														   Configuration.UGC_UploadFeeMin,
														   Configuration.UGC_UploadFeeMax);
			uploadFeeCurrency = Configuration.UGC_UploadFeeCurrency;
			derivativeUploadFeeCurrency = Configuration.UGC_DerivativeUploadFeeCurrency;
			autoPurchasedItemInvType = Configuration.UGC_AutoPurchasedItemInvType;
			autoPurchasedDerivativeItemInvType = Configuration.UGC_AutoPurchasedDerivativeItemInvType;
		}
        
		protected void Page_Load(object sender, EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect(GetLoginURL());
				return;
			}

			// update title
			Title = "Create a Custom Item";
			((MainTemplatePage)Master).SearchTitle = Title;

			string sUploadId = null;
			string sCommission = null;

			// Get upload ID
			if (IsPostBack)
			{
				// get upload id from form
				sUploadId = drpUGCUploads.SelectedValue;
			}
			else
			{
				// get upload id from params
				sUploadId = Request.Params["uploadid"];
				sCommission = Request.Params["commission"];
			}

			int uploadId = -1;
			if (sUploadId != null)
			{
				try
				{
					uploadId = Convert.ToInt32(sUploadId);
				}
				catch (System.Exception) { }
			}


			//commission
			if (sCommission != null)
			{
				int commission = 0;
				try
				{
					commission = Convert.ToInt32(sCommission);
				}
				catch (System.Exception) { }

				txtPrice.Text = Convert.ToString(commission);
			}

			// disable upload dropdown list for now
			drpUGCUploads.Enabled = false;      // disable dropdown for now

			// Load pending uploads from DB (refresh even if IsPostBack)
			uploadList = UGCUtility.GetUploadedUGCList(KanevaWebGlobals.CurrentUser.UserId, uploadId, true);
			drpUGCUploads.DataTextField = "Name";
			drpUGCUploads.DataValueField = "ID";
			drpUGCUploads.DataSource = uploadList;
			drpUGCUploads.DataBind();
			if (sUploadId != null)
			{
				int selId = -1;
				for (int i = 0; i < drpUGCUploads.Items.Count; i++)
				{
					if (drpUGCUploads.Items[i].Value == sUploadId)
					{
						selId = i;
						break;
					}
				}

				if (selId != -1)
					drpUGCUploads.SelectedIndex = selId;
			}



			if (IsPostBack)
			{
				//Check if current upload ID is still available

				bool bFound = false;

				for (int i = 0; i < uploadList.Count; i++)
				{
					if (uploadList[i].ID == uploadId)
					{
						bFound = true;
						break;
					}
				}

				if (!bFound)
				{
					Response.Redirect(Request.Url.AbsolutePath + "?uploadid=" + sUploadId);
					return;
				}
			}
			else
			{
				// Set to default image first
				imgPreview.ImageUrl = GeneratePreviewImage("");
				
				imgPreview.Width = previewWidth;
				imgPreview.Height = previewHeight;

				// disable controls
				EnableAllControls(false);

				//populate the pass type drop down
				PopulatePassGroupList();


				//temporary solution relying on certain assumptions
				//filtering out male female categories
				PopulateCategoryList();

				//if (KanevaWebGlobals.CurrentUser.Age < 18)
				//{
				//    litAccessPass.Text = "style=\"display:none\"";
				//}

				if (drpUGCUploads.Items.Count == 0)
				{
					drpUGCUploads.Items.Insert(0, new ListItem("-- No pending uploads --", "-1"));
					sUploadId = null;

					// todo: redirect to a page explaining how to upload ugc items
				}
				else
				{
					drpUGCUploads.Items.Insert(0, new ListItem("-- Select an uploaded file --", "-1"));
					if (sUploadId == null)
						drpUGCUploads.SelectedIndex = 0;

					// update UI
					drpUGCUploads_SelectedIndexChanged(null, null);
				}

                // If this is a D.O. animation configure page as needed
                UGCUpload currItem = GetUploadItemByID(drpUGCUploads.SelectedValue);
                currItem.AnimTargetActorGLID = UGCUtility.GetTargetActorGlid(currItem);
                if (currItem.IsDOAnimation)
                {
                    divCategory.Style.Add ("display", "none");
                    spnNameLabel.InnerText = "Animation";
                    rfvCategory.Enabled = false;
                }
                else
                {
                    spnNameLabel.InnerText = "Item";
                    divCategory.Style.Add ("display", "block");
                    rfvCategory.Enabled = true;
                }

                // Default name and description if passed
                if (Request.Params["name"] != null)
                {
                    txtTitle.Text = Request.Params["name"].ToString();
                }
                if (Request.Params["description"] != null)
                {
                    txtDescription.Text = Request.Params["description"].ToString();
                }
            }
		}

		protected void ValidationFailed(string errorMessage)
		{
			ShowErrorOnStartup(errorMessage);
			// Call javascript to hide upload progress
			if (MagicAjax.MagicAjaxContext.Current.IsAjaxCall)
				MagicAjax.AjaxCallHelper.Write("HideUploadProgress();");
		}

        /// <summary>
        /// btnConfirm_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConfirm_Click(object sender, EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect(GetLoginURL());
				return;
			}

            // Process inputs.
            string itemName = Server.HtmlEncode((txtTitle.Text ?? string.Empty).Trim());
            string itemDesc = Server.HtmlEncode((txtDescription.Text ?? string.Empty).Trim());
            string itemKW = Server.HtmlEncode((txtKeywords.Text ?? string.Empty).Trim());

            // Only items that should be rewards are Kaneva items. User 4230696 is the only user id
            // that we recognize uploading Kaneva items. This id is also in WOKItem.IsKanevaOwned property.
            // If we need other users to be able to upload Kaneva items we'll have to add the user it here
            // and in WOKItem
            string userSelectedCurrency = Constants.CURR_CREDITS; // (pnlPayWith.Visible && rdoRewards.Checked ? Constants.CURR_REWARDS : Constants.CURR_CREDITS);
            if (KanevaWebGlobals.CurrentUser.UserId == 4230696)
            {
                userSelectedCurrency = Constants.CURR_REWARDS;
            }

            WOKItem.ItemActiveStates itemStatus = rdoPublic.Checked ? WOKItem.ItemActiveStates.Public : WOKItem.ItemActiveStates.Private;

            UGCUpload currItem = GetUploadItemByID(drpUGCUploads.SelectedValue);
            if (currItem == null)
            {
                ValidationFailed("Unable to proceed: unrecognized upload ID. Please resubmit your design from Kaneva 3D Client.");
                return;
            }

            currItem.AnimTargetActorGLID = UGCUtility.GetTargetActorGlid(currItem);
            currItem.DefaultGlid = Request["defaultGlid"] ?? string.Empty;

            int itemPassType = -1;
            if (!Int32.TryParse(ddl_PassType.SelectedValue, out itemPassType))
            {
                ValidationFailed("Sorry we were unable to process your pass type.");
                return;
            }

            int category = 0;
            if(!Int32.TryParse(ddlCategories.SelectedValue, out category))
            {
                ValidationFailed("Sorry we were unable to process your category.");
                return;
            }

            uint designerPrice;
            if (!GetDesignerPrice(txtPrice.Text, out designerPrice))
            {
                ValidationFailed("Please enter a valid commission amount");
                return;
            }

            // Finalize the upload.
            string errMsg;
            UGCUtility.FinalizeUGCUploadToShop(KanevaWebGlobals.CurrentUser, currItem, chkDerivable.Checked, itemName, itemDesc,
                itemKW, Common.GetVisitorIPAddress(), Convert.ToInt32(ddlCategories.SelectedValue), itemPassType,
                itemStatus, designerPrice, userSelectedCurrency, chkBlastUpload.Checked, out errMsg);

            // Return error if confirmation failed.
            if (!string.IsNullOrWhiteSpace(errMsg))
            {
                ValidationFailed(errMsg);
                return;
            }
		
			// Track suucessful upload via GA
			Response.Redirect(ResolveUrl("~/MyInventory.aspx?up=1&c=" + category));
		}

		protected void btnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/MyInventory.aspx");
		}

		protected void drpUGCUploads_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				UGCUpload currItem = GetUploadItemByID(drpUGCUploads.SelectedValue);

				lblUploadTime.Text = "";
				lblUploadSummary.Text = "";
				if (currItem == null)
				{
					// disable controls
					EnableAllControls(false);

					// ---select an uploaded file---
					lblUploadTime.Text = "Please select an uploaded file from list above.";
					lblUploadSummary.Text = "";

					if (Request.Params["uploadid"] != null)
						ShowErrorOnStartup("Upload #" + Request.Params["uploadid"] + " not found. Either upload was incomplete or you have already finished item creation.", false);
				}
				else
				{
					// enable controls
					EnableAllControls(true);

					// Preview image
					imgPreview.ImageUrl = GeneratePreviewImage(drpUGCUploads.SelectedValue);
					imgPreview.Width = previewWidth;
					imgPreview.Height = previewHeight;

					lblUploadTime.Text = Convert.ToString(currItem.UpdateTime);
					lblUploadSummary.Text = Convert.ToString(currItem.Summary);
					hidUploadType.Value = Convert.ToString((int)currItem.Type);

					string txnCurrency = null;
					if (currItem.IsDerivation)
					{
						chkDerivable.Checked = false;
						chkDerivable.Disabled = true;
						trBaseCommission.Visible = true;
						divUploadFee.Visible = autoPurchaseUploadedDerivativeItem; // hide upload fee panel if item is not automatically purchased on creation
                        txnCurrency = UGCUtility.GetUploadFeeCurrency((int)currItem.Type, true);
					}
					else
					{
                        // check uploaded textures
                        int numTextures = 0;
                        IList<UGCUpload.eType> textureUploadTypes;
                        UGCUtility.GetTextureUploadTypesFromMasterUploadType(currItem.Type, out textureUploadTypes);
                        if (textureUploadTypes != null && textureUploadTypes.Count > 0)
                        {
                            DataTable dtTextures = UGCUtility.GetUGCUploads(KanevaWebGlobals.CurrentUser.UserId, textureUploadTypes, currItem.ID, null, -1);
                            if (dtTextures != null)
                                numTextures = dtTextures.Rows.Count;
                        }

						chkDerivable.Checked = numTextures > 0;    // check derivable option by default
						chkDerivable.Disabled = numTextures == 0;  // If no textures, disable derivable option
						trBaseCommission.Visible = false;
						divUploadFee.Visible = autoPurchaseUploadedItem; // hide upload fee panel if item is not automatically purchased on creation
                        txnCurrency = UGCUtility.GetUploadFeeCurrency((int)currItem.Type, false);
					}

                    if (txnCurrency == Constants.CURR_CREDITS)
                    {
                        imgUploadFeeCurrency.ImageUrl = "~/images/silver_coins.jpg";

                        pnlPayWith.Visible = false;
                    }
                    else if (txnCurrency == Constants.CURR_REWARDS)
                    {
                        imgUploadFeeCurrency.ImageUrl = "~/images/gold_coins.jpg";

                        pnlPayWith.Visible = false;
                    }
                    else if (txnCurrency == Constants.CURR_USERSELECTED)
                    {
                        // Show both images?
                        imgUploadFeeCurrency.ImageUrl = "~/images/gold_coins.jpg";

                        // All UGC items are automatically credits only, no user selection so don't show drop down
                        pnlPayWith.Visible = false;
                    }

				}
			}
			catch (System.Exception)
			{
			}

			UpdatePrice();
		}

		protected void txtPrice_TextChanged(object sender, EventArgs e)
		{
			UpdatePrice();
		}

		protected void UpdatePrice()
		{
			lblBasePrice.Text = "";
			lblUploadFee.Text = "";
			lblCommission.Text = "";
			lblCatalogCharge.Text = "";
			lblSellingPrice.Text = "";

			UGCUpload currItem = GetUploadItemByID(drpUGCUploads.SelectedValue);

			if (currItem != null)
			{
				lblBasePrice.Text = Convert.ToString(currItem.BasePrice);
				int uploadFee = -1;
				// calculate upload fee
				uploadFee = uploadFeeCalculator.getUploadFee((int)currItem.Type, currItem.BasePrice, currItem.BaseCommission, currItem.IsDerivation);
				lblUploadFee.Text = Convert.ToString(uploadFee);
				lblBaseCommission.Text = Convert.ToString(currItem.BaseCommission);

				uint commission;
				if (GetDesignerPrice(txtPrice.Text, out commission))
				{
					int priceSubtotal = Convert.ToInt32(currItem.BasePrice + currItem.BaseCommission + commission);
					int catalogCharge = (int)(priceSubtotal * Configuration.CatalogChargeFactor);
					int sellingPrice = priceSubtotal + catalogCharge;
					lblCommission.Text = Convert.ToString(commission);
					lblCatalogCharge.Text = Convert.ToString(catalogCharge);

                    if (uploadType == UGCUpload.eType.ACTIONITEM && Request["bundleGlids"] != null)
                    {
                        // Handle special case for Smart Objects since they are bundles
                        // WOKItem bundleToUpdatePrice = shoppingFacade.GetItem(bundleGLID);
                        var shoppingFacade = GetShoppingFacade;

                        string[] arrGlids = Request["bundleGlids"].Split(',');
                        WOKItem wokItem;
                        List<WOKItem> list = new List<WOKItem>();

                        foreach (string glid in arrGlids)
                        {
                            wokItem = shoppingFacade.GetItem(Convert.ToInt32(glid));
                            list.Add(wokItem);
                        }

                        BundlePrice bundlePrice = shoppingFacade.CalculateBundlePrice(list, (int)commission, KanevaWebGlobals.CurrentUser.UserId);
                        // For Bundles Only: The stored market price is the actual web price
                        lblSellingPrice.Text = bundlePrice.TotalBundle.ToString ();  
                    }
                    else
                    {
					    lblSellingPrice.Text = Convert.ToString(sellingPrice);
                    }
				}
				else
				{
					m_logger.Error("Bad designer commission input \"" + txtPrice.Text + "\" passed form validation");
				}

              
			}
		}

		private void PopulatePassGroupList()
		{
			ddl_PassType.DataSource = WebCache.GetPassGroupListFiltered(((int)Constants.ePASS_TYPE.ACCESS).ToString() + "," + ((int)Constants.ePASS_TYPE.GENERAL).ToString());
			ddl_PassType.DataTextField = "name";
			ddl_PassType.DataValueField = "pass_group_id";
			ddl_PassType.DataBind();

			ddl_PassType.Items.Insert(0, new ListItem("-- Select --", "-1"));
			ddl_PassType.SelectedValue = "-1";
		}

		private void PopulateCategoryList()
		{
			int uploadId = 0;

			if (Request.Params["uploadid"] != null)
			{
				uploadId = Convert.ToInt32(Request.Params["uploadid"]);
			}
			
			// Debug message shown?
			lblUploadType.Visible = false;

            if (uploadType == UGCUpload.eType.SND_OGG_KRX)
            {
                imgPreview.ImageUrl = UGCUtility.GetDefaultPreviewSoundImageUrl();
            }

            // SET Cat DDL to show correct choices.
            List<ItemCategory> lItemCategory = UGCUtility.GetValidCategoryListForUpload(uploadId);
            ddlCategories.DataSource = lItemCategory;
            ddlCategories.DataTextField = "Name";
            ddlCategories.DataValueField = "ItemCategoryId";
            ddlCategories.DataBind();

            ddlCategories.Items.Insert(0, new ListItem("-- Select --", "-1"));
            ddlCategories.SelectedValue = "-1";
		}

		protected string GeneratePreviewImage(string uploadId)
		{
			int maxWidth = 100, maxHeight = 100;
			UGCUtility.GetDefaultPreviewImageSize(ref maxWidth, ref maxHeight);

			// default image
			string imageUrl = UGCUtility.GetDefaultPreviewImageUrl();
            previewWidth = maxWidth;
            previewHeight = maxHeight;

            // Handle "defaultGlid" parameter for smart objects (no longer used?)
            if (Request["defaultGlid"] != null)
            {
                // Reuse defaultGlid's thumbnail
                WOKItem wiDefaultGlid = null;
                try
                {
                    wiDefaultGlid = GetShoppingFacade.GetItem(Convert.ToInt32(Request["defaultGlid"]));
                }
                catch (System.Exception) { }

                if (wiDefaultGlid != null)
                {
                    // TextureFilestore should already be written into ThumbnailLargePath
                    imageUrl = KanevaGlobals.TextureServer + "/" + wiDefaultGlid.ThumbnailLargePath;
                    previewWidth = 100;
                    previewHeight = 100;
                }

                return imageUrl;
            }

            if (uploadId == "")
            {
                //m_logger.Info("GeneratePreviewImage(): uploadId not provided - use default thumbnail");
                return imageUrl;
            }

            // UGC uploads - thumbnails generated and uploaded from the client
			int nUploadId = 0;
            try
            {
                nUploadId = Convert.ToInt32(uploadId);
            }
            catch (System.Exception)
            {
                m_logger.Warn("GeneratePreviewImage: invalid uploadId \"" + uploadId + "\" - use default thumbnail");
                return imageUrl;
            }

			if (nUploadId != 0)
			{
				string previewImageName = uploadId + ".jpg";
				string previewImagePath = KanevaGlobals.TexturePath + "\\" + KanevaWebGlobals.CurrentUser.UserId.ToString() + "\\uploadTemp\\" + previewImageName;

				// Try generating preview image if not already exist
                if (!UGCUtility.AssetFileExists(previewImagePath))
				{
					// Handle icons
					DataTable dtIcons = UGCUtility.GetUGCUploads(KanevaWebGlobals.CurrentUser.UserId, UGCUpload.eType.ICON, nUploadId);
					if (dtIcons != null && dtIcons.Rows.Count > 0)
					{
						DataRow drIcon = dtIcons.Rows[0];   //@@Temp: Use 'first' icon by default

						string iconFullPath = null;
						try
						{
							iconFullPath = Convert.ToString(drIcon["uploadName"]);
						}
						catch (System.Exception) {}

						if (iconFullPath == null)
						{
                            m_logger.Warn("GeneratePreviewImage(" + uploadId + "): error obtaining thumbnail uploadName - use default thumbnail");
                            return imageUrl;
                        }

						System.Drawing.Image img = null;

						try
						{
                            if (Configuration.UseAmazonS3Storage)
                            {
                                string originalImage = (Configuration.ImageServer + "/" + iconFullPath).Replace("\\", "/");

                                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(originalImage);
                                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                                   
                                System.IO.Stream receiveStream = httpWebResponse.GetResponseStream();
                                img = System.Drawing.Image.FromStream(receiveStream);
                            }
                            else
                            {
                                img = System.Drawing.Image.FromFile(iconFullPath);
                            }

							ImageHelper.SaveThumbnail(ImageHelper.TEMP_IMAGE, img, maxHeight, maxWidth, "", "",
								previewImageName, iconFullPath, KanevaGlobals.TexturePath, KanevaWebGlobals.CurrentUser.UserId, nUploadId, ".jpg");
						}
						catch (System.Exception ex)
                        {
                            m_logger.Error("GeneratePreviewImage(" + uploadId + "): error saving thumbnail image - source=[" + iconFullPath + "], dest=[" + previewImagePath + "], exception=[" + ex.ToString() + "]");
                        }
						finally
						{
							if (img != null)
								img.Dispose();
						}
					}
				}

				// Check if preview image is generated
                if (!UGCUtility.AssetFileExists(previewImagePath))
                {
                    m_logger.Warn("GeneratePreviewImage(" + uploadId + "): thumbnail [" + previewImagePath + "] not generated - use default thumbnail");
                    return imageUrl;
                }
                else
                {
				    System.Drawing.Image img = null;

				    try
				    {
                        // Get image size information
                        if (Configuration.UseAmazonS3Storage)
                        {
                            string originalImage = (Configuration.ImageServer + "/" + previewImagePath).Replace("\\", "/");

                            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(originalImage);
                            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                            System.IO.Stream receiveStream = httpWebResponse.GetResponseStream();
                            img = System.Drawing.Image.FromStream(receiveStream);
                        }
                        else
                        {
                            img = System.Drawing.Image.FromFile(previewImagePath);
                        }

					    previewWidth = img.Width;
					    previewHeight = img.Height;

                        m_logger.Debug("GeneratePreviewImage(" + uploadId + "): thumbnail [" + previewImagePath + "] width [" + previewWidth.ToString() + "] height [" + previewHeight.ToString() + "]");

					    // Set image URL
					    imageUrl = KanevaGlobals.TextureServer + "/" + KanevaGlobals.TextureFilestore + KanevaWebGlobals.CurrentUser.UserId.ToString() + "/uploadTemp/" + previewImageName;
				    }
				    catch (System.Exception ex)
                    {
                        m_logger.Error("GeneratePreviewImage(" + uploadId + "): error obtaining thumbnail image info for [" + previewImagePath + "], exception [" + ex.ToString() + "]");
                    }
				    finally
				    {
					    if (img != null)
						    img.Dispose();
				    }
                }
            }

			return imageUrl;
		}

		protected UGCUpload GetUploadItemByID(string sUploadId)
		{
			if (uploadList == null || sUploadId == null)
				return null;

			int uploadId = 0;
			try
			{
				uploadId = Convert.ToInt32(sUploadId);
			}
			catch (System.Exception)
			{
				return null;
			}

			for (int i = 0; i < uploadList.Count; i++)
			{
				if (uploadList[i].ID == uploadId)
					return uploadList[i];
			}

			return null;
		}

		protected void EnableAllControls(bool bEnable)
		{
			txtTitle.Enabled = bEnable;
			ddlCategories.Enabled = bEnable;
			txtDescription.Enabled = bEnable;
			txtKeywords.Enabled = bEnable;
			rdoPublic.Disabled = !bEnable;
			ddl_PassType.Enabled = bEnable;
			txtPrice.Enabled = bEnable;
			chkDerivable.Disabled = !bEnable;
            btnConfirm.Enabled = bEnable;
			btnCancel.Enabled = bEnable;
            rdoCredits.Disabled = !bEnable;
            rdoRewards.Disabled = !bEnable;
		}

		protected bool GetDesignerPrice(string userInput, out uint designerPrice)
		{
			designerPrice = 0;

			if (userInput.Trim().Length > 0)
			{
				try
				{
					designerPrice = Convert.ToUInt32(userInput.Trim());
				}
				catch (Exception)
				{
					return false;
				}

				if (designerPrice > 5000000)
				{
					designerPrice = 5000000;
				}
			}

			return true;
		}

        private string GetUploadFeeCurrencyValue(string txnCurrency, string userSelectedCurrency)
        {
            if (txnCurrency == Constants.CURR_USERSELECTED)
            {
                // User selected is allowable, set based on thier choice
                if (userSelectedCurrency == Constants.CURR_REWARDS)
                {
                    return Constants.CURR_REWARDS;
                }
                else
                {
                    return Constants.CURR_CREDITS;
                }
            }

            return txnCurrency;
        }

        private int GetAutoPurchaseInventoryType( int uploadType, bool bDerive )
        {
            if( bDerive )
                return autoPurchasedDerivativeItemInvType.GetValueInt(uploadType, -1);
            else
                return autoPurchasedItemInvType.GetValueInt(uploadType, -1);
        }

		protected Literal litAccessPass;

		protected TextBox txtTitle, txtDescription, txtKeywords, txtPrice;
		protected HtmlInputCheckBox chkRestricted;
        protected HtmlInputRadioButton rdoPublic, rdoPrivate, rdoCredits, rdoRewards;
		protected Button btnConfirm, btnCancel;
		protected HtmlInputCheckBox chkDerivable, chkBlastUpload;
        protected Panel pnlPayWith;
        protected HtmlGenericControl spnNameLabel;
        protected HtmlGenericControl divCategory;
        protected RequiredFieldValidator rfvCategory;

		protected DropDownList drpUGCUploads;
		protected DropDownList ddlCategories;
		protected Label lblUploadTime, lblUploadSummary, lblBasePrice, lblUploadFee, lblCommission, lblCatalogCharge, lblSellingPrice, lblBaseCommission;
		protected Label lblUploadType;

		protected Image imgPreview;
		protected HtmlTableRow trBaseCommission;
		protected DropDownList ddl_PassType;

		protected HtmlGenericControl divUploadFee;
		protected HtmlGenericControl divPublicAccess;
		protected Image imgUploadFeeCurrency;

		protected HiddenField hidUploadType;

		protected bool autoPurchaseUploadedItem, autoPurchaseUploadedDerivativeItem;
		protected UGCUtility.UploadFeeCalculator uploadFeeCalculator;
		protected OptionValueMap uploadFeeCurrency, derivativeUploadFeeCurrency;
        protected OptionValueMap autoPurchasedItemInvType, autoPurchasedDerivativeItemInvType;

		protected IList<UGCUpload> uploadList;

		/// <summary>
		/// Logger
		/// </summary>
		private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
	}
}
