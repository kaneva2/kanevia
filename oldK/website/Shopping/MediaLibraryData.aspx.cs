///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using KlausEnt.KEP.Kaneva;
using System.Data;
using System.Web.UI.HtmlControls;
using Kaneva.PresentationLayer.Shopping.usercontrols;

namespace Kaneva.PresentationLayer.Shopping
{
    public partial class MediaLibraryData : BasePage
    {
        #region Declarations

        private int itemsPerPage = 20;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData(KanevaWebGlobals.GetUserId(), KanevaWebGlobals.CurrentUser.CommunityId);
            }
        }

        /// <summary>
        /// BindData
        /// </summary>
        private void BindData(int userId, int channelId)
        {
            // Groups
            ddlGroup.Items.Add(new ListItem("All", "-1"));

            if (userId != -1)
            {
                MediaFacade mediaFacade = new MediaFacade();
                PagedList<AssetGroup> pdtAssetGroups = mediaFacade.GetAssetGroups(channelId, "", "", 1, Int32.MaxValue);
                if (pdtAssetGroups.Count > 0)
                {
                    for (int i = 0; i < pdtAssetGroups.Count; i++)
                    {
                        ddlGroup.Items.Add(new ListItem(pdtAssetGroups[i].Name.ToString(), pdtAssetGroups[i].AssetGroupId.ToString()));
                    }
                }
            }

            int group_id = Convert.ToInt32(ddlGroup.SelectedValue);
            BindPictures(group_id, userId, channelId, 1);
        }

        private void BindPictures(int group_id, int user_id, int channelId, int pageNum)
        {
            PagedDataTable pds;

            // if the group id is valid, get pictures for that group
            if (group_id != -1)
            {
                string orderby = "asset_id" + " " + "ASC";

                pds = StoreUtility.GetAssetsInGroup(group_id, channelId, (int)Constants.eASSET_TYPE.PICTURE, orderby, "", pageNum, itemsPerPage);
            }
            // otherwise get all pics
            else
            {
                string orderby = "created_date" + " " + "ASC";
                string filter = "a.status_id <> " + (int)Constants.eASSET_STATUS.MARKED_FOR_DELETION;
                int assetTypeId = (int)Constants.eASSET_TYPE.PICTURE;

                pds = StoreUtility.GetAssetsInChannel(channelId, true, true, assetTypeId, filter, orderby, pageNum, itemsPerPage);
            }

            // Set control visibility.
            lblNoAssets.Visible = (pds.TotalCount == 0);
            divMediaLibraryPhotos.Visible = (pds.TotalCount > 0);
            btnSave.Visible = (pds.TotalCount > 0);

            dlPictures.DataSource = pds;
            dlPictures.DataBind();

            pgBottom.CurrentPageNumber = pageNum;
            pgBottom.NumberOfPages = Math.Ceiling((double)pds.TotalCount / itemsPerPage).ToString();
            pgBottom.DrawControl();
        }

        /// <summary>
        /// dlPictures_ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dlPictures_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;
                HtmlGenericControl spnImageTitle = (HtmlGenericControl)e.Item.FindControl("spnImageTitle");

                if (spnImageTitle != null)
                {
                    spnImageTitle.InnerText = KanevaGlobals.TruncateWithEllipsis(drv["name"].ToString(), 13);
                }
            }
        }

        protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            int group_id = Convert.ToInt32(ddlGroup.SelectedValue);

            BindPictures(group_id, KanevaWebGlobals.GetUserId(), KanevaWebGlobals.CurrentUser.CommunityId, 1);
        }

        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            int group_id = Convert.ToInt32(ddlGroup.SelectedValue);

            BindPictures(group_id, KanevaWebGlobals.GetUserId(), KanevaWebGlobals.CurrentUser.CommunityId, e.PageNumber);
        }

        #region Helper Methods

        protected string GetMediaImageURL(string imagePath, string size, int assetId, int assetTypeId)
        {
            return StoreUtility.GetMediaImageURL(imagePath, size, assetId, assetTypeId, 0, Page);
        }

        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pgBottom.PageChanged += new PageChangeEventHandler(pg_PageChange);
        }
        #endregion
    }
}