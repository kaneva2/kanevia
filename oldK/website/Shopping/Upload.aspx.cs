///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;

using log4net;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

using Amazon.S3;
using Amazon.S3.Transfer;

namespace Kaneva.PresentationLayer.Shopping
{
    public class Upload : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

            if (!IsPostBack)
            {
                List<ItemTemplate> lItemTemplates = WebCache.GetCachedTemplates();

                imgItem.Src = StoreUtility.GetItemImageURL(string.Empty, "la");

                // The Dropdown
                cboTextureType.DataTextField = "Name";
                cboTextureType.DataValueField = "GlobalId";
                cboTextureType.DataSource = lItemTemplates;
                cboTextureType.DataBind();

                cboTextureType.Items.Insert(0, new ListItem("Select an Item", ""));
                cboTextureType.SelectedIndex = 0;

                //populate the pass type drop down
                PopulatePassGroupList();

                // The pictures of the dropdown
                rptTextures.DataSource = lItemTemplates;
                rptTextures.DataBind();

                // Build Javascript
                StringBuilder strJS = new StringBuilder (100);
                foreach (ItemTemplate itemTemplate in lItemTemplates)
                {
                    strJS.Append ("$('text" + itemTemplate.GlobalId.ToString () + "').style.display = 'none';");
                }

                litJS.Text = strJS.ToString ();
            }

            Title = "Upload a Custom Texture";
            ((MainTemplatePage)Master).SearchTitle = "Upload a Custom Texture";

            if (KanevaWebGlobals.CurrentUser.Age < 19)
            {
                litAccessPass.Text = "style=\"display:none\"";
            }
         }

        private void PopulatePassGroupList()
        {
            ddl_PassType.DataSource = WebCache.GetPassGroupListFiltered(((int)Constants.ePASS_TYPE.ACCESS).ToString() + "," + ((int)Constants.ePASS_TYPE.GENERAL).ToString());
            ddl_PassType.DataTextField = "name";
            ddl_PassType.DataValueField = "pass_group_id";
            ddl_PassType.DataBind();

            ddl_PassType.Items.Insert(0, new ListItem("-- Select --", "-1"));
            ddl_PassType.SelectedValue = "-1";

        }
        private void ShowError (string msg)
        {
            cvGeneral.ErrorMessage += cvGeneral.ErrorMessage.Length > 0 ? "<br/>" + msg : msg;
            cvGeneral.IsValid = false;
        }
        /// <summary>
        /// btnUpload_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

            ShoppingFacade shoppingFacade = new ShoppingFacade();
            WOKItem item = new WOKItem();
            Int32 baseGlobalId = 0;
            bool isErr = false;

            if (KanevaGlobals.IsPottyMouth (txtTitle.Text.Trim ()))
            {
                ShowError ("Item Name includes one or more restricted words.");
                rfvTitle.IsValid = false;
                rfvTitle.ErrorMessage = "";
                isErr = true;
            }
            if (KanevaGlobals.IsPottyMouth (txtDescription.Text.Trim ()))
            {
                ShowError ("Description includes one or more restricted words.");
                rfvDesc.IsValid = false;
                rfvDesc.ErrorMessage = "";
                isErr = true;
            }
            if (KanevaGlobals.IsPottyMouth (txtKeywords.Text.Trim ()))
            {
                ShowError ("Tags includes one or more restricted words.");
                cvTags.IsValid = false;
                isErr = true;
            }

            // Figure out the base item from the filename
            HttpPostedFile fTexture = inpTexture.PostedFile;
            if (fTexture == null || fTexture.ContentLength.Equals(0))
            {
                rfvTexture.IsValid = false;
                isErr = true;
            }
            else
            {
                string strExtensionUpperCase = Path.GetExtension(fTexture.FileName).ToUpper();
                if (!strExtensionUpperCase.Equals(".JPG") && !strExtensionUpperCase.Equals(".JPEG"))
                {
                    revTexture.IsValid = false;
                    isErr = true;
                }
            }

            // Validate image upload. Must be either nothing, a media library photo, or an upload.
            int assetId = 0;
            HttpPostedFile fThumbnail = null;
            if (string.IsNullOrWhiteSpace(hfMediaLibPhoto.Value))
            {
                // Get the thumbnail to upload
                fThumbnail = inpThumbnail.PostedFile;
                if (fThumbnail == null || fThumbnail.ContentLength.Equals(0))
                {
                    revThumbnail.IsValid = false;
                    isErr = true;
                }
                else if (fThumbnail.ContentLength > KanevaGlobals.MaxUploadedImageSize)
                {
                    revThumbnail.IsValid = false;
                    revThumbnail.ErrorMessage = "Thumbnail Image is too large, please select a photo smaller than " + KanevaGlobals.FormatImageSize(KanevaGlobals.MaxUploadedImageSize) + ".";
                    isErr = true;
                }
                else
                {
                    // Thumbnail must be a JPG, JPEG, GIF!!!
                    string strExtensionUpperCase = Path.GetExtension(fThumbnail.FileName).ToUpper();
                    if (!strExtensionUpperCase.Equals(".JPG") && !strExtensionUpperCase.Equals(".JPEG") &&
                        !strExtensionUpperCase.Equals(".GIF"))
                    {
                        revThumbnail.IsValid = false;
                        isErr = true;
                    }
                }
            }
            else if (!Int32.TryParse(hfMediaLibPhoto.Value, out assetId) || assetId <= 0)
            {
                revThumbnail.IsValid = false;
                isErr = true;
            }

            // If any of the above validations failed, return to page and display error messages
            if (isErr)
            {
                return;
            }

    
            // Get the base global id from the dropdown
            try
            {
                //fileName = System.IO.Path.GetFileNameWithoutExtension(fTexture.FileName);
                baseGlobalId = Convert.ToInt32(cboTextureType.Value);
            }
            catch (Exception)
            {
                ShowError("Error finding original item, please make sure filename has not been modified.");
                return;
            }


            // Make sure it is a valid base global id allowed for customizing
            WOKItem baseItem = shoppingFacade.GetItem(baseGlobalId);

            if (baseItem == null || baseItem.TemplatePath.Length.Equals(0))
            {
                ShowError("This template does not have a valid item.");
                return;
            }

            // QH - Access Pass UGC. Certain items are flagged as access pass and by default sets themselves as access pass flagged items on upload 
            if (baseItem.PassTypeId == (int)Constants.ePASS_TYPE.ACCESS)
            {
                item.PassTypeId = baseItem.PassTypeId;
            }
            else
            {
                //get the selected pass type
                try
                {
                    item.PassTypeId = Convert.ToInt32(ddl_PassType.SelectedValue);
                    if (item.PassTypeId < 0)
                    {
                        rfvPassType.IsValid = false;
                        return;
                    }
                }
                catch (FormatException)
                {
                }
            }

            item.BaseGlobalId = Convert.ToUInt32 (baseGlobalId);

            // Get the other params
            item.ItemCreatorId = Convert.ToUInt32 (KanevaWebGlobals.CurrentUser.UserId);
            item.Name = Server.HtmlEncode(txtTitle.Text.Trim());
            item.DisplayName = Server.HtmlEncode(txtTitle.Text.Trim());
            item.Description = Server.HtmlEncode(txtDescription.Text.Trim());
            item.Keywords = Server.HtmlEncode(txtKeywords.Text.Trim());
            item.ArmAnywhere = baseItem.ArmAnywhere;
            item.DestroyWhenUsed = baseItem.DestroyWhenUsed;
            item.Disarmable = baseItem.Disarmable;
            item.InventoryType = Kaneva.BusinessLayer.Facade.Configuration.UGC_SellingCurrency;
            item.ItemActive = rdoPublic.Checked ? (int)WOKItem.ItemActiveStates.Public : (int)WOKItem.ItemActiveStates.Private; 
            item.MarketCost = baseItem.MarketCost;
            item.RequiredSkill = baseItem.RequiredSkill;
            item.RequiredSkillLevel = baseItem.RequiredSkillLevel;
            item.SellingPrice = 0;
            item.Stackable = baseItem.Stackable;
            item.UseType = baseItem.UseType;
            item.DerivationLevel = WOKItem.DRL_ITEMRETEX;

            // Copy use value from base item (for UGC furniture)
            ItemParameter itemParameter = shoppingFacade.GetItemParameter(baseItem.GlobalId, baseItem.UseType);
            if (itemParameter != null)
            {
                try
                {
                    item.UseValue = Convert.ToInt32(itemParameter.ParamValue);
                }
                catch (System.Exception) { }
            }

            try
            {
                item.DesignerPrice = Convert.ToUInt32 (txtPrice.Text.Trim());
            }
            catch (Exception){}

            if (item.DesignerPrice > 5000000)
            {
                item.DesignerPrice = 5000000;
            }

            // Add it to the db
            int globalId = shoppingFacade.AddCustomItem(item);
            
            // Copy the categories
            List<ItemCategory> itemCatagories = shoppingFacade.GetItemCategoriesByItemId(baseItem.GlobalId);
            int categoryIndex = 1;

            foreach (ItemCategory itemCategory in itemCatagories)
            {
                shoppingFacade.AddCategoryToItem (globalId, itemCategory.ItemCategoryId, categoryIndex);
                categoryIndex++;
            }

            // Save custom texture to the NAS
            string strTextureFilename = "";
            string strPathUserIdGlobalId = Path.Combine (KanevaWebGlobals.CurrentUser.UserId.ToString (), globalId.ToString());
            string strFilePathTexture = Path.Combine(KanevaGlobals.TexturePath, strPathUserIdGlobalId);
            ImageHelper.UploadImageFromUser(ref strTextureFilename, fTexture, strFilePathTexture, KanevaGlobals.MaxUploadedImageSize);

            // Update the path to the texture
            string texturePathNoFilestore = Path.Combine(strPathUserIdGlobalId, strTextureFilename).Replace("\\", "/");
            item.TexturePath = Path.Combine(KanevaGlobals.TextureFilestore, texturePathNoFilestore);
            shoppingFacade.UpdateCustomItemTexture(item);

            // Make sure it has not been modified from it's original size!
            // Verify the texture is either 256x512 or 256x256
            System.Drawing.Image imgSizeCheck = null;
            try
            {
                if (Configuration.UseAmazonS3Storage)
                {
                    string originalImage = (Configuration.TextureServer + "/" + item.TexturePath).Replace("\\", "/");

                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(originalImage);
                    HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    
                    System.IO.Stream receiveStream = httpWebResponse.GetResponseStream();
                    imgSizeCheck = System.Drawing.Image.FromStream(receiveStream);
                }
                else
                {
                    //Create an image object from a file on disk
                    imgSizeCheck = System.Drawing.Image.FromFile(Path.Combine(KanevaGlobals.TexturePath, texturePathNoFilestore.Replace("/", "\\")));
                }


                // Get the width and height of the template to make sure it is not modified
                List<ItemTemplate> lItemTemplates = WebCache.GetCachedTemplates();
                 
                foreach (ItemTemplate itemTemplate in lItemTemplates)
                {
                    if (itemTemplate.GlobalId.Equals (baseItem.GlobalId))
                    {
                        if ((!imgSizeCheck.Width.Equals(itemTemplate.Width)) || (!imgSizeCheck.Height.Equals(itemTemplate.Height)))
                        {
                            if (imgSizeCheck != null)
                            {
                                imgSizeCheck.Dispose();
                            }

                            shoppingFacade.DeleteCustomItemTexture(globalId, KanevaWebGlobals.CurrentUser.UserId, false);
                            ShowError("Unable to upload because the texture template was modified from its original dimensions.");
                            return;
                        }
                    }
                }
                
            }
            catch (Exception exc)
            {
                m_logger.Error("Error in upload click ", exc);
            }
            finally
            {
                if (imgSizeCheck != null)
                {
                    imgSizeCheck.Dispose();
                }
            }

            // Current texture path
            string strTextureFilePath = Path.Combine(KanevaGlobals.TexturePath, texturePathNoFilestore.Replace("/", "\\"));

            //// Try DXT Compression
            //try
            //{
            //    //Create process
            //    System.Diagnostics.ProcessStartInfo processInfo =
            //        new System.Diagnostics.ProcessStartInfo(@Request.PhysicalApplicationPath + "bin\\nvdxt.exe", 
            //        "-quality_production -dxt1c -file " + strTextureFilePath + " -outdir " + Path.Combine(KanevaGlobals.TexturePath, strPathUserIdGlobalId));

            //    //Some other useful options if you want to capture the output to process in your app, good for command line apps
            //    processInfo.UseShellExecute = false;
            //    processInfo.CreateNoWindow = false;
            //    processInfo.WorkingDirectory = @Request.PhysicalApplicationPath + "bin\\";
            //    //processInfo.RedirectStandardError = true;
            //    //processInfo.RedirectStandardOutput = true;

            //    System.Diagnostics.Process dxtProcess = System.Diagnostics.Process.Start(processInfo);
            //    dxtProcess.WaitForExit(10);
            //    int exitCode = dxtProcess.ExitCode;
            //    dxtProcess.Close();

            //    //// See if the dds file exits
            //    string strDXTFileName = Path.GetFileNameWithoutExtension(strTextureFilePath) + ".dds";
            //    string strDXTPath = Path.Combine(strPathUserIdGlobalId, strDXTFileName);
            //    string strDXTFullPath = Path.Combine(KanevaGlobals.TexturePath, strDXTPath);

            //    if (System.IO.File.Exists(strDXTFullPath))
            //    {
            //        strTextureFilePath = strDXTFullPath;
            //    }
            //    else
            //    {
            //        m_logger.Debug("Could not find compressed .dds");
            //    }
            //}
            //catch (Exception exc)
            //{
            //    m_logger.Error("Error in dxt compression", exc);
            //}

            // Encrypt the texture
            string strTextureEncryptedFilename = KanevaGlobals.GenerateUniqueString(10) + ".dat";
            string strTextureEncryptPath = Path.Combine(strPathUserIdGlobalId, strTextureEncryptedFilename).Replace("\\", "/");

            // Hack for Ryans change to support random folders not in URL
            if (Configuration.UseAmazonS3Storage)
            {
                 if (ImageHelper.EncryptTextureToFile(item.TexturePath, Path.Combine(KanevaGlobals.TexturePath, strTextureEncryptPath.Replace("/", "\\"))))
                {
                    // Update the encrypted path
                    shoppingFacade.UpdateCustomItemTextureEncryption(item, Path.Combine(KanevaGlobals.TextureFilestore, strTextureEncryptPath));
                }
            }
            else
            {
                if (ImageHelper.EncryptTextureToFile(strTextureFilePath, Path.Combine(KanevaGlobals.TexturePath, strTextureEncryptPath.Replace("/", "\\"))))
                {
                    // Update the encrypted path
                    shoppingFacade.UpdateCustomItemTextureEncryption(item, Path.Combine(KanevaGlobals.TextureFilestore, strTextureEncryptPath));
                }
            }

            // Generate the thumbnails. First try using a media library image.
            if (assetId > 0)
            {
                SetItemThumbnailsToAssetImage(assetId, globalId);
            }
            // Next try using an uploaded file.
            else if (fThumbnail != null && fThumbnail.ContentLength > 0)
            {
                string thumbnailFilename = "";
                strPathUserIdGlobalId = Path.Combine(KanevaWebGlobals.CurrentUser.UserId.ToString(), globalId.ToString());
                strFilePathTexture = Path.Combine(KanevaGlobals.TexturePath, strPathUserIdGlobalId);

                try
                {
                    ImageHelper.UploadImageFromUser(ref thumbnailFilename, fThumbnail, strFilePathTexture, KanevaGlobals.MaxUploadedImageSize);
                    string imagePath = fThumbnail.FileName.Remove(0, fThumbnail.FileName.LastIndexOf("\\") + 1);
                    string origImagePath = strFilePathTexture + Path.DirectorySeparatorChar + thumbnailFilename;

                    ImageHelper.GenerateWOKItemThumbs(thumbnailFilename, origImagePath, KanevaGlobals.TexturePath, KanevaWebGlobals.CurrentUser.UserId, globalId);
                }
                catch { }
            }


			// Blast if they wanted to Blast
			if (chkBlastUpload.Checked)
			{
				int currUserId = KanevaWebGlobals.CurrentUser.UserId;

				// getting a new instance of the item so all the fields are updated.
				WOKItem item2 = shoppingFacade.GetItem(item.GlobalId);

				m_logger.Debug("Blasting Item");
				try
				{
                    // Metrics tracking for accepts
                    UserFacade userFacade = new UserFacade();
                    string requestId = userFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_SHOP_ITEM_UPLOAD, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
                    string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_BLAST, 0);

                    string url = "http://" + KanevaGlobals.ShoppingSiteName + "/ItemDetails.aspx?gId=" + item2.GlobalId + "&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeSentIdBlast;
					string itemThumbnail = StoreUtility.GetItemImageURL(item2.ThumbnailLargePath, "la");

					BlastFacade blastFacade = new BlastFacade();

					blastFacade.SendShopUploadItemBlast(currUserId, KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.NameNoSpaces,
						item2.DisplayName, item2.Description, url, itemThumbnail, "", KanevaWebGlobals.CurrentUser.ThumbnailSmallPath);

				}
				catch (Exception ex)
				{
					m_logger.Error("Blasting Item upload Failed. Item:" + item2.GlobalId + " UserId: " + currUserId + " " + ex.ToString());

				}
			}

            Response.Redirect (ResolveUrl("~/MyInventory.aspx?up=1&c="+ item.Category1.ToString()));
        }

        /// <summary>
        /// 
        /// </summary>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MyInventory.aspx");
        }

        protected HtmlInputFile inpTexture, inpThumbnail;
        protected Literal litAccessPass, litJS;

        protected TextBox txtTitle, txtDescription, txtKeywords, txtPrice;
        protected HtmlInputCheckBox chkRestricted, chkBlastUpload;
        protected HtmlInputRadioButton rdoPublic, rdoPrivate;

        protected HtmlSelect cboTextureType;
        protected DropDownList ddl_PassType;
        protected Repeater rptTextures;

        protected RequiredFieldValidator rfvTexture, rfvPassType, rfvTitle, rfvDesc;
        protected RegularExpressionValidator revTexture, revThumbnail;
        protected CustomValidator cvGeneral, cvTags, rfvThumbnail;
        protected ValidationSummary valSum;
        protected HiddenField hfMediaLibPhoto;
        protected HtmlImage imgItem;

        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}
