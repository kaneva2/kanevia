<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="true" CodeBehind="MySales.aspx.cs" Inherits="Kaneva.PresentationLayer.Shopping.MySales" %>

<asp:Content ID="cntUpload" runat="server" ContentPlaceHolderID="cphBody">

    <script type="text/javascript" src="./jscript/swfobject.js"><!-- // External JS --></script>

    <div id="mainContent">
		<div class="column listColumn first">
			<script type="text/javascript">
				var flashvars = {};
				var params = {};
				params.quality = "high";
				params.allowscriptaccess="always";
				params.wmode = "transparent";
				var attributes = {};
				attributes.id = "flashContent";
				swfobject.embedSWF("http://streaming.kaneva.com/ImageServer/media/shop/mystore/halfWrapper.swf", "flash1", "410", "180", "9.0.0", "expressInstall.swf", flashvars, params, attributes);
			</script>
			<div class="flashArea4">
				<div id="flash1"><a href="http://www.adobe.com/go/getflashplayer">
					<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
					</a>
				</div>
			</div>
			<div id="soldList">
				<ul>
					<li class="soldMargin">My Profits This Week:&nbsp;<asp:Label id="lblCreditsThisWeek" Text="0" runat="server"/></li>
					<li class="soldMargin">Items Sold This Week: <asp:Label id="lblItemsSoldThisWeek" Text="0" runat="server"/></li>
				</ul>
			</div>
			<div id="salesList">
				<h3>Item Sales:</h3>
				<ul>
					<asp:Repeater ID="rptItems" runat="server">
                    <HeaderTemplate>
						<li>
							<ul class="bold">
								<li class="itemName">Item Name</li>
								<li class="itemsSold">Items Sold</li>
								<li class="itemSales">TotalSales</li>
								<li class="itemWklySales">Credits Earned</li>
							</ul>
						</li>
                    </HeaderTemplate>
                    <ItemTemplate>                
                        <li class="highlightRow">                
							<ul>
								<li class="itemName"><a href='<%# GetItemDetailsURL (DataBinder.Eval (Container.DataItem, "GlobalId").ToString ())%>'><%# DataBinder.Eval (Container.DataItem, "Name") %></a></li>
								<li class="itemsSold"><%# DataBinder.Eval(Container.DataItem, "NumberSoldOnWeb")%></li>
								<li class="itemSales"><%# DataBinder.Eval(Container.DataItem, "SalesTotal")%></li>
								<li class="itemWklySales"><%# DataBinder.Eval(Container.DataItem, "SalesDesignerTotal")%></li>
                            </ul>
						</li>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
						<li>
							<ul>
								<li class="itemName"><a href='<%# GetItemDetailsURL (DataBinder.Eval (Container.DataItem, "GlobalId").ToString ())%>'><%# DataBinder.Eval (Container.DataItem, "Name") %></a></li>
								<li class="itemsSold"><%# DataBinder.Eval(Container.DataItem, "NumberSoldOnWeb")%></li>
								<li class="itemSales"><%# DataBinder.Eval(Container.DataItem, "SalesTotal")%></li>
								<li class="itemWklySales"><%# DataBinder.Eval(Container.DataItem, "SalesDesignerTotal")%></li>                        
							</ul>
						</li>
                    </AlternatingItemTemplate>
					</asp:Repeater>
				</ul>
				<div class="clearit nosales">
					<asp:Label id="lblNoSales" visible="false" runat="server"><span style="color:Navy;size:24pt;font-weight:bold">No Sales Were Found.</span> </asp:Label>
				</div>
			</div>
		</div>
      
		<!-- start the 3rd column -->
      
		<div class="column favoriteItems">
			<div class="lrgButton"><a id="A2" runat="server" href="~/Upload.aspx">Add New Items Now!</a></div>
			<div class="smlButton"><a id="A3" runat="server" href="~/bundle.aspx">Add a Bundle of Items</a></div>
			<h3>My Most Popular Items</h3>
			<ul id="browse">
				<asp:Repeater ID="rptPopular" runat="server">
					<ItemTemplate>
						<li style='<%# GetStyleImage (DataBinder.Eval (Container.DataItem, "ThumbnailLargePath").ToString (), "la")%>'><a id="A1" href='<%# GetItemDetailsURL (DataBinder.Eval (Container.DataItem, "GlobalId").ToString ())%>'><%# DataBinder.Eval (Container.DataItem, "Name") %><span class="price"><%# DataBinder.Eval(Container.DataItem, "WebPrice")%></span></a></li>
					</ItemTemplate>
				</asp:Repeater>
            
				<asp:Literal ID="litFiller" runat="server"></asp:Literal>
			</ul>
		</div>
    </div>
    
	<div class="clearit"><!-- clear the floats --></div>

</asp:Content>
