///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
    public partial class VIPPriceData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "text/plain";
            Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "no-cache");
            Response.Expires = -1;

            BindDeedItems();
        }

        private void BindDeedItems()
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            int gId = 0;

            try
            {
                gId = Convert.ToInt32(Request["gId"]);
            }
            catch (Exception)
            {
                gId = 0;
            }

            WOKItem item = shoppingFacade.GetItem(gId);

            int VIPPrice = GetItemCheckoutPrice(item);
           
            if (VIPPrice == 0)
            {
                lblVIPPrice.Text = "FREE";
            }
            else
            {
                lblVIPPrice.Text = VIPPrice.ToString("N0");
            }
        }


        private int GetItemCheckoutPrice(WOKItem item)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            HashSet<int> usedGlids = new HashSet<int>();

            // If item is a zone then we have to calcuate the designer fee for each item included
            // in the zone and the zone designer fee to get the VIP pricing.
            if (item.UseType == WOKItem.USE_TYPE_CUSTOM_DEED)
            {
                List<DeedItem> deedList = shoppingFacade.GetDeedSalesItemList(item.GlobalId, (int)item.ItemCreatorId);

                uint itemsNotOwnedDesignerPrice = 0;
                foreach (DeedItem di in deedList)
                {
                    if (!usedGlids.Contains(di.GlobalId) || di.UseType == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM)
                    {
                        usedGlids.Add(di.GlobalId);
                        itemsNotOwnedDesignerPrice += di.DeedItemPrice;

                        // Game items may have bundled glids that we need to process.
                        if (di.UseType == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM)
                        {
                            foreach (DeedItem bundledItem in di.BundleItems)
                            {
                                if (!usedGlids.Contains(bundledItem.GlobalId))
                                {
                                    usedGlids.Add(bundledItem.GlobalId);
                                    itemsNotOwnedDesignerPrice += bundledItem.DeedItemPrice;
                                }
                            }
                        }
                    }
                }

                return (int)(itemsNotOwnedDesignerPrice + item.DesignerPrice);
            }
            else if (item.UseType == WOKItem.USE_TYPE_BUNDLE)
            {
                return item.BundleWebPriceVIP;
            }
            else if (item.UseType == WOKItem.USE_TYPE_SCRIPT_GAME_ITEM)
            {
                ScriptGameItem sgItem = (new ScriptGameItemFacade()).GetSnapshotScriptGameItem(item.GlobalId);
                WOKItem gameItemBundle = shoppingFacade.GetItem(sgItem.BundleGlid, false, false);
                ScriptGameItemPrice sgiPrice = new ScriptGameItemPrice(item, gameItemBundle);

                return sgiPrice.WebPriceVIP;
            }

            return item.WebPriceVIP;
        }

    }
}