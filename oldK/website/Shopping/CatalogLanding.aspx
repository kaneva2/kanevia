<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="true"
	 Codebehind="CatalogLanding.aspx.cs" Inherits="Kaneva.PresentationLayer.Shopping.CatalogLanding" %>

<asp:Content ID="cntUpload" runat="server" ContentPlaceHolderID="cphBody">
	 <script type="text/javascript" src="./jscript/swfobject.js"><!-- // External JS --></script>

	 <div id="content" class="browseWidth">
		  <asp:HyperLink runat="server" ID="topImg"  />
		  <div id="dvTopFlash" runat="server">			   
			   <script type="text/javascript">
						 var flashvars = {};
						 var params = {};
						 params.quality = "high";
						 params.wmode = "transparent";
						 params.allowscriptaccess="always";
						 var attributes = {};
						 attributes.id = "flashContent";
						 swfobject.embedSWF("<asp:Literal runat="server" id="litTopSwf"></asp:Literal>", "flash1", "721", "180", "9.0.0", "expressInstall.swf", flashvars, params, attributes);
					 </script>

			   <div id="flashContent" class="flashArea3">
					<div id="flash1">
						 <a href="http://www.adobe.com/go/getflashplayer">
							  <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif"
								   alt="Get Adobe Flash player" />
						 </a>
					</div>
			   </div>
		  </div>
		  <asp:Literal ID="litSoundStyle" runat="server"></asp:Literal>
		  <ul id="browse">
			   <asp:Repeater ID="rptItems" runat="server">
					<ItemTemplate>
						 <li style='<%# GetStyleImage (DataBinder.Eval (Container.DataItem, "ThumbnailLargePath").ToString (), "la")%>'>
							  <a id="A1" href='<%# GetItemDetailsURL (DataBinder.Eval (Container.DataItem, "GlobalId").ToString ())%>'>
								   <%# DataBinder.Eval (Container.DataItem, "Name") %>
								   <span class="price">
										<%# DataBinder.Eval (Container.DataItem, "WebPrice") %>
								   </span></a><div id="Div1" class="animation" runat="server" visible='<%# Convert.ToBoolean ( DataBinder.Eval(Container.DataItem, "IsAnimated")) %>'><!-- empty --></div></li>
					</ItemTemplate>
			   </asp:Repeater>
		  </ul>
		  

		  
		<!-- Clear the floats -->  
		<div class="clearit"></div>
		<div class="endRule"><img src="./images/content_sideRule.gif" alt="Verticle Rule" /></div>		  

			<div style="float:left;width:718px;height:315px;overflow:hidden;text-align:center;padding-top:20px;" id="subImgs" runat="server" visible="false">
				<asp:HyperLink runat="server" ID="bottomImg" Width="360" Height="315"/>
				<asp:HyperLink runat="server" ID="bottomImg2" Width="360" Height="315"/>
			</div>
		  
			<div id="dvBottomFlash" runat="server">
				<script type="text/javascript">
							var flashvars = {};
							var params = {};
							params.quality = "high";
							params.wmode = "transparent";
							params.allowscriptaccess="always";
							var attributes = {};
							attributes.id = "flashContent";
							swfobject.embedSWF("<asp:Literal runat="server" id="litBottomSwf"></asp:Literal>", "flash2", "721", "<asp:Literal runat="server" id="litBtmSwfHeight" />", "9.0.0", "expressInstall.swf", flashvars, params, attributes);
						</script>

				<div class="genericFlshWrapper">
					<div id="flash2" class="genericFlashArea">
							<a href="http://www.adobe.com/go/getflashplayer">
								<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif"
									alt="Get Adobe Flash player" />
							</a>
					</div>
				</div>
			</div>	
		  
		<div class="endRule"><img src="./images/content_sideRule.gif" alt="Verticle Rule" /></div>
	</div>
</asp:Content>
