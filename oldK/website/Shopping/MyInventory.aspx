<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="true" CodeBehind="MyInventory.aspx.cs" Inherits="Kaneva.PresentationLayer.Shopping.MyInventory" %>
<%@ Register TagPrefix="Shopping" TagName="Pager" Src="./usercontrols/Pager.ascx" %>
<asp:Content ID="cntUpload" runat="server" ContentPlaceHolderID="cphBody">

	<div id="sortItems">
		<label for="pageSort">Show: </label>
        <asp:DropDownList runat="server" id="ddlSort" AutoPostBack="True" OnSelectedIndexChanged="ddlSort_OnSelectedIndexChanged">
            <asp:ListItem Selected="True" Text="All" Value=""></asp:ListItem>
            <asp:ListItem value="0-9">0-9</asp:ListItem>
            <asp:ListItem value="Sym">Sym</asp:ListItem>
            <asp:ListItem value="A">A</asp:ListItem>
            <asp:ListItem value="B">B</asp:ListItem>
            <asp:ListItem value="C">C</asp:ListItem>
    
            <asp:ListItem value="D">D</asp:ListItem>
            <asp:ListItem value="E">E</asp:ListItem>
            <asp:ListItem value="F">F</asp:ListItem>
            <asp:ListItem value="G">G</asp:ListItem>
            <asp:ListItem value="H">H</asp:ListItem>
            <asp:ListItem value="I">I</asp:ListItem>
    
            <asp:ListItem value="J">J</asp:ListItem>
            <asp:ListItem value="K">K</asp:ListItem>
            <asp:ListItem value="L">L</asp:ListItem>
            <asp:ListItem value="M">M</asp:ListItem>
            <asp:ListItem value="N">N</asp:ListItem>
            <asp:ListItem value="O">O</asp:ListItem>
    
            <asp:ListItem value="P">P</asp:ListItem>
            <asp:ListItem value="Q">Q</asp:ListItem>
            <asp:ListItem value="R">R</asp:ListItem>
            <asp:ListItem value="S">S</asp:ListItem>
            <asp:ListItem value="T">T</asp:ListItem>
            <asp:ListItem value="U">U</asp:ListItem>
    
            <asp:ListItem value="V">V</asp:ListItem>
            <asp:ListItem value="W">W</asp:ListItem>
            <asp:ListItem value="X">X</asp:ListItem>
            <asp:ListItem value="Y">Y</asp:ListItem>
          <asp:ListItem value="Z">Z</asp:ListItem>
        </asp:DropDownList>
    </div>
	<div>
        <asp:Label id="lbl_NoCatalogItems" visible="false" runat="server">
            <span style="color:Navy; size:24pt; font-weight:bold">No Catalog Items Were Found.</span>
        </asp:Label>
    </div>
	<div id="inventoryBTN">
		<ul>
			<li><a id="A1" runat="server" href="~/Upload.aspx">Add New Item</a></li>
			<li><a id="A2" runat="server" href="~/bundle.aspx">Add New Bundle</a></li>
			<li><asp:LinkButton id="lnkDelete" OnClick="lnkDelete_Click" title="Delete selected items." runat="server" CausesValidation="False" Text="Delete Selected Items"></asp:LinkButton></li>
		</ul>
	</div>
	<div id="content">
		<div id="inventoryList">
			<ul>
				<asp:Repeater ID="rptItems" runat="server" >
					<HeaderTemplate>
						<li>
							<!-- ch: Adds the header to the grid. also adds the edit area -->
							<ul class="bold">
								<li class="checkItem">&nbsp;</li><li class="itemImage">&nbsp;</li>
								<li class="itemName">&nbsp;</li>
								<li class="itemPrice">Current Price</li>
								<li class="itemsSold">Items Sold</li>
								<li class="itemSales">Total Sales</li>
								<li class="crEarned">Credits Earned</li>
								<li class="itemLink">&nbsp;</li>
								<li class="itemLink">&nbsp;</li>
							</ul>
						</li>
					</HeaderTemplate>
					<ItemTemplate>                
						<li class="highlightRow">
							<ul><!-- the chack box and the image must go on the same line or they will not display properly -->
								<li class="checkItem"><asp:checkbox id="chkEdit" runat="server" /></li><li class="itemImage"><img src='<%# GetItemImageURL (DataBinder.Eval (Container.DataItem, "ThumbnailSmallPath").ToString (), "sm")%>' alt="" /></li>
								<li class="itemName"><%# DataBinder.Eval (Container.DataItem, "Name") %>
								<input type="hidden" runat="server" id="hidGlboalId" value='<%#DataBinder.Eval(Container.DataItem, "GlobalId")%>' name="hidGlboalId"/>
								</li>
								<li class="itemPrice"><%# GetItemPrice(Eval("WebPrice"), Eval("MarketCost"), Eval("UseType")) %></li>
								<li class="itemsSold"><%# DataBinder.Eval(Container.DataItem, "NumberSoldOnWeb")%></li>
								<li class="itemSales"><%# DataBinder.Eval(Container.DataItem, "SalesTotal")%></li>                         
								<li class="crEarned"><%# DataBinder.Eval(Container.DataItem, "SalesDesignerTotal")%></li>
								<li class="itemLink"><a href='<%# GetItemDetailsURL (DataBinder.Eval (Container.DataItem, "GlobalId").ToString ())%>'>Details</a></li>
								<li class="itemLink"><a href='<%# GetItemEditURL (DataBinder.Eval (Container.DataItem, "GlobalId").ToString ())%>'>Edit</a></li>
							</ul>
						</li>
					</ItemTemplate>
					<AlternatingItemTemplate>
						<li>
							<ul><!-- the chack box and the image must go on the same line or they will not display properly -->
								<li class="checkItem"><asp:checkbox id="chkEdit" runat="server" /></li><li class="itemImage"><img src='<%# GetItemImageURL (DataBinder.Eval (Container.DataItem, "ThumbnailSmallPath").ToString (), "sm")%>' alt="" /></li>
								<li class="itemName"><%# DataBinder.Eval (Container.DataItem, "Name") %>
									<input type="hidden" runat="server" id="hidGlboalId" value='<%#DataBinder.Eval(Container.DataItem, "GlobalId")%>' name="hidGlboalId"/>
								</li>
								<li class="itemPrice"><%# GetItemPrice(Eval("WebPrice"), Eval("MarketCost"), Eval("UseType")) %></li>
								<li class="itemsSold"><%# DataBinder.Eval(Container.DataItem, "NumberSoldOnWeb")%></li>
								<li class="itemSales"><%# DataBinder.Eval(Container.DataItem, "SalesTotal")%></li>
								<li class="crEarned"><%# DataBinder.Eval(Container.DataItem, "SalesDesignerTotal")%></li>
								<li class="itemLink"><a href='<%# GetItemDetailsURL (DataBinder.Eval (Container.DataItem, "GlobalId").ToString ())%>'>Details</a></li>
								<li class="itemLink"><a href='<%# GetItemEditURL (DataBinder.Eval (Container.DataItem, "GlobalId").ToString ())%>'>Edit</a></li>
							</ul>
						</li>
					</AlternatingItemTemplate>
				</asp:Repeater>
			</ul>
		</div>
		<div class="clearit"><!-- clear the floats --></div>

		<div id="pageList" class="clearit" style="width:97%;">
			<ul>
				<li>
					<Shopping:Pager runat="server" isajaxmode="False" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
				</li>
			</ul>
		</div>
	</div>
  
	<div class="clearit"><!-- clear the floats --></div>
 
</asp:Content>
