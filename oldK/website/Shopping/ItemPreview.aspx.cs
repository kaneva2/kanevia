///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
	public partial class ItemPreview : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			litStatus.Visible = true;
			
			// Determine type
			int gId = 0;

			litStatus.Text += "<br /> Start";

			try
			{
				gId = Convert.ToInt32(Request.Params["gId"]);
				litStatus.Text += "<br /> Global Id:" + gId;
			}
			catch 
			{
				litStatus.Text += "<br /> Error 1";
			}

			ShoppingFacade shoppingFacade = new ShoppingFacade();
			WOKItem item = shoppingFacade.GetItem(gId);
			
			if (item.UseType == 200)
			{
				showAnimationPreview(item);
			}

		}

		private void showAnimationPreview(WOKItem item)
		{
			// get the last image
			string lastImage = StoreUtility.GetItemImageURL(item.ThumbnailAssetdetailsPath, "ad");
			litStatus.Text += "<br /> image:" + lastImage;

			// make the base string prefix
			string imagePrefix = "";
			try
			{
				imagePrefix = lastImage.Substring(0, lastImage.Length - 7);
				litStatus.Text += "<br /> image stub:" + imagePrefix;
			}
			catch
			{
				litStatus.Text += "<br /> Error with string prefix.";
			}

			// figure out how many images
			int numImages = 0;

			try
			{
				numImages = Convert.ToInt16(lastImage.Substring(lastImage.Length - 7, 3));
				litStatus.Text += "<br /> image count:" + numImages.ToString();
			}
			catch
			{
				litStatus.Text += "<br /> Error getting number of images.";
			}

			// if no images then use default.
			if (numImages == 0)
			{
				litImgArray.Text = "\"" + StoreUtility.GetItemImageURL(item.ThumbnailAssetdetailsPath, "ad") + "\"";
			}

			StringBuilder sb = new StringBuilder();

			for (int i = 1; i < numImages; i++)
			{
				string iString = i.ToString();

				if (iString.Length < 3)
					iString = "0" + iString;

				if (iString.Length < 3)
					iString = "0" + iString;

				sb.Append("\"" + imagePrefix + iString + ".jpg\",");
			}

			if (sb.ToString().Length > 0)
			{
				litImgArray.Text = sb.ToString().Substring(0, sb.ToString().Length - 1);
			}
		}

		private void showSoundPreview(WOKItem item)
		{
			/*
			 * old sound code

			 if (item.UseValue < 0)
			 {
				  string SoundServer = System.Configuration.ConfigurationManager.AppSettings["SoundServer"].ToString();							  
				  litSoundPreview.Text = SoundServer + "/" + item.ItemCreatorId.ToString() + "/" + item.GlobalId.ToString() + ".ogg";
				  litSoundPreview2.Text = "<source src=\"" + SoundServer + "/" + item.ItemCreatorId.ToString() + "/" + item.GlobalId.ToString() + ".ogg\" type=\"video/ogg\">";
				  divSoundPreview.Visible = true;
				  imgItem.Visible = false;
				  if (Request.Browser.Browser.Contains("Mozilla"))
				  {
					   divIEplayer.Visible = false;
					   divFFplayer.Visible = true;
				  }
				  else
				  {
					   divIEplayer.Visible = true;
					   divFFplayer.Visible = false;
				  }
			 }
			 */ 
		}

	}
}
