﻿<%@ Page Language="C#" masterpagefile="~/masterpages/MainTemplatePage.Master" AutoEventWireup="true" CodeBehind="Bundle.aspx.cs" Inherits="Kaneva.PresentationLayer.Shopping.Bundle" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:content id="cntUpload" runat="server" contentplaceholderid="cphBody">
	<script src="./jscript/prototype.js" type="text/javascript" language="javascript"></script>
	<script type="text/javascript">
		function ItemClick(obj) {
			if (obj.className == 'unchecked') {
				obj.className = 'checked';
			}
			else {
				obj.className = 'unchecked';
			}
		}
		function GetDeleteItems() {
			var delItems = new Array();
			var tbl = $('tblItems');
			if (tbl != null) {
				var rows = tbl.getElementsByTagName('tr');
				for (i = 0; i < rows.length; i++) {
					if (rows[i].className == 'checked') {
						delItems[delItems.length] = rows[i].cells[3].innerText;
					}
				}
				if (delItems.length == 0) {
					alert('No items are selected to delete.');
					return false;
				}
				$('hidDelIDs').value = delItems.toString();
			}
		}
	</script>
	<div id="bundles" style="display:block;">

		<asp:scriptmanager id="smBundles" runat="server" enablepartialrendering="true"></asp:scriptmanager>

		<asp:updateprogress id="upUpdate" runat="server">
			<progresstemplate>
				<div class="updating">Updating...</div>
			</progresstemplate>
		</asp:updateprogress>

		<asp:updatepanel id="upMessageSummary" runat="server" childrenastriggers="true" updatemode="conditional">
 			<triggers>
				<asp:AsyncPostBackTrigger controlid="btnAddToBundle" eventname="Click" />
				<asp:AsyncPostBackTrigger controlid="btnDelete" eventname="Click" />
			</triggers>
			<contenttemplate>
				<div id="itemsContainer">
					<div class="gids"><label>Enter <a onclick="$('PopUp').style.display='block';" onfocus='this.blur();' class="aGID" href="javascript:void(0);">GIDs</a> (separated by commas):</label> <asp:textbox id="txtGIDs" runat="server" style="width:300px;" ></asp:textbox> <div id="btn"><asp:linkbutton causesvalidation="false" id="btnAddToBundle" class="buttonFixed" runat="server" onclick="btnAddToBundle_Click"><span class="addbtn">Add to Bundle</span></asp:linkbutton></div> </div> 
					<div id="PopUp" border="0">
						<a onmouseover="this.style.cursor='pointer'" onfocus="this.blur();" onclick="$('PopUp').style.display='none';" >
						<img runat="server" src="~/images/gidinfo.gif" width="339" height="169" /></a>
					</div>
					<div class="errSum" id="divError" runat="server" visible="false"></div>	
					<asp:validationsummary cssclass="errSum" id="valSum" showsummary="true" runat="server" /> 																															   
					<asp:customvalidator id="cvItems" runat="server" display="None" errormessage="A bundle must include 2 or more items." ></asp:customvalidator>
					<div class="addedItemsContainer">
						<div class="tblHeader">
							<div class="name">Name</div>
							<div class="price">Price</div>
							<div class="id">GID</div>
							<div class="ap">AP</div>
						</div>
						<div class="bundleList">
							<asp:repeater id="rptBundleGIDs" runat="server">
								<headertemplate>
									<table id="tblItems" border="0" cellpadding="0" cellspacing="0">
								</headertemplate>
								<itemtemplate>
									<tr class="unchecked" onclick="ItemClick(this);" onmouseover="this.style.cursor='pointer';">
										<td class="name"><%# Eval("DisplayName") %></td>
										<td class="price"><%# GetItemPrice(Eval("WebPrice"), Eval("MarketCost"), Eval("ItemCreatorId")) %></td>
										<td class="spacer"></td>
										<td class="id"><%# Eval("GlobalId") %></td>
										<td class="ap"><%# IsAPItem (Eval("IsAP")) %></td>
									</tr>	
								</itemtemplate>
								<footertemplate>
									</table>
								</footertemplate>
							</asp:repeater>
						</div>
						<div class="deleteItem">
							<asp:hiddenfield id="hidDelIDs" runat="server" value="" clientidmode="Static" />
							<div><asp:linkbutton causesvalidation="false" id="btnDelete" class="buttonFixed" runat="server" onclientclick="return GetDeleteItems();" onclick="btnDeleteFromBundle_Click"><span class="deletebtn">Delete Selected</span></asp:linkbutton></div>	
							<div id="divAP" class="apimg" runat="server" visible="false"><img src="images/pass_content.gif" /> Includes AP Items</div>
						</div>
					</div>
					<div class="clearit"></div>
					<span class="note">*In the above list, commission is not included in the price of items you own.</span>
					<div class="divider"></div>
				</div>
			</contenttemplate>
		</asp:updatepanel>

		<div class="colWidth">
			<div class="formStyle">
				<span class="fieldNote">* Fields marked with an asterisk (*) are required.</span>
				<div class="required">
					<label>Bundle Name:*</label><br />
					<asp:requiredfieldvalidator id="rfvTitle" class="validator" runat="server" controltovalidate="txtTitle" text="*" errormessage="Bundle Name is a required field." display="static"></asp:requiredfieldvalidator>
					<asp:textbox id="txtTitle" runat="server" width="260px" cssclass="inputText" maxlength="125"></asp:textbox>
				</div>
				<div class="required">
					<label for="thumbnail_image">Thumbnail Image:*</label><br />
					<asp:RegularExpressionValidator id="revThumbnail" class="validator1" runat="server" text="*" ErrorMessage="Thumbnail Image must be a .jpg, .jpeg, or .gif file type." ValidationExpression="^.+\.([jJ][pP][gG]|[gG][iI][fF]|[jJ][pP][eE][gG])$" ControlToValidate="inpThumbnail" display="static" ></asp:RegularExpressionValidator><asp:requiredfieldvalidator id="rfvThumbnail" runat="server" class="validator1" controltovalidate="inpThumbnail" text="*" errormessage="Thumbnail Image is a required field." display="static"></asp:requiredfieldvalidator>
					<input runat="server" class="inputText" id="inpThumbnail" type="file" size="15" />
					<span class="formNote"><span class="bold">Protect your design!</span> Do NOT upload your texture file as the thumbnail image, or your design could be stolen.
						<br />
						<br />
						For a thumbnail:
						<br />
						Try the item on your Avatar and take a screenshot. Resize the image to 90 x 90 pixels, and then save it as a .jpg or .gif file.</span>
				</div>
				<div class="required">
					<label for="description">Description:*</label><br />
					<asp:requiredfieldvalidator id="rfvDesc" runat="server" class="validator" controltovalidate="txtDescription" text="*" errormessage="Description is a required field." display="static"></asp:requiredfieldvalidator>
					<asp:textbox id="txtDescription" runat="server" maxlength="300" textmode="MultiLine" rows="3" columns="20"></asp:textbox>
				</div>
				<div class="required">
					<label>Tags:</label><br />
					<asp:textbox id="txtKeywords" class="formNote" maxlength="100" textmode="MultiLine" runat="server"></asp:textbox>
					<span class="formNote">Tags are unique keywords that help people find items on Shop Kaneva.</span>
				</div>
			</div>
		</div>
		
		<div class="colWidth last">
			
			<asp:updatepanel id="Updatepanel1" runat="server" updatemode="conditional">
 				<triggers>
					<asp:AsyncPostBackTrigger controlid="btnAddToBundle" eventname="Click" />
					<asp:AsyncPostBackTrigger controlid="btnDelete" eventname="Click" />
					<asp:AsyncPostBackTrigger controlid="txtPrice" eventname="TextChanged" />
				</triggers>
 				<contenttemplate>
			
				<div class="formStyle">
					<div class="required formBox">
						<label>Design Commission:</label>
						<asp:rangevalidator id="rvPrice" class="validator1" controltovalidate="txtPrice" minimumvalue="0" maximumvalue="5000000" type="Integer" runat="server" errormessage="Commission must be a numeric value between 0 and 5,000,000." text="*" display="Static"></asp:rangevalidator>
						<asp:textbox id="txtPrice" maxlength="7" width="60px" cssclass="inputText" ontextchanged="txtPrice_TextChanged" autopostback="true" runat="server" text="0"></asp:textbox>
						<span class="formNote">Enter the number of Credits you want to earn from the sale of this item.<br />
							Note: The base price and a listing fee will be added to your Design Commission, for the final price of the item.  Your
							Design Commission is not included in the price of items you own.</span>
					</div>

					<div>
						<div>Purchase with:  <span id="spnPurchaseType" runat="server"></span></div>
						<div class="pricing">
							<div class="title">Bundle Pricing:</div>
							<div class="row"><span>Items Total*</span><div id="totalNotOwnedItems" runat="server">0</div></div>
							<div class="row"><span>Items You Own + 10% catalog fee</span><div id="totalOwnedItems" runat="server">0</div></div>
							<div class="row"><span>Design Commission + 10% catalog fee</span><div id="totalDesignCommission" runat="server">0</div></div>
							<div class="line"></div>
							<div class="row total"><span>Total Price:</span><div id="totalBundle" runat="server">0</div></div>
							<p>*Does not include items you own</p>
						</div>
					</div>

					<div>
						<label>Blast:</label>&nbsp;<input runat="server" class="checkBox" id="chkBlastUpload" type="checkbox" />
						<span class="formNote">Blast item details to all your friends to let them know about your new item available for purchase.</span>
					</div>
				</div>

				</contenttemplate>
			</asp:updatepanel>

		</div>
		<div class="formBtns2 clearit">
			<asp:linkbutton id="btnSubmit" text="Add Bundle" class="btn_submit" onclientclick="if (typeof(Page_ClientValidate) == 'function') if (!Page_ClientValidate()){return false;};" onclick="btnAdd_Click" runat="server" ></asp:linkbutton> 
			<a id="btnCancel" class="btn_submit" runat="server" href="~/MyInventory.aspx">Cancel</a>
		</div>

	</div>

</asp:content>
