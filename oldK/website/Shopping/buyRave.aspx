<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplateNoBorder.Master" AutoEventWireup="true" CodeBehind="buyRave.aspx.cs" Inherits="Kaneva.PresentationLayer.Shopping.buyRave" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>


<asp:Content ID="cntUpload" runat="server" ContentPlaceHolderID="cphBody">

<link href="css/facebox.css"rel="stylesheet" type="text/css" />	


<div style="width:360px;height:152px;" id="divData" runat="server">


<ajax:ajaxpanel id="Ajaxpanel1" runat="server">


<div id="divBuyRave" runat="server" style="display:none;text-align:center;">

	<div style="display:block;height:101px;width:340px;margin-bottom:8px;">
		<img id="Img1" src="~/images/facebox/largeRave.gif" height="101" width="97" runat="server" style="float:left;" />
		<div style="margin:10px 0 0 10px;font-family:Verdana;font-size:12px;float:left;width:226px;height:82px;">
			<div style="text-align:left;color:#4c4c4c;"><p>Additional Raves cost 200 credits/rewards.</p></div>
			<div style="text-align:left;color:#4c4c4c;"><p>How would you like to pay for this Rave?</p></div>
		</div>
	</div>

	<div id="divCredits" runat="server">
		<div>
			<span runat="server" id="spnCreditsBtn"><asp:linkbutton id="btnCredits" runat="server" text="Pay with Credits" oncommand="Buy_Click" commandargument="credits" /></span>
			<span style="padding:0px 5px;">-or-</span>
			<span runat="server" id="spnRewardsBtn"><asp:linkbutton id="btnRewards" runat="server" text="Pay with Rewards" oncommand="Buy_Click" commandargument="rewards" /></span> 
		</div>
																									  
		<div id="divRewards" runat="server" style="height:40px;margin-top:5px;margin-left:10px;">
			<div style="float:left;text-align:center;width:150px;font-family:Verdana;font-size:12px;color:#4c4c4c;margin-left:8px;"><asp:checkbox id="cbCredits"  autopostback="true" oncheckedchanged="AlwaysPayWith" text="Always purchase Raves with Credits" runat="server" /></div> 
			<div style="float:left;text-align:left;width:150px;margin-left:35px;font-family:Verdana;font-size:12px;color:#4c4c4c;"><asp:checkbox id="cbRewards" autopostback="true" oncheckedchanged="AlwaysPayWith" text="Always purchase Raves with Rewards" runat="server" /></div> 
		</div>
	</div>

	<div id="divXtraTxt" runat="server" style="display:none;font-family:Verdana;margin:10px 6px 0 0;border:solid 1px blue;background-color:#def3fc;padding:8px;font-size:12px;text-align:left;color:#4c4c4c;">
		<span><b>NOTE:</b> If you select the "Always purchase with Rewards" option all your purchases will default to that currency.
		Once you run out of Rewards it will automatically switch to Credits.</span><br /><br />
		<span>You can change this setting at anytime in your <a id="aSettings" href="" runat="server" target="_blank">My Account > Preferences</a> settings.</span><br /><br />
		<div class="facebox_btn">
		<asp:linkbutton id="btnConfirm"  runat="server" oncommand="BuyRaveWithAlwaysPurchase_Click" text="CONFIRM"/>
		<asp:linkbutton id="btnCancel" runat="server" onclick="btnCancel_Click" text="CANCEL"></asp:linkbutton>
		</div>
	</div>									
											  
</div>																									  


<div id="divBuyMegaRave" runat="server" style="display:none;">
	<div style="height:115px;width:320px;margin-bottom:15px;margin-right:80px;">
		<img id="Img2" src="~/images/facebox/largeMegaRave.gif" height="115" width="113" runat="server" style="float:left;" />
		<div style="margin-left: 120px;">
			<div style="margin-left:15px; margin-bottom:1em;font-family:Verdana;font-size:12px;color:#4c4c4c">MegaRaves cost 2000 <br />Credits/Rewards.</div>
			<div id="Div1" class="facebox_btn_mega" runat="server" style="margin-bottom: 1em;">
				<asp:linkbutton id="btnMegaRaveCredits" runat="server" text="MegaRave with Credits" oncommand="Buy_Click" commandargument="credits" />
			</div>
			<div id="Div2" class="facebox_btn_mega" runat="server" style="margin-bottom: 1em;">
				<asp:linkbutton id="btnBegaRaveRewards" runat="server" text="MegaRave with Rewards" oncommand="Buy_Click" commandargument="rewards" /> 
			</div>
		</div>
	</div>

	<div id="div3" runat="server" style="display:block;font-family:Verdana;margin-top:20px;border:solid 1px blue;background-color:#fffea9;padding:5px;font-size:12px;text-align:left;">
		<span style="font-family:Verdana;font-size:12px;color:#4c4c4c"><b>NOTE:</b> MegaRaves are very special raves.  They give the person you MegaRave 
		<span id="spnMegaRaveValue" runat="server"></span> Raves, 
		<span id="spnRaveeCommissionAmt" runat="server"></span> Rewards and display a special effect in 3D on their profiles.
	</div>									

</div>


<div id="divSuccess" runat="server" style="display:none;">
 <img id="Img5" src="~/images/facebox/largeSuccess.gif" height="115" width="113" runat="server" style="float:left;margin-right:10px;" />
	<p style="font-family:Verdana;font-size:12px;color:#4c4c4c;padding-top:50px;">Thank you for your purchase!</p>
	
	<div style="width:85%;text-align:right;">
	<a href="" id="aClose" runat="server" onfocus="javascript:amtHasChanged=true;">Close</a></div>

<script type="text/javascript">
function IsIE ()
{					  
	if ((navigator.userAgent.indexOf('MSIE') != -1) && (navigator.userAgent.indexOf('Win') != -1))
	{ return true; }
	else
	{ return false;	}
}
var amtHasChanged = <asp:literal id="litAmtHasChanged" runat="server" text="false"></asp:literal>;
var totRaves = '<asp:literal id="litTotRaves" runat="server" text="-1"></asp:literal>';
var totCredits = '<asp:literal id="litTotCredits" runat="server" text="-1"></asp:literal>';
var totRewards = '<asp:literal id="litTotRewards" runat="server" text="-1"></asp:literal>';
function Update (raves, credits, rewards)
{
	try
	{							   	
		if (amtHasChanged)
		{									
			if (IsIE() && raves>-1)
			{
				var doc = parent.document;
				var ctrlRewards = doc.getElementById(parent.spnRewardsName);
				var ctrlCredits = doc.getElementById(parent.spnCreditsName);
				var ctrlRaveCount = doc.getElementById(parent.raveCountCtrlName);
				ctrlRaveCount.innerText = raves;   
				ctrlRewards.innerText = rewards;     
				ctrlCredits.innerText = credits; 
				
				parent.$j.facebox.close();
			}
			else
			{	
				parent.location.replace(parent.location.href);
			}
		}
		else
		{	
			parent.$j.facebox.close();
		}
	}
	catch  (err)
	{						   
		// reload parent page
		parent.location.replace(parent.location.href);
	}
}
function UpdateAndClose ()
{
	Update (totRaves, totCredits, totRewards);
}
try {document.getElementById('<asp:literal id="litCloseBtnClientId" runat="server"></asp:literal>').focus();}catch(err){}
</script>
</div>
											

<div id="divBuyCredits" runat="server" style="display:none;background-color:#fffea9;height:180px;width:352px;border:solid 1px yellow;">
    <img id="Img3" src="~/images/facebox/largeAlert.gif" height="115" width="113" runat="server" style="float:left" />
	<p style="font-family:Verdana;font-size:12px;color:#4c4c4c;padding-top:50px;">You don't currently have enough Credits to purcahse this Rave.<br /></p>
	<div class="clearit"></div>
	<div class="facebox_btn" style="margin:20px 0 0 10px;">
	<asp:linkbutton id="btnBuyXtraCredits" runat="server" onclick="btnBuyXtraCredits_Click" text="Buy Extra Credits"  />
	</div>
</div>

												 
<div id="divUseOtherCurrency" runat="server" style="display:none;background-color:#fffea9;height:180px;width:352px;border:solid 1px yellow;">
    <img id="Img4" src="~/images/facebox/largeAlert.gif" height="115" width="113" runat="server" style="float:left" />
	<p style="font-family:Verdana;font-size:12px;color:#4c4c4c;padding-top:50px;">You don't currently have enough <span id="spnCurrentCurrency" runat="server"></span> to purchase this Rave, credits will be
	used instead until you gain more <span id="spnNewCurrency" runat="server"></span>.</p>
	<div class="clearit"></div>
	<div class="facebox_btn" style="margin:20px 0 0 10px;">
	<asp:linkbutton id="btnBuyWith_1" oncommand="btnBuyWith_Click" runat="server" text="Buy with Credits" />
	<asp:linkbutton id="btnBuyWith_2" oncommand="btnBuyWith_Click" text="Learn how to earn Rewards" runat="server"/>
	</div>
</div>
												 
</ajax:ajaxpanel>


</div>


<style type="text/css">
body {background-color:#fff;
background-image:none;
margin-top:0;
}
</style>

<script type="text/javascript">	

 var el = document.getElementById('__AjaxCall_Wait');
 if (el != null)
	el.style.display="none";

function SetHeight (ht)
{												  
	window.frameElement.style.height = ht + 'px';	  
}
</script>

</asp:Content>