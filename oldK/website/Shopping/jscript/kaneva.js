
function clearDefault(el) {
  if (el.defaultValue==el.value) el.value = ""
}

function clearDefaultandCSS(el) {
	if (el.defaultValue==el.value) el.value = ""
	// If Dynamic Style is supported, clear the style
	if (el.style) el.style.cssText = ""
}


function SelectAll (theElement, checkit)
{
 var theForm = theElement.form;
 for (var i = 0; i<theForm.elements.length; i++) 
  {
	if (theForm[i].type == 'checkbox')
		{theForm[i].checked = checkit;}
  }
}

function Select_All (checkit)
{
	var theForm = document.forms[0];
	for (var i = 0; i<theForm.elements.length; i++) 
	{
		if (theForm[i].type == 'checkbox' && !theForm[i].disabled)
			{theForm[i].checked = checkit;}
	}
}

function setCheckedValue (radioObj, newValue) {
	if(!radioObj)
		return;
	var radioLength = radioObj.length;
	if(radioLength == undefined) {
		radioObj.checked = (radioObj.value == newValue.toString());
		return;
	}
	for(var i = 0; i < radioLength; i++) {
		radioObj[i].checked = false;
		if(radioObj[i].value == newValue.toString()) {
			radioObj[i].checked = true;
		}
	}
}

// This routine is called by the onLoad event - Handles the init of the display
function OnLoad()
{
	// Change the "Loading..." text
	ShowLoading (true, 'Rendering...');
	// Make sure the "Loading..." display is hidden (so we can see the data)
	// Use .setTimeout() to deal with bug when page is refreshed (and "Loading..." doesn't hide)
	window.setTimeout ('ShowLoading (false);',100);
}

// Shows or Hides the "Loading..." message
function ShowLoading(blnShow, strText)
{
	var divLoadingText	= document.getElementById ('divLoadingText');
	var divData			= document.getElementById ('divData');
	var divLoading = document.getElementById('divLoading');

	if (!divLoadingText)	return;
	if (!divData)			return;
	if (!divLoading)		return;

	if (blnShow)
	{
		if (strText)	divLoadingText.innerHTML = strText;
		divData.style.display	 = 'none';
		divLoading.style.display = '';
	}
	else
	{
		divLoading.style.display = 'none';
		divData.style.display	 = '';
		divLoadingText.innerHTML = 'Loading...';
	}
}

// checkForEnterKey - this method checks to see if the enter key was pressed
// and returns true if it was the last key pressed
function checkForEnterKey (e) 
{	
	var key;	
	
	if(window.event)
		key = window.event.keyCode;     //IE
	else
		key = e.which;
		
	if (key == 13)
		return true;
	else
		return false;                      
}

// Generates and displays a thumbnail and filename for an uploaded image file.
function PreviewThumbnailUpload(fileInputId, previewTextId, previewImageId) {
    var fileInput = document.getElementById(fileInputId);
    var previewTextContainer = document.getElementById(previewTextId);
    var previewImage = document.getElementById(previewImageId);

    if (typeof (fileInput) !== "undefined" && fileInput !== null && fileInput.type === "file") 
    {
        if (typeof (previewTextContainer) !== "undefined" && previewTextContainer !== null) {
            var filename = fileInput.value;
            var lastIndex = filename.lastIndexOf("\\");
            if (lastIndex >= 0) {
                filename = filename.substring(lastIndex + 1);
            }

            if (previewTextContainer.innerText) {
                previewTextContainer.innerText = filename;
            }
            else if (previewTextContainer.textContent) {
                previewTextContainer.textContent = filename;
            }
        }

        if (typeof (previewImage) !== "undefined" && previewImage !== null
            && typeof (fileInput.files) !== "undefined"
            && typeof (fileInput.files[0]) !== "undefined" && fileInput.files[0] !== null) {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(fileInput.files[0]);

            oFReader.onload = function (oFREvent) {
                previewImage.src = oFREvent.target.result;
            };
        }
    }
}