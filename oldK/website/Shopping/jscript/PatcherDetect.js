var m_IsBlocked = false;
var checkClientLaunch;

function IsBlocked(IsBlockedURL, userId, gameId, communityId) {

    m_IsBlocked = false;

    jQuery.ajax(IsBlockedURL, {
        async: false,
        data: { "uId": userId, "gId": gameId, "cId": communityId },
        error: function (error) { m_IsBlocked = false; },
        success: function (response) {
            m_IsBlocked = false;
            if (response.responseText == "YES") {
                m_IsBlocked = true;
            }
        }
    });

    return m_IsBlocked;
}

function trackMetrics(trackingUrl, requestTrackingURLParam, requestTrackingGUID, isIEParam, userAgentParam) {
    var BrowserIE = 'false';

    if (((navigator.userAgent.indexOf('MSIE') != -1) && (navigator.userAgent.indexOf('Win') != -1) ||
        (navigator.userAgent.indexOf('Trident') != -1) && (navigator.userAgent.indexOf('Win') != -1))) {
        BrowserIE = 'true';
    }

    jQuery.ajax(trackingUrl, {
        data: { requestTrackingURLParam: requestTrackingGUID, isIEParam: BrowserIE, userAgentParam: "'" + navigator.userAgent + "'" }
    });
}

function LaunchPatcher(stpUrl, tryOn, email, playNowGuid) {

    var iframe = document.getElementById("patcherframe");

    if (typeof (iframe) === "undefined" || iframe === null) {
        var frameTag = document.createElement("iframe");
        frameTag.setAttribute("id", "patcherframe");
        frameTag.setAttribute("display", "none");
        frameTag.setAttribute("width", "0");
        frameTag.setAttribute("height", "0");
        document.body.appendChild(frameTag);
        iframe = document.getElementById("patcherframe");
    }

    stpUrl = stpUrl + (email ? "&userid=" + email : "");
    stpUrl = stpUrl + (tryOn ? "&try=" + tryOn : "");
    stpUrl = stpUrl + (playNowGuid ? "&playnowguid=" + playNowGuid : "");
    stpUrl = stpUrl + (typeof (flashVarsToken) != "undefined" && flashVarsToken ? "&" + flashVarsToken : "");

    iframe.contentWindow.location = stpUrl;
}

function CheckClientLaunch(playNowGuid, timeout, retryLimit, retryCount) {
    jQuery.ajax({
        type: "POST",
        url: root + "services/clientlaunchcheck.asmx/HasClientLaunched",
        data: '{"playNowGuid":"' + playNowGuid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (data) {
        var obj = data.d;
        if (obj == null) {
            return;
        }
        if (obj == true) {
            jQuery(".jquery-notify-bar-wrapper .notify-bar-text-wrapper").html("<div class=\"notify-logo\"></div><span class=\"notify-header\">Kaneva is Ready!</span><br/>Click on the Kaneva icon in your taskbar to play.");
            setTimeout(function () { jQuery(".notify-bar-close").click() }, timeout);
        } else if (retryCount < retryLimit) {
            if (retryCount >= retryLimit / 2) {
                jQuery(".jquery-notify-bar-wrapper .notify-bar-text-wrapper").html("<div class=\"notify-logo\"></div><span class=\"notify-header\">Launching Kaneva</span><br/>Kaneva is taking longer to load than usual. Please continue to wait.");
                jQuery(".jquery-notify-bar-wrapper .notify-bar-text-wrapper").css("width", "518px");
            }

            checkClientLaunch = setTimeout(function () { CheckClientLaunch(playNowGuid, timeout, retryLimit, retryCount +1) }, timeout);
        } else {
            jQuery(".jquery-notify-bar-wrapper .notify-bar-text-wrapper").removeAttr("style");
            jQuery(".jquery-notify-bar").addClass("error");
            jQuery(".jquery-notify-bar-wrapper .notify-bar-text-wrapper").html("<div class=\"notify-logo\"></div><span class=\"notify-header\">Oops</span><br/>There was an error launching Kaneva. <a href=\"" + nopluginUrl + "\">Click here</a>");
        }
    });
}

function DetectPlugin(CurrentPatcherVersion, WokGameId, stpUrl, tryOn, email, showNotifyBar, playNowGuid) {
    // Default behavior is to show the notification bar on detection
    showNotifyBar = (showNotifyBar == undefined || showNotifyBar);

    LaunchPatcher(stpUrl, tryOn, email, playNowGuid);

    if (showNotifyBar) {
        jQuery.notifyBar({
            html: "<div class=\"notify-logo\"></div><span class=\"notify-header\">Launching Kaneva</span><br/>Kaneva should appear in a moment.",
            close: true,
            closeOnClick: false,
            closeOnOver: false,
            position: 'bottom',
            onHide: function () { clearTimeout(checkClientLaunch) }
        });

        var timeout = 5000;
        var retryLimit = 10;
        var retryCount = 1;

        checkClientLaunch = setTimeout(function () { CheckClientLaunch(playNowGuid, timeout, retryLimit, retryCount) }, timeout);
    }
}