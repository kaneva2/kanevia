﻿/* Javascript Document: Landing Pages 10/30/2012 */

var $j = jQuery.noConflict();

$j(document).ready(function () {

    // Sign-in
    $j('.showSignIn').click(function () {
        $j('.headerLinks').hide();
        $j('.signIn').show();
    });
    $j('.signIn').hide();
    $j('.headerLinks').show();
});