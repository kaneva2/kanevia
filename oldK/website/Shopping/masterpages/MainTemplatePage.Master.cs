///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Kaneva.PresentationLayer.Shopping
{
    public partial class MainTemplatePage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ucTitleSearch.SearchTitle = _SearchTitle;
                ucTitleSearch.SearchBoxText = _SearchBoxText;
            }

            // Global Javascripts
            litJavascripts.Text = "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" + ResolveUrl("~/jscript/kaneva.js?v=1") + "\"></script>" +
                "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" + ResolveUrl("~/jscript/shop.js") + "\"></script>";
        }

        public string SearchTitle
        {
            get { return _SearchTitle; }
            set { _SearchTitle = value; }
        }

        public string SearchBoxText
        {
            get { return _SearchBoxText; }
            set { _SearchBoxText = value; }
        }

        private string _SearchTitle = "Shop Kaneva";
        private string _SearchBoxText = "All";
    }
}
