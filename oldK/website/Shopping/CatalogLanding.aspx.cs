///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace Kaneva.PresentationLayer.Shopping
{
    public class CatalogLanding : BasePage
    {
        #region Declarations
        protected Repeater rptItems;
        protected Literal litTopSwf, litBottomSwf, litBtmSwfHeight, litSoundStyle;
        protected HtmlContainerControl dvTopFlash, dvBottomFlash, subImgs;
        protected HyperLink topImg, bottomImg, bottomImg2;

        List<uint> listCategoryIds = new List<uint> (); 

        #endregion

        #region Page Load
        protected void Page_Load (object sender, EventArgs e)
        {
            //get the parameters
            GetRequestParams ();

            if (!IsPostBack)
            {
                //retrieve the data for the top level category
                //and set the 
                GetTopLevelData ();

                //bind the CatalogItems
                BindItemData ();
            }
        }
        #endregion

        #region Helper Functions

        //delegate function used in LIst FindAll search
        private bool IsDesiredTopCategory (ItemCategory category)
        {
            return category.ItemCategoryId == listCategoryIds[0];
        }

        /// <summary>
        /// BindItemData
        /// </summary>
        private void GetTopLevelData ()
        {
            try
            {
                //this get the cached top level categories
                List<ItemCategory> topCatList = WebCache.GetTopLevelCategories ();

                if (topCatList.Count > 0)
                {
                    //find the data for the one in the parameters
                    ItemCategory topCatData = topCatList.Find (IsDesiredTopCategory);
  //                  ItemCategory subCatData = (topCatData == null ? GetShoppingFacade.GetItemCategory (categoryId) : null);
                    ItemCategory subCatData = (topCatData == null ? GetShoppingFacade.GetItemCategory (listCategoryIds[0]) : null);

                    //check to make sure the category was found - default id = 0
  //                  if ((topCatData != null && topCatData.ItemCategoryId == categoryId) ||
  //                      (subCatData != null && subCatData.ItemCategoryId == categoryId && !string.IsNullOrWhiteSpace (subCatData.MarketingPath)))
                    if ((topCatData != null && topCatData.ItemCategoryId == listCategoryIds[0]) ||
                        (subCatData != null && subCatData.ItemCategoryId == listCategoryIds[0] && !string.IsNullOrWhiteSpace (subCatData.MarketingPath)))
                    {
                        //if we're dealing with a sub-category, use it instead
                        topCatData = (topCatData ?? subCatData);

                        //set the page title and search bar
                        ((MainTemplatePage) Master).SearchTitle = ((MainTemplatePage) Master).SearchBoxText = Server.HtmlDecode (topCatData.Name);

                        try
                        {
                            if (string.IsNullOrWhiteSpace (topCatData.MarketingPath))
                            {
                                // make the flash stuff go away
                                dvTopFlash.Visible = false;
                                dvBottomFlash.Visible = false;

                                // go to the search page to show 3 rows
                                Response.Redirect ("catalog.aspx?cId=" + string.Join("|", listCategoryIds.ToArray()) + "&s=");
                            }
                            else
                            {
                                //get the swfs for the page
                                //this is temporary functionality that will become uncessary with new shop site
                                string swfPaths = topCatData.MarketingPath;

                                //load the marketing flash movies										
                                if (swfPaths.Length > 1)
                                {
                                    string[] paths = swfPaths.Split ('|');
                                    if (swfPaths.Contains (".jpg") || swfPaths.Contains (".gif"))
                                    {
                                        dvTopFlash.Visible = false;
                                        dvBottomFlash.Visible = false;

                                        if (!string.IsNullOrWhiteSpace (paths[0]) && paths[0] != "#")
                                        {
                                            topImg.ImageUrl = paths[0];
                                            topImg.NavigateUrl = "Catalog.aspx?s=&cId=" + Request.Params["cId"].ToString ();
                                            topImg.Visible = true;
                                        }
                                        else if (paths[0] == "#")
                                        {
                                            //# used as a placeholder for instances in which we want to use marketing path but don't want top image
                                            rptItems.Visible = false;
                                            subImgs.Style["padding-top"] = "5px";
                                        }

                                        if (paths.Length > 1)
                                        {
                                            bottomImg.ImageUrl = paths[1];
                                            bottomImg.Visible = true;
                                            bottomImg.NavigateUrl = "Catalog.aspx?cId=" + paths[2];
                                            bottomImg.Style.Add ("display", "inline");
                                            subImgs.Visible = true;
                                        }

                                        if (paths.Length > 4)
                                        {
                                            bottomImg2.ImageUrl = paths[3];
                                            bottomImg2.Visible = true;
                                            bottomImg2.Style.Add ("display", "inline");
                                            bottomImg2.NavigateUrl = "Catalog.aspx?cId=" + paths[4];
                                        }
                                    }
                                    else
                                    {
                                        litTopSwf.Text = paths[0];
                                        if (paths.Length > 1)
                                        {
                                            litBottomSwf.Text = paths[1];

                                            //another temporary hack not needed once site moves
                                            if (paths[1].IndexOf ("furnishingsFeature-wrapper") > 0)
                                            {
                                                litBtmSwfHeight.Text = "920";
                                            }
                                            else
                                            {
                                                litBtmSwfHeight.Text = "315";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch { }

                    }
  //                  else if (categoryId > 0)
                    else if (listCategoryIds[0] > 0)
                    {
                        // This is not a top-level category. Try just showing the catalog page.
                        Response.Redirect ("catalog.aspx?cId=" + listCategoryIds[0]);
                    }
                }
            }
            catch { }
        }


        /// <summary>
        /// BindItemData
        /// </summary>
        private void BindItemData ()
        {
            bool showRestricted = false;

            try
            {
                showRestricted = UserHasMaturePass ();
            }
            catch (Exception) { }

            PagedList<WOKItem> plWokItems = GetShoppingFacade.SearchItems ("", 0, listCategoryIds[0], 8, 1, "date_added DESC", showRestricted, false, 0);
            rptItems.DataSource = plWokItems;
            rptItems.DataBind ();

            try
            {
                if (listCategoryIds[0] > 0)
                {
                    if ((UGCUpload.eType)WebCache.GetCachedUploadType(listCategoryIds[0]) == UGCUpload.eType.SND_OGG_KRX)
                    {
                        litSoundStyle.Text = "<style>ul#browse li a{ text-indent: 0px; background: #ffffff; filter:alpha(opacity=85); " +
                            "-moz-opacity:0.85; -khtml-opacity: 0.85; opacity: 0.85; }</style>";
                    }
                }
            }
            catch { }
        }

        public void GetRequestParams ()
        {
            if (Request["cId"] != null)
            {
                try
                {
                    string[] ids = Request["cId"].ToString ().Split ('|');

                    for (int i = 0; i < ids.Length; i++)
                    {
                        listCategoryIds.Add(uint.Parse (ids[i]));
                    }
                }
                catch (Exception) { };
            }
            else if (Request["cName"] != null)
            {
                try
                {
                    string cName = Request["cName"].ToString ();

                    // Check parent categories if specified
                    if (Request["pcName"] != null)
                    {
                        string pcName = Request["pcName"].ToString ();
                        ItemCategory parentCat = GetShoppingFacade.GetItemCategory (pcName);

                        List<ItemCategory> catList = GetShoppingFacade.GetItemCategoriesByParentCategoryId (parentCat.ItemCategoryId);
                        foreach (ItemCategory cat in catList)
                        {
                            if (cat.Name.Equals (cName))
                            {
   //                             categoryId = cat.ItemCategoryId;
                                listCategoryIds.Add (cat.ItemCategoryId);
                                break;
                            }
                        }
                    }
                    // Get category by name directly
                    else
                    {
    //                    ItemCategory cat = GetShoppingFacade.GetItemCategory (cName);
    //                    categoryId = cat.ItemCategoryId;
                        listCategoryIds.Add (GetShoppingFacade.GetItemCategory (cName).ItemCategoryId);
                    }

                    // If we find valid categories, restart processing using "cId" param for Navigation.ascx.
                    if (listCategoryIds.Count > 0)
                    {
                        Server.Transfer("~/CatalogLanding.aspx?cId=" + string.Join("|", listCategoryIds), false);
                    }
                }
                catch (Exception) { };
            }
        }
        #endregion
    }
}
