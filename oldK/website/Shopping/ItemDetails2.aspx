﻿<%@ Page Language="C#" MasterPageFile="~/masterpages/MainTemplateNoLeftNav.Master" AutoEventWireup="true" CodeBehind="ItemDetails2.aspx.cs" Inherits="Kaneva.PresentationLayer.Shopping.ItemDetails2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="cntUpload" runat="server" ContentPlaceHolderID="cphBody">
	<script src="jscript/PatcherDetect.js?v5" language="JavaScript" type="text/javascript"></script>
	<script src="jscript/jquery/jquery-ui.min.js" language="JavaScript" type="text/javascript"></script>
	<script src="jscript/facebox/jquery.facebox.js" language="JavaScript" type="text/javascript"></script>
	<script type="text/javascript">    
		<!--
		var $j = jQuery.noConflict();
		var pn_js = function playnow() {}
		// Length of Comment
		var textboxtextSave = '';
		var maxChars = 256;
		var gId = <asp:literal id="litGlobalId" runat="server"></asp:literal>;

		$j(document).ready(function () {

			$j.ajax({url: 'VIPPriceData.aspx?gId=' + gId, success: function(result){
			    var isVIP = <asp:literal id="litIsVIP" runat="server"></asp:literal>;
			    var style = "";
			    if (isVIP) {
			        style = "color:#997b62;font-size:14px;";
			    } 
			    $j('#pVIPPrice').html('<span style="' + style + '">' + result + ' ' + $j('#lblVIPPurchaseType').text() + '</span>');
			}});

			<asp:literal id="litLoadDeedItems" runat="server"></asp:literal>

			popFacebox = function () {
				$j.facebox({ iframe: 'buyRave.aspx?globalId=' + gId});
			};

			$j('.infoButton').on('mouseenter mouseleave', function () {
				$j('.smartObjectInfo').toggle();
			});

			msg_dialog = $j('<div></div>')
				.html('')
				.dialog({
					autoOpen: false,
					resizable: false,
					modal: true,
					height: 192,
					width: 367,
					buttons: [
						{
							text: "OK",
							id: "dialogButtonOK",
							click: function () {
								$j(this).dialog("close");
							}
						},
						{
							text: "Cancel",
							click: function () {
								$j(this).dialog("close");
							}
						}
					],
					open: function () {
						$j(":button:contains('OK')").removeClass('dialogButton okButton');
						$j(":button:contains('OK')").addClass('dialogButton okButton');

						$j(":button:contains('Cancel')").removeClass('dialogButton cancelButton');
						$j(":button:contains('Cancel')").addClass('dialogButton cancelButton');
					}
				});

		});
		
		function openMsgDialog(title, msg, okurl) {
			msg_dialog.dialog({ title: title });
			msg_dialog.html(msg);
			
			if (okurl) {
				$j(":button:contains('OK')").unbind("click").click(
					function () {
						parent.document.location = okurl;
					}
				);

				$j(":button:contains('Cancel')").prop("disabled", false);
				$j(":button:contains('Cancel')").show();
				$j(":button:contains('OK')").removeClass('okButtonCenter');
			}
			else {
				$j(":button:contains('OK')").unbind("click").click(
					function () {
						msg_dialog.dialog("close");
					}
				);
				
				$j(":button:contains('Cancel')").prop("disabled", true);
				$j(":button:contains('Cancel')").hide();
				$j(":button:contains('OK')").addClass('okButtonCenter');
			}

			msg_dialog.dialog('open');
		}

		function TryOnDeed(url, gameId, email, version, playNowGuid)
		{
			url += "/?";
			TryIt(url, gameId, 0, '0', email, version, playNowGuid);
		}

		function TryOn(gameId, gId, useType, email, version, playNowGuid)
		{
			var url = 'kaneva://' + gameId + "/?";
			TryIt(url, gameId, gId, useType, email, version, playNowGuid);
		}

		function TryIt (url, gameId, gId, useType, email, version, playNowGuid)
		{																  
			var strTryOn = '';
			if (gId > 0)
			{
				strTryOn = '' + gId + ':' + useType;
			}

			DetectPlugin(version, gameId, url, strTryOn, email, true, playNowGuid);
		}

		function CharsRemain(t, span_id)
		{								 						   
			var val = $j(t).val().length;
			
			if (val > maxChars) {
				$j(t).val(textboxtextSave);
			}
			else {	 
				$j('#'+span_id).text(maxChars-val+' ');
				textboxtextSave = $j(t).val();
			}   
		}
		  
		function ProcessOrder ()
		{										 			  
			$j('#divComplete').hide();
			$j('#tblBalances').hide();	  
			$j('#divProcessing').show();
		}
		  
		function ResetProcess ()
		{													 
			$j('#divComplete').hide();
			$j('#divProcessing').hide();
			$j('#tblBalances').show();
		}	
			
		function LogGoogleAnalyticsCommerce (account, orderId, total, city, state, country, sku, name, cat, price, quantity)
		{
			var _gaq2 = _gaq || []; 
			_gaq2.push(['t2._setAccount', account]); 
			_gaq2.push(['t2._setDomainName', 'kaneva.com']);
			_gaq2.push(['t2._trackPageview']); 
			_gaq2.push(['t2._addTrans', 
				orderId,    // order ID - required 
				'Shop',		// affiliation or store name 
				total,      // total - required 
				'0',        // tax 
				'0',        // shipping 
				city,       // city 
				state,		// state or province 
				country     // country 
			]); 
		 
			// add item might be called for every item in the shopping cart 
			// where your ecommerce engine loops through each item in the cart and 
			// prints out _addItem for each  
			_gaq2.push(['t2._addItem', 
				orderId,    // order ID - required 
				sku,        // SKU/code 
				name,       // product name 
				cat,		// category or variation 
				price,      // unit price - required 
				quantity    // quantity - required 
			]); 
			_gaq2.push(['t2._trackTrans']); //submits transaction to the Analytics servers 
		}

		function popWarning ()
		{							 
			var availHeight; 
			var availWidth; 
			var divContain = $('mainContent');
			availHeight = divContain.offsetHeight;
			availWidth = divContain.offsetWidth; 
			var popupHeight = 230; 		
			var popupWidth = 400; 		
			var pop = $('divWarning');
			pop.style.top = (availHeight-popupHeight)/2;			
			pop.style.left = (availWidth-popupWidth)/2;
			pop.style.width = popupWidth + "px";
			pop.style.height = popupHeight + "px";
			pop.style.display = "block"; 		
		}
		
		function showMsg()
		{			 
			var type = $j('#ddlWorlds option:selected').attr('type');
			var msgbox = $j('#existingMsgbox');
			var show = false;
			var head = '<p class="bold warning">Warning:</p>';
					
			switch (type)
			{
				case '2':
					txt='<span class="bold warning">This entire world will be replaced</span>. All objects will be removed from the world and the game system.<br/><span class="bold">This cannot be undone.</span></p>';
					show = true;
					break;
				case '3':
					head = '<p class="bold">Note</p>';
					txt='Find this World in your <span class="bold">Import World Menu</span> in world.</p>';
					show = true;
					break;
				case '5':
					txt='This new world will <span class="bold">overwrite your Home</span>. All items in your Home will be returned to your inventory/storage.</p>';
					show = true;
					break;
			}
			
			if (show)
			{
				msgbox.html(head+txt);
				msgbox.show();
			}
			else {msgbox.hide();}
		}
				
		function showzone(id)
		{
			if (!$j('#'+id).hasClass('selected'))
			{
				$j('[name="zonesection"].selected').nextAll('div .blank').slideUp();
				$j('[name="zonesection"]').removeClass('selected');
			
				$j('#'+id).addClass('selected');
				$j('#'+id + ' input:radio').prop('checked',true);
				$j('#'+$j('#'+id + ' input:radio').attr('id')+'Msg').slideToggle();
			}
		}

		$j( function() {
		    $j("#tabs").tabs({
		        activate: function( event, ui ) {
		            if (ui.newPanel.attr("id") == 'tabReviews') {
		                $j("#Review").show();
		                $j("#btnAddReview").show();
		            }
		            else {
		                $j("#btnAddReview").hide();
		            }  
		        }
		    });
		    $j("#tabs").show();
		    if ($j("#tabReviews").is(':visible')){
		        $j("#btnAddReview").show();
		    }
		});

		

		$j(function () {
			$j('#imgUseType').tooltip({
				position: {
					my: "left-34 bottom+64",
					at: "bottom",
					using: function (position, feedback) {
						$j(this).css(position);
						$j("<div>")
						.addClass("arrow")
						.addClass(feedback.vertical)
						.appendTo(this);
					}
				}
			});
		});
		<asp:literal id="litGMPurchase" runat="server"></asp:literal>
		
	//--> 
	</script>
	
	<link href="jscript/facebox/jquery.facebox.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="jscript/jquery/jquery-ui.min.css" type="text/css" media="all" />
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <style>
	  .ui-tooltip, .arrow:after {
		background: #fff;
		border: 1px solid #ccc;
		width: 290px;
	  }
	  .ui-tooltip {
		padding: 10px 20px;
		color: #333;
		box-shadow: 0 0 2px #000; 
	  }
	  .arrow {
		width: 35px;
		height: 12px;
		overflow: hidden;
		position: absolute;
		left: 280px;
		margin-left: 0px;	  
		bottom: 0px;
	  }
	  .arrow.top {
		top: -12px;
		bottom: auto;
	  }
	  .arrow.left {
		left: 0;
	  }
	  .arrow:after {
		content: "";
		position: absolute;	  
		left: 6px;
		top: -0px;
		width: 14px;
		height: 26px;
		box-shadow: 6px 5px 9px -9px black;
		-webkit-transform: rotate(45deg);
		-ms-transform: rotate(45deg);
		transform: rotate(45deg);
	  }
	  .arrow.top:after {
		bottom: -20px;
		top: auto;
	  }
	  .adminfree, .purchasenew a.adminfree {
		  float:right; 
		  margin-top:8px;
		  border:none;
		  background:transparent;
		  font-weight:bold;
		  text-decoration:underline;
	  }
	  .adminfree {margin-top:20px;}
	  .purchasenew a.adminfree:hover {text-decoration:none;}
	</style>

	<div id="divAccessMessage" runat="server" visible="false">
		<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
			<tr>
				<td valign="top" align="center">
					<table border="0" cellpadding="0" cellspacing="0" width="75%">
						<tr>
							<td class="frTopLeft"></td>
							<td class="frTop"></td>
							<td class="frTopRight"></td>
						</tr>
						<tr>
							<td class="frBorderLeft">
								<img id="Img1" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td valign="top" class="frBgIntMembers">
								<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td valign="top" align="center">
											<div class="module whitebg">
												<span class="ct"><span class="cl"></span></span>
												<h2 id="h2Title" runat="server"></h2>
												<table cellpadding="6" cellspacing="0" border="0" width="95%">
													<tr>
														<td rowspan="3" valign="top">
															<img src="../images/access_pass.png" border="0" />
														</td>
														<td align="left" valign="top" id="tdActionText" runat="server" style="padding:0 20px 20px 20px;width:50%"></td>
													</tr>
													<tr>
														<td align="center" valign="top" id="tdButton" runat="server">
															<asp:Button ID="btnAction" runat="server"></asp:Button>
														</td>
													</tr>
													<tr>
														<td align="left" valign="top" id="tdActionText2" runat="server" style="padding:20px 20px 20px 20px;width:50%"></td>
													</tr>
													<tr>
														<td colspan="2" align="left" valign="top" id="tdPolicyText" runat="server"></td>
													</tr>
												</table>
												<span class="cb"><span class="cl"></span></span>
											</div>
										</td>
									</tr>
								</table>
							</td>
							<td class="frBorderRight"><img id="Img2" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
						</tr>
						<tr>
							<td class="frBottomLeft"></td>
							<td class="frBottom"></td>
							<td class="frBottomRight"></td>
						</tr>
						<tr>
							<td><img id="Img3" runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	
	<asp:scriptmanager id="smMain" runat="server" enablepartialrendering="true"></asp:scriptmanager>

	<div id="divDetails" runat="server" visible="false" style="width:879px;display:inline-block;margin:0 25px;border:0px solid red;">
		<!-- top row with back button and category breadcrumb -->
		<div id="previous" style="width:879px;text-align:left;">
			<div style="float:left;width:40%;">
			<asp:updatepanel id="upBack" runat="server">
				<contenttemplate>
					<p id="pBack" runat="server"><a id="A1" href="JavaScript:history.back(1)">&#171; Back to Previous Page</a></p>
				</contenttemplate>
			</asp:updatepanel>
			</div>
			<div style="float:right;text-align:right;width:50%;"><p>
				<asp:Repeater ID="rptCategories" runat="server">
					<headertemplate>Category: </headertemplate>
					<ItemTemplate>
						<a href="<%# (Convert.ToInt32(DataBinder.Eval(Container.DataItem, "ParentCategoryId")) == 0 ? "CatalogLanding" : "Catalog") %>.aspx?cId=<%# DataBinder.Eval(Container.DataItem, "ItemCategoryId")%>"><%# DataBinder.Eval(Container.DataItem, "Description") %></a>
					</ItemTemplate>
					<separatortemplate> > </separatortemplate>
				</asp:Repeater>
			</p></div>
		</div>
		
        <div class="clearit"></div>
		<div id="content" style="margin-top:10px;">
			
			<!-- LEFT COLUMN -->
			<div id="left_column" style="border:0px solid green;display:inline-block;width:352px;float:left;margin-right:20px;">
				<!-- Item Preview -->
				<div class="itemImage img-center" style="clear:both;width:348px;height:268px;background-color:#f6f6f6;border:0px solid #a43799;text-align:center;overflow:hidden;">
					<iframe id="ifItemPreview" runat="server" scrolling="no" width="320" height="240" frameborder="0" style="margin-top:0px;" visible="false"></iframe> 					
					<img runat="server" id="imgItem" src="" alt="" />
					<div id="divRestricted" class="restricted" runat="server"></div>
				</div>
				<!-- Item Preview End -->
				
				<!-- Rave and Report This Row -->
		    <asp:updatepanel id="Updatepanel1" runat="server">
			    <contenttemplate>
				<div style="border: 0px solid blue; display: inline-block; width: 99%;margin-top:16px;margin-bottom:26px;">
					<div id="ravethis" style="float:left;">
						<asp:LinkButton ID="lbRave" runat="Server" OnClick="lbRave_OnClick" Text="" OnClientClick="_gaq.push(['_trackPageview','/clickRaveItem.aspx'])">
						<span><asp:literal ID="litRaveCount" runat="server"/></span></asp:LinkButton>
					</div>
					<asp:LinkButton ID="lbRpt" runat="Server" CssClass="btn_rpt" style="float: right;" Text="" OnClientClick="_gaq.push(['_trackPageview','/clickReportItem.aspx'])">Report This</asp:LinkButton>
				</div>
                    </contenttemplate>
                </asp:updatepanel>

				<div class="clearit"></div>

				<!-- Related Items Grid -->
				<asp:Panel ID="pnlRelated" runat="server" clientidmode="static">
					<div id="related" style="border:0px solid red; width:348px; margin: auto;">
						<h3 style="margin-top:0;text-align:left;">Related Items</h3>
						<ul id="browse" style="width:348px">
							<asp:Repeater ID="rptItems" runat="server" >
								<ItemTemplate>
									<li class="img-center" style='<%# GetStyleImage (DataBinder.Eval (Container.DataItem, "ThumbnailLargePath").ToString (), "la")%>; width: 86px; height: 86px;'>
										<a style="" href='<%# GetItemDetailsURL (DataBinder.Eval (Container.DataItem, "GlobalId").ToString ())%>'>
											<%# DataBinder.Eval (Container.DataItem, "Name") %>
											<div id="Div2" class="animation" runat="server" visible='<%# Convert.ToBoolean ( DataBinder.Eval(Container.DataItem, "IsAnimated")) %>'><!-- empty --></div>
											<span class="price"><%# DataBinder.Eval(Container.DataItem, "WebPrice")%></span></a>
									</li>
								</ItemTemplate>
							</asp:Repeater>
						</ul>
					</div>
				</asp:Panel>

				<div class="clearit"></div>
				
				<!-- Social Buttons -->
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div style="margin:40px 0;text-align:center;">
                    <div style="float:left;margin:0 0 8px 10px;">Share <asp:literal id="litShareItemName" runat="server"></asp:literal> :</div>
                    <div class="addthis_inline_share_toolbox" ></div>
                </div>    			

			</div>
			
			<!-- RIGHT COLUMN -->
			<asp:updatepanel id="upHidePurchase" runat="server" updatemode="conditional">
				<contenttemplate>
					<span id="spnHideDuringPurchase" runat="server">

			            <div id="right_column" style="border:0px solid blue;display:inline-block;float:right;width:503px;">

				            <div style="border:0px solid red;min-height:320px;">
                                <div style="float:left;">
					                <h2 style="text-align:left;margin:0;font-size:22px;font-weight:bold;width:430px;line-height:20px;"><asp:Label runat="server" ID="lblName"></asp:Label>
						                <span id="spnAnimationOnly" runat="server" visible="false" style="padding-left:2px;font-size:10px;font-weight:normal;">(animation only)</span>
					                </h2>
					                <div class="clearit"></div>
				
					                <div style="margin-top:11px;">Created by: <a id="aUser" runat="server" href="#"><asp:Label runat="server" ID="lblUsername"></asp:Label></a></div>
				                </div>
                                <div style="float:right;width:68px;overflow:hidden;"><img id="imgUseType" clientidmode="Static" runat="server" title="" /></div>
				
				                <div class="clearit"></div>
				
				                <div id="pricing" style="border-top:1px solid #dcdcdc;margin-top:6px;width:100%;display:inline-block;">
					                <div style="float:left;width:310px;border:0px solid red;margin-top:18px;">
						                <h3 id="pricecontainer" runat="server" style="border:0px solid red;text-align:left;width:300px;height:28px; ">
							                <div id="divListPriceLabel" runat="server" style="font-weight:bold;font-size:13px;float:left;width:90px;color:#ccc;">Your Price:</div>
							                <div id="listPrice" runat="server" style="font-size:14px;float:left;margin-right:5px;color:#ccc;font-weight:bold;"><asp:Label runat="server" ID="lblPrice" style=""></asp:Label></div>
							                <asp:label runat="server" style="font-size:13px;color:#ccc;font-weight:bold;" id="lblPurchaseType"></asp:label>
						                </h3>
							            <h3 id="h3VIP" runat="server" style="text-align:left;width:320px;background-color:#f6f6f6;height:47px;border:0px solid green;">
							                <div id="vippricecontainer" runat="server" clientidmode="static" style="display:inline-block;width:304px;margin:8px;">
						                        <a href="#" runat="server" id="lnkVIP"><img src="images/vip_save.png" style="float:left;margin-right:10px;display:block;"/></a>
								                <div id="divVIPPriceLabel" runat="server" style="font-size:13px;float:left;width:70px;">VIP Price:</div>
                                                <div id="pVIPPrice" style="float:left;font-size:13px;margin-right:5px;width:120px;"><img id="Img6" class="imgLoading" runat="server" src="~/images/ajax-loader_sm.gif" alt="Loading..." border="0"/><span class="loadingText"></span></div> 
								                <asp:Label runat="server" font-size="13px" ID="lblVIPPurchaseType" clientidmode="static" style="display:none;"></asp:Label>
						                        <div id="divVIPSave" runat="server" visible="true" style="border:0px solid red;color:#6f35cc;float:left;font-size:11px;line-height:12px;text-align:center;clear:right;"></div>
                                                <div id="buyVIPContainer" runat="server" visible="true" style="margin:0;padding-left:3px;float:right;"><a href="javascript:void(0);" id="aBuyVIP" runat="server" style="font-size:10px;font-weight:normal;">Learn more</a></div>
                                            </div>
						                </h3>
					                </div>

					                <div id="buttons" class="floatRight" style="border:0px solid black;margin-top:11px;">
						                <asp:LinkButton id="btn_purchase" name="btn_purchase" runat="Server" CssClass="btnxlarge orange" style="margin-bottom:10px;"
							                OnClick="lbPurchase_OnClick" Text="Buy This Item" ToolTip="Buy This Item" OnClientClick="_gaq.push(['_trackPageview','/clickPurchaseItemClick.aspx'])"></asp:LinkButton>
						
						                <asp:linkbutton id="btn_preview" name="btn_preview" runat="server" CssClass="btnxlarge grey" text="Try This" onclientclick="_gaq.push(['_trackPageview','/clickTryItem.aspx'])"></asp:linkbutton>
					                </div>
				                </div>
				                <div class="clearit"></div>
                    
                                <div style="height:16px;margin:8px 0 10px 0;">
                                    <div id="divRequiredItem" runat="server" visible="false" style="clear:both;display:block;float:left;"><span style="color:Red;font-weight:bold;font-size:12px;">REQUIRES: </span> <a id="aReqItem" runat="server"></a></div>
                                    <asp:linkbutton id="lbGetForFree" OnClientClick="GMPurchase();" onclick="lbGetForFree_OnClick" class="adminfree" runat="server" visible="false" style="margin:0 2px 0 0;float:right;" text="Admin Get For Free &#187;"></asp:linkbutton>
                                </div>
                                <div id="description">	
					                <span class="bold" style="font-size:13px;">Description:</span> 
					                <div><asp:Label runat="server" ID="lblDescription" visible="false"></asp:Label></div>
				                </div>
				   
	                            <a runat="server" style="clear:both;margin:14px 2px 0 0;float:right;" id="aAdminEdit" visible="false">Admin Edit</a>
                                <div class="clearit"></div>
                            	<div id="divOwnerBtns" runat="server" visible="false" class="" style="width:100%;display:inline-block;clear:both;text-align:center;">
			                        <div style="width:55%;margin:auto;">
                                    <a runat="server" id="aOwnerEdit" visible="false" class="btnsmall grey" style="float:left;font-size:12px;">Edit Item</a>
                                    <a id="aTransTracker" runat="server" class="btnsmall grey" style="float:right;font-size:12px;">View My Sales</a>
	                                </div>
                                </div>
                            
                            </div>
                
                            <!-- TABS SECTION -->
                            <div id="tabs" style="display:none; border: none;border-bottom: 1px solid #dcdcdc; max-height: 285px;">
                                <ul>
                                    <li id="liTabDeedItems" runat="server" visible="false"><a href="#tabDeedItems">Included in this Pre Built World</a></li>
                                    <li id="liTabBundleItems" runat="server" visible="false"><a href="#tabBundleItems" id="aTabBundles" runat="server">Included in this Bundle</a></li>
                                    <li id="liTabAnimations" runat="server" visible="false"><a href="#tabAnimations">Animations for this Item</a></li>
                                    <li><a href="#tabReviews">Reviews (<asp:Label ID="lblNumberOfReviews1" runat="server" />)</a></li>
                                </ul>
                                <div id="tabReviews">
						            <asp:updatepanel id="upReviews" runat="server" updatemode="conditional">
                                        <contenttemplate>
                                            <div id="userReview1" class="tab-data-container" style="height:auto;">
							                    <!-- Add server functionality to change the display atribute from none to block if there are no reviews -->
							                    <div runat="server" ID="litNoReviews" Visible="False" style="text-align:center;font-size:16px;border:none;margin-top:40px;font-weight:bold;">
								                    Be the first to review this item.
							                    </div>
							                    <asp:Repeater ID="rptReviews" runat="server" onitemdatabound="rptReviews_ItemDataBound">
								                    <headertemplate>
                                                        <table style="margin-top:10px;padding:0;width:99%;"><tbody>
								                    </headertemplate>
                                                    <ItemTemplate>
									                    <!-- Start Review -->
									                    <tr>
                                                            <td valign="top" style="padding-top:5px;padding-bottom:5px;width:65px;">
                                                                <div class="img-center" style="width:50px;height:50px;border:1px solid #f0f0f0;margin-right:15px;">
											                        <img src='<%# GetProfileImageURL (DataBinder.Eval (Container.DataItem, "ThumbnailSmallPath").ToString(), "sq", "M") %>' alt="" class="floatLImg" />
										                        </div>
                                                            </td>
                                                            <td style="padding-top:5px;padding-bottom:5px">
                                                                <a href='<%# GetPersonalChannelUrl (DataBinder.Eval (Container.DataItem, "username").ToString()) %>' style="margin-bottom:2px;font-size:13px;font-weight:normal;">
												                    <%# DataBinder.Eval (Container.DataItem, "Username") %></a><br />
											                    <div style="font-size:12px;margin:0;padding:0;"><%# DataBinder.Eval (Container.DataItem, "Comment") %></div>
											                    <asp:LinkButton style="float:right;color:#999;" id="lbDeleteComment" runat="server" visible="false" text="Delete" 
												                    commandargument='<%# DataBinder.Eval (Container.DataItem, "ReviewId") %>' onclick="lbDeleteReview_OnClick" cssclass="btn_deleteReview"></asp:LinkButton>
                                                            </td>
									                    </tr>
									                    <!-- End Review -->
								                    </ItemTemplate>
                                                    <footertemplate>
                                                        </tbody></table>
                                                    </footertemplate>
							                    </asp:Repeater>
						                    </div>
                                            <!-- start the hidden Add Reviews column -->
						                    <div id="addNewReview"  style="display: none;position:absolute; left: 0px; bottom: 0px; width: 501px; height:254px;background: rgba(255, 255, 255, 0.7);">
                                                <div style="position:absolute; left: 0px; bottom: 0px;width: 500px; height: 197px; border: 1px solid #b8b8b8; background-color: #e3e3e3;opacity:1;">
							                        <div style="width:470px;margin:auto;margin-top:16px;">
								                        <a href="javascript:void(0);" onclick="$j('#addNewReview').hide();return false;" style="position:absolute;top:10px;right:10px;">X</a>
                                                        <h2>Tell us what you think:</h2>
								                        <div class="note"><span id="spnRemain">256</span> characters remaining</div>
								                        <div class="clearit"></div>
								                        <textarea runat="server" id="txtComment" rows="5" style="width: 462px;margin-bottom:10px;" name="txtComment"
									                        onkeyup="CharsRemain(this,'spnRemain')" maxlength="140"></textarea>
								                        <asp:linkbutton ID="lbSubmitComment" runat="Server" OnClick="lbSubmitComment_OnClick" style="float:right;padding-top:5px;color:#fff;"
									                        CssClass="btnmedium blue" Text=" Submit "></asp:linkbutton>
							                        </div>
						                        </div>
                                            </div>
                                        </contenttemplate>
                                        <triggers>
                                            <asp:asyncpostbacktrigger controlid="lbSubmitComment" />
                                        </triggers>
                                    </asp:updatepanel>
                                </div>
					            <div id="tabDeedItems" clientidmode="static" runat="server" visible="false">
						            <div class="tab-data-container">
							            <div id="divDeedItemsData">
                                            <div style="text-align:center;margin-top:30px;display:inline-block;width:99%;">
                                                <img id="Img4" class="imgLoading" runat="server" src="~/images/ajax-loader.gif" alt="Loading..." border="0"/>
                                            </div> 
                                        </div>
						            </div>
					            </div>
					            <div id="tabBundleItems" clientidmode="static" class="bundleItems" runat="server" visible="false">
						            <div class="tab-data-container">
						                <asp:repeater id="rptBundleItems" runat="server" >
							                <itemtemplate>
								                <div class="biRow" style="clear:both;margin-top:7px;display:block;height:26px;">
				                                    <div class="floatLeft" style="width:25px;height:25px;border:1px solid #f0f0f0;margin-right:15px;overflow:hidden;">
                                                        <div class="img-center" style="width:45px;height:45px;">
                                                            <img src='<%# GetItemImageURL (DataBinder.Eval (Container.DataItem, "ThumbnailSmallPath").ToString(), "sm")%>' />
                                                        </div>
                                                    </div>
									                <div class="floatLeft" style="margin-top:4px;">
                                                        <a href='ItemDetails.aspx?gId=<%# DataBinder.Eval (Container.DataItem, "GlobalId") %>'><%# DataBinder.Eval (Container.DataItem, "DisplayName") %></a>
									                    <%# ShowBundleItemQuantity (DataBinder.Eval (Container.DataItem, "Quantity"), DataBinder.Eval (Container.DataItem, "UseType"))%>	
								                    </div>
                                                </div>
							                </itemtemplate>
						                </asp:repeater>
						            </div>
					            </div>
					            <div id="tabAnimations" clientidmode="static" runat="server" visible="false" style="padding-top:10px;">
						            <div id="divAnimations" class="divBundleListTitle" runat="server"></div>
						            <div class="tab-data-container">
						                <asp:repeater id="rptAnimations" runat="server" >
							                <itemtemplate>
								                <div class="biRow" style="display:block;clear:both;height:47px;margin-bottom:6px;">
									                <div class="floatLeft" style="width:45px;height:45px;border:1px solid #f0f0f0;margin-right:15px;overflow:hidden;">
                                                        <div class="img-center" style="width:45px;height:45px;">
                                                            <img style="width:100%;" src='<%# GetItemImageURL (DataBinder.Eval (Container.DataItem, "ThumbnailLargePath").ToString(), "la")%>' alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="floatLeft" style="margin-top:15px;">
									                    <a href='ItemDetails.aspx?gId=<%# DataBinder.Eval (Container.DataItem, "GlobalId") %>'><%# DataBinder.Eval (Container.DataItem, "DisplayName") %></a>
                                                    </div>
								                </div>
							                </itemtemplate>
						                </asp:repeater>
						            </div>
					            </div>
                    
                            </div>
                				
                            <div style="width:100%;text-align:center;margin-top:10px;height:30px;">
                                <asp:LinkButton id="btnAddReview" runat="Server" CssClass="btnmedium grey" style="margin:auto;padding-top:5px;display:none;" onclientclick="$j('#addNewReview').show();return false;"
					                Text="Write a Review" clientidmode="static" ToolTip="Write a Review" visible="true"></asp:LinkButton>
                            </div>

			            </div>
                            
                    </span>
				</contenttemplate>
			</asp:updatepanel>

            <!-- BUY COLUMN -->
		    <asp:updatepanel id="upThirdColumn" runat="server">
			    <contenttemplate>

				    <asp:Panel ID="pnlBuy" runat="server" Visible="false" clientidmode="static">
					    <div id="divBuy" clientidmode="static" class="purchasenew" runat="server">
						    <div class="boxTop"></div>
						    <div class="boxMiddle">
							    <h3>Purchase Summary</h3><div class="chkbox"><label><asp:CheckBox runat="server" ID="chkBlastPurchase" />Blast This Purchase</label></div> 
							    <hr />
							    <div class="divBorder margintop">
								    <div class="boxTopWhite"></div>
								    <div class="boxMiddleWhite desc">
									    <table callpadding="0" cellspacing="0">
										    <tr>
											    <td class="bold">Item</td>
											    <td class="tdWidth price">Price</td>
											    <td class="bold tdWidth hide">Qty</td>
										    </tr>
										    <tr>
											    <td><asp:Label ID="lblItemNamePurchase" runat="server"></asp:Label></td>
											    <td class="tdWidth"><asp:Label ID="lblPriceCheckout" runat="server" class="price"></asp:Label></td>
											    <td class="tdWidth hide">
												    <asp:TextBox ID="txtQuantity" MaxLength="3" OnTextChanged="txtQuantity_TextChanged"
													    AutoPostBack="true" runat="server" Width="20px"></asp:TextBox>
											    </td>
										    </tr>
									    </table>
								    </div>
								    <div class="boxBottomWhite"></div>
							    </div>
							    <div class="divBorder margintop">
								    <div class="boxTopWhite"></div>
								    <div class="boxMiddleWhite">
									    <table class="balances" id="tblBalances">
										    <tr>
											    <td class="bold rule label"><span id="spnCreditBalance" runat="server" clientidmode="static">Credit Balance: <asp:Label ID="lblCreditsCheckout" runat="server" Text="0"></asp:Label><div class="purchase_msg alert" id="divCreditsMsg" runat="server"></div></span></td>
											    <td class="rule" style="text-align:right;"><asp:LinkButton ID="lbComplete" runat="Server" OnClientClick="ProcessOrder();_gaq.push(['_trackPageview','/clickPurchaseItemCredits.aspx']);"
											    OnClick="lbCompleteCredits_OnClick" CssClass="btnxlarge orange" style="border:none;float:right;font-size:15px;" Text="Buy with Credits"></asp:LinkButton>
											    <asp:LinkButton ID="lbGetCredits" visible="false" runat="Server" CssClass="btnxlarge grey" style="float:right;height:27px;" Text="Get Credits"></asp:LinkButton>
                                                    <span id="spnCreditsMsg" runat="server"></span>
											    </td>
										    </tr>
										    <tr class="bold">
											    <td class="label"><span id="spnRewardBalance" runat="server">Reward Balance: <asp:Label ID="lblRewards" runat="server" Text="0"></asp:Label><div class="purchase_msg alert" id="divRewardsMsg" runat="server"></div></span></td>
											    <td class="tdWidth"><asp:LinkButton ID="lbCompleteRewards" runat="Server" OnClientClick="ProcessOrder();_gaq.push(['_trackPageview','/clickPurchaseItemRewards.aspx']);"
											        OnClick="lbCompleteRewards_OnClick" CssClass="btnxlarge orange" style="border:none;float:right;font-size:15px;" Text="Buy with Rewards"></asp:LinkButton>
                                                    <a href="http://docs.kaneva.com/mediawiki/index.php/Fame_and_Rewards\" target="_blank" id="lnkEarnRewards" runat="server" class="btnlarge grey" style="font-size:12px;float:right;padding-top:5px;padding-bottom:0;height:21px;" visible="false">How to Earn Rewards</a>
                                                    <span id="spnRewardsMsg" runat="server"></span>
											    </td>
										    </tr>
									    </table>
								
									    <div id="divProcessing"><div>Processing...</div></div>
									    <div id="divComplete" style="display:none;">
									
									    </div>
								    </div>
								    <div class="boxBottomWhite"></div>
							    </div>
							    <!-- New HTML: ad the id to the atag and removed the class button1 -->
							    <div class="aleignButton" id="divCancel">
							    </div>
						    </div>
						    <div class="boxBottom"></div>
					    </div>
				    </asp:Panel>

				    <asp:Panel ID="pnlZoneSelect" runat="server" Visible="false">	
					    <div id="divZoneSelect" class="purchasenew" runat="server">
						    <div class="boxTop"></div>
						    <div class="boxMiddle zonesmiddle">
							    <h3>Apply Pre Built World To</h3>
							    <hr />	 
							    <div class="divBorder zoneselect">
								    <a href="javascript:void(0);" onclick="showzone(this.id);" id="aNew3DWorld" name="zonesection" class="selected" runat="server" clientidmode="static">
								    <input checked="true" type="radio" class="nopad" runat="server" id="radNew3DApp" name="rdoCommType" value="new3dapp" clientidmode="static" />
								    <span class="radiolabel">New World</span></a>
								    <div id="radNew3DAppMsg" style="display:block;" class="blank" name="zonemsg" runat="server" clientidmode="static">
									    <div class="indent">
										    <asp:TextBox id="txt3DAppName" runat="server" width="200px" cssclass="inputText" onmouseup="$j(this).addClass('clicked');" maxlength="50" text=""></asp:TextBox>
										    <div class="inlineerr" id="divNewWorldTxtBoxErr" runat="server"></div>
									    </div>
									    <div class="selection">
										    <hr class="dotted" style="display:none;" />
										    <div class="indent" style="display:none;">
											    <p>Select Template:</p>
											    <asp:radiobuttonlist id="rblTemplates" runat="server">
												    <asp:listitem value="3"> <img id="Img5" src="images/purchase/graphic_52x52_communityTemplate.png" width="52" height="52" /> Community Template</asp:listitem>
											    </asp:radiobuttonlist>
										    </div>
										    <div style="display:none;">
											    <span id="rbCommTemplateMsg"></span>
											    <span id="rbAdvTemplateMsg"><p class="bold">Note:</p><p>Find this World in your <span class="bold">Import World Menu</span>.</p></span>
											    <span id="rbTreasureTemplateMsg"><p class="bold">Note:</p><p>Find this World in your <span class="bold">Import World Menu</span>.</p></span>
										    </div>
										    <div id="templatemsg" class="templatemsg" style="display:none;"></div>
									    </div>
								    </div>
 
								    <a href="javascript:void(0);" onclick="showzone(this.id);" id="aExistingWorld" name="zonesection" runat="server" clientidmode="static">
								    <input type="radio" runat="server" id="radExisting" name="rdoCommType" value="homeComm" clientidmode="static" />
								    <span class="radiolabel">Existing World</span></a>
								    <div id="radExistingMsg" style="display:none;" name="zonemsg" class="blank" runat="server" clientidmode="static">
									    <div class="indent">
										    <asp:dropdownlist id="ddlWorlds" clientidmode="Static" runat="server" width="200px" cssclass="worldlist" onchange="showMsg();" ></asp:dropdownlist>
										    <div class="inlineerr existing" id="divExistingWorldError" runat="server"></div>
									    </div>	
									    <div class="selection">
										    <div id="existingMsgbox" class="homemsg" style="display:none;">
											    <p class="bold">Note:</p> Find this World in your <span class="bold">Import World Menu</span>.</p>
										    </div>
									    </div>
								    </div>

								    <a href="javascript:void(0);" onclick="showzone(this.id);" id="aImportZone" name="zonesection" runat="server" clientidmode="static">
								    <input type="radio" runat="server" id="radImport" name="rdoCommType" value="importList" clientidmode="static" />
								    <span class="radiolabel">Import World List</span></a>
								    <div id="radImportMsg" style="display:none;" name="zonemsg" class="blank" runat="server" clientidmode="static">
									    <div class="selection">
										    <div class="homemsg">
											    <p class="bold ">Note:</p> 
											    Find this World in your <span class="bold">Import World Menu.</span>
										    </div>
									    </div>
								    </div>
 
								    <span style="display:none;">
								    <input type="radio" runat="server" onclick="javascript:$('divNewComm').style.display='inline';$('divHomeWarning').style.display='none';$('divCommWarning').style.display='none';$('divNew3Dapp').style.display='none';" id="radNew" name="rdoCommType" value="newComm" />
								    <div id="divNewComm" style="display:none;">
									    <asp:TextBox id="txtName" runat="server" onfocus="if(this.value=='Enter Name Here')this.value='';this.className='inputText';"></asp:TextBox>
								    </div>
								    </span> 
							    </div>
							    <div class="buttonalign">
								    <asp:LinkButton ID="lnkZoneNext" runat="Server" onclick="btnChooseCommNext_OnClick" CssClass="btnmedium orange" style="border:none;float:right;" Text=" Next "></asp:LinkButton>
								    <asp:LinkButton ID="lbCompleteCommunityGetForFree" runat="Server" onclick="lbCompleteCommunityGetForFree_OnClick" CssClass="adminfree" Text="Admin Get For Free >>" style="display:none;border:"></asp:LinkButton>
							    </div>		   
						    </div>
						    <div class="boxBottom"></div>
						    <p class="zonecopy">A copy of this World can be found in your <span class="bold">Import World Menu.</span></p>
					    </div>
				    </asp:Panel>

				    <asp:Panel ID="pnlComplete" runat="server" Visible="False" clientidmode="static">
					    <div id="divComplete" class="purchasenew">
						    <div class="boxTop"></div>
						    <div class="boxMiddle">
							    <h3>Purchase Summary</h3> 
							    <hr />
							    <div class="divBorder margintop">
								    <div class="boxTopWhite"></div>
								    <div class="boxMiddleWhite desc">
									    <p>Thank You!</p>
									    <asp:Literal ID="litSuccessMessage" runat="server" />
									    <img src="images/purchase/findininventory.jpg" />
									    <div class="btncontainer">
										    <div class="holder">
										    <a id="lbContinueShopping" clientidmode="static" CssClass="" href="~/" runat="server" OnClientClick="_gaq.push(['_trackPageview','/clickPurchaseItemDone.aspx'])">Continue Shopping</a>
										    <div>OR</div>
										    <a href="javascript:void(0);" runat="server" id="lbPlayNow_Complete" onclick="pn_js();" class="playnow"></a>
										    </div>
									    </div>
								    </div>
								    <div class="boxBottomWhite"></div>
								    <div class="aleignButton" id="div1"></div>
							    </div>
						    </div>
						    <div class="boxBottom"></div>
					
					    </div>
				    </asp:Panel>

				    <asp:Panel ID="pnlCompleteZonePurchase" runat="server" Visible="False" clientidmode="static">
					    <div id="divCompleteZone" clientidmode="static" class="purchasenew" runat="server">
						    <div class="boxTop"></div>
						    <div class="boxMiddle">
							    <h3>Thank You</h3>
							    <hr />
							    <div class="divBorder margintop">
								    <div class="boxTopWhite"></div>
								    <div class="boxMiddleWhite">
									    <p>Your purchase is complete.</p>
									    <div class="btncontainer">
										    <a href="~/" runat="server" >Continue Shopping</a>
										    <a href="javascript:void(0);" runat="server" id="aPlayNow" onclick="pn_js();" class="playnow"></a>
									    </div>
								    </div>
								    <div class="boxBottomWhite"></div>
								    <div class="aleignButton"></div>
							    </div>	
						    </div>
						    <div class="boxBottom"></div>
					    </div>
				    </asp:Panel>

			    </contenttemplate>
		    </asp:updatepanel>

		</div>
	</div>






		
 
	<style>
        .img-center {display: table-cell; vertical-align: middle;background-position: center center;}

        .ui-tabs.ui-widget { }   
        .ui-tabs.ui-widget.ui-widget-content {padding-left: 0;padding-right: 0;}
        .ui-tabs .ui-tabs-panel { padding: 0;}
        .ui-corner-all{ border-radius: 0;}
        .ui-tabs-nav.ui-helper-reset.ui-helper-clearfix.ui-widget-header { border: none;border-bottom: 1px solid #dcdcdc;padding-left: 10px;padding-bottom: 0;background-color: #fff;}
        .tab-data-container {overflow: auto; height: 254px; max-height:245px; min-height:100px;}

	    .ui-state-active, .ui-widget-content .ui-state-active, 
        .ui-widget-header .ui-state-active, a.ui-button:active, 
        .ui-button:active, .ui-button.ui-state-active:hover {
	        border: 1px solid #dcdcdc;
	        background:#fff;
	    }
        .ui-widget-content a
	    {
            color: #333399;
	    }
        .ui-state-active a,
        .ui-state-active a:link,
        .ui-state-active a:visited {
            color:#454545;
        }
		.ui-dialog {background-color:transparent;padding:0;background-image:url(images/purchase/graphic_367x185_popUpWindow.png);z-index:2;}
		.ui-widget {border:none;}
		.ui-dialog-titlebar	{color:#fff;background-color:transparent;background-image:none;border:none;}
		.ui-dialog-titlebar .ui-dialog-titlebar-close {background-image:url(images/buttons/closeoutButton.gif);right:0.6em;top:16px;}
		.ui-dialog .ui-dialog-buttonpane {background-color: transparent; text-align: center; padding: 0 0 26px 0; border: none;}
		.ui-dialog .ui-dialog-buttonpane .ui-dialog-buttonset {float:none;}
		.ui-dialog-buttonset {display:block;width:298px;margin:auto;padding:0;margin-top:0; text-align:center;}
		.ui-dialog-buttonset button.dialogButton {background-image:url(images/btn_purchase.jpg);border:none;font-size:14px;line-height:2em;color:#fff;text-decoration:none;background-color:transparent;display:block;text-align:center;height:30px;width:144px;margin-right:auto;margin-left:auto;margin-top:0;}
		.ui-dialog-buttonset button.okButton {float:left;margin-right:10px;}
		.ui-dialog-buttonset button.okButtonCenter {float:none;margin-right:auto;}
		.ui-dialog .ui-dialog-content {padding-top:.2em;padding-left:2em;padding-right:2em; height: 65px !important; margin-top:20px;overflow:visible;}
		.ui-dialog-content p {padding:0; margin:0;}
		.ui-dialog-content p.title {font-weight:bold;margin:0;}
		.ui-dialog-content p.desc {font-size:14px;margin-top:6px; text-align:center;}
		.ui-widget-overlay {Alpha(Opacity=50);opacity:0.5;background-color:rgb(0,0,0);background-image:none;height:100%;width:100%;}
		.ui-dialog img:focus, .ui-dialog a:focus {border:none;}
	
		a.at-icon-wrapper.at-share-btn.at-svc-twitter,
		a.at-icon-wrapper.at-share-btn.at-svc-facebook,
		a.at-icon-wrapper.at-share-btn.at-svc-compact { font-weight: normal;}
	</style>

							


	

</asp:Content>
