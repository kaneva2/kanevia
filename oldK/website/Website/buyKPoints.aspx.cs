///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for buyKPoints.
	/// </summary>
	public class buyKPoints : MainTemplatePage
	{
		protected buyKPoints () 
		{
			Title = "Buy K-Points";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			Response.Redirect ("~/mykaneva/managecredits.aspx");

			if (!IsPostBack)
			{
				drpKPoints.DataTextField = "display";
                drpKPoints.DataValueField = "promotion_id";
                drpKPoints.DataSource = StoreUtility.GetActivePromotions(Constants.CURR_KPOINT, 0); ;
				drpKPoints.DataBind ();
			}

			AddBreadCrumb (new BreadCrumb ("Buy K-Points", GetCurrentURL(), "", 0));
		}

		
		#region Helper Methods
		/// <summary>
		/// Redirect to download screen
		/// </summary>
		/// <param name="assetId"></param>
		private void RedirectToPaymentSelection (int orderId)
		{																		  
			Response.Redirect (ResolveUrl ("~/checkout/paymentSelection.aspx?orderId=" + orderId.ToString()));
		}

		#endregion

		#region Event Handlers
		/// <summary>
		/// They want to purchase a point bucket
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnPurchase_Click (object sender, EventArgs e) 
		{
			if (!Request.IsAuthenticated)
			{
				ShowErrorOnStartup ("Please sign in to purchase K-Points");		   //TODO*****
				return;
			}

			int userId = GetUserId ();

			int orderId = StoreUtility.CreateOrder (userId, Common.GetVisitorIPAddress(), (int) Constants.eORDER_STATUS.CHECKOUT);

			// Set the purchase type
			StoreUtility.SetPurchaseType (orderId, Constants.ePURCHASE_TYPE.KPOINT_ONLY);

            DataRow drPointBucket = StoreUtility.GetPromotion(Convert.ToInt32(drpKPoints.SelectedValue));

			// Point Bucket item name
            string itemName = Server.HtmlEncode(drPointBucket["promotion_description"].ToString());

			// Record the transaction in the database, marked as checkout
			int transactionId = StoreUtility.PurchasePoints (userId, (int) Constants.eTRANSACTION_STATUS.CHECKOUT, itemName, 0, (int) Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE, 
				Convert.ToDouble (drPointBucket ["dollar_amount"]), Convert.ToDouble (drPointBucket ["kei_point_amount"]), Convert.ToDouble (drPointBucket ["free_points_awarded_amount"]), 0, Common.GetVisitorIPAddress());

			// Record the point bucket purchased
            StoreUtility.UpdatePointTransactionPointBucket(transactionId, Convert.ToInt32(drPointBucket["promotion_id"]));

			// Set the order id on the point purchase transaction
			StoreUtility.UpdatePointTransaction (transactionId, orderId);
			StoreUtility.UpdateOrderPointTranasactionId (orderId, transactionId);

			// Send them to select a payment method
			RedirectToPaymentSelection (orderId);
		}

		#endregion

		#region Declerations

		protected DropDownList drpKPoints;
		protected PlaceHolder phBreadCrumb;
		protected HtmlAnchor aPB1, aPB2, aPB3, aPB4, aPB5;

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
