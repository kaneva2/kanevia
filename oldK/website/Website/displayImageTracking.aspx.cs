///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
    public class displayImageTracking : System.Web.UI.Page
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
          Response.ContentType = "image/gif";
          Response.AppendHeader("Content-Length", _imgbytes.Length.ToString());
            
          Response.BinaryWrite(_imgbytes);

          recordView ();           
        }

        private bool useCached(HttpRequest req)
        {
            string ifmod = req.Headers["If-Modified-Since"];
            return ifmod == null ? false : DateTime.Parse(ifmod).AddHours(24) >= DateTime.Now;
        }

        private void recordView()
        {
          int user_id = 0;
          try
          {
            user_id = Convert.ToInt32(Request.QueryString["u"]);
          }
          catch
          {
          }

          int mailing_id = 0;
          try
          {
            mailing_id = Convert.ToInt32(Request.QueryString["m"]);
          }
          catch
          {
          }

          int invite_id = 0;
          try
          {
            invite_id = Convert.ToInt32(Request.QueryString["invite"]);
          }
          catch
          {
          }

          string ipaddress = Request.ServerVariables["REMOTE_ADDR"];
          string useragent = Request.ServerVariables["HTTP_USER_AGENT"];

          try
          {
            if(invite_id > 0)
              recordViewInvite(Convert.ToInt32(Request.QueryString["invite"]));

            Mailer.Logger.LogEmailOpen(mailing_id);
            MailUtility.recordView(user_id, mailing_id, ipaddress, useragent);
          }
          catch(Exception exc)
          {
            m_logger.Error("Error tracking.", exc);
          }
        }

        private void recordViewInvite (int inviteId)
        {
            MailUtility.RecordViewInvite (inviteId);
        }

        private static byte[] _imgbytes =
                Convert.FromBase64String("R0lGODlhAQABAIAAANvf7wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==");

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}
