<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="RegTest.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.RegTest" %>
<%@ Register TagPrefix="Kaneva" TagName="Registration" Src="~/usercontrols/register/Registration.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Reg Test Page</title>
    <script type="text/javascript" src="http://cmullins-lptp1/Kaneva/jscript/prototype-1.6.0.3-min.js"></script>
	<link href="http://cmullins-lptp1/Kaneva/css/home/home.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <form id="form1" runat="server">
    <div>		                                                                  
		<Kaneva:Registration id="rp" runat="server"></Kaneva:Registration>
    </div>
    </form>
</body>
</html>
