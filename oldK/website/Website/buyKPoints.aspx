<%@ Page language="c#" Codebehind="buyKPoints.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.buyKPoints" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="usercontrols/HeaderText.ascx" %>
<Kaneva:HeaderText runat="server" Text="Purchase K-Points"/>
<asp:PlaceHolder id="phBreadCrumb" runat="server"/>
<center>
					<TABLE cellSpacing="0" cellPadding="0 width="750" border="0">
						<tr>
						<td valign="top">
							<table border="0">
							<tr>							
							<td class="bodyText" width="300" valign="top"><br><br>
							<span class="orangeHeader3">What are K-Points?</span>&nbsp;<a href="http://docs.kaneva.com/bin/view/public/KanevaFaqKpoints" target="_resource" class="LnavText">K-Point FAQ</a><br>
							K-Points are like online arcade 
tokens or credits to use to buy any of Kaneva�s digital assets. Use K-Points to 
pay-as-you-go and watch more movies or play more online games. K-Points provide 
you with the best value. When you buy larger amounts of K-Points, you are 
rewarded with more FREE K-Points!<br><br>
							K-Points give you maximum 
flexibility and put you in control of how and when you want to use them. 
K-Points can be easily purchased directly from our site through our 
secure payment processing system. Take advantage of anytime access to your 
personal �My Kaneva� page to check your K-Points balance, buy more K-Points and 
manage your account.<br><br>
							As you experience all that Kaneva has to offer, you will discover lots of ways to use 
your K-Points. You can choose to download and watch a wide range of movies and 
keep them to add to your film collection. You can also play any number of online 
games featured on Kaneva or purchase a digital art asset to add to a game you 
create. Check out our Publish and Create sections to learn more. Buy your 
K-Points today and get started playing on Kaneva!										
							</td>
							<td width="350" align="left" valign="top">
								<table width="350" cellpadding="0" cellspacing="0" border="0">
								<tr><td>
								<img runat="server" src="~/images/spacer.gif" width="50" height="20" border="0"/>
								</td></tr>
								<tr>
								<td><br>
								<img runat="server" src="~/images/KPP_01.gif" border="0" alt="Purchase this package"/><br/>
								<img runat="server" src="~/images/KPP_02.gif" border="0" alt="Purchase this package"/><br/>
								<img runat="server" src="~/images/KPP_03.gif" border="0" alt="Purchase this package"/><br/>
								<img runat="server" src="~/images/KPP_04.gif" border="0" alt="Purchase this package"/><br/>
								<img runat="server" src="~/images/KPP_05.gif" border="0" alt="Purchase this package"/><br/>						
								</td>
								</tr>					
								</table>
							
							</td>
							</tr>
							</table>
							<br>
							
							<table cellpadding="0" cellspacing="0" border="0" width="600">
								<tr>
								<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1"/></td><td width="580" align="center" class="headerText"><b id="rounded2">
 <i><span><img border="0" runat="server" src="~/images/spacer.gif" width="580" height="8" ID="Img1"/></span></i> 
</b></td><td width="10" bgcolor="white"><img runat="server" src="~/images/spacer.gif" width="10" height="1"/></td>
								</tr>
							</table>
							<table cellpadding="0" cellspacing="0" border="0" width="600">
							<tr>
							<td width="10"><img src="images/spacer.gif" width="10" height="20"/></td>
							<td>							
							<table cellpadding="0" cellspacing="1" border="0" width="580" bgcolor="#ededed" style="border: 1px solid #ededed;">
								<tr>
								<td colspan="3"><br></td>
								</tr>
								<tr>							
									<td align="right" valign="middle" class="subHead4" style="line-height: 24px;" width="40%">
									<b>Buy K-Points: &nbsp;</b>
									</td>
									<td valign="middle" width="26%" align="center">
										<asp:DropDownList class="filter2" id="drpKPoints" runat="Server"/> 
									</td>
									<td valign="middle" width="33%" align="left">&nbsp;<asp:button id="btnPurchase" runat="Server" onClick="btnPurchase_Click" class="Filter2" Text=" purchase "/>&nbsp;&nbsp;</td>						
								</tr>						
								<tr>
								<td colspan="3"><br></td>
								</tr>
							</table>
							</td>
							</tr>
							</table>
							

						</td></tr>
						</table><br><br><br>
</center>