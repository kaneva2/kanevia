<%@ Page language="c#" Codebehind="suggestions.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.suggestions" %>

<link href="css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="css/friends.css" rel="stylesheet" type="text/css"/>
<link href="css/kanevaBroadBand.css" rel="stylesheet" type="text/css" />		
<link href="css/kanevaText.css" rel="stylesheet" type="text/css">

<center>

	
	<table runat="server" id="tblIris" Visible="False" cellpadding="0" cellspacing="0" border="0" width="750">
		<tr>
			<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
			<td colspan="3" align="left" class="dateStamp" bgcolor="#dddddd" style="border-left: 1px solid #cccccc; border-right: 1px solid #cccccc;">
				&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" id="chkIReceive" class="Filter2"/> Receiving files problem.
			</td>
			<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>
		<tr>
			<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
			<td colspan="3" align="left" class="dateStamp" style="border-left: 1px solid #cccccc; border-right: 1px solid #cccccc;">
				&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" id="chkIPublish" class="Filter2"/> Publishing problem.
			</td>
			<td><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>
		<tr>
			<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
			<td colspan="3" align="left" class="dateStamp" bgcolor="#dddddd" style="border-left: 1px solid #cccccc; border-right: 1px solid #cccccc;">
				&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" id="chkIViewing" class="Filter2"/> Viewing Content problem.
			</td>
			<td><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>
		<tr>
			<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
			<td colspan="3" align="left" class="dateStamp" style="border-left: 1px solid #cccccc; border-right: 1px solid #cccccc;">
				&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" id="chkIGames" class="Filter2"/> Playing Games problem.
			</td>
			<td><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>
		<tr>
			<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
			<td colspan="3" align="left" class="dateStamp" bgcolor="#dddddd" style="border-left: 1px solid #cccccc; border-right: 1px solid #cccccc;">
				&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" id="chkIGUI" class="Filter2"/> GUI problem.
			</td>
			<td><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>
		<tr>
			<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
			<td colspan="3" align="left" class="dateStamp" style="border-left: 1px solid #cccccc; border-right: 1px solid #cccccc;">
				&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" id="chkIOther" class="Filter2"/> Other problem.
			</td>
			<td><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>
	</table>
	
	<table runat="server" id="tblGame" Visible="False" cellpadding="0" cellspacing="0" border="0" width="750">
		<tr>
			<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
			<td colspan="3" align="left" class="dateStamp" bgcolor="#dddddd" style="border-left: 1px solid #cccccc; border-right: 1px solid #cccccc;">
				&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" id="chkGGraphics" class="Filter2"/> Graphics Bug.
			</td>
			<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>
	</table>


<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
					<td valign="top" class="newdatacontainer">
						<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="boxInside">
							<tr>
								<td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
								<td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
								<td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
							</tr>
							<tr class="boxInside">
								<td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
								<td class="boxInsideContent">
									<table width="100%"  border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
											<td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
											<td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
										</tr>
										<tr>
											<td><img runat="server" src="~/images/spacer.gif" width="8" height="6"/></td>
											<td width="100%" class="textGeneralGray01" style="font-size:12px;font-weight:bold" align="left">
											<img runat="server" src="~/images/spacer.gif" width="2" height="1"/><asp:label id="lblTitle" runat="server" text="Feedback, Suggestions & Support" /></td>
											<td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
										</tr>
										<tr>
											<td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
											<td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
											<td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
										</tr>
									</table>
								</td>
								<td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
							</tr>
							<tr>
								<td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
								<td class="boxInsideBottom"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
								<td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
							</tr>
						</table>
						<table  border="0" cellpadding="0" cellspacing="0" width="100%" >
							<tr>
								<td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
								<td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
								<td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
							</tr>
							<tr class="boxInside">
								<td rowspan="2" class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
								<td class="boxInsideContent">
									<table width="100%"  border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
											<td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
											<td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
										</tr>
										<tr>
											<td><img runat="server" src="~/images/spacer.gif" width="8" height="6"/></td>
														

											<!-- Feedback Desc -->
											<td id="tdFeedbackDesc" runat="server" width="100%" visible="False" class="textGeneralGray01" align="left">
												Want to help us improve Kaneva?  Please use the form below to provide us with feedback and suggestions or to report a bug, technical issue or policy violation.
												<br><br>

												<ul><li><b>Before you ask a question, please check our 
												<a runat="server" href="http://www.kaneva.com/community/CommunityPage.aspx?communityId=1118&pageId=14277" id="A2">FAQ</a>.</b></li>	
												<li>Want to talk to Kaneva about a business matter?   Please go to our 
												<a runat="server" href="~/overview/contactUs.aspx" id="A3">Contact Us page</a>.	</li>	
												<li>Want to talk to Kaneva about a copyright or ownership issue?   Please go to our 
												<a runat="server" href="~/overview/copyright.aspx" id="A4">Copyright page</a>.	</li></ul>	
												</td>
														 
											<!-- Business Desc -->
											<td id="tdBusinessDesc" runat="server" width="100%" visible="False" class="textGeneralGray01" align="left">Business Matters<br />
												<br />Thank you for your interest in Kaneva. We're as excited as you are about the business opportunities and 
												correspondence you'd like to initiate with us. Please include as much information as possible, but please give 
												us a few days to review and respond to your requests.<br><br>
												If you have a legal issue, please go <a runat="server" href="~/overview/copyright.aspx" id="A1">here</a>.
											</td>
														
											<!-- Web Site Desc -->
											<td id="tdWebSiteDesc" runat="server" width="100%" visible="False" class="textGeneralGray01" align="left">Report a Problem or Bug <br />
												<br />Your comments can help make our site and products better for everyone. If you've found something incorrect, 
												broken, or hard to use or understand on a Kaneva web page or Kaneva product, let us know so that we can improve it. 
												Optionally include your email address if you'd like a response, but please give us a few days to review and respond 
												to your requests.
											</td>
														
														
											<td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
										</tr>
										<tr>
											<td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
											<td><img runat="server" src="~/images/spacer.gif" width="6" height="8"/></td>
											<td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
										</tr>
									</table>
								</td>
								<td rowspan="2" class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
							</tr>
							<tr class="boxInsideContent">
								<td align="center">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
											<td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
											<td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
										</tr>
										<tr>
											<td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
											<td width="100%">
															



												<!-- BEGIN: Feedback -->
												<table  runat="server" id="tblFeedback" visible="False" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="boxInsideContent">
													<tr>
														<td width="7" class="intBorderCTopLeft"><img runat="server" src="~/images/spacer.gif" width="6" height="6" id="Img5"/></td>
														<td colspan="3" class="intBorderCTop"><img runat="server" src="~/images/spacer.gif" width="300" height="7" id="Img13"/><img runat="server" src="~/images/spacer.gif" width="300" height="7" id="Img41"/></td>
														<td width="6" class="intBorderCTopRight"><img runat="server" src="~/images/spacer.gif" width="6" height="6" id="Img42"/></td>
													</tr>
													<tr>
														<td class="intBorderCBorderLeft"><img runat="server" src="~/images/spacer.gif" width="6" height="2" id="Img43"/></td>
															<td class="intBorderCBgInt1">&nbsp;</td>
														<td class="intBorderCBgInt1" colspan="2"><span class="textGeneralGray11">Please mark as many of the following boxes that apply:</span></td>
														<td class="intBorderCBorderRight">&nbsp;</td>
													</tr>
													<tr>
														<td class="intBorderCBottomLeft"><img runat="server" src="~/images/spacer.gif" width="7" height="7" id="Img44"/></td>
														<td colspan="3" class="intBorderCBottom"><img runat="server" src="~/images/spacer.gif" width="7" height="7" id="Img45"/><img runat="server" src="~/images/spacer.gif" width="7" height="7" id="Img46"/></td>
														<td class="intBorderCBottomRight"><img runat="server" src="~/images/spacer.gif" width="7" height="7" id="Img47"/></td>
													</tr>
													<tr class="intColor1">
														<td class="intBorderC1BorderLeft1">&nbsp;</td>
														<td class="intColor1" height="25"></td>
														<td class="intColor1" align="center"><asp:checkbox runat="server" id="chkFCopyrightContent"/></td>
														<td class="intColor1">Virtual World of Kaneva download or install issue.</td>
														<td class="intBorderC1BorderRight1">&nbsp;</td>
													</tr>
													<tr class="intColor2">
														<td class="intBorderC1BorderLeft1">&nbsp;</td>
														<td class="intColor2" height="25"></td>
														<td class="intColor2" align="center"><asp:checkbox runat="server" id="chkFWokTech"/></td>
														<td class="intColor2">Virtual World of Kaneva technical issue.</td>
														<td class="intBorderC1BorderRight1">&nbsp;</td>
													</tr>
													<tr class="intColor1">
														<td class="intBorderC1BorderLeft1">&nbsp;</td>
														<td class="intColor1" height="25"></td>
														<td class="intColor1" align="center"><asp:checkbox runat="server" id="chkFBug"/></td>
														<td class="intColor1">Page has a bug or broken link.</td>
														<td class="intBorderC1BorderRight1">&nbsp;</td>
													</tr>
													<tr class="intColor2">
														<td class="intBorderC1BorderLeft1">&nbsp;</td>
														<td class="intColor2" height="25"></td>
														<td class="intColor2" align="center"><asp:checkbox runat="server" id="chkFPage"/></td>
														<td class="intColor2">Page contains typographical error.</td>
														<td class="intBorderC1BorderRight1">&nbsp;</td>
													</tr>
													<tr class="intColor1">
														<td class="intBorderC1BorderLeft1">&nbsp;</td>
														<td class="intColor1" height="25"></td>
														<td class="intColor1" align="center"><asp:checkbox runat="server" id="chkFMature"/></td>
														<td class="intColor1">Inappropriate and/or pornographic material.</td>
														<td class="intBorderC1BorderRight1">&nbsp;</td>
													</tr>
													<tr >
														<td class="intBorderC1BottomLeft"><img runat="server" src="~/images/spacer.gif" width="7" height="7" id="Img49"/></td>
														<td colspan="3" class="intBorderC1Bottom"><img runat="server" src="~/images/spacer.gif" width="7" height="7" id="Img50"/><img runat="server" src="~/images/spacer.gif" width="7" height="7" id="Img51"/></td>
														<td class="intBorderC1BottomRight"><img runat="server" src="~/images/spacer.gif" width="7" height="7" id="Img52"/></td>
													</tr>
												</table>
												<!-- END: Feedback -->


	
   												<!-- BEGIN: Business -->
												<table runat="server" id="tblBusiness" visible="False" width="100%" border="0" align="left" cellpadding="0" cellspacing="0" class="boxInsideContent">
													<tr>
														<td width="7" class="intBorderCTopLeft"><img runat="server" src="~/images/spacer.gif" width="6" height="6" id="Img1"/></td>
														<td colspan="3" class="intBorderCTop"><img runat="server" src="~/images/spacer.gif" width="300" height="7" id="Img2"/><img runat="server" src="~/images/spacer.gif" width="300" height="7" id="Img3"/></td>
														<td width="6" class="intBorderCTopRight"><img runat="server" src="~/images/spacer.gif" width="6" height="6" id="Img4"/></td>
													</tr>
													<tr>
														<td class="intBorderCBorderLeft"><img runat="server" src="~/images/spacer.gif" width="6" height="2" id="Img6"/></td>
															<td class="intBorderCBgInt1">&nbsp;</td>
														<td class="intBorderCBgInt1" colspan="2"><span class="textGeneralGray11">Please mark as many of the following boxes that apply:</span></td>
														<td class="intBorderCBorderRight">&nbsp;</td>
													</tr>
													<tr>
														<td class="intBorderCBottomLeft"><img runat="server" src="~/images/spacer.gif" width="7" height="7" id="Img7"/></td>
														<td colspan="3" class="intBorderCBottom"><img runat="server" src="~/images/spacer.gif" width="7" height="7" id="Img8"/><img runat="server" src="~/images/spacer.gif" width="7" height="7" id="Img9"/></td>
														<td class="intBorderCBottomRight"><img runat="server" src="~/images/spacer.gif" width="7" height="7" id="Img10"/></td>
													</tr>
													<tr class="intColor1">
														<td class="intBorderC1BorderLeft1">&nbsp;</td>
														<td valign="middle" class="intColor1" height="25"><img runat="server" src="~/images/spacer.gif" width="6" height="1" id="Img11"/></td>
														<td valign="middle" class="intColor1" align="center"><asp:checkbox runat="server" id="chkStudioPartner"/></td>
														<td valign="middle" class="intColor1">I'd like info on becoming a Studio Partner.</td>
														<td class="intBorderC1BorderRight1">&nbsp;</td>
													</tr>
													<tr class="intColor2">
														<td class="intBorderC1BorderLeft1">&nbsp;</td>
														<td class="intColor2" height="25"></td>
														<td class="intColor2" align="center"><asp:checkbox runat="server" id="chkDistributionPartner"/></td>
														<td class="intColor2">I'd like info on becoming a Distribution Partner.</td>
														<td class="intBorderC1BorderRight1">&nbsp;</td>
													</tr>
													<tr class="intColor1">
														<td class="intBorderC1BorderLeft1">&nbsp;</td>
														<td class="intColor1" height="25"></td>
														<td class="intColor1" align="center"><asp:checkbox runat="server" id="chkTechPartner"/></td>
														<td class="intColor1">I'd like info on becoming a Technology Partner.</td>
														<td class="intBorderC1BorderRight1">&nbsp;</td>
													</tr>
													<tr class="intColor2">
														<td class="intBorderC1BorderLeft1">&nbsp;</td>
														<td class="intColor2" height="25"></td>
														<td class="intColor2" align="center"><asp:checkbox runat="server" id="chkEducationPartner"/></td>
														<td class="intColor2"> I'd like info on becoming an Education Partner.</td>
														<td class="intBorderC1BorderRight1">&nbsp;</td>
													</tr>
													<tr class="intColor1">
														<td class="intBorderC1BorderLeft1">&nbsp;</td>
														<td class="intColor1" height="25"></td>
														<td class="intColor1" align="center"><asp:checkbox runat="server" id="chkKGP"/></td>
														<td class="intColor1">I'd like to request a copy of the Kaneva Game Platform.</td>
														<td class="intBorderC1BorderRight1">&nbsp;</td>
													</tr>
													<tr class="intColor2">
														<td class="intBorderC1BorderLeft1">&nbsp;</td>
														<td class="intColor2" height="25"></td>
														<td class="intColor2" align="center"><asp:checkbox runat="server" id="chkPress"/></td>
														<td class="intColor2">I've got a press inquiry.</td>
														<td class="intBorderC1BorderRight1">&nbsp;</td>
													</tr>
													<tr class="intColor1">
														<td class="intBorderC1BorderLeft1">&nbsp;</td>
														<td class="intColor1" height="25"></td>
														<td class="intColor1" align="center"><asp:checkbox runat="server" id="chkAdvertising"/></td>
														<td class="intColor1">I've got an advertising inquiry.</td>
														<td class="intBorderC1BorderRight1">&nbsp;</td>
													</tr>
													<tr class="intColor2">
														<td class="intBorderC1BorderLeft1">&nbsp;</td>
														<td class="intColor2" height="25"></td>
														<td class="intColor2" align="center"><asp:checkbox runat="server" id="chkBusMisc"/></td>
														<td class="intColor2">I've got a business issue not listed above.</td>
														<td class="intBorderC1BorderRight1">&nbsp;</td>
													</tr>
													<tr >
														<td class="intBorderC1BottomLeft"><img runat="server" src="~/images/spacer.gif" width="7" height="7" id="Img12"/></td>
														<td colspan="3" class="intBorderC1Bottom"><img runat="server" src="~/images/spacer.gif" width="7" height="7" id="Img15"/><img runat="server" src="~/images/spacer.gif" width="7" height="7" id="Img16"/></td>
														<td class="intBorderC1BottomRight"><img runat="server" src="~/images/spacer.gif" width="7" height="7" id="Img17"/></td>
													</tr>
												</table>
												<!-- END: Business -->


														
												<!-- BEGIN: Web Site -->
												<table runat="server" id="tblWebSite" visible="False" width="100%" border="0" align="left" cellpadding="0" cellspacing="0" class="boxInsideContent">
													<tr>
														<td width="7" class="intBorderCTopLeft"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
														<td colspan="3" class="intBorderCTop"><img runat="server" src="~/images/spacer.gif" width="300" height="7"/><img runat="server" src="~/images/spacer.gif" width="300" height="7"/></td>
														<td width="6" class="intBorderCTopRight"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
													</tr>
													<tr>
														<td class="intBorderCBorderLeft"><img runat="server" src="~/images/spacer.gif" width="6" height="2"/></td>
															<td class="intBorderCBgInt1">&nbsp;</td>
														<td class="intBorderCBgInt1" colspan="2"><span class="textGeneralGray11">Please mark as many of the following boxes that apply:</span></td>
														<td class="intBorderCBorderRight">&nbsp;</td>
													</tr>
													<tr>
														<td class="intBorderCBottomLeft"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
														<td colspan="3" class="intBorderCBottom"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
														<td class="intBorderCBottomRight"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
													</tr>
													<tr class="intColor1">
														<td class="intBorderC1BorderLeft1">&nbsp;</td>
														<td valign="middle" class="intColor1" height="25"><img runat="server" src="~/images/spacer.gif" width="6" height="1"/></td>
														<td valign="middle" class="intColor1" align="center"><asp:checkbox runat="server" id="chkMature" /></td>
														<td valign="middle" class="intColor1" align="left">Report content as inappropriate for those under the age of 18.</td>
														<td class="intBorderC1BorderRight1">&nbsp;</td>
													</tr>







													<tr class="intColor1">
														<td class="intBorderC1BorderLeft1">&nbsp;</td>
														<td class="intColor1" height="25"></td>
														<td class="intColor1" align="center"><asp:checkbox runat="server" id="chkFInapContent" /></td>
														<td class="intColor1" align="left">Report this item as pornographic. </td>





















														<td class="intBorderC1BorderRight1">&nbsp;</td>
													</tr>
													<tr class="intColor1">
														<td class="intBorderC1BorderLeft1">&nbsp;</td>
														<td class="intColor1" height="25"></td>














														<td class="intColor1" align="center"><asp:checkbox runat="server" id="chkLanguage"/></td>
														<td class="intColor1" align="left">Report content that violates Kaneva's policy on offensive language.</td>














														<td class="intBorderC1BorderRight1">&nbsp;</td>
													</tr>
													<tr >
														<td class="intBorderC1BottomLeft"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
														<td colspan="3" class="intBorderC1Bottom"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
														<td class="intBorderC1BottomRight"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
													</tr>
												</table>
												<!-- END: Web Site -->
															
															
															
												<table width="100%"  border="0" cellspacing="0" cellpadding="0">
													<tr><td>&nbsp;</td></tr>
													<tr><td align="left" class="boxInsideContent"><img runat="server" src="~/images/spacer.gif" width="6" height="4"/>Details:</td></tr>
													<tr><td>&nbsp;</td></tr>
												</table>
												<table width="100%" border="0" cellpadding="0" cellspacing="0" >
													<tr>
														<td class="bbBoxInsideTopLeft"></td>
														<td class="bbBoxInsideTop"></td>
														<td class="bbBoxInsideTopRight"></td>
													</tr>
													<tr>
														<td class="bbBoxInsideLeft"><img runat="server" src="~/images/spacer.gif" width="5" height="1"/></td>
														<td class="bbBoxInsideContent" align="left">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
																<tr>
																	<td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
																	<td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="10"/></td>
																	<td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
																</tr>
																<tr>
																	<td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
																	<td width="100%">
																					
																		<asp:validationsummary showmessagebox="False" showsummary="True" class="formError" id="valSum" displaymode="BulletList" runat="server" headertext="Please correct the following errors:"/>

																		<table cellpadding="0" cellspacing="0" border="0" width="100%">	
																			<tr id="trEmail" runat="server">
																				<td align="right" nowrap class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="45" height="5"/></td>
																				<td width="2%" align="left" nowrap class="textGeneralGray01">&nbsp;My Email</td>
																				<td width="2%" align="left" nowrap class="textGeneralGray01">:</td>
																				<td width="1%"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
																				<td width="40%" class="textGeneralGray01" align="left"><asp:textbox id="txtEmail" class="formKanevaText"  style="width:320px" maxlength="100" runat="server"/></td>
																				<td width="48%" rowspan="6" align="left" class="textGeneralGray01" valign="top">&nbsp;</td>
																			</tr>
																			<tr><td colspan="6"><img runat="server" src="~/images/spacer.gif" width="1" height="6"/></td></tr>
																			<tr id="trSubject" runat="server" visible="true">
																				<td align="right" nowrap class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="45" height="5" id="Img14"/></td>
																				<td align="left" nowrap class="textGeneralGray01"><span class="asterisk">*</span>&nbsp;Subject</td>
																				<td align="left" nowrap class="textGeneralGray01">:</td>
																				<td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
																				<td align="left" colspan="2"><span class="textGeneralGray01">
																					<asp:textbox id="txtSummary" class="formKanevaText"  maxlength="100" runat="server" style="width:320px;vertical-align: middle;"/>
																					<asp:requiredfieldvalidator id="rftxtSummary" enabled="True" display="None" controltovalidate="txtSummary" errormessage="Subject is a required field." runat="server"/>
																				</span>
																				</td>
																			</tr>
																			<tr id="trCategorySpacer" runat="server" visible="false"><td colspan="5"><img runat="server" src="~/images/spacer.gif" width="1" height="6"/></td></tr>
																			<tr id="trCategory" runat="server" visible="false">
																				<td align="right" nowrap class="textGeneralGray01">&nbsp;</td>
																				<td align="left" nowrap class="textGeneralGray01"><span class="asterisk">*</span>&nbsp;Category</td>
																				<td align="left" nowrap class="textGeneralGray01">:</td>
																				<td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
																				<td align="left" class="textGeneralGray03">
																					<asp:dropdownlist class="formKanevaText" id="drpCategory" runat="Server" width="400px">
																						<asp:listitem  value="">Please Select...</asp:ListItem >
																						<asp:listitem  value="Feedback">General Feedback</asp:ListItem >
																						<asp:listitem  value="KANEVA Editor">Kaneva Editor</asp:ListItem >
																						<asp:listitem  value="KANEVA Game Client">Kaneva Game Client</asp:ListItem >
																						<asp:listitem  value="KANEVA Games">Kaneva Games</asp:ListItem >
																						<asp:listitem  value="KANEVA IRIS">Kaneva Media Launcher</asp:ListItem >
																						<asp:listitem  value="KANEVA Web Site">Kaneva Web Site</asp:ListItem >
																						<asp:listitem  value="KANEVA Documentation">Kaneva Documentation</asp:ListItem >
																						<asp:listitem  value="Suggestion Box">Suggestion</asp:ListItem >
																						<asp:listitem  value="Other">Other</asp:ListItem >
																					</asp:dropdownlist>
																					<asp:requiredfieldvalidator id="rfdrpCategory" enabled="True" controltovalidate="drpCategory" text="*" errormessage="Category is a required field." display="Dynamic" runat="server"/>
																				</td>
																			</tr>
																			<tr><td colspan="5" align="right" nowrap class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="6"/></td></tr>
																			<tr>
																				<td align="right" nowrap class="textGeneralGray01">&nbsp;</td>
																				<td align="left" nowrap class="textGeneralGray01" valign="middle"><span class="asterisk">*</span>&nbsp;Message</td>
																				<td align="left" nowrap class="textGeneralGray01">:</td>
																				<td>&nbsp;</td>
																				<td align="left" class="textGeneralGray03">
																					<asp:textbox id="txtComment" textmode="multiline" rows="10" class="formKanevaText"  style="width:560px" maxlength="100" runat="server"/>
																					<asp:requiredfieldvalidator id="rftxtComment" controltovalidate="txtComment" errormessage="Message is a required field." display="None" runat="server"/>
																				</td>
																				<td width="48%" align="right" valign="bottom" class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="10" height="5"/></td>
																			</tr>
																			<tr><td colspan="6" align="right" class="underline"><img runat="server" src="~/images/spacer.gif" width="5" height="10"/></td></tr>
																			<tr><td colspan="6" align="right" nowrap class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="5" height="10"/></td></tr>
																			<tr>
																				<td colspan="2" valign="top"><span class="asterisk" style="font-size:10px;padding-left:20px">* Required</span></td>
																				<td colspan="4" align="right" nowrap class="textGeneralGray01">
																					<asp:imagebutton id="btnSubmit" runat="server"  causesvalidation="False" onclick="btnSubmit_Click" imageurl="~/images/buttons/sendMessage.gif" width="107" height="26" border="0" />
																					<img runat="server" src="~/images/spacer.gif" width="10" height="5"/>
																				</td>
																			</tr>
																			<tr><td colspan="6" nowrap class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td></tr>
																		</table>
																	</td>
																	<td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
																</tr>
																<tr>
																	<td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
																	<td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="7"/></td>
																	<td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
																</tr>
															</table>
														</td>
														<td class="bbBoxInsideRight"><img runat="server" src="~/images/spacer.gif" width="5" height="1"/></td>
													</tr>
													<tr>
														<td class="bbBoxInsideBottomLeft"></td>
														<td class="bbBoxInsideBottom"></td>
														<td class="bbBoxInsideBottomRight"></td>
													</tr>
												</table>
											</td>
											<td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
										</tr>
										<tr>
											<td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
											<td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
											<td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
								<td class="boxInsideBottom2"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
								<td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td><img id="Img18" runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>




</center>