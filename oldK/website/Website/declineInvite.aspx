<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="declineInvite.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.declineInvite" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<style type="text/css">
<!--
.colR {
	background: #96B6C3;
}

.colR_top {
	background: url("images/invite/vlp_colR_top.gif") no-repeat;
	height: 12px;
	width: 312px;
}
.colR_btm {
	background: url("images/invite/vlp_colR_bottom.gif") no-repeat;
	height: 12px;
	width: 312px;
}
.innerWrapper {
	height: 432px;
	padding-left: 0px;
}
#tyContent {
	margin: 0px;
	padding: 0px;
	height:400px;	
}
#tyContent h2 {
	font-size: 13px;
	font-weight: normal;
	color: #28505F;
	margin: 0px;
	padding: 0px;
}
.noThanks_pop {
	background: url("images/invite/bg_noThanks.gif") no-repeat;
	display:block;
	height:385px;
	margin:15px 0pt 0pt;
	padding:20px 0pt 0pt;
	width:286px;
	text-align: center;
}
.noThanks_pop table {
	margin: 0px 0px 0px 22px;
	padding: 0px;
	width: 247px;
	border-width: 0px;
	border-style: none;
	border-collapse: collapse;
}
.noThanks_pop tr {
	margin: 0px;
}
.noThanks_pop th {}
.noThanks_pop td {
	padding-top: 5px;
	padding-bottom: 5px;
}
.noThx {
	text-align: center;
}
.noThx a:link, .noThx a:visited {
	background: url("images/invite/btn_noThanks.gif") no-repeat center -25px;
	text-indent: -5000px;
	display: block;
	height: 25px;
}
.noThx a:hover {
	background: url("images/invite/btn_noThanks.gif") no-repeat center 0px;
}
#btnSubmit {
	height: 25px;
	width: 128px;
	margin-right: 10px;
	border-width: 0px;
	border-style: none;
}
-->
</style>
<link href="css/new.css" rel="stylesheet" type="text/css">
<!--[if IE]> <style type="text/css">@import "css/new_IE.css";</style> <![endif]-->


<table cellpadding="0" cellspacing="0" border="0" width="990" align="center">
	<tr>
		<td colspan="3"><img runat="server" src="~/images/spacer.gif" id="Img2" width="1" height="14" /></td>
	</tr>
	<tr>
		<td colspan="3">
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" id="Img6" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" >
							<tr>
								<td width="968" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="968">
										<tr>
											<td width="645" valign="top" align="center">
												<div id="joinBox">
													<div id="joinContent">
														<h1>New to Kaneva?</h1>
														<ul class="wide floatleft">
															<li class="wide1"><img runat="server" src="~/images/join_friends.gif" width="40" height="40" border="0">
																<h3>World at Play</h3>
																<p>Meet friends, explore places, play games, music, videos and win prizes. Jump in and play.</p>
															</li>
															<li class="wide2"><img runat="server" src="~/images/join_avatar.gif" width="40" height="40" border="0">
																<h3>You & Your Friends</h3>
																<p>The place to hangout and chat with your friends together in 3D. Discover the digital you.</p>
															</li>
															<li class="wide3"><img runat="server" src="~/images/join_home.gif" width="40" height="40" border="0">
																<h3>Your 3D Home</h3>
																<p>Design and decorate your ultimate 3D home...show off your favorite videos and photos. Your free home awaits.</p>
															</li>
														</ul>
													</div>
													<div id="joinButton"><a runat="server" href="" id="aRegister" style="text-decoration:none;" class="nohover"><img runat="server" src="~/images/btn_joinnow_og.gif" alt="Join Now!" border="0"  /></a></div>
												</div>
											</td>
											<td width="11"></td>
											<td width="312" align="center" valign="top">
												
												<ajax:ajaxpanel id="ajpDecline" runat="server">
												
												<div class="colR">
                                                    <div class="colR_top"><!-- Empty --></div>
														<div class="innerWrapper">
															<div id="tyContent">
																<div class="noThanks_pop" id="divNoThanks" runat="server" style="display:none;">
																	<table id="tblDecline" runat="server">
																		<tr>
																			<td><asp:dropdownlist id="ddlDeclineReason" runat="server" style="width:230px;display:none;"></asp:dropdownlist></td>
																		</tr>
																		<tr>
																			<td><h2>May we ask why?</h2>
																				<asp:textbox id="txtDeclineInfo" runat="server" style="width:230px" maxlength="500" rows="15" textmode="MultiLine" wrap="true"></asp:textbox>
																			</td>
																		</tr>
																		<tr>
																			<td align="center"><asp:imagebutton id="btnSubmit" runat="server" tabindex="2" bordercolor="1" imageurl="~/images/invite/btn_subNoThanks.gif" alternatetext=" Submit " onclick="btnSubmit_Click" causesvalidation="true" /></td>
																		</tr>
																	</table>
																	<div id="divThankYou" runat="server" style="display:none;width:80%;height:90%;text-align:center;margin-top:50px;"><h2>Thank you for your feedback.</h2></div>
																</div>
															</div>
															<div class="noThx"><asp:linkbutton id="lbNoThanks" runat="server" onclick="lbNoThanks_Click">No Thanks</asp:linkbutton></div>
														</div>
                                                    <div class="colR_btm"><!-- Empty --></div>
												</div>
												
												
												</ajax:ajaxpanel>
												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img7" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
												

