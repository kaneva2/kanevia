///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KlausEnt.KEP.Kaneva
{
    public partial class newWorldFame : BasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            try
            {
                // For any lingering links, redirect to new world fame community
                Response.Redirect (GetBroadcastChannelUrl (GetCommunityFacade.GetCommunity (Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["WorldFameCommunityId"])).NameNoSpaces));
            }
            catch
            {
                Response.Redirect ("~/community/communitypage.aspx?communityId=1");
            }
        }
    }
}