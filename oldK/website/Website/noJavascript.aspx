<%@ Page language="c#" Codebehind="noJavascript.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.noJavascript" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Kaneva No Javascript Enabled</title>
    <link rel="stylesheet" href="css/Kaneva.css" type="text/css" />
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout" topmargin="0" leftmargin="0">
	
    <form id="frmMain" method="post" runat="server">
		<table cellpadding="0" cellspacing="0" border="0" width="560">							
			<tr>
			<td valign="middle" class="belowFilter" colspan="2" width="268" height="23" align="left">
			
				<img runat="server" src="~/images/klogo.gif"/>
			
			</td>
			<td colspan="5" width="292" height="23" align="right" class="belowFilter2" valign="middle"></td>						
			</tr>
		</table>
		<BR><BR><BR>
		<CENTER>
		<table cellpadding="0" cellspacing="1" border="0" width="500" ID="Table2">
			<tr>
			<td class="bodyText" align="center"><br><br>
			<b style="font-size: 14px;">You are seeing this page because your internet browser currently does not support javascript. www.kaneva.com requires an 
				Internet browser setting that enables javascript.</B><BR><BR>

			Once you have set your browser to run javacript, close it, open a new browser 
			window and return to <A href="http://www.kaneva.com/">http://www.kaneva.com</A> to continue your digital entertainment experience.<br><br><br>

						
			</td>
			</tr>
		</table>
		
		</CENTER>
		<BR><BR><BR>
		
		<a href="#IE">Explorer</a>, <a href="#Netscape">Netscape</a>, <a href="#Mozilla">Mozilla</a>, and <a href="#AOL">AOL</a> browsers.</p> 
		
		<hr noshade size=1><div align="right"></div> 

<a name="IE"><b>Internet Explorer 3.X</b></a> 
<ol> 
<li>Select <i>Options</i> from the <i>View </i>menu. 
<li>Click <i>Security</i>. 
<li>Check <i>Enable Java Programs</i>. 
<li>Click <i>OK</i>. 
<li>Click <i>Reload</i>. 
</ol> 


<b>Internet Explorer 4.X</b> 
<ol> 
<li>Select <i>Internet Options</i> from the <i>View </i>menu. 
<li>Click the <i>Security</i> tab. 
<li>Click <i>Custom</i>. 
<li>Click <i>Settings</i>. 
<li>Scroll down to locate <i>Scripting</i>. 
<li>Click <i>Enable for Active Scripting</i>. 
<li>Click <i>OK</i>. 
<li>Click <i>Reload</i>. 
</ol> 


<b>Internet Explorer 5.X </b> 
<ol> 
<li>Open Internet Explorer. 
<li>Select <i>Internet Options</i> from the <i>Tools</i> menu. 
<li>In <i>Internet Options</i> dialog box select the <i>Security</i> tab. 
<li>Click Custom level button at bottom. The Security Settings dialog 
box will pop up. 
<li>Under Scripting category enable <i>Active Scripting</i>, <i>Allow paste options via script</i> and <i>Scripting of Java applets</i> 
<li>Click <i>OK</i> twice to close out. 
<li>Hit <i>Refresh</i>.</ol> 


<b>Internet Explorer 5.X for MACOSX</b> 
<ol> 
<li>Open Internet Explorer. 
<li>Select <i>Preferences</i> from the <i>Explorer</i> menu. 
<li>Click the arrow next to <i>Web Browser</i>. 
<li>Click <i>Web Content</i>. 
<li>Under <i>Active Content</i> check <i>Enable Scripting</i>. 
<li>Click <i>OK</i>. 
<li>Click <i>Refresh</i>. 
</ol> 

<b>Internet Explorer 5 for MACOS9</b> 
<ol> 
<li>Open Internet Explorer. 
<li>Select <i>Preferences</i> from the <i>Edit</i> menu. 
<li>Click the arrow next to <i>Web Browser</i>. 
<li>Click <i>Web Content</i>. 
<li>Under <i>Active Content</i> check <i>Enable Scripting</i>. 
<li>Click <i>OK</i>. 
<li>Click <i>Refresh</i>. 
</ol> 


<b>Internet Explorer 6.X </b> 
<ol> 
<li>Open Internet Explorer. 
<li>Select <i>Internet Options</i> from the <i>Tools</i> menu. 
<li>In <i>Internet Options</i> dialog box select the <i>Security</i> tab. 
<li>Click Custom level button at bottom. The Security settings dialog box will pop up. 
<li>Under Scripting category enable <i>Active Scripting</i>, <i>Allow paste options via script</i> and <i>Scripting of Java applets</i> 
<li>Click <i>OK</i> twice to close out. 
<li>Hit <i>Refresh</i>.</ol> 

<hr noshade size=1><div align="right"></div> 

<a name="Netscape"><b>Netscape 3.X</b> </a> 

<ol> 
<li>Select <i>Options</i> from the <i>Edit</i> menu. 
<li>Click <i>Network Preferences</i>. 
<li>Click <i>Languages</i>. 
<li>Check both <i>Enable Java</i> and <i>Enable JavaScript</i>. 
<li>Click <i>OK</i>. 
<li>Click <i>Reload</i>. 
</ol> 

<b>Netscape 4.X</b> 
<ol> 
<li>Open Netscape. 
<li>Select <i>Preferences</i> from the <i>Edit</i> menu. 
<li>Click <i>Advanced</i>. 
<li>Check both <i>Enable Java</i> and <i>Enable JavaScript</i> 
<li>Click <i>OK</i>. 
<li>Click <i>Reload</i>. 
</ol> 

<b>Netscape 4.X for MACOS9</b> 
<ol> 
<li>Open Netscape. 
<li>Select <i>Preferences</i> from the <i>Edit</i> menu. 
<li>Click <i>Advanced</i>. 
<li>Check both <i>Enable Java</i> and <i>Enable JavaScript</i> 
<li>Click <i>OK</i>. 
<li>Click <i>Reload</i>. 
</ol> 

<b>Netscape 6.X for MACOSX</b> 
<ol> 
<li>Open Netscape. 
<li>Select <i>Preferences</i> from the <i>Edit</i> menu. 
<li>Click <i>Advanced</i> 
<li>Check both <i>Enable Java</i> and <i>Enable JavaScript for Navigator</i> 
<li>Click <i>OK</i>. 
<li>Click <i>Reload</i>. 
</ol> 

<b>Netscape 7.X</b> 
<ol> 
<li>Open Netscape. 
<li>Select <i>Preferences</i> from the <i>Edit</i> menu. 
<li>Click the arrow next to <i>Advanced</i>. 
<li>Click <i>Scripts &amp; Plugins</i>. 
<li>Check <i>Navigator</i> beneath "Enable Javascript for". 
<li>Click <i>OK</i>. 
<li>Click <i>Reload</i>. 
</ol> 
		
<hr> 
<br><br> 
<a name="Mozilla"><b>Mozilla 1.X</b> </a> 
<ol> 
<li>Open Mozilla. 
<li>Select <i>Preferences</i> from the <i>Edit</i> menu. 
<li>Click the arrow next to <i>Advanced</i>. 
<li>Click <i>Scripts &amp; Plugins</i>. 
<li>Check <i>Navigator</i> beneath "Enable Javascript for". 
<li>Click <i>OK</i>. 
<li>Click <i>Reload</i>. 
</ol> 
		
		<hr noshade size=1><div align="right"></div> 

<a name="AOL"><b>AOL 3.0</b> </a> 

<ol> 
<li>Click <i>Prefs</i>. 
<li>Click <i>Security</i>. 
<li>Check <i>Enable Java Programs</i>, if the box is blank, single click on it. 
<li>Click <i>OK </i>. 
<li>Click the browser's <i>back</i> button, once back, click <i>reload</i>. 
</ol> 

<b>AOL 4.0 and 5.0</b> 
<ol> 
<li>Click <i>My AOL</i>. 
<li>Click <i>Preferences</i>. 
<li>Click <i>WWW</i>. 
<li>Click the <i>Secutity</i> tab. 
<li>Click <i>Custom</i>. 
<li>Click Settings. 
<li>Scroll down to locate <i>Scripting</i>. 
<li>Click <i>Enable for Active Scripting</i>. 
<li>Click <i>OK</i>, then click the <i>Reload</i> button. 
</ol> 


<b>Note to AOL 4.0 & 5.0 users</b><br><br> 

When using the use AOL browser, you may get a Javascript error but, when using an external browser (i.e., Netscape or Internet Explorer) this does not occur. AOL uses compressed graphics by Default. This doesn't allow Java to work while browsing inside AOL. (Additionally, this makes photos you view on web pages look less than optimal.)<br><br> 

Do the following: 
<ol> 
<li>Click <i>My AOL</i>. 
<li>Click <i>Preferences</i>. 
<li>Click the <i>WWW</i> icon. 
<li>Click <i>Web Graphics</i> tab. 
<li>Uncheck the box next to <i>Use Compressed Graphics</i>. 
<li>Click <i>OK</i> Restart the computer. 
</ol> 

    </form>
	
  </body>
</html>
