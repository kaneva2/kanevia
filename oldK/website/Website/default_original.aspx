<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default_original.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.DefaultWebForm" %>
<%@ Register TagPrefix="Kaneva" TagName="MKContainer" Src="usercontrols/doodad/ContainerMyKaneva.ascx" %>

<style type="text/css">
<!--
@import url("css/myKaneva_NEW.css");
-->
</style>


<div id="stage" style="text-align:left;">

	<!-- Left Column -->
	<div id="leftRail">
		
		<kaneva:mkcontainer id="MKcontainerProfile" runat="server" ControlToLoad="usercontrols/doodad/UserProfile.ascx" />
		<kaneva:mkcontainer id="MKcontainerActionItems" runat="server" ControlToLoad="usercontrols/doodad/ActionItems.ascx" />
		<kaneva:mkcontainer id="MKcontainerNotifications" runat="server" ControlToLoad="usercontrols/doodad/Notifications.ascx" />

	</div>

	<!-- Center Column -->
	<div id="centerRail">

		<kaneva:mkcontainer id="MKcontainerBlastsActive" runat="server" ControlToLoad="mykaneva/blast/BlastsActive.ascx" />
		
	</div>
	
	<!-- Right Column -->
	<div id="rightRail">
	
		<kaneva:mkcontainer id="MKcontainerFriendFinder" runat="server" ControlToLoad="usercontrols/doodad/FriendFinder.ascx" />
		<kaneva:mkcontainer id="MKcontainerFriendInviter" runat="server" ControlToLoad="usercontrols/doodad/FriendInviter.ascx" />
		<kaneva:mkcontainer id="MKcontainerEventsList" runat="server" ControlToLoad="usercontrols/doodad/EventsList.ascx" />
		<kaneva:mkcontainer id="MKcontainerPasses" runat="server" ControlToLoad="usercontrols/doodad/Passes.ascx" />
	
	</div>

</div>

