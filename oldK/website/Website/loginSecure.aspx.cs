///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Web.Security;
using System.Security.Principal;
using log4net;
using Facebook;
using System.Web.Script.Serialization;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using KlausEnt.KEP.Kaneva.channel;


namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for loginSecure.
	/// </summary>
    public class loginSecure : BasePage
    {
        protected loginSecure ()
        {
        }

        private void Page_Load (object sender, System.EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                // If the user is an automated test user, log them out.
                if (GetUserFacade.IsUserAutomatedTestAccount(KanevaWebGlobals.CurrentUser))
                {
                    LogoutSmokeTestUser();
                }
                else
                {
                    string redirectUrl = "http://" + KanevaGlobals.SiteName + "/default.aspx";
                    Response.Redirect(redirectUrl);
                }
            }

            //set the meta tags
            ((GenericLandingPageTemplate)Master).Title = "Kaneva. Step into a world of real friends and good times.";
            ((GenericLandingPageTemplate)Master).MetaDataDescription = "Already a member? Log in to Kaneva. Where your Profile, Friends, Media and favorite Communities are teleported into a modern-day 3D world where you can explore, socialize and experience entertainment in an entirely new way.";

            if (!IsPostBack)
            {
                if (Request.UrlReferrer != null)
                {
                    ViewState["rurl"] = Request.UrlReferrer.AbsoluteUri.ToString();
                }

                //this function should only be called if coming from the 
                //login form on the home page
				if ((PreviousPage != null) && 
                    ((PreviousPage.AppRelativeVirtualPath.IndexOf("Home.aspx") > 0) ||
                     (PreviousPage.AppRelativeVirtualPath.IndexOf ("Home_a.aspx") > 0) ||
                     (PreviousPage.AppRelativeVirtualPath.IndexOf("Home_b.aspx") > 0) ||
                     (PreviousPage.AppRelativeVirtualPath.IndexOf("loginsocial.aspx") > 0) ||
                     (PreviousPage is ProfileLandingPage) ||
                     (PreviousPage is assetDetailsLandingPage) ||
                     (PreviousPage is registerInfoKaneva) ||
                     (PreviousPage is CommunityPage)
                    ))
                {
                    PassedSignInCheck ();
                }

                // Check to see if user is loggin in with their Facebook Account
                if (Request["accessToken"] != null && Request["accessToken"].ToString().Length > 0)
                {
                    try
                    {
                        // Check if current FB user has a Kaneva account already connected
                        string accessToken = Request["accessToken"].ToString ();
                        bool isError = false;
                        string errMsg = "";
                        FacebookUser fbuser = GetSocialFacade.GetCurrentUser (accessToken, out isError, out errMsg);

                        if (fbuser.Id == 0)
                        {
                            errorbox.InnerText = "Login Failed: We could not authenticate your Facebook account.";
                            errorbox.Visible = true;
                        }
                        else
                        {
                            // check to see if user has already connected fb account
                            User user = GetUserFacade.GetUserByFacebookUserId (fbuser.Id);

                            // We will get a user object if there is a user account connected to this Facebook user id
                            if (user.UserId > 0)
                            {
                                GetUserFacade.SetFacebookAccessToken (fbuser.Id, accessToken);
                                // user's account is already connected with Facebook - login user
                                attemptLogin (user.Email); // (fbuser.Email);
                            }
                            else
                            {  // No Kaneva account connected to this Facebook account, now check to see if we have 
                                // an account that has a matching email
                                user = GetUserFacade.GetUserByEmail (fbuser.Email);

                                if (user.UserId > 0)
                                {
                                    // account not connected with facebook, but we do have a matching email
                                    Server.Transfer ("~/loginsocial.aspx?match=");
                                }
                                else  // could not find a matching email address
                                {
                                    Response.Redirect ("~/register/kaneva/registerinfo.aspx?accessToken=" + accessToken);
                                }
                            }
                        }
                    }
                    catch (ThreadAbortException)
                    {
                        // do nothing
                    }
                    catch (Exception ex)
                    {
                        m_logger.Error ("Error trying to login with Facebook account.", ex);
                    }
                }

                // Login from the SmokeTest user's temporary auth token.
                if (Request["stToken"] != null && Request["stToken"].ToString().Length > 0)
                {
                    try
                    {
                        // Check to see if this user is a valid SmokeTester user.
                        string accessToken = Request["stToken"].ToString();
                        User stUser = GetUserFacade.CheckSmokeTestUserLoginKey(accessToken);

                        if (stUser.UserId == 0 || !GetUserFacade.IsUserAutomatedTestAccount(stUser))
                        {
                            errorbox.InnerText = "Login Failed: Invalid test user account.";
                            errorbox.Visible = true;
                        }
                        else
                        {
                            chkRememberLogin.Checked = true;
                            attemptLogin(stUser.Email);
                        }
                    }
                    catch (Exception ex)
                    {
                        m_logger.Error("Error trying to login with SmokeTester account.", ex);
                    }
                }

                // Set the focus
                SetFocus (txtUserName);
                
                aLostPassword.HRef = ResolveUrl ("~/lostPassword.aspx");
                aRegister.HRef = ResolveUrl (KanevaGlobals.JoinLocation);
            }
        }

        protected void LogoutSmokeTestUser()
        {
            System.Web.HttpCookie aCookie;
            string bannerCookieName = KanevaWebGlobals.GetBannerCookieName();
            int limit = Request.Cookies.Count - 1;
            for (int i = limit; i != -1; i--)
            {
                aCookie = Request.Cookies[i];
                if (aCookie.Name != bannerCookieName)
                {
                    aCookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(aCookie);
                }
            }

            // Logout of single sign-on
            GetUserFacade.CleanupSingleSignOn(KanevaWebGlobals.CurrentUser.UserId);

            FormsAuthentication.SignOut();
            Session.Abandon();
        }

        /// <summary>
        /// Check if this is passed from another page
        /// </summary>    
        protected void PassedSignInCheck ()
        {
            try
            {   // Attempt to grab values from the previouspage's login form.  Should either be in the masterpage directly, or
                // in a Header.ascx usercontrol instance in the previouspage's master page.
                if (PreviousPage.Master != null && (PreviousPage.Master.FindControl ("loginForm") != null || PreviousPage.Master.FindControl("navDefault").FindControl("loginForm") != null))
                {
                    usercontrols.LoginForm _loginForm = null;

                    if (PreviousPage.Master.FindControl("loginForm") != null)
                    {
                        _loginForm = ((usercontrols.LoginForm)PreviousPage.Master.FindControl("loginForm"));
                    }
                    else
                    {
                        _loginForm = ((usercontrols.LoginForm)PreviousPage.Master.FindControl("navDefault").FindControl("loginForm"));
                    }

                    if (_loginForm != null)
                    {
                        // Getting the posted values from a property in the previous page isn't always reliable.
                        // In some strange instances, ASP.NET seems to assign a different UniqueID to the controls
                        // on the login form on cross-page post than it does on rendering. This occurs on registerInfo.aspx
                        // for example, but not on the homepage. To correct this, make sure we're getting the posted value
                        // for the right UniqueID.
                        string uNameRaw = Request.Form[Request.Form["loginForm_txtUserNameUID"].ToString()].ToString();
                        string passRaw = Request.Form[Request.Form["loginForm_txtPasswordUID"].ToString()].ToString();

                        txtUserName.Value = (_loginForm.Username == "" && uNameRaw != "" ? uNameRaw : _loginForm.Username);
                        txtPassword.Value = (_loginForm.PassWord == "" && passRaw != "" ? passRaw : _loginForm.PassWord);
                        chkRememberLogin.Checked = _loginForm.RememberMe;

                        attemptLogin ();
                    }
                }
                else
                {   // used for connecting facebook accounts (loginsocial.aspx)
                    usercontrols.LoginFormSocial _loginForm = ((usercontrols.LoginFormSocial) PreviousPage.FindControl ("loginForm"));

                    if (_loginForm != null)
                    {
                        txtUserName.Value = _loginForm.Username;
                        txtPassword.Value = _loginForm.PassWord;
                        chkRememberLogin.Checked = _loginForm.RememberMe;

                        ConnectToFacebook = true;

                        attemptLogin ();
                    }
                }
            }
            catch (ThreadAbortException)
            {
                // do nothing
            }
            catch (Exception ex)
            {
                m_logger.Warn ("Error trying to get value from login form.", ex);
            }

        }

        private void ShowPageErrors (bool isEmailErr, bool isPasswordErr, string err)
        {
            string errClassname = " error ";

            // Reset the labels
            lblEmail.Attributes["class"] = "";
            lblPassword.Attributes["class"] = "";

            // Reset the Fields
            txtUserName.Attributes["class"] = "";
            txtPassword.Attributes["class"] = "";

            if (isEmailErr)
            {
                lblEmail.Attributes["class"] = errClassname;
                txtUserName.Attributes["class"] = errClassname;
            }

            if (isPasswordErr)
            {
                lblPassword.Attributes["class"] = errClassname;
                txtPassword.Attributes["class"] = errClassname;
            }

            cvBlank.IsValid = false;
            cvBlank.ErrorMessage = "Login Failed: " + err;
        }

        /// <summary>
        /// The login click event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLogin_Click (Object sender, EventArgs e)
        {
            attemptLogin ();
        }

        protected void attemptLogin ()
        {
            attemptLogin (""); 
        }

        /// <summary>
        /// The login process
        /// </summary>
        protected void attemptLogin (string tokenUserEmail)
        {
            // Try to log in here
            int roleMembership = 0;
            UserFacade userFacade = new UserFacade();

            // Doing some checking because we have two forms on this page...
            string email = Server.HtmlEncode (txtUserName.Value);
            string password = Server.HtmlEncode (txtPassword.Value);
            // May want to persist login info later.
            bool bPersistLogin = chkRememberLogin.Checked;

            // If token email is passed in that means the user has logged into facebook and they
            // have a kaneva profile that is linked to their facebook account, or they're a SmokeTester user. Now we need to log them
            // into Kaneva.  Set these values so Authorize method will log them in accordingly
            bool isTokenAuthorized = false;
            if (tokenUserEmail.Length > 0)
            {
                email = tokenUserEmail;
                isTokenAuthorized = true;
            }

            int validLogin = UsersUtility.Authorize (email, password, 0, ref roleMembership, Common.GetVisitorIPAddress(), true, isTokenAuthorized);

            string results = "";
            cvBlank.IsValid = true;
            cvBlank.ErrorMessage = string.Empty;

            bool isEmailErr = false;
            bool isPasswordErr = false;

            switch (validLogin)
            {
                case 0:
                    results = "Not authenticated.";
                    break;
                case (int) Constants.eLOGIN_RESULTS.NOT_VALIDATED:
                    LoginUser (email, bPersistLogin);
                    break;
                case (int) Constants.eLOGIN_RESULTS.SUCCESS:
                    LoginUser (email, bPersistLogin);
                    break;
                case (int) Constants.eLOGIN_RESULTS.USER_NOT_FOUND:
                    userFacade.InsertUserLoginIssue(0, Common.GetVisitorIPAddress(), "Email address " + email + " not found", "INVALID_EMAIL", "WEB");
                    results = "Email address " + email + " was not found.";
                    isEmailErr = true;
                    SetFocus (txtUserName);
                    break;
                case (int) Constants.eLOGIN_RESULTS.INVALID_PASSWORD:
                    {
                        userFacade.InsertUserLoginIssue(0, Common.GetVisitorIPAddress(), "Invalid password for username " + email, "INVALID_PASSWORD", "WEB");
                        m_logger.Warn ("Failed login (invalid password) for email '" + email + "' from IP " + Common.GetVisitorIPAddress());
                        userFacade.UpdateFailedLogins (email);
                        results = "Invalid password.";
                        isPasswordErr = true;
                        SetFocus (txtPassword);
                        break;
                    }
                case (int) Constants.eLOGIN_RESULTS.NO_GAME_ACCESS:
                    results = "No access to this game.";
                    break;
                case (int) Constants.eLOGIN_RESULTS.ACCOUNT_DELETED:
                    results = "This account has been deleted.";
                    SetFocus (txtUserName);
                    break;
                case (int) Constants.eLOGIN_RESULTS.ACCOUNT_LOCKED:
                    {
                        userFacade.InsertUserLoginIssue(0, Common.GetVisitorIPAddress(), "Locked account username '" + email + "' tried to sign in", "BANNED", "WEB");
                        m_logger.Warn ("Locked account " + email + " tried to sign in from IP " + Common.GetVisitorIPAddress());
                        results = "This account has been locked by the Kaneva administrator";
                        SetFocus (txtUserName);
                        break;
                    }
                default:
                    results = "Not authenticated.";
                    break;
            }

            // Did they fail login?
            if (results.Length > 0)
            {
                ShowPageErrors (isEmailErr, isPasswordErr, results);
            }
        }

        /// <summary>
        /// LoginUser
        /// </summary>
        private void LoginUser (string email, bool bPersistLogin)
        {
            UserFacade userFacade = new UserFacade();
            DataRow drUser = UsersUtility.GetUserFromEmail (email);
            int userId = Convert.ToInt32 (drUser["user_id"]);
            FormsAuthentication.SetAuthCookie(userId.ToString (), bPersistLogin);

            // Add the Ideascale Cookie
            // What are the steps in implementing a Cookie based SSO?
            // - Your authentication system (after user logs in) must set a SESSION LEVEL (Expires when browser is closed) and a DOMAIN LEVEL cookie. You can name the Cookie anything you want. The value of the cookie should be a DES Encrypted and Base64 Encoded String of the user's email address. 
            // - Domain Level Cookie (Domain=.mycompany.com) 
            // - Session Level Cookie (Timeout = When Browser Closes) 
            // - Cookie Name (Anything You want) 
            // - Cookie Value (DES Encrypted + Base64 Encoded String of the user's email address) 

            HttpCookie aCookie = new HttpCookie ("IdeaAuth");
            aCookie.Expires = DateTime.Now.AddYears (1);  // Per Ideascalse only for browser session
            aCookie.Value = IdeaScaleEncrypt (email);
            aCookie.Domain = ".kaneva.com";
            Response.Cookies.Add (aCookie);

            int loginId = userFacade.UpdateLastLogin(userId, Common.GetVisitorIPAddress(), Server.MachineName);

            // Now take their current IP and and update their cookie.
            Response.Cookies["ipcheck"].Value = Common.GetVisitorIPAddress();
            Response.Cookies["ipcheck"].Expires = DateTime.Now.AddYears (1);

            // Set the userId in the session for keeping track of current users online
            Session["userId"] = userId;
            Session["loginId"] = loginId;

            // Temporarily store encrypted username and password in session to facilitate single sign-on.  
            userFacade.SetupSingleSignOn(userId, Server.HtmlEncode(txtUserName.Value), Server.HtmlEncode(txtPassword.Value));

            string url = "http://" + KanevaGlobals.SiteName + "/default.aspx";
            //string url = ResolveUrl ("~/default.aspx");

            // Forward them somewhere
            if (Request["logretURL"] != null)
            {
                url = "http://" + KanevaGlobals.SiteName + Server.UrlDecode(Request.QueryString.ToString()).Replace("logretURL=", "");
            }
            else if (Request["logretURLNH"] != null)
            {
                url = Server.UrlDecode(Request.QueryString.ToString()).Replace("logretURLNH=", "").Replace("https:", "http:");
            }
            // Handle login from parature
            else if (ViewState["rurl"] != null &&
                (
                   (ViewState["rurl"].ToString ().ToLower ().IndexOf ("s3.parature.com") > 0) ||
                   (ViewState["rurl"].ToString ().ToLower ().IndexOf ("support.kaneva.com") > 0)
                 ))
            {
                // Get the right info. NOTE : Add these to GetUserFromEmail later.
                User user = userFacade.GetUser (userId);

                url = Common.GetHelpURL (user.FirstName, user.LastName, email);
            }
            // Handle login from ideasclase
            else if (ViewState["rurl"] != null &&
                (ViewState["rurl"].ToString ().ToLower ().IndexOf ("ideas.kaneva.com") > 0)
            )
            {
                url = "http://ideas.kaneva.com";
            }
            // 0004331: Login from Homepage or Developer go to My Kaneva 
            else if (ViewState["rurl"] != null &&
                (
                    (ViewState["rurl"].ToString().ToLower().IndexOf("home.aspx") > 0) ||
                    (ViewState["rurl"].ToString().ToLower().IndexOf("home_a.aspx") > 0) ||
                    (ViewState["rurl"].ToString().ToLower().IndexOf("home_b.aspx") > 0) ||
                    (ViewState["rurl"].ToString().ToLower().IndexOf("free-virtual-world.kaneva") > 0) ||
                    (ViewState["rurl"].ToString().ToLower().IndexOf("home.kaneva") > 0) ||
                    (ViewState["rurl"].ToString().ToLower().EndsWith("www.kaneva.com")) ||
                    (ViewState["rurl"].ToString().ToLower().EndsWith("www.kaneva.com/")) ||
                    (ViewState["rurl"].ToString().ToLower().IndexOf("developer/default.aspx") > 0) ||
                    (ViewState["rurl"].ToString().ToLower().IndexOf("developer.kaneva.com") > 0)
                 ))
            {
                url = "http://" + KanevaGlobals.SiteName + "/default.aspx";
            }
            else if (ViewState["rurl"] != null &&
                ViewState["rurl"].ToString ().ToLower ().IndexOf ("register.aspx") < 0 &&
                ViewState["rurl"].ToString ().ToLower ().IndexOf ("registerinfo.aspx") < 0 &&
                ViewState["rurl"].ToString ().ToLower ().IndexOf ("loginsecure.aspx") < 0 &&
                ViewState["rurl"].ToString ().ToLower ().IndexOf ("lostpasswordchange.aspx") < 0)
            {
                //do this only when user is not coming from regiset page
                url = ViewState["rurl"].ToString ();
            }


            // Check to see if we need to display the interstitial on this login
            try
            {
                User currUser = userFacade.GetUser (userId);
                if (KanevaGlobals.LoginCountsToShowPromo.Length > 0 &&
                    currUser.Stats.NumberOfLogins <= KanevaGlobals.LoginCountsToShowPromo[KanevaGlobals.LoginCountsToShowPromo.Length - 1] &&
                    !currUser.HasVIPPass &&
                    (Request.Params["nopromo"] == null || Convert.ToBoolean(Request.Params["nopromo"]) == false))
                {
                    foreach (int i in KanevaGlobals.LoginCountsToShowPromo)
                    {
                        if (currUser.Stats.NumberOfLogins == i)
                        {
                            url = (ResolveUrl ("~/mykaneva/promo.aspx?url=") + url);
                            break;
                        }
                    }
                }
            }
            catch { }

            // if user logged in via Facebook and they are connecting an existing Kaneva account to 
            // their Facebook account, add their FB info to the database.  If user is already connected
            // then the accessToken will not be passed in the request object so the connect code will be skipped.
            // Skipping this code is normal behavior for use already connected and just logging in with FB account
            if (ConnectToFacebook)
            {
                if (Request["accessToken"] != null && Request["accessToken"].ToString ().Length > 0)
                {
                    try
                    {
                        string accessToken = Request["accessToken"].ToString ();
                        bool isError = false;
                        string errMsg = "";
                        FacebookUser fbuser = GetSocialFacade.GetCurrentUser (accessToken, out isError, out errMsg); 
                        if (GetUserFacade.ConnectUserToFacebook (userId, fbuser.Id, accessToken) > 0)
                        {
                            // Clear any Facebook cache
                            GetUserFacade.InvalidateKanevaUserFacebookCache (userId);
                            
                            // ** send them to the next step in Facebook connect which is invite your friends
                            url = ResolveUrl ("~/mykaneva/inviteFriend.aspx");
                        }
                    }
                    catch { }
                }
            }

            Response.Clear ();
            Response.AddHeader ("Refresh", string.Concat ("0;URL=", url));
            Response.Write ("<script language=\"javascript\">window.location.replace(\"");
            Response.Write (url);
            Response.Write ("\");</script>");
		    Response.End();

        }

        /// <summary>
        /// IdeaScaleEncrypt
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        private string IdeaScaleEncrypt (string plainText)
        {
            DESCryptoServiceProvider tds = new DESCryptoServiceProvider ();

            byte[] plainByte = Encoding.ASCII.GetBytes (plainText);

            // Set private key
            string key = "lksiekim";
            tds.Key = Encoding.ASCII.GetBytes (key);

            //tds.BlockSize = 64; Not specified for DES
            //tds.KeySize = 64;   Not specified for DES
            //tds.Padding = PaddingMode.Zeros;  Not specified for DES
            tds.IV = new byte[] { 0x0, 0x0, 0x0, 0x0,  
                0x0,0x0,0x0,0x0};
            tds.Mode = CipherMode.ECB;

            // Encryptor object
            ICryptoTransform cryptoTransform = tds.CreateEncryptor ();

            // Memory stream object
            MemoryStream ms = new MemoryStream ();

            // Crpto stream object
            CryptoStream cs = new CryptoStream (ms, cryptoTransform, CryptoStreamMode.Write);

            // Write encrypted byte to memory stream
            cs.Write (plainByte, 0, plainByte.Length);
            cs.FlushFinalBlock ();

            // Get the encrypted byte length
            byte[] cryptoByte = ms.ToArray ();

            ms.Close ();
            cs.Close ();

            // Convert into base 64
            return Convert.ToBase64String (cryptoByte);
        }

        private bool ConnectToFacebook
        {
            get { return _connectToFB; }
            set { _connectToFB = value; }
        }


        #region Declerations

        private bool _connectToFB = false;

        protected HtmlInputText txtUserName;
        protected HtmlInputText txtPassword;
        protected HtmlAnchor aLostPassword, aRegister, aUserName;
        protected HtmlContainerControl errorbox;
        protected ImageButton btnLogin2;
        protected LinkButton btnLogin;
        protected CheckBox chkRememberLogin;
        protected CustomValidator cvBlank;
        protected HtmlContainerControl lblEmail, lblPassword;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations

        #region Web Form Designer generated code
        override protected void OnInit (EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent ();
            base.OnInit (e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.Load += new System.EventHandler (this.Page_Load);
        }
        #endregion
    }
}
