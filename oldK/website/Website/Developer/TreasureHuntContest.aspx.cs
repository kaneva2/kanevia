///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
namespace KlausEnt.KEP.Kaneva.Developer
{
    public partial class TreasureHuntContest : SortedBasePage
    {
        protected TreasureHuntContest() 
		{
			Title = "Treasure Hunt Contest";
		}

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}