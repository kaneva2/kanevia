﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TreasureHuntContest.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.Developer.TreasureHuntContest" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		

<link href="../css/new.css" type="text/css" rel="stylesheet">

	    <link href="http://developer.kaneva.com/css/base/Base.css" type="text/css" rel="stylesheet" />
	    <link href="http://developer.kaneva.com/css/base/navigation.css" type="text/css" rel="stylesheet" />

 <link href="http://developer.kaneva.com/css/Brochure/Default.css" type="text/css" rel="stylesheet" />  


<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img id="Img1" runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img id="Img2" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr colspan="2">
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<img runat="server" src="~/images/treasure_hunt_contest_feb_banner.jpg"/>
												</td>
									
												
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="968">
										<tr>
											<td width="628" valign="top">

<p>
<h2>Enter the Treasure Hunt Contest for a chance to win</h2>
We're looking for a great 3D Treasure Hunt game. The most innovative, social, and fun games will be eligible for a total of $900 cash prizes and be showcased throughout Kaneva's 3D Virtual World.
</p>
    <p>
You start off with an easy-to-modify Treasure Hunt template.
</p>
<p>
<h2>Awards</h2>
The top three Contest entries are eligible for the following prizes:
<ul>
    <li>1st prize: $500</li>
    <li>2nd prize: $300</li>
    <li>3rd prize: $100</li>
    </ul>
</p>
<p>
<h2>Game Entry Specification</h2>
Check on Shop Kaneva for Zones with themes like Mystery, Horror, and Adventure. The Treasure Hunt template includes a wizard for dropping items and treasures.     
</p>
<p>
Treasure Hunt games published before midnight of February 14st are automatically entered.
</p>
<p>
<h2>Eligibility</h2>
The Treasure Hunt Challenge is open to all submissions. 
</p>

<p>
Additional Eligibility Requirements:
<ul>
    <li>A 3D game can only win once.  A Developer can repeatedly win with different games.</li>
    <li>All games/apps must be submitted to Kaneva, published, and publicly available by the February 14 deadline.</li>
    <li>A Kaneva account with a valid email address is necessary for winner notification.</li>
    <li>Entrants must be 13 years of age or older to participate (under 18 years of age must have a legal guardian approve and agree to the terms of this contest.)</li>
    <li>Kaneva reserves the right to disqualify any entry if the 3D App does not comply with the Developer Principles and Policies.</li>
    <li>Kaneva reserves the right to modify the Contest at any time; including but not limited to, extending the time, updating the rules and/or modifying the prizes to be awarded on completion of the contest.</li>
    <li>Employees of Kaneva are not eligible. </li>
    <li>Void where prohibited by law.</li>
    </ul>
    </p>
											</td>
											<td width="20"><img id="Img3" runat="server" src="~/images/spacer.gif" width="20" height="1" /></td>
											<td width="320" valign="top">
<p>		
<br />	<br />										
<p style="font-size:10pt;font-weight:bold;color:Black">Download the Kaneva 3D App Server</p>
<a id="downloadSetup" href="http://patch.kaneva.com/patchdata/Developer/Kaneva3DAppSetup.exe" onclick="javascript:_gaq.push(['_trackPageview', '/download/Kaneva3DAppSetup']);">
<img src="http://developer.kaneva.com/images/freedownload_257x50.png" width="257" height="50" border="0" />
</a>
</p>
<p>
You start off with a Treasure Hunt template for making a hidden object game.<br />
</p>
<p>
<h2>Judging</h2>
Kaneva will judge all entries based on  popularity, engagement, and gameplay.  Invite your friends to your game to increase your chances of winning.<br />
</p>
<p>
<h2>Timeline</h2>
<ol>
    <li>January : Competition starts</li>
    <li>February 14th: Final submission date.  Submissions close at 5PM (EDT).</li>
    <li>Winners announced February 20th</li>
</ol>
</p>
<p>
Have additional questions? Join the Kaneva 3D Apps Forums and ask!<br />
</p>
												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img id="Img8" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img id="Img9" runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
				  
